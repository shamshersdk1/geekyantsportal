<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Jobs\GatewayRequest;

class GatewayRequestSecondJobTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $data_to_post['arp']                    = $this->arpData();
        $data_to_post['connections']            = $this->connectionsData();
        $data_to_post['gateway_id']             = 2;
        $data_to_post['date']                   = date('Y-m-d');
        $data_to_post['hr']                     = date('H');
        $data_to_post['min']                    = date('i');
        $data_to_post['ts']                     = time();

        $gatewayrequest = new GatewayRequest(json_encode($data_to_post));
        $gatewayrequest->handle();

    }

    private function arpData(){
        return 'IP address       HW type     Flags       HW address            Mask     Device
10.1.136.116     0x1         0x0         4c:ed:fb:22:8a:06     *        eno1
10.1.240.101     0x1         0x0         20:a6:0c:32:c5:15     *        eno1
10.1.2.20        0x1         0x0         f0:18:98:57:07:45     *        eno1
10.1.191.248     0x1         0x0         f0:25:b7:fc:c2:7a     *        eno1
10.1.100.16      0x1         0x0         14:bd:61:f1:11:66     *        eno1
10.1.13.116      0x1         0x0         d4:61:9d:32:75:0c     *        eno1
10.1.14.30       0x1         0x0         98:01:a7:d5:20:1b     *        eno1
10.1.107.234     0x1         0x0         98:9c:57:ac:3f:d2     *        eno1
10.1.227.42      0x1         0x0         f8:95:ea:78:78:ca     *        eno1
10.1.174.225     0x1         0x0         f0:79:60:26:ce:58     *        eno1
10.1.165.45      0x1         0x0         c0:ee:fb:dc:70:12     *        eno1
54.152.208.245   0x1         0x6         fc:e3:3c:9d:4f:1f     *        enp1s0f0
10.1.104.201     0x1         0x0         a8:db:03:03:bd:a1     *        eno1
10.1.183.220     0x1         0x0         38:f9:d3:6e:fe:4a     *        eno1
10.1.1.22        0x1         0x2         00:17:61:12:aa:f1     *        eno1
10.1.11.94       0x1         0x0         f0:18:98:9a:f5:fc     *        eno1
10.1.131.171     0x1         0x0         0c:51:01:34:f2:d5     *        eno1
10.1.180.28      0x1         0x0         30:35:ad:ba:79:c8     *        eno1
10.1.205.59      0x1         0x0         70:4d:7b:1e:fb:de     *        eno1
10.1.161.16      0x1         0x0         58:40:4e:0b:32:86     *        eno1
10.1.13.16       0x1         0x0         98:e0:d9:94:d0:45     *        eno1
10.0.211.54      0x1         0x0         00:00:00:00:00:00     *        enp1s0f0
10.1.176.81      0x1         0x0         44:04:44:e1:f6:e3     *        eno1
10.1.214.237     0x1         0x0         64:b0:a6:ed:80:ec     *        eno1
10.1.11.195      0x1         0x0         f0:18:98:77:0b:1b     *        eno1
10.1.100.29      0x1         0x2         b0:4e:26:b3:35:ff     *        eno1
10.1.157.182     0x1         0x0         00:0a:f7:8e:01:ff     *        eno1
10.1.132.151     0x1         0x0         90:68:c3:e7:30:13     *        eno1
10.1.196.2       0x1         0x0         38:53:9c:25:91:61     *        eno1
10.1.128.98      0x1         0x0         c4:b3:01:95:b4:82     *        eno1
10.1.112.224     0x1         0x2         b0:4e:26:b3:39:9b     *        eno1
10.1.147.229     0x1         0x0         04:92:26:24:33:86     *        eno1
10.1.12.168      0x1         0x0         98:01:a7:bd:8e:b9     *        eno1
8.8.8.8          0x1         0x6         fc:e3:3c:9d:4f:1f     *        enp1s0f0
106.51.64.1      0x1         0x2         fc:e3:3c:9d:4f:1f     *        enp1s0f0
10.1.2.61        0x1         0x0         c0:ee:fb:56:45:0f     *        eno1
10.1.189.115     0x1         0x2         6c:c4:d5:6f:fa:3c     *        eno1
10.1.212.177     0x1         0x0         c0:9f:05:7d:68:07     *        eno1
10.1.12.18       0x1         0x0         f0:18:98:01:50:d2     *        eno1
10.1.11.104      0x1         0x0         d4:61:9d:30:42:5a     *        eno1
10.1.2.180       0x1         0x0         24:18:1d:dd:61:81     *        eno1
10.1.2.30        0x1         0x0         f4:0f:24:32:c6:28     *        eno1
10.1.190.126     0x1         0x0         1c:56:fe:f6:f6:dc     *        eno1
106.51.67.30     0x1         0x2         fc:e3:3c:9d:4f:1f     *        enp1s0f0
10.1.195.209     0x1         0x0         f0:76:6f:ed:fb:81     *        eno1
10.1.184.188     0x1         0x0         f0:18:98:6d:e5:3c     *        eno1
10.1.203.85      0x1         0x0         70:bb:e9:58:4b:f0     *        eno1
10.1.100.26      0x1         0x2         70:4f:57:33:4f:7a     *        eno1
10.1.245.121     0x1         0x0         04:d1:3a:bc:fd:9b     *        eno1
10.1.223.242     0x1         0x0         98:01:a7:c9:0a:47     *        eno1
10.1.174.98      0x1         0x2         a0:d7:95:9c:f9:0a     *        eno1
10.1.2.80        0x1         0x0         dc:a9:04:89:88:18     *        eno1
10.1.11.254      0x1         0x0         8c:85:90:73:6c:b3     *        eno1
10.1.172.56      0x1         0x0         08:c5:e1:b9:98:de     *        eno1
106.51.67.220    0x1         0x0         fc:e3:3c:9d:4f:1f     *        enp1s0f0
10.1.200.162     0x1         0x0         00:6f:64:54:d3:da     *        eno1
10.1.106.248     0x1         0x0         38:f9:d3:71:b5:09     *        eno1
10.1.111.36      0x1         0x2         70:4f:57:33:4f:7a     *        eno1
10.1.175.80      0x1         0x0         64:a2:f9:57:f2:4f     *        eno1
10.1.228.153     0x1         0x0         98:09:cf:54:7f:70     *        eno1
10.1.22.167      0x1         0x0         00:00:00:00:00:00     *        eno1
10.1.227.140     0x1         0x0         b8:c7:4a:b5:5a:cb     *        eno1
10.1.13.26       0x1         0x0         30:35:ad:ba:aa:ce     *        eno1
10.1.11.185      0x1         0x0         74:8d:08:29:e9:8f     *        eno1
10.1.2.40        0x1         0x0         f0:18:98:56:ee:fa     *        eno1
10.1.104.34      0x1         0x0         64:a2:f9:da:d6:de     *        eno1
10.1.13.196      0x1         0x0         30:35:ad:cc:4a:ec     *        eno1
10.1.10.22       0x1         0x0         f0:18:98:52:c1:f1     *        eno1
10.1.10.141      0x1         0x0         2c:f0:ee:1a:cf:78     *        eno1
10.1.177.0       0x1         0x0         44:80:eb:68:e5:9e     *        eno1
10.1.200.78      0x1         0x0         7c:46:85:19:c1:9a     *        eno1
10.1.251.195     0x1         0x0         38:53:9c:18:59:04     *        eno1
10.1.10.90       0x1         0x0         8c:85:90:75:80:3c     *        eno1
10.1.119.183     0x1         0x2         38:f9:d3:6e:c6:9e     *        eno1
10.1.248.21      0x1         0x2         00:b3:62:b8:17:3b     *        eno1
10.1.130.183     0x1         0x0         60:9a:c1:d0:2c:46     *        eno1
10.1.149.29      0x1         0x0         20:47:47:10:58:df     *        eno1
10.1.14.70       0x1         0x0         98:01:a7:d5:40:af     *        eno1
10.1.109.179     0x1         0x0         2c:33:61:07:b3:85     *        eno1
10.1.13.6        0x1         0x0         f4:5c:89:ad:18:bb     *        eno1
10.1.11.114      0x1         0x0         30:35:ad:ba:79:c8     *        eno1
10.1.250.215     0x1         0x2         00:24:32:16:72:3a     *        eno1
10.1.2.190       0x1         0x0         88:e9:fe:88:42:5b     *        eno1
10.1.251.142     0x1         0x0         00:6f:64:e6:53:2b     *        eno1
10.0.100.17      0x1         0x0         00:00:00:00:00:00     *        enp1s0f0
10.1.125.21      0x1         0x0         70:bb:e9:28:3f:2c     *        eno1
10.1.14.1        0x1         0x0         d4:61:9d:30:52:8e     *        eno1
10.1.151.86      0x1         0x0         04:d1:3a:a6:6e:2b     *        eno1
10.1.180.183     0x1         0x0         b4:cb:57:b6:41:b5     *        eno1
10.1.152.13      0x1         0x0         74:b5:87:27:70:b6     *        eno1
10.1.130.134     0x1         0x0         cc:9f:7a:3b:5d:d2     *        eno1
10.1.214.244     0x1         0x2         b0:4e:26:b3:37:c1     *        eno1
10.1.12.129      0x1         0x0         70:f0:87:bd:b1:7a     *        eno1
10.1.251.73      0x1         0x0         64:a2:f9:07:97:5a     *        eno1
10.1.243.191     0x1         0x0         cc:fd:17:f1:31:3a     *        eno1
10.1.245.16      0x1         0x0         3c:2e:f9:15:e2:21     *        eno1
10.1.232.26      0x1         0x0         e4:46:da:b7:ad:09     *        eno1
10.1.211.189     0x1         0x0         d4:61:9d:30:42:5a     *        eno1
10.0.232.250     0x1         0x0         00:00:00:00:00:00     *        enp1s0f0
10.1.214.226     0x1         0x0         d0:d7:83:d4:19:ed     *        eno1
10.1.12.226      0x1         0x0         84:10:0d:6c:24:82     *        eno1
10.1.108.150     0x1         0x2         68:c4:4d:70:88:6c     *        eno1
10.1.249.64      0x1         0x0         ac:5f:3e:8d:c6:d1     *        eno1
10.1.217.78      0x1         0x0         04:b1:67:c7:ae:b3     *        eno1
10.1.12.195      0x1         0x0         30:35:ad:bc:9c:5e     *        eno1
10.1.13.186      0x1         0x0         88:1f:a1:03:8c:02     *        eno1
10.1.229.4       0x1         0x0         4c:21:d0:24:57:a1     *        eno1
10.1.227.112     0x1         0x0         64:a2:f9:93:44:5e     *        eno1
10.1.2.101       0x1         0x0         8c:85:90:48:be:12     *        eno1
10.1.174.196     0x1         0x0         40:33:1a:b6:ba:10     *        eno1
10.1.210.213     0x1         0x0         68:fb:7e:99:f0:96     *        eno1
10.1.2.50        0x1         0x0         78:4f:43:4f:fd:fa     *        eno1
10.1.168.139     0x1         0x0         80:ce:b9:97:85:02     *        eno1
10.1.119.131     0x1         0x0         a0:d7:95:ca:25:ae     *        eno1
10.1.195.216     0x1         0x0         70:bb:e9:52:bf:e4     *        eno1
10.1.179.167     0x1         0x0         38:f9:d3:6e:13:92     *        eno1
10.1.131.122     0x1         0x0         38:f9:d3:72:df:3c     *        eno1
10.1.230.220     0x1         0x0         88:b4:a6:ca:4c:97     *        eno1
10.1.11.215      0x1         0x0         34:fc:ef:a3:90:12     *        eno1
10.1.152.129     0x1         0x0         18:31:bf:dc:8d:1e     *        eno1
10.1.161.252     0x1         0x0         20:a6:0c:ac:fe:10     *        eno1
10.1.10.151      0x1         0x0         f0:18:98:02:72:02     *        eno1
10.1.123.38      0x1         0x0         e0:62:67:74:12:33     *        eno1
10.1.104.247     0x1         0x0         2c:33:61:73:ab:6f     *        eno1
10.1.190.135     0x1         0x0         3c:2e:f9:75:e3:e2     *        eno1
10.1.13.166      0x1         0x0         d4:61:9d:34:c7:64     *        eno1
10.1.100.31      0x1         0x2         b0:4e:26:a1:03:f5     *        eno1
10.1.210.36      0x1         0x0         2c:33:61:34:6d:a9     *        eno1
10.1.30.21       0x1         0x0         08:6d:41:b7:64:88     *        eno1
10.1.161.84      0x1         0x2         20:a6:0c:90:db:0a     *        eno1
10.1.225.169     0x1         0x0         24:2e:02:3e:f8:11     *        eno1
10.1.158.64      0x1         0x0         64:a2:f9:c6:90:45     *        eno1
10.1.10.230      0x1         0x0         88:e9:fe:86:29:d9     *        eno1
10.1.175.85      0x1         0x0         80:ce:b9:58:55:ef     *        eno1
10.1.12.254      0x1         0x0         e0:ac:cb:82:ca:72     *        eno1
10.1.20.114      0x1         0x0         00:00:00:00:00:00     *        eno1
10.1.11.225      0x1         0x0         f0:18:98:56:e6:58     *        eno1
10.1.12.88       0x1         0x0         8c:85:90:48:a0:a4     *        eno1
10.1.4.5         0x1         0x2         b4:a3:82:c9:46:f0     *        eno1
10.1.105.171     0x1         0x2         38:f9:d3:71:ac:de     *        eno1
10.1.116.157     0x1         0x0         7c:76:68:7b:9c:00     *        eno1
10.1.10.11       0x1         0x0         f0:18:98:9e:30:6e     *        eno1
10.0.14.20       0x1         0x0         00:00:00:00:00:00     *        enp1s0f0
10.1.100.28      0x1         0x2         b0:4e:26:b3:37:c1     *        eno1
10.1.173.25      0x1         0x0         d0:a6:37:25:45:e7     *        eno1
10.1.171.85      0x1         0x0         38:53:9c:25:28:96     *        eno1
10.1.10.130      0x1         0x0         f0:18:98:37:cd:24     *        eno1
10.1.176.27      0x1         0x0         b0:e2:35:53:7d:3f     *        eno1
10.1.202.241     0x1         0x0         a4:12:32:ea:55:91     *        eno1
10.1.108.36      0x1         0x0         c8:3c:85:81:45:83     *        eno1
10.1.237.130     0x1         0x0         7c:04:d0:d5:45:be     *        eno1
10.1.2.60        0x1         0x0         f0:18:98:37:d2:fd     *        eno1
10.1.12.68       0x1         0x0         8c:85:90:73:63:c4     *        eno1
10.0.1.1         0x1         0x0         00:00:00:00:00:00     *        enp1s0f0
10.1.113.152     0x1         0x0         38:a4:ed:ce:87:09     *        eno1
10.1.141.208     0x1         0x2         08:f4:ab:cd:ef:f1     *        eno1
10.1.121.41      0x1         0x0         d4:61:9d:9c:35:ec     *        eno1
10.1.2.161       0x1         0x0         f0:18:98:83:20:ea     *        eno1
10.1.100.25      0x1         0x2         70:4f:57:81:79:25     *        eno1
10.1.216.52      0x1         0x0         80:ce:b9:66:3a:d5     *        eno1
10.1.149.62      0x1         0x2         b0:4e:26:a1:03:f5     *        eno1
10.1.171.111     0x1         0x2         d4:63:c6:9e:82:32     *        eno1
10.1.213.19      0x1         0x0         40:4d:7f:80:94:67     *        eno1
10.1.159.32      0x1         0x0         f4:f5:db:b0:f7:2b     *        eno1
10.1.207.132     0x1         0x0         18:f0:e4:aa:fc:5f     *        eno1
10.1.13.56       0x1         0x0         f0:79:60:2b:88:60     *        eno1
10.1.123.63      0x1         0x0         70:ec:e4:56:c2:31     *        eno1
10.1.125.173     0x1         0x2         64:a2:f9:93:46:89     *        eno1
10.1.217.67      0x1         0x0         dc:bf:e9:c0:ef:47     *        eno1
10.1.199.97      0x1         0x2         b8:41:a4:37:85:4e     *        eno1
10.1.144.33      0x1         0x2         10:94:bb:e9:ba:70     *        eno1
10.1.11.235      0x1         0x0         88:c9:d0:fc:90:fd     *        eno1
10.1.11.184      0x1         0x0         f0:18:98:42:52:d5     *        eno1
10.1.176.90      0x1         0x0         7c:76:68:5e:db:4d     *        eno1
10.1.112.251     0x1         0x2         b0:4e:26:b3:39:b6     *        eno1
10.1.158.21      0x1         0x0         98:01:a7:bc:56:9f     *        eno1
10.1.10.120      0x1         0x0         f0:18:98:02:76:0d     *        eno1
10.1.216.87      0x1         0x0         70:bb:e9:23:66:5c     *        eno1
10.1.12.80       0x1         0x0         74:8d:08:2f:5d:5a     *        eno1
10.1.163.133     0x1         0x2         b0:4e:26:b3:35:ff     *        eno1
10.1.161.241     0x1         0x0         a8:3e:0e:60:55:8e     *        eno1
10.1.230.72      0x1         0x0         f0:79:60:2b:89:52     *        eno1
10.1.12.215      0x1         0x0         f0:79:60:29:53:60     *        eno1
10.1.11.45       0x1         0x0         68:fb:7e:a9:06:b8     *        eno1
10.1.113.10      0x1         0x0         d8:63:75:48:b4:d3     *        eno1
10.1.111.169     0x1         0x0         90:68:c3:e5:51:09     *        eno1
10.1.122.168     0x1         0x0         d8:32:e3:29:e3:d4     *        eno1
10.1.100.20      0x1         0x0         00:00:00:00:00:00     *        eno1
10.1.140.88      0x1         0x0         94:65:2d:cc:24:95     *        eno1
10.1.12.78       0x1         0x0         8c:85:90:75:01:98     *        eno1
10.1.170.218     0x1         0x0         94:65:2d:ce:2d:3f     *        eno1
10.1.226.238     0x1         0x0         50:3c:ea:a2:e9:dd     *        eno1
10.1.10.100      0x1         0x0         8c:85:90:74:ee:e4     *        eno1
10.0.100.18      0x1         0x0         00:00:00:00:00:00     *        enp1s0f0
1.1.1.1          0x1         0x6         fc:e3:3c:9d:4f:1f     *        enp1s0f0
10.1.100.2       0x1         0x2         20:47:47:10:58:df     *        eno1
10.1.168.240     0x1         0x0         0c:f3:46:1c:ef:5d     *        eno1
10.1.14.0        0x1         0x0         f0:79:60:0c:7f:a4     *        eno1
10.1.134.29      0x1         0x0         a4:5e:60:1f:71:87     *        eno1
10.1.148.202     0x1         0x0         d8:32:e3:7c:42:0d     *        eno1
10.1.12.38       0x1         0x0         f0:18:98:56:73:66     *        eno1
10.1.12.128      0x1         0x0         88:e9:fe:86:18:e8     *        eno1
10.1.213.179     0x1         0x0         34:08:bc:d7:52:f3     *        eno1
10.1.242.148     0x1         0x0         6c:ab:31:42:53:1b     *        eno1
10.1.205.67      0x1         0x0         f4:0f:24:21:4e:8b     *        eno1
10.1.233.237     0x1         0x0         38:f9:d3:6e:f7:d0     *        eno1
10.1.151.250     0x1         0x0         7c:04:d0:d7:14:84     *        eno1
10.1.154.69      0x1         0x0         c0:ee:fb:80:14:6c     *        eno1
10.1.14.31       0x1         0x0         9c:f3:87:a2:dd:40     *        eno1
10.1.2.3         0x1         0x2         e4:e0:a6:9a:59:d3     *        eno1
10.1.189.121     0x1         0x0         64:a2:f9:c2:55:7e     *        eno1
10.1.11.126      0x1         0x0         f0:18:98:76:26:a9     *        eno1
10.1.103.138     0x1         0x0         40:4d:7f:b2:71:9e     *        eno1
10.1.2.100       0x1         0x0         88:19:08:c4:f4:a0     *        eno1
10.1.11.95       0x1         0x0         c4:9a:02:3e:a3:5a     *        eno1
10.0.190.135     0x1         0x0         00:00:00:00:00:00     *        enp1s0f0
10.1.241.112     0x1         0x0         fc:aa:b6:02:23:32     *        eno1
10.1.232.73      0x1         0x0         18:31:bf:dc:2b:b8     *        eno1
10.1.252.12      0x1         0x0         c8:3c:85:2c:d0:72     *        eno1
10.1.112.38      0x1         0x0         60:8e:08:a3:32:9e     *        eno1
10.1.10.150      0x1         0x0         58:40:4e:0c:49:55     *        eno1
10.1.198.131     0x1         0x0         ac:c3:3a:b7:8a:a6     *        eno1
10.1.142.140     0x1         0x0         ec:d0:9f:57:e7:7f     *        eno1
10.1.222.85      0x1         0x0         b4:cd:27:83:20:b7     *        eno1
10.1.212.218     0x1         0x0         38:ca:da:00:cb:74     *        eno1
10.1.14.79       0x1         0x0         48:bf:6b:d4:af:1e     *        eno1
10.1.100.30      0x1         0x2         b0:4e:26:b3:39:b6     *        eno1
10.1.11.174      0x1         0x0         10:94:bb:ee:92:b2     *        eno1
10.1.10.110      0x1         0x0         ac:bc:32:99:9e:77     *        eno1
10.1.121.233     0x1         0x0         a8:3e:0e:60:86:1d     *        eno1
10.1.13.96       0x1         0x2         a4:d1:8c:d2:36:5e     *        eno1
10.1.105.5       0x1         0x0         98:01:a7:c6:78:c1     *        eno1
10.1.226.111     0x1         0x0         f4:0f:24:32:04:04     *        eno1
10.1.183.110     0x1         0x0         64:a2:f9:d3:6f:41     *        eno1
10.1.30.2        0x1         0x0         a4:d1:8c:d0:7a:90     *        eno1
10.1.216.129     0x1         0x0         ac:c3:3a:b0:cd:d9     *        eno1
10.1.250.49      0x1         0x0         cc:61:e5:28:b9:8b     *        eno1
10.1.128.46      0x1         0x0         48:bf:6b:d3:f3:b4     *        eno1
10.1.2.181       0x1         0x0         dc:a9:04:85:cd:7f     *        eno1
10.1.11.224      0x1         0x0         24:18:1d:ac:75:19     *        eno1
10.1.210.207     0x1         0x2         d8:68:c3:81:4a:89     *        eno1
10.1.10.160      0x1         0x0         f0:18:98:2f:b2:87     *        eno1
10.1.235.187     0x1         0x0         24:df:6a:63:c7:ed     *        eno1
10.1.163.118     0x1         0x0         e4:46:da:89:4a:c9     *        eno1
10.1.150.112     0x1         0x0         f4:5c:89:ac:fa:e7     *        eno1
10.1.179.81      0x1         0x0         ac:d1:b8:cf:4d:31     *        eno1
10.1.190.131     0x1         0x0         6c:ab:31:40:c6:b3     *        eno1
10.1.12.235      0x1         0x0         f0:79:60:2b:9d:b2     *        eno1
10.1.211.61      0x1         0x0         ac:37:43:a2:7d:81     *        eno1
10.1.210.19      0x1         0x2         98:e0:d9:94:d0:87     *        eno1
10.1.195.188     0x1         0x0         28:ff:3c:3f:9a:2a     *        eno1
10.1.100.27      0x1         0x2         70:4f:57:33:4e:a6     *        eno1
10.1.12.98       0x1         0x0         f0:18:98:9d:a2:76     *        eno1
10.1.231.209     0x1         0x0         fc:aa:b6:03:93:e1     *        eno1
10.1.11.34       0x1         0x0         f0:18:98:9b:41:76     *        eno1
10.1.174.205     0x1         0x2         50:a6:7f:a1:f7:c1     *        eno1
10.1.176.123     0x1         0x2         ac:3c:0b:56:d3:dd     *        eno1
10.1.215.215     0x1         0x0         10:07:b6:36:57:6d     *        eno1
10.1.141.33      0x1         0x0         7c:78:7e:46:af:cd     *        eno1
10.1.238.255     0x1         0x0         9c:fc:01:a4:2a:37     *        eno1
10.0.13.237      0x1         0x0         00:00:00:00:00:00     *        enp1s0f0
10.1.220.71      0x1         0x0         98:28:a6:52:d2:f9     *        eno1
10.1.243.82      0x1         0x0         f0:d7:aa:5a:f6:0a     *        eno1
10.1.134.18      0x1         0x0         20:39:56:c8:69:b0     *        eno1
10.1.13.197      0x1         0x0         98:01:a7:d2:c5:67     *        eno1
10.1.171.247     0x1         0x0         98:01:a7:dd:59:e1     *        eno1
10.1.185.164     0x1         0x0         d8:c7:71:35:e9:11     *        eno1
10.1.4.1         0x1         0x2         b4:2e:99:13:31:08     *        eno1
10.1.148.70      0x1         0x0         3c:28:6d:19:70:fe     *        eno1
10.1.2.10        0x1         0x0         78:4f:43:71:ec:a5     *        eno1
10.1.14.51       0x1         0x0         ac:bc:32:b7:ab:03     *        eno1
10.1.164.126     0x1         0x0         e0:33:8e:23:db:22     *        eno1
10.1.213.47      0x1         0x0         64:cc:2e:1a:7e:73     *        eno1
10.1.162.71      0x1         0x2         70:4f:57:33:4e:a6     *        eno1
10.1.200.198     0x1         0x0         38:53:9c:14:71:c3     *        eno1
10.1.203.30      0x1         0x2         94:65:2d:96:74:5d     *        eno1
10.1.100.22      0x1         0x0         00:17:61:12:aa:f1     *        eno1
10.1.13.106      0x1         0x0         30:35:ad:bc:7a:0a     *        eno1
10.1.14.20       0x1         0x0         7c:04:d0:d4:72:9a     *        eno1
10.1.241.192     0x1         0x2         08:d4:6a:a6:b6:93     *        eno1
10.1.175.94      0x1         0x0         88:e9:fe:88:41:63     *        eno1
10.1.30.12       0x1         0x0         98:e0:d9:a4:ab:89     *        eno1
10.1.216.88      0x1         0x2         9c:30:5b:d1:a3:34     *        eno1
10.1.10.51       0x1         0x0         8c:85:90:72:0f:a7     *        eno1
10.1.161.194     0x1         0x0         98:0c:a5:d2:73:07     *        eno1
10.1.121.75      0x1         0x2         38:53:9c:c0:a3:62     *        eno1
10.1.222.143     0x1         0x0         98:01:a7:be:ef:21     *        eno1
10.1.204.122     0x1         0x0         d8:32:e3:56:df:d2     *        eno1
10.1.14.99       0x1         0x0         98:01:a7:ba:45:df     *        eno1
10.1.159.66      0x1         0x0         80:7a:bf:bb:5f:68     *        eno1
10.1.120.108     0x1         0x0         94:65:2d:75:fd:85     *        eno1
10.1.255.208     0x1         0x2         4c:bb:58:9b:8b:eb     *        eno1
10.1.179.51      0x1         0x0         70:ec:e4:29:84:02     *        eno1
10.1.100.32      0x1         0x2         b0:4e:26:b3:39:9b     *        eno1
10.1.224.217     0x1         0x0         c4:0b:cb:68:16:99     *        eno1
10.1.2.102       0x1         0x0         8c:85:90:56:43:64     *        eno1
10.1.226.87      0x1         0x0         98:01:a7:c6:c7:f3     *        eno1
10.1.191.61      0x1         0x0         d8:32:e3:5a:8f:52     *        eno1
10.1.158.68      0x1         0x0         f0:18:98:02:76:0d     *        eno1
10.1.200.91      0x1         0x0         28:ff:3c:36:46:49     *        eno1
10.1.30.7        0x1         0x0         98:e0:d9:7a:2f:59     *        eno1
10.1.193.120     0x1         0x0         f4:5c:89:af:24:61     *        eno1
10.1.2.170       0x1         0x0         08:6d:41:b7:56:f6     *        eno1
10.1.227.133     0x1         0x0         2c:33:61:79:6e:1c     *        eno1';
    }

    private function connectionsData(){
        return 'ipv4     2 tcp      6 52741 ESTABLISHED src=10.1.104.201 dst=74.125.200.188 sport=42022 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=42022 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 133932 ESTABLISHED src=10.1.216.129 dst=74.125.24.188 sport=34002 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=34002 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 139992 ESTABLISHED src=10.1.147.229 dst=74.125.68.188 sport=40320 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=40320 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 431855 ESTABLISHED src=127.0.0.1 dst=127.0.0.1 sport=52774 dport=389 src=127.0.0.1 dst=127.0.0.1 sport=389 dport=52774 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 35023 ESTABLISHED src=10.1.226.238 dst=74.125.68.188 sport=57366 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=57366 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 379721 ESTABLISHED src=10.1.112.38 dst=172.217.194.188 sport=38998 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=38998 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 141400 ESTABLISHED src=10.1.2.170 dst=74.125.68.188 sport=60451 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=60451 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 168222 ESTABLISHED src=10.1.195.216 dst=74.125.200.188 sport=47169 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=47169 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 149432 ESTABLISHED src=10.1.226.238 dst=74.125.200.188 sport=42554 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=42554 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 394033 ESTABLISHED src=10.1.12.128 dst=76.223.31.44 sport=57915 dport=443 src=76.223.31.44 dst=106.51.80.211 sport=443 dport=57915 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 123777 ESTABLISHED src=10.1.168.139 dst=172.217.194.188 sport=37902 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=37902 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 146474 ESTABLISHED src=10.1.250.49 dst=209.85.144.188 sport=41807 dport=5228 src=209.85.144.188 dst=106.51.80.211 sport=5228 dport=41807 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 384558 ESTABLISHED src=10.1.12.88 dst=54.249.82.168 sport=59733 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=59733 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 65756 ESTABLISHED src=10.1.2.50 dst=74.125.130.188 sport=51878 dport=443 src=74.125.130.188 dst=106.51.80.211 sport=443 dport=51878 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 70066 ESTABLISHED src=10.1.12.88 dst=54.249.82.168 sport=50164 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=50164 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 385442 ESTABLISHED src=10.1.13.6 dst=74.125.68.188 sport=52518 dport=443 src=74.125.68.188 dst=106.51.80.211 sport=443 dport=52518 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 409721 ESTABLISHED src=10.1.128.46 dst=54.249.82.169 sport=49892 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=49892 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 214290 ESTABLISHED src=10.1.212.177 dst=74.125.199.188 sport=45822 dport=443 src=74.125.199.188 dst=106.51.80.211 sport=443 dport=45822 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 394720 ESTABLISHED src=10.1.198.131 dst=74.125.200.188 sport=46239 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=46239 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 58764 ESTABLISHED src=10.1.200.78 dst=74.125.68.188 sport=46045 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=46045 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 58605 ESTABLISHED src=10.1.30.2 dst=74.125.68.188 sport=58462 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=58462 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 390551 ESTABLISHED src=10.1.131.122 dst=74.125.130.188 sport=49556 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=49556 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 148364 ESTABLISHED src=10.1.171.247 dst=74.125.130.188 sport=55387 dport=443 src=74.125.130.188 dst=106.51.80.211 sport=443 dport=55387 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 150001 ESTABLISHED src=10.1.250.49 dst=74.125.130.188 sport=48151 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=48151 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 420181 ESTABLISHED src=10.1.2.181 dst=74.125.68.188 sport=52847 dport=443 src=74.125.68.188 dst=106.51.80.211 sport=443 dport=52847 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 419778 ESTABLISHED src=10.1.158.68 dst=54.249.82.169 sport=63994 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=63994 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 139169 ESTABLISHED src=10.1.148.202 dst=74.125.130.188 sport=39329 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=39329 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 36769 ESTABLISHED src=10.1.226.238 dst=74.125.130.188 sport=48136 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=48136 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 414973 ESTABLISHED src=10.1.198.131 dst=74.125.68.188 sport=53174 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=53174 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 390999 ESTABLISHED src=10.1.12.128 dst=74.125.200.188 sport=51765 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=51765 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 63160 ESTABLISHED src=10.1.148.202 dst=74.125.130.188 sport=38638 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=38638 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 420046 ESTABLISHED src=10.1.2.181 dst=172.217.194.188 sport=64048 dport=443 src=172.217.194.188 dst=106.51.80.211 sport=443 dport=64048 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 221886 ESTABLISHED src=10.1.226.238 dst=209.85.144.188 sport=36326 dport=5228 src=209.85.144.188 dst=106.51.80.211 sport=5228 dport=36326 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 232280 ESTABLISHED src=10.1.226.238 dst=74.125.68.188 sport=53150 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=53150 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 124742 ESTABLISHED src=10.1.2.190 dst=172.217.194.188 sport=49252 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=49252 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 73006 ESTABLISHED src=10.1.158.68 dst=54.249.82.169 sport=51371 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=51371 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 409144 ESTABLISHED src=10.1.203.30 dst=74.125.200.188 sport=49032 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=49032 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 67669 ESTABLISHED src=10.1.216.87 dst=74.125.68.188 sport=45334 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=45334 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 420370 ESTABLISHED src=10.1.121.233 dst=74.125.68.188 sport=40448 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=40448 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 59942 ESTABLISHED src=10.1.148.70 dst=74.125.129.188 sport=40460 dport=5228 src=74.125.129.188 dst=106.51.80.211 sport=5228 dport=40460 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 413380 ESTABLISHED src=10.1.195.216 dst=74.125.200.188 sport=43829 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=43829 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 417650 ESTABLISHED src=10.1.128.98 dst=13.248.151.210 sport=52914 dport=443 src=13.248.151.210 dst=106.51.80.211 sport=443 dport=52914 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 148149 ESTABLISHED src=10.1.148.202 dst=172.217.194.188 sport=47157 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=47157 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 431998 ESTABLISHED src=10.1.4.5 dst=52.77.118.102 sport=50528 dport=6800 src=52.77.118.102 dst=106.51.80.211 sport=6800 dport=50528 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 148277 ESTABLISHED src=10.1.216.129 dst=74.125.200.188 sport=36824 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=36824 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 396824 ESTABLISHED src=10.1.116.157 dst=74.125.68.188 sport=36928 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=36928 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 417523 ESTABLISHED src=10.1.211.189 dst=13.248.151.210 sport=59099 dport=443 src=13.248.151.210 dst=106.51.80.211 sport=443 dport=59099 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 73012 ESTABLISHED src=10.1.158.68 dst=54.249.82.169 sport=51376 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=51376 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 67508 ESTABLISHED src=10.1.216.87 dst=74.125.68.188 sport=45292 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=45292 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 53356 ESTABLISHED src=10.1.195.216 dst=74.125.68.188 sport=42132 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=42132 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 404597 ESTABLISHED src=10.1.154.69 dst=74.125.200.188 sport=55885 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=55885 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 392739 ESTABLISHED src=10.1.189.121 dst=74.125.130.188 sport=46036 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=46036 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 50186 ESTABLISHED src=10.1.195.216 dst=74.125.200.188 sport=39428 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=39428 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 139202 ESTABLISHED src=10.1.235.187 dst=74.125.130.188 sport=46725 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=46725 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 384558 ESTABLISHED src=10.1.12.88 dst=54.249.82.169 sport=59745 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=59745 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 63360 ESTABLISHED src=10.1.2.190 dst=74.125.200.188 sport=56724 dport=443 src=74.125.200.188 dst=106.51.80.211 sport=443 dport=56724 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 226118 ESTABLISHED src=10.1.226.238 dst=74.125.68.188 sport=52856 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=52856 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 396744 ESTABLISHED src=10.1.168.139 dst=74.125.200.188 sport=58076 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=58076 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 431852 ESTABLISHED src=10.1.4.1 dst=52.220.65.93 sport=61879 dport=443 src=52.220.65.93 dst=106.51.80.211 sport=443 dport=61879 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 47522 ESTABLISHED src=10.1.195.216 dst=74.125.24.188 sport=39173 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=39173 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 72750 ESTABLISHED src=10.1.237.130 dst=151.101.152.106 sport=53959 dport=443 src=151.101.152.106 dst=106.51.80.211 sport=443 dport=53959 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 67968 ESTABLISHED src=10.1.175.85 dst=74.125.68.188 sport=55240 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=55240 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 417825 ESTABLISHED src=10.1.183.220 dst=54.249.82.168 sport=56242 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=56242 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 67211 ESTABLISHED src=10.1.195.216 dst=74.125.200.188 sport=43108 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=43108 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 158465 ESTABLISHED src=10.1.11.224 dst=74.125.130.188 sport=47868 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=47868 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 134417 ESTABLISHED src=10.1.12.88 dst=54.249.82.169 sport=65101 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=65101 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 130205 ESTABLISHED src=10.1.211.189 dst=74.125.68.188 sport=49512 dport=443 src=74.125.68.188 dst=106.51.80.211 sport=443 dport=49512 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 44635 ESTABLISHED src=10.1.113.152 dst=23.3.70.40 sport=37260 dport=80 src=23.3.70.40 dst=106.51.80.211 sport=80 dport=37260 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 400013 ESTABLISHED src=10.1.104.201 dst=52.89.124.203 sport=34468 dport=443 src=52.89.124.203 dst=106.51.80.211 sport=443 dport=34468 [ASSURED] mark=0 zone=0 use=2
ipv4     2 icmp     1 7 src=63.143.42.247 dst=106.51.80.211 type=8 code=0 id=7684 src=106.51.80.211 dst=63.143.42.247 type=0 code=0 id=7684 mark=0 zone=0 use=2
ipv4     2 tcp      6 71423 ESTABLISHED src=10.1.226.87 dst=54.249.82.169 sport=65510 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=65510 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 393237 ESTABLISHED src=10.1.122.168 dst=74.125.68.188 sport=49816 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=49816 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 53140 ESTABLISHED src=10.1.105.5 dst=54.249.82.168 sport=55125 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=55125 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 160633 ESTABLISHED src=10.1.171.111 dst=172.217.194.188 sport=48259 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=48259 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 415755 ESTABLISHED src=10.1.198.131 dst=74.125.130.188 sport=58636 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=58636 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 71533 ESTABLISHED src=10.1.147.229 dst=74.125.68.188 sport=45551 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=45551 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 380225 ESTABLISHED src=10.1.154.69 dst=74.125.68.188 sport=33336 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=33336 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 58269 ESTABLISHED src=10.1.14.31 dst=5.9.138.13 sport=65223 dport=80 src=5.9.138.13 dst=106.51.80.211 sport=80 dport=65223 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 54729 ESTABLISHED src=10.1.226.238 dst=74.125.68.188 sport=60624 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=60624 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 418322 ESTABLISHED src=10.1.216.87 dst=74.125.200.188 sport=43194 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=43194 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 47694 ESTABLISHED src=10.1.147.229 dst=74.125.24.188 sport=39436 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=39436 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 408050 ESTABLISHED src=10.1.12.128 dst=74.125.68.188 sport=65125 dport=443 src=74.125.68.188 dst=106.51.80.211 sport=443 dport=65125 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 134350 ESTABLISHED src=10.1.152.129 dst=74.125.199.188 sport=41587 dport=5228 src=74.125.199.188 dst=106.51.80.211 sport=5228 dport=41587 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 126181 ESTABLISHED src=10.1.2.190 dst=172.217.194.188 sport=49618 dport=443 src=172.217.194.188 dst=106.51.80.211 sport=443 dport=49618 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 133939 ESTABLISHED src=10.1.105.5 dst=54.249.82.169 sport=52744 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=52744 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 400615 ESTABLISHED src=10.1.203.85 dst=74.125.130.188 sport=48324 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=48324 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 167137 ESTABLISHED src=10.1.158.68 dst=54.249.82.169 sport=63564 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=63564 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 404409 ESTABLISHED src=10.1.30.2 dst=172.217.194.188 sport=63403 dport=443 src=172.217.194.188 dst=106.51.80.211 sport=443 dport=63403 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 47472 ESTABLISHED src=10.1.175.85 dst=74.125.68.188 sport=49848 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=49848 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 419693 ESTABLISHED src=10.1.195.216 dst=74.125.200.188 sport=37492 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=37492 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 419816 ESTABLISHED src=10.1.158.68 dst=54.249.82.169 sport=63978 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=63978 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 134416 ESTABLISHED src=10.1.12.88 dst=54.249.82.169 sport=65099 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=65099 [ASSURED] mark=0 zone=0 use=2
ipv4     2 icmp     1 29 src=106.51.80.211 dst=8.8.8.8 type=8 code=0 id=9749 src=8.8.8.8 dst=106.51.80.211 type=0 code=0 id=9749 mark=0 zone=0 use=2
ipv4     2 tcp      6 63967 ESTABLISHED src=10.1.2.40 dst=74.125.130.188 sport=59297 dport=443 src=74.125.130.188 dst=106.51.80.211 sport=443 dport=59297 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 56671 ESTABLISHED src=10.1.2.190 dst=172.217.194.188 sport=62492 dport=443 src=172.217.194.188 dst=106.51.80.211 sport=443 dport=62492 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 396076 ESTABLISHED src=10.1.165.45 dst=74.125.68.188 sport=45578 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=45578 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 71240 ESTABLISHED src=10.1.226.87 dst=54.249.82.168 sport=65340 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=65340 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 134627 ESTABLISHED src=10.1.175.85 dst=209.85.144.188 sport=35996 dport=5228 src=209.85.144.188 dst=106.51.80.211 sport=5228 dport=35996 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 431956 ESTABLISHED src=10.1.4.1 dst=40.90.189.152 sport=52801 dport=443 src=40.90.189.152 dst=106.51.80.211 sport=443 dport=52801 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 146387 ESTABLISHED src=10.1.216.129 dst=74.125.130.188 sport=60557 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=60557 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 136900 ESTABLISHED src=10.1.165.45 dst=74.125.24.188 sport=47256 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=47256 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 65129 ESTABLISHED src=10.1.2.101 dst=172.217.194.188 sport=63966 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=63966 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 164220 ESTABLISHED src=10.1.189.121 dst=172.217.194.188 sport=48074 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=48074 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 402053 ESTABLISHED src=10.1.152.129 dst=108.174.10.10 sport=42052 dport=443 src=108.174.10.10 dst=106.51.80.211 sport=443 dport=42052 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 47849 ESTABLISHED src=10.1.195.216 dst=74.125.68.188 sport=41148 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=41148 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 125788 ESTABLISHED src=10.1.30.2 dst=74.125.130.188 sport=59995 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=59995 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 167215 ESTABLISHED src=10.1.12.128 dst=172.217.194.188 sport=57991 dport=443 src=172.217.194.188 dst=106.51.80.211 sport=443 dport=57991 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 154023 ESTABLISHED src=10.1.226.238 dst=172.217.194.188 sport=36822 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=36822 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 134426 ESTABLISHED src=10.1.104.201 dst=74.125.68.188 sport=33448 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=33448 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 431991 ESTABLISHED src=10.1.4.1 dst=213.227.168.135 sport=61017 dport=80 src=213.227.168.135 dst=106.51.80.211 sport=80 dport=61017 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 142883 ESTABLISHED src=10.1.171.247 dst=74.125.68.188 sport=55277 dport=443 src=74.125.68.188 dst=106.51.80.211 sport=443 dport=55277 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 419813 ESTABLISHED src=10.1.158.68 dst=54.249.82.169 sport=64017 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=64017 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 133960 ESTABLISHED src=10.1.30.2 dst=74.125.68.188 sport=60190 dport=443 src=74.125.68.188 dst=106.51.80.211 sport=443 dport=60190 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 382346 ESTABLISHED src=10.1.122.168 dst=172.217.194.188 sport=39347 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=39347 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 149637 ESTABLISHED src=10.1.195.216 dst=74.125.24.188 sport=43398 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=43398 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 155094 ESTABLISHED src=10.1.227.140 dst=74.125.68.188 sport=47426 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=47426 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 44363 ESTABLISHED src=10.1.226.238 dst=74.125.200.188 sport=56636 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=56636 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 71464 ESTABLISHED src=10.1.226.87 dst=54.249.82.169 sport=49212 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=49212 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 167121 ESTABLISHED src=10.1.158.68 dst=54.249.82.169 sport=63553 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=63553 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 73007 ESTABLISHED src=10.1.158.68 dst=54.249.82.169 sport=51370 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=51370 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 418204 ESTABLISHED src=10.1.226.87 dst=54.249.82.169 sport=60654 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=60654 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 156345 ESTABLISHED src=10.1.11.126 dst=54.249.82.168 sport=61075 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=61075 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 61231 ESTABLISHED src=10.1.171.247 dst=74.125.130.188 sport=62613 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=62613 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 394991 ESTABLISHED src=10.1.198.131 dst=74.125.193.188 sport=33728 dport=5228 src=74.125.193.188 dst=106.51.80.211 sport=5228 dport=33728 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 239152 ESTABLISHED src=10.1.226.238 dst=74.125.200.188 sport=52316 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=52316 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 384558 ESTABLISHED src=10.1.12.88 dst=54.249.82.169 sport=59743 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=59743 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 72570 ESTABLISHED src=10.1.105.5 dst=54.249.82.169 sport=59876 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=59876 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 419788 ESTABLISHED src=10.1.158.68 dst=54.249.82.169 sport=64016 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=64016 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 119484 ESTABLISHED src=10.1.154.69 dst=74.125.24.188 sport=40279 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=40279 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 159289 ESTABLISHED src=10.1.128.46 dst=54.249.82.168 sport=52601 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=52601 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 419783 ESTABLISHED src=10.1.158.68 dst=54.249.82.169 sport=63984 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=63984 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 71424 ESTABLISHED src=10.1.226.87 dst=54.249.82.169 sport=65515 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=65515 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 417837 ESTABLISHED src=10.1.116.157 dst=74.125.193.188 sport=47046 dport=5228 src=74.125.193.188 dst=106.51.80.211 sport=5228 dport=47046 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 70357 ESTABLISHED src=10.1.128.46 dst=54.249.82.169 sport=57750 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=57750 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 409743 ESTABLISHED src=10.1.216.87 dst=23.62.12.32 sport=38517 dport=80 src=23.62.12.32 dst=106.51.80.211 sport=80 dport=38517 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 389635 ESTABLISHED src=10.1.12.128 dst=74.125.200.188 sport=54005 dport=443 src=74.125.200.188 dst=106.51.80.211 sport=443 dport=54005 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 63101 ESTABLISHED src=10.1.171.247 dst=172.217.194.188 sport=64217 dport=443 src=172.217.194.188 dst=106.51.80.211 sport=443 dport=64217 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 150935 ESTABLISHED src=10.1.232.26 dst=74.125.68.188 sport=34090 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=34090 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 49194 ESTABLISHED src=10.1.180.183 dst=74.125.24.188 sport=39660 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=39660 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 415124 ESTABLISHED src=10.1.226.111 dst=172.217.194.188 sport=59412 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=59412 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 420041 ESTABLISHED src=10.1.2.181 dst=54.249.82.169 sport=52799 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=52799 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 74793 ESTABLISHED src=10.1.213.47 dst=74.125.68.188 sport=45008 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=45008 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 167130 ESTABLISHED src=10.1.158.68 dst=54.249.82.168 sport=63391 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=63391 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 39374 ESTABLISHED src=10.1.2.190 dst=74.125.24.188 sport=55872 dport=443 src=74.125.24.188 dst=106.51.80.211 sport=443 dport=55872 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 378714 ESTABLISHED src=10.1.232.26 dst=74.125.68.188 sport=40003 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=40003 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 159289 ESTABLISHED src=10.1.128.46 dst=54.249.82.168 sport=52602 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=52602 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 71240 ESTABLISHED src=10.1.226.87 dst=54.249.82.168 sport=65339 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=65339 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 32769 ESTABLISHED src=10.1.232.26 dst=74.125.68.188 sport=58861 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=58861 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 409036 ESTABLISHED src=10.1.2.190 dst=74.125.200.188 sport=59631 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=59631 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 153132 ESTABLISHED src=10.1.165.45 dst=172.217.192.188 sport=44517 dport=5228 src=172.217.192.188 dst=106.51.80.211 sport=5228 dport=44517 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 62430 ESTABLISHED src=10.1.116.157 dst=74.125.24.188 sport=47592 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=47592 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 65351 ESTABLISHED src=10.1.226.238 dst=64.233.186.188 sport=52994 dport=5228 src=64.233.186.188 dst=106.51.80.211 sport=5228 dport=52994 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 125922 ESTABLISHED src=10.1.211.189 dst=74.125.68.188 sport=49476 dport=443 src=74.125.68.188 dst=106.51.80.211 sport=443 dport=49476 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 239824 ESTABLISHED src=10.1.226.238 dst=74.125.68.188 sport=55254 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=55254 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 384558 ESTABLISHED src=10.1.12.88 dst=54.249.82.168 sport=59736 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=59736 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 416938 ESTABLISHED src=10.1.12.88 dst=54.249.82.169 sport=60717 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=60717 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 72570 ESTABLISHED src=10.1.105.5 dst=54.249.82.169 sport=59875 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=59875 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 63767 ESTABLISHED src=10.1.228.153 dst=172.217.194.188 sport=39406 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=39406 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 50773 ESTABLISHED src=10.1.2.190 dst=74.125.24.188 sport=62349 dport=443 src=74.125.24.188 dst=106.51.80.211 sport=443 dport=62349 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 154483 ESTABLISHED src=10.1.183.220 dst=54.249.82.169 sport=54447 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=54447 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 409743 ESTABLISHED src=10.1.216.87 dst=23.62.12.32 sport=38523 dport=80 src=23.62.12.32 dst=106.51.80.211 sport=80 dport=38523 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 56341 ESTABLISHED src=10.1.195.216 dst=172.217.194.188 sport=37845 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=37845 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 136970 ESTABLISHED src=10.1.116.157 dst=74.125.68.188 sport=37054 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=37054 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 151943 ESTABLISHED src=10.1.2.190 dst=74.125.200.188 sport=57338 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=57338 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 156345 ESTABLISHED src=10.1.11.126 dst=54.249.82.168 sport=61074 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=61074 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 411749 ESTABLISHED src=10.1.2.181 dst=74.125.200.188 sport=62776 dport=443 src=74.125.200.188 dst=106.51.80.211 sport=443 dport=62776 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 392027 ESTABLISHED src=10.1.211.189 dst=74.125.68.188 sport=53247 dport=443 src=74.125.68.188 dst=106.51.80.211 sport=443 dport=53247 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 412093 ESTABLISHED src=10.1.122.168 dst=74.125.24.188 sport=46713 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=46713 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 60633 ESTABLISHED src=10.1.2.161 dst=54.249.82.169 sport=55928 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=55928 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 58716 ESTABLISHED src=10.1.200.78 dst=74.125.24.188 sport=39362 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=39362 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 409743 ESTABLISHED src=10.1.216.87 dst=23.62.12.32 sport=38525 dport=80 src=23.62.12.32 dst=106.51.80.211 sport=80 dport=38525 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 388860 ESTABLISHED src=10.1.211.189 dst=74.125.130.188 sport=64754 dport=443 src=74.125.130.188 dst=106.51.80.211 sport=443 dport=64754 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 159519 ESTABLISHED src=10.1.226.87 dst=54.249.82.169 sport=59553 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=59553 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 134212 ESTABLISHED src=10.1.222.85 dst=74.125.24.188 sport=40898 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=40898 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 66887 ESTABLISHED src=10.1.148.202 dst=74.125.68.188 sport=48863 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=48863 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 133593 ESTABLISHED src=10.1.12.128 dst=74.125.68.188 sport=49488 dport=443 src=74.125.68.188 dst=106.51.80.211 sport=443 dport=49488 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 134639 ESTABLISHED src=10.1.235.187 dst=74.125.68.188 sport=47330 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=47330 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 68490 ESTABLISHED src=10.1.216.87 dst=74.125.20.188 sport=41685 dport=443 src=74.125.20.188 dst=106.51.80.211 sport=443 dport=41685 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 167121 ESTABLISHED src=10.1.158.68 dst=54.249.82.169 sport=63549 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=63549 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 148888 ESTABLISHED src=10.1.229.4 dst=172.217.194.188 sport=54941 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=54941 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 391185 ESTABLISHED src=10.1.172.56 dst=54.249.82.169 sport=42926 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=42926 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 409432 ESTABLISHED src=10.1.174.196 dst=54.249.82.169 sport=50310 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=50310 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 378448 ESTABLISHED src=10.1.225.169 dst=74.125.68.188 sport=37487 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=37487 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 167153 ESTABLISHED src=10.1.158.68 dst=54.249.82.169 sport=63461 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=63461 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 67454 ESTABLISHED src=10.1.195.216 dst=74.125.130.188 sport=47486 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=47486 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 419777 ESTABLISHED src=10.1.158.68 dst=54.249.82.169 sport=63995 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=63995 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 132342 ESTABLISHED src=10.1.226.238 dst=74.125.68.188 sport=42728 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=42728 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 147875 ESTABLISHED src=10.1.12.128 dst=54.249.82.169 sport=55646 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=55646 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 154379 ESTABLISHED src=10.1.171.247 dst=172.217.194.188 sport=59803 dport=443 src=172.217.194.188 dst=106.51.80.211 sport=443 dport=59803 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 404611 ESTABLISHED src=10.1.200.78 dst=209.85.200.188 sport=54868 dport=443 src=209.85.200.188 dst=106.51.80.211 sport=443 dport=54868 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 150342 ESTABLISHED src=10.1.2.180 dst=74.125.24.188 sport=47408 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=47408 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 404461 ESTABLISHED src=10.1.222.85 dst=172.217.194.188 sport=54646 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=54646 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 431958 ESTABLISHED src=10.1.4.1 dst=74.125.24.188 sport=52608 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=52608 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 419773 ESTABLISHED src=10.1.158.68 dst=54.249.82.169 sport=63985 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=63985 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 50855 ESTABLISHED src=10.1.116.157 dst=172.217.194.188 sport=57472 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=57472 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 71498 ESTABLISHED src=10.1.128.98 dst=76.223.31.44 sport=53860 dport=443 src=76.223.31.44 dst=106.51.80.211 sport=443 dport=53860 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 146387 ESTABLISHED src=10.1.148.202 dst=74.125.199.188 sport=38707 dport=443 src=74.125.199.188 dst=106.51.80.211 sport=443 dport=38707 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 244197 ESTABLISHED src=10.1.175.85 dst=74.125.68.188 sport=33438 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=33438 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 57074 ESTABLISHED src=10.1.235.187 dst=74.125.130.188 sport=48147 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=48147 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 414667 ESTABLISHED src=10.1.175.85 dst=74.125.200.188 sport=46528 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=46528 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 382659 ESTABLISHED src=10.1.216.129 dst=74.125.68.188 sport=37383 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=37383 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 62818 ESTABLISHED src=10.1.148.202 dst=74.125.68.188 sport=48003 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=48003 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 134417 ESTABLISHED src=10.1.12.88 dst=54.249.82.168 sport=65105 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=65105 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 150162 ESTABLISHED src=10.1.203.30 dst=74.125.130.188 sport=46841 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=46841 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 412751 ESTABLISHED src=10.1.203.85 dst=74.125.68.188 sport=44138 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=44138 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 47345 ESTABLISHED src=10.1.189.121 dst=74.125.24.188 sport=39468 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=39468 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 147875 ESTABLISHED src=10.1.12.128 dst=54.249.82.169 sport=55647 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=55647 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 431095 ESTABLISHED src=10.1.248.21 dst=17.242.12.76 sport=49364 dport=5223 src=17.242.12.76 dst=106.51.80.211 sport=5223 dport=49364 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 51080 ESTABLISHED src=10.1.10.230 dst=172.217.194.188 sport=57363 dport=443 src=172.217.194.188 dst=106.51.80.211 sport=443 dport=57363 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 46679 ESTABLISHED src=10.1.131.122 dst=54.249.82.168 sport=49730 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=49730 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 412220 ESTABLISHED src=10.1.106.248 dst=34.197.12.217 sport=56268 dport=443 src=34.197.12.217 dst=106.51.80.211 sport=443 dport=56268 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 413150 ESTABLISHED src=10.1.222.85 dst=74.125.24.188 sport=49164 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=49164 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 391540 ESTABLISHED src=10.1.12.128 dst=74.125.200.188 sport=54377 dport=443 src=74.125.200.188 dst=106.51.80.211 sport=443 dport=54377 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 70066 ESTABLISHED src=10.1.12.88 dst=54.249.82.168 sport=50161 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=50161 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 419771 ESTABLISHED src=10.1.168.139 dst=172.217.194.188 sport=52690 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=52690 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 64151 ESTABLISHED src=10.1.195.216 dst=74.125.130.188 sport=46246 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=46246 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 390116 ESTABLISHED src=10.1.211.189 dst=74.125.200.188 sport=65310 dport=443 src=74.125.200.188 dst=106.51.80.211 sport=443 dport=65310 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 417652 ESTABLISHED src=10.1.131.122 dst=54.249.82.169 sport=50776 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=50776 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 166717 ESTABLISHED src=10.1.249.64 dst=74.125.200.188 sport=37777 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=37777 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 378479 ESTABLISHED src=10.1.225.169 dst=209.85.144.188 sport=46769 dport=5228 src=209.85.144.188 dst=106.51.80.211 sport=5228 dport=46769 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 134403 ESTABLISHED src=10.1.12.88 dst=74.125.200.188 sport=56280 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=56280 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 397114 ESTABLISHED src=10.1.116.157 dst=74.125.130.188 sport=50612 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=50612 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 409693 ESTABLISHED src=10.1.128.46 dst=74.125.130.188 sport=63124 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=63124 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 47534 ESTABLISHED src=10.1.13.6 dst=172.217.194.188 sport=55959 dport=443 src=172.217.194.188 dst=106.51.80.211 sport=443 dport=55959 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 384558 ESTABLISHED src=10.1.12.88 dst=54.249.82.168 sport=59741 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=59741 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 72694 ESTABLISHED src=10.1.113.10 dst=74.125.68.188 sport=50945 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=50945 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 67861 ESTABLISHED src=10.1.216.87 dst=172.217.194.188 sport=47591 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=47591 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 158097 ESTABLISHED src=10.1.12.88 dst=54.249.82.168 sport=58152 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=58152 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 64032 ESTABLISHED src=10.1.216.129 dst=74.125.68.188 sport=52726 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=52726 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 148012 ESTABLISHED src=10.1.11.225 dst=54.249.82.168 sport=60093 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=60093 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 64831 ESTABLISHED src=10.1.13.6 dst=172.217.194.188 sport=56366 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=56366 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 415943 ESTABLISHED src=10.1.195.216 dst=74.125.68.188 sport=41024 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=41024 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 393396 ESTABLISHED src=10.1.12.68 dst=74.125.130.188 sport=57533 dport=443 src=74.125.130.188 dst=106.51.80.211 sport=443 dport=57533 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 415121 ESTABLISHED src=10.1.198.131 dst=74.125.24.188 sport=60400 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=60400 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 71464 ESTABLISHED src=10.1.226.87 dst=54.249.82.169 sport=49208 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=49208 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 410782 ESTABLISHED src=10.1.12.128 dst=74.125.68.188 sport=59301 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=59301 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 394903 ESTABLISHED src=10.1.226.111 dst=172.217.194.188 sport=49325 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=49325 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 388861 ESTABLISHED src=10.1.172.56 dst=172.217.194.188 sport=38676 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=38676 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 384874 ESTABLISHED src=10.1.30.2 dst=74.125.130.188 sport=62755 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=62755 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 222928 ESTABLISHED src=10.1.226.238 dst=74.125.200.188 sport=49486 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=49486 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 63853 ESTABLISHED src=10.1.131.122 dst=74.125.68.188 sport=51447 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=51447 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 70928 ESTABLISHED src=10.1.226.87 dst=54.249.82.168 sport=64141 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=64141 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 417276 ESTABLISHED src=10.1.11.126 dst=54.249.82.169 sport=58704 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=58704 [ASSURED] mark=0 zone=0 use=2
ipv4     2 udp      17 121 src=10.1.4.1 dst=172.217.163.131 sport=58241 dport=443 src=172.217.163.131 dst=106.51.80.211 sport=443 dport=58241 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 156037 ESTABLISHED src=10.1.12.215 dst=54.249.82.169 sport=60747 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=60747 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 384558 ESTABLISHED src=10.1.12.88 dst=54.249.82.168 sport=59740 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=59740 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 417877 ESTABLISHED src=10.1.11.195 dst=74.125.200.188 sport=49253 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=49253 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 132962 ESTABLISHED src=10.1.175.85 dst=74.125.68.188 sport=42232 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=42232 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 153123 ESTABLISHED src=10.1.11.195 dst=172.217.194.188 sport=63448 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=63448 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 152936 ESTABLISHED src=10.1.222.85 dst=74.125.130.188 sport=48928 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=48928 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 419785 ESTABLISHED src=10.1.12.215 dst=13.248.151.210 sport=51746 dport=443 src=13.248.151.210 dst=106.51.80.211 sport=443 dport=51746 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 397244 ESTABLISHED src=10.1.195.216 dst=74.125.24.188 sport=38424 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=38424 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 406152 ESTABLISHED src=10.1.216.129 dst=74.125.199.188 sport=49629 dport=443 src=74.125.199.188 dst=106.51.80.211 sport=443 dport=49629 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 142483 ESTABLISHED src=10.1.171.247 dst=74.125.68.188 sport=55245 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=55245 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 168252 ESTABLISHED src=10.1.189.121 dst=172.217.194.188 sport=42664 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=42664 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 56822 ESTABLISHED src=10.1.211.189 dst=74.125.24.188 sport=63750 dport=443 src=74.125.24.188 dst=106.51.80.211 sport=443 dport=63750 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 46837 ESTABLISHED src=10.1.13.186 dst=76.223.31.44 sport=64499 dport=443 src=76.223.31.44 dst=106.51.80.211 sport=443 dport=64499 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 395879 ESTABLISHED src=10.1.104.201 dst=74.125.130.188 sport=56492 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=56492 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 48071 ESTABLISHED src=10.1.235.187 dst=172.217.194.188 sport=49429 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=49429 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 420471 ESTABLISHED src=10.1.189.121 dst=172.217.194.188 sport=39678 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=39678 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 417670 ESTABLISHED src=10.1.235.187 dst=74.125.68.188 sport=37493 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=37493 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 44160 ESTABLISHED src=10.1.12.215 dst=172.217.194.188 sport=53541 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=53541 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 394937 ESTABLISHED src=10.1.226.111 dst=13.76.142.210 sport=49293 dport=443 src=13.76.142.210 dst=106.51.80.211 sport=443 dport=49293 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 71036 ESTABLISHED src=10.1.11.225 dst=54.249.82.169 sport=57229 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=57229 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 417650 ESTABLISHED src=10.1.128.98 dst=13.248.151.210 sport=52913 dport=443 src=13.248.151.210 dst=106.51.80.211 sport=443 dport=52913 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 395954 ESTABLISHED src=10.1.131.122 dst=40.90.189.152 sport=49679 dport=443 src=40.90.189.152 dst=106.51.80.211 sport=443 dport=49679 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 146408 ESTABLISHED src=10.1.122.168 dst=172.217.194.188 sport=43413 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=43413 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 134037 ESTABLISHED src=10.1.122.168 dst=74.125.68.188 sport=39601 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=39601 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 240113 ESTABLISHED src=10.1.226.238 dst=172.217.194.188 sport=45720 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=45720 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 72664 ESTABLISHED src=10.1.241.192 dst=74.125.68.188 sport=40384 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=40384 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 64635 ESTABLISHED src=10.1.195.216 dst=74.125.200.188 sport=42536 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=42536 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 147874 ESTABLISHED src=10.1.12.128 dst=54.249.82.169 sport=55626 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=55626 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 68635 ESTABLISHED src=10.1.11.126 dst=54.249.82.169 sport=53179 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=53179 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 130198 ESTABLISHED src=10.1.2.190 dst=74.125.200.188 sport=49975 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=49975 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 159634 ESTABLISHED src=10.1.105.5 dst=54.249.82.169 sport=55828 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=55828 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 159634 ESTABLISHED src=10.1.105.5 dst=54.249.82.169 sport=55829 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=55829 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 57007 ESTABLISHED src=10.1.2.190 dst=74.125.68.188 sport=55038 dport=443 src=74.125.68.188 dst=106.51.80.211 sport=443 dport=55038 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 240119 ESTABLISHED src=10.1.226.238 dst=172.217.194.188 sport=45746 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=45746 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 140009 ESTABLISHED src=10.1.13.6 dst=172.217.194.188 sport=52504 dport=443 src=172.217.194.188 dst=106.51.80.211 sport=443 dport=1430 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 73005 ESTABLISHED src=10.1.158.68 dst=54.249.82.169 sport=51368 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=51368 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 69765 ESTABLISHED src=10.1.216.87 dst=74.125.200.188 sport=39997 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=39997 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 390808 ESTABLISHED src=10.1.203.85 dst=54.249.82.168 sport=42338 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=42338 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 391774 ESTABLISHED src=10.1.10.230 dst=74.125.200.188 sport=58498 dport=443 src=74.125.200.188 dst=106.51.80.211 sport=443 dport=58498 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 40681 ESTABLISHED src=10.1.171.247 dst=74.125.24.188 sport=57614 dport=443 src=74.125.24.188 dst=106.51.80.211 sport=443 dport=57614 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 134083 ESTABLISHED src=10.1.175.85 dst=74.125.24.188 sport=58678 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=58678 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 51493 ESTABLISHED src=10.1.165.45 dst=64.233.176.188 sport=43651 dport=5228 src=64.233.176.188 dst=106.51.80.211 sport=5228 dport=43651 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 419791 ESTABLISHED src=10.1.158.68 dst=54.249.82.168 sport=63966 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=63966 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 384317 ESTABLISHED src=10.1.2.190 dst=74.125.24.188 sport=51681 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=51681 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 150064 ESTABLISHED src=10.1.148.202 dst=74.125.199.188 sport=40813 dport=443 src=74.125.199.188 dst=106.51.80.211 sport=443 dport=40813 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 159619 ESTABLISHED src=10.1.105.5 dst=74.125.24.188 sport=56835 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=56835 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 47567 ESTABLISHED src=10.1.113.10 dst=172.217.194.188 sport=42660 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=42660 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 133730 ESTABLISHED src=10.1.2.40 dst=74.125.68.188 sport=56602 dport=443 src=74.125.68.188 dst=106.51.80.211 sport=443 dport=56602 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 68593 ESTABLISHED src=10.1.113.10 dst=74.125.193.188 sport=40414 dport=5228 src=74.125.193.188 dst=106.51.80.211 sport=5228 dport=40414 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 44307 ESTABLISHED src=10.1.226.238 dst=74.125.200.188 sport=56606 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=56606 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 421218 ESTABLISHED src=10.1.222.143 dst=54.249.82.168 sport=53382 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=53382 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 416355 ESTABLISHED src=10.1.195.216 dst=74.125.68.188 sport=41074 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=41074 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 152864 ESTABLISHED src=10.1.30.2 dst=74.125.24.188 sport=61012 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=61012 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 156061 ESTABLISHED src=10.1.12.215 dst=172.217.194.188 sport=63454 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=63454 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 49099 ESTABLISHED src=10.1.226.238 dst=108.177.12.188 sport=45116 dport=5228 src=108.177.12.188 dst=106.51.80.211 sport=5228 dport=45116 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 148004 ESTABLISHED src=10.1.11.225 dst=74.125.24.188 sport=60007 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=60007 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 394033 ESTABLISHED src=10.1.12.128 dst=76.223.31.44 sport=57916 dport=443 src=76.223.31.44 dst=106.51.80.211 sport=443 dport=57916 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 214576 ESTABLISHED src=10.1.212.177 dst=74.125.68.188 sport=44297 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=44297 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 164239 ESTABLISHED src=10.1.189.121 dst=54.249.82.169 sport=47102 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=47102 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 70927 ESTABLISHED src=10.1.226.87 dst=54.249.82.168 sport=64136 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=64136 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 142511 ESTABLISHED src=10.1.211.189 dst=74.125.68.188 sport=52309 dport=443 src=74.125.68.188 dst=106.51.80.211 sport=443 dport=52309 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 167150 ESTABLISHED src=10.1.158.68 dst=54.249.82.169 sport=63176 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=63176 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 421219 ESTABLISHED src=10.1.222.143 dst=74.125.68.188 sport=53287 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=53287 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 71036 ESTABLISHED src=10.1.11.225 dst=54.249.82.169 sport=57228 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=57228 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 138054 ESTABLISHED src=10.1.250.49 dst=172.217.194.188 sport=37087 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=37087 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 52616 ESTABLISHED src=10.1.195.216 dst=74.125.130.188 sport=44456 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=44456 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 47577 ESTABLISHED src=10.1.213.47 dst=172.217.194.188 sport=55617 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=55617 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 66224 ESTABLISHED src=10.1.165.45 dst=172.217.194.188 sport=41162 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=41162 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 67211 ESTABLISHED src=10.1.216.87 dst=74.125.68.188 sport=44856 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=44856 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 405328 ESTABLISHED src=10.1.104.201 dst=74.125.68.188 sport=33254 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=33254 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 393396 ESTABLISHED src=10.1.12.68 dst=74.125.130.188 sport=57532 dport=443 src=74.125.130.188 dst=106.51.80.211 sport=443 dport=57532 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 414517 ESTABLISHED src=10.1.13.6 dst=74.125.68.188 sport=51889 dport=443 src=74.125.68.188 dst=106.51.80.211 sport=443 dport=51889 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 406391 ESTABLISHED src=10.1.122.168 dst=74.125.24.188 sport=45069 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=45069 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 158097 ESTABLISHED src=10.1.12.88 dst=54.249.82.168 sport=58155 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=58155 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 139757 ESTABLISHED src=10.1.195.216 dst=74.125.24.188 sport=43348 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=43348 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 411004 ESTABLISHED src=10.1.120.108 dst=74.125.68.188 sport=48378 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=48378 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 403213 ESTABLISHED src=10.1.147.229 dst=74.125.68.188 sport=38099 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=38099 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 401540 ESTABLISHED src=10.1.189.121 dst=74.125.200.188 sport=46338 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=46338 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 129363 ESTABLISHED src=10.1.2.170 dst=172.217.194.188 sport=59625 dport=443 src=172.217.194.188 dst=106.51.80.211 sport=443 dport=59625 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 136332 ESTABLISHED src=10.1.198.131 dst=172.217.194.188 sport=34448 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=34448 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 397607 ESTABLISHED src=10.1.226.238 dst=74.125.200.188 sport=38728 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=38728 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 299 ESTABLISHED src=49.206.14.231 dst=106.51.80.211 sport=3530 dport=10022 src=106.51.80.211 dst=49.206.14.231 sport=10022 dport=3530 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 60289 ESTABLISHED src=10.1.195.216 dst=74.125.68.188 sport=42820 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=42820 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 46667 ESTABLISHED src=10.1.131.122 dst=74.125.200.188 sport=49576 dport=443 src=74.125.200.188 dst=106.51.80.211 sport=443 dport=49576 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 384902 ESTABLISHED src=10.1.222.85 dst=74.125.130.188 sport=56784 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=56784 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 150238 ESTABLISHED src=10.1.105.5 dst=54.249.82.169 sport=64316 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=64316 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 213507 ESTABLISHED src=10.1.212.177 dst=172.217.194.188 sport=48479 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=48479 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 384120 ESTABLISHED src=10.1.215.215 dst=74.125.130.188 sport=41332 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=41332 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 407003 ESTABLISHED src=10.1.113.152 dst=74.125.68.188 sport=58613 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=58613 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 393484 ESTABLISHED src=10.1.216.129 dst=172.217.194.188 sport=49477 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=49477 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 420657 ESTABLISHED src=10.1.104.34 dst=74.125.68.188 sport=40024 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=40024 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 409743 ESTABLISHED src=10.1.216.87 dst=106.51.144.155 sport=47059 dport=443 src=106.51.144.155 dst=106.51.80.211 sport=443 dport=47059 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 71042 ESTABLISHED src=10.1.11.225 dst=74.125.24.188 sport=50434 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=50434 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 414299 ESTABLISHED src=10.1.2.190 dst=74.125.24.188 sport=52004 dport=443 src=74.125.24.188 dst=106.51.80.211 sport=443 dport=52004 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 415682 ESTABLISHED src=10.1.198.131 dst=74.125.130.188 sport=58640 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=58640 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 47337 ESTABLISHED src=10.1.12.128 dst=74.125.24.188 sport=59215 dport=443 src=74.125.24.188 dst=106.51.80.211 sport=443 dport=59215 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 153073 ESTABLISHED src=10.1.222.85 dst=54.148.37.5 sport=60040 dport=443 src=54.148.37.5 dst=106.51.80.211 sport=443 dport=60040 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 147857 ESTABLISHED src=10.1.12.128 dst=54.249.82.168 sport=49920 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=49920 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 146524 ESTABLISHED src=10.1.148.202 dst=74.125.130.188 sport=40947 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=40947 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 73011 ESTABLISHED src=10.1.158.68 dst=54.249.82.168 sport=51348 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=51348 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 406675 ESTABLISHED src=10.1.122.168 dst=74.125.24.188 sport=46147 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=46147 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 70927 ESTABLISHED src=10.1.226.87 dst=54.249.82.168 sport=64135 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=64135 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 134417 ESTABLISHED src=10.1.12.88 dst=54.249.82.169 sport=65104 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=65104 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 239899 ESTABLISHED src=10.1.226.238 dst=172.217.194.188 sport=45654 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=45654 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 384942 ESTABLISHED src=10.1.229.4 dst=74.125.200.188 sport=56220 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=56220 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 412226 ESTABLISHED src=10.1.120.108 dst=172.217.194.188 sport=37128 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=37128 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 50173 ESTABLISHED src=10.1.168.139 dst=74.125.24.188 sport=55346 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=55346 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 147787 ESTABLISHED src=10.1.2.101 dst=74.125.200.188 sport=63346 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=63346 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 158770 ESTABLISHED src=10.1.121.233 dst=74.125.68.188 sport=44701 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=44701 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 396588 ESTABLISHED src=10.1.131.122 dst=172.217.194.188 sport=49284 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=49284 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 71423 ESTABLISHED src=10.1.226.87 dst=54.249.82.169 sport=65513 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=65513 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 72677 ESTABLISHED src=10.1.251.73 dst=172.217.194.188 sport=45842 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=45842 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 388679 ESTABLISHED src=10.1.12.68 dst=74.125.24.188 sport=50177 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=50177 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 68087 ESTABLISHED src=10.1.113.10 dst=74.125.200.188 sport=54955 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=54955 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 391222 ESTABLISHED src=10.1.172.56 dst=74.125.130.188 sport=35792 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=35792 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 378169 ESTABLISHED src=10.1.113.152 dst=172.217.194.188 sport=43040 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=43040 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 63835 ESTABLISHED src=10.1.131.122 dst=54.249.82.169 sport=51891 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=51891 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 384805 ESTABLISHED src=10.1.104.201 dst=52.89.124.203 sport=59646 dport=443 src=52.89.124.203 dst=106.51.80.211 sport=443 dport=59646 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 391183 ESTABLISHED src=10.1.12.128 dst=74.125.68.188 sport=63478 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=63478 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 65623 ESTABLISHED src=10.1.222.85 dst=74.125.68.188 sport=36730 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=36730 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 156075 ESTABLISHED src=10.1.12.215 dst=54.249.82.168 sport=61100 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=61100 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 394022 ESTABLISHED src=10.1.12.128 dst=172.217.194.188 sport=52526 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=52526 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 40067 ESTABLISHED src=10.1.195.216 dst=74.125.130.188 sport=40958 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=40958 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 71241 ESTABLISHED src=10.1.226.87 dst=54.249.82.168 sport=65346 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=65346 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 417651 ESTABLISHED src=10.1.131.122 dst=54.249.82.169 sport=50774 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=50774 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 64428 ESTABLISHED src=10.1.175.85 dst=74.125.200.188 sport=42730 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=42730 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 136647 ESTABLISHED src=10.1.211.189 dst=172.217.194.188 sport=49991 dport=443 src=172.217.194.188 dst=106.51.80.211 sport=443 dport=49991 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 384558 ESTABLISHED src=10.1.12.88 dst=54.249.82.168 sport=59731 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=59731 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 51073 ESTABLISHED src=10.1.213.47 dst=172.217.194.188 sport=38189 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=38189 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 47985 ESTABLISHED src=10.1.222.85 dst=74.125.24.188 sport=34908 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=34908 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 407143 ESTABLISHED src=10.1.113.152 dst=74.125.200.188 sport=39105 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=39105 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 166571 ESTABLISHED src=10.1.12.215 dst=74.125.68.188 sport=52801 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=52801 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 149612 ESTABLISHED src=10.1.147.229 dst=74.125.24.188 sport=49420 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=49420 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 47497 ESTABLISHED src=10.1.13.6 dst=74.125.130.188 sport=58081 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=58081 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 167638 ESTABLISHED src=10.1.249.64 dst=172.217.194.188 sport=33745 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=33745 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 412375 ESTABLISHED src=10.1.2.180 dst=74.125.130.188 sport=36644 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=36644 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 51852 ESTABLISHED src=10.1.250.49 dst=64.233.171.188 sport=47560 dport=5228 src=64.233.171.188 dst=106.51.80.211 sport=5228 dport=47560 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 416938 ESTABLISHED src=10.1.12.88 dst=54.249.82.168 sport=60718 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=60718 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 66535 ESTABLISHED src=10.1.104.201 dst=74.125.68.188 sport=50990 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=50990 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 246557 ESTABLISHED src=10.1.226.238 dst=74.125.130.188 sport=46848 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=46848 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 389070 ESTABLISHED src=10.1.211.189 dst=74.125.130.188 sport=65276 dport=443 src=74.125.130.188 dst=106.51.80.211 sport=443 dport=65276 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 384618 ESTABLISHED src=10.1.211.189 dst=74.125.130.188 sport=64729 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=64729 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 75651 ESTABLISHED src=10.1.12.215 dst=54.249.82.168 sport=56571 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=56571 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 133884 ESTABLISHED src=10.1.148.202 dst=74.125.68.188 sport=46160 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=46160 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 386897 ESTABLISHED src=10.1.2.190 dst=74.125.68.188 sport=54801 dport=443 src=74.125.68.188 dst=106.51.80.211 sport=443 dport=54801 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 158097 ESTABLISHED src=10.1.12.88 dst=54.249.82.168 sport=58150 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=58150 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 421487 ESTABLISHED src=10.1.222.143 dst=54.249.82.168 sport=57359 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=57359 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 146528 ESTABLISHED src=10.1.216.129 dst=209.85.200.188 sport=53467 dport=443 src=209.85.200.188 dst=106.51.80.211 sport=443 dport=53467 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 156044 ESTABLISHED src=10.1.12.215 dst=54.249.82.169 sport=60572 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=60572 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 67045 ESTABLISHED src=10.1.237.130 dst=151.101.152.106 sport=61835 dport=443 src=151.101.152.106 dst=106.51.80.211 sport=443 dport=61835 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 50215 ESTABLISHED src=10.1.226.238 dst=172.217.194.188 sport=50312 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=50312 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 154692 ESTABLISHED src=10.1.171.247 dst=74.125.200.188 sport=60027 dport=443 src=74.125.200.188 dst=106.51.80.211 sport=443 dport=60027 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 65557 ESTABLISHED src=10.1.232.26 dst=74.125.68.188 sport=59817 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=59817 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 64068 ESTABLISHED src=10.1.226.238 dst=74.125.20.188 sport=48320 dport=443 src=74.125.20.188 dst=106.51.80.211 sport=443 dport=48320 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 415356 ESTABLISHED src=10.1.172.56 dst=172.217.194.188 sport=43760 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=43760 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 138492 ESTABLISHED src=10.1.148.202 dst=172.217.194.188 sport=44605 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=44605 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 76144 ESTABLISHED src=10.1.2.181 dst=74.125.200.188 sport=58109 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=58109 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 383966 ESTABLISHED src=10.1.198.131 dst=74.125.130.188 sport=57515 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=57515 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 386877 ESTABLISHED src=10.1.203.30 dst=74.125.130.188 sport=49883 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=49883 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 213335 ESTABLISHED src=10.1.212.177 dst=64.233.177.188 sport=37811 dport=443 src=64.233.177.188 dst=106.51.80.211 sport=443 dport=37811 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 46797 ESTABLISHED src=10.1.211.189 dst=74.125.24.188 sport=62333 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=62333 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 44053 ESTABLISHED src=10.1.131.122 dst=74.125.68.188 sport=49563 dport=443 src=74.125.68.188 dst=106.51.80.211 sport=443 dport=49563 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 75851 ESTABLISHED src=10.1.189.121 dst=172.217.194.188 sport=49140 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=49140 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 158097 ESTABLISHED src=10.1.12.88 dst=54.249.82.168 sport=58149 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=58149 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 410953 ESTABLISHED src=10.1.104.201 dst=74.125.130.188 sport=60098 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=60098 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 134447 ESTABLISHED src=10.1.175.85 dst=74.125.130.188 sport=57764 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=57764 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 71241 ESTABLISHED src=10.1.226.87 dst=54.249.82.168 sport=65345 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=65345 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 392901 ESTABLISHED src=10.1.12.128 dst=74.125.130.188 sport=49790 dport=443 src=74.125.130.188 dst=106.51.80.211 sport=443 dport=49790 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 67279 ESTABLISHED src=10.1.216.87 dst=74.125.24.188 sport=44443 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=44443 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 44315 ESTABLISHED src=10.1.226.238 dst=173.194.175.188 sport=47724 dport=5228 src=173.194.175.188 dst=106.51.80.211 sport=5228 dport=47724 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 420193 ESTABLISHED src=10.1.2.181 dst=54.249.82.169 sport=52873 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=52873 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 68484 ESTABLISHED src=10.1.216.87 dst=172.217.194.188 sport=48107 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=48107 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 384558 ESTABLISHED src=10.1.12.88 dst=54.249.82.168 sport=59735 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=59735 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 65751 ESTABLISHED src=10.1.229.4 dst=74.125.24.188 sport=54249 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=54249 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 397675 ESTABLISHED src=10.1.175.85 dst=209.85.200.188 sport=52054 dport=443 src=209.85.200.188 dst=106.51.80.211 sport=443 dport=52054 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 401005 ESTABLISHED src=10.1.226.238 dst=74.125.68.188 sport=43056 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=43056 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 396494 ESTABLISHED src=10.1.104.201 dst=74.125.68.188 sport=60614 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=60614 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 417882 ESTABLISHED src=10.1.11.225 dst=54.249.82.169 sport=59550 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=59550 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 75282 ESTABLISHED src=10.1.213.47 dst=64.233.171.188 sport=57473 dport=5228 src=64.233.171.188 dst=106.51.80.211 sport=5228 dport=57473 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 394010 ESTABLISHED src=10.1.175.85 dst=74.125.68.188 sport=59420 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=59420 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 420906 ESTABLISHED src=10.1.175.85 dst=74.125.24.188 sport=32848 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=32848 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 66559 ESTABLISHED src=10.1.148.202 dst=172.217.194.188 sport=44440 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=44440 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 68473 ESTABLISHED src=10.1.195.216 dst=74.125.24.188 sport=45513 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=45513 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 394508 ESTABLISHED src=10.1.203.85 dst=172.217.194.188 sport=46276 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=46276 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 393371 ESTABLISHED src=10.1.235.187 dst=74.125.130.188 sport=49286 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=49286 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 156429 ESTABLISHED src=10.1.250.49 dst=172.217.194.188 sport=38970 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=38970 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 404172 ESTABLISHED src=10.1.216.87 dst=74.125.68.188 sport=42535 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=42535 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 68638 ESTABLISHED src=10.1.11.126 dst=54.249.82.169 sport=63806 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=63806 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 153809 ESTABLISHED src=10.1.226.238 dst=74.125.200.188 sport=43682 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=43682 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 6440 ESTABLISHED src=10.1.10.110 dst=74.125.130.188 sport=65096 dport=443 src=74.125.130.188 dst=106.51.80.211 sport=443 dport=65096 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 71464 ESTABLISHED src=10.1.226.87 dst=54.249.82.169 sport=49213 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=49213 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 393420 ESTABLISHED src=10.1.12.68 dst=74.125.130.188 sport=57566 dport=443 src=74.125.130.188 dst=106.51.80.211 sport=443 dport=57566 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 50210 ESTABLISHED src=10.1.168.139 dst=74.125.193.188 sport=50164 dport=5228 src=74.125.193.188 dst=106.51.80.211 sport=5228 dport=50164 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 147874 ESTABLISHED src=10.1.12.128 dst=54.249.82.169 sport=55648 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=55648 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 409743 ESTABLISHED src=10.1.216.87 dst=23.62.12.32 sport=38527 dport=80 src=23.62.12.32 dst=106.51.80.211 sport=80 dport=38527 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 394129 ESTABLISHED src=10.1.226.87 dst=54.249.82.169 sport=63574 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=63574 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 411151 ESTABLISHED src=10.1.216.129 dst=172.217.194.188 sport=43540 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=43540 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 71498 ESTABLISHED src=10.1.128.98 dst=76.223.31.44 sport=53859 dport=443 src=76.223.31.44 dst=106.51.80.211 sport=443 dport=53859 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 150517 ESTABLISHED src=10.1.226.87 dst=54.249.82.168 sport=60537 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=60537 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 158097 ESTABLISHED src=10.1.12.88 dst=54.249.82.168 sport=58156 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=58156 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 71458 ESTABLISHED src=10.1.14.30 dst=54.249.82.168 sport=57369 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=57369 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 393280 ESTABLISHED src=10.1.12.68 dst=74.125.130.188 sport=50739 dport=443 src=74.125.130.188 dst=106.51.80.211 sport=443 dport=50739 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 393223 ESTABLISHED src=10.1.222.85 dst=74.125.24.188 sport=47980 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=47980 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 417011 ESTABLISHED src=10.1.203.85 dst=74.125.68.188 sport=48646 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=48646 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 137789 ESTABLISHED src=10.1.2.170 dst=74.125.200.188 sport=60424 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=60424 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 167151 ESTABLISHED src=10.1.12.215 dst=172.217.194.188 sport=53871 dport=443 src=172.217.194.188 dst=106.51.80.211 sport=443 dport=53871 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 397691 ESTABLISHED src=10.1.250.49 dst=74.125.24.188 sport=43143 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=43143 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 133939 ESTABLISHED src=10.1.105.5 dst=54.249.82.169 sport=52745 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=52745 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 133750 ESTABLISHED src=10.1.189.121 dst=74.125.68.188 sport=41544 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=41544 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 61785 ESTABLISHED src=10.1.216.129 dst=172.217.194.188 sport=41864 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=41864 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 68594 ESTABLISHED src=10.1.113.10 dst=74.125.193.188 sport=40415 dport=5228 src=74.125.193.188 dst=106.51.80.211 sport=5228 dport=40415 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 417979 ESTABLISHED src=10.1.203.85 dst=74.125.200.188 sport=40390 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=40390 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 150553 ESTABLISHED src=10.1.216.87 dst=74.125.68.188 sport=48985 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=48985 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 391757 ESTABLISHED src=10.1.12.128 dst=74.125.200.188 sport=57820 dport=443 src=74.125.200.188 dst=106.51.80.211 sport=443 dport=57820 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 150412 ESTABLISHED src=10.1.122.168 dst=74.125.24.188 sport=46234 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=46234 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 413145 ESTABLISHED src=10.1.222.85 dst=52.89.124.203 sport=55362 dport=443 src=52.89.124.203 dst=106.51.80.211 sport=443 dport=55362 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 391812 ESTABLISHED src=10.1.147.229 dst=74.125.199.188 sport=41825 dport=5228 src=74.125.199.188 dst=106.51.80.211 sport=5228 dport=41825 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 46234 ESTABLISHED src=10.1.2.170 dst=74.125.24.188 sport=56607 dport=443 src=74.125.24.188 dst=106.51.80.211 sport=443 dport=56607 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 68883 ESTABLISHED src=10.1.203.30 dst=172.217.194.188 sport=39840 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=39840 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 67953 ESTABLISHED src=10.1.216.87 dst=203.205.211.75 sport=49182 dport=443 src=203.205.211.75 dst=106.51.80.211 sport=443 dport=49182 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 70649 ESTABLISHED src=10.1.183.220 dst=54.249.82.169 sport=63015 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=63015 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 50480 ESTABLISHED src=10.1.104.201 dst=74.125.200.188 sport=40366 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=40366 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 134417 ESTABLISHED src=10.1.12.88 dst=54.249.82.168 sport=65102 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=65102 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 72750 ESTABLISHED src=10.1.237.130 dst=151.101.152.106 sport=53960 dport=443 src=151.101.152.106 dst=106.51.80.211 sport=443 dport=53960 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 50077 ESTABLISHED src=10.1.2.170 dst=172.217.194.188 sport=57247 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=57247 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 135001 ESTABLISHED src=10.1.147.229 dst=172.217.194.188 sport=46523 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=46523 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 393254 ESTABLISHED src=10.1.120.108 dst=172.217.194.188 sport=43613 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=43613 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 68639 ESTABLISHED src=10.1.11.126 dst=54.249.82.169 sport=63876 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=1427 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 71423 ESTABLISHED src=10.1.226.87 dst=54.249.82.169 sport=65509 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=65509 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 59857 ESTABLISHED src=10.1.30.2 dst=74.125.68.188 sport=59310 dport=443 src=74.125.68.188 dst=106.51.80.211 sport=443 dport=59310 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 391461 ESTABLISHED src=10.1.232.26 dst=74.125.200.188 sport=43880 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=43880 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 412727 ESTABLISHED src=10.1.250.49 dst=74.125.68.188 sport=38693 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=38693 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 382429 ESTABLISHED src=10.1.122.168 dst=74.125.200.188 sport=46220 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=46220 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 167137 ESTABLISHED src=10.1.158.68 dst=54.249.82.169 sport=63326 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=63326 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 416447 ESTABLISHED src=10.1.195.216 dst=74.125.200.188 sport=37484 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=37484 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 414777 ESTABLISHED src=10.1.2.181 dst=172.217.194.188 sport=63258 dport=443 src=172.217.194.188 dst=106.51.80.211 sport=443 dport=63258 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 382345 ESTABLISHED src=10.1.122.168 dst=209.85.200.188 sport=38944 dport=443 src=209.85.200.188 dst=106.51.80.211 sport=443 dport=38944 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 68216 ESTABLISHED src=10.1.216.87 dst=172.217.194.188 sport=47611 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=47611 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 132178 ESTABLISHED src=10.1.226.238 dst=74.125.130.188 sport=33404 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=33404 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 391371 ESTABLISHED src=10.1.12.128 dst=74.125.130.188 sport=50923 dport=443 src=74.125.130.188 dst=106.51.80.211 sport=443 dport=50923 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 406548 ESTABLISHED src=10.1.216.129 dst=172.217.194.188 sport=58664 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=58664 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 70066 ESTABLISHED src=10.1.12.88 dst=54.249.82.168 sport=50160 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=50160 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 64275 ESTABLISHED src=10.1.226.238 dst=64.233.186.188 sport=52996 dport=5228 src=64.233.186.188 dst=106.51.80.211 sport=5228 dport=52996 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 148711 ESTABLISHED src=10.1.222.85 dst=172.217.194.188 sport=56476 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=56476 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 51958 ESTABLISHED src=10.1.198.131 dst=74.125.24.188 sport=41279 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=41279 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 417699 ESTABLISHED src=10.1.165.45 dst=74.125.200.188 sport=47540 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=47540 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 407657 ESTABLISHED src=10.1.12.128 dst=74.125.200.188 sport=57206 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=57206 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 392779 ESTABLISHED src=10.1.12.128 dst=172.217.194.188 sport=62150 dport=443 src=172.217.194.188 dst=106.51.80.211 sport=443 dport=62150 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 131726 ESTABLISHED src=10.1.226.238 dst=74.125.200.188 sport=38546 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=38546 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 68639 ESTABLISHED src=10.1.11.126 dst=54.249.82.169 sport=63877 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=63877 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 393371 ESTABLISHED src=10.1.148.202 dst=74.125.68.188 sport=38104 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=38104 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 70066 ESTABLISHED src=10.1.12.88 dst=54.249.82.169 sport=50163 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=50163 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 409743 ESTABLISHED src=10.1.216.87 dst=23.62.12.32 sport=38519 dport=80 src=23.62.12.32 dst=106.51.80.211 sport=80 dport=38519 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 404701 ESTABLISHED src=10.1.147.229 dst=74.125.24.188 sport=49983 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=49983 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 149539 ESTABLISHED src=10.1.2.180 dst=74.125.68.188 sport=50574 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=50574 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 388343 ESTABLISHED src=10.1.2.190 dst=74.125.24.188 sport=52130 dport=443 src=74.125.24.188 dst=106.51.80.211 sport=443 dport=52130 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 50743 ESTABLISHED src=10.1.203.30 dst=74.125.200.188 sport=47697 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=47697 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 150372 ESTABLISHED src=10.1.189.121 dst=74.125.68.188 sport=44174 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=44174 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 158097 ESTABLISHED src=10.1.12.88 dst=54.249.82.169 sport=58148 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=58148 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 73009 ESTABLISHED src=10.1.158.68 dst=54.249.82.169 sport=51374 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=51374 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 66907 ESTABLISHED src=10.1.148.202 dst=74.125.68.188 sport=48885 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=48885 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 70066 ESTABLISHED src=10.1.12.88 dst=54.249.82.169 sport=50162 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=50162 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 419785 ESTABLISHED src=10.1.158.68 dst=54.249.82.169 sport=63988 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=63988 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 431996 ESTABLISHED src=10.1.4.1 dst=213.227.168.135 sport=61016 dport=80 src=213.227.168.135 dst=106.51.80.211 sport=80 dport=61016 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 384558 ESTABLISHED src=10.1.12.88 dst=54.249.82.169 sport=59737 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=59737 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 37277 ESTABLISHED src=10.1.198.131 dst=74.125.130.188 sport=60732 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=60732 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 419794 ESTABLISHED src=10.1.158.68 dst=54.249.82.168 sport=63965 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=63965 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 151296 ESTABLISHED src=10.1.226.238 dst=74.125.24.188 sport=37372 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=37372 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 149052 ESTABLISHED src=10.1.104.201 dst=74.125.200.188 sport=57050 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=57050 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 69866 ESTABLISHED src=10.1.216.87 dst=172.217.194.188 sport=41485 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=41485 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 404347 ESTABLISHED src=10.1.229.4 dst=74.125.24.188 sport=51345 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=51345 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 70928 ESTABLISHED src=10.1.226.87 dst=54.249.82.168 sport=64142 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=64142 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 71464 ESTABLISHED src=10.1.226.87 dst=54.249.82.169 sport=49209 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=49209 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 154483 ESTABLISHED src=10.1.183.220 dst=54.249.82.169 sport=54446 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=54446 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 53135 ESTABLISHED src=10.1.105.5 dst=74.125.130.188 sport=49505 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=49505 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 167143 ESTABLISHED src=10.1.158.68 dst=74.125.68.188 sport=52986 dport=443 src=74.125.68.188 dst=106.51.80.211 sport=443 dport=52986 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 71389 ESTABLISHED src=10.1.13.16 dst=54.249.82.169 sport=60504 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=60504 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 420162 ESTABLISHED src=10.1.249.64 dst=74.125.68.188 sport=51194 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=51194 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 133555 ESTABLISHED src=10.1.235.187 dst=74.125.68.188 sport=46848 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=46848 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 158097 ESTABLISHED src=10.1.12.88 dst=54.249.82.168 sport=58146 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=58146 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 148257 ESTABLISHED src=10.1.216.129 dst=74.125.200.188 sport=50108 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=50108 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 62741 ESTABLISHED src=10.1.213.47 dst=74.125.68.188 sport=58911 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=58911 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 150238 ESTABLISHED src=10.1.105.5 dst=54.249.82.169 sport=64317 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=64317 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 73009 ESTABLISHED src=10.1.158.68 dst=54.249.82.169 sport=51375 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=51375 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 48023 ESTABLISHED src=10.1.195.216 dst=74.125.193.188 sport=46415 dport=5228 src=74.125.193.188 dst=106.51.80.211 sport=5228 dport=46415 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 150241 ESTABLISHED src=10.1.175.85 dst=74.125.68.188 sport=44858 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=44858 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 46534 ESTABLISHED src=10.1.2.170 dst=74.125.68.188 sport=57219 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=57219 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 54069 ESTABLISHED src=10.1.104.201 dst=74.125.200.188 sport=42048 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=42048 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 134808 ESTABLISHED src=10.1.2.190 dst=172.217.194.188 sport=54628 dport=443 src=172.217.194.188 dst=106.51.80.211 sport=443 dport=54628 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 213426 ESTABLISHED src=10.1.212.177 dst=172.217.194.188 sport=45035 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=45035 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 398470 ESTABLISHED src=10.1.12.68 dst=74.125.68.188 sport=59551 dport=443 src=74.125.68.188 dst=106.51.80.211 sport=443 dport=59551 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 159451 ESTABLISHED src=10.1.152.129 dst=74.125.68.188 sport=49375 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=49375 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 140338 ESTABLISHED src=10.1.12.128 dst=74.125.24.188 sport=51518 dport=443 src=74.125.24.188 dst=106.51.80.211 sport=443 dport=51518 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 47890 ESTABLISHED src=10.1.168.139 dst=172.217.194.188 sport=39752 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=39752 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 46785 ESTABLISHED src=10.1.211.189 dst=52.187.129.203 sport=63240 dport=443 src=52.187.129.203 dst=106.51.80.211 sport=443 dport=63240 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 140342 ESTABLISHED src=10.1.13.6 dst=74.125.68.188 sport=63352 dport=443 src=74.125.68.188 dst=106.51.80.211 sport=443 dport=63352 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 394935 ESTABLISHED src=10.1.226.111 dst=137.116.151.74 sport=49289 dport=443 src=137.116.151.74 dst=106.51.80.211 sport=443 dport=49289 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 396614 ESTABLISHED src=10.1.104.201 dst=74.125.68.188 sport=60688 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=60688 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 403229 ESTABLISHED src=10.1.189.121 dst=172.217.194.188 sport=45286 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=45286 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 58535 ESTABLISHED src=10.1.2.170 dst=74.125.200.188 sport=58149 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=58149 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 167141 ESTABLISHED src=10.1.158.68 dst=54.249.82.169 sport=63507 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=63507 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 151451 ESTABLISHED src=10.1.195.216 dst=74.125.68.188 sport=45653 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=45653 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 412368 ESTABLISHED src=10.1.148.202 dst=74.125.130.188 sport=46780 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=46780 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 152307 ESTABLISHED src=10.1.229.4 dst=74.125.130.188 sport=38512 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=38512 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 66337 ESTABLISHED src=10.1.11.195 dst=74.125.130.188 sport=58544 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=58544 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 70650 ESTABLISHED src=10.1.183.220 dst=54.249.82.169 sport=63041 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=63041 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 71448 ESTABLISHED src=10.1.116.157 dst=172.217.194.188 sport=59894 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=59894 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 138467 ESTABLISHED src=10.1.203.30 dst=74.125.24.188 sport=37722 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=37722 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 44246 ESTABLISHED src=10.1.226.238 dst=74.125.200.188 sport=56538 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=56538 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 416938 ESTABLISHED src=10.1.12.88 dst=54.249.82.169 sport=60716 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=60716 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 409743 ESTABLISHED src=10.1.216.87 dst=23.62.12.32 sport=38521 dport=80 src=23.62.12.32 dst=106.51.80.211 sport=80 dport=38521 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 417861 ESTABLISHED src=10.1.11.195 dst=54.249.82.169 sport=61382 dport=80 src=54.249.82.169 dst=106.51.80.211 sport=80 dport=61382 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 57902 ESTABLISHED src=10.1.198.131 dst=74.125.130.188 sport=33401 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=33401 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 47771 ESTABLISHED src=10.1.148.202 dst=74.125.68.188 sport=44085 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=44085 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 387185 ESTABLISHED src=10.1.216.129 dst=74.125.130.188 sport=37522 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=37522 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 134171 ESTABLISHED src=10.1.235.187 dst=74.125.24.188 sport=46079 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=46079 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 71018 ESTABLISHED src=10.1.11.225 dst=54.249.82.168 sport=57220 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=57220 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 417464 ESTABLISHED src=10.1.211.189 dst=13.248.151.210 sport=59100 dport=443 src=13.248.151.210 dst=106.51.80.211 sport=443 dport=59100 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 70066 ESTABLISHED src=10.1.12.88 dst=54.249.82.169 sport=50159 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=50159 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 63336 ESTABLISHED src=10.1.148.202 dst=172.217.194.188 sport=44408 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=44408 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 134417 ESTABLISHED src=10.1.12.88 dst=54.249.82.168 sport=65100 dport=443 src=54.249.82.168 dst=106.51.80.211 sport=443 dport=65100 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 150188 ESTABLISHED src=10.1.226.111 dst=74.125.68.188 sport=60443 dport=443 src=74.125.68.188 dst=106.51.80.211 sport=443 dport=60443 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 384982 ESTABLISHED src=10.1.12.215 dst=74.125.200.188 sport=61943 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=61943 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 417436 ESTABLISHED src=10.1.211.189 dst=74.125.130.188 sport=61643 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=61643 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 150292 ESTABLISHED src=10.1.216.129 dst=74.125.68.188 sport=39789 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=39789 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 39852 ESTABLISHED src=10.1.168.139 dst=74.125.24.188 sport=54254 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=54254 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 417179 ESTABLISHED src=10.1.152.129 dst=74.125.200.188 sport=46000 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=46000 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 167139 ESTABLISHED src=10.1.158.68 dst=54.249.82.169 sport=63559 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=63559 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 150542 ESTABLISHED src=10.1.250.49 dst=74.125.24.188 sport=39460 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=39460 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 42797 ESTABLISHED src=10.1.226.238 dst=74.125.20.188 sport=46672 dport=5228 src=74.125.20.188 dst=106.51.80.211 sport=5228 dport=46672 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 60405 ESTABLISHED src=10.1.180.183 dst=74.125.200.188 sport=46088 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=46088 [ASSURED] mark=0 zone=0 use=2
ipv4     2 unknown  2 547 src=0.0.0.0 dst=224.0.0.1 [UNREPLIED] src=224.0.0.1 dst=0.0.0.0 mark=0 zone=0 use=2
ipv4     2 tcp      6 72438 ESTABLISHED src=10.1.121.233 dst=74.125.200.188 sport=41321 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=41321 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 128028 ESTABLISHED src=10.1.148.202 dst=74.125.68.188 sport=46098 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=46098 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 406763 ESTABLISHED src=10.1.222.143 dst=54.249.82.169 sport=52910 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=52910 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 397288 ESTABLISHED src=10.1.203.30 dst=74.125.24.188 sport=42474 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=42474 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 393800 ESTABLISHED src=10.1.216.87 dst=74.125.130.188 sport=48700 dport=5228 src=74.125.130.188 dst=106.51.80.211 sport=5228 dport=48700 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 391103 ESTABLISHED src=10.1.172.56 dst=54.249.82.169 sport=42888 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=42888 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 53131 ESTABLISHED src=10.1.158.21 dst=74.125.130.188 sport=58232 dport=443 src=74.125.130.188 dst=106.51.80.211 sport=443 dport=58232 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 217920 ESTABLISHED src=10.1.226.238 dst=172.217.194.188 sport=42006 dport=5228 src=172.217.194.188 dst=106.51.80.211 sport=5228 dport=42006 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 413017 ESTABLISHED src=10.1.229.4 dst=74.125.24.188 sport=43707 dport=5228 src=74.125.24.188 dst=106.51.80.211 sport=5228 dport=43707 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 150541 ESTABLISHED src=10.1.152.129 dst=74.125.200.188 sport=47923 dport=5228 src=74.125.200.188 dst=106.51.80.211 sport=5228 dport=47923 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 150346 ESTABLISHED src=10.1.198.131 dst=209.85.200.188 sport=41028 dport=443 src=209.85.200.188 dst=106.51.80.211 sport=443 dport=41028 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 73006 ESTABLISHED src=10.1.158.68 dst=54.249.82.169 sport=51372 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=51372 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 164239 ESTABLISHED src=10.1.189.121 dst=54.249.82.169 sport=47100 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=47100 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 167121 ESTABLISHED src=10.1.158.68 dst=54.249.82.169 sport=63550 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=63550 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 158097 ESTABLISHED src=10.1.12.88 dst=54.249.82.169 sport=58153 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=58153 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 67532 ESTABLISHED src=10.1.216.87 dst=74.125.68.188 sport=45314 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=45314 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 417826 ESTABLISHED src=10.1.183.220 dst=54.249.82.169 sport=56268 dport=443 src=54.249.82.169 dst=106.51.80.211 sport=443 dport=56268 [ASSURED] mark=0 zone=0 use=2
ipv4     2 tcp      6 63211 ESTABLISHED src=10.1.226.238 dst=74.125.68.188 sport=34596 dport=5228 src=74.125.68.188 dst=106.51.80.211 sport=5228 dport=34596 [ASSURED] mark=0 zone=0 use=2';
    }
}
