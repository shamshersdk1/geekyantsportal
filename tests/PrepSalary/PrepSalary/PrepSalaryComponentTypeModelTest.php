<?php
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PrepSalaryComponentTypeModelTest extends TestCase
{
     use DatabaseTransactions;
    // /**
    //  * A basic test example.
    //  *
    //  * @return void
    //  */
     public function testNameError()
    {
        $obj = factory(App\Models\Audited\Salary\PrepSalaryComponentType::class)->create(['name' => '']);
        $nameError = $obj->getErrors();
        $nameFieldError = $nameError->all()[0];
        $this->assertEquals($nameFieldError,"The name field is required.");
    }

    public function testCodeError()
    {
        $obj = factory(App\Models\Audited\Salary\PrepSalaryComponentType::class)->create(['code' => '']);
        $codeError = $obj->getErrors();
        $codeFieldError = $codeError->all()[0];
        $this->assertEquals($codeFieldError,"The code field is required.");
    }

    public function testServiceClassError()
    {
        $obj = factory(App\Models\Audited\Salary\PrepSalaryComponentType::class)->create(['service_class' => '']);
        $serviceClassError = $obj->getErrors();
        $serviceClassError = $serviceClassError->all()[0];
        $this->assertEquals($serviceClassError,"The service class field is required.");
    }
}
