<?php
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AppraisalTest extends TestCase
{
     use DatabaseTransactions;
     
    /**
     *  Assumeing July as current month
     *  Checks the appraisal if the effective month is previous month
     *  Example: effective date is 2019-06-15
     *  Expected Output :
     *  start_date should be 2019-07-01
     *  and end date should be 2019-07-31
     * @return void
     */

     public function testPreviousMonthAppraisal()
     {
        $effective_date = date('Y-m-d', strtotime(date('Y-m-15').' -1 MONTH'));
        $start_date = date('Y-m-01');
        $last_date = date('Y-m-t',strtotime($start_date));

        $userObject = \Auth::user();
        $prepSalaryComponent = factory(App\Models\Audited\Salary\PrepSalaryComponent::class)->create();
        $obj = new App\Services\SalaryService\ApprisalSalaryComponent();
        $obj->setComponent($prepSalaryComponent->id);
        $obj->setUserId($userObject->id);
        $obj->setMonthYear($prepSalaryComponent->salary->month->month,$prepSalaryComponent->salary->month->year);
        $get = factory(App\Models\Appraisal\Appraisal::class)->create(['effective_date' => $effective_date]);
        $response = $obj->generate();

        $get = App\Models\Audited\Appraisal\PrepAppraisal::where('prep_salary_id',$prepSalaryComponent->salary->id)->first();
        $this->assertEquals($last_date,$get->end_date);
        $this->assertEquals($start_date,$get->start_date);
     }

    /**
     *  A new person joins at 2019-07-15.
     *  Example: effective date is 2019-07-15
     *  the start_date should be 2019-07-15
     *  and end date should be 2019-07-31
     * @return void
     */
     public function testCurrentMonthAppraisal()
     {
        $effective_date = date('Y-m-d', strtotime(date('Y-m-15').' -0 MONTH'));
        $start_date = $effective_date;
        $date = date('Y-m-01');
        $last_date = date('Y-m-t',strtotime($date));
         
        $userObject = \Auth::user();
        $prepSalaryComponent = factory(App\Models\Audited\Salary\PrepSalaryComponent::class)->create();
        $obj = new App\Services\SalaryService\ApprisalSalaryComponent();
        $obj->setComponent($prepSalaryComponent->id);
        $obj->setUserId($userObject->id);
        $obj->setMonthYear($prepSalaryComponent->salary->month->month,$prepSalaryComponent->salary->month->year);
        $get = factory(App\Models\Appraisal\Appraisal::class)->create(['effective_date' => $effective_date]);
        $response = $obj->generate();

        $get = App\Models\Audited\Appraisal\PrepAppraisal::where('prep_salary_id',$prepSalaryComponent->salary->id)->first();
        $this->assertEquals($last_date,$get->end_date);
        $this->assertEquals($start_date,$get->start_date);
     }

     /**
     *  Checks the appraisal if the effective month is in future month
     *  Example: effective date is 2019-08-15
     *  the start_date should be 2019-07-01
     *  and end date should be 2019-07-31
     * @return void
     */
     public function testFutureMonthAppraisal()
     {
        $effective_date = date('Y-m-d', strtotime(date('Y-m-15').' +1 MONTH'));
        $start_date = date('Y-m-01');
        $last_date = date('Y-m-t',strtotime($start_date));

        $userObject = \Auth::user();
        factory(App\Models\Audited\Salary\PrepSalary::class)->create();
        $prepSalaryComponent = factory(App\Models\Audited\Salary\PrepSalaryComponent::class)->create();
        $obj = new App\Services\SalaryService\ApprisalSalaryComponent();
        $obj->setComponent($prepSalaryComponent->id);
        $obj->setUserId($userObject->id);
        $obj->setMonthYear($prepSalaryComponent->salary->month->month,$prepSalaryComponent->salary->month->year);
        $get = factory(App\Models\Appraisal\Appraisal::class)->create(['effective_date' => $effective_date]);
        $response = $obj->generate();

        $get = App\Models\Audited\Appraisal\PrepAppraisal::where('prep_salary_id',$prepSalaryComponent->salary->id)->first();
        $this->assertNull($get->end_date);
        $this->assertNull($get->start_date);
     }

     /**
     *  Joining Date : 1-7-2019
     *  Confirmation Date : 7-7-2019
     *   Appraisal : 21-7-2019
     *   Appraisal : 25-7-2019 
     * @return void
     */
     public function testNewData() {
         $effective_date[0] = date('Y-m-01');
         $effective_date[1] = date('Y-m-07');
         $effective_date[2] = date('Y-m-21');
         $effective_date[3] = date('Y-m-25');
         
         $start_date[0] = date('Y-m-01');
         $last_date[0] = date('Y-m-d', strtotime($effective_date[1]. ' - 1 days'));
         $start_date[1] = date('Y-m-07');
         $last_date[1] = date('Y-m-d', strtotime($effective_date[2]. ' - 1 days'));
         $start_date[2] = date('Y-m-21');
         $last_date[2] = date('Y-m-d', strtotime($effective_date[3]. ' - 1 days'));
         $start_date[3] = date('Y-m-25');
         $last_date[3] = date("Y-m-t");

         //Create new user
        $newUser = factory(App\Models\User::class)->create();

        $prepSalaryComponent = factory(App\Models\Audited\Salary\PrepSalaryComponent::class)->create();
        $obj = new App\Services\SalaryService\ApprisalSalaryComponent();
        $obj->setComponent($prepSalaryComponent->id);
        $obj->setUserId($userObject->id);
        $obj->setMonthYear($prepSalaryComponent->salary->month->month,$prepSalaryComponent->salary->month->year);

        factory(App\Models\Appraisal\Appraisal::class)->create(['user_id' => $newUser->id,'effective_date' => $effective_date[0]]);
        factory(App\Models\Appraisal\Appraisal::class)->create(['user_id' => $newUser->id,'effective_date' => $effective_date[1]]);
        factory(App\Models\Appraisal\Appraisal::class)->create(['user_id' => $newUser->id,'effective_date' => $effective_date[2]]);
        factory(App\Models\Appraisal\Appraisal::class)->create(['user_id' => $newUser->id,'effective_date' => $effective_date[3]]);
        $response = $obj->generate();

        $get = App\Models\Audited\Appraisal\PrepAppraisal::where('prep_salary_id',$prepSalaryComponent->salary->id)->get();
        for($i=3;$i>=0;$i=$i-1){
            $this->assertEquals($last_date[$i],$get[$i]->end_date);
            $this->assertEquals($start_date[$i],$get[$i]->start_date);
         }
     }

      /**
     *  Joining Date : 15-6-2019
     *  Confirmation Date : 15-7-2019
     *   Appraisal : 1-7-2019 to 14-7-2019
     *   Appraisal : 15-7-2019  to 31-7-2019
     * @return void
     */
     public function testAppraisalBothMonthData() {
         $effective_date[0] = date('Y-m-d', strtotime(date('Y-m-15').' -1 MONTH'));
         $effective_date[1] = date('Y-m-15');
         
         $start_date[0] = date('Y-m-01');
         $last_date[0] = date('Y-m-d', strtotime($effective_date[1]. ' - 1 days'));
         $start_date[1] = $effective_date[1];
         $last_date[1] = date("Y-m-t");

         //Create new user
        $newUser = factory(App\Models\User::class)->create();

        $prepSalaryComponent = factory(App\Models\Audited\Salary\PrepSalaryComponent::class)->create();
        $obj = new App\Services\SalaryService\ApprisalSalaryComponent();
        $obj->setComponent($prepSalaryComponent->id);
        $obj->setUserId($userObject->id);
        $obj->setMonthYear($prepSalaryComponent->salary->month->month,$prepSalaryComponent->salary->month->year);

        factory(App\Models\Appraisal\Appraisal::class)->create(['user_id' => $newUser->id,'effective_date' => $effective_date[0]]);
        factory(App\Models\Appraisal\Appraisal::class)->create(['user_id' => $newUser->id,'effective_date' => $effective_date[1]]);
        $response = $obj->generate();

        $get = App\Models\Audited\Appraisal\PrepAppraisal::where('prep_salary_id',$prepSalaryComponent->salary->id)->get();
        for($i=2;$i>=0;$i=$i-1){
            $this->assertEquals($last_date[$i],$get[$i]->end_date);
            $this->assertEquals($start_date[$i],$get[$i]->start_date);
         }
     }

     /**
     *  Joining Date : 10-6-2019
     *  Confirmation Date : 28-7-2019
     *   Appraisal : 1-7-2019 to 27-7-2019
     *   Appraisal : 28-7-2019  to 31-7-2019
     * @return void
     */
     public function testAppraisalAfterSalaryProcessed() {
         $effective_date[0] = date('Y-m-d', strtotime(date('Y-m-10').' -1 MONTH'));
         $effective_date[1] = date('Y-m-28');
         
         $start_date[0] = date('Y-m-01');
         $last_date[0] = date('Y-m-d', strtotime($effective_date[1]. ' - 1 days'));
         $start_date[1] = $effective_date[1];
         $last_date[1] = date("Y-m-t");

         //Create new user
        $newUser = factory(App\Models\User::class)->create();

        $prepSalaryComponent = factory(App\Models\Audited\Salary\PrepSalaryComponent::class)->create();
        $obj = new App\Services\SalaryService\ApprisalSalaryComponent();
        $obj->setComponent($prepSalaryComponent->id);
        $obj->setUserId($userObject->id);
        $obj->setMonthYear($prepSalaryComponent->salary->month->month,$prepSalaryComponent->salary->month->year);

        factory(App\Models\Appraisal\Appraisal::class)->create(['user_id' => $newUser->id,'effective_date' => $effective_date[0]]);
        factory(App\Models\Appraisal\Appraisal::class)->create(['user_id' => $newUser->id,'effective_date' => $effective_date[1]]);
        $response = $obj->generate();

        $get = App\Models\Audited\Appraisal\PrepAppraisal::where('prep_salary_id',$prepSalaryComponent->salary->id)->get();
        for($i=2;$i>=0;$i=$i-1){
            $this->assertEquals($last_date[$i],$get[$i]->end_date);
            $this->assertEquals($start_date[$i],$get[$i]->start_date);
         }
     }
}
