<?php

return [
    'default' => 'slack',
    'bd_channel' => env("BD_CHANNEL_ID"),
    'hr_channel' => env("HR_CHANNEL_ID"),
    'admin_channel' => env("ADMIN_CHANNEL_ID"),
    'incident_channel' => env("INCIDENT_CHANNEL_ID"),
    'bonus_channel' => env("BONUS_CHANNEL_ID"),
    'loan_request_channel' => env("LOAN_REQUEST_CHANNEL_ID"),
    'user_attendance_channel' => env("USER_ATTENDANCE_CHANNEL_ID"),
    'leave_channel' => env("LEAVE_NOTIFICATION"),

    'services' => [
        'slack' => [
            'namespace' => '\App\Services\MessagingAppService\PostMessageService\SlackMessageService',
            'token' => 'sqlite',
        ],
        // 'flock'=> [
        //     'namespace' =>  '\App\Services\MessagingAppService\PostMessageService\FlockMessageServie.php',
        //     'token'     => 'sqlite'
        // ]
    ],
];
