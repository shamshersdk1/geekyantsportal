<?php

return [
    'lead_api_domain' => env('LEAD_API_DOMAIN','https://leads.geekyants.com/api'),
    'lead_api_post_url' => env('LEAD_API_POST_URL','https://leads.geekyants.com/api/lead/v1/'),
    'token' => env('TOKEN'),
];