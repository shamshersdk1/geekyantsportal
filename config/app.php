<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
     */
    'shared_key_auth_notify' => env('shared_key_auth_notify', 'p8s2@o0rD998$%J'),
    'geekyants_portal_token' => env('GEEKYANTS_PORTAL_TOKEN', 'goldtree9'),

    'authorities' => [
        'pratik@geekyants.com' => ['admin', 'user'],
        'sanket@geekyants.com' => ['admin', 'user'],
    ],

    'log_reminder' => env('LOG_REMINDER', 'Please log your todays work.'),

    'debug' => env('APP_DEBUG', false),

    /*
    |--------------------------------------------------------------------------
    | Application URL
    |--------------------------------------------------------------------------
    |
    | This URL is used by the console to properly generate URLs when using
    | the Artisan command line tool. You should set this to the root of
    | your application so that it is used when running Artisan tasks.
    |
     */

    'url' => 'https://geekyants.com',
    'name' => 'GeekyAnts Website',
    'api_url' => env('API_URL', 'https://portal.geekyants.com/api/v1/'),
    'reference_string' => env('REFERENCE_STRING', 'Ref/GA/BLR/'),

    /*
    |--------------------------------------------------------------------------
    | Application Timezone
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default timezone for your application, which
    | will be used by the PHP date and date-time functions. We have gone
    | ahead and set this to a sensible default for you out of the box.
    |
     */

    'timezone' => env('TIMEZONE', 'UTC'),

    /*
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
     */

    'locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Application Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the current one
    | is not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
     */

    'fallback_locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
     */

    'key' => env('APP_KEY', '23123&@*@#&KJJKJ'),

    'cipher' => 'AES-128-CBC',

    /*
    |--------------------------------------------------------------------------
    | Logging Configuration
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log settings for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Settings: "single", "daily", "syslog", "errorlog"
    |
     */

    'log' => 'single',

    'slack_url' => 'https://slack.com/api/',

    'slack_token' => env('SLACK_TOKEN', 'xoxp-2469679622-2469679624-84410828327-6bb91e5d1909e2a9eee947ad6c4471ff'),
    'slack_bot_token' => env('SLACK_BOT_TOKEN', 'xoxp-2469679622-2469679624-84410828327-6bb91e5d1909e2a9eee947ad6c4471ff'),
    'leave_notification' => env('LEAVE_NOTIFICATION'),
    'late_notification' => env('LATE_NOTIFICATION'),
    'loan_notification' => env('LOAN_NOTIFICATION'),

    'interest_rate' => env('INTEREST_RATE', '0.01025'),
    'min_outstanding_amount' => env('MIN_OUTSTANDING_AMT', '20000'),
    /*
    |--------------------------------------------------------------------------
    | Autoloaded Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
     */

    'providers' => [

        /*
         * Laravel Framework Service Providers...
         */
        'Illuminate\Foundation\Providers\ArtisanServiceProvider',
        'Illuminate\Auth\AuthServiceProvider',
        'Illuminate\Bus\BusServiceProvider',
        'Illuminate\Cache\CacheServiceProvider',
        'Illuminate\Foundation\Providers\ConsoleSupportServiceProvider',
        //'Illuminate\Routing\ControllerServiceProvider',
        'Illuminate\Cookie\CookieServiceProvider',
        'Illuminate\Database\DatabaseServiceProvider',
        'Illuminate\Encryption\EncryptionServiceProvider',
        'Illuminate\Filesystem\FilesystemServiceProvider',
        'Illuminate\Foundation\Providers\FoundationServiceProvider',
        'Illuminate\Hashing\HashServiceProvider',
        'Illuminate\Mail\MailServiceProvider',
        'Illuminate\Pagination\PaginationServiceProvider',
        'Illuminate\Pipeline\PipelineServiceProvider',
        'Illuminate\Queue\QueueServiceProvider',
        'Illuminate\Redis\RedisServiceProvider',
        'Illuminate\Auth\Passwords\PasswordResetServiceProvider',
        'Illuminate\Session\SessionServiceProvider',
        'Illuminate\Translation\TranslationServiceProvider',
        'Illuminate\Validation\ValidationServiceProvider',
        'Illuminate\View\ViewServiceProvider',

        /*
         * Application Service Providers...
         */
        'App\Providers\AppServiceProvider',
        //'App\Providers\BusServiceProvider',
        //'App\Providers\ConfigServiceProvider',
        'App\Providers\EventServiceProvider',
        'App\Providers\RouteServiceProvider',
        'App\Providers\Slack\SlackServiceProvider',
        'App\Providers\Slack\SlackInteractiveComponentsProvider',
        Weblee\Mandrill\MandrillServiceProvider::class,
        Barryvdh\DomPDF\ServiceProvider::class,
        'Illuminate\Notifications\NotificationServiceProvider',
        Maatwebsite\Excel\ExcelServiceProvider::class,
        Laravel\Socialite\SocialiteServiceProvider::class,
        //Illuminate\Foundation\Bus\DispatchesJobs;
        Sentry\SentryLaravel\SentryLaravelServiceProvider::class,
        Maatwebsite\Excel\ExcelServiceProvider::class,
        // Spatie\GoogleCalendar\GoogleCalendarServiceProvider::class,
        OwenIt\Auditing\AuditingServiceProvider::class,
        Collective\Html\HtmlServiceProvider::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
     */

    'aliases' => [

        'App' => 'Illuminate\Support\Facades\App',
        'Artisan' => 'Illuminate\Support\Facades\Artisan',
        'Auth' => 'Illuminate\Support\Facades\Auth',
        'Blade' => 'Illuminate\Support\Facades\Blade',
        'Bus' => 'Illuminate\Support\Facades\Bus',
        'Cache' => 'Illuminate\Support\Facades\Cache',
        'Config' => 'Illuminate\Support\Facades\Config',
        'Cookie' => 'Illuminate\Support\Facades\Cookie',
        'Crypt' => 'Illuminate\Support\Facades\Crypt',
        'DB' => 'Illuminate\Support\Facades\DB',
        'Eloquent' => 'Illuminate\Database\Eloquent\Model',
        'Event' => 'Illuminate\Support\Facades\Event',
        'File' => 'Illuminate\Support\Facades\File',
        'Hash' => 'Illuminate\Support\Facades\Hash',
        'Input' => 'Illuminate\Support\Facades\Input',
        'Inspiring' => 'Illuminate\Foundation\Inspiring',
        'Lang' => 'Illuminate\Support\Facades\Lang',
        'Log' => 'Illuminate\Support\Facades\Log',
        'Mail' => 'Illuminate\Support\Facades\Mail',
        'Password' => 'Illuminate\Support\Facades\Password',
        'Queue' => 'Illuminate\Support\Facades\Queue',
        'Redirect' => 'Illuminate\Support\Facades\Redirect',
        'Redis' => 'Illuminate\Support\Facades\Redis',
        'Request' => 'Illuminate\Support\Facades\Request',
        'Response' => 'Illuminate\Support\Facades\Response',
        'Route' => 'Illuminate\Support\Facades\Route',
        'Schema' => 'Illuminate\Support\Facades\Schema',
        'Session' => 'Illuminate\Support\Facades\Session',
        'Storage' => 'Illuminate\Support\Facades\Storage',
        'URL' => 'Illuminate\Support\Facades\URL',
        'Validator' => 'Illuminate\Support\Facades\Validator',
        'View' => 'Illuminate\Support\Facades\View',
        'MandrillMail' => Weblee\Mandrill\MandrillFacade::class,
        'Excel' => Maatwebsite\Excel\Facades\Excel::class,
        'Socialite' => Laravel\Socialite\Facades\Socialite::class,
        'Sentry' => Sentry\SentryLaravel\SentryFacade::class,
        'PDF' => Barryvdh\DomPDF\Facade::class,
        'Excel' => Maatwebsite\Excel\Facades\Excel::class,
        'Form' => Collective\Html\FormFacade::class,
        'Html' => Collective\Html\HtmlFacade::class,
        // 'GoogleCalendar' => Spatie\GoogleCalendar\GoogleCalendarFacade::class,
    ],

    'noreply-email' => env('NOREPLY_EMAIL', 'support@geekyants.com'),
    'support-email' => env('SUPPORT_EMAIL', 'leads@sahusoft.com'),
    'admin-email' => env('ADMIN_EMAIL', 'founders@sahusoft.com'),
    'leave-request-email' => env('LEAVE_REQUEST_EMAIL', 'safiullah@geekyants.com'),
    'admin-name' => env('ADMIN_NAME', 'Founders'),
    'test-account-email' => env('TEST_ACCOUNT_EMAIL', 'testaccount@gmail.com'),
    'geekyants_website_url' => env('WEBSITE_URL', 'https://geekyants.com'),
    'geekyants_portal_url' => env('PORTAL_URL', 'https://portal.geekyants.com'),
    'geekyants_leads_url' => env('LEADS_URL', 'https://leads.geekyants.com'),
    'audit-email' => env('AUDIT_EMAIL', 'pratik@geekyants.com'),
];
