<?php

return [
    'request-email'         => env('REQUEST_LOAN', 'request-loan@geekyants.com'),
    'approve-email'         => env('APPROVE_LOAN', 'disburse-loan@geekyants.com'),
    'disburse-email'        => env('DISBURSE_LOAN', 'paid-loan@geekyants.com'),
    'reject-email'          => env('REJECT_LOAN', 'reject-loan@geekyants.com'),
];
