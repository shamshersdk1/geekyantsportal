<?php

return [

    /*
     * Path to a json file containing the credentials of a Google Service account.
     */
    'client_secret_json' => base_path(env('GOOGLE_CALENDAR_JSON','google_calendar_client_secret.json')),

    /*
     *  The id of the Google Calendar that will be used by default.
     */
    'calendar_id' => env('CALENDAR_ID'),

];
