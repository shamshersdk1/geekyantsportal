<?php

return [
    'slashcommands' => [
        '\App\Services\SlackService\SlackListnerService\SlashCommand\SlackTimesheet',
        '\App\Services\SlackService\SlackListnerService\SlashCommand\SlackTeam',
        '\App\Services\SlackService\SlackListnerService\SlashCommand\SlackAdd',
        '\App\Services\SlackService\SlackListnerService\SlashCommand\SlackMarklate',
        '\App\Services\SlackService\SlackListnerService\SlashCommand\SlackRemovelate',
        '\App\Services\SlackService\SlackListnerService\SlashCommand\SlackRemove',
        '\App\Services\SlackService\SlackListnerService\SlashCommand\SlackConnect',
        '\App\Services\SlackService\SlackListnerService\SlashCommand\SlackDisconnect',
        '\App\Services\SlackService\SlackListnerService\SlashCommand\SlackSshkey',
    ],
    'interactivecomponents' => [
        // '\App\Services\SlackService\SlackListnerService\InteractiveComponents\SlackTimesheet',
    ],
    'slack_api_token' => env('GEEKYANTS_SLACK_API_TOKEN'),
    'bd_channel_id' => env('BD_CHANNEL_ID'),
    'project_channel_id' => env('PROJECT_CHANNEL_ID'),
    'slack_channel_id' => env('SLACK_LEAD_CHANNEL'),
    'ant_manager_id' => env('ANT_MANAGER_ID','UA5AZNUMR'),
];
