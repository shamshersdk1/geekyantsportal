<?php
return [
    'unavailable_audits' => 'No User Audits available',
    'detail' => 'Report for :name [:employee_id] - ',
    'created'            => [
        'metadata' => 'On :audit_created_at, :user_name [:audit_ip_address] created this record via :audit_url',
        'modified' => [
            'user_name'   => 'Employee Name <strong>:new</strong>',
            'effective_date'   => 'Effective Date <strong>:new</strong>',
            'type_name'   => 'Type <strong>:new</strong>',
            'id'   => 'Record ID <strong>:new</strong>',
        ],
        
    ],
    'updated'            => [
        'metadata' => 'On :audit_created_at, :user_name [:audit_ip_address] updated this record via :audit_url',
        'modified' => [
            'user_name'   => 'Employee Name <strong>:new</strong>',
            'effective_date'   => 'The Appraisal Effective Date has been modified from <strong>:old</strong> to <strong>:new</strong>',
            'type_name'   => 'The Appraisal Type has been modified from <strong>:old</strong> to <strong>:new</strong>',
        ],

    ],
];