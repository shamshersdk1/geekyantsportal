<?php
return [
    'unavailable_audits' => 'No User Audits available',
    'detail' => 'Report for :name [:employee_id] - ',
    'created'            => [
        'metadata' => 'On :audit_created_at, :user_name [:audit_ip_address] created this record via :audit_url',
        'modified' => [
            'user_name'   => 'Employee Name <strong>:new</strong>',
            'bonus_type'   => 'Bonus Type <strong>:new</strong>',
            'value'   => 'Value <strong>:new</strong>',
            'value_date'   => 'Value Date <strong>:new</strong>',
            'id'   => 'Record ID <strong>:new</strong>',
        ],
        
    ],
    'updated'            => [
        'metadata' => 'On :audit_created_at, :user_name [:audit_ip_address] updated this record via :audit_url',
        'modified' => [
            'user_name'   => 'Employee Name <strong>:new</strong>',
            'value'   => 'The Appraisal Bonus Value has been modified from <strong>:old</strong> to <strong>:new</strong>',
            'value_date'   => 'The Appraisal Bonus Value Date has been modified from <strong>:old</strong> to <strong>:new</strong>',
            'bonus_type'   => 'Bonus Type <strong>:new</strong>',
        ],

    ],
];