<?php
return [
    'unavailable_audits' => 'No User Details Audits available',
    'name' => 'Report for :name [:employee_id] - ',
    'created'            => [
        'metadata' => 'On :audit_created_at, :user_name [:audit_ip_address] created this record via :audit_url',
        'modified' => [
            'id'   => 'Record ID <strong>:new</strong>',
            'user_id'   => 'User Id <strong>:new</strong>',
            'pan'   => 'PAN number <strong>:new</strong>',
            'aadhar_no' => 'Aadhar Number<strong>:new</strong>',
            'bank_ac_no' => 'Bank A/C No. <strong>:new</strong>',
            'bank_ifsc_code' => 'Bank IFSC Code <strong>:new</strong>',
            'pf_no' => 'PF Number<strong>:new</strong>',
            'uan_no' => 'UAN Number<strong>:new</strong>',
            'esi_no' => 'ESI Number<strong>:new</strong>',
        ],
        
    ],
    'updated'            => [
        'metadata' => 'On :audit_created_at, :user_name [:audit_ip_address] updated this record via :audit_url',
        'modified' => [
            'pan'   => 'The PAN number has been modified from <strong>:old</strong> to <strong>:new</strong>',
            'aadhar_no' => 'The Aadhar Number has been modified from <strong>:old</strong> to <strong>:new</strong>',
            'bank_ac_no' => 'The Bank A/C No. has been modified from <strong>:old</strong> to <strong>:new</strong>',
            'bank_ifsc_code' => 'The Bank IFSC Code has been modified from <strong>:old</strong> to <strong>:new</strong>',
            'pf_no' => 'The PF Number has been modified from <strong>:old</strong> to <strong>:new</strong>',
            'uan_no' => 'The UAN Number has been modified from <strong>:old</strong> to <strong>:new</strong>',
            'esi_no' => 'The ESI Number has been modified from <strong>:old</strong> to <strong>:new</strong>',
        ],

    ],
];