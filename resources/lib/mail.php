<?php
	
	require_once "vendor/autoload.php";

	class EMail {

		const TO_EMAIL = 'leads@sahusoft.com';
		const TO_NAME = 'Web Contact';
		
		const FROM_NAME = 'Web Contact';
		const FROM_EMAIL = 'support@sahusoft.com';
		
		const USERNAME = 'Sahu Soft India Pvt Ltd';
		const PASSWORD = 'gIVBDCqDJCeb6NQgsMvqOQ';
		const HOSTNAME = 'smtp.mandrillapp.com';
		const CONTACT_SUBJECT = 'GeekyAnts Contact Enquiry';

		private $phpMailObj;

		public function __construct() {

			$this->phpMailObj = new PHPMailer(true);
			$this->phpMailObj->IsSMTP();
			$this->phpMailObj->Host = self::HOSTNAME;
			//$this->phpMailObj->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
			$this->phpMailObj->SMTPAuth   = true;                  // enable SMTP authentication
			$this->phpMailObj->SMTPSecure = "tls";                 // sets the prefix to the servier
			$this->phpMailObj->Port       = 587;                   // set the SMTP port for the GMAIL server
			$this->phpMailObj->Username   = self::USERNAME;
			$this->phpMailObj->Password   = self::PASSWORD;

		}

		public function send($from, $name, $body) {

			try {
				
				$this->phpMailObj->SetFrom(self::FROM_EMAIL, self::FROM_NAME);
				$this->phpMailObj->AddReplyTo($from, $name);
				//$this->phpMailObj->SetFrom($from, $name);
				// $this->phpMailObj->AddReplyTo($from, $name);
				$this->phpMailObj->AddAddress(self::TO_EMAIL, self::TO_NAME);
				$this->phpMailObj->Subject    = self::CONTACT_SUBJECT;
				$this->phpMailObj->MsgHTML($body);

				if(!$this->phpMailObj->Send()) {
					//$errors = $mail->ErrorInfo;
					return false;
				} else {
				  return true;
				}

			} catch (phpmailerException $e) {
				//echo $e->errorMessage(); //Pretty error messages from PHPMailer
			} catch (Exception $e) {
				//echo $e->getMessage(); //Boring error messages from anything else!
			}

		}

	}
