Good Morning, Your team members for below project:
@foreach( $projectDetails as $index => $projectDetail )
{{$index+1}}. {{$projectDetail['name']}} - @if(count($projectDetail['users']) > 0 && $projectDetail['totalHours'] > 0 )time logged by  @foreach ($projectDetail['users'] as $user) {{$user}}, @endforeach [ {{$projectDetail['totalHours']}} hrs ] @else no timesheet entries @endif 
@endforeach
