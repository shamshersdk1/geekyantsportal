Hey *{{$manager}}*,
a new appraisal has been activated for *{{$user}}*.
Visit {{$url}} to view more details.