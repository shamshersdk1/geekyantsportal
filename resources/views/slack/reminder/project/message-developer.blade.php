@if ( $type == 'assigned' )
@if ( $endDate != null )
You have been *assigned* to the project *{{$projectObj->project_name}}* from *{{date_in_view($startDate)}}* to *{{date_in_view($endDate)}}*
@else
You have been *assigned* to the project *{{$projectObj->project_name}}* from *{{date_in_view($startDate)}}*
@endif
@else
You have been *removed* from the project *{{$projectObj->project_name}}*.
@endif