@if ( $type == 'assigned' )
@if ( $endDate != null )
*{{$user->name}}* have been *assigned* to the project *{{$projectObj->project_name}}* from *{{date_in_view($startDate)}}* to *{{date_in_view($endDate)}}*
@else
*{{$user->name}}* have been *assigned* to the project *{{$projectObj->project_name}}* from *{{date_in_view($startDate)}}*
@endif
@else
*{{$user->name}}* have been *removed* from the project *{{$projectObj->project_name}}*.
@endif