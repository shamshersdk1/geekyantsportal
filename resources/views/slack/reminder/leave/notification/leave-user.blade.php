Hey *{{$user}}*,
your {{$leave->leaveType->title}} leave from {{date_in_view($leave->start_date)}} to {{date_in_view($leave->end_date)}} has been *{{$leave->status}}* by {{$approver}}