<html>
    <head>
        <title>GeekyAnts - Purchase Order</title>
    </head>
  <style type="text/css">
  * {
    margin:0;
    padding: 0;
  }
        body {
            font-size:12px;
            line-height:16px;
            font-family: 'arial', sans-serif;
            padding: 50px 50px 0 50px;
        }

        h1.title {
            font-size: 24px;
            margin: 30px 0;
            text-align: center;
        }
        h3 {
            margin: 20px 0 5px 0;
        }
        .text-center {
            text-align: center;
        }
        table {
            margin: 50px 0;
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;
        }
        td, th {
            padding: 5px;
            vertical-align: top;

        }
        .table-border {
            border: solid 1px #444;
        }
        .table-border td, .table-border th {
            border: solid 1px #444;
        }
        .footer {
            font-size: 10px;
            position: fixed;
            bottom: 0px;
            left: 50px;
            right: 50px;
            height: 150px;
        }
        .footer p {
            margin:0;
        }
    </style> 
        
    <body>
        <div class="wrapper">
            <div>
                <img align='right' src="images/logo-dark.png" width="150px" />
            </div>
            <div style="clear: both;">
                <h1 class="title">PURCHASE ORDER</h1>
                <table class="table">
                    <tr>
                        <td colspan="2">
                            <div style="clear:both">
                                <div class="info-table">
                                    <label>Dated As :</label> <span> {{date_in_view($purchaseOrderObj->created_at)}}</span>
                                </div>
                                <div class="info-table">
                                    <label>Purchase Order :</label> <span> PO-{{$purchaseOrderObj->id}}</span>
                                </div>
                                <div class="info-table">
                                    <span> {{$purchaseOrderObj->reference_number}}</span>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td width="50%">
                            <div>
                                <h3>About Vendor:</h3>
                                <div class="info-table">
                                    <label>Vendor Name: </label> 
                                    <span> {{$vendorContact ? $vendorContact->name : ''}}</span>
                                </div>
                                <div class="info-table">
                                    <label>Company Name: </label> <span> {{$purchaseOrderObj->vendor ? $purchaseOrderObj->vendor->name : ''}}</span>
                                </div>
                                <div class="info-table">
                                    <label>Address: </label> <span> {{$purchaseOrderObj->vendor ? $purchaseOrderObj->vendor->address : ''}}</span>
                                </div>
                                <div class="info-table">
                                    <label>Phone: </label> <span> {{!empty($purchaseOrderObj->vendor) ? $purchaseOrderObj->vendor->landline_number : $vendorContact->mobile }}</span>
                                </div>
                                <div class="info-table">
                                    <label>GST: </label> <span> {{!empty($purchaseOrderObj->vendor) ? $purchaseOrderObj->vendor->gst : $vendorContact->gst }}</span>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div>
                                <h3>Ship To:</h3>
                                <div>Kumar Pratik<br />
                                    {{ $purchaseOrderObj->company->name}}<br />
                                    {{ $purchaseOrderObj->company->shipping_address }}  
                                    
                                </div>
                                <div><b>Phone:</b> {{ $purchaseOrderObj->company->phone}}<br /><b>GST:</b> {{ $purchaseOrderObj->company->gst}}</div>
                            </div>
                        </td>
                    </tr>
                </table>
                <table class="tabel table-border" width="100%">
                    <thead>
                        <tr>
                            <th>Details</th>
                            <th class="text-center">Quantity</th>
                            <th class="text-center">Unit Price</th>
                            <th class="text-center">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach( $purchaseOrderObj->purchaseItems as $purchaseItems )
                        <tr>
                            <td>
                                {{$purchaseItems->name}}
                            </td>
                            <td class="text-center">
                                {{$purchaseItems->quantity}}
                            </td>
                            <td class="text-center">
                                {{$purchaseItems->unit_price}}
                            </td>
                            <td class="text-center">
                                {{$purchaseItems->total}}
                            </td>
                        </tr>
                        @endforeach
                        <tr>
                            <td colspan="3" style="text-align: right;">
                                <big><b>Grand Total</b></big>
                            </td>
                            <td class="text-center">
                                <big><b>{{$purchaseOrderObj->total }}</b></big>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div  style="padding-bottom: 100px;">
                <table class="table">
                    <tr>
                        <td width="50%">
                            <div>
                                <p><b>Authorised by,</b></p>
                                <br/><br />
                                <p><b>Director</b></p>
                            </div>
                        </td>
                        <td width="50%">
                            <div>
                                <p><b>Authorised by,</b></p>
                                <br/><br />
                                <p><b>Finance Head</b></p>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="footer">
            <p><b>{{strtoupper($purchaseOrderObj->company->name) }}</b>, Reg no. {{strtoupper($purchaseOrderObj->company->registration_number) }}</p>
            @if($purchaseOrderObj->company->registerd_office)
                <p><b>Regd. Office:</b> Amgola Road, Muzaffarpur, P.S: Kaji Mohammadpur, Bihar 842002 India</p>
            @endif
            @if($purchaseOrderObj->company->corporate_office)
                <p><b>Corp. Office:</b> {{$purchaseOrderObj->company->corporate_office}}</p>
            @endif
            <p>{{$purchaseOrderObj->company->phone}} | {{$purchaseOrderObj->company->email}} | {{$purchaseOrderObj->company->website}}</p>
        </div>
    </body>
</html>