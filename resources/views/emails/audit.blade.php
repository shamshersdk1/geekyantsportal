<!DOCTYPE html>
<html>
    <head>
        <style>
            p {
                font-weight:initial;
                font-size:initial;
            }
            table {
                font-family: arial, sans-serif;
                border-collapse: collapse;
                width: 100%;
            }
            caption, td, th {
                border: 1px solid #dddddd;
                text-align: left;
                padding: 8px;
            }
        </style>
    </head>
    <body>
        <div>
            <p> Summary Report: </p>
            <table>
                <thead>
                    <th>User</th>
                    <th>Model</th>
                </thead>
                @foreach ($data['message'] as $key => $value)
                    <tr rowspan={{count($value)}}>
                        <td>
                            {{\App\Models\User::find($key)->name ?? "No User"}}
                        </td>
                        <td>
                            <table>
                                @foreach ($value as $index => $item)
                                    <tr>
                                        @php
                                            $model = explode("\\", $index);
                                        @endphp
                                        <td>
                                            {{$model[count($model) - 1]}} ({{count($item)}})
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </body>
</html>