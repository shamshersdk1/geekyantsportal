<table>
	<tr>
        <td style="background: #FFF;display: inline-flex;width: 100%; border-bottom: 1px solid #DDD;">
            <img src="" style="margin:0;" height="100px" />
            <h2 style="margin: 0;padding: 15px 0 0 20px;font-style: 28px;font-weight: 600;"> <span style="font-size:18px;color: #888"><br />Leave Request</span></h2>
        </td>
    </tr>
    <tr>
        <td class="content price-details" style="padding:0 20px; font-size: 20px;">
            <table> 
            	<tr>
            		<td colspan="2">
            			<h4 style="font-weight: 500;margin:0;">{{$name}} has reqested for leave</h4>
            		</td>
            	</tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="content">
            <p align="center" style="margin:0;padding:15px;">
                <a href="{{ url('/admin/leave-confirmation') }}" class="button" style="display: inline-block;color: white;background: #71bc37;border: solid #71bc37;border-width: 10px 20px 8px;border-radius: 4px;text-decoration: none;font-size: 20px">See Request</a>
            </p>
        </td>
    </tr>
</table>
 