@extends('layouts.email')
@section('css')
@parent
<style type="text/css">
   .email-content{
   padding: 20px;
   }
   .email-content p{
   line-height: 26px;
   }
   .leave-approved {
       background-color:#00C851;
   }
   .leave-cancelled {
       background-color:#ffbb33;
   }
   .leave-rejected {
       background-color:#ff4444;
   }
</style>
@endsection
@section('content')
<!-- <div class="email-content">
   <h2>Hello {{$array['user']}},</h2>
      
      @if($array['leave']->status == 'rejected')
          <p>
              <b>{{$array['leave']->user->name}}'s {{$array['leave']->type}} leave</b> was <b>{{$array['leave']->status}}</b> by <b>{{$array['leave']->approver->name}}</b> for <b>{{$array['leave']->days}}</b> working days from <b>{{date("M d, Y", strtotime($array['leave']->start_date))}}</b> to <b>{{date("M d, Y", strtotime($array['leave']->end_date))}}</b> due to <b>{{$array['leave']->cancellation_reason}}</b>.
          </p>
      @else
          @if($array['leave']->status == 'cancelled')
              <p>
              @if($array['leave']->user->name == $array['user'])
                  @if($array['leave']->user->id == $array['leave']->approver->id)
                      <b>You</b> <b>{{$array['leave']->status}}</b> your <b>{{$array['leave']->type}}</b> leave of <b>{{$array['leave']->days}}</b> working days from <b>{{date("M d, Y", strtotime($array['leave']->start_date))}}</b> to <b>{{date("M d, Y", strtotime($array['leave']->end_date))}}</b>.
                  @else
                      <b>Your {{$array['leave']->type}}</b> leave of <b>{{$array['leave']->days}}</b> working days from <b>{{date("M d, Y", strtotime($array['leave']->start_date))}}</b> to <b>{{date("M d, Y", strtotime($array['leave']->end_date))}}</b> was <b>{{$array['leave']->status}}</b> by <b>{{$array['leave']->approver->name}}</b>.
                  @endif
              @else
                  <b>{{$array['leave']->user->name}}'s {{$array['leave']->type}}</b> leave of <b>{{$array['leave']->days}}</b> working days from <b>{{date("M d, Y", strtotime($array['leave']->start_date))}}</b> to <b>{{date("M d, Y", strtotime($array['leave']->end_date))}}</b> was <b>{{$array['leave']->status}}</b> by <b>{{$array['leave']->approver->name}}</b>.
              @endif
              </p>
          @else
              <p>
                  <b>{{$array['leave']->user->name}}'s {{$array['leave']->type}} leave</b> was <b>{{$array['leave']->status}}</b> by <b>{{$array['leave']->approver->name}}</b> for <b>{{$array['leave']->days}}</b> working days from <b>{{date("M d, Y", strtotime($array['leave']->start_date))}}</b> to <b>{{date("M d, Y", strtotime($array['leave']->end_date))}}</b>.
              </p>
          @endif
      @endif
   </div> -->
    <tr>
        <td style="height: 31px;text-align:center;color:#FFFFFF;font-size: 15px;" class="leave-{{$array['leave']->status}}">Leave {{ucfirst($array['leave']->status)}}</td>
    </tr>

<tr>
   <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr>
            <td width="30" class="digiby_hide">&nbsp;</td>
            <td valign="top" class="digiby_side_space">
               <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                     <td height="26"></td>
                  </tr>
                  <tr>
                     <td class="digiby_dark_grey_txt" style="font-family:Arial, sans-serif;font-size:20px;text-align:left;color:#333333;line-height:23px;">
                        Hello {{$array['user']}}, 
                     </td>
                  </tr>
                  <tr>
                     <td height="10" style="line-height:1px;font-size:1px;">&nbsp;</td>
                  </tr>
                  <tr>
                     <td class="digiby_grey_txt" style="font-family:Arial, sans-serif;font-size:13px;text-align:left;color:#333 !important;line-height:20px;">
                        @if($array['leave']->status == 'rejected')
                        <P style="color:#333 !important">
                           <b >{{$array['leave']->user->name}}'s {{$array['leave']->leaveType->title}} leave</b> was <b>{{$array['leave']->status}}</b> by <b>{{$array['leave']->approver->name}}</b> for <b>{{$array['leave']->days}}</b> working days from <b>{{date("M d, Y", strtotime($array['leave']->start_date))}}</b> to <b>{{date("M d, Y", strtotime($array['leave']->end_date))}}</b> due to <b>{{$array['leave']->cancellation_reason}}</b>.
                        </p>
                        @else
                        @if($array['leave']->status == 'cancelled')
                        <p style="color:#333 !important">
                           @if($array['leave']->user->name == $array['user'])
                           @if($array['leave']->user->id == $array['leave']->approver->id)
                           <b>You</b> <b>{{$array['leave']->status}}</b> your <b>{{$array['leave']->leaveType->title}}</b> leave of <b>{{$array['leave']->days}}</b> working days from <b>{{date("M d, Y", strtotime($array['leave']->start_date))}}</b> to <b>{{date("M d, Y", strtotime($array['leave']->end_date))}}</b>.
                           @else
                           <b>Your {{$array['leave']->leaveType->title}}</b> leave of <b>{{$array['leave']->days}}</b> working days from <b>{{date("M d, Y", strtotime($array['leave']->start_date))}}</b> to <b>{{date("M d, Y", strtotime($array['leave']->end_date))}}</b> was <b>{{$array['leave']->status}}</b> by <b>{{$array['leave']->approver->name}}</b>.
                           @endif
                           @else
                           <b>{{$array['leave']->user->name}}'s {{$array['leave']->leaveType->title}}</b> leave of <b>{{$array['leave']->days}}</b> working days from <b>{{date("M d, Y", strtotime($array['leave']->start_date))}}</b> to <b>{{date("M d, Y", strtotime($array['leave']->end_date))}}</b> was <b>{{$array['leave']->status}}</b> by <b>{{$array['leave']->approver->name}}</b>.
                           @endif
                        </p>
                        @else
                        <p style="color:#333 !important">
                           <b>{{$array['leave']->user->name}}'s {{$array['leave']->leaveType->title}} leave</b> was <b>{{$array['leave']->status}}</b> by <b>{{$array['leave']->approver->name}}</b> for <b>{{$array['leave']->days}}</b> working days from <b>{{date("M d, Y", strtotime($array['leave']->start_date))}}</b> to <b>{{date("M d, Y", strtotime($array['leave']->end_date))}}</b>.
                        </p>
                        @endif
                        @endif
                     </td>
                  </tr>
                  <tr>
                     <td height="18" style="line-height:1px;font-size:1px;">&nbsp;</td>
                  </tr>
                  <tr>
                     <td height="36"></td>
                  </tr>
               </table>
            </td>
            <td width="30" class="digiby_hide">&nbsp;</td>
         </tr>
      </table>
   </td>
</tr>
@endsection
@section('js')
@parent
@endsection