<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="#" rel="stylesheet" type="text/css">
        <style>
            html, body {
                height: 100%;
                background-color:#F9F9F9;
            }
            body {
                margin: 0 auto;
                padding: 15px;
                text-align: center;
            }
            img {
                width: 200px;
                margin: 0 auto;
                padding: 15px;
            }
            .white-pages {
                width:50%;
                background-color: #FFFFFF;
                margin: 0 auto;
                border-radius: 7px;
                padding: 5px;
            }
           .sick-leave-apply {
                text-align:left;
                margin-left:85px;
                margin-top: 30px;
                padding-bottom: 15px;
            }
            .medkit-icon {
                margin-right:5px;
                font-size:20px;
            }
            .sick-leave-type{
                font-size:20px;
                margin-top: 15px;
            }
            .seek-day-leave p {
                text-align: justify;
                padding-left: 145px;
                padding-bottom: 7px;
                margin: 0 auto;
            }
       </style>
    </head>
    <body>
        <div class="white-pages">
            <div class="image">
                <img src="images/logo-dark.png">
            </div>
            <div>
                <h6>GeekyAnts’ Weekly Employee-leave</h6>
            </div>    
            <div>    
                <h6> Sunday, february 11th - saturday, february 17th</h6>
            </div>
            <div style="margin-top: 45px;">
                <h5 style="font-size: 18px;">Hope you had a good weekend! Here's a summary of Last Week leaves</h5>
            </div>
            <div>
                <div class="sick-leave-apply">
                    <span class="medkit-icon"><i class="fa fa-medkit"></i></span>
                    <span class="sick-leave-type">Four people were on Sick-leave :</span>
                </div>
                <div class="seek-day-leave">
                    <p><b>Chetan</b> was on leave on 12th february, Monday .</p>
                    <p><b>Ayushi</b> was on leave on 15th february, Thursday .</p>
                    <p><b>Rudra</b> was on leave on 15th february, Thursday .</p>
                    <p><b>Saarika</b> was on leave on 16th february, Friday .</p>
                </div>
            </div>
            <div>
                <div class="sick-leave-apply">    
                    <span class="medkit-icon"><i class="fa fa-money"></i></span>
                    <span class="sick-leave-type">Two people were on Paid-leave :</span>
                </div>
                <div class="seek-day-leave">
                    <p><b>Sachin</b> was on leave on 12th february, Monday .</p>
                    <p><b>Mushin</b> was on leave on 15th february, Thursday .</p>
                    <p><b>Saurabh.K</b> was on leave on 16th february, Friday .</p>
                </div>
            </div>
            <div>
                <div class="sick-leave-apply">    
                    <span class="medkit-icon"><i class="fa fa-info-circle"></i></span>
                    <span class="sick-leave-type">Three people were on Unpaid-leave :</span>
                </div>
                <div class="seek-day-leave">
                    <p><b>Sachin</b> was on leave on 13th february, Tuesday .</p>
                    <p><b>Mushin</b> was on leave on 15th february, Thursday .</p>
                </div>
            </div>
            <div style="margin-top: 20px; padding: 20px;">
                <a href="#">View a complete list of member changes</a>
            </div>
        <hr/>
        </div>
        <div></div>
    </body>
</html>