	<table>
    	<tr>
            <td style="background: #FFF;display: inline-flex;width: 100%; border-bottom: 1px solid #DDD;">
                <img src="" style="margin:0;" height="100px" />
                <h2 style="margin: 0;padding: 15px 0 0 20px;font-style: 28px;font-weight: 600;"> <span style="font-size:18px;color: #888"><br />Password reset request.</span></h2>
            </td>
        </tr>
        <tr>
            <td class="content price-details" style="padding:0 20px; font-size: 20px;">
                <table> 
                	<tr>
                		<td colspan="2">
                			<h4 style="font-weight: 500;margin:0;">Hi {{$name}}</h4>
                		</td>
                	</tr>
                	<tr>
                		<td colspan="2">
                			Welcome to the GeekyAnts.
                		</td>
                	</tr>
                    <tr>
                       
                    </tr>
                    <tr>
                       
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="content">
                <p align="center" style="margin:0;padding:15px;">
                    <a href="{{ url('/password/reset-password/'.$token . '?email=' . urlencode($email)) }}" class="button" style="display: inline-block;color: white;background: #71bc37;border: solid #71bc37;border-width: 10px 20px 8px;border-radius: 4px;text-decoration: none;font-size: 20px">Reset Password</a>
                </p>
            </td>
        </tr>
        <tr>
            <td class="content" style="padding:5px 20px 5px 0;">
                <h2 align="right" style="margin:0; color:green;font-weight: 600;font-size: 26px;">Thanks for being a GeekyAnts customer.</h2>
            </td> 
        </tr>
        <tr>
            <td class="content" style="padding:5px 20px 5px 0;">
                <h2 align="right" style="margin:0; color:green;font-weight: 600;font-size: 26px;">Sincerely,
                 GeekyAnts</h2>
            </td>
        </tr>
        <tr>
            <td>
              
            </td>
        </tr>
    </table>
 