@extends('layouts.email')
@section('css')
@parent
<style type="text/css">
   .email-content{
   padding: 20px;
   }
   .email-content p{
   line-height: 26px;
   }
</style>
@endsection
@section('content')
<tr>
    <td style="height: 31px;background-color:#2477BA;text-align:center;color:#FFFFFF;font-size: 15px;">Loan Reviewed</td>
</tr>
<tr>
   <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr>
            <td width="30" class="digiby_hide">&nbsp;</td>
            <td valign="top" class="digiby_side_space">
               <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                     <td height="26"></td>
                  </tr>
                  <tr>
                     <td class="digiby_dark_grey_txt" style="font-family:Arial, sans-serif;font-size:20px;text-align:left;color:#333333;line-height:23px;">Hello, </td>
                  </tr>
                  <tr>
                     <td height="10" style="line-height:1px;font-size:1px;">&nbsp;</td>
                  </tr>
                  <tr>
                     <td class="digiby_grey_txt" style="font-family:Arial, sans-serif;font-size:13px;text-align:left;color:#333 !important;line-height:20px;">
                        The loan raised for <b>{{$array['user']}}</b> for the amount of <b>{{$array['amount']}}</b> on <b>{{$array['date']}}</b> is reviewed by <b>Human Resources</b>. 
                        <br />
                     </td>
                  </tr>
                  <tr>
                     <td height="18" style="line-height:1px;font-size:1px;">&nbsp;</td>
                  </tr>
                  <tr>
                     <td class="digiby_grey_txt" style="font-family:Arial, sans-serif;font-size:13px;text-align:left;color:#333;line-height:20px;">
                        Please visit  <a class="btn" href="{{\Config::get('app.geekyants_portal_url')}}/admin/reviewed-loan-requests/{{$array['id']}}"><u> the portal</u> </a> to <b style="color:#333 !important;">reconcile</b> the loan.
                     </td>
                  </tr>
                  <tr>
                     <td height="18" style="line-height:1px;font-size:1px;">&nbsp;</td>
                  </tr>
                  <tr>
                     <td height="36"></td>
                  </tr>
               </table>
            </td>
            <td width="30" class="digiby_hide">&nbsp;</td>
         </tr>
      </table>
   </td>
</tr>
@endsection
@section('js')
@parent
@endsection