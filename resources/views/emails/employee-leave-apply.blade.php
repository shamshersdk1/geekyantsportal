@extends('layouts.email')
@section('css')
@parent
<style type="text/css">
   .email-content{
   padding: 20px;
   }
   .email-content p{
   line-height: 26px;
   }
</style>
@endsection
@section('content')
<!-- <div class="email-content">
   <h2>Hello {{$array['admin']}},</h2>
      <p>
          A <b>{{$array['leave']->type}} leave</b> was requested by <b>{{$array['leave']->user->name}}</b> for <b>{{$array['leave']->days}}</b> working days from <b>{{date("M d, Y", strtotime($array['leave']->start_date))}}</b> to <b>{{date("M d, Y", strtotime($array['leave']->end_date))}}</b>.
           <br />
           @if(!empty($array['previousLeaves']) && count($array['previousLeaves']) > 0)<b>Previous Leaves:</b> @foreach($array['previousLeaves'] as $prev) <span> {{ucfirst($prev->type)}} leave from {{$prev->start_date}} to {{$prev->end_date}} </span> <br> @endforeach @endif
           <br/>
           @if($array['admin'] == 'Admin')
              Please visit  <a href="{{\Config::get('app.geekyants_portal_url').'/admin/leave-section/confirmation'}}"> the dashboard </a> to <b>approve</b> or <b>reject</b> the leave.
           @else
              Please visit  <a href="{{\Config::get('app.geekyants_portal_url').'/admin/reporting-manager-leaves'}}"> the dashboard </a> to <b>approve</b> or <b>reject</b> the leave.
           @endif
      </p>
   </div> -->
    <tr>
        <td style="height: 31px;background-color:#2477BA;text-align:center;color:#FFFFFF;font-size: 15px;">New Leave Request</td>
    </tr>
<tr>
   <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr>
            <td width="30" class="digiby_hide">&nbsp;</td>
            <td valign="top" class="digiby_side_space">
               <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                     <td height="26"></td>
                  </tr>
                  <tr>
                     <td class="digiby_dark_grey_txt" style="font-family:Arial, sans-serif;font-size:20px;text-align:left;color:#333333;line-height:23px;">Hello {{$array['admin']}}, </td>
                  </tr>
                  <tr>
                     <td height="10" style="line-height:1px;font-size:1px;">&nbsp;</td>
                  </tr>
                  <tr>
                     <td class="digiby_grey_txt" style="font-family:Arial, sans-serif;font-size:13px;text-align:left;color:#333 !important;line-height:20px;">
                        A <b>{{$array['leave']->leaveType->title}} leave</b> was requested by <b>{{$array['leave']->user->name}}</b> for <b>{{$array['leave']->days}}</b> working days from <b>{{date("M d, Y", strtotime($array['leave']->start_date))}}</b> to <b>{{date("M d, Y", strtotime($array['leave']->end_date))}}</b>. 
                        <br />
                        <!-- @if(!empty($array['previousLeaves']) && count($array['previousLeaves']) > 0)<b style="color:#333 !important;">Previous Leaves:</b>  @foreach($array['previousLeaves'] as $prev) <span> {{ucfirst($prev->type)}} leave from {{$prev->start_date}} to {{$prev->end_date}} </span> <br> @endforeach @endif
                           <br/> -->
                     </td>
                  </tr>
                  <tr>
                     <td height="18" style="line-height:1px;font-size:1px;">&nbsp;</td>
                  </tr>
                  <tr>
                     <td class="digiby_grey_txt" style="font-family:Arial, sans-serif;font-size:13px;text-align:left;color:#333;line-height:20px;">
                        <b> Total Sick Leave:</b><span>{{$array['sick']}}</span><br/>
                        <b> Total Earned Leave:</b><span>{{$array['earned']}}</span><br/>
                     </td>
                  </tr>
                  <tr>
                     <td height="18" style="line-height:1px;font-size:1px;">&nbsp;</td>
                  </tr>
                  <tr>
                     <td class="digiby_grey_txt" style="font-family:Arial, sans-serif;font-size:13px;text-align:left;color:#333;line-height:20px;">
                        <b style="color:#333 !important;">Previous Leaves:</b>
                     </td>
                  </tr>
                  <tr>
                    @if(empty($array['previousLeaves']) || count($array['previousLeaves']) == 0)
                        <td>No previous leaves</td>
                    @else
                     <td class="digiby_grey_txt" style="font-family:Arial, sans-serif;font-size:13px;text-align:left;color:#333;line-height:20px;">
                        <table width="100%" cellpadding="0" cellspacing="0"  border="1" bordercolor="#D5DDF1" style="min-width:100%; border:1px solid #D5DDF1">
                           <thead>
                              <tr>
                                 <th scope="col" style="padding:5px;color:#333 !important; font-family: Arial,sans-serif; font-size: 13px; line-height:16px;">From</th>
                                 <th scope="col" style="padding:5px; color:#333 !important; font-family: Arial,sans-serif; font-size: 13px; line-height:16px;">To</th>
                                 <th scope="col" style="padding:5px; color:#333 !important; font-family: Arial,sans-serif; font-size: 13px; line-height:16px;">Leave type</th>
                                 <th scope="col" style="padding:5px; color:#333 !important; font-family: Arial,sans-serif; font-size: 13px; line-height:16px;">Working days</th>
                              </tr>
                           </thead>
                           <tbody>
                                 @foreach($array['previousLeaves'] as $prev)
                                    <tr>
                                        <td valign="top"  style="padding:5px; color:#333 !important; font-family: Arial,sans-serif; font-size: 13px; line-height:16px;">{{date_in_view($prev->start_date)}}</td>
                                        <td valign="top"  style="padding:5px; color:#333 !important; font-family: Arial,sans-serif; font-size: 13px; line-height:16px;">{{date_in_view($prev->end_date)}} </td>
                                        <td valign="top"  style="padding:5px; color:#333 !important; font-family: Arial,sans-serif; font-size: 13px; line-height:16px;">{{ucfirst($prev->leaveType->title)}}</td>
                                        <td valign="top"  align="center" style="padding:5px; color:#333 !important; font-family: text-align:center;Arial,sans-serif; font-size: 13px; line-height:16px;">{{$prev->days}}</td>
                                    </tr>
                                 @endforeach
                           </tbody>
                        </table>
                     </td>
                    @endif
                  </tr>
                  <tr>
                     <td height="18" style="line-height:1px;font-size:1px;">&nbsp;</td>
                  </tr>
                  <tr>
                     <td class="digiby_grey_txt" style="font-family:Arial, sans-serif;font-size:13px;text-align:left;color:#333;line-height:20px;">
                        @if($array['admin'] == 'Admin')
                        Please visit  <a href="{{\Config::get('app.geekyants_portal_url').'/admin/leave-section/confirmation'}}"> the dashboard </a> to <b style="color:#333 !important;">approve</b> or <b style="color:#333 !important;">reject</b> the leave.
                        @else
                        Please visit  <a href="{{\Config::get('app.geekyants_portal_url').'/admin/reporting-manager-leaves'}}"> the dashboard </a> to <b style="color:#333 !important;">approve</b> or <b style="color:#333 !important;">reject</b> the leave.
                        @endif
                     </td>
                  </tr>
                  <tr>
                     <td height="18" style="line-height:1px;font-size:1px;">&nbsp;</td>
                  </tr>
                  <tr>
                     <td height="36"></td>
                  </tr>
               </table>
            </td>
            <td width="30" class="digiby_hide">&nbsp;</td>
         </tr>
      </table>
   </td>
</tr>
@endsection
@section('js')
@parent
@endsection