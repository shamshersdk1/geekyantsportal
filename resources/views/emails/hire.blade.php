<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
   xmlns:v="urn:schemas-microsoft-com:vml"
   xmlns:o="urn:schemas-microsoft-com:office:office">
   <head>
      <title></title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0 " />
      <meta name="format-detection" content="telephone=no" />
      <style type="text/css">
         body {
         margin: 0 !important;
         padding: 0 !important;
         -webkit-text-size-adjust: 100% !important;
         -ms-text-size-adjust: 100% !important;
         -webkit-font-smoothing: antialiased !important;
         }
         img {
         border: 0 !important;
         outline: none !important;
         }
         p {
         margin: 0px !important;
         padding: 0px !important;
         }
         table {
         border-collapse: collapse;
         mso-table-lspace: 0px;
         mso-table-rspace: 0px;
         }
         td, a, span {
         border-collapse: collapse;
         mso-line-height-rule: exactly;
         }
         .ExternalClass * {
         line-height: 100%;
         }
         .digiby_red_link a {
         text-decoration: none;
         color: #333;
         }
         .digiby_white_link a {
         text-decoration: none;
         color: #ffffff;
         }
         .digiby_dark_grey_txt a {
         text-decoration: none;
         color: #333333;
         }
         .digiby_grey_txt a {
         text-decoration: none;
         color: #333;
         }
         .digiby_yellow_link a {
         text-decoration: none;
         color: #d9bc32;
         }
         .digiby_green_link a {
         text-decoration: none;
         color: #94b93b;
         }
         .digiby_blue_link a {
         text-decoration: none;
         color: #58b2d4;
         }
         .digiby_golden_link a {
         text-decoration: none;
         color: #333;
         }
         .digiby_footer a {
         text-decoration: none;
         color: #333;
         }
         .digiby_br1 {
         display: none;
         }
         @media only screen and (min-width:480px) and (max-width:650px) {
         table[class=digiby_main_table] {
         width: 100% !important;
         }
         table[class=digiby_wrapper] {
         width: 100% !important;
         }
         td[class=digiby_hide], br[class=digiby_hide] {
         display: none !important;
         }
         td[class=digiby_align_center] {
         text-align: center !important;
         }
         td[class=digiby_pad_top] {
         padding-top: 20px !important;
         }
         td[class=digiby_side_space] {
         padding-left: 20px !important;
         padding-right: 20px !important;
         }
         td[class=digiby_bg_center] {
         background-position: center !important;
         }
         img[class=digiby_full_width] {
         width: 100% !important;
         height: auto !important;
         max-width: 100% !important;
         }
         td[class=digiby_pad_btm] {
         padding-bottom: 28px !important;
         }
         }
         @media only screen and (max-width:480px) {
         table[class=digiby_main_table] {
         width: 100% !important;
         }
         table[class=digiby_wrapper] {
         width: 100% !important;
         }
         td[class=digiby_hide], br[class=digiby_hide] {
         display: none !important;
         }
         td[class=digiby_align_center] {
         text-align: center !important;
         }
         td[class=digiby_pad_top] {
         padding-top: 20px !important;
         }
         td[class=digiby_side_space] {
         padding-left: 20px !important;
         padding-right: 20px !important;
         }
         td[class=digiby_bg_center] {
         background-position: center !important;
         }
         td[class=digiby_height_80] {
         height: 200px !important;
         }
         img[class=digiby_full_width] {
         width: 100% !important;
         height: auto !important;
         max-width: 100% !important;
         }
         td[class=digiby_pad_btm] {
         padding-bottom: 28px !important;
         }
         td[class=digiby_width_178] {
         width: 178px !important;
         }
         }
      </style>
   </head>
   <body style="margin:0px; padding:0px; background-color:#fff;">
      <!--Full width table start-->
      <table width="100%" border="0" cellspacing="0" cellpadding="0" align="left" style="background-color:#fff;">
         <tr>
            <td align="center" valign="top">
               <table align="left" class="digiby_main_table" width="650" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;" bgcolor="#ffffff">
                  <tr>
                     <td class="digiby_hide" style="line-height:1px;min-width:650px;" bgcolor="#ffffff"><img src="http://www.emailonacid.com/images/emails/emailmonks1/spacer.gif" height="1"  width="650" style="max-height:1px; min-height:1px; display:block; width:650px; min-width:650px;" border="0" /></td>
                  </tr>
                  <tr>
                     <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                           <tr>
                              <td width="30" class="digiby_hide">&nbsp;</td>
                              <td valign="top" class="digiby_side_space">
                                 <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                       <td height="26"></td>
                                    </tr>
                                    <tr>
                                       <td height="10" style="line-height:1px;font-size:1px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                       <td class="digiby_grey_txt" style="font-family:Arial, sans-serif;font-size:13px;text-align:left;color:#333333!important;line-height:20px;">
                                          @if(!empty($email->name))
                                          Name : {{$email->name}}  <br/>
                                          @endif
                                        
                                          @if(!empty($email->email))
                                          Email : {{$email->email}}  <br/>
                                          @endif
                                        
                                          @if(!empty($email->company))
                                          Company : {{$email->company}} <br/>
                                          @endif
                                          @if(!empty($email->skype))
                                          Skype : {{$email->skype}}<br/>
                                          @endif 
                                          @if(!empty($email->preferred_timezone))
                                          Preferred Timezone : {{$email->preferred_timezone}}<br/>
                                          @endif
                                          @if(!empty($email->referred_by))
                                          Refered By : {{$email->referred_by}}<br/>
                                          @endif
                                          @if(!empty($email->requirement))
                                          Requirement : {{$email->requirement}}<br/>
                                          @endif
                                          
                                          @if(!empty($email->project_type))
                                          Project Type : {{ucfirst($email->project_type)}} Project
                                          @endif<br/><br/>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td class="digiby_grey_txt" style="font-family:Arial, sans-serif;font-size:13px;text-align:left;color:#333333;line-height:20px;">
                                          @if(!empty($email->technology) && count($email->technology)>0)
                                          Using the following technologies : <br/>
                                          <ul>
                                             @foreach($email->technology as $tech)
                                             <li> {{$tech}}</li>
                                             @endforeach
                                          </ul>
                                          @endif<br/>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td class="digiby_grey_txt" style="font-family:Arial, sans-serif;font-size:13px;text-align:left;color:#333333;line-height:20px;">
                                          @if(!empty($email->technology) && count($email->docs)>0)
                                          I have the following functional documents : <br/>
                                          <ul>
                                             @foreach($email->docs as $doc)
                                             <li>
                                                {{$doc}}
                                             </li>
                                             @endforeach
                                          </ul>
                                          @endif
                                       </td>
                                    </tr>
                                    <tr>
                                       <td height="18" style="line-height:1px;font-size:1px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                       <td height="36"></td>
                                    </tr>
                                    
                                    <tr>
                                       <td height="36"></td>
                                    </tr>
                                 </table>
                              </td>
                              <td width="30" class="digiby_hide">&nbsp;</td>
                           </tr>
                        </table>
                     </td>
                  </tr>
                  <tr>
                     <td height="30"></td>
                  </tr>
               </table>
            </td>
         </tr>
      </table>
   </body>
</html>