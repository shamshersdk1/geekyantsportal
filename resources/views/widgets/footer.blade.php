    <section class="primary-footer hidden">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- <div class="col-sm-4 no-padding col-xs-12 col-sm-4">
                        <a class="" href="/home"><img style="height: 36px;" src="{{ asset('images/logo-with-base-422X100.png') }}" class="img-responsive footer-logo"></a>
                    </div>
                    <div class="col-md-8 col-xs-12 col-sm-8 col-md-push-6 " style="padding-right: 0px;">
                        <div class="number-pro">10,680,916<i class="fa fa-plus number" aria-hidden="true"></i>Projects</div>
                    </div> -->
                    <div class="col-sm-3">
                        <h2 class="text-center count-number">10 </h2>
                        <h6 class="text-center text-uppercase about-number">Years of Experience</h6>
                    </div>
                    <div class="col-sm-3">
                        <h2 class="text-center count-number">50 </h2>
                        <h6 class="text-center text-uppercase about-number">Team Members</h6>
                    </div>
                    <div class="col-sm-3">
                        <h2 class="text-center count-number">20</h2>
                        <h6 class="text-center text-uppercase about-number">Clients</h6>
                    </div>
                    <div class="col-sm-3">
                        <h2 class="text-center count-number">5000</h2>
                        <h6 class="text-center text-uppercase about-number">Projects</h6>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- ====================== footer ===================== -->
    <footer class="clearfix">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 footer-meet-geeks">
                    <h4 class="meet-geeks">Meet Geeks</h4>
                    <ul>
                        <li><a href="/team">Our Team</a></li>
                        <li><a href="/culture" target="_blank">Culture @GeekyAnts</a></li>
                        <li><a href="/gallery" target="_blank">Gallery</a></li>
                        <li><a href="/jobs">Careers</a></li>
                        <li><a href="https://blog.geekyants.com/" target="_blank">Blog</a></li>
                        
                    </ul>
                </div>
                <div class="col-sm-4 footer-meet-geeks">
                    <h4 class="meet-geeks">Quick Links</h4>
                    <ul>
                        <li><a href="/open-source">Products</a></li>
                        <li><a href="/service">Services</a></li>
                        <li><a href="/technology">Technologies</a></li>
                        <li><a href="/events">Events</a></li>
                        <li><a href="/contact">Contact</a></li>
                    </ul>
                </div>
                <div class="col-sm-4 footer-meet-geeks">
                    <h4 class="meet-geeks">Be Tuned With Us</h4>
                    
                    <!-- Begin MailChimp Signup Form -->
                    <div id="mc_embed_signup">
                        <form action="//sahusoft.us10.list-manage.com/subscribe/post?u=1255321b94080746030fac796&amp;id=2d9293ea37" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                            <div id="mc_embed_signup_scroll">
                                <div class="mc-field-group input-group">
                                    <span class="input-group-addon envelope-adon" id="basic-addon1"><i class="fa fa-envelope"></i></span>
                                    <input class="form-control subscribe-input" type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
                                    <span class="input-group-btn">
                                        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_1255321b94080746030fac796_2d9293ea37" tabindex="-1" value=""></div>
                                        <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="btn btn-danger">
                                    </span>
                                </div>
                                    
                                <div id="mce-responses" class="clear">
                                    <div class="response" id="mce-error-response" style="display:none"></div>
                                    <div class="response" id="mce-success-response" style="display:none"></div>
                                </div> 
                                <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                            </div>
                        </form>
                    </div>

                    <!--End mc_embed_signup-->
                    
                    <div class="social-icon">
                        <a href="https://www.facebook.com/geekyants" target="_blank"><div class="fa-icon-space"><i class="fa fa-facebook-official fa-2x fa-icon" aria-hidden="true"></i></div></a>
                        <a href="https://twitter.com/geekyants" target="_blank"><div class="fa-icon-space "><i class="fa fa-twitter-square footer-twitter fa-2x fa-icon" aria-hidden="true"></i></div></a>
                        <a href="https://www.linkedin.com/company/geekyants-software-pvt-ltd" target="_blank"><div class="fa-icon-space"><i class="fa fa-linkedin-square fa-2x fa-icon" aria-hidden="true"></i></div></a>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </footer>