<style type="text/css">
body{
	line-height:12px;
	font-family:"Arial Black";
}
	table td, table th{
		border:1px solid black;
		padding: 5px;
	}
	table{
		table-layout:fixed;
		border-collapse:collapse;
	}
	td{
		line-height:14px;
	}
	th{
		font-weight:normal;
		line-height:20px;
	}
	.footer{
		font-size:14px;
	}
	.value{
		font-weight:bold;
	}
	.tablehead{
		margin-bottom:5px;
	}
	
</style>
<head>
    <title>
        Payslip
    </title>
</head>

<body>
	<div class="row" style="">
        <img align='right' src="images/logo-dark.png" height="25px" width="125px"></img>
    </div>
    <div>
        <div class="row" style="clear:both">
            <p align="CENTER"><u><b>SALARY PAYSLIP </b></u></p>
        </div>
        <p align="CENTER" class="tablehead"><u><b>PERSONAL DETAILS</b></u></p>
        <div id="table1" >
            <table align="center" width="93%" class="table table-fixed">
                <colgroup>
                    <col width="20%">
                    <col width="40%">
                    <col width="20%">
                    <col width="20%">
                </colgroup>
                <tbody>
                    <tr>
                        <td>
                            Employee Name
                        </td>
                        <td class="value" colspan="3">
                            {{empty($payslip->user)?"":$payslip->user->name}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Duration
                        </td>
                        <td class="value" style="font-size:14px">
                            @if(!empty($payslip->payslip_month))
                                @if($payslip->payslip_month->month < 4 && $payslip->payslip_month->year <= 2018)
                                    {{DateTime::createFromFormat('!m',$payslip->payslip_month->month)->format('F').' '.$payslip->payslip_month->year}}
                                @else
                                    @php
                                        $end_date = $payslip->payslip_month->year."-".substr("0".$payslip->payslip_month->month, -2)."-25";
                                        $start_date = date("Y-m-26", strtotime("-1 month" ,strtotime($end_date)));
                                    @endphp
                                    {{date("j M, y", strtotime($start_date))}} to {{date("j M, y", strtotime($end_date))}}
                                @endif
                            @endif
                        </td>
                        <td>
                            Employee Code
                        </td>
                        <td class="value">
                            {{empty($payslip->user)?"":$payslip->user->employee_id}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            No. of Working Days
                        </td>
                        <td class="value">
                            {{empty($payslip->working_day_month)?"":$payslip->working_day_month}}
                        </td>
                        <td>
                            No. of Days Worked
                        </td>
                        <td class="value">
                            {{empty($payslip->days_worked)?"":$payslip->days_worked}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Designation
                        </td>
                        <td class="value">
                            {{empty($payslip->designation)?"":$payslip->designation}}
                        </td>
                        <td>
                            UAN
                        </td>
                        <td class="value">
                            {{empty($payslip->uan_no)?"":$payslip->uan_no}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            PAN
                        </td>
                        <td class="value">
                            {{empty($payslip->pan_no)?"":$payslip->pan_no}}
                        </td>
                        <td>
                            DOJ
                        </td>
                        <td class="value">
                            {{empty($payslip->date_of_joining)?"":date_in_view($payslip->date_of_joining)}}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <br>
        <p align="CENTER" class="tablehead"> <b><u>SALARY BREAK-UP</u></b></p>
        <table width="93%" align="center">
            <colgroup><col width="25%">
                <col width="25%">
                <col width="25%">
                <col width="25%">
            </colgroup>
            <tbody>
                <tr>
                    <td colspan="2" align="center">
                        EARNINGS
                    </td>
                    <td colspan="2" align="center">
                        DEDUCTIONS
                    </td>
                </tr>
                <tr>
                    <td>
                        Basic
                    </td>
                    <td class="value">
                        {{empty($payslip->basic)?"0":$payslip->basic}}
                    </td>
                    <td>
                        Employee's P.F.
                    </td>
                    <td class="value">
                        {{empty($payslip->pf_12_employee)?"0":$payslip->pf_12_employee}}
                    </td>
                </tr>
                <tr>
                    <td>
                        House Rent Allowance
                    </td>
                    <td class="value">
                        {{empty($payslip->hra)?"0":$payslip->hra}}
                    </td>
                    <td>
                        Employee's E.S.I.
                    </td>
                    <td class="value">
                        {{empty($payslip->esi_1_75_employee)?"0":$payslip->esi_1_75_employee}}
                    </td>
                </tr>
                <tr>
                    <td>
                        Conveyance Allowance
                    </td>
                    <td class="value">
                        {{empty($payslip->conveyance_allowance)?"0":$payslip->conveyance_allowance}}
                    </td>
                    <td>
                        Voulantary P.F.
                    </td>
                    <td class="value">
                        {{empty($payslip->vpf)?"0":$payslip->vpf}}
                    </td>
                </tr>
                <tr>
                    <td>
                        Car Allowance
                    </td>
                    <td class="value">
                        {{empty($payslip->car_allowance)?"0":$payslip->car_allowance}}
                    </td>
                    <td>
                        P.T.
                    </td>
                    <td class="value">
                        {{empty($payslip->professional_tax)?"0":$payslip->professional_tax}}
                    </td>
                </tr>
                <tr>
                    <td>
                        Medical Allowance
                    </td>
                    <td class="value">
                        {{empty($payslip->medical_allowance)?"0":$payslip->medical_allowance}}
                    </td>
                    <td>
                        Food Deduction
                    </td>
                    <td class="value">
                        {{empty($payslip->food_deduction)?"0":$payslip->food_deduction}}
                    </td>
                </tr>
                <tr>
                    <td>
                        Food Allowance
                    </td>
                    <td class="value">
                        {{empty($payslip->food_allowance)?"0":$payslip->food_allowance}}
                    </td>
                    <td>
                        Medical Insurance
                    </td>
                    <td class="value">
                        {{empty($payslip->medical_insurance)?"0":$payslip->medical_insurance}}
                    </td>
                </tr>
                <tr>
                    <td>
                        Special Allowance
                    </td>
                    <td class="value">
                        {{empty($payslip->special_allowance)?"0":$payslip->special_allowance}}
                    </td>
                    <td>
                        T.D.S.
                    </td>
                    <td class="value">
                        {{empty($payslip->tds)?"0":$payslip->tds}}
                    </td>
                </tr>
                <tr>
                    <td>
                        Arrear Salary
                    </td>
                    <td class="value">
                        {{empty($payslip->arrear_salary)?"0":$payslip->arrear_salary}}
                    </td>
                    <td>
                        Loan Deduction
                    </td>
                    <td class="value">
                        {{empty($payslip->loan)?"0":$payslip->loan}}
                    </td>
                </tr>
                <tr>
                    <td>
                        Performance Bonus
                    </td>
                    <td class="value">
                        {{empty($payslip->performance_bonus)?"0":$payslip->performance_bonus}}
                    </td>
                    <td>
                        Other Deduction
                    </td>
                    <td class="value">
                        {{"0"}}
                    </td>
                </tr>
                <tr>
                    <td>
                        Annual Bonus
                    </td>
                    <td class="value">
                        {{empty($payslip->annual_bonus)?"0":$payslip->annual_bonus}}
                    </td>
                    <td>

                    </td>
                    <td>
                        
                    </td>
                </tr>
                <tr>
                    <td>
                        Qtr Variable Bonus
                    </td>
                    <td class="value">
                        {{empty($payslip->qtr_variable_bonus)?"0":$payslip->qtr_variable_bonus}}
                    </td>
                    <td>
                        
                    </td>
                    <td>
                        
                    </td>
                </tr>
                <tr>
                    <td>
                        Non-Cash Incentive
                    </td>
                    <td class="value">
                        {{empty($payslip->non_cash_incentive)?"0":$payslip->non_cash_incentive}}
                    </td>
                    <td>
                        
                    </td>
                    <td>
                        
                    </td>
                </tr>
                <tr>
                    <td>
                        Leave Encashment
                    </td>
                    <td class="value">
                        {{empty($payslip->advance_salary)?"0":$payslip->advance_salary}}
                    </td>
                    <td>
                        
                    </td>
                    <td>
                        
                    </td>
                </tr>
                <tr>
                    <td>
                        Gross Earnings
                    </td>
                    <td class="value">
                        {{empty($payslip->gross_earnings)?"0":$payslip->gross_earnings}}
                    </td>
                    <td>
                        Total Deductions
                    </td>
                    <td class="value">
                        {{empty($payslip->total_deduction_month)?"0":$payslip->total_deduction_month}}
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <b>Net Salary Payable</b>
                    </td>
                    <td class="value">
                        {{empty($payslip->net_salary_month)?"0":$payslip->amount_payable}}
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <b>Net Salary Paid via Online Transfer</b>
                    </td>
                    <td class="value">
                        {{empty($payslip->net_salary_month)?"0":($payslip->net_salary_month=='-')?"0":$payslip->amount_payable}}
                    </td>
                </tr>
            </tbody>
        </table>
        <br>

        <div class="footer">
            <p align="LEFT" style="margin-left:2em" >Authorised by,</p>
            <br>
            <p align="LEFT" style="margin-left:2em">Director</p>
            <br>
            <p align="center">SAHU SOFT INDIA PRIVATE LIMITED, Reg no. U72200BR2006PTC011902</p>
            <p align="center">Regd. Office: Amgola Road, Muzaffarpur, P.S: Kaji Mohammadpur, Bihar 842002 India</p>
            <p align="center">Corp. Office: No 18, 1st & 2nd Floor, 2nd Cross Road, N S Palya, BTM Layout Stage 2, Bengaluru – 76,</p>
            <p align="center">Karnataka, India</p>
            <p align="center">+91 80 409 74929 | info@geekyants.com | www.sahusoft.com</p>
        </div>
    </div>
</body>