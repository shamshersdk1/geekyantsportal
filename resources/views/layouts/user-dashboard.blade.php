@extends('layouts.plane')
@section('page_heading','User Dashboard')
<script>
    window.api_url = '{{ config('app.api_url') }}';
</script>
@section('body')
@php
    $userLoggedIn = Auth::check();
    $user = Auth::user();
@endphp
    <nav class="navbar navbar-default navbar-fixed-top admin-navbar" role="navigation" >
        <div class="container-fluid">
         <a class="navbar-brand" href="/admin">
                    <img src="/images/logo-dark.png" class="" style="width:140px;" alt="GeekyAnts Portal">
                </a>
            <ul class="nav navbar-nav navbar-right top-nav-menu" style="margin: 0px;">
                @if($userLoggedIn)
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{$user->name}}
                    <span class="caret"></span></a>
                    <!-- ================ drop down list ==================== -->
                    <ul class="dropdown-menu dropdown-menu-right">
                    @inject('userAccessService', 'App\Services\UserAccessService')
                    @if($userAccessService->grantAccess())
                        <li>
                            <a href="/admin/dashboard"><i class="fa fa-sign-in fa-lg admindashboard-icon"></i> Admin Dashboard</a>
                        </li>
                    @endif
                        <li>
                            <a href="/admin/logout"><i class="fa fa-sign-out fa-lg admindashboard-icon"></i> Log out</a>
                        </li>
                    </ul>
                </li>
                @endif
            </ul>
        </div>
    </nav>

    <div id="page-wrapper" class="admin-dashboard">
        <div class="container-fluid">
            @yield('main-content')
        </div>
    </div>

<style>
.nav li a {
    text-align : left;
}
</style>

@endsection