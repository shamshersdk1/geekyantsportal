@extends('layouts.plane')
@section('page_heading','Dashboard')
@section('body')
<script>
    window.api_url = '{{ config('app.api_url') }}';
</script>
@php

    $userLoggedIn = Auth::check();
    $user = Auth::user();
    $userRoles = [];
    if(!empty($user->roles) && count($user->roles) > 0) {
        foreach( $user->roles as $role ) {
            array_push($userRoles, $role->code);
        }
    }

    $superAdmin = ($user->role == "admin" && $user->isAdmin()) ? true : false;
@endphp
<div id="wrapper" class="admin-wrapper">
<nav class="navbar navbar-inverse navbar-fixed-top admin-navbar" role="navigation">
   <div class="container-fluid">
      <div class="custom-navigation">
         <div class="navbar-header">
            <button type="button" class="menu-icon">
               <span class="sr-only">Toggle navigation</span>
               <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand admin-dashboard-logo visible-xs" href="/admin">
               <img src="/images/logo-icon.png" alt="GeekyAnts Portal" style="width: 26px;">
            </a>
            <a class="navbar-brand admin-dashboard-logo hidden-xs" href="/admin">
               <img src="/images/logo-dark.png" alt="GeekyAnts Portal">
            </a>
             
         </div>
         <div class="styled-select ml-auto-xs">
            <select class="filterby " id="menu-select-filter">
               @if($superAdmin || in_array('superAdmin', $userRoles))
                  <option value="all">All Menus</option>
               @endif
               @if($superAdmin || in_array('user', $userRoles))
                  <option value="user">User</option>
               @endif
               @if($superAdmin || in_array('human-resources', $userRoles))
                  <option value="human-resource">Human Resourcs</option>
               @endif
               @if($superAdmin || in_array('team-lead', $userRoles))
                  <option value="team-lead">Team lead</option>
               @endif
               @if($superAdmin || in_array('reporting-manager', $userRoles))
                  <option value="reporting-manager">Reporting Manager</option>
               @endif
               @if($superAdmin || in_array('calendar', $userRoles))
                  <option value="calender">Calendar manager</option>
               @endif
               @if($superAdmin || in_array('account', $userRoles))
                  <option value="finance">Finance Manager</option>
               @endif
               @if($superAdmin || in_array('account', $userRoles))
                  <option value="account-manager">Account Manager</option>
               @endif
               @if($superAdmin || in_array('office-admin', $userRoles))
                  <option value="office-admin">Office Admin</option>
               @endif
               @if($superAdmin || in_array('system-admin', $userRoles))
                  <option value="system-admin">Management</option>
               @endif
               @if($superAdmin || in_array('system-admin', $userRoles))
                  <option value="system-admin">System Admin</option>
               @endif
               @if($superAdmin || in_array('event-manager', $userRoles))
                  <option value="event-manager">Event Manager</option>
               @endif
               @if($superAdmin || in_array('cms-manager', $userRoles))
                  <option value="cms-manager">CMS Manager</option>
               @endif
               @if($superAdmin || in_array('superAdmin', $userRoles))
                  <option value="super-admin">Super admin</option>
               @endif
            </select>
            <span class="fa fa-sort-desc"></span>
         </div>

      <!-- Top Menu Items -->
         <div class="ml-auto hidden-xs">
            <ul class="nav navbar-nav navbar-right top-nav-menu">
               @if($userLoggedIn)
               <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{$user->name}}
                  <span class="caret"></span></a>
                  <!-- ================ drop down list ==================== -->
                  <ul class="dropdown-menu dropdown-menu-right">
                     @if($superAdmin)
                     <li><a href="/admin/users/{{$user->id}}/edit"><i class="fa fa-cog admindashboard-icon"></i> Settings</a></li>
                     <li><a href="/admin/users/{{$user->id}}/profile/edit"><i class="fa fa-cog  admindashboard-icon"></i> Info Update </a></li>
                     @endif
                     <li>
                        <a href="/admin/logout"><i class="fa fa-sign-out  admindashboard-icon"></i> Log out</a>
                     </li>
                  </ul>
               </li>
               @endif
            </ul>
            @if($user->hasRole('admin'))
            <a href="/admin/super-admin-switch" class="btn hidden-xs @if($user->role == 'admin')btn-success" style="margin: 10px 0 10px 0">Switch back to normal user</a> @else btn-danger" style="margin: 10px 0 10px 0">Switch to Super Admin</a> @endif
            @endif
         </div>
      </div>
      <section class="mobile-sidebar " id="mobile-sidebar">
         <div class="form-group no-margin search-wrap">
            <span class="fa fa-search form-control-feedback d-flex align-items-center justify-content-center" style="display:flex !important"></span>
            <input type="text" id="search" class="form-control rounded-0" placeholder="Search">
          </div>
         <ul class="nav navbar-nav side-nav">

         @if($user->hasRole('admin'))
            <li class="visible-xs">
               <a href="/admin/super-admin-switch" class="btn @if($user->role == 'admin')btn-success">Switch back to normal user</a> @else btn-danger">Switch to Super Admin</a> @endif
            </li>
         @endif

         @if($userLoggedIn)
            @if($superAdmin || in_array('reporting-manager', $userRoles))
            <li class="{{(Request::path() == 'admin/new-dashboard' || Request::is('admin/new-dashboard/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/new-dashboard') }}">
                  Dashboard
               </a>
            </li>
            @endif
            
            <!--    User View   -->
            <li class="all user menu-heading info" id="user">
               User
               <span class="label label-pill pull-right">
                  <i class="fa fa-user"></i>
               </span>
            </li>
            @if($superAdmin || in_array('user', $userRoles))
            <li class="all user {{(Request::path() == 'user/dashboard' || Request::is('user/dashboard/*')) ? 'active' : ''}}">
               <a href="{{ url('user/dashboard') }}#user">My View</a>
            </li>
            <li class="all user {{(Request::path() == 'admin/user-tasks' || Request::is('admin/user-tasks/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/user-tasks') }}#user"> My Tasks</a>
            </li>
            <li class="all user {{(Request::path() == 'admin/new-timesheet' || Request::is('admin/new-timesheet/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/new-timesheet') }}#user">Log Timesheet</a>
            </li>
            <li class="all user {{(Request::path() == 'admin/new-timesheet/history' || Request::is('admin/new-timesheet/history/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/new-timesheet/history') }}#user">My Timesheets</a>
            </li>
            <li class="all user {{(Request::path() == 'admin/feedback/my-review' || Request::is('admin/feedback/my-review/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/feedback/my-review') }}#user">My Reviews</a>
            </li>
            <li class="all user {{(Request::path() == 'admin/loan-requests' || Request::is('admin/loan-requests/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/loan-requests') }}#user"> My Loans</a>
            </li>
            <li class="all user {{(Request::path() == 'admin/goal-user-view' || Request::is('admin/goal-user-view/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/goal-user-view') }}#user">My Goals</a>
            </li>
            <li class="all user {{(Request::path() == 'user/wifi-password' || Request::is('user/wifi-password/*')) ? 'active' : ''}}">
               <a href="{{ url('user/wifi-password') }}#user">Network Password</a>
            </li>
            <li class="all user {{(Request::path() == 'admin/shared-credential' || Request::is('admin/shared-credential/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/shared-credential') }}#user">Shared Credentials</a>
            </li>
             <li class="all user {{(Request::path() == 'admin/user-contacts' || Request::is('admin/user-contacts/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/user-contacts') }}#user">Contact List</a>
            </li>
            <li class="all user {{(Request::path() == 'admin/incident' || Request::is('admin/incident/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/incident') }}#user">Support Ticket</a>
            </li>
            <li class="all user {{(Request::path() == 'user/it-saving' || Request::is('user/it-saving/*')) ? 'active' : ''}}">
               <a href="{{ url('user/it-saving') }}">IT Savings</a>
            </li>
            
            <li class="all user {{(Request::path() == 'policies' || Request::is('policies/*')) ? 'active' : ''}}">
               <a href="{{ url('policies') }}#user">Policies</a>
            </li>
            @endif

            <!-- Human Resource -->
            @if($superAdmin || in_array('human-resources', $userRoles))
            <li class="human-resource all menu-heading success" id="hr">
               Human Resource
               <span class="label label-pill pull-right">
                  <i class="fa fa-id-card"></i>
               </span>
            </li>
            <li class="human-resource all {{(Request::path() == 'admin/seat-management' || Request::is('admin/seat-management/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/seat-management') }}#hr">Seat Management</a>
            </li>
            <li class="human-resource all">
               <a href="#"> Attendance Section <i class="fa fa-angle-left pull-right"></i></a>
               <ul id="attendance" class="sidebar-submenu second-lavel" style="display: none;">
                  <li class="{{(Request::path() == 'admin/attendance-overview' || Request::is('admin/attendance-overview/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/attendance-overview') }}">Attendance Overview</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/attendance-register' || Request::is('admin/attendance-register/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/attendance-register') }}">Attendance Register</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/attendance' || Request::is('admin/attendance/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/attendance') }}">Attendance</a>
                  </li>
               </ul>
            </li>
            <li class="human-resource all">
               <a href="javascript:;" data-toggle="collapse" data-target="#appraisal">Appraisal<i class="fa fa-angle-left pull-right"></i></a>
               <ul id="appraisal" class=" sidebar-submenu second-lavel" style="display:none">
                  <li class="{{(Request::path() == 'admin/appraisal' || Request::is('admin/appraisal/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/appraisal') }}">Appraisal</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/appraisal/appraisal-type' || Request::is('admin/appraisal/appraisal-type/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/appraisal/appraisal-type') }}">Appraisal Type</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/appraisal/appraisal-component-type' || Request::is('admin/appraisal/appraisal-component-type/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/appraisal/appraisal-component-type') }}">Appraisal Component Type</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/appraisal/appraisal-bonus-type' || Request::is('admin/appraisal/appraisal-bonus-type/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/appraisal/appraisal-bonus-type') }}">Appraisal Bonus Type</a>
                  </li>
               </ul>
            </li>
            <li class="human-resource all">
               <a href="#"> Leave Section <i class="fa fa-angle-left pull-right"></i></a>
               <ul id="leave" class="sidebar-submenu second-lavel" style="display: none;">
                  <li class="{{(Request::path() == 'admin/leave-section/mis-leave-report' || Request::is('admin/leave-section/mis-leave-report/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/leave-section/mis-leave-report') }}">MIS Leave</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/leave-section/leave-report' || Request::is('admin/leave-section/leave-report/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/leave-section/leave-report') }}">Leave Report</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/leave-section/leave-balance-manager' || Request::is('admin/leave-section/leave-balance-manager/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/leave-section/leave-balance-manager') }}">Leave Balance Manager</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/leave-section/confirmation' || Request::is('admin/leave-section/confirmation/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/leave-section/confirmation') }}">Leave Confirmations</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/leave-section/compoff' || Request::is('admin/leave-section/compoff/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/leave-section/compoff') }}">Comp Off</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/leave-section/overview' || Request::is('admin/leave-section/overview/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/leave-section/overview') }}">Leave Overview</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/leave-section/overview' || Request::is('admin/leave-section/overview/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/leave-section/overview') }}">User Leave Overview</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/leave-deduction-month' || Request::is('admin/leave-deduction-month/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/leave-deduction-month') }}">Leave Deduction</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/calender-event' || Request::is('admin/calender-event/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/calender-event') }}">Calendar Event</a>
                  </li>
               </ul>
            </li>
            <li class="human-resource all">
               <a href="#"> <i class="fa fa-angle-left pull-right"></i>Loans </a>
               @php $expanded = false;
               $array = ['admin/loan-requests', 'admin/loan-requests/*', 'admin/pending-loan-requests', 'admin/pending-loan-requests/*', 'admin/submitted-loan-requests', 'admin/submitted-loan-requests/*', 'admin/reviewed-loan-requests', 'admin/reviewed-loan-requests/*', 'admin/reconciled-loan-requests', 'admin/reconciled-loan-requests/*', 'admin/approved-loan-requests', 'admin/approved-loan-requests/*', 'admin/loan-applications', 'admin/loan-applications/*'];
               foreach($array as $path) {
               if(Request::is($path)) {
               $expanded = true;
               continue;
               }
               }
               @endphp
               <ul id="leave" class="sidebar-submenu second-lavel" style="display: none;">
                  <li class="{{(Request::path() == 'admin/loan-requests' || Request::is('admin/loan-requests/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/loan-requests') }}">
                     My Loans</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/submitted-loan-requests' || Request::is('admin/submitted-loan-requests/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/submitted-loan-requests') }}">
                     Submitted Loan Requests</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/reconciled-loan-requests' || Request::is('admin/reconciled-loan-requests/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/reconciled-loan-requests') }}">
                     Reconciled Loan Requests</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/loan-applications' || Request::is('admin/loan-applications/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/loan-applications') }}">
                     All Loan Applications</a>
                  </li>
               </ul>
            </li>
            <li class="human-resource all">
               <a href="#"> Finance <i class="fa fa-angle-left pull-right"></i></a>
               <ul id="leave" class="sidebar-submenu second-lavel" style="display: none;">
                  <li class="{{(Request::path() == 'admin/loans/dashboard' || Request::is('admin/loans/dashboard/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/loans/dashboard') }}">Loans Overview</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/upload-payslip' || Request::is('admin/upload-payslip/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/upload-payslip') }}">Payslips Overview</a>
                  </li>
               </ul>
            </li>
            <li class="human-resource all">
               <a href="#"> Payroll <i class="fa fa-angle-left pull-right"></i></a>
               <ul id="leave" class="sidebar-submenu second-lavel" style="display: none;">
                  <li class="{{(Request::path() == 'admin/payroll-month' || Request::is('admin/payroll-month/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/payroll-month') }}">Payroll Data</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/onsite-allowance' || Request::is('admin/onsite-allowance/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/onsite-allowance') }}">Onsite Allowance</a>
                  </li>
               </ul>
            </li>
            <li class="human-resource all">
               <a href="#"> Users <i class="fa fa-angle-left pull-right"></i></a>
               <ul id="leave" class="sidebar-submenu second-lavel" style="display: none;">
                  <li class="{{(Request::path() == 'admin/users' || Request::is('admin/users/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/users') }}">Users</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/designations' || Request::is('admin/designations/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/designations') }}">Designations</a>
                  </li>
               </ul>
            </li>
           <li class="human-resource all">
               <a href="#">Insurance<i class="fa fa-angle-left pull-right"></i></a>
               <ul id="insuranceID" class="sidebar-submenu second-lavel" style="display: none;">
                  <li class="{{(Request::path() == '/admin/insurance-policy' || Request::is('/admin/insurance-policy/*')) ? 'active' : ''}}">
                     <a href="/admin/insurance-policy">Insurance Policy</a>
                  </li>
                  <li class="{{(Request::path() == '/admin/insurance-policy-user-report' || Request::is('/admin/insurance-policy-user-report/*')) ? 'active' : ''}}">
                     <a href="/admin/insurance-policy-user-report">Insurance Policy User Status</a>
                  </li>
               </ul>
            </li>
           <li class="human-resource all">
               <a href="javascript:;"> Bonus <i class="fa fa-angle-left pull-right"></i></a>
               <ul id="bonus-hr" class="sidebar-submenu second-lavel" style="display: none;">
                  <li class="{{(Request::path() == 'admin/bonus-request' || Request::is('admin/bonus-request/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/bonus-request') }}">Bonus Requests </a>
                 </li>
                 <li class="{{(Request::path() == 'admin/bonus-due' || Request::is('admin/bonus-due/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/bonus-due') }}">Due Bonuses </a>
                 </li>
                 <li class="{{(Request::path() == 'admin/bonus-due-user' || Request::is('admin/bonus-due-user/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/bonus-due-user') }}">User Due Bonuses </a>
                 </li>
                 <li class="{{(Request::path() == 'admin/bonus-paid' || Request::is('admin/bonus-paid/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/bonus-paid') }}">Paid Bonuses </a>
                 </li>
                 <li class="{{(Request::path() == 'admin/bonus' || Request::is('admin/bonus/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/bonus') }}">All Bonuses </a>
                 </li>
                 <li class="{{(Request::path() == 'admin/appraisal-bonus' || Request::is('admin/appraisal-bonus/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/appraisal-bonus') }}">Appraisal Bonus</a>
                 </li>
               </ul>
            </li>
            <li class="human-resource all">
               <a href="#"> Requests <i class="fa fa-angle-left pull-right"></i>
               </a>
               <ul class="sidebar-submenu second-lavel" style="display: none;">
                  <li class="{{(Request::path() == 'admin/request-approval' || Request::is('admin/request-approval/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/request-approval') }}">All Requests</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/my-requests' || Request::is('admin/my-requests/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/my-requests') }}">My Requests</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/approval-requests' || Request::is('admin/approval-requests/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/approval-requests') }}">Approval Request</a>
                  </li>
               </ul>
            </li>
            <li class="human-resource all {{(Request::path() == 'admin/user-report' || Request::is('admin/user-report/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/user-report') }}#hr">Team Structure</a>
            </li>
            @endif

            <!-- Team Lead-->
            @if( $superAdmin || in_array('team-lead', $userRoles))
            <li class="all team-lead light-green menu-heading" id="tl">
               Team Lead
               <span class="label label-pill pull-right">
                  <i class="fa fa-group"></i>
               </span>
            </li>
            <li class="all team-lead">
               <a href="#">Feedback <i class="fa fa-angle-left pull-right"></i></a>
               <ul id="leave" class="sidebar-submenu second-lavel" style="display: none;">
                  <li class="{{(Request::path() == 'admin/feedback/question' || Request::is('admin/feedback/question/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/feedback/question') }}">Questions</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/feedback/review' || Request::is('admin/feedback/review/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/feedback/review') }}">Review</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/feedback/report' || Request::is('admin/feedback/report/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/feedback/report') }}">Reports</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/feedback/my-review' || Request::is('admin/feedback/my-review/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/feedback/my-review') }}">My Reviews</a>
                  </li>
               </ul>
            </li>
            <li class="all team-lead {{(Request::path() == 'admin/new-timesheet/review' || Request::is('admin/new-timesheet/review/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/new-timesheet/review') }}#tl">Review Timesheets</a>
            </li>
            @endif

            <!-- Reporting-manager -->
            @if( $superAdmin || in_array('reporting-manager', $userRoles))
            <li class="all reporting-manager warning menu-heading" id="rm">
               Reporting manager
               <span class="label label-pill pull-right">
                  <i class="fa fa-address-book-o"></i>
               </span>
            </li>
            <li class="all reporting-manager {{(Request::path() == 'admin/reporting-manager-leaves' || Request::is('admin/reporting-manager-leaves/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/reporting-manager-leaves') }}#rm">Leaves</a>
            </li>
            <!-- <li> <a href="{{ url('admin/bonus-request') }}" class="sub-link-li">Bonus Requests </a></li> -->
            <li class="all reporting-manager {{(Request::path() == 'admin/timesheet' || Request::is('admin/timesheet/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/timesheet') }}#rm">Timesheet</a>
            </li>
            <li class="all reporting-manager {{(Request::path() == 'admin/timesheet-review' || Request::is('admin/timesheet-review/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/timesheet-review') }}#rm">Approve Timesheets</a>
            </li>
            <li class="all reporting-manager {{(Request::path() == 'admin/seating-arrangement' || Request::is('admin/seating-arrangement/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/seating-arrangement') }}#rm">Seating Arrangement</a>
            </li>
            <li class="all reporting-manager">
               <a href="#">Loans <i class="fa fa-angle-left pull-right"></i></a>
               <ul id="leave" class="sidebar-submenu second-lavel" style="display: none;">
                  <li class="{{(Request::path() == 'admin/pending-loan-requests' || Request::is('admin/pending-loan-requests/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/pending-loan-requests') }}" class="sub-link-li">
                     Pending Loan Requests</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/loan-applications' || Request::is('admin/loan-applications/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/loan-applications') }}" class="sub-link-li">
                     All Loan Applications</a>
                  </li>
               </ul>
            </li>
            <li class="all reporting-manager">
               <a href="#">Goals <i class="fa fa-angle-left pull-right"></i></a>
               <ul id="leave" class="sidebar-submenu second-lavel" style="display: none;">
                  <li class="{{(Request::path() == 'admin/goals' || Request::is('admin/goals/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/goals') }}" class="sub-link-li">Goals</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/goal-assignment' || Request::is('admin/goal-assignment/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/goal-assignment') }}" class="sub-link-li">Goal Assignment</a>
                 </li>
                 <li class="{{(Request::path() == 'admin/goal-user-list' || Request::is('admin/goal-user-list/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/goal-user-list') }}" class="sub-link-li">User List</a>
                 </li>
               </ul>
            </li>
            <li class="all reporting-manager">
               <a href="#">
               Requests <i class="fa fa-angle-left pull-right"></i>
               </a>
               <ul class="sidebar-submenu second-lavel" style="display: none;">
                  <li class="{{(Request::path() == 'admin/request-approval' || Request::is('admin/request-approval/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/request-approval') }}">All Requests</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/my-requests' || Request::is('admin/my-requests/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/my-requests') }}">My Requests</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/approval-requests' || Request::is('admin/approval-requests/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/approval-requests') }}">Approval Request</a>
                  </li>
               </ul>
            </li>
            <li class="all reporting-manager">
               <a href="#">Feedback <i class="fa fa-angle-left pull-right"></i></a>
               <ul id="leave" class="sidebar-submenu second-lavel" style="display: none;">
                  <li class="{{(Request::path() == 'admin/feedback/question' || Request::is('admin/feedback/question/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/feedback/question') }}">Questions</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/feedback/review' || Request::is('admin/feedback/review/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/feedback/review') }}">Review</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/feedback/report' || Request::is('admin/feedback/report/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/feedback/report') }}">Reports</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/feedback/my-review' || Request::is('admin/feedback/my-review/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/feedback/my-review') }}">My Reviews</a>
                  </li>
               </ul>
            </li>
            <li class="all reporting-manager {{(Request::path() == 'admin/new-timesheet/lead-view' || Request::is('admin/new-timesheet/lead-view/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/new-timesheet/lead-view') }}#rm">Approve Timesheets</a>
            </li>
            <li class="all reporting-manager {{(Request::path() == 'admin/new-timesheet/review' || Request::is('admin/new-timesheet/review/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/new-timesheet/review') }}#rm">Review Timesheets</a>
            </li>
            <li class="all reporting-manager">
               <a href="#">VPF <i class="fa fa-angle-left pull-right"></i></a>
               <ul id="leave" class="sidebar-submenu second-lavel" style="display: none;">
                   <li class="{{(Request::path() == 'admin/vpf' || Request::is('admin/vpf/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/vpf') }}">VPF</a>
                  </li>
               </ul>
            </li>
            <li class="all reporting-manager">
               <a href="javascript:;"> Bonus <i class="fa fa-angle-left pull-right"></i></a>
               <ul id="bonus-rm" class="sidebar-submenu second-lavel" style="display: none;">
                  <li class="{{(Request::path() == 'admin/bonus-request' || Request::is('admin/bonus-request/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/bonus-request') }}">Bonus Requests </a>
                 </li>
                 <li class="{{(Request::path() == 'admin/bonus-due' || Request::is('admin/bonus-due/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/bonus-due') }}">Due Bonuses </a>
                 </li>
                 <li class="{{(Request::path() == 'admin/bonus' || Request::is('admin/bonus/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/bonus') }}">All Bonuses </a>
                 </li>
                 <li class="{{(Request::path() == 'admin/appraisal-bonus' || Request::is('admin/appraisal-bonus/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/appraisal-bonus') }}">Appraisal Bonus</a>
                 </li>
               </ul>
            </li>
            <li class="all reporting-manager {{(Request::path() == 'admin/user-report' || Request::is('admin/user-report/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/user-report') }}#rm">Team Structure</a>
            </li>
            @endif

            <!-- Calendar manager -->
            @if( $superAdmin || in_array('calendar', $userRoles))
            <li class="all calender info menu-heading" id="cm">
               Calendar manager
               <span class="label label-pill pull-right">
                  <i class="fa fa-calendar"></i>
               </span>
            </li>
            <li class="all calender {{(Request::path() == 'admin/calendar' || Request::is('admin/calendar/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/calendar') }}#cm">Calendar</a>
            </li>
            @endif

            <!-- Finance Manager  -->
            @if( $superAdmin || in_array('account', $userRoles))
            <li class="all finance menu-heading danger" id="fm">
               Finance Manager
               <span class="label label-pill pull-right">
                  <i class="fa fa-money"></i>
               </span>
            </li>
            <li class="all finance">
               <a href="#">
               Loans <i class="fa fa-angle-left pull-right"></i>
               </a>
               <ul class="sidebar-submenu second-lavel" style="display: none;">
                  <li class="{{(Request::path() == 'admin/loans/dashboard' || Request::is('admin/loans/dashboard/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/loans/dashboard') }}">Loans Overview</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/approved-loan-requests' || Request::is('admin/approved-loan-requests/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/approved-loan-requests') }}">
                     Approved Loan Requests
                     </a>
                  </li>
                  <li class="{{(Request::path() == 'admin/loan-applications' || Request::is('admin/loan-applications/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/loan-applications') }}">
                     All Loan Applications
                     </a>
                  </li>
                  <li class="{{(Request::path() == 'admin/loans-ledger' || Request::is('admin/loans-ledger/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/loans-ledger') }}">Loans Ledger</a>
                  </li>
               </ul>
            </li>
            <li class="all finance">
               <a href="#">
               Prepare Salary <i class="fa fa-angle-left pull-right"></i>
               </a>
               <ul class="sidebar-submenu second-lavel" style="display: none;">
                  @if($superAdmin)
                  <li class="{{(Request::path() == 'admin/prep-salary-component-type' || Request::is('admin/prep-salary-component-type/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/prep-salary-component-type') }}">Prepare Salary Component Type</a>
                  </li>
                  @endif
                  <li class="{{(Request::path() == 'admin/prep-salary' || Request::is('admin/prep-salary/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/prep-salary') }}">
                     Prepare Salary
                     </a>
                  </li>
               </ul>
            </li>
            <li class="all finance {{(Request::path() == 'admin/bank-transfer' || Request::is('admin/bank-transfer/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/bank-transfer') }}">Bank Transfer</a>
            </li>
            <li class="all finance {{(Request::path() == 'admin/bank-transfer-hold' || Request::is('admin/bank-transfer-hold/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/bank-transfer-hold') }}#fm">Bank Transfer Hold</a>
            </li>

            <li class="all finance {{(Request::path() == 'admin/upload-payslip' || Request::is('admin/upload-payslip/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/upload-payslip') }}#fm">Payslips</a>
            </li>
            <li class="all finance {{(Request::path() == 'admin/payroll' || Request::is('admin/payroll/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/payroll') }}#fm">Payroll Groups</a>
            </li>
            <li class="all finance {{(Request::path() == 'admin/bonuses' || Request::is('admin/bonuses/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/bonuses') }}#fm">Bonuses</a>
            </li>
            <li class="all finance {{(Request::path() == 'admin/create-payroll-csv' || Request::is('admin/create-payroll-csv/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/create-payroll-csv') }}#fm">Create Payroll CSV</a>
            </li>
            <li class="all finance {{(Request::path() == 'admin/bonus-ledger' || Request::is('admin/bonus-ledger/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/bonus-ledger') }}#fm">Bonus Ledger</a>
            </li>
            <li class="all finance {{(Request::path() == 'admin/transaction-summary' || Request::is('admin/transaction-summary/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/transaction-summary') }}#fm">Transaction Summary</a>
            </li>
            <li class="all finance {{(Request::path() == 'admin/salary-transfer' || Request::is('admin/salary-transfer/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/salary-transfer') }}#fm">Salary Transfer</a>
            </li>
            <li class="all finance {{(Request::path() == 'admin/transaction' || Request::is('admin/transaction/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/transaction') }}#fm">Transaction</a>
            </li>
            <li class="all finance {{(Request::path() == 'admin/payslip-data' || Request::is('admin/payslip-data/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/payslip-data') }}#fm">Payslip Data</a>
            </li>
            <li class="all finance {{(Request::path() == 'admin/onsite-allowance' || Request::is('admin/onsite-allowance/*')) ? 'active' : ''}}"> 
               <a href="{{ url('admin/onsite-allowance') }}#fm">Onsite Allowance</a>
            </li>
            <li class="all finance">
               <a href="#">
               Payroll <i class="fa fa-angle-left pull-right"></i>
               </a>
               <ul class="sidebar-submenu second-lavel" style="display: none;">
                  <li class="{{(Request::path() == 'admin/payroll-rule-month' || Request::is('admin/payroll-rule-month/*')) ? 'active' : ''}}"> 
                     <a href="{{ url('admin/payroll-rule-month') }}">Payroll Rule</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/payroll-month' || Request::is('admin/payroll-month/*')) ? 'active' : ''}}"> 
                     <a href="{{ url('admin/payroll-month') }}">Payroll Data</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/bonus' || Request::is('admin/bonus/*')) ? 'active' : ''}}"> 
                     <a href="{{ url('admin/bonus') }}">Bonus</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/lop' || Request::is('admin/lop/*')) ? 'active' : ''}}"> 
                     <a href="{{ url('admin/lop') }}">LOP</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/upload-payslip' || Request::is('admin/upload-payslip/*')) ? 'active' : ''}}"> 
                      <a href="{{ url('admin/upload-payslip') }}">Payslips</a>
                   </li>
                  <li class="{{(Request::path() == 'admin/payroll' || Request::is('admin/payroll/*')) ? 'active' : ''}}"> 
                     <a href="{{ url('admin/payroll') }}">Payroll Groups</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/bonuses' || Request::is('admin/bonuses/*')) ? 'active' : ''}}"> 
                     <a href="{{ url('admin/bonuses') }}">Bonuses</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/create-payroll-csv' || Request::is('admin/create-payroll-csv/*')) ? 'active' : ''}}"> 
                     <a href="{{ url('admin/create-payroll-csv') }}">Create Payroll CSV</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/bonus-ledger' || Request::is('admin/bonus-ledger/*')) ? 'active' : ''}}"> 
                     <a href="{{ url('admin/bonus-ledger') }}">Bonus Ledger</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/transaction' || Request::is('admin/transaction/*')) ? 'active' : ''}}"> 
                     <a href="{{ url('admin/transaction') }}">Transaction</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/payslip-data' || Request::is('admin/payslip-data/*')) ? 'active' : ''}}"> 
                     <a href="{{ url('admin/payslip-data') }}">Payslip Data</a>
                  </li>
                  <!-- <li> <a href="{{ url('admin/onsite-allowance') }}">Onsite Allowance</a></li> -->
               </ul>
            </li>
            <li class="all finance {{(Request::path() == 'admin/it-saving' || Request::is('admin/it-saving/*')) ? 'active' : ''}}"> 
               <a href="{{ url('admin/it-saving') }}#fm">IT Saving</a>
            </li>
            <li class="all finance {{(Request::path() == 'admin/it-saving/month' || Request::is('admin/it-saving/month/*')) ? 'active' : ''}}"> 
               <a href="{{ url('admin/it-saving/month') }}#fm">IT Saving Month</a>
            </li>
            <li class="all finance">
               <a href="#">
               Project & Resources <i class="fa fa-angle-left pull-right"></i>
               </a>
               <ul class="sidebar-submenu second-lavel" style="display: none;">
                  <li><a href="{{ url('admin/project') }}#fm">Projects</a></li>
                  <li><a href="{{ url('admin/project-resource-report') }}">Projects Resource Report</a></li>
                  <li><a href="{{ url('admin/resource-report') }}#fm">Resource Report</a></li>
                  <li><a href="{{ url('admin/resource-hours-report') }}#fm">Resource Hours Report</a></li>
                  <li> <a href="{{ url('admin/project-slack-report') }}#fm">Project Slack Report</a></li>
               </ul>
            </li>
            <li class="all finance">
               <a href="#">
               Requests <i class="fa fa-angle-left pull-right"></i>
               </a>
               <ul class="sidebar-submenu second-lavel" style="display: none;">
                  <li class="{{(Request::path() == 'admin/request-approval' || Request::is('admin/request-approval/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/request-approval') }}">All Requests</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/my-requests' || Request::is('admin/my-requests/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/my-requests') }}">My Requests</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/approval-requests' || Request::is('admin/approval-requests/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/approval-requests') }}">Approval Request</a>
                  </li>
               </ul>
            </li>
            <li class="all finance {{(Request::path() == 'admin/new-timesheet/review' || Request::is('admin/new-timesheet/review/*')) ? 'active' : ''}}"> 
               <a href="{{ url('admin/new-timesheet/review') }}#fm">Review Timesheets</a>
            </li>
            <li class="all finance">
               <a href="#">Deductions<i class="fa fa-angle-left pull-right"></i></a>
               <ul class="sidebar-submenu second-lavel" style="display: none;">
                  <li class="{{(Request::path() == 'admin/loan-deduction' || Request::is('admin/loan-deduction/*')) ? 'active' : ''}}"> 
                     <a href="{{ url('admin/loan-deduction') }}">Loan Deductions</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/vpf-deduction' || Request::is('admin/vpf-deduction/*')) ? 'active' : ''}}"> 
                     <a href="{{ url('admin/vpf-deduction') }}">VPF Deductions</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/fixed-bonus' || Request::is('admin/fixed-bonus/*')) ? 'active' : ''}}"> 
                     <a href="{{ url('admin/fixed-bonus') }}">Fixed Bonuses</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/variable-pay' || Request::is('admin/variable-pay/*')) ? 'active' : ''}}"> 
                     <a href="{{ url('admin/variable-pay') }}">Variable Pay</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/insurance-deduction' || Request::is('admin/insurance-deduction/*')) ? 'active' : ''}}"> 
                     <a href="{{ url('admin/insurance-deduction') }}">Insurance Deductions</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/food-deduction' || Request::is('admin/food-deduction/*')) ? 'active' : ''}}"> 
                     <a href="{{ url('admin/food-deduction') }}">Food Deductions</a>
                  </li>
               </ul>
            </li>
            <li class="all finance">
               <a href="#">Adhoc Payment<i class="fa fa-angle-left pull-right"></i></a>
               <ul class="sidebar-submenu second-lavel" style="display: none;">
                   <li class="{{(Request::path() == 'admin/adhoc-payment-component' || Request::is('admin/adhoc-payment-component/*')) ? 'active' : ''}}"> 
                     <a href="{{ url('admin/adhoc-payment-component') }}">Adhoc Payment Component</a>
                  </li>
                   <li class="{{(Request::path() == 'admin/adhoc-payments' || Request::is('admin/adhoc-payments/*')) ? 'active' : ''}}"> 
                     <a href="{{ url('admin/adhoc-payments') }}">Adhoc Payments</a>
                  </li>
               </ul>
            </li>
            @endif

            <!-- Account manager -->
            @if( $superAdmin || in_array('account', $userRoles))
            <li class="all account-manager warning menu-heading" id="am">
               Account manager
               <span class="label label-pill pull-right">
                  <i class="fa fa-lock"></i>
               </span>
            </li>
            <li class="all account-manager {{(Request::path() == 'admin/reporting-managers' || Request::is('admin/reporting-managers/*')) ? 'active' : ''}}"> 
               <a href="{{ url('admin/reporting-managers') }}#am">Reporting Managers</a>
            </li>
            <li class="all account-manager {{(Request::path() == 'admin/reference-number' || Request::is('admin/reference-number/*')) ? 'active' : ''}}"> 
               <a href="{{ url('admin/reference-number') }}#am">
               Reference Number</a>
            </li>
            <li class="all account-manager {{(Request::path() == 'admin/project' || Request::is('admin/project/*')) ? 'active' : ''}}"> 
               <a href="{{ url('admin/project') }}#am">Projects</a>
            </li>
            <li class="all account-manager {{(Request::path() == 'admin/project-resource-report' || Request::is('admin/project-resource-report/*')) ? 'active' : ''}}"> 
               <a href="{{ url('admin/project-resource-report') }}#am">Projects Resource Report</a>
            </li>
            <li class="all account-manager {{(Request::path() == 'admin/resource-report' || Request::is('admin/resource-report/*')) ? 'active' : ''}}"> 
               <a href="{{ url('admin/resource-report') }}#am">Resource Report</a>
            </li>
            <li class="all account-manager {{(Request::path() == 'admin/resource-hours-report' || Request::is('admin/resource-hours-report/*')) ? 'active' : ''}}"> 
               <a href="{{ url('admin/resource-hours-report') }}#am">Resource Hours Report</a>
            </li>
            <li class="all account-manager {{(Request::path() == 'admin/project-slack-report' || Request::is('admin/project-slack-report/*')) ? 'active' : ''}}"> 
               <a href="{{ url('admin/project-slack-report') }}#am">Project Slack Report</a>
            </li>
            @endif

            <!-- Office admin -->
            @if( $superAdmin || in_array('office-admin', $userRoles))
            <li class="all office-admin yellow menu-heading" id="oa">
               Office Admin
               <span class="label label-pill pull-right">
                  <i class="fa fa-building-o"></i>
               </span>
            </li>
            <li class="all office-admin {{(Request::path() == 'admin/reference-number' || Request::is('admin/reference-number/*')) ? 'active' : ''}}"> 
               <a href="{{ url('admin/reference-number') }}#om">
               Reference Number</a>
            </li>
            <li class="all office-admin">
               <a href="#">Asset Management<i class="fa fa-angle-left pull-right"></i></a>
               <ul id="asset-management" class="sidebar-submenu second-lavel" style="display: none;">
                  <li class="{{(Request::path() == 'admin/asset-management/asset-category' || Request::is('admin/asset-management/asset-category/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/asset-management/asset-category') }}">Category</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/asset-management/asset' || Request::is('admin/asset-management/asset/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/asset-management/asset') }}">Asset</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/asset-management/network-asset' || Request::is('admin/asset-management/network-asset/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/asset-management/network-asset') }}">Network Asset</a>
                  </li>
               </ul>
            </li>
            <li class="all office-admin {{(Request::path() == 'admin/vendor' || Request::is('admin/vendor/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/vendor') }}#om"> Vendor</a>
            </li>
            <li class="all office-admin {{(Request::path() == 'admin/incident' || Request::is('admin/incident/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/incident') }}#om">Support Ticket</a>
            </li>
            <li class="all office-admin {{(Request::path() == 'admin/purchase-orders' || Request::is('admin/purchase-orders/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/purchase-orders') }}#om">Purchase Orders</a>
            </li>
            <li class="all office-admin {{(Request::path() == 'admin/expense' || Request::is('admin/expense/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/expense') }}#om"> Expense</a>
            </li>
            @endif

            <!-- System Admin -->
            @if($superAdmin || in_array('system-admin', $userRoles) || in_array('management', $userRoles) )
            <li class="all system-admin purpal menu-heading">
               System Admin
               <span class="label label-pill pull-right">
                  <i class="fa fa-laptop"></i>
               </span>
            </li>
            <li class="all system-admin" id="sa">
               <a href="javascript:;" data-toggle="collapse" data-target="#ip-manager">Ip Manager<i class="fa fa-angle-left pull-right"></i></a>
               <ul id="ip-manager" class=" sidebar-submenu second-lavel" style="display:none">
                  <li class="{{(Request::path() == 'admin/ip-address-range' || Request::is('admin/ip-address-range/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/ip-address-range') }}">Ip Address Range</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/assign-ip-address' || Request::is('admin/assign-ip-address/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/assign-ip-address') }}">Assign Ip Address </a>
                  </li>
                  <li class="{{(Request::path() == 'admin/all-network-devices' || Request::is('admin/all-network-devices/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/all-network-devices') }}">All Network Devices</a>
                  </li>
                  <li class="{{(Request::path() == 'user/wifi-password' || Request::is('user/wifi-password/*')) ? 'active' : ''}}">
                     <a href="{{ url('user/wifi-password') }}">Set WiFi Password</a>
                  </li>
               </ul>
            </li>
            <li class="all system-admin">
               <a href="{{ url('admin/ssh-keys') }}#sa">
               SSH Keys</a>
            </li>
            <li class="all system-admin">
               <a href="javascript:;" data-toggle="collapse" data-target="#ip-manager">Asset Management<i class="fa fa-angle-left pull-right"></i></a>
               <ul id="ip-manager" class=" sidebar-submenu second-lavel" style="display:none">
                  <li class="{{(Request::path() == 'admin/asset-management/asset-category' || Request::is('admin/asset-management/asset-category/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/asset-management/asset-category') }}">Category</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/asset-management/asset' || Request::is('admin/asset-management/asset/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/asset-management/asset') }}">Asset</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/all-network-devices' || Request::is('admin/all-network-devices/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/all-network-devices') }}">All Network Devices</a>
                  </li>
               </ul>
            </li>
            <li class="all system-admin {{(Request::path() == 'admin/vendor' || Request::is('admin/vendor/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/vendor') }}#sa"> Vendor</a>
            </li>
            <li class="all system-admin {{(Request::path() == 'admin/shared-credential' || Request::is('admin/shared-credential/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/shared-credential') }}#sa">Credentials</a>
            </li>
            <li class="all system-admin">
               <a href="javascript:;" data-toggle="collapse" data-target="#server-management">Server Management<i class="fa fa-angle-left pull-right"></i></a>
               <ul id="server-management" class=" sidebar-submenu second-lavel" style="display:none">
                  <li class="{{(Request::path() == 'admin/server-management/server' || Request::is('admin/server-management/server/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/server-management/server') }}">Server</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/server-management/server-provider' || Request::is('admin/server-management/server-provider/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/server-management/server-provider') }}">Server Provider</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/server-management/server-type' || Request::is('admin/server-management/server-type/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/server-management/server-type') }}">Server Types</a>
                  </li>
               </ul>
            </li>
            @endif

            <!-- Event Manager -->
            @if ( $superAdmin || in_array('event-manager', $userRoles) )
            <li class="all event-manager green menu-heading" id="event-manager">
               Event Manager
               <span class="label label-pill pull-right">
                  <i class="fa fa-calendar-check-o"></i>
               </span>
            </li>
            <li class="all event-manager {{(Request::path() == 'admin/tech-events' || Request::is('admin/tech-events/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/tech-events') }}#event-manager">
               Tech Events</a>
            </li>
            @endif

            <!-- CMS Manager -->
            @if($superAdmin || in_array('cms-manager', $userRoles))
            <li class="cms-manager all pink menu-heading" id="cms">
               CMS Manager
               <span class="label label-pill pull-right">
                  <i class="fa fa-code"></i>
               </span>
            </li>
            <li class="cms-manager all {{(Request::path() == 'admin/cms-project' || Request::is('admin/cms-project/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/cms-project') }}#cms">Project</a>
            </li>
            <li class="cms-manager all {{(Request::path() == 'admin/cms-developers' || Request::is('admin/cms-developers/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/cms-developers') }}#cms">Developers</a>
            </li>
            <li class="cms-manager all {{(Request::path() == 'admin/tech-talk-videos' || Request::is('admin/tech-talk-videos/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/tech-talk-videos') }}#cms">Tech Talk Videos</a>
            </li>
            <li class="cms-manager all {{(Request::path() == 'admin/meetups' || Request::is('admin/meetups/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/meetups') }}#cms">Meetups</a>
            </li>
            @endif

            <!-- Super Admin -->
            @if($superAdmin)
            <li class="all super-admin orange menu-heading" id="super-admin">
               Super Admin
               <span class="label label-pill pull-right">
                  <i class="fa fa-user-circle"></i>
               </span>
            </li>
            <li class="all super-admin {{(Request::path() == 'admin/users' || Request::is('admin/users/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/users') }}#super-admin">Users</a>
            </li>
            <li class="all super-admin {{(Request::path() == 'admin/timesheet/all' || Request::is('admin/timesheet/all/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/timesheet/all') }}#super-admin">Timesheet</a>
            </li>
            <li class="all super-admin {{(Request::path() == 'admin/mis-review-report' || Request::is('admin/mis-review-report/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/mis-review-report') }}#super-admin">MIS Timesheets</a>
            </li>
            <!-- <li class="all super-admin">
               <a href="{{ url('admin/user-review-salary') }}" class=""><i class="fa fa-rupee fa-fw  fa-lg" aria-hidden="true"></i> Review Salary</a>
            </li> -->
            <li class="all super-admin {{(Request::path() == 'admin/review-report-project' || Request::is('admin/review-report-project/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/review-report-project') }}#super-admin">Project Timesheets</a>
            </li>
            <li class="all super-admin">
               <a href="javascript:;" data-toggle="collapse" data-target="#bonus-admin">Bonus<i class="fa fa-angle-left pull-right"></i></a>
               <ul id="bonus-admin" class="sidebar-submenu second-lavel" style="display:none">
                  <li class="{{(Request::path() == 'admin/bonus/additional-work-days' || Request::is('admin/bonus/additional-work-days/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/bonus/additional-work-days') }}">Additional Work Days </a>
                  </li>
                  <li class="{{(Request::path() == 'admin/bonus/on-site' || Request::is('admin/bonus/on-site/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/bonus/on-site') }}">On Site </a>
                  </li>
                  <li class="{{(Request::path() == 'admin/bonus/tech-talk' || Request::is('admin/bonus/tech-talk/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/bonus/tech-talk') }}">Tech-Talk </a>
                  </li>

                  <li class="{{(Request::path() == 'admin/bonus/referral' || Request::is('admin/bonus/referral/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/bonus/referral') }}">Referral </a>
                  </li>
               </ul>
            </li>
            <li class="all super-admin {{(Request::path() == 'admin/reviewed-loan-requests' || Request::is('admin/reviewed-loan-requests/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/reviewed-loan-requests') }}#super-admin">Reviewed Loan Requests</a>
            </li>
            <li class="all super-admin {{(Request::path() == 'admin/loan-applications' || Request::is('admin/loan-applications/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/loan-applications') }}#super-admin">All Loan Applications</a>
            </li>
            <li class="all super-admin {{(Request::path() == 'admin/designations' || Request::is('admin/designations/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/designations') }}#super-admin">Designations</a>
            </li>
            <li class="all super-admin {{(Request::path() == 'admin/technology' || Request::is('admin/technology/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/technology') }}#super-admin">Technology</a>
            </li>
            <li class="all super-admin {{(Request::path() == 'admin/skill' || Request::is('admin/skill/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/skill') }}#super-admin">Skills</a>
            </li>
            <li class="all super-admin {{(Request::path() == 'admin/company' || Request::is('admin/company/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/company') }}#super-admin">Company</a>
            </li>
            <li class="all super-admin {{(Request::path() == 'admin/payroll-rules-csv' || Request::is('admin/payroll-rules-csv/*')) ? 'active' : ''}}#super-admin">
               <a href="{{ url('admin/payroll-rules-csv') }}">Payroll Rules for CSV</a>
            </li>
            <li class="all super-admin {{(Request::path() == 'admin/department' || Request::is('admin/department/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/department') }}#super-admin">Support Department</a>
            </li>
            <li class="all super-admin {{(Request::path() == 'admin/expense-head' || Request::is('admin/expense-head/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/expense-head') }}#super-admin">Expense Head</a>
            </li>
            <li class="all super-admin {{(Request::path() == 'admin/payment-method' || Request::is('admin/payment-method/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/payment-method') }}#super-admin">Payment Method</a>
            </li>
            <li class="all super-admin {{(Request::path() == 'admin/goal-report' || Request::is('admin/goal-report/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/goal-report') }}#super-admin">Goal Reports</a>
            </li>
            <li class="all super-admin {{(Request::path() == 'admin/role' || Request::is('admin/role/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/role') }}#super-admin">Role</a>
            </li>
            <li class="all super-admin {{(Request::path() == 'admin/slack-user-map' || Request::is('admin/slack-user-map/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/slack-user-map') }}#super-admin">Slack User Mapping</a>
            </li>
            <li class="all super-admin {{(Request::path() == 'admin/slack-access-logs' || Request::is('admin/slack-access-logs/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/slack-access-logs') }}#super-admin">Slack Access Log</a>
            </li>
            <li class="all super-admin {{(Request::path() == 'admin/late-comers' || Request::is('admin/late-comers/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/late-comers') }}#super-admin">Late Comers</a>
            </li>
            <li class="all super-admin">
               <a href="javascript:;" data-toggle="collapse" data-target="#spam">Leads Email<i class="fa fa-angle-left pull-right"></i></a>
               <ul id="spam" class="sidebar-submenu second-lavel" style="display:none">
                  <li class="{{(Request::path() == 'admin/bad-leads-filter' || Request::is('admin/bad-leads-filter/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/bad-leads-filter') }}">
                     Bad Leads Filter</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/hire-email' || Request::is('admin/hire-email/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/hire-email') }}">
                     Email List</a>
                  </li>
                  <li class="{{(Request::path() == 'admin/hire-email-review' || Request::is('admin/hire-email-review/*')) ? 'active' : ''}}">
                     <a href="{{ url('admin/hire-email-review') }}">
                     Review</a>
                  </li>
               </ul>
            </li>
            <li class="all super-admin {{(Request::path() == 'admin/leads' || Request::is('admin/leads/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/leads') }}#super-admin">Leads</a>
            </li>
            <li class="all super-admin {{(Request::path() == 'admin/dashboard-events' || Request::is('admin/dashboard-events/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/dashboard-events') }}#super-admin">Dashboard Events </a>
            </li>
            <li class="all super-admin {{(Request::path() == 'admin/calendar' || Request::is('admin/calendar/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/calendar') }}#super-admin">Calendar</a>
            </li>
            <li class="all super-admin {{(Request::path() == 'admin/logs' || Request::is('admin/logs/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/logs') }}#super-admin">Access Logs</a>
            </li>
            <li class="all super-admin {{(Request::path() == 'admin/checklist' || Request::is('admin/checklist/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/checklist') }}#super-admin">Checklist</a>
            </li>
            @endif

            @if( $superAdmin || in_array('hr-associate', $userRoles))
              <li class="all hr-associate{{(Request::path() == 'admin/user-report' || Request::is('user/user-report/*')) ? 'active' : ''}}">
               <a href="{{ url('admin/user-report') }}#user">Team Structure</a>
            </li>
            @endif
            <li class="visible-xs">
               <a href="/admin/logout"><i class="fa fa-sign-out  admindashboard-icon"></i> Log out</a>
            </li>
            @endif
         </ul>
      </section>
   </div>
</nav>
<div id="page-wrapper" class="admin-dashboard">
   @if (session('superAdminMessage'))
   <div class="alert alert-success">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <span>{{ session('superAdminMessage') }}</span><br/>
   </div>
   @endif
   @yield('main-content')
</div>
<script>
   $.sidebarMenu = function (menu) {
    var animationSpeed = 300,
      subMenuSelector = '.sidebar-submenu';

    $(menu).on('click', 'li a', function (e) {
      var $this = $(this);
      var checkElement = $this.next();

      if (checkElement.is(subMenuSelector) && checkElement.is(':visible')) {
        checkElement.slideUp(animationSpeed, function () {
          checkElement.removeClass('menu-open');
        });
        checkElement.parent("li").removeClass("active");
      }

      //If the menu is not visible
      else if ((checkElement.is(subMenuSelector)) && (!checkElement.is(':visible'))) {
        //Get the parent menu
        var parent = $this.parents('ul').first();
        //Close all open menus within the parent
        var ul = parent.find('ul:visible').slideUp(animationSpeed);
        //Remove the menu-open class from the parent
        ul.removeClass('menu-open');
        //Get the parent li
        var parent_li = $this.parent("li");

        //Open the target menu and add the menu-open class
        checkElement.slideDown(animationSpeed, function () {
          //Add the class active to the parent li
          checkElement.addClass('menu-open');
          parent.find('li.active').removeClass('active');
          parent_li.addClass('active');
        });
      }
      //if this isn't a link, prevent the page from being redirected
      if (checkElement.is(subMenuSelector)) {
        e.preventDefault();
      }
    });
   }
   $.sidebarMenu($('.side-nav'))


   $(document).ready(function () {

    var current = location.pathname.split('/').pop();
    var string = location.pathname;
   var segments = string.split("/");
   var lastUrl = segments[segments.length-2];
    $('.side-nav li a').each(function () {
      var hrefCurrent = $(this).attr("href").split('/').pop()
      var $this = $(this);
      // if the current path is like this link, make it active
      // if($this.attr('href').indexOf(current) !== -1){
      if (current == hrefCurrent || lastUrl == hrefCurrent) {
        $this.addClass('active');
        if ($(this).hasClass('active')) {
          $(this).parent('li').addClass('second-level');
          $(this).parent().parent('ul').addClass('menu-open');
          $(this).parent().parent('ul').parent('li').addClass('active');

        }
      }

    })


    var string = location.pathname;
   var segments = string.split("/");
   var lastUrl = segments[segments.length-2];

    var current = location.pathname.split('/').pop();

    $('.second-lavel .second-level a').each(function () {
      var hrefCurrent = $(this).attr("href").split('/').pop()

      var $this = $(this);
      // if the current path is like this link, make it active
      // if($this.attr('href').indexOf(current) !== -1){
      if (current == hrefCurrent || lastUrl == hrefCurrent) {
        $(this).parent().parent().parent('ul').addClass('menu-open');
        $(this).parent().parent().parent('ul').parent('li').addClass('active');


      }

    })
   })

   var searchedLoginClaims = sessionStorage.getItem("product");
   if (searchedLoginClaims != undefined || searchedLoginClaims != null) {
    $("#menu-select-filter").first().find(":selected").removeAttr("selected");
    $("#menu-select-filter").find("option").each(function () {
      if ($(this).val() == searchedLoginClaims) {
        $(this).attr("selected", true);
        var current = this.value;
        // alert(current);
        if (current == 'all') {
          $('.side-nav').find('li.all').show();
        } else {
          $('.side-nav').find('li').hide();
          $('.side-nav').find('li.all.' + current).show();
          $('.side-nav').find('li.all.' + current).addClass('current');
            // $('.current').addClass('active');
            $('.current').children('.sidebar-submenu').addClass('menu-open');


        }

      }


    });
   }

   $('#menu-select-filter').change(function () {
    sessionStorage.setItem("product", $(" #menu-select-filter").first().val());
    location.reload();
   });

   $("#search").on("keyup", function () {
      if (this.value.length > 0) {   
        $("li").hide().filter(function () {
          return $(this).text().toLowerCase().indexOf($("#search").val().toLowerCase()) != -1;
        }).show(); 
      }  
      else { 
        $("li").show();
      }
   }); 

   $(".menu-icon").click(function () {
         $('body').toggleClass('hideMenu');
      });
      $(window).resize(function() {
          if ($(window).width() < 767) {
              $('body').addClass('hideMenu');
          }
      }).resize();

</script>
@endsection
