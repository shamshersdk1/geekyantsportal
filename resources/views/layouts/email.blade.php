<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
   xmlns:v="urn:schemas-microsoft-com:vml"
   xmlns:o="urn:schemas-microsoft-com:office:office">
   <html>
      <head>
         <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
         <meta http-equiv="X-UA-Compatible" content="IE=edge" />
         <meta name="viewport" content="width=device-width, initial-scale=1.0 " />
         <meta name="format-detection" content="telephone=no" />
         <title>Leave Request</title>
         <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
         <style>
            html, body {
            height: 100%;
            }
            body {
            margin: 0;
            padding: 0;
            font-family: arial;
            font-size: 13px;
            }
            .container {
            margin: 20px auto;
            width: 40%;
            border:1px solid #ddd;
            border-radius: 5px;
            }
            .header{
            background-color:#f4eee6;
            color: #333;
            text-align: center;
            font-size: 25px;
            padding: 20px 0px;
            border-radius: 5px 5px 0 0;
            border-bottom: 1px solid #ddd;
            }
            .signature{
            padding: 20px;
            }
            .content {
            display: inline-block;
            }
            .footer{
            background-color:#f4eee6;
            text-align: center;
            color: #333;
            padding: 20px 0px;
            font-size: 15px;
            }
            @media(max-width:768px){
            .container {
            width:98%;
            margin:20px auto;
            }
            }
            body {
            margin: 0 !important;
            padding: 0 !important;
            -webkit-text-size-adjust: 100% !important;
            -ms-text-size-adjust: 100% !important;
            -webkit-font-smoothing: antialiased !important;
            }
            img {
            border: 0 !important;
            outline: none !important;
            }
            p {
            margin: 0px !important;
            padding: 0px !important;
            }
            table {
            border-collapse: collapse;
            mso-table-lspace: 0px;
            mso-table-rspace: 0px;
            }
            td, a, span {
            border-collapse: collapse;
            mso-line-height-rule: exactly;
            }
            .ExternalClass * {
            line-height: 100%;
            }
            .digiby_red_link a {
            text-decoration: none;
            color: #f06060;
            }
            .digiby_white_link a {
            text-decoration: none;
            color: #ffffff;
            }
            .digiby_dark_grey_txt a {
            text-decoration: none;
            color: #333333;
            }
            .digiby_grey_txt a {
            text-decoration: none;
            color: #333;
            }
            .digiby_yellow_link a {
            text-decoration: none;
            color: #333;
            }
            .digiby_green_link a {
            text-decoration: none;
            color: #333;
            }
            .digiby_blue_link a {
            text-decoration: none;
            color: #333;
            }
            .digiby_golden_link a {
            text-decoration: none;
            color: #333;
            }
            .digiby_footer a {
            text-decoration: none;
            color: #333;
            }
            .digiby_br1 {
            display: none;
            }
            @media only screen and (min-width:480px) and (max-width:650px) {
            table[class=digiby_main_table] {
            width: 100% !important;
            }
            table[class=digiby_wrapper] {
            width: 100% !important;
            }
            td[class=digiby_hide], br[class=digiby_hide] {
            display: none !important;
            }
            td[class=digiby_align_center] {
            text-align: center !important;
            }
            td[class=digiby_pad_top] {
            padding-top: 20px !important;
            }
            td[class=digiby_side_space] {
            padding-left: 20px !important;
            padding-right: 20px !important;
            }
            td[class=digiby_bg_center] {
            background-position: center !important;
            }
            img[class=digiby_full_width] {
            width: 100% !important;
            height: auto !important;
            max-width: 100% !important;
            }
            td[class=digiby_pad_btm] {
            padding-bottom: 28px !important;
            }
            }
            @media only screen and (max-width:480px) {
            table[class=digiby_main_table] {
            width: 100% !important;
            }
            table[class=digiby_wrapper] {
            width: 100% !important;
            }
            td[class=digiby_hide], br[class=digiby_hide] {
            display: none !important;
            }
            td[class=digiby_align_center] {
            text-align: center !important;
            }
            td[class=digiby_pad_top] {
            padding-top: 20px !important;
            }
            td[class=digiby_side_space] {
            padding-left: 20px !important;
            padding-right: 20px !important;
            }
            td[class=digiby_bg_center] {
            background-position: center !important;
            }
            td[class=digiby_height_80] {
            height: 200px !important;
            }
            img[class=digiby_full_width] {
            width: 100% !important;
            height: auto !important;
            max-width: 100% !important;
            }
            td[class=digiby_pad_btm] {
            padding-bottom: 28px !important;
            }
            td[class=digiby_width_178] {
            width: 178px !important;
            }
            }
         </style>
         @yield('css')
      </head>
      <!-- <body>
         <tr class="container">
             <div class="header">
                 <div>
                     <img class="pull-left" src="http://geekyants.com/images/logo-icon.png" style="width:40px; margin-top: 10px" />
                 </div>
                 GeekyAnts
             </div> -->
      <body style="margin:0px; padding:0px; background-color:#fff;">
         <!--Full width table start-->
         <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="background-color:#fff;">
         <tr>
            <td align="center" valign="top">
               <table align="center" class="digiby_main_table" width="450"  cellspacing="0" cellpadding="0" style="table-layout:fixed; border-radius:4px; border:1px solid #dddddd" bgcolor="#ffffff">
                  <tr>
                     <td class="digiby_hide" style="line-height:1px;min-width:650px;" bgcolor="#ffffff"><img src="http://www.emailonacid.com/images/emails/emailmonks1/spacer.gif" height="1"  width="650" style="max-height:1px; min-height:1px; display:block; width:650px; min-width:650px;" border="0" /></td>
                  </tr>
                  <!-- ===== //PRE-HEADER SECTION ===== --> 
                  <!-- ===== HEADER SECTION ===== -->
                  <tr>
                     <td bgcolor="#f4eee6" style="border-bottom:1px solid #dddddd">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                           <tr>
                              <td height="10">&nbsp;</td>
                           </tr>
                           <tr>
                              <td>
                                 <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                       <td width="30" class="digiby_hide">&nbsp;</td>
                                       <td class="digiby_side_space">
                                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                             <tr>
                                                <td width="100%" valign="top">
                                                   <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                      <tr>
                                                         <td height="8" style="line-height:1px;font-size:1px;">&nbsp;</td>
                                                      </tr>
                                                      <tr>
                                                         <td align="center" style="font-size:22px; font-weight:bold" > <img  src="http://geekyants.com/images/logo-icon.png" style="width:40px;font-size:22px; font-weight:bold" />
                                                            <br/>
                                                            GeekyAnts
                                                         </td>
                                                      </tr>
                                                   </table>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                       <td width="30" class="digiby_hide">&nbsp;</td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           <tr>
                              <td height="10">&nbsp;</td>
                           </tr>
                        </table>
                     </td>
                  </tr>
                  @yield('content')
                  <tr>
                     <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                           <tr>
                              <td width="30" class="digiby_hide">&nbsp;</td>
                              <td valign="top" class="digiby_side_space">
                                 <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                       <td class="digiby_grey_txt" style="font-family:Arial, sans-serif;font-size:13px;text-align:left;color:#333;line-height:20px;">
                                          Thank you.    <br/><br/>                   
                                          Best,            <br/>
                                          Team GeekyAnts    <br/>  
                                          (GeekyAnts.com) <br/><br/> 
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                              <td width="30" class="digiby_hide">&nbsp;</td>
                           </tr>
                        </table>
                     </td>
                  </tr>
                  <tr>
                     <td height="36"></td>
                  </tr>
                  <!-- <tr>
                     <td class="digiby_grey_txt" style="font-family:Arial, sans-serif;font-size:13px;text-align:left;color:#333;line-height:20px;">    
                        Thank you.                         <br />
                        Best,            <br/>
                        Team GeekyAnts   <br/>   
                        (GeekyAnts.com)<br/>          
                     </td>
                     </tr> -->
                  <!-- <div class="signature">
                     <p>Thank you.</p>
                     <p>Best,</p>
                     <p>Team GeekyAnts</p>
                     <p>(GeekyAnts.com)</p>
                     </div> -->
                  <!-- </div> -->
                  <!-- <div class="footer">geekyants.com</div>
                     </div> -->
                  @yield('js')
                  <tr>
                     <td bgcolor="#f4eee6">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                           <tr>
                              <td height="10">&nbsp;</td>
                           </tr>
                           <tr>
                              <td>
                                 <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                       <td width="30" class="digiby_hide">&nbsp;</td>
                                       <td class="digiby_side_space">
                                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                             <tr>
                                                <td width="100%" valign="top">
                                                   <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                      <tr>
                                                         <td height="8" style="line-height:1px;font-size:1px;">&nbsp;</td>
                                                      </tr>
                                                      <tr>
                                                         <td class="digiby_dark_grey_txt" style="font-family:Arial, sans-serif;font-size:14px;color:#1155cc;line-height:18px;text-align:center;">
                                                            <a href="https://geekyants.com/" target="_blank" style="text-decoration:underline;color:#1155cc; ">geekyants.com
                                                            </a> 
                                                         </td>
                                                      </tr>
                                                   </table>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                       <td width="30" class="digiby_hide">&nbsp;</td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           <tr>
                              <td height="10">&nbsp;</td>
                           </tr>
                        </table>
                     </td>
                  </tr>
                  
               </table>
      </body>