<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>

	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="@yield('description', 'GeekyAnts')">
    <meta name="author" content="GeekyAnts">
    <meta name="keywords" content="@yield('keywords', 'GeekyAnts')">

    <meta property="og:title" content="@yield('page_heading', 'GeekyAnts')" />
    <meta property="og:description" content="@yield('description', 'GeekyAnts')" />
    <meta property="og:type" content="website "/>
    <meta property="og:locale" content="en_US" />
    <meta property="og:url" content="//www.geekyants.com"/>
    <meta property="og:image" content="@yield('profileImg', '//geekyants.com/images/logo-icon-lg.png')" />

    <meta itemprop="name" content="@yield('name', 'GeekyAnts')" />
    <meta itemprop="description" content="@yield('description', 'GeekyAnts')" />
    <meta itemprop="image" content="@yield('profileImg', '//geekyants.com/images/logo-icon-lg.png')" />
    <meta itemprop="publisher" content="GeekyAnts" />
    <meta itemprop="url" content="//www.geekyants.com" />
    <meta itemprop="editor" content="GeekyAnts" />
    <meta itemprop="headline" content="@yield('headline', 'GeekyAnts')" />
    <meta itemprop="inLanguage" content="English" />
    <meta itemprop="articleSection" content="@yield('articleSection', 'GeekyAnts')" />
    <meta itemprop="sourceOrganization" content="GeekyAnts" />
    <meta itemprop="keywords" content="@yield('keywords', 'GeekyAnts')" />

    <meta name="DC.title" content="@yield('page_heading', 'GeekyAnts')" />
    <meta name="dc.source" CONTENT="//www.GeekyAnts.com">
    <meta name="dc.keywords" CONTENT="@yield('keywords', 'GeekyAnts')">
    <meta name="dc.subject" CONTENT="@yield('subject', 'GeekyAnts')">
    <meta name="dc.description" CONTENT="@yield('description', 'GeekyAnts')">
    <meta name="DC.Type" content="Web Application Development" >


	<title>GeekyAnts - @yield('page_heading')</title>

	<link rel="stylesheet" href="{{ elixir("assets/stylesheets/styles.css") }}" />
    <link rel="stylesheet" href="{{ elixir("css/app.css") }}" />
    <link rel="stylesheet" href="{{ asset("assets/dist-2/summernote.css") }}" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />

    <link rel="stylesheet" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
    <script src="{{ elixir("assets/scripts/vendor.js") }}" type="text/javascript"></script>
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="60">

    @yield('body')

<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
  
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script> -->

<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.js"></script>
    
    <script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.1/js/dataTables.fixedColumns.min.js"></script> -->

    <script src="{{ elixir("assets/scripts/custom.js") }}" type="text/javascript"></script>
    <script src="{{ asset("assets/dist-2/summernote.css") }}"></script>
    <script src="{{ asset("assets/dist-2/summernote.min.js") }}" type="text/javascript"></script>
    <script src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js" type="text/javascript"></script>
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script> -->
    <!-- <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.2.0/js/bootstrap.min.js" type="text/javascript"></script> -->

    @yield('js')
    @include('layouts.analytics')
    <script>

      $('.multi-item-carousel').carousel({
                interval: 5000
            });
      
            $('.carousel[data-type="multi"] .item').each(function() {
                var next = $(this).next();
                if (!next.length) {
                    next = $(this).siblings(':first');
                }
                next.children(':first-child').clone().appendTo($(this));

                for (var i = 0; i < 1; i++) {
                    next = next.next();
                    if (!next.length) {
                        next = $(this).siblings(':first');
                    }
                    next.children(':first-child').clone().appendTo($(this));
                }
            });
        $('.grid').masonry({
          // options
          itemSelector: '.grid-item',
          columnWidth: 200
        });

    </script>
    <script>
        if (window.location.pathname.match('/mobile') && window.location.pathname.match('/mobile').length > 0) {
            document.getElementsByTagName('body')[0].setAttribute('style', 'height: 100%');
            document.getElementsByTagName('html')[0].setAttribute('style', 'height: 100%');
        } else {
            document.getElementsByTagName('body')[0].removeAttribute('style');
            document.getElementsByTagName('html')[0].removeAttribute('style');
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function(){    
            $("#size").remove();
        });
    </script>    

</body>


</html>