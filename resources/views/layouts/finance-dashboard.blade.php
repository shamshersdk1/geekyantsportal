@extends('layouts.plane')
@section('page_heading','Dashboard')
@section('body')
<script>
    window.api_url = '{{ config('app.api_url') }}';
</script>
@php

    $userLoggedIn = Auth::check();
    $user = Auth::user();
    $userRoles = [];
    if(!empty($user->roles) && count($user->roles) > 0) {
        foreach( $user->roles as $role ) {
            array_push($userRoles, $role->code);
        }
    }

    $superAdmin = ($user->role == "admin" && $user->isAdmin()) ? true : false;
@endphp
<div id="wrapper" class="admin-wrapper">
    <nav class="navbar navbar-inverse navbar-fixed-top admin-navbar" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand admin-dashboard-logo" href="/admin">
                    <img src="/images/logo-dark.png" class="" style="width:140px;" alt="GeekyAnts Portal">
                </a>
            </div>
            <!-- Top Menu Items -->
           <div style="display:flex; flex-direction:row-reverse">
                <ul class="nav navbar-nav navbar-right top-nav-menu" >
                    @if($userLoggedIn)
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{$user->name}}
                        <span class="caret"></span></a>
                        <!-- ================ drop down list ==================== -->
                        <ul class="dropdown-menu dropdown-menu-right">
                            @if($superAdmin)
                            <li><a href="/admin/users/{{$user->id}}/edit"><i class="fa fa-cog fa-lg fa-fw  admindashboard-icon"></i> Settings</a></li>
                            <li><a href="/admin/users/{{$user->id}}/profile/edit"><i class="fa fa-cog fa-lg admindashboard-icon"></i> Info Update </a></li>
                            @endif
                            <li>
                                <a href="/admin/logout"><i class="fa fa-sign-out fa-lg admindashboard-icon"></i> Log out</a>
                            </li>
                        </ul>
                    </li>
                    @endif
                </ul>
                @if($user->hasRole('admin'))
                    <a href="/admin/super-admin-switch" class="btn @if($user->role == 'admin')btn-success" style="margin: 10px 0 10px 0">Switch back to normal user</a> @else btn-danger" style="margin: 10px 0 10px 0">Switch to Super Admin</a> @endif
                @endif
            </div>

            <div class="collapse navbar-collapse navbar-sidebar-admin">

                <ul class="nav navbar-nav side-nav">
                    <!-- ================ Dashboard ========================= -->
                    @if($userLoggedIn)
                        @if($superAdmin || in_array('reporting-manager', $userRoles))
                            <li class="{{(Request::path() == 'admin/new-dashboard'|| Request::is('admin/new-dashboard/*') || Request::is('admin/dashboard/*') || Request::is('admin/dashboard') ) ? 'active' : ''}}">
                                <a href="{{ url('admin/new-dashboard') }}" class=""><i class="fa fa-tachometer fa-fw fa-lg"></i>Dashboard</a>
                            </li>
                        @endif
                    <!--    My View   -->
                    @if($superAdmin || in_array('user', $userRoles))
                        <li class="{{Request::is('admin/*') ? '' : ''}}">
                            <a href="{{ url('user') }}" class=""><i class="fa fa-user fa-fw  fa-lg"></i>

                                My View</a>
                        </li>
                    @endif
                    @if(in_array('user', $userRoles) && !$superAdmin && !in_array('system-admin', $userRoles) )
                            <li class="{{(Request::path() == 'admin/shared-credential') ? 'active' : ''}}">
                                <a href="{{ url('admin/shared-credential') }}" class=""><i class="fa fa-fw fa-lock fa-lg"></i>
                                    Shared Credentials</a>
                            </li>
                        @endif
                    <!-- -->
                    <!-- Contact  -->
                    @if($superAdmin || in_array('user', $userRoles))
                        <li class="{{(Request::path() == 'admin/user-contacts' || Request::is('admin/user-contacts/*')) ? 'active' : ''}}">
                            <a href="{{ url('admin/user-contacts') }}" class=""><i class="fa fa-address-card fa-fw fa-lg"></i>
                            Contact List</a>
                        </li>
                    @endif
                    <!-- -->
                       <!-- Reporting Manager -->
                       @if($superAdmin )
                            <li class="{{(Request::path() == 'admin/reporting-managers' || Request::is('admin/reporting-managers/*')) ? 'active' : ''}}">
                                <a href="{{ url('admin/reporting-managers') }}" class=""><i class="fa fa-clipboard fa-fw fa-lg"></i>

                                    Reporting Managers</a>
                            </li>
                        @endif

                        <!-- Credentials -->
                       @if($superAdmin || in_array('system-admin', $userRoles) )
                            <li class="{{(Request::path() == 'admin/credential' || Request::is('admin/credential/*')) ? 'active' : ''}}">
                                <a href="{{ url('admin/credential') }}" class=""><i class="fa fa-lock fa-fw fa-lg"></i>
                                    Credentials</a>
                            </li>
                        @endif

                        @if($superAdmin || in_array('user', $userRoles))
                            <li class="{{Request::is('admin/user-tasks/*') || Request::is('admin/user-tasks') ? 'active' : ''}}">
                                <a href="{{ url('admin/user-tasks') }}" class="sub-link-li" style="padding-left:15px"><i class="fa fa-fw fa-list-ul fa-lg"></i> My Tasks</a>
                            </li>
                        @endif


                    <!-- -->
                          <!--    Seat Management   -->

                        @if($superAdmin || in_array('human-resources', $userRoles))
                            <li class="{{Request::path() == 'admin/seat-management' || Request::is('admin/seat-management/*') ? 'active' : ''}}">
                                <a href="{{ url('admin/seat-management') }}" class=""><i class="fa fa-indent fa-fw fa-large"></i>
                                Seat Management</a>
                            </li>
                        @endif
                    <!--    Attendance   -->
                        @if($superAdmin || in_array('human-resources', $userRoles))
                            <li class="{{Request::path() == 'admin/attendance' || Request::is('admin/attendance/*') ? 'active' : ''}}">
                                <a href="{{ url('admin/attendance') }}" class=""><i class="fa fa-address-book fa-fw fa-lg"></i>
                                    Attendance</a>
                            </li>
                        @endif

                    <!--   LEAVES     -->
                    @if($superAdmin || in_array('reporting-manager', $userRoles) || in_array('team-lead', $userRoles))
                        <li class="{{Request::is('admin/reporting-manager-leaves/*') || Request::is('admin/reporting-manager-leaves') ? 'active' : ''}}">
                            <a href="{{ url('admin/reporting-manager-leaves') }}" class=""><i class="fa fa-fw fa-calendar-times-o fa-lg"></i>Leaves</a>
                        </li>
                    @endif
                     <!-- ================ Bonus ========================= -->
                    @if($superAdmin || in_array('reporting-manager', $userRoles))
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#bonuses"><i class="fa fa-money fa-lg fa-fw"></i>Bonus <i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="bonuses" class="{{(Request::path() == 'admin/bonus'|| Request::is('admin/bonus/*') || Request::is('admin/bonus/additional-work-days') || Request::is('admin/bonus/additional-work-days/*')) ? 'expanded' : 'collapse'}}">
                                <li class="{{(Request::path() == 'admin/bonus/additional-work-days' || Request::is('admin/bonus/additional-work-days/*') ) ? 'active' : ''}}">
                                    <a href="{{ url('admin/bonus/additional-work-days') }}" class="sub-link-li">Additional Work Days </a>
                                </li>
                                <li class="{{(Request::path() == 'admin/bonus/on-site' || Request::is('admin/bonus/on-site/*') ) ? 'active' : ''}}">
                                    <a href="{{ url('admin/bonus/on-site') }}" class="sub-link-li">On Site </a>
                                </li>
                                <li class="{{(Request::path() == 'admin/bonus/performance' || Request::is('admin/bonus/performance/*') ) ? 'active' : ''}}">
                                    <a href="{{ url('admin/bonus/performance') }}" class="sub-link-li">Performance </a>
                                </li>
                                <li class="{{(Request::path() == 'admin/bonus/tech-talk' || Request::is('admin/bonus/tech-talk/*') ) ? 'active' : ''}}">
                                    <a href="{{ url('admin/bonus/tech-talk') }}" class="sub-link-li">Tech-Talk </a>
                                </li>
                                @if( $superAdmin || in_array('admin', $userRoles) )
                                <li class="{{(Request::path() == 'admin/bonus/referral' || Request::is('admin/bonus/referral/*') ) ? 'active' : ''}}">
                                    <a href="{{ url('admin/bonus/referral') }}" class="sub-link-li">Referral </a>
                                </li>
                                @endif
                                @if( $superAdmin || in_array('admin', $userRoles) )
                                <li class="{{(Request::path() == 'admin/bonus-request' || Request::is('admin/bonus-request/*') ) ? 'active' : ''}}">
                                    <a href="{{ url('admin/bonus-request') }}" class="sub-link-li">Bonus Requests </a>
                                </li>
                                @endif
                            </ul>
                        </li>
                    @endif
                    <!-- TIMESHEET -->
                    @if( !$superAdmin && in_array('reporting-manager', $userRoles) )
                    <li class="{{Request::path() == 'admin/timesheet' ? 'active' : ''}}">
                        <a href="{{ url('admin/timesheet') }}" class="sub-link-li">Timesheet</a>
                    </li>
                    @endif
                    @if($superAdmin || in_array('user', $userRoles) )
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#loans"><i class="fa fa-inr fa-lg fa-fw"></i>Loans<i class="fa fa-fw fa-caret-down"></i></a>
                            @php $expanded = false;
                                 $array = ['admin/loan-requests', 'admin/loan-requests/*', 'admin/pending-loan-requests', 'admin/pending-loan-requests/*', 'admin/submitted-loan-requests', 'admin/submitted-loan-requests/*', 'admin/reviewed-loan-requests', 'admin/reviewed-loan-requests/*', 'admin/reconciled-loan-requests', 'admin/reconciled-loan-requests/*', 'admin/approved-loan-requests', 'admin/approved-loan-requests/*', 'admin/loan-applications', 'admin/loan-applications/*'];
                                 foreach($array as $path) {
                                    if(Request::is($path)) {
                                        $expanded = true;
                                        continue;
                                    }
                                 }
                            @endphp
                            <!-- <ul id="loans" class="{{(Request::is('admin/loan-requests') || Request::is('admin/loan-requests/*') || Request::is('admin/new-loan-requests') || Request::is('admin/new-loan-requests/*') || Request::is('admin/loan-requests-history') || Request::is('admin/loan-requests-history/*')) ? 'expanded' : 'collapse'}}"> -->
                            <ul id="loans" class="{{($expanded) ? 'expanded' : 'collapse'}}">
                                <li class="{{(Request::is('admin/loan-requests') || Request::is('admin/loan-requests/*')) ? 'active' : ''}}">
                                    <a href="{{ url('admin/loan-requests') }}" class="sub-link-li">
                                        My Loans</a>
                                </li>
                                @if($superAdmin || in_array('reporting-manager', $userRoles))
                                    <li class="{{(Request::is('admin/pending-loan-requests') || Request::is('admin/pending-loan-requests/*')) ? 'active' : ''}}">
                                        <a href="{{ url('admin/pending-loan-requests') }}" class="sub-link-li">
                                            Pending Loan Requests</a>
                                    </li>
                                @endif
                                @if($superAdmin || in_array('human-resources', $userRoles))
                                    <li class="{{(Request::is('admin/submitted-loan-requests') || Request::is('admin/submitted-loan-requests/*')) ? 'active' : ''}}">
                                        <a href="{{ url('admin/submitted-loan-requests') }}" class="sub-link-li">
                                            Submitted Loan Requests</a>
                                    </li>
                                @endif
                                @if($superAdmin || in_array('management', $userRoles))
                                    <li class="{{(Request::is('admin/reviewed-loan-requests') || Request::is('admin/reviewed-loan-requests/*')) ? 'active' : ''}}">
                                        <a href="{{ url('admin/reviewed-loan-requests') }}" class="sub-link-li">
                                            Reviewed Loan Requests</a>
                                    </li>
                                @endif
                                @if($superAdmin || in_array('human-resources', $userRoles))
                                    <li class="{{(Request::is('admin/reconciled-loan-requests') || Request::is('admin/reconciled-loan-requests/*')) ? 'active' : ''}}">
                                        <a href="{{ url('admin/reconciled-loan-requests') }}" class="sub-link-li">
                                            Reconciled Loan Requests</a>
                                    </li>
                                @endif
                                @if($superAdmin || in_array('account', $userRoles))
                                    <li class="{{(Request::is('admin/approved-loan-requests') || Request::is('admin/approved-loan-requests/*')) ? 'active' : ''}}">
                                        <a href="{{ url('admin/approved-loan-requests') }}" class="sub-link-li">
                                            Approved Loan Requests</a>
                                    </li>
                                @endif
                                @if($superAdmin || in_array('human-resources', $userRoles) || in_array('reporting-manager', $userRoles) || in_array('account', $userRoles) || in_array('management', $userRoles))
                                    <li class="{{(Request::is('admin/loan-applications') || Request::is('admin/loan-applications/*')) ? 'active' : ''}}">
                                        <a href="{{ url('admin/loan-applications') }}" class="sub-link-li">
                                            All Loan Applications</a>
                                    </li>
                                @endif
                            </ul>
                        </li>
                    @endif
                    @if($superAdmin)
                        <!-- <li class="{{(Request::path() == 'admin/reporting-managers' || Request::is('admin/contact')) ? 'active' : ''}}">
                            <a href="{{ url('admin/contact') }}" class=""><i class="fa fa-address-card fa-lg"></i>
                            Contact </a>
                        </li> -->
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#spam"><i class="fa fa-envelope fa-fw fa-lg"></i>Leads Email<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="spam" class="{{(Request::path() == 'admin/bad-leads-filter' || Request::is('admin/bad-leads-filter/*') || Request::is('admin/hire-email') || Request::is('admin/hire-email/*') || Request::path() == 'admin/hire-email-review' || Request::is('admin/hire-email-review/*')  ) ? 'expanded' : 'collapse'}}">
                                <li class="{{(Request::path() == 'admin/bad-leads-filter' || Request::is('admin/bad-leads-filter/*')) ? 'active' : ''}}">
                                    <a href="{{ url('admin/bad-leads-filter') }}" class="sub-link-li">
                                    Bad Leads Filter</a>
                                </li>
                                <li class="{{(Request::path() == 'admin/hire-email' || Request::is('admin/hire-email/*')) ? 'active' : ''}}">
                                    <a href="{{ url('admin/hire-email') }}" class="sub-link-li">
                                    Email List</a>
                                </li>
                                <li class="{{(Request::path() == 'admin/hire-email-review' || Request::is('admin/hire-email-review/*')) ? 'active' : ''}}">
                                    <a href="{{ url('admin/hire-email-review') }}" class="sub-link-li">
                                    Review</a>
                                </li>
                            </ul>
                        </li>
                        @endif

                    <!-- ================ cms panel ========================= -->
                    @if($superAdmin || in_array('cms-manager', $userRoles))
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#cms"><i class="fa fa-th fa-fw fa-lg admindashboard-icon"></i>Cms <i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="cms" class="{{(Request::path() == 'admin/cms-technology'|| Request::is('admin/cms-technology/*') || Request::is('admin/cms-project') || Request::is('admin/cms-project/*') || Request::is('admin/cms-developers') || Request::is('admin/cms-developers/*')   ) ? 'expanded' : 'collapse'}}">
                            <li class="{{(Request::path() == 'admin/cms-technology' || Request::is('admin/cms-technology/*') ) ? 'active' : ''}}">
                                    <a href="{{ url('admin/cms-technology') }}" class="sub-link-li">Technology</a>
                                </li>
                                <li class="{{(Request::path() == 'admin/cms-project' || Request::is('admin/cms-project/*') ) ? 'active' : ''}}">
                                    <a href="{{ url('admin/cms-project') }}" class="sub-link-li">Project</a>
                                </li>
                                <li class="{{(Request::path() == 'admin/cms-developers' || Request::is('admin/cms-developers/*') ) ? 'active' : ''}}">
                                    <a href="{{ url('admin/cms-developers') }}" class="sub-link-li">Developers</a>
                                </li>
                            </ul>
                        </li>
                    @endif
                     <!-- ================================ Lead ======================== -->
                        <!--@if($superAdmin || in_array('lead', $userRoles))
                            <li>
                                <a href="javascript:;" data-toggle="collapse" data-target="#lead"><i class="fa fa-user fa-lg admindashboard-icon"></i>Leads <i class="fa fa-fw fa-caret-down"></i></a>
                                <ul id="lead" class="{{in_array(Request::path(), ['admin/lead', 'admin/lead/create']) ? 'expanded' : 'collapse'}}">
                                    <li class="{{Request::path() == 'admin/lead' ? 'active' : ''}}">
                                        <a href="{{ url('admin/lead') }}" class="sub-link-li">Dashboard</a>
                                    </li>
                                    <li class="{{Request::path() == 'admin/lead/create' ? 'active' : ''}}">
                                        <a href="{{ url('admin/lead/create') }}" class="sub-link-li">New Lead</a>
                                    </li>
                                </ul>
                            </li>
                        @endif-->

                        <!-- ================================ Lead ======================== -->
                        <!--@if($superAdmin || in_array('lead', $userRoles))
                            <li>
                                <a href="javascript:;" data-toggle="collapse" data-target="#lead"><i class="fa fa-user fa-lg admindashboard-icon"></i>Leads <i class="fa fa-fw fa-caret-down"></i></a>
                                <ul id="lead" class="{{in_array(Request::path(), ['admin/lead', 'admin/lead/create']) ? 'expanded' : 'collapse'}}">
                                    <li class="{{Request::path() == 'admin/lead' ? 'active' : ''}}">
                                        <a href="{{ url('admin/lead') }}" class="sub-link-li">Dashboard</a>
                                    </li>
                                    <li class="{{Request::path() == 'admin/lead/create' ? 'active' : ''}}">
                                        <a href="{{ url('admin/lead/create') }}" class="sub-link-li">New Lead</a>
                                    </li>
                                </ul>
                            </li>
                        @endif-->
                        <!-- ========================== time log ================================= -->
                        <!--@if($superAdmin || in_array('timelog', $userRoles))
                            <li>
                                <a href="javascript:;" data-toggle="collapse" data-target="#timelog"><i class="fa fa-clock-o fa-lg admindashboard-icon"></i>My Timelog <i class="fa fa-fw fa-caret-down"></i></a>
                                <ul id="timelog" class="{{in_array(Request::path(), ['admin/timelog/dashboard', 'admin/timelog/history']) ? 'expanded' : 'collapse'}}">
                                    <li class="{{Request::path() == 'admin/timelog/dashboard' ? 'active' : ''}}">
                                        <a href="{{ url('admin/timelog/dashboard') }}" class="sub-link-li">Dashboard</a>
                                    </li>
                                    <li class="{{Request::path() == 'admin/timelog/history' ? 'active' : ''}}">
                                        <a href="{{ url('admin/timelog/history') }}" class="sub-link-li">History</a>
                                    </li>
                                </ul>
                            </li>
                        @endif-->
                        <!-- ======================= Project ===================== -->
                        <!--@if($superAdmin || in_array('project', $userRoles))
                            <li>
                                <a href="#" data-toggle="collapse" data-target="#project"><i class="fa fa-folder-open fa-lg admindashboard-icon"></i>Project <i class="fa fa-fw fa-caret-down"></i></a>
                                <ul id="project" class="{{in_array(Request::path(), ['admin/project', 'admin/project/create', 'admin/project/sprint-dashboard']) ? 'expanded' : 'collapse'}}">
                                    <li class="{{Request::path() == 'admin/project' ? 'active' : ''}}">
                                        <a href="{{ url('admin/project') }}" class="sub-link-li">Dashboard</a>
                                    </li>
                                    <li class="{{Request::path() == 'admin/project/create' ? 'active' : ''}}">
                                        <a href="{{ url('admin/project/create') }}" class="sub-link-li">Create Project</a>
                                    </li>
                                    <li class="{{Request::path() == 'admin/project/sprint-dashboard' ? 'active' : ''}}">
                                        <a href="{{ url('admin/project/sprint-dashboard') }}" class="sub-link-li">Sprint Dashboard</a>
                                    </li>
                                </ul>
                            </li>
                        @endif-->
                        <!--@if($superAdmin || in_array('my-sprints', $userRoles))
                            @if(Request::path() === 'admin/my-sprints')
                                <li class="active">
                            @else
                                <li>
                            @endif
                                    <a href="/admin/my-sprints"><i class="fa fa-briefcase fa-lg admindashboard-icon"></i> My Sprints</a>
                                </li>
                        @endif-->
                        <!-- @if($superAdmin || in_array('employee-leave', $userRoles))
                            <li>
                                <a href="#" data-toggle="collapse" data-target="#employee-leave"><i class="fa fa-briefcase fa-lg admindashboard-icon"></i> My Leaves <i class="fa fa-fw fa-caret-down"></i></a>
                                <ul id="employee-leave" class="{{in_array(Request::path(), ['admin/employee-leave/apply', 'admin/employee-leave/history']) ? 'expanded' : 'collapse'}}">
                                    <li class="{{Request::path() == 'admin/employee-leave/apply' ? 'active' : ''}}">
                                        <a href="{{ url('admin/employee-leave/apply') }}" class="sub-link-li">Apply Leave</a>
                                    </li>
                                    <li class="{{Request::path() == 'admin/employee-leave/history' ? 'active' : ''}}">
                                        <a href="{{ url('admin/employee-leave/history') }}" class="sub-link-li">My Leave History</a>
                                    </li>
                                </ul>
                            </li>
                        @endif -->
                        <!--    My View   -->

                        <!-- @if($superAdmin || in_array('view', $userRoles))
                            <li class="{{Request::is('admin/*') ? '' : ''}}">
                                <a href="{{ url('user/dashboard') }}" class="sub-link-li">My View</a>
                            </li>
                        @endif -->

                        <!-- -->

                        <!--    Leave Section    -->

                        @if( $superAdmin || in_array('human-resources', $userRoles))
                            <li>
                                <a href="#" data-toggle="collapse" data-target="#leave"><i class="fa fa-calendar fa-fw fa-lg admindashboard-icon"></i> Leave Section <i class="fa fa-fw fa-caret-down"></i></a>
                                <ul id="leave" class="{{(Request::path() == 'admin/leave-section/confirmation' || Request::is('admin/leave-section/confirmation/*') || Request::is('admin/leave-section/compoff') || Request::is('admin/leave-section/compoff/*') || Request::is('admin/leave-section/overview') || Request::is('admin/leave-section/overview/*') || Request::is('admin/calender-event') ) ? 'expanded' : 'collapse'}}">
                                    <li class="{{(Request::path() == 'admin/leave-section/confirmation' || Request::is('admin/leave-section/confirmation/*') )  ? 'active' : ''}}">
                                        <a href="{{ url('admin/leave-section/confirmation') }}" class="sub-link-li">Leave Confirmations</a>
                                    </li>
                                    <li class="{{(Request::path() == 'admin/leave-section/compoff' || Request::is('admin/leave-section/compoff/*') )  ? 'active' : ''}}">
                                        <a href="{{ url('admin/leave-section/compoff') }}" class="sub-link-li">Comp Off</a>
                                    </li>
                                  <!--  <li class="{{Request::path() == 'admin/leave-section/report' ? 'active' : ''}}">
                                        <a href="{{ url('admin/leave-section/report') }}" class="sub-link-li">Leave Report</a>
                                    </li> -->
                                    <!-- <li class="{{Request::path() == 'admin/leave-section/mis-report' ? 'active' : ''}}">
                                        <a href="{{ url('admin/leave-section/mis-report') }}" class="sub-link-li">MIS Leave Report</a>
                                    </li> -->
                                    <li class="{{(Request::path() == 'admin/leave-section/overview' || Request::is('admin/leave-section/overview/*') ) ? 'active' : ''}}">
                                        <a href="{{ url('admin/leave-section/overview') }}" class="sub-link-li">Leave Overview</a>
                                    </li>
                                    <li class="{{(Request::path() == 'admin/leave-section/user-leave-report' || Request::is('admin/leave-section/user-leave-report/*') ) ? 'active' : ''}}">
                                        <a href="{{ url('admin/leave-section/overview') }}" class="sub-link-li">User Leave Overview</a>
                                    </li>
                                    <li class="{{(Request::path() == 'admin/calender-event') ? 'active' : ''}}">
                                        <a href="{{ url('admin/calender-event') }}" class="sub-link-li">Calendar Event</a>
                                    </li>
                                </ul>
                            </li>
                        @endif
                        <!--        -->


                        <!-- Finance Group  -->
                        @if( $superAdmin || in_array('account', $userRoles) || in_array('human-resources', $userRoles) || in_array('reporting-manager', $userRoles))
                            <li>
                                <a href="#" data-toggle="collapse" data-target="#finance"><i class="fa fa-money fa-lg admindashboard-icon"></i> Finance <i class="fa fa-fw fa-caret-down"></i></a>

                                <ul id="finance" class="{{in_array(Request::path(), ['admin/loans','admin/loans/dashboard' , 'admin/payroll', 'admin/bonuses','admin/upload-payslip','admin/bonus-ledger','admin/loans-ledger']) ? 'expanded' : 'collapse'}}">
                                  <!--  <li class="{{Request::path() == 'admin/loans/dashboard' ? 'active' : ''}}">
                                        <a href="{{ url('admin/loans/dashboard') }}" class="sub-link-li">Loans</a>
                                    </li> -->
                                    @if ( $superAdmin )
                                        <li class="{{Request::path() == 'admin/loans/dashboard' ? 'active' : ''}}">
                                            <a href="{{ url('admin/loans/dashboard') }}" class="sub-link-li">Loans</a>
                                        </li>
                                    @elseif( in_array('account', $userRoles) )
                                        <li class="{{Request::path() == 'admin/loans' ? 'active' : ''}}">
                                            <a href="{{ url('admin/loans') }}" class="sub-link-li">Loans</a>
                                        </li>
                                    @endif

                                    @if( $superAdmin || in_array('account', $userRoles))
                                        <li class="{{Request::path() == 'admin/upload-payslip' ? 'active' : ''}}">
                                            <a href="{{ url('admin/upload-payslip') }}" class="sub-link-li">Payslips</a>
                                        </li>
                                        <li class="{{Request::path() == 'admin/payroll' ? 'active' : ''}}">
                                            <a href="{{ url('admin/payroll') }}" class="sub-link-li">Payroll Groups</a>
                                        </li>
                                        <li class="{{Request::path() == 'admin/bonuses' ? 'active' : ''}}">
                                            <a href="{{ url('admin/bonuses') }}" class="sub-link-li">Bonuses</a>
                                        </li>
                                        <li class="{{Request::path() == 'admin/create-payroll-csv' ? 'active' : ''}}">
                                            <a href="{{ url('admin/create-payroll-csv') }}" class="sub-link-li">Create Payroll CSV</a>
                                        </li>
                                        <li class="{{Request::path() == 'admin/loans-ledger' ? 'active' : ''}}">
                                            <a href="{{ url('admin/loans-ledger') }}" class="sub-link-li">Loans Ledger</a>
                                        </li>
                                        <li class="{{Request::path() == 'admin/bonus-ledger' ? 'active' : ''}}">
                                            <a href="{{ url('admin/bonus-ledger') }}" class="sub-link-li">Bonus Ledger</a>
                                        </li>
                                    @endif
                                    <li class="{{Request::path() == 'admin/payslip-data' ? 'active' : ''}}">
                                        <a href="{{ url('admin/payslip-data') }}" class="sub-link-li">Payslip Data</a>
                                    </li>
                                </ul>
                            </li>
                        @endif
                        <!-- -->

                        <!-- Project & Resources  -->
                        @if( $superAdmin || in_array('account', $userRoles))
                            <li>
                                <a href="#" data-toggle="collapse" data-target="#projectresources"><i class="fa fa-fw fa-users fa-lg admindashboard-icon"></i> Project & Resources <i class="fa fa-fw fa-caret-down"></i></a>

                                <ul id="projectresources" class="{{Request::is('admin/project', 'admin/project/*', 'admin/resource-report', 'admin/resource-report/*', 'admin/resource-hours-report', 'admin/resource-hours-report/*', 'admin/project-resource-report', 'admin/project-resource-report/*', 'admin/project-slack-report', 'admin/project-slack-report/*') ? 'expanded' : 'collapse'}}">
                                    <li class="{{Request::is('admin/project', 'admin/project/*') ? 'active' : ''}}">
                                        <a href="{{ url('admin/project') }}" class="sub-link-li">Projects</a>
                                    </li>
                                    <li class="{{Request::is('admin/project-resource-report', 'admin/project-resource-report/*') ? 'active' : ''}}">
                                        <a href="{{ url('admin/project-resource-report') }}" class="sub-link-li">Projects Resource Report</a>
                                    </li>
                                    <li class="{{Request::is('admin/resource-report', 'admin/resource-report/*') ? 'active' : ''}}">
                                        <a href="{{ url('admin/resource-report') }}" class="sub-link-li">Resource Report</a>
                                    </li>
                                    <li class="{{Request::is('admin/resource-hours-report', 'admin/resource-hours-report/*') ? 'active' : ''}}">
                                        <a href="{{ url('admin/resource-hours-report') }}" class="sub-link-li">Resource Hours Report</a>
                                    </li>
                                    <li class="{{Request::is('admin/project-slack-report', 'admin/project-slack-report/*') ? 'active' : ''}}">
                                        <a href="{{ url('admin/project-slack-report') }}" class="sub-link-li">Project Slack Report</a>
                                    </li>
                                </ul>
                            </li>
                        @endif
                        <!-- -->

                        <!-- ================================ Admin =========================== -->
                        <!-- Admin  -->

                        @if( $superAdmin || in_array('human-resources', $userRoles))
                            <li>
                                <a href="#" data-toggle="collapse" data-target="#admin"><i class="fa fa-user-circle fa-fw fa-lg admindashboard-icon"></i> Admin <i class="fa fa-fw fa-caret-down"></i></a>
                                <ul id="admin" class="{{Request::is('admin/users', 'admin/users/*', 'admin/timesheet/all', 'admin/role','admin/technology', 'admin/company', 'admin/calendar', 'admin/dashboard-events','admin/logs','admin/skill','admin/designations', 'admin/designations/*') ? 'expanded' : 'collapse'}}">
                                    @if( $superAdmin || in_array('human-resources', $userRoles))
                                        <li class="{{Request::is('admin/users', 'admin/users/*') ? 'active' : ''}}">
                                            <a href="{{ url('admin/users') }}" class="sub-link-li">Users</a>
                                        </li>
                                    @endif
                                    @if( $superAdmin || in_array('calendar', $userRoles))
                                        <li class="{{Request::path() == 'admin/calendar' ? 'active' : ''}}">
                                            <a href="{{ url('admin/calendar') }}" class="sub-link-li">Calendar</a>
                                        </li>
                                    @endif
                                    @if( $superAdmin)
                                        <li class="{{Request::path() == 'admin/timesheet/all' ? 'active' : ''}}">
                                            <a href="{{ url('admin/timesheet/all') }}" class="sub-link-li">Timesheet</a>
                                        </li>
                                        <li class="{{(Request::path() == 'admin/role'|| Request::is('admin/role/*')) ? 'active' : ''}}">
                                            <a href="{{ url('admin/role') }}" class="sub-link-li">Role</a>
                                        </li>
                                        <li class="{{(Request::path() == 'admin/dashboard-events'|| Request::is('admin/dashboard-events/*')) ? 'active' : ''}}">
                                            <a href="{{ url('admin/dashboard-events') }}" class="sub-link-li">Dashboard Events </a>
                                        </li>

                                        <li class="{{(Request::path() == 'admin/technology') ? 'active' : ''}}">
                                            <a href="{{ url('admin/technology') }}" class="sub-link-li">Technology</a>
                                        </li>
                                        <li class="{{(Request::path() == 'admin/logs') ? 'active' : ''}}">
                                            <a href="{{ url('admin/logs') }}" class="sub-link-li">Access Logs</a>
                                        </li>
                                        <li class="{{(Request::path() == 'admin/skill') ? 'active' : ''}}">
                                            <a href="{{ url('admin/skill') }}" class="sub-link-li">View Skills</a>
                                        </li>
                                        <li class="{{(Request::path() == 'admin/company') ? 'active' : ''}}">
                                            <a href="{{ url('admin/company') }}" class="sub-link-li">Company</a>
                                        </li>
                                        <li class="{{(Request::path() == 'admin/payroll-rules-csv') ? 'active' : ''}}">
                                            <a href="{{ url('admin/payroll-rules-csv') }}" class="sub-link-li">Payroll Rules for CSV</a>
                                        </li>
                                        <li class="{{(Request::path() == 'admin/checklist') ? 'active' : ''}}">
                                            <a href="{{ url('admin/checklist') }}" class="sub-link-li">checklist</a>
                                        </li>
                                        <li class="{{(Request::path() == 'admin/leads') ? 'active' : ''}}">
                                            <a href="{{ url('admin/leads') }}" class="sub-link-li">Leads</a>
                                        </li>
                                        <li class="{{(Request::path() == 'admin/slack-user-map') ? 'active' : ''}}">
                                            <a href="{{ url('admin/slack-user-map') }}" class="sub-link-li">Slack User Mapping</a>
                                        </li>
                                        <li class="{{(Request::path() == 'admin/slack-access-logs') ? 'active' : ''}}">
                                            <a href="{{ url('admin/slack-access-logs') }}" class="sub-link-li">Slack Access Log</a>
                                        </li>
                                        <li class="{{(Request::path() == 'admin/lateComers') ? 'active' : ''}}">
                                            <a href="{{ url('admin/late-comers') }}" class="sub-link-li">Late Comers</a>
                                        </li>
                                        <li class="{{Request::is('admin/department', 'admin/department/*') ? 'active' : ''}}">
                                            <a href="{{ url('admin/department') }}" class="sub-link-li">Support Department</a>
                                        </li>
                                        <li class="{{Request::is('admin/expense-head', 'admin/expense-head/*') ? 'active' : ''}}">
                                            <a href="{{ url('admin/expense-head') }}" class="sub-link-li">Expense Head</a>
                                        </li>
                                        <li class="{{Request::is('admin/payment-method', 'admin/payment-method/*') ? 'active' : ''}}">
                                            <a href="{{ url('admin/payment-method') }}" class="sub-link-li">Payment Method</a>
                                        </li>
                                    @endif
                                    @if( $superAdmin || in_array('human-resources', $userRoles))
                                        <li class="{{Request::is('admin/designations', 'admin/designations/*') ? 'active' : ''}}">
                                            <a href="{{ url('admin/designations') }}" class="sub-link-li">Designations</a>
                                        </li>
                                    @endif
                                </ul>
                            </li>
                        @endif
                        <!-- -->

                        @if( $superAdmin || in_array('office-admin', $userRoles) || in_array('system-admin', $userRoles)  )
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#asset-management"><i class="fa fa-laptop fa-lg fa-fw admindashboard-icon"></i>Asset Management<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="asset-management" class="{{(Request::path() == 'admin/asset-management/asset-category' || Request::is('admin/asset-management/asset-category/*') || Request::is('admin/asset-management/asset') || Request::is('admin/asset-management/asset/*') ) ? 'expanded' : 'collapse'}}">
                                <li class="{{(Request::path() == 'admin/asset-management/asset-category' || Request::is('admin/asset-management/asset-category/*')) ? 'active' : ''}}">
                                    <a href="{{ url('admin/asset-management/asset-category') }}" class="sub-link-li">Category</a>
                                </li>
                                <li class="{{(Request::path() == 'admin/asset-management/asset' || Request::is('admin/asset-management/asset/*')) ? 'active' : ''}}">
                                    <a href="{{ url('admin/asset-management/asset') }}" class="sub-link-li">Asset</a>
                                </li>
                                <li class="{{(Request::path() == 'admin/asset-management/network-asset' || Request::is('admin/asset-management/network-asset/*')) ? 'active' : ''}}">
                                    <a href="{{ url('admin/asset-management/network-asset') }}" class="sub-link-li">Network Asset</a>
                                </li>
                            </ul>
                        </li>
                        <li class="{{(Request::path() == 'admin/vendor' || Request::is('admin/vendor/*')) ? 'active' : ''}}">
                            <a href="{{ url('admin/vendor') }}"><i class="fa fa-vcard-o fa-fw  fa-lg"></i> Vendor</a>
                        </li>
                       @endif
                       @if ( $superAdmin || in_array('event-manager', $userRoles) )
                            <li class="{{Request::path() == 'admin/tech-events' || Request::is('admin/tech-events/*') ? 'active' : ''}}">
                                <a href="{{ url('admin/tech-events') }}" class=""><i class="fa fa-address-book fa-fw fa-lg"></i>
                                    Tech Events</a>
                            </li>
                        @endif
                        </li>
                    @endif
                    @if($superAdmin || in_array('reference-generator', $userRoles))
                        <li class="{{Request::path() == 'admin/reference-number' || Request::is('admin/reference-number/*') ? 'active' : ''}}">
                            <a href="{{ url('admin/reference-number') }}" class=""><i class="fa fa-hashtag fa-fw  fa-lg"></i>
                                Reference Number</a>
                        </li>
                    @endif
                    @if($superAdmin || in_array('user', $userRoles) || in_array('reporting-manager', $userRoles))
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#my-team"><i class="fa fa-bullseye fa-lg fa-fw "></i>Goals<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="my-team" class="{{Request::is('admin/goal*') || Request::is('admin/user-tasks') ? 'expanded' : 'collapse'}}">
                                @if ( $superAdmin || in_array('reporting-manager', $userRoles) )
                                <li class="{{Request::is('admin/goals/*') || Request::is('admin/goals') ? 'active' : ''}}">
                                    <a href="{{ url('admin/goals') }}" class="sub-link-li">Goals</a>
                                </li>
                                <li class="{{Request::is('admin/goal-assignment/*') || Request::is('admin/goal-assignment') ? 'active' : ''}}">
                                    <a href="{{ url('admin/goal-assignment') }}" class="sub-link-li">Goal Assignment</a>
                                </li>
                                <li class="{{Request::is('admin/goal-user-list/*') || Request::is('admin/goal-user-list') ? 'active' : ''}}">
                                    <a href="{{ url('admin/goal-user-list') }}" class="sub-link-li">User List</a>
                                </li>
                                @endif
                                <li class="{{Request::is('admin/goal-user-view/*') || Request::is('admin/goal-user-view') ? 'active' : ''}}">
                                    <a href="{{ url('admin/goal-user-view') }}" class="sub-link-li">User View</a>
                                </li>
                                @if ($superAdmin)
                                <li class="{{Request::is('admin/goal-report/*') || Request::is('admin/goal-report') ? 'active' : ''}}">
                                    <a href="{{ url('admin/goal-report') }}" class="sub-link-li">Reports</a>
                                </li>
                                @endif
                            </ul>
                        </li>
                    @endif
                 <!--   @if ( $superAdmin || in_array('human-resource', $userRoles) || in_array('reporting-manager', $userRoles) || in_array('account', $userRoles) )
                        <li class="{{Request::path() == 'admin/request-approval' || Request::is('admin/request-approval/*') ? 'active' : ''}}">
                            <a href="{{ url('admin/request-approval') }}" class=""><i class="fa fa-bullhorn fa-fw fa-lg"></i>
                                Requests</a>
                        </li>
                    @endif -->
                    @if($superAdmin || in_array('human-resource', $userRoles) || in_array('reporting-manager', $userRoles) || in_array('account', $userRoles) )
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#requests"><i class="fa fa-bullhorn fa-lg fa-fw "></i>Requests<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="requests" class="{{Request::is('admin/request-approval') || Request::is('admin/request-approval/*') || Request::is('admin/my-requests*') || Request::is('admin/approval-requests*') ? 'expanded' : 'collapse'}}">
                                <li class="{{Request::is('admin/request-approval/*') || Request::is('admin/request-approval') ? 'active' : ''}}">
                                    <a href="{{ url('admin/request-approval') }}" class="sub-link-li">All Requests</a>
                                </li>
                                <li class="{{Request::is('admin/my-requests/*') || Request::is('admin/my-requests') ? 'active' : ''}}">
                                    <a href="{{ url('admin/my-requests') }}" class="sub-link-li">My Requests</a>
                                </li>
                                <li class="{{Request::is('admin/approval-requests/*') || Request::is('admin/approval-requests') ? 'active' : ''}}">
                                    <a href="{{ url('admin/approval-requests') }}" class="sub-link-li">Approval Request</a>
                                </li>
                            </ul>
                        </li>
                    @endif
                    @if($superAdmin || in_array('system-admin', $userRoles))
                       <li>
                           <a href="javascript:;" data-toggle="collapse" data-target="#ip-manager"><i class="fa fa-server fa-lg fa-fw "></i> Ip Manager<i class="fa fa-fw fa-caret-down"></i></a>
                           <ul id="ip-manager" class="{{Request::is('admin/ip-address-range') || Request::is('admin/ip-address-range/*') || Request::is('admin/assign-ip-address') || Request::is('admin/assign-ip-address/*')|| Request::is('admin/my-network-devices') || Request::is('admin/my-network-devices/*') ? 'expanded' : 'collapse'}}">                   
                                   <li class="{{Request::is('admin/ip-address-range/*') || Request::is('admin/ip-address-range') ? 'active' : ''}}">
                                       <a href="{{ url('admin/ip-address-range') }}" class="sub-link-li">Ip Address Range</a>
                                   </li>
                                   <li class="{{Request::is('admin/assign-ip-address/*') || Request::is('admin/assign-ip-address') ? 'active' : ''}}">
                                       <a href="{{ url('admin/assign-ip-address') }}" class="sub-link-li">Assign Ip Address </a>
                                   </li>
                                    <li class="{{Request::is('admin/all-network-devices/*') || Request::is('admin/all-network-devices') ? 'active' : ''}}">
                                        <a href="{{ url('admin/all-network-devices') }}" class="sub-link-li">All Network Devices</a>
                                    </li>
                           </ul>
                       </li>
                   @endif
                    @if($superAdmin || in_array('system-admin', $userRoles))
                    <li class="{{(Request::path() == 'admin/ssh-keys' || Request::is('admin/ssh-keys/*')) ? 'active' : ''}}">
                        <a href="{{ url('admin/ssh-keys') }}" class=""><i class="fa fa-key fa-fw fa-lg"></i>
                        SSH Keys</a>
                    </li>
                    @endif
                    <li>
                        <a href="{{ url('admin/new-timesheet') }}" class=""><i class="fa fa-clock-o fa-fw  fa-lg" aria-hidden="true"></i>Log Timesheet</a>
                    </li>
                    <li>
                        <a href="{{ url('admin/new-timesheet/history') }}" class=""><i class="fa fa-clock-o fa-fw  fa-lg" aria-hidden="true"></i>My Timesheets</a>
                    </li>
                    <li>
                        <a href="{{ url('admin/new-timesheet/lead-view') }}" class=""><i class="fa fa-clock-o fa-fw  fa-lg" aria-hidden="true"></i>Approve Timesheets</a>
                    </li>
                    @if($superAdmin || in_array('team-lead', $userRoles) || in_array('reporting-manager', $userRoles) || in_array('user', $userRoles))
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#feedback"><i class="fa fa-bullhorn fa-lg fa-fw "></i>Feedback<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="feedback" class="{{Request::is('admin/feedback/question') || Request::is('admin/feedback/review') || Request::is('admin/feedback/report') ? 'expanded' : 'collapse'}}">
                                @if($superAdmin || in_array('team-lead', $userRoles) || in_array('reporting-manager', $userRoles))
                                <li class="{{Request::is('admin/feedback/question')  ? 'active' : ''}}">
                                    <a href="{{ url('admin/feedback/question') }}" class="sub-link-li">Questions</a>
                                </li>
                                <li class="{{Request::is('admin/feedback/review/*') || Request::is('admin/feedback/review') ? 'active' : ''}}">
                                    <a href="{{ url('admin/feedback/review') }}" class="sub-link-li">Review</a>
                                </li>
                                <li class="{{Request::is('admin/feedback/report/*') || Request::is('admin/feedback/report') ? 'active' : ''}}">
                                    <a href="{{ url('admin/feedback/report') }}" class="sub-link-li">Reports</a>
                                </li>
                                @endif
                                <li class="{{Request::is('admin/feedback/my-review/*') || Request::is('admin/feedback/my-review') ? 'active' : ''}}">
                                    <a href="{{ url('admin/feedback/my-review') }}" class="sub-link-li">My Reviews</a>
                                </li>
                            </ul>
                        </li>
                    @endif
                    <li>
                        <a href="{{ url('admin/new-timesheet/review') }}" class=""><i class="fa fa-clock-o fa-fw  fa-lg" aria-hidden="true"></i>Review Timesheets</a>
                    </li>
                    <li>
                        <a href="{{ url('admin/incident') }}" class=""><i class="fa fa-wrench fa-fw fa-lg" aria-hidden="true"></i>Support Ticket</a>
                    </li>
                    @if($superAdmin || in_array('office-admin', $userRoles))
                    <li class="{{(Request::path() == 'admin/purchase-orders' || Request::is('admin/purchase-orders/*')) ? 'active' : ''}}">
                        <a href="{{ url('admin/purchase-orders') }}" class=""><i class="fa fa-shopping-cart fa-fw fa-lg"></i>
                        Purchase Orders</a>
                    </li>
                    <li class="{{(Request::path() == 'admin/expense' || Request::is('admin/expense/*')) ? 'active' : ''}}">
                        <a href="{{ url('admin/expense') }}" class=""><i class="fa fa-money fa-fw fa-lg"></i>
                        Expense</a>
                    </li>
                    @endif
                    @if( $superAdmin )
                    <li>
                        <a href="{{ url('admin/mis-review-report') }}" class=""><i class="fa fa-clock-o fa-fw  fa-lg" aria-hidden="true"></i>MIS Timesheets</a>
                    </li>
                    <li>
                        <a href="{{ url('admin/review-report-project') }}" class=""><i class="fa fa-clock-o fa-fw  fa-lg" aria-hidden="true"></i>Project Timesheets</a>
                    </li>
                    @endif
                    <li>
                        <a href="{{ url('admin/it-saving') }}" class=""><i class="fa fa-money fa-fw  fa-lg" aria-hidden="true"></i>IT Saving</a>
                    </li>
                    
                    
                </ul>
            </div>
        </div>
    </nav>

    <div id="page-wrapper" class="admin-dashboard">
        @if (session('superAdminMessage'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('superAdminMessage') }}</span><br/>
            </div>
        @endif

        @yield('main-content')
    </div>

@endsection
