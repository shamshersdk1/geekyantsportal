@extends('layouts.plane')
@section('body')
<div id="wrapper" class="admin-mobile-wrapper">
    <div id="page-wrapper" class="admin-mobile-dashboard">
        @yield('main-content')
    </div>
</div>
@endsection