@extends('layouts.plane')
@section('page_heading','Dashboard')



@section('body')
<div id="wrapper">
        <!-- ============== Navigation ==================-->
    <nav id="mainNav" class="navbar navbar-main navbar-default navbar-custom navbar-fixed-top front-page-navbar other-page">
        <div class="container">
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span><i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" id="geekybimg1" href="/"><img src="/images/logo-dark.png" class="img-responsive logo-img " alt=""></a>
                <a class="navbar-brand page-scroll hidden" id="geekywimg1" href="/"><img src="/images/logo-with-base-422X100.png" class="img-responsive logo-img " alt=""></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                        <a href="/open-source">Products</a>
                    </li>
                    <li>
                        <a href="/service">Services</a>
                    </li>
                    <li>
                        <a href="/technology">Technologies</a>
                    </li>
                    <li class="hidden-sm">
                        <a href="/react">React</a>
                    </li>
                    <li class="hidden-sm">
                        <a href="/angular">Angular</a>
                    </li>
                    <li>
                        <a href="http://mobile-development.geekyants.com/">App Development</a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            app themes&nbsp;<span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu" style="width: 230px;">
                            <li><a style="text-align:left;" target="_blank" href="https://geekyants.com/uber-clone-taxi-app-script/?utm_source=website&utm_medium=header&utm_campaign=geekyants"><i class="fa fa-btn fa-sign-out"></i> Uber clone Taxi App</a></li>
                            <li><a style="text-align:left;" target="_blank" href="https://geekyants.com/tinder-clone-dating-app-script/?utm_source=website&utm_medium=header&utm_campaign=geekyants"><i class="fa fa-btn fa-sign-out"></i> Tinder like Dating App</a></li>
                            <!-- <li><a href="https://strapmobile.com?utm_source=nativebase&utm_medium=header&utm_campaign=StrapMobile" target="_blank"><i class="fa fa-btn fa-sign-out"></i>premium</a></li> -->
                        </ul>
                    </li>
                    <li>
                        <a href="/contact">Contact</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>


    @yield('content')

    
    <!-- ======================= primary footer ============= -->
    @include('widgets.footer')
    

        
</div>
@stop
