@extends('layouts.plane')
@section('body')
<div class="container-fluid">
	<div class="login-wrapper">
		<div class="row">

			<div class="col-md-6 col-md-offset-3">
				<div class="logo-holder-panel">
					<a class="admin-login-logo" href="/"><img src="/images/logo-dark.png" class="img-responsive admin-logo-img center-block" alt=""></a>
				</div>
			</div>

			<div class="col-md-6 col-md-offset-3 panel-login-admin">
				<!-- <div class="logo-holder-panel">
					<a class="admin-login-logo" href="/"><img src="/images/logo-dark.png" class="img-responsive admin-logo-img center-block" alt=""></a>
				</div> -->
				@if(!empty($errors->all()))
						<div class="alert alert-danger">
							@foreach ($errors->all() as $error)
								<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
								<span>{{ $error }}</span><br/>
						  	@endforeach
						</div>
					@endif
					@if (session('message'))
					    <div class="alert alert-success">
					    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					        <span>{{ session('message') }}</span><br/>
					    </div>
					@endif
				<div class="panel panel-default">
					<div class="panel-heading text-center">Forget password</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<form class="form-horizontal" role="form" method="POST" action="/password/forgot">
									<input type="hidden" name="_token" value="{{ csrf_token() }}">

									<!-- <div class="form-group">
										<label class="col-md-4 control-label">E-Mail Address</label>
										<div class="col-md-6">
											<input type="email" class="form-control" name="email" value="{{ old('email') }}">
										</div>
									</div> -->
									<div class="input-group custom-input-group">
									  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-user fa-lg" aria-hidden="true"></i></span>
									  	<input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email">
									</div>

									<!-- <div class="input-group custom-input-group">
									  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
									  <input type="password" class="form-control" name="password" placeholder="Password">
									</div> -->
									<div class="row">
										<!-- <div class="col-md-6">
											<label class="checkbox-inline">
											  	<input type="checkbox" id="inlineCheckbox1" value="option1"> Remember
											</label>
										</div> -->
										
										<!-- <div class="col-md-6 text-left">
											<a href="signup" class="admin-gray-text">Sign up</a>
										</div> -->
										
									</div>
									<div class="sign-btn">
										
										<button type="submit" class="btn btn-primary btn-block admin-signin" >
												Reset Password
											</button>
									</div>
									
									<!-- <div class="form-group">
										<label class="col-md-4 control-label">Password</label>
										<div class="col-md-6">
											<input type="password" class="form-control" name="password">
										</div>
									</div>

									<div class="form-group">
										<div class="col-md-6 col-md-offset-4">
											<div class="checkbox">
												<label>
													<input type="checkbox" name="remember"> Remember Me
												</label>
											</div>
										</div>
									</div>

									<div class="form-group">
										<div class="col-md-6 col-md-offset-4">
											<button type="submit" class="btn btn-primary" style="margin-right: 15px;">
												Login
											</button> -->

											<!--<a href="/password/email">Forgot Your Password?</a>-->
										<!-- </div>
									</div> -->
								</form>
							</div>
						</div>		
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
