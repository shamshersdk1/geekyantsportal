<!DOCTYPE html>
<html>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <head>
        <style>
            table {
                border: 1px solid #dddddd;
                font-family: arial, sans-serif;
                border-collapse: collapse;
                width: 100%;
                page-break-inside: avoid;
                font-size: 11px;
            }
            tr, td {
                border: 1px solid #dddddd;
                text-align: left;
                padding: 8px;
                page-break-inside: avoid;
                font-size: 11px;
            }
            p {
                font-size:12px;
            }
        </style>
    </head>
    <body>
            @php
                $collection = $data["attatchment"];
                $values = array_values($collection);
                $keys = array_keys($collection);
            @endphp
             
            @forelse ($keys as $key)
                <h2> Audit Report: {{$data["date"]}}</h2>
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <hr />
                <h3>Report for {{$key}}</h3>
                <hr />
                @foreach ($collection[$key] as $item)
                    <div style="page-break-after: always;"></div>
                    <p>
                        <strong>{{$item["event"]}}</strong> on {{date("F dS, Y", strtotime($item["created_at"]))}} {{date("g:i:s a", strtotime($item["created_at"]))}}
                        <strong>
                            @php
                                $model = explode("\\", $item["auditable_type"]);
                            @endphp
                                {{$model[count($model) - 1]}} (id: {{$item["auditable_id"]}})
                            {{$item["user_name"] ?? ""}}
                        </strong>
                    <p><br />
                    <strong>Old Values:</strong>
                    <table>
                        @forelse ($item["old_values"] as $old_values_key => $old_values_item)
                            <tr>
                                <td>
                                    {{$old_values_key}}
                                </td>
                                <td style="max-width:250px; white-space:normal; word-wrap:break-word">
                                    @if (strpos($old_values_key, 'image_content'))
                                        Image Data
                                    @else
                                        {{$old_values_item ?? "No Data"}}
                                    @endif
                                </td>
                            </tr>
                        @empty
                            <tr><td>No Data</td></tr>
                        @endforelse
                    </table><br />
                    <strong>New Values:</strong>
                    <table>
                        @forelse ($item["new_values"] as $new_values_key => $new_values_item)
                            <tr>
                                <td>
                                    {{$new_values_key}}
                                </td>
                                <td style="max-width:250px; white-space:normal; word-wrap:break-word">
                                    @if (strpos($new_values_key, 'image_content'))
                                        Image Data
                                    @else
                                        {{$new_values_item ?? "No Data"}}
                                    @endif
                                </td>
                            </tr>
                            @empty
                            <tr><td>No Data</td></tr>
                        @endforelse
                    </table>
                    <hr />
                @endforeach
            @empty
                No auditable changes for this day...
            @endforelse
    </body>
</html>