@extends('layouts.user-dashboard')
@section('main-content')
<div class="error-page">
    <h1>404</h1>
    <h2 class="m-t-50"> Looks like you're lost</h2>
    <div class="text">The page you are looking for is not available!</div>
</div>
<style>
    .error-page {
        text-align: center;
        position: absolute;
        width: 100%;
        top: 40%;
        right:0;
    }
    h1 {
        font-size: 144px;
        font-weight: 100;
    }
</style>
@endsection