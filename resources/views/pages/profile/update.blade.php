@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">List of Users</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li><a href="{{ url('admin/users') }}">List of Users</a></li>
        		</ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <a href="/admin/users/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Add new User</a>
            </div>

		</div>
	</div>
	<div class="row">
        <div class="col-sm-12">
            
        </div>
    </div>
</div>
@endsection