@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Server</h1>
                <ol class="breadcrumb">
                        <li><a href="/admin">Admin</a></li>
                        <li class="active">List of Servers  </li>
                </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                {{Form::open(['url' => '/admin/server-management/server/create', 'method' => 'get'])}}
                    {{Form::button('<i class="fa fa-plus fa-fw"></i>  Add new Server', array('type' => 'submit', 'class' => 'btn btn-success'));}}
                {{Form::close()}}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
    	@if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span><br/>
                  @endforeach
            </div>
        @endif
        @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
        @endif
        </div>
    <div class="server-list-view">

        <div class="panel panel-default">
            <table class="table table-striped">
                <thead>
                    <th width="5%">#</th>
                    <th width="10%">Server Name</th>
                    <th width="10%">City</th>
                    <th width="10%">Country</th>
                    <th width="10%">Provider</th>
                    <th width="10%">Type</th>
                    <th width="10%">Provider Account</th>
                    <th>Actions</th>
                </thead>
                @if(count($serverObj) > 0)
                    @foreach($serverObj as $index=>$server)
                        <tr>
                            <td class="td-text">{{$index+1}}</td>
                            <td class="td-text">{{$server->name}}</td>
                            <td class="td-text">{{$server->city}}</td>
                            <td class="td-text">{{$server->country}}</td>
                            <td class="td-text">{{$server->provider?$server->provider->name :""}}</td>
                            <td class="td-text">{{$server->type?$server->type->name:""}}</td>
                            <td class="td-text">
                                @if($server->providerAccount != null)
                                    @foreach ($providerList as $x)
                                        @if($x->id == $server->providerAccount->provider_id)
                                            {{$x->name}}
                                        @endif
                                    @endforeach : {{$server->account?$server->providerAccount->username:''}}
                                @endif
                            </td>
                            <td>
                            <td class="td-text d-flex flex-row">
                                {{Form::open(['url' => '/admin/server-management/server/'.$server->id.'/manage', 'method' => 'get'])}}
                                    {{Form::button('<i class="fa fa-eye fa-fw btn-icon-space"></i>  Manage', array('type' => 'submit', 'class' => 'btn btn-primary'));}}
                                {{Form::close()}}
                                {{Form::open(['url' => '/admin/server-management/server/'.$server->id.'/manage-access', 'method' => 'get'])}}
                                    {{Form::button('<i class="fa fa-eye fa-fw btn-icon-space"></i>  Manage Access', array('type' => 'submit', 'class' => 'btn btn-primary'));}}
                                {{Form::close()}}
                                {{Form::open(['url' => '/admin/server-management/server/'.$server->id.'/edit', 'method' => 'get'])}}
                                    {{Form::button('<i class="fa fa-edit fa-fw btn-icon-space"></i>  Edit', array('type' => 'submit', 'class' => 'btn btn-warning'));}}
                                {{Form::close()}}
                                {{Form::open(['url' => '/admin/server-management/server/'.$server->id.'/delete', 'method' => 'get'])}}
                                    {{Form::button('<i class="fa fa-trash fa-fw btn-icon-space"></i>  Delete', array('type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => 'return confirm("Are you sure you want to delete this item?")'));}}
                                {{Form::close()}}
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="10">No results found.</td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
        @if(!empty($serverObj))
            {{$serverObj->links()}}
        @endif
	</div>
</div>
@endsection
