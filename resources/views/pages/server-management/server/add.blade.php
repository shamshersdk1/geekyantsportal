@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Server</h1>
                <ol class="breadcrumb">
					<li><a href="/admin">Admin</a></li>
					<li><a href="{{ url('admin/server-management/server') }}">List of Servers</a></li>
					<li class="active">Add New Server</li>
                </ol>
            </div>
			<div class="col-sm-4 text-right m-t-10">
				<button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
			</div>
        </div>
    </div>
        <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		            @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-body">
					{{ Form::open(['url' => '/admin/server-management/server', 'style' => 'padding:10px', 'id' => 'formId']) }}
						{{Form::token();}}
						<div class="form-group col-md-6">
							{{ Form::label('provider', 'Provider:', ['class' => 'col-md-4', 'style' => 'padding-top:2%', 'align' => 'left']) }}
							<div class="col-md-8 col-sm-12 col-xs-12" style="padding:0px">
								@if(isset($providerList))
									@if(!$providerObj)
										{{Form::select('provider_id', $providerList->pluck('name','id'), null, ['id'=>'selectid3', 'placeholder'=>'Select provider', 'style' => 'width:35%'])}}
									@else 
										{{Form::select('provider_id', $providerList->pluck('name','id'), $providerObj->id, ['id'=>'selectid3', 'placeholder'=>'Select provider', 'style' => 'width:35%'])}}
									@endif
								@endif
							</div>
						</div>
						<div class="form-group col-md-6">
							{{ Form::label('account', 'Provider Account:', ['class' => 'col-md-4', 'style' => 'padding-top:2%', 'align' => 'left']) }}
							<div class="col-md-8 col-sm-12 col-xs-12" style="padding:0px">
								@if(isset($accountList))
									{{Form::select('provider_account_id', $accountList->pluck('provider_name', 'id'), null, ['id'=>'selectid4', 'placeholder'=>'Select provider account', 'style' => 'width:35%', 'value' => '$typeList->id'])}}
								@endif
							</div>
						</div>	
						<div class="form-group col-md-6">
							{{ Form::label('name', 'Server Name:', ['class' => 'col-md-4', 'style' => 'padding-top:2%', 'align' => 'left']) }}
							<div class="col-md-8" style="padding:0">
								{{ Form::text('name', '', ['class' => 'form-control']) }}
                            </div>
						</div>
						<div class="form-group col-md-6">
							{{ Form::label('hostname', 'Host Name:', ['class' => 'col-md-4', 'style' => 'padding-top:2%', 'align' => 'left']) }}
							<div class="col-md-8" style="padding:0">
								{{ Form::text('hostname', '', ['class' => 'form-control']) }}
							</div>
						</div>
						<div class="form-group col-md-6">
							{{ Form::label('city', 'City:', ['class' => 'col-md-4', 'style' => 'padding-top:2%', 'align' => 'left']) }}
							<div class="col-md-8" style="padding:0">
								{{ Form::text('city', '', ['class' => 'form-control']) }}
							</div>
						</div>
						<div class="form-group col-md-6">
							{{ Form::label('country', 'Country:', ['class' => 'col-md-4', 'style' => 'padding-top:2%', 'align' => 'left']) }}
							<div class="col-md-8" style="padding:0">
								{{ Form::text('country', '', ['class' => 'form-control']) }}
							</div>
						</div>
						<div class="form-group col-md-6">
							{{ Form::label('type', 'Type:', ['class' => 'col-md-4', 'style' => 'padding-top:2%', 'align' => 'left']) }}
							<div class="col-md-8 col-sm-12 col-xs-12" style="padding:0px">
								@if(isset($typeList))
									{{Form::select('type_id', $typeList->pluck('name', 'id'), null, ['id'=>'selectid2', 'placeholder'=>'Select type', 'style' => 'width:35%', 'value' => '$typeList->id'])}}
								@endif
							</div>
						</div>
						<div class="form-group col-md-6">
							{{ Form::label('ip', 'Primary IP:', ['class' => 'col-md-4', 'style' => 'padding-top:2%', 'align' => 'left']) }}
							<div class="col-md-8 col-sm-12 col-xs-12" style="padding:0px">
								{{ Form::text('primary_ip', '', ['class' => 'form-control']) }}
							</div>
						</div>
						<div class="form-group col-md-6">
							{{ Form::label('password', 'Root Password:', ['class' => 'col-md-4', 'style' => 'padding-top:2%', 'align' => 'left']) }}
							<div class="col-md-8" style="padding:0"> 
								{{ Form::text('root_password', '', ['class' => 'form-control']) }}
							</div>
						</div>
						<div class="form-group col-md-6">
							{{ Form::label('notes', 'Notes:', ['class' => 'col-md-4', 'style' => 'padding-top:2%', 'align' => 'left']) }}
							<div class="col-md-8" style="padding: 0; margin-left: -5px">
								{{Form::textarea('notes', null,['placeholder'=>'Enter Notes','rows'=> 5, 'style' => 'width:100%;margin:5px;','class'=>"form-control"])}}
							</div>
						</div>
						<div class="col-md-12">
							{{Form::submit('Save', ['style' => 'display: block; margin: 0 auto;', 'class' => 'btn btn-success'])}}
						</div>							

					{{ Form::close() }}
				</div>
			</div>
		</div>	
	</div>
</div>
<script>
	$('#selectid3').change(function(){
		console.log('here');
    var optionSelected = $("option:selected", this);
    optionValue = this.value;
	console.log(optionValue);
	if (optionValue) { 
    	window.location = "/admin/server-management/server/create/"+optionValue; 
    }
});

</script>
@endsection