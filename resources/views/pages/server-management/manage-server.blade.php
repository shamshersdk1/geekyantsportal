@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
	<!--breadcrumb-->
    <div class="breadcrumb-wrap">
		<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Server</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
					<li><a href="{{ url('admin/server-management/server') }}">List of Servers</a></li>
					<li class="active">Manage - {{$serverObj->name}}</li>
                </ol>
			</div>
			<div class="col-sm-4 text-right m-t-10">
				{{Form::button('<i class="fa fa-caret-left fa-fw"></i> Back', array('type' => 'submit','class' => 'btn btn-default','onclick' => 'window.history.back();'));}}
            </div>
        </div>
	</div>
	<!--error and message-->
	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
	<!--start of main content-->
	<!--server details-->
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">Server Details</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-6">
						 	{{Form::label('server_name', 'Server Name:')}}
							<span style="text-transform: capitalize;">{{$serverObj->name?$serverObj->name:''}}</span><br/>
							{{Form::label('primary_ip', 'Primary IP:')}}
							<span style="text-transform: capitalize;">{{$serverObj->primary_ip?$serverObj->primary_ip:''}}</span><br/>
							{{Form::label('city', 'City:')}}
							<span style="text-transform: capitalize;">{{$serverObj->city?$serverObj->city:''}}</span><br/>
							{{Form::label('country', 'Country:')}}
							<span style="text-transform: capitalize;">{{$serverObj->country?$serverObj->country:''}}</span><br/>
						</div>
						<div class="col-md-6">
							{{Form::label('hostname', 'HostName:')}}
							<span style="text-transform: capitalize;">{{$serverObj->hostname?$serverObj->hostname:''}}</span><br/>
							{{Form::label('root_password', 'Root Password:')}}
							<span style="text-transform: capitalize;">{{$serverObj->root_password?$serverObj->root_password:''}}</span><br/>
							{{Form::label('provider', 'Provider:')}}
							<span style="text-transform: capitalize;">{{$serverObj->provider?$serverObj->provider->name:''}}</span><br/>
							{{Form::label('type', 'Type:')}}
							<span style="text-transform: capitalize;">{{$serverObj->type?$serverObj->type->name:''}}</span><br/>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--server notes & server meta-->
	<div class="row">
		<!--server notes-->
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">Server Notes</div>
				<div class="panel-body">
					<!--Add New Note-->
					{{Form::open(['url' => 'admin/server-management/server/'.$serverObj->id.'/add-note'])}}
						{{Form::textarea('notes', null,['placeholder'=>'Enter Notes','rows'=> 5, 'style' => 'width:100%;margin:5px;','class'=>"form-control"])}}
						<div class="col-md-12">
							{{Form::submit('Save',['class'=>"btn btn-success pull-right",'style'=>"display: block; margin: 0 auto;"])}}
						</div>
					{{Form::close()}}
					<!--Display Existing Note(s)-->
					<div>
						@if(isset($serverObj->note))
						@foreach($serverObj->note as $serverNote)
						<div class="row">
							<div style="padding:10px;">
								<p>{{$serverNote->notes}}</p>
								<span class="pull-right"><small>{{datetime_in_view($serverNote->created_at)}}</small></span>
							</div>
						</div>
						@endforeach
						@else
						<div class="row">
							<div style="padding:10px;">
								<span style="text-align:center;" class="align-center"><big>No Notes exist for this server</big></span>
							</div>
						</div>
						@endif
					</div>
				</div>
			</div>
		</div>
		<!--server meta-->
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">Server Meta</div>
				<div class="panel-body">
					<!--Add New Meta-->
					<div>
					{{Form::open(['url' => 'admin/server-management/server/'.$serverObj->id.'/add-meta'])}}
							<div class="col-md-12">
								{{Form::text('key', null,['placeholder'=>'Enter key', 'style' => 'width:100%;margin:5px;','class'=>"form-control"])}}
								{{Form::text('value', null,['placeholder'=>'Enter value', 'style' => 'width:100%;margin:5px;','class'=>"form-control"])}}
							</div>
							<div class="col-md-12">
								{{Form::submit('Save',['class'=>"btn btn-success pull-right",'style'=>"display: block; margin: 0 auto;"])}}
							</div>
					{{Form::close()}}
					</div>
					<!--Display Existing Meta(s)-->
					<div>
						<table class="table table-striped">
							<thead>
								<th width="10%">#</th>
								<th width="15%">Key</th>
								<th width="10%">Value</th>
								<th width="10%">Actions</th>
							</thead>
							@if(isset($serverObj->meta))
								@foreach($serverObj->meta as $serverMeta)
								<tbody>
									<tr>
										<td class="td-text">{{$serverMeta->id?$serverMeta->id:''}}</td>
										<td class="td-text">{{$serverMeta->key?$serverMeta->key:''}}</td>
										<td class="td-text">{{$serverMeta->value?$serverMeta->value:''}}</td>
										<td>
											{{Form::open(['url' => '/admin/server-management/server/'.$serverObj->id.'/delete-meta/'.$serverMeta->id, 'method' => 'post', 'onclick' => 'return confirm("Are you sure you want to delete this item?")'])}}
												{{Form::button('<i class="fa fa-trash btn-icon-space"></i>  Delete', array('type' => 'submit', 'class' => 'btn btn-danger btn-sm'));}}
											{{Form::close()}}
										</td>
									</tr>
								</tbody>
								@endforeach
							@else
							<tbody>
								<tr>
									<td colspan="4" style="text-align:center;"><span class="align-center"><big>No Meta exists for this server</big></span></td>
								</tr>
							</tbody>
						@endif
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--accounts and access-->
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">Server Account Access</div>
				<div class="panel-body">
						<table class="table table-striped">
							<thead>
								<th width="10%">#</th>
								<th width="15%">UserName</th>
								<th width="10%">Active Status</th>
								<th width="10%">Accessed Users</th>
								<th width="10%">Actions</th>
							</thead>
							@if(isset($serverObj->account))
								<tbody>
								@foreach($serverObj->account as $serverAccount)
									<tr>
										<td class="td-text">{{$serverAccount->id?$serverAccount->id:''}}</td>
										<td class="td-text">{{$serverAccount->username?$serverAccount->username:''}}</td>
										<td class="td-text">@if($serverAccount->is_active)<span class="label label-success" style="padding:10px;">Yes</span>@else<span class="label label-danger" style="padding:10px;">No</span>@endif</td>
										<td class="td-text">
											@if(isset($serverAccount->access))
												@foreach($serverAccount->access as $accessed)
													@if(isset($accessed->user))
														<span class="label label-primary" style="padding:6px;">{{$accessed->user->name}}</span><br/>
													@endif
												@endforeach
											@endif
										</td>
										<td>
										{{Form::open(['url' => 'admin/server-management/server/'.$serverObj->id.'/manage-access', 'method' => 'get'])}}
											{{Form::button('<i class="fa fa-eye btn-icon-space"></i> Manage', array('type' => 'submit','class' => 'btn btn-primary btn-sm'));}}
										{{Form::close()}}
										</td>
									</tr>
								@endforeach
								</tbody>
							@else
							<tbody>
								<tr>
									<td colspan="5" style="text-align:center;"><span class="align-center"><big>No Access exists for this server</big></span></td>
								</tr>
							</tbody>
						@endif
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--end of main content-->
</div>
@endsection
