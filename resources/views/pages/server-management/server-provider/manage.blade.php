@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
		<div class="row">
            <div class="col-sm-8">
				<h1 class="admin-page-title">Server Provider Account</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
					<li><a href="{{ url('admin/server-management/server-provider') }}">Server Provider</a></li>
					<li class="active">{{$serverProvider->name}}</li>
                </ol>
			</div>
			<div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
				
				</div>
        </div>
	</div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel panel-default">
				<div class="panel-heading">Add Server Provider Account</div>
				<div class="panel-body">
					{{ Form::open(['url' => '/admin/server-management/server-provider/'.$serverProvider->id.'/server-acccount', 'method' => 'post']) }}
					<div class="form-group col-md-6">
                    	{{ Form::label('username', 'Username :',['class' => 'col-md-4','style' => 'padding-top:2%','align' => 'left'])}}
						<div class="col-md-8" style="padding:0">
                    		{{ Form::text('username', '',['class' => 'form-control']) }}
						</div>
					</div>
					<div class="form-group col-md-6">
                    {{ Form::label('password', 'Password :',['class' => 'col-md-4','style' => 'padding-top:2%'])}}
						<div class="col-md-8" style="padding:0">
                    		{{ Form::text('password', '',['class' => 'form-control']) }}
						</div>
					</div>
					<div class="form-group col-md-6">
                    	{{ Form::label('url', 'URL :',['class' => 'col-md-4','style' => 'padding-top:2%','align' => 'left'])}}
						<div class="col-md-8" style="padding:0">
                    		{{ Form::text('url', '',['class' => 'form-control']) }}
						</div>
					</div>
					<div class="form-group col-md-6">
                    {{ Form::label('notes', 'Notes :',['class' => 'col-md-4','style' => 'padding-top:2%'])}}
						<div class="col-md-8" style="padding:0">
                    		{{ Form::text('notes', '',['class' => 'form-control']) }}
						</div>
					</div>
					<div class="form-group col-md-6">
                    {{ Form::label('self_owned', 'Self Owned :',['class' => 'col-md-4','style' => 'padding-top:2%','align' => 'left'])}}
						<div class="col-md-8 col-sm-12 col-xs-12" style="padding:0px">
                    		{{ Form::select('self_owned', ['1' => 'True', '0' => 'False'], '1',['id'=>'selectid2', 'style' => 'width:35%'])}}
						</div>
					</div>
					<div class="col-md-12">
					{{ Form::submit('Save',['class' => 'btn btn-success','style' => 'display: block; margin: 0 auto']) }}
					</div>
                    {{ Form::close() }}
				</div>
				<table class="table table-striped">
				<thead>
					<th width="10%">#</th>
					<th width="10%">Self Owned</th>
					<th width="15%">Username</th>
					<th width="10%">Password</th>
					<th width="10%">Note</th>
                    <th width="10%">URL</th>
					<th width="25%" class="text-right">Actions</th>
				</thead>

				@if(count($serverProvider->providerAccounts) > 0)
					@foreach($serverProvider->providerAccounts as $providerAccount)
						<tr>
							<td class="td-text">{{$providerAccount->id}}</td>
							<td class="td-text">
							@if($providerAccount->is_self_owned == 1)
								<label>Yes</label>
							@else
								<label>No</label>
							@endif
							<td class="td-text"> {{$providerAccount->username}}</td>
							<td class="td-text"> {{$providerAccount->password}}</td>
							<td class="td-text"> {{$providerAccount->notes}}</td>
                            <td class="td-text"> {{$providerAccount->url}}</td>
							<td class="text-right">
								<a href="{{$serverProvider->id}}/server-acccount/{{$providerAccount->id}}" class="btn btn-info crud-btn btn-sm"><i class="fa fa-pencil btn-icon-space" aria-hidden="true"></i>Edit</a>
								<div style="display:inline-block;" class="deleteform">
								{{ Form::open(['url' => '/admin/server-management/server-provider/'.$serverProvider->id.'/server-acccount/'.$providerAccount->id, 'method' => 'delete']) }}
								{{ Form::submit('Delete',['class' => 'btn btn-danger', 'onclick' => 'return(confirm("Are you sure you want to delete this?"))']) }}
								{{ Form::close() }}
								</div>
							</td>
						</tr>
					@endforeach
				@else
					<tr>
						<td colspan="7">No Provider Accounts.</td>
					</tr>
				@endif

				</table>

			</div>
		</div>
	</div>
</div>
<script>
	$(function () {
		$('#type option[value="{{old('type')}}"]').attr("selected",true);
	});
</script>
@endsection
