@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
		<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Server Provider Account</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
					<li><a href="{{ url('admin/server-management/server-provider') }}">Server Provider Account</a></li>
                    <li><a href="{{ url('admin/server-management/server-provider/'.$serverProviderAccount->provider->id) }}">{{$serverProviderAccount->provider->name}}</a></li>
                    <li class="active">Edit Server Provider Account - {{$serverProviderAccount->id}}</li>
                </ol>
			</div>
			<div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
        </div>
	</div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">Edit Provider Account</div>
				<div class="panel-body">
				{{ Form::open(['url' => '/admin/server-management/server-provider/'.$serverProviderAccount->provider->id.'/server-acccount/'.$serverProviderAccount->id, 'method' => 'put']) }}
					<div class="form-group col-md-6">
                    	{{ Form::label('username', 'Username :',['class' => 'col-md-4','style' => 'padding-top:2%','align' => 'left'])}}
						<div class="col-md-8" style="padding:0">
                    		{{ Form::text('username',$serverProviderAccount->username,['class' => 'form-control']) }}
						</div>
					</div>
					<div class="form-group col-md-6">
                    {{ Form::label('password', 'Password :',['class' => 'col-md-4','style' => 'padding-top:2%'])}}
						<div class="col-md-8" style="padding:0">
                    		{{ Form::text('password', $serverProviderAccount->password,['class' => 'form-control']) }}
						</div>
					</div>
					<div class="form-group col-md-6">
                    	{{ Form::label('url', 'URL :',['class' => 'col-md-4','style' => 'padding-top:2%','align' => 'left'])}}
						<div class="col-md-8" style="padding:0">
                    		{{ Form::text('url', $serverProviderAccount->url,['class' => 'form-control']) }}
						</div>
					</div>
					<div class="form-group col-md-6">
                    {{ Form::label('notes', 'Notes :',['class' => 'col-md-4','style' => 'padding-top:2%'])}}
						<div class="col-md-8" style="padding:0">
                    		{{ Form::text('notes', $serverProviderAccount->notes,['class' => 'form-control']) }}
						</div>
					</div>
					<div class="form-group col-md-6">
                    {{ Form::label('self_owned', 'Self Owned :',['class' => 'col-md-4','style' => 'padding-top:2%','align' => 'left'])}}
						<div class="col-md-8 col-sm-12 col-xs-12" style="padding:0px">
                    		{{ Form::select('self_owned', ['1' => 'True', '0' => 'False'], $serverProviderAccount->is_self_owned,['id'=>'selectid2', 'style' => 'width:35%'])}}
						</div>
					</div>
					<div class="col-md-12">
					{{ Form::submit('Save',['class' => 'btn btn-success','style' => 'display: block; margin: 0 auto']) }}
					</div>
                    {{ Form::close() }}

				</div>
            </div>
		</div>
	</div>
</div>
<script>
	$(function () {
		$('#type option[value="{{ucfirst($serverProviderAccount->is_self_owned)}}"]').prop("selected",true);
		$('#rule-selector option[value="{{$serverProviderAccount->name}}"]').prop("selected",true);
	});
</script>
@endsection
