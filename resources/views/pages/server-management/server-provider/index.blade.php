@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
		<div class="row">
            <div class="col-sm-8">
				<h1 class="admin-page-title">Server Provider</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li class="active">Server Provider</a></li>
                </ol>
			</div>
			<div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
        </div>
	</div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">Add Server Provider</div>
				<div class="panel-body">
				{{ Form::open(['url' => '/admin/server-management/server-provider', 'method' => 'post']) }}
					<div class="form-group col-md-6">
                    	{{ Form::label('name', 'Name :',['class' => 'col-md-4','style' => 'padding-top:2%','align' => 'left'])}}
						<div class="col-md-8" style="padding:0">
                    		{{ Form::text('name', '',['class' => 'form-control']) }}
						</div>
					</div>
					<div class="col-md-12">
					{{ Form::submit('Save',['class' => 'btn btn-success','style' => 'display: block; margin: 0 auto']) }}
					</div>
                    {{ Form::close() }}
				</div>
				</div>
				<div class="panel panel-default">
				<table class="table table-striped">
				<thead>
					<th width="10%">#</th>
					<th width="10%">Name</th>


					<th width="25%" class="text-right">Actions</th>
				</thead>
				@if(isset($serverProviders))
					@if(count($serverProviders) > 0)
						@foreach($serverProviders as $serverProvider)
							<tr>
								<td class="td-text">{{$serverProvider->id}}</td>
								<td class="td-text"> {{$serverProvider->name}}</td>
								<td class="text-right">
									<a href="/admin/server-management/server-provider/{{$serverProvider->id}}" class="btn btn-warning btn-sm"><i class="fa fa-gear btn-icon-space" aria-hidden="true"></i>Manage</a>
									<a href="/admin/server-management/server-provider/{{$serverProvider->id}}/edit" class="btn btn-info btn-sm"><i class="fa fa-pencil btn-icon-space" aria-hidden="true"></i>Edit</a>
									<div style="display:inline-block;" class="deleteform">
									@if(!$serverProvider->servers || count($serverProvider->servers) == 0)
										{{ Form::open(['url' => '/admin/server-management/server-provider/'.$serverProvider->id, 'method' => 'delete']) }}
										{{ Form::submit('Delete',['class' => 'btn btn-danger', 'onclick' => 'return confirm("Are you sure you want to delete this item?")']) }}
										{{ Form::close() }}
									@endif
									</div>
								</td>
							</tr>
						@endforeach
					@else
						<tr>
							<td colspan="7">Nothing found.</td>
						</tr>
					@endif
				@endif

				</table>

			</div>
		</div>
	</div>
</div>
<script>
	$(function () {
		$('#type option[value="{{old('type')}}"]').attr("selected",true);
	});
</script>
@endsection
