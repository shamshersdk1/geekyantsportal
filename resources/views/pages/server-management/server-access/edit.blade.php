@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid dashboard-container" >
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Server Access</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
					<li><a href="{{ url('admin/server-management/server') }}">List of Servers</a></li>
                    <li><a href="{{ url('admin/server-management/server/'.$serverObj->id.'/manage-access') }}">{{$serverObj->name}} - Manage Server Access</a></li>
                    <li class="active">Edit Access</a></li>

                </ol>
            </div>
        </div>
    </div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
    </div>
    <div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">Edit Server Access</div>
				<div class="panel-body">
				{{ Form::open(array('url' => "admin/server-management/server/{$serverObj->id}/{$serverAccountAccess->id}/update")) }}
					<!-- <form action = "" method="POST" style="padding:10px;" >  -->
						<div class="form-group col-md-6">
						{{ Form::Label('user', 'User Name:',array('class' =>'col-md-4' , 'style' =>'padding:3%')) }}
							<div class="col-md-8" style="padding:0">
							<div class="col-sm-8">
						{{ Form::select('user_id', $users->pluck('name','id'),$serverAccountAccess->user_id, array('class' => 'editSelect' ,'placeholder' => 'Select A User'));}}

                        </div>
							</div>
						</div>
						<div class="form-group col-md-6">
						{{ Form::Label('account_type', 'Account:',array('class' =>'col-md-4' , 'style' =>'padding:3%')) }}
							<div class="col-md-8" style="padding:0">
							{{ Form::select('account_id',$serverObj->account->pluck('username','id'),$serverAccountAccess->account_id, array('class' => 'editSelect1'));}}

							</div>
						</div>
						{{Form::submit('save',array('class' => 'btn btn-success ','style'=>'display:block;margin:auto'))}}
						{{ Form::close() }}
					<!-- </form> -->
				</div>

		</div>
    </div>

</div>
<script type="text/javascript">
	$(function () {
		$('.editSelect').select2({
			placeholder: 'Select a user',
			allowClear:true
		});
		$('.editSelect1').select2({
			placeholder: 'Select a user',
			allowClear:true
		});
	});
</script>

@endsection
