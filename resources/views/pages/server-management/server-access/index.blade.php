@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid dashboard-container" >
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Server Access</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
					<li><a href="{{ url('admin/server-management/server') }}">List of Servers</a></li>
                    <li class="active">{{$serverObj->name}} - Manage Server Access</a></li>

                </ol>
            </div>
        </div>
    </div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
	<div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Server Details</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Server Name:</label>
                            <span style="text-transform: capitalize;">{{$serverObj->name?$serverObj->name:''}}</span><br/>
                            <label>Primary IP:</label>
                            <span style="text-transform: capitalize;">{{$serverObj->primary_ip?$serverObj->primary_ip:''}}</span><br/>
                            <label>City:</label>
                            <span style="text-transform: capitalize;">{{$serverObj->city?$serverObj->city:''}}</span><br/>
                            <label>Country:</label>
                            <span style="text-transform: capitalize;">{{$serverObj->country?$serverObj->country:''}}</span><br/>
                        </div>
                        <div class="col-md-6">
                            <label>HostName:</label>
                            <span style="text-transform: capitalize;">{{$serverObj->hostname?$serverObj->hostname:''}}</span><br/>
                            <label>Root Password:</label>
                            <span style="text-transform: capitalize;">{{$serverObj->root_password?$serverObj->root_password:''}}</span><br/>
                            <label>Provider:</label>
                            <span style="text-transform: capitalize;">{{$serverObj->provider?$serverObj->provider->name:''}}</span><br/>
                            <label>Type:</label>
                            <span style="text-transform: capitalize;">{{$serverObj->type?$serverObj->type->name:''}}</span><br/>
                        </div>
                    </div>
                </div>
</div>
        </div>
    </div>

	<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">Add Server Account</div>
					<div class="panel-body">
					{{ Form::open(array('url' => "admin/server-management/server/{$serverObj->id}/add-server-account")) }}
							<div class="form-group col-md-6">
							{{ Form::Label('account', 'Account Name:',array('class' =>'col-md-4' , 'style' =>'padding:3%')) }}
								<div class="col-md-8" style="padding:0">
								<div class="col-sm-8">
									{{ Form::text('username', null, array('style' => 'width:100%;height:40px')) }}

	
							</div>
								</div>
							</div>
		
							{{Form::submit('Save',array('class' => 'btn btn-success ','style'=>'display:block;margin:auto'))}}
							{{ Form::close() }}
						<!-- </form> -->
					</div>
				</div>
	
			</div>
		</div>


    <div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">Add Server Access</div>
				<div class="panel-body">
				{{ Form::open(array('url' => "admin/server-management/server/{$serverObj->id}/manage-access")) }}
					<!-- <form action = "" method="POST" style="padding:10px;" >  -->
						<div class="form-group col-md-6">
						{{ Form::Label('user', 'User Name:',array('class' =>'col-md-4' , 'style' =>'padding:3%')) }}
							<div class="col-md-8" style="padding:0">
							<div class="col-sm-8">
						{{ Form::select('user_id', $users->pluck('name','id'),null, array('class' => 'userSelect'  ,'placeholder' => 'Select User'));}}

                        </div>
							</div>
						</div>
						<div class="form-group col-md-6">
						{{ Form::Label('account_type', 'Account:',array('class' =>'col-md-4' , 'style' =>'padding:3%')) }}
							<div class="col-md-8" style="padding:0">
							{{ Form::select('account_id', $serverObj->account->pluck('username','id'),null, array('class' => 'userSelect1','placeholder' => 'Select Account'));}}

							</div>
						</div>
						{{Form::submit('Save',array('class' => 'btn btn-success ','style'=>'display:block;margin:auto'))}}
						{{ Form::close() }}
					<!-- </form> -->
				</div>
			</div>

		</div>
    </div>
    <div class="row">
    	<div class="panel panel-default">
			<table class="table table-striped">
				<thead>
					<th width="20%">#</th>
					<th width="25%">Account</th>
					<th width="25%">User Name</th>
					<th class="text-right">Actions</th>
				</thead>
				<tbody>
					@if(count($serverAccountAccess)>0)
						@foreach($serverAccountAccess as $index=> $serverAccount)
						<tr>
							<td>{{$index + 1}}</td>
							<td>{{$serverAccount[0]->account ? $serverAccount[0]->account->username :''}}
							<td>
							@foreach($serverAccount as $account)
								<div style="padding-bottom:10px">
									<div class="row">
										<div class="col-sm-6">
											{{$account->user ? $account->user['name'] : ''}} 
										</div>
										<div class="col-sm-6">
											<div class="row">
												<div class="col-sm-6">
													<a href="{{$account->id}}/edit" class="btn btn-info crud-btn btn-sm"><i class="fa fa-pencil btn-icon-space" aria-hidden="true" style="margin-bottom:10"></i>Edit</a>
												</div>
												<div class="col-sm-2">
													<form action="{{$account->id}}/delete" method="post" style="display:inline-block;" class="deleteform">
														<input name="_method" type="hidden" value="DELETE">
														{{Form::submit('Delete',array('class' => 'btn btn-danger btn-sm', 'style'=> 'margin-bottom:10px', 'onclick' => 'return confirm("Are you sure you want to delete this item?")'))}}
													</form>
												</div>
											</div>
										</div>
									</div>
								</div>
							@endforeach
							</td>
							<td class="text-right">
				
								<form action="{{$serverAccount[0]->account->id}}/delete-account" method="post" style="display:inline-block;" class="deleteform">
									<input name="_method" type="hidden" value="DELETE">
									{{Form::submit('Delete Account',array('class' => 'btn btn-danger', 'onclick' => 'return confirm("Are you sure you want to delete this item?")'))}}
									<!-- <button class="btn btn-danger btn-sm" type="submit"><i class="fa fa-trash btn-icon-space" aria-hidden="true"></i>Delete</button> -->
								</form>
							</td>
						</tr>
						@endforeach
					@else
						<td colspan="4" style="text-align:center;"><span class="align-center"><big>No Access exists for this Server</big></span></td>
					@endif
				</tbody>
			</table>
		</div>
    </div>
</div>
<script type="text/javascript">
	$(function () {
		$('.userSelect').select2({
			placeholder: 'Select a user',
			allowClear:true
		});
		$('.userSelect1').select2({
			placeholder: 'Select a user',
			allowClear:true
		});
	});
</script>
@endsection
