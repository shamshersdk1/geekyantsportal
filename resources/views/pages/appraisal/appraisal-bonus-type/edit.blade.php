@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid dashboard-container" >
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Appraisal Bonus Type</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
					<li><a href="{{ url('admin/appraisal/appraisal-bonus-type') }}">Appraisal Bonus Type</a></li>
					<li class="active">{{$appraisalBonusType->id}}</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>

    <div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">Add Appraisal Bonus Type</div>
					<div class="panel-body">
						{{ Form::open(array('url' => 'admin/appraisal/appraisal-bonus-type/'.$appraisalBonusType->id,'method'=>'put')) }}
							<div class="row">
								<div class="form-group col-md-6">
									{{ Form::Label('Code', 'Code:',array('class' =>'col-md-4')) }}
										<div class="col-md-8" style="padding:0">
											<div class="col-sm-8">
												{{ Form::text('code', $appraisalBonusType->code, array('style' => 'width:150%;height:40px')) }}

											</div>
										</div>
								</div>
							</div>
							<div class="row">

							<div class="form-group col-md-6">
								{{ Form::Label('is_loanable', 'Can be applied against loan :',array('class' =>'col-md-4')) }}
									<div class="col-md-8" style="padding:0">
										<div class="col-sm-8">
										{{ Form::select('is_loanable', ['1' => 'Yes', '0' => 'No'], $appraisalBonusType->is_loanable,['id'=>'selectid2', 'style' => 'width:150%'])}}
										</div>
									</div>
								</div>
							</div>

						<div class="row">	
						<div class="form-group col-md-6">
						{{ Form::Label('description', 'Description:',array('class' =>'col-md-4')) }}
							<div class="col-md-8" style="padding:0">
								<div class="col-sm-8">
								{{ Form::textarea('description',$appraisalBonusType->description,['class'=>'form-control', 'rows' => 5 ,'style'=>'width:150%'] ) }}

								</div>
							</div>
						</div>
						</div>
						</div>

						{{Form::submit('Update',array('class' => 'btn btn-success ','style'=>'display:block;margin-left:250px'))}}
						{{ Form::close() }}
					<!-- </form> -->
				</div>
			<div class="row">
    	
    </div>
	</div>

    </div>
    
</div>

@endsection
