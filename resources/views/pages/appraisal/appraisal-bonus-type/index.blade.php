@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid dashboard-container" >
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Appraisal Bonus Type</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
					<li><a href="{{ url('admin/appraisal/appraisal-bonus-type') }}">Appraisal Bonus Type</a></li>

                </ol>
            </div>
        </div>
    </div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>

    <div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">Add Appraisal Bonus Type</div>
					<div class="panel-body">
						{{ Form::open(array('url' => "admin/appraisal/appraisal-bonus-type")) }}
							<div class="row">
								<div class="form-group col-md-12">
									{{ Form::Label('Code', 'Code:',array('class' =>'col-md-4' , 'style' =>'padding:1%', 'align' => 'right')) }}
										<div class="col-md-8" style="padding:0">
											<div class="col-sm-4">
												{{ Form::text('code', null, array('style' => 'width:150%;height:40px')) }}
											</div>
										</div>
								</div>
							</div>

							<div class="row">
							<div class="form-group col-md-12">
								{{ Form::Label('is_loanable', 'Can be applied against loan :',array('class' =>'col-md-4' , 'style' =>'padding:1%', 'align' => 'right')) }}
									<div class="col-md-8" style="padding:0">
										<div class="col-sm-4">
										{{ Form::select('is_loanable', ['1' => 'Yes', '0' => 'No'], '1',['id'=>'selectid2', 'style' => 'width:150%;height:40px'])}}
										</div>
									</div>
								</div>
							</div>

							<div class="row">
							<div class="form-group col-md-12">
								{{ Form::Label('description', 'Description:',array('class' =>'col-md-4' , 'style' =>'padding:1%', 'align' => 'right')) }}
								<div class="col-md-8" style="padding:0">
									<div class="col-sm-4">
										{{ Form::textarea('description',null,['class'=>'form-control', 'rows' => 5 ,'style'=>'width:150%'] ) }}
									</div>
								</div>
							</div>
							<div class="col-md-12">
								{{ Form::submit('Save',['class' => 'btn btn-success','style' => 'display: block; margin: 0 auto']) }}
							</div>
                    		{{ Form::close() }}
					</div>
				</div>
			</div>
		</div>
    	<div class="panel panel-default">
			<table class="table table-striped">
				<thead>
					<th width="10%">#</th>
					<th width="20%">Code</th>
					<th width="25%">Description</th>
					<th width="25%">Against Loan</th>
					<th width="20%">Action</th>
				</thead>
				<tbody>
					@if(count($appraisalBonusType)>0)
						@foreach($appraisalBonusType as $index=> $bonusType)
						<tr>
							<td>{{$index + 1}}</td>
							<td>{{$bonusType->code ? $bonusType->code : ''}}</td>
							<td>{{$bonusType->description ? $bonusType->description : ''}}</td>
							<td>
							@if($bonusType->is_loanable == 1)
								<span class="label label-primary">Yes<span>
							@else	
								<span class="label label-warning">No<span>
							@endif
							</td>
							
							<td class="text-right">
							<a href="appraisal-bonus-type/{{$bonusType->id}}/edit" class="btn btn-info crud-btn btn-sm"><i class="fa fa-pencil btn-icon-space" aria-hidden="true" style="margin-bottom:10"></i>Edit</a>
							<div style="display:inline-block;" class="deleteform">
                                {{ Form::open(['url' => '/admin/appraisal/appraisal-bonus-type/'.$bonusType->id, 'method' => 'delete']) }}
                                {{Form::button('<i class="fa fa-trash fa-fw btn-icon-space"></i>  Delete', array('type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick' => 'return confirm("Are you sure you want to delete this item?")'));}}
								{{ Form::close() }}
                                </div>
							</td>
						</tr>
						@endforeach
					@else
						<td colspan="4" style="text-align:center;"><span class="align-center"><big>No Appraisal Bonus Type</big></span></td>
					@endif
				</tbody>
			</table>
		</div>
	</div>

    </div>
    
</div>

@endsection
