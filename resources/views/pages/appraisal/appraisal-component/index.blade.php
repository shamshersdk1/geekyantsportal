@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
		<div class="row">
            <div class="col-sm-8">
				<h1 class="admin-page-title">Appraisal Component </h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
					<li class="active">Appraisal Component</li>
                </ol>
			</div>
			<div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
        </div>
	</div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">Add Appraisal Component </div>
				<div class="panel-body" id="main-panel">
                    {{ Form::open(['url' => 'admin/appraisal/component/add', 'method' => 'post']) }}
                    @if($appraisalComponentTypes)
                        @foreach($appraisalComponentTypes as $component)
                        <div class="row">
                            <div class="form-group col-sm-3">
                                {{ Form::label('component', 'Component ',['class' => 'col-md-4','style' => 'padding-top:2%','align' => 'left'])}}
                            </div>
                            <div class="form-group col-sm-3" style="padding:0">
                            {{ Form::text('', $component->code,['class' => 'form-control']) }}
                            </div>
                            <div class="form-group col-sm-3">
                                {{ Form::label('value', 'Value ',['class' => 'col-md-4','style' => 'padding-top:2%'])}}
                            </div>  
                            <div class="col-sm-3" style="padding:0">
                                    {{ Form::text("$component->id[value]", '',['class' => 'form-control']) }}
                            </div>
                        </div>
                        @endforeach
                    @endif

					<!-- <div id="form-control">
                    <div class="form-group col-md-5">
                    	{{ Form::label('component', 'Component :',['class' => 'col-md-4','style' => 'padding-top:2%','align' => 'left'])}}
						<div class="col-md-8" style="padding:0">
                        {{ Form::select('component_id', $appraisalComponentTypes->pluck('code','id'),null, array('class' => 'select1' ,'placeholder' => 'Select A Component'));}}
						</div>
					</div>
					<div class="form-group col-md-5">
                    {{ Form::label('value', 'Value :',['class' => 'col-md-4','style' => 'padding-top:2%'])}}
						<div class="col-md-8" style="padding:0">
                    		{{ Form::text('value', '',['class' => 'form-control']) }}
						</div>
                    </div>
                    </div> -->
					<div class="col-md-12">
					{{ Form::submit('Save',['class' => 'btn btn-success','style' => 'display: block; margin: 0 auto']) }}
					</div>
                    {{ Form::close() }}
				</div>
				</div>
				
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function () {
		$('.select1').select2({
			placeholder: 'Select a user',
			allowClear:true
		});

	});
   
</script>

@endsection
