@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
		<div class="row">
            <div class="col-sm-9">
				<h1 class="admin-page-title">Appraisal </h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="/admin/appraisal">Appraisal</a></li>
                    <li class="active">Show</a></li>
					<li > {{$appraisal->id}}</li>


                </ol>
			</div>
			<div class="col-sm-3">
                {{Form::open(['url'=>'admin/appraisal/'.$appraisal->id.'/edit' ,'method'=>'get'])}}
                {{Form::button('<i class="fa fa-edit fa-fw btn-icon-space"></i>  Edit Appraisal', array('type' => 'submit', 'class' => 'btn btn-warning'));}}
                {{Form::close()}}
            </div>
		
        </div>
	</div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
	<div class="row">
         <div class="col-sm-12">
            <div class="row">
               	<div class="col-sm-4">
					<div>
					<div class="panel panel-default">
						<div class="info-table">
							<label>Employee Name </label>
							<span>{{$appraisal->user->name}}</span>
						</div>
						<div class="info-table">
							<label>Employee Id </label>
							<span>{{$appraisal->user->employee_id}}</span>
						</div>
						<div class="info-table">
							<label>Joining Date </label>
							<span>{{date_in_view($appraisal->user->joining_date)}}</span>
						</div>
						<div class="info-table">
							<label>Confirmation Date </label>
							<span>{{date_in_view($appraisal->user->confirmation_date)}}</span>
						</div>
						<div class="info-table">
							<label>Gender </label>
							<span>{{$appraisal->user->gender}}</span>
						</div>
						<div class="info-table">
							<label>Pan </label>
							<span>{{$appraisal->user->pan}}</span>
						</div>
					</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div>
					<div class="panel panel-default">
						<div class="info-table">
							<label>Effective Date </label>
							<span>{{date_in_view($appraisal->effective_date)}}</span>
						</div>
						<div class="info-table">
							<label>Appraisal Type </label>
							<span>{{$appraisal->type->name}}</span>
						</div>
						<div class="info-table">
							<label>Monthly Gross Salary</label>
							<span>{{round($appraisal->getMonthlyGrossSalary(),2)}}</span>
						</div>
						<div class="info-table">
							<label>Monthly CTC </label>
							<span>{{$appraisal->getMonthlyCtc()}}</span>
						</div>
						<div class="info-table">
							<label>Monthly Gross Earnings </label>
							<span>{{$appraisal->getMonthlyGrossEarnings()}}</span>
						</div>
						
		
					</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div>
					<div class="panel panel-default">
						<div class="info-table">
							<label>Special Allowance </label>
							<span>{{$appraisal->getSpecialAllowance()}}</span>
						</div>
						<div class="info-table">
							<label>Special Allowance Internal</label>
							<span>{{$appraisal->getSpecialAllowanceInternal()}}</span>
						</div>
						<div class="info-table">
							<label>Annual Gross Salary</label>
							<span>{{$appraisal->getAnnualGrossSalary()}}</span>
						</div>
						<div class="info-table">
							<label>Annual CTC</label>
							<span>{{$appraisal->getAnnualCtc()}}</span>
						</div>
						<div class="info-table">
							<label>Annual Gross Earnings </label>
							<span>{{$appraisal->getAnnualGrossEarnings()}}</span>
						</div>
						
						
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
						<div class="row">
						<div class="col-md-6">
						<div class="panel panel-heading">
						<div class="table-responsive-sm">
						<table class="table table-bordered">
						<caption><strong>Part A : Fixed Salary Details</strong></caption>
						<thead>
							<tr class="active">
							<th width="60%" class="text-center"><strong>Particulares</strong></th>
							<th width="20%" class="text-center"><strong>Annual</strong></th>
							<th width="20%" class="text-center"><strong>Monthly</strong></th>
							</tr>
						</thead>
						<tbody>
							@if($appraisal->appraisalComponent)
							@php
							$fixedSalary = 0;
							@endphp
							@foreach($appraisal->appraisalComponent as $component)
							@if($component->value > 0 && $component->appraisalComponentType->is_computed==0)
								<tr>
									<td>{{$component->appraisalComponentType->name}}  </td>
									<td>{{$component->value * 12}}</td>
									<td>{{round($component->value,2)}}</td>
								</tr>
								@php
								$fixedSalary += ($component->value * 12);
								@endphp
							@endif
							@endforeach
							<tr class="active">
								<td class="text-right"><strong>Gross Salary </strong></td>
								<td>{{$fixedSalary}}</td>
								<td>{{round($fixedSalary/12,2)}}</td>
							</tr>

							<tr>
								<td colspan="3"><strong>Other Salary Benefits: </strong></td>
					
							</tr>
							@php
							$otherBenefits = 0;
							@endphp
							@foreach($appraisal->appraisalComponent as $component)
								@if($component->appraisalComponentType->code == "pf-employeer" || $component->appraisalComponentType->code == "pf-other" || 
							$component->appraisalComponentType->code == "esi-employeer")
									@if($component->value < 0)
										<tr>
											<td>{{$component->appraisalComponentType->name}}  </td>
											<td>{{abs($component->value * 12)}}</td>
											<td>{{abs(round($component->value,2) )}}</td>
										</tr>
										@php
										$otherBenefits += abs($component->value * 12);
										@endphp
									@endif
								@endif
							@endforeach
							<tr class="active">
								<td class="text-right">Total Other Benefits </td>
								<td>{{$otherBenefits}}</td>
								<td>{{round($otherBenefits/12,2)}}</td>
							</tr>
							<tr class="active">
								<td class="text-right">Total Fixed CTC - (A) </td>
								<td>{{$fixedSalary + $otherBenefits}}</td>
								<td>{{($fixedSalary+ $otherBenefits)/12}}</td>
							
							</tr>
							@endif
						</tbody>
						</table>
						</div>
						</div>
						</div>
						
						<div class="col-md-6">
						<div class="panel panel-heading">
						<div class="table-responsive-sm">
						<table class="table table-bordered">
						<caption><strong>Part B : Variable Bonus Details</strong></caption>
						<thead>
							<tr class="active">
							<th width="40%" class="text-center"><strong>Particulares</strong></th>
							<th width="20%" class="text-center"><strong>Annual</strong></th>
							<th width="20%" class="text-center"><strong>Monthly</strong></th>
							<th width="20%" class="text-center"><strong>Date</strong></th>
							</tr>
						</thead>
						<tbody>
							@if($bonusTypes)
							@php
							$variableBonus = 0;
							@endphp
							@foreach($bonusTypes as $index => $bonusType)
                                <tr>
									<td><label>{{$bonusType['description']}} </label></td>
                                    <td>{{$appraisal->appraisalBonus->where('appraisal_bonus_type_id',$bonusType->id)->first()->value?$appraisal->appraisalBonus->where('appraisal_bonus_type_id',$bonusType->id)->first()->value:"-"}}</td>
									<td>{{$appraisal->appraisalBonus->where('appraisal_bonus_type_id',$bonusType->id)->first()->value?round($appraisal->appraisalBonus->where('appraisal_bonus_type_id',$bonusType->id)->first()->value/12,2):"-"}}</td>
									<td><small>{{$appraisal->appraisalBonus->where('appraisal_bonus_type_id',$bonusType->id)->first()->value_date ? date_in_view($appraisal->appraisalBonus->where('appraisal_bonus_type_id',$bonusType->id)->first()->value_date):"-"}}<small></td>
                                </tr>
								@php
								$variableBonus += ($appraisal->appraisalBonus[$index]['value']);
								@endphp
							@endforeach
							 		<tr class="active">
                                        <td class="text-right"><label><strong>Total Variabe CTC -(B)</strong></label></td>
                                        <td>{{ $variableBonus}}</td>
										<td>{{ round($variableBonus/12,2)}}</td>
										<td></td>
                                    </tr>
									<tr class="active">
                                        <td class="text-right"><label><strong>Total CTC for the Year - (A+B)</strong></label></td>
                                        <td>{{$fixedSalary + $otherBenefits + $variableBonus}}</td>
										<td>{{round(($fixedSalary + $otherBenefits + $variableBonus)/12, 2)}}</td>
										<td></td>
                                    </tr>
							@endif
						</tbody>
						</table>
						</div>

		</div>
	</div>
	</div>
</div>

<script type="text/javascript">
 $(document).ready(function() {
        $('#effectiveDate').datetimepicker({
            format: 'YYYY-MM-DD'
        });
		$('.userSelect1').select2({
			placeholder: 'Select a user',
			allowClear:true
		});
		$('.selectid4').select2({
			placeholder: 'Select a user',
			allowClear:true
		}); 
    });
</script>

@endsection
