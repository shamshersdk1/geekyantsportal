@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
		<div class="row">
            <div class="col-sm-10">
				<h1 class="admin-page-title">Appraisal </h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
					<li class="active">Appraisal</li>

                </ol>
            </div>
            <div class="col-sm-2">
                {{Form::open(['url'=>'admin/appraisal/add' ,'method'=>'get'])}}
                    {{Form::button('<i class="fa fa-plus fa-fw"></i>  Add Appraisal', array('type' => 'submit', 'class' => 'btn btn-success'));}}
                {{Form::close()}}
            </div>
		
        </div>
    </div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
    <div class="row">
    	<div class="panel panel-default">
			<table class="table table-striped" id="all-appraisals">
				<thead>
					<th width="5%">#</th>
					<th widht="10%">Employee Id</th>
					<th width="20%">User Name</th>
                    <th width="10%">Appraisal Type</th>
                    <th width="10%">Effective Date</th>
					<th width="10%">Annual Gross</th>
					<th width="10%">CTC</th>
                    <th width="10%">Created At</th>
                    <th width="25%" class="text-right"> Actions</th>

				</thead>
				<tbody>
					@if(count($appraisals)>0)
						@foreach($appraisals as $appraisal)
						<tr>
							<td>{{$appraisal->id}}</td>
							<td>{{$appraisal->user ? $appraisal->user->employee_id : ''}}</td>
							<td>{{$appraisal->user ? $appraisal->user->name : ''}}</td>
							<td>{{$appraisal->type ? $appraisal->type->name : ''}}</td>
                            <td>{{date_in_view($appraisal->effective_date)}}</td>
							<td>&#8377; {{$appraisal->getAnnualGrossSalary() ? $appraisal->getAnnualGrossSalary() : ''}}</td>
							<td>&#8377; {{$appraisal->getAnnualCtc() ? $appraisal->getAnnualCtc() : ''}}</td>
                            <td>{{datetime_in_view($appraisal->created_at)}}</td>
							
							<td class="text-right">
								<a href="appraisal/{{$appraisal->id}}" class="btn btn-warning btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View</a>
							<a href="appraisal/{{$appraisal->id}}/edit" class="btn btn-info btn-sm"><i class="fa fa-pencil btn-icon-space" aria-hidden="true"></i>Edit</a>
							<div style="display:inline-block;" class="deleteform">
                            	{{ Form::open(['url' => '/admin/appraisal/'.$appraisal->id, 'method' => 'delete']) }}
							    {{Form::button('<i class="fa fa-trash fa-fw btn-icon-space"></i>  Delete', array('type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick' => 'return confirm("Are you sure you want to delete this item?")'));}}
                                {{ Form::close() }}
                            </div>
							</td>
						</tr>
						@endforeach
					@else
						<td colspan="5" style="text-align:center;"><span class="align-center"><big>No Bonus Type</big></span></td>
					@endif
				</tbody>
			</table>
		</div>
    </div>
</div>
<script>
 $(document).ready(function() {
	 $tableHeight = $( window ).height();
		var t = $('#all-appraisals').DataTable( {
            pageLength:500, 
            "ordering": false,
			scrollY: $tableHeight - 200,
			scrollX: true,
			scrollCollapse: true,
			fixedColumns:   {
				leftColumns: 3
			}
            
        } );
});
</script>
@endsection