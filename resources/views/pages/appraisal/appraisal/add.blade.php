@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
		<div class="row">
            <div class="col-sm-10">
				<h1 class="admin-page-title">Appraisal </h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="/admin/appraisal">Appraisal</a></li>
					<li class="active"> Add </li>

                </ol>
			</div>
		
        </div>
	</div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-heading">
				<div class="panel-body">
				    {{ Form::open(array('url' => "admin/appraisal/add")) }}
                        <div class="row">
							<div class="form-group col-md-6">
								{{ Form::Label('user', 'User Name:',array('class' =>'col-md-4' , 'style' =>'padding:3%')) }}
								<div class="col-md-8 col-sm-12 col-xs-12" style="padding:0">
									{{ Form::select('user_id', $users->pluck('name','id'),null, array('class' => 'userSelect1'  ,'placeholder' => 'Select User'));}}
								</div>
							</div>
                    
							<div class="form-group col-md-6">
								{{ Form::label('account', 'Appraisal Type:', ['class' => 'col-md-4', 'style' => 'padding-top:2%', 'align' => 'left']) }}
								<div class="col-md-8 col-sm-12 col-xs-12" style="padding:0px">
									{{ Form::select('type_id', $appraisalTypes->pluck('name','id'),null, array('class' => 'userSelect1'  ,'placeholder' => 'Select Appraisal Type'));}}
								</div>
							</div>	
						</div>
					<div class="row">
							<div class="form-group col-md-6">
								{{ Form::Label('effectiveDate', 'Effective Date:',array('class' =>'col-md-4' , 'style' =>'padding:3%')) }}
								<div class=' input-group date' id='effectiveDate'>
						
									<input type='text' id="datetimeinputstart" class="form-control" autocomplete="off" name="effective_date"  placeholder="Effective Date" />
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
							</div>
						</div>
						<div class="row">
							<h4 style="margin:25px">Set Appraisal Components</h4>
					
							<table>		
							@if(count($appraisalComponentTypes)>0)
								@foreach($appraisalComponentTypes as $component)
								@if($component->is_computed == 0)
								<tr>
									<td>
										<div class="form-group col-sm-3" style="padding:0;margin-left:35px">
											{{ Form::label('', $component->name,['style'=>'padding:8px'] )}}
										</div>
									</td>
									<td>
										<div class="form-group col-sm-3" style="padding:0;width:200px">
											{{ Form::text("component[$component->id]", '',['class' => 'form-control']) }}
										</div>
									</td>
								</tr>
								@endif
								@endforeach
							@endif
							</table>
						</div>

							<div class="row">
								<h4 style="margin:25px">Set Appraisal Bonus</h4>
								@if(count($appraisalBonusType)>0)
									@foreach($appraisalBonusType as $bonusType)
									<div class="row">
										<div class="form-group col-sm-1" style="padding:0;margin-right:20px;margin-left:35px">
										{{ Form::label('', $bonusType->description,['style'=>'padding:8px'] )}}
										</div>
							
										<div class="form-group col-sm-2" style="margin-right:20px">
												{{ Form::text("bonus[$bonusType->id][value]", '',['class' => 'form-control']) }}
										</div>
										
										{{Form::date("bonus[$bonusType->id][value_date]",'Enter Value date')}}
									</div>

									@endforeach
							
								@endif
							</div>

                        <div class="form-group col-md-12">
							{{Form::submit('Save',array('class' => 'btn btn-success ','style'=>'display:block;color:white;margin:auto'))}}
							{{ Form::close() }}
				        </div>
						</div>

			
            
	</div>
</div>

<script type="text/javascript">
 $(document).ready(function() {
        $('#effectiveDate').datetimepicker({
            format: 'YYYY-MM-DD'
		});
		$('#valueDate').datetimepicker({
            format: 'YYYY-MM-DD'
        });
		$('.userSelect1').select2({
			placeholder: 'Select a user',
			allowClear:true
		});
		$('.selectid4').select2({
			placeholder: 'Select a user',
			allowClear:true
		}); 
    });
</script>

@endsection
