@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
	<div class="row margin-top">
		<div class="col-md-5">
			<p class="lead">Whats New!</p>
			<div class="panel panel-default">
				<ul class="bs-callout bs-callout-info">
				  	<li>User Profile Join date and confirmation date added</li>
					<li>Admin Leave Confirmation implemented</li>
					<li>Project Quotation</li>
					<li>Small refactors</li>
					<li>For Global User My Sprint created</li>
				</ul>
			</div>
		</div>
		<div class="col-md-7">
			<p class="lead">Change Log History</p>
			<div class="heading-list">03 Oct 2016</div>
		  	<ul class="list-group">
		  		<li class="list-group-item">
		  			User Profile Join date and confirmation date added 
		  			- <span class="label label-primary pull-right">Sachin</span>
		  		</li>
		  		<li class="list-group-item">
		  			Admin Leave Confirmation implemented
		  			- <span class="label label-primary pull-right">Ila</span>
		  		</li>
				<li class="list-group-item">
					Project Quotation page 
					- <span class="label label-primary pull-right">Abhishek</span>
				</li>
				<li class="list-group-item">
					Small refactors
				</li>
				<li class="list-group-item">
					For Global User My Sprint created 
					<span class="label label-primary pull-right"> - Ila</span>
				</li>
			</ul>

			<div class="heading-list bg-warning">Old Logs</div>
				<ul class="list-group">
					<li  class="list-group-item">Basic Version 1.0 Released</li>
				</ul>
			</div>
		</div>
	</div>
</div>
@endsection