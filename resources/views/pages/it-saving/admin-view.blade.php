@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">IT Saving</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="/admin/it-saving">IT Saving</a></li>
                    <li class="active">{{month_year_in_view($financialYear->start_date)}} - {{month_year_in_view($financialYear->end_date)}}</li>
                    
                </ol>
            </div>

            
            
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
    	@if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span><br/>
                  @endforeach
            </div>
        @endif
        @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
        @endif
        <div class="row">
            <div class="pull-right" style="padding:5px;">
            @if(($currentFinancialYear < date('Y') && (count($itSavings) > 0)) || (!(count($itSavings) > 0)))
                <a href="{{$financialYear->id}}/create" class="btn btn-success"><i class="fa fa-plus btn-icon-space" aria-hidden="true"></i>Add</a>
            @endif
            </div>
        </div>
    <div class="user-list-view">
        <div class="panel panel-default">
            <table class="table table-striped" id="it-saving-table">
                <thead>
                    <th>#</th>
                    <th>User Name</th>
                    <th>Employee ID</th>
                    <th>Email</th>
                    <th>Status</th>
                    <th>Updated At</th>
                    <th>Action</th>
                </thead>
                <tbody>
                
                @if(count($itSavings) > 0 )
                    @foreach($itSavings as $index => $item)
                        <tr>
                            <td class="td-text">{{$index+1}}</td>
                             <td class="td-text">{{$item->user->name}}</td>
                             <td class="td-text">{{$item->user->employee_id}}</td>
                             <td class="td-text">{{$item->user->email}}</td>
                             <td class="td-text">
                                @if($item->status == 'completed')
                                    <span class="label label-success">Completed</span>
                                @else
                                    <span class="label label-danger">Pending</span>
                                @endif
                                </td>
                            <td>{{datetime_in_view($item->updated_at)}}</td>
                            <td class="td-text">
                                <a href="{{$financialYear->id}}/{{$item->user->id}}" class="btn btn-primary  btn-sm">View Submit</a>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="7" class="text-center">No Records</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
	</div>
</div>
<script>
    $(document).ready(function() {
        $tableHeight = $( window ).height();
        var t = $('#it-saving-table').DataTable( {
            pageLength:500, 
            scrollY: $tableHeight - 200,
            scrollX: true,
            scrollCollapse: true,
            "columnDefs": [ {
                "searchable": false,
                "orderable": false,
                "targets": 0
            } ],
            "order": [[ 2, 'asc' ]],
            fixedHeader: true
        } );
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    });

</script>
@endsection
