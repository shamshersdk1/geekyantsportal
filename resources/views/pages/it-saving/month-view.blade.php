@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">IT Saving Month</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="/admin/it-saving/month">IT Saving Month</a></li>
                    <li class="active">{{$monthObj ? $monthObj->formatMonth() : " "}}</li>
                </ol>
            </div>

            <div class="col-sm-2 pull-right" style="background-color: white;padding:0%">
                @if(isset($months))
                    <select id="selectid2" name="user"  placeholder= "{{$monthObj ? $monthObj->formatMonth() : "Select Month"}}">
                        <option value=""></option>
                        @foreach($months as $x)
                            <option value="{{$x->id}}" >{{$x->formatMonth()}}</option>
                        @endforeach
                    </select>
                @endif
            </div>
            
        </div>
    </div>
    @if(!$monthObj->itSavingMonthSetting || ($monthObj->itSavingMonthSetting &&  $monthObj->itSavingMonthSetting->value == 'open'))
        <div class="pull-right">
            <span>
                <a href="/admin/it-saving/month/{{$monthObj->id}}/regenerate" class="btn btn-warning"><i class="fa fa-gear fa-fw"></i>Regenerate</a>
            </span>
        </div>
        <div class="pull-right">
            <span>
                <a href="/admin/it-saving/month/{{$monthObj->id}}/status-month" class="btn btn-danger"><i class="fa fa-lock fa-fw"></i>Lock</a>
            </span>
        </div>
    @endif
    @if($monthObj && $monthObj->itSavingMonthSetting &&  $monthObj->itSavingMonthSetting->value == 'locked')
            <div class="pull pull-right">
                <span class="label label-primary">Locked at {{ $monthObj->itSavingMonthSetting ? datetime_in_view($monthObj->itSavingMonthSetting->created_at) : ''}}</span>
            </div>
        @endif

    <div class="row">
        <div class="col-md-12">
    	@if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span><br/>
                  @endforeach
            </div>
        @endif
        @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
        @endif
    <div class="user-list-view">
        <div class="panel panel-default">
            <table class="table table-striped" id="it-saving-table">
                <thead>
                    <th class="text-center">#</th>
                    <th class="text-center">User Name</th>
                    <th class="text-center">Employee ID</th>
                    <th class="text-center">Email</th>
                    @if(count($ItSavingObjMonths) > 0)
                    <th class="text-center">Total Investments</th>
                    <th class="text-center">Total Deductions</th>
                        @if($columns)
                            @foreach($columns as $column)
                            <th class="text-center">{{ucfirst(str_replace("_"," ",$column->COLUMN_NAME))}}</th>
                            @endforeach
                        @endif
                    @endif
                </thead>
                <tbody>
                @if(count($ItSavingObjMonths) > 0)
                    @foreach($ItSavingObjMonths as $index => $item)
                        <tr>
                            <td class="text-center">{{$index+1}}</td>
                             <td class="text-left">{{$item->user->name}}</td>
                             <td class="text-center">{{$item->user->employee_id}}</td>
                             <td class="text-left">{{$item->user->email}}</td>
                            @if(count($columns)>0)
                            <td class="text-center">{{round($item->total_investments,2)}}</td>
                              <td class="text-center">{{round($item->total_deductions,2)}}</td>
                                @foreach($columns as $column)
                                @if($column->COLUMN_NAME == 'other_multiple_investments')
                                    <td class="text-center">{{$item->others->where('type',"other_multiple_investments")->sum('value')}}</td>
                                @elseif($column->COLUMN_NAME == 'other_multiple_deductions')
                                    <td class="text-center">{{$item->others->where('type',"other_multiple_deductions")->sum('value')}}</td>
                                @elseif($column->COLUMN_NAME == 'agree')
                                    @if($item[$column->COLUMN_NAME] == 1)
                                      <td class="text-center">  <span class="label label-success">Yes</span></td>
                                    @else
                                       <td class="text-center">  <span class="label label-danger">No</span></td>
                                    @endif
                                @else
                                <td class="text-center">{{$item[$column->COLUMN_NAME] ? $item[$column->COLUMN_NAME] : '-'}}</td>
                                @endif
                                @endforeach
                            @endif
                    
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="7" class="text-center">No Records</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
	</div>
</div>
<script>
    $(document).ready(function() {
        var t = $('#it-saving-table').DataTable( {
            pageLength:500, 
			fixedHeader: true,
			"columnDefs": [ {
				"searchable": true,
                "orderable": true,
            } ],
			scrollY:        true,
            scrollX:        true,
            paging:         false,
            scrollCollapse: true,
            fixedColumns:   {
                leftColumns: 3,
            },
        } );
    });

$('#selectid2').change(function(){
        console.log('here');
        var optionSelected = $("option:selected", this);
        optionValue = this.value;
        console.log(optionValue);
        if (optionValue) { 
            window.location = "/admin/it-saving/month/"+optionValue; 
        }
    });
</script>
@endsection
