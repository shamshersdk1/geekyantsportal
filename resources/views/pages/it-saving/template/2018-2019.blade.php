@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">IT Saving</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="/admin/it-saving">IT Saving</a></li>
                    <li class="active">Update</li>
                    <li class="active">{{$user->id}}</li>
                </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <buton type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button> 
            </div>
        </div>
    </div>
    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif
    @if ($isLocked == true) 
        @if((Auth::user()->role == 'admin' || Auth::user()->hasRole('account')))
            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>Month Settings has been locked.</span><br/>
            </div>
        @else
            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>This information can only be modified between 1st and 20th of every month. Form is locked for the month.</span><br/>
            </div>
        @endif
    @elseif(!(Auth::user()->hasRole('admin') || Auth::user()->hasRole('account')))
        <div class="alert alert-warning">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <span>This information cannot be modified past 20th of the month.</span><br/>
        </div>
    @endif
    
    <div>
        <div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="info-table">
                            <label>Name: </label>
                            <span>{{$user->name}} {{$user->employee_id}}</span>
                        </div>
                        <div class="info-table">
                            <label>Designation: </label>
                            <span>{{$user->designation}}</span>
                        </div>
                        <div class="info-table">
                            <label>DOJ: </label>
                            <span>{{date_in_view($user->joining_date)}}</span>
                        </div>
                        <div class="info-table">
                            <label>Email: </label>
                            <span>{{$user->email}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="info-table">
                            <label>PAN No: </label>
                            <span>{{$user->pan}}</span>
                        </div>
                        <div class="info-table">
                            <label>Gender: </label>
                            <span>{{$user->gender}}</span>
                        </div>
                        <div class="info-table">
                            <label>DOB: </label>
                            <span>{{$user->dob}}</span>
                        </div>
                        <div class="info-table">
                            <label>Contact No.: </label>
                            @if($user->user_contact)
                            <span>{{$user->user_contact->mobile}}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <form class="form-horizontal" method="post" @if($ItSavingObj) action="/admin/it-saving/{{$ItSavingObj->id}}" @else action="/admin/it-saving" @endif enctype="multipart/form-data">
    @if($ItSavingObj)
        <input type="hidden" name="_method" value="PUT"> 
    @endif
    <input type="hidden" name="financial_year_id" value="{{$financialYear->id}}">
    <input type="hidden" name="user_id" value="{{$user->id}}">
    <h4 class="table-heading-it-saving">HRA: Rent paid per month</h4>
    <div class="panel panel-default m-b-lg">
        <table class="table table-striped table-condensed">
            <tr>
                <th colspan="2" width="40%"></th>
                <th width="15%" class="text-center">Rs.</th>
                <th width="45%">Proof Document Required</th>
            </tr>
            <tr>
                <td width="5%"> 1.</td>
                <td width="35%">Rent (per month)</td>
                <td width="15%">
                    <input type="number" @if($isLocked) disabled @endif
                     name="rent_monthly" class="form-control small-width" 
                        @if($ItSavingObj) value={{$ItSavingObj->rent_monthly}} @endif
                        >
                </td>
                <td width="45%"></td>
            </tr>
            <tr>
                <td>2.</td>
                <td>Total Rent Paid (per annum)</td>
                <td><input type="number" @if($isLocked) disabled @endif name="rent_yearly" class="form-control small-width"
                        @if($ItSavingObj) value={{$ItSavingObj->rent_yearly}} @endif>
                </td>
                <td>
                    Original Rent Receipts every month (with Revenue Stamp above Rs. 4999/-) or Rent Agreement. Receipt should contain <strong>PAN of Landlord if Rent for the year exceeds Rs. 1 Lakh</strong>.
                </td>
            </tr>
        </table>
    </div>
    <h4 class="table-heading-it-saving">INVESTMENTS U/S 80C, capped at Rs 1.5 Lac</h4>
    <div class="panel panel-default m-b-lg">
        <table class="table table-striped table-condensed">
            <tr>
                <td width="5%">1.</td>
                <td width="35%">Provident Fund (PF)</td>
                <td width="15%">
                    <input type="number" @if($isLocked) disabled @endif  name="pf" class="form-control small-width"
                     @if($ItSavingObj) value={{$ItSavingObj->pf}} @endif>
                </td>
                <td width="45%">No proof document required</td>
            </tr>
            <tr>
                <td>2.</td>
                <td>Employees Contribution under New Pension Scheme 80CCD(1)</td>
                <td class="text-center">
                    <input type="number" @if($isLocked) disabled @endif name="pension_scheme_1" class="form-control small-width"
                     @if($ItSavingObj) value={{$ItSavingObj->pension_scheme_1}} @endif>
                </td>
                <td>No proof document required</td>
            </tr>
            <tr>
                <td>3.</td>
                <td>Employees Contribution under New Pension Scheme 80CCD(1B) (Max Rs.50000.00 In Addition to u/s 80C)</td>
                <td class="text-center">
                    <input type="number" @if($isLocked) disabled @endif name="pension_scheme_1b" class="form-control small-width"
                     @if($ItSavingObj) value={{$ItSavingObj->pension_scheme_1b}} @endif>
                </td>
                <td>No proof document required</td>
            </tr>
            <tr>
                <td>4.</td>
                <td>Public Provident Fund (PPF)</td>
                <td class="text-center">
                    <input type="number" @if($isLocked) disabled @endif name="ppf" class="form-control small-width"
                     @if($ItSavingObj) value={{$ItSavingObj->ppf}} @endif>
                </td>
                <td>Can claim if Paid for Self, Spouse or Children. Receipts / Statement is required as proof.</td>
            </tr>
            <tr>
                <td>5.</td>
                <td>Contribution to Certain Pension Funds</td>
                <td class="text-center">
                    <input type="number" @if($isLocked) disabled @endif name="central_pension_fund" class="form-control small-width"
                     @if($ItSavingObj) value={{$ItSavingObj->central_pension_fund}} @endif>
                </td>
                <td>Certificate</td>
            </tr>
            <tr>
                <td>6.</td>
                <td>Housing Loan Repayment Principal (payable in F.Y. 2018-19)</td>
                <td class="text-center">
                    <input type="number" @if($isLocked) disabled @endif name="lic" class="form-control small-width"
                     @if($ItSavingObj) value={{$ItSavingObj->lic}} @endif>
                </td>
                <td>As per Provisional Bank Certificate</td>
            </tr>
            <tr>
                <td>7.</td>
                <td>Insurance Premium (LIC)</td>
                <td class="text-center">
                    <input type="number" @if($isLocked) disabled @endif name="housing_loan_repayment" class="form-control small-width"
                     @if($ItSavingObj) value={{$ItSavingObj->housing_loan_repayment}} @endif>
                </td>
                <td>Can claim if Paid for Self, Spouse or Children. Receipts / Statement is required as proof.</td>
            </tr>
            <tr>
                <td>8.</td>
                <td>Allowable Term Deposit / Fixed Deposit with Schedule Bank for 5 years & above</td>
                <td class="text-center">
                    <input type="number" @if($isLocked) disabled @endif name="term_deposit" class="form-control small-width"
                     @if($ItSavingObj) value={{$ItSavingObj->term_deposit}} @endif>
                </td>
                <td>Certificate</td>
            </tr>
            <tr>
                <td>9.</td>
                <td>National Saving Scheme / Certificate</td>
                <td class="text-center">
                    <input type="number" @if($isLocked) disabled @endif name="national_saving_scheme" class="form-control small-width"
                     @if($ItSavingObj) value={{$ItSavingObj->national_saving_scheme}} @endif>
                </td>
                <td>Can claim the amount Paid for Self Only. Certificate is required as prrof</td>
            </tr>
            <tr>
                <td>10.</td>
                <td>Tax Saving Mutual Funds</td>
                <td class="text-center">
                    <input type="number" @if($isLocked) disabled @endif name="tax_saving" class="form-control small-width"
                     @if($ItSavingObj) value={{$ItSavingObj->tax_saving}} @endif>
                </td>
                <td>Can claim the amount Paid for Self Only. Certificate / Acknowledgement is required as proof</td>
            </tr>
            <tr>
                <td>11.</td>
                <td>Children Education Expenses / Tuition Fees</td>
                <td class="text-center">
                    <input type="number" @if($isLocked) disabled @endif name="children_expense" class="form-control small-width"
                     @if($ItSavingObj) value={{$ItSavingObj->children_expense}} @endif>
                </td>
                <td>Payment of School/College fees for Children (Max 2 Children) for Full Time Education only. Certificate / Copy of vouchers</td>
            </tr>
            <tr>
                <td>12.</td>
                <td>Others, (Please specify if any)</td>
                <td class="text-center">
                    <input type="number" @if($isLocked) disabled @endif name="other_investment" class="form-control small-width"
                     @if($ItSavingObj) value={{$ItSavingObj->other_investment}} @endif>
                </td>
                <td>Certificate / Receipts</td>
            </tr>
        </table>
    </div>

    <h4 class="table-heading-it-saving">OTHER PERMITTED DEDUCTIONS (Total Investments U/S 80C - limited to Rs 1,50,000/- only)</h4>
    <div class="panel panel-default m-b-lg">
        <table class="table table-striped table-condensed">
            <tr>
                <td width="5%">1.</td>
                <td width="35%">80D - Medical Insurance Premium (Maximum Rs. 25,000; Rs. 50,000 for senior citizens)</td>
                <td width="15%">
                    <input type="number" @if($isLocked) disabled @endif name="medical_insurance_premium" class="form-control small-width"
                     @if($ItSavingObj) value={{$ItSavingObj->medical_insurance_premium}} @endif>
                </td>
                <td width="45%">The benefit is available to individuals for health insurance premiums paid for self, spouse, children, and parents.<br />Receipt / Statement</td>
            </tr>
            <tr>
                <td>2.</td>
                <td>80DDB - Expenditure on Medical Treatment for specified disease</td>
                <td class="text-center">
                    <input type="number" @if($isLocked) disabled @endif name="medical_treatment_expense" class="form-control small-width"
                     @if($ItSavingObj) value={{$ItSavingObj->medical_treatment_expense}} @endif>
                </td>
                <td>Form 10-I + expense vouchers</td>
            </tr>
            <tr>
                <td>3.</td>
                <td>80E - Repayment of Interest against Educational Loan</td>
                <td class="text-center">
                    <input type="number" @if($isLocked) disabled @endif name="educational_loan" class="form-control small-width"
                     @if($ItSavingObj) value={{$ItSavingObj->educational_loan}} @endif>
                </td>
                <td>Interest paid on Education Loan (Upto 8 years from initial year in which Interest payment started). Bank Certificate required as proof.</td>
            </tr>
            <tr>
                <td>4.</td>
                <td>80G - Donations (only for Prime Minister’s National Relief Fund, the Chief Minister’s Relief Fund or the Lieutenant Governor’s Relief Fund)</td>
                <td class="text-center">
                    <input type="number" @if($isLocked) disabled @endif name="donation" class="form-control small-width"
                     @if($ItSavingObj) value={{$ItSavingObj->donation}} @endif>
                </td>
                <td>Receipts/ 100% only if Proof submitted</td>
            </tr>
            <tr>
                <td>5.</td>
                <td>80GG - Rent Paid but not in Receipt of HRA(Declaration in Form No. 10 BA required</td>
                <td class="text-center">
                    <input type="number" @if($isLocked) disabled @endif name="rent_without_receipt" class="form-control small-width"
                     @if($ItSavingObj) value={{$ItSavingObj->rent_without_receipt}} @endif>
                </td>
                <td>Form No.10BA + Receipts</td>
            </tr>
            <tr>
                <td>6.</td>
                <td>80U-Permanent Physical Disability (Normal Rs. 75000/- and Severe Rs.1,25,000/-)</td>
                <td class="text-center">
                    <input type="number" @if($isLocked) disabled @endif name="physical_disablity" class="form-control small-width"
                     @if($ItSavingObj) value={{$ItSavingObj->physical_disablity}} @endif>
                </td>
                <td>Certificate</td>
            </tr>
            <tr>
                <td>7.</td>
                <td>Others - (Please specify if any)</td>
                <td class="text-center">
                    <input type="number" @if($isLocked) disabled @endif name="other_deduction" class="form-control small-width"
                     @if($ItSavingObj) value={{$ItSavingObj->other_deduction}} @endif>
                </td>
                <td>Related documents</td>
            </tr>
        </table>
    </div>

    <div class="panel panel-default m-b-lg">
        <table class="table">
            <tr>
                <th colspan="2" width="40%" class="text-right"><h4>Total Deductions</h4></th>
                <th width="15%" class="text-center"><h4>0</h4></th>
                <th width="45%"></th>
            </tr>
        </table>
    </div>

    <h4 class="table-heading-it-saving">PREVIOUS EMPLOYMENT SALARY</h4>
    <div class="panel panel-default m-b-lg">
        <table class="table table-striped table-condensed">
            <tr>
                <th>Salary Paid</th>
                <th>TDS (in Rs.)</th>
                <th>Form 16 / Form 12 B from previous employer</th>
            </tr>
            <tr>
                <td><input type="number" @if($isLocked) disabled @endif name="salary_paid" class="form-control" style="width: 100px;"
                 @if($ItSavingObj) value={{$ItSavingObj->salary_paid}} @endif>
                </td>
                <td><input type="number" @if($isLocked) disabled @endif name="tds" class="form-control" style="width: 100px;"
                 @if($ItSavingObj) value={{$ItSavingObj->tds}} @endif>
                </td>
                <td>
                    <label>
                        <input type="text" @if($isLocked) disabled @endif  name="previous_form_16_12b" placeholder="Yes/No"
                         @if($ItSavingObj) @if($ItSavingObj->previous_form_16_12b==1) value="Yes" @else value="No" @endif @endif> &nbsp; I have  Form 16 / Form 12 B from previous employer
                    </label>
                </td>
            </tr>
        </table>
    </div>

    <div class="panel panel-deafault">
        <div class="panel-body">
            <label>
                <input type="checkbox" name="aggree" required> &nbsp;
                I, do hereby declare that the proof of fresh investments will be submitted by 15th February, 2019. Further, incase of any change in above declaration, I would revise it and inform the insitute immediately. I shall indemnify the institute for all cost and consequences if any information is found to be incorrect.
            </label>
        </div>
    </div>

    <div class="panel panel-deafault">
        <div class="panel-body">
            <h5>Notes</h5>
            <ol>
                <li>The date of Investments must be between 01-04-2016 to 31-03-2017</li>
                <li>Deduction u/s 10(13A) for HRA is available only where employees have actually paid the rent.</li>
                <li>Deduction under section 80C+80CCC+80CCD(1) can’t excedds Rs 150,000/- ( Section 80CCE)</li>
                <li>Deduction u/s 80D shall be allowed only if the payment is made by any mode other than cash.</li>
                <li>Maximum Deduction u/s 80D shall be allowed only upto RS 30,000/- in case of parents (Senior Citizen) & RS 25,000/- for Family, himself & parents (Non-Senior Citizen</li>
            </ol>
        </div>
    </div>
    <div class="clearfix m-b text-center">
        <button type="reset" class="btn btn-default ">Cancel</button>
        <button type="submit" class="btn btn-success"  @if($isLocked) disabled @endif>Save</button>
    </div>
    </form>
</div>
@endsection
