@extends('layouts.admin-dashboard')
@section('main-content')
@php
    $userLoggedIn = Auth::check();
    $user = Auth::user();
    $superAdmin = ($user->role == "admin" && $user->isAdmin()) ? true : false;
@endphp

<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Financial Year</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li class="active">IT Savings</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
    	@if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span><br/>
                  @endforeach
            </div>
        @endif
        @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
        @endif  
    <div class="user-list-view">
        <div class="panel panel-default"> 
            <table class="table table-striped">
                <thead>
                    <th>#</th>
                    <th>Financial Year</th>
                    <th>Status</th>
                    <th>Created At</th>
                    <th></th>
                </thead>
                @if(count($financialYear) > 0)
                    @foreach($financialYear as $index => $item)
                        <tr>
                            <td class="td-text">{{$index+1}}</td>
                            <td class="td-text">{{month_year_in_view($item->start_date)}} - {{month_year_in_view($item->end_date)}}</td>
                            <td class="td-text">
                                @if($item->status == 'running')
                                    <span class="label label-success">{{ucfirst($item->status)}}</span>
                                @elseif($item->status == 'completed')
                                    <span class="label label-primary">{{ucfirst($item->status)}}</span>
                                @else
                                    <span class="label label-primary">{{ucfirst($item->status)}}</span>
                                @endif
                            </td>
                            <td class="td-text">{{date_in_view($item->created_at)}}</td>
                            <td class="td-text">
                                @if(!$superAdmin)
                                <a href="it-saving/{{$item->id}}/{{$user->id}}" class="btn btn-primary  btn-sm">View</a>
                                @endif
                                @if($superAdmin)
                                <a href="it-saving/{{$item->id}}" class="btn btn-primary  btn-sm">View</a>
                                 @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="3">No Records</td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
	</div>
</div>
@endsection
