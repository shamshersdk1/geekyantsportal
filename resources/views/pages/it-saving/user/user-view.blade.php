@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">IT Saving</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">User</a></li>
                    <li><a href="/user/it-saving">IT Saving</a></li>
                </ol>
            </div>

            
            
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
    	@if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span><br/>
                  @endforeach
            </div>
        @endif
        @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
        @endif
        <div class="row">
            <div class="pull-right" style="padding:5px;">
            @if(($currentFinancialYear < date('Y') && (count($itSavings) > 0)) || (!(count($itSavings) > 0)))
                <a href="it-saving/create" class="btn btn-success"><i class="fa fa-plus btn-icon-space" aria-hidden="true"></i>Add</a>
            @endif
            </div>
        </div>
    <div class="user-list-view">
        <div class="panel panel-default">
        
            <table class="table table-striped" id="it-saving-table">
                <thead>
                    <th class="td-text">#</th>
                    <th class="td-text">Financial Year</th>
                    <th class="td-text">Agree</th>
                    <th class="td-text">Created At</th>
     
                    
                    <th class="td-text">Updated At</th>
                    <th class="td-text">Status</th>
                    <th class="td-text">Action</th>
                </thead>
                <tbody>
                
                @if(count($itSavings) > 0 )
                    @foreach($itSavings as $index => $item)
                        <tr>
                            <td class="td-text">{{$index+1}}</td>
                             <td class="td-text">Apr,{{$item->financialYear->year}} - Mar,{{($item->financialYear->year)+1}}</td>
                             <td class="td-text">
                                @if($item->agree)
                                    <span class="label label-success">Yes</span>
                                @else
                                    <span class="label label-danger">No</span>
                                @endif
                                </td>
                             <td class="td-text">{{datetime_in_view($item->created_at)}}</td>
                    
                            <td>{{datetime_in_view($item->updated_at)}}</td>
                            <td class="td-text">
                                @if($item->status == "pending")
                                    <span class="label label-warning">Pending</span>
                                @elseif($item->status == "in_progress")
                                    <span class="label label-success">In Progress</span>
                                @elseif($item->status == "completed")
                                    <span class="label label-success">Completed</span>
                                @else
                                    <span class="label label-warning">Pending</span>
                                @endif
                                </td>
                            <td class="td-text">
                                <a href="it-saving/{{$item->id}}" class="btn btn-primary  btn-sm">
                                @if($item->financialYear->is_locked == 1)
                                   View
                                @else
                                    Edit & View
                                @endif
                                </a>
                                <div style="display:inline-block;">
								{{ Form::open(['url' => 'user/it-saving/'.$item->id, 'method' => 'delete']) }}
								{{ Form::submit('Delete',['class' => 'btn btn-danger','onclick' => 'return confirm("Are you sure you want to delete this item?")']) }}
								{{ Form::close() }}
								</div>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="7" class="text-center">No Records</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
	</div>
</div>
<script>
    $(document).ready(function() {
        var t = $('#it-saving-table').DataTable( {
            pageLength:500, 
            "columnDefs": [ {
                "searchable": false,
                "orderable": false,
                "targets": 0
            } ],
            fixedHeader: true
        } );
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    });

</script>
@endsection
