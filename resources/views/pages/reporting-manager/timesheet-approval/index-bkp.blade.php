@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">My Team</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/users') }}">List of Users</a></li>
                </ol>
            </div>
        </div>
    </div>
    <div class="row" ng-app="myApp" ng-controller="rmTimesheetReviewCtrl">
            <div class="col-sm-12">
                <b>Review pending timsheet</b>
                <table class="table table-bordered table-hover bg-white">
                <thead>
                    <tr>
                        <th width="10%">Date</th>
                        <th width="30%">Timelog</th>
                        <th width="5%">Total Hrs</th>
                        <th width="5%">Auto-Approved</th>
                        <th width="30%">Additonal</th>
                    </tr>
                </thead>
                <tbody>

                     @if(!empty($data['timesheet_details']['timesheets']['days']))
                        @foreach($data['timesheet_details']['timesheets']['days'] as $day)
                        <tr @if(!empty($day))
                            @if($day['on_leave'] == 'true')
                            class = "danger"
                            @elseif($day['is_weekend'] == 'true')
                            class = "warning"
                             @elseif($day['is_holiday'] == 'true')
                            class = "info"
                        @endif>
                        <td >
                            {{date_in_view($day['date'])}}
                            @if(isset($day['on_leave']) && $day['on_leave']=='true')
                                <strong>(On Leave)</strong>
                            @endif
                            @if(isset($day['is_weekend']) && $day['is_weekend']=='true')
                                <strong>(Weekend)</strong>
                            @endif
                            @if(isset($day['is_holiday']) && $day['is_holiday']=='true')
                                <strong>(Holiday)</strong>
                            @endif
                        </td>

                        <td>
                            @if(!empty($day['projects']) && count($day['projects']) > 0)
                            @foreach($day['projects'] as $project)
                                <div>
                                    <div>
                                        <strong>{{$project['project_name']}}</strong>
                                    </div>
                                    @foreach($project['user_timesheets'] as $userTimesheeet)
                                        <div class="m-b-10">
                                            @if($userTimesheeet['status'] ==='approved')
                                            <span class="label label-success">Approved</span>&nbsp;
                                            @elseif($userTimesheeet['status'] === 'pending')
                                            <span class="label label-warning">Pending</span>&nbsp;
                                            @endif
                                            <strong>{{$userTimesheeet['duration']}}</strong> {{$userTimesheeet['task']}}

                                        </div>
                                    @endforeach
                                </div>
                            @endforeach
                            @endif
                        </td>
                        <td class="text-center">{{$day['employee_total_time'] ?? 0}}</td>

                            <td class="text-center">
                                @if(!empty($day['employee_total_time']))
                                    {{ $day['employee_total_time'] <= 8 ? $day['employee_total_time']: 8 }}
                                @endif
                            </td>

                        <td class="">
                            <div class="row row-sm">
                                <div class="col-sm-6 pull-right">
                                    @if(isset($day['approve_extra_hour']) && $day['approve_extra_hour'] )
                                        <div class="input-group m-b-5 text-left">

                                            <span class="input-group-addon">
                                                Extra Hrs
                                            </span>

                                                <input class="form-control text-center" disabled="true" type="number" name="additional" value="{{$day['extra_hour']['extra_hours']}}"/>


                                        </div>
                                        <select class="form-control clearfix" disabled="true" value="{{$day['extra_hour']['project_id']}}">
                                            @foreach($day['projects'] as $project)
                                                <option value="{{$project['id']}}">{{$project['project_name']}}</option>
                                            @endforeach
                                        </select>
                                        <label class="m-t-10" >
                                        </label>
                                    @endif
                                    @if(!empty($day['onsite_bonus_request']))
                                        <label class="m-t-10">
                                            Bonus Request :  Onsite
                                            @if(isset($day['onsite_bonus_request']['status']) && $day['onsite_bonus_request']['status'] == 'approved')
                                                <span class="label label-success">Approved</span>&nbsp;
                                            @elseif(isset($day['onsite_bonus_request']['status']) && $day['onsite_bonus_request']['status'] == 'pending')
                                            <span  class="label label-warning">Pending</span>&nbsp;
                                            @endif

                                        </label>
                                    @endif
                                    </div>
                                    @if(!empty($day['bonus_requests']) && count($day['bonus_requests']) > 0)
                                    @foreach($day['bonus_requests'] as $bonusRequest)
                                        <div class="col-sm-6">
                                            @if(!empty($bonusRequest['type']) && $bonusRequest['type'] =='additional')
                                                <div class="well well-sm no-m-b">
                                                    <div class="clearfix m-b-5" >
                                                        Requested for <strong class="pull-right"> [{{$bonusRequest['type']}}]</strong>
                                                    </div>
                                                    <div>
                                                        <textarea disabled="true" class="form-control m-b-5" value="{{$bonusRequest['notes']}}" placeholder="Additional Notes..."></textarea>
                                                    </div>
                                                    @if(!empty($bonusRequest['status']) && $bonusRequest['status'] =='pending')
                                                        <span class="label label-primary">Pending</span>
                                                    @elseif(!empty($bonusRequest['status']) && $bonusRequest['status'] =='approved')
                                                        <span class="label label-success">Approved</span>
                                                    @elseif(!empty($bonusRequest['status']) && $bonusRequest['status'] =='rejected')
                                                        <span class="label label-danger">
                                                        Rejected</span>
                                                    @endif
                                                    @if(!empty($bonusRequest['status']) && $bonusRequest['status'] =='approved')
                                                        <span class="small">by {{$bonusRequest['approved_bonus']['approver']['name']}}
                                                        </span>
                                                    @elseif(!empty($bonusRequest['status']) && $bonusRequest['status'] =='rejected')
                                                        <span class="small">by
                                                        {{$bonusRequest['approved_bonus']['approved_by']['name']}}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            @endif
                                        @endforeach
                                    @endif
                            </div>
                        </td>
                    </tr>
                    @endif
                    @endforeach
                    @endif
                </tbody>
            </table>
            </div>
        </div>
</div>
@endsection