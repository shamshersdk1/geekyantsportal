@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid dashboard-container" ng-app="myApp" ng-controller="rmTimesheetReviewCtrl" >
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Pending Timesheet for Approval</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/users') }}">List of Users</a></li>
                </ol>
            </div>
        </div>
    </div>
    <div>
        <div>
            <b class="text-warning">Note: Timesheet will be auto-approved/auto-locked till last 5 working days if the logged hours less than 9 hr(s) for each day. You will only get the request to approve the timesheet for which there is an Extra Hour Request, Onsite Bonues request or Additional work-day bonus request!</b>
        </div>
        <div class="panel panel-default" ng-repeat="user in data">

            <div class="panel-heading">
                <h4 class="panel-title">
                    %%user.name%%
                    <a target="_blank" href="new-timesheet/review?user=%%user.id%%" class="pull-right">View Full timelog</a>
                </h4>
            </div>
            <table class="table table-bordered table-hover bg-white">
                <thead>
                    <tr>
                        <th width="10%">Date</th>
                        <th width="30%">Timelog</th>
                        <th width="5%">Total Hrs</th>
                        <th width="5%">Auto-Approved</th>
                        <th width="30%">Additonal</th>
                    </tr>
                </thead>

                <tbody>
                    <tr class="primary" ng-repeat="(date, timelog) in user.dates"
                    ng-class="{'danger' : timelog.on_leave == true, 'warning' : timelog.is_weekend ==true, 'info' : timelog.is_holiday ==true}"
                    >
                        <td>
                            <strong>%%date | date:fullDate %%
                            <span ng-if="timelog_on_leave == true"><strong>(On Leave)</strong></span>
                            <span ng-if="timelog.is_weekend == true"><strong>(Weekend)</strong></span>
                            <span ng-if="timelog.is_holiday == true"><strong>(Holiday)</strong></span>
                            </strong>
                        </td>
                        <td>
                            <div ng-repeat="project in timelog.timesheet">
                                <div>
                                    <strong>%%project.name%%</strong>
                                </div>
                                    <div class="m-b-10" ng-repeat="task in project.tasks">
                                        <strong>%%task.duration%%</strong> %%task.task%%
                                    </div>
                            </div>
                        </td>
                        <td class="text-center">%%timelog.total_hours%%</td>
                        <td class="text-center" >%%timelog.total_hours - timelog.extra%%</td>
                        <td class="">
                            <div class="row row-sm">
                                <div class="col-sm-6 pull-right" ng-show="timelog.extra">
                                    <div class="input-group m-b-5 text-left">
                                        <span class="input-group-addon">
                                            Extra Hrs
                                        </span>
                                        <input ng-disabled="timelog.is_processed" class="form-control text-center" type="number" name="extra" ng-model="timelog.extra"/>
                                    </div>
                                    <select ng-disabled="timelog.is_processed" class="form-control clearfix" ng-model="timelog.project_id">
                                        <option value="" ng-repeat="project in timelog.timesheet" ng-value="project.project_id">%%project.name%%</option>
                                    </select>
                                    <label class="m-t-10" >
                                    </label>
                                    <label class="m-t-10">
                                        <span class="label label-primary" ng-show="timelog.status == 'prending'">Pending</span>
                                            <span class="label label-success" ng-show="timelog.status == 'approved'">Approved</span>
                                            <span class="label label-danger" ng-show="timelog.status == 'rejected'">Rejected</span>
                                            <span class="small" ng-show="timelog.status == 'approved' || timelog.status == 'rejected'">by %%timelog.approved_by%%
                                            </span>

                                    </label>
                                    <div class="text-right" ng-hide="timelog.is_processed">
                                            <a href="" class="btn btn-sm btn-default text-danger" ng-click="form.rejectExtraHour($parent.$index, date)" ng-disabled="timelog.extra_loading">Reject</a>
                                            <a href="" class="btn btn-sm btn-success" ng-click="form.approveExtraHour($parent.$index, date)" ng-disabled="timelog.extra_loading">Approve</a>
                                        </div>
                                </div>
                                <div class="col-sm-6" ng-show="timelog.additional_bonus">
                                    <div class="well well-sm no-m-b">
                                        <div class="clearfix m-b-5" >
                                            Requested for <strong class="pull-right"> [additonal]</strong>
                                        </div>
                                        <div>
                                            <textarea ng-disabled="timelog.additional_bonus.status == 'approved' || timelog.additional_bonus.status == 'rejected'" class="form-control m-b-5" ng-model="timelog.additional_bonus.notes" placeholder="Additional Notes..."></textarea>
                                        </div>
                                        <div class="text-right" ng-hide="timelog.additional_bonus.status == 'approved' || timelog.additional_bonus.status == 'rejected' ">
                                            <a href="" class="btn btn-sm btn-default text-danger" ng-click="form.rejectAdditionalBonus($parent.$index, date)" ng-disabled="bonus_request.loading">Reject</a>
                                            <a href="" class="btn btn-sm btn-success" ng-click="form.approveAdditionalBonus($parent.$index, date)" ng-disabled="bonus_request.loading">Apporve</a>
                                        </div>
                                            <span class="label label-primary" ng-show="timelog.additional_bonus.status == 'pending'">Pending</span>
                                            <span class="label label-success" ng-show="timelog.additional_bonus.status == 'approved'">Approved</span>
                                            <span class="label label-danger" ng-show="timelog.additional_bonus.status == 'rejected'">Rejected</span>
                                            <span class="small" ng-show="timelog.additional_bonus.status != 'pending'">by %%timelog.additional_bonus.approver.name%%
                                            </span>
                                            
                                    </div>
                                </div>
                                 <div class="col-sm-6" ng-show="timelog.onsite_bonus">
                                    <div class="well well-sm no-m-b">
                                        <div class="clearfix m-b-5" >
                                            Requested for <strong class="pull-right"> [onsite]</strong>
                                        </div>
                                        <div>
                                            <textarea ng-disabled="timelog.onsite_bonus.status == 'approved' || timelog.onsite_bonus.status == 'rejected'" class="form-control m-b-5" ng-model="timelog.onsite_bonus.notes" placeholder="Additional Notes..."></textarea>
                                        </div>
                                        <div class="text-right" ng-hide="timelog.onsite_bonus.status == 'approved' || timelog.onsite_bonus.status == 'rejected' ">
                                        <a href="" class="btn btn-sm btn-default text-danger" ng-click="form.rejectOnsiteBonus($parent.$index, date)" ng-disabled="timelog.onsite_bonus.loading">Reject</a>
                                            <a href="" class="btn btn-sm btn-success" ng-click="form.approveOnsiteBonus($parent.$index, date)" ng-disabled="timelog.onsite_bonus.loading">Apporve</a>
                                        </div>
                                            <span class="label label-primary" ng-show="timelog.onsite_bonus.status == 'pending'">Pending</span>
                                            <span class="label label-success" ng-show="timelog.onsite_bonus.status == 'approved'">Approved</span>
                                            <span class="label label-danger" ng-show="timelog.onsite_bonus.status == 'rejected'">Rejected</span>
                                            <span class="small" ng-show="timelog.onsite_bonus.status != 'pending'">by %%timelog.onsite_bonus.reviewer.name%%</span>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div ng-show="data.length == 0">
            <b>You have no pending timsheet for approval.</b>
        </div>
    </div>
</div>
@endsection