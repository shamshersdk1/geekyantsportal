@extends(Request::is('*')?'pages.userView.partials.usernav':'pages.userView.partials.nav')
@section('sub-content')
<div class="dashboard-container">
    <div class="user-loan">
            <div class="row">
                <div class="col-sm-6">
                    <h5>Loan Request</h5>
                    <div class="panel panel-default">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <th>Amount</th>
                                <th>EMI</th>
                                <th>Status</th>
                                <th class="text-right">Action</th>
                                @if(count($pendingLoans) > 0)
                                    @foreach($pendingLoans as $loan)
                                        @if($loan->status != 'disbursed')
                                            <tr>
                                                <td class="td-text">
                                                    {{$loan->amount}}
                                                </td>
                                                <td class="td-text">
                                                    {{$loan->emi}}
                                                </td>
                                                <td class="td-text">
                                                    {{$loan->status}}
                                                </td>             
                                            <td class="td-text text-right">
                                            <a href="/admin/loan-applications/{{$loan->id}}" class="btn btn-info crud-btn btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View</a>                                  

                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="4">No results found.</td>
                                    </tr>
                                @endif
                            </table>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                <h5>Approved Loans</h5>
                    <div class="panel panel-default">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <th>Amount</th>
                                <th>EMI</th>
                                <th>Transaction Status</th>
                                <th class="text-right">Action</th>
                                @if(count($loans) > 0)
                                    @foreach($loans as $loan)
                                        <tr>
                                            <td class="td-text">
                                                {{$loan->amount ? $loan->amount :''}}
                                            </td>
                                            <td class="td-text">
                                                {{$loan->emi ? $loan->emi :''}}
                                            </td>
                                            <td class="td-text">
                                                {{$loan->status ? $loan->status : ''}}
                                            </td>
                                            <td class="td-text text-right">
                                                    @if(Request::is('admin/users/*'))
                                                    <a href="/admin/users/{{$user->id}}/loans/{{$loan->id}}" class="btn btn-info crud-btn btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View</a>    
                                                    <form action="/admin/loans/{{$loan->id}}" method="post" style="display:inline-block;" class="deleteform">
                                                        <input name="_method" type="hidden" value="DELETE">
                                                        <button class="btn btn-danger crud-btn btn-sm" type="submit"><i class="fa fa-trash btn-icon-space" aria-hidden="true"></i>Delete</button>
                                                    </form>
                                                    @else
                                                    <a href="/user/loans/{{$loan->id}}" class="btn btn-info crud-btn btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View</a>    
                                                    @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="4">No results found.</td>
                                    </tr>
                                @endif
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection