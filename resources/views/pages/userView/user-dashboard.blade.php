@extends(Request::is('*')?'pages.userView.partials.usernav':'pages.userView.partials.nav')
@section('sub-content')
<script>
   $(function(){
       $('.news-grid,#carousel-left,#carousel-right').hover(
       function () {
           $('#carousel-left').css('opacity','1');
           $('#carousel-right').css('opacity','1');
       },
       function () {
           $('#carousel-left').css('opacity','0');
           $('#carousel-right').css('opacity','0');
       });
   });
</script>
<div class="dashboard-container" ng-app="myApp" ng-controller="employeeLeaveNewCtrl">
   <div class="row">
       <div class="col-md-4">
        <h4 class="admin-section-heading">Team sturcture</h4>
        <div class="panel panel-default">
          <ul class="tree">
              @if($user->reportingManager && $user->reportingManager->reportingManager)
                <li>
                  <input type="checkbox" checked="checked" id="{{$user->id}}" />
                  <label class="tree_label" for="{{$user->id}}">
                      {{$user->reportingManager->reportingManager->name}}
                      <span class="label label-success">RM</span>
                  </label>
                </li>
              @endif
              @if($user->reportingManager)
                <li>
                    <input type="checkbox" checked="checked" id="rm-{{$user->id}}" />
                    <label class="tree_label" for="rm-{{$user->id}}">
                        <img src="{{$user->reportingManager->photo}} " />{{$user->reportingManager->name}}
                        <span class="label label-success">RM</span>
                    </label>
                    <ul>
                      @if($user)
                        <li>
                          <input type="checkbox" id="user-{{$user->id}}" />
                          <label class="tree_label" for="user-{{$user->id}}">
                              <h4>{{$user->name}}</h4>
                          </label>
                          <ul>
                              @foreach($user->reportees as $reportee)
                                  <li>
                                      <span class="tree_label">{{$reportee->name}}</span>
                                  </li>
                              @endforeach
                          </ul>
                        </li>
                      @endif
                    </ul>
                  </li>
              @endif
          </ul>
        </div>

        <div class="clearfix">
          <h4 class="pull-left admin-section-heading">My Projects</h4>
        </div>
        @if(!empty($tlProjects))
            @foreach($tlProjects as $tlProject)
                <div class="panel panel-default">
                    <div class="panel-heading bg-white clearfix">
                        <h4 class="panel-title pull-left">
                            <a href="/admin/project/{{$tlProject['project']->id}}/dashboard" target="_blank">#{{$tlProject['project']->id}}
                            {{$tlProject['project']->project_name}}</a>
                        </h4>
                        <label class="label label-primary custom-label pull-right">Team Lead</label>
                    </div>
                    <table class="table table-condensed">
                    @foreach($tlProject['resources'] as $resource)
                        <tr>
                            <td width="2%"></td>
                            <td width="50%"><label>{{$resource->resource->name}}</label></td>
                            <td width="48%">
                                <small>{{date_in_view($resource->start_date)}}
                                @if($resource->end_date)
                                    - {{date_in_view($resource->end_date)}}
                                @endif
                                </small>
                            </td>
                        </tr>
                    @endforeach
                    </table>
                </div>
            @endforeach
        @else
            <div class="panel panel-default">
                <div class="panel-body text-center">
                    <h4 class="text-muted">No Project found</h4>
                </div>
            </div>
        @endif
        @if(!empty($userProjects))
            @foreach($userProjects as $userProject)
                <div class="panel panel-default">
                    <div class="panel-heading bg-white clearfix">
                        <h4 class="panel-title pull-left"><a href="{{$userProject->project->project_url}} ">
                            {{$userProject->project->id}} {{$userProject->project->project_name}}
                        </a>
                        </h4>
                        <label class="label label-success custom-label pull-right">Developer</label>
                    </div>
                </div>
            @endforeach
        @else
            <div class="panel panel-default">
                <div class="panel-body text-center">
                    <h4 class="text-muted">No Project found</h4>
                </div>
            </div>
        @endif
      </div>
      <div class="col-md-4">
        <h4 class="admin-section-heading">Balance leaves</h4>
        <div class="m-b">
            <!-- <div class="leave-number">
                <div><small>Paid</small><h4>{{$consumedPaidLeaves }}/{{$totalPaidLeaves}}</h4></div>
                <div><small>Sick</small><h4>{{$consumedSickLeaves}}/{{$totalSickLeaves}}</h4></div>
            </div> -->
            <div class="leave-number">
                <div><small>Paid</small><h4>{{$userRemainingLeaveDetail['paid']}}</h4></div>
                <div><small>Sick</small><h4>{{$userRemainingLeaveDetail['sick']}}</h4></div>
                <div><small>Optional</small><h4>{{$userRemainingLeaveDetail['optional-holiday']}}</h4></div>
                <div><small>Marriage</small><h4>{{$userRemainingLeaveDetail['marriage']}}</h4></div>
              </div>
              <div class="leave-number">
                <div><small>Emergency</small><h4>{{$userRemainingLeaveDetail['emergency']}}</h4></div>
                <div><small>Paternity</small><h4>{{$userRemainingLeaveDetail['paternity']}}</h4></div>
                <div><small>Maternity</small><h4>{{$userRemainingLeaveDetail['maternity']}}</h4></div>
                <div><small>Comp Off</small><h4>{{$userRemainingLeaveDetail['comp-off']}}</h4></div>
            </div>
            <div class="btn-group btn-group-justified">
                  <a class="btn btn-warning" ng-click="openModal()">I am sick today</button>
              <a href="/user/leaves" class="btn btn-success">Apply Leave</a>
            </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading orange">My Upcoming Leaves </div>
          <div class="upcoming-leaves-data-block">
            @if(empty($nextLeave))
               <div class="panel-body">
               No Upcoming Leaves
               </div>
            @else
            <div class="panel-body">
              <div>
                <label class="label label-primary custom-label">
                  @if($nextLeave->type=='half')
                    {{ucfirst($nextLeave->type)}} Day Leave
                  @else
                    {{ucfirst($nextLeave->type)}} Leave
                  @endif
                </label>
                {{date('j M Y',strtotime($nextLeave->start_date))}} to {{date('j M Y',strtotime($nextLeave->end_date))}}
              </div>

              <div class="row m-t-5">
                <div class="col-sm-8 m-t-5">
                  <small>
                  Approved by @if(!empty($nextLeave->approver)){{$nextLeave->approver->name}}@endif
                  </small>
                </div>
                <div class="col-sm-4 text-right">
                  <form action="/admin/leave-section/status/{{$nextLeave->id}}" method="post" class="rejectform">
                  <button class="btn btn-danger btn-xs" name="cancel" value="cancel">Cancel</button>
                </form>
                </div>
              </div>
            </div>
            @endif
          </div>
        </div>
      </div>

      <div class="col-md-4">
        <div class="panel panel-default grid-item hidden">
          <div class="panel-heading purpal">
             Upcoming Events
             <span class="pull-right">
             @if(count($events)>0)
             <a href="my-events">View All</a>
             @endif
             </span>
          </div>
          @if(count($events)==0)
          <div class="panel-body">
             No Upcoming Events
          </div>
          @else
          <table class="table table-striped" style="flex:1">

             <tbody>
                @foreach($events as $event)
                <tr>
                   <td>
                      {{$event->title}}
                   </td>
                   <td class="text-right">
                      <small style="display:block">{{date('j M',strtotime($event->date))}}</small>
                      at <small>{{date('g:i a',strtotime($event->time))}}</small>
                   </td>
                </tr>
                @endforeach
                @endif
             </tbody>
          </table>
        </div>

        @if(!empty($news)&&count($news)>0)
        <div class="panel panel-default hidden">
           <div class="panel-heading purpal">News</div>
           <table class="table table-striped">
              <tbody>
                 <tr>
                    <td class="bg-white">
                       <div id="carousel" class="carousel slide" data-ride="carousel">
                          <!-- Wrapper for slides -->
                          <div class="carousel-inner" role="listbox">
                             @foreach( $news as $newsitem )
                             <div class="item {{ $loop->first ? ' active' : '' }}">
                                <div>
                                   <div class="edit-news-head">
                                      {{$newsitem->title}}
                                   </div>
                                   <div class="news-date">
                                      {{date('j M Y',strtotime($newsitem->date))}}
                                   </div>
                                   <div class="news">
                                      {{$newsitem->description}}
                                   </div>
                                </div>
                             </div>
                             @endforeach
                          </div>
                          <!-- Controls -->
                          <a id="carousel-left" class="left carousel-control" href="#carousel" role="button" data-slide="prev">
                          <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                          <span class="sr-only">Previous</span>
                          </a>
                          <a id="carousel-right" class="right carousel-control" href="#carousel" role="button" data-slide="next">
                          <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                          <span class="sr-only">Next</span>
                          </a>
                       </div>
                    </td>
                 </tr>
              </tbody>
           </table>
        </div>
        @endif
        <div class="table-responsive">
            <table class="table table-striped bg-white table-border">
               <thead>
                  <tr>
                     <th>Holidays {{date('Y')}}</th>
                     <th>Day</th>
                  </tr>
               </thead>
               <tbody>
                  @if(!empty($holidays))
                     @foreach($holidays as $holiday)
                        @if(!empty($active)&& $holiday->date==$active->date)
                        <tr style="border-left: 5px solid #00C15F" style="{{$holiday->type =='Others' ? 'background-color:#FFDAB9' : ''}}">
                           <td class="td-text">
                              <small style="font-weight:900;color:#555">
                              {{date('j M Y',strtotime($holiday->date))}} ({{$holiday->reason}}) 
                              @if($holiday->type=='Others')
                              <span>*Optional*</span>
                              @endif
                              </small>
                           </td>
                           <td class="td-text">
                              <small style="font-weight:900;color:#555">
                              {{date('l',strtotime($holiday->date))}}
                              </small>
                           </td>
                        </tr>
                        @else
                        
                        <tr style="{{$holiday->type =='Others' ? 'background-color:#FFDAB9 !important' : ''}}">
                           <td class="td-text">
                              <small>
                              {{date('j M Y',strtotime($holiday->date))}} ({{$holiday->reason}})
                              </small>
                              @if($holiday->type=='Others')
                              <span>*Optional*</span>
                              @endif
                           </td>
                           <td class="td-text">
                              <small>
                              {{date('l',strtotime($holiday->date))}}
                              </small>
                           </td>
                        </tr>
                        @endif
                     @endforeach
                  @else
                  <tr>
                     <td colspan="2">No results found.</td>
                  </tr>
                  @endif
               </tbody>
            </table>
        </div>
        <div class="table-responsive">
            <table class="table table-striped bg-white table-border">
               <thead>
                  <tr>
                     <th>Upcoming Birthdays</th>
                     <th class="text-right">
                        @if(!empty($bdays))
                        <a href="birthdays">View All</a>
                        @endif
                     </th>
                  </tr>
               </thead>
               <tbody>
                  @if(empty($bdays))
                  <tr>
                     <td>
                        No Upcoming Birthdays
                     </td>
                  </tr>
                  @else
                  @foreach($bdays as $array)
                     @if($array['active']==1)
                     <tr style="border-left: 5px solid #00C15F">
                        @else
                     <tr>
                        @endif
                        <td>
                           {{$array['user']->name}}
                        </td>
                        <td class="text-right">
                           {{date('j M',strtotime($array['user']->dob))}}
                        </td>
                     </tr>
                     @endforeach
                  @endif
               </tbody>
            </table>
        </div>
      </div>
  </div>
</div>
@endsection