@extends(Request::is('*')?'pages.userView.partials.usernav':'pages.userView.partials.nav')
@section('sub-content')

<script type="text/javascript">
	window.projects = '<?php echo json_encode($projects); ?>';
	window.user = '<?php echo json_encode($user); ?>';
	window.months = '<?php echo json_encode($months); ?>';
    window.week = '<?php echo json_encode($week); ?>';
	window.data = '<?php echo json_encode($data, JSON_UNESCAPED_SLASHES); ?>';
</script>

<div ng-app="myApp" ng-controller="overtimeallwanceCtrl">
    <div class="dashboard-container">
        <div class="loader" ng-show="model.loading"></div>
        <div class="row">
            <div class="col-lg-9 col-md-12">
                <div class="m-b-5">
                    <div class="form-horizontal m-b-10">
                        <div class="flex-class align-items-center justify-content-between">
                            <div>
                                <button ng-disabled="!data.previous" class="btn btn-sm btn-primary" ng-click="model.previousMonth()">PREV</button>
                            </div>
                            <div>
                               <h4 style="margin: 4px 0 0 0;">%%data.calendarHeader%%</h4>
                            </div>
                            <div>
                                <button ng-disabled="!data.next" class="btn btn-sm btn-primary pull-right" ng-click="model.nextMonth()">NEXT</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-calndar bg-white">
                        <tr>
                            <th ng-repeat="label in data.labels">%%label%%</th>
                        </tr>
                        <tr ng-repeat="i in [] | range: data.weeksInMonth track by $index">
                            <td ng-repeat="j in [1,2,3,4,5,6,7]" class="text-center ">
                                <span ng-if="!data.days[(i*7)+ (j-1)].date"> - </span>
                                <div ng-if="data.days[(i*7)+ (j-1)].date">
                                    <span ng-if="(data.is_locked && data.days[(i*7)+ (j-1)].day <= 25) || (data.is_next_locked && data.days[(i*7)+ (j-1)].day > 25) "><i class="fa fa-lock"></i></span>
                                    <span class="date">%% data.days[(i*7)+ (j-1)].day %%</span>
                                    <div class="label-wrap m-b-5 m-t-10" ng-hide="(!data.days[(i*7)+ (j-1)].on_site && (data.is_locked && data.days[(i*7)+ (j-1)].day <= 25 || data.is_next_locked && data.days[(i*7)+ (j-1)].day > 25  ) || data.days[(i*7)+ (j-1)].onsite_eligible == 'false'  )  ">
                                        <label class="label label-primary">
                                            <input ng-disabled="(data.is_locked && data.days[(i*7)+ (j-1)].day <= 25) || (data.is_next_locked && data.days[(i*7)+ (j-1)].day > 25) || data.days[(i*7)+ (j-1)].on_site_approved " type="checkbox" ng-checked="data.days[(i*7)+ (j-1)].bonus.onsite_bonus" name="checkbox_onsite_week[data.days.day]" ng-click="model.onOnSiteCheckBoxClick(data.days[(i*7)+ (j-1)], (i*7)+(j-1))" > On Site
                                        </label>
                                        <i ng-if="data.days[(i*7)+ (j-1)].bonus.onsite_bonus_loading === 'progress'" class="loader-spin fa">&#xf110;</i>
                                        <i ng-if="data.days[(i*7)+ (j-1)].bonus.onsite_bonus_submitted && data.days[(i*7)+ (j-1)].bonus.onsite_bonus_status" class="fa fa-check text-success on-site-status-icon"></i>
                                        <i ng-if="data.days[(i*7)+ (j-1)].bonus.onsite_bonus_submitted && data.days[(i*7)+ (j-1)].bonus.onsite_bonus_status === false" class="fa fa-times text-danger on-site-status-icon"></i>

                                    </div>
                                    <small ng-if="data.days[(i*7)+ (j-1)].warning_message">%%data.days[(i*7)+ (j-1)].warning_message%%</small>
                                    <div ng-if="data.days[(i*7)+ (j-1)].is_weekend == 'true' || data.days[(i*7)+ (j-1)].is_holiday == 'true' || data.days[(i*7)+ (j-1)].on_leave == 'true'   ">
                                        <div class="label-wrap" ng-hide="!data.days[(i*7)+ (j-1)].additional && (data.is_locked && data.days[(i*7)+ (j-1)].day <= 25 || data.is_next_locked && data.days[(i*7)+ (j-1)].day > 25 )">
                                            <label class="label" ng-class="model.data[week[day]].weekend.status === 'approved'? 'label-success': (model.data[week[day]].weekend.status === 'rejected'? 'label-warning' : 'label-info')">
                                                <input ng-disabled="(data.is_locked && data.days[(i*7)+ (j-1)].day <= 25) || (data.is_next_locked && data.days[(i*7)+ (j-1)].day > 25) || data.days[(i*7)+ (j-1)].additional_approved " type="checkbox" ng-checked="data.days[(i*7)+ (j-1)].bonus.additional_bonus" name="checkbox_weekend_week[data.days.day]" ng-click="model.onAdditionalWorkCheckBoxClick(data.days[(i*7)+(j-1)], (i*7)+(j-1) )"> I worked
                                            </label>
                                            <i ng-if="data.days[(i*7)+ (j-1)].bonus.additional_bonus_loading" class="loader-spin fa">&#xf110;</i>
                                            <i ng-if="data.days[(i*7)+ (j-1)].bonus.additional_bonus_submitted && data.days[(i*7)+ (j-1)].bonus.additional_bonus_status" class="fa fa-check text-success extra-work-status-icon"></i>
                                            <i ng-if="data.days[(i*7)+ (j-1)].bonus.additional_bonus_submitted && data.days[(i*7)+ (j-1)].bonus.additional_bonus_status === false" class="fa fa-times text-danger extra-work-status-icon"></i>
                                            <span ng-if="data.days[(i*7)+ (j-1)].is_holiday == 'true' " class="leave-span">%%data.days[(i*7)+ (j-1)].holiday.reason%%</span>
                                            <span ng-if="data.days[(i*7)+ (j-1)].on_leave == 'true' " class="leave-span">On Leave</span>
                                        </div>
                                    </div>
                                </div>
                               
                            </td>
                        </tr>
                    </table> 
                </div>
            </div>
            <div class="col-lg-3 col-md-12">

                <div class="panel panel-default clearfix">
                    <div class="panel-heading">
                        <h4 class="panel-title">Bonuses</h4>
                    </div>
                    <div >
                        <!-- <div ng-if="noRecordsFlag" class="panel-body">
                            No records found.
                        </div> -->
                        <div class="table-responsive">
                            <table class="table no-margin table-condensed">
                                 <tbody ng-repeat="day in data.days | orderBy: reverse:true track by $index">
                                    <tr ng-show="day.bonus.additional_bonus">
                                        <td width="88">
                                            <div class="small"> %%day.date | date:'mediumDate'%%</div>
                                            <div style="display: table; margin: 4px 0;">
                                                <div style="display: table-cell; vertical-align: middle;     text-transform: capitalize;">
                                                    <span ng-show="day.bonus.additional_bonus.status == 'pending'" class="text-warning small"><i class="fa fa-fw fa-exclamation" uib-tooltip="%%day.bonus.additional_bonus.status%%"></i></span>
                                                    <span ng-show="day.bonus.additional_bonus.status == 'approved'" class="text-success small"><i class="fa fa-fw fa-check" uib-tooltip="%%day.bonus.additional_bonus.status%%"></i></span>
                                                    <span ng-show="day.bonus.additional_bonus.status == 'rejected' || bonus.status == 'cancelled' "class="text-danger small"><i class="fa fa-fw fa-ban" uib-tooltip="%%day.bonus.additional_bonus.status%%"></i></span>
                                                </div>
                                                <div style="display: table-cell; vertical-align: middle;">
                                                    <span ng-show="day.bonus.additional_bonus" class="label label-primary">additional</span>
                                                </div>

                                            </div>
                                        </td>
                                        <td>
                                             <div>
                                                <!-- <span style="text-transform: capitalize;" class="label label-default">SUB TYPE</span> -->
                                            </div>
                                            <div class="m-b-10">
                                                <span ng-show="day.bonus.additional_bonus.duration == '8'" class="label label-default">Full Day</span>
                                                <span ng-show="day.bonus.additional_bonus.duration == '6'" class="label label-default">6 Hours</span>
                                                <span ng-show="day.bonus.additional_bonus.duration == '4'" class="label label-default">Half Day</span>
                                                <span ng-show="day.bonus.additional_bonus.duration == '2'" class="label label-default">2 Hours</span>
                                                <span style="text-transform: uppercase;" class="label label-default">%%day.bonus.additional_bonus.redeem_type%%</span>
                                            </div>

                                            <ui-select name="projects[bonus.id]" ng-disabled="true" multiple ng-model="day.bonus.additional_bonus.projects"  theme="select2" class="small">
                                                <ui-select-match>%%$item.project_name%%</ui-select-match>
                                                <ui-select-choices repeat="project.id as project in day.assigned_projects | filter: $select.search">
                                                    %%project.project_name%%
                                                </ui-select-choices>
                                            </ui-select>
                                        </td>
                                    </tr>
                                    <tr ng-show="day.bonus.onsite_bonus">

                                        <td width="88">
                                            <div class="small"> %%day.date | date:'mediumDate'   %%</div>
                                            <div style="display: table; margin: 4px 0;">
                                                <div style="display: table-cell; vertical-align: middle;     text-transform: capitalize;">
                                                    <span ng-show="day.bonus.onsite_bonus.status == 'pending'" class="text-warning small"><i class="fa fa-fw fa-exclamation" uib-tooltip="%%bonus.onsite_bonus.status%%"></i></span>
                                                    <span ng-show="day.bonus.onsite_bonus.status == 'approved'" class="text-success small"><i class="fa fa-fw fa-check" uib-tooltip="%%day.bonus.onsite_bonus.status%%"></i></span>
                                                    <span ng-show="day.bonus.onsite_bonus.status == 'rejected' || bonus.status == 'cancelled' "class="text-danger small"><i class="fa fa-fw fa-ban" uib-tooltip="%%bonus.onsite_bonus.status%%"></i></span>
                                                </div>
                                                <div style="display: table-cell; vertical-align: middle;">
                                                    <span class="label label-primary">onsite</span>
                                                </div>
                                                <div>
                                                    <span class="label label-default">%%day.bonus.onsite_bonus.onsite_allowance.type%%</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                             <div>
                                                <!-- <span style="text-transform: capitalize;" class="label label-default">SUB TYPE</span> -->
                                            </div>
                                            <div>
                                                <span ng-show="day.bonus.additional_bonus.duration == '8'" class="label label-default">Full Day</span>
                                                <span ng-show="day.bonus.additional_bonus.duration == '6'" class="label label-default">6 Hours</span>
                                                <span ng-show="day.bonus.additional_bonus.duration == '4'" class="label label-default">Half Day</span>
                                                <span ng-show="day.bonus.additional_bonus.duration == '2'" class="label label-default">2 Hours</span>
                                                <span style="text-transform: uppercase;" class="label label-default">%%day.bonus.additional_bonus.redeem_type%%</span>
                                            </div>

                                            <ui-select name="projects[bonus.id]" ng-disabled="true" multiple ng-model="model.selected_projects[bonus.id]"  theme="select2" class="small">
                                                <ui-select-match>%%$item.project_name%%</ui-select-match>
                                                <ui-select-choices repeat="project.id as project in model.projects | filter: $select.search">
                                                    %%project.project_name%%
                                                </ui-select-choices>
                                            </ui-select>
                                        </td>
                                    </tr>
                                    <tr ng-repeat="bonus in day.bonus.t" >
                                        <td width="88">
                                            <div class="small"> %%day.date | date:'mediumDate'   %%</div>
                                            <div style="display: table; margin: 4px 0;">
                                                <div style="display: table-cell; vertical-align: middle;     text-transform: capitalize;">
                                                    <span ng-show="bonus.status == 'pending'" class="text-warning small"><i class="fa fa-fw fa-exclamation" uib-tooltip="%%bonus.status%%"></i></span>
                                                    <span ng-show="bonus.status == 'approved'" class="text-success small"><i class="fa fa-fw fa-check" uib-tooltip="%%bonus.status%%"></i></span>
                                                    <span ng-show="bonus.status == 'rejected' || bonus.status == 'cancelled' "class="text-danger small"><i class="fa fa-fw fa-ban" uib-tooltip="%%bonus.status%%"></i></span>
                                                </div>
                                                <div style="display: table-cell; vertical-align: middle;">
                                                    <span ng-show="bonus.type == 'onsite'" class="label label-primary">%%bonus.type%%</span>
                                                    <span ng-show="bonus.type != 'onsite'" class="label label-info">%%bonus.type%%</span>
                                                </div>

                                            </div>
                                        </td>
                                        <td>
                                            <div ng-show="bonus.type == 'onsite'" >
                                                <span style="text-transform: capitalize;" class="label label-default">%%bonus.sub_type%%</span>
                                            </div>
                                            <div ng-show="bonus.type == 'additional'" >
                                                <span ng-show="bonus.redeem_info.duration == '8h'" class="label label-default">Full Day</span>
                                                <span ng-show="bonus.redeem_info.duration == '6h'" class="label label-default">6 Hours</span>
                                                <span ng-show="bonus.redeem_info.duration == '4h'" class="label label-default">Half Day</span>
                                                <span ng-show="bonus.redeem_info.duration == '2h'" class="label label-default">2 Hours</span>
                                                <span style="text-transform: uppercase;" class="label label-default">%%bonus.redeem_info.redeem_type%%</span>
                                            </div>

                                            <ui-select name="projects[bonus.id]" ng-disabled="true" multiple ng-model="model.selected_projects[bonus.id]"  theme="select2" class="small">
                                                <ui-select-match>%%$item.project_name%%</ui-select-match>
                                                <ui-select-choices repeat="project.id as project in model.projects | filter: $select.search">
                                                    %%project.project_name%%
                                                </ui-select-choices>
                                            </ui-select>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection