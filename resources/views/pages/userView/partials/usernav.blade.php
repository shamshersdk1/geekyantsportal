@extends('layouts.admin-dashboard')
@section('main-content')
<style>
.panel.panel-horizontal {
    display:table;
    width:100%;
    margin-bottom:0px;
    border-style:solid;
    border-width:0px 0px 1px 0px;
}
.panel.panel-horizontal > .panel-heading, .panel.panel-horizontal > .panel-body{
    display:table-cell;
}
.panel.panel-horizontal > .panel-heading {
    /*width: 20%;*/
    background-color:#FFF;
    border-right: none;
    border-bottom: none;
}

/*.info-right-table td{
    padding: 4px 2px !important;
}*/
.icons {
    font-size: 24px;
}
.info-right-table .icons td {
    padding:8px 2px 0 2px !important;
}
.navbar.navbar-default{
    background-color:#FFF;
}


@media(max-width:900px){
    #options > li{
        width:100%;
    }
}
#options{
    width:100%;
    li {
        a {
            border: 2px solid transparent;
        }
    }
}
#options > .active > a, #options > .active > a:hover, #options > li > a:hover{
    text-decoration: none;
    border-radius:0px;
    border-bottom-width: 2px !important;
    border-bottom-style: solid;
    border-bottom-color: #5470AC !important;
    color: #5470AC !important;
    background-color: transparent !important;
    -webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */
    -moz-box-sizing: border-box;    /* Firefox, other Gecko */
    box-sizing: border-box;
}
small{
    color:#32435C !important;
    font-size: 82%;
}
</style>
<div style="padding: 15px 15px 0 15px;">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="panel panel-default user-profile-details no-margin">
                <div class="panel-heading profile-image-container hidden-xs" style="@if (!empty($user->profile->image)) background: url({{$user->profile->image}}) @endif">
                </div>
                <div class="panel-body profile-details-container">
                    <div>
                        <div class="d-flex">
                            <div class="visible-xs">
                                @if (!empty($user->profile->image)) 
                                    <img src="{{$user->profile->image}}" class="img-circle" style="width: 40px; margin-right: 10px;" />
                                @endif
                            </div>
                            <div>
                                <ul style="list-style-type: none; padding: 0; margin: 0; ">
                                    <li><h4 style="margin:0">{{$user->name}}</h4></li>
                                    <li style="margin-bottom:2%"><small>{{empty($user->profile->title) ? "-" : $user->profile->title}}</small></li>
                                </ul>
                            </div>
                        </div>
                        <ul style="list-style-type: none; padding: 0; margin: 0;">
                            <li class="m-t-10">{{$user->email}}</li>
                            <li class="m-t-5">{{empty($user->profile->phone) ? "-" : $user->profile->phone}}</li>
                        </ul>
                    </div>
                    <table class="info-right-table" style="float:right">
                        <tr>
                            <td><small>Emp ID</small></td>
                            <td><small> : </small></td>
                            <td class="p-l-5"><small>{{empty($user->employee_id) ? "-" : $user->employee_id}}</small></td>
                        </tr>
                        <tr>
                            <td><small>DOJ</small></td>
                            <td><small> : </small></td>
                            <td class="p-l-5"><small>{{empty($user->joining_date) ? "-" : date_in_view($user->joining_date)}}</small></td>
                        </tr>
                        <tr>
                            <td><small>DOC</small></td>
                            <td><small> : </small></td>
                            <td class="p-l-5"><small>{{empty($user->confirmation_date) ? "-" : date_in_view($user->confirmation_date)}}</small></td>
                        </tr>
                        <tr class="icons">
                            <td colspan="3">
                            @if(!empty($user->profile->twitter))
                            <a href="{{$user->profile->twitter}}"><i class="fa fa-twitter-square" style="color:#00aced" aria-hidden="true"></i></a>
                            @endif
                            @if(!empty($user->profile->facebook))
                            <a href="{{$user->profile->facebook}}"><i class="fa fa-fw fa-facebook-square" style="color:#3b5998" aria-hidden="true"></i></a>
                            @endif
                            @if(!empty($user->profile->google))
                            <a href="{{$user->profile->google}}"><i class="fa fa-fw fa-google-plus-square" style="color:#dd4b39" aria-hidden="true"></i></a>
                            @endif
                            @if(!empty($user->profile->linkedin))
                            <a href="{{$user->profile->linkedin}}"><i class="fa fa-fw fa-linkedin-square" style="color:#007bb6" aria-hidden="true"></i></a>
                            @endif
                            @if(!empty($user->profile->github))
                            <a href="{{$user->profile->github}}"><i class="fa fa-github-square" style="color:#333" aria-hidden="true"></i></a>
                            @endif
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <nav class="navbar navbar-default">
                <div class="">
                    <ul class="nav navbar-nav no-margin" id="options">
                        <li @if(Request::is('user/*')||Request::is('user')) @endif><a href="/user">Dashboard</a></li>
                        <li @if(Request::is('user/profile/*')||Request::is('user/profile')) class="active" @endif><a href="/user/profile">Profile</a></li>
                        <li @if(Request::is('user/salary-details/*')||Request::is('user/salary-details')) class="active" @endif><a href="/user/salary-details">Salary Details</a></li>
                        <li @if(Request::is('user/leaves/*')||Request::is('user/leaves')) class="active" @endif><a href="/user/leaves">Leaves</a></li>
                        <li @if(Request::is('user/loans/*')||Request::is('user/loans')) class="active" @endif><a href="/user/loans">Loans</a></li>
                        <li @if(Request::is('user/bonuses/*')||Request::is('user/bonuses')) class="active" @endif><a href="/user/bonuses">Bonus</a></li>
                        <li @if(Request::is('user/skill/*')||Request::is('user/skill')) class="active" @endif><a href="/user/skill">Skills</a></li>
                        <li @if(Request::is('user/timesheet/*')||Request::is('user/timesheet')) class="active" @endif><a href="/user/timesheet">Timesheet</a></li>
                        <li @if(Request::is('user/assets/*')||Request::is('user/assets')) class="active" @endif><a href="/user/assets">Assets</a></li>
                        <!-- <li @if(Request::is('user/feedback/*')||Request::is('user/feedback')) class="active" @endif><a href="/user/feedback">Feedback</a></li> -->
                    </ul>
                </div>
            </nav>
        </div>
    </div>
        <div >
            @if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                        @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif
        </div>
    </div>
@yield('sub-content')
@endsection
