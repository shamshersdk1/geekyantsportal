@include('pages/admin/userView/modals/add-news-modal')
@include('pages/admin/userView/modals/edit-news-modal')
@include('pages/admin/admin-leave-section/edit-leave-modal')
@include('pages/admin/admin-leave-section/javascript')
<script>
	let news;
	function updateNews() {
		news.title = $('#editNewsModal input[name="title"]').val();
		news.date = $('#editNewsModal input[name="date"]').val();
		news.description = $('#editNewsModal input[name="description"]').val();
		$.ajax({
			type: 'PUT',
			url: '/admin/news/'+role.id,
			dataType: 'text',
			data: news,
			beforeSend: function() {
				$("div.loading").show();
				$("div.content").hide();
			},
			complete: function() {
				$("div.loading").hide();
				$("div.content").show();
			},
			success: function(data) {
				$('#editNewsModal').modal('hide');
				window.location.reload();
			},
			error: function(errorResponse){
				$('#editNewsModal').modal('hide');
				console.log(errorResponse);
				window.location.reload();
			}
		});
	}
	$(function(){
		$("#addNewsModal").on("show.bs.modal", function(){
			$(this).find("input")
				.val('')
				.end()
		});

		$("#addNewsModal").on("hidden.bs.modal", function(){
			$(this).find("input")
				.val('')
				.end()
		});
		$('#editNewsModal').on("show.bs.modal", function (e) {
			let id = $(e.relatedTarget).data('id')
			$.ajax({
				type: 'GET',
				url: '/admin/news/'+id,
				beforeSend: function() {
					$("div.loading").show();
					$("div.content").hide();

				},
				complete: function() {
					$("div.loading").hide();
					$("div.content").show();
				},
				success: function(data) {
					news = data;
                    console.log(news);
					$('#editNewsModal input[name="title"]').val(data.title);
					$('#editNewsModal input[name="date"]').val(data.date);
					$('#editNewsModal textarea[name="description"]').val(data.description);
				}
			});
		});

		$("#editNewsModal").on("hidden.bs.modal", function(){
			$(this).find("input")
				.val('')
				.end()
		});
	});
</script>