<script>
$(function (){
    $('.sick-leave').hide();
    $("select[name='type']").on('change',function(e){
        if(e.target.value=="sick")
        {
            $('.paid-unpaid-leave').hide();
            $('.sick-leave').show();
        }
        else{
            $('.paid-unpaid-leave').show();
            $('.sick-leave').hide();
        }
    });
    $("#leaveProjectsModal").on("hidden.bs.modal", function(){
            $('.projects-table').remove();
    });
    $("#apply-leave-btn").click(function (e){
        e.preventDefault();
        var heading = "<h5 style='display:block;padding-left:5px' class='text-left current-leave-info'>Type: "+$("#leave-type-selector option:selected").text()+"</h5>";
        heading +="<h5 style='display:block;padding-left:5px' class='text-left current-leave-info'>Start date: "+$("#datetimeinputstart").val()+"</h5>";
        heading +="<h5 style='display:block;padding-left:5px' class='text-left current-leave-info'>End date: "+$("#datetimeinputend").val()+"</h5>";
        heading +="<h5 style='display:block;padding-left:5px' class='text-left current-leave-info'>Note: "+$("#user-leave-form [name='reason']").val()+"</h5>";
        $("#leaveProjectsModal .current-leave-info").remove();
        $("#leaveProjectsModal .modal-body.content").append(heading);
        // $("#user-leave-form").submit();
        var id = '{{$user->id}}';
        if($("select[name='type']").val()=="Select Leave type")
        {
            alert("Leave type not specified");
        }
        if($("select[name='type']").val()=="sick")
        {
            alert("Contact your reporting manager or team lead(s)");
        }
        
        var start = $("#datetimeinputstart").val();
        var end = $("#datetimeinputend").val();
        if((start != "")&&(end != ""))
        {
            $.ajax({
                type: 'GET',
                url: '/user/leaves/check?id='+id+
                    '&start_date='+start+
                    '&end_date='+end,
                dataType: 'json',
                success: function(data) {
                    if(data.length!=0) {
                        $('#leaveProjectsModal').modal('show');
                        $("div.loading").show();
                        $("div.content").hide();
                        var x = '';
                        for(var i=0;i<Object.keys(data).length-2;i++)
                        {
                            x += '<table class="projects-table table table-bordered table-responsive table-striped">';
                            x +='<thead><tr>';
                            x +='<th colspan="'+(data.dates.length+1)+'" style="color:#EF3B40">Project : '+data[i].name+'</th>';
                            x +='</tr></thead><tr><th>Dates</th>';
                            for(var l=0;l<data.dates.length;l++)
                            {
                                if(data.code[l]==1) {
                                    x +='<th style="background-color:grey">'+data.dates[l]+'</th>';
                                } else {
                                    x +='<th>'+data.dates[l]+'</th>';
                                }
                            }
                            x +='</tr>';
                                for(var j=0;j<data[i].users.length;j++)
                                {
                                    x+='<tr>';
                                    x +='<td>'+data[i].users[j].name+'</td>';
                                    for(var k=0;k<data[i].users[j].dates.length;k++)
                                    {
                                        if(data.code[k]==1) {
                                            x+='<td style="background-color:grey"></td>';
                                        } else {
                                            if(data[i].users[j].dates[k]!=0) {
                                                x +='<td style="background-color:#EF3B40;color:white">'+data[i].users[j].dates[k]+'</td>';
                                            } else {
                                            x +='<td></td>';
                                            }
                                        }
                                    }
                                    x+='</tr>';
                                }
                            x+='<tr><td colspan="'+(data.dates.length+1)+'"><textarea style="width:100%" class="reason-textarea" data-projectid = "'+data[i].id+'" rows = "3" placeholder = "Describe the impact of your absence in this project?" name="reasonText" required></textarea></td></tr>';
                            x+='</table>';
                        }
                        $("#leaveProjectsModal .modal-body.content").append(x);
                        $("div.loading").hide();
                        $("div.content").show();
                    } else {
                        $("#user-leave-form").submit();
                    }
                },
                error: function(errorResponse){
                    $('#leaveProjectsModal').modal('hide');
                    // console.log(errorResponse);
                    window.location.reload();
                }
            });
            $('#leaveProjectsModal').show();
        } else {
            alert("Dates not provides");
        }
    });
    $("#reasons-form").submit(function (e){
        e.preventDefault();
        var count = 0;
        var requestObject = {};
        requestObject.type = $("#leave-type-selector option:selected").val();
        requestObject.start_date = $("#datetimeinputstart").val();
        requestObject.end_date = $("#datetimeinputend").val();
        requestObject.reason = $("#user-leave-form [name='reason']").val();
        requestObject.user_id = '{{$user->id}}';
        $.ajax({
            type: 'POST',
            url: '/user/leaves/overlapping-request/leave',
            dataType: 'json',
            data: requestObject,
            success: function(data) {
                $(".reason-textarea").each(function (value){
                    var formData = {};
                    
                    formData.project_id = $(this).data('projectid');
                    formData.reason = $(this).val();
                    formData.user_id = data.user_id;
                    formData.leave_id = data.leave_id;
                    $.ajax({
                        type: 'POST',
                        url: '/user/leaves/overlapping-request/reasons',
                        dataType: 'json',
                        data: formData,
                        success: function(data) {
                        },
                        error: function(errorResponse){
                            $('#leaveProjectsModal').modal('hide');
                            if(count == 0) {
                                alert(errorResponse.responseText);
                                count++;
                            }
                        }
                    });
                });
            window.location.reload();
            },
            error: function(errorResponse){
                $('#leaveProjectsModal').modal('hide');
                if(count == 0) {
                    alert(errorResponse.responseText);
                    count++;
                }
            }
        });
    });
});
</script>
<style>
#leaveProjectsModal tr td:first-child {
    min-width: 180px;
    text-align: left;
} 
#leaveProjectsModal th,#leaveProjectsModal td {
    min-width: 5em;
}
#leaveProjectsModal thead {
    border-bottom: 1px solid black; 
}
</style>