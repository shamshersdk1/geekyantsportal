@extends('layouts.admin-dashboard')
@section('main-content')
<style>
.panel.panel-horizontal {
    display:table;
    width:100%;
    margin-bottom:0px;
    border-style:solid;
    border-width:0px 0px 1px 0px;
}
.panel.panel-horizontal > .panel-heading, .panel.panel-horizontal > .panel-body{
    display:table-cell;
}
.panel.panel-horizontal > .panel-heading {
    width: 20%;
    background-color:#FFF;
    border-right: 1px solid #ddd;
    border-bottom:0;
}
.info-right-table td{
    padding:0px 15px 0px 15px !important;
}
.icons {
    font-size: 20px;
}
.navbar.navbar-default{
    background-color:#FFF;
}
#options > li > a{
text-transform:none;
color:#888;
width:100%;
}
#options > li{
width:13%;
}
@media(max-width:768px){
    #options > li{
width:100%;
}
}
#options{
    width:100%;
}
#options > .active > a, #options > .active > a:hover, #options > li > a:hover{
text-decoration: none;
border-radius:0px;
border-bottom-width: 2px !important;
border-bottom-style: solid;
border-bottom-color: #3b5998 !important;
color: #007bb6 !important;
background-color: transparent !important;
-webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */
-moz-box-sizing: border-box;    /* Firefox, other Gecko */
box-sizing: border-box;
}
</style>
<div class="container-fluid">
    <div class="row" style="margin:2% 0% 1.5% 0%">
        <div class="col-xs-12 col-sm-12 col-md-12" style="padding:0">
            <div class="panel panel-default panel-horizontal">
                <div class="panel-heading" style="width:12%;padding:0;@if (!empty($user->profile->image))background:url({{$user->profile->image}}); @endif background-size:100% 100%">
                </div>
                <div class="panel-body" style="padding-top:2%">
                    <ul class="col-xs-6 col-sm-6 col-md-6" style="list-style-type: none;display:inline;padding:0;margin:0">
                        <li><h4 style="margin:0">{{$user->name}}</h4></li>
                        <li style="margin-bottom:2%"><small style="color:#aaa">{{empty($user->profile->title) ? "-" : $user->profile->title}}</small></li>
                        <li><small style="color:#aaa">{{$user->email}}</small></li>
                        <li><small style="color:#aaa">{{empty($user->profile->phone) ? "-" : $user->profile->phone}}</small></li>
                    </ul>
                    <table class="info-right-table">
                        <tr>
                            <td><small style="color:#aaa">Emp ID</small></td>
                            <td><small style="color:#aaa"> : </small></td>
                            <td><small style="color:#aaa">{{empty($user->employee_id) ? "-" : $user->employee_id}}</small></td>
                        </tr>
                        <tr>
                            <td><small style="color:#aaa">DOJ</small></td>
                            <td><small style="color:#aaa"> : </small></td>
                            <td><small style="color:#aaa">{{empty($user->joining_date) ? "-" : date_in_view($user->joining_date)}}</small></td>
                        </tr>
                        <tr>
                            <td><small style="color:#aaa">DOC</small></td>
                            <td><small style="color:#aaa"> : </small></td>
                            <td><small style="color:#aaa">{{empty($user->confirmation_date) ? "-" : date_in_view($user->confirmation_date)}}</small></td>
                        </tr>
                        <tr class="icons">
                            <td colspan="3">
                            @if(!empty($user->profile->twitter))
                            <a href="{{$user->profile->twitter}}"><i class="fa fa-twitter-square" style="color:#00aced" aria-hidden="true"></i></a>
                            @endif
                            @if(!empty($user->profile->facebook))
                            <a href="{{$user->profile->facebook}}"><i class="fa fa-facebook-square" style="color:#3b5998" aria-hidden="true"></i></a>
                            @endif
                            @if(!empty($user->profile->google))
                            <a href="{{$user->profile->google}}"><i class="fa fa-google-plus-square" style="color:#dd4b39" aria-hidden="true"></i></a>
                            @endif
                            @if(!empty($user->profile->linkedin))
                            <a href="{{$user->profile->linkedin}}"><i class="fa fa-linkedin-square" style="color:#007bb6" aria-hidden="true"></i></a>
                            @endif
                            @if(!empty($user->profile->github))
                            <a href="{{$user->profile->github}}"><i class="fa fa-github-square" style="color:#333" aria-hidden="true"></i></a>
                            @endif
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <ul class="nav navbar-nav col-xs-12 col-sm-12 col-md-12" id="options" style="margin-top:0">
                        <li @if(Request::is('admin/users/*/profile')||Request::is('admin/users/*/profile/*')) class="active" @endif><a href="/admin/users/{{$user->id}}/profile"><small>Profile</small></a></li>
                        <li @if(Request::is('admin/users/*/salary-breakdown/*')||Request::is('admin/users/*/salary-breakdown')) class="active" @endif><a href="/admin/users/{{$user->id}}/salary-breakdown"><small>Salary Breakdown</small></a></li>
                        <li @if(Request::is('admin/users/*/payslips/*')||Request::is('admin/users/*/payslips')) class="active" @endif><a href="/admin/users/{{$user->id}}/payslips"><small>Payslips</small></a></li>
                        <li @if(Request::is('admin/users/*/appraisals/*')||Request::is('admin/users/*/appraisals')) class="active" @endif><a href="/admin/users/{{$user->id}}/appraisals"><small>Appraisals</small></a></li>
                        <li @if(Request::is('admin/users/*/leaves/*')||Request::is('admin/users/*/leaves')) class="active" @endif><a href="/admin/users/{{$user->id}}/leaves"><small>Leaves</small></a></li>
                        <li @if(Request::is('admin/users/*/loans/*')||Request::is('admin/users/*/loans')) class="active" @endif><a href="/admin/users/{{$user->id}}/loans"><small>Loans</small></a></li>
                        <li @if(Request::is('admin/users/*/bonuses/*')||Request::is('admin/users/*/bonuses')) class="active" @endif><a href="/admin/users/{{$user->id}}/bonuses"><small>Bonus</small></a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</div>
<div class="col-md-12">
    @if(!empty($errors->all()))
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ $error }}</span><br/>
                @endforeach
        </div>
    @endif
    @if (session('message'))
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <span>{{ session('message') }}</span><br/>
        </div>
    @endif
</div>
@yield('sub-content')
@endsection
