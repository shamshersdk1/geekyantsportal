@extends(Request::is('*')?'pages.userView.partials.usernav':'pages.userView.partials.nav')
@section('sub-content')
<style>
.panel.panel-horizontal {
    display:table;
    width:100%;
    margin-bottom:0px;
    border-style:solid;
    border-width:0px 0px 1px 0px;
}
.panel.panel-horizontal > .panel-heading, .panel.panel-horizontal > .panel-body{
    border-radius:0px;
    display:table-cell;
}
.panel.panel-horizontal > .panel-heading {
    width: 20%;
    background-color:#FFF;
    border-right: 1px solid #ddd;
    border-bottom:0;
}
</style>
<script>
		$(function(){
		$(".deleteform").submit(function(event){
                return confirm('Are you sure?');
            });
        });
</script>
<div class="dashboard-container">
    <div class="row">
        <div class="col-sm-6">
        @include('pages.userView.payslips.payslips');
        </div>
        <div class="col-sm-6">
            @include('pages.userView.payslips.appraisals');
        </div>
    </div>
</div>
@endsection
