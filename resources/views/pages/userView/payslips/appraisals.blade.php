@extends(Request::is('*')?'pages.userView.partials.usernav':'pages.userView.partials.nav')
@section('sub-content')

<script>
		$(function(){
		$(".deleteform").submit(function(event){
                return confirm('Are you sure?');
            });
        });
</script>
   <div class="col-sm-6">
                <div class="clearfi text-right">
                    <h4 class="admin-section-heading pull-left m-t-15">Appraisals</h4>
                    <a href="" class="btn btn-default btn-sm"><i class="fa fa-rupee fa-fow"></i> Show Salary Breakdown</a>
                     @if(Auth::user()->isAdmin())
                        <a href="appraisals/create" class="btn btn-success btn-sm"><i class="fa fa-plus fa-fw"></i> Add Appraisal</a>
                    @endif
                </div>
                <div class="panel panel-default text-center m-t-15"><h4>Appraisals Details</h4></div>
                <div class="dashboard-container user-dashborad-container">
    @if(count($appraisals) > 0)
    @foreach($appraisals as $appraisal)
    <div>
        <div class="panel panel-default panel-horizontal">
            <div class="panel-heading"@if($appraisal->is_active==1)style="border-left: 10px solid #269900"@endif>
                <h3 class="panel-title">{{$appraisal->name}}</h3>
                <small style="color:#aaa">{{date("j F Y", strtotime($appraisal->effective_date))}}</small>
                <small style="color:#aaa">@if(!empty($appraisal->end_date)) to {{date("j F Y", strtotime($appraisal->end_date))}}@endif</small>
            </div>
            <div class="panel-body">
                <ul  style="list-style-type: none;display:inline-block;">
                    <li></li>
                    <li>Annual Gross Salary:{{$appraisal->annual_gross_salary?$appraisal->annual_gross_salary:0}}</li>
                    <li>Monthly  Bonus: {{$appraisal->monthly_var_bonus?$appraisal->monthly_var_bonus:0}}</li>
                    <li>Annual Bonus: {{$appraisal->annual_bonus?$appraisal->annual_bonus:0}}</li>
                </ul>
                @if(Auth::user()->isAdmin())
                <div style="float:right;padding-right:0px;margin-top:2%">
                @if(!$appraisal->is_active)
                    <form style="display:inline" method="POST" action="/admin/users/{{$appraisal->user->id}}/appraisals/{{$appraisal->id}}">
                        {{method_field('PUT')}}
                        <button type="submit" class="btn btn-success crud-btn btn-sm" name="activate" value="activate" style="width:90px">Set as active</button>
                    </form>
                    @endif
                    <a href="appraisals/{{$appraisal->id}}/edit" class="btn btn-info crud-btn btn-sm"><i class="fa fa-pencil btn-icon-space" aria-hidden="true"></i>Edit</a>
                    <form action="appraisals/{{$appraisal->id}}" method="post" style="display:inline-block;" class="deleteform">
                        <input name="_method" type="hidden" value="DELETE">
                        <button class="btn btn-danger crud-btn btn-sm" type="submit"><i class="fa fa-trash btn-icon-space" aria-hidden="true"  style="width:10px"></i>Delete</button>
                    </form>
                </div>
                @endif
            </div>
        </div>
    </div>
    @endforeach
    @else
    <div >
        <div class="panel panel-default panel-horizontal">
            <div class="panel-body ">
                <ul style="list-style-type: none;display:inline">
                    <h5>No results Found</h5>
                </ul>
            </div>
        </div>
    </div>
    @endif
    <!-- @if(Auth::user()->isAdmin())
    <div class="text-right m-t-10">
        <a href="{{Request::url()}}/create" class="btn btn-success btn-lg"><i class="fa fa-plus" ></i></a>
    </div>
    @endif -->
</div>
            </div>
        </div>

@endsection
