@extends(Request::is('*')?'pages.userView.partials.usernav':'pages.userView.partials.nav')
@section('sub-content')

<script>
		$(function(){
		$(".deleteform").submit(function(event){
                return confirm('Are you sure?');
            });
        });
</script>
<div class="dashboard-container">
    <div class="user-payslip">
        <div class="row">
            <div class="col-sm-6">
                <h4 class="admin-section-heading">Payslips</h4>
                <div class="panel panel-default " style="overflow:auto">
                    <table class="table table-striped">
                        <thead>
                            <th>Month</th>
                            <th>Working days</th>
                            <th>Days Worked</th>
                            @if(Request::is('admin/users/*'))
                                <th>Gross Salary</th>
                                <th>Loan</th>
                                <th>Total Deduction</th>
                                <th>Annual Bonus</th>
                                <th>Qtr Bonus</th>
                                <th>Performance Bonus</th>
                            @endif
                            <th>Gross Earning</th>
                            <th class="text-right">Action</th>
                        </thead>
                        <?php $c = 0;?>
                        @if(count($result) > 0)
                            @foreach($result['data'] as $data)
                                <tr>
                                    <td>
                                        @if(!empty($result['payslip'][$c]['month'])){{DateTime::createFromFormat('!m',$result['payslip'][$c]['month'])->format('F')}},@endif
                                        @if(!empty($result['payslip'][$c]['year'])){{$result['payslip'][$c]['year']}} &nbsp&nbsp @endif
                                    </td>
                                    <td>{{$data->working_day_month}}</td>
                                    <td>{{$data->days_worked}}</td>
                                    @if(Request::is('admin/users/*'))
                                        <td>{{$data->gross_salary}}</td>
                                        <td>{{$data->loan}}</td>
                                        <td>{{$data->total_deduction_month}}</td>
                                        <td>{{$data->annual_bonus}}</td>
                                        <td>{{$data->qtr_variable_bonus}}</td>
                                        <td>{{$data->performance_bonus}}</td>
                                    @endif
                                    <td>{{$data->amount_payable}}</td>
                                    <td class="text-right">
                                        <a target="_blank" href="/admin/employee-payslip/{{$data->id}}" class="btn btn-info crud-btn btn-xs"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View</a>
                                    </td>
                                </tr>
                            <?php $c++;?>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="11">No results found.</td>
                            </tr>
                        @endif
                    </table>
                </div>
            </div>
            <div class="col-sm-6">
                <h4 class="admin-section-heading">Appraisals</h4>
                <div class="user-dashborad-container">
                    @if(count($appraisals) > 0)
                    @foreach($appraisals as $appraisal)
                    <div>
                        <div class="panel panel-default mo-margin">
                            <div class="panel-heading clearfix"@if($appraisal->is_active==1)style="border-left: 5px solid #269900"@endif>
                                <div class="pull-left">
                                    <h4 class="panel-title">{{$appraisal->name}}</h4>
                                    <small style="color:#aaa">{{date("j F Y", strtotime($appraisal->effective_date))}}</small>
                                    <small style="color:#aaa">@if(!empty($appraisal->end_date)) to {{date("j F Y", strtotime($appraisal->end_date))}}@endif</small>
                                </div>

                                @if(Auth::user()->isAdmin())
                                <div class="pull-right m-t-5">
                                @if(!$appraisal->is_active)
                                    <form style="display:inline-block;" method="POST" action="/admin/users/{{$appraisal->user->id}}/appraisals/{{$appraisal->id}}">
                                        {{method_field('PUT')}}
                                        <button type="submit" class="btn btn-success btn-sm" name="activate" value="activate">Set as active</button>
                                    </form>
                                    @endif
                                    <a href="appraisals/{{$appraisal->id}}/edit" class="btn btn-info btn-sm"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    <form action="appraisals/{{$appraisal->id}}" method="post" style="display:inline-block;" class="deleteform">
                                        <input name="_method" type="hidden" value="DELETE">
                                        <button class="btn btn-danger btn-sm" type="submit"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                    </form>
                                </div>
                                @endif 
                            </div>
                            <div class="panel-body" @if($appraisal->is_active==1)style="border-left: 5px solid #269900"@endif> 

                                <ul class="no-margin no-padding" style="list-style: none;">
                                    <li>
                                        <span class="text-muted">Annual Gross Salary: </span>
                                        <big>{{$appraisal->annual_gross_salary?$appraisal->annual_gross_salary:0}}</big></li>
                                    <li>
                                        <span class="text-muted">Monthly  Bonus: </span>
                                        <big>{{$appraisal->monthly_var_bonus?$appraisal->monthly_var_bonus:0}}</big>
                                    </li>
                                    <li>
                                        <span class="text-muted">Annual Bonus: </span>
                                        <big>{{$appraisal->annual_bonus?$appraisal->annual_bonus:0}}</big>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @else
                    <div >
                        <div class="panel panel-default panel-horizontal">
                            <div class="panel-body ">
                                <ul style="list-style-type: none;display:inline">
                                    <h5>No results Found</h5>
                                </ul>
                            </div>
                        </div>
                    </div>
                    @endif
                    <!-- @if(Auth::user()->isAdmin())
                    <div class="text-right m-t-10">
                        <a href="{{Request::url()}}/create" class="btn btn-success btn-lg"><i class="fa fa-plus" ></i></a>
                    </div>
                    @endif -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection