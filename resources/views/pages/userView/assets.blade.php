@extends(Request::is('*')?'pages.userView.partials.usernav':'pages.userView.partials.nav')
@section('sub-content')
<div class="dashboard-container">
    <div class="row">
        <div class="col-sm-8">
            <h4 class="admin-section-heading">Currently Assigned Assets</h4>
            <div class="panel panel-default m-b-lg">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <tr>
                            <th width="25%">Asset Name</th>
                            <th width="20%">IP Address</th>
                            <th>Other info</th>
                            <th class="text-right" width="25%">Assigned On</th>
                        </tr>
                        @if(count($currentAssets) > 0 )
                            @foreach($currentAssets as $currentAsset)
                                <tr style="border-left: 5px solid #00C15F">
                                    <td>
                                        {{ $currentAsset->asset ? $currentAsset->asset->name : '' }}
                                        <br/><small>
                                        {{ $currentAsset->asset ? $currentAsset->asset->serial_number : '' }}
                                        </small>
                                    </td>
                                    <td>
                                        @foreach($currentAsset->asset->metas as $meta)
                                            @if($meta->key == 'WIFI-MAC' || $meta->key == 'LAN-MAC')
                                                <small>{{$meta->key}} : {{ $meta->ipMapper ? $meta->ipMapper->userIp->ip_address : '' }}</small>
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>
                                        @if($currentAsset->asset )
                                            @foreach($currentAsset->asset->metas as $meta)
                                                <small>{{$meta->key}} : {{$meta->value}}</small><br />
                                            @endforeach
                                        @endif
                                    </td>
                                    <td class="text-right">
                                            {{ date_in_view($currentAsset->from_date)}}
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="4">No results found.</td>
                            </tr>
                        @endif
                    </table>
                </div>
            </div>
            <h4 class="admin-section-heading">Previously Assigned Assets</h4>
            <div class="panel panel-default">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <tr>
                            <th width="25%">Asset Name</th>
                            <th width="15%">IP Address</th>
                            <th>Other Info</th>
                            <th class="text-right" width="35%">Duration</th>
                        </tr>
                        @if(count($prevoiusAssets) > 0 )
                            @foreach($prevoiusAssets as $prevoiusAsset)
                                <tr style="border-left: 5px solid #00C15F">
                                    <td>
                                        {{ $prevoiusAsset->asset ? $prevoiusAsset->asset->name : '' }}
                                        <br/><small>{{ $prevoiusAsset->asset ? $prevoiusAsset->asset->serial_number : '' }}</small>
                                    </td>
                                    <td>
                                        {{ $prevoiusAsset->ipMapper ? $prevoiusAsset->ipMapper->userIp->ip_address : '' }}
                                    </td>
                                    <td>
                                        @if($prevoiusAsset->asset )
                                            @foreach($prevoiusAsset->asset->metas as $meta)
                                                <small>{{$meta->key}} : {{$meta->value}}</small><br />
                                            @endforeach
                                        @endif
                                    </td>
                                    <td class="text-right">
                                        <span>{{ date_in_view($prevoiusAsset->from_date)}}</span> 
                                        <span class="text-muted">&nbsp; - &nbsp;</span> 
                                        <span>{{ $prevoiusAsset->to_date ? date_in_view($prevoiusAsset->to_date) : '' }}</span>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="4">No results found.</td>
                            </tr>
                        @endif
                    </table>
                </div>
            </div>
        </div>
        <div class="col-sm-4" ng-app="myApp">
            <my-network-devices user-id={{$user->id}}></my-network-devices>
        </div>
    </div>
</div>
@endsection