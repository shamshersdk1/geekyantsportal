@extends(Request::is('*')?'pages.userView.partials.usernav':'pages.userView.partials.nav')
@section('sub-content')
<style>
    td{
        color: #888;
    }
</style>
<div class="dashboard-container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="panel panel-default">
                    <table class="table table-striped">
                        <th>EMI</th>
                        <th>Due Date</th>
                        <th class="text-right">Status</th>
                        @if(count($emis) > 0)
                            @foreach($emis as $emi)
                                <tr>
                                    <td class="td-text">
                                        {{$emi->amount}}
                                    </td>
                                    <td class="td-text">
                                        {{date('j M Y',strtotime($emi->due_date))}}
                                    </td>
                                    <td class="text-right">
                                    @if($emi->status == "paid")
                                        <span class="label label-success custom-label">Paid</span>
                                    @elseif($emi->status == "pending")
                                        <span class="label label-info custom-label">Pending</span>
                                    @endif
                                </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="3">No results found.</td>
                            </tr>
                        @endif
                </table>
            </div>
            @if(count($emis) > 0)
                {{$emis->links()}}
            @endif
        </div>
    </div>
</div>
@endsection