@extends(Request::is('*')?'pages.userView.partials.usernav':'pages.userView.partials.nav')
@section('sub-content')
<script type="text/javascript">
	window.user = '<?php echo json_encode($user); ?>';
</script>
<div class="dashboard-container">
    <div class="row apply-leave" ng-app="myApp" ng-controller="employeeLeaveCtrl">
        <div class="col-xs-12 col-sm-12 col-md-5">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="leave-stat">
                        <div class="num">
                            <span>{{$consumedSickLeaves}}</span>
                            <span class="devider">/</span>
                            <!-- <span>{{ $totalSickLeaves }}</span>  -->
                            <span>5</span>
                        </div>
                        <div class="text">
                            <div class="stat-heading">Sick Leave</div>
                            <div class="stat-info">
                                <div><b>{{$consumedSickLeaves}}</b> Consumed</div>
                                 <div><b>5</b> Available</div> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="leave-stat">
                        <div class="num">
                            <span>{{$consumedPaidLeaves}}</span>
                            <span class="devider">/</span>
                            <span>6</span> 
                        </div>
                        <div class="text">
                            <div class="stat-heading">Paid Leave</div>
                            <div class="stat-info">
                                <div><b>{{$consumedPaidLeaves}}</b> Consumed</div>
                                 <div><b>6</b> Available</div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top:1%">
                <div class="col-md-12 m-t-30">
                    <!-- <form action="/user/leaves/requestLeave" id="user-leave-form" method="post"> -->
                    <form name="leaveForm" ng-submit="form.applyLeave(leaveForm)" novalidate>
                        <div ng-if="success" style="padding:0">
                          <p style="font-size:15px;color: green;"><b>Leave applied successfully</b></p>
                        </div>
                         <h5>APPLY FOR LEAVE</h5>

                        <div class="panel panel-default">

                            <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 paid-unpaid-leave m-b-10">
                                    <div class="form-group ">
                                        <label for="Select Leave Type" class=" control-label remove-padding">Select Leave Type</label>
                                        <select class="col-md-8 form-control remove-width "  name="leave_type"  ng-model="form.data.type" required ng-style="form.error && leaveForm.leave_type.$invalid && {border: '1px solid red'}">
                                            <option ng-repeat="leaveType in leaveTypes" value="%%leaveType.value%%"> %%leaveType.name%% </option>
                                        </select>
                                    </div>
                                    <span class="leave-type-duration" ng-show = "form.error && leaveForm.leave_type.$invalid">
                                        <span ng-show = "leaveForm.leave_type.$error.required">Leave type is required is required.</span>
                                    </span>
                                </div>

                                <!-- ======================== Date Picker ======================= -->
                                <div class="col-md-12 no-padding ma-bot-10 paid-unpaid-leave" ng-show="form.data.type != 'sick'">
                                    <div class="form-group">
                                    <label for="" class="col-md-4 control-label leave-duration-padding">Leave Duration</label>
                                        <div class="col-md-8 increase-width">
                                            <div class='input-group date' id='datetimepickerStart' ng-style="form.error && leaveForm.date_range.$invalid && {border: '1px solid red'}">
                                            <input name="date_range" date-range-picker class="form-control date-picker" type="text" ng-model="datePicker.date" min="datePicker.min" options="datePicker.options" ng-required="true"/><span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <span class="leave-type-duration" ng-show = "form.error && leaveForm.date_range.$invalid">
                                        <span ng-show = "leaveForm.date_range.$error.required">Date range is required.</span>
                                    </span>
                                </div>
                                <!-- ====================== reasones ================ -->
                                <div class="col-md-12 notes-margin ma-top-10 paid-unpaid-leave" ng-show="form.data.type != 'sick'">
                                    <div class="form-group">
                                        <label for="">Notes</label>
                                        <textarea name="reason" id="reason" class="form-control" rows="5" placeholder="Please enter reason for applying leave" ng-model="form.data.reason"   
                                                  required ng-style="leaveForm.reason.$error.required && form.error && {border: '1px solid red'}">
                                                  %%form.data.reason%%</textarea>
                                        <span class="notes-required" ng-show = "form.error && leaveForm.reason.$invalid">
                                            <span ng-show = "leaveForm.reason.$error.required">Reason is required.</span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        <div ng-if="form.error" class="sick-leave col-md-12" style="padding:0">
                          <p style="font-size:15px;color: #DE0000;"><b>  %% form.error_message %% </b></p>
                        </div>
                        <input type="submit" class="btn btn-success btn-crud paid-unpaid-leave" ng-disabled="form.submitting" value="Apply Leave" ng-if="form.data.type != 'sick'" c>
                        <!-- <i ng-show="form.loading" class="fa fa-spinner fa-spin"></i> -->
                        <div ng-show="form.loading" class="sm-loader"></div>

                         <!-- <i ng-show="form.loading" class="fa fa-spinner fa-spin"></i> -->
                    </form>
                <!--Sick leave approve section-->
                <div ng-if="form.data.type == 'sick'" class="sick-leave col-md-12" style="padding:0">
                    <p style="font-size:15px;color: #DE0000;"><b> Please talk to your team leads and reporting manager to apply your sick leave. </b></p>
                    <div class="leave-stat">

                        <div class="text">
                            @if(!empty($data) && !empty($data['reporting_manager_name']))
                                <div class="stat-heading">Reporting manager <span class="pull-right">Contact</span></div>
                                <div class="stat-info">
                                    <div>{{$data['reporting_manager_name']}} <span class="pull-right"> {{$data['reporting_manager_phone']}}</span></div>
                                </div>
                            @else
                                <div class="stat-heading">Reporting manager not available, contact an admin</div>
                            @endif
                        </div>
                    </div>
                    <div class="leave-stat">
                        <div class="text">
                            @if(!empty($data) && !empty($data['team_leads']))
                                <div class="stat-heading">Team Leads <span class="pull-right">Contact</span></div>
                                <div class="stat-info">
                                @foreach($data['team_leads'] as $name => $number)
                                    <div>{{$name}} <span class="pull-right"> {{$number}}</span></div>
                                @endforeach
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            <!--end of Sick leave section-->
                </div>
            </div>
                 </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-7">
            <div class="row bg-white">
                <grid-view model="gridViewModel" columns="gridViewColumns" actions="gridViewActions"></grid-view>
            </div>
        </div>
    </div>
</div>


@endsection
