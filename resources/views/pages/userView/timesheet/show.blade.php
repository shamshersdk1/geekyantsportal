@extends(Request::is('*')?'pages.userView.partials.usernav':'pages.userView.partials.nav')
@section('sub-content')
	<div class="row">
		<h1> {{$selected_date}} </h1>
		<form action="/user/timesheet/detail" method="get">
		<div class="col-sm-6">
			<div class='input-group date' id='datetimepicker1'>
				<input type='text' class="form-control" placeholder="Select Date" id="selected_date" name="selected_date" />
				<span class="input-group-addon">
					<span class="glyphicon glyphicon-calendar"></span>
				</span>
			</div>
		 <button class="btn btn-success crud-btn" type="submit">Check</button>
		</div>
		</form>
        	<div class="col-md-12">
				<form action="/user/timesheet/save" method="post">
				<input name="selected_date" type="hidden" value={{$selected_date}}>
				<input name="user_type" type="hidden" value="user">
					<div class="panel panel-default">
						<table class="table table-striped">
							<thead>
								<tr>
									<th width="25%" class="td-text">Projects</th>
									<th width="15%"> Hours/Approved Hours </th>
									<th width="15%" class="td-text">Approver</th></th>
									<th width="15%" class="td-text">Approval Date</th></th>
									<th width="15%" class="td-text">Status</th></th>
							</thead>
								@if(isset($timesheets))
									@foreach($timesheets as $timesheet)
										<tr>	
									<td width="25%" class="td-text">{{ $timesheet->project->project_name }}</td>
									
									@if(!isset($timesheet->approver))
									<td width="15%"><input type="number" class="form-control" name="hours[{{$timesheet->id}}]" placeholder="0" value={{ $timesheet->hours }}></td>
									<td width="15%" class="td-text"> - </td>
									<td width="15%" class="td-text"> -  </td>
									<td width="15%" class="td-text"> Submitted </td>
									@else
									<td width="15%">{{ $timesheet->hours }} / {{ $timesheet->approved_hours }} </td>
									<td width="15%" class="td-text"> {{ $timesheet->approver->name }} </td>
									<td width="15%" class="td-text"> {{ $timesheet->approval_date }}  </td>
									<td width="15%" class="td-text"> Approved </td>
									@endif
									</tr>
									@endforeach
								@endif
								
						</table>
					</div>
					<button class="btn btn-success crud-btn" type="submit">Save and Submit</button>
				</form>
			</div>
	</div>
	
@endsection