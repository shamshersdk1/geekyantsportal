@extends(Request::is('*')?'pages.userView.partials.usernav':'pages.userView.partials.nav')
@section('sub-content')
@include('pages.userView.timesheet.controls',['start_date' => $start_date, 'end_date' => $end_date])
<script type="text/javascript">
	window.dates = '<?php echo json_encode($dates); ?>';
	window.projects = {{ $projects }};
	window.verticalTotals = '<?php echo json_encode($verticalTotals); ?>';
	window.user = '<?php echo json_encode($user); ?>';
</script>
	<div class="row">
		<div class="container">
			<div class="col-sm-2 text-left" style="margin-top: 5px; margin-left: -40px;">
				<input type="hidden" name="date" id="date" value="2017-12-10">
				<input type="hidden" name="date_end" id="date_end" value="2017-12-17">
			</div>	
		</div>
	</div>
	<div ng-app="myApp" ng-controller="timesheetCtrl" class="row" style="margin-left: 1px; margin-right: 1px;">		
        <div class="col-md-12 user-timesheet">
			<div class="panel panel-default">
				<table class="table table-striped">
					<thead>
						<tr>
							<th width="15%" class="td-text">Projects</th>
	   						<th ng-repeat="date in dates"> 
	   							<div>%% date %%</div>
	   						</th>

							<th width="15%" class="text-center td-text">Total hours</th>
						</tr>
					</thead>
						<tr ng-repeat="project in projects"> 
							<td>
								<a href="/user/timesheet/%%project.id%%"> %%project.name%% </a>
							</td>
							<td ng-repeat="d in project.data">
								<span class="form-control" ng-if="d.is_assigned  ==  false" ng-style=" { 'background-color' : '#00d55f', 'text-align' :'center'}" style="width: 75%; height: 30px;" > N/A </span>
									<span ng-if="d.is_assigned  ==  true" >
								<input ng-style=" { 'background-color' : !(d.is_assigned) ? '#00d55f' : (d.is_holiday) ? '#DFE8FB' : (d.approver_id) ? '#eee' : (d.on_leave) ? '#FF0000' : '' }"
									   type="text" 
                                       class="form-control" 
                                       style="width: 75%; height: 30px;"        
                                       title="text-area" 
                                       ng-disabled=" (d.date > currentDate || !d.is_assigned ) ?  true : false" 
                                       ng-click="addTime(project.name,d,user.name)"
                                       ng-value="d.hours | formatHours: d.hours" />
                              <a href="#" class="tooltip" data-tip="this is the tip!"></a>
							  </span>
                          	</td>	
							<td style="text-align: center"><b>%%project.data | sumByRow: d.hours%%</b></td>
						</tr>
						<tr >
							<td> Daily Totals </td>
							<td style="padding-left: 22px;" ng-repeat="veritcalTotal in verticalTotals track by $index"><b>%%veritcalTotal | formatHours%%  </b></td>
							<td>  </td>
						</tr>
				</table>
			</div>
		</div>
	<script>
		$(function () {
	  		$('[data-toggle="popover"]').popover()
		})
	</script>
	<script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker();
            });
        </script>
@endsection