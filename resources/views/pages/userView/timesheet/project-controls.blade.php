<div class="col-md-6">
    <div class="row">    
        <div class="col-md-4" style="margin-top: 3px;">
            <a  id="previous" class="prev btn btn-default margin-bottom-sm js-filter-date-actions" style="color: #616161;"><i class="fa fa-arrow-left" aria-hidden="true"></i></a> 
            <a  id="next" class="next btn btn-default margin-bottom-sm js-filter-date-actions" style="color: #616161;"><i class="fa fa-arrow-right" aria-hidden="true"></i></a> 
        </div>
       <div class="col-md-8" style="margin-left:-95px;">
            <div class="form-group">
                <div>
                    <input type="text" class="form-control disable-input" value="{{date('D, j M', strtotime($start_date))}} - {{date('D, j M', strtotime($end_date))}}" readonly="true">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="row">
        <!-- <div class="col-xs-1 text-right">
            <div class="green-small-box"></div>
        </div>
        <div class="col-xs-4 pl0">
            <div>Unassigned Project</div>
        </div>
        <div class="col-xs-1 text-right">
            <div class="red-small-box"></div>
        </div>
        <div class="col-xs-6 pl0">
            <div>Leave</div>
        </div> -->
        <div class="onleave pull-right m-r-15">
      <div class="red-small-box inline"><span>On Leave</span></div>
       <div class="small-sat-sun-box inline"><span>Holiday</span></div>
       <div class="free-green-background inline"><span>Free</span></div>
      
    </div>
    </div>
    <div class="row">
        <!-- <div class="col-xs-1 text-right">
            <div class="small-sat-sun-box"></div>
        </div> -->
        <!-- <div class="col-xs-11 pl0">
            <div>Holiday</div>
        </div> -->
    </div>
</div>
<script>
function getQueryString(date)
    {
        var yyyy = date.getFullYear();
        var mm = date.getMonth()+1;
        if(mm < 10)
        {
            mm = '0'+ mm;
        }
        var dd = date.getDate();
        if(dd < 10)
        {
            dd = '0'+ dd;
        }
        return yyyy+'-'+mm+'-'+dd;
    }
$(function(){
    var flag=0;
    var range = 7;
    var url = window.location.pathname;
    var id = url.substring(url.lastIndexOf('/') + 1);
    @if(Request::get('start_date') && Request::get('end_date'))
        var start_date = new Date('{{Request::get('start_date')}}');
        var end_date = new Date('{{Request::get('end_date')}}');
        if(start_date.getDay() == 1 && end_date.getDay() == 0)
        {
            flag = 1;
            $('#dates').val(start_date.toDateString() +" to "+ end_date.toDateString());
             var dates_value = getQueryString(start_date)+' '+'-'+' '+getQueryString(end_date);
             $("#dates_value").val(dates_value);
        }
    @endif
    if(flag == 0)
    {
        var d = new Date();
        var diff = d.getDay() - 1;
        if(diff==-1)
        {
            diff=6;
        }
        var start_date = new Date();
        var end_date = new Date();
        start_date.setDate(d.getDate() - diff);
        end_date.setDate(start_date.getDate() + (range - 1));
        $('#dates').val(start_date.toDateString() +" to "+ end_date.toDateString());
        var dates_value = getQueryString(start_date)+' '+'-'+' '+getQueryString(end_date);
        $("#dates_value").val(dates_value);
    }
    start_date.setDate(start_date.getDate() + range);
    end_date.setDate(end_date.getDate() + range);
    var message = '?start_date='+getQueryString(start_date)+'&end_date='+getQueryString(end_date);
    $('#next').prop(
        'href',
        '/user/timesheet/'+id+message
    );
    start_date.setDate(start_date.getDate() - 2*range);
    end_date.setDate(end_date.getDate() - 2*range);
    var message = '?start_date='+getQueryString(start_date)+'&end_date='+getQueryString(end_date);
    $('#previous').prop(
        'href',
        '/user/timesheet/'+id+message
    );
});

</script>
<br>
<br>