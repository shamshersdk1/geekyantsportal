@extends(Request::is('*')?'pages.userView.partials.usernav':'pages.userView.partials.nav')
@section('sub-content')
@include('pages.userView.timesheet.project-controls',['start_date' => $start_date, 'end_date' => $end_date])
<script type="text/javascript">
   window.dates = '<?php echo json_encode($dates); ?>';
   window.projects = {{ $projects }};
   window.user = '<?php echo json_encode($user); ?>';
</script>
<div class="row">
   <div class="col-md-12">
      <div class="col-sm-2 text-left" style="margin-top: 5px; margin-left: -40px;">
         <!-- <input type="hidden" name="date" id="date" value="2017-12-10">
         <input type="hidden" name="date_end" id="date_end" value="2017-12-17"> -->
      </div>
   
<div class="col-md-12 user-time-sheet">
   <div class="panel panel-default ">
      <div class="panel-heading bg-white text-center">
         <h3>{{$project->project_name}}</h3>
      </div>
      <div class="panel-body " style="padding-top:0; padding-bottom:0">
      <div ng-app="myApp" ng-controller="projectDetailCtrl" class="row project-detail  ">
         <div class=" detail-date col-md-12 flex-class" ng-repeat="detail in projects.data" ng-init="parentIndex = $index">
             <div class="categories">
             <span ng-if="detail.date == '2018-03-12'" class="work flex-class">
               
               </span>
               <span ng-if="detail.on_leave" class="leave flex-class">Leave</span>
               <span ng-if="detail.is_holiday" class="holiday flex-class">holiday</span>
               <span ng-if="!detail.is_assigned" class="free flex-class">free</span>
             </div>
            <h4 class="col-md-4 text-center">
               <!-- <div class="leave-text">Leave</div> -->
             
               %% detail.date | date:'EEE, MMM d' %%
            </h4>
            <div class="col-md-8 description">
               <div class="" ng-repeat="each_task in existingItems[$index] track by $index" >
                
                  <div class="col-md-6 p-l-0">
                     <div class="form-group">
                        <input type="text" ng-change="changed(parentIndex,$index,detail)" ng-disabled=" (detail.date > currentDate || !detail.is_assigned || detail.approver_id )  ?  true : false" class="%%(each_task.error && !each_task.reason) ?  'form-control light-placeholder error' : 'form-control input-description light-placeholder'%%"
                           placeholder="Description" ng-model="each_task.reason" required="true" ng-model-options="{allowInvalid: true, updateOn: 'default', debounce: 1000 }">
                     </div>
                  </div>
                  <div class="input-group col-md-4">
                     <input type="number" ng-change="changed(parentIndex,$index,detail)" ng-disabled="  (detail.date > currentDate || !detail.is_assigned || detail.approver_id )  ?  true : false" class="%%(each_task.error && !each_task.duration) ?  'form-control error' : 'form-control input-index-detail'%%"
                        step="00.5" max="24" min="0" placeholder="Hours" ng-model="each_task.duration" ng-model-options="{allowInvalid: true, updateOn: 'default', debounce: 100 }"
                        required="true">
                     <div ng-if="$index != (existingItems[$parent.$index].length)-1" class="input-group-btn">
                        <a href="javascript:void(0);"  ng-disabled=" (detail.approver_id) ?  true : false"
                           class="btn btn-default" ng-click="!detail.approver_id && removeDateRange(parentIndex,$index, detail)">
                        <i ng-class="{'fa fa-remove text-danger' : (!detail.approver_id)}"></i>
                        <i ng-class="{'glyphicon glyphicon-lock' : detail.approver_id}"></i>
                        </a>
                     </div>
                     <div ng-if="$index == (existingItems[$parent.$index].length)-1" class="input-group-btn">
                        <a ng-if="$index != 0" href="javascript:void(0);"  ng-disabled=" (detail.approver_id) ?  true : false"
                           class="btn btn-default" ng-click="!detail.approver_id && removeDateRange(parentIndex,$index, detail)">
                        <i ng-class="{'fa fa-remove text-danger' : (!detail.approver_id)}"></i>
                        <i ng-class="{'glyphicon glyphicon-lock' : detail.approver_id}"></i>
                        <a href="javascript:void(0);" ng-hide=" (detail.date > currentDate || !detail.is_assigned || detail.approver_id ) ?  true : false" 
                           class="btn btn-default" ng-click="addDateRange(parentIndex,$index)">
                        <i ng-class="{'fa fa-plus' : (!detail.approver_id)}"></i>
                        </a>
                        
                  
                     </div>
                     <div class="spinner" ng-if="loading[detail.date][$index]">
                        <i class="fa fa-spinner fa-spin"></i>
                  </div>
                   
                  </div>
                 
               </div>
               <!-- <div ng-if="loading[parentIndex]">
                        Loading...
                  </div> -->
               
            </div>
         </div>
      </div>
</div>
   </div>
</div>
</div>
</div>
<script>
   $(function () {
    		$('[data-toggle="popover"]').popover()
   })
</script>
<script type="text/javascript">
   $(function () {
       $('#datetimepicker1').datetimepicker();
   });
</script>
@endsection