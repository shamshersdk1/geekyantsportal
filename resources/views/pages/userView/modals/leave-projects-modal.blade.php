<style>
#leaveProjectsModal .modal-dialog {
    width: 100%;
    margin: 0;
    padding: 0;
    right:0;
    bottom:0;
    display: inline-block;
}
#leaveProjectsModal .modal-body {
    height: 86vh;
    overflow-y: auto;
    overflow-x: auto;
}
#leaveProjectsModal .modal-header {
    height: 7vh;
}
#leaveProjectsModal .modal-footer {
    height: 7vh;
}
#leaveProjectsModal .modal {
    text-align: center;
}
</style>
<div class="modal fade modal-fullscreen" id="leaveProjectsModal" tabindex="-1" role="dialog" aria-labelledby="leaveProjectsModalLabel">
	<form class="form-horizontal" id="reasons-form" name="modal-projects-form" enctype="multipart/form-data">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" style="float:left;display:block" id="leaveProjectsModalLabel"> Overlapping Leave</h4>
                </div>
                <div class="modal-body loading" style="min-height: 543px;">
                    <div class="col-md-2 col-md-offset-5">
                        <i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
                    </div>
                </div>
                <div class="modal-body content">
                </div>
                <div class="modal-footer" style="text-align:right">
                    <span>
                        <button type="submit" class="btn btn-success">
                            Send Request
                        </button>
                    </span>
                </div>
            </div>
        </div>
    </form>
</div>