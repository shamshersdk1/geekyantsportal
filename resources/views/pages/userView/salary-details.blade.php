@extends(Request::is('*')?'pages.userView.partials.usernav':'pages.userView.partials.nav')
@section('sub-content')
<div class="dashboard-container">
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		            @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
            @endif
            @if(session('alert-class'))
                <div class="alert alert-danger">
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ session('alert-class') }}</span><br/>
		        </div>
            @endif
	    </div>
	</div>
    <div class="bg-white">
        <ul class="nav nav-tabs nav-tabs-user" role="tablist">
            <li role="presentation" class="active">
                <a href="#salary-details" aria-controls="salary-details" role="tab" data-toggle="tab">Salary Breakdown</a>
            </li>
            <li role="presentation">
                <a href="#tds" aria-controls="payslip" role="tab" data-toggle="tab">TDS Breakdown</a>
            </li>
            <li role="presentation">
                <a href="#payslip" aria-controls="payslip" role="tab" data-toggle="tab">Payslips</a>
            </li>
        </ul>

        <div class="tab-content">
            <div id="salary-details" role="tabpanel" class="tab-pane active">
                <!-- Salary Related Data -->
                <!-- To be removed from here -->
                <div class="panel panel-default">
            		<div class="table-responsive">
                        <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <th>Month</th>
                                @foreach($monthsArr as $month)
                                    <th>{{date('M, Y', strtotime($month['year'].'-'.$month['month']))}}</th>
                                @endforeach
                            </tr> 
                            <tr>
                                <th>Worked Days</th>
                                @foreach($monthsArr as $month)
                                    <th>{{$tdsData[$month['year'].'_'.$month['month']]['worked_days'] ?? '-'}}</th>
                                @endforeach
                            </tr> 
                            @foreach($appraisalCompType as $componenet)
                                    <tr>      
                                        <td>{{$componenet->name}}</td>  
                                        @foreach($monthsArr as $month)
                                            @if($componenet->id == $tdsId && (($month['month'] >= $currMonth) || ($month['year'] > $financialYearObj->year)))
                                               @if(isset($tdsSummary[$monthObj['year'].'_'.$monthObj['month']]['tds-for-the-month']))
                                                    <td>{{custom_round($tdsSummary[$monthObj['year'].'_'.$monthObj['month']]['tds-for-the-month'])}}</td>
                                                @elseif(isset($tdsSummary[$monthObj['year'].'_'.$monthObj['month']]['tds']))
                                                    <td>{{custom_round($tdsSummary[$monthObj['year'].'_'.$monthObj['month']]['tds'])}}</td>
                                                @else 
                                                    <td>0</td>
                                                @endif 
                                            @else
                                                <td>{{$tdsData[$month['year'].'_'.$month['month']]['appraisal_components'][$componenet->id] ? custom_round($tdsData[$month['year'].'_'.$month['month']]['appraisal_components'][$componenet->id]) : '-'}}</td>
                                            @endif    
                                        @endforeach
                                    </tr>
                                
                            @endforeach
                            <tr>
                                <th>Monthly Gross Salary</th>
                                @foreach($monthsArr as $month)
                                    <th>{{$tdsData[$month['year'].'_'.$month['month']]['monthly_gross_salary'] ? custom_round($tdsData[$month['year'].'_'.$month['month']]['monthly_gross_salary']) : '-' }}</th>
                                @endforeach
                            </tr>
                            @if($tdsData['fl_deduct_head'] == 1)
                            <tr>
                                <th colspan="13"><b>DEDUCTION</b></th>
                            </tr>   
                            @if($tdsData['fl_deduct_vpf'] == 1)                         
                            <tr>
                                <td>Vpf</td>
                                @foreach($monthsArr as $month)
                                    <td>{{ $tdsData[$month['year'].'_'.$month['month']]['vpf'] ? custom_round($tdsData[$month['year'].'_'.$month['month']]['vpf']) : '-' }}</td>
                                @endforeach
                            </tr>
                            @endif
                            @if($tdsData['fl_deduct_food_deduction'] == 1)                         
                            <tr>
                                <td>Food Deduction</td>
                                @foreach($monthsArr as $month)
                                    <td>{{ $tdsData[$month['year'].'_'.$month['month']]['food_deduction'] ? custom_round($tdsData[$month['year'].'_'.$month['month']]['food_deduction']) : '-' }}</td>
                                @endforeach
                            </tr>
                            @endif
                            @if($tdsData['fl_deduct_insurance'] == 1)                         
                            <tr>
                                <td>Insurance</td>
                                @foreach($monthsArr as $month)
                                    <td>{{ $tdsData[$month['year'].'_'.$month['month']]['insurance'] ? custom_round($tdsData[$month['year'].'_'.$month['month']]['insurance']) : '-' }}</td>
                                @endforeach
                            </tr>
                            @endif
                            @if($tdsData['fl_deduct_loan'] == 1)                         
                            <tr>
                                <td>Loan EMI</td>
                                @foreach($monthsArr as $month)
                                    <td>{{$tdsData[$month['year'].'_'.$month['month']]['loan'] ? custom_round($tdsData[$month['year'].'_'.$month['month']]['loan']) : '-' }}</td>
                                @endforeach
                            </tr>
                            @endif
                            @endif
                            @if($tdsData['fl_appr_bonus_head'] == 1)
                                <tr>
                                    <th colspan="13">APPRAISAL BONUSES</th>
                                </tr>
                                @foreach($appraisalBonusType as $bonus)
                                    @if($tdsData['fl_appr_bonus' . $bonus->id] == 1)
                                        <tr>     
                                            <td>{{$bonus->description}}</td>  
                                            @foreach($monthsArr as $month)
                                                <td>{{$tdsData[$month['year'].'_'.$month['month']]['appraisal_bonuses'][$bonus->id] ? custom_round($tdsData[$month['year'].'_'.$month['month']]['appraisal_bonuses'][$bonus->id]) : '-' }}</td>
                                            @endforeach
                                        </tr>
                                    @endif
                                @endforeach
                            @endif
                            @if($tdsData['fl_appr_bonus_other'] == 1)
                                <tr>
                                    <td>Other Appraisal Bonus</td>
                                    @foreach($monthsArr as $month)
                                        <td>{{$tdsData[$month['year'].'_'.$month['month']]['appraisal_bonuses']['other'] ? custom_round($tdsData[$month['year'].'_'.$month['month']]['appraisal_bonuses']['other']) : '-' }}</td>
                                    @endforeach
                                </tr>
                            @endif
                            @if($tdsData['fl_bonus_head'] == 1)
                            <tr>
                                <th colspan="13"><b>OTHER BONUSES</b></th>
                            </tr>
                            @if($tdsData['fl_bonus_onsite'] == 1)
                                <tr>
                                    <td>Onsite</td>
                                    @foreach($monthsArr as $month)
                                        <td>{{$tdsData[$month['year'].'_'.$month['month']]['bonuses']['onsite'] ? custom_round($tdsData[$month['year'].'_'.$month['month']]['bonuses']['onsite']) : '-' }}</td>
                                    @endforeach
                                </tr>
                            @endif
                            @if($tdsData['fl_bonus_additional'] == 1)
                                <tr>
                                    <td>Additional Workday</td>
                                    @foreach($monthsArr as $month)
                                        <td>{{$tdsData[$month['year'].'_'.$month['month']]['bonuses']['additional'] ? custom_round($tdsData[$month['year'].'_'.$month['month']]['bonuses']['additional']) : '-' }}</td>
                                    @endforeach
                                </tr>
                            @endif
                            @if($tdsData['fl_bonus_extra_hour'] == 1)
                                <tr>
                                    <td>Extra Hour Bonus</td>
                                    @foreach($monthsArr as $month)
                                        <td>{{$tdsData[$month['year'].'_'.$month['month']]['bonuses']['extra'] ? custom_round($tdsData[$month['year'].'_'.$month['month']]['bonuses']['extra']) : '-' }}</td>
                                    @endforeach
                                </tr>
                            @endif
                            @if($tdsData['fl_bonus_tech_talk'] == 1)
                                <tr>
                                    <td>Tech Talk</td>
                                    @foreach($monthsArr as $month)
                                        <td>{{$tdsData[$month['year'].'_'.$month['month']]['bonuses']['tech_talk'] ? custom_round($tdsData[$month['year'].'_'.$month['month']]['bonuses']['tech_talk']) : '-' }}</td>
                                    @endforeach
                                </tr>
                            @endif
                            @if($tdsData['fl_bonus_performance'] == 1)
                                <tr>
                                    <td>Performance</td>
                                    @foreach($monthsArr as $month)
                                        <td>{{$tdsData[$month['year'].'_'.$month['month']]['bonuses']['performance'] ? custom_round($tdsData[$month['year'].'_'.$month['month']]['bonuses']['performance']) : '-' }}</td>
                                    @endforeach
                                </tr>
                            @endif
                            @if($tdsData['fl_bonus_referral'] == 1)
                                <tr>
                                    <td>Referral Bonus</td>
                                    @foreach($monthsArr as $month)
                                        <td>{{$tdsData[$month['year'].'_'.$month['month']]['bonuses']['referral'] ? custom_round($tdsData[$month['year'].'_'.$month['month']]['bonuses']['referral']) : '-' }}</td>
                                    @endforeach
                                </tr>
                            @endif
                            @if($tdsData['fl_bonus_other'] == 1)
                                <tr>
                                    <td>Other</td>
                                    @foreach($monthsArr as $month)
                                        <td>{{$tdsData[$month['year'].'_'.$month['month']]['bonuses']['other'] ? custom_round($tdsData[$month['year'].'_'.$month['month']]['bonuses']['other']) : '-' }}</td>
                                    @endforeach
                                </tr>
                            @endif
                            @endif
                            @if($tdsData['fl_adhoc_pay_head'] == 1)
                                <tr>
                                    <th colspan="13"><b>ADHOC PAYMENTS</b></th>
                                </tr>
                                @foreach($adhocPaymentComponents as $adhocPaymentComponent)
                                    @if($tdsData['fl_adhoc_pay' . $adhocPaymentComponent->id] == 1)
                                        <tr>      
                                            <td>{{$adhocPaymentComponent->description}}</td> 
                                            @foreach($monthsArr as $month)
                                                <td>{{$tdsData[$month['year'].'_'.$month['month']]['adhoc_payments'][$adhocPaymentComponent->id] ? custom_round($tdsData[$month['year'].'_'.$month['month']]['adhoc_payments'][$adhocPaymentComponent->id]) : '-' }}</td>
                                            @endforeach
                                        </tr>
                                    @endif
                                @endforeach
                            @endif
                        </tbody>
                        </table>
                    </div>
                </div>
                <!-- To be removed till here -->
            </div>
            <div id="tds" role="tabpanel" class="tab-pane">
                <div class="panel panel-default">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <th><h6>Month</h6></th>
                                    @foreach($monthsArr as $month)
                                        <th>{{date('M, Y', strtotime($month['year'].'-'.$month['month']))}}</th>
                                    @endforeach

                                </tr>
                                <tr><th colspan="13"><h5>INCOME</h5></th></tr>
                                <tr>
                                    <th>Gross Annual Salary</th>
                                    @foreach($monthsArr as $month)
                                        <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['gross-annual-salary'] ?? 0)}}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Previous Employment Salary</th>
                                    @foreach($monthsArr as $month)
                                        @if(isset($tdsSummary[$month['year'].'_'.$month['month']]['previous-employment-salary']))
                                            <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['previous-employment-salary'] ?? 0)}}</td>
                                        @elseif(isset($tdsSummary[$month['year'].'_'.$month['month']]['income-paid']))
                                            <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['income-paid'] ?? 0)}}</td>
                                        @else 
                                            <td>0</td>
                                        @endif
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Loan Interest Income</th>
                                    @foreach($monthsArr as $month)
                                        <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['loan-interest-income'] ?? 0)}}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>CTC Bonus</th>
                                    @foreach($monthsArr as $month)
                                        @if(isset($tdsSummary[$month['year'].'_'.$month['month']]['ctc-bonus']))
                                            <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['ctc-bonus'] ?? 0)}}</td>
                                        @elseif(isset($tdsSummary[$month['year'].'_'.$month['month']]['annual-appraisal-bonus']))
                                            <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['annual-appraisal-bonus'] ?? 0)}}</td>
                                        @else 
                                            <td>0</td>
                                        @endif
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Other Bonus</th>
                                    @foreach($monthsArr as $month)
                                        @if(isset($tdsSummary[$month['year'].'_'.$month['month']]['other-bonus']))
                                            <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['other-bonus'] ?? 0)}}</td>
                                        @elseif(isset($tdsSummary[$month['year'].'_'.$month['month']]['gross-other-bonus']))
                                            <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['gross-other-bonus'] ?? 0)}}</td>
                                        @else 
                                            <td>0</td>
                                        @endif
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Total Income for the Year</th>
                                    @foreach($monthsArr as $month)
                                    <td>
                                        {{custom_format(
                                            $tdsSummary[$month['year'].'_'.$month['month']]['gross-annual-salary'] + 
                                            $tdsSummary[$month['year'].'_'.$month['month']]['previous-employment-salary'] + 
                                            $tdsSummary[$month['year'].'_'.$month['month']]['income-paid'] +
                                            $tdsSummary[$month['year'].'_'.$month['month']]['ctc-bonus'] + 
                                            $tdsSummary[$month['year'].'_'.$month['month']]['annual-appraisal-bonus'] +
                                            $tdsSummary[$month['year'].'_'.$month['month']]['other-bonus'] + 
                                            $tdsSummary[$month['year'].'_'.$month['month']]['gross-other-bonus'] +
                                            $tdsSummary[$month['year'].'_'.$month['month']]['loan-interest-income'] 
                                        ?? 0)}}
                                    </td>
                                    @endforeach
                                </tr>

                                <tr><th colspan="13"><h5>DEDUCTION</h5></th></tr>
                                <tr>
                                    <th>HRA Exemption</th>
                                    @foreach($monthsArr as $month)
                                    <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['hra-exemption'])}}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Standard Deduction</th>
                                    @foreach($monthsArr as $month)
                                        <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['standard-deduction'] ?? 0)}}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Employee IT Declared u/s. 80C, limited to Rs.1.5L</th>
                                    @foreach($monthsArr as $month)
                                        <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['it-saving-investment'] ?? 0)}}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Other Employee IT Declared</th>
                                    @foreach($monthsArr as $month)
                                        <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['it-saving-deduction'] ?? 0)}}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Gross Professional Tax</th>
                                    @foreach($monthsArr as $month)
                                        <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['gross-professional-tax'] ?? 0)}}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Total Deduction</th>
                                    @foreach($monthsArr as $month)
                                        <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['hra-exemption'] + $tdsSummary[$month['year'].'_'.$month['month']]['gross-professional-tax'] + $tdsSummary[$month['year'].'_'.$month['month']]['it-saving-deduction'] + $tdsSummary[$month['year'].'_'.$month['month']]['it-saving-investment'] + $tdsSummary[$month['year'].'_'.$month['month']]['standard-deduction'] ?? 0)}}</td>
                                    @endforeach
                                </tr>

                                <tr><th colspan="13"><h5>TAX CALCULATION</h5></th></tr>
                                <tr>
                                    <th>Net Taxable Income for the Year</th>
                                    @foreach($monthsArr as $month)
                                        @if(isset($tdsSummary[$month['year'].'_'.$month['month']]['net-taxable-income']))
                                            <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['net-taxable-income'] ?? 0)}}</td>
                                        @elseif(isset($tdsSummary[$month['year'].'_'.$month['month']]['net-taxable-salary']))
                                            <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['net-taxable-salary'] ?? 0)}}</td>
                                        @else 
                                            <td>0</td>
                                        @endif
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Rebate if Net Taxable Income upto Rs. 5 Lakhs</th>
                                    @foreach($monthsArr as $month)
                                        <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['rebate'] ?? 0)}}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Annual Income Tax</th>
                                    @foreach($monthsArr as $month)
                                        @if(isset($tdsSummary[$month['year'].'_'.$month['month']]['annual-income-tax']))
                                            <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['annual-income-tax'] ?? 0)}}</td>
                                        @elseif(isset($tdsSummary[$month['year'].'_'.$month['month']]['gross-tds']))
                                            <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['gross-tds'] ?? 0)}}</td>
                                        @else 
                                            <td>0</td>
                                        @endif
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Taxable Income after Rebate</th>
                                    @foreach($monthsArr as $month)
                                        @if(isset($tdsSummary[$month['year'].'_'.$month['month']]['annual-income-tax-after-rebate']))
                                            <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['annual-income-tax-after-rebate'] ?? 0)}}</td>
                                        @elseif(isset($tdsSummary[$month['year'].'_'.$month['month']]['taxable-income-after-rebate']))
                                            <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['taxable-income-after-rebate'] ?? 0)}}</td>
                                        @else 
                                            <td>0</td>
                                        @endif
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Annual Edu Cess Payable</th>
                                    @foreach($monthsArr as $month)
                                        <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['edu-cess'] ?? 0)}}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Total Annual Income Tax Payable</th>
                                    @foreach($monthsArr as $month)
                                        @if(isset($tdsSummary[$month['year'].'_'.$month['month']]['total-annual-income-tax-payable']))
                                            <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['total-annual-income-tax-payable'] ?? 0)}}</td>
                                        @elseif(isset($tdsSummary[$month['year'].'_'.$month['month']]['taxable-payable']))
                                            <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['taxable-payable'] ?? 0)}}</td>
                                        @else 
                                            <td>0</td>
                                        @endif
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>TDS Already deducted till date</th>
                                    @foreach($monthsArr as $month)
                                        <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['tds-paid-till-date'] ?? 0)}}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Balance Tax Payable</th>
                                    @foreach($monthsArr as $month)
                                        <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['balance-tax-payable'] ?? 0)}}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>TDS(for the month)</th>
                                    @foreach($monthsArr as $month)
                                        @if(isset($tdsSummary[$month['year'].'_'.$month['month']]['tds-for-the-month']))
                                            <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['tds-for-the-month'] ?? 0)}}</td>
                                        @elseif(isset($tdsSummary[$month['year'].'_'.$month['month']]['tds']))
                                            <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['tds'] ?? 0)}}</td>
                                        @else 
                                            <td>0</td>
                                        @endif
                                    @endforeach
                                </tr>                            
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div id="payslip" role="tabpanel" class="tab-pane">
                <div class="panel panel-default">
                    <!-- Payslip Data -->
                    <!-- To be removed from here -->
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <tr>
                                <th>Month</th>                 
                                <th class="text-right">Action</th>
                            </tr>
                            @foreach ($payslips as $payslip)
                            <tr>
                                <td>{{$payslip->month->formatMonth()}}</td>
                                <td class="text-right">
                                    <a href="/user/payslip/{{$payslip->month->id}}" class="btn btn-primary" target="_blank"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i> Payslip</a>
                                    <a href="/user/payslip/{{$payslip->month->id}}/download" class="btn btn-primary"><i class="fa fa-download btn-icon-space" aria-hidden="true"></i> Payslip</a>
                                    @if($payslip->month_id > 18)
                                        <a href="/user/tds-breakdown/{{$payslip->month->id}}" class="btn btn-primary" target="_blank"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i> TDS Breakdown</a>
                                        <a href="/user/tds-breakdown/{{$payslip->month->id}}/download" class="btn btn-primary"><i class="fa fa-download btn-icon-space" aria-hidden="true"></i> TDS Breakdown</a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                    <!-- To be removed till here -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection