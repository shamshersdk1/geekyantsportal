@extends(Request::is('*')?'pages.userView.partials.usernav':'pages.userView.partials.nav')
@section('sub-content')
<script type="text/javascript">
   window.feedbackMonthDetails = '<?php echo json_encode($feedbackMonthDetails); ?>';
   window.user = '<?php echo json_encode($user); ?>';
</script>
<div ng-app="myApp" ng-controller="userFeedbackCtrl" class="dashboard-container">
   <div class="row">
      <div class="col-sm-6">
         <div class="panel panel-default">
            <table class="table">
               <tr>
                  <th>Month</th>
                  <th>Rating</th>
                  <th>Monthly Variable</th>
                  <th>Variable Pay</th>
                  <th>Status</th>
                  <th></th>
               </tr>
               <tr ng-repeat="monthRow in feedbackMonthDetails">
                  <td>
                     %%monthRow.month%% / %%monthRow.year%%
                  </td>
                  <td>
                     <i ng-repeat="n in [] | range: monthRow.rating" class="fa fa-star text-success"></i>
                     <i ng-repeat="n in [] | range: 10 - monthRow.rating" class="fa fa-star text-muted"></i>
                     %%monthRow.rating%%
                  </td>
                  <td>
                     %%monthRow.monthly_variable%%
                  </td>
                  <td>
                     %%monthRow.variable_pay%%
                  </td>
                  <td><label class="label label-warning">Due</label></td>
                  <td class="text-right">
                     <button ng-click="loadReviews(monthRow)" class="btn btn-success btn-sm"><i class="fa fa-eye"></i></button>
                  </td>
               </tr>
               <tr ng-show="feedbackMonthDetails.length < 1">
                <td colspan="6" >No records found</td>
               </tr>
            </table>
         </div>
      </div>
      <div class="col-sm-6" ng-if="review_details.length > 0">
         <div class="panel panel-default">
            <div class="panel-body">
               <div class="row">
                  <div class="col-sm-4">
                     <h5 style="margin:0">%%selected_month.month%% / %%selected_month.year%%</h5>
                  </div>
                  <div class="project-rating-star col-sm-5" style="margin:0" star-rating rating="selected_month.rating" read-only="true" max-rating="10"></div>
                  <div class="col-sm-3">
                     <h5 style="margin:0">%%selected_month.variable_pay%% / %%selected_month.monthly_variable%%</h5>
                  </div>
               </div>

            <!-- <ng-container ng-repeat="row in review_details">
               <li class="list-group-item" style="background:#D4DCE6"><b>Project: %%row.project.project_name%%</b></li>
               <div ng-repeat="reviewer in row.project.reviewer">
                   <li class="list-group-item"><b>Reviewer: %%reviewer.name%%</b> </li>
                   <li class="list-group-item" ng-repeat="project_review in reviewer.project_reviews">
                       <p>%%project_review.question%%<span style="font-size: 11px">( as <span class="small label-%%project_review.role_type.code%%">%%project_review.role_type.name%%</span> )</span></p>
                       <div class="project-rating-star" star-rating rating="project_review.rating" read-only="true" max-rating="10"></div>
                   </li>
               </div>
               </ng-container> -->
            <div class="user-wrap">
               <ng-container ng-repeat="row in review_details">
                  <uib-accordion close-others="oneAtATime" >
                     <div uib-accordion-group class="panel-default user-feedback" is-open="status.open" >
                        <uib-accordion-heading>%%row.project.project_name%% <i class="fa fa-chevron-down pull-right"></i></uib-accordion-heading>
                        <div ng-repeat="reviewer in row.project.reviewer">
                           <uib-accordion close-others="oneAtATime">
                              <div uib-accordion-group class="panel-default" is-open="status2.open" >
                                 <uib-accordion-heading>
                                    <div class="row">
                                       <div class="col-sm-5">
                                          %%reviewer.name%% <label class="label label-primary">Team Lead</label>  
                                       </div>
                                       <div class="col-sm-5">
                                          <span class="project-rating-star pull-right" style="margin:0" star-rating rating="project_review.rating" read-only="true" max-rating="10"></span>
                                       </div>
                                       <div class="col-sm-2">
                                          <i class="fa fa-chevron-down pull-right"></i>
                                       </div>
                                    </div>
                                 </uib-accordion-heading>
                                 <!-- <li class="list-group-item"><b>Reviewer: %%reviewer.name%%</b> </li> -->
                                 <div ng-repeat="project_review in reviewer.project_reviews">
                                    <p>%%project_review.question%%<span style="font-size: 11px">( as <span class="small label-%%project_review.role_type.code%%">%%project_review.role_type.name%%</span> )</span></p>
                                    <div class="project-rating-star" star-rating rating="project_review.rating" read-only="true" max-rating="10"></div>
                                 </div>
                              </div>
                           </uib-accordion>
                        </div>
                     </div>
                  </uib-accordion>
               </ng-container>
            </div>
         </div>
      </div>
      <div class="col-sm-6" ng-if="review_details.length < 1 && selected_month ">
            <div class="panel panel-default">
               <div class="panel-body">
                   No records found for the month of %%selected_month.month%% / %%selected_month.year%%
               </div>
            </div>
      </div>
   </div>
</div>
@endsection