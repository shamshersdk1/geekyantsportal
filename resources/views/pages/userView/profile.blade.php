@extends(Request::is('*')?'pages.userView.partials.usernav':'pages.userView.partials.nav')
@section('sub-content')
<div class="dashboard-container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="profile-grid">
                <div class="panel panel-default profile-grid-item m-b-0">
                    <div class="panel-heading">INTRODUCTION</div>
                    
                    <div class="panel-body">
                    <p align="justify" class="sm-text">  @if(!empty($user->profile->intro)){{$user->profile->intro}}@endif</p>
                    </div>
                </div>
                <div class="panel panel-default profile-grid-item m-r-15 m-b-0">
                    <div class="panel-heading">SKILLS</div>
                    <div class="panel-body">
                    @if($keywords!="")
                    @foreach($keywords as $keyword)
                    <p type="button" class="btn btn-primary" style="background-color:transparent;border-color:#B8B7BC;padding:1% 3% 1% 3%;font-size:1em;color:#5A5665;border-radius:25px;margin-top:0;">{{$keyword}}</p>
                    @endforeach
                    @endif
                    </div>
                </div>
                <div class="panel panel-default profile-grid-item m-b-0">
                    <div class="panel-heading">EXPERIENCE</div>
                    <div class="panel-body">
                        @if($skills!="")
                        <ul style="padding-left:0;" class="arrow-list">
                        @foreach($skills as $skill)
                        <li style="margin-bottom:1%;font-size:1em;color:#5A5665;">{{$skill['skill'] }}<span class="label" style="background-color:{{$colours[rand(0,count($colours)-1)]}};height:15px;line-height:12px;margin-left:5px;">{{ $skill['percentage']}} %</span></li>
                        @endforeach
                        </ul>
                        @endif
                    </div>
                </div>
                <div class="panel panel-default profile-grid-item m-r-15 m-b-0">
                    <div class="panel-heading">INTEREST & EXPERTISE</div>
                    <div class="panel-body">
                        @if($knowledge!="")
                        <ul class="two-part-list arrow-list" style="padding-left:0;">
                        @foreach($knowledge as $item)
                        <li style="margin-bottom:1%;font-size:1em;color:#aaa; font-weight:300">{{$item}}</li>
                        @endforeach
                        </ul>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if(Request::is('admin/users/*'))
    <div class="fab">
        <a href="{{Request::url()}}/edit" style="color:white"><i class="fa fa-pencil"></i></a>
    </div>
    @endif  
</div>
@endsection

