@extends(Request::is('*')?'pages.userView.partials.usernav':'pages.userView.partials.nav')
@section('sub-content')
<div class="dashboard-container">
    <div class="panel panel-default" style="overflow:auto">
    <div class="panel-heading bg-white" >All Birthdays</div>
    <div class="panel-body p-0 ">
        <table class="table table-striped">
            <th>Name</th>
            <th class="text-right">Birthday</th>
            @if(empty($bdays))
                <tr>
                    <td>
                        Data unavailable
                    </td>
                </tr>
                @else
                @foreach($bdays as $array)
                @if($array['active']==1)
                    <tr style="border-left: 5px solid #269900">
                @else  
                    <tr>
                @endif
                    <td style="line-height:2em">
                        {{$array['user']->name}}
                    </td>
                    <td class="text-right" style="line-height:2em">
                        {{date('j M',strtotime($array['user']->dob))}}
                    </td>
                </tr>
                @endforeach
                @endif
        </table>
        </div>
    </div>
</div>
@endsection