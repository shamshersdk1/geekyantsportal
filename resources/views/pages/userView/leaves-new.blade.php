@extends(Request::is('*')?'pages.userView.partials.usernav':'pages.userView.partials.nav')
@section('sub-content')
<script type="text/javascript">
    window.user = '<?php echo json_encode($user); ?>';
    window.optionalLeaves = <?php echo json_encode($optionalLeaves); ?>;
    window.userOptionalLeave = <?php echo json_encode($userOptionalLeave); ?>;

</script>
<div class="dashboard-container" ng-app="myApp" ng-controller="employeeLeaveNewCtrl">
    <div class="apply-leave">
        <div class="row m-b hidden">
            <div class="col-sm-6">
                <div class="leave-stat">
                    <div class="num">
                        <span>{{$consumedSickLeaves}}</span>
                        <span class="devider">/</span>

                        <span>{{ $totalSickLeaves }}</span>
                    </div>
                    <div class="text">
                        <div class="stat-heading">Sick Leave</div>
                        <div class="stat-info">
                            <div><b>{{$consumedSickLeaves}}</b> Consumed</div>
                            <div><b>{{ $totalSickLeaves - $consumedSickLeaves}}</b> Available</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="leave-stat">
                    <div class="num">
                        <span>{{$consumedPaidLeaves}}</span>
                        <span class="devider">/</span>
                        <span>{{$totalPaidLeaves}}</span>
                    </div>
                    <div class="text">
                        <div class="stat-heading">
                            Paid Leave
                            <span class="label label-primary custom-label pull-right carry-leaves" ng-if="$carryForwardLeaves" placement="top" uib-tooltip="You have {{ $carryForwardLeaves }} carry forward leave(s) which has been added to your Paid leaves">
                                <i class="fa fa-info"></i>
                            </span>
                        </div>
                        <div class="stat-info">
                            <div><b>{{$consumedPaidLeaves}}</b> Consumed</div>
                            <div><b>{{$totalPaidLeaves - $consumedPaidLeaves}}</b> Available</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <h4 class="admin-section-heading">Balance leaves</h4>
        <div class="m-b">
            <div class="leave-number">
                <div><small>Paid</small><h4>{{$userRemainingLeaveDetail['paid']}}</h4></div>
                <div><small>Sick</small><h4>{{$userRemainingLeaveDetail['sick']}}</h4></div>
                <div><small>Optional</small><h4>{{$userRemainingLeaveDetail['optional-holiday']}}</h4></div>
                <div><small>Marriage</small><h4>{{$userRemainingLeaveDetail['marriage']}}</h4></div>
                <div><small>Emergency</small><h4>{{$userRemainingLeaveDetail['emergency']}}</h4></div>
                <div><small>Paternity</small><h4>{{$userRemainingLeaveDetail['paternity']}}</h4></div>
                <div><small>Maternity</small><h4>{{$userRemainingLeaveDetail['maternity']}}</h4></div>
                <div><small>Comp Off</small><h4>{{$userRemainingLeaveDetail['comp-off']}}</h4></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">Apply for Leave</h4>
                    </div>
                    <div class="panel-body">
                        <!-- <form action="/user/leaves/requestLeave" id="user-leave-form" method="post"> -->
                        <form name="leaveForm" ng-submit="form.applyLeave(leaveForm)" novalidate class="form-horizontal">
                            <div ng-if="success" class="alert alert-success">
                              <div>Leave applied successfully</div>
                            </div>

                            <div class="form-group paid-unpaid-leave m-b-5">
                                <div class="col-sm-5">
                                    <div class="clearfix">
                                        <label for="Select Leave Type">Select Leave Type</label>
                                        <select class="col-md-8 form-control" name="leave_type"  ng-model="form.data.type" required ng-style="form.error && leaveForm.leave_type.$invalid && {border: '1px solid red'}">
                                            <option ng-repeat="leaveCategory in leaveCategories" value="%%leaveCategory.code%%"> %%leaveCategory.title%% </option>
                                        </select>
                                    </div>
                                    <div class="small text-danger" ng-show="form.error && leaveForm.leave_type.$invalid">
                                        <span ng-show="leaveForm.leave_type.$error.required">Leave type is required.</span>
                                    </div>
                                </div>
                                <div class="col-sm-7">
                                    <!-- ======================== Date Picker ======================= -->

                                    <!-- In case of optional leave hide it -->
                                    <div class="paid-unpaid-leave clearfix" ng-if="form.data.type !== 'sick' && form.data.type !== 'optional-holiday'" >
                                        <div class="clearfix">
                                            <label>Leave Duration</label>
                                            <div class='input-group date' id='datetimepickerStart' ng-style="form.error && leaveForm.date_range.$invalid && {border: '1px solid red'}">
                                                <input name="date_range" date-range-picker class="form-control date-picker" type="text" ng-model="datePicker.date" min="datePicker.min" options="datePicker.options" ng-required="true"/>
                                                <span date-range-picker class="input-group-addon" ng-model="datePicker.date" min="datePicker.min">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>

                                        <div class="small text-danger" ng-show = "form.error && leaveForm.date_range.$invalid">
                                            <span ng-show = "leaveForm.date_range.$error.required">Date range is required.</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div ng-show="form.data.type == 'optional-holiday'">
                                <label>Select the Leave Date</label>
                                <div class="well well-sm no-margin">
                                    <div class="row">
                                        <div ng-repeat="optionalLeave in optional_leaves">
                                        <label class="col-sm-6 m-t-5">
                                            <input type="radio" name="optional_date" ng-model="form.data.start_date" ng-value="optionalLeave.date" ng-disabled="formatDate(optionalLeave.date) < todayDate"/>
                                            <span class="text-muted">%%optionalLeave.date | dateFormat %%<strong> %%optionalLeave.reason%% </strong></span>
                                        </label>
                                    </div>
                                    </div>
                                </div>

                                <!-- Incase user has taken one of the leaves from option leave -->
                                <strong ng-if="form.data.type == 'optional-holiday'" class="text-warning small">Only one optional leave can be taken in a calender year</strong>
                                <strong ng-if="user.optional" class="text-warning small">You have already used one of the optional leaves.</strong>
                            </div>
                             <div ng-show="form.data.type == 'other'">
                                    <div class="small text-warning m-t-10 m-b-10">
                                        <strong>This will be approved by Management Team.</strong>
                                    </div>
                                    <div class="small text-danger m-b-10" ng-show="form.error && form.radioSelectionError">
                                        Leave Type is required.
                                    </div>
                                    <div class="row">
                                        <div
                                            class="col-sm-6"
                                            ng-repeat="leaveType in leaveTypes"
                                            ng-hide="leaveType.code == 'sick' || leaveType.code == 'paid' || leaveType.code == 'optional-holiday' "
                                            >
                                            <label class="text-weight">
                                                <input
                                                type="radio"
                                                ng-model="form.data.leave_type_id"
                                                name="optionsRadios"
                                                id="first"
                                                value="%%leaveType.id%%"
                                                />
                                                %%leaveType.title%%
                                            </label>
                                        </div>
                                    </div>
                                <!-- Incase user has taken one of the leaves from option leave -->
                                <strong ng-if="form.data.type == 'optional-holiday'" class="text-warning small">Only one optional leave can be taken in a calender year</strong>
                                <strong ng-if="user.optional" class="text-warning small">You have already used one of the optional leaves.</strong>
                            </div>
                            <!-- ====================== reasones ================ -->
                            <div class="paid-unpaid-leave m-t-15" ng-show="form.data.type != 'sick'">
                                <div class="">
                                    <div class="clearfix">
                                        <label for="">Notes</label>
                                        <div ng-hide="form.duration == null || form.data.type == 'optional-holiday'" class="pull-right">
                                            <label class="label label-info custom-label">%%form.duration%% Working days</label>
                                        </div>
                                        <div class="pull-right">
                                            <i ng-show="form.durationLoading" class="loader-spin-relative fa">&#xf110;</i>
                                        </div>
                                    </div>
                                    <textarea name="reason" id="reason" class="form-control" rows="5" placeholder="Please enter reason for applying leave" ng-model="form.data.reason"
                                              required ng-style="leaveForm.reason.$error.required && form.error && {border: '1px solid red'}">
                                              %%form.data.reason%%</textarea>
                                    <span class="small text-danger" ng-show = "form.error && leaveForm.reason.$invalid">
                                        <span ng-show = "leaveForm.reason.$error.required">Reason is required.</span>
                                    </span>
                                </div>
                            </div>
                            <div ng-show="form.data.type != 'sick'">
                            <div ng-hide="form.warnings == null" class="small text-warning">
                                <label ng-if="form.warnings != null">Warning: %%form.warnings%%</label>
                            </div>

                            <div ng-if="form.error" class="sick-leave">
                              <p class="small text-danger" ng-if="form.error_message != null"><b> Error: %% form.error_message %% </b></p>
                            </div>
                            <input type="submit" class="btn btn-success m-t-15 pull-right paid-unpaid-leave" ng-disabled="form.submitting" value="Apply Leave" >
                            <div ng-show="form.loading" class="sm-loader"></div>
                            </div>

                        </form>
                        <!--Sick leave approve section-->
                        <div ng-show="form.data.type =='sick'" class="sick-leave">
                            <button class="btn btn-warning pull-right" type="button" ng-click="openModal()">I am sick today</button>
                        </div>
                    </div>

                </div>

                <h4 class="admin-section-heading">Leave Transactions</h4>
                <div class="panel panel-default">
                    <div class="table-responsive">
                        <table class="table table-striped bg-white">
                            <tbody>
                                <tr>
                                    <th>Leave Type</th>
                                    <th>No. of Days</th>
                                    <th>Description</th>
                                    <th class="text-right">Transaction Date</th>
                                </tr>
                                @if($leaveCredit)
                                    @foreach($leaveCredit as $row)
                                        <tr>
                                            <td>
                                                @if($row->leaveType->code == 'paid' )
                                                <span class="label label-primary">{{$row->leaveType->title}}</span>
                                                @elseif($row->leaveType->code == 'sick' )
                                                <span class="label label-warning">{{$row->leaveType->title}}</span>
                                                @else
                                                <span class="label label-danger">{{$row->leaveType->title}}</span>
                                                @endif
                                            </td>

                                            <td>{{$row->days}}</td>

                                            @if($row->reference_type=="App\Models\Leave\Leave")
                                                <td>
                                                    <small>{{date_to_ddmmyyyy($row->reference->start_date)}} to {{date_to_ddmmyyyy($row->reference->end_date)}}</small>
                                                    &nbsp;
                                                    @if($row->reference->status == "approved")
                                                        <span class="label label-success">{{$row->reference->status}}</span>
                                                    @elseif($row->reference->status == "pending")
                                                        <span class="label label-warning">{{$row->reference->status}}</span>
                                                    @else
                                                            <span class="label label-danger">{{$row->reference->status}}</span>

                                                @endif
                                                </td>
                                            @elseif($row->reference_type=="App\Models\Leave\LeaveCalendarYear")
                                                <td>Yearly Credit (Pro Rata Basis)</td>
                                            @elseif($row->reference_type=="App\Models\Admin\LeaveBalanceManager")
                                            <td>Reason : <small>{{$row->leaveBalance->reason}}</small></td>
                                            @else
                                                <td>{{$row->reference_type}}</td>
                                            @endif
                                            <td class="text-right"><small>{{datetime_in_view($row->created_at)}}</small></td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="4">
                                            No leaves credited for this year
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>

            <div class="col-md-6">
                <grid-view model="gridViewModel" columns="gridViewColumns" actions="gridViewActions"></grid-view>
            </div>
        </div>
    </div>
</div>

@endsection
