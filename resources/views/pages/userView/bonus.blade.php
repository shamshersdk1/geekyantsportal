@extends(Request::is('*')?'pages.userView.partials.usernav':'pages.userView.partials.nav')
@section('sub-content')
<div class="dashboard-container">
    <div class="panel panel-default">
        <table class="table table-striped">
                        <th>Month</th>
                        <th>Amount</th>
                        @if(Request::is('admin/users/*'))
                        <th>Notes</th>
                        <th>Status</th>
                        <th class="text-right">Action</th>
                        @else
                        <th class="text-right">Notes</th>
                        @endif
                        @if(count($bonuses) > 0)
                            @foreach($bonuses as $bonus)
                                <tr>
                                    <td class="td-text">
                                        {{date('M, Y',strtotime($bonus->date))}}
                                    </td>
                                    <td class="td-text">
                                        {{$bonus->amount}}
                                    </td>
                                    @if(Request::is('admin/users/*'))
                                    <td class="td-text">
                                        {{$bonus->notes}}
                                    </td>
                                    <td class="td-text">
                                        @if($bonus->status == "approved")
                                            <span class="label label-success custom-label">Approved</span>
                                        @elseif($bonus->status == "pending")
                                            <span class="label label-info custom-label">Pending</span>
                                        @elseif($bonus->status == "rejected")
                                            <span class="label label-danger custom-label">Rejected</span>
                                        @else
                                            <span class="label label-default custom-label">No Status</span>
                                        @endif
                                    </td>
                                    <td class="td-text text-right">
                                        <a href="/admin/bonuses/{{$bonus->id}}" type="button" class="btn btn-info crud-btn btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                                    @if($bonus->status == "pending")
                                        <a href="/admin/bonuses/{{$bonus->id}}/edit" type="button" class="btn btn-info crud-btn btn-sm"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
                                        <form method="POST" action="/admin/bonuses/{{$bonus->id}}" enctype="multipart/form-data" style="display:inline">
                                            {{method_field('PUT')}}
                                            <button type="submit" name="approve" value="approve" class="btn btn-success crud-btn btn-sm"><i class="fa fa-check" aria-hidden="true"></i> Approve</button>
                                        </form>
                                        <form method="POST" action="/admin/bonuses/{{$bonus->id}}" enctype="multipart/form-data" style="display:inline">
                                            {{method_field('PUT')}}
                                            <button type="submit" name="reject" value="reject" class="btn btn-danger crud-btn btn-sm"><i class="fa fa-times" aria-hidden="true"></i> Reject</button>
                                        </form>
                                    @elseif($bonus->status == "approved"&&!$bonus->paid)
                                        <form method="POST" action="/admin/bonuses/{{$bonus->id}}" enctype="multipart/form-data" style="display:inline">
                                            {{method_field('PUT')}}
                                            <button type="submit" name="paid" value="paid" class="btn btn-success crud-btn btn-sm">Mark As Paid</button>
                                        </form>
                                    @endif
                                    <form action="/admin/bonuses/{{$bonus->id}}" method="post" style="display:inline-block;" class="deleteform">
                                        <input name="_method" type="hidden" value="DELETE">
                                        <button class="btn btn-danger crud-btn btn-sm" type="submit"><i class="fa fa-trash btn-icon-space" aria-hidden="true"></i>Delete</button>
                                    </form>
                                    </td>
                                    @else
                                        <td class="td-text text-right">
                                            {{$bonus->notes}}
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="5">No results found.</td>
                            </tr>
                        @endif
        </table>
    </div>
</div>
<script>
		$(function(){
		$(".deleteform").submit(function(event){
                return confirm('Are you sure?');
            });
        });
</script>
@endsection