@extends(Request::is('*')?'pages.userView.partials.usernav':'pages.userView.partials.nav')
@section('sub-content')
<div class="dashboard-container">
    <div class="row">
    
        <div class="col-xs-12 col-sm-12 col-md-7 user-skill">
            <div class="panel panel-default" style="margin-bottom:10px">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead >
                            <th>Technology</th>
                            <th>Status</th>
                            <th>Date</th>
                        </thead>
                        @if(count($records) > 0)
                            @foreach($records as $record)
                                <tr>
                                    <td class="td-text">{{$record->technology->name}}</td>
                                    <td class="td-text">
                                        @if($record->status == "pending")
                                        <span class="label label-info custom-label">{{$record->status}}</span>
                                        @elseif($record->status == "approved")
                                        <span class="label label-primary custom-label">{{$record->status}}</span>
                                        @elseif($record->status == "rejected")
                                        <span class="label label-danger custom-label">{{$record->status}}</span>
                                        @endif
                                    </td>
                                    <td class="td-text">{{date('j F Y - h:i a',strtotime($record->created_at))}}</td>     
                                </tr>
                            @endforeach        
                        @else
                            <tr>
                                <td colspan="3">No results found.</td>
                                <td></td>
                                <td></td>
                            </tr>
                        @endif
                    </table>
                </div>
            </div>
            @if(count($records) > 0)
                {{$records->links()}}
                @endif
        </div>
        <div class="col-xs-12 col-sm-12 col-md-5">
            <div class="row user-skill" >
                <div class="col-md-12">
                    <form action="/user/skill/store" method="post">
                       
                        <div class="panel panel-default ">
                            <div class="panel-heading">APPLY FOR SKILLS</div>
                            <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 ma-bot-10">
                                     
                                    <div class="form-group ma-bot-10">
                                        <label for="">Select Skills </label>
                                        <select  data-placeholder="Search Here" style="width:35%;"  name="technology_id" class="js-example-basic-single" required="true">
                                            <option value=""></option>
                                            @foreach($techlist as $list)
                                            <option value="{{$list->id}}">{{$list->name}}</option>
                                            @endforeach
                                        </select>
                                        
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-success crud-btn" type="submit" style="float: right;">Apply For Skill</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
         $('.js-example-basic-single').select2();
    })
</script>
@endsection