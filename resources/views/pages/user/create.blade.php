@extends('layouts.admin-dashboard')
@section('main-content')
<section class="new-project-section">
   <div class="container-fluid">
      <div class="breadcrumb-wrap">
         <div class="row">
            <div class="col-sm-8">
               <h1 class="admin-page-title">Add new User</h1>
               <ol class="breadcrumb">
                  <li><a href="/admin">Admin</a></li>
                  <li><a href="{{ url('admin/users') }}">Users</a></li>
                  <li class="active">Add</li>
               </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
               <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
         </div>
      </div>
      @if(!empty($errors->all()))
      <div class="alert alert-danger">
         @foreach ($errors->all() as $error)
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
         <span>{{ $error }}</span><br/>
         @endforeach
      </div>
      @endif
      @if (session('message'))
      <div class="alert alert-success">
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
         <span>{{ session('message') }}</span><br/>
      </div>
      @endif
      <form class="form-horizontal" method="post" action="/admin/users" enctype="multipart/form-data">
         <div class="panel panel-default">
            <div class="panel-body">
               <div class="row">
                  <div class="col-sm-12 pull-right">
                     <div class="form-group pull-right">
                        <label for="" class="col-sm-4 ">Activate</label>
                        <div class="col-sm-6">
                           <div class="onoffswitch">
                              <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch">
                              <label class="onoffswitch-label" for="myonoffswitch">
                              <span class="onoffswitch-inner"></span>
                              <span class="onoffswitch-switch"></span>
                              </label>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label for="" class="col-sm-8">Name</label>
                        <div class="col-sm-8">
                           <input type="text" class="form-control" id="" name="name" placeholder="Full Name" value="{{ old('name') }}" required>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="" class="col-sm-8">Email</label>
                        <div class="col-sm-8">
                           <input type="email" class="form-control" id="" name="email" placeholder="Email" value="{{ old('email') }}" required>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="" class="col-sm-8">Employee Id</label>
                        <div class="col-sm-8">
                           <input type="text" class="form-control" id="" name="employee_id" placeholder="Employee Id" value="{{ old('employee_id') }}" >
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="" class="col-sm-8">Gender :</label>
                        <div class="btn-group col-sm-7" data-toggle="buttons">
                           <label class="" style="margin:5px">
                           <input type="radio" id="male" name="gender" value="male"> Male
                           </label>
                           <label class="" style="margin:5px">
                           <input type="radio" id="female" name="gender" value="female"> Female
                           </label>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="" class="col-sm-8">Date of Birth</label>
                        <div class="col-sm-8">
                           <div class="input-group date" id="datetimepicker3" style="width:100%">
                              <input type="text" name="dob" class="form-control"/>
                              <span class="input-group-addon" style="position:relative">
                              <span class="fa fa-calendar"></span>
                              </span>
                           </div>
                        </div>
                     </div>
                     <div class="form-group ">
                        <label class="col-md-8">Previous Experience :</label>
                        <div class="col-md-8">
                           <div class="row">
                              <div class="col-sm-4">
                                 <select class="form-control" name="months">
                                    @for ($i = 0; $i <= 12 ; $i++)
                                    <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor 
                                 </select>
                              </div>
                              <div class="col-sm-2">
                                 <h5 style="margin-left:-15px;">Month</h5>
                              </div>
                              <div class="col-sm-4">
                                 <select class="form-control" name="years">
                                    @for ($i = 0; $i <= 15 ; $i++)
                                    <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor 
                                 </select>
                              </div>
                              <div class="col-sm-2">
                                 <h5 style="margin-left:-15px;">Year</h5>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for="" class="col-sm-8">Joining Date</label>
                        <div class="col-sm-8">
                           <div class="input-group date" id="datetimepicker1" style="width:100%">
                              <input type="text" name="joining_date" class="form-control"/>
                              <span class="input-group-addon" style="position:relative">
                              <span class="fa fa-calendar"></span>
                              </span>
                           </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="" class="col-sm-8">Confirmation Date</label>
                        <div class="col-sm-8">
                           <div class="input-group date" id="datetimepicker2" style="width:100%">
                              <input type="text" name="confirmation_date" class="form-control"/>
                              <span class="input-group-addon" style="position:relative">
                              <span class="fa fa-calendar"></span>
                              </span>
                           </div>
                        </div>
                     </div>
                     @if( count($designations) > 0 )
                     <div class="form-group">
                        <label for="" class="col-sm-8">Designation:</label>
                        <div class="col-sm-8">
                           <select style="width:35%;"  name="designation" class="lead_list" >
                              <option value=""></option>
                              @foreach($designations as $designation)
                              <option value="{{$designation->id}}">{{$designation->designation}}</option>
                              @endforeach
                           </select>
                        </div>
                     </div>
                     @endif
                     <div class="form-group">
                        <label for="" class="col-sm-8">Reporting Manager Name:</label>
                        <div class="col-sm-8">
                           <select style="width:35%;"  name="parent_id" class="lead_list" >
                              <option value=""></option>
                              @foreach($leaduser as $manager)
                              @if($manager->hasRole('reporting-manager'))
                              <option value="{{$manager->id}}">{{$manager->name}}</option>
                              @endif
                              @endforeach
                           </select>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="" class="col-sm-8">Access Level:</label>
                        <div class="col-sm-8">
                           <div class="row">
                              @foreach ( $roles as $role )
                              <div class="col-sm-6">
                                 <div class="form-check form-check-inline">
                                    <label class="form-check-label">
                                    <input class="form-check-input" name="levels[]" type="checkbox" value="{{$role->id}}" @if($role->code == 'user') checked disabled @endif>  {{$role->name}}
                                    </label>
                                 </div>
                              </div>
                              @endforeach
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="text-center">
            <button type="submit" class="btn btn-success">Create new User</button>
         </div>
      </form>
   </div>
</section>
<script type="text/javascript">
   $(function () {
       $('#datetimepicker1').datetimepicker({
   format:'YYYY-MM-DD',
   });
       $('#datetimepicker2').datetimepicker({
   format:'YYYY-MM-DD',
       	useCurrent: false //Important! See issue #1075
       });
       $('#datetimepicker3').datetimepicker({   
   format:'YYYY-MM-DD',
           useCurrent: false //Important! See issue #1075
       });
       $("#datetimepicker6").on("dp.change", function (e) {
           $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
       });
       $("#datetimepicker7").on("dp.change", function (e) {
           $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
       });
       $('.lead_list').select2({
   	placeholder: 'Select an option',
   	allowClear:true
   });
   });
</script>
<style>
   .active.btn.focus, .active.btn {
   background-color:#2BA7CF;
   }
</style>
@endsection