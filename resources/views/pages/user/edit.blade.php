@extends('layouts.admin-dashboard')
@section('main-content')
<section class="new-project-section">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">Edit Profile</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
			  			<li><a href="{{ url('admin/profile') }}">Profiles</a></li>
			  			<li class="active">Edit</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>

	    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (!empty('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif

		<form class="form-horizontal" method="post" action="/admin/user-update/update/{{$user->id}}" enctype="multipart/form-data">
			
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">User Info</h4>
				</div>
				<div class="form-group">
			    	<label for="" class="col-sm-2 control-label">Name</label>
			    	<div class="col-sm-6">
			      		<input type="text" class="form-control" id="" name="name" placeholder="Name" value="{{$user->name}}" required disabled>
			    	</div>
			  	</div>
				<div class="form-group">
					<label for="" class="col-sm-2 control-label">Email</label>
					<div class="col-sm-6">
						<input type="email" class="form-control" id="" name="email" placeholder="Email" value="{{$user->email}}" required disabled> 
					</div>
				</div>
				@if(isset($user->profile))
			  	<div class="form-group">
			  		<label for="" class="col-sm-2 control-label">Title / Designation</label>
			    	<div class="col-sm-6">
			      		<input type="text" class="form-control" id="" name="title" placeholder="title" value="{{$user->profile->title}}" required disabled>
			    	</div>
			  	</div>
			  	
			  	<div class="form-group">
			    	<label for="" class="col-sm-2 control-label">Image</label>
			    	<div class="col-sm-6">
			      		<input type="file" class="form-control" id="" name="image" placeholder="Image" value="{{$user->profile->image}}">
			    	</div>
			  	</div>

			  	<div class="form-group">
			    	<label for="" class="col-sm-2 control-label"></label>
			    	<div class="col-sm-6">
			      		<img src="{{$user->profile->image}}" alt="profile image">
			    	</div>
			  	</div>

			  	<div class="form-group">
			    	<label for="" class="col-sm-2 control-label">Github</label>
			    	<div class="col-sm-6">
			      		<input type="url" class="form-control" id="" name="github" placeholder="github url" value="{{$user->profile->github}}" >
			    	</div>
			  	</div>
			  	<div class="form-group">
			    	<label for="" class="col-sm-2 control-label">Stack Overflow</label>
			    	<div class="col-sm-6">
			      		<input type="url" class="form-control" id="" name="stack_overflow" placeholder="Stack Overflow url" value="{{$user->profile->stack_overflow}}" >
			    	</div>
			  	</div>
			  	<div class="form-group">
			    	<label for="" class="col-sm-2 control-label">Phone</label>
			    	<div class="col-sm-6">
			      		<input type="text" class="form-control" id="" name="phone" placeholder="Phone" value="{{$user->profile->phone}}">
			    	</div>
			  	</div>
				@endif
		  	</div>
  	  		<div class="text-center">
		  		<button type="submit" class="btn btn-success">Update</button>
		  	</div>
		</form>
	</div>
</section>
@endsection
