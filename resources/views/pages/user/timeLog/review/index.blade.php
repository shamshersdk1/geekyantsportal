@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid timelog">
  <div class="breadcrumb-wrap">
    <div class="row">
      <div class="col-sm-8">
        <h1 class="admin-page-title"></h1>
        <ol class="breadcrumb">
          <li><a>Timesheet</a></li>
          <li><a>Review</a></li>
        </ol>
      </div>
      <div class="col-sm-4 text-right m-t-10">
        <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="table-responsive">
      <table class="table">
        <thead>
          <tr>
              <th  class="td-text" width="15%">Date</th>
            <th  class="td-text" width="15%">Projects</th>
            <!-- ngRepeat: date in dates -->
            <th  class="td-text" width="15%">Total hours</th>
            <th  class="td-text" >Description</th>
            <th  class="td-text" ><span class="pull-right">Action</span></th>
          </tr>
        </thead>
        <tbody>
          <!-- ngRepeat: project in projects -->
          <tr>
              <td>8th Aug</td>
            <td>
              GeekyAnts Portal
            </td>
            <td>
              8
            </td>
            <td>
              Lorem Ipsum is simply dummy text of the printing and typesetting industry.
            </td>
            <td>
                <span class="pull-right"><a href="" class="btn btn-primary btn-sm crud-btn" style="display: inline-block;" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil"></i> Edit</a></span>
              </td>
          </tr>
          <tr>
              <td>8th Aug</td>
            <td>
              GeekyAnts Portal
            </td>
            <td>
              8
            </td>
            <td>
              Lorem Ipsum is simply dummy text of the printing and typesetting industry.
            </td>
            <td>
                <span class="pull-right"><a href="" class="btn btn-primary btn-sm crud-btn" style="display: inline-block;" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil"></i> Edit</a></span>

            </td>
          </tr>
          <tr>
                  <td>8th Aug</td>
            <td>
              GeekyAnts Portal
            </td>
            <td>
              8
            </td>
            <td>
              Lorem Ipsum is simply dummy text of the printing and typesetting industry.
            </td>
            <td>
                <span class="pull-right"><a href="" class="btn btn-primary btn-sm crud-btn" style="display: inline-block;" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil"></i> Edit</a></span>

            </td>
          </tr>
          <tr>
                  <td>8th Aug</td>
            <td>
              GeekyAnts Portal
            </td>
            <td>
              8
            </td>
            <td>
              Lorem Ipsum is simply dummy text of the printing and typesetting industry.
            </td>
            <td>
                <span class="pull-right"><a href="" class="btn btn-primary btn-sm crud-btn" style="display: inline-block;" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil"></i> Edit</a></span>
              </td>
          </tr>
          <tr>
              <td>8th Aug</td>
            <td>
              GeekyAnts Portal
            </td>
            <td>
              8
            </td>
            <td>
              Lorem Ipsum is simply dummy text of the printing and typesetting industry.
            </td>
            <td>
              <span class="pull-right"><a href="" class="btn btn-primary btn-sm crud-btn" style="display: inline-block;" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil"></i> Edit</a></span>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
 
</div>

<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
  
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">GeekyAnts Portal</h4>
        </div>
        <div class="modal-body">
                <div class="row">
                    <div class="col-md-6" style="width: 90%;">
                       <div class="row " >
                        
                            <div class="col-sm-10">
                                <div class="form-group">
                                    <textarea  placeholder="description"  style="width:100%" class="form-control" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="input-group col-sm-2">
                              <input type="number" value="10" class="form-control" style="width:70px"/>
                            </div>
                          
                        </div><!-- end ngRepeat: range in existingItems -->
                    </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button class="btn btn-success "> save</button>

        </div>
      </div>
  
    </div>
  </div>
@endsection