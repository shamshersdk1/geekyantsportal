@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid timelog" ng-app="myApp" ng-controller="newTimesheetCtrl">
  <div class="breadcrumb-wrap">
    <div class="row">
      <div class="col-sm-8">
        <h1 class="admin-page-title"></h1>
        <ol class="breadcrumb">
          <li><a href="/admin">Admin</a></li>
          <li><a>Timesheet</a></li>
        </ol>
      </div>
      <div class="col-sm-4 text-right m-t-10">
        <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
      </div>
    </div>
  </div>
  <div class="panel panel-default" >
    <div class="panel-body">
      <div class="row date-select">
        <div class="col-md-4 ">
           <select class="project" id="selectedDate" name="selectedDate" ng-model="selectedDate">
              <option value="%%dates[0]%%">Today</option>
              <option value="%%dates[1]%%">Yesterday</option>
              <option value="%%dates[2]%%">%%dates[2]%%</option>
              <option value="%%dates[3]%%">%%dates[3]%%</option>
              <option value="%%dates[4]%%">%%dates[4]%%</option>
          </select>
        </div>
      </div>
    </div>
  </div>
  <form>
    <div class="panel panel-default">
      <table class="table log-timesheet-table">
        <thead>
          <tr>
            <th  class="td-text" width="30%">Projects</th>
            <!-- ngRepeat: date in dates -->
            <th  class="td-text" >Description</th>
            <th  class="td-text" width="20%"><span class="pull-right">Total hours</span></th>

          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="i in [0,1,2,3,4]">
            <td ng-if="projectList.length > 0">
                <ui-select ng-model="selectedProject[i]" on-select="selectProject($select.selected, i)" theme="select2" name="name" id="name" ng-style="form.userSelectError && {border: '1px solid red'}">
                    <ui-select-match>%%$select.selected.name%%</ui-select-match>
                    <ui-select-choices repeat="project.id as project in projectList | filter: $select.search">
                        %%project.name%%
                    </ui-select-choices>
                </ui-select>
            </td>
            <td>
              <textarea class="form-control"  rows="2" style="" ng-model="tasks[i]"></textarea>
            </td>
            <td>
              <span class="pull-right"><input  class="form-control hours-input" type="text" ng-model="loggedTime[i]" style="width:70px"/></span>
            </td>
          </tr>
          
        </tbody>
      </table>
    </div>
    <div class="col-sm-12 text-center">
      <button class="btn btn-success text-center" ng-click="submit()"> Submit</button>
    </div>
  </form>
</div>
<script>
  $(document).ready(function() {
    $('.project').select2();
  });
</script>
@endsection