@extends('layouts.admin-dashboard')
@section('main-content')
	<section class="timelog-dashboard">
		<div class="container-fluid">
			<div class="breadcrumb-wrap">
		    	<div class="row">
		            <div class="col-sm-12">
		                <h1 class="admin-page-title">My Sprints</h1>
		                <ol class="breadcrumb">
		        		  	<li><a href="/admin">Admin</a></li>
				  			<li class="active">My Sprint</li>
		        		</ol>
		            </div>
		        </div>
		    </div>
		    <div class="row">
		    	<div class="col-md-12">
						@if(!empty($errors->all()))
							<div class="alert alert-danger">
							@foreach ($errors->all() as $error)
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							<span>{{ $error }}</span><br/>
							@endforeach
							</div>
							@endif
							@if (session('message'))
							<div class="alert alert-success">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							<span>{{ session('message') }}</span><br/>
							</div>
						@endif
						<div class="panel panel-default">
					    	<table class="display table table-striped">
				    			<thead>
									<tr>
								        <th width="15%">Sprint Id</th>
								        <th width="50%">Sprint Name</th>
								        <th width="35%" class="text-right">Actions</th>
								    </tr>
								</thead>
								@if(isset($userSprints))
									@if(count($userSprints) > 0)
										@foreach($userSprints as $sprint)
								      	<tr>
									      	<td width="80%">
									      		<b>{{$sprint->id}}</b> 
									      		{{$sprint->title}}
									      	</td>
									        <td width="20%"class="text-right">
									        	<a href="/admin/project/{{$sprint->id}}/view-sprint" class="btn btn-success btn-sm">View Sprint</a>
									        	<a href="/admin/project/{{$sprint->project_id}}/dashboard" class="btn btn-warning btn-sm">View Project</a>
									        </td>
								      	</tr>
								      	@endforeach
								    @else
								    	<tr>
								    		<td colspan="2">No Result found</td>
								    	</tr>
								    @endif
								@endif
		        			</table>
				  		</div>
					
				</div>
			</div>
		</div>
	</section>

@endsection
