<div class="panel panel-default">
    <table class="table table-bordered table-striped">
        <tbody>
                            <tr>
                                <th><h6>Month</h6></th>
                                @foreach($monthsArr as $month)
                                    <th>{{date('M, Y', strtotime($month['year'].'-'.$month['month']))}}</th>
                                @endforeach
                            </tr>
                            <tr><th colspan="13"><h5>INCOME</h5></th></tr>
                            <tr>
                                <th>Gross Annual Salary</th>
                                @foreach($monthsArr as $month)
                                    <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['gross-annual-salary'] ?? 0)}}</td>
                                @endforeach
                            </tr>
                            <tr>
                                <th>Previous Employment Salary</th>
                                @foreach($monthsArr as $month)
                                    @if(isset($tdsSummary[$month['year'].'_'.$month['month']]['previous-employment-salary']))
                                        <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['previous-employment-salary'] ?? 0)}}</td>
                                    @elseif(isset($tdsSummary[$month['year'].'_'.$month['month']]['income-paid']))
                                        <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['income-paid'] ?? 0)}}</td>
                                    @else 
                                        <td>0</td>
                                    @endif
                                @endforeach
                            </tr>
                            <tr>
                                <th>Loan Interest Income</th>
                                @foreach($monthsArr as $month)
                                    <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['loan-interest-income'] ?? 0)}}</td>
                                @endforeach
                            </tr>
                            <tr>
                                <th>CTC Bonus</th>
                                @foreach($monthsArr as $month)
                                    @if(isset($tdsSummary[$month['year'].'_'.$month['month']]['ctc-bonus']))
                                        <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['ctc-bonus'] ?? 0)}}</td>
                                    @elseif(isset($tdsSummary[$month['year'].'_'.$month['month']]['annual-appraisal-bonus']))
                                        <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['annual-appraisal-bonus'] ?? 0)}}</td>
                                    @else 
                                        <td>0</td>
                                    @endif
                                @endforeach
                            </tr>
                            <tr>
                                <th>Other Bonus</th>
                                @foreach($monthsArr as $month)
                                    @if(isset($tdsSummary[$month['year'].'_'.$month['month']]['other-bonus']))
                                        <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['other-bonus'] ?? 0)}}</td>
                                    @elseif(isset($tdsSummary[$month['year'].'_'.$month['month']]['gross-other-bonus']))
                                        <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['gross-other-bonus'] ?? 0)}}</td>
                                    @else 
                                        <td>0</td>
                                    @endif
                                @endforeach
                            </tr>
                            <tr>
                                <th>Total Income for the Year</th>
                                @foreach($monthsArr as $month)
                                <td>
                                    {{custom_format(
                                        $tdsSummary[$month['year'].'_'.$month['month']]['gross-annual-salary'] + 
                                        $tdsSummary[$month['year'].'_'.$month['month']]['previous-employment-salary'] + 
                                        $tdsSummary[$month['year'].'_'.$month['month']]['income-paid'] +
                                        $tdsSummary[$month['year'].'_'.$month['month']]['ctc-bonus'] + 
                                        $tdsSummary[$month['year'].'_'.$month['month']]['annual-appraisal-bonus'] +
                                        $tdsSummary[$month['year'].'_'.$month['month']]['other-bonus'] + 
                                        $tdsSummary[$month['year'].'_'.$month['month']]['gross-other-bonus'] +
                                        $tdsSummary[$month['year'].'_'.$month['month']]['loan-interest-income'] 
                                    ?? 0)}}
                                </td>
                                @endforeach
                            </tr>
                            <tr><th colspan="13"><h5>DEDUCTION</h5></th></tr>
                            <tr>
                                <th>HRA Exemption</th>
                                @foreach($monthsArr as $month)
                                <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['hra-exemption'])}}</td>
                                @endforeach
                            </tr>
                            <tr>
                                <th>Standard Deduction</th>
                                @foreach($monthsArr as $month)
                                    <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['standard-deduction'] ?? 0)}}</td>
                                @endforeach
                            </tr>
                            <tr>
                                <th>Employee IT Declared u/s. 80C, limited to Rs.1.5L</th>
                                @foreach($monthsArr as $month)
                                    <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['it-saving-investment'] ?? 0)}}</td>
                                @endforeach
                            </tr>
                            <tr>
                                <th>Other Employee IT Declared</th>
                                @foreach($monthsArr as $month)
                                    <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['it-saving-deduction'] ?? 0)}}</td>
                                @endforeach
                            </tr>
                            <tr>
                                <th>Gross Professional Tax</th>
                                @foreach($monthsArr as $month)
                                    <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['gross-professional-tax'] ?? 0)}}</td>
                                @endforeach
                            </tr>
                            <tr>
                                <th>Total Deduction</th>
                                @foreach($monthsArr as $month)
                                    <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['hra-exemption'] + $tdsSummary[$month['year'].'_'.$month['month']]['gross-professional-tax'] + $tdsSummary[$month['year'].'_'.$month['month']]['it-saving-deduction'] + $tdsSummary[$month['year'].'_'.$month['month']]['it-saving-investment'] + $tdsSummary[$month['year'].'_'.$month['month']]['standard-deduction'] ?? 0)}}</td>
                                @endforeach
                            </tr>
                            <tr><th colspan="13"><h5>TAX CALCULATION</h5></th></tr>
                            <tr>
                                <th>Net Taxable Income for the Year</th>
                                @foreach($monthsArr as $month)
                                    @if(isset($tdsSummary[$month['year'].'_'.$month['month']]['net-taxable-income']))
                                        <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['net-taxable-income'] ?? 0)}}</td>
                                    @elseif(isset($tdsSummary[$month['year'].'_'.$month['month']]['net-taxable-salary']))
                                        <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['net-taxable-salary'] ?? 0)}}</td>
                                    @else 
                                        <td>0</td>
                                    @endif
                                @endforeach
                            </tr>
                            <tr>
                                <th>Rebate if Net Taxable Income upto Rs. 5 Lakhs</th>
                                @foreach($monthsArr as $month)
                                    <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['rebate'] ?? 0)}}</td>
                                @endforeach
                            </tr>
                            <tr>
                                <th>Annual Income Tax</th>
                                @foreach($monthsArr as $month)
                                    @if(isset($tdsSummary[$month['year'].'_'.$month['month']]['annual-income-tax']))
                                        <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['annual-income-tax'] ?? 0)}}</td>
                                    @elseif(isset($tdsSummary[$month['year'].'_'.$month['month']]['gross-tds']))
                                        <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['gross-tds'] ?? 0)}}</td>
                                    @else 
                                        <td>0</td>
                                    @endif
                                @endforeach
                            </tr>
                            <tr>
                                <th>Taxable Income after Rebate</th>
                                @foreach($monthsArr as $month)
                                    @if(isset($tdsSummary[$month['year'].'_'.$month['month']]['annual-income-tax-after-rebate']))
                                        <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['annual-income-tax-after-rebate'] ?? 0)}}</td>
                                    @elseif(isset($tdsSummary[$month['year'].'_'.$month['month']]['taxable-income-after-rebate']))
                                        <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['taxable-income-after-rebate'] ?? 0)}}</td>
                                    @else 
                                        <td>0</td>
                                    @endif
                                @endforeach
                            </tr>
                            <tr>
                                <th>Annual Edu Cess Payable</th>
                                @foreach($monthsArr as $month)
                                    <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['edu-cess'] ?? 0)}}</td>
                                @endforeach
                            </tr>
                            <tr>
                                <th>Total Annual Income Tax Payable</th>
                                @foreach($monthsArr as $month)
                                    @if(isset($tdsSummary[$month['year'].'_'.$month['month']]['total-annual-income-tax-payable']))
                                        <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['total-annual-income-tax-payable'] ?? 0)}}</td>
                                    @elseif(isset($tdsSummary[$month['year'].'_'.$month['month']]['taxable-payable']))
                                        <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['taxable-payable'] ?? 0)}}</td>
                                    @else 
                                        <td>0</td>
                                    @endif
                                @endforeach
                            </tr>
                            <tr>
                                <th>TDS Already deducted till date</th>
                                @foreach($monthsArr as $month)
                                    <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['tds-paid-till-date'] ?? 0)}}</td>
                                @endforeach
                            </tr>
                            <tr>
                                <th>Balance Tax Payable</th>
                                @foreach($monthsArr as $month)
                                    <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['balance-tax-payable'] ?? 0)}}</td>
                                @endforeach
                            </tr>
                            <tr>
                                <th>TDS(for the month)</th>
                                @foreach($monthsArr as $month)
                                    @if(isset($tdsSummary[$month['year'].'_'.$month['month']]['tds-for-the-month']))
                                        <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['tds-for-the-month'] ?? 0)}}</td>
                                    @elseif(isset($tdsSummary[$month['year'].'_'.$month['month']]['tds']))
                                        <td>{{custom_format($tdsSummary[$month['year'].'_'.$month['month']]['tds'] ?? 0)}}</td>
                                    @else 
                                        <td>0</td>
                                    @endif
                                @endforeach
                            </tr>                            
                        </tbody>
    </table>
</div>
