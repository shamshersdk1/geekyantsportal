<div class="user-list-view">
    <div class="panel panel-default">
        <table class="table table-bordered table-striped">
            <tr>
                <th>Month</th>
                @foreach($monthsArr as $month)
                    <th>{{date('M, Y', strtotime($month['year'].'-'.$month['month']))}}</th>
                @endforeach
            </tr> 
            <tr>
                <th>Working Days</th>
                @foreach($monthsArr as $month)
                    <th>{{$tdsData[$month['year'].'_'.$month['month']]['working_days'] ?? '-'}}</th>
                @endforeach
            </tr> 
            @foreach($appraisalCompType as $componenet)
                <tr>      
                    <td>{{$componenet->name}}</td>  
                    @foreach($monthsArr as $month)
                        @if($componenet->id == $tdsId && (($month['month'] >= $prepMonth) || ($month['year'] > $financialYearObj->year)))
                            @if(isset($tdsSummary[$lastMonth['year'].'_'.$lastMonth['month']]['tds-for-the-month']))
                                <td>{{custom_round($tdsSummary[$lastMonth['year'].'_'.$lastMonth['month']]['tds-for-the-month'])}}</td>
                            @elseif(isset($tdsSummary[$lastMonth['year'].'_'.$lastMonth['month']]['tds']))
                                <td>{{custom_round($tdsSummary[$lastMonth['year'].'_'.$lastMonth['month']]['tds'])}}</td>
                            @else 
                                <td>0</td>
                            @endif 
                        @else
                            <td>{{$tdsData[$month['year'].'_'.$month['month']]['appraisal_components'][$componenet->id] ? custom_round($tdsData[$month['year'].'_'.$month['month']]['appraisal_components'][$componenet->id]) : '-'}}</td>
                        @endif    
                    @endforeach
                </tr>
            @endforeach
            <tr>
                <th>Monthly Gross Salary</th>
                @foreach($monthsArr as $month)
                <th>{{$tdsData[$month['year'].'_'.$month['month']]['monthly_gross_salary'] ? custom_round($tdsData[$month['year'].'_'.$month['month']]['monthly_gross_salary']) : '-' }}</th>
                @endforeach
            </tr>
            <tr>
                <th colspan="13"><b>DEDUCTION</b></th>
            </tr>

            <tr>
                <th>Vpf</th>
                @foreach($monthsArr as $month)
                    <th>{{ $tdsData[$month['year'].'_'.$month['month']]['vpf'] ? custom_round($tdsData[$month['year'].'_'.$month['month']]['vpf']) : '-' }}</th>
                @endforeach
            </tr>
            <tr>
                <th>Food Deduction</th>
                @foreach($monthsArr as $month)
                    <th>{{ $tdsData[$month['year'].'_'.$month['month']]['food_deduction'] ? custom_round($tdsData[$month['year'].'_'.$month['month']]['food_deduction']) : '-' }}</th>
                @endforeach
            </tr>
            <tr>
                <th>Insurance</th>
                @foreach($monthsArr as $month)
                    <th>{{ $tdsData[$month['year'].'_'.$month['month']]['insurance'] ? custom_round($tdsData[$month['year'].'_'.$month['month']]['insurance']) : '-' }}</th>
                @endforeach
            </tr>
            <tr>
                <th>Loan EMI</th>
                @foreach($monthsArr as $month)
                    <th>{{$tdsData[$month['year'].'_'.$month['month']]['loan'] ? custom_round($tdsData[$month['year'].'_'.$month['month']]['loan']) : '-' }}</th>
                @endforeach
            </tr>
            <tr>
                <th colspan="13">APPRAISAL BONUSES</th>
            </tr>
            @foreach($appraisalBonusType as $bonus)
            <tr>      
                <td>{{$bonus->description}}</td>  
                @foreach($monthsArr as $month)
                    <td>{{$tdsData[$month['year'].'_'.$month['month']]['appraisal_bonuses'][$bonus->id] ? custom_round($tdsData[$month['year'].'_'.$month['month']]['appraisal_bonuses'][$bonus->id]) : '-' }}</td>
                @endforeach
            </tr>
            @endforeach
            <tr>
                <td>Other Appraisal Bonus</td>
                @foreach($monthsArr as $month)
                    <td>{{$tdsData[$month['year'].'_'.$month['month']]['appraisal_bonuses']['other'] ? custom_round($tdsData[$month['year'].'_'.$month['month']]['appraisal_bonuses']['other']) : '-' }}</td>
                @endforeach
            </tr>
            <tr>
                <th colspan="13"><b>OTHER BONUSES</b></th>
            </tr>
            <tr>
                <td>Onsite</td>
                @foreach($monthsArr as $month)
                    <td>{{$tdsData[$month['year'].'_'.$month['month']]['bonuses']['onsite'] ? custom_round($tdsData[$month['year'].'_'.$month['month']]['bonuses']['onsite']) : '-' }}</td>
                @endforeach
            </tr>
            <tr>
                <td>Additional Workday</td>
                @foreach($monthsArr as $month)
                    <td>{{$tdsData[$month['year'].'_'.$month['month']]['bonuses']['additional'] ? custom_round($tdsData[$month['year'].'_'.$month['month']]['bonuses']['additional']) : '-' }}</td>
                @endforeach
            </tr>
              <tr>
                <td>Extra Hour Bonus</td>
                @foreach($monthsArr as $month)
                    <td>{{$tdsData[$month['year'].'_'.$month['month']]['bonuses']['extra'] ? custom_round($tdsData[$month['year'].'_'.$month['month']]['bonuses']['extra']) : '-' }}</td>
                @endforeach
            </tr>
              <tr>
                <td>Tech Talk</td>
                @foreach($monthsArr as $month)
                    <td>{{$tdsData[$month['year'].'_'.$month['month']]['bonuses']['tech_talk'] ? custom_round($tdsData[$month['year'].'_'.$month['month']]['bonuses']['tech_talk']) : '-' }}</td>
                @endforeach
            </tr>
            <tr>
                <td>Performance</td>
                @foreach($monthsArr as $month)
                    <td>{{$tdsData[$month['year'].'_'.$month['month']]['bonuses']['performance'] ? custom_round($tdsData[$month['year'].'_'.$month['month']]['bonuses']['performance']) : '-' }}</td>
                @endforeach
            </tr>
            <tr>
                <td>Referral Bonus</td>
                @foreach($monthsArr as $month)
                    <td>{{$tdsData[$month['year'].'_'.$month['month']]['bonuses']['referral'] ? custom_round($tdsData[$month['year'].'_'.$month['month']]['bonuses']['referral']) : '-' }}</td>
                @endforeach
            </tr>
            <tr>
                <td>Other</td>
                @foreach($monthsArr as $month)
                    <td>{{$tdsData[$month['year'].'_'.$month['month']]['bonuses']['other'] ? custom_round($tdsData[$month['year'].'_'.$month['month']]['bonuses']['other']) : '-' }}</td>
                @endforeach
            </tr>
            <tr>
                <th colspan="13"><b>Adhoc Payments</b></th>
            </tr>
            @foreach($adhocPaymentComponents as $adhocPaymentComponent)
            <tr>      
                <td>{{$adhocPaymentComponent->description}}</td> 
                    @foreach($monthsArr as $month)
                                <td>{{$tdsData[$month['year'].'_'.$month['month']]['adhoc_payments'][$adhocPaymentComponent->id] ? custom_round($tdsData[$month['year'].'_'.$month['month']]['adhoc_payments'][$adhocPaymentComponent->id]) : '-' }}</td>
                     @endforeach
            </tr>
            @endforeach
        </table>
    </div>
</div>