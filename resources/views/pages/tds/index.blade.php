@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">List of Users</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/users') }}">List of Users</a></li>
                    <li><a class="active">TDS Summary for {{$userObj->name}}</a></li>                      
        		</ol>
            </div>
		</div>
    </div>
    <div class="col-sm-4 pull-left">
    <label>Processing salary for {{$monthObj ? $monthObj->formatMonth() : null}}</label>
    </div>
    <div class="col-sm-2 pull-right">
        @if(isset($userList))
            <select id="selectid2" name="user"  placeholder= "{{$userObj->name}}">
                <option value=""></option>
                @foreach($userList as $x)
                    <option value="{{$x->id}}" >{{$x->employee_id}} - {{$x->name}}</option>
                @endforeach
            </select>
        @endif
    </div>
    <div class="col-sm-2 pull-right">
        @if(isset($financialYearList))
            <select id="selectid3" name="year"  placeholder= "{{$financialYearObj->year}} - {{$financialYearObj->year + 1}}">
                <option value=""></option>
                @foreach($financialYearList as $x)
                    <option value="{{$x->id}}" >{{$x->year}} - {{$x->year + 1}}</option>
                @endforeach
            </select>
        @endif
    </div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		            @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
            @endif
            @if(session('alert-class'))
                <div class="alert alert-danger">
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ session('alert-class') }}</span><br/>
		        </div>
            @endif
	    </div>
	</div>
        <div class="bg-white">
            <ul class="nav nav-tabs nav-tabs-user" role="tablist">
                <li role="presentation" class="active">
                    <a href="#salary-details" aria-controls="salary-details" role="tab" data-toggle="tab">Salary Breakdown</a>
                </li>
                <li role="presentation">
                    <a href="#tds" aria-controls="payslip" role="tab" data-toggle="tab">TDS Breakdown</a>
                </li>
            </ul>

            <div class="tab-content">
                <div id="salary-details" role="tabpanel" class="tab-pane active">
                    @include('pages/tds/_partials.monthly-salary-summary')
                </div>
                <div id="tds" role="tabpanel" class="tab-pane">
                    @include('pages/tds/_partials.tds-summary')
                </div>
            </div>
        </div>
</div>
<script>
    $('#selectid3').change(function(){
        console.log('here');
        var optionSelected = $("option:selected", this);
        optionValue = this.value;
        console.log(optionValue);
        if (optionValue) { 
            window.location = "/admin/users/"+{{$userObj->id}}+"/tds-summary/"+optionValue; 
        }
    });
    $('#selectid2').change(function(){
        console.log('here');
        var optionSelected = $("option:selected", this);
        optionValue = this.value;
        console.log(optionValue);
        if (optionValue) { 
            window.location = "/admin/users/"+optionValue+"/tds-summary/"+{{$financialYearObj->id}}; 
        }
    });

</script>
@endsection

