@extends('layouts.admin-dashboard')
@section('main-content')
    <div class="container-fluid mis-report">
        <div class="breadcrumb-wrap">
           <div class="row">
              <div class="col-sm-8">
                 <h1 class="admin-page-title">User Leave Report</h1>
                 <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li class="active">Leave Report</li>
                 </ol>
              </div>
           </div>
        </div>
        @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif

        <table class="table table-bordered table-hover bg-white">
          <tr>
              <th width="120px" class="text-center">Year</th>
              <th width="120px" class="text-center">Action(s)</th>
          </tr>
          @if(count($years) == 0 )
            <tr>
               <td colspan="2">
                  No Record Found
               </td>
            </tr>

         @endif
         @if(isset($years))
            @foreach($years as $year)
               <tr>
                  <td class="text-center">{{$year->year}}</td>
                  <td class="text-center">
                     <a class="btn btn-success" href="leave-report/{{$year->id}}"><i class="fa fa-eye"></i> View</a>
                 </tr>
            @endforeach
         @endif
        </table>
    </div>
@endsection
