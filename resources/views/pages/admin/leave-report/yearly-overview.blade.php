@extends('layouts.admin-dashboard')
@section('main-content')
    <div class="container-fluid mis-report">
        <div class="breadcrumb-wrap">
           <div class="row">
              <div class="col-sm-8">
                 <h1 class="admin-page-title">User Leave Report</h1>
                 <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="/admin/leave-section/leave-report">Leave Report</a></li>
                 <li><a class="active">{{$yearObj->year}}</a></li>
                 </ol>
              </div>
              <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
           </div>
        </div>
        @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif
        @if($till_date != null)
            <div class="text-success">
                <h4>User Consumed Leaves till date {{date_in_view($till_date)}}</h4>
            </div>
        @else 
            <div class="text-success">
                <h4>User Consumed Leaves for year - {{$yearObj->year}}</h4>
            </div>
        @endif
        <div class="row">
            <div class="col-sm-12">
                <form class="form-group row" action="{{$yearObj->id}}" method="get">
                    <div class="col-sm-4">
                        <div class='input-group date' id='datetimepickerEnd_search'>
                            <input type='text' id="ed" class="form-control" name="till_date" value="<?=($till_date != null ? $till_date : '')?>" autocomplete="off" placeholder="Till Date" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-5" style="padding-top:4px">
                        <button type="submit" class="btn btn-primary center" style="margin-left: 2px;"> <i class="fa fa-search"></i> &nbsp; Search</button>
                        <a class="btn btn-danger" href="{{$yearObj->id}}">
                            <i class="fa fa-rotate-left fa-fw"></i> &nbsp; Clear
                        </a>
                    </div>  
                    <div class="col-sm-3 text-right" style="padding-top:4px;">
                    @if($till_date!=null)
                    <a class="btn btn-primary" href="{{$yearObj->id}}/download?till_date={{$till_date}}">
                    @else
                    <a class="btn btn-primary" href="{{$yearObj->id}}/download">
                    @endif
                        <i class="fa fa-download"></i> &nbsp; Download report
                    </a>
                    </div>
                </form>
        </div>
        <!--data starts here-->
        <table class="table table-bordered table-hover bg-white" id="leaves">
          <thead>
              <th>#</th>
              <th>Emp. ID</th>
              <th width="180px" class="text-left">Username</th>
              <th class="text-center" colspan="2">PL</th>
              <th class="text-center" colspan="2">SL</th>
              <th class="text-center" colspan="2">Optional</th>
              <th class="text-center" colspan="2">Marriage</th>
              <th class="text-center" colspan="2">Emergency</th>
              <th class="text-center" colspan="2">Paternity</th>
              <th class="text-center" colspan="2">Maternity</th>
              <th class="text-center" colspan="2">Compensatory Off</th>
              <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    @for($i=1;$i<count($leaveTypes);$i++)
                        <th class="text-center">Used</th>
                        <th class="text-center">Total</th>
                    @endfor
              </tr>
          </thead>
          <tbody>
          @if(($users))
          @foreach($users as $row)
          <tr>
              <td></td>
                <td class="text-left">{{$row->employee_id}}</td>
                <td class="text-left">{{$row->name}}</div>
                   
                <td class="text-center">{{!empty($usedLeaveData[$row->id]['paid']) ? $usedLeaveData[$row->id]['paid']: 0}} </td>
                <td> {{!empty($leaveBalance[$row->id]['paid']) ? $leaveBalance[$row->id]['paid']: 0}}</td>
                <td class="text-center">{{!empty($usedLeaveData[$row->id]['sick']) ? $usedLeaveData[$row->id]['sick'] : 0}}</td>
                <td>{{!empty($leaveBalance[$row->id]['sick']) ? $leaveBalance[$row->id]['sick'] : 0}}</td>
                <td class="text-center">{{!empty($usedLeaveData[$row->id]['optional-holiday']) ? $usedLeaveData[$row->id]['optional-holiday']: 0}}</td>
                <td>{{!empty($leaveBalance[$row->id]['optional-holiday']) ? $leaveBalance[$row->id]['optional-holiday']: 0}}</td>
                <td class="text-center">{{!empty($usedLeaveData[$row->id]['marriage']) ? $usedLeaveData[$row->id]['marriage']: 0}}</td>
                <td>{{!empty($leaveBalance[$row->id]['marriage']) ? $leaveBalance[$row->id]['marriage']: 0}}</td>
                <td class="text-center">{{!empty($usedLeaveData[$row->id]['emergency']) ? $usedLeaveData[$row->id]['emergency']: 0}}</td>
                <td>{{!empty($leaveBalance[$row->id]['emergency']) ? $leaveBalance[$row->id]['emergency']: 0}}</td>
                <td class="text-center">{{!empty($usedLeaveData[$row->id]['paternity']) ? $usedLeaveData[$row->id]['paternity']: 0}}</td>
                <td>{{!empty($leaveBalance[$row->id]['paternity']) ? $leaveBalance[$row->id]['paternity']: 0}}</td>
                <td class="text-center">{{!empty($usedLeaveData[$row->id]['maternity']) ? $usedLeaveData[$row->id]['maternity']: 0}}</td>
                <td>{{!empty($leaveBalance[$row->id]['maternity']) ? $leaveBalance[$row->id]['maternity']: 0}}</td>
                <td class="text-center">{{!empty($usedLeaveData[$row->id]['comp-off']) ? $usedLeaveData[$row->id]['comp-off']: 0}}</td>
                <td>{{!empty($leaveBalance[$row->id]['comp-off']) ? $leaveBalance[$row->id]['comp-off']: 0}}</td>
          </tr>
          @endforeach
          @else
          <tr>
           <td class="text-center" colspan="8">No leaves found for this month</td>
          </tr>
          @endif
        </tbody>
        </table>
    </div>
    <script>
 $(document).ready(function() {
      $tableHeight = $( window ).height();
        var t = $('#leaves').DataTable( {
            pageLength:500,
            scrollY: $tableHeight - 370,
            scrollX: true,
            scrollCollapse: true,
            fixedColumns:   {
                leftColumns: 3
            }
        } );
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
});
        $(function(){
            $('#datetimepickerEnd_search').datetimepicker({
                minDate: '2018-01-01',
                format: 'YYYY-MM-DD',
                useCurrent:false
            });
        });
    </script>
@endsection
