@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
		<div class="row">
            <div class="col-sm-8">
				<h1 class="admin-page-title">Adhoc Payment Component</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
					<li><a href="{{ url('admin/adhoc-payment-component') }}">Adhoc Payment Component</a></li>
					<li class="active">{{$adhocPaymentComponentObj->description}}</li>
                </ol>
			</div>
			<div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
        </div>
	</div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">Add Adhoc Payment Value</div>
				<div class="panel-body">
                        {{ Form::open(['url' => '/admin/adhoc-payments/'.$adhocPaymentObj->id, 'method' => 'put']) }}
				        {{ Form::token() }}
                        <div class="row">
							<div class="form-group col-md-6">
								{{ Form::Label('user', 'User Name:',array('class' =>'col-md-4' , 'style' =>'padding:3%')) }}
								<div class="col-md-8 col-sm-12 col-xs-12" style="padding:0">
									{{ Form::select('user_id', $users->pluck('name','id'),$adhocPaymentObj->user_id, array('class' => 'userSelect1'  ,'placeholder' => 'Select User'));}}
								</div>
							</div>
                    
							<div class="form-group col-md-6">
								{{ Form::label('', 'Adhoc Payment Component:', ['class' => 'col-md-4', 'style' => 'padding-top:2%', 'align' => 'left']) }}
								<div class="col-md-8 col-sm-12 col-xs-12" style="padding:0px">
									{{ Form::select('adhoc_payment_component_id', $adhocComponents->pluck('key','id'),$adhocPaymentObj->adhoc_payment_component_id, array('class' => 'userSelect1'  ,'placeholder' => 'Select Appraisal Type'));}}
								</div>
							</div>	
						</div>
					<div class="row">
							<div class="form-group col-md-6">
								{{ Form::Label('amount', 'Amount:',array('class' =>'col-md-4' , 'style' =>'padding:3%')) }}
								<div class='col-md-8 col-sm-12 col-xs-12' style="padding:0px">
                                    {{ Form::text('amount',$adhocPaymentObj->amount,['class' => 'form-control']) }}
								</div>
                            </div>
                            <div class="form-group col-md-6">
								{{ Form::Label('comment', 'Comment :',array('class' =>'col-md-4' , 'style' =>'padding:3%')) }}
								<div class="col-md-8 col-sm-12 col-xs-12" style="padding:0">
										{{ Form::textarea('comment',$adhocPaymentObj->comment,['class'=>'form-control', 'rows' => 5] ) }}
								</div>
							</div>
                        </div>
                        
					<div class="col-md-12">
					{{ Form::submit('Update',['class' => 'btn btn-success','style' => 'display: block; margin: 0 auto']) }}
					</div>
                    {{ Form::close() }}
				</div>
				</div>
				
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
 $(document).ready(function() {
		$('.userSelect1').select2({
			placeholder: 'Select a user',
			allowClear:true
		});
    });
</script>
<script>
	$(function () {
		$('#type option[value="{{old('type')}}"]').attr("selected",true);
	});
</script>
@endsection
