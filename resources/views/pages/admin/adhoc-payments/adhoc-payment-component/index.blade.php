@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
		<div class="row">
            <div class="col-sm-8">
				<h1 class="admin-page-title">Adhoc Payment Component</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
					<li class="active">Adhoc Payment Component</li>
                </ol>
			</div>
			<div class="col-sm-4 text-right m-t-10">
            </div>
        </div>
	</div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">Add Adhoc Payment Component</div>
				<div class="panel-body">
					{{ Form::open(['url' => '/admin/adhoc-payment-component', 'method' => 'post']) }}
					<div class="form-group col-md-12">
                    	{{ Form::label('key', 'Key :',['class' => 'col-md-4','style' => 'padding-top:1%','align' => 'right'])}}
						<div class="col-md-4" style="padding:0">
                    		{{ Form::text('key', '',['class' => 'form-control']) }}
						</div>
					</div>
					<div class="form-group col-md-12">
                    {{ Form::label('description', 'Description :',['class' => 'col-md-4','style' => 'padding-top:1%','align' => 'right'])}}
						<div class="col-md-4" style="padding:0">
                    		{{ Form::text('description', '',['class' => 'form-control']) }}
						</div>
					</div>
					<div class="form-group col-md-12">
                    {{ Form::label('type', 'Type :',['class' => 'col-md-4','align' => 'right'])}}
						<div class="col-md-4" style="padding:0">
							{{Form::radio('type', 'credit');}} Credit   
							{{Form::radio('type', 'debit');}}   Debit
						</div>
					</div>
					<div class="col-md-12">
					{{ Form::submit('Save',['class' => 'btn btn-success','style' => 'display: block; margin: 0 auto']) }}
					</div>
                    {{ Form::close() }}
				</div>
				</div>
				<div class="panel panel-default">
				<table class="table table-striped">
				<thead>
					<th width="10%">#</th>
					<th width="10%">key</th>
					<th width="15%">Description</th>
					<th width="15%">Type</th>
					<th width="25%" class="text-right">Actions</th>
				</thead>
				@if(isset($adhocPaymentComponents))
				@if(count($adhocPaymentComponents) > 0)
					@foreach($adhocPaymentComponents as $index=>$adhocPaymentComponent)
						<tr>
							<td class="td-text">{{$index+1}}</td>
							<td class="td-text"> {{$adhocPaymentComponent->key}}</td>
							<td class="td-text"> {{$adhocPaymentComponent->description}}</td>
							<td class="td-text"> 
								@if($adhocPaymentComponent->type == 'credit')
								<span class="label label-primary custom-label">Credit</span>
								@else
								<span class="label label-danger custom-label">Debit</span>
								@endif
							</td>
							<td class="text-right">
								<a href="/admin/adhoc-payment-component/{{$adhocPaymentComponent->id}}" class="btn btn-info crud-btn btn-sm"><i class="fa fa-pencil btn-icon-space" aria-hidden="true"></i>Edit</a>
								<div style="display:inline-block;" class="deleteform">
								{{ Form::open(['url' => '/admin/adhoc-payment-component/'.$adhocPaymentComponent->id, 'method' => 'delete']) }}
                                {{Form::button('<i class="fa fa-trash"></i> Delete', array('type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => 'return confirm("Are you sure you want to delete this item?")'));}}
								{{ Form::close() }}
								</div>
							</td>
						</tr>
					@endforeach
				@else
					<tr >
						<td colspan="5" class="text-center">No adhoc payment components added.</td>
					</tr>
				@endif
				@endif
				</table>
			</div>
		</div>
	</div>
</div>
<script>
	$(function () {
		$('#type option[value="{{old('type')}}"]').attr("selected",true);
	});
</script>
@endsection
