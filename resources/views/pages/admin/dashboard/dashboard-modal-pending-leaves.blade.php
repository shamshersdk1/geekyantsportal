<div class="modal fade" id="dashboardPendingLeavesModal" tabindex="-1" role="dialog" aria-labelledby="dashboardConfirmationModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title" id="dashboardPendingLeavesLabel">Pending Leaves</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <table class="table table-striped table-fixed bg-white">
              <thead>
                <tr>
                        <th>Name</th>
                        <th>Type</th>
                        <th>From Date</th>
                        <th>To Date</th>
                        <th>No. of Days</th>
                        <th class="text-right">Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($pendingLeaves as $leave)
                     <tr>
                        <td>
                           {{$leave->user ? $leave->user->name : null}}
                        </td>
                        <td>
                           @if($leave->leaveType)
                              @if( $leave->leaveType->code == 'sick' && ( $leave->half == 'first' || $leave->half == 'second' ) )
                                 <span class="label label-info custom-label">{{ ucfirst($leave->half)}} Half Leave</span>
                              @elseif($leave->leaveType->code == 'sick')
                                 <span class="label label-danger custom-label">Sick Leave</span>
                              @elseif($leave->leaveType->code == 'paid')
                                 <span class="label label-primary custom-label">Paid Leave</span>
                              @else
                                 <span class="label label-warning custom-label">{{ ucfirst($leave->leaveType->title)}} Leave</span>
                              @endif
                           @endif
                        </td>
                        <td>
                           {{date('j M Y',strtotime($leave->start_date))}}
                        </td>
                        <td>
                           {{date('j M Y',strtotime($leave->end_date))}}
                        </td>
                        @if($leave->days==0.5)
                        <td>{{ucfirst($leave->half)}} half</td>
                        @else
                        <td>{{floor($leave->days)}}</td>
                        @endif
                        <td class="text-right">
                           <form action="/admin/leave-section/status/{{$leave->id}}" method="post" class="rejectform">
                              @if($leave->status == "pending")
                              <button class="btn btn-info btn-sm" type="button"
                                      ng-click="showModal({{$leave->id}})" data-title="Algorithms"><i class="fa fa-eye fa-fw"></i> View</button>
                              @else
                              <button class="btn btn-success btn-sm" disabled="disabled">Approve</button>
                              <button class="btn btn-danger btn-sm" disabled="disabled">Reject</button>
                              @endif
                           </form>
                        </td>
                     </tr>
                     @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" style="margin-top:10px;" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
