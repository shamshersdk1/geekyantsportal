<div class="panel panel-default">
	<div class="panel-heading clearfix" style="padding:0;">
		<h3 class="pull-left panel-title" style="padding: 13px 15px;">Leaves</h3>
		<ul class="nav nav-tabs pull-right" id="leaves" role="tablist" style="margin:0;">
		  <li class="" role="presentation">
		    <a class="nav-link" id="todays-leaves-tab" data-toggle="tab" href="#todays-leaves" role="tab" aria-controls="todays-leaves" aria-selected="false">Today's</a>
		  </li>
		  <li class="active" role="presentation">
		    <a class="nav-link" id="pending-leaves-tab" data-toggle="tab" href="#pending-leaves" role="tab" aria-controls="pending-leaves" aria-selected="true">Pending</a>
		  </li>
		  <li class="" role="presentation">
		    <a class="nav-link" id="upcoming-leaves-tab" data-toggle="tab" href="#upcoming-leaves" role="tab" aria-controls="upcoming-leaves" aria-selected="false">Upcoming</a>
		  </li>
		</ul>
	</div>
	<div class="">
		<div class="tab-content" id="leavesContent">
		  	<div class="tab-pane" id="todays-leaves" role="tabpanel">
		  		<table class="table table-striped bg-white">
                  <thead>
                     <tr>
                        <th>Today's Leaves</th>
                        @if ( count($todayAllLeaves) > 5 )
                        <th class="text-right">
                              <a data-toggle="modal" data-target="#dashboardModal" href="#dashboardModal">
                                    View All
                              </a>
                        </th>
                        @endif
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <th>Name</th>
                        <th class="text-right">Type</th>
                     </tr>
                     @if(count($todayAllLeaves) < 1)
                     <tr>
                        <td colspan="2">
                           No leaves today
                        </td>
                     </tr>
                     @else
                     @for ($i = 0; $i < 5; $i++)
                     @if( !empty($todayAllLeaves[$i]) )
                     <tr>
                        <td>
                           {{$todayAllLeaves[$i]->user->name}}
                        </td>
                        <td class="text-right">
                           @if($todayAllLeaves[$i]->type=='half')
                           {{ ucfirst($todayAllLeaves[$i]->type)}} Day Leave
                           @elseif($todayAllLeaves[$i]->type=='sick')
                           <span class="label label-danger custom-label">Sick Leave</span>
                           @elseif($todayAllLeaves[$i]->type=='paid')
                           <span class="label label-primary custom-label">Paid Leave</span>
                           @else
                           <span class="label label-success custom-label">{{ ucfirst($todayAllLeaves[$i]->type)}} Leave</span>
                           @endif
                        </td>
                     </tr>
                     @endif
                     @endfor
                     @endif
                  </tbody>
            	</table>
		  	</div>
		  	<div class="tab-pane active" id="pending-leaves" role="tabpanel">
           		<table class="table table-striped bg-white">
	              	<thead>
	                 	<tr>
	                    	<th colspan="5">Pending Leaves</th>
	                    	<th class="text-right">{{$pendingLeaves->count()}} of {{$pendingLeaves->total()}}</th>
	                 	</tr>
	              	</thead>
	              	<tbody>
	                 	<tr>
		                    <th>Name</th>
		                    <th>Type</th>
		                    <th>From Date</th>
		                    <th>To Date</th>
		                    <th>No. of Days</th>
		                    <th class="text-right">Action</th>
	                 	</tr>
		                @if(empty($pendingLeaves))
		                <tr>
		                    <td colspan="6">
		                       No pending leaves
		                    </td>
		                </tr>
		                @else
	                	@foreach($pendingLeaves as $leave)
	                 	<tr>
		                    <td>
		                       {{$leave->user->name}}
		                    </td>
		                    <td>
		                       @if($leave->type=='half')
		                       {{ ucfirst($leave->type)}} Day Leave
		                       @elseif($leave->type=='sick')
		                       <span class="label label-danger custom-label">Sick Leave</span>
		                       @elseif($leave->type=='paid')
		                       <span class="label label-primary custom-label">Paid Leave</span>
		                       @else
		                       <span class="label label-success custom-label">{{ ucfirst($leave->type)}} Leave</span>
		                       @endif
		                    </td>
		                    <td>
		                       {{date('j M Y',strtotime($leave->start_date))}}
		                    </td>
		                    <td>
		                       {{date('j M Y',strtotime($leave->end_date))}}
		                    </td>
		                    @if($leave->days==0.5)
		                    <td>{{ucfirst($leave->half)}} half</td>
		                    @else
		                    <td>{{floor($leave->days)}}</td>
		                    @endif
		                    <td class="text-right">
		                       <form action="/admin/leave-section/status/{{$leave->id}}" method="post" class="rejectform">
		                          @if($leave->status == "pending")
		                          <button class="btn btn-info btn-sm" type="button"
		                                  ng-click="showModal({{$leave->id}})" data-title="Algorithms"><i class="fa fa-eye fa-fw"></i> View</button>
		                          @else
		                          <button class="btn btn-success btn-sm" disabled="disabled">Approve</button>
		                          <button class="btn btn-danger btn-sm" disabled="disabled">Reject</button>
		                          @endif
		                       </form>
		                    </td>
	                 	</tr>
	                 	@endforeach
	                 	@endif
	              	</tbody>
	           	</table>
           	</div>
		  	<div class="tab-pane" id="upcoming-leaves" role="tabpanel">
		  		<table class="table table-striped bg-white">
                  	<thead>
                     <tr>
                        <th colspan="2">Upcoming Leaves</th>
                        @if ( count($upcomingAllLeaves) > 5 )
                        <th class="text-right">
                              <a data-toggle="modal" data-target="#dashboardUpcomingModal" href="#dashboardUpcomingModal">
                                    View All
                              </a>
                        </th>
                        @endif
                     </tr>
                  	</thead>
                  	<tbody>
                     <tr>
                        <th>Name</th>
                        <th>Duration</th>
                        <th class="text-right">Date</th>
                     </tr>
                     @if(count($upcomingAllLeaves) < 1 )
                     <tr>
                        <td colspan="3">
                           No upcoming leaves
                        </td>
                     </tr>
                     @else
                     @for ($j = 0; $j < 5; $j++)
                     @if( !empty($upcomingAllLeaves[$j]) )
                     <tr>
                        <td>
                           {{$upcomingAllLeaves[$j]->user->name}}
                        </td>
                        @if($upcomingAllLeaves[$j]->days==0.5)
                        <td>{{ucfirst($upcomingAllLeaves[$j]->half)}} half</td>
                        @elseif($upcomingAllLeaves[$j]->type == 'sick')
                        <td>
                           <span class="label label-danger custom-label">Sick Leave</span>
                        </td>
                        @elseif($upcomingAllLeaves[$j]->type == 'paid')
                        <td>
                           <span class="label label-primary custom-label">Paid Leave</span>
                        </td>
                        @else
                        <td>{{floor($upcomingAllLeaves[$j]->days)}} Days</td>
                        @endif
                        <td class="text-right">
                           From {{date('j M',strtotime($upcomingAllLeaves[$j]->start_date))}} to {{date('j M',strtotime($upcomingAllLeaves[$j]->end_date))}}
                        </td>
                     </tr>
                     @endif
                     @endfor
                     @endif
                  	</tbody>
               	</table>
           	</div>
		</div>
	</div>
</div>