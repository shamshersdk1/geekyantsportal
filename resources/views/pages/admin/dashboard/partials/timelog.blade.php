<table class="table table-striped bg-white">
  	<thead>
     	<tr>
	        <th>Timelogs</th>
	        <th colclass="text-right" colspan="5">
	              <a data-toggle="modal" data-target="#dashboardModal" href="#dashboardModal">
	                    View All
	              </a>
	        </th>
     	</tr>
  	</thead>
    <tbody>
        <thead>
	        <tr>
	          <th width="10%">Date</th>
	          <th width="15%">User</th>
	          <th width="10%">Projects</th>
	          <th width="10%">Total hours</th>
	          <th width="40%">Task</th>
	          <th class="text-right" width="10%">Action</th>
	        </tr>
	      </thead>
	   <tbody>
	   	<tr> 
          <td>03 Oct 2018</td>
          <td>User Name</td>
          <td>Project Name</td>
          <td>
            <input disabled="" class="form-control ng-pristine ng-untouched ng-valid ng-not-empty" type="text" ng-model-options="{ debounce: 1000 }" ng-model="record.duration" style="width:70px">
          </td>
          <td>
            <textarea disabled="" class="form-control ng-pristine ng-untouched ng-valid ng-not-empty" rows="2" style="" ng-model="record.task" ng-model-options="{ debounce: 1000 }"></textarea>
          </td>
          <td></td>
        </tr>
        <tr>
          	<td colspan="3"></td>
          	<td>
              <input ng-disabled="record.review.approver_id &amp;&amp; record.review.approver_id != 0" class="form-control ng-pristine ng-untouched ng-valid ng-not-empty" type="text" ng-model="record.review.approved_duration" style="width:70px">
          	</td>
          	<td>
              <textarea ng-disabled="record.review.approver_id &amp;&amp; record.review.approver_id != 0" class="form-control ng-pristine ng-untouched ng-valid ng-not-empty" rows="2" style="" ng-model="record.review.approval_comments"></textarea>
          	</td>
          	<td class="text-right" ng-if="!record.review.approver_id || record.review.approver_id == 0" class="ng-scope">
            	<button class="btn btn-sm btn-success text-center" ng-click="approve(record)">Approve</button>
          	</td>
        </tr>
     </tbody>
</table>