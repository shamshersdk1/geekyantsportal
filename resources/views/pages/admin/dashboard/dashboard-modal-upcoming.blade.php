<div class="modal fade" id="dashboardUpcomingModal" tabindex="-1" role="dialog" aria-labelledby="dashboardUpcomingModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title" id="dashboardUpcomingModalLabel">Upcoming Leaves</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <table class="table table-striped table-fixed bg-white">
              <thead>
                <tr>
                  <th width="30%">Name</th>
                  <th width="30%">Type</th>
                  <th width="5%">Days</th>
                  <th width="35%" class="text-right">Duration</th>
                </tr>
              </thead>
              <tbody>
                @foreach($upcomingLeaves as $leave)
                  <tr>
                        <td width="35%">
                           {{$leave->user ? $leave->user->name : ''}}
                        </td>
                        @if( $leave->leaveType->code == 'sick' && ( $leave->half == 'first' || $leave->half == 'second' ) )
                         <td >{{ ucfirst($leave->half)}} Half Leave</td>
                        @elseif($leave->leaveType->code == 'sick')
                        <td >
                           <span class="label label-danger custom-label">Sick Leave</span>
                        </td>
                        @elseif($leave->leaveType->code == 'paid')
                        <td >
                           <span class="label label-primary custom-label">Paid Leave</span>
                        </td>
                        @else
                        <td><span class="label label-warning custom-label">{{ ucfirst($leave->leaveType->title)}} Leave</span></td>
                        
                        @endif
                        <td ><span class="label label-info custom-label">{{floor($leave->days)}}</span></td>
                        <td  class="text-right">
                           From {{date('j M',strtotime($leave->start_date))}} to {{date('j M',strtotime($leave->end_date))}}
                        </td>
                     </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" style="margin-top:10px;" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
