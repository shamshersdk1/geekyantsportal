<div class="modal fade" id="dashboardConfirmationModal" tabindex="-1" role="dialog" aria-labelledby="dashboardConfirmationModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title" id="dashboardConfirmationModalLabel">Confirmations</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <table class="table table-striped table-fixed bg-white">
              <thead>
                <tr>
                  <th colspan="2">Name</th>
                     <th class="text-right">Joining Date</th>
                </tr>
              </thead>
              <tbody>
                @foreach($confirmations as $confirmation)
                  <tr>
                     <td>
                        {{$confirmation['name']}}
                     </td>
                     <td>
                        <span class="label label-default">{{$confirmation['diff']}}</span>
                     </td>
                     <td class="text-right">
                        <small>{{date_in_view($confirmation['date'])}}</small>
                     </td>
                  </tr>
                  @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" style="margin-top:10px;" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
