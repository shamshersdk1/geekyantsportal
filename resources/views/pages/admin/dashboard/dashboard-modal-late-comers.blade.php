<div class="modal fade" id="dashboardLateModal" tabindex="-1" role="dialog" aria-labelledby="dashboardLateModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title" id="dashboardLateModalLabel">Late Comers</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <table class="table table-striped table-fixed bg-white">
              <thead>
                <tr>
                  <th>Name</th>
                </tr>
              </thead>
              <tbody>
                @foreach($lateComers as $lateComer)
                  <tr>
                        <td width="35%">
                           {{$lateComer->user->name}}
                        </td>
                     </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" style="margin-top:10px;" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
