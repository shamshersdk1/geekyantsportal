@extends('layouts.admin-dashboard')
@section('page_heading','dashboard')

@section('main-content')
<script>
   $(function(){
       $('.news-grid,#carousel-left,#carousel-right').hover(
       function () {
           $('#carousel-left').css('opacity','1');
           $('#carousel-right').css('opacity','1');
       },
       function () {
           $('#carousel-left').css('opacity','0');
           $('#carousel-right').css('opacity','0');
       });
   });
</script>
<div class="dashboard-container dashboard-main" ng-app="myApp">
   <div class="btn-row clearfix">
      <div class="m-t-15 m-b">
         <a class="btn btn-success" href="/admin/bonuses/create">
            <i class="fa fa-plus"></i> Add Bonus
         </a>
         <a class="btn btn-success" href="/admin/bonuses/create">
            <i class="fa fa-plus"></i> Feedback
         </a>
      </div>
   </div>
   <div class="row" ng-controller="reportingManagerLeaveCtrl">
      <div class="col-md-6">
         @include('pages/admin/dashboard/partials/leaves')
         @include('pages/admin/dashboard/partials/timelog')
         <div class="row">
            <div class="col-md-8">
               @include('pages/admin/dashboard/partials/projects')
            </div>
            <div class="col-md-4">
               @include('pages/admin/dashboard/partials/my-team')
            </div>
         </div>
      </div>
      <div class="col-md-6">
         @include('pages/admin/dashboard/partials/calendar')
      </div>
   </div>
      <div class="row">
         <div class="col-md-6">
               <table class="table table-striped bg-white">
                  <thead>
                     <tr>
                         <th colspan="3"> Pending Appraisal</th>
                     </tr>
                     </thead>
                  <tbody>
                     <tr>
                        <th colspan="2">Name</th>
                        <th class="text-right">Last Update On</th>
                     </tr>
                     @if(empty($appraisals))
                     <tr>
                        <td>
                           No pending appraisals
                        </td>
                        <td></td>
                        <td></td>
                     </tr>
                     @else
                     @foreach($appraisals as $appraisal)
                     <tr>
                        <td>
                           {{$appraisal['name']}}
                        </td>
                        <td>
                           <span class="label label-default">{{$appraisal['diff']}}</span>
                        </td>
                        <td class="text-right">
                           {{date_in_view($appraisal['date'])}}
                        </td>
                     </tr>
                     @endforeach
                     @endif
                  </tbody>
               </table>

         </div>
         <div class="col-md-6 p-l-0">

               <table class="table table-striped bg-white">
               <thead>
                     <tr>
                         <th colspan="3">      Pending Confirmation</th>
                     </tr>
                     </thead>
                  <tbody>
                     <tr>
                        <th colspan="2">Name</th>
                        <th class="text-right">Joining Date</th>
                     </tr>
                     @if(empty($confirmations))
                     <tr>
                        <td>
                           No pending confirmations
                        </td>
                        <td></td>
                        <td></td>
                     </tr>
                     @else
                     @foreach($confirmations as $confirmation)
                     <tr>
                        <td>
                           {{$confirmation['name']}}
                        </td>
                        <td>
                           <span class="label label-default">{{$confirmation['diff']}}</span>
                        </td>
                        <td class="text-right">
                           {{date_in_view($confirmation['date'])}}
                        </td>
                     </tr>
                     @endforeach
                     @endif
                  </tbody>
               </table>

         </div>
      </div>
   </div>
   <div class="col-md-3 p-l-0">
      <div class="row">
            <div class="col-sm-12">
                  <table class="table table-striped bg-white">
                        <thead>
                           <th>Today's Late Comers</th>
                           @if( count($lateComersList) > 5 )
                           <th>
                                 <span class="pull-right">
                                       <a data-toggle="modal" data-target="#dashboardLateModal" href="#dashboardLateModal">
                                             View All
                                       </a>
                                 </span>     
                           </th>
                           @endif
                        </thead>
                        <tbody>
                           @if(count($lateComersList) < 1)
                           <tr>
                                 <td>
                                 No late comers today
                                 </td>
                           </tr>
                           @else
                                 @for ($i = 0; $i < 5; $i++)
                                       @if( !empty($lateComersList[$i]) )
                                       <tr>
                                             <td colspan="2">{{$lateComersList[$i]->user->name}}</td>
                                       </tr>
                                       @endif
                                 @endfor
                           @endif
                        </tbody>
                  </table>
            </div>
      </div>
         
      <div class="col-sm-12 p-0">

            <table class="table table-striped bg-white">
               <thead>
                  <tr>
                     <th>Birthdays</th>
                     <th></th>
                     <th></th>
                     <th></th>
                  </tr>
               </thead>
               @if ( !empty($upcomingBirthdays) )
               @foreach( $upcomingBirthdays as $upcomingBirthday )
               @if ( $upcomingBirthday['is_next'] )
               <tr  style="border-left: 5px solid #269900">
                  <td>{{date('j M',strtotime($upcomingBirthday['dob']))}}
                  <td>
                  <td>{{$upcomingBirthday['name']}}
                  <td>
               </tr>
               @else
               <tr>
                  <td>{{date('j M',strtotime($upcomingBirthday['dob']))}}
                  <td>
                  <td>{{$upcomingBirthday['name']}}
                  <td>
               </tr>
               @endif
               @endforeach
               @endif
            </table>

      </div>
      <div class="col-sm-12 p-0">

            <table class="table table-striped bg-white">
               <thead>
                  <tr>
                     <th>Holidays {{date('Y')}}</th>
                     <th>Day</th>
                  </tr>
               </thead>
               <tbody>
                  @if(count($holidays) > 0)
                  @foreach($holidays as $holiday)
                  @if(!empty($active)&&$holiday->date==$active->date)
                  <tr style="border-left: 5px solid #269900">
                     <td class="td-text"><small style="font-weight:900;color:#555">{{date('j M Y',strtotime($holiday->date))}} ({{$holiday->reason}})</small></td>
                     <td class="td-text"><small style="font-weight:900;color:#555"> {{date('l',strtotime($holiday->date))}}</small></td>
                  </tr>
                  @else
                  <tr>
                     <td class="td-text"><small>{{date('j M Y',strtotime($holiday->date))}} ({{$holiday->reason}})</small></td>
                     <td class="td-text"><small> {{date('l',strtotime($holiday->date))}}</small></td>
                  </tr>
                  @endif
                  @endforeach
                  @else
               </tbody>
               <tbody>
                  <tr>
                     <td colspan="2">No results found.</td>
                  </tr>
               </tbody>
               @endif
            </table>

      </div>
      <div class="col-sm-12 p-0">


            <table class="table table-striped bg-white">
            <thead>
                  <tr>
                     <th>  News</th>

                  </tr>
               </thead>
               @if(count($news) != 0 )
               <tbody>
                  <tr>
                     <td class="bg-white">
                        <div id="carousel" class="carousel slide" data-ride="carousel">
                           <!-- Wrapper for slides -->
                           <div class="carousel-inner" role="listbox">
                              @foreach( $news as $newsitem )
                              <div class="item {{ $loop->first ? ' active' : '' }}">
                                 <div>
                                    <div class="edit-news-head">{{$newsitem->title}}
                                       <button class="edit-news-btn pull-right" data-toggle="modal" data-target="#editNewsModal"
                                          data-id="{{$newsitem->id}}" data-title="Algorithms"><i class="fa fa-pencil-square-o fa-fw"></i></button>
                                    </div>
                                    <div class="news-date">
                                       {{date('j M Y',strtotime($newsitem->date))}}
                                    </div>
                                    <div class="news">
                                       {{$newsitem->description}}
                                    </div>
                                 </div>
                              </div>
                              @endforeach
                           </div>
                           <!-- Controls -->
                           <a id="carousel-left" class="left carousel-control" href="#carousel" role="button" data-slide="prev">
                           <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                           <span class="sr-only">Previous</span>
                           </a>
                           <a id="carousel-right" class="right carousel-control" href="#carousel" role="button" data-slide="next">
                           <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                           <span class="sr-only">Next</span>
                           </a>
                        </div>
                     </td>
                  </tr>
               </tbody>
               @endif
               @if(count($news) == 0 )
               <tbody>
                  <tr>
                     <td>No News avaialble</td>
                  </tr>
               </tbody>
               @endif
            </table>

      </div>
   </div>
</div>
@include('pages.admin.dashboard.dashboard-modal-today', ['todayLeaves' => $todayAllLeaves ])
@include('pages.admin.dashboard.dashboard-modal-upcoming', ['upcomingLeaves' => $upcomingAllLeaves ])
@include('pages.admin.dashboard.dashboard-modal-late-comers', ['lateComers' => $lateComersList ])
@include('pages.admin.userView.partials.js')
@endsection
