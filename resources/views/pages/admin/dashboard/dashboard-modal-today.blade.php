<div class="modal fade" id="dashboardModal" tabindex="-1" role="dialog" aria-labelledby="dashboardModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title" id="dashboardModalLabel">Today's Leaves</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <table class="table table-fixed table-striped bg-white">
              <thead>
                <tr>
                  <th width="50%">Name</th>
                  <th width="50%" class="text-right">Type</th>
                </tr>
              </thead>
              <tbody>
                @foreach($todayLeaves as $leave)
                  <tr>
                    <td width="50%">{{$leave->user->name}}</td>
                    <td width="50%" class="text-right">
                        @if( $leave->leaveType->code == 'sick' && ( $leave->half == 'first' || $leave->half == 'second' ) )
                          <span class="label label-info custom-label">{{ ucfirst($leave->half)}} Half Leave</span>
                        @elseif($leave->leaveType->code == 'sick')
                          <span class="label label-danger custom-label">Sick Leave</span>
                        @elseif($leave->leaveType->code == 'paid')
                          <span class="label label-primary custom-label">Paid Leave</span>
                        @else
                          <span class="label label-warning custom-label">{{ ucfirst($leave->leaveType->title)}} Leave</span>
                        @endif
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" style="margin-top:10px;" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
