<div class="modal fade" id="dashboardPendingLoans" tabindex="-1" role="dialog" aria-labelledby="dashboardPendingLoansLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title" id="dashboardUpcomingModalLabel">Pending Loans</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
          <table class="table table-striped">
               
               <tbody>
                  <tr>
                     <th>User</th>
                     <th>Amount</th>
                     <th>EMI</th>
                     <th>Status</th>
                     <th class="text-right">Action</th>
                        @if(count($pendingLoans) > 0)
                           @foreach($pendingLoans as $loan)
                              @if($loan->status != 'disbursed')
                                 <tr>

                                    <td class="td-text">
                                       {{$loan->user? $loan->user->name : ''}}
                                    </td>
                                    <td class="td-text">
                                       {{$loan->amount}}
                                    </td>
                                    <td class="td-text">
                                       {{$loan->emi}}
                                    </td>
                                    <td class="td-text">
                                       {{$loan->status}}
                                    </td>             
                                    <td class="td-text text-right">
                                       <a href="/admin/loan-applications/{{$loan->id}}" class="btn btn-info crud-btn btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View</a>                                  
                                    </td>
                                 </tr>
                              @endif
                           @endforeach
                        @else
                           <tr>
                              <td colspan="3">No results found.</td>
                           </tr>
                        @endif
                  </tr>
               </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" style="margin-top:10px;" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
