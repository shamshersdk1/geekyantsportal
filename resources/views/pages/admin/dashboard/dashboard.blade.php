@extends('layouts.admin-dashboard')
@section('page_heading','dashboard')

@section('main-content')
<script>
   $(function(){
       $('.news-grid,#carousel-left,#carousel-right').hover(
       function () {
           $('#carousel-left').css('opacity','1');
           $('#carousel-right').css('opacity','1');
       },
       function () {
           $('#carousel-left').css('opacity','0');
           $('#carousel-right').css('opacity','0');
       });
   });
</script>
<div class="dashboard-container dashboard-main clearfix" ng-app="myApp">
<div class="clearfix m-t-15 m-b-10">
   <a class="btn btn-success btn-sm" href="/admin/bonuses/create"><i class="fa fa-money fa-fw"></i> Add Bonus</a>
   <a class="btn btn-success btn-sm" href="/admin/payslip/pending"><i class="fa fa-file-o fa-fw"></i> Approve Payslip</a>
   <a class="btn btn-primary btn-sm pull-right hidden-xs" type="button" data-toggle="modal" data-target="#addNewsModal"><i class="fa fa-fw fa-newspaper-o"></i> Add News</a>
   <a class="btn btn-danger btn-sm pull-right" href="/user/leave-overview">Leave Overview</a>
   <!-- <a class="btn btn-success" style="margin-right:1%" type="button" data-toggle="modal" data-target="#addEventModal"><i class="fa fa-plus"></i> Add Event</a> -->
</div>


<div class="row" ng-controller="reportingManagerLeaveCtrl">
   <div class="col-lg-9 col-md-8">
      <div class="row">
         <!-- <div class="col-sm-12">
            @include('pages.partials.leave-overview')
         </div> -->
         
         <div class="col-sm-12">
            <div class="table-responsive">
               <table class="table table-striped bg-white">
                  <thead>
                     <tr>
                        <th colspan="5">Pending Leaves</th>
                        <th class="text-right">
                        @if ( count($pendingLeaves) > 5 )
                           <a data-toggle="modal" data-target="#dashboardPendingLeavesModal" href="#dashboardPendingLeavesModal">View All</a>
                        @endif
                        </th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <th>Name</th>
                        <th>Type</th>
                        <th>From Date</th>
                        <th>To Date</th>
                        <th>No. of Days</th>
                        <th class="text-right">Action</th>
                     </tr>
                     @if(empty($pendingLeaves))
                     <tr>
                        <td colspan="6">
                           No pending leaves
                        </td>
                     </tr>
                     @else
                     @foreach($pendingLeaves->take(5) as $pendingLeave)
                     <tr>
                        <td>
                           {{$pendingLeave->user ? $pendingLeave->user->name : null}}
                        </td>
                        <td>
                           @if($pendingLeave->leaveType)
                              @if( $pendingLeave->leaveType->code == 'sick' && ( $pendingLeave->half == 'first' || $pendingLeave->half == 'second' ) )
                                 <span class="label label-info custom-label">{{ ucfirst($pendingLeave->half)}} Half Leave</span>
                              @elseif($pendingLeave->leaveType->code == 'sick')
                                 <span class="label label-danger custom-label">Sick Leave</span>
                              @elseif($pendingLeave->leaveType->code == 'paid')
                                 <span class="label label-primary custom-label">Paid Leave</span>
                              @else
                                 <span class="label label-warning custom-label">{{ ucfirst($pendingLeave->leaveType->title)}} Leave</span>
                              @endif
                           @endif
                        </td>
                        <td>
                           {{date('j M Y',strtotime($pendingLeave->start_date))}}
                        </td>
                        <td>
                           {{date('j M Y',strtotime($pendingLeave->end_date))}}
                        </td>
                        @if($pendingLeave->days==0.5)
                        <td>{{ucfirst($pendingLeave->half)}} half</td>
                        @else
                        <td>{{floor($pendingLeave->days)}}</td>
                        @endif
                        <td class="text-right">
                           <form action="/admin/leave-section/status/{{$pendingLeave->id}}" method="post" class="rejectform">
                              @if($pendingLeave->status == "pending")
                              <button class="btn btn-info btn-sm" type="button"
                                      ng-click="showModal({{$pendingLeave->id}})" data-title="Algorithms"><i class="fa fa-eye fa-fw"></i> View</button>
                              @else
                              <button class="btn btn-success btn-sm" disabled="disabled">Approve</button>
                              <button class="btn btn-danger btn-sm" disabled="disabled">Reject</button>
                              @endif
                           </form>
                        </td>
                     </tr>
                     @endforeach
                     @endif
                  </tbody>
               </table>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-md-6">
               <div class="table-responsive">   
                  <table class="table table-striped bg-white">
                     <thead>
                        <tr>
                           <th colspan="2">Upcoming Leaves</th>
                           <th class="text-right">
                              @if ( count($upcomingAllLeaves) > 5 )
                                 <a data-toggle="modal" data-target="#dashboardUpcomingModal" href="#dashboardUpcomingModal">View All</a>
                              @endif
                           </th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr>
                           <th width="30%">Name</th>
                           <th width="40%">Type</th>
                           <th width="30%" class="text-right">Date</th>
                        </tr>
                        @if(count($upcomingAllLeaves) < 1 )
                        <tr>
                           <td colspan="3">
                              No upcoming leaves
                           </td>
                        </tr>
                        @else
                        @for ($j = 0; $j < 5; $j++)
                           @if( !empty($upcomingAllLeaves[$j]) )
                           <tr>
                              <td>
                                 {{$upcomingAllLeaves[$j]->user->name}}
                              </td>
                              <td>
                                 @if( $upcomingAllLeaves[$j]['leaveType']['code'] == 'sick' && ( $upcomingAllLeaves[$j]->half == 'first' || $upcomingAllLeaves[$j]->half == 'second' )  )
                                    <span class="label label-info custom-label">{{ucfirst($upcomingAllLeaves[$j]->half)}} half</span>
                                    <span class="label label-info custom-label">{{$upcomingAllLeaves[$j]->days}}</span>
                                 @elseif($upcomingAllLeaves[$j]['leaveType']['code'] == 'sick')
                                    <span class="label label-danger custom-label">Sick Leave</span>
                                    <span class="label label-info custom-label">{{floor($upcomingAllLeaves[$j]->days)}}</span>
                                 @elseif($upcomingAllLeaves[$j]['leaveType']['code'] == 'paid')
                                    <span class="label label-primary custom-label">Paid Leave</span>
                                    <span class="label label-info custom-label">{{floor($upcomingAllLeaves[$j]->days)}}</span>
                                 @else
                                 <span class="label label-warning custom-label">{{ucfirst($upcomingAllLeaves[$j]['leaveType']['title'])}} Leave</span>
                                    <span class="label label-info custom-label">{{floor($upcomingAllLeaves[$j]->days)}}</span>
                                 @endif
                              </td>
                              <td class="text-right">
                                 <small>From {{date('j M',strtotime($upcomingAllLeaves[$j]->start_date))}} to {{date('j M',strtotime($upcomingAllLeaves[$j]->end_date))}}</small>
                              </td>
                           </tr>
                           @endif
                        @endfor
                        @endif
                     </tbody>
                  </table>
               </div>
               <div class="table-responsive">
                  <table class="table table-striped bg-white">
                     <thead>
                        <tr>
                           <th colspan="4">Loan Request</th>
                           <th class="text-right">
                              @if (count($pendingLoans) > 5)
                                 <a data-toggle="modal" data-target="#dashboardPendingLoans" href="#dashboardPendingLoans">View All</a>
                              @endif
                           </th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr>
                           <th>User</th>
                           <th>Amount</th>
                           <th>Date</th>
                           <th>Status</th>
                           <th class="text-right">Action</th>
                              @if(count($pendingLoans) > 0)
                                 @foreach($pendingLoans as $loan)
                                    @if($loop->index < 5)
                                       <tr>

                                          <td class="td-text">
                                             @if($loan->user)
                                                {{$loan->user->name}}<br/>
                                                <small>({{$loan->user->employee_id}})</small>
                                             @endif
                                          </td>
                                          <td class="td-text">
                                             {{$loan->amount ? $loan->amount : ''}}
                                          </td>
                                          <td class="td-text">
                                             {{$loan->created_at ? date_in_view($loan->created_at) : ''}}
                                          </td>
                                          <td class="td-text">
                                             @if($loan->status=='pending')
                                                <label class="label label-primary">{{$loan->status}}</label>
                                             @elseif($loan->status=='submitted')
                                             <label class="label label-warning">{{$loan->status}}</label>
                                             @elseif($loan->status=='review')
                                             <label class="label label-dark">{{$loan->status}}</label>
                                             @elseif($loan->status=='reconcile')
                                             <label class="label label-info">{{$loan->status}}</label>
                                             @elseif($loan->status=='approved')
                                             <label class="label label-success">{{$loan->status}}</label>
                                             @else
                                             <label class="label label-danger">{{$loan->status}}</label>
                                             @endif
                                          </td>             
                                          <td class="td-text text-right">
                                             @if($loan->status=='pending')
                                                <a href="/admin/pending-loan-requests/{{$loan->id}}" class="btn btn-info crud-btn btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View</a>                                  
                                             @elseif($loan->status=='submitted')
                                                <a href="/admin/submitted-loan-requests/{{$loan->id}}" class="btn btn-info crud-btn btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View</a>                                  
                                             @elseif($loan->status=='review')
                                                <a href="/admin/reviewed-loan-requests/{{$loan->id}}" class="btn btn-info crud-btn btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View</a>                                  
                                             @elseif($loan->status=='reconcile')
                                                <a href="/admin/reconciled-loan-requests/{{$loan->id}}" class="btn btn-info crud-btn btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View</a>                                  
                                             @elseif($loan->status=='approved')
                                                <a href="/admin/approved-loan-requests/{{$loan->id}}" class="btn btn-info crud-btn btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View</a>                                  
                                             @else
                                             @endif
                                          </td>
                                       </tr>
                                    @endif
                                 @endforeach
                              @else
                                 <tr>
                                    <td colspan="3">No results found.</td>
                                 </tr>
                              @endif
                        </tr>
                     </tbody>
                  </table>
               </div>
               <div class="table-responsive">
                  <table class="table table-striped bg-white">
                     <thead>
                        <tr>
                           <th colspan="3"> Pending Appraisal</th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr>
                           <th colspan="2">Name</th>
                           <th class="text-right">Last Update On</th>
                        </tr>
                        @if(empty($appraisals))
                        <tr>
                           <td>
                              No pending appraisals
                           </td>
                           <td></td>
                           <td></td>
                        </tr>
                        @else
                        @foreach($appraisals as $appraisal)
                        <tr>
                           <td>
                              {{$appraisal['name']}}
                           </td>
                           <td>
                              <span class="label label-default">{{$appraisal['diff']}}</span>
                           </td>
                           <td class="text-right">
                              <small>{{date_in_view($appraisal['date'])}}</small>
                           </td>
                        </tr>
                        @endforeach
                        @endif
                     </tbody>
                  </table>
               </div>
         </div>
         <div class="col-md-6">
            <div class="table-responsive">
               <table class="table table-striped bg-white">
                  <thead>
                     <tr>
                        <th>Today's Leaves</th>
                        <th class="text-right">
                           @if ( count($todayAllLeaves) > 5 )
                              <a data-toggle="modal" data-target="#dashboardModal" href="#dashboardModal">View All</a>
                           @endif
                        </th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <th>Name</th>
                        <th class="text-right">Type</th>
                     </tr>
                     @if(count($todayAllLeaves) < 1)
                     <tr>
                        <td colspan="2">No leaves today</td>
                     </tr>
                     @else
                        @for ($i = 0; $i < 5; $i++)
                           @if( !empty($todayAllLeaves[$i]) )
                           <tr>
                              <td>
                                 {{$todayAllLeaves[$i]->user->name}}
                              </td>
                              <td class="text-right">
                                 @if($todayAllLeaves[$i]['leaveType']['code'] == 'sick' && ( $todayAllLeaves[$i]->half == 'first' || $todayAllLeaves[$i]->half == 'second' ))
                                    <span class="label label-info custom-label">{{ ucfirst($todayAllLeaves[$i]->type)}} Day Leave</span>
                                 @elseif($todayAllLeaves[$i]['leaveType']['code']=='sick')
                                    <span class="label label-danger custom-label">Sick Leave</span>
                                 @elseif($todayAllLeaves[$i]['leaveType']['code']=='paid')
                                    <span class="label label-primary custom-label">Paid Leave</span>
                                 @else
                                    <span class="label label-warning custom-label">{{ ucfirst($todayAllLeaves[$i]['leaveType']['title'])}} Leave</span>
                                 @endif
                              </td>
                           </tr>
                           @endif
                        @endfor
                     @endif
                  </tbody>
               </table>
            </div>
            <div class="table-responsive">
               <table class="table table-striped bg-white">
                  <thead>
                     <tr>
                        <th colspan="2">Pending Confirmation</th>
                        <th class="text-right">
                           @if ( count($confirmations) > 5 )
                              <a data-toggle="modal" data-target="#dashboardConfirmationModal" href="#dashboardConfirmationModal">View All</a>
                           @endif
                        </th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <th colspan="2">Name</th>
                        <th class="text-right">Joining Date</th>
                     </tr>
                     @if(empty($confirmations))
                     <tr>
                        <td>
                           No pending confirmations
                        </td>
                        <td></td>
                        <td></td>
                     </tr>
                     @else
                     @for ($i = 0; $i < 5; $i++)
                           @if( !empty($confirmations[$i]) )
                           <tr>
                              <td>
                                 {{$confirmations[$i]['name']}}
                              </td>
                              <td>
                                 <span class="label label-default">{{$confirmations[$i]['diff']}}</span>
                              </td>
                              <td>
                                 <small>{{date_in_view($confirmations[$i]['date'])}}</small>
                              </td>
                           </tr>
                           @endif
                        @endfor
                     @endif
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   <div class="col-lg-3 col-md-4">
      <div class="row">
            <div class="col-sm-12">
               <div class="table-responsive">
                  <table class="table table-striped bg-white">
                        <thead>
                              <th>Today's Late Comers</th>
                              @if( count($lateComersList) > 5 )
                              <th>
                                    <span class="pull-right">
                                          <a data-toggle="modal" data-target="#dashboardLateModal" href="#dashboardLateModal">
                                                View All
                                          </a>
                                    </span>     
                              </th>
                              @endif
                         </thead>
                        <tbody>
                              @if(count($lateComersList) < 1)
                              <tr>
                                    <td>
                                    No late comers today
                                    </td>
                              </tr>
                              @else
                                    @for ($i = 0; $i < 5; $i++)
                                          @if( !empty($lateComersList[$i]) )
                                          <tr>
                                                <td colspan="2">{{$lateComersList[$i]->user->name}}</td>
                                          </tr>
                                          @endif
                                    @endfor
                              @endif
                        </tbody>
                  </table>
               </div>
            </div>
      </div>
         
      <div class="col-sm-12 p-0">
         <div class="table-responsive">
            <table class="table table-striped bg-white">
               <thead>
                  <tr>
                     <th>Birthdays</th>
                     <th></th>
                     <th></th>
                     <th></th>
                  </tr>
               </thead>
               @if ( !empty($upcomingBirthdays) )
               @foreach( $upcomingBirthdays as $upcomingBirthday )
               @if ( $upcomingBirthday['is_next'] )
               <tr  style="border-left: 5px solid #269900">
                  <td>{{date('j M',strtotime($upcomingBirthday['dob']))}}
                  <td>
                  <td>{{$upcomingBirthday['name']}}
                  <td>
               </tr>
               @else
               <tr>
                  <td>{{date('j M',strtotime($upcomingBirthday['dob']))}}
                  <td>
                  <td>{{$upcomingBirthday['name']}}
                  <td>
               </tr>
               @endif
               @endforeach
               @endif
            </table>
         </div>
      </div>
      <div class="col-sm-12 p-0">
         <div class="table-responsive">
            <table class="table table-striped bg-white">
               <thead>
                  <tr>
                     <th>Holidays {{date('Y')}}</th>
                     <th>Day</th>
                  </tr>
               </thead>
               <tbody>
                  @if(count($holidays) > 0)
                  @foreach($holidays as $holiday)
                  @if(!empty($active)&&$holiday->date==$active->date)
                  @if($holiday->type == "Others")
                     <tr style="border-left: 5px solid #00C15F; background:#FFDAB9 !important">
                        <td class="td-text">
                           <small style="font-weight:900;color:#555">
                              {{date('j M Y',strtotime($holiday->date))}} ({{$holiday->reason}}) &nbsp;*Optional*
                           </small>
                        </td>
                  @elseif($holiday->type == "Weekend")
                     <tr style="border-left: 5px solid #00C15F; background-color:#AFEEEE">
                        <td class="td-text">
                           <small style="font-weight:900;color:#555">
                              {{date('j M Y',strtotime($holiday->date))}} ({{$holiday->reason}}) &nbsp;*Weekend*
                           </small>
                        </td>
                  @else 
                     <tr style="border-left: 5px solid #00C15F">
                        <td class="td-text">
                           <small style="font-weight:900;color:#555">
                              {{date('j M Y',strtotime($holiday->date))}} ({{$holiday->reason}})
                           </small>
                        </td>
                  @endif
                        <td class="td-text">
                           <small style="font-weight:900;color:#555">
                           {{date('l',strtotime($holiday->date))}}
                           </small>
                        </td>
                     </tr>
                  @else
                  @if($holiday->type == "Others")
                     <tr class='optional' style="background:#FFDAB9 !important">
                           <td class="td-text">
                                 <small style="font-weight:900;color:#555">
                                    {{date('j M Y',strtotime($holiday->date))}} ({{$holiday->reason}}) &nbsp;*Optional*
                                 </small>
                              </td>
                  @elseif($holiday->type == "Weekend")
                     <tr class='success' style="background-color:#AFEEEE">
                           <td class="td-text">
                                 <small style="font-weight:900;color:#555">
                                    {{date('j M Y',strtotime($holiday->date))}} ({{$holiday->reason}}) &nbsp;*Weekend*
                                 </small>
                              </td>
                  @else 
                     <tr>
                           <td class="td-text">
                                 <small style="font-weight:900;color:#555">
                                    {{date('j M Y',strtotime($holiday->date))}} ({{$holiday->reason}})
                                 </small>
                              </td>
                  @endif
                     <td class="td-text">
                        <small>
                        {{date('l',strtotime($holiday->date))}}
                        </small>
                     </td>
                  </tr>
                  @endif
                  @endforeach
                  @else
               </tbody>
               <tbody>
                  <tr>
                     <td colspan="2">No results found.</td>
                  </tr>
               </tbody>
               @endif
            </table>
         </div>
      </div>
      <div class="col-sm-12 p-0">
         <div class="table-responsive">
            <table class="table table-striped bg-white">
                  <thead><tr><th>  News</th></tr></thead>
                  @if(count($news) != 0 )
                  <tbody>
                     <tr>
                        <td class="bg-white">
                           <div id="carousel" class="carousel slide" data-ride="carousel">
                              <!-- Wrapper for slides -->
                              <div class="carousel-inner" role="listbox">
                                 @foreach( $news as $newsitem )
                                 <div class="item {{ $loop->first ? ' active' : '' }}">
                                    <div>
                                       <div class="edit-news-head">{{$newsitem->title}}
                                          <button class="edit-news-btn pull-right" data-toggle="modal" data-target="#editNewsModal"
                                             data-id="{{$newsitem->id}}" data-title="Algorithms"><i class="fa fa-pencil-square-o fa-fw"></i></button>
                                       </div>
                                       <div class="news-date">
                                          {{date('j M Y',strtotime($newsitem->date))}}
                                       </div>
                                       <div class="news">
                                          {{$newsitem->description}}
                                       </div>
                                    </div>
                                 </div>
                                 @endforeach
                              </div>
                              <!-- Controls -->
                              <a id="carousel-left" class="left carousel-control" href="#carousel" role="button" data-slide="prev">
                              <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                              <span class="sr-only">Previous</span>
                              </a>
                              <a id="carousel-right" class="right carousel-control" href="#carousel" role="button" data-slide="next">
                              <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                              <span class="sr-only">Next</span>
                              </a>
                           </div>
                        </td>
                     </tr>
                  </tbody>
                  @endif
                  @if(count($news) == 0 )
                  <tbody>
                     <tr>
                        <td>No News avaialble</td>
                     </tr>
                  </tbody>
                  @endif
            </table>
         </div>
      </div>
   </div>
</div>
@include('pages.admin.dashboard.dashboard-modal-today', ['todayLeaves' => $todayAllLeaves ])
@include('pages.admin.dashboard.dashboard-modal-upcoming', ['upcomingLeaves' => $upcomingAllLeaves ])
@include('pages.admin.dashboard.dashboard-modal-pending-loans', ['pendingLoans' => $pendingLoans ])
@include('pages.admin.dashboard.dashboard-modal-late-comers', ['lateComers' => $lateComersList ])
@include('pages.admin.dashboard.dashboard-modal-confirmation', ['confirmations' => $confirmations ])
@include('pages.admin.dashboard.dashboard-modal-pending-leaves', ['pendingLeaves' => $pendingLeaves ])
@include('pages.admin.userView.partials.js')
@endsection
