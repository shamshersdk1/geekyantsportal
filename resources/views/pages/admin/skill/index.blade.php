@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Records</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li class="active">Skill View</li>
                </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <a href="/admin/skill/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i> Add new Skill</a>
            </div>
        </div>
    </div>

    <div class="row">
         @if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                      @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif
        <div class="col-md-10">
            <form action="/admin/skill"  method="get">
                <div class="input-group">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-7">
                                <input type="text" id="su" name="searchuser" placeholder="User's Name" value="<?=( isset( $_GET['searchuser'] ) ? $_GET['searchuser'] : '' )?>"   class="form-control searchuser" >
                            </div>
                            <div class="col-md-2">
                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-primary btn-sm" >Search</button>
                                 </span>
                            </div>  
                            <div class="col-md-3">
                                 <button id="btnReset" class="btn btn-primary btn-sm">Reset</button>
                            </div>
                        </div>
                    </div>    
                </div>
            </form> 
        </div>
        <div class="col-md-12 user-list-view">
           
            <div class="panel panel-default">
                <table class="table table-striped">
                    <thead>
                        <th>Name of User</th>
                        <th>Pending Skills</th>
                        <th>Approved Skills</th>
                        <th class="text-right">Actions</th>
                    </thead>    
                    @if(count($users) > 0)
                        @if(isset($users))
                            @foreach($users as $user)
                                <tr>
                                    <td class="td-text"><a href="/admin/skill/{{$user->id}}/edit">{{$user->name}}</a></td>
                                    <td>{{$techArray[$user->id]}}</td>
                                    <td>{{$approvedSkills[$user->id]}}</td>
                                    <td class="text-right"> 
                                        <a href="/admin/skill/{{$user->id}}/edit" class="btn btn-info crud-btn btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View</a>
                                    </td>       
                                </tr>
                            @endforeach
                        @endif
                    @else
                        <tr>
                            <td colspan="6" class="text-cenbter">No results found.</td>
                        </tr>
                    @endif
                </table>
            </div>
        </div>  
    </div>
    <div class="pull-right">
            {{ $users->appends(array_except(Request::query(),'users'))->links(); }}
    </div>
</div>
<script type="text/javascript">
    $(function(){
        $( ".searchuser" ).autocomplete({
            source: {{$jsonuser}}
        });
        $("#btnReset").click(function(){
           window.location.href='/admin/skill';
        }); 
    });
</script>
@endsection



