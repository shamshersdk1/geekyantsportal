@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Add Skills</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/skill') }}">Skill</a></li>
                    <li class="active">Add</li>
                </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i>Back</button>
            </div>
        </div>
    </div>
        <div class="row">
            <div class="col-md-12">
            	@if(!empty($errors->all()))
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <span>{{ $error }}</span><br/>
                          @endforeach
                    </div>
                @endif
                @if (session('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ session('message') }}</span><br/>
                    </div>
                @endif   
	            <form class="form-horizontal" action="/admin/skill" method="post">
                    {{csrf_field()}}
                    <div class="panel panel-default" style="padding-top: 25px; padding-bottom: 25px;">    
                        <div class="form-group">
                           <label for="name" class="col-sm-3" style="padding-left: 55px;">Enter Name :</label>
                           <div class="col-sm-7">
                                <input type="text" name="name" placeholder="User's Name" value="{{old('name')}}" class="form-control searchuser" required>
                                <p id="name" style="color: red"></p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-sm-3" style="padding-left: 55px;">Select Technologies :</label>
                            <div class="btn-group col-sm-7" data-toggle="buttons">
                                @foreach ( $technologies as $tech )
                                    <label class="btn btn-default" style="margin:5px">
                                        <input type="checkbox" autocomplete="off" name="tech[]" value="{{$tech->id}}"> {{$tech->name}}
                                    </label>
                                @endforeach
                            </div>
                        </div>
                        <div class="text-center" style="padding-top: 15px;">
                            <button type="reset" class="btn btn-default"><i class="fa fa-rotate-left fa-fw"></i> Clear</button> 
                            <button type="submit" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Save</button>
                        </div>
                    </div>
                </form>
           </div>			
	   </div>
</div>
<script>
    $(function(){
        $( ".searchuser" ).autocomplete({
            source: {{$jsonuser}}
        });
    });
</script>
<style>
    .active.btn.focus, .active.btn {
        background-color:#5cb85c;
    }
</style>
@endsection