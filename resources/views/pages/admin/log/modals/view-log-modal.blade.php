<div class="modal fade" id="viewRecordModal" tabindex="-1" role="dialog" aria-labelledby="editEventModalLabel">
   <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="editEventModalLabel">View Records<span class="pull pull-right" id="date"></span></h4>
            </div>
            <div class="modal-body">
                <form>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Ip Details:</label>
                    <div class="col-sm-10">
                      <p id="ip"></p>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Route:</label>
                    <div class="col-sm-10">
                      <p id="route"></p>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Role:</label>
                    <div class="col-sm-10">
                      <p class="label label-info custom-label" id="user_role"></p>
                      <p class="label label-danger custom-label" id="admin_role"></p>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Data:</label>
                    <div class="col-sm-10">
                      <div id="data"></div>
                    </div>
                </div>
            </form>
            </div>
            <div class="modal-footer">
                <span class="pull-right">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </span>
            </div>
        </div>
    </div>
</div>


 
  
 
