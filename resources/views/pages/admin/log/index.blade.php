@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Records</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
		  			<li class="active">Log View</li>
        		</ol>
            </div>
        </div>
    </div>

    <div class="row ">
    	@if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span><br/>
                  @endforeach
            </div>
        @endif
        @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
		@endif
		<div class="col-md-12 search-section-top bg-white">
    	<div class="col-md-10">
            <form action="/admin/logs"  method="get">
                <div class="input-group">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-4">
                                <input type="text" id="su" name="searchuser" placeholder="User's Name" value="<?=( isset( $_GET['searchuser'] ) ? $_GET['searchuser'] : '' )?>"   class="form-control searchuser" >
                            </div>
                            <div class="col-md-4">
                                <div class='input-group date' id='datetimepickerStart_search'>
                                    <input type='text' id="sd" class="form-control" value="<?=( isset( $_GET['created_at'] ) ? $_GET['created_at'] : '' )?>" autocomplete="off" name="created_at" placeholder="Search By Date" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-primary btn-sm button-space">Search</button>
                                </span>
                            	<span class="input-group-btn">
                                    <button type="submit" class="btn btn-primary btn-sm button-space">Reset</button>
                                 </span>
                            </div>  
                        </div>
                    </div>    
                </div>
            </form>
		</div>
		</div>
        <div class="col-md-12 user-list-view m-t-15">
            <div class="panel panel-default">
            	<table class="table table-striped">
            		<thead>
	                    <th>Name of User</th>
	                    <th>User Role</th>
	                    <th>Ip Details</th>
	                    <th>Route</th>
	                    <th>Date</th>
	                    <th class="text-right">Actions</th>
	                </thead>    
            		@if(count($records) > 0)
	            		@if(isset($records))
		            		@foreach($records as $record)
			            		<tr>
			            			<td class="td-text">{{$record->user->name}}</td>
			            			<td>
				            			@if($record->user_role == "admin")
				            			<span class="label label-danger custom-label">{{$record->user_role}}</span>
				            			@else
				            			<span class="label label-info custom-label">{{$record->user_role}}</span>
				            			@endif
			            			</td>
			            			<td class="td-text"> {{$record->ip}}</td>
			            			<td class="td-text"> {{$record->route_path}}</td>
			            			<td class="td-text">{{date('j F Y - h:i a',strtotime($record->created_at))}}</td>
			            			<td class="text-right">
                                    <button class="btn btn-info btn-sm" type="button"
									data-toggle="modal" data-target="#viewRecordModal"
									data-id="{{$record->id}}" data-title="Algorithms"><i class="fa fa-eye"></i>View</button>                          
                                	</td> 		
			            		</tr>
		            		@endforeach
		            	@endif
	            	@else
	            		<tr>
	            			<td colspan="6" class="text-cenbter">No results found.</td>
	            		</tr>
	            	@endif
            	</table>
			</div>
		</div>	
		 <div class="pull-right">
            {{ $records->appends(array_except(Request::query(),'records'))->links(); }}
        </div>
	</div>
</div>
@include('pages/admin/log/modals/view-log-modal')
<script>
	let event;
	$(function(){
		$('#viewRecordModal').on("show.bs.modal", function (e) {
			let id = $(e.relatedTarget).data('id')
			$.ajax({
				type: 'GET',
				url: '/admin/logs/'+id,
				beforeSend: function() {
					$("div.loading").show();
					$("div.content").hide();
				},
				complete: function() {
					$("div.loading").hide();
					$("div.content").show();
				},
				success: function(res) {
					event = res;
					if(res.user_role == "admin"){
						$('#viewRecordModal #admin_role').text(" "+res.user_role);
						$('#viewRecordModal #user_role').hide();
						$('#viewRecordModal #admin_role').show();
					}
					else{
						$('#viewRecordModal #user_role').text(" "+res.user_role);
						$('#viewRecordModal #admin_role').hide();
						$('#viewRecordModal #user_role').show();
					}
					$('#viewRecordModal #ip').text(res.ip);
					$('#viewRecordModal #route').text(res.route_path);
					var d = new Date(res.created_at);
					var month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
					var date = d.getDate() + " " + month[d.getMonth()] + " " + d.getFullYear();
					var time = d.toLocaleTimeString().toLowerCase();
					var datadata = date +","+ time;
					$('#viewRecordModal #date').text(res.date);
					var k = JSON.parse(res.raw_post);
					$("#viewRecordModal #data").empty();
					for (let [key, value] of Object.entries(k)) {
						$("#viewRecordModal #data").append(key+ ":" + "  " + value + "<br>");
					    console.log(key, value);
					}
				}
			});
		});

		$("#viewRecordModal").on("hidden.bs.modal", function(){
			$(this).find("input")
				.val('')
				.end()
		});

		
        $('#datetimepickerStart_search').datetimepicker({
            minDate: new Date().getMonth() <= 3 ? new Date(new Date().getFullYear()-1+"-04-01") : new Date(new Date().getFullYear()+"-04-01"), //Set min date to April 1
            //Set max date to upcoming march
            maxDate: new Date().getMonth() <= 2 ? new Date(new Date().getFullYear()+"-03-31") : new Date(new Date().getFullYear()+1+"-03-31"),
            format: 'YYYY-MM-DD'
        });

        $( ".searchuser" ).autocomplete({
            source: {{$jsonuser}}
        });
       
        $("#btnReset").click(function(){
           window.location.href='/admin/logs';
    	}); 
	});
</script>
@endsection
