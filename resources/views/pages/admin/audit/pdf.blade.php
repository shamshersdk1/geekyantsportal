<!DOCTYPE html>
<html>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <head>
        <style>
            table {
                border: 1px solid #dddddd;
                font-family: arial, sans-serif;
                border-collapse: collapse;
                width: 100%;
                page-break-inside: avoid;
                font-size: 11px;
            }
            tr, td {
                border: 1px solid #dddddd;
                text-align: left;
                padding: 8px;
                page-break-inside: avoid;
                font-size: 11px;
            }
            p {
                font-size:12px;
            }
        </style>
    </head>
    <body>
        <h2> Audit Report: {{date_in_view($date)}}</h2>
        <ul>

           

            @forelse ($data['audits'] as $audit)
                @if($audit->auditable_type == 'App\Models\Admin\ItSaving')
                    <li>
                         
                        <hr />@lang('audit-it-saving.name', $audit->auditable->user->toArray())<hr />
                        @lang('audit-it-saving.'.$audit->event.'.metadata', $audit->getMetadata())
                        @foreach ($audit->getModified() as $attribute => $modified)
                        <ul>
                            <li>@lang('audit-it-saving.'.$audit->event.'.modified.'.$attribute, $modified)</li>
                        </ul>
                        @endforeach
                    </li>
                @endif
                @if($audit->auditable_type == 'App\Models\UserDetail')
                    <li>
                        <hr />@lang('audit-user-detail.name', $audit->auditable->user->toArray())<hr />
                        @lang('audit-user-detail.'.$audit->event.'.metadata', $audit->getMetadata())
                        @foreach ($audit->getModified() as $attribute => $modified)
                        <ul>
                            <li>@lang('audit-user-detail.'.$audit->event.'.modified.'.$attribute, $modified)</li>
                        </ul>
                        @endforeach
                    </li>
                @endif
                
                @if($audit->auditable_type == 'App\Models\User')
                    <li>
                        <hr />@lang('audit-user.detail', $audit->auditable->toArray())<hr />
                        @lang('audit-user.'.$audit->event.'.metadata', $audit->getMetadata())
                        @foreach ($audit->getModified() as $attribute => $modified)
                        <ul>
                            <li>@lang('audit-user.'.$audit->event.'.modified.'.$attribute, $modified)</li>
                        </ul>
                        @endforeach
                    </li>
                @endif
            @empty
            <p>@lang('audit-user-detail.unavailable_audits')</p>
            @endforelse

            @foreach ($data['appraisalAudits'] as $user_id => $audits)
            <li>
                <hr />@lang('audit-appraisal-component.name', App\Models\User::find($user_id)->toArray())<hr />
                @foreach ($audits as $audit)
                    @foreach ($audit->getModified() as $attribute => $modified)
                    <ul>
                        <li>@lang('audit-appraisal-component.'.$audit->event.'.modified.'.$attribute, $modified)</li>
                    </ul>
                    @endforeach
                @endforeach
            </li>
            @endforeach

        </ul>
    </body>
</html>