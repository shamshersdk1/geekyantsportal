<!DOCTYPE html>
<html>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <head>
        <style>
            table {
                border: 1px solid #dddddd;
                font-family: arial, sans-serif;
                border-collapse: collapse;
                width: 100%;
                page-break-inside: avoid;
                font-size: 11px;
            }
            tr, td {
                border: 1px solid #dddddd;
                text-align: left;
                padding: 8px;
                page-break-inside: avoid;
                font-size: 11px;
            }
            p {
                font-size:12px;
            }
        </style>
    </head>
    <body>
    <div class="row">
        <ul>
            @forelse ($audits as $audit)
            
                @if($audit->auditable_type == 'App\Models\UserDetail')
                    <li>
                        @lang('audit-user-detail.name', $audit->auditable->user->toArray())
                        @lang('audit-user-detail.'.$audit->event.'.metadata', $audit->getMetadata())
                        @foreach ($audit->getModified() as $attribute => $modified)
                        <ul>
                            <li>@lang('audit-user-detail.'.$audit->event.'.modified.'.$attribute, $modified)</li>
                        </ul>
                        @endforeach
                    </li>
                @endif
                @if($audit->auditable_type == 'App\Models\User')
                    <li>
                        @lang('audit-user.detail', $audit->auditable->toArray())
                        @lang('audit-user.'.$audit->event.'.metadata', $audit->getMetadata())
                        @foreach ($audit->getModified() as $attribute => $modified)
                        <ul>
                            <li>@lang('audit-user.'.$audit->event.'.modified.'.$attribute, $modified)</li>
                        </ul>
                        @endforeach
                    </li>
                @endif
            @empty
            <p>@lang('audit-user-detail.unavailable_audits')</p>
            @endforelse
        </ul>
    </div>

