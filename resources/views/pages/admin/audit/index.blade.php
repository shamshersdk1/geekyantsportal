@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-6">
                <h1 class="admin-page-title">Audits</h1>
                <ol class="breadcrumb">
                      <li><a href="/admin">Admin</a></li>
                      <li><a href="/admin/bank-transfer">Audits</a></li>
                </ol>
            </div>
            
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                    @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif
            @if(session('alert-class'))
                <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ session('alert-class') }}</span><br/>
                </div>
            @endif
        </div>
    </div>
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel panel-default">
                <table class="table table-striped" id="bank-table">
                <thead>
                    <th class="td-text">#</th>
                    <th class="td-text">Model</th>
                    <th class="td-text">Action</th>
                    <th class="td-text">User</th>
                    <th class="td-text">Time</th>
                    <th class="td-text">Old Values</th>
                    <th class="td-text">New Values</th>
                </thead>
                  <tbody id="audits">
                    @foreach($audits as $audit)
                      <tr>
                        <td>{{ $audit->id }}</td>
                        <td>{{ $audit->auditable_type }} (id: {{ $audit->auditable_id }})</td>
                        <td>{{ $audit->event }}</td>
                        <td>{{ $audit->user->name }}</td>
                        <td>{{ datetime_in_view($audit->created_at) }}</td>
                        <td>
                          <table class="table">
                            @foreach($audit->old_values as $attribute => $value)
                              <tr>
                                <td><b>{{ $attribute }}</b></td>
                                <td>{{ $value }}</td>
                              </tr>
                            @endforeach
                          </table>
                        </td>
                        <td>
                          <table class="table">
                            @foreach($audit->new_values as $attribute => $value)
                              <tr>
                                <td><b>{{ $attribute }}</b></td>
                                <td>{{ $value }}</td>
                              </tr>
                            @endforeach
                          </table>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
            </table>
    </div>
@endsection

