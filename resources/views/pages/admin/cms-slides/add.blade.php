@extends('layouts.admin-dashboard')
@section('main-content')

<section class="new-project-section">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">CMS Developers</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
			  			<li><a href="{{ url('admin/cms-developers') }}">CMS Developers</a></li>
			  			<li class="active">Add</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>

	    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif

		<form class="form-horizontal" method="post" action="/admin/cms-slides" enctype="multipart/form-data">
			<div class="panel panel-default">
				<div class="panel-body">
				    <div class="row">
				    	<div class="col-md-12">
						  	<div class="form-group">
								<label for="" class="col-sm-2 control-label">CMS Developer Technology*: </label>
								<div class="col-sm-4" style="padding-top: 10px">
								<input type="hidden" name="cms_technology_id" value="{{ $cmsDeveloper->id }}">
									{{$cmsDeveloper->name}}
								</div>
							</div>
						  	<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Title</label>
						    	<div class="col-sm-6">
						      		<input type="text" class="form-control" id="title" name="title" value="{{ old('title') }}">
						    	</div>
						  	</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Description :</label>
								<div class="col-sm-4">
									<textarea class="form-control" id="description" name="description" rows="3" placeholder="Enter details">{{old('description')}}</textarea>
								</div>
							</div>
							<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">User Name</label>
						    	<div class="col-sm-6">
						      		<input type="text" class="form-control" id="user_name" name="user_name" value="{{ old('user_name') }}">
						    	</div>
						  	</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Status:</label>
								<div class="col-sm-4">
									<select class="form-control" name="status">
										<option value="1">Active</option>
										<option value="0">Inactive</option>
									</select>
								</div>
							</div>
						  	<div class="form-group">
								<label for="" class="col-sm-2 control-label">Upload Image :</label>
								<div class="col-sm-4">    
									<input type="file" id="file" class="form-control" name="file" multiple="false">
								</div>
							</div>
					  	</div>
					</div>
				</div>
			</div>
  	  		<div class="text-center">
		  		<button type="submit" class="btn btn-success">Add Slide</button>
		  	</div>
		</form>
	</div>
</section>
@endsection