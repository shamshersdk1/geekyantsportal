@extends('layouts.admin-dashboard')
@section('main-content')

<section class="new-project-section">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">Referral Bonuses</h1>
                    <ol class="breadcrumb">
                        <li><a href="/admin">Admin</a></li>
                        <li><a href="{{ url('admin/bonus/referral') }}">Referral Bonuses</a></li>
                        <li class="active">Create </li>
                    </ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>

	    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif
		<form class="form-horizontal" method="post" action="/admin/bonus/referral" enctype="multipart/form-data">
			<div class="panel panel-default">
				<div class="panel-body">
				    <div class="row">
				    	<div class="col-md-12">
						<div class="form-group">
								<label for="" class="col-sm-2 control-label">Referred By<span style="color: red">*</span><br><span >(Bonus to be given.)</span></label>
								<div class="input-group col-sm-4">
									@if(isset($userList))
										<select id="selectid22" name="referral_id"  style="width=35%;" placeholder= "Select name">
											<option value=""></option>
											@foreach($userList as $x)
												<option value="{{$x->id}}" >{{$x->name}}</option>
											@endforeach
										</select>
									@endif
								</div>
							</div>
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">Referred Employee<span style="color: red">*</span></label>
								<div class="input-group col-sm-4">
									@if(isset($userList))
										<select id="selectid2" name="user_id"  style="width=35%;" placeholder= "Select name">
											<option value=""></option>
											@foreach($userList as $x)
									        	<option value="{{$x->id}}" >{{$x->name}}</option>
										    @endforeach
										</select>
									@endif
								</div>
							</div>						
							<!-- ======================== Date Picker ======================= -->
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Due On<span style="color: red">*</span></label>
                                <div class='input-group col-sm-4' id='datetimepicker6'>
                                    <input type='text' class="form-control" name="due_date"/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <!-- ======================== Date Picker ======================= -->
                            <div class="form-group">
								<label for="" class="col-sm-2 control-label">Amount<span style="color: red">*</span></label>
								<div class="input-group col-sm-4">
									<input type="number" id="Amount" class="form-control" step="0.01" placeholder="0.00" name="amount" required/>
								</div>
							</div>
                            <div class="form-group">
								<label for="" class="col-sm-2 control-label">Notes</label>
								<div class="input-group col-sm-4">
									<textarea name="notes" id="notes" rows="5" style="width: 100%" placeholder="Notes..." ></textarea>
								</div>
							</div>
					  	</div>
					</div>
				</div>
			</div>
  	  		<div class="text-center">
		  		<button type="submit" class="btn btn-success">Save</button>
		  	</div>
		</form>
	</div>
</section>
<script type="text/javascript">
    $(function () {
        $('#datetimepicker6').datetimepicker({
            format: 'YYYY-MM-DD'
        });
    });
</script>
@endsection
