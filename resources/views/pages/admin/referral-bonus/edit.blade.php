@extends('layouts.admin-dashboard')
@section('main-content')

<section class="new-project-section">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">Referral Bonuses</h1>
                    <ol class="breadcrumb">
                        <li><a href="/admin">Admin</a></li>
                        <li><a href="{{ url('admin/bonus/referral') }}">Referral Bonuses</a></li>
						<li>{{$referralBonus->id}}</li>
                        <li class="active">Edit</li>
                    </ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>

	    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif
		<form class="form-horizontal" method="post" action="/admin/bonus/referral/{{$referralBonus->id}}" enctype="multipart/form-data">
		 {{ method_field('PUT') }}
			<div class="panel panel-default">
				<div class="panel-body">
				    <div class="row">
				    	<div class="col-md-12">
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">Referred Employee<span style="color: red">*</span></label>
								<div class="input-group col-sm-4">
									@if(isset($userList))
										<select id="selectid2" name="user_id"  style="width=35%;" placeholder= "Select name">
											@foreach($userList as $user)
												@if( $user->id == $referralBonus->user_id)
													<option value="{{ $user->id}}" selected> {{$user->name}}</option>
												@else
													<option value=" {{$user->id}}"> {{$user->name}}</option>
												@endif
											@endforeach
										</select>
									@endif
								</div>
							</div>
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">Referred By<span style="color: red">*</span></label>
								<div class="input-group col-sm-4">
									@if(isset($userList))
										<select id="selectid22" name="referral_id"  style="width=35%;" placeholder= "Select name">
											@foreach($userList as $user)
												@if( $user->id == $referralBonus->referral_id)
													<option value="{{ $user->id}}" selected> {{$user->name}}</option>
												@else
													<option value=" {{$user->id}}"> {{$user->name}}</option>
												@endif
											@endforeach
										</select>
									@endif
								</div>
							</div>
							<div class="form-group">
                                <label for="" class="col-sm-2 control-label">Due On<span style="color: red">*</span></label>
                                <div class='input-group col-sm-4' id='datetimepicker6'>
                                    <input type='text' class="form-control" name="due_date" value="{{$referralBonus->due_date}}"/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">Amount<span style="color: red">*</span></label>
								<div class="input-group col-sm-4">
									<input type="number" id="Amount" class="form-control" step="0.01" placeholder="0.00" name="amount" value="{{$referralBonus->amount}}" required/>
								</div>
							</div>
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">Notes</label>
								<div class="input-group col-sm-4">
									<textarea name="notes" id="notes" rows="5" style="width: 100%" placeholder="Notes..." >{{$referralBonus->notes}}</textarea>
								</div>
							</div>
					  	</div>
					</div>
				</div>
			</div>
  	  		<div class="text-center">
		  		<button type="submit" class="btn btn-success">Save</button>
		  	</div>
		</form>
	</div>
</section>
<script type="text/javascript">
    $(function () {
        $('#datetimepicker6').datetimepicker({
            format: 'YYYY-MM-DD'
        });
    });
</script>
@endsection
