@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">GeekyAnts Policies</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Dashboard</a></li>
        		  	<li>List of Policies</li>
        		</ol>
            </div>
            <!-- <div class="col-sm-4 text-right m-t-10">
                <a href="/admin/users/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Add new User</a>
            </div> -->
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<h4>Policies</h4>
			<div class="panel panel-default">
				<div class="table-responsive">
					<table class="table table-hover table-striped">
						<tr>
							<th width="50%">Policy Name</th>
							<!-- <th>Publication Date</th> -->
							<th>Effective From</th>
							<th>Last Modified</th>
							<th class="text-right">Action</th>
						</tr>
						@foreach( $policies as $policy )
						<tr>
							<td>
								<strong>{{ $policy['name'] }}</strong><br />
								<small>{{ $policy['number'] }}</small>
							</td>
							<!-- <td>{{ $policy['publication_date'] }}</td> -->
							<td>{{ date_in_view($policy['effective_date']) }}</td>
							<td>{{ date_in_view($policy['updated_at']) }}</td>
							<td class="text-right">
								<a href="{{ $policy['url'] }}" target="_blank" class="btn btn-info btn-sm">View</a>
							</td>
						</tr>                  
						@endforeach
					</table>
				</div>
			</div>
		</div>
		<!-- <div class="col-md-4">
			<h4>Notification</h4>
			<div class="alert alert-info">
				<div>To know everything about your office <a target="_blank" href="https://www.notion.so/geekyants/GeekyAnts-8081c324f93f41159730a10f6f23e034"><strong>Visit here</strong></a></div>
			</div>
			<div class="panel panel-default">
				<div class="table-responsive">
					<table class="table table-hover table-striped">
						<tr>
							<th width="60%">Notification</th>
							<th>Date</th>
							<th class="text-right">Action</th>
						</tr>
						@foreach( $notifications as $notification )
						<tr>
							<td><strong>{{ $notification['name'] }}</strong></td>
							<td>{{  date_in_view($policy['effective_date']) }}</td>
							<td class="text-right">
								<a href="{{ $notification['url'] }}" target="_blank" class="btn btn-info btn-sm">View</a>
							</td>
						</tr>                  
						@endforeach
					</table>
				</div>
			</div>
		</div> -->
	</div>
</div>
@endsection

