@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
		<div class="row">
            <div class="col-sm-8">
				<h1 class="admin-page-title">Policy</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
					<li><a href="{{ url('admin/policy/') }}">Policy</a></li>
					<li class="active">{{$policy->id}}</li>
                </ol>
			</div>
			<div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
        </div>
	</div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">Edit Policy</div>
				<div class="panel-body">
				{{ Form::open(['url' => '/admin/policy/'.$policy->id, 'method' => 'put']) }}
				{{ Form::token() }}
				<div class="form-group col-md-12">
                    	{{ Form::label('name', 'Name :',['class' => 'col-md-4','style' => 'padding-top:1%','align' => 'right'])}}
						<div class="col-md-4" style="padding:0">
                    		{{ Form::text('name', $policy->name,['class' => 'form-control']) }}
						</div>
					</div>
					<div class="form-group col-md-12">
                    {{ Form::label('reference_number', 'Reference Number :',['class' => 'col-md-4','style' => 'padding-top:1%','align' => 'right'])}}
						<div class="col-md-4" style="padding:0">
                    		{{ Form::text('reference_number', $policy->reference_number,['class' => 'form-control']) }}
						</div>
					</div>
                    <div class="form-group col-md-12">
                    {{ Form::label('url', 'Url :',['class' => 'col-md-4','style' => 'padding-top:1%','align' => 'right'])}}
						<div class="col-md-4" style="padding:0">
                    		{{ Form::text('url', $policy->url,['class' => 'form-control']) }}
						</div>
					</div>
                    <div class="form-group col-md-12">
                    {{ Form::label('status', 'Status :',['class' => 'col-md-4','align' => 'right'])}}
						<div class="col-md-4" style="padding:0">
							{{Form::radio('status', true,$policy->status === 'active' ? true : false);}} Active  
							{{Form::radio('status', false,$policy->status === 'deactive' ? true : false);}} Deactive
						</div>
					</div>
                    <div class="form-group col-md-12">
                    {{ Form::label('type', 'Type :',['class' => 'col-md-4','align' => 'right'])}}
						<div class="col-md-4" style="padding:0">
							{{Form::radio('type', true,$policy->type === 'policy' ? true : false);}} Policy  
							{{Form::radio('type', false,$policy->type === 'notification' ? true : false);}} Notification
						</div>
					</div>
                    <div class="form-group col-md-12">
                    {{ Form::label('effectiveDate', 'Effective Date:',['class' => 'col-md-4','align' => 'right'])}}
                        <div class="col-md-4" style="padding:0">
                        <div class=' input-group date' id='effectiveDate'>
                            <input value ={{$policy->effective_date}} type='text' id="datetimeinputstart" class="form-control" autocomplete="off" name="effective_date"  placeholder="Effective Date" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                        </div>
					</div>
				<div class="col-md-12">
					{{ Form::submit('Update',['class' => 'btn btn-success','style' => 'display: block; margin: 0 auto']) }}
				</div>
				{{ Form::close() }}
				</div>
				</div>
				<div class="panel panel-default">
			</div>
		</div>
	</div>
</div>
<script>
	$(function () {
		$('#type option[value="{{old('type')}}"]').attr("selected",true);
	});
    $(document).ready(function() {
        $('#effectiveDate').datetimepicker({
            format: 'YYYY-MM-DD'
		});
    });
</script>
@endsection
