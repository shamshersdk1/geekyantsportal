@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
		<div class="row">
            <div class="col-sm-8">
				<h1 class="admin-page-title">Policy</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
					<li class="active">Policy</li>
                </ol>
			</div>
			<div class="col-sm-4 text-right m-t-10">
            </div>
        </div>
	</div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">Add New Policy</div>
				<div class="panel-body">
					{{ Form::open(['url' => '/admin/policy', 'method' => 'post']) }}
					<div class="form-group col-md-12">
                    	{{ Form::label('name', 'Name :',['class' => 'col-md-4','style' => 'padding-top:1%','align' => 'right'])}}
						<div class="col-md-4" style="padding:0">
                    		{{ Form::text('name', '',['class' => 'form-control']) }}
						</div>
					</div>
					<div class="form-group col-md-12">
                    {{ Form::label('reference_number', 'Reference Number :',['class' => 'col-md-4','style' => 'padding-top:1%','align' => 'right'])}}
						<div class="col-md-4" style="padding:0">
                    		{{ Form::text('reference_number', '',['class' => 'form-control']) }}
						</div>
					</div>
                    <div class="form-group col-md-12">
                    {{ Form::label('url', 'Url :',['class' => 'col-md-4','style' => 'padding-top:1%','align' => 'right'])}}
						<div class="col-md-4" style="padding:0">
                    		{{ Form::text('url', '',['class' => 'form-control']) }}
						</div>
					</div>
                    <div class="form-group col-md-12">
                    {{ Form::label('status', 'Status :',['class' => 'col-md-4','align' => 'right'])}}
						<div class="col-md-4" style="padding:0">
							{{Form::radio('status', true);}} Active  
							{{Form::radio('status', false,true);}} Deactive
						</div>
					</div>
                    <div class="form-group col-md-12">
                    {{ Form::label('type', 'Type :',['class' => 'col-md-4','align' => 'right'])}}
						<div class="col-md-4" style="padding:0">
							{{Form::radio('type', true,true);}} Policy  
							{{Form::radio('type', false);}} Notification
						</div>
					</div>
                    <div class="form-group col-md-12">
                    {{ Form::label('effectiveDate', 'Effective Date:',['class' => 'col-md-4','align' => 'right'])}}
                        <div class="col-md-4" style="padding:0">
                        <div class=' input-group date' id='effectiveDate'>
                            <input type='text' id="datetimeinputstart" class="form-control" autocomplete="off" name="effective_date"  placeholder="Effective Date" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                        </div>
					</div>
					<div class="col-md-12">
					{{ Form::submit('Save',['class' => 'btn btn-success','style' => 'display: block; margin: 0 auto']) }}
					</div>
                    {{ Form::close() }}
				</div>
				</div>
				<div class="panel panel-default">
				<table class="table table-hover table-striped">
					<tr>
						<th>Name</th>
                        <th>Reference Number</th>
						<th>Effective From</th>
						<th>Last Modified</th>
                        <th>Type</th>
						<th>Active</th>
						<th class="text-right">Action</th>
					</tr>
					@foreach( $policies as $policy )
					<tr>
						<td>
							<strong>{{ $policy['name'] }}</strong><br />
						</td>
                        <td>
                            <small>{{ $policy['reference_number'] }}</small>
                        </td>
						<td>{{ date_in_view($policy['effective_date']) }}</td>
						<td>{{ datetime_in_view($policy['updated_at']) }}</td>
                        <td>
                            {{$policy['type']}}
                        </td>
						<td>
								<div class="col-md-4">
								<div class="onoffswitch">
									<input type="checkbox" value="{{$policy->id}}" name="onoffswitch[]" onchange="toggle(this)"
									class="onoffswitch-checkbox" id="{{$policy->id}}"
									<?php echo (!empty($policy->status) && $policy->status == 'active') ? 'checked' : ''; ?>>
									<label class="onoffswitch-label" for= {{$policy->id}} >
										<span class="onoffswitch-inner"></span>
										<span class="onoffswitch-switch"></span>
									</label>
								</div>
							</div>
						</td>
						<td class="text-right">
                        	<a href="/admin/policy/{{$policy->id}}" class="btn btn-info crud-btn btn-sm"><i class="fa fa-pencil btn-icon-space" aria-hidden="true"></i>Edit</a>
							<a href="{{ $policy['url'] }}" target="_blank" class="btn btn-info btn-sm">View</a>
                            <div style="display:inline-block;" class="deleteform">
								{{ Form::open(['url' => '/admin/policy/'.$policy->id, 'method' => 'delete']) }}
								{{ Form::submit('Delete',['class' => 'btn btn-danger btn-sm','onclick' => 'return confirm("Are you sure you want to delete this item?")']) }}
								{{ Form::close() }}
							</div>
                        </td>
					</tr>                  
					@endforeach
				</table>
			</div>
		</div>
	</div>
</div>
<script>
	$(function () {
		$('#type option[value="{{old('type')}}"]').attr("selected",true);
	});
    $(document).ready(function() {
        $('#effectiveDate').datetimepicker({
            format: 'YYYY-MM-DD'
		});
    });
    $(function(){
        toggle = function (item) {
            window.location = "/admin/policy/update-status/"+item.value;
		}
	});
</script>
@endsection
