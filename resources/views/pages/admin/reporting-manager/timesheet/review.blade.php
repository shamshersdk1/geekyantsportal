@extends('layouts.admin-dashboard')
@section('main-content')
    <div class="container-fluid mis-report" ng-app="myApp" ng-controller="rmTimesheetReviewCtrl">
        <div class="breadcrumb-wrap">
           <div class="row">
              <div class="col-sm-8">
                 <h1 class="admin-page-title">Timesheet Review</h1>
                 <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/timesheet-review') }}">Weeks</a></li>
                    <li><a href="{{ url('admin/timesheet-review/'.$weekObj->id) }}">{{$weekObj->formatWeek()}}</a></li>
                    <li>{{$user->name}}</li>
                 </ol>
              </div>
              <div class="col-sm-4 text-right m-t-10">
                    <a href="{{ url('admin/timesheet-review/'.$weekObj->id.'/'. $user->id) }}" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</a>
                   @if($userWeeklyTimesheet->status != 'approved')
                   <button {{ $userWeeklyTimesheet->status != 'approved' && isset($data['pending_bonuse_request']) && $data['pending_bonuse_request'] > 0 ? 'disabled' : ''}} type="button" class="btn btn-success" ng-click="model.approveWeeklyTimesheet({{$weekObj->id}},{{$user->id}},'{{$weekObj->start_date}}')"><i class="fa fa-check fa-fw"></i> Approve</button>
                   @endif
                   <!-- <button ng-if="model.data.user_monthly_timesheet.status == 'pending' && model.data.can_approve" class="btn btn-primary btn-sm" ng-click="model.reviewTimesheet()" >Approve Timesheet</button> -->

                </div>
           </div>
        </div>

       <div class="row">
            <div class="col-sm-4">
                <table class="table table-striped table-condensed table-bordered bg-white">
                    <tr><td>Name</td><th>{{$user->name}}</th></tr>
                    <tr><td>Month</td><th>{{$weekObj->formatWeek()}}</th></tr>
                    <tr><td>Start Date</td><th>{{date_in_view($weekObj->getStartDay())}}</th></tr>
                    <tr><td>End Date</td><th>{{date_in_view($weekObj->getEndDay())}}</th></tr>
                    <tr><td>Status</td><th>
                    @if($userWeeklyTimesheet->status && $userWeeklyTimesheet->status=='pending')
                    <span class="label label-warning">{{$userWeeklyTimesheet->status}}</span>
                    @else
                    <span class="label label-primary">{{$userWeeklyTimesheet->status}}</span>
                    @endif
                    </th></tr>
                </table>
            </div>
            <div class="col-sm-4">
                <table class="table table-striped table-condensed table-bordered bg-white">
                    <tr>
                        <td>Employee's Expected Hours <small class="text-muted">After considering leaves / holidays</small></td>
                        <th>{{!empty($data['timesheets']['expected_hours']) ? $data['timesheets']['expected_hours'] : 0}}</th>
                    </tr>
                    <tr>
                        <td>Employee's Actual Work Hours</td>
                        <th>{{isset($data['timesheet_overview']['total_hour']) ? $data['timesheet_overview']['total_hour']: 0}}</th>
                    </tr>
                    <tr>
                        <td>Approved Hours</td>
                        <th>{{(isset($data['timesheet_overview']['total_hour']) && isset($data['timesheet_overview']['extra_hour'])) ? $data['timesheet_overview']['total_hour'] - $data['timesheet_overview']['extra_hour'] : 0}}</th>
                    </tr>
                    <tr>
                        <td>Total Extra Hours</td>
                        <th>{{isset($data['timesheet_overview']['extra_hour']) ? $data['timesheet_overview']['extra_hour']:0}}</th>
                    </tr>
                </table>
            </div>
            <div class="col-sm-4">
                <b>Extra Hour(s) Request</b>
                <table class="table table-bordered table-condensed table-hover bg-white">
                    <tr>
                        <th width="35%">Date</th>
                        <th class="text-center" width="15%">Hours</th>
                        <th width="10%">Status</th>
                    </tr>
                    @if(count($data['timesheet_details']['bonuses']['extra_hours'])!=0)
                        @foreach($data['timesheet_details']['bonuses']['extra_hours'] as $extra_hours)
                        <tr>
                                <td>
                                {{date_in_view($extra_hours->date)}}

                                </td>
                                <td class="text-center">
                                    {{$extra_hours->extra_hours}}
                                </td>
                                <td>

                                    @if($extra_hours->status === 'approved')
                                        <label class="label label-success">{{$extra_hours->status}}</label>
                                    @else
                                        <label class="label label-danger">{{$extra_hours->status}}</label>
                                    @endif

                                </td>
                        </tr>
                        @endforeach
                     @else
                        <tr>
                        <td colspan="3">No Records Found</td>
                        </tr>
                     @endif
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <b>Bonus Request</b>
                <table class="table table-bordered table-condensed table-hover bg-white">
                    <tr>
                        <th>Type </th>
                        <th>Date</th>
                        <th>Description</th>
                        <th>Status</th>
                        <th>Updated By</th>
                        <!-- <th>Amount</th> -->
                    </tr>
                    @if($data['timesheet_details']['bonuses']['extra_hours'] && count($data['bonuses']['bonus_requests'])>0)
                        @foreach($data['bonuses']['bonus_requests'] as $request)
                            <tr>
                                <td>
                                    {{$request->type}}
                                    @if(!empty($request->redeem_type['hours']))
                                        <label class="label label-default">{{$request->redeem_type['hours']}} hr(s)</label>
                                    @endif
                                </td>
                                <td>
                                    {{date_in_view($request->date)}}
                                </td>
                                <td>
                                    @if($request->type=="extra")
                                        Hours:{{json_decode($request->redeem_type)->hours}}h</td>
                                    @elseif($request->type=='additional')
                                        Hours::{{json_decode($request->redeem_type)->duration}}
                                    @elseif($request->type=='onsite')
                                        Onsite:

                                   @endif
                                <td>
                                    @if($request->status && $request->status == 'approved')
                                        <label class="label label-success">{{$request->status}}</label>
                                    @else
                                        <label class="label label-danger">{{$request->status}}</label>
                                    @endif
                                </td>
                                <td>
                                    @if($request->approvedBy)
                                        {{$request->approvedBy->name}} at {{datetime_in_view($request->updated_at)}}
                                    @endif
                                </td>
                                <!-- <td>
                                    @if($request->approvedBonus)
                                        {{$request->approvedBonus->amount}}
                                    @endif
                                </td> -->
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5">
                                No record found
                            </td>
                        </tr>
                    @endif
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <b>Month Report</b>
                <table class="table table-bordered table-hover bg-white">
                <thead>
                    <tr>
                        <th width="10%">Date</th>
                        <th width="30%">Timelog</th>
                        <th width="5%">Total Hrs</th>
                        <th width="5%">Auto-Approved</th>
                        <th width="30%">Additonal</th>
                    </tr>
                </thead>
                <tbody>
                     @if(!empty($data['timesheet_details']['timesheets']['days']))
                        @foreach($data['timesheet_details']['timesheets']['days'] as $day)
                        <tr
                            @if($day['on_leave'] == 'true')
                            class = "danger"
                            @elseif($day['is_weekend'] == 'true')
                            class = "warning"
                             @elseif($day['is_holiday'] == 'true')
                            class = "info"
                        @endif>
                            <td >
                            <!-- {{$day['is_weekend']}} -->
                                {{date_in_view($day['date'])}}
                                @if($day['on_leave']=='true')
                                    <strong>(On Leave)</strong>
                                @endif
                                @if($day['is_weekend']=='true')
                                    <strong>(Weekend)</strong>
                                @endif
                                @if($day['is_holiday']=='true')
                                    <strong>(Holiday)</strong>
                                @endif
                            </td>
                            <td>
                            @if(!empty($day['projects']) && count($day['projects']) > 0)
                            @foreach($day['projects'] as $project)
                                <div>
                                    <div>
                                        <strong>{{$project['project_name']}}</strong>
                                    </div>
                                    @foreach($project['user_timesheets'] as $userTimesheeet)
                                        <div class="m-b-10">

                                            {{$userTimesheeet['task']}} <strong>[{{$userTimesheeet['duration']}}]</strong>

                                        </div>
                                    @endforeach
                                </div>
                            @endforeach
                            @endif
                        </td>
                            <td class="text-center">{{isset($day['employee_total_time']) ? $day['employee_total_time'] : 0 }}</td>
                            <td class="text-center">
                            @if(!empty($day['employee_total_time']))
                                    {{ $day['employee_total_time'] <= 8 ? $day['employee_total_time']: 8 }}
                                @endif
                            </td>
                            <td class="">
                                <div class="row row-sm">
                                    <div class="col-sm-6 pull-right">


                                        @if(isset($day['extra_hour']) )
                                            <div class="input-group m-b-5 text-left">

                                                <span class="input-group-addon">
                                                    Extra Hrs
                                                </span>

                                                    <input class="form-control text-center" disabled="true" type="number" name="additional" value="{{$day['extra_hour']['extra_hours']}}"/>

                                            </div>
                                            <select class="form-control clearfix" disabled="true" }}">
                                                @foreach($day['projects'] as $project)
                                                    <option value="{{$project['id']}}">{{$project['project_name']}}</option>
                                                @endforeach
                                            </select>
                                            <label class="m-t-10" >
                                            @if(isset($day['onsite_bonus_request']['status']) && $day['onsite_bonus_request']['status'] == 'approved')
                                                    <span class="label label-success">Approved</span>&nbsp;
                                                @elseif(isset($day['onsite_bonus_request']['status']) && $day['onsite_bonus_request']['status'] == 'pending')
                                                <span  class="label label-warning">Pending</span>&nbsp;
                                                @endif

                                            </label>
                                        @endif
                                        @if(!empty($day['onsite_bonus_request']))
                                            <label class="m-t-10">
                                                Bonus Request :  Onsite
                                                @if(isset($day['onsite_bonus_request']['status']) && $day['onsite_bonus_request']['status'] == 'approved')
                                                    <span class="label label-success">Approved</span>&nbsp;
                                                @elseif(isset($day['onsite_bonus_request']['status']) && $day['onsite_bonus_request']['status'] == 'pending')
                                                <span  class="label label-warning">Pending</span>&nbsp;
                                                @endif

                                            </label>
                                        @endif
                                        </div>
                                        @if(!empty($day['bonus_requests']) && count($day['bonus_requests']) > 0)
                                        @foreach($day['bonus_requests'] as $bonusRequest)
                                            <div class="col-sm-6">
                                                @if(!empty($bonusRequest['type']))
                                                    <div class="well well-sm no-m-b">
                                                        <div class="clearfix m-b-5" >
                                                            Requested for <strong class="pull-right"> [{{$bonusRequest['type']}}]</strong>
                                                        </div>
                                                        <div>
                                                            <textarea disabled="true" class="form-control m-b-5" value="{{$bonusRequest['notes']}}" placeholder="Additional Notes..."></textarea>
                                                        </div>
                                                        @if(!empty($bonusRequest['status']) && $bonusRequest['status'] =='pending')
                                                            <span class="label label-primary">Pending</span>
                                                        @elseif(!empty($bonusRequest['status']) && $bonusRequest['status'] =='approved')
                                                            <span class="label label-success">Approved</span>
                                                        @elseif(!empty($bonusRequest['status']) && $bonusRequest['status'] =='rejected')
                                                            <span class="label label-danger">
                                                            Rejected</span>
                                                        @elseif(!empty($bonusRequest['status']) && $bonusRequest['status'] =='cancelled')
                                                            <span>Cancelled</span>
                                                        @endif
                                                        @if(!empty($bonusRequest['status']) && $bonusRequest['status'] =='approved')
                                                            <span class="small">by {{$bonusRequest['approved_bonus']['approver']['name']}}
                                                            </span>
                                                        @elseif(!empty($bonusRequest['status']) && $bonusRequest['status'] =='rejected')
                                                            <span class="small">
                                                            {{$bonusRequest['approved_bonus']['approvedBy']['name']}}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                @endif
                                            @endforeach
                                        @endif
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
            </div>
        </div>
        <!-- <div class="clearfix text-center">
            <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            <button type="button" class="btn btn-success"><i class="fa fa-check fa-fw"></i> Approve</button>
        </div> -->
    </div>
@endsection
