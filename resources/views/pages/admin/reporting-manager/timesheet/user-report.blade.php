@extends('layouts.admin-dashboard')
@section('main-content')
<script type="text/javascript">
    window.userId = '<?php echo $user->id; ?>';
    window.weekId = '<?php echo $weekObj->id; ?>';
</script>

    <div class="container-fluid" ng-app="myApp" ng-controller="rmTimesheetReviewCtrl">
        <div class="breadcrumb-wrap">
           <div class="row">
              <div class="col-sm-8">
                 <h1 class="admin-page-title">Weekly Timesheet : {{ $weekObj->formatWeek() }} of {{$user->name}}</h1>
                 <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/timesheet-review') }}">Weeks</a></li>
                    <li><a href="{{ url('admin/timesheet-review/'.$weekObj->id) }}">{{$weekObj->formatWeek()}}</a></li>
                    <li>{{$user->name}}</li>
                 </ol>
              </div>
           </div>
        </div>
        @if (!empty($error) && $error === true)
            <strong>{{$error_message}}</strong>
        @endif
        @if (!empty($success) && $success  == 1)
            <div class="row">
                <div class="col-sm-4">
                    <table class="table table-striped table-bordered bg-white">
                        <tr><td>Name</td><th>{{$user->name}}</th></tr>
                        <tr><td>Week</td><th>{{$weekObj->formatWeek()}}</th></tr>
                        <tr><td>RM</td><th>{{$user->reportingManager ? $user->reportingManager->name : ''}}</th></tr>
                        <tr><td>Status</td>
                        <th>
                            <span class="label label-warning" ng-if="model.data.user_weekly_timesheet.status =='pending'">%%model.data.user_weekly_timesheet.status %%</span>&nbsp;
                            <span class="label label-primary" ng-if="model.data.user_weekly_timesheet.status =='approved'">%%model.data.user_weekly_timesheet.status%% </span>
                            <span><small>by %%model.data.user_weekly_timesheet.approver.name%% at  %%model.data.user_weekly_timesheet.updated_at| dateToISO %%</small></span>
                        </th></tr>

                    </table>
                </div>
                <div class="col-sm-4">
                    <table class="table table-striped table-bordered bg-white m-b-10">
                        <tr scope="col">
                            <th> Project Name </th><th> Approved Hrs</th><th> Employee Hrs </th><th>Extra Hours</th>
                        </tr>
                        <tr ng-repeat="project in model.data.project_map">
                            <td>%%project.project.project_name%%</td>
                            <td>%%project.approved_total_time%%</td>
                            <td>%%project.employee_total_time%%</td>
                        </tr>
                        <tr>
                            <td><strong>Total</strong></td>
                            <td>
                                @if(isset($data['timesheet_overview']['total_hour']) && isset($data['timesheet_overview']['extra_hour']))
                                    <strong>{{$data['timesheet_overview']['total_hour'] - $data['timesheet_overview']['extra_hour']}}</strong>
                                @endif
                            </td>

                            <td>
                                @if(isset($data['timesheet_overview']['total_hour']))
                                    <strong>{{$data['timesheet_overview']['total_hour']}}</strong>
                                @endif
                            </td>
                            <td>
                                @if(isset($data['timesheet_overview']['extra_hour']))
                                    <strong>{{$data['timesheet_overview']['extra_hour']}}</strong>
                                @endif
                            </td>

                        </tr>
                    </table>
                </div>
                <div class="col-sm-4">
                <a href="/admin/timesheet-review/{{$weekObj->id}}/{{$user->id}}/review" class="btn btn-primary btn-sm">Review </a>
                <br/>
                <!-- <button ng-if="model.data.user_monthly_timesheet.status == 'pending' && model.data.can_approve" class="btn btn-primary btn-sm" ng-click="model.reviewTimesheet()" >Approve Timesheet</button> -->
                    <span ng-if="model.data.user_monthly_timesheet.status == 'approved'">Timesheet approved by  %%model.data.user_monthly_timesheet.approver.name%% on %%model.data.user_monthly_timesheet.updated_at | dateToISO%%</span>

                </div>
            </div>

            <table class="table table-bordered table-hover bg-white">
                <thead>
                    <tr>
                        <th width="10%">Date</th>
                        <th width="30%">Timelog</th>
                        <th width="5%">Total Hrs</th>
                        <th width="5%">Auto-Approved</th>
                        <th width="30%">Additonal</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="day in model.data.days track by $index" ng-if="day" ng-class="{'danger' : day.on_leave !== 'false', 'warning' : day.is_weekend !=='false', 'info' : day.is_holiday !=='false'}">
                        <td>
                            <!-- %%day.projects[0].user_timesheets[2]%% -->
                            %%day.date | date %% <br />
                            <span ng-if="day.on_leave !== 'false'"><strong>(On Leave)</strong></span>
                            <span ng-if="day.is_weekend !== 'false'"><strong>(Weekend)</strong></span>
                            <span ng-if="day.is_holiday !== 'false'"><strong>(Holiday)</strong></span>
                        </td>

                        <td>

                         <div ng-repeat="project in day.projects">
                                <div><strong>%%project.project_name%%</strong></div>
                                    <div ng-repeat="projects in project.user_timesheets">

                                      %%projects.task%%  <strong>[%%projects.duration  %% ]&nbsp</strong>
                                    </div>
                            </div>
                        </div>
                        </td>
                        <td class="text-center">%%day.employee_total_time%%</td>
                        <td class="text-center">
                            <span  ng-if="day.employee_total_time">%%day.employee_total_time <= 8 ? day.employee_total_time : 8 %%</span>
                        </td>

                        <td class="">
                            <div class="row row-sm" >
                                <div class="col-sm-6 pull-right" ng-if="day.extra_hour">
                                    <div class="input-group m-b-5 text-left" >
                                        <span class="input-group-addon">
                                            Extra Hrs
                                        </span>
                                        <input  ng-disabled="day.extra_hour.status=='approved' || day.extra_hour.status == 'rejected'"class="form-control text-center" type="number" name="additional" ng-model="day.extra_hour.extra_hours"/>

                                    </div>

                                    <select class="form-control clearfix"  ng-disabled="day.extra_hour.status=='approved' || day.extra_hour.status == 'rejected'" ng-model="day.extra_hour.project_id">
                                        <option ng-repeat="project in day.projects" ng-value="%%project.id%%">%%project.project_name%%</option>
                                    </select>
                                    <label class="m-t-10" ng-show="day.extra_hour.project_id">
                                            <span ng-if="day.extra_hour.status == 'approved'"  class="label label-success">Approved</span>
                                            <span ng-if="day.extra_hour.status == 'rejected'"  class="label label-danger">Rejected</span>

                                            <small ng-if="day.extra_hour.status">by %% day.extra_hour.approver.name%% at %%day.extra_hour.created_at | dateToISO%%</small>



                                            <div class="text-right" ng-hide="day.extra_hour.status === 'approved' || day.extra_hour.status === 'rejected' ">

                                            <a href="" class="btn btn-sm btn-default text-danger" ng-click="model.addExtra($index,'rejected')" >Reject</a>
                                            <a href="" class="btn btn-sm btn-success" ng-click="model.addExtra($index,'approved')" >Approve</a>

<!--
                                            <span ng-if="day.extra_hour.status == 'approved'"  class="label label-success">Approved</span>
                                            <span ng-if="day.extra_hour.status == 'rejected'"  class="label label-danger">
                                            Rejected</span>
                                            <span class="small" ng-if="day.extra_hour.status == 'approved'">by %%day%%</span>
                                            <span class="small" ng-if="day.extra_hour.status == 'rejected'">by %%day%%</span> -->

                                    </div>
                                    </label>
                                    <label class="m-t-10" ng-show="day.onsite_bonus_request">
                                        Bonus Request :  Onsite
                                        <span ng-if="day.onsite_bonus_request.status == 'approved'" class="label label-success">Approved</span>&nbsp;
                                        <span ng-if="day.onsite_bonus_request.status == 'pending'" class="label label-warning">Pending</span>&nbsp;
                                    </label>
                                </div>
                                <div class="col-sm-6" ng-repeat="bonus_request in day.bonus_requests">
                                    <div class="well well-sm no-m-b">
                                        <div class="clearfix m-b-5" >
                                            Requested for <strong class="pull-right"> [%%bonus_request.type%%]
                                                <span ng-if="bonus_request.redeem_info.redeem_type == 'pl'">[PL]</span>
                                                <span ng-if="bonus_request.redeem_info.redeem_type == 'encash'">[ENCASH]</span>
                                            </strong>

                                        </div>
                                        <div class="clearfix m-b-5">
                                            <select class="input-sm form-control" ng-model="bonus_request.redeem_info.onsite_allowance_id" style="margin: -4px 0;" ng-if="bonus_request.type == 'onsite'" ng-disabled="bonus_request.status =='approved' || bonus_request.status == 'rejected'">
                                            <option ng-repeat="x in bonus_request.onsite_eligibility.allowanceObj" ng-value="%%x.id%%">%%x.project.project_name%% ( %%x.type%%)</option>
                                        </select>
                                        </div>
                                        <div>
                                            <textarea ng-disable="bonus_request.status == 'approved'||bonus_request.status == 'rejected' " class="form-control m-b-5" ng-model="bonus_request.notes"placeholder="Additional Notes..."></textarea>
                                            <!-- <textarea  class="form-control m-b-5" ng-model="bonus_request.notes"placeholder="Additional Notes..."></textarea> -->
                                        </div>
                                         <div class="text-right" ng-hide="bonus_request.status == 'approved' || bonus_request.status == 'rejected' ">
                                            <a href="" class="btn btn-sm btn-default text-danger" ng-click="model.rejectBonusRequest($parent.$index, $index)" ng-disabled="bonus_request.loading">Reject</a>
                                            <a href="" class="btn btn-sm btn-success" ng-click="model.approveBonusRequest($parent.$index, $index)" ng-disabled="bonus_request.loading">Apporve</a>
                                        </div>

                                            <span ng-if="bonus_request.status == 'approved'"  class="label label-success">Approved</span>
                                            <span ng-if="bonus_request.status == 'rejected'"  class="label label-danger">
                                            Rejected</span>
                                            <span class="small" ng-if="bonus_request.status == 'approved'">by %%bonus_request.approved_bonus.approver.name%% </span>
                                            <span class="small" ng-if="bonus_request.status == 'rejected'">by %%bonus_request.approved_by.name%% %%bonus_request.updated_at%%</span>

                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    @endif
@endsection