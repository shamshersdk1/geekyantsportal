@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">List of Weeks</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/timesheet-review') }}">Weeks</a></li>
                </ol>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
        	@if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                      @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif
            <div class="user-list-view">
                <div class="panel panel-default">
                    <table class="display table table-striped">
                        <thead>
                            <tr>
                                <th width="25%">Week</th>
                                <th width="20%">Pending / Total</th>
                                <th width="20%">Created At</th>
                                <th width="35%" class="text-right" >Action</th>
                            </tr>
                        </thead>

                        @foreach($weeks as $week)
                            <tr>
                                <td>{{date_in_view($week->start_date)}} - {{date_in_view($week->end_date)}}</td>
                                <td>{{$week->pending->count()}}/{{$week->weekUser->count()}}</td>
                                <td>{{datetime_in_view($week->created_at)}}</td>
                                <td   class="text-center">
                                <div class="row">
                                    <div class="col-sm-2"></div>


                                    <div class="col-sm-6">
                                    @if(Auth::user()->role=='admin')
                                    <form method="post" action="week-lock/{{$week->id}}">
                                        @if($week->is_locked!=1)
                                            <button  class="btn btn-danger">Lock</button>
                                        @else
                                            <span class="label label-primary"> Locked </span>
                                            <small><br />at {{date_in_view($week->locked_at)}}</small>
                                        @endif
                                    </form>
                                    @endif
                                    </div>
                                    <div class="col-sm-4">
                                    <a href="/admin/timesheet-review/{{$week->id}}" class="btn btn-primary">View</a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                    </table>
                    <div class="col-md-12">
                        <div class="pageination pull-right">
                            <nav aria-label="Page navigation">
                                <ul class="pagination">
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <a class="btn btn-success crud-btn btn-lg add-newproj-btn" onclick="window.location='{{ url("admin/cms-project/create") }}'"><i class="fa fa-plus btn-icon-space" aria-hidden="true"></i>Add new Project</a> -->
        </div>
    </div>
</div>
@endsection
@section('js')
@parent
	<script>

		function deletetech() {
			var x = confirm("Are you sure you want to delete?");
			if (x)
				return true;
			else
				return false;
		}
	</script>
@endsection
