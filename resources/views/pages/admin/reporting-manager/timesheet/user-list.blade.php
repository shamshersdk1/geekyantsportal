@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-5">
                <h1 class="admin-page-title">List of Users</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/timesheet-review') }}">Weeks</a></li>
                    <li>{{$weekObj->formatWeek()}}</li>
                </ol>
            </div>
            <div class="col-sm-6">
            @if( date('Y-m-d') > $weekObj->end_date)
                <form action="timesheet-review/{{$weekObj->id}}" class=" text-right" method="post">
                    <button class="btn btn-primary"  >Smart Approval</button>
                </form>
                <small>Note: Auto approve all the timesheet having no bonus request and weekly timesheet hours is 40 hrs</small>
           @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
        	@if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                      @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif
            <div class="user-list-view">
                <div class="panel panel-default">
                    <table class="display table table-striped">
                        <thead>
                            <tr>
                                <th width="5%">#</th>
                                <th>Resource</th>
                                <th>RM</th>
                                <th>Status</th>
                                <th>Approved By </th>
                                <th>Created At</th>
                                <th class="sorting text-right">Action</th>
                            </tr>
                        </thead>
                        @foreach($users as $user)
                            <tr>
                                <td>{{$user->id}}</td>
                                <td>{{$user->user ? $user->user->name : ''}}</td>
                                <td>{{$user->parent ? $user->parent->name : ''}}</td>
                                <td>@if($user->status == 'approved')
                                        <span class="label label-success">Approved</span>
                                    @else
                                        <span class="label label-primary">Pending</span>
                                    @endif
                                </td>
                                <td>
                                    <div><strong>
                                    @if($user->approver)
                                        <small>{{$user->approver->name}} at {{datetime_in_view($user->updated_at)}}</small>
                                    @endif
                                </td>
                                <td>{{datetime_in_view($user->created_at)}}</td>
                                <td class="sorting text-right">
                                    <a href="/admin/timesheet-review/{{$weekObj->id}}/{{$user->user_id}}" class="btn btn-primary">View</a>
                                </td>
                            </tr>
                        @endforeach
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
@parent
	<script>

		function deletetech() {
			var x = confirm("Are you sure you want to delete?");
			if (x)
				return true;
			else
				return false;
		}
	</script>
@endsection
