@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title"></h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li><a href="{{ url('admin/my-team') }}">My Team</a></li>
					<li><a>Edit</a></li>
        		</ol>
            </div>
			<div class="col-sm-4 text-right m-t-10">
				<button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
			</div>
        </div>
    </div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>

    <div class="row">
	    <div class="col-md-12">
            @if(isset($user))
            <div class="panel-heading">
                    <h4 class="panel-title">User Basic Detail</h4>
                </div>
            	<div class="panel panel-default custom-space">
					<div class="row">
							<div class="col-md-2">
								<label>Name :</label>
							</div>
							<div class="">
							    {{$user->name}}
							</div> 
                            <br/>
							<div class="col-md-2">
								<label>Joining Date:</label>
							</div>
							<div class="">
								{{date_to_ddmmyyyy($user->joining_date)}}
                            </div>
                            <br/>
							<div class="col-md-2">
								<label>Confirmation Date:</label>
							</div>
							<div class="">
							    {{date_to_ddmmyyyy($user->confirmation_date)}}
                            </div>
                            <br/>
							<div class="col-md-2">
								<label>Employee Id:</label>
							</div>
							<div class="">
							    {{$user->employee_id}}
                            </div> 
                            <br/>
							<div class="col-md-2">
								<label>Date of Birth:</label>
							</div>
							<div class="">
								{{date_to_ddmmyyyy($user->dob)}}
							</div>
						</div>
                    </div>
                    <div class="panel-heading">
                            <h4 class="panel-title">User Settings</h4>
                        </div>
                        <div class="panel panel-default">
                            <span class="">
                                <label>Timesheet Required</label>
                                <input type='text' class="form-control" value="{{$user->timesheetRequired->value == 1 ? 'Yes' : 'No'}}"/>
                            </span>
                        </div>
                    <div class="panel-heading">
                        <h4 class="panel-title">User Timesheet Lock</h4>
                    </div>
					<div class="panel panel-default">
                        @if(isset($userLockObj))
                        <form action="save" method="POST" style="display:inline-block;" >

                            <input name="_method" type="hidden" value="PUT">

                            
                            <div class="">
                                <label>Lock Date</label>
                            </div>
                            <div class='input-group date' id='lock_date'>
                                <input type='text' id="sd" class="form-control" value="{{$user->timesheetLock->date ?? null}}" autocomplete="off" name="lock_date" minDate="{{$userLockObj->billing_date ?? null}}"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                            <div class="">
                                <label>Billing Lock Date</label>
                            </div>
                            <div class='input-group date' id='billing_date'>
                                <input type='text' id="sd" class="form-control" value="{{$user->timesheetLock->billing_date ?? null}}" autocomplete="off" name="billing_date" min="{{$userLockObj->date ?? null}}"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                            
                            <div class="text-center">
                                <button type="submit" class="btn btn-success">Update</button>
                            </div>
                        </form>
                        @else
                            <label>User Timesheet Lock Detail not found</label>
                        @endif
                    </div>
                    
					
            @else
                <label>Profile table is not linked</label>
            @endif
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $('#lock_date').datetimepicker({
            format: 'YYYY-MM-DD'
        });
        $('#billing_date').datetimepicker({
            format: 'YYYY-MM-DD'
        });
    });
    
 </script>
@endsection
