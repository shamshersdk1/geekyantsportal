@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">My Team</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/my-team') }}">My Team</a></li>
                </ol>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
    	@if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span><br/>
                  @endforeach
            </div>
        @endif
        @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
        @endif  
    <div class="user-list-view">
        <div class="panel panel-default"> 
            <table class="table table-striped">
                <thead>
                    <th width="">#</th>
                    <th width="">Name</th>
                    @if(Auth::user()->hasRole('admin'))
                        <th width="">RM</th>
                    @endif
                    <th width="">Lock Date</th>
                    <th width="">Billing Lock Date</th>
                    <th width="">Comment</th>
                    @if(Auth::user()->hasRole('admin'))
                        <th width="">Is Processed</th>
                    @endif
                    <th width="">Updated At</th>
                    @if(Auth::user()->hasRole('admin'))
                        <th width="">Timesheet Required</th>
                    @endif
                    <th width="">Actions</th>
                </thead>
                @if(count($users) > 0)
                    @foreach($users as $index => $user)
                        <tr>
                            <td class="td-text">{{$index+1}}</td>
                            <td class="td-text">{{$user->name}}</td>
                            @if(Auth::user()->hasRole('admin'))
                                <td class="td-text">{{$user->reportingManager->name ?? null}}</td>
                            @endif
                            <td class="td-text">{{date_in_view($user->timesheetLock->date ?? null)}}</td>
                            <td class="td-text">{{date_in_view($user->timesheetLock->billing_date ?? null)}}</td>
                            <td class="td-text">{{$user->timesheetLock->comment ?? null}}</td>
                            @if(Auth::user()->hasRole('admin'))
                                <td class="td-text">
                                    {{$user->timesheetLock ? (($user->timesheetLock->is_processed == 1 ) ? "Processed" : "Not Processed") : null}}
                                </td>
                            @endif
                            <td class="td-text">{{datetime_in_view($user->timesheetLock->updated_at ?? null)}}</td>
                            <td>
                                {{$user->timesheetRequired ? ($user->timesheetRequired->value == 1 ? "Yes" : "No") : null}}
                            </td>
                            <td class="td-text">
                            <div class="col-sm-4">
                                <a href="new-timesheet/review?user={{$user->id}}&monthId={{$monthId}}" class="btn btn-success btn-sm">View</a>
                            </div>  
                            <div class="col-sm-4">
                                <form action="/admin/my-team/{{$user->id}}/edit" method="put" style="display:inline-block;" >
                                    <button class="btn btn-primary pull-right btn-sm" type="submit"><i class="fa fa-edit btn-icon-space" aria-hidden="true"></i>Edit</button>
                                </form>
                            </div>   
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="10">No results found.</td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
    @if(!empty($users))
    {{$users->links()}}
@endif
	</div>
</div>
@endsection
