<@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <h1>Tech Talk</h1>
            <div class="breadcrumb-wrap">
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="/admin/tech-talk">Tech Talk</a></li>
                    <li class="active">Participate</li>
                </ol>
            </div>
            
        </div>
    </div>
   
    <div class="row">
        <div class="col-sm-6">
             <form method="post" action="/admin/tech-talk/{{$techTalkObj->id}}"> 

            
             <div class="form-group">
                <label>Employee Name</label>
                @if(isset($users))
                <select id="selectid1" name="employee_id" placeholder="Select name" style="width:50%;height:4%;">
                    <option value="$emp_name}}" >{{$emp_name}}</option>
                    @foreach($users as $user)
                    <option value="{{$user->id}}">{{$user->name}}</option>
                    @endforeach
                </select>
                @endif
            </div>
                <div class="form-group">
                    <label>Title</label>
                    <input type="text" class="form-control" name="title" value="{{$techTalkObj->title}}">
                </div>
                <div class="form-group">
                    <label>Description</label>
                    <textarea id="description" rows="3"  class="form-control" name="description" >{{$techTalkObj->description}}</textarea>
                </div>
                <div class="form-group">
                <div class='input-group date' id='datetimepicker1'>
					<input type='text' name="event_date" placeholder = "Event Date" class="form-control" value="{{$techTalkObj->event_date}}">
						<span class="input-group-addon">
							<span><i class="fa fa-calendar" aria-hidden="true"></i></span>
						</span>
				</div>
                </div>
               
                <button  type="submit" class="btn btn-success">Update</button>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
	$(function () {
		$('#datetimepicker1').datetimepicker({
			format: 'DD-MM-YYYY'
		});
		
	});
</script>
@endsection 
