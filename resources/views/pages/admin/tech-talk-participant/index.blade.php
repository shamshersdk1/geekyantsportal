@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-10">
                <h1>Tech Talk Participants</h1>
                <ol class="breadcrumb">
                    <li>Admin</li>
                    <li>Tech Talk Dashboard</li>
                </ol>
            </div>
            <div class="col-sm-2">
            <button type="button"  class="btn btn-success"  onclick="window.location='http://geekyants-portal.local.geekydev.com/admin/tech-talk/create'">Add Participants</button>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
       
       
        @if(session('message'))
        <div class="alert alert-success">
        <span>{{session('message')}}</span><br/>
        </div>
        @endif
            <table class="table table-striped">
                <tr>
                    <th width="5%">#</th>
                    <th width="11%">User Id</th>
                    <th width="11%">User name</th>
                    <th width="11%">Title</th>
                    <th width="11%">Description</th>
                    <th width="11%">Date</th>
                    <th width="11%">Status</th>
                    <th width="11%">Created At</th>
                    <th width="18%">Actions</th>
                </tr>
                @if(!empty($techtalk))
                    @foreach($techtalk as $techtalks )

                        <tr>
                            <td>{{$techtalks->id}}</td>
                             <td>{{$techtalks->employee_id}}</td>
                             <td>@if($techtalks->user) {{$techtalks->user->name}}@endif</td>
                             <td>{{$techtalks->title}}</td>
                             <td>{{$techtalks->description}}</td>
                             <td>{{date_in_view($techtalks->event_date)}}</td>
                             <td>{{$techtalks->status}}</td> 
                             <td>{{date_in_view($techtalks->created_at)}}</td> 
                             <td>
                                 <a class="btn btn-danger" href="/admin/tech-talk/edit/{{$techtalks->id}}" >Edit</a>
                                  <a class="btn btn-success" onclick="return confirm('Are you sure want to delete this participant?')?href='/admin/tech-talk/delete/{{$techtalks->id}}':href='/admin/tech-talk'">Delete</a>
                             </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="7">No results found...</td>
                    </tr>
                @endif
                
            </table>
        </div>
    </div>
</div>
<script>
    // function deleteEntry(id){
    //     if(confirm("Are You Sure Want to Delete this Participant Entry!")){
    //         window.location.href="tech-talk/id";
    //     }
        
    }
</script>
@endsection 
