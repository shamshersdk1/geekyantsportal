@extends('layouts.admin-dashboard')
@section('main-content')
    <div class="container-fluid">
        <div class="breadcrumb-wrap">
            <div class="row">
                <div class="col-sm-8">
                    <h1 class="admin-page-title">Salary Review for <strong>Megha Kumari</strong> (B102) for the Month of <strong>Feb</strong></h1>
                    <ol class="breadcrumb">
                        <li><a href="/admin">Admin</a></li>
                        <li class="active">Salary Review</li>
                    </ol>
                </div>
                <div class="col-sm-4 text-right m-t-10">
                    <button type="button" class="btn btn-warning">On Hold</button>
                    <button type="button" class="btn btn-success">Approve</button>
                </div>
            </div>
        </div>
        <div class="main-content">
            <div class="row row-sm">
                <div class="col-sm-4">
                    <h5>Emplyoee Info</h5>  
                    <div class="panel panel-default m-b-10">
                        <div class="info-table">
                            <label>Employee Name</label>
                            <span>Megha Kumari</span>
                        </div>
                        <div class="info-table">
                            <label>Employee Code</label>
                            <span>B123</span>
                        </div>
                        <div class="info-table">
                            <label>Designation</label>
                            <span>Ui / UX Lead</span>
                        </div>
                        <div class="info-table">
                            <label>Period</label>
                            <span>Jan</span>
                        </div>
                        <div class="info-table">
                            <label>DOJ</label>
                            <span>11th June 2012</span>
                        </div>
                        <div class="info-table">
                            <label>DOB</label>
                            <span>10th June 1986</span>
                        </div>
               
                        
                        <div class="info-table">
                            <label>PF UAN</label>
                            <span>100584817291</span>
                        </div>
                        <div class="info-table">
                            <label>ESI No.</label>
                            <span>NA</span>
                        </div>
                        <div class="info-table">
                            <label>PAN</label>
                            <span>BGRPK9156P</span>
                        </div>
                        <div class="info-table">
                            <label>Bank A/c. No</label>
                            <span>3711655279</span>
                        </div>
                    </div>

                </div>
                <div class="col-sm-4">
                    <h5>CTC</h5>    
                    <div class="panel panel-default m-b-5">
                        <div class="info-table">
                            <label>CTC</label>
                            <span>1234567</span>
                        </div>
                        <div class="info-table">
                            <label>Monthly</label>
                            <span>{{$ctc / 12}}</span>
                        </div>
                        <div class="info-table">
                            <label>Revised On</label>
                            <span>1 Feb 2019</span>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="info-table">
                            <label>Basic</label>
                            <span>{{$ctc}}</span>
                        </div>
                        <div class="info-table">
                            <label>House Rent Allowance</label>
                            <span>{{$ctc / 12}}</span>
                        </div>
                        <div class="info-table">
                            <label>Car Allowance</label>
                            <span>{{$ctc / 12}}</span>
                        </div>
                        <div class="info-table">
                            <label>Food Allowance</label>
                            <span>{{$ctc / 12}}</span>
                        </div>
                        <div class="info-table">
                            <label>Leave Travel Allowance</label>
                            <span>{{$ctc / 12}}</span>
                        </div>
                        <div class="info-table">
                            <label>Special Allowance</label>
                            <span>{{$ctc / 12}}</span>
                        </div>
                        <div class="info-table">
                            <label>Employee’s E.S.I.</label>
                            <span>{{$it_saving}}</span>
                        </div>
                        <div class="info-table">
                            <label>Professional Tax</label>
                            <span>{{$it_saving}}</span>
                        </div>
                        <div class="info-table">
                            <label>Food Deduction</label>
                            <span>{{$it_saving}}</span>
                        </div>
                        <div class="info-table">
                            <label>Medical Insurance</label>
                            <span>{{$it_saving}}</span>
                        </div>
                        <div class="info-table">
                            <label>T.D.S.</label>
                            <span>{{$it_saving}}</span>
                        </div>
                        <div class="info-table">
                            <label>Other Deduction</label>
                            <span>{{$it_saving}}</span>
                        </div>
                    </div>

                    
                    @include('pages.admin.salary.partials.tax-info')
                </div>
                <div class="col-sm-4">
                    
                    @include('pages.admin.salary.partials.leave-info')

                    @include('pages.admin.salary.partials.earnings')

                    @include('pages.admin.salary.partials.tax-deduction')
                </div>
            </div>
            <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row row-sm">
                                <div class="col-sm-3">
                                    <label>Total Earning</label>
                                    <p class="no-margin text-info"><strong>39000/-</strong></p>
                                </div>
                                <div class="col-sm-3">
                                    <label>Total Deduction</label>
                                    <p class="no-margin text-info"><strong>4000/-</strong></p>
                                </div>
                                <div class="col-sm-3">
                                    <label>Net Earning</label>
                                    <p class="no-margin text-info"><strong>35000/-</strong></p>
                                </div>

                                <div class="col-sm-3 text-right m-t-10">
                                    <button type="button" class="btn btn-warning">On Hold</button>
                                    <button type="button" class="btn btn-success">Approve</button>
                                </div>
                            </div>
                        </div>
                    </div>
            
        </div>
    </div>
@endsection