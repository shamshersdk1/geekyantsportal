@extends('layouts.admin-dashboard')
@section('main-content')
    @if(!$success)
        <div class="container-fluid">
            Something went wrong.
        </div>
    @else
        <div class="container-fluid">
            <div class="breadcrumb-wrap">
                <div class="row">
                    <div class="col-sm-8">
                        <h1 class="admin-page-title">Salary Review MIS</strong></h1>
                        <ol class="breadcrumb">
                            <li><a href="/admin">Admin</a></li>
                            <li class="active">Salary Review</li>
                        </ol>
                    </div>
                    <div class="col-sm-4 text-right">
                        <label class="no-margin">Last Month Working days</label>
                        <div><p class="lead no-margin">{{count($lastMonthCalendar['summary']['workingDays'])}} / {{count($lastMonthCalendar['summary']['workingDays']) * 8}}hrs</p></div>
                    </div>
                    <div class="col-sm-4 text-right">
                        <label class="no-margin">This Month Working days</label>
                        <div><p class="lead no-margin">{{count($thisMonthCalendar['summary']['workingDays'])}} / {{count($thisMonthCalendar['summary']['workingDays']) * 8}}hrs</p></div>
                    </div>
                </div>
            </div>
            <table class="table table-bordered table-hover bg-white">
                <tr>
                    <td>Employee ID</td>
                    <td>Employee Name</td>
                    <td>Annual Variable Pay</td>
                    <td>Monthly Vairable Pay</td>
                    <td>Annual Inhand</td>
                    <td>Monthly Inhand</td>
                    <td>Days Worked</td>
                    <td>Used SL</td>
                    <td>Used PL</td>
                    <td>Applicable Paid Days</td>
                    <td>Variable Pay (Last Month) - Timesheet</td>
                    <td>Variable Pay (Last Month) - Rating</td>
                    <td>Variable Pay (Sum)</td>
                    <td>Domestic Onsite (Local)</td>
                    <td>Domestic Onsite (Outstation)</td>
                    <td>International Onsite</td>
                    <td>Tech Talk Bonus</td>
                    <td>Annual Bonus</td>
                    <td>Confirmation Bonus</td>
                    <td>Extra Working Days (Weekends)</td>
                    <td>Extra Working Days (National Holidays)</td>
                    <td>Working Hrs</td>
                    <td>Approved Hrs</td>
                    <td>Employee Hrs</td>
                </tr>
                @foreach($users as $user)
                    @php 
                        if(!isset($payslipData[$user->id])) 
                            continue;
                    @endphp
                    <tr>
                        <td>{{$user->employee_id}}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$payslipData[$user->id]['payslipMaster']->variable_pay}}</td>
                        <td><strong>{{round($payslipData[$user->id]['payslipMaster']->variable_pay/12,2)}}</strong></td>
                        <td>{{$payslipData[$user->id]['payslipMaster']->annual_in_hand}}</td>
                        <td><strong>{{round($payslipData[$user->id]['payslipMaster']->annual_in_hand/12,2)}}</strong></td>
                        <td>{{$userCalendar[$user->id]['workedDays']}}</td>
                        <td>{{$userCalendar[$user->id]['sickLeaves']+$userCalendar[$user->id]['halfLeaves']}}</td>
                        <td>{{$userCalendar[$user->id]['paidLeaves']}}</td>
                        <td>-</td>
                        <td>{{$varaiblePay[$user->id]['timesheetAmount']}}</td>
                        <td>{{$varaiblePay[$user->id]['ratingAmount']}}</td>
                        <td>{{$varaiblePay[$user->id]['totalAmount']}}</td>
                        <td>{{$payslipData[$user->id]['payslipItems']->domestic_onsite_local}}</td>
                        <td>{{$payslipData[$user->id]['payslipItems']->domestic_onsite_outstation}}</td>
                        <td>{{$payslipData[$user->id]['payslipItems']->international_onsite}}</td>
                        <td>{{$payslipData[$user->id]['payslipItems']->tech_talk_bonus}}</td>
                        <td>{{$payslipData[$user->id]['payslipItems']->annual_bonus}}</td>
                        <td>{{$payslipData[$user->id]['payslipItems']->confirmation_bonus}}</td>
                        <td>{{$payslipData[$user->id]['payslipItems']->extra_working_day}}</td>
                        <td>{{$payslipData[$user->id]['payslipItems']->national_holiday_working_bonus}}</td>
                        <td>{{count($lastMonthCalendar['summary']['workingDays']) * 8}}</td>
                        <td>{{$timesheetSummary[$user->id]['approved_hrs']}}</td>
                        <td>{{$timesheetSummary[$user->id]['employee_hrs']}}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    @endif
@endsection