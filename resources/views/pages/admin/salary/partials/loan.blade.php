<div class="clearfix">
    <h5 class="pull-left">Loan</h5>
   
</div>
<div class="panel panel-default">
    @foreach( $loanData as $loan)
    <div class="info-table">
        <label>Loan Amount</label>
        <span>
            {{ $loan['loan_amount'] }}
            <span class="status  pull-right">
                @if($loan_status == 'approved')
                <i class="fa fa-check text-success"></i>
                @else
                <i class="fa fa-exclamation text-warning"></i>
                @endif
            </span>
        </span>
    </div>
    <div class="info-table">
        <label>Balance Amount</label>
        <span>{{ $loan['loan_balance'] }}</span>
    </div>
    <div class="info-table">
        <label>Monthly Deduction</label>
        <span>
            {{ $loan['monthly_deduction'] }}
            <span class="status  pull-right">
                @if($loan_status == 'approved')
                <i class="fa fa-check text-success"></i>
                @else
                <i class="fa fa-exclamation text-warning"></i>
                @endif
            </span>
        </span>
    </div>
    @endforeach    
</div>