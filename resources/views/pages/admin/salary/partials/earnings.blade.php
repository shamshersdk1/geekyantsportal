<h5>Earnings</h5>
<div class="panel panel-default">
    
    <div class="info-table">
        <label>Annual Bonus</label>
        <span>
            {{$ctc / 12}}
            <span class="status  pull-right">
                @if($bonus_status == 'approved')
                <i class="fa fa-check text-success"></i>
                @else
                <i class="fa fa-exclamation text-warning"></i>
                @endif
            </span>
        </span>
    </div>
    <div class="info-table">
        <label>Confirmation Bonus</label>
        <span>
            {{$ctc / 12}}
            <span class="status  pull-right">
                @if($bonus_status == 'approved')
                <i class="fa fa-check text-success"></i>
                @else
                <i class="fa fa-exclamation text-warning"></i>
                @endif
            </span>
        </span>
    </div>
    <!-- <div class="info-table">
        <label>Other</label>
        <span>{{$other_bonus_amount}}</span>
    </div> -->
</div>