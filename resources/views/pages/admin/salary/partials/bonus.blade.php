<h5>Extra / Bonuses</h5>
<div class="panel panel-default">
    <div class="info-table">
        <label>Onsite (<small>Previous month</small>)</label>
        <span>2 Days</span>
    </div>
    <div class="info-table">
        <label>Additional Hours (<small>Previous month</small>)</label>
        <span>32 hours</span>
    </div>
     <div class="info-table">
        <label>Holiday Working (<small>Previous month</small>)</label>
        <span>NA</span>
    </div>
    <div class="info-table">
        <label>Referral (<small>Previous month</small>)</label>
        <span>NA</span>
    </div>
    <div class="info-table">
        <label>Tech Talk (<small>Previous month</small>)</label>
        <span>3500/-</span>
    </div>
</div>