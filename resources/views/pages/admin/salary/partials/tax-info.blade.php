<div class="clearfix">
    <h5 class="pull-left">Tax Details</h5>
</div>
<div class="panel panel-default">
    <div class="info-table">
        <label>Prev earning of this this financial year</label>
        <span>{{$prev_earning}}</span>
    </div>
    <div class="info-table">
        <label>IT</label>
        <span>{{$it_saving}}</span>
    </div>
</div>