<h5>CTC</h5>    
<div class="panel panel-default m-b-5">
    <div class="info-table">
        <label>CTC</label>
        <span>{{$userData->activeAppraisal->monthly_var_bonus}}</span>
    </div>
    <div class="info-table">
        <label>Monthly</label>
        <span>{{$ctc / 12}}</span>
    </div>
    <div class="info-table">
        <label>Revised On</label>
        <span>1 Feb 2019</span>
    </div>
</div>
<div class="panel panel-default">
    <div class="info-table">
        <label>Basic</label>
        <span>{{$ctc}}</span>
    </div>
    <div class="info-table">
        <label>House Rent Allowance</label>
        <span>{{$ctc / 12}}</span>
    </div>
    <div class="info-table">
        <label>Car Allowance</label>
        <span>{{$ctc / 12}}</span>
    </div>
    <div class="info-table">
        <label>Food Allowance</label>
        <span>{{$ctc / 12}}</span>
    </div>
    <div class="info-table">
        <label>Leave Travel Allowance</label>
        <span>{{$ctc / 12}}</span>
    </div>
    <div class="info-table">
        <label>Special Allowance</label>
        <span>{{$ctc / 12}}</span>
    </div>
    <div class="info-table">
        <label>Employee’s E.S.I.</label>
        <span>{{$it_saving}}</span>
    </div>
    <div class="info-table">
        <label>Professional Tax</label>
        <span>{{$it_saving}}</span>
    </div>
    <div class="info-table">
        <label>Food Deduction</label>
        <span>{{$it_saving}}</span>
    </div>
    <div class="info-table">
        <label>Medical Insurance</label>
        <span>{{$it_saving}}</span>
    </div>
    <div class="info-table">
        <label>T.D.S.</label>
        <span>{{$it_saving}}</span>
    </div>
    <div class="info-table">
        <label>Other Deduction</label>
        <span>{{$it_saving}}</span>
    </div>
</div>