<div class="clearfix">
    <h5 class="pull-left">Deduction</h5>
  <!--   <div class="status  pull-right m-t-10">
        @if($tax_status == 'approved')
        <i class="fa fa-check text-success"></i>
        @else
        <i class="fa fa-exclamation text-warning"></i>
        @endif
        <span class="capitalize">{{$tax_status}}</span>
    </div> -->
</div>
<div class="panel panel-default">
    <div class="info-table">
        <label>Employee’s P.F.</label>
        <span>{{$prof_tax}}</span>
    </div>
    <div class="info-table">
        <label>Voluntary P.F.</label>
        <span>{{$it_saving}}</span>
    </div>
    <div class="info-table">
        <label>Employee’s E.S.I.</label>
        <span>{{$it_saving}}</span>
    </div>
    <div class="info-table">
        <label>Professional Tax</label>
        <span>{{$it_saving}}</span>
    </div>
    <div class="info-table">
        <label>Food Deduction</label>
        <span>{{$it_saving}}</span>
    </div>
    <div class="info-table">
        <label>Medical Insurance</label>
        <span>{{$it_saving}}</span>
    </div>
    <div class="info-table">
        <label>T.D.S.</label>
        <span>{{$it_saving}}</span>
    </div>
    <div class="info-table">
        <label>Other Deduction</label>
        <span>{{$it_saving}}</span>
    </div>
</div>