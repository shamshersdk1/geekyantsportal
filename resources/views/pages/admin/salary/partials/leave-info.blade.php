<div class="clearfix">
    <h5 class="pull-left">Leave Encashment / Deduction</h5>
</div>
<div class="panel panel-default">
    @if($encash_pl)
    <div class="info-table">
        <label>Encashed PL</label>
        <span>
            [{{$encash_pl}}] - {{$encash_pl * 2000}}
            <span class="status  pull-right">
                @if($leave_status == 'approved')
                <i class="fa fa-check text-success"></i>
                @else
                <i class="fa fa-exclamation text-warning"></i>
                @endif
            </span>
        </span>
    </div>
    @endif
    @if($additional_pl)
    <div class="info-table">
        <label>Additional</label>
        <span>
            [{{$additional_pl}}] - {{$additional_pl * 2000}}
            <span class="status  pull-right">
                @if($leave_status == 'approved')
                <i class="fa fa-check text-success"></i>
                @else
                <i class="fa fa-exclamation text-warning"></i>
                @endif
            </span>
        </span>
    </div>
    @endif
    @if($lop)
    <div class="info-table">
        <label>Loss of Pay</label>
        <span>
            [{{$lop}}] - {{$lop * 2000}}
            <span class="status  pull-right">
                @if($leave_status == 'approved')
                <i class="fa fa-check text-success"></i>
                @else
                <i class="fa fa-exclamation text-warning"></i>
                @endif
            </span>
        </span>
    </div>
    @endif
</div>