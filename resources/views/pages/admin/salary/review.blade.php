@extends('layouts.admin-dashboard')
@section('main-content')
    @if(!$success)
        <div class="container-fluid">
            Invalid User!
        </div>
    @else
        <div class="container-fluid">
            <div class="breadcrumb-wrap">
                <div class="row">
                    <div class="col-sm-8">
                        <h1 class="admin-page-title">Salary Review for <strong>{{$user->name}}</strong> ({{$user->employee_id}}) for the Month of <strong>{{$month_string}}</strong></h1>
                        <ol class="breadcrumb">
                            <li><a href="/admin">Admin</a></li>
                            <li class="active">Salary Review</li>
                        </ol>
                    </div>
                    <div class="col-sm-4 text-right">
                        <label class="no-margin">Working days</label>
                        <div><p class="lead no-margin">22 / 160hrs</p></div>
                    </div>
                </div>
            </div>
            <h4>{{$user->name}}</h4>
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-3">
                            <div>
                                <label class="no-margin">Employee ID</label>
                                <div><p class="lead">{{$user->employee_id}}</p></div>
                            </div>
                            <div>
                                <label class="no-margin">Joining Date</label>
                                <div><p class="lead">12 Oct 2015</p></div>
                            </div>
                            <div>
                                <label class="no-margin">Conformation Date</label>
                                <div><p class="lead no-margin">12 Dec 2015</p></div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div>
                                <label class="no-margin">Salary</label>
                                <div><p class="lead">4.5L</p></div>
                            </div>
                            <div>
                                <label class="no-margin">Monthly Inhand</label>
                                <div><p class="lead">25000</p></div>
                            </div>
                            <div>
                                <label class="no-margin">Monthly Variable</label>
                                <div><p class="lead no-margin">5000</p></div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <label class="no-margin">Leave</label>
                            <div class="m-t-10">
                                <div><p class="lead no-margin">Sick Leave: 2</p></div>
                                <div class="m-t-5">22, 23</div>
                            </div>
                            <div class="m-t-10">
                                <div><p class="lead no-margin">Paid Leave: 2</p></div>
                                <div class="m-t-5">22, 23</div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div>
                                <label class="no-margin">Filled Timelog</label>
                                <div><p class="lead">120</p></div>
                            </div>
                            <div>
                                <label class="no-margin">Variable Pay</label>
                                <div><label class="label label-success label-lg m-b">3000</label></div>
                            </div>
                            <div>
                                <label class="no-margin">Timesheet pay</label>
                                <div><label class="label label-success label-lg">2000</label></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <form>
                    <div class="row">
                        <div class="col-sm-3">
                            <h4>Review and update</h4>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <label>Variable Pay</label>
                                    <input type="text" name="" class="form-control" value="3000" />
                                </li>
                                <li class="list-group-item">
                                    <label>Timesheet</label>
                                    <input type="text" name="" class="form-control" value="2000" />
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-3">
                            <h4>Outsource</h4>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <label>Domestic Local</label>
                                    <input type="text" name="" class="form-control" value="4" />
                                </li>
                                <li class="list-group-item">
                                    <label>Domestic Outstation</label>
                                    <input type="text" name="" class="form-control" placeholder="Number of days" />
                                </li>
                                <li class="list-group-item">
                                    <label>Internation Onsite</label>
                                    <input type="text" name="" class="form-control" placeholder="Number of days" />
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-3">
                            <h4>Additional</h4>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <label>Extra Hours</label>
                                    <input type="text" name="" class="form-control" value="24" />
                                </li>
                                <li class="list-group-item">
                                    <label>Extra Working Day</label>
                                    <input type="text" name="" class="form-control" placeholder="Number of days" />
                                </li>
                                <li class="list-group-item">
                                    <label>National Holiday</label>
                                    <input type="text" name="" class="form-control"  />
                                </li>
                                <li class="list-group-item">
                                    <label>Reimbursement</label>
                                    <input type="text" name="" class="form-control" value="2000" />
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-3">
                            <h4>Additional</h4>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <label>Bonus</label>
                                    <input type="text" name="" class="form-control" value="2000" />
                                </li>
                                <li class="list-group-item">
                                    <label>Annual Bonus</label>
                                    <input type="text" name="" class="form-control" value="2000" />
                                </li>
                                <li class="list-group-item">
                                    <label>Confirmation Bonus</label>
                                    <input type="text" name="" class="form-control" value="2000" />
                                </li>
                               
                                <li class="list-group-item">
                                    <label>Tech Talk Bonus</label>
                                    <input type="text" name="" class="form-control" value="2000" />
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="text-right m-b">
                        <input type="submit" name="" value="Submit" class="btn btn-success btn-lg" />
                    </div>
                </form>
            </div>
            <div>
                {{dump($user)}}
                {{dump($output)}}
            </div>
        </div>
    @endif
@endsection