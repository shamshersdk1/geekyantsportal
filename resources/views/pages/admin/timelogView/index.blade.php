@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid timelog">
   <div class="breadcrumb-wrap">
      <div class="row">
         <div class="col-sm-8">
            <h1 class="admin-page-title"></h1>
            <ol class="breadcrumb">
               <li><a>Admin</a></li>
               <li><a>Timelog View</a></li>
            </ol>
         </div>
      </div>
   </div>
   <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#project1">Project 1</a></li>
      <li><a data-toggle="tab" href="#project2">Project 2</a></li>
      <li><a data-toggle="tab" href="#project3">Project 3</a></li>
      <li><a data-toggle="tab" href="#project4">Project 4</a></li>
   </ul>
   <div class="panel panel-default">
      <div class="panel-body">
         <div class="tab-content">
            <div id="project1" class="tab-pane fade in active">
               <div class="row">
                  <div class="col-sm-12">
                     <h4>Megha</h4>
                  </div>
                  <div class="col-sm-6" >
                     <div class="panel panel-default col-sm-12">
                        <table class="table">
                           <thead>
                              <th width="90">Hours</th>
                              <th>Description</th>
                           </thead>
                           <tbody>
                              <tr>
                                  <td>
                                      <input class="form-control" value="4"  type="number"/>
                                   </td>
                                   <td>
                                      <textarea class="form-control">Sed ut perspiciatis unde omnis iste natus error sit</textarea>
                                   </td>
                              
                              </tr>
                              <tr>
                                  <td>
                                      <input class="form-control" value="4"  type="number"/>
                                   </td>
                                   <td>
                                      <textarea class="form-control">Sed ut perspiciatis unde omnis iste natus error sit</textarea>
                                   </td>
                              </tr>
                              <tr>
                                  <td>
                                      <input class="form-control" value="4"  type="number"/>
                                   </td>
                                   <td>
                                      <textarea class="form-control">Sed ut perspiciatis unde omnis iste natus error sit</textarea>
                                   </td>
                              </tr>
                           </tbody>
                        </table>
                        <div class="expection col-sm-12">
                           <div class="col-sm-12">
                              <label>Expectation</label>
                           </div>
                           <div class="col-sm-12">
                              <input class="form-control" value="4" />
                              <div class="checkbox checkbox-default">
                                 <input id="checkbox2" type="checkbox" checked="">
                                 <label for="checkbox2">
                                 Sed ut perspiciatis unde omnis iste natus error sit
                                 </label>
                              </div>
                           </div>
                        </div>
                        <div class="panel-footer col-sm-12">
                           <button class="btn btn-danger btn-sm pull-right" type="button" > Reject</button>
                           <button class="btn btn-success btn-sm pull-right" type="button" style="margin-right:10px;" > Approve</button>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="panel panel-default col-sm-12">
                        <table class="table">
                           <thead>
                              <th width="90">Hours</th>
                              <th>Description</th>
                           </thead>
                           <tbody>
                              <tr>
                                  <td>
                                      <input class="form-control" value="4"  type="number"/>
                                   </td>
                                   <td>
                                      <textarea class="form-control">Sed ut perspiciatis unde omnis iste natus error sit</textarea>
                                   </td>
                              </tr>
                              <tr>
                                  <td>
                                      <input class="form-control" value="4"  type="number"/>
                                   </td>
                                   <td>
                                      <textarea class="form-control">Sed ut perspiciatis unde omnis iste natus error sit</textarea>
                                   </td>
                              </tr>
                              <tr>
                                  <td>
                                      <input class="form-control" value="4"  type="number"/>
                                   </td>
                                   <td>
                                      <textarea class="form-control">Sed ut perspiciatis unde omnis iste natus error sit</textarea>
                                   </td>
                              </tr>
                           </tbody>
                        </table>
                        <div class="expection col-sm-12">
                           <div class="col-sm-12">
                              <label>Expectation</label>
                           </div>
                           <div class="col-sm-12">
                              <input class="form-control" value="4" />
                              <div class="checkbox checkbox-default">
                                 <input id="checkbox2" type="checkbox" checked="">
                                 <label for="checkbox2">
                                 Sed ut perspiciatis unde omnis iste natus error sit
                                 </label>
                              </div>
                           </div>
                        </div>
                        <div class="panel-footer col-sm-12">
                           <button class="btn btn-danger btn-sm pull-right" type="button" > Reject</button>
                           <button class="btn btn-success btn-sm pull-right" type="button" style="margin-right:10px;" > Approve</button>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="panel panel-default col-sm-12">
                        <table class="table">
                           <thead>
                              <th width="90">Hours</th>
                              <th>Description</th>
                           </thead>
                           <tbody>
                              <tr>
                                  <td>
                                      <input class="form-control" value="4"  type="number"/>
                                   </td>
                                   <td>
                                      <textarea class="form-control">Sed ut perspiciatis unde omnis iste natus error sit</textarea>
                                   </td>
                              </tr>
                              <tr>
                                  <td>
                                      <input class="form-control" value="4"  type="number"/>
                                   </td>
                                   <td>
                                      <textarea class="form-control">Sed ut perspiciatis unde omnis iste natus error sit</textarea>
                                   </td>
                              </tr>
                              <tr>
                                  <td>
                                      <input class="form-control" value="4"  type="number"/>
                                   </td>
                                   <td>
                                      <textarea class="form-control">Sed ut perspiciatis unde omnis iste natus error sit</textarea>
                                   </td>
                              </tr>
                           </tbody>
                        </table>
                        <div class="expection col-sm-12">
                           <div class="col-sm-12">
                              <label>Expectation</label>
                           </div>
                           <div class="col-sm-12">
                              <input class="form-control" value="4" />
                              <div class="checkbox checkbox-default">
                                 <input id="checkbox2" type="checkbox" checked="">
                                 <label for="checkbox2">
                                 Sed ut perspiciatis unde omnis iste natus error sit
                                 </label>
                              </div>
                           </div>
                        </div>
                        <div class="panel-footer col-sm-12">
                           <button class="btn btn-danger btn-sm pull-right" type="button" > Reject</button>
                           <button class="btn btn-success btn-sm pull-right" type="button" style="margin-right:10px;" > Approve</button>
                        </div>
                     </div>
                  </div>
               </div>
               <!--User2-->
               <div class="row">
                  <div class="col-sm-12">
                     <h4>varun</h4>
                  </div>
                  <div class="col-sm-6" >
                     <div class="panel panel-default col-sm-12">
                        <table class="table">
                           <thead>
                              <th width="90">Hours</th>
                              <th>Description</th>
                           </thead>
                           <tbody>
                              <tr>
                                  <td>
                                      <input class="form-control" value="4"  type="number"/>
                                   </td>
                                   <td>
                                      <textarea class="form-control">Sed ut perspiciatis unde omnis iste natus error sit</textarea>
                                   </td>
                              </tr>
                              <tr>
                                  <td>
                                      <input class="form-control" value="4"  type="number"/>
                                   </td>
                                   <td>
                                      <textarea class="form-control">Sed ut perspiciatis unde omnis iste natus error sit</textarea>
                                   </td>
                                </tr>
                              </tr>
                              <tr>
                                  <td>
                                      <input class="form-control" value="4"  type="number"/>
                                   </td>
                                   <td>
                                      <textarea class="form-control">Sed ut perspiciatis unde omnis iste natus error sit</textarea>
                                   </td>
                              </tr>
                           </tbody>
                        </table>
                        <div class="expection col-sm-12">
                           <div class="col-sm-12">
                              <label>Expectation</label>
                           </div>
                           <div class="col-sm-12">
                              <input class="form-control" value="4" />
                              <div class="checkbox checkbox-default">
                                 <input id="checkbox2" type="checkbox" checked="">
                                 <label for="checkbox2">
                                 Sed ut perspiciatis unde omnis iste natus error sit
                                 </label>
                              </div>
                           </div>
                        </div>
                        <div class="panel-footer col-sm-12">
                           <button class="btn btn-danger btn-sm pull-right" type="button" > Reject</button>
                           <button class="btn btn-success btn-sm pull-right" type="button" style="margin-right:10px;" > Approve</button>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="panel panel-default col-sm-12">
                        <table class="table">
                           <thead>
                              <th width="90">Hours</th>
                              <th>Description</th>
                           </thead>
                           <tbody>
                              <tr>
                                  <td>
                                      <input class="form-control" value="4"  type="number"/>
                                   </td>
                                   <td>
                                      <textarea class="form-control">Sed ut perspiciatis unde omnis iste natus error sit</textarea>
                                   </td>
                              </tr>
                              <tr>
                                  <td>
                                      <input class="form-control" value="4"  type="number"/>
                                   </td>
                                   <td>
                                      <textarea class="form-control">Sed ut perspiciatis unde omnis iste natus error sit</textarea>
                                   </td>
                                </tr>
                              </tr>
                              <tr>
                                  <td>
                                      <input class="form-control" value="4"  type="number"/>
                                   </td>
                                   <td>
                                      <textarea class="form-control">Sed ut perspiciatis unde omnis iste natus error sit</textarea>
                                   </td>
                              </tr>
                           </tbody>
                        </table>
                        <div class="expection col-sm-12">
                           <div class="col-sm-12">
                              <label>Expectation</label>
                           </div>
                           <div class="col-sm-12">
                              <input class="form-control" value="4" />
                              <div class="checkbox checkbox-default">
                                 <input id="checkbox2" type="checkbox" checked="">
                                 <label for="checkbox2">
                                 Sed ut perspiciatis unde omnis iste natus error sit
                                 </label>
                              </div>
                           </div>
                        </div>
                        <div class="panel-footer col-sm-12">
                           <button class="btn btn-danger btn-sm pull-right" type="button" > Reject</button>
                           <button class="btn btn-success btn-sm pull-right" type="button" style="margin-right:10px;" > Approve</button>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="panel panel-default col-sm-12">
                        <table class="table">
                           <thead>
                              <th width="90">Hours</th>
                              <th>Description</th>
                           </thead>
                           <tbody>
                              <tr>
                                  <td>
                                      <input class="form-control" value="4"  type="number"/>
                                   </td>
                                   <td>
                                      <textarea class="form-control">Sed ut perspiciatis unde omnis iste natus error sit</textarea>
                                   </td>
                                </tr>
                              </tr>
                              <tr>
                                  <td>
                                      <input class="form-control" value="4"  type="number"/>
                                   </td>
                                   <td>
                                      <textarea class="form-control">Sed ut perspiciatis unde omnis iste natus error sit</textarea>
                                   </td>
                                </tr>
                              </tr>
                              <tr>
                                  <td>
                                      <input class="form-control" value="4"  type="number"/>
                                   </td>
                                   <td>
                                      <textarea class="form-control">Sed ut perspiciatis unde omnis iste natus error sit</textarea>
                                   </td>
                              </tr>
                           </tbody>
                        </table>
                        <div class="expection col-sm-12">
                           <div class="col-sm-12">
                              <label>Expectation</label>
                           </div>
                           <div class="col-sm-12">
                              <input class="form-control" value="4" />
                              <div class="checkbox checkbox-default">
                                 <input id="checkbox2" type="checkbox" checked="">
                                 <label for="checkbox2">
                                 Sed ut perspiciatis unde omnis iste natus error sit
                                 </label>
                              </div>
                           </div>
                        </div>
                        <div class="panel-footer col-sm-12">
                           <button class="btn btn-danger btn-sm pull-right" type="button" > Reject</button>
                           <button class="btn btn-success btn-sm pull-right" type="button" style="margin-right:10px;" > Approve</button>
                        </div>
                     </div>
                  </div>
               </div>
               <!--End User2-->
            </div>
            <!--Project2-->
            <div id="project2" class="tab-pane fade">
               <div class="row">
                  <div class="col-sm-12">
                     <h4>Megha</h4>
                  </div>
                  <div class="col-sm-6" >
                     <div class="panel panel-default col-sm-12">
                        <table class="table">
                           <thead>
                              <th width="90">Hours</th>
                              <th>Description</th>
                           </thead>
                           <tbody>
                              <tr>
                                <td>
                                        <input class="form-control" value="4"  type="number"/>
                                      </td>
                                      <td>
                                        <textarea class="form-control">Sed ut perspiciatis unde omnis iste natus error sit</textarea>
                                      </td>
                                  </tr>
                              </tr>
                              <tr>
                                  <td>
                                      <input class="form-control" value="4"  type="number"/>
                                   </td>
                                   <td>
                                      <textarea class="form-control">Sed ut perspiciatis unde omnis iste natus error sit</textarea>
                                   </td>
                                </tr>
                              </tr>
                              <tr>
                                  <td>
                                      <input class="form-control" value="4"  type="number"/>
                                   </td>
                                   <td>
                                      <textarea class="form-control">Sed ut perspiciatis unde omnis iste natus error sit</textarea>
                                   </td>
                                </tr>
                              </tr>
                           </tbody>
                        </table>
                        <div class="expection col-sm-12">
                           <div class="col-sm-12">
                              <label>Expectation</label>
                           </div>
                           <div class="col-sm-12">
                              <input class="form-control" value="4" />
                              <div class="checkbox checkbox-default">
                                 <input id="checkbox2" type="checkbox" checked="">
                                 <label for="checkbox2">
                                 Sed ut perspiciatis unde omnis iste natus error sit
                                 </label>
                              </div>
                           </div>
                        </div>
                        <div class="panel-footer col-sm-12">
                           <button class="btn btn-danger btn-sm pull-right" type="button" > Reject</button>
                           <button class="btn btn-success btn-sm pull-right" type="button" style="margin-right:10px;" > Approve</button>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="panel panel-default col-sm-12">
                        <table class="table">
                           <thead>
                              <th width="90">Hours</th>
                              <th>Description</th>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                    <input class="form-control" value="4" />
                                 </td>
                                 <td>
                                    <textarea class="form-control"></textarea>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <input class="form-control" value="4" />
                                 </td>
                                 <td>
                                    <textarea class="form-control"></textarea>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <input class="form-control" value="4" />
                                 </td>
                                 <td>
                                    <textarea class="form-control"></textarea>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                        <div class="expection col-sm-12">
                           <div class="col-sm-12">
                              <label>Expectation</label>
                           </div>
                           <div class="col-sm-12">
                              <input class="form-control" value="4" />
                              <div class="checkbox checkbox-default">
                                 <input id="checkbox2" type="checkbox" checked="">
                                 <label for="checkbox2">
                                 Sed ut perspiciatis unde omnis iste natus error sit
                                 </label>
                              </div>
                           </div>
                        </div>
                        <div class="panel-footer col-sm-12">
                           <button class="btn btn-danger btn-sm pull-right" type="button" > Reject</button>
                           <button class="btn btn-success btn-sm pull-right" type="button" style="margin-right:10px;" > Approve</button>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="panel panel-default col-sm-12">
                        <table class="table">
                           <thead>
                              <th width="90">Hours</th>
                              <th>Description</th>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                    <input class="form-control" value="4" />
                                 </td>
                                 <td>
                                    <textarea class="form-control"></textarea>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <input class="form-control" value="4" />
                                 </td>
                                 <td>
                                    <textarea class="form-control"></textarea>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <input class="form-control" value="4" />
                                 </td>
                                 <td>
                                    <textarea class="form-control"></textarea>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                        <div class="expection col-sm-12">
                           <div class="col-sm-12">
                              <label>Expectation</label>
                           </div>
                           <div class="col-sm-12">
                              <input class="form-control" value="4" />
                              <div class="checkbox checkbox-default">
                                 <input id="checkbox2" type="checkbox" checked="">
                                 <label for="checkbox2">
                                 Sed ut perspiciatis unde omnis iste natus error sit
                                 </label>
                              </div>
                           </div>
                        </div>
                        <div class="panel-footer col-sm-12">
                           <button class="btn btn-danger btn-sm pull-right" type="button" > Reject</button>
                           <button class="btn btn-success btn-sm pull-right" type="button" style="margin-right:10px;" > Approve</button>
                        </div>
                     </div>
                  </div>
               </div>
               <!--User2-->
               <div class="row">
                  <div class="col-sm-12">
                     <h4>varun</h4>
                  </div>
                  <div class="col-sm-6" >
                     <div class="panel panel-default col-sm-12">
                        <table class="table">
                           <thead>
                              <th width="90">Hours</th>
                              <th>Description</th>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                    <input class="form-control" value="4" />
                                 </td>
                                 <td>
                                    <textarea class="form-control"></textarea>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <input class="form-control" value="4" />
                                 </td>
                                 <td>
                                    <textarea class="form-control"></textarea>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <input class="form-control" value="4" />
                                 </td>
                                 <td>
                                    <textarea class="form-control"></textarea>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                        <div class="expection col-sm-12">
                           <div class="col-sm-12">
                              <label>Expectation</label>
                           </div>
                           <div class="col-sm-12">
                              <input class="form-control" value="4" />
                              <div class="checkbox checkbox-default">
                                 <input id="checkbox2" type="checkbox" checked="">
                                 <label for="checkbox2">
                                 Sed ut perspiciatis unde omnis iste natus error sit
                                 </label>
                              </div>
                           </div>
                        </div>
                        <div class="panel-footer col-sm-12">
                           <button class="btn btn-danger btn-sm pull-right" type="button" > Reject</button>
                           <button class="btn btn-success btn-sm pull-right" type="button" style="margin-right:10px;" > Approve</button>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="panel panel-default col-sm-12">
                        <table class="table">
                           <thead>
                              <th width="90">Hours</th>
                              <th>Description</th>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                    <input class="form-control" value="4" />
                                 </td>
                                 <td>
                                    <textarea class="form-control"></textarea>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <input class="form-control" value="4" />
                                 </td>
                                 <td>
                                    <textarea class="form-control"></textarea>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <input class="form-control" value="4" />
                                 </td>
                                 <td>
                                    <textarea class="form-control"></textarea>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                        <div class="expection col-sm-12">
                           <div class="col-sm-12">
                              <label>Expectation</label>
                           </div>
                           <div class="col-sm-12">
                              <input class="form-control" value="4" />
                              <div class="checkbox checkbox-default">
                                 <input id="checkbox2" type="checkbox" checked="">
                                 <label for="checkbox2">
                                 Sed ut perspiciatis unde omnis iste natus error sit
                                 </label>
                              </div>
                           </div>
                        </div>
                        <div class="panel-footer col-sm-12">
                           <button class="btn btn-danger btn-sm pull-right" type="button" > Reject</button>
                           <button class="btn btn-success btn-sm pull-right" type="button" style="margin-right:10px;" > Approve</button>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="panel panel-default col-sm-12">
                        <table class="table">
                           <thead>
                              <th width="90">Hours</th>
                              <th>Description</th>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                    <input class="form-control" value="4" />
                                 </td>
                                 <td>
                                    <textarea class="form-control"></textarea>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <input class="form-control" value="4" />
                                 </td>
                                 <td>
                                    <textarea class="form-control"></textarea>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <input class="form-control" value="4" />
                                 </td>
                                 <td>
                                    <textarea class="form-control"></textarea>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                        <div class="expection col-sm-12">
                           <div class="col-sm-12">
                              <label>Expectation</label>
                           </div>
                           <div class="col-sm-12">
                              <input class="form-control" value="4" />
                              <div class="checkbox checkbox-default">
                                 <input id="checkbox2" type="checkbox" checked="">
                                 <label for="checkbox2">
                                 Sed ut perspiciatis unde omnis iste natus error sit
                                 </label>
                              </div>
                           </div>
                        </div>
                        <div class="panel-footer col-sm-12">
                           <button class="btn btn-danger btn-sm pull-right" type="button" > Reject</button>
                           <button class="btn btn-success btn-sm pull-right" type="button" style="margin-right:10px;" > Approve</button>
                        </div>
                     </div>
                  </div>
               </div>
               <!--End User2-->
            </div>
            <!--End of Project2-->
            <!--Project3-->
            <div id="project3" class="tab-pane fade">
               <div class="row">
                  <div class="col-sm-12">
                     <h4>Megha</h4>
                  </div>
                  <div class="col-sm-6" >
                     <div class="panel panel-default col-sm-12">
                        <table class="table">
                           <thead>
                              <th width="90">Hours</th>
                              <th>Description</th>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                    <input class="form-control" value="4"  type="number"/>
                                 </td>
                                 <td>
                                    <textarea class="form-control">Sed ut perspiciatis unde omnis iste natus error sit</textarea>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <input class="form-control" value="4"  type="number"/>
                                 </td>
                                 <td>
                                    <textarea class="form-control">Sed ut perspiciatis unde omnis iste natus error sit Sed ut perspiciatis unde omnis iste natus error sit</textarea>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <input class="form-control" value="4"  type="number"/>
                                 </td>
                                 <td>
                                    <textarea class="form-control" >Sed ut perspiciatis unde omnis iste natus error sit</textarea>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                        <div class="expection col-sm-12">
                           <div class="col-sm-12">
                              <label>Expectation</label>
                           </div>
                           <div class="col-sm-12">
                              <input class="form-control" value="4" />
                              <div class="checkbox checkbox-default">
                                 <input id="checkbox2" type="checkbox" checked="">
                                 <label for="checkbox2">
                                 Sed ut perspiciatis unde omnis iste natus error sit
                                 </label>
                              </div>
                           </div>
                        </div>
                        <div class="panel-footer col-sm-12">
                           <button class="btn btn-danger btn-sm pull-right" type="button" > Reject</button>
                           <button class="btn btn-success btn-sm pull-right" type="button" style="margin-right:10px;" > Approve</button>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="panel panel-default col-sm-12">
                        <table class="table">
                           <thead>
                              <th width="90">Hours</th>
                              <th>Description</th>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                    <input class="form-control" value="4" />
                                 </td>
                                 <td>
                                    <textarea class="form-control"></textarea>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <input class="form-control" value="4" />
                                 </td>
                                 <td>
                                    <textarea class="form-control"></textarea>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <input class="form-control" value="4" />
                                 </td>
                                 <td>
                                    <textarea class="form-control"></textarea>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                        <div class="expection col-sm-12">
                           <div class="col-sm-12">
                              <label>Expectation</label>
                           </div>
                           <div class="col-sm-12">
                              <input class="form-control" value="4" />
                              <div class="checkbox checkbox-default">
                                 <input id="checkbox2" type="checkbox" checked="">
                                 <label for="checkbox2">
                                 Sed ut perspiciatis unde omnis iste natus error sit
                                 </label>
                              </div>
                           </div>
                        </div>
                        <div class="panel-footer col-sm-12">
                           <button class="btn btn-danger btn-sm pull-right" type="button" > Reject</button>
                           <button class="btn btn-success btn-sm pull-right" type="button" style="margin-right:10px;" > Approve</button>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="panel panel-default col-sm-12">
                        <table class="table">
                           <thead>
                              <th width="90">Hours</th>
                              <th>Description</th>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                    <input class="form-control" value="4" />
                                 </td>
                                 <td>
                                    <textarea class="form-control"></textarea>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <input class="form-control" value="4" />
                                 </td>
                                 <td>
                                    <textarea class="form-control"></textarea>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <input class="form-control" value="4" />
                                 </td>
                                 <td>
                                    <textarea class="form-control"></textarea>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                        <div class="expection col-sm-12">
                           <div class="col-sm-12">
                              <label>Expectation</label>
                           </div>
                           <div class="col-sm-12">
                              <input class="form-control" value="4" />
                              <div class="checkbox checkbox-default">
                                 <input id="checkbox2" type="checkbox" checked="">
                                 <label for="checkbox2">
                                 Sed ut perspiciatis unde omnis iste natus error sit
                                 </label>
                              </div>
                           </div>
                        </div>
                        <div class="panel-footer col-sm-12">
                           <button class="btn btn-danger btn-sm pull-right" type="button" > Reject</button>
                           <button class="btn btn-success btn-sm pull-right" type="button" style="margin-right:10px;" > Approve</button>
                        </div>
                     </div>
                  </div>
               </div>
               <!--User2-->
               <div class="row">
                  <div class="col-sm-12">
                     <h4>varun</h4>
                  </div>
                  <div class="col-sm-6" >
                     <div class="panel panel-default col-sm-12">
                        <table class="table">
                           <thead>
                              <th width="90">Hours</th>
                              <th>Description</th>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                    <input class="form-control" value="4" />
                                 </td>
                                 <td>
                                    <textarea class="form-control"></textarea>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <input class="form-control" value="4" />
                                 </td>
                                 <td>
                                    <textarea class="form-control"></textarea>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <input class="form-control" value="4" />
                                 </td>
                                 <td>
                                    <textarea class="form-control"></textarea>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                        <div class="expection col-sm-12">
                           <div class="col-sm-12">
                              <label>Expectation</label>
                           </div>
                           <div class="col-sm-12">
                              <input class="form-control" value="4" />
                              <div class="checkbox checkbox-default">
                                 <input id="checkbox2" type="checkbox" checked="">
                                 <label for="checkbox2">
                                 Sed ut perspiciatis unde omnis iste natus error sit
                                 </label>
                              </div>
                           </div>
                        </div>
                        <div class="panel-footer col-sm-12">
                           <button class="btn btn-danger btn-sm pull-right" type="button" > Reject</button>
                           <button class="btn btn-success btn-sm pull-right" type="button" style="margin-right:10px;" > Approve</button>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="panel panel-default col-sm-12">
                        <table class="table">
                           <thead>
                              <th width="90">Hours</th>
                              <th>Description</th>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                    <input class="form-control" value="4" />
                                 </td>
                                 <td>
                                    <textarea class="form-control"></textarea>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <input class="form-control" value="4" />
                                 </td>
                                 <td>
                                    <textarea class="form-control"></textarea>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <input class="form-control" value="4" />
                                 </td>
                                 <td>
                                    <textarea class="form-control"></textarea>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                        <div class="expection col-sm-12">
                           <div class="col-sm-12">
                              <label>Expectation</label>
                           </div>
                           <div class="col-sm-12">
                              <input class="form-control" value="4" />
                              <div class="checkbox checkbox-default">
                                 <input id="checkbox2" type="checkbox" checked="">
                                 <label for="checkbox2">
                                 Sed ut perspiciatis unde omnis iste natus error sit
                                 </label>
                              </div>
                           </div>
                        </div>
                        <div class="panel-footer col-sm-12">
                           <button class="btn btn-danger btn-sm pull-right" type="button" > Reject</button>
                           <button class="btn btn-success btn-sm pull-right" type="button" style="margin-right:10px;" > Approve</button>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="panel panel-default col-sm-12">
                        <table class="table">
                           <thead>
                              <th width="90">Hours</th>
                              <th>Description</th>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                    <input class="form-control" value="4" />
                                 </td>
                                 <td>
                                    <textarea class="form-control"></textarea>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <input class="form-control" value="4" />
                                 </td>
                                 <td>
                                    <textarea class="form-control"></textarea>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <input class="form-control" value="4" />
                                 </td>
                                 <td>
                                    <textarea class="form-control"></textarea>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                        <div class="expection col-sm-12">
                           <div class="col-sm-12">
                              <label>Expectation</label>
                           </div>
                           <div class="col-sm-12">
                              <input class="form-control" value="4" />
                              <div class="checkbox checkbox-default">
                                 <input id="checkbox2" type="checkbox" checked="">
                                 <label for="checkbox2">
                                 Sed ut perspiciatis unde omnis iste natus error sit
                                 </label>
                              </div>
                           </div>
                        </div>
                        <div class="panel-footer col-sm-12">
                           <button class="btn btn-danger btn-sm pull-right" type="button" > Reject</button>
                           <button class="btn btn-success btn-sm pull-right" type="button" style="margin-right:10px;" > Approve</button>
                        </div>
                     </div>
                  </div>
               </div>
               <!--End Project3-->
            </div>
            <div id="project4" class="tab-pane fade">
               <h3>Menu 3</h3>
               <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
@endsection