@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Add User</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
		  			<li><a href="{{ url('admin/events') }}">Events</a></li>
        		</ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
        </div>
    </div>
        <div class="row">
        <div class="col-md-12">
    	@if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span><br/>
                  @endforeach
            </div>
        @endif
        @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
        @endif   
		<form name = "myForm" method="POST" action="/admin/user-events" enctype="multipart/form-data" >
            <div class="panel panel-default" style="padding-top: 15px;">
                <div class="row">
                    <div class="col-md-12" style="padding:2% 4% 0% 4%">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" id="attno" name="attcount" value="1">
                            <div class="form-group col-md-6">
                                <label class="col-md-3" style="padding-top:2%" align="left" for="name">User:</label>
                                <div class="col-md-9" style="padding:0">
                                    <input type="text" id="name" name="name" class="form-control" placeholder="Name" required value="{{ old('name') }}">
                                </div>
                            </div>
                        <div class="form-group col-md-6">
                            <label class="col-md-3" style="padding-top:2%" align="left" for="event">Event:</label>
                            <div class="col-md-9 col-sm-12 col-xs-12">
                            <select class="form-control col-md-12 col-sm-12 col-xs-12" style="border-radius:0px;-webkit-appearance: none" name="event" id="event">
                                <option value="">---Select Event---</option>
                                @foreach($events as $event)
                                    <option value="{{$event->id}}">{{$event->name}}</option>
                                @endforeach
                            </select>
                            <span class="glyphicon glyphicon-triangle-bottom form-control-feedback" style="opacity:0.3;left:88%" ></span>
                        </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="col-md-3" style="padding-top:2%" align="left" for="date">Date:</label>
                            <div class="col-md-9 col-sm-12 col-xs-12 input-group date">
                                <input name="date" type="text" placeholder="Select Date" class="form-control" id="date" value="{{ old('date') }}" required>
                                <!-- <span class="input-group-addon"> -->
                                    <span class="glyphicon glyphicon-calendar form-control-feedback" style="opacity:0.3" ></span>
                                <!-- </span> -->
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="col-md-3" style="padding-top:2%" align="left" for="file">Attachments:</label>
                            <div style="float:right" class="col-md-9">
                            <div class="col-md-8" style="padding:0">
                                <input type="file" class="form-control" name="attachment1">
                                <input type="hidden" id="dummy">
                            </div>
                                <button type="button" id="btAdd" style="opacity:0.7;border-radius:0;height:2.8em;float:right" class="btn col-md-4"><i class="fa fa-plus btn-icon-space" style="color:#4F4F4F" aria-hidden="true"></i>Add more</button>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="col-md-3" style="padding-top:2%" align="left" for="admin">Approved By:</label>
                            <div class="col-md-9" style="padding:0">
                                <input type="text" id="approved_by" name="approved_by" class="form-control" placeholder="Admin Name" required value="{{ old('approved_by') }}">
                                <!-- <label style="padding-top:3%" class="form-group" for="uploaded_by">Uploaded By: {{Auth::user()->name}}</label> -->
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="col-md-3" style="padding-top:2%" align="left" for="name">Note:</label>
                            <div class="col-md-9">
                                <textarea name="note" id="note" rows="3" class="form-control" placeholder="Notes..." ></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <button type="reset" class="btn btn-default"><i class="fa fa-rotate-left fa-fw"></i> Clear</button> 
                <button type="submit" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Save</button>
            </div>
		</form>
	</div>
			
	</div>
</div>
<script>
    $(function () { 
        if({{$eventno}}>0)
        {
            $('#event option[value="{{$eventno}}"]').attr("selected",true);
        }
        else
        {
            $('#event option[value="{{old('event')}}"]').attr("selected",true);
        }
        var count=1;
        $('#btAdd').click(function() {
            if (count < 5) {
                count = count + 1;
                $('#dummy').before('<input type="file" class="form-control" name="attachment'+count+'">');
                $("#attno").val(count);
            }});
        $("#note").text("{{old('note')}}");
        $( "#date" ).datepicker({ dateFormat: 'yy-mm-dd'}); 
        $( "#name" ).autocomplete({
            source: {{$jsonuser}}
        });
        $( "#approved_by" ).autocomplete({
            source: {{$jsonadmin}}
        });
    });
</script>
@endsection