@extends('layouts.admin-dashboard')
@section('main-content')
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">User Event View</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
				  		<li><a href="{{ url('admin/events') }}">Events</a></li>
				  		<li class="active">User Event</li>
		        	</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
        </div>
        @if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span><br/>
                  @endforeach
            </div>
        @endif
        @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
        @endif   
	    <div class="row">
	        <div class="col-md-12">
	            <div class="panel panel-default">
				    <div class="panel-body">
				    	<form name = "myForm" style="padding:10px;" > 
			    			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			    			<b>Event Name:</b> {{$userevent->event->name}} <br><br>
                            <b>User :</b> {{$userevent->user->name}} <br><br>
                            <b>Added On :</b> {{$userevent->added_on}} <br><br>
                            <b>Approved By :</b> {{$userevent->approver->name}} <br><br>
                            <b>Uploaded By :</b> {{$userevent->uploader->name}} <br><br>
                            <b>Note :</b> {{$userevent->note}} <br><br>
			    		</form>
                    </div>
                    <table class="table table-striped">
                    <thead>
	            		<th width="10%">#</th>
	            		<th width="15%">File name</th>
	            		<th class="text-right">Actions</th>
					</thead>
					@if(count($userevent->attachments) > 0)
                    @foreach($userevent->attachments as $attachment)
                        <tr>
                        <td class="td-text"><a>{{$attachment->id}}</a></td>
                        <td class="td-text"><a href="/admin/attachments/download/{{$attachment->id}}"> {{$attachment->attachment}}</a></td>
                        <td class="text-right">
                            <!-- <a href="/admin/user-events/{{$userevent->id}}" class="btn btn-success crud-btn btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View</a>	
                            <a href="/admin/user-events/{{$userevent->id}}/edit" class="btn btn-info crud-btn btn-sm"><i class="fa fa-pencil btn-icon-space" aria-hidden="true"></i>Edit</a> -->

                            <form action="/admin/user-events-attachment/{{$attachment->id}}" method="post" style="display:inline-block;" class="deleteform">
                                <input name="_method" type="hidden" value="DELETE">
                                <button class="btn btn-danger crud-btn btn-sm" type="submit"><i class="fa fa-trash btn-icon-space" aria-hidden="true"></i>Delete</button>
                            </form>
                        </td>
                        </tr>
                    @endforeach
                    @else
		            		<tr>
		            			<td colspan="3">No Attachments added</td>
		            			<td></td>
		            			<td></td>
		            		</tr>
		            @endif
	            	</table>
				</div>
			</div>	
		</div>
    </div>
    <script>
		$(function(){
			$(".deleteform").submit(function(event){
					return confirm('Are you sure?');
			});
        });
	</script>
@endsection