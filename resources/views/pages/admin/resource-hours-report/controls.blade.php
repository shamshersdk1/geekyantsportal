<div class="col-sm-6">
    <div class="row">    
        <div class="col-sm-3" style="margin-top: 3px;">
            <a  id="previous" class="prev btn btn-default margin-bottom-sm js-filter-date-actions" style="color: #616161;"><i class="fa fa-arrow-left" aria-hidden="true"></i></a> 
            <a  id="next" class="next btn btn-default margin-bottom-sm js-filter-date-actions" style="color: #616161;"><i class="fa fa-arrow-right" aria-hidden="true"></i></a> 
        </div>
       <div class="col-sm-9">
            <div class="form-group">
                <div class='input-group date'>
                    <input type='text' id="dates_value" class="form-control" disabled/>
                    <span class="input-group-addon">
                        <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-sm-6">
    <div>
        <div class="col-xs-1">
            <div class="red-small-box"></div>
        </div>
        <div class="col-xs-11">
            <div>On Leave</div>
        </div>
    </div>
    <div>
    <div class="col-xs-1">
        <div class="red-small-box free-green-background"></div>
    </div>
    <div class="col-xs-11">
        <div>Unassigned</div>
    </div>
</div>
</div>
<script>
function getQueryString(date)
    {
        var yyyy = date.getFullYear();
        var mm = date.getMonth()+1;
        if(mm < 10)
        {
            mm = '0'+ mm;
        }
        var dd = date.getDate();
        if(dd < 10)
        {
            dd = '0'+ dd;
        }
        return yyyy+'-'+mm+'-'+dd;
    }
$(function(){
    var flag=0;
    var range = 7;
    @if(Request::get('start_date'))
        var start_date = new Date('{{Request::get('start_date')}}');
        var end_date = new Date();
        end_date.setDate(start_date.getDate() + 6);
        if(start_date.getDay() == 1 && end_date.getDay() == 0)
        {
            flag = 1;
            $('#dates').val(start_date.toDateString() +" to "+ end_date.toDateString());
             var dates_value = getQueryString(start_date)+' '+'-'+' '+getQueryString(end_date);
             $("#dates_value").val(dates_value);
        }
    @endif
    if(flag == 0)
    {
        var d = new Date();
        var diff = d.getDay() - 1;
        if(diff==-1)
        {
            diff=6;
        }
        var start_date = new Date();
        var end_date = new Date();
        start_date.setDate(d.getDate() - diff);
        end_date.setDate(start_date.getDate() + 6);
        $('#dates').val(start_date.toDateString() +" to "+ end_date.toDateString());
        var dates_value = getQueryString(start_date)+' '+'-'+' '+getQueryString(end_date);
        $("#dates_value").val(dates_value);
    }
    start_date.setDate(start_date.getDate() + range);
    end_date.setDate(start_date.getDate() + 6);
    var message = '?start_date='+getQueryString(start_date)+'&end_date='+getQueryString(end_date);
    $('#next').prop(
        'href',
        '/admin/resource-hours-report'+message
    );
    start_date.setDate(start_date.getDate() - 2*range);
    end_date.setDate(start_date.getDate() + 6);
    var message = '?start_date='+getQueryString(start_date)+'&end_date='+getQueryString(end_date);
    $('#previous').prop(
        'href',
        '/admin/resource-hours-report'+message
    );
});

</script>
<br>
<br>