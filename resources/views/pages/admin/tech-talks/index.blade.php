@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid" >
    <div class="breadcrumb-wrap">
      <div class="row">
      <div class="col-sm-8">
        <h1 class="admin-page-title">Tech Talk</h1>
        <ol class="breadcrumb">
          <li><a href="/admin">Admin</a></li>
          <li class="active">Tech-Talk</li>
        </ol>
      </div>
    </div>
    </div>
<div class="row bg-white search-section-top">
  <div class="col-md-8">
    <ul class="btn-group" style="padding:0">
    <a href="" type="button" class="btn btn-primary ">All</a>
    <a href="" type="button" class="btn btn-default ">Approved</a>
    <a href="" type="button" class="btn btn-default ">Requested</a>
    <a href="" type="button" class="btn btn-default ">Pending</a>
    </ul>
  </div>
  <div class="col-md-4">
  <a href="" class="btn btn-success pull-right" style="margin-left:10px" data-toggle="modal" data-target="#myModal"><i class="fa fa-edit fa-fw"></i>Approved</a>
    <a href="" class="btn btn-success pull-right">Requested</a>
  </div>
</div>
  <div class="user-list-view m-t-30">
    <div class="panel panel-default ">
        <table class="table table-striped">
        </table>
     
    </div>
  </div>

  </div>
</div>
</div>

@endsection