@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-6">
                <h1 class="admin-page-title">Appraisal Bonus</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li><a href="{{ url('admin/appraisal-bonus') }}">Appraisal Bonus</a></li>
        		  	<li>{{$month->formatMonth()}}</li>
        		</ol>
            </div>
            <div class="col-sm-6">

                @if(!$month->vpfSetting || ($month->vpfSetting &&  $month->vpfSetting->value == 'unlocked'))
                    <div class="pull-right m-t-10">
                        <span>
                            <a href="/admin/appraisal-bonus/{{$month->id}}/regenerate" class="btn btn-warning"><i class="fa fa-gear fa-fw"></i>Regenerate</a>
                        </span>
                    </div>
                    <div class="pull-right m-t-10">
                        <span>
                            <a href="/admin/appraisal-bonus/{{$month->id}}/status-month" class="btn btn-danger"><i class="fa fa-lock fa-fw"></i>Lock</a>
                        </span>
                    </div>
                @endif
			</div>
		</div>
    </div>
    @if($month->vpfSetting &&  $month->vpfSetting->value == 'locked')
        <div class="pull pull-right">
            <span class="label label-primary">Locked at {{datetime_in_view($month->vpfSetting->created_at)}}</span>
        </div>
    @endif
	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
    </div>

    <form name="editForm" method="post" action="/admin/appraisal-bonus/{{$month->id}}/update">
        @if(!$month->vpfSetting ||  $month->vpfSetting->value == 'unlocked')
                <div class="row">
                    <div class="pull-right m-t-1 ">
                        <button type="submit" class="btn btn-success "><i class="fa fa-plus fa-fw"></i>Save</button>
                    </div>
                </div>
        @endif
        <div class="user-list-view">
            <div class="panel panel-default">
                <table class="table table-striped">
                    <tr>
                        <th class="text-center">#</th>
                        <th class="text-left">Name</th>
                        <th class="text-center">Type</th>
                        <th class="text-center">Actual Amount</th>
                        <th class="text-center">Amount</th>
                        <th class="text-center">Created At</th>
                        <th class="pull-right">Action</th>
                    </tr>
                    @if(count($appraisals) > 0)
                        @foreach($appraisals as $index => $appraisal)
                            <tr>
                                <td class="text-center">{{ $index +1 }}</td>
                                <td class="text-left"> {{ $appraisal->appraisal->user->employee_id}} | {{ $appraisal->appraisal->user->name}}</td>
                                @if($appraisal->type=="annual")
                                <td class="text-center">Annual</td>
                                <td class="text-center">{{ $appraisal->appraisal->annual_bonus}}</td>
                                @elseif($appraisal->type=="monthly_variable")
                                <td class="text-center">Monthly</td>
                                <td class="text-center">{{ round($appraisal->appraisal->monthly_var_bonus/12)}}</td>
                                @else
                                <td class="text-center">Others</td>
                                @endif
                                <td class="text-center">
                                    @if(($month->vpfSetting && $month->vpfSetting->value == 'locked')||$appraisal->type=="annual")
                                        {{$appraisal->amount}}
                                    @else
                                        <input class="text-center" name="new_amount[{{$appraisal->id}}]" type="text" value="{{$appraisal->amount}}" />
                                    @endif
                                </td>
                                <td class="text-center">{{ datetime_in_view($appraisal->created_at)}}</td>
                                <td class="pull-right">
                                    @if(!$month->vpfSetting || $month->vpfSetting->value == 'unlocked')
                                        @if(($appraisal->appraisal && !$appraisal->appraisal->userAnnualBonus) && ($appraisal->appraisal && $appraisal->appraisal->annual_bonus>0))
                                            <a href="/admin/appraisal-bonus/{{$month->id}}/{{$appraisal->appraisal->user_id}}/addAnnualBonus" class="btn btn-primary btn-sm crude-btn"><i class="fa fa-plus fa-fw"></i>Add Annual Bonus</a>
                                        @endif
                                     <a href="/admin/appraisal-bonus/{{$appraisal->id}}/delete" class="btn btn-danger btn-sm crude-btn"><i class="fa fa-trash fa-fw"></i>Delete</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="3">
                                <td>No Records found</td>
                            </td>
                        </tr>

                    @endif
                </table>
            </div>
        </div>
    </form>
</div>
@endsection
