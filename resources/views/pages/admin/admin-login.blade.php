@extends('layouts.plane')
@section('body')
<div class="container-fluid">
	<div class="login-main-wrapper login-wrapper">
		

			<div class="">
				<div class="logo-holder-panel">
					<a class="admin-login-logo" href="/"><img src="/images/logo-dark.png" class="img-responsive admin-logo-img center-block" alt=""></a>
				</div>
			</div>

			<div class="panel-login-admin">
				<!-- <div class="logo-holder-panel">
					<a class="admin-login-logo" href="/"><img src="/images/logo-dark.png" class="img-responsive admin-logo-img center-block" alt=""></a>
				</div> -->
				@if(!empty($errors->all()))
					<div class="alert alert-danger">
						@foreach ($errors->all() as $error)
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							<span>{{ $error }}</span><br/>
					  	@endforeach
					</div>
				@endif
				@if (session('message'))
				    <div class="alert alert-success">
				    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				        <span>{{ session('message') }}</span><br/>
				    </div>
				@endif
				<div class="panel panel-default">
					<div class="panel-heading text-center">Login</div>
					<div class="panel-body text-center">
						<a href="{{ route('social.redirect', ['provider' => 'google']) }}" class="btn btn-lg btn-danger"><i class="fa fa-google-plus" aria-hidden="true"></i> Sign in with Google</a>
								  
						<!-- <form class="form-horizontal" role="form" method="POST" action="login">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<div class="input-group custom-input-group">
								<span class="input-group-addon" id="basic-addon1"><i class="fa fa-user fa-lg" aria-hidden="true"></i></span>
								<input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="User Name">
							</div>
							<div class="input-group custom-input-group">
								<span class="input-group-addon" id="basic-addon1"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
								<input type="password" class="form-control" name="password" placeholder="Password">
							</div>
							<div class="row">
								<div class="col-md-6 text-left">
									<a href="signup" class="admin-gray-text">Sign up</a>
								</div>
								<div class="col-md-6 text-right">
									<a href="/password/reset" class="admin-gray-text">Forgot Password?</a>
								</div>
							</div>
							<div class="sign-btn">
								<button type="submit" class="btn btn-primary btn-block admin-signin" >
									SIGN IN
								</button>
							</div>

							</div>
						</form> -->

					</div>
				</div>
			</div>

		
	</div>
</div>
@endsection
