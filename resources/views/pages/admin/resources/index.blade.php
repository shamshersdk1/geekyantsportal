@extends('layouts.admin-dashboard')
    @section('main-content')
    <div class="container-fluid">
        <div class="breadcrumb-wrap">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="admin-page-title">Resources</h1>
                    <ol class="breadcrumb">
                        <li><a href="/admin">Admin</a></li>
                        <li class="active">Resources</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- ============================= admin leave Board ================================ -->
        <div class="row">
            <div class="col-md-12">
                @if(!empty($errors->all()))
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <span>{{ $error }}</span><br/>
                          @endforeach
                    </div>
                @endif
                @if (session('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ session('message') }}</span><br/>
                    </div>
                @endif
            </div>
            <!--======================== date component ==================================================== -->
            <form action="/admin/resources/show"  method="get">
                    <div class="input-group">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-10">
                                    <div class='input-group date' id='datetimepickerStart_search'>
                                        <input type='text' id="sd" class="form-control" value="<?=( isset( $_GET['check_date'] ) ? $_GET['check_date'] : '' )?>" autocomplete="off" name="check_date" placeholder="Check Date" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn btn-primary btn-sm" >Search</button>
                                     </span>
                                </div>  
                            </div>
                        </div>    
                    </div>
                </form>
                <!--   ====================================================================      -->
            
        </div>
    </div>
@endsection