@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Resources</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
		  			<li class="active">Resources</li>
        		</ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
	    	@if(!empty($errors->all()))
	            <div class="alert alert-danger">
	                @foreach ($errors->all() as $error)
	                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                    <span>{{ $error }}</span><br/>
	                  @endforeach
	            </div>
	        @endif
	        @if (session('message'))
	            <div class="alert alert-success">
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ session('message') }}</span><br/>
	            </div>
	        @endif
			<div class="panel panel-default">
            	<table class="table table-striped">
            		<th width="10%" class="sorting_asc">#</th>
            		<th width="45%" class="sorting_asc">Name of Resource</th>
                    <th width="45%">Allocation %</th>
            		@if(count($resources) > 0)
	            		@if(isset($resources))
		            		@foreach($resources as $resource)
			            		<tr>
			            			<td class="td-text"><a>{{$resource->id}}</a></td>
			            			<td class="td-text"><a> {{$resource->name}}</a></td>
                                    <td class="td-text"><a>{{$resource->allocation}}</a></td>
			            		</tr> 
		            		@endforeach
		            	@endif
	            	@else
	            		<tr>
	            			<td colspan="3" class="text-cenbter">No results found.</td>
	            		</tr>
	            	@endif
            	</table>
			</div>
		</div>	
	</div>
</div>

@endsection
@section('js')
@parent
	<script>
		
		function deletetech() {
			var x = confirm("Are you sure you want to delete?");
			if (x)
				return true;
			else
				return false;
		}
	</script>
@endsection
