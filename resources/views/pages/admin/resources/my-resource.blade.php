@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Resources</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li class="active">Mentor List</li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
         @if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                      @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif
        <div class="col-md-12">      
            <div class="panel panel-default">
                <table class="table table-striped">
                    <thead>
                        <th>Name of User</th>
                    </thead>    
                    @if(count($records) > 0)
                        @if(isset($records))
                            @foreach($records as $record)
                                <tr>
                                    <td class="td-text">{{$record->name}}</td>          
                                </tr>
                            @endforeach
                        @endif
                    @else
                        <tr>
                            <td colspan="1" class="text-cenbter">No results found.</td>
                        </tr>
                    @endif
                </table>
            </div>
        </div>  
    </div>
    <div class="pull-right">
            {{ $records->appends(array_except(Request::query(),'records'))->links(); }}
    </div>
</div>
<script type="text/javascript">
    $(function(){
        $( ".searchuser" ).autocomplete({
            source: {{$jsonuser}}
        });
        $("#btnReset").click(function(){
           window.location.href='/admin/resources/my-resource';
        }); 
    });
</script>
@endsection
