@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid" ng-app="myApp" ng-controller="vendorIndexCtrl">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Vendors</h1>
                <ol class="breadcrumb">
                <li><a href="/admin">Admin</a></li>
                <li class="active">Vendors</li>
            </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <button class="btn btn-success crude-btn" ng-click="addVendor()"><i class="fa fa-plus fa-fw"></i>Add new Vendor</button>
            </div>
        </div>
    </div>
    
    @if(!empty($errors->all()))
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ $error }}</span><br/>
            @endforeach
        </div>
    @endif
    @if (session('message'))
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <span>{{ session('message') }}</span><br/>
        </div>
    @endif

    <div class="row bg-white">
        <grid-view model="gridViewModel" columns="gridViewColumns"   actions="gridViewActions"></grid-view>
    </div>
    
</div>



@endsection