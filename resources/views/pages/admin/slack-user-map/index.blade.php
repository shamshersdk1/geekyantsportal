@extends('layouts.admin-dashboard')
@section('main-content')
<script type="text/javascript">
	function jsonEscape(str)  {
        return str.replace(/\r?\n|\r/g, '');
    }
    window.users = '{{$usersjson}}';
    window.slackUsers = '{{$slackUsers}}';
    window.assigned = '{{$assigned}}';

</script>
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Map users with their slack ids</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li class="active">Slack User Map</li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row" ng-app="myApp" ng-controller="slackmapCtrl">
         @if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span><br/>
                @endforeach
            </div>
        @endif
        <div ng-if="data.error" class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" ng-click="errorDismiss()" aria-label="close">&times;</a>
            <span>%%data.errorMessage%%</span><br/>
        </div>
        @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
        @endif
        <div ng-if="data.messageSent" class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" ng-click="messageDismiss()" aria-label="close">&times;</a>
            <span>Message Sent</span><br/>
        </div>
        <div class="row" style="margin-left: 1px; margin-right: 1px;">		
            <!-- <pre ng-repeat="user in users">
                <p ng-if="user.slackUser">%%user.slackUser.email%%</p>
                <p ng-if="!user.slackUser">Nothing found</p>
            </pre> -->
            <!-- <pre>%%assigned%%</pre> -->
            <div class="col-md-12 user-list-view">
                <div class="panel panel-default">
                    <table class="table table-striped">
                        <thead>
                            <th width="10%">Employee Id</th>
                            <th width="30%">Name of User</th>
                            <th width="40%">Slack Users List</th>
                            <th class="text-right">Action</th>
                        </thead>
                        <tbody>
                            <tr ng-repeat="user in users">
                                <td>%%user.employee_id%%</td>
                                <td>%%user.name%%</td>
                                <td>
                                    <!-- <select class="form-control" ng-model="assigned[user.id]" ng-options="slackUser.email for slackUser in slackUsers track by slackUser.id" ng-selected="%%user.slackUser.id%%" ng-change="update(%%user.id%%)">
                                        <option value="">No slack id selected</option>
                                    </select> -->
                                    <!--<pre>%%slackUsers%%</pre>-->
                                <ui-select ng-model="assigned[user.id]"  theme="select2" name="selectid2" on-select="update(user.id)">
                                    <ui-select-match>%%assigned[user.id].email%%</ui-select-match>
                                    <ui-select-choices repeat="slack_user in slackUsers | filter: $select.search">
                                        %%slack_user.email%%
                                    </ui-select-choices>
                                </ui-select>                                  
                                    <small ng-if="showError[user.id].code == 'error'" style="color:red"><i class="fa fa-exclamation-circle" aria-hidden="true"></i>  %%showError[user.id].text%%</small>
                                    <small ng-if="showError[user.id].code == 'message'" style="color:green"><i class="fa fa-check" aria-hidden="true"></i>  %%showError[user.id].text%%</small>
                                </td>
                                <td class="text-right"><button class="btn btn-success" ng-click="showModal(user)">Send Message</button></td>   
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>  
        </div>  
    </div>
</div>

@endsection



