@extends('layouts.admin-dashboard')
@section('main-content')
<section class="project-dashboard">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title"></h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
			  			<li><a href="/admin/project-quotation">Project Quotation Dashboard</a></li>
			  			<li class="active">View</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
		                <a href="/admin/project-quotation" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i>Back</a>
	            </div>
	        </div>
	    </div>
    	@if(!empty($errors->all()))
		    <div class="alert alert-danger">
		        @foreach ($errors->all() as $error)
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ $error }}</span><br/>
		          @endforeach
		    </div>
		@endif
		@if (session('message'))
		    <div class="alert alert-success">
		        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		        <span>{{ session('message') }}</span><br/>
		    </div>
		@endif
		<div class="row">
			<div class="col-sm-4">
				<h4 class="admin-section-heading">Basic Info</h4>
				<div class="panel panel-default">
					
					<div class="info-table">
						<label>Project Name: </label>
						<span>{{$quotationinfo->name}}</span>
					</div>
					<div class="info-table">
						<label>Description: </label>
						<span>{{$quotationinfo->description}}</span>
					</div>
					
				</div>
			</div>
			<div class="col-sm-4">
				<h4 class="admin-section-heading">Client Details</h4>
				<div class="panel panel-default">
				  	
				  	@if(isset($companies))
						<div class="info-table">
							<label>Company Name: </label>
							<span>{{$companies->name}}</span>
						</div>
						<div class="info-table">
							<label>Email: </label>
							<span>
								@if(isset($companies))
									{{$companies->email}}
								@endif
							</span>
						</div>
					@endif
					<div class="info-table">
						<label>Primary Contact: </label>
						<span>
							@if(isset($contacts))
			    				@foreach($contacts as $contact)
			    					{{$contact->first_name}} {{$contact->last_name}}
			    				@endforeach
			    			@endif
			    		</span>
					</div>
					<div class="info-table">
						<label>Primary Contact No.: </label>
						<span>
							@if(isset($companies->mobile))
								{{$companies->mobile}}
							@else
								<span>
								Not mention	
								</span>
							@endif
			    		</span>
					</div>
				</div>
			</div>	
			
		</div>

		<!-- ========================= panel 3 ============================= -->
		<div class="row">
			<div class="col-md-6">
				<div>
					<div class="row">
						<div class="col-sm-8">
							<h4>MileStone</h4>
						</div>
						<div class="col-sm-4 text-right m-t-10">
							<a href="/admin/project-quotation/{{$quotationinfo->id}}/add-milestone" class="btn btn-success btn-xs"><i class="fa fa-plus fa-fw"></i> Add Milestone</a>
						</div>
					</div>
					<div class="panel panel-default">
						<table class="table table-striped">
			    			<thead>
									<tr>
							        <th>#</th>
							        <th>Milestone Title</th>
							        <th>Delivery Date</th>
							        <th>Cost</th>
							        <th>Action</th>
							       
							    </tr>
							</thead>
							@if(count($milestones) > 0)
								@foreach($milestones as $milestone)
							      	<tr>
							      		<td>{{$milestone->id}}</td>
								        <td><a href="/admin/project-quotation/{{$quotationinfo->id}}/{{$milestone->id}}/show-milestone" >{{$milestone->title}} </a></td>
								        <td>{{$milestone->delivery_date}}</td>
								        <td>{{$milestone->cost}}</td>
								        
								        <td class="text-right">
								        	<button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#favoritesModal" onclick="getMilestone({{$milestone->id}})">Map Sprint</button>
								        	<a href="/admin/project-quotation/{{$quotationinfo->id}}/{{$milestone->id}}/edit-milestone" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i>
								        	</a>
								        	<form action="/admin/project-quotation/{{$milestone->id}}/delete-milestone" method="post" style="display:inline-block;">
									    		<input name="_method" type="hidden" value="DELETE">
									    		<button class="btn btn-danger btn-xs" type="submit" onclick="return deletemilestone()"><i class="fa fa-trash"></i></button>
									    	</form>
								        	 <!-- <a href="" class="btn btn-info btn-sm"><i class="fa fa-edit fa-fw"></i> Edit</a>
								        	<a href="" class="btn btn-success btn-sm"><i class="fa fa-eye fa-fw"></i> View</a>  -->
								        </td>
							      	</tr>
						      	@endforeach
						    @else
						    	<tr>
						    		<td colspan="5" class="text-center">No milestone Found.</td>
						    	</tr>
						    @endif
						</table>
					</div>
				</div>
				
			</div>
		<!-- ================== sprint panel ============== -->

		</div>
	</div>

	<div class="modal fade" id="favoritesModal" 
	     tabindex="-1" role="dialog" 
	     aria-labelledby="favoritesModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
		    <form action="/admin/project-quotation/map-sprints" method="post">
		      <div class="modal-header">
		        <button type="button" class="close" 
		          data-dismiss="modal" 
		          aria-label="Close">
		          <span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" 
		        id="favoritesModalLabel">Map Sprints</h4>
		      </div>
		      <div class="modal-body">
		      		
		      			<div class="panel-body">
		      			    <div class="row">
		      			    	<div class="col-md-12">	
					  				<div class="form-group col-md-12">
								    	<label for="" class="col-sm-2 control-label">Project Name</label>
								    	<div class="col-sm-10">
											<select id="selectid2" name="project_id" class="form-control" style="">
												<option value="">Select</option>
												@if(isset($projects))
													@if(count($projects) > 0)
														@foreach($projects as $project)
															<option value="{{$project->id}}">{{$project->project_name}}</option>
														@endforeach
													@endif
												@endif
											</select>
											<input type="hidden" name="milestone_id" id="milestone_id">
								    	</div>
							  		</div>
							  		<div class="form-group col-md-12">
							  			<label for="" class="col-sm-2 control-label">Sprints</label>
					  			    	<div class="col-sm-10" id = "check">
					  			    	</div> 
								  	</div>
								  	<div class="form-group col-md-12">
							  			<label for="" class="col-sm-2 control-label">Saved Sprints</label>
					  			    	<div class="col-sm-10" id = "sprints">
					  			    	</div>  
								  	</div>
					  			</div>
					  		</div>
					  	</div>
		      		
		      </div>
		      <div class="modal-footer">
		        <button type="button" 
		           class="btn btn-default" 
		           data-dismiss="modal">Close</button>
		        <span class="pull-right">
		        	<button type="submit" class="btn btn-primary">Save</button>
		        </span>
		      </div>
		    </form>
	    </div>
	  </div>
	</div>
</section>


@endsection
@section('js')
@parent
	<script>
			

		$('#selectid2').change(function() {
		    var data = "";
		    $.ajax({
		        type:"GET",
		        url : "http://geekyants-website.local.geekydev.com/admin/project-quotation/get-sprints/"+$(this).val(),
		        async: false,
		        success : function(response) {
		        	$('#check').empty();
		        	console.log("insuccess", response);
		            data = response;
		            for (i = 0; i < data.length; i++) {
		            	console.log("title",data[i].title);
		            	var radioBtn = $('<input type="checkbox" name="sprints[]" value="'+data[i].id+'">&nbsp<span>'+data[i].title+
		            		'</span><br>');
		                radioBtn.appendTo('#check');
		            }
		        },
		        error: function() {
		        	$('#check').empty();
		            alert('Error occured');
		        }
		    });
		    // var string = data.message.split(",");
		    // var array = string.filter(function(e){return e;});
		    // var select = $('selectbox2');
		    // select.empty();
		    // $.each(array, function(index, value) {          
		    //     select.append(
		    //             $('<option></option>').val(value).html(value)
		    //         );
		    // });
		    //     $('#selectbox2').show();
		});

		function getMilestone(id) {
			$('#milestone_id').val(id);
			$.ajax({
			    type:"GET",
			    url : "http://geekyants-website.local.geekydev.com/admin/project-quotation/get-milestone-sprints/"+id,
			    async: false,
			    success : function(response) {
			    	$('#sprints').empty();
			    	console.log("milestone success",response);
			        data = response;
			        for (i = 0; i < data.length; i++) {
			        	console.log("title 1",data[i].title);
			        	var radioBtn = $('<input type="checkbox" name="sprints[]" checked value="'+data[i].id+'">&nbsp<span>'+data[i].title+
			        		'</span><br>');
			            radioBtn.appendTo('#sprints');
			        }
			    },
			    error: function() {
			    	$('#check').empty();
			        alert('Error occured');
			    }
			});
		}

		function deletemilestone() {
			var x = confirm("Are you sure you want to delete?");
			if(x)
				return true;
			else
				return false;
		}
	</script>
@endsection