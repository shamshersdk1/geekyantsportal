@extends('layouts.admin-dashboard')
@section('main-content')
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-12">
	                <h1 class="admin-page-title">Milestones </h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
					  	<!-- <li class="active">New Lead</li> -->
	        		</ol>
	            </div>
	            <!-- <div class="col-sm-4 text-right m-t-10">
	                <a href="/admin/cms-technology/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i> Add new Technology</a>
	            </div> -->
	        </div>
	    </div>
	    <div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="row">
		        	@if(!empty($errors->all()))
			            <div class="alert alert-danger">
			                @foreach ($errors->all() as $error)
			                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			                    <span>{{ $error }}</span><br/>
			                  @endforeach
			            </div>
			        @endif
			        @if (session('message'))
			            <div class="alert alert-success">
			                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			                <span>{{ session('message') }}</span><br/>
			            </div>
			        @endif
				</div>
			</div>
			    <div class="row">
					<div class="col-md-8 col-md-offset-2">
						<div class="row">
				        	@if(!empty($errors->all()))
					            <div class="alert alert-danger">
					                @foreach ($errors->all() as $error)
					                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					                    <span>{{ $error }}</span><br/>
					                  @endforeach
					            </div>
					        @endif
					        @if (session('message'))
					            <div class="alert alert-success">
					                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					                <span>{{ session('message') }}</span><br/>
					            </div>
					        @endif
						</div>
					</div>
					<div class="row">
						<div class="col-sm-4">
							<h4 class="admin-section-heading">Basic Info</h4>
							<div class="panel panel-default">
								
								<div class="info-table">
									<label>Project Name: </label>
									<span>{{$quotationinfo->name}}</span>
								</div>
								<div class="info-table">
									<label>Description: </label>
									<span>{{$quotationinfo->description}}</span>
								</div>
								
							</div>
						</div>
						<div class="col-sm-4">
							<h4 class="admin-section-heading">Client Details</h4>
							<div class="panel panel-default">
							  	
							  	@if(isset($companies))
									<div class="info-table">
										<label>Company Name: </label>
										<span>{{$companies->name}}</span>
									</div>
									<div class="info-table">
										<label>Email: </label>
										<span>
											@if(isset($companies))
												{{$companies->email}}
											@endif
										</span>
									</div>
								@endif
								<div class="info-table">
									<label>Primary Contact: </label>
									<span>
										@if(isset($contacts))
						    				@foreach($contacts as $contact)
						    					{{$contact->first_name}} {{$contact->last_name}}
						    				@endforeach
						    			@endif
						    		</span>
								</div>
								<div class="info-table">
									<label>Primary Contact No.: </label>
									<span>
										@if(isset($companies->mobile))
											{{$companies->mobile}}
										@else
											<span>
											Not mention	
											</span>
										@endif
						    		</span>
								</div>
							</div>
						</div>	
						
					</div>
			<div class="row">
				<div class="col-md-8">
					<div class="panel panel-default panel-leadmanagment">
						
						<!-- <div class="panel-heading">
						    <h3 class="panel-title">Lead Managment</h3>
						</div> -->
						@if(isset($projectQuotationMilestones))
						  	<div class="panel-body custom-space">
						    	<div class="row">
						    		<div class="col-md-12">
						    			<form class="form-horizontal" method="post" action="/admin/project-quotation/{{$projectQuotationMilestones->id}}/update-milestone">
					    					<div class="form-group">
					    				  	<label for="inputEmail3" class="col-sm-2 control-label">Milestone Title </label>
					    				  	<div class="col-sm-9">
												<input type="text" class="form-control" name="title" id="inputEmail3" placeholder="Delivery Date" value ="{{$projectQuotationMilestones->title}}">
					    				  	</div>
					    					</div>
										  	<div class="form-group">
										    	<label for="inputEmail3" class="col-sm-2 control-label">Milestone Details </label>
										    	<div class="col-sm-9">
										      		<textarea class="form-control" name="details" id="inputEmail3" placeholder="Details">   {{$projectQuotationMilestones->details}}</textarea> 
										      		<input type="hidden" name="quotationId" value ="{{$projectQuotationMilestones->project_quotation_id}}">
										    	</div>
										  	</div>
										  	<div class="form-group">
										    	<label for="inputEmail3" class="col-sm-2 control-label">Delivery Date</label>
										    	<div class="col-sm-9">
										      		<input type="date" class="form-control" name="date" id="inputEmail3" placeholder="Delivery Date" value ="{{$projectQuotationMilestones->delivery_date}}">
										    	</div>
										  	</div>
										  	<div class="form-group">
										    	<label for="inputEmail3" class="col-sm-2 control-label">Cost</label>
										    	<div class="col-sm-9">
										      		<input type="number" class="form-control" name="cost" id="inputEmail3" placeholder="cost" value ="{{$projectQuotationMilestones->cost}}">
										    	</div>
										  	</div>
										  	
										  	
										  	<div class="col-md-6 col-md-offset-3">
										  		<button type="submit" class="btn btn-success btn-block">update Milestone</button>
										  	</div>
										  	
										</form>
						    		</div>
						    	</div>
						  	</div>
					  	@endif
					</div>
				</div>
			</div>
		</div>
	</div>
@stop