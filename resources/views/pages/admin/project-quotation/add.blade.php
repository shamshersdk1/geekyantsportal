@extends('layouts.admin-dashboard')
@section('main-content')

<section class="new-project-section">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">Add Project Quotation</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
	        		  	<li><a href="/admin/project-quotation">Project Quotation Dashboard</a></li>
			  			<li class="active">Add</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>

	    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif

		<form class="form-horizontal" method="post" action="/admin/project-quotation" enctype="multipart/form-data">
			<div class="panel panel-default">
				<div class="panel-body">
				    <div class="row">
				    	<div class="col-md-12">
						  	<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Client Name</label>
						    	<div class="col-sm-6">
						    		@if(isset($companies))
										<select id="selectid2" name="company_id" class="form-control" style="">
											<option value="">Select</option>
											@foreach($companies as $x)
									        	<option value="{{$x->id}}" >{{$x->name}}</option>
										    @endforeach
										</select>
									@endif
									@if(isset($company))
										<div>{{$company->name}}</div>
										<input type="hidden" name="company_id" value="{{$company->id}}">

									@endif

						    	</div>
						  	</div>
						  	<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Project Name</label>
						    	<div class="col-sm-6">
						      		<input type="text" class="form-control" id="" name="project_quotation" value="{{ old('project_name') }}">
						    	</div>
						  	</div>
						  	
					  	  	<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Description</label>
						    	<div class="col-sm-6">
						      		<textarea class="form-control" rows="5" name="description">{{ old('notes') }}</textarea>
						    	</div>
						  	</div>
					  	</div>
					</div> 
				</div>
			</div>
  	  		<div class="text-center">
		  		<button type="reset" class="btn btn-default">Clear</button>
		  		<button type="submit" class="btn btn-success">Create New Project Quotation</button>
		  	</div>
		</form>
	</div>
</section>

@endsection