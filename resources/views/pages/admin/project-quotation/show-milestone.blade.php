@extends('layouts.admin-dashboard')
@section('main-content')
<section class="project-dashboard">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title"></h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
			  			<li><a href="/admin/project-quotation">Project Quotation Dashboard</a></li>
			  			<li class="active">View</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
		                <a href="/admin/project-quotation" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i>Back</a>
	            </div>
	        </div>
	    </div>
    	@if(!empty($errors->all()))
		    <div class="alert alert-danger">
		        @foreach ($errors->all() as $error)
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ $error }}</span><br/>
		          @endforeach
		    </div>
		@endif
		@if (session('message'))
		    <div class="alert alert-success">
		        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		        <span>{{ session('message') }}</span><br/>
		    </div>
		@endif
		<div class="row">
			<div class="col-sm-4">
				<h4 class="admin-section-heading">Basic Info</h4>
				<div class="panel panel-default">
					
					<div class="info-table">
						<label>Project Name: </label>
						<span>{{$quotationinfo->name}}</span>
					</div>
					<div class="info-table">
						<label>Description: </label>
						<span>{{$quotationinfo->description}}</span>
					</div>
					
				</div>
			</div>
			<div class="col-sm-4">
				<h4 class="admin-section-heading">Client Details</h4>
				<div class="panel panel-default">
				  	
				  	@if(isset($companies))
						<div class="info-table">
							<label>Company Name: </label>
							<span>{{$companies->name}}</span>
						</div>
						<div class="info-table">
							<label>Email: </label>
							<span>
								@if(isset($companies))
									{{$companies->email}}
								@endif
							</span>
						</div>
					@endif
					<div class="info-table">
						<label>Primary Contact: </label>
						<span>
							@if(isset($contacts))
			    				@foreach($contacts as $contact)
			    					{{$contact->first_name}} {{$contact->last_name}}
			    				@endforeach
			    			@endif
			    		</span>
					</div>
					<div class="info-table">
						<label>Primary Contact No.: </label>
						<span>
							@if(isset($companies->mobile))
								{{$companies->mobile}}
							@else
								<span>
								Not mention	
								</span>
							@endif
			    		</span>
					</div>
				</div>
			</div>
			<div class="col-sm-8">
				<h4 class="admin-section-heading">MileStone Details</h4>
	            <div class="panel panel-default">

				    <div class="panel-body panel panel-white">
				    	<form name = "myForm"> 
				    	@if(isset($milestone))
				    		@if(isset($milestone->title))
				    			<div class="info-table"><b>Title :</b> {{$milestone->title}} </div>
				    		@endif
			    			@if(isset($milestone->details))
			    				<div class="info-table"><b>Details :</b> {{$milestone->details}} </div>
			    			@endif
			    			@if(isset($milestone->cost))
			    				<div class="info-table"><b>Cost :</b> {{$milestone->cost}} </div>
			    			@endif
			    			@if(isset($milestone->cost))
			    				<div class="info-table"><b>Delivery Date :</b> {{$milestone->delivery_date}} </div>
			    			@endif
			    			@if(isset($milestone->sprints))
			    				
			    					<div class="info-table"><b>Sprint Name :
			    					@foreach($milestone->sprints as $sprint)</b>
			    						{{$sprint->title}} ,
			    					@endforeach
			    					</div>
			    			@endif	
			    			
			    		@endif
				    	</form>
				    	<!-- <a href="/admin/cms-technology" class="btn btn-primary crud-btn back-btn btn-lg"><i class="fa fa-arrow-left" aria-hidden="true"></i>Back</a> -->
				    </div>
				</div>	
			</div>	
			
		</div>

		<!-- ========================= panel 3 ============================= -->
	


@endsection
