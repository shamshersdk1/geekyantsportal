@extends('layouts.admin-dashboard')

@section('main-content')
	<section class="lead-dashboard-section">
		<div class="container-fluid">
			<div class="breadcrumb-wrap">
		    	<div class="row">
		            <div class="col-sm-8">
		                <h1 class="admin-page-title">Project Quotation </h1>
		                <ol class="breadcrumb">
		        		  	<li><a href="/admin">Admin</a></li>
		        		  	<li class="active">Project Quotation Dashboard</li>
		        		</ol>
		            </div>
		            <div class="col-sm-4 text-right m-t-10">
		                <a href="/admin/project-quotation/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Add Project Quotation</a>
		            </div>
		        </div>
		    </div>
		    <div class="row">
	  			<div class="col-md-12">
	  				@if(!empty($errors->all()))
			            <div class="alert alert-danger">
			                @foreach ($errors->all() as $error)
			                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			                    <span>{{ $error }}</span><br/>
			                  @endforeach
			            </div>
			        @endif
			        @if (session('message'))
			            <div class="alert alert-success">
			                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			                <span>{{ session('message') }}</span><br/>
			            </div>
			        @endif
	  			</div>
				<div class="col-md-12">
					<div class="panel panel-default">
					  	<table class="display table table-striped" cellpadding="10">
						  	<thead>
								<tr>
							        <th width="12%">Name</th>
							        <th width="10%">Client</th>
							        <th width="40%">Description</th>
							        <th class="text-center" width="35%">Action</th>
							    </tr>

							</thead>
				   	 		<tbody>
				   	 		@if(count($projectQuotations)>0)
					   	 		@foreach($projectQuotations as $projectQuotation)
						   	 		<tr>
						   	 			@if(count($projectQuotations) == 0 )
								   	 		<div>
								   	 			<td colspan="6" class="text-center">No Record Found</td>
								   	 		</div>
							   	 		@endif
							   	 		@if(isset($projectQuotations))
								   	 		
							                    <td>{{$projectQuotation->name}}</td>
							                    @if(isset($projectQuotation->client))
					                    			<td><strong>{{$projectQuotation->client->name}}</strong></td> 
				                    			@else
													<td> - </td> 
				                    			@endif
					                    		<td>{{$projectQuotation->description}}</td>
							                    <td class="text-right">
							                    	<form name = "myForm" method="get" style="display:inline-block;" action="/admin/project-quotation/{{$projectQuotation->id}}">
								            					<button class="btn btn-success btn-sm crud-btn" >View </button>  
								            		</form>	
							                    	
							                    </td>
								            
								        @endif
								    </tr>
							    @endforeach
							@else
								<tr>
									<td colspan="4">
									No record found
									</td>
								</tr>
							@endif
							    
			            	</tbody>
				   	 	</table>
				   	</div>
				</div>
			</div>
		</div>
	</section>
@stop
