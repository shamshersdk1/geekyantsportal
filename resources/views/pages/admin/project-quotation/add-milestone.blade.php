@extends('layouts.admin-dashboard')
@section('main-content')
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
					  	<li><a href="/admin/project-quotation/{{$quotationId}}">Project Quotation Dashboard</a></li>
					  	<li class="active">Add Milestone</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>
	    <div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="row">
		        	@if(!empty($errors->all()))
			            <div class="alert alert-danger">
			                @foreach ($errors->all() as $error)
			                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			                    <span>{{ $error }}</span><br/>
			                  @endforeach
			            </div>
			        @endif
			        @if (session('message'))
			            <div class="alert alert-success">
			                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			                <span>{{ session('message') }}</span><br/>
			            </div>
			        @endif
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4">
					<h4 class="admin-section-heading">Basic Info</h4>
					<div class="panel panel-default">
						
						<div class="info-table">
							<label>Project Name: </label>
							<span>{{$quotationinfo->name}}</span>
						</div>
						<div class="info-table">
							<label>Description: </label>
							<span>{{$quotationinfo->description}}</span>
						</div>
						
					</div>
				</div>
				<div class="col-sm-4">
					<h4 class="admin-section-heading">Client Details</h4>
					<div class="panel panel-default">
					  	
					  	@if(isset($companies))
							<div class="info-table">
								<label>Company Name: </label>
								<span>{{$companies->name}}</span>
							</div>
							<div class="info-table">
								<label>Email: </label>
								<span>
									@if(isset($companies))
										{{$companies->email}}
									@endif
								</span>
							</div>
						@endif
						<div class="info-table">
							<label>Primary Contact: </label>
							<span>
								@if(isset($contacts))
				    				@foreach($contacts as $contact)
				    					{{$contact->first_name}} {{$contact->last_name}}
				    				@endforeach
				    			@endif
				    		</span>
						</div>
						<div class="info-table">
							<label>Primary Contact No.: </label>
							<span>
								@if(isset($companies->mobile))
									{{$companies->mobile}}
								@else
									<span>
									Not mention	
									</span>
								@endif
				    		</span>
						</div>
					</div>
				</div>	
				
			</div>
			<div class="row">
				<div class="col-md-8">
					<div class="panel panel-default panel-leadmanagment">
						
						<!-- <div class="panel-heading">
						    <h3 class="panel-title">Lead Managment</h3>
						</div> -->
					  	<div class="panel-body custom-space">
					    	<div class="row">
					    		<div class="col-md-12">
					    			<form class="form-horizontal" method="post" action="/admin/project-quotation/store-milestone">
				    					<div class="form-group">
				    				  	<label for="inputEmail3" class="col-sm-2 control-label">Milestone Title</label>
				    				  	<div class="col-sm-9">
				    				    	<input type="text" class="form-control" name="title" placeholder="Title" value="{{Request::old('title')}}">
				    				  	</div>
				    					</div>
									  	<div class="form-group">
									    	<label for="inputEmail3" class="col-sm-2 control-label">Milestone Details</label>
									    	<div class="col-sm-9">
									      		<textarea type="text" class="form-control" name="details" id="inputEmail3" placeholder="Details">{{Request::old('details')}}</textarea> 
									      		<input type="hidden" name="quotationId" value = {{$quotationId}}>
									    	</div>
									  	</div>
									  	<div class="form-group">
									    	<label for="inputEmail3" class="col-sm-2 control-label">Delivery Date</label>
									    	<div class="col-sm-9">
									      		<input type="date" class="form-control" name="date" id="inputEmail3" placeholder="Delivery Date" value="{{Request::old('date')}}">
									    	</div>
									  	</div>
									  	<div class="form-group">
									    	<label for="inputEmail3" class="col-sm-2 control-label">Cost</label>
									    	<div class="col-sm-9">
									      		<input type="number" class="form-control" name="cost" id="inputEmail3" placeholder="cost" value="{{Request::old('cost')}}">
									    	</div>
									  	</div>
									  	
									  	
									  	<div class="col-md-6 col-md-offset-3">
									  		<button type="submit" class="btn btn-success btn-block">Add Milestone</button>
									  	</div>
									  	
									</form>
					    		</div>
					    	</div>
					  	</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop