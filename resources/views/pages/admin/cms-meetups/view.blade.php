@extends('layouts.admin-dashboard')
@section('main-content')
<section>
   <div class="container-fluid">
      <div class="breadcrumb-wrap">
         <div class="row">
            <div class="col-sm-6">
               <h1>Meetups </h1>
               <ol class="breadcrumb">
                  <li><a href="/admin">Admin</a></li>
                  <li><a href="/admin/meetups">Meetups</a></li>
                  <li class="active">View</li>
               </ol>
            </div>
         </div>
      </div>
      @if(!empty($errors->all()))
      <div class="alert alert-danger">
         @foreach ($errors->all() as $error)
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
         <span>{{ $error }}</span><br/>
         @endforeach
      </div>
      @endif
      @if (session('message'))
      <div class="alert alert-success">
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
         <span>{{ session('message') }}</span><br/>
      </div>
      @endif
      <div class="row cms-dev">
         <div class="col-md-12">
            <h4>Detail</h4>
            <div class="panel panel-default">
               <div class="panel-body row">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <div class=" col-md-6">
                  
                     <div class="row m-b">
                        <div class="col-sm-4"><label>Title : </label></div>
                        <div class="col-sm-8">React Meetup 2019</div>
                     </div>
                     <div class=" row m-b">
                     <div class="col-sm-4"><label>Desctiption : </label></div>
                        <div class="col-sm-8"> Our team members (Digvijay Wanchoo, Sanket Sahu, Krutarth Dave, Vaishali Anand, Sumant and Shubham Kumar) were organised a meetup on React Native & Flutter </div>
                     </div>
                     <div class="row m-b">
                     <div class="col-sm-4"><label>Status : </label></div>
                        <div class="col-sm-8">upcoming</div>
                     </div>
                  </div>
                  <div class=" col-md-6">
                     <div class="row m-b">
                     <div class="col-sm-4"><label>Display : </label></div>
                        <div class="col-sm-8"><img src="" style="height: 200px; width:200px;"></div>
                     </div>
                     <div class="row m-b">
                     <div class="col-sm-4"><label>Logo : </label></div>
                        <div class="col-sm-8"> <img src="" style="height: 50px; width:50px;"></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
@endsection