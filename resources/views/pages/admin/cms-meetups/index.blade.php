@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title"> Meetups</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
		  			<li class="active"> Meetups Dashboard</li>
        		</ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
				<a href="/admin/meetups/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Add Meetups</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
	    	@if(!empty($errors->all()))
	            <div class="alert alert-danger">
	                @foreach ($errors->all() as $error)
	                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                    <span>{{ $error }}</span><br/>
	                  @endforeach
	            </div>
	        @endif
	        @if (session('message'))
	            <div class="alert alert-success">
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ session('message') }}</span><br/>
	            </div>
			@endif
		</div>
		<div class="user-list-view col-sm-12">
            <div class="panel panel-default">
            	<table class="table table-striped">
					<tr>
						<th width="30%">Details</th>
						<th width="15%">Location</th>
						<th width="15%">Attending Member</th>
						<th width="10%">Status</th>
						<th class="text-right sorting_asc" width="18%">Actions</th>
					</tr>
				
	            		<tr>
	            			<td>
								<h5 style="margin:0">React Meetup 2019</h5>
								<small class="text-primary" style="color: #337ab7;">Our team members (Digvijay Wanchoo, Sanket Sahu, Krutarth Dave, Vaishali Anand, Sumant and Shubham Kumar) were organised a meetup on React Native & Flutter</small>
							</td>
							<td style="vertical-align:top !important">
								<h5  style="margin:0">Jun 26 th 2019</h5>
								<small class="text-primary" style="color: #337ab7;">Bangalore</small>
							</td>
							<td ><span> Sanket Sahu</span>, <span>Digvijay Wanchoo</span>, <span>Krutarth Dave</span></td>
							<td><div class="badge badge-primary">Upcoming</div></td>
							<td class="text-right">
							<a class="btn btn-primary btn-sm crude-btn" href="/admin/meetups/view">View</a>
							<button class="btn btn-info btn-sm crude-btn" type="submit">Edit</button>
							<button class="btn btn-danger btn-sm crude-btn" type="submit">Delete</button>

							</td>
	            		</tr>
            	</table>
				<div class="col-md-12">
                    <div class="pageination pull-right">
                        <nav aria-label="Page navigation">
                              <ul class="pagination">
                              </ul>
                        </nav>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>
</div>
<script>
    
</script>
@endsection
