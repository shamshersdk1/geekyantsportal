@extends('layouts.admin-dashboard')
@section('main-content')

<section class="new-project-section">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">Meetups</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
			  			<li><a href="{{ url('admin/meetups') }}">Meetups</a></li>
			  			<li class="active">Add</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <a href="{{ url('admin/meetups') }}"  class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</a>
	            </div>
	        </div>
	    </div>

	    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif

		<form class="form-horizontal" method="post" action="/admin/tech-events" enctype="multipart/form-data">
			<div class="row">
			    <div class="col-md-8 col-sm-offset-2">
			    	<div class="panel panel-default m-t-15">
						<div class="panel-body">
				    
							<div class="form-group">
						    	<label for="" class="col-sm-3 control-label">Title</label>
						    	<div class="col-sm-8">
						      		<input type="text" class="form-control" id="" name="title" value="{{ old('title') }}">
						    	</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Status</label>
								<div class="col-sm-8">
									<select class="form-control" name="status">
										<option value="1">Upcoming</option>
										<option value="0">Past</option>
									</select>
								</div>
							</div>
                            <div class="form-group">
								<label class="col-sm-3 control-label">Attending</label>
                                <div class="col-sm-8">
                             <select class="user_id" id="user_id" name="user_id" style="width=35%;">
                                <option value=""></option>
                                @foreach($users as $user)
                                    @if ( old('user_id') == $user->id )
                                        <option value="{{$user->id}}" selected >{{$user->name}}</option>
                                    @else
                                        <option value="{{$user->id}}" >{{$user->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                            </div>
                            </div>
						
						  	<div class="form-group">
						    	<label for="" class="col-sm-3 control-label">Location</label>
						    	<div class="col-sm-8">
						      		<input type="text" class="form-control" id="location" name="location" value="{{ old('location') }}">
						    	</div>
						  	</div>
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Upload Display Image</label>
								<div class="col-sm-8">   
                                <div class=" form-control">
									<input type="file" id="display_file"  name="display_file" multiple="false" style="border:none">
                                </div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Description</label>
								<div class="col-sm-8">
									<textarea class="form-control" id="description" name="description" rows="4" placeholder="Enter details">{{old('description')}}</textarea>
								</div>
							</div>
                        
							<div class="form-group">
						    	<label for="" class="col-sm-3 control-label">URL</label>
						    	<div class="col-sm-8">
						      		<input type="text" class="form-control" id="url" name="url" value="{{ old('url') }}">
						    	</div>
						  	</div>
							<div class="form-group ">
								<label for="" class="col-sm-3 control-label">Upload Logo Image</label>
								<div class="col-sm-8">    
                                 <div class=" form-control">
									<input type="file" id="logo_file" class="" name="logo_file" multiple="false" style="border:none">
                                </div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-4 col-sm-offset-3">
									<div class='input-group date' id='datetimepicker1'>
									<input type='text' name="start_date" placeholder = "Start Date" class="form-control" />
										<span class="input-group-addon">
											<span><i class="fa fa-calendar" aria-hidden="true"></i></span>
										</span>
									</div>
								</div>
								<div class="col-sm-4">
									<div class='input-group date' id='datetimepicker2'>
									<input type='text' name="end_date" placeholder = "End Date" class="form-control" />
										<span class="input-group-addon">
											<span><i class="fa fa-calendar" aria-hidden="true"></i></span>
										</span>
									</div>
								</div>
							</div>

					  	</div>
					</div>
				</div>
			</div>
  	  		<div class="text-center">
		  		<button type="submit" class="btn btn-success">Add Meetups</button>
		  	</div>
		</form>
	</div>
</section>
<script type="text/javascript">
	$(function () {
		$('#datetimepicker1').datetimepicker({
			format: 'DD-MM-YYYY'
		});
		$('#datetimepicker2').datetimepicker({
			format: 'DD-MM-YYYY'
		});
        $('#user_id').select2();
	});
</script> 
@endsection
