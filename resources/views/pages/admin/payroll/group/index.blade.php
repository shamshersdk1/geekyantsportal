@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">List of Payroll Groups</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li><a href="{{ url('admin/payroll') }}">List of Groups</a></li>
        		</ol>
			</div>
			<div class="col-sm-4 text-right">
                <button class="btn btn-success" type="button" data-toggle="modal" data-target="#addGroupModal"><i class="fa fa-plus fa-fw"></i>Add New Payroll Group</button>
            </div>
        </div>
    </div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
	<div class="user-list-view">
		<div class="panel panel-default">
			<table class="table table-striped">
				<th>#</th>
				<th>Name</th>
				<th>Created at</th>
				<th class="text-right">Actions</th>
				@if(count($groups) > 0)
					@foreach($groups as $group)
						<tr>
							<td class="td-text">
								{{$group->id}}
							</td>
							<td class="td-text">
								{{$group->name}}
							</td>
							<td class="td-text">
								{{datetime_in_view($group->created_at)}}
							</td>
							<td class="td-text text-right">
								<a href="/admin/payroll/group/{{$group->id}}/rules" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Add rules</a> 
								@if( $group->user_id == $user->id || $user->isAdmin())
									<form id="deleteForm" style="display:inline" method="post">
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
										<input name="_method" type="hidden" value="DELETE" >
										<button type = "button" class = "btn btn-danger btn-sm" onclick="deletegroup({{$group->id}});" 
										data-toggle="tooltip" data-placement="top" 
										title="Delete!">
											<i class="fa fa-trash"></i> Delete
										</button>
									</form>
								@else
									Not authorised
								@endif
							</td>
						</tr>
					@endforeach
				@else
					<tr>
						<td colspan="3">No results found.</td>
						<td></td>
						<td></td>
					</tr>
				@endif
		</table>
		@include('pages/admin/payroll/group/add-group-modal')
		</div>
	</div>
@endsection


@section('js')
	<script>
		function deletegroup($id) {
			var x = confirm("Are you sure you want to delete?");
			if (x === true) {
				$('#deleteForm').attr('action', '/admin/payroll/group/'+$id);
				$('#deleteForm').submit();
				return;
			}
		}
		$(function() {

            $("#addGroupModal").on("show.bs.modal", function(){
                $(this).find("input,textarea,select")
                   .val('')
                   .end()
            });

            $("#addGroupModal").on("hidden.bs.modal", function(){
                $(this).find("input,textarea,select")
                   .val('')
                   .end()
            });
		});
	</script>

@endsection