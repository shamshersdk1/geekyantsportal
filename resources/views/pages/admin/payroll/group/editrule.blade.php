@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
		<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">{{$rule->name}}</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
					<li><a href="{{ url('admin/payroll') }}">List of Groups</a></li>
                    <li><a href="{{ url('/admin/payroll/group/'.$groupObj->id.'/rules') }}">{{$groupObj->name}}</a></li>
                    <li class="active">Edit Rule</li>
                </ol>
			</div>
			<div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
        </div>
	</div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">Edit Rule</div>
				<div class="panel-body">
					<form action = "/admin/payroll/group/{{$groupObj->id}}/rules/{{$rule->id}}" method="POST" style="padding:10px;" > 
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						{{method_field('PUT')}}
						<div class="form-group col-md-6">
							<label class="col-md-4" style="padding-top:2%" align="left" for="name">Name:</label>
							<div class="col-md-8" style="padding:0">
								<select name="name" id="rule-selector" class="form-control rule-name">
									<option value="">Select a rule name</option>
								</select>
								<!-- <input type="text" id="name" name="name" class="form-control rule-name" placeholder="Name" required value="{{ $rule->name }}"> -->
							</div>
						</div>
						<div class="form-group col-md-6">
							<label class="col-md-4" style="padding-top:2%" align="left" for="name">Calculation Type:</label>
							<div class="col-md-8" style="padding:0">
								<input type="text" id="calculation_type" name="calculation_type" class="form-control" required value="{{ $rule->calculation_type }}">
							</div>
						</div>
						<div class="form-group col-md-6">
							<label class="col-md-4" style="padding-top:2%" align="left" for="event">Type:</label>
							<div class="col-md-8 col-sm-12 col-xs-12" style="padding:0px">
								<select class="form-control col-md-12 col-sm-12 col-xs-12" style="border-radius:0px;-webkit-appearance: none" name="type" id="type">
									<option value="Credit">Credit</option>
									<option value="Debit">Debit</option>
								</select>
								<span class="glyphicon glyphicon-triangle-bottom form-control-feedback" style="opacity:0.3;left:88%" ></span>
							</div>
						</div>
						<div class="form-group col-md-6">
							<label class="col-md-4" style="padding-top:2%" align="left" for="name">Calculation Value:</label>
							<div class="col-md-8" style="padding:0">
								<input type="text" id="calculation_value" name="calculation_value" class="form-control" required value="{{ $rule->calculation_value }}">
							</div>
						</div>
						<div class="form-group col-md-6">
							<label class="col-md-4" style="padding-top:2%" align="left" for="event">Calculated Upon:</label>
							<div class="col-md-8 col-sm-12 col-xs-12" style="padding:0px">
								<select class="form-control col-md-12 col-sm-12 col-xs-12" style="border-radius:0px;-webkit-appearance: none" name="calculated_upon" id="calculated_upon">
									<option value="-1" selected></option>
									<option value="0">Annual Gross Salary</option>
									@if(count($groupObj->rules) > 0)
										@foreach($groupObj->rules as $var)
											<option value="{{$var->id}}">@if(!empty($var->csvValue)){{$var->csvValue->value}}@else{{$var->name}}@endif</option>
										@endforeach
									@endif
								</select>
								<span class="glyphicon glyphicon-triangle-bottom form-control-feedback" style="opacity:0.3;left:88%" ></span>
							</div>
						</div>
						<div class="col-md-12">
							<button style="display: block; margin: 0 auto;" type="submit" class="btn btn-success">Save</button>
						</div>
					</form>
				</div>
            </div>
		</div>	
	</div>
</div>
<script>
	$(function () {
		$('#type option[value="{{ucfirst($rule->type)}}"]').prop("selected",true);
		$('#calculated_upon option[value="{{$rule->calculated_upon}}"]').prop("selected",true);
		// $( ".rule-name" ).autocomplete({
			// 		source: {{$groupRules}}
			// });
			
		var rulesArray = {{$groupRules}};
		if(rulesArray.length > 0) {
			var option = '';
			$.each(rulesArray, function( key, value) {
				option = option + '<option value="'+value.key+'">'+value.value+'</option>';
			});
			$("#rule-selector").append(option);
		}
		$('#rule-selector option[value="{{$rule->name}}"]').prop("selected",true);

	});
</script>
@endsection