
<div class="modal fade" id="addGroupModal" tabindex="-1" role="dialog" aria-labelledby="addGroupModalLabel">
    <form action="/admin/payroll/group" method="POST">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="addGroupModalLabel">Add Payroll Group</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 ma-bot-10">
                            <div class="form-group ma-bot-10">
                                <label for="name">Group Name:</label>
                                <input type="text" name="name" class="form-control" placeholder="Name" value="{{ old('name') }}" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>&nbsp;
                    <span class="pull-right">
                        <button type="submit" class="btn btn-primary">
                            Save
                        </button>
                    </span>
                </div>
            </div>
        </div>
    </form>
</div>
