
<div class="modal fade" id="editCSVruleModal" tabindex="-1" role="dialog" aria-labelledby="editCSVruleModalLabel">
    <form id="editCSVruleform" method="POST">
	{{csrf_field()}}
	{{method_field('PUT')}}
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="editCSVruleModalLabel">Add Payroll Group</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-sm-12 ma-bot-10">
                            <div class="col-sm-2 ma-bot-10" style="padding:10px 0 0 15px">
                                <label for="name">Key:</label>
                            </div>
                            <div class="col-sm-10">
                                <input type="text" name="key" class="form-control" placeholder="Key" value="{{ old('key') }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12 ma-bot-10">
                            <div class="col-sm-2 ma-bot-10" style="padding:10px 0 0 15px">
                                <label for="name">Value:</label>
                            </div>
                            <div class="col-sm-10">
                                <input type="text" name="value" class="form-control" placeholder="Value" value="{{ old('value') }}" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>&nbsp;
                    <span class="pull-right">
                        <button type="submit" class="btn btn-primary">
                            Save
                        </button>
                    </span>
                </div>
            </div>
        </div>
    </form>
</div>
