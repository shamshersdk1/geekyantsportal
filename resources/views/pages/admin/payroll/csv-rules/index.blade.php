@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">List of CSV Rules</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li><a href="/admin/payroll-rules-csv">List of CSV Rules</a></li>
        		</ol>
			</div>
			<div class="text-right" style="padding-right:15px">
                <button class="btn btn-success" type="button" data-toggle="modal" data-target="#addCSVruleModal"><i class="fa fa-plus fa-fw"></i>Add New CSV Rule</button>
            </div>
        </div>
    </div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
	<div class="user-list-view">
		<div class="panel panel-default">
			<table class="table table-striped">
				<th width="10%">#</th>
				<th width="20%">Key</th>
                <th width="20%">Value</th>
				<th class="text-right">Actions</th>
				@if(!empty($rules))
					@foreach($rules as $rule)
						<tr>
							<td>{{$rule->id}}</td>
							<td>{{$rule->key}}</td>
                            <td>{{$rule->value}}</td>
							<td class="text-right">
								<button class="btn btn-info btn-sm" data-id="{{$rule->id}}" type="button" data-toggle="modal" style="display:inline" data-target="#editCSVruleModal"><i class="fa fa-eye fa-fw"></i>  Edit</button>
								<form action="payroll-rules-csv/{{$rule->id}}" class="deleteform" method="post" style="display:inline">
								{{method_field('DELETE')}}
								{{csrf_field()}}
									<button class="btn btn-danger btn-sm"><i class="fa fa-trash"></i>  Delete</button>
								</form>
							</td>
						</tr>
					@endforeach
				@else
					<tr>
						<td colspan="4">No results found.</td>
					</tr>
				@endif
			</table>
		</div>
	</div>
@include('pages/admin/payroll/csv-rules/add-csv-rule-modal')
@include('pages/admin/payroll/csv-rules/edit-csv-rule-modal')
<script>
    $(function() {
        $("#addCSVruleModal").on("show.bs.modal", function(){
            $(this).find("input,textarea,select")
            .val('')
            .end()
        });

        $("#addCSVruleModal").on("hidden.bs.modal", function(){
            $(this).find("input,textarea,select")
            .val('')
            .end()
        });
		$('#editCSVruleModal').on("show.bs.modal", function (e) {
			let id = $(e.relatedTarget).data('id')
			$.ajax({
				type: 'GET',
				url: '/admin/payroll-rules-csv/'+id,
				beforeSend: function() {
					$("div.loading").show();
					$("div.content").hide();
				},
				complete: function() {
					$("div.loading").hide();
					$("div.content").show();
				},
				success: function(data) {
					$('#editCSVruleModal input[name="key"]').val(data.key);
                    $('#editCSVruleModal input[name="value"]').val(data.value);
					$('#editCSVruleModal input[name="class_name"]').val(data.class_name);
					$('#editCSVruleModal #editCSVruleform').prop('action', 'payroll-rules-csv/'+data.id);
				}
			});
		});
		$("#editCSVruleModal").on("hidden.bs.modal", function(){
            $(this).find("input,textarea,select")
            .val('')
            .end()
        });
		$(".deleteform").submit(function(event){
            return confirm('Are you sure?');
        });
    });
</script>
@endsection
