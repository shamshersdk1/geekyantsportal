@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap assign-tasks">
    	<div class="flex-class align-items-center justify-content-between">
            <div>
                <h1 class="admin-page-title">Assign Tasks</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li>Assign Tasks</li>
        		</ol>
            </div>
            <div class="color-coding-div">
                <h5>Color Coding:</h5>
                <li><i class="fa fa-square unavailable"></i> Unavailable(Weekend/Holiday/On-Leave)</li>
                <li><i class="fa fa-square available"></i> Available</li>
            </div>
        </div>
	</div>
    <div class="user-list-view assign-task">
        <div class="row">
            <div class="col-md-12">
                @if(!empty($errors->all()))
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <span>{{ $error }}</span><br/>
                        @endforeach
                    </div>
                @endif
                @if (session('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ session('message') }}</span><br/>
                    </div>
                @endif
            </div>
        </div>
        <div class="row m-b-10">
            <div class="col-xs-5 col-sm-6">
                <input class="daterange form-control" @if(!empty($dates)) value='{{date("j F, Y", strtotime($dates[0]))}} to {{date("j F, Y", strtotime($dates[count($dates)-1]))}}' @endif>
            </div>
            <div class="col-xs-7 col-sm-6" style="float:right">
                <form action="/admin/shared-task" method="post" class="flex-class justify-content-end">
               
                    <select class="add-developer" style="background-color:white" name="new_developers[]" multiple="multiple">
                        @if(!empty($available_users))
                            @foreach($available_users as $user)
                                    <option value="{{$user->id}}">{{$user->name}}</option>
                            @endforeach
                        @endif
                    </select>
                    <button style="display:inline;height:32px;margin-top:0;" type="submit" class="btn btn-success fa fa-plus"> Add </button>
                </form>
            </div>
        </div>
        <div class="panel panel-default">
            <div id="table-scroll" class="table-scroll">
                <div class="table-wrap">
                    <table class="main-table">
                        <thead>
                            <tr>
                                <th class="fixed-side" width="150">Shared With</th>
                                <th class="fixed-side next" width="150">Developer</th>
                                <th>Notes</th>
                                @if(!empty($dates))
                                    @foreach($dates as $key => $date)
                                        @if($date == "today")
                                            <th class="today">Today</th>
                                        @else
                                            <th class="{{$date_code[$key]}}">{{date('j M', strtotime($date))}}</th>
                                        @endif
                                    @endforeach
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($data))
                                @foreach($data as $array)
                                    <tr>
                                        <td class="fixed-side" width="150">
                                            <select class="select-lead" data-user_id="{{$array['user_id']}}" name="leads[]" multiple="multiple" @if($array['is_removed']) disabled @endif>
                                                @if(!empty($users))
                                                    @foreach($users as $user)
                                                        @if($user->id != $array['user_id'] && $user->id != $array['creator_id'] && $user->id != $array['parent_id'])
                                                            <option value="{{$user->id}}" @if(in_array($user->id, $array['owner_array'])) selected @endif>{{$user->name}}</option>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </select>
                                        </td>
                                        <td class="fixed-side" style="left:170px">
                                            {{$array['user']}}
                                            @if(!$array['is_rm'] && !$array['is_removed'] && !$array['is_dev'])
                                                <a href="/admin/shared-task/detach/{{$array['user_id']}}" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            @endif
                                        </td>
                                        <td><textarea class="notes-box" data-user_id="{{$array['user_id']}}" @if($array['is_removed']) disabled @endif>{{$array['notes']}}</textarea></td>
                                        @foreach($array['dates'] as $key => $user_dates)
                                            <td @if($date_code[$key] == "weekend" || $user_dates == "on-leave") class="weekend" @elseif($date_code[$key] == "today") class="today" @endif><textarea data-user_id="{{$array['user_id']}}" data-date="{{$dates[$key]}}" class="textbox @if($user_dates == 'on-leave') weekend @else {{$user_dates}} @endif" @if($array['is_removed']) disabled @endif>{{$array['tasks'][$key]}}</textarea></td>
                                        @endforeach
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td class="fixed-side">Nothing added yet</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>     
                </div>
            </div>
        </div>
    </div>
</div>


@include('pages.admin.user-task.style')
@include('pages.admin.user-task.script')
@endsection