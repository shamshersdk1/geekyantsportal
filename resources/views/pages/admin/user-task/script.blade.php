
<script>

    function autosize(textarea) {
        $current = $(textarea).height(); 
        $new = $(textarea).prop("scrollHeight");
        if(($new - $current ) >= textarea.cols) {
            $(textarea).height(($(textarea).prop("scrollHeight")));
        }
    }

    var textChange = _.debounce(function(e) {
        var date = $(e.target).data('date');
        var user_id = $(e.target).data('user_id');
        var task_group_id = $(e.target).data('task_group_id');
        var text = $(e.target).val();
        $.ajax({
            type: 'POST',
            url: '/api/v1/user-tasks',
            dataType: 'text',
            data: {date : date, user_id: user_id, task_group_id: task_group_id, text: text, is_note: false},
            success: function(data) {
                $(e.target).removeClass("error");
            },
            error: function(errorResponse){
                $(e.target).addClass("error");
            }
        });     
    }, 500);

    var noteChange = _.debounce(function(e) {
        var user_id = $(e.target).data('user_id');
        var task_group_id = $(e.target).data('task_group_id');
        var text = $(e.target).val();
        $.ajax({
            type: 'POST',
            url: '/api/v1/user-tasks',
            dataType: 'text',
            data: {date : null, user_id: user_id, task_group_id: task_group_id, text: text, is_note: true},
            success: function(data) {
                $(e.target).removeClass("error");
            },
            error: function(errorResponse){
                $(e.target).addClass("error");
            }
        });
    }, 500);

    $(document).ready(function () {

        $(document).on("input", "textarea", function() {
            autosize(this);
        });
        
        $("textarea").each(function () {
            autosize(this);
        
        });
        
        $('.select-lead').select2({
            placeholder: "Shared with...",
        });

        $('.add-developer').select2({
            placeholder: "Add developers...",
            theme: "classic",
            width: "resolve",
        });
        
        $('.select-lead').change(function(e) {
            var user_id = $(e.target).data('user_id');
            var task_group_id = $(e.target).data('task_group_id');
            var owner_array = $(e.target).select2("val");
            $.ajax({
                type: 'POST',
                url: '/api/v1/shared-tasks',
                dataType: 'text',
                data: {user_id : user_id, owner_array: owner_array, task_group_id: task_group_id},
                success: function(data) {
                    $(e.target).removeClass("error");
                },
                error: function(errorResponse){
                    $(e.target).addClass("error");
                }
            });
        });
        
        $(".textbox").bind("keyup", textChange);
        $(".notes-box").bind("keyup", noteChange);
        
        $('.daterange').daterangepicker({
            alwaysShowCalendars: true,
            autoUpdateInput: false,
            locale: {
                format: "DD MMMM, YYYY"
            },
            ranges: {
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 2 Weeks': [moment().subtract(2, 'weeks').startOf('isoWeek'), moment().subtract(1, 'weeks').endOf('isoWeek')],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }); 

        $('.daterange').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD MMMM, YYYY') + ' to ' + picker.endDate.format('DD MMMM, YYYY'));
            $url = window.location.href.split('?')[0];
            window.location.href = $url+'?start_date='+picker.startDate.format('YYYY-MM-DD')+'&end_date='+picker.endDate.format('YYYY-MM-DD');
        });
        
    });
</script>