@extends('layouts.plane')
@section('body')
<div class="container-fluid">
	<div class="login-wrapper">
		<div class="row">

			<div class="col-md-6 col-md-offset-3">
				<div class="logo-holder-panel-signup">
					<a class="admin-login-logo" href="/"><img src="/images/logo-dark.png" class="img-responsive admin-logo-img center-block" alt=""></a>
				</div>
			</div>

			<div class="col-md-6 col-md-offset-3 panel-signup-admin">
				@if(!empty($errors->all()))
						<div class="alert alert-danger">
							@foreach ($errors->all() as $error)
								<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
								<span>{{ $error }}</span><br/>
						  	@endforeach
						</div>
					@endif
					@if (session('message'))
					    <div class="alert alert-success">
					    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					        <span>{{ session('message') }}</span><br/>
					    </div>
					@endif
				<div class="panel panel-default">
					<div class="panel-heading text-center">SIGNUP</div>
					
					<div class="panel-body">
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<form class="" role="form" method="POST" action="register">
									<div class="input-group custom-input-group">
									  	<span class="input-group-addon" id="basic-addon1"><i class="fa fa-user fa-lg" aria-hidden="true"></i></span>
									  	<input type="text" class="form-control" name="name" placeholder="Name" value="{{ old('name') }}">
									</div>
									<div class="input-group custom-input-group">
									  	<span class="input-group-addon" id="basic-addon1"><i class="fa fa-at fa-lg" aria-hidden="true"></i></span>
									  	<input type="text" class="form-control" name="email" placeholder="Email" value="{{ old('email') }}">
									</div>

									<div class="input-group custom-input-group">
									  	<span class="input-group-addon" id="basic-addon1"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
									  	<input type="password" class="form-control" name="password" placeholder="Password">
									</div>

									<div class="input-group custom-input-group">
									  	<span class="input-group-addon" id="basic-addon1"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
									  	<input type="password" class="form-control" name="confirmPassword" placeholder="Confirm Password">
									</div>

									<div class="row">
										<div class="col-md-12 text-right">
											<a href="" class="login-here">Already have account? <a href="admin" class="login">Login <i class="fa fa-sign-in" aria-hidden="true"></i></a></a>
										</div>
									</div>
									<div class="sign-btn">
										<button type="submit" class="btn btn-primary btn-block admin-signin" >
												REGISTER</button>
									</div>
								</form>
							</div>
						</div>		
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
