@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Events</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/events') }}">Events</a></li>
                </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <a href="/admin/events/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Add New Event</a>
            </div>
        </div>
    </div>
    <div class="row">
            <!-- <ol class="breadcrumb">
                <h2 class="breadcrumb-head-page">Technology Add</h2>
                <li><a href="/admin">Admin</a></li>
                <li><a href="{{ url('admin/cms-technology') }}">Technology</a></li>
                <li><a href="{{ url('admin/cms-technology') }}">Add</a></li>
            </ol> -->
        <div class="col-md-12">
            @if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                    @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif   
            <div class="panel panel-default">
            <table class="table table-striped">
                <thead>
                    <th >#</th>
                    <th width="10%">Logo</th>
                    <th width="10%">Event Name</th>
                    <th width="10%">Date</th>
                    <th width="10%">Location</th>
                    <th width="10%">Video</th>
                    <th width="10%">Published</th>
                    <th width="10%">Created By</th>
                    <th class="text-right">Actions</th>
                </thead>
                    @if(count($events) > 0)
                        @foreach($events as $event)
                            <tr>
                                <td class="td-text"><a>{{$event->id}}</a></td>
                            <td class="td-text"><img src="{{$event->logo_url}}" width="60px" height="40px"></td>
                                <td class="td-text"><a href="/admin/events/{{$event->id}}"> {{$event->title}}</a></td>
                                <td class="td-text">{{$event->date}}</td>
                                <td class="td-text">{{$event->location}}</td>
                                <td class="td-text"><a href="{{$event->video_link}}" class="btn btn-success crud-btn btn-sm" target="_blank"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>Video</a></td>
                                <td class="td-text">
                                    
                                </td>
                                <td class="td-text">
                                    @if($event->creator)
                                        {{$event->creator->name}}
                                    @endif
                                </td>
                                <td class="text-right">
                                    <a href="/admin/events/{{$event->id}}" class="btn btn-warning"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View</a>
                                    <a href="/admin/events/{{$event->id}}/edit" class="btn btn-info"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>Edit</a>	
                                    

                                    <form action="/admin/events/{{$event->id}}" method="post" style="display:inline-block;" class="deleteform">
                                        <input name="_method" type="hidden" value="delete">
                                        <button class="btn btn-danger" type="submit"><i class="fa fa-trash btn-icon-space" aria-hidden="true"></i>Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="3">No results found.</td>
                            <td></td>
                            <td></td>
                        </tr>
                    @endif
            </table>
            </div>
	    </div>	
    </div>
</div>
<script>
    let event;
    function updateEvent() {
        event.name = $('#editEventModal input[name="name"]').val();
        $.ajax({
            type: 'PUT',
            url: '/admin/events/'+event.id,
            dataType: 'text',
            data: event,
            beforeSend: function() {
                $("div.loading").show();
                $("div.content").hide();
            },
            complete: function() {
                $("div.loading").hide();
                $("div.content").show();
            },
            success: function(data) {
                $('#editEventModal').modal('hide');
                window.location.reload();
            },
            error: function(errorResponse){
                $('#editEventModal').modal('hide');
                console.log(errorResponse);
                window.location.reload();
            }
        });
    }
    $(function(){
        $("#addEventModal").on("show.bs.modal", function(){
            $(this).find("input")
                .val('')
                .end()
        });

        $("#addEventModal").on("hidden.bs.modal", function(){
            $(this).find("input")
                .val('')
                .end()
        });
        $('#editEventModal').on("show.bs.modal", function (e) {
			let id = $(e.relatedTarget).data('id')
			$.ajax({
				type: 'GET',
				url: '/admin/events/'+id+'/edit',
				beforeSend: function() {
					$("div.loading").show();
					$("div.content").hide();
				},
				complete: function() {
					$("div.loading").hide();
					$("div.content").show();
				},
				success: function(data) {
					event = data;
					$('#editEventModal input[name="name"]').val(data.name);
				}
			});
		});

		$("#editEventModal").on("hidden.bs.modal", function(){
			$(this).find("input")
				.val('')
				.end()
		});
        $(".deleteform").submit(function(event){
                return confirm('Are you sure?');
        });
    });
</script>
@endsection