@extends('layouts.admin-dashboard')
@section('main-content')
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">Event View</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
				  		<li><a href="{{ url('admin/events') }}">Events</a></li>
				  		<li class="active">View</li>
		        	</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
                    <form action="/admin/user-events/create" method="get" style="display:inline-block;">
		    			<input name="event" type="hidden" value="{{$event->id}}">
		    			<button style="display: none" class="btn btn-success" type="submit"><i class="fa fa-plus fa-fw"></i>Add User</button>
                    </form>
                </div>
	        </div>
		</div>
		@if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span><br/>
                  @endforeach
            </div>
        @endif
        @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
        @endif
	    <div class="row">
	        <div class="col-md-12">
	            <div class="panel panel-default">
				    <div class="panel-body">
				    	<form name = "myForm" style="padding:10px;" > 
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<div class="row">
								<div class="col-md-6"><b>Id :</b> {{$event->id}} <br></div>
								<div class="col-md-6"><b>Name :</b> {{$event->name}} <br></div>
							</div>
							<div class="row">
								<div class="col-md-6"><b>Date :</b> {{$event->date}} <br></div>
								<div class="col-md-6"><b>Title :</b> {{$event->title}} <br></div>
							</div>
							<div class="row">
								<div class="col-md-6"><b>Location :</b> {{$event->location}} <br></div>
								<div class="col-md-6"><b>Description:</b> {{$event->description}} <br></div>
							</div>
							<div class="row">
								<div class="col-md-6"><b>url :</b> {{$event->url}} <br></div>
								<div class="col-md-6"><b>Type:</b> {{$event->type}} <br></div>
							</div>
							<div class="row">
								<div class="col-md-6"><b>Logo Url :</b> {{$event->logo_url}} <br></div>
								<div class="col-md-6"><b>Blog Url:</b> {{$event->blog_url}} <br></div>
							</div>
							<div class="row">
								<div class="col-md-6"><b>Created By :</b> {{$event->created_by}} <br></div>
							</div>
							
			    		</form>
					</div>
					</div>
					<div class="panel panel-default">
					<table class="table table-striped">
                    <thead>
	            		<th class="text-left">#</th>
	            		<th class="text-left">Name of User</th>
	            		<!-- <th class="text-right">Actions</th> -->
					</thead>
					@if(count($userevents) > 0)
                    @foreach($userevents as $userevent)
						<tr>
						<td class="td-text"><a>{{$userevent->id}}</a></td>
						<td class="td-text"><a href="/admin/user-events/{{$userevent->id}}" > {{$userevent->user->name}}</a></td>
						<!-- <td class="text-right">
							<a href="/admin/user-events/{{$userevent->id}}" class="btn btn-success crud-btn btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View</a>	
							<a href="/admin/user-events/{{$userevent->id}}/edit" class="btn btn-info crud-btn btn-sm"><i class="fa fa-pencil btn-icon-space" aria-hidden="true"></i>Edit</a>

							<form action="/admin/user-events/{{$userevent->id}}" method="post" style="display:inline-block;" class="deleteform">
								<input name="_method" type="hidden" value="DELETE">
								<button class="btn btn-danger crud-btn btn-sm" type="submit"><i class="fa fa-trash btn-icon-space" aria-hidden="true"></i>Delete</button>
							</form>
						</td>
						-->
						</tr>
                    @endforeach
                    @else
		            		<tr>
		            			<td colspan="3">No results found.</td>
		            			<td></td>
		            			<td></td>
		            		</tr>
		            @endif
					</table>
				</div>
			</div>	
		</div>
	</div>
	<script>
		$(function(){
			$(".deleteform").submit(function(event){
					return confirm('Are you sure?');
			});
        });
	</script>
@endsection