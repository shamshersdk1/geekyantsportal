@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
   <div class="breadcrumb-wrap">
      <div class="row">
         <div class="col-sm-8">
            <h1>Edit Event</h1>
            <ol class="breadcrumb">
               <li><a href="/admin">Admin</a></li>
               <li><a href="/admin/events">Event</a></li>
               <li class="active">Edit</li>
            </ol>
         </div>
         <div class="col-sm-4">
         </div>
      </div>
   </div>
   @if(!empty($errors->all()))
   <div class="alert alert-danger">
      @foreach ($errors->all() as $error)
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <span>{{ $error }}</span><br/>
      @endforeach
   </div>
   @endif
   @if (session('message'))
   <div class="alert alert-success">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <span>{{ session('message') }}</span><br/>
   </div>
   @endif
   <div class="panel panel-default">
      <div class="panel-body">
      <form class="form-horizontal" method="post" action="/admin/events/{{$event->id}}" enctype="multipart/form-data">
        <input name="_method" type="hidden" value="PUT" />    
            <div class="form-group col-sm-6">
               <label for="name" class="col-sm-4  control-label">Name *:</label>
               <div class="col-sm-8">
               <input type="text" class="form-control" id="name" name="name" value="{{$event->name}}" placeholder="Enter Name" required> </input>
               </div>
            </div>
            <div class="form-group col-sm-6">
               <label for="name" class="col-sm-4  control-label">Image Url :</label>
               <div class="col-sm-8">
                  <input type="text" class="form-control" id="name" name="img_url" value="{{$event->img_url}}" placeholder="Enter Image Url"> </input>
               </div>
            </div>
            <div class="form-group col-sm-6" style="clear:left">
               <label for="name" class="col-sm-4  control-label">Title *:</label>
               <div class="col-sm-8">
                  <input type="text" class="form-control" id="name" name="title" value="{{$event->title}}" placeholder="Enter Title" required> </input>
               </div>
            </div>
            <div class="form-group col-sm-6">
               <label for="name" class="col-sm-4  control-label">Location *:</label>
               <div class="col-sm-8">
                  <input type="text" class="form-control" id="name" name="location" value="{{$event->location}}" placeholder="Enter Location" required/>
               </div>
            </div>
            <div class="form-group col-sm-6">
               <label  class="col-sm-4 control-label">Date* : </label>
               <div class="col-sm-8">
                  <div class='input-group date' id='purchase_date'>
                     <input type='text' id="sd" class="form-control" autocomplete="off" name="event_date" value="{{$event->date}}" required/>
                     <span class="input-group-addon">
                     <span class="glyphicon glyphicon-calendar"></span>
                     </span>
                  </div>
               </div>
            </div>
            <div class="form-group col-sm-6" >
                <div class="row">
                    <div class="form-group col-sm-4 control-label">
                        <label class="control-label">Assign User :</label>
                    </div>
                    <div class="form-group col-sm-8 control-label"  style="float:right">
                    <select class="add-developer" name="new_developers[]" multiple="multiple">
                        @if(!empty($available_users))
                            @foreach($available_users as $user)
                                @if($user->id)
                                    <option value="{{$user->id}}" selected="selected">
                                        @if($user->user)
                                            {{$user->user->name}}
                                        @endif
                                        </option>
                                @endif
                            @endforeach
                            @foreach($all_users as $user)
                                    <option value="{{$user->id}}" >{{$user->name}}</option>
                            @endforeach
                        @endif
                    </select>
                    </div>
                </div>
            </div>
            
            <div class="form-group col-sm-6" style="clear:left">
               <label class="col-sm-4 control-label"> Url :</label>
               <div class="col-sm-8">
                  <input type="text" class="form-control" id="google_drive_link" name="event_link" value="{{$event->url}}" placeholder="Enter Event Url"> </input>
               </div>
            </div>
            <div class="form-group col-sm-6">
               <label class="col-sm-4 control-label">Description :</label>
               <div class="col-sm-8">
                  <textarea class="form-control" id="description" name="description" rows="3" value="{{$event->description}}" placeholder="Enter Description"></textarea>
               </div>
            </div>
            <div class="form-group col-sm-6" style="clear:left">
               <label class="col-sm-4 control-label">Logo Url :</label>
               <div class="col-sm-8">
                  <input type="text" class="form-control" id="google_drive_link" name="logo_url" value="{{$event->logo_url}}" placeholder="Enter Logo Url"> </input>
               </div>
            </div>
            <div class="form-group col-sm-6">
               <label class="col-sm-4 control-label">Blog Url :</label>
               <div class="col-sm-8">
                  <input type="text" class="form-control" id="google_drive_link" name="blog_url" value="{{$event->blog_url}}" placeholder="Enter Blog Url"> </input>
               </div>
            </div>
            <div class="form-group col-sm-6" style="clear:left">
               <label class="col-sm-4 control-label">Video Link :</label>
               <div class="col-sm-8">
                  <input type="text" class="form-control" id="google_drive_link" name="video_link" value="{{$event->video_link}}" placeholder="Enter Video Link"> </input>
               </div>
            </div>
      </div>
   </div>
   <div class="text-center" style="margin-top: 30px;">
   <button type="submit" class="btn btn-primary btn-sm crude-btn "> Update</button>
   </div>
   </form>
</div>

<script>
$(document).ready(function() {
    $('.add-developer').select2(
        {"text": "label attribute"}
    );
});
</script>

<script type="text/javascript">
   $(function () {
       $('#purchase_date').datetimepicker({
       format: 'YYYY-MM-DD'
   });

   });
</script>
<script>
   $(function () {
       $('#datetimepickerStart').datetimepicker({
           format: 'YYYY-MM-DD'
       });

       $('#datetimepickerEnd').datetimepicker({
           format: 'YYYY-MM-DD'
       });
       if($("#datetimeinputstart").val()!="")
       {
           $('#datetimepickerEnd').data("DateTimePicker").minDate($("#datetimeinputstart").val());
       }

           $('.user_list').select2({
           placeholder: 'Select an option',
           allowClear:true
       });
   });
</script>
<script type="text/javascript">
    $(document).ready(function() {
            $('.module-tag').select2({
                maximumSelectionSize: 1,
                @if(!empty($makeList))
                tags: {{$makeList}},
                @endif
            });
            $('.module-tag2').select2({
                maximumSelectionSize: 1,
                @if(!empty($modelList))
                tags: {{$modelList}},
                @endif
            });
    });
</script>
@include('pages.admin.user-task.style')
@include('pages.admin.user-task.script')
@endsection
