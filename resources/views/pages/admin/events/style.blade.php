 <style>
    /* .data-table th, .data-table td {
        white-space: nowrap;
        text-align:center;
    }
    .static-col {
        width: 10em !important;
        background-color:#ddd;
        margin-left:-1px;
        vertical-align:middle !important;
        background-color: #92BFDF;
        font-weight: 900;
    }
   
    .table-wrapper {
        overflow-x: scroll;
        width: 100%;
        margin: 0 auto;
    }
    
    .table {
        margin-bottom:0;
    }

    .data-table textarea {
        margin-bottom:-5px;
        border-width:0;
    
        font-weight:500;
        border-bottom-width: 1px;
        border-bottom-color: #EEE;
    }

    .data-table td {
        padding:0 !important;
    }
    
   


    .data-table td.weekend > textarea.on-leave {
        width:4.4em !important;
    }
    .data-table td.holiday {
        background-color:#333;
    }
    .data-table textarea.free,.data-table textarea.working {
        background-color:#FBF2C8;
    }
    .data-table textarea.on-leave {
        background-color:#700;
        color:#FFF;
    }
    .data-table th {
        background-color: #C6E0F1;
    }--> */
    .error
    {
        width:11.5em;
        border: 5px solid #f00;
    }
    li
    {
        list-style-type: none;
    }
    .color-coding-div .unavailable
    {
        color:#d9534f;
    }
    .color-coding-div .available
    {
        color:#DFE8FB;
    }
    .color-coding-div
    {
        float:right;
        margin-right:15px;
    }
    .panel
    {
        margin-top:5px;
    }
    .select2-container {
        width: 75% !important;
    }
    .main-table td .weekend, .main-table th.weekend {
        width:4.4em !important;
        color:#FFF;
    }
    .main-table th.weekend {
        background-color:#d9534f;
    }
    .main-table th.today, .main-table td.today {
        background-color:#00C851 !important;
        color: #FFF;
    }
    .on-leave {
        color:#FFF;
        background-color:#d9534f;
    }
</style> 