@extends('layouts.admin-dashboard')
@section('main-content')
    <div class="container-fluid mis-report">
        <div class="breadcrumb-wrap">
           <div class="row">
              <div class="col-sm-8">
                 <h1 class="admin-page-title">MIS Leave Overview</h1>
                 <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="/admin/leave-section/mis-leave-report">MIS Leave </a></li>
                    <li class="active">{{$month->formatMonth()}}</li>
                 </ol>
              </div>
              <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
           </div>
        </div>
        <table class="table table-bordered table-hover bg-white">
          <tr>
              <th width="180px" class="text-left">Username</th>
              <th class="text-center">PL</th>
              <th class="text-center">SL</th>
              <th class="text-center">Other</th>
              <th class="text-center">LOP</th>
              <th class="text-center">Total</th>
              <th class="text-center">Remaining</th>
              <th class="text-center" width="180px">Action(s)</th>
          </tr>
          @if(($userLeaveDetail))
          @foreach($userLeaveDetail as $row)
          <tr>
           <td class="text-left">{{$row["user_name"]}}<br/><small>{{$row["employee_id"]}}</small> </div>
           <td class="text-center">{{$row["paid"]}}</td>
           <td class="text-center">{{$row["sick"]}}</td>
           <td class="text-center">{{$row["other"]}}
             @if($row["emergency"]==1)<br/> <span class="label label-primary">Emergency</span>@endif
             @if($row["marriage"]==1)<br/> <span class="label label-primary">Marriage</span>@endif
             @if($row["maternity"]==1)<br/> <span class="label label-primary">Maternity</span>@endif
             @if($row["paternity"]==1)<br/> <span class="label label-primary">Paternity</span>@endif
             @if($row["comp-off"]==1)<br/> <span class="label label-primary">Compensatory Off</span>@endif
            </td>
            <td class="text-center">{{$row["unpaid"]}}</td>
            <td class="text-center">{{$row["total"]}}</td>
            <td class="text-center">
             {{$row["availablePaidLeaves"]}}/{{$row["remaining_pl"]}} <div class="label label-info">PL</div><br/>
             {{$row["availableSickLeaves"]}}/{{$row["remaining_sl"]}} <div class="label label-warning">SL</div>
            </td>
            <td class="text-center">
             <a class="btn btn-sm btn-success" href="{{$month->id}}/{{$row['id']}}">
              <i class="fa fa-file-o"></i> &nbsp; View Report
             </a>
            </td>
          </tr>
          @endforeach
          @else
          <tr>
           <td class="text-center" colspan="8">No leaves found for this month</td>
          </tr>
          @endif
        </table>
    </div>
@endsection
