@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid mis-report">
  <div class="breadcrumb-wrap">
    <div class="row">
      <div class="col-sm-8">
        <h1 class="admin-page-title">{{$user->name}}'s Leave Report</h1>
        <ol class="breadcrumb">
          <li><a href="/admin">Admin</a></li>
          <li><a href="/admin/leave-section/mis-leave-report">MIS Leave </a></li>
          <li><a href="/admin/leave-section/mis-leave-report/{{$monthValid->id}}">{{$monthValid->formatMonth()}}</a></li>
          <li class="active">{{$monthValid->year}}</li>
        </ol>
      </div>
    </div>
  </div>
  <div class="row mis-overview" >
    <div class="col-sm-7">
      <div class="panel">
        <div class="panel-body">
          <!--left section data start-->

          @foreach($userLeaveDetail as $index => $monthRow)
          <div class="panel-group leave-mis" id="accordion_{{$index}}">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a class="accordion-toggle collapsed" data-toggle="collapse" data-target="#{{$index}}" href="#{{$index}}">
                    <div class="d-flex align-items-center justify-content-between">
                      <div class="d-flex align-items-center">
                        <span>{{$months[$index]->formatMonth()}}</span>
                      </div>
                      <div class="d-flex align-items-center ml-auto">
                        <span class="badge m-r-5">{{$monthRow["monthlyTotal"]["paid"]}} PL</span>
                        <span class="badge m-r-5">{{$monthRow["monthlyTotal"]["sick"]}} SL</span>
                      </div>

                      <i class="fa fa-chevron-down"></i>
                    </div>
                  </a>
                </h4>
              </div>
              <div id="{{$index}}" class="panel-collapse collapse">
                <div class="panel-body">
                  <table class="table table-bordered">
                    <tr>
                      <th>From</th>
                      <th>To</th>
                      <th>Type</th>
                      <th>Approved by</th>
                      <th>Approved date</th>
                    </tr>
                    @if($monthRow["yearlyView"])
                    @foreach($monthRow["yearlyView"] as $row)
                    <tr>
                      <td>{{date_in_view($row["from_date"])}}</td>
                      <td>{{date_in_view($row["to_date"])}}</td>
                      <td>{{$row["type"]}}</td>
                      <td>{{$row["approved_by"]}}</td>
                      <td>{{date_in_view($row["approved_date"])}}</td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                      <td class="text-center" colspan="5">No Leaves taken this Month</td>
                    </tr>
                    @endif
                  </table>
                </div>
              </div>
            </div>
          </div>
          @endforeach
          <!--left section data end-->
        </div>
      </div>
    </div>
    <div class="col-sm-5">
        <div class="panel panel-default " style="width:100%">
          <div class="panel-body" >
            <div class="d-flex justify-content-between align-items-center">
               <div class="text-center border-right"><small>Total Leave</small><h4>{{$allLeaveTotal}}</h4></div>
               <div class="text-center border-right"><small>Total PL</small><h4>{{$totalLeaveDetail["used_pl"]}}</h4></div>
               <div class="text-center border-right"><small>Total SL</small><h4>{{$totalLeaveDetail["used_sl"]}}</h4></div>
               <div class="text-center border-right"><small>Remaining PL </small><h4>{{$totalLeaveDetail["available_pl"]}}/{{$totalLeaveDetail["remaining_pl"]}}</h4></div>
               <div class="text-center border-right"><small>Remaining SL </small><h4>{{$totalLeaveDetail["available_sl"]}}/{{$totalLeaveDetail["remaining_sl"]}}</h4></div>
            </div>

          </div>
        </div>

      <table class="table table-striped bg-white">
        <thead>
          <tr>
            <th colspan="2">Upcoming Leaves</th>
            <th class="text-right">
            </th>
          </tr>
        </thead>
        <tbody>
        <tr>
          <th>From</th>
          <th>To</th>
          <th>No. of Days</th>
          <th class="text-right">Action</th>
        </tr>
        @if($upcomingLeaves)
        @foreach($upcomingLeaves as $leave)
        <tr>
          <td>{{date_in_view($leave->start_date)}}</td>
          <td>{{date_in_view($leave->end_date)}}</td>
          <td>{{$leave->days}}</td>
          <td class="text-right">
          <button disabled class="btn btn-info btn-sm" type="button" ><i class="fa fa-eye fa-fw"></i> View</button>
          </form>
          </td>
        </tr>
        @endforeach
        @else
          <tr>
            <td colspan="3">
            No upcoming leaves
            </td>
          </tr>
          @endif
        </tbody>
        </table>
      <table class="table table-striped bg-white">
        <thead>
          <tr>
            <th colspan="2">Leave Transactions</th>
            <th class="text-right">
            </th>
          </tr>
        </thead>
        <tbody>
        <tr>
          <th>Leave Type</th>
          <th>No. of Days</th>
          <th>Transaction for</th>
          <th>Transaction Date</th>
        </tr>
        @if($leaveCredit)
        @foreach($leaveCredit as $row)
        <tr>
          <td>
            @if($row->leaveType->code == 'paid' )
              <span class="label label-primary">{{$row->leaveType->title}}</span>
            @elseif($row->leaveType->code == 'sick' )
              <span class="label label-warning">{{$row->leaveType->title}}</span>
            @else
              <span class="label label-danger">{{$row->leaveType->title}}</span>
            @endif
          </td>
          <td>{{$row->days}}</td>

          @if($row->reference && $row->reference->calendarYear)
          <td>{{$row->reference->calendarYear->year}}</td>
          @else
          <td>??</td>
          @endif
          <!-- @if($row->reference_type=="App\Models\Leave\Leave")
          <td>Leave Taken</td>
          @elseif($row->reference_type=="App\Models\Leave\LeaveCalendarYear")
          <td>Yearly Credit (Pro Rata Basis)</td>
          @else
          <td>??</td>
          @endif -->
          <td>{{date_in_view($row->created_at)}}</td>
        </tr>
        @endforeach
        @else
          <tr>
            <td colspan="4">
            No leaves credited for this year
            </td>
          </tr>
          @endif
        </tbody>
        </table>
    </div>
  </div>
</div>
@endsection
