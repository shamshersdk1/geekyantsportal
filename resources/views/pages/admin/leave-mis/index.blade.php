@extends('layouts.admin-dashboard')
@section('main-content')
    <div class="container-fluid mis-report">
        <div class="breadcrumb-wrap">
           <div class="row">
              <div class="col-sm-8">
                 <h1 class="admin-page-title">MIS Leave Report</h1>
                 <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li class="active">MIS Leave </li>
                 </ol>
              </div>
           </div>
        </div>
        <table class="table table-bordered table-hover bg-white">
          <tr>
              <th width="120px" class="text-center">Month</th>
              <th width="120px" class="text-center">Action(s)</th>
          </tr>
          @if(count($months) == 0 )
            <tr>
               <td colspan="2">
                  No Record Found
               </td>
            </tr>

         @endif
         @if(isset($months))
            @foreach($months as $month)
               <tr>
                  <td class="text-center">{{$month->formatMonth()}}</div>
                  <td class="text-center">
                  <a class="btn btn-success" href="mis-leave-report/{{$month->id}}"><i class="fa fa-eye"></i> View</a>
                 </tr>
            @endforeach
         @endif

          <!-- <tr>
           <td class="text-center">March, 2019 </div>
           <td class="text-center">
            <a class="btn btn-success" href="mis-leave-report/overview"><i class="fa fa-eye"></i> View</a>
          </tr> -->
        </table>
    </div>
@endsection
