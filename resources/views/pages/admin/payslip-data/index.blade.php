@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Payslip Data</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
		  			<li class="active">Payslip Data</li>
        		</ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
	    	@if(!empty($errors->all()))
	            <div class="alert alert-danger">
	                @foreach ($errors->all() as $error)
	                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                    <span>{{ $error }}</span><br/>
	                  @endforeach
	            </div>
	        @endif
	        @if (session('message'))
	            <div class="alert alert-success">
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ session('message') }}</span><br/>
	            </div>
			@endif
		</div>
		<div class="col-md-12">
			<div class="panel ">
				<div class="panel-body">
					<div class="form-group col-md-12">
						<label for="Name" class="col-sm-3 col-md-2 control-label">Month:</label>
						<div class="col-sm-4 col-md-2" >
							<select class="form-control" name="payslip_data_month" id="payslip_data_month">
								@if ( count($payslipMonths) > 0 )
									@foreach( $payslipMonths as $payslipMonth )
										@if ( $selectedMonth->id == $payslipMonth->id )
											<option value={{$payslipMonth->id}} selected>{{month_year_in_view($payslipMonth->date)}}</option>
										@else
											<option value={{$payslipMonth->id}} >{{month_year_in_view($payslipMonth->date)}}</option>
										@endif
									@endforeach
								@endif
							</select>
						</div>
					</div>
					<div class="form-group col-md-12">
						<label for="Name" class="col-sm-3 col-md-2 control-label"></label>
						<div class="col-sm-4 col-md-2" >
							<button id="data-master" class="btn btn-success" onclick="show(this.id)">View Payslip Data Master</button>
						</div>
					</div>
					<div class="form-group col-md-12">
						<label for="Name" class="col-sm-3 col-md-2 control-label"></label>
						<div class="col-sm-4 col-md-2" >
							<button id="data-item" class="btn btn-success" onclick="show(this.id)">View Payslip Data Items</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
</div>
</div>
@endsection
@section('js')
@parent
<script>
	function show(id) {
		var selected_id = $("#payslip_data_month" ).val();
		var url = '/admin/payslip-data/'+id+'/'+selected_id;
		window.location.href = url;
		return false;
	}
</script>
@endsection
