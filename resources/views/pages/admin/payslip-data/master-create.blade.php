@extends('layouts.admin-dashboard')
@section('main-content')

<section class="new-project-section">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">Payslip Data</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
			  			<li><a href="{{ url('admin/payslip-data') }}">Payslip Data</a></li>
						<li class="active">Create</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>

	    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
		@endif
		<form class="form-horizontal" method="post" action="/admin/payslip-data/data-master" enctype="multipart/form-data">
			<input name="payslip_data_month_id" type="hidden" value={{$payslipMonth->id}} />
			<div class="user-list-view">
				<div class="panel panel-default">
					<table class="table table-striped">
						<thead>
							<tr>
								<th width="20%">User</th>
								<th width="10%">Payslip Month</th>
								<th width="35%">Variable Pay</th>
								<th width="35%">Annual Inhand</th>
							</tr>
						</thead>
						<tbody>
							@if ( count($users) > 0 )
								@foreach ( $users as $index => $user )
								<tr>
									<td>
										{{$user->name}}
									</td>
									<td>
										{{month_year_in_view($payslipMonth->date)}}
									</td>
									<td>
										<input type="text" id="variable_pay[{{$user->id}}]" name="variable_pay[{{$user->id}}]" class="form-control" placeholder="Variable Pay" value="{{ old('variable_pay[$user->id]') }}">
									</td>
									<td>
										<input type="text" id="annual_inhand[{{$user->id}}]" name="annual_inhand[{{$user->id}}]" class="form-control" placeholder="Annual Inhand" value="{{ old('annual_inhand[$user->id]') }}">
									</td>
								</tr>
								@endforeach
							@endif
						</tbody>
					</table>     
				</div>
			</div>
			<div class="text-center">
				<button type="submit" class="btn btn-success">Submit</button>
			</div>
		</form>
	</div>
</section>
@endsection
