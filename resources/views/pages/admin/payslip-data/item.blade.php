@extends('layouts.admin-dashboard')
@section('main-content')

<section class="new-project-section">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">Payslip Data</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
			  			<li><a href="{{ url('admin/payslip-data') }}">Payslip Data</a></li>
						<li class="active">Payslip Data Items</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>

	    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
		@endif
		<form class="form-horizontal" method="post" action="/admin/payslip-data/data-item" enctype="multipart/form-data">
			@if ( count($payslipDataMonth) > 0 )
				<input name="payslip_data_month_id" type="hidden" value={{$payslipDataMonth->id}} />
			@endif

			<div class="row leave-overview">
				<div class="col-md-12 m-t-15">
					@if($payslipDataMonth->is_locked)
						<div class="pull-right">
							<span class="label label-danger custom-label">Payslip locked</span>
						</div>
					@endif
					<div class="panel panel-default user-leave-table">
						<table class="table table-striped "  style="width:100%">
							<thead>
								<tr>
									<th>User</th>
									<th>Payslip Month</th>
									<th>Tech Talk Bonus</th>
									<th>Annual Bonus</th>
									<th>Confirmation Bonus</th>
									<th>Domestic Outstation</th>
									<th>Domestic Local</th>
									<th>Internation Onsite</th>
									<th>Extra Working Day</th>
									<th>National Holiday</th>
								</tr>
							</thead>
							<tbody>
								@if ( count($payslipMonthItemList) > 0 )
									@foreach ( $payslipMonthItemList as $index => $payslipMonthItem )
									<tr>
										<td>
											{{$payslipMonthItem->user->name}}
											<span class="small pull-right">( {{$payslipMonthItem->user->employee_id}} )</span>
										</td>
										<td>
											{{month_year_in_view($payslipMonthItem->payslipDataMonth->date)}}
										</td>
										<td>
											<input @if( $payslipDataMonth->is_locked) disabled @endif  type="text" id="tech_talk_bonus[{{$payslipMonthItem->id}}]" name="tech_talk_bonus[{{$payslipMonthItem->id}}]" class="form-control"  value="{{ $payslipMonthItem->tech_talk_bonus }}">
										</td>
										<td>
											<input @if( $payslipDataMonth->is_locked) disabled @endif  type="text" id="annual_bonus[{{$payslipMonthItem->id}}]" name="annual_bonus[{{$payslipMonthItem->id}}]" class="form-control" value="{{ $payslipMonthItem->annual_bonus }}">
										</td>
										<td>
											<input @if( $payslipDataMonth->is_locked) disabled @endif  type="text" id="confirmation_bonus[{{$payslipMonthItem->id}}]" name="confirmation_bonus[{{$payslipMonthItem->id}}]" class="form-control"  value="{{ $payslipMonthItem->confirmation_bonus }}">
										</td>
										<td>
											<input @if( $payslipDataMonth->is_locked) disabled @endif  type="text" id="domestic_onsite_outstation[{{$payslipMonthItem->id}}]" name="domestic_onsite_outstation[{{$payslipMonthItem->id}}]" class="form-control"  value="{{ $payslipMonthItem->domestic_onsite_outstation }}">
										</td>
										<td>
											<input @if( $payslipDataMonth->is_locked) disabled @endif  type="text" id="domestic_onsite_local[{{$payslipMonthItem->id}}]" name="domestic_onsite_local[{{$payslipMonthItem->id}}]" class="form-control"  value="{{ $payslipMonthItem->domestic_onsite_local }}">
										</td>
										<td>
											<input @if( $payslipDataMonth->is_locked) disabled @endif  type="text" id="international_onsite[{{$payslipMonthItem->id}}]" name="international_onsite[{{$payslipMonthItem->id}}]" class="form-control"  value="{{ $payslipMonthItem->international_onsite }}">
										</td>
										<td>
											<input @if( $payslipDataMonth->is_locked) disabled @endif  type="text" id="extra_working_day[{{$payslipMonthItem->id}}]" name="extra_working_day[{{$payslipMonthItem->id}}]" class="form-control"  value="{{ $payslipMonthItem->extra_working_day }}">
										</td>
										<td>
											<input @if( $payslipDataMonth->is_locked) disabled @endif  type="text" id="national_holiday_working_bonus[{{$payslipMonthItem->id}}]" name="national_holiday_working_bonus[{{$payslipMonthItem->id}}]" class="form-control"  value="{{ $payslipMonthItem->national_holiday_working_bonus }}">
										</td>
									</tr>
									@endforeach
								@endif
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="text-center">
				<button type="submit" class="btn btn-success">Submit</button>
			</div>
		</form>
	</div>
</section>
<script type="text/javascript">
    $(function(){
    $("#leave-overview").DataTable( {
        scrollX: true,
        scrollCollapse: true,
         paging:  false,
         fixedColumns:   {
            leftColumns: 2
        }
 	});
});
</script>
@endsection
