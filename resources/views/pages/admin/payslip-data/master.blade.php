@extends('layouts.admin-dashboard')
@section('main-content')

<section class="new-project-section">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">Payslip Data</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
			  			<li><a href="{{ url('admin/payslip-data') }}">Payslip Data</a></li>
						<li class="active">Payslip Data Master</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
				@if(!$payslipDataMonth->is_locked)
					<a href="/admin/payslip-data/data-master/{{$payslipDataMonth->id}}/lock" class="btn btn-danger">Lock Payslip</a>
				@endif
					<button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>

	    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
		@endif
		<form class="form-horizontal" method="post" action="/admin/payslip-data/data-master" enctype="multipart/form-data">
			@if ( count($payslipMonthMasterList) > 0 )
				<input name="payslip_data_month_id" type="hidden" value={{$payslipMonthMasterList[0]->id}} />
			@endif
			<div class="row leave-overview">
				<div class="col-md-12 m-t-15">
					@if($payslipDataMonth->is_locked)
						<div class="pull-right">
							<span class="label label-danger custom-label">Payslip locked</span>
						</div>
					@endif
					<div class="panel panel-default user-leave-table">
						<table class="table table-striped "  style="width:100%">
							<thead>
								<th width="20%">User</th>
								<th width="10%">Payslip Month</th>
								<th width="10%">Salary On Hold</th>
								<th style="background-color: #FFB6C1" width="30%">Variable Pay <span style="color: red">( {{month_year_in_view($lastMonth)}} )</span> </th>
								<th width="30%">Annual Inhand <span style="color: green">( {{month_year_in_view($currentMonth)}} )</span></th>
							</thead>
							<tbody>
								@if ( count($payslipMonthMasterList) > 0 )
								@foreach ( $payslipMonthMasterList as $index => $payslipMonthMaster )
								<tr>
									<td>
										{{$payslipMonthMaster->user->name}}
									<span class="small pull-right">( {{$payslipMonthMaster->user->employee_id}} )</span>
									</td>
									<td>
										{{month_year_in_view($payslipMonthMaster->payslipDataMonth->date)}}
									</td>
									<td>
										<input @if( $payslipDataMonth->is_locked) disabled @endif class="form-check-input" name="salary_on_hold[{{$payslipMonthMaster->id}}]" type="checkbox" value="{{$payslipMonthMaster->id}}" @if($payslipMonthMaster->salary_on_hold )checked @endif>
									</td>
									<td style="background-color: #FFB6C1">
										<input @if( $payslipDataMonth->is_locked) disabled @endif  type="number" defaultValue="0" id="variable_pay[{{$payslipMonthMaster->id}}]" name="variable_pay[{{$payslipMonthMaster->id}}]" class="form-control" value="{{ $payslipMonthMaster->variable_pay ? $payslipMonthMaster->variable_pay : 0 }}">
									</td>
									<td>
										<input @if( $payslipDataMonth->is_locked) disabled @endif  type="number" defaultValue="0" id="annual_inhand[{{$payslipMonthMaster->id}}]" name="annual_inhand[{{$payslipMonthMaster->id}}]" class="form-control" value="{{ $payslipMonthMaster->annual_in_hand ? $payslipMonthMaster->annual_in_hand : 0 }}">
									</td>
								</tr>
								@endforeach
								@endif
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="text-center">
				<button type="submit" class="btn btn-success">Submit</button>
			</div>
		</form>
	</div>
</section>
<script type="text/javascript">
    $(function(){
    $("#leave-overview").DataTable( {
        scrollX: true,
        scrollCollapse: true,
         paging:  false,
         fixedColumns:   {
            leftColumns: 2
        }
 	});
});
</script>
@endsection
