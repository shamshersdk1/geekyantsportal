@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 style="display:inline" class="admin-page-title">Payslip of @if(!empty($payslip->month)){{DateTime::createFromFormat('!m',$payslip->month)->format('F')}}@endif
                @if(!empty($payslip->year)){{$payslip->year}}@endif</h1><h5 style="display:inline" > (Uploaded by @if(!empty($payslip->user->name)){{$payslip->user->name}}@endif)</h5>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li><a href="{{ url('admin/upload-payslip') }}">List of Payslips</a></li>
        		  	<li><a >Payslip of @if(!empty($payslip->month)){{DateTime::createFromFormat('!m',$payslip->month)->format('F')}}@endif
				@if(!empty($payslip->year)){{$payslip->year}} &nbsp&nbsp @endif</a></li>
					@if($payslip->status=='approved')
					<span class="label label-success custom-label">{{ucfirst($payslip->status)}}</span>
					@elseif($payslip->status=='rejected')
					<span class="label label-danger custom-label">{{ucfirst($payslip->status)}}</span>
					@else
					<span class="label label-warning custom-label">{{ucfirst($payslip->status)}}</span>
					@endif
				</ol>
			</div>
			<div class="col-sm-4 text-right m-t-10">
				<a href='/admin/payslip/month/download/{{$payslip->id}}' class="btn btn-sm btn-primary"><i class="fa fa-download btn-icon-space" aria-hidden="true"></i>Download</a>
				@if($payslip->status=='pending')
				<a href='/admin/payslip/pending/approve/{{$payslip->id}}' class="btn btn-sm btn-success"><i class="fa fa-check btn-icon-space" aria-hidden="true"></i>Approve</a>
				<a href='/admin/payslip/pending/reject/{{$payslip->id}}' class="btn btn-sm btn-danger"><i class="fa fa-times btn-icon-space" aria-hidden="true"></i>Reject</a>
				@endif
				<button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
        </div>
    </div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
	<div class="panel panel-default">
		<table class="table table-striped ">
			<caption>No. of working days in this month : {{empty($payslip->payslipData[0]->working_day_month)?"Unavailable":$payslip->payslipData[0]->working_day_month}}</caption>
			<th>#</th>
			<th>Employee Id</th>
			<th>Name</th>
			<th>Designation</th>
			<th>No of Days Worked</th>
			<th>Gross Salary</th>
			<th>Loan</th>
			<th>Total Deduction</th>
			<th>Annual Bonus</th>
			<th>Qtr Bonus</th>
			<th>Performance Bonus</th>
			<th>Amount Payable</th>
			@if($payslip->status=='approved'||$payslip->status=='pending')
			<th class="text-right">Action</th>
			@endif
			@if(count($payslip->payslipData) > 0)
	    		@foreach($payslip->payslipData as $data)
					<tr
					@if(!empty($data->user)&&empty($data->user->confirmation_date))
						class="danger"
					@endif
					>
						<td class="td-text">
							{{$data->id}}
						</td>
						<td class="td-text">
							@if( !empty($data->user))
								{{$data->user->employee_id}}
							@endif
						</td>
						<td class="td-text">
							@if( !empty($data->user))
								{{$data->user->name}}
							@endif
						</td>
						<td class="td-text">
								{{$data->designation}}
						</td>
						<td class="td-text">
							{{$data->days_worked}}
						</td>
						<td class="td-text">
								{{$data->gross_salary}}
						</td>
						<td class="td-text">
								{{$data->loan}}
						</td>
						<td class="td-text">
								{{$data->total_deduction_month}}
						</td>
						<td class="td-text">
								{{$data->annual_bonus}}
						</td>
						<td class="td-text">
								{{$data->qtr_variable_bonus}}
						</td>
						<td class="td-text">
								{{$data->performance_bonus}}
						</td>
						<td class="td-text">
								{{$data->amount_payable}}
						</td>
						@if($payslip->status=='approved'||$payslip->status=='pending')
						<td class="td-text text-right">
								<a target="_blank" href="/admin/payslip/approved/view/{{$data->id}}" class="btn btn-info crud-btn btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View</a>	
								<!-- <a href='/admin/payslip/approved/download/{{$data->id}}' class="btn btn-sm btn-primary"><i class="fa fa-download btn-icon-space" aria-hidden="true"></i>Download</a> -->
						</td>
						@endif
					</tr>
				@endforeach
	    	@else
	    		<tr>
	    			<td colspan="3">No results found.</td>
	    			<td></td>
	    			<td></td>
	    		</tr>
	    	@endif
	   </table>
   
</div>
@endsection


@section('js')
	<script>
	</script>

@endsection