@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 style="display:inline" class="admin-page-title">Payslip of @if(!empty($payslip->month)){{DateTime::createFromFormat('!m',$payslip->month)->format('F')}}@endif
                @if(!empty($payslip->year)){{$payslip->year}}@endif</h1>
                <h5 style="display:inline"> 
                    (Uploaded by @if(!empty($payslip->user->name)){{$payslip->user->name}}@endif)
                </h5>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/upload-payslip') }}">List of Payslips</a></li>
                    <li><a >Payslip of @if(!empty($payslip->month)){{DateTime::createFromFormat('!m',$payslip->month)->format('F')}}@endif
                @if(!empty($payslip->year)){{$payslip->year}} &nbsp&nbsp @endif</a></li>
                    @if($payslip->status=='approved')
                    <span class="label label-success custom-label">{{ucfirst($payslip->status)}}</span>
                    @elseif($payslip->status=='rejected')
                    <span class="label label-danger custom-label">{{ucfirst($payslip->status)}}</span>
                    @else
                    <span class="label label-warning custom-label">{{ucfirst($payslip->status)}}</span>
                    @endif
                </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <a href='/admin/payslip/month/download/{{$payslip->id}}' class="btn btn-sm btn-primary"><i class="fa fa-download btn-icon-space" aria-hidden="true"></i>Download</a>
                @if($payslip->status=='pending')
                <a href='/admin/payslip/pending/reject/{{$payslip->id}}' class="btn btn-sm btn-danger"><i class="fa fa-times btn-icon-space" aria-hidden="true"></i>Reject</a>
                @endif
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                      @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif
        </div>
    </div>
    <div class="panel panel-default">
        <table class="table table-striped">
            <caption><span class="btn btn-sm btn-danger">The following record cannot be considered for the current month of salary processing.</span></caption>
            <th>Employee Id</th>
            <th>Name</th>
            <!-- <th>Designation</th> -->
            <th>No of Days Worked</th>
            <th>Gross Salary</th>
            <th>Loan</th>
            <th>Total Deduction</th>
            <th>Annual Bonus</th>
            <th>Qtr Bonus</th>
            <th>Performance Bonus</th>
            <th>Amount Payable</th>
            <th>Comment</th>
            <th>Status</th>
            @if(count($payslipData) > 0)
                @foreach($payslipData as $data)
                    @if( !empty($data['status']) && $data['status'] =='rejected')
                    <tr>
                        <td class="td-text">
                            @if( !empty($data['employee_id']))
                                {{$data['employee_id']}}
                            @endif
                        </td>
                        <td class="td-text">
                            @if( !empty($data['name']))
                                {{$data['name']}}
                            @endif
                        </td>
                        <td class="td-text">
                            @if( !empty($data['days_worked']))
                                {{$data['days_worked']}}
                            @endif
                            
                        </td>
                        <td class="td-text">
                            @if( !empty($data['gross_salary']))
                                {{$data['gross_salary']}}
                            @endif
                                
                        </td>
                        <td class="td-text">
                            @if( !empty($data['loan']))
                                {{$data['loan']}}
                            @endif
                                
                        </td>
                        <td class="td-text">
                            @if( !empty($data['total_deduction_month']))
                                {{$data['total_deduction_month']}}
                            @endif
                                
                        </td>
                        <td class="td-text">
                            @if( !empty($data['annual_bonus']))
                                {{$data['annual_bonus']}}
                            @endif
                                
                        </td>
                        <td class="td-text">
                            @if( !empty($data['qtr_variable_bonus']))
                                {{$data['qtr_variable_bonus']}}
                            @endif
                                
                        </td>
                        <td class="td-text">
                            @if( !empty($data['performance_bonus']))
                                {{$data['performance_bonus']}}
                            @endif
                                
                        </td>
                        <td class="td-text">
                            @if( !empty($data['amount_payable']))
                                {{$data['amount_payable']}}
                            @endif
                        </td>
                        <td>
                            @if( !empty($data['comment']))
                                {{$data['comment']}}
                            @endif
                        </td>
                        <td>
                            @if( !empty($data['status']) && $data['status'] == 'approved')
                                <span class="label label-success custom-label">{{ucfirst('approved')}}</span>
                            @elseif( !empty($data['status']) && $data['status'] == 'rejected')
                                <span class="label label-danger custom-label">{{ucfirst('rejected')}}</span>
                            @endif
                        </td>
                    </tr>
                    @endif
                @endforeach
            @else
                <tr>
                    <td colspan="3">No results found.</td>
                    <td></td>
                    <td></td>
                </tr>
            @endif
       </table>
   
</div>
<form method="POST" action="/admin/payslip/pending/approve/{{$payslip->id}}">
    <div class="container-fluid">
        <button class="pull pull-right btn btn-primary" style="margin-bottom: 18px; margin-right: -12px;" type="submit">Submit & Approve</button>
    </div>
        <div class="panel panel-default">
            <table class="table table-striped ">
                <caption><span><input id="select-all" type="checkbox"/> <b> &nbsp; Select All</b></span><span class="pull pull-right">No. of working days in this month : {{empty($payslip->working_day_month)?"Unavailable":$payslip->working_day_month}}
                </span>
                </caption>
                <th></th>
                <th>Employee Id</th>
                <th>Name</th>
                <!-- <th>Designation</th> -->
                <th>No of Days Worked</th>
                <th>Gross Salary</th>
                <th>Loan</th>
                <th>Total Deduction</th>
                <th>Annual Bonus</th>
                <th>Qtr Bonus</th>
                <th>Performance Bonus</th>
                <th>Amount Payable</th>
                <th>Comment</th>
                <th>Status</th>
                @if(count($payslipData) > 0)
                    @foreach($payslipData as $data)
                        @if( !empty($data['status']) && $data['status'] != 'rejected')
                        <tr>
                            <td class="td-text">
                                <input name="employeeIds[]" class="employee-rec" value="{{$data['employee_id']}}" type="checkbox" name="">
                            </td>
                            <td class="td-text">
                                @if( !empty($data['employee_id']))
                                    {{$data['employee_id']}}
                                @endif
                            </td>
                            <td class="td-text">
                                @if( !empty($data['name']))
                                    {{$data['name']}}
                                @endif
                            </td>
                            <td class="td-text">
                                @if( !empty($data['days_worked']))
                                    {{$data['days_worked']}}
                                @endif
                                
                            </td>
                            <td class="td-text">
                                @if( !empty($data['gross_salary']))
                                    {{$data['gross_salary']}}
                                @endif
                                    
                            </td>
                            <td class="td-text">
                                @if( !empty($data['loan']))
                                    {{$data['loan']}}
                                @endif
                                    
                            </td>
                            <td class="td-text">
                                @if( !empty($data['total_deduction_month']))
                                    {{$data['total_deduction_month']}}
                                @endif
                                    
                            </td>
                            <td class="td-text">
                                @if( !empty($data['annual_bonus']))
                                    {{$data['annual_bonus']}}
                                @endif
                                    
                            </td>
                            <td class="td-text">
                                @if( !empty($data['qtr_variable_bonus']))
                                    {{$data['qtr_variable_bonus']}}
                                @endif
                                    
                            </td>
                            <td class="td-text">
                                @if( !empty($data['performance_bonus']))
                                    {{$data['performance_bonus']}}
                                @endif
                                    
                            </td>
                            <td class="td-text">
                                @if( !empty($data['amount_payable']))
                                    {{$data['amount_payable']}}
                                @endif
                            </td>
                            <td>
                                @if( !empty($data['comment']))
                                    {{$data['comment']}}
                                @endif
                            </td>
                            <td>
                                @if( !empty($data['status']) && $data['status'] == 'approved')
                                    <span class="label label-success custom-label">{{ucfirst('approved')}}</span>
                                @elseif( !empty($data['status']) && $data['status'] == 'inactive')
                                    <span class="label label-warning custom-label">{{ucfirst('inactive')}}</span>
                                @endif
                            </td>
                        </tr>
                        @endif
                    @endforeach
                @else
                    <tr>
                        <td colspan="3">No results found.</td>
                        <td></td>
                        <td></td>
                    </tr>
                @endif
           </table>
        </div>
</form>
@endsection


@section('js')
    <script>
        $(function() {
            $( "#select-all" ).click(function() {
                $('input:checkbox').not(this).prop('checked', this.checked);
            });
            $( ".employee-rec" ).click(function() {
                $( "#select-all" ).prop('checked', false);
            });
        });
    </script>

@endsection