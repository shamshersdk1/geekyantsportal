@extends('layouts.admin-dashboard')
@section('main-content')
    <section class="sprint-dashboard">
        <div class="container-fluid">
            <div class="breadcrumb-wrap">
                <div class="row">
                    <div class="col-sm-8">
                        <h1 class="admin-page-title">Payslip </h1>
                        <ol class="breadcrumb">
                            <li><a href="/admin">Admin</a></li>
                            <li ><a href="/admin/upload-payslip">List of Payslips</a></li>
                            <li class="active">Uplaod</li>
                        </ol>
                    </div>
                </div>
            </div>
            @if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                      @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="panel ">
                        <div class="panel-body">
                            <form action = "/admin/upload-payslip" method="post" role="form" enctype="multipart/form-data" id="payslipForm" class="add-product-form form-horizontal">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group row">
                                    <label for="Name" class="col-sm-3 col-md-2 control-label">Month:</label>
                                    <div class="col-sm-3 col-md-2" >
                                            <select class="form-control" name="month" id="month">
                                                <option value="1">January</option>
                                                <option value="2">Feburary</option>
                                                <option value="3">March</option>
                                                <option value="4">April</option>
                                                <option value="5">May</option>
                                                <option value="6">June</option>
                                                <option value="7">July</option>
                                                <option value="8">August</option>
                                                <option value="9">September</option>
                                                <option value="10">October</option>
                                                <option value="11">November</option>
                                                <option value="12">December</option>
                                            </select>
                                    </div>
                                </div>
                            <div class="form-group row">
                            <label for="Name" class="col-sm-3 col-md-2 control-label">Year:</label>
                            <div class="col-sm-3 col-md-2" >
                            <select class="form-control" name="year" id="year"></select>
                            </div>
                            </div>
                            <div class="form-group row">
                            <label for="Name" class="col-sm-3 col-md-2 control-label">Csv file:</label>
                            <div class="col-sm-9">
                            <input type="file" class="form-control" name="csv_file" placeholder="Csv file" required value="{{ old('csv_file') }}">
                            </div>
                            </div>
                            <div class="form-group row">
                                <label for="Name" class="col-sm-3 col-md-2 control-label">Template:</label>
                                <div class="col-sm-3 col-md-2" >
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="template"  value="3-aug-2018" checked>
                                        <label class="form-check-label" for="exampleRadios1">
                                            August 2018
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="template"  value="2-apr-2018">
                                        <label class="form-check-label" for="exampleRadios1">
                                            April 2018
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="template" value="1-jan-2018">
                                        <label class="form-check-label" for="exampleRadios2">
                                            January 2018
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                            <div class="col-sm-12 pull-right">
                            <button type="submit" class="btn btn-success pull-right" id="save">Save</button>
                            </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('js')
<script>
    $(document).ready(function() {

        $('#payslipForm').on('submit', function(e){
                var month = $('#month').val();
                if ( Math.floor(month) != month || !$.isNumeric(month)) {

                    alert('Please fill a integre value to month');
                    e.preventDefault();
                    return false;
                }

                var year = $('#year').val();
                if ( Math.floor(year) != year || !$.isNumeric(year)) {

                    alert('Please fill a integre value to year');
                    e.preventDefault();
                    return false;
                }

                var csvFile = $('#csvFile').val(); 
                if(csvFile=='') 
                { 
                    alert("Please enter a csv file");
                    e.preventDefault();
                    return false;
                }

            });

        $date= new Date();
        $month=$date.getMonth()+1;
        $year=$date.getFullYear();
        $max=$date.getFullYear()+1;
        $options="";
        for($y = 2017 ; $y <=$max; $y++){
            $options += "<option value='"+$y+"'>"+ $y +"</option>";
        }
        $("#year").html($options);
        $('#month option[value="'+$month+'"]').attr("selected","true");
        $('#year option[value="'+($max-1)+'"]').attr("selected","true");
        // getPayslip = function(element) {
        //     var yearSelected = $('#yearSelected').find(":selected").text();
        //     console.log(yearSelected);
        //     url = '/admin/my-payslip/'+$(element).text()+'/'+yearSelected;
        //     console.log(url);
        //     window.open(url, "_self");    
        // }
    });
</script>

@endsection