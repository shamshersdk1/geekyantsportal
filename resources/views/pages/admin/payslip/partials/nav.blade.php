<div class="row" style="margin-bottom:1%">
    <div class="col-md-12">
        <ul class="btn-group" style="padding:0">
            <a href="/admin/upload-payslip" type="button" class="btn btn-default @if(Request::is('admin/upload-payslip'))btn-success @endif">All</a>
            <a href="/admin/payslip/approved" type="button" class="btn btn-default @if(Request::is('admin/payslip/approved'))btn-success @endif">Approved</a>
            <a href="/admin/payslip/pending" type="button" class="btn btn-default @if(Request::is('admin/payslip/pending'))btn-success @endif">Pending</a>
            <a href="/admin/payslip/rejected" type="button" class="btn btn-default @if(Request::is('admin/payslip/rejected'))btn-success @endif">Rejected</a>
        </ul>
    </div>
</div>