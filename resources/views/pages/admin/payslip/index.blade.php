@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">List of Payslips</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li><a href="{{ url('admin/upload-payslip') }}">List of Payslips</a></li>
        		</ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <a href="/admin/upload-payslip/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Upload payslip</a>
            </div>
        </div>
	</div>
	@include('pages.admin.payslip.partials.nav')
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
	<div class="user-list-view">
	<div class="panel panel-default">
		<table class="table table-striped">
			<th>Month</th>
			<th>Year</th>
			<th>Uploaded by</th>
			<th>Approved by</th>
			<th>Template</th>
			<th>Status</th>
			<th class="text-right">Actions</th>
			@if(count($payslips) > 0)
	    		@foreach($payslips as $payslip)
					<tr>
						<td class="td-text">
							{{DateTime::createFromFormat('!m',$payslip->month)->format('F')}}
						</td>
						<td class="td-text">
							{{$payslip->year}}
						</td>
						<td class="td-text">
							@if( !empty($payslip->user))
								{{$payslip->user->name}}
							@endif
						</td>
						<td class="td-text">
							@if( !empty($payslip->approver))
								{{$payslip->approver->name}}
							@endif
						</td>
						<td class="td-text">
							{{$payslip->template ? strtoupper($payslip->template) : 'JAN-2018'}}
						</td>
						<td class="td-text">
							@if($payslip->status=='approved')
							<span class="label label-success custom-label">{{ucfirst($payslip->status)}}</span>
							@elseif($payslip->status=='processing')
							<span class="label label-primary custom-label">{{ucfirst($payslip->status)}}</span>
							@elseif($payslip->status=='rejected')
							<span class="label label-danger custom-label">{{ucfirst($payslip->status)}}</span>
							@else
							<span class="label label-warning custom-label">{{ucfirst($payslip->status)}}</span>
							@endif
						</td>
						<td class="td-text text-right">
							@if( $payslip->user_id == $user->id || $user->isAdmin())
								@if($payslip->status != 'processing')
								<a href="/admin/payslip/{{$payslip->id}}" class="btn btn-info crud-btn btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View</a>	
								@endif
									@if($payslip->status=='pending')
									<form id="deleteForm" style="display:inline" method="post">
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
										<input name="_method" type="hidden" value="DELETE" >
										<button type = "button" class = "btn btn-danger btn-sm" onclick="deletePayslip({{$payslip->id}});" 
										data-toggle="tooltip" data-placement="top" 
										title="Delete!">
											<i class="fa fa-trash"></i> &nbspDelete
										</button>
									</form>
									@endif
							@else
								Not authorised
							@endif
						</td>
					</tr>
				@endforeach
	    	@else
	    		<tr>
	    			<td colspan="3">No results found.</td>
	    			<td></td>
	    			<td></td>
	    		</tr>
	    	@endif
	   </table>
   </div>
</div>
@endsection


@section('js')
	<script>
		function deletePayslip($id) {
			var x = confirm("Are you sure you want to delete?");
			if (x === true) {
				$('#deleteForm').attr('action', '/admin/payslip/'+$id);
				$('#deleteForm').submit();
				return;
			}
		}
	</script>

@endsection