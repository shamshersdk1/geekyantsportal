@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">List of Approved Payslip of month @if(!empty($payslip->month)){{$payslip->month}}@endif
                year @if(!empty($payslip->year)){{$payslip->year}}@endif</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li><a href="{{ url('admin/payslip/pending') }}">List of Approved Payslip</a></li>
        		  	<li><a >View</a></li>
        		</ol>
            </div>
        </div>
    </div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
	<div class="panel panel-default">
		<table class="table table-striped">
			<th>#</th>
			<th>Employee Id</th>
			<th>Email</th>
			@if(count($payslip->payslipData) > 0)
	    		@foreach($payslip->payslipData as $data)
					<tr>
						<td class="td-text">
							{{$data->id}}
						</td>
						<td class="td-text">
							@if( !empty($data->user))
								{{$data->user->employee_id}}
							@endif
						</td>
						<td class="td-text">
							{{$data->email}}
						</td>
						<td class="td-text">
							<a href='/admin/payslip/approved/view/{{$data->id}}' class="btn btn-sm btn-primary">View</a>
							<a href='/admin/payslip/approved/download/{{$data->id}}' class="btn btn-sm btn-danger" target="_blank">Download</a>
						</td>
					</tr>
				@endforeach
	    	@else
	    		<tr>
	    			<td colspan="3">No results found.</td>
	    			<td></td>
	    			<td></td>
	    		</tr>
	    	@endif
	   </table>
   
</div>
@endsection


@section('js')
	<script>
	</script>

@endsection