@extends('layouts.admin-dashboard')
@section('main-content')
    <section class="sprint-dashboard">
        <div class="container-fluid">
            <div class="breadcrumb-wrap">
                <div class="row">
                    <div class="col-sm-8">
                        <h1 class="admin-page-title">Payslip </h1>
                        <ol class="breadcrumb">
                            <li><a href="/admin">Admin</a></li>
                            <li class="active">Payslip</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-3 col-md-2">
                    <div class="m-b">
                        <label>Year of</label>
                        <select class="form-control" id="yearSelected">
                            <option selected>2016</option>
                            <option>2015</option>
                            <option>2014</option>
                            <option>2013</option>
                        </select>
                    </div>
                    <div class="list-group">
                      <a onClick="getPayslip(this)" class="list-group-item">Dec</a>
                      <a onClick="getPayslip(this)" class="list-group-item ">Nov</a>
                      <a onClick="getPayslip(this)" class="list-group-item">Oct</a>
                      <a onClick="getPayslip(this)" class="list-group-item">Sep</a>
                      <a onClick="getPayslip(this)" class="list-group-item" >Aug</a>
                      <a onClick="getPayslip(this)" class="list-group-item" >July</a>
                      <a onClick="getPayslip(this)" class="list-group-item">June</a>
                      <a onClick="getPayslip(this)" class="list-group-item">May</a>
                      <a onClick="getPayslip(this)" class="list-group-item">Apr</a>
                      <a onClick="getPayslip(this)" class="list-group-item">Mar</a>
                      <a onClick="getPayslip(this)" class="list-group-item">Feb</a>
                      <a onClick="getPayslip(this)" class="list-group-item">Jan</a>
                    </div>
                </div>
                @if(isset($userPayslip['payslips']) && count($userPayslip['payslips']) > 0 )
                    <div class="col-xs-9 col-md-10">
                        <div class="payslip-wrap panel panel-default">
                            <div class="panel-body">
                                <table width="100%">
                                    <tr>
                                        <td class="payslip-info" width="70%">
                                            <h1><b>Payslip</b> {{$userPayslip['payslips']['payroll_month']}}</h1>
                                        </td>
                                        <td class="text-right" width="30%" style="vertical-align: top;">
                                            <img src="/images/logo-dark.png" width="150" />
                                        </td>
                                    </tr>
                                </table>

                                <h2 class="m-t">{{$userPayslip['payslips']['employee_name']}}</h2>
                                <table width="100%" class="payslip-table">
                                    <tr>
                                        <th>Emp no</th>
                                        <th>Date Joined</th>
                                        <th>Department</th>
                                        <th>Designation</th>
                                    </tr>
                                    <tr>
                                        <td>{{$userPayslip['payslips']['payroll_month']}}</td>
                                        <td>{{$userPayslip['payslips']['payroll_month']}}</td>
                                        <td>{{$userPayslip['payslips']['payroll_month']}}</td>
                                        <td>Software Engineer</td>
                                    </tr>
                                    <tr>
                                        <th>Payment Mode</th>
                                        <th>Bank</th>
                                        <th>Bank IFSC</th>
                                        <th>bank Account</th>
                                    </tr>
                                    <tr>
                                        <td>Bank Transfer</td>
                                        <td>Kotak Mahindra Bank Limited</td>
                                        <td>KKBK0000421</td>
                                        <td>123456789</td>
                                    </tr>
                                    <tr>
                                        <th>Pan</th>
                                        <th>uan</th>
                                        <th colspan="2">PF number</th>
                                    </tr>
                                    <tr>
                                        <td>B1013131</td>
                                        <td>3232423432</td>
                                        <td>43243242342</td>
                                    </tr>
                                </table>

                                <h3 class="m-t">Salary Breakdown</h3>
                                <table width="100%" class="payslip-table">
                                    <tr>
                                        <th>Actual Payable days</th>
                                        <th>Total Working Days</th>
                                        <th>Loss of Pay Days</th>
                                        <th>Days Payable</th>
                                    </tr>
                                    <tr>
                                        <td>{{$userPayslip['payslips']['actual_payable_days']}}</td>
                                        <td>{{$userPayslip['payslips']['working_days']}}</td>
                                        <td>{{$userPayslip['payslips']['loss_of_pay_days']}}</td>
                                        <td>{{$userPayslip['payslips']['days_payable']}}</td>
                                    </tr>
                                </table>

                                <table width="100%" class="payslip-table " style="margin-bottom: 0;">
                                    <tr>
                                        <td style="vertical-align: top; padding-bottom: 0;" width="50%">
                                            <table width="100%" class="payslip-table table table-bordered">
                                                <tr> 
                                                    <td colspan="2" class="text-center"><h3>Earnings</h3></td>
                                                </tr>
                                                <tr>
                                                    <td>Basic </td>
                                                    <td width="150" class="text-right">{{$userPayslip['payslips']['basic']}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Dearness Allowance</td>
                                                    <td class="text-right">{{$userPayslip['payslips']['transport_allowance']}}</td>
                                                </tr>
                                                <tr>
                                                    <td>House Rent Allowance</td>
                                                    <td class="text-right">{{$userPayslip['payslips']['hra']}}</td>
                                                </tr>
                                                <tr>
                                                     <td>Conveyance Allowance</td>
                                                    <td class="text-right">{{$userPayslip['payslips']['transport_allowance']}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Medical Allowance </td>
                                                    <td class="text-right">{{$userPayslip['payslips']['special_medical_insurance']}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Car Allowance</td>
                                                    <td class="text-right">{{$userPayslip['payslips']['car_allowance']}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Telephone Allowance</td>
                                                    <td class="text-right">{{$userPayslip['payslips']['telephone_allowance'] or '-'}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Food Allowance</td>
                                                    <td class="text-right">{{$userPayslip['payslips']['food_allowance']}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Special Allowance</td>
                                                    <td class="text-right">{{$userPayslip['payslips']['special_allowance']}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Performance Bonus</td>
                                                    <td class="text-right">{{$userPayslip['payslips']['performace_bonus'] or '-'}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Annual Bonus</td>
                                                    <td class="text-right">{{$userPayslip['payslips']['annual_bonus']}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Gross Earnings</td>
                                                    <td class="text-right">{{$userPayslip['payslips']['gross_earning']}}</td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td style="vertical-align: top; padding-bottom: 0;">
                                            <table width="100%" class="payslip-table table table-bordered no-border-left">
                                                <tr>
                                                    <td colspan="2" class="text-center"><h3>  Deductions</h3></td>
                                                </tr>
                                                <tr>
                                                    <td>T.D.S. </td>
                                                    <td width="150" class="text-right">{{$userPayslip['payslips']['total_income_tax']}}</td>
                                                </tr>
                                                <tr>
                                                    <td>P.T.</td>
                                                    <td class="text-right">{{$userPayslip['payslips']['professional_tax']}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Food Deduction</td>
                                                    <td class="text-right">{{$userPayslip['payslips']['food_allowance']}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Medical Insurance</td>
                                                    <td class="text-right">{{$userPayslip['payslips']['special_medical_insurance']}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Employee’s P.F.</td>
                                                    <td class="text-right">{{$userPayslip['payslips']['pf_employee']}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Employee’s E.S.I.</td>
                                                    <td class="text-right">0</td>
                                                </tr>
                                                <tr>
                                                    <td>Loan Deduction</td>
                                                    <td class="text-right">{{$userPayslip['payslips']['loan_emi'] or '-'}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Other Deduction</td>
                                                    <td class="text-right">-</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>Total Deductions</td>
                                                    <td class="text-right">{{$userPayslip['payslips']['total_deduction_payslip']}}</td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <table class="table table-bordered payslip-table">
                                    <tr>
                                        <td >Net Salary Payable</td>
                                        <td width="150" class="text-right"><b>{{$userPayslip['payslips']['total_net_payde']}}</b></td>
                                    </tr>
                                    <tr>
                                        <td>Net Salary Paid via Online Transfer</td>
                                        <td class="text-right"><b>{{$userPayslip['payslips']['net_payd_a_b_c']}}</b></td>
                                    </tr>
                                </table>

                                <div class="text-center m-t">
                                    <p><small>SAHU SOFT INDIA PRIVATE LIMITED, </small>
                                    <small class="text-muted">Reg no. U72200BR2006PTC011902

                                    <br />Regd. Office: Amgola Road, Muzaffarpur ,P.S: Kaji Mohammadpur, Bihar 842002 India

                                    <br />Corp. Office: #18, 2nd Cross, BTM Layout, NS Palya, BTM 2nd stage, Bangalore - 560076, Karnataka, India</small></p>
                                    
                                    <table width="100%" style="margin-top: 20px;">
                                        <tr>
                                            <td width="33%">+91 80 409 74929</td>
                                            <td width="33%"><a href="mailto:info@geekyants.com">info@geekyants.com</a></td>
                                            <td width="33%"><a href="http://sahusoft.com/" target="_blank">www.sahusoft.com</a></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="text-center"> No payslip of this month is added. Choose another month </div>
                @endif
            </div>
        </div>
    </section>
@endsection
@section('js')
<script>
    $(document).ready(function() {
        getPayslip = function(element) {
            var yearSelected = $('#yearSelected').find(":selected").text();
            console.log(yearSelected);
            url = '/admin/my-payslip/'+$(element).text()+'/'+yearSelected;
            console.log(url);
            window.open(url, "_self");    
        }
    });
</script>

@endsection