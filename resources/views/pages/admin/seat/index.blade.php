@extends('layouts.admin-dashboard')

@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Seat</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li><a href="{{ url('admin/seats') }}">List of Floors</a></li>
        		</ol>
            </div>
		</div>
	</div>
	<div class="panel panel-default noradius">
		<table class="table table-striped">
			<th width="20%" >Floor</th>
			<th width="20%" >Total Seats</th>
			<th width="20%" >Allocated Seats</th>
			<th width="20%" >Empty Seats</th>
			<th width="20%" class="text-right" style="padding-right: 50px">Actions</th>
			@if(count($floors) > 0)
	    		@foreach($floors as $floor)
					<tr>
						<td >
							{{ $floor['floor'] }} 
						</td>
						<td >
							<span class="label label-info custom-label">{{ $floor['details']['total_seats'] }}</span>
						</td>
						<td >
							<span class="label label-danger custom-label">{{ $floor['details']['occupied_seats'] }}</span>
						</td>
						<td>
							<span class="label label-success custom-label">{{ $floor['details']['empty_seats'] }} </span>
						</td>
						<td class="text-right">
							<a href="/admin/seats/{{$floor['floor']}}/show" class="btn btn-success crud-btn btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View Seat Plan</a>
						</td>
					</tr>
				@endforeach
	    	@else
	    		<tr>
	    			<td colspan="3">No results found.</td>
	    			<td></td>
	    			<td></td>
	    		</tr>
	    	@endif
	   </table>
	</div>
</div>	
@endsection

