@extends('layouts.admin-dashboard')
@section('main-content')
<script type="text/javascript">
    window.seats = {{ $seats }}
	window.users = {{ $users }}
</script>
<!--  -->   
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Seat</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li><a href="{{ url('admin/seats') }}">List of Floors</a></li>
                    <li>Floor {{ $floor }}</a></li>
        		</ol>
            </div>
            <div "col-sm-4">
                <div>Empty Seats: {{ $allocation_details['empty_seats'] }} </div>
                <div>Occupied Seats: {{ $allocation_details['occupied_seats'] }} </div>
            </div>
        </div>
    </div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
 <!--  -->   
    <div ng-app="myApp" ng-controller="seatCtrl" class="row" style="margin-left: 1px; margin-right: 1px; margin-top:15px;">
        <div ng-repeat="seat in seats track by $index" class="col-md-2 col-sm-2">
            <div ng-click="editSeat(seat)" ng-if="$index%12 < 6 " style="border-bottom: 1px dashed"  >
                <div class="td-text-center">
                  <div ng-if="seat.user.name" style="display: inline-block; position: relative; width: 45px; height: 45px;">
                        <img src="%%seat.user_profile.image%%" style="position: absolute; top: 2px; left: 2px; width: 42px; height: 42px;" />
                  </div>
                  <div ng-if="!seat.user.name" style="display: inline-block; position: relative; width: 45px; height: 45px;">
                        <i class="fa fa-user" style="position: absolute; top: 12px; left: 2px; width: 42px; height: 42px; font-size: 20px; color: green "></i>
                  </div>
                </div>
                <div ng-if="seat.user.name" class="td-text-center" style="font-size: 10px">
                    %% seat.user.name %%
                </div>
                <div ng-if="!seat.user.name" class="td-text-center" style="font-size: 10px">
                     -
                </div>  
                <div class="td-text-center">
                    %% seat.name %%
                </div> 
            </div>

            <div ng-click="editSeat(seat)" ng-if="$index%12 >= 6" style="margin-bottom: 80px" >
                <div class="td-text-center">
                    <div ng-if="seat.user.name" style="display: inline-block; position: relative; width: 45px; height: 45px;">
                        <img src="%%seat.user_profile.image%%" style="position: absolute; top: 2px; left: 2px; width: 42px; height: 42px;" />
                    </div>
                    <div ng-if="!seat.user.name" style="display: inline-block; position: relative; width: 45px; height: 45px;">
                        <i class="fa fa-user" style="position: absolute; top: 12px; left: 2px; width: 42px; height: 42px; font-size: 20px; color: green "></i>
                    </div>
                  </div>
                <div ng-if="seat.user.name" class="td-text-center" style="font-size: 10px">
                    %% seat.user.name %%
                </div>
                <div ng-if="!seat.user.name" class="td-text-center" style="font-size: 10px">
                    -
                </div>
                <div class="td-text-center">
                    %% seat.name %%
                </div> 
            </div>
            
        </div>
    </div>
</div>


@endsection

