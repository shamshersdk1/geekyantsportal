@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">List of Calendar Events</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/calender-event') }}">Calendar Events</a></li>
                </ol>
            </div>
            
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
        	@if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                      @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif
            <div class="user-list-view">
                <div class="panel panel-default">
                    <table class="display table table-striped">
                        <thead>
                            <tr>
                                <th width="5%">#</th>
                                <th>Event Name</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th>Created At</th>
                                <th>Reference Type</th>
                                <th>Reference Id</th>
                                <th>Status</th>
                                <!-- <th class="sorting text-right" width="35%">Action</th> -->
                            </tr>
                        </thead>
                        @foreach($data as $event)
                            <tr>
                                <td>{{$event->id}}</td>
                                <td>{{$event->name}}</td>
                                <td>{{datetime_in_view($event->start_date_time)}}</td>
                                <td>{{datetime_in_view($event->end_date_time)}}</td>
                                <td>{{datetime_in_view($event->created_at)}}</td>
                                <td>{{$event->reference_type}}</td>
                                <td>{{$event->reference_id}}</td>
                                <td>
                                    @if($event->status =='confirmed')
                                        <span class="label label-primary custom-label">{{ucfirst($event->status)}}</span>
                                    @elseif($event->status =='tentative')
                                        <span class="label label-default custom-label">{{ucfirst($event->status)}}</span>
                                    @elseif($event->status =='cancelled')
                                        <span class="label label-danger custom-label">{{ucfirst($event->status)}}</span>
                                    @endif    
                                </td>
                            </tr>
                        @endforeach
                    </table>
                    <div class="col-md-12">
                        <div class="pageination pull-right">
                            <nav aria-label="Page navigation">
                                <ul class="pagination">
                                    {{ $data->render() }}
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <a class="btn btn-success crud-btn btn-lg add-newproj-btn" onclick="window.location='{{ url("admin/cms-project/create") }}'"><i class="fa fa-plus btn-icon-space" aria-hidden="true"></i>Add new Project</a> -->
        </div>
    </div>
</div>
@endsection
@section('js')
@parent
	<script>
		
		function deletetech() {
			var x = confirm("Are you sure you want to delete?");
			if (x)
				return true;
			else
				return false;
		}
	</script>
@endsection