@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid"  ng-app="myApp" ng-controller="purchaseOrderCtrl">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Purchase Orders</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
		  			<li class="active">Purchase Orders</li>
        		</ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
				<a href="#" ng-click="createPurchaseOrder()" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Add Purchase Order</a>
            </div>
        </div>
    </div>
	<div class="row">
         <div class="col-sm-9">
         	@if(!empty($errors->all()))
	         	<div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
			            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			            <span>{{ $error }}</span><br/>
		            @endforeach
	         	</div>
	        @endif
            <div class="row">
               <div class="col-sm-12 m-b clearfix">
               		<button class="btn btn-sm btn-success"><i class="fa fa-check"></i> Approve</button>
                    <button class="btn btn-sm btn-danger"><i class="fa fa-times"></i> Reject</button>

                  	<div class="pull-right">
                     	<button class="btn btn-sm btn-warning"><i class="fa fa-download"></i> Download PO</button>
                     	<button class="btn btn-sm btn-info"><i class="fa fa-upload"></i> Upload Signed copy</button>

                     	<!-- If the signed copy is uploaded -->
                     	<button class="btn btn-sm btn-primary"><i class="fa fa-file-pdf-o"></i></button>
                  	</div>
               </div>
            </div>
            <div class="incident-wrap clearfix m-b">
               <div class="row row-sm">
                  <input type="hidden" name="_token" value="9e0EVWte5bQFFg5xYDPqa5cDIxqynpmswqRRgJUv">
                  <div class="col-sm-6">
                     <div class="panel panel-default m-b-5">
                        <div class="info-table">
                           <label>Vendor Name: </label> 
                           <span> </span>
                        </div>
                        <div class="info-table">
                           <label>Reference Number: </label>
                           <span></span>
                        </div>
                        <div class="info-table">
                           <label>Payment Terms: </label>
                           <span></span>
                        </div>
                        <div class="info-table">
                           <label>GST Included: </label>
                           <span></span>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="panel panel-default m-b-5">
                        <div class="info-table">
                           <label>Status: </label>
                           <span style="display: inline-block;"
                              class="label label-primary custom-label">
                           </span>
                        </div>
                        <div class="info-table">
                           <label>Delivery Date: </label>
                           <span><small></small></span>
                        </div>
                        <div class="info-table">
                           <label>Created By: </label>
                           <span><small></small></span>
                        </div>
                        <div class="info-table">
                           <label>Approved By: </label>
                           <span><small></small></span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="incident-msg">
                <h4 class="admin-section-heading">Purchase Items</h4>
                <div class="panel panel-default">
                    <table class="table">
                     	<thead>
                     		<tr>
                     			<th></th>
                     			<th>Item name</th>
                     			<th width="10%" class="text-center">Qty</th>
                     			<th class="text-center">Unit Price</th>
                     			<th class="text-center">Total</th>
                     			<th></th>
                     			<th class="text-right">Action</th>
                     		</tr>
                     	</thead>

                     	<!-- Repeat tbody instead of tr -->
                     	<tbody>
                     		<tr>
                     			<td class="text-center" width="30"><a href=""><i class="fa fa-chevron-up"></i></a></td>
                     			<td>Monitor</td>
                     			<td class="text-center">
                     				<input class="form-control text-center" type="" name="" value="2" />
                     			</td>
                     			<td class="text-center">$100</td>
                     			<td class="text-center">$200</td>
                     			<td></td>
                     			<td class="text-right">
                     				<a href="" class="btn btn-success btn-xs">
                     					<i class="fa fa-check"></i> Approve
                     				</a>
                     				<a href="" class="btn btn-danger btn-xs">
                     					<i class="fa fa-remove"></i> Reject
                     				</a>
                     			</td>
                     		</tr>
                     		<tr>
                     			<td colspan="7" class="bg-light">
                     				<div class="panel panel-default no-margin">
                     					<div class="panel-body">
                     						<ul>
		                     					<li>Qty updated from 5 - 8 by Meera <span class="pull-right">16th Nov 2018 at 4:00pm</span></li>
		                     					<li>Approved by Sarwar <span class="pull-right">16th Nov 2018 at 4:00pm</span></li>
		                     				</ul>
		                     			</div>
	                     			</div>
                     			</td>
                     		</tr>
                     	</tbody>

                     	
                     	<tbody>
                     		<tr>
                     			<td class="text-center" width="30"><a href=""><i class="fa fa-chevron-down"></i></a></td>
                     			<td>Monitor</td>
                     			<td class="text-center">
                     				<input class="form-control text-center" type="" name="" value="2" />
                     			</td>
                     			<td class="text-center">$100</td>
                     			<td class="text-center">$200</td>
                     			<td class="text-right">
                     				<b class="text-success">Approved by Sarwar</b>
                     			</td>
                     			<td class="text-right">
                     				<a href="" class="btn btn-success btn-xs">
                     					<i class="fa fa-check"></i> Approve
                     				</a>
                     				<a href="" class="btn btn-danger btn-xs">
                     					<i class="fa fa-remove"></i> Reject
                     				</a>
                     			</td>
                     		</tr>
                     	</tbody>
                     	<tbody>
                     		<tr>
                     			<td class="text-center" width="30"><a href=""><i class="fa fa-chevron-down"></i></a></td>
                     			<td>Monitor</td>
                     			<td class="text-center">
                     				<input class="form-control text-center" type="" name="" value="2" />
                     			</td>
                     			<td class="text-center">$100</td>
                     			<td class="text-center">$200</td>
                     			<td class="text-right">
                     				<b class="text-success">Approved by Sarwar</b>
                     			</td>
                     			<td class="text-right">
                     				<b class="text-success">Approved by Pratik</b>
                     			</td>
                     		</tr>
                     	</tbody>
                     	<tbody>
                     		<tr>
                     			<td class="text-center" width="30"><a href=""><i class="fa fa-chevron-down"></i></a></td>
                     			<td>Monitor</td>
                     			<td class="text-center">
                     				<input class="form-control text-center" type="" name="" value="2" />
                     			</td>
                     			<td class="text-center">$100</td>
                     			<td class="text-center">$200</td>
                     			<td class="text-right">
                     				
                     			</td>
                     			<td class="text-right">
                     				<b class="text-danger">Rejected by Pratik</b>
                     			</td>
                     		</tr>
                     	</tbody>
                    </table>
                </div>
            </div>
         </div>
         <div class="col-sm-3 status-tree">
		    <ul>
		        <li>
		            <span class="grey"></span>
		            <div class="small text-info" style="text-transform: capitalize;"><b>Activity title</b></div>
		            <div class="small m-b-5"><b>Activity details</b></div>
		            <small>Megha</small>
		            <small><i class="fa fa-clock-o"></i> 19 Nov 2018 at 03:00pm</small>
		        </li>
		    </ul>
		 </div>
         
      </div>
	</div>
</div>
@endsection
