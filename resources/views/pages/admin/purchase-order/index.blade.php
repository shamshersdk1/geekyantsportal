@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid"  ng-app="myApp" ng-controller="purchaseOrderCtrl">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Purchase Orders</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
		  			<li class="active">Purchase Orders</li>
        		</ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
				<a href="#" ng-click="createPurchaseOrder()" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Add Purchase Order</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
	    	@if(!empty($errors->all()))
	            <div class="alert alert-danger">
	                @foreach ($errors->all() as $error)
	                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                    <span>{{ $error }}</span><br/>
	                  @endforeach
	            </div>
	        @endif
	        @if (session('message'))
	            <div class="alert alert-success">
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ session('message') }}</span><br/>
	            </div>
			@endif
		</div>
		<grid-view model="gridViewModel" columns="gridViewColumns" actions="gridViewActions"></grid-view>
	</div>
</div>
</div>
@endsection
