@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Users</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li><a href="/admin/goal-user-list">Users List</a></li>
                    <li><a href="/admin/user-goals/{{$user_id}}">{{$name}} Goals</a></li>
                    <li><a>Create User Goals</a></li>
        		</ol>
            </div>
		</div>
	</div>
	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
	<div class="panel panel-default">
        <form action="/admin/user-goals/{{$user_id}}" method="POST">
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-3">
                            <label>Name: </label>
                        </div>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="name" value={{old('name')}}>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-3">
                            <label>Duration: </label>
                        </div>
                        <div class="col-sm-9">
                            <select class="form-control" id="year-selector" name="year" required>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="padding-top:15px">
                <div class="col-sm-12 text-center">
                    <button type="reset" class="btn btn-primary">Reset</button>
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
            </div>
        </div>
        </form>
	</div>
</div>	

<script>
    $(function() {
        if(moment().quarter() == 1) {
            var start = moment().year()-1;
        } else {
            var start = moment().year();
        }
        $("#year-selector").append('<option value="'+start+'" @if($current) disabled @endif>'+start+' - '+(++start)+'</option>');
        $("#year-selector").append('<option value="'+start+'" @if($next) disabled @endif>'+start+' - '+(++start)+'</option>');
        // $("#year-selector").append('<option value="'+start+'" @if($next) disabled @endif>'+start+' - '+(++start)+'</option>');
    });
</script>
@endsection
