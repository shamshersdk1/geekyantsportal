@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Users</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li><a href="/admin/goal-user-list">Users List</a></li>
                    <li><a>{{$name}} Goals</a></li>
        		</ol>
            </div>
            <div class="col-sm-4 text-right" style="padding-top:15px">
                <a class="btn btn-success" href="/admin/user-goals/{{$user_id}}/create"> <i class="fa fa-plus"></i>  Add</a>
            </div>
		</div>
	</div>
	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
	<div class="panel panel-default noradius">
		<table class="table table-striped">
			<th>#</th>
			<th>Name</th>
            <th>From</th>
            <th>To</th>
            <th>Status</th>
            <th>Created At</th>
			<th class="text-right">Action</th>
			@if(count($user_goals) > 0)
	    		@foreach($user_goals as $goal)
					<tr>
                        <td>{{$goal->id}}</td>
						<td >
							{{$goal->name}} 
						</td>
						<td >
							{{ date_in_view($goal->from_date) }} 
						</td>
                        <td >
							{{ date_in_view($goal->to_date) }} 
						</td>
                        <td>
                            <span class="label @if($goal->status == 'pending') label-warning 
											   @elseif($goal->status == 'running') label-info 
											   @elseif($goal->status == 'completed') label-success 
											   @elseif($goal->status == 'closed') label-danger 
											   @else label-primary 
											   @endif"
							>
								{{ucfirst($goal->status)}}
							</span>
                        </td>
                        <td>{{datetime_in_view($goal->created_at)}}</td>
						<td class="text-right">
							<a type="button" href="/admin/goal-user-list/{{$goal->id}}" class="btn btn-info btn-sm crude-btn"><i class="fa fa-eye"></i> View</a>
							<a type="button" href="/admin/goal-user-list/{{$goal->user_id}}/{{$goal->id}}/edit" class="btn btn-primary btn-sm crude-btn">Edit</a>
							<a type="button" href="/admin/goal-user-list/{{$goal->user_id}}/{{$goal->id}}/review" class="btn btn-success btn-sm crude-btn">Review</a>
						</td>
					</tr>
				@endforeach
	    	@else
	    		<tr>
	    			<td colspan="3">No results found.</td>
	    		</tr>
	    	@endif
	   </table>
	</div>
	{{$user_goals->render()}}
</div>	
@endsection

