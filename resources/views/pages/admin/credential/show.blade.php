@extends('layouts.admin-dashboard')
@section('main-content')
    <div class="container-fluid">
        <div class="breadcrumb-wrap">
            <div class="row">
                <div class="col-sm-8">
                    <h1>Credential</h1>
                    <ol class="breadcrumb">
                        <li><a href="/admin">Admin</a></li>
                        <li><a href="/admin/credential">Credential</a></li>
                        <li class="active">1</li>
                    </ol>
                </div>
            </div>
        </div>
        @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif
        <form class="form-horizontal" >
		<input type="hidden" name="credential_category_id" value="{{$credential_category->id}}"
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="" class="col-sm-2 control-label">Name: </label>
							<div class="col-sm-6">
								<input type="text" class="form-control" id="name" name="name" value="{{$name}}">
							</div>
						</div>
					@foreach ( $fields as $index => $field )
						<div class="form-group">
							<label for="" class="col-sm-2 control-label">{{ $field['title'] }}: </label>
							<div class="col-sm-6">
								<input type="text" class="form-control" id="{{$field['key']}}" name="{{$field['key']}}" value="{{$field['value']}}">
							</div>
						</div>
					@endforeach
					</div>
				</div>
			</div>
			<div class="text-center" style="margin-top: 30px;">
				<button type="submit" class="btn btn-primary btn-sm crude-btn "> SAVE</button>
			</div>
		</div>
		</form>
@endsection