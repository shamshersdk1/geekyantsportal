@extends('layouts.admin-dashboard')
@section('main-content')
@if(isset($sharedCredentials))
<script type="text/javascript">
	window.credentials = '<?php echo json_encode($sharedCredentials); ?>';
</script>
@endif
@if(isset($credentials))
<script type="text/javascript">
    window.credentials = '<?php echo json_encode($credentials); ?>';
</script>
@endif
<div class="container-fluid" ng-app="myApp" ng-controller="credentialIndexCtrl">
    <div class="breadcrumb-wrap">
        <div class="flex-class justify-content-between align-items-center">
            <div>
                <h1>Shared Credential</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li class="active">Shared Credential</li>
                </ol>
            </div>
            <div>
                @if(isset($credentials))
                <a href="/admin/shared-credential/create" class="btn btn-success">
                    <i class="fa fa-plus fa-fw"></i>
                    Add Credential
                </a>
                @endif
            </div>
        </div>
    </div>
    <!-- Message block -->
    @if(!empty($errors->all()))
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ $error }}</span><br/>
                @endforeach
        </div>
    @endif
    @if (session('message'))
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <span>{{ session('message') }}</span><br/>
        </div>
    @endif

    @if(isset($sharedCredentials))
        <div class="user-list-view">
        <h4> List of Credential </h4>
      <div class="panel panel-default">
            
                <table id="credential_table" class="table table-striped table-condensed1 scroll-table">
                    <thead>
                    <th width="10%" class="text-left">Name</th>
                    <th width="15%" class="text-left">Login URL</th>
                    <th width="15%" class="text-left">User / Pass</th>
                    </thead>
                    <tbody>
                    @foreach( $sharedCredentials as $index => $credential )
                    <tr>
                    <td class="text-left">{{$credential['name']}}</td>
                    <td class="text-left"><span class="link-wrap"><code>{{$credential['details'][0]['value']}}</code></span>

                        </td>
                    <td class="text-left">   
                        <span><i class="fa-fw fa fa-user"></i> {{$credential['details'][1]['value']}}</span> 
                                
                                <div class="m-t-5">
                                    <span>
                                        <i class="fa-fw fa fa-key"></i> 
                                        <div style="display: inline-block;">
                                            <span ng-show="showPass[{{$index}}]">{{$credential['details'][2]['value']}}</span>
                                        </div>
                                        <a href class="btn btn-sm btn-link no-padding" ng-click="showPass[{{$index}}] = !showPass[{{$index}}]">
                                            <span ng-show="!showPass[{{$index}}]">Show</span>
                                            <span ng-show="showPass[{{$index}}]">Hide</span>
                                            password
                                        </a>
                                    </span>
                                
                                </div>
                            
                            
                            
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            
      </div>
   </div>

    @endif

    @if(isset($credentials))
    <div class="user-list-view" >
      <div class="panel panel-default">  
            
            <table id="credential_table" class="table table-striped table-condensed1 scroll-table">
                <thead>
                    <th width="15%" class="text-left">Name</th>
                    <th width="30px" class="text-left">Login URL</th>
                    <th width="20%" class="text-left">User / Pass</th>
                    <th width="15%" class="text-left">Shared with</th>
                    <th width="20%" class="text-right">Actions</th>
                </thead>
                <tbody>
                    <tr ng-repeat="credential in credentials">
                        <td>
                            <h5 class="no-margin">%%credential['name']%%</h5>
                            <div class="m-t-5"><small class="text-muted">By - %%credential['created_by']%%</small></div>
                        </td>
                        <td>
                            <div class="w-230">
                                <span class="link-wrap"><code>%%credential['details'][0]['value']%%</code></span>
                            </div>
                        </td>
                        <td>
                            <span><i class="fa-fw fa fa-user"></i> %%credential['details'][1]['value']%%</span> 
                            <div class="m-t-5">
                                <span>
                                    <i class="fa-fw fa fa-key"></i> 
                                    <div style="display: inline-block;">
                                        <span ng-show="showPass">%%credential['details'][2]['value']%%</span>
                                    </div>
                                    <a href class="btn btn-sm btn-link no-padding" ng-click="showPass = !showPass">
                                        <span ng-show="!showPass">Show</span>
                                        <span ng-show="showPass">Hide</span>
                                        password
                                    </a>
                                </span>
                            </div>
                        </td>
                        <td>
                            <li ng-repeat="shared_user in credential['shared_users']">%%shared_user['name']%%</li> 
                            <span ng-blur="showShared(credential)">%%selectedUsers%% </span> 

                        </td>
                        <td class="text-right">
                            <span>
                                <button class="btn btn-success crude-btn btn-sm crendentail-modal" 
                                    ng-click="showModal(credential)" tooltip="Share">
                                    <i class="fa fa-user-plus"></i>
                                </button>
                            </span>
                            <span>
                                <a href="/admin/shared-credential/%%credential['id']%%/edit" class="btn btn-default crude-btn btn-sm"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                            </span>
                            <span>
                                <button class="btn btn-default text-danger crude-btn btn-sm" 
                                    ng-click="delete(credential)"><i class="fa fa-trash"></i></button>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        
      </div>
   </div>
   @endif
</div>

<script>
$(function(){    
    $("td span").click(function(){
    var range = document.createRange();
    range.selectNodeContents(this);  
    var sel = window.getSelection(); 
    sel.removeAllRanges(); 
    sel.addRange(range);
    document.execCommand("Copy");
});
});
</script>
@endsection
