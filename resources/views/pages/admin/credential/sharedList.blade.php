@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1>Credential</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li class="active">Credential</li>
                </ol>
            </div>
        </div>
    </div>
    <!-- Message block -->
    @if(!empty($errors->all()))
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ $error }}</span><br/>
                @endforeach
        </div>
    @endif
    @if (session('message'))
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <span>{{ session('message') }}</span><br/>
        </div>
    @endif

    <div class="user-list-view">
        <h4> List of Credential </h4>
      <div class="panel panel-default">
         <div class="panel-body"></div>
         
         <table id="credential_table" class="table table-striped">
            <thead>
               <th width="10%" class="text-left">#</th>
               <th width="15%" class="text-left">Name</th>
               <th width="15%" class="text-left">Credentials</th>
            </thead>
            <tbody>
            @foreach( $credentials as $index => $credential )
            <tr>
               <td class="text-left">{{$index+1}}</td>
               <td class="text-left">{{$credential['name']}} </td>
               <td class="text-left">   
                    <div class="">
                        <span>
                            URL: 
                        </span>
                        <span class="data-copy">
                        {{$credential['details'][0]['value']}} 
                        </span>
                        <br>
                        <span>
                            Username:
                        </span>
                        <span class="data-copy">
                            {{$credential['details'][1]['value']}}
                        </span>
                        <br>
                        <span class="data-copy">
                            Password:
                        </span>
                        <span class="data-copy">
                            {{$credential['details'][2]['value']}}
                        </span>
                    </div>
                </td>
            </tr>
            @endforeach
            </tbody>
         </table>
      </div>
   </div>
</div>


<script>
$(function(){    
    $(document).ready(function() {
    $('#credential_table').DataTable();
})
$(".data-copy").click(function(){
    var range = document.createRange();
    range.selectNodeContents(this);  
    var sel = window.getSelection(); 
    sel.removeAllRanges(); 
    sel.addRange(range);
    document.execCommand("Copy");
});
});
</script>
@endsection
