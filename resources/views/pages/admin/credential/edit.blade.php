@extends('layouts.admin-dashboard')
@section('main-content')
    <div class="container-fluid">
        <div class="breadcrumb-wrap">
            <div class="flex-class align-items-center">
                <div>
                    <h1>Shared Credential</h1>
                    <ol class="breadcrumb">
                        <li><a href="/admin">Admin</a></li>
                        <li><a href="/admin/shared-credential">Shared Credential</a></li>
                        <li class="active">{{$credential->id}}</li>
                    </ol>
                </div>
            </div>
        </div>
        @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif
        <form class="form-horizontal" method="post" action="/admin/shared-credential/{{$credential->id}}" enctype="multipart/form-data">
        <input name="_method" type="hidden" value="PUT" />
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Name: </label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="name" name="name" value="{{$credential->name}}" >
                  </div>
                </div>
              @foreach ( $fields as $index => $field )
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">{{ $field['title'] }}: </label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="{{$field['key']}}" name="{{$field['key']}}" value="{{$field['value']}}">
                  </div>
                </div>
              @endforeach
                  <div class="form-group">
                      <label for="notes" class="col-sm-2 control-label">Notes: </label>
                      <div class="col-sm-6">
                          <textarea class="form-control" style="width:100%;" rows="5" placeholder="Notes" name="notes">{{ $credential->notes }}</textarea>
                      </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
        <div class="text-center">
          <button type="submit" class="btn btn-success">Update</button>
        </div>
		</form>
  </div>
@endsection