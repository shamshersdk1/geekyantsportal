@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
<div class="breadcrumb-wrap">
   <div class="row">
      <div class="col-sm-12">
         <h1 class="admin-page-title">Contact List</h1>
         <ol class="breadcrumb">
            <li><a href="/admin">Admin</a></li>
            <li class="active">Contact List </li>
         </ol>
      </div>
   </div>
</div>
<div class="row user-contact">
   <div class="col-md-12">
      <div class="col-md-3 p-0 pull-right">
         <div class="input-group stylish-input-group">
            <input type="text" class="form-control"  placeholder="Search" >
            <span class="input-group-addon">
            <button type="submit">
            <span class="glyphicon glyphicon-search"></span>
            </button>  
            </span>
         </div>
      </div>
      <div class="col-md-9 p-0 ">
         <h2>All Contacts</h2>
      </div>
   </div>
   <div class="col-md-12 ">
      <!-- <div class="panel panel-default col-md-12 p-0">
         <div class="panel-heading col-md-12 ">
            <div class="col-sm-4 p-0  ">
               <select class="select-contact form-control">
                  <option>MS. Muhsin</option>
                  <option>MS. Muhsin</option>
               </select>
            </div>
            <div class="col-sm-8 pull-right sort-list">
               <div class="pull-right">
                  <label for="sel1">Sort By:</label>
                  <select id="sel1">
                     <option>Email</option>
                     <option>Name</option>
                  </select>
               </div>
            </div>
         </div>
         <div class="panel-body flex-class">
            <div class="contact-wrap flex-class flex-header">
               <div class="contact-wrap-heading">Contact</div>
               <div class="contact-wrap-heading-emer">Social Contact</div>
               <div class="contact-wrap-heading-emer">Emergency Contact</div>
            </div>
            <div class="contact-wrap flex-class">
               <div class="main-contact flex-class">
                  <div class="user-img">
                     <div class="img-container" style="background:url(/images/team/muhsin.jpg);  background-size:100% 100%">
                     </div>
                  </div>
                  <div class="user-detail">
                     <div class="user-name">
                        MS. Muhsin <br/>muhsin@geekyants.com
                     </div>
                     <div class="user-number">
                        <i class="fa fa-phone"></i>
                        <a href="tel:1234567898">
                        1234567898</a>
                     </div>
                   
                  </div>
               </div>
               <div class="contact-wrap-heading-emer">
                  <span><i class="fa fa-skype"></i></span><a href="skype:saurabh">saurabh</a>
                  <br/><span><i class="fa fa-github"></i></span><span><a href="https://github.com/saurabh" target="_blank">https://github.com/saurabh</a></span>
                  <br/>
                  <span><i class="fa fa-bitbucket" aria-hidden="true"></i></span><span><a href="https://bitbucket.org/saurabh" target="_blank">https://bitbucket.org/saurabh</a></span>
               </div>
         
                <div class="contact-wrap-heading-emer">
               <div class="emergency-num">
                        <div class="num"><i class="fa fa-phone"></i>
                           <a href="tel:1234567898">
                           1234567898</a>
                        </div>
                     </div>
               
            </div>
            </div>
         
         
            <div class="contact-wrap flex-class">
               <div class="main-contact flex-class">
                  <div class="user-img">
                     <div class="img-container" style="background:url(/images/team/muhsin.jpg);  background-size:100% 100%">
                     </div>
                  </div>
                  <div class="user-detail">
                     <div class="user-name">
                        MS. Muhsin <br/>muhsin@geekyants.com
                     </div>
                     <div class="user-number">
                        <i class="fa fa-phone"></i>
                        <a href="tel:1234567898">
                        1234567898</a>
                     </div>
                     
                  </div>
               </div>
               <div class="contact-wrap-heading-emer">
                  <span><i class="fa fa-skype"></i></span><a href="skype:saurabh">saurabh</a>
                  <br/><span><i class="fa fa-github"></i></span><span><a href="https://github.com/saurabh" target="_blank">https://github.com/saurabh</a></span>
                  <br/>
                  <span><i class="fa fa-bitbucket" aria-hidden="true"></i></span><span><a href="https://bitbucket.org/saurabh" target="_blank">https://bitbucket.org/saurabh</a></span>
               </div>
               <div class="contact-wrap-heading-emer">
               <div class="emergency-num">
                        <div class="num"><i class="fa fa-phone"></i>
                           <a href="tel:1234567898">
                           1234567898</a>
                        </div>
                     </div>
                 </div>
               
            </div>
         </div>
         
         </div> -->
      <div class="panel panel-default ">
         <table class="table" id="conatct">
            <thead>
               <th>Contact</th>
               <th>Social Contact</th>
               <th>Personal Info</th>
               <th>Emergency Contact</th>
               <th>Activate</th>
               <th>Action</th>
            </thead>
            <tbody>
               <tr>
                  <td>
                     <div class="">
                        <div class="main-contact flex-class">
                           <div class="user-img">
                              <div class="img-container" style="background:url(/images/team/muhsin.jpg);  background-size:100% 100%">
                              </div>
                           </div>
                           <div class="user-detail">
                              <div class="user-name">
                                 MS. Muhsin <br/>muhsin@geekyants.com
                              </div>
                              <div class="user-number">
                                 <i class="fa fa-phone"></i>
                                 <a href="tel:1234567898">
                                 1234567898</a>
                              </div>
                           </div>
                        </div>
                  </td>
                  <td><div class="">
                  <span><i class="fa fa-skype"></i></span>
                  <a href="skype:pratiks">pratiks</a>
                  <br>
                  <span><i class="fa fa-github"></i></span>
                  <span>
                  <a href="https://github.com/spf13/viper" target="_blank">Check github url </a>
                  </span>
                  <br>
                  <span><i class="fa fa-apple"></i></span>
                  <span>
                  <a href="viper" target="_blank">viper </a>
                  </span>
                  <br>
                  <span><i class="fa fa-bitbucket" aria-hidden="true"></i></span>
                  <span><a href="https://github.com/spf13/viper" target="_blank">Check bitbucket url</a>
                  </span>
                  </div></td>
                  <td><div class="contact-wrap-heading-emer">
                  <span></span>
                  Sahu Soft India Pvt Ltd  No. 18,  
                  <br>
                  <span>
                  2nd Cross Road, N S Playa, , 2nd Stage BTM Layout 
                  </span>
                  <br>
                  <span>
                  Bangalore 
                  </span>
                  <br>
                  <span></span>
                  <span>
                  karnataka 
                  </span>
                  <span>
                  560078 
                  </span>
                  </div></td>
                  <td><div class="">
                  <div class="emergency-num">
                  <div class="num"><i class="fa fa-phone"></i>
                  <a href="tel:24354354646">
                  24354354646</a>
                  </div>
                  </div>
                  <span class="emergency-num"><i class="fa fa-envelope"></i> <a href="mailto:pratik@gmail.com" target="_blank"> pratik@gmail.com</a>
                  </span>
                  </div></td>
                  <td> <label class="switch">
                    <input type="checkbox">
                    <span class="slider round"></span>
                    </label>
                </td>
                  <td><div class=" last">
                  <form name="editForm" method="get" action="/admin/user-contacts/2/edit">
                  <button type="submit" class="btn btn-info btn-sm crude-btn">Update Info</button> 
                  <button type="submit" class="btn btn-danger btn-sm ">Delete</button> 
                  
                 </form>    
                  </div></td>
                
               </tr>
            </tbody>
         </table>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
   $(function(){
             $(".select-contact").select2({
   placeholder: "All Contacts",
   allowClear: true
   });
   });
   $(document).ready(function() {
    $('#conatct').DataTable( {
        "scrollX": true,
        "fixedColumns":true
    } );
} );
</script>
@endsection