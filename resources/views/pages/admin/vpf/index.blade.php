@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-6">
                <h1 class="admin-page-title">VPF</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li>VPF</li>
        		</ol>
            </div>
			<div class="col-sm-6">
			<div class="pull-right m-t-10">
				<a href="/admin/vpf/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Add VPF</a>
            </div>
			</div>
		</div>
	</div>
	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
    </div>   
    <div class="user-list-view">
        <div class="panel panel-default">
            <table class="table table-striped">
                <tr>
                    <th class="text-center">#</th>
                    <th class="text-center">Name</th>
                    <th class="text-center">Amount</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">Created At</th>
                    <th class="pull-right">Action</th>
                </tr>
                @if( empty($vpfList) || count($vpfList) == 0 )
                    <tr>
                        <td>
                            <td class="text-center">No Records found</td>
                            </td>
                        </tr>
                @endif
                @if(isset($vpfList) > 0)
                    @foreach($vpfList as $index => $vpf)
                        <tr>
                            <td class="text-center">{{ $vpf->user_id}}</td>
                            <td class="text-center"> {{ $vpf->user ? $vpf->user->name : null; }} </td>
                            <td class="text-center"> {{ $vpf->amount }} </td>
                            <td class="text-center">
                                    @if($vpf->status == 'pending') <span>Pending</span>@endif 
                                    @if($vpf->status == 'approved') <span>Approved</span>@endif 
                                    @if($vpf->status == 'rejected') <span>Rejected</span>@endif 
                                </td>
                            <td class="text-center"> {{ datetime_in_view($vpf->created_at) }} </td>
                            <td class="pull-right">
                            @if($vpf->status == 'pending')
                                    <span>
                                    <form name="showForm" method="get" action="/admin/vpf/approve/{{$vpf->id}}"  style="display: inline-block;">
                                            <button style="display:none;" type="submit" class="btn btn-success btn-sm crude-btn">Approve</button> 
                                    </form>
                                    <span>
                                    <span>
                                        <form name="editForm" method="get" action="/admin/vpf/rejected/{{$vpf->id}}"  style="display: inline-block;">
                                            <button style="display:none;" type="submit" class="btn btn-danger btn-sm crude-btn">Reject</button> 
                                        </form>
                                    </span>
                                @endif

                                <span>
                                <form name="showForm" method="get" action="/admin/vpf/{{$vpf->id}}"  style="display: inline-block;">
                                    <button type="submit" class="btn btn-primary btn-sm crude-btn"><i class="fa fa-eye fa-fw"></i>View</button> 
                                </form>
                                <span>
                                <span>
                                <form name="editForm" method="get" action="/admin/vpf/{{$vpf->id}}/edit"  style="display: inline-block;">
                                    <button type="submit" class="btn btn-warning btn-sm crude-btn"><i class="fa fa-edit fa-fw"></i>Edit</button> 
                                </form>
                                </span>
                                
                            <span>
                                <form name="deleteForm" method="post" action="/admin/vpf/{{$vpf->id}}"  style="display: inline-block;">
                                    <input name="_method" type="hidden" value="delete" />
                                    <button type="submit" class="btn btn-danger btn-sm crude-btn"><i class="fa fa-trash fa-fw"></i>Delete</button> 
                                </form>
                            </span>
                            </td>
                        </tr>
                    @endforeach
                @endif  
             </table>
        </div>
    </div>
</div>	
@endsection

