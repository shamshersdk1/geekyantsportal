@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-6">
                <h1 class="admin-page-title">Voluntary Provident Fund</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
                      <li><a href="{{ url('admin/vpf') }}">VPF</a></li>
                      <li>{{$vpf->user->name}}</li>
        		</ol>
            </div>
			
		</div>
	</div>
	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
    </div>

    <div class="row">
	        <div class="col-md-12">
	            <div class="panel panel-default">
				    <div class="panel-body">
				    	<form name = "myForm" style="padding:10px;" > 
                            <b>Name :</b> {{$vpf->user->name}} <br>
                            <b>Employee ID :</b> {{$vpf->user->employee_id}} <br>
                            <b>Amount :</b> {{$vpf->amount}} <br>
                            <b>Created At :</b> {{datetime_in_view($vpf->created_at)}} <br>
                            <b>Created By :</b> {{$vpf->creator->name}} <br>
			    		</form>
				    </div>
				</div>
			</div>	
        </div>  
        <div class="row">
	        <div class="col-md-12">
	            <div class="panel panel-default">
				    <div class="panel-body">
                            <b>Logs History : </b>
				    </div>
				</div>
			</div>	
		</div>    
    <div class="user-list-view">
        <div class="panel panel-default">
            <table class="table table-striped">
                <tr>
                    <th class="text-center">Amount</th>
                    <th class="text-center">Created By</th>
                    <th class="text-center">Created At</th>
                    <th class="text-center">Updated At</th>
                </tr>
                @if( empty($vpfLogs) || count($vpfLogs) == 0 )
                <tr>
                    <td>
                        <td class="text-center">No Records found</td>
                    </td>
                </tr>
                @endif
                @if(isset($vpfLogs) > 0)
                    @foreach($vpfLogs as $index => $vpf)
                        <tr>
                            <td class="text-center"> {{ $vpf->amount }} </td>
                            
                            <td class="text-center"> {{ $vpf->creator ? $vpf->creator->name : null; }} </td>
                            <td class="text-center"> {{ datetime_in_view($vpf->created_at) }} </td>
                            <td class="text-center"> {{ datetime_in_view($vpf->updated_at) }} </td>  
                        </tr>
                    @endforeach
                @endif  
            </table>
        </div>
    </div>
</div>	
@endsection

