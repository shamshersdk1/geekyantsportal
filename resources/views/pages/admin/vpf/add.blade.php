@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-6">
                <h1 class="admin-page-title">Voluntary Provident Fund</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li><a href="{{ url('admin/seat-management') }}">VPF</a></li>
        		</ol>
            </div>
		</div>
	</div>
	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		            @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-body">
            <form class="form-horizontal" method="post" action="/admin/vpf" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token()}}">
                <input type="hidden" name="from_admin" value="1">
                    <div class="form-group">
                        <label for="" class="col-sm-2 col-sm-offset-2 control-label">Employee Name</label>
                            <div class="input-group col-sm-4">
                                @if(isset($userList))
                                    <select id="selectid2" name="user_id"  style="width=35%;" placeholder= "Select name" required>
                                        <option value=""></option>
                                        @foreach($userList as $x)
                                            <option value="{{$x->id}}" >{{$x->name}}</option>
                                        @endforeach
                                    </select>
                                @endif
                            </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-sm-2 col-sm-offset-2 control-label">Voluntary Provident Fund:</label>
                        <div class="col-sm-4">
                        <input type="number" class="form-control" value="" name="amount" placeholder="" min="1" max="999999" required/>
                    </div>
                    </div>
                    <div class="text-center" style="margin-top: 30px;">
                        <button type="submit" class="btn btn-primary btn-sm crude-btn "> SAVE</button>
                    </div>
            </form>
        </div>
    </div>
</div>	
@endsection

