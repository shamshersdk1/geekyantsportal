@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-6">
                <h1 class="admin-page-title">Edit Voluntary Provident Fund</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li><a href="{{ url('admin/seat-management') }}">VPF</a></li>
        		</ol>
            </div>
		</div>
	</div>
	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		            @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-body">
            <form class="form-horizontal" method="post" action="/admin/vpf/{{$vpf->id}}" enctype="multipart/form-data">
                <input name="_method" type="hidden" value="PUT" />                  
                    <div class="form-group">
                        <label for="name" class="col-sm-2 col-sm-offset-2 control-label">User : </label>
                        <div class="col-sm-4">
                            <label class="form-control" name="user">{{$vpf->user->name}}</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-sm-2 col-sm-offset-2 control-label">Voluntary Provident Fund:</label>
                        <div class="col-sm-4">
                            <input type="number" class="form-control" value="{{$vpf->amount}}" name="amount" placeholder="" min="1" max="999999" required/>
                        </div>
                    </div>
                    <div class="text-center" style="margin-top: 30px;">
                        <button type="submit" class="btn btn-primary btn-sm crude-btn "> EDIT</button>
                    </div>
            </form>
        </div>
    </div>
</div>	
@endsection

