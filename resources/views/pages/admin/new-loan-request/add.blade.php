@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Request Loan</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/loan-requests') }}">My Loans</a></li>
                    <li class="active">Add</li>
                </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
    	@if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span><br/>
                  @endforeach
            </div>
        @endif
        @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
        @endif   
		<form name = "myForm" method="post" action="/admin/loan-requests" enctype="multipart/form-data" >
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="panel panel-default" style="padding-top: 15px;">
                <div class="row">
                    <div class="col-sm-12">
                        <label for="name" class="col-sm-2">Enter Amount:</label>
                        <div class="col-sm-4  form-group">
                            <input type="text" name="amount" class="form-control" placeholder="0.00" required value="{{ old('amount') }}">
                        </div>
                        <label for="name" class="col-sm-2">Enter EMI:</label>
                        <div class="col-sm-4 form-group">
                            <input type="text" name="emi" class="form-control" placeholder="0.00" required value="{{ old('emi') }}">
                        </div>
                        <label for="name" class="col-sm-2">Select Loan Type:</label>
                        <div class="col-sm-4  form-group">
                            <select name="type" id="loan-type-selector" class="form-control" required>
                                <option value="">Select Repayment Option</option>
                                <option value="annual">Against Annual Bonus</option>
                                <option value="qtr">Against Quarterly Bonus</option>
                                <option value="monthly">Monthly(Deducted from salary)</option>
                            </select>
                        </div>
                        <label for="name" class="col-sm-2">Description:</label>
                        <div class="col-sm-4 form-group">
                            <textarea name="description" class="form-control" placeholder="Enter the description" rows="5">@if(!empty(old('description'))){{old('description')}}@endif</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <button type="reset" class="btn btn-default"><i class="fa fa-rotate-left fa-fw"></i> Clear</button> 
                <button type="submit" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Save</button>
            </div>
		</form>
        </div>
	</div>
</div>
<script>
    $(function(){
        $('.datetimepicker').datetimepicker({
			format:'DD-MM-YYYY',
			useCurrent:false,
		});
        var old_type = "{{old('type')}}";
        $("#loan-type-selector option[value='"+old_type+"']").prop('selected', true);
    });
</script>
@endsection