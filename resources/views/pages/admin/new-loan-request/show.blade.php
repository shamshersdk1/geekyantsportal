@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Request Loan</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="{{str_replace('/'.$loan_request->id, '', url()->current())}}">Loan Request</a></li>
                    <li class="active">View</li>
                </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
    	@if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span><br/>
                  @endforeach
            </div>
        @endif
        @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
        @endif   
        <div class="row">
	        <div class="col-md-12" style="padding-left:30px; padding-top:20px;">
                <div class="panel panel-default">   
                    <div class="panel-body ">   
                        <div class="row">
                            <div class="col-sm-6">
                                <h4>Loan Details</h4>
                            </div>
                            <div class="col-sm-3 text-center" style="float:right">
                                <label>Status :</label>
                                @if($loan_request->status == "pending")
                                    <span class="label label-info custom-label">Pending</span><br/>
                                    <small> (Waiting for Reporting Manager)</small>
                                @elseif($loan_request->status == "submitted")
                                    <span class="label label-primary custom-label">Submitted</span><br/>
                                    <small> (Waiting for Human Resources)</small>
                                @elseif($loan_request->status == "review")
                                    <span class="label label-primary custom-label">Under Review</span><br/>
                                    <small> (Waiting for Management)</small>
                                @elseif($loan_request->status == "reconcile")
                                    <span class="label label-primary custom-label">Reconcile</span><br/>
                                    <small> (Waiting for Human Resources)</small>
                                @elseif($loan_request->status == "approved")
                                    <span class="label label-success custom-label">Approved</span><br/>
                                @elseif($loan_request->status == "rejected")
                                    <span class="label label-danger custom-label">Rejected</span><br/>
                                @elseif($loan_request->status == "disbursed")
                                    <span class="label label-success custom-label">Disbursed</span>
                                @else
                                    <span class="label label-info custom-label">No Status</span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>User : </label>
                                        <span>{{ $loan_request->user->name }}</span>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Type : </label>
                                        <td>{{($loan_request->appraisal_bonus_id === null ) ? "Against Salary" : $loan_request->appraisalBonus->appraisalBonusType->description}}</td>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>Amount : </label>
                                        <span> &#x20B9; {{ $loan_request->amount }} </span>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>EMI : </label>
                                        <span> &#x20B9; {{ $loan_request->emi }}</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>Approved Amount :</label>
                                        <span style="white-space:pre">&#x20B9; {{ $loan_request->approved_amount }}</span>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Emi Start Date :</label>
                                        <span style="white-space:pre">{{ date_in_view($loan_request->emi_start_date) }}</span>
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>Application Date :</label>
                                        <span style="white-space:pre">{{ date_in_view($loan_request->application_date) }}</span>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Reason :</label>
                                        <span style="white-space:pre">{{ $loan_request->description }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 text-center" style="float:right;margin-top:1em">
                                @if($file)
                                    <a href="/admin/salary-transfer" target="_blank" class="btn btn-danger btn-sm">Initiate Payment</a>
                                @endif
                            </div>
                            <div class="col-sm-3 text-center" style="float:right;margin-top:1em">
                                @if($file)
                                    <a href="{{$loan_request->file->url()}}" target="_blank" class="btn btn-success btn-sm">View Agreement</a>
                                @endif
                            </div>                 
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8">
                        <div class="panel panel-default">
                            <div class="panel-body ">
                                <div class="row" style="margin-bottom:15px;">
                                    <div class="col-sm-12">
                                        <form action="/admin/new-loan-requests/{{$loan_request->id}}" method="post" style="display:inline" enctype="multipart/form-data">
                                            {{method_field('PUT')}}
                                            @if($loan_request->status == "reconcile" && !$file)
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <label style="float:left; padding-top:5px; padding-left:15px;">Agreement :</label>
                                                        <div class="col-sm-8">
                                                            <input id="agreement" type="file" name="file" required class="form-control" style="border:0">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row col-sm-12" style="padding:0 0 0 30px">  
                                                    <label>Comment :</label>
                                                    <textarea name="comment" rows="5" style="width:100%">{{old('comment')}}</textarea>
                                                    <div style="display:inline; float:right">
                                                        <button type="submit" name="approve" value="approve" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Approve</button>
                                                        <button type="submit" name="reject" value="reject" class="btn btn-danger reject-btn btn-sm" formnovalidate><i class="fa fa-times"></i> Reject</button>
                                                    </div>
                                                </div>
                                            @elseif($loan_request->status != "approved")
                                                <div class="row col-sm-12" style="padding:0 0 0 30px">  
                                                    <label>Comment :</label>
                                                    <textarea name="comment" rows="5" style="width:100%">{{old('comment')}}</textarea>
                                                    <div style="display:inline; float:right">
                                                        <button type="submit" name="approve" value="approve" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Approve</button>
                                                        <button type="submit" name="reject" value="reject" class="btn btn-danger reject-btn btn-sm" formnovalidate><i class="fa fa-times"></i> Reject</button>
                                                    </div>
                                                </div>
                                            @endif
                                            
                                        </form>
                                    </div>
                                </div>
                                @if(!empty($comments))
                                    @foreach($comments as $item)

                                    <div class="row"style=" padding:15px; border: 1px solid lightgrey;">
                                        <div class="col-sm-12">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label>{{$item['user']}}:</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <small style="float:right;">
                                                    <label> 
                                                    @if($item['status'] == "pending")
                                                        <span class="label label-primary custom-label">Pending</span>
                                                    @elseif($item['status'] == "approved")
                                                        <span class="label label-success custom-label">Approved</span> 
                                                    @elseif($item['status'] == "rejected")
                                                        <span class="label label-danger custom-label">Rejected</span>                                                              
                                                    @else
                                                    @endif
                                                    </label>
                                                    {{$item['time']}}</small>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                    
                                                    {{ $item['comment'] }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                  <div class="col-sm-4">
               
                    
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h4>Loan History</h4>
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Amount</th>
                                            <th>Remaining</th>
                                            <th>Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(!empty($previous_loans))
                                            @foreach($previous_loans as $index => $loan)
                                                <tr>
                                                    <td>{{$index+1}}</td>
                                                    <td>{{$loan->amount}}</td>
                                                    <td>{{$loan->remaining}}</td>
                                                    <td>{{date_in_view($loan->application_date)}}</td>
                                                </tr>      
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="4"> No Existing loans</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                                @if(!empty($previous_loans))
                                    {{$previous_loans->render()}}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>	
    </div>
</div>	
<script>
		$(function(){
            $(".reject-btn").click(function(event){
                return confirm('Are you sure?');
            });
            $('#payment_date_datepicker').datetimepicker({
                minDate: new Date().getMonth() <= 3 ? new Date(new Date().getFullYear()-1+"-01-01") : new Date(new Date().getFullYear()+"-01-01"), //Set min date to April 1
                //Set max date to upcoming march
                maxDate: new Date().getMonth() <= 2 ? new Date(new Date().getFullYear()+"-12-31") : new Date(new Date().getFullYear()+1+"-12-31"),
                format: 'DD-MM-YYYY'
            });
        });
</script>
@endsection