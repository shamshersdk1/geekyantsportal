@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
   <div class="breadcrumb-wrap">
      <div class="row">
         <div class="col-sm-8">
            <h1 class="admin-page-title">Review Summary</h1>
            <ol class="breadcrumb">
               <li><a href="/admin">Admin</a></li>
               <li><a >Review & Apprisal</a></li>
               <li><a>Salary Details</a></li>
            </ol>
         </div>
      </div>
   </div>
   <!--Process review modal -->
   <div class="row">
      <div class="col-sm-12 user-list-view  emp-overview">
         <div class="panel panel-default panel-horizontal user-profile-details">
            <div class="panel-heading profile-image-container" style="background:url('http://geekyants-portal.local.geekydev.com/images/team/muhsin.jpg');  background-size:100% 100%">
            </div>
            <div class="panel-body profile-details-container" style="padding-top:2%">
               <ul class="emp-detail" style="list-style-type: none;display:inline;padding:0;margin:0">
                  <li>
                     <h4 style="margin:0">MS. Muhsin</h4>
                  </li>
                  <li ><small style="color:#aaa">UI/UX designer</small></li>
                  <li><small style="color:#aaa">ms.@sahusoft.com</small></li>
                  <li><small style="color:#aaa">+91 80 409 74929</small></li>
               </ul>
               <ul class="salary-details" style="list-style-type: none;display:inline;padding:0;margin:0">
                  <li><small>CTC <span>: 3,00,000</span></small></li>
                  <li ><small >Annual Bonus <span>: 60,000</span></small></li>
                  <li><small >Quarterly Bonus: <span>:10,000</span></small></li>
               </ul>
               <div class="right-table">
                  <table>
                     <tbody>
                        <tr>
                           <td><small>Emp ID</small></td>
                           <td><small> : </small></td>
                           <td><small>B201</small></td>
                        </tr>
                        <tr>
                           <td><small>DOJ</small></td>
                           <td><small> : </small></td>
                           <td><small>17 Jul 2017</small></td>
                        </tr>
                        <tr>
                           <td><small>DOC</small></td>
                           <td><small> : </small></td>
                           <td><small>26 Aug 2017</small></td>
                        </tr>
                        <tr>
                           <td colspan="3">
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-5">
         <!-- <div id="process" class="col-md-6 user-list-view"  >
            <div class="panel panel-default" style="min-height: 302px;">
               <div class="panel-heading">
                  <h4><span>Akarsh Srivastava</span>'s Review</h4>
               </div>
               <div class="panel-body ">
                  <div class="col-sm-10 col-sm-offset-1">
                     <div class="form-group col-md-12 p-l-0">
                        <label class="col-sm-3" style="font-size:16px; padding-top:10px;">Feedback:</label> 
                        <textarea class=" col-md-9" rows="3" style="resize:none"></textarea>     
                     </div>
                     <div class=" col-md-12 text-center" style="border:0 !important">
                        <a tyle="submit" class="btn btn-primary text-center m-t-30 ">Approve</a>
                        <a tyle="submit" class="btn btn-danger text-center m-t-30 ">Reject</a>
                        <a tyle="submit" class="btn btn-warning text-center m-t-30 ">Send Message </a>
                     </div>
                  </div>
               </div>
            </div>
            </div> -->
         <div class="salary-revision col-md-12 review-section p-0">
            <div class="panel panel-default">
               <div class="panel-heading time">
                  <h4>Salary Revision  <small class="pull-right"><span class="glyphicon glyphicon-time text-right"></span> 12 mins ago</small></h4>
               </div>
               <div class="panel-body">
                  <ul class="chat">
                     <li class="left clearfix" style="border:0">
                        <div class="chat-body clearfix">
                           <div class="stat expected" style="clear:left">
                              <p>
                                 <span>Expected</span><br/> <span class="text-center">5,00,000</span>
                              </p>
                           </div>
                           <div class="stat recom">
                              <p>
                                 <span>Recommendation</span><br> <span class="text-center">4,50,000</span>
                              </p>
                           </div>
                        </div>
                        <div class=" " style="border:0 !important">
                           <a tyle="submit" class="btn btn-success   ">Approve</a>
                           <a tyle="submit" class="btn btn-primary  ">Send Message </a>
                           <a tyle="submit" class="btn btn-danger   ">Reject</a>
                        </div>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
         <!--Reperting manager feedback -->
         <div class="review-section col-md-12 rm-feedback p-0">
            <div class="panel panel-default">
               <div class="panel-heading">
                  <h4>Manager Feedback</h4>
               </div>
               <div class="panel-body">
                  <div id="mycheckbox">
                     <label class="custom-checkbox">
                        <input type="checkbox" id="checkbox1"> 
                        <h4>Review has salary changes? </h4>
                        <span class="checkmark"></span
                     </label>
                  </div>
                  <form class="reporting-mg-feedback m-t-30" style="display:none;">
                     <h5>Employee Expected Salary</h5>
                     <div class="form-group row ">
                        <label class="col-sm-2 col-form-label"> CTC:</label>
                        <div class="col-sm-10">
                           <input class="form-control-plaintext"> 
                        </div>
                     </div>
                     <div class="form-group row m-t-30">
                        <label class="col-sm-2 col-form-label">Reason:</label>
                        <div class="col-sm-10">
                           <textarea class="form-control-plaintext col-sm-8" rows="4"></textarea>
                        </div>
                     </div>
                     <h5>Reporting Manager Recommendation</h5>
                     <div class="form-group row m-t-30">
                        <label class="col-sm-2 col-form-label"> CTC:</label>
                        <div class="col-sm-10">
                           <input class="form-control-plaintext"> 
                        </div>
                     </div>
                     <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Reason:</label>
                        <div class="col-sm-10">
                           <textarea class="form-control-plaintext col-sm-8" rows="4"></textarea>
                        </div>
                     </div>
                  </form>
                  <div class=" col-md-12 p-0 ">
                     <div class="form-group col-md-12 p-l-0 m-t-30">
                        <label class="col-sm-12 col-form-label p-l-0"> Feedback:</label>
                        <div class="col-sm-12 p-0">
                           <textarea class="form-control-plaintext col-sm-12" rows="4"></textarea>
                        </div>
                     </div>
                     <div class=" text-center col-md-12 p-0" style="border:0 !important;">
                        <button tyle="submit" class="btn btn-primary pull-right  ">Send</button>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- End of Reperting manager feedback -->
         <!--summary of last Reviews -->
         <div class="salary-revision col-md-12 review-section p-0" style="clear:left">
            <div class="panel panel-default">
               <div class="panel-heading">
                  <h4>Summary of Last  Salary Revision</h4>
               </div>
               <div class="panel-body p-0">
                  <table class="table table-striped">
                     <thead>
                        <tr>
                           <th>Appraisal(CTC)</th>
                           <th>Amount</th>
                           <th>Percentage</th>
                           <th class="text-right">date</th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr>
                           <td>1st </td>
                           <td>
                              2,00,000
                           </td>
                           <td>
                              50%
                           </td>
                           <td class="text-right">
                              30 Nov 2015
                           </td>
                        </tr>
                        <tr>
                           <td>2nd </td>
                           <td>
                              2,00,000
                           </td>
                           <td>
                              50%
                           </td>
                           <td class="text-right">
                              30 Nov 2015
                           </td>
                        </tr>
                        <tr>
                           <td>3rd</td>
                           <td>
                              2,00,000
                           </td>
                           <td>
                              50%
                           </td>
                           <td class="text-right">
                              30 Nov 2015
                           </td>
                        </tr>
                        <tr>
                           <td>4th </td>
                           <td>
                              2,00,000
                           </td>
                           <td>
                              50%
                           </td>
                           <td class="text-right">
                              30 Nov 2015
                           </td>
                        </tr>
                        <tr>
                           <td>5th </td>
                           <td>
                              2,00,000
                           </td>
                           <td>
                              50%
                           </td>
                           <td class="text-right">
                              30 Nov 2015
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
         <!-- End of summary of last Reviews -->
      </div>
      <div class="timeline col-md-7 p-l-0">
         <div class="time-container col-sm-12">
            <div class="left time text-right col-sm-1">
            </div>
            <div class="right deatils panel col-sm-11">
               <div class="panel-heading p-0">
                  <h4>Feedback</h4>
                  <small>Jan 21</small>
               </div>
               <div class="panel-body">
                
                  <div class="feedback">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution 
                  </div>
                   <div class="detail-name pull-right"> <small>- Name</small></div>
               </div>
            </div>
         </div>

         <div class="time-container col-sm-12">
            <div class="left time text-right col-sm-1">
            </div>
            <div class="right deatils panel col-sm-11">
               <div class="panel-heading p-0">
                  <h4>Salary Revision</h4>
                  <small>Jan 21</small>
               </div>
               <div class="panel-body">
                
                  <div class="feedback">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution 
                  </div>
                   <div class="detail-name pull-right"> <small>- Name</small></div>
               </div>
            </div>
         </div>
         <div class="time-container col-sm-12">
            <div class="left time text-right col-sm-1">
            </div>
            <div class="right deatils panel col-sm-11">
               <div class="panel-heading p-0">
                  <h4>Project Review</h4>
                  <small>Jan 21</small>
               </div>
               <div class="panel-body">
                 
                  <div class="feedback">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution 
                  </div>
                   <div class="detail-name pull-right"> <small>- Name</small></div>
               </div>
            </div>
         </div>

      </div>
      <!-- feedback list for Manager-->
      <!-- <div class="feedback-revision col-md-7 p-l-0  review-section" >
         <div class="panel panel-default">
            <div class="panel-heading">
               <h4>Feedback</h4>
            </div>
            <div class="panel-body">
               <ul class="chat">
                  <li class="left clearfix">
                     <div class="chat-body clearfix">
                        <div class="heading-chat">
                           <strong class="primary-font">Jack Sparrow</strong> <small class="pull-right text-muted">
                           <span class="glyphicon glyphicon-time"></span> 12 mins ago</small>
                        </div>
                        <p>
                           Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, adipisci
                        </p>
                     </div>
                  </li>
                  <li class="left clearfix">
                     <div class="chat-body clearfix">
                        <div class="heading-chat">
                           <strong class="primary-font">Jack Sparrow</strong> <small class="pull-right text-muted">
                           <span class="glyphicon glyphicon-time"></span> 12 mins ago</small>
                        </div>
                        <p>
                           Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, adipisci
                        </p>
                     </div>
                  </li>
                  <li class="left clearfix">
                     <div class="chat-body clearfix">
                        <div class="heading-chat">
                           <strong class="primary-font">Jack Sparrow</strong> <small class="pull-right text-muted">
                           <span class="glyphicon glyphicon-time"></span> 12 mins ago</small>
                        </div>
                        <p>
                           Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, adipisci
                        </p>
                     </div>
                  </li>
                  <li class="left clearfix">
                     <div class="chat-body clearfix">
                        <div class="heading-chat">
                           <strong class="primary-font">Varun Sahu</strong> <small class="pull-right text-muted">
                           <span class="glyphicon glyphicon-time"></span> 10 months ago</small>
                        </div>
                        <p>
                           Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, adipisci
                        </p>
                     </div>
                  </li>
               </ul>
            </div>
         </div>
      </div> -->
      <!-- End of feedback list for Manager-->
      <!-- Project Review -->
      <!-- <div class="project-revision col-md-7 p-l-0 review-section">
         <div class="panel panel-default">
            <div class="panel-heading">
               <h4>Project Review</h4>
            </div>
            <div class="panel-body">
               <ul class="chat">
                  <li class="left clearfix">
                     <div class="chat-body clearfix">
                        <div class="heading-chat">
                           <strong class="primary-font">Project Name</strong> <small class="pull-right text-muted">
                           <span class="glyphicon glyphicon-time"></span> 3 month ago</small>
                        </div>
                        <p>
                           Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, adipisci
                        </p>
                     </div>
                  </li>
                  <li class="left clearfix">
                     <div class="chat-body clearfix">
                        <div class="heading-chat">
                           <strong class="primary-font">Project Name</strong> <small class="pull-right text-muted">
                           <span class="glyphicon glyphicon-time"></span> 10 months ago</small>
                        </div>
                        <p>
                           Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, adipisci
                        </p>
                     </div>
                  </li>
               </ul>
            </div>
         </div>
      </div> -->


      <!-- End of Project Review -->
      <!--History of previous Feedback-->
      <!-- <div class="col-md-6 user-list-view privious-history">
         <div class="panel panel-default">
            <div class="panel-heading">
               <h4>History of Previous Feedback</h4>
            </div>
            <div class="panel-body">
               <ul class="nav nav-tabs" >
                  <li class="active"><a  data-toggle="tab" href="#salary">Salary Revision</a></li>
                  <li><a data-toggle="tab" href="#reviews">Reviews </a></li>
                  <li><a  data-toggle="tab" href="#pro-reviews">Project Reviews</a></li>
               </ul>
               <div class="tab-content" >
                  <div id="salary" class="tab-pane fade in active " >
                     <h4>Salary Revision</h4>
                     <table class="table table-striped">
                        <thead>
                           <tr>
                              <th>Amount</th>
                              <th>Percentage</th>
                              <th class="text-right">date</th>
                           </tr>
                        </thead>
                        <tbody>
                           <tr>
                              <td>
                                 2,00,000
                              </td>
                              <td>
                                 50%
                              </td>
                              <td class="text-right">
                                 30 Nov 2015
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <div id="reviews" class="tab-pane fade" >
                     <h4>Review</h4>
                     <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                  </div>
                  <div id="pro-reviews" class="tab-pane fade" >
                     <h4>Project Review</h4>
                     <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                  </div>
               </div>
            </div>
         </div>
         </div> -->
   </div>
   <!-- End of History of previous Feedback-->
</div>
</div>
@endsection
@section('js')
<script>
   $(document).ready(function () {
     var checkbox = $('#checkbox1');
     $(checkbox).change(function () {
         if (this.checked) 
         
            $('.reporting-mg-feedback').show();
         else 
             $('.reporting-mg-feedback').hide();
     });
   });
</script>
@endsection