  <div class="col-md-6  user-list-view p-l-0" >
   <div class="panel panel-default">
      <div class="panel-heading redBox">
       List of Pending Apprisal letters
      </div>
      <table class="table table-striped">
         <!-- <thead>
            <tr>
                <th colspan="3"></th>
            </tr>
            </thead> -->
         <tbody>
            <tr>
               <th >Employee</th>
               <th>Reporting Manager</th>
               <th>Start Date</th>
               <th class="text-right">Action</th>
            </tr>
            <tr>
               <td>
                  Akarsh Srivastava
               </td>
               <td>
                 Saurabh Sahu
               </td>
               <td>22th Jan 2018</td>
               <td class="text-right">
                  <a data-toggle="modal" data-target="#apprisal-letter" class="btn btn-primary btn-sm"  > Document Printed & Ready </a>
               </td>
            </tr>
            <tr>
               <td>
                  Akshay Jain
               </td>
               <td>
                  Varun Sahu
               </td>
               <td>22th Jan 2018</td>
               <td class="text-right">
                  <a data-toggle="modal" data-target="#apprisal-letter" class="btn btn-primary btn-sm"  > Document Printed & Ready </a>
               </td>
            </tr>
         </tbody>
      </table>
   </div>
</div>
<!--  End of Add  feedback  content-->

<div id="apprisal-letter" class="modal fade" role="dialog">
      <div class="modal-dialog">
         <!-- Modal content-->
         <div class="modal-content row">
            <div class="modal-header col-md-12">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title">Apprisal Letter</h4>
            </div>
            <div class="modal-body col-md-12">
                <h4 class="text-center">Is Apprisal letter printed & ready?</h4>
               <div class="emp-review-btn">
                  <a  class="btn btn-success" >Yes</a>
                  <a class="btn btn-danger">No</a>
               </div> 
            </div>
           
         </div>
      </div>
   </div>


