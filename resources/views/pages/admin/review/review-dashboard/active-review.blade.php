<div class="col-md-6 p-l-0">
    <div class="panel panel-default">
    <div class="panel-heading redBox">
        Active Review List
    </div>
    <table class="table table-striped">
        <!-- <thead>
            <tr>
                <th colspan="3"></th>
            </tr>
            </thead> -->
        <tbody>
            <tr>
                <th >Name</th>
                <th>Schedule Date</th>
                <th class="text-right">Action</th>
            </tr>
            <tr>
                <td>
                Akarsh Srivastava
                </td>
                <td>
                01th Jan 2018
                </td>
                <td class="text-right">
                <a type="submit" class="btn btn-success btn-sm" href="{{ url('/admin/salary-changes') }}"  >Add Review</a>
                </td>
            </tr>
            <tr>
                <td>
                Akshay Jain
                </td>
                <td>
                01th Jan 2018
                </td>
                <td class="text-right">
                <a type="submit" class="btn btn-success btn-sm" href="{{ url('/admin/salary-changes') }}"  >Add Review</a>
                </td>
            </tr>
        </tbody>
    </table>
    </div>
</div>
<!--  Add Review feedback Modal content-->
</div>
<!--  End of Add Review feedback  content-->