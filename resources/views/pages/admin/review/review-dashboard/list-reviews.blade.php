  <div class="col-md-6  user-list-view " >
   <div class="panel panel-default">
      <div class="panel-heading redBox">
        List of Reviews
      </div>
      <table class="table table-striped">
         <!-- <thead>
            <tr>
                <th colspan="3"></th>
            </tr>
            </thead> -->
         <tbody>
            <tr>
               <th >Employee</th>
             
               <th> Date</th>
               <th class="text-right">Action</th>
            </tr>
            <tr>
               <td>
                  Akarsh Srivastava
               </td>
              
               <td>22th Jan 2018</td>
               <td class="text-right">
                  <a data-toggle="modal" data-target="#meeting2"class="btn btn-success btn-sm"  >Schedule Handover Meeting</a>
               </td>
            </tr>
            <tr>
               <td>
                  Akshay Jain
               </td>
               
               <td>22th Jan 2018</td>
               <td class="text-right">
                  <a data-toggle="modal" data-target="#meeting2" class="btn btn-success btn-sm"  >Schedule Handover Meeting</a>
               </td>
            </tr>
         </tbody>
      </table>
   </div>
</div>

<!--  End of Add  feedback  content-->

 <!--  Approve Meeting Invite content-->
   <div id="meeting2" class="modal fade" role="dialog">
      <div class="modal-dialog">
         <!-- Modal content-->
         <div class="modal-content row">
            <div class="modal-header col-md-12">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title">Schedule Handover Meeting</h4>
            </div>
            <div class="modal-body col-md-12">
               <div class="col-sm-10 col-sm-offset-1">
                    <div class="form-group col-md-12 p-l-0 hide-emp" style="display:none;">
                  <label class="col-sm-2" style="font-size:16px; padding-top:10px;">Employee:</label> 
                <div class="col-md-10 " style="padding-right:0px;">
                <select class="js-example-basic-single " name="state">
                        <option>Varun</option>
                        <option >Shrutika</option>
                </select>
                </div>
                </div>
                  <div class="form-group col-sm-12 p-l-0">
                      <label class=" col-sm-2" style="font-size:16px; padding-top:10px;"> Time:</label>  
                     <div class='input-group date col-sm-10' id='datetimepicker2'>
                         
                        <input type='text' class="form-control" />
                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                     </div>
                  </div>
                  <div class="form-group col-md-12 p-l-0">
                     <label class="col-sm-2" style="font-size:16px; padding-top:10px;">Notes:</label> 
                     <textarea class=" col-md-10" rows="3" style="resize:none"></textarea>     
                  </div>
                 
            </div>
            <div class="modal-footer col-md-12" style="border:0 !important">
               <button tyle="submit" class="btn btn-primary text-center m-t-30 ">Invite</button>
            </div>
         </div>
      </div>
   </div>
   </div>
   <!--  End of Meeting Invite Modal content-->


