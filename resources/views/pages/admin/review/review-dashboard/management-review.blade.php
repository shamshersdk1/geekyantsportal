  <div class="col-md-6  user-list-view" >
   <div class="panel panel-default">
      <div class="panel-heading redBox">
        Review Request by Management
      </div>
      <table class="table table-striped">
         <!-- <thead>
            <tr>
                <th colspan="3"></th>
            </tr>
            </thead> -->
         <tbody>
            <tr>
               <th >Employee</th>
               <th>Reporting Manager</th>
               <th>Start Date</th>
               <th class="text-right">Action</th>
            </tr>
            <tr>
               <td>
                  Akarsh Srivastava
               </td>
               <td>
                 Saurabh Sahu
               </td>
               <td>22th Jan 2018</td>
               <td class="text-right">
                  <a href="{{ url('/admin/salary-changes') }}" class="btn btn-primary btn-sm"  >Process Review</a>
               </td>
            </tr>
            <tr>
               <td>
                  Akshay Jain
               </td>
               <td>
                  Varun Sahu
               </td>
               <td>22th Jan 2018</td>
               <td class="text-right">
                  <a href="{{ url('/admin/salary-changes') }}" class="btn btn-primary btn-sm"  >Process Review</a>
               </td>
            </tr>
         </tbody>
      </table>
   </div>
</div>
<!--  End of Add  feedback  content-->


