@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Goal Assignment</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li>Assign Goals</li>
        		</ol>
            </div>
		</div>
	</div>
    <div class="row">
        <div class="col-md-7">
            <form>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="form-group"> 
                            <h4 class="no-margin">Megha Kumari</h4>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="">
                                    <label>From</label>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <select class="form-control">
                                                <option>Jan</option>
                                                <option>Feb</option>
                                                <option selected="selected">Mar</option>
                                                <option>April</option>
                                                <option>May</option>
                                                <option>June</option>
                                                <option>July</option>
                                                <option>Aug</option>
                                                <option>Sep</option>
                                                <option>Oct</option>
                                                <option>Nov</option>
                                                <option>Dec</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-6">
                                            <select class="form-control">
                                                <option>2016</option>
                                                <option>2017</option>
                                                <option selected="selected">2018</option>
                                                <option>2019</option>
                                                <option>2020</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="">
                                    <label>To</label>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <select class="form-control">
                                                <option>Jan</option>
                                                <option>Feb</option>
                                                <option selected="selected">Mar</option>
                                                <option>April</option>
                                                <option>May</option>
                                                <option>June</option>
                                                <option>July</option>
                                                <option>Aug</option>
                                                <option>Sep</option>
                                                <option>Oct</option>
                                                <option>Nov</option>
                                                <option>Dec</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-6">
                                            <select class="form-control">
                                                <option>2016</option>
                                                    <option>2017</option>
                                                    <option>2018</option>
                                                    <option selected="selected">2019</option>
                                                    <option>2020</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div>  
                    <div class="m-t-10">
                        <div class="panel-group custom-panel-group" id="gaols" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default relative">
                                <div class="panel-heading" role="tab" id="goal_1">
                                    <a role="button" data-toggle="collapse" data-parent="#gaols" href="#goal_1_content" aria-expanded="true" aria-controls="goal_1_content" class="remove-icon-wrap">
                                        <div class="row">
                                        <div class="col-sm-5">Goal 1</div>
                                        <div class="col-sm-4">
                                            <label class="label label-primary">Manager</label> 
                                            <label class="label label-primary">Developer</label>
                                        </div>
                                        <div class="col-sm-3"><span class="badge">3 Projects</span></div>
                                        </div>
                                        <span class="clickable remove-icon" data-effect="fadeOut">
                                        <i class="fa fa-times"></i>
                                        </span>
                                    </a>
                                </div>
                                <div id="goal_1_content" class="panel-collapse collapse" role="tabpanel" aria-labelledby="goal_1">
                                    <div class="panel-body">
                                    @include('pages.admin.goals.form.goal-form')
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default relative">
                                <div class="panel-heading" role="tab" id="goal_1">
                                    <a role="button" data-toggle="collapse" data-parent="#gaols" href="#goal_2_content" aria-expanded="true" aria-controls="goal_2_content" class="remove-icon-wrap">
                                        <div class="row">
                                            <div class="col-sm-5">Goal 2</div>
                                            <div class="col-sm-4">
                                                <label class="label label-primary">Manager</label> 
                                                <label class="label label-primary">Designer</label>
                                            </div>
                                            <div class="col-sm-3"><span class="badge">3 Projects</span></div>
                                        </div>
                                        <span class="clickable remove-icon" data-effect="fadeOut">
                                        <i class="fa fa-times"></i>
                                        </span>
                                    </a>
                                </div>
                                <div id="goal_2_content" class="panel-collapse collapse" role="tabpanel" aria-labelledby="goal_1">
                                    <div class="panel-body">
                                    @include('pages.admin.goals.form.goal-form')
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default relative">
                                <div class="panel-heading" role="tab" id="goal_2">
                                    <a role="button" data-toggle="collapse" data-parent="#gaols" href="#goal_3_content" aria-expanded="true" aria-controls="goal_3_content" class="remove-icon-wrap">
                                        <div class="row">
                                            <div class="col-sm-12">IDP 1</div>
                                        </div>
                                        <span class="clickable remove-icon" data-effect="fadeOut">
                                        <i class="fa fa-times"></i>
                                        </span>
                                    </a>
                                </div>
                                <div id="goal_3_content" class="panel-collapse collapse" role="tabpanel" aria-labelledby="goal_2">
                                    <div class="panel-body">
                                    @include('pages.admin.goals.goal-idp.idp-form')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <a href="{{ url('admin/goal-user-to-confirm') }}" class="btn btn-success pull-right">
                            <i class="fa fa-fw fa-lock"></i> Confirm and Lock Golas
                        </a>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-5">
            <div class="list-group">
                <h5>Previous comments</h5><div class="list-group-item">
                    <div><small class="text-primary">22th April 2018</small></div>
                    Previous Comments
                </div>
                <div class="list-group-item">
                    <div class="text-primary"><small class="text-primary">22th April 2018</small></div>
                    Previous Comments
                </div>
            </div>

            <div class="">
                <label>Add comment</label>
                <form class="panel panel-default">
                    <textarea class="form-control" rows="4"></textarea>
                    <div class="panel-footer text-right">
                        <button class="btn btn-success" type="submit">
                            <i class="fa fa-plus"></i> Add
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection