<form name="userGoalForm[%%$index%%]">
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <label class="col-sm-2" style="margin-left:-12px;">Goal</label>
                <input type="text" class="form-control" name="title" placeholder="Main goal" ng-model="form.data.goals[$index].title" required
                ng-disabled="userGoal.status == 'submitted' || userGoal.status == 'running' ">
            </div>
        </div>            
    </div>             
    <div class="row">                    
        <div class="col-sm-6">    
            <div class="form-group">
                <label for="">Tag:</label>
                <ui-select ng-model="form.data.goals[$index].tags" multiple theme="bootstrap" name="selectid2" ng-disabled="userGoal.status == 'submitted' || userGoal.status == 'running'">
                    <ui-select-match >%%$item%%</ui-select-match>
                    <ui-select-choices repeat="tag in data.tags | filter: $select.search">
                        %%tag%%
                    </ui-select-choices>
                </ui-select>
            </div>
        </div>    
        <div class="col-sm-6">    
            <div class="form-group">
                <label>Success Metrics</label>
                <input type="text" class="form-control" name="success_metric" placeholder="" ng-model="form.data.goals[$index].success_metric" required
                       ng-style="form.errors[$index].indexOf('success_metric')>-1 && {border: '1px solid red'}"
                       ng-disabled="userGoal.status == 'submitted' || userGoal.status == 'running'">
            </div>
        </div>
    </div> 
    <div class="row">    
        <div class="col-sm-12">
            <label>Process</label>
            <textarea class="form-control" rows="10" name="process" ng-disabled="userGoal.status == 'submitted' || userGoal.status == 'running'" 
                      placeholder="How to achieve the goal" ng-model="form.data.goals[$index].process" required></textarea>
        </div>
    </div>
</form>

@section('js')
@parent


@endsection