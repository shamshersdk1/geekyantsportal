<form name="newForm"  novalidate style="padding-top:15px">
<div class="label-name"><span class="label label-success custom-label pull-right">Goal</span></div>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
               <!-- <div class="label-name"> <label style="margin-left:-12px;">Goal</label></div> -->
                <input type="text" class="form-control" name="title" placeholder="Main goal" ng-model="form.data.newGoal.title" required
                       ng-style="newForm.title.$invalid && submitted && {border: '1px solid red'}">
            </div>
          
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="">Tag:</label>
                <ui-select ng-model="form.data.newGoal.tags" multiple theme="bootstrap" name="selectid2" ng-style="form.data.newGoal.tags.length == 0 && submitted && {border: '1px solid red'}">
                    <ui-select-match>%%$item%%</ui-select-match>
                    <ui-select-choices repeat="tag in data.tags | filter: $select.search">
                        %%tag%%
                    </ui-select-choices>
                </ui-select>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label>Success Metricss</label>
                <input type="text" class="form-control" name="success_metric" placeholder="" ng-model="form.data.newGoal.success_metric"
                    required ng-style="newForm.success_metric.$invalid && submitted && {border: '1px solid red'}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <label>Process</label>
            <textarea class="form-control" rows="10" name="process" placeholder="How to achieve the goal" ng-model="form.data.newGoal.process"
                      ng-style="newForm.process.$invalid && submitted && {border: '1px solid red'}" required></textarea>
        </div>
    </div>
</form>
@section('js') @parent @endsection