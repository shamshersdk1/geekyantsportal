<form name="newIDPForm" class="form-horizontal" novalidate>
    <div class="label-name">
    <span class="label label-info custom-label">IDP</span>

    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group col-sm-12">
                <label>Individual Development Program</label>
                <input type="text" class="form-control" name="title" placeholder="IDP" ng-model="form.data.newGoal.title" required 
                       ng-style="newIDPForm.title.$invalid && submitted && {border: '1px solid red'}">
            </div>
            <div class="form-group col-sm-2 pull-right">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <label>Process</label>
            <textarea class="form-control" name="process" rows="10" placeholder="How to achieve the goal" ng-model="form.data.newGoal.process"
                ng-style="newIDPForm.process.$invalid && submitted && {border: '1px solid red'}" required></textarea>
        </div>
    </div>
</form>
@section('js') @parent @endsection