<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            <label>Individual Development Programe</label> 
            <input type="text" class="form-control" placeholder="IDP" ng-model="form.data.goals[$index].title" ng-style="form.errors[$index].indexOf('title')>-1 && {border: '1px solid red'}"
                   ng-disabled="userGoal.status == 'running'">
        </div>
    </div>            
</div>             
<div class="row">    
    <div class="col-sm-12">
        <label>Process</label>
        <textarea class="form-control" rows="10" placeholder="How to achieve the goal" ng-model="form.data.goals[$index].process"
                  ng-style="form.errors[$index].indexOf('process')>-1 && {border: '1px solid red'}"
                  ng-disabled="userGoal.status == 'running'"></textarea>
    </div>
</div>

@section('js')
@parent
@endsection