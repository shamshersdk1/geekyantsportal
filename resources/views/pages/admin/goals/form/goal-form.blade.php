<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            <label class="col-sm-2" style="margin-left:-12px;">Goal</label>
            <input type="text" class="form-control" name="title" placeholder="Main goal" @if(!empty(old('title')))value="{{old('title')}}" @elseif(!empty($goal->title))value="{{$goal->title}}"@endif required>
        </div>
    </div>            
</div>             
<div class="row">                    
    <div class="col-sm-6">    
        <div class="form-group">
            <label for="">Tag:</label>
            <input class="module-tag" name="tags" multiple="multiple"/>
        </div>
    </div>    
    <div class="col-sm-6">    
        <div class="form-group">
            <label>Success Metrics</label>
            <input type="text" class="form-control" name="success_metric" placeholder="" @if(!empty(old('success_metric')))value="{{old('success_metric')}}" @elseif(!empty($goal->success_metric))value="{{$goal->success_metric}}"@endif required>
        </div>
    </div>
</div> 
<div class="row">    
    <div class="col-sm-12">
        <label>Process</label>
        <textarea class="form-control" rows="10" name="process" placeholder="How to achieve the goal" required>@if(!empty(old('process'))){{old('process')}} @elseif(!empty($goal->process)){{$goal->process}}@endif</textarea>
    </div>
</div>

@section('js')
@parent
<script type="text/javascript">
   $(document).ready(function() {
        $('.module-tag').select2({
            @if(!empty($tags))
            tags: {{$tags}}
            @endif
        });
   });
</script>
@endsection