@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">List of Goals</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li>Main Goals</li>
        		</ol>
            </div>
			<div class="col-sm-4 text-right m-t-10">
                <a href="/admin/goals/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i> Add Goal</a>
			</div>
		</div>
	</div>
    <!-- <form class="form-inline">
            <div class="form-group">
                <label class="goals-current"for="exampleFormControlSelect1">From</label>
                <select class="form-control" id="exampleFormControlSelect1">
                    <option>2010</option>
                    <option>2011</option>
                    <option>2012</option>
                    <option>2013</option>
                    <option>2014</option>
                    <option>2015</option> 
                    <option>2016</option>
                    <option>2017</option>
                    <option>2018</option>
                    <option>2019</option>
                    <option>2020</option>
                </select>
            </div>
            <div class="form-group">
                <label class="goals-current" for="exampleFormControlSelect2">To</label>
                <select class="form-control" id="exampleFormControlSelect2">
                    <option>2010</option>
                    <option>2011</option>
                    <option>2012</option>
                    <option>2013</option>
                    <option>2014</option>
                    <option>2015</option> 
                    <option>2016</option>
                    <option>2017</option>
                    <option>2018</option>
                    <option>2019</option>
                    <option>2020</option>
                </select>
            </div>
            <button type="text" class="btn btn-primary goals-search btn-sm">Search</button>
    </form> -->
    <div class="row">
        <div class="col-sm-12">
            @if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                    @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif 
        </div>
    </div>

    <div class="panel panel-default goals-list-margin">
        <table class=" table">
            <thead>
                <tr>
                    <th width="15%" >Goal</th>
                    <th width="40%">Process</th>
                    <th width="10%">Success Metrics</th>
                    <th width="17%">Tag</th>
                    <th width="8%" class="text-right">Action</th>
                </tr> 
            </thead>
            <tbody>
                @if(count($goals) > 0 )
                    @foreach($goals as $goal)
                        <tr>
                            <td>{{$goal->title}}</td>
                            <td style="white-space: normal; word-break: break-all;">{{$goal->process}}</td>
                            <td>{{$goal->success_metric}}</td>
                            <td>
                                @if(!empty($goal->tags))
                                    @foreach($goal->tags as $tag)
                                        <span class="label label-primary custom-label">{{$tag->name}}</span>
                                    @endforeach
                                @endif  
                            </td>
                            <td class="text-right">
                                <div style="max-width: 80px;" class="pull-right">
                                    <a href="{{ url('admin/goals/'.$goal->id) }}" class="btn btn-success btn-sm m-b-5 btn-block">
                                        <i class="fa fa-eye"></i> View
                                    </a>
                                    <a href="{{ url('admin/goals/'.$goal->id.'/edit') }}" class="btn btn-default  btn-sm">
                                        <i class="fa fa-edit text-warning"></i> 
                                    </a>
                                    <a href="{{ url('admin/goals/'.$goal->id.'/delete') }}" class="btn btn-default text-danger btn-sm delete-btn">
                                        <i class="fa fa-trash"></i> 
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">No Records Found</td>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>	

<script>
    $(function() {
        $(".delete-btn").on('click', function() {
            return confirm("Are you sure?");
        });
    });
</script>
@endsection
