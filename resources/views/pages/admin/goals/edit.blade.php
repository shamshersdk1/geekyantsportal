@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Edit Goal</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/goals') }}">Goal List</a></li>
                    <li>Edit</li>
                    <li>{{$goal->id}}</li>
        		</ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
        </div>
	</div>
    <div class="row">
        <div class="col-sm-12">
            @if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                    @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif 
        </div>
    </div>
    <form action="/admin/goals/{{$goal->id}}" method="post">
        <div class="panel panel-default">
            <div class="panel-body">
                {{csrf_field()}}
                {{method_field('PUT')}}
                @include('pages.admin.goals.form.goal-form')
            </div>
        </div>
        <div class="text-center">
           <button type="reset" class="btn btn-default"><i class="fa fa-rotate-left fa-fw"></i> Clear</button> 
           <button type="submit" class="btn btn-success"><i class="fa fa-plus fa-fw"></i> Save</button>
        </div>
    </form>

    <script>
        $(function() {
            $(".module-tag").val({{$selected}});
        });
    </script>
@endsection

