@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="flex-class align-items-center">
            <div>
                <h1 class="admin-page-title">List of Goals</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li>My Goals</li>
        		</ol>
            </div>
		</div>
	</div>
    
    <div class="row">
        <div class="col-sm-12">
            @if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                    @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif 
        </div>
    </div>
    <div class="panel panel-default goals-list-margin">
       
            <table class="table">
                <thead>
                    <tr>
                        <th width="5%" >#</th>
                        <th width="20%">From Date</th>
                        <th width="20%">To Date</th>
                        <th width="20%">Status</th>
                        <th width="20%" class="text-right">Action</th>
                    </tr> 
                </thead>
                <tbody>
                    @if(count($goals) > 0 )
                        @foreach($goals as $goal)
                            <tr>
                                <td>{{$goal->id}}</td>
                                <td>{{ date_in_view($goal->from_date)}}</td>
                                <td>{{ date_in_view($goal->to_date)}}</td>
                                <td>{{$goal->status}}</td>
                                <td class="text-right">
                                    <a href="{{ url('admin/goal-user-view/'.$goal->id) }}" class="btn btn-info btn-sm">
                                        <i class="fa fa-eye"></i> View
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5">No Records Found</td>
                        </tr>
                    @endif
                </tbody>
            </table>
       
    </div>
</div>	
@endsection
