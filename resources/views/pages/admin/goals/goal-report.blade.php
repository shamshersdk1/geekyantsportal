@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Goal Report</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
                    <li><a href="/admin/goals">Goals</a></li>
        		  	<li>Reports</li>
        		</ol>
            </div>
		</div>
	</div>
    
    <div class="row">
        <div class="col-sm-12">
            @if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                    @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif 
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 bg-white search-section-top">
            <form action="/admin/goal-report/month"  method="get" class="row">
                <div class="col-sm-8">     
                    <select  class="form-control" id="month" name="id"  mulitple>
                    <option selected="true" disabled="disabled">Select a Month</option>    
                        @php 
                            for ($i = 0; $i < 12; $i++) {
                            $time = strtotime(sprintf('%d months', $i));   
                            $label = date('F', $time);   
                            $value = date('n', $time);
                            echo "<option value='$value'>$label</option>";
                            }
                        @endphp
                    </select>                              
                </div>
                <div class="col-sm-2">
                    <span class="input-group-btn text-right">
                        <button type="submit" style="margin-right:15px" class="btn btn-primary btn-sm button-space">Search</button>
                        <a href="/admin/goal-report" class="btn btn-primary btn-sm button-space">Reset</a>
                    </span>
                </div>  
            </form>
        </div>
    </div> 

    <div class="panel panel-default goals-list-margin">
        <table class=" table ">
            <thead>
                <tr>
                    <th width="15%" >Name</th>
                    <th width="10%">From Date</th>
                    <th width="10%">To Date</th>
                    <th width="20%">Status</th>
                    <th width="20%" class="text-right">Action</th>
                </tr> 
            </thead>
            <tbody>
                @if(count($user_goals) > 0 )
                    @foreach($user_goals as $goal)
                        <tr>
                            <td>{{$goal->user->name}}</td>
                            <td>{{ date_in_view($goal->from_date)}}</td>
                            <td>{{ date_in_view($goal->to_date)}}</td>
                            <td>{{$goal->status}}</td>
                            <td class="text-right">
                                <a href="{{ url('admin/goal-user-view/'.$goal->id) }}" class="btn btn-info btn-sm">
                                    <i class="fa fa-eye"></i> View
                                </a>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">No Records Found</td>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>	
@endsection
