@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">{{$goal->title}}</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
                    <li><a href="/admin/goals">List of Goals</a></li>
        		  	<li>Main Goals</li>
        		</ol>
            </div>
			<div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
		</div>
	</div>
    <div class="row">
    <div class="col-sm-5">

   
    <h4 class=" admin-section-heading">{{$goal->title}}</h4>

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="info-table">
                <label>Success Metrics:</label>
                <span >{{$goal->success_metric}}</span>
            </div>
                <div class="info-table">
                    @if(!empty($goal->tags))
                    <label>For:</label>
                    <span>
                        @foreach($goal->tags as $tag)
                            <label class="label label-primary custom-label" style="color:#fff;margin-right:5px; display:inline-block;width:auto;">{{$tag->name}}</label>
                        @endforeach
                    @endif 
</span>
                </div>
                <div class="info-table">
                <label>Process:</label>

            <span style="white-space:pre">{{$goal->process}}</span>
</div>
        </div>
    </div>
</div>
</div>
</div>	
@endsection
