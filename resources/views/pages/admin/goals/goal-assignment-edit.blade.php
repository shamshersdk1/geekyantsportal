@extends('layouts.admin-dashboard') @section('main-content')
<script type="text/javascript">
    window.userGoal = '<?php echo json_encode($userGoal); ?>';
</script>
<div class="container-fluid goal-edit-section" ng-App="myApp">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Goal Assignment</h1>
                <ol class="breadcrumb">
                    <li>
                        <a href="/admin">Admin</a>
                    </li>
                    <li>
                        <a href="/admin/goal-user-list">Users List</a>
                    </li>
                    <li>
                        <a>{{$userGoal->id}}</a>
                    </li>
                    <li>
                        <a>Edit</a>
                    </li>
                </ol>
            </div>
        </div>
    </div>
    <div class="row" ng-controller="editGoalCtrl">
        <div class="col-md-7">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-sm-5">
                        <div class="form-group row">
                            <label for="name" class=" col-sm-3 control-label text-weight">Name</label>
                            <div class="col-sm-9">
                                <input type="text" name="name" class="form-control" disabled value="{{ $user->name }}" /> </input>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <!-- <input date-range-picker class="date-picker form-control" type="text" ng-model="datePicker.date" options="datePicker.options" 
                                   ng-change="func()" disabled> -->
                        <input type="text" name="name" class="form-control" disabled value="{{ date_in_view($userGoal->from_date)}} to {{ date_in_view($userGoal->to_date)}}  "
                        /> </input>
                    </div>
                    <div class="label-name">
                        <span class="label label-info custom-label pull-right">%%userGoal.status%%</span>
                    </div>
                </div>
            </div>
            <div class="label label-info custom-label" ng-show="userGoal.status == 'submitted'">
                Waiting for the user to accept the goal
            </div>
            <button class="btn btn-success" role="button" data-toggle="collapse" href="#showForm" aria-expanded="false" aria-controls="showForm" 
                    ng-hide="userGoal.status != 'pending' && userGoal.status != 'confirmed'  ">
                <i class="fa fa-plus fa-fw"></i> Add Goal
            </button>
            <span>
                <button class="btn btn-success" role="button" data-toggle="collapse" href="#showIDP" aria-expanded="false" aria-controls="showIDP" 
                        ng-hide="userGoal.status != 'pending' && userGoal.status != 'confirmed' ">
                    <i class="fa fa-plus fa-fw"></i> Add new IDP
                </button>
            </span>
            <div>
                <div class="panel panel-default collapse m-t-10" id="showForm">
                    <div class="panel-body">
                        @include('pages.admin.goals.form.angular.goal-form-new')
                    </div>
                    <div class="panel-footer text-right">
                        <button type="submit" class="btn btn-success" ng-click="form.createGoal(false)">
                            <i class="fa fa-plus fa-fw"></i> Add</button>
                    </div>
                </div>
                <div class="panel panel-default collapse m-t-10" id="showIDP">
                    <div class="panel-body">
                        @include('pages.admin.goals.form.angular.idp-form-new')
                    </div>
                    <div class="panel-footer text-right">
                        <button type="submit" class="btn btn-success" ng-click="form.createGoal(true)">
                            <i class="fa fa-plus fa-fw"></i> Add</button>
                    </div>
                </div>
                <div class="m-t-10">
                    <div class="panel-group custom-panel-group " id="user_goals" role="tablist" aria-multiselectable="true">
                        <div ng-hide="GoalData.userGoalItemsLoaded" class="text-center" >
                            <i class="fa fa-spinner fa-spin fa-lg"></i>
                        </div>
                        <uib-accordion>
                            <div class="panel panel-default" ng-repeat="item in form.data.goals">
                                <div uib-accordion-group is-open="form.data.goals[$index].is_open">
                                    <uib-accordion-heading>
                                        <div ng-hide="item.is_idp">
                                            <div class="panel-heading">
                                                <a role="button" data-toggle="collapse" data-parent="#user_goals" href="#goal_%%item.id%%_content" aria-expanded="true" aria-controls="goal_%%item.id%%_content"
                                                   class="remove-icon-wrap" >
                                                    <div class="label-name">
                                                            <span class="label label-info custom-label">GOAL</span>
                                                        </div>
                                                    <div class="row">
                                                        <div class="col-sm-4">%%item.title%%</div>
                                                        <div class="col-sm-4" >
                                                            <label class="label label-primary" ng-repeat="tag in item.tags">%%tag%%</label>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <span class="badge" >%%item.success_metric%%</span>
                                                        </div>
                                                       
                                                    </div>
                                                    <span ng-hide="userGoal.status != 'pending' && userGoal.status != 'confirmed' " class="clickable remove-icon" ng-click="form.removeGoal($index); $event.stopPropagation();">
                                                        <i class="fa fa-times"></i>
                                                    </span>
                                                </a>
                                            </div>
                                        </div>
                                        <div ng-show="item.is_idp">
                                            <div class="panel-heading" role="tab" id="user_goal_%%item.id%%">
                                                <a role="button" data-toggle="collapse" data-parent="#user_goals" href="#goal_%%item.id%%_content" aria-expanded="true" aria-controls="goal_%%item.id%%_content"
                                                    class="remove-icon-wrap">
                                                    <div class="row">
                                                        <div class="col-sm-10">%%item.title%%</div>
                                                        <div class="col-sm-2">
                                                            <span class="label label-info custom-label">IDP</span>
                                                        </div>
                                                    </div>
                                                    <span ng-hide="userGoal.status != 'pending' && userGoal.status != 'confirmed' " class="clickable remove-icon" ng-click="form.removeGoal($index); $event.stopPropagation();" data-effect="fadeOut">
                                                        <i class="fa fa-times"></i>
                                                    </span>
                                                </a>

                                            </div>
                                        </div>
                                    </uib-accordion-heading>
                                    <div ng-hide="item.is_idp">
                                        <div class="panel-body">
                                            @include('pages.admin.goals.form.angular.goal-form-edit')
                                        </div>
                                    </div>
                                    <div ng-show="item.is_idp">
                                        <div class="panel-body">
                                            @include('pages.admin.goals.form.angular.idp-form-edit')
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </uib-accordion>
                        <!-- <div class="panel panel-default relative">
                                <div class="panel-heading" role="tab" id="goal_fixed_idp">
                                    <a role="button" data-toggle="collapse" data-parent="#gaols" href="#goal_fixed_idp_content" aria-expanded="true" aria-controls="goal_fixed_idp_content" class="remove-icon-wrap">
                                        <div class="row">
                                            <div class="col-sm-12">IDP 1</div>
                                        </div>
                                        <span class="clickable remove-icon" data-effect="fadeOut">
                                        <i class="fa fa-times"></i>
                                        </span>
                                    </a>
                                </div>
                                <div id="goal_fixed_idp_content" class="panel-collapse collapse" role="tabpanel" aria-labelledby="goal_fixed_idp">
                                    <div class="panel-body">
                                    @include('pages.admin.goals.goal-idp.idp-form')
                                    </div>
                                </div>
                            </div> -->
                    </div>
                </div>
                <div ng-show="userGoal.status == 'confirmed'">
                    <a class="btn btn-primary pull-right btn-spinner-md" ng-click="form.store('running')">
                        <div ng-if="!freezeGoalLoading">Freeze Goal</div>
                        <div ng-if="freezeGoalLoading"><i class="fa fa-spinner fa-spin fa fa-fw"></i></div>
                    </a>
                    <a class="btn btn-danger pull-right btn-spinner-lg" style="margin-right: 10px" ng-click="form.store('submitted')">
                        <div ng-if="!setGoalLoading">Send back to User</div>
                        <div ng-if="setGoalLoading"><i class="fa fa-spinner fa-spin fa fa-fw"></i></div>
                    </a>
                </div>
                <div ng-show="userGoal.status == 'pending'">
                    <a class="btn btn-primary pull-right btn-spinner-md" ng-click="form.store('submitted')">
                        <div ng-if="!setGoalLoading">Set Goal</div>
                        <div ng-if="setGoalLoading"><i class="fa fa-spinner fa-spin fa fa-fw"></i></div>
                    </a>
                    <a class="btn btn-success pull-right btn-spinner-lg" style="margin-right: 10px" ng-click="form.store('pending')">
                        <div ng-if="!saveAsDraftLoading">Save as Draft</div>
                        <div ng-if="saveAsDraftLoading"><i class="fa fa-spinner fa-spin fa fa-fw"></i></div>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-5" ng-show="userGoal.status == 'pending' || userGoal.status == 'confirmed' ">
            <div class="row form-group">
                <div class="col-sm-6">
                    <label for="">Search:</label>
                    <input type="text" name="" class="form-control" ng-model="search.text" ng-model-options="{ debounce: 500 }" ng-change="filter()"
                    />
                </div>
                <div class="col-sm-6">
                    <label for="">Tag:</label>
                    <ui-select ng-model="search.tags" multiple theme="bootstrap" name="selectid2" ng-change="filter()">
                        <ui-select-match>%%$item%%</ui-select-match>
                        <ui-select-choices repeat="tag in data.tags | filter: $select.search">
                            %%tag%%
                        </ui-select-choices>
                    </ui-select>
                    
                </div>
            </div>
            <div class="panelpanel-default1" ng-hide="userGoal.status == 'running' ">
                <div class="panel-group custom-panel-group" id="goals" role="tablist" aria-multiselectable="true">
                    <!-- <div class="panel panel-default relative">
                      <div class="panel-heading" role="tab" id="module_2">
                        <a role="button" data-toggle="collapse" data-parent="#gaols" href="#goal_fixed_content_right" aria-expanded="true" aria-controls="goal_fixed_content_right" class="remove-icon-wrap add-wrap">
                            <div class="row">
                                <div class="col-sm-6">Goal 2</div>
                                    <div class="col-sm-4">
                                      <label class="label label-primary">Manager</label> 
                                      <label class="label label-primary">Developer</label>
                                    </div>
                                <div class="col-sm-2"><span class="badge">3 Projects</span></div>
                            </div>
                            <span class="clickable add-icon" data-effect="fadeOut">
                                <i class="fa fa-plus"></i>
                            </span>
                        </a>
                      </div>
                      <div id="goal_fixed_content_right" class="panel-collapse collapse" role="tabpanel" aria-labelledby="module_2">
                         <div class="panel-body">
                            Goal description
                         </div>
                      </div>
                   </div> -->
                    <div ng-hide="GoalData.loaded" class="text-center" style="padding:20px 0">
                        <i class="fa fa-spinner fa-spin fa-lg"></i>
                    </div>
                    <div class="panel panel-default relative" ng-repeat="goal in data.goals">
                        <div class="panel-heading" role="tab" id="goal_%%goal.id%%">
                            <a role="button" data-toggle="collapse" data-parent="#goals" href="#goal_%%goal.id%%_content_right" aria-expanded="true"
                                aria-controls="goal_%%goal.id%%_content_right" class="remove-icon-wrap add-wrap">
                                <div class="row">
                                    <div class="col-sm-6">%%goal.title%%</div>
                                    <div class="col-sm-4" >
                                        <label class="label label-primary" ng-repeat="tag in goal.tags">%%tag.name%%</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <span class="badge col-sm-12">%%goal.success_metric%%</span>
                                    </div>
                                </div>
                                <span class="clickable add-icon" data-effect="fadeOut" ng-click="form.addGoal($index); $event.stopPropagation();">
                                    <i class="fa fa-plus"></i>
                                </span>
                            </a>
                        </div>
                        <div id="goal_%%goal.id%%_content_right" class="panel-collapse collapse" role="tabpanel" aria-labelledby="goal_%%goal.id%%">
                            <div class="panel-body" style="white-space:pre">%%goal.process%%</div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<script type="text/javascript">
    $(function () {
        $('.module-tag').select2({
            @if(!empty($tags))
            tags: {
                {
                    $tags
                }
            }
            @endif
        });
    });
</script>
@endsection