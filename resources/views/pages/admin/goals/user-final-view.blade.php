@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Manager year end view</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/goals') }}">Goal List</a></li>
                    <li>Create</li>
                </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
        </div>
    </div>
    
    <form>
        <div class="row">    
            <div class="col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4 style="margin-top:0;">Goals to achive</h4>
                        <h5 class="no-margin">From April 2017 To March 2018</h5>
                    </div>
                </div>
                <div class="panel-group custom-panel-group" id="gaols" role="tablist" aria-multiselectable="true">
                   <div class="panel panel-default relative">
                      <div class="panel-heading" role="tab" id="goal_1">
                        <a role="button" data-toggle="collapse" data-parent="#gaols" href="#goal_1_content_right" aria-expanded="true" aria-controls="goal_1_content_right" class="remove-icon-wrap add-wrap">
                            <div class="row">
                                <div class="col-sm-6">Goal 1</div>
                                    <div class="col-sm-4">
                                      <label class="label label-primary">Manager</label> 
                                      <label class="label label-primary">Developer</label>
                                    </div>
                                <div class="col-sm-2"><span class="badge">3 Projects</span></div>
                            </div>
                            
                        </a>
                      </div>
                      <div id="goal_1_content_right" class="panel-collapse collapse" role="tabpanel" aria-labelledby="goal_1">
                         <div class="panel-body">
                            goal description
                         </div>
                      </div>
                      <div class="panel-footer">
                            <div class="row">
                                <div class="col-sm-3">
                                    <div>
                                        <label>Success Matrix</label><br />
                                        <input type="" name="" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-9">
                                    <label>Review</label><br />
                                    <textarea class="form-control" rows="5"></textarea>
                                </div>
                            </div>
                       </div>
                   </div>
                   
                   <div class="panel panel-default relative">
                      <div class="panel-heading" role="tab" id="module_2">
                        <a role="button" data-toggle="collapse" data-parent="#gaols" href="#goal_2_content_right" aria-expanded="true" aria-controls="goal_2_content_right" class="remove-icon-wrap add-wrap">
                            <div class="row">
                                <div class="col-sm-6">Goal 2</div>
                                    <div class="col-sm-4">
                                      <label class="label label-primary">Manager</label> 
                                      <label class="label label-primary">Developer</label>
                                    </div>
                                <div class="col-sm-2"><span class="badge">3 Projects</span></div>
                            </div>
                        </a>
                      </div>
                      <div id="goal_2_content_right" class="panel-collapse collapse" role="tabpanel" aria-labelledby="module_2">
                         <div class="panel-body">
                            goal description
                         </div>
                      </div>
                      <div class="panel-footer">
                            <div class="row">
                                <div class="col-sm-3">
                                    <div>
                                        <label>Success Matrix</label><br />
                                        <input type="" name="" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-9">
                                    <label>Review</label><br />
                                    <textarea class="form-control" rows="5"></textarea>
                                </div>
                            </div>
                       </div>
                      
                   </div>
                
                    <div class="panel panel-default">
                      <div class="panel-heading" role="tab" id="module_3">
                        <a role="button" data-toggle="collapse" data-parent="#gaols" href="#goal_3_content_right" aria-expanded="true" aria-controls="goal_3_content_right" class="remove-icon-wrap add-wrap">
                            <h5 class="panel-title text-primary">My IDP</h5>
                        </a>
                      </div>
                      <div id="goal_3_content_right" class="panel-collapse collapse" role="tabpanel" aria-labelledby="module_3">
                         <div class="panel-body">
                            goal description
                         </div>
                      </div>
                          <div class="panel-footer">
                            <label>Review</label><br />
                            <textarea class="form-control" rows="5"></textarea>
                           </div>
                          
                       </div>
                   </div>
                <div class="text-right">
                   <button type="submit" class="btn btn-success pull-left"><i class="fa fa-check fa-fw"></i> Final Submit</button>

                   <button type="reset" class="btn btn-default"><i class="fa fa-rotate-left fa-fw"></i> Clear</button> 
                   <button type="submit" class="btn btn-success"><i class="fa fa-plus fa-fw"></i> Save</button>
                   
                </div>         
            </div>
        </div>
    </form>
@endsection

