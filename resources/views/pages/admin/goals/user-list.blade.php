@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Users</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li><a>Users List</a></li>
        		</ol>
            </div>
		</div>
	</div>
	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
	<div id="exampleAccordion" data-children=".item">
		@if( !empty($users) )
			@foreach( $users as $i => $user )
				<div class="item">
					<div data-toggle="collapse" class="panel" style="padding: 10px;" data-parent="#exampleAccordion" 
						 href="#exampleAccordion{{$i}}" role="button" aria-expanded="false" aria-controls="exampleAccordion{{$i}}">
						<span>{{$user->employee_id}}</span><span style="margin-left: 30px">{{ $user->name }}</span>
						<i class="more-less glyphicon glyphicon-plus pull-right"></i>
					</div>
					<div id="exampleAccordion{{$i}}" class="collapse" role="tabpanel" style="margin-top: 20px">
						<div  >
						@if ( !empty($user->goals) )
							<table class="table table-striped">
								@foreach($user->goals as $index => $goal)
									<tr>
										<td class="text-center">{{ $index + 1 }}</td>
										<td class="text-center"> {{ !empty($goal->name) ? $goal->name: '' }} </td>
										<td class="text-center"> {{ date_in_view($goal->from_date) }} </td>
										<td class="text-center"> {{ date_in_view($goal->to_date) }} </td>
										<td class="text-center"> {{ $goal->status }} </td>
										<td class="text-center"> {{ date_in_view($goal->created_at) }}  </td>
										<td class="text-center">
												@if($goal->status == 'submitted' || $goal->status == 'confirmed' )
													<a type="button" href="/admin/goal-user-list/{{$goal->user_id}}/{{$goal->id}}/view" class="btn btn-info btn-sm crude-btn">View</a>
												@endif
												@if($goal->status == 'pending' || $goal->status == 'confirmed' )
													<a type="button" href="/admin/goal-user-list/{{$goal->user_id}}/{{$goal->id}}/edit" class="btn btn-primary btn-sm crude-btn">Edit</a>
												@endif
												@if($goal->status == 'running')
													<a type="button" href="/admin/goal-user-list/{{$goal->user_id}}/{{$goal->id}}/review" class="btn btn-success btn-sm crude-btn">Review</a>
												@endif
												@if($goal->status == 'completed')
													<a type="button" href="/admin/goal-user-list/{{$goal->user_id}}/{{$goal->id}}/review" class="btn btn-success btn-sm crude-btn">View Details</a>
												@endif
										</td>
									</tr>
								@endforeach
								</table>
						@else
							<div style="background-color: #EED4D4; padding: 20px"> No records found </div>
						@endif
					</div>
					</div>
				</div>
			@endforeach
		@endif
	</div>
</div>	
@endsection

