@extends('layouts.admin-dashboard') @section('main-content')
<script type="text/javascript">
    window.userGoal = '<?php echo json_encode($userGoal); ?>';
</script>
<div class="container-fluid" ng-App="myApp">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Manager year end view</h1>
                <ol class="breadcrumb">
                    <li>
                        <a href="/admin">Admin</a>
                    </li>
                    <li>
                        <a href="/admin/goal-user-list">Users List</a>
                    </li>
                    <li>
                        <a>{{$userGoal->id}}</a>
                    </li>
                    <li>
                        <a>Review</a>
                    </li>
                </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default">
                    <i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
        </div>
    </div>

    <form>
        <div class="row" ng-controller="reviewGoalCtrl">
            <div class="col-sm-7">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4 style="margin-top:0;">{{$user->name}}
                            <span class="pull-right">
                            <button ng-hide="userGoal.status == 'completed'" type="submit" class="btn btn-success btn-spinner-md" style="text-align: center" ng-click="completeGoal()">
                                <div ng-if="!completeLoading">Complete</div>
                                <div ng-if="completeLoading"><i class="fa fa-spinner fa-spin fa fa-fw"></i></div>
                            </button>
                            </span></h4>
                        <h5 class="no-margin">{{date_in_view($userGoal->from_date)}} to {{date_in_view($userGoal->to_date)}}
                            <span class="label label-info custom-label pull-right">%%userGoal.status%%</span>
                        </h5>
                        
                    </div>
                </div>
                <div class="panel panel-default relative" ng-repeat="item in form.data.goals">
                    <div class="panel-heading" role="tab" id="goal_%%item.id%%">
                        <a role="button" data-toggle="collapse" data-parent="#goals" href="#goal_%%item.id%%_content_right" aria-expanded="true"
                            aria-controls="goal_%%item.id%%_content_right" class="remove-icon-wrap add-wrap">
                            <div class="row">
                                <div class="col-sm-6">%%item.title%%</div>
                                <div class="col-sm-4" ng-show="!item.is_idp">
                                    <label class="label label-primary" ng-repeat="tag in item.tags">%%tag%%</label>
                                </div>
                                <div class="col-sm-2">
                                    <span class="label label-info custom-label" ng-show="!item.is_idp">GOAL</span>
                                    <span class="label label-info custom-label" ng-show="item.is_idp">IDP</span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div id="goal_%%item.id%%_content_right" class="panel-collapse collapse" role="tabpanel" aria-labelledby="goal_%%item.id%%">
                        <div class="panel-body" style="white-space:pre">%%item.process%%</div>
                    </div>
                    <div class="panel-footer">
                        <div class="user-review">
                            <div class="form-group">
                                <label>Employee Review</label>
                                <div>TO be decided</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8">
                                <label>Review</label>
                                <br />
                                <textarea class="form-control" ng-model="form.data.goals[$index].review_comment" 
                                            ng-model-options="{ debounce: 500 }" ng-change="updateReview(item)" rows="5" ng-disabled="disableFields"></textarea>
                            </div>
                            <div class="col-sm-4">
                                <div>
                                    <label>Success Matrix</label>
                                    <br />
                                    <input value="%%item.success_metric%%" class="form-control" ng-disabled="disableFields">
                                </div>
                                <div class="m-t-10">
                                    <label>Points</label>
                                    <br />
                                    <input type="text" name="points" ng-model="form.data.goals[$index].points" class="form-control" 
                                            ng-model-options="{ debounce: 200 }" ng-change="updatePoints(item)" ng-disabled="disableFields">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" ng-if="item.id">
                        <comment-data commentable-id="item.id" commentable-type="user_goal_item" is-disabled="%%disableFields%%"></comment-data>
                    </div>
                </div>

                <div class="panel panel-default" ng-show="display_submit">
                    <div class="panel-body">
                        <label>Overall Feedback</label>
                        <br />
                        <textarea class="form-control" ng-model="userGoal.overall_feedback" rows="5" ng-style="form.overall_feedback_error && {border: '1px solid red'}"></textarea>
                    </div>
                </div>
                <div ng-show="display_submit" class="text-right">
                    <button type="submit" class="btn btn-success" ng-click="form.store()"> Submit</button>
                </div>
                
            </div>
            <div class="col-md-5">
                <div class="row" ng-if="userGoal.id">
                    <comment-data commentable-id="userGoal.id" commentable-type="user_goal" is-disabled="%%disableFields%%"></comment-data>
                </div>
            </div>
        </div>
    </form>
    @endsection