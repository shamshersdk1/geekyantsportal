@extends('layouts.admin-dashboard') @section('main-content')
<script type="text/javascript">
    window.goal = '<?php echo json_encode($goal); ?>';
    window.user = '<?php echo json_encode($user); ?>';
</script>
<div class="container-fluid goal-edit-section" ng-App="myApp">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                @if(!$manager_flag)
                    <h1 class="admin-page-title">User View</h1>
                    <ol class="breadcrumb">
                        <li>
                            <a href="/admin">Admin</a>
                        </li>
                        <li>
                            <a href="/admin/goal-user-view">My Goals</a>
                        </li>
                        <li>
                            <a>{{$goal->id}}</a>
                        </li>
                    </ol>
                @else
                    <h1 class="admin-page-title">Manager User Goal View</h1>
                    <ol class="breadcrumb">
                        <li>
                            <a href="/admin">Admin</a>
                        </li>
                        <li>
                            <a href="/admin/goal-user-list">Users List</a>
                        </li>
                        <li>
                            <a>{{$goal->id}}</a>
                        </li>
                    </ol>
                @endif
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default">
                    <i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
        </div>
    </div>

    <div class="row" ng-controller="userGoalCtrl">
        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel-body">
                <div class="label-name"><span class="label label-info custom-label pull-right">%%userGoal.status%%</span></div> 
                    <h4 style="margin-top:0;">Goals to achieve</h4>
                    <h5 class="no-margin">From {{ date_in_view($goal->from_date) }} to {{ date_in_view($goal->to_date) }}</h5>
                </div>
            </div>
            @if(!$manager_flag)
            <div ng-show="userGoal.status == 'submitted'">
                <button class="btn btn-success" role="button" data-toggle="collapse" href="#showIDP" aria-expanded="false" aria-controls="showIDP">
                    <i class="fa fa-plus fa-fw"></i> Add new IDP
                </button>
            </div>
            <div class="label label-info custom-label text-right" ng-show="userGoal.status == 'confirmed'">
                Goal has been confirmed        
            </div>
            @endif
            @if($manager_flag)
            <div ng-show="userGoal.status == 'submitted' || userGoal.status == 'confirmed'">
                <button class="btn btn-success" role="button" data-toggle="collapse" href="#showIDP" aria-expanded="false" aria-controls="showIDP">
                    <i class="fa fa-plus fa-fw"></i> Add new IDP
                </button>
            </div>
            @endif
            <div class="panel panel-default collapse m-t-10" id="showIDP">
                <div class="panel-body">
                    @include('pages.admin.goals.goal-idp.idp-form')
                </div>
                <div class="panel-footer text-right">
                    <button type="submit" class="btn btn-success" data-toggle="%%collapse%%" href="#showIDP" ng-click="form.createGoal(true)">
                        <i class="fa fa-plus fa-fw"></i> Add</button>
                </div>
            </div>

            <div class="panel-group custom-panel-group m-t-10">
                <div class="panel panel-default relative" ng-repeat="item in form.data.goals">
                    <div ng-hide="item.is_idp">
                    <div class="label-name"><span class="label label-info custom-label">GOAL</span></div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group col-sm-12">
                                    <label class="col-sm-2" style="margin-left:-12px;margin-top:10px">Goal</label>
                                    <input type="text" class="form-control" name="title" placeholder="Main goal" value="%%item.title%%" disabled>
                                </div>
                             
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group col-sm-12">
                                    <label>Process</label>
                                    <textarea class="form-control" rows="5" name="process" placeholder="How to achieve the goal" disabled>%%item.process%%</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group col-sm-12">
                                    <comment-data commentable-id="item.id" commentable-type="user_goal_item" is-disabled="%%disableFields%%"></comment-data>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div ng-hide="!item.is_idp">
                        <div class="label-name"> <span class="label label-info custom-label">IDP</span>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group col-sm-12">
                                    <label class="col-sm-2" style="margin-left:-12px;margin-top:10px">Goal</label>
                                    <input type="text" class="form-control" name="title" placeholder="Main goal" ng-model="form.data.goals[$index].title">
                                </div>
                               
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group col-sm-12">
                                    <label>Process</label>
                                    <textarea class="form-control" rows="5" name="process" placeholder="How to achieve the goal" ng-model="form.data.goals[$index].process"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row" ng-if="item.id">
                            <comment-data commentable-id="item.id" commentable-type="user_goal_item"></comment-data>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-right" ng-show="userGoal.status == 'submitted' || userGoal.status == 'confirmed' ">
                @if( $manager_flag )
                    <button ng-show="userGoal.status == 'submitted'" type="submit" class="btn btn-success btn-spinner-sm" style="margin-right: 10px" ng-click="form.store('submitted')">
                        <div ng-if="!saveLoading">Save</div>
                        <div ng-if="saveLoading"><i class="fa fa-spinner fa-spin fa fa-fw"></i></div>
                    </button>
                    <a ng-show="userGoal.status == 'confirmed'" class="btn btn-primary pull-right btn-spinner-md" ng-click="form.store('running')">
                        <div ng-if="!freezeGoalLoading">Freeze Goal</div>
                        <div ng-if="freezeGoalLoading"><i class="fa fa-spinner fa-spin fa fa-fw"></i></div>
                    </a>
                    <a ng-show="userGoal.status == 'confirmed'" class="btn btn-danger pull-right btn-spinner-lg" style="margin-right: 10px" ng-click="form.store('submitted')">
                        <div ng-if="!saveLoading">Send back to User</div>
                        <div ng-if="saveLoading"><i class="fa fa-spinner fa-spin fa fa-fw"></i></div>
                    </a>

                @else
                    <button ng-hide="userGoal.status == 'confirmed'" type="submit" class="btn btn-success btn-spinner-sm" style="margin-right: 10px" 
                            ng-click="form.store('submitted')">
                        <div ng-if="!saveLoading">Save</div>
                        <div ng-if="saveLoading"><i class="fa fa-spinner fa-spin fa fa-fw"></i></div>
                    </button>
                    <button ng-hide="userGoal.status == 'confirmed'" type="submit" class="btn btn-success btn-spinner-md" ng-click="form.store('confirmed')">
                        <div ng-if="!confirmLoading">Confirm</div>
                        <div ng-if="confirmLoading"><i class="fa fa-spinner fa-spin fa fa-fw"></i></div>
                    </button>
                @endif
            </div>
        </div>
    </div>
    @endsection