@extends('layouts.admin-dashboard')
@section('main-content')

<section class="new-project-section">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">Floors</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
			  			<li><a href="{{ url('admin/bad-leads-filter') }}">Bad Leads Filter</a></li>
			  			<li class="active">Add</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>

	    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif

		<form class="form-horizontal" method="post" action="/admin/bad-leads-filter/{{$badLeadsFilter->id}}" enctype="multipart/form-data">
			<input name="_method" type="hidden" value="PUT" />
			<div class="panel panel-default">
				<div class="panel-body">
				    <div class="row">
				    	<div class="col-md-12">
						  	<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Field Name</label>
						    	<div class="col-sm-6">
						    		<select id="selectid2" name="field_name"  style="width=35%;" placeholder= "Select an option">
											@if ( $badLeadsFilter->field_name == 'name' )
												<option value="name" selected >Name</option>
											@else
												<option value="name" >Name</option>
											@endif
											@if ( $badLeadsFilter->field_name == 'company' )
												<option value="company" selected>Company</option>
											@else
												<option value="company" >Company</option>
											@endif
											@if ( $badLeadsFilter->field_name == 'email' )
												<option value="email" selected>Email</option>
											@else
												<option value="email" >Email</option>
											@endif
											@if ( $badLeadsFilter->field_name == 'skype' )
												<option value="skype" selected>Skype</option>
											@else
												<option value="skype" >Skype</option>
											@endif
											@if ( $badLeadsFilter->field_name == 'hear_about_us' )
												<option value="hear_about_us" selected>We’re curious, how did you hear about us?</option>
											@else
												<option value="hear_about_us" >We’re curious, how did you hear about us?</option>
											@endif
											@if ( $badLeadsFilter->field_name == 'developers_type' )
												<option value="developers_type" selected>What kind of developers you need?</option>
											@else
												<option value="developers_type" >What kind of developers you need?</option>
											@endif
									</select>
						    	</div>
						  	</div>
						  	<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Filter Value</label>
						    	<div class="col-sm-6">
						      		<input type="text" class="form-control" id="filter_value" name="filter_value" value="{{ $badLeadsFilter->filter_value }}">
						    	</div>
						  	</div>
					  	</div>
					</div>
				</div>
			</div>
  	  		<div class="text-center">
		  		<button type="submit" class="btn btn-success">Save</button>
		  	</div>
		</form>
	</div>
</section>
@endsection
