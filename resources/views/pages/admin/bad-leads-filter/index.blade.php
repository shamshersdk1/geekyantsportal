@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Bad Lead Filters</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li><a href="{{ url('admin/bad-leads-filter') }}">Bad Lead Filters</a></li>
        		</ol>
            </div>
			<div class="col-sm-4">
			<div class="pull-right m-t-10" style="margin-right: 5px">
				<a href="/admin/bad-leads-filter/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Add Bad Leads Filter</a>
            </div>
			</div>
		</div>
	</div>
	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
	<div class="panel panel-default noradius">
		<table class="table table-striped">
			<th width="5%" >Id</th>
			<th width="30%" >Field Name</th>
			<th width="45%">Field Value</th>
			<th class="text-right" width="20%">Action</th>
			@if(count($badLeadsFilterList) > 0)
	    		@foreach($badLeadsFilterList as $badLeadsFilter)
					<tr>
						<td >
							{{ $badLeadsFilter->id }} 
						</td>
						<td >
							{{ $badLeadsFilter->field_name }} 
						</td>
						<td >
							{{ $badLeadsFilter->filter_value }} 
						</td>
						<td class="text-right">
							<span>
								<form name="editForm" method="get" action="/admin/bad-leads-filter/{{$badLeadsFilter->id}}/edit"  style="display: inline-block;">
									<button type="submit" class="btn btn-success btn-sm crude-btn"><i class="fa fa-trash fa-fw"></i>Edit</button> 
								</form>
							</span>
							<span>
								<form name="deleteForm" method="post" action="/admin/bad-leads-filter/{{$badLeadsFilter->id}}"  style="display: inline-block;">
									<input name="_method" type="hidden" value="delete" />
									<button type="submit" class="btn btn-danger btn-sm crude-btn"><i class="fa fa-trash fa-fw"></i>Delete</button> 
								</form>
							</span>
						</td>
					</tr>
				@endforeach
	    	@else
	    		<tr>
	    			<td colspan="3">No results found.</td>
	    			<td></td>
	    			<td></td>
	    		</tr>
	    	@endif
	   </table>
	</div>
</div>	
@endsection

