@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 style="display:inline" class="admin-page-title">{{$name}} payslips</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li><a href="{{ url('admin/employee-payslip') }}">List of Payslip</a></li>
					<span class="label label-success custom-label">Approved</span>
				</ol>
			</div>
        </div>
    </div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
	<div class="panel panel-default">
		<table class="table table-striped">
			<th>Employee Id</th>
			<th>Name</th>
			<th>Month</th>
			<th>No. of working days</th>
			<th>No. of Days Worked</th>
			<th>Gross Salary</th>
			<th>Loan</th>
			<th>Total Deduction</th>
			<th>Annual Bonus</th>
			<th>Qtr Bonus</th>
			<th>Performance Bonus</th>
			<th>Amount Payable</th>
			<th class="text-right">Action</th>
			<?php $c=0;?>
            @if(count($result) > 0)
                @foreach($result['data'] as $data)
                    <tr>
                        <td class="td-text">
                            @if( !empty($data->user))
                                {{$data->user->employee_id}}
                            @endif
                        </td>
                        <td class="td-text">
                            @if( !empty($data->user))
                                {{$data->user->name}}
                            @endif
                        </td>
                        <td class="td-text">
                            @if(!empty($result['payslip'][$c]['month'])){{DateTime::createFromFormat('!m',$result['payslip'][$c]['month'])->format('F')}},@endif
                            @if(!empty($result['payslip'][$c]['year'])){{$result['payslip'][$c]['year']}} &nbsp&nbsp @endif
                        </td>
                        <td class="td-text">
                            {{$data->working_day_month}}
                        </td>
                        <td class="td-text">
                            {{$data->days_worked}}
                        </td>
                        <td class="td-text">
                                {{$data->gross_salary}}
                        </td>
                        <td class="td-text">
                                {{$data->loan}}
                        </td>
                        <td class="td-text">
                                {{$data->total_deduction_month}}
                        </td>
                        <td class="td-text">
                                {{$data->annual_bonus}}
                        </td>
                        <td class="td-text">
                                {{$data->qtr_variable_bonus}}
                        </td>
                        <td class="td-text">
                                {{$data->performance_bonus}}
                        </td>
                        <td class="td-text">
                                {{$data->amount_payable}}
                        </td>
                        <td class="td-text text-right">
                                <a target="_blank" href="/admin/employee-payslip/{{$data->id}}" class="btn btn-info crud-btn btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View</a>    
                        </td>
                    </tr>
                <?php $c++;?>
                @endforeach
            @else
                <tr>
                    <td colspan="3">No results found.</td>
                    <td></td>
                    <td></td>
                </tr>
            @endif
       </table>
   
</div>
@endsection