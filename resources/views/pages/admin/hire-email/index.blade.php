@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Leads Email</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li><a href="{{ url('admin/hire-email') }}">Hire Emails</a></li>
        		</ol>
            </div>
			<div class="col-sm-4">
			
			</div>
		</div>
	</div>
	<div class="col-md-6" style="float:right">
		<ul class="btn-group" style="padding:0;float:right">
			<a href="/admin/hire-email" type="button" class="btn btn-default @if (!Request::get('status')) btn-success @endif" >All</a>
			<a href="/admin/hire-email?status=pending" type="button" class="btn btn-default @if (Request::get('status') == 'pending') btn-success @endif" >Pending</a>
			<a href="/admin/hire-email?status=sent" type="button" class="btn btn-default @if (Request::get('status') == 'sent') btn-success @endif" >Sent</a>
			<a href="/admin/hire-email?status=spam" type="button" class="btn btn-default @if (Request::get('status') == 'spam') btn-success @endif" >Spam</a>
			<a href="/admin/hire-email?status=rejected" type="button" class="btn btn-default @if (Request::get('status') == 'rejected') btn-success @endif" >Rejected</a>
		</ul>
		<h4 style="float:right">Filters:&nbsp&nbsp </h4>
	</div>
	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
	<div class="panel panel-default noradius">
		<table class="table table-striped">
			<th width="5%" >#</th>
			<th width="10%" >Name</th>
			<th width="15%" >Company</th>
			<th width="15%">Email</th>
			<th width="10%">Status</th>
			<th width="15%">Comment</th>
			<th width="15%">Recieved At</th>
			<th width="15%" class="text-right">Action</th>
			@if(count($hireEmailList) > 0)
	    		@foreach($hireEmailList as $hireEmail)
					<tr>
						<td >
							{{ $hireEmail->id }} 
						</td>
						<td >
							{{ $hireEmail->name }} 
						</td>
						<td >
							{{ $hireEmail->company }} 
						</td>
						<td >
							{{ $hireEmail->email }} 
						</td>
						<td>
							<span
							@if($hireEmail->status == 'sent')
							class="label label-primary"
							@elseif($hireEmail->status == 'rejected')
								class="label label-danger"
							@else
								class="label label-default"
							@endif
							>{{ucfirst($hireEmail->status)}}</span><br />
						<!-- <span
							@if($hireEmail->review_status == 'reviewed')
							class="label label-primary"
							@elseif($hireEmail->review_status == 'spam')
								class="label label-danger"
							@else
								class="label label-default"
							@endif
							>{{ucfirst($hireEmail->review_status)}}</span> -->
						</td>
						<td style="white-space:pre">{{$hireEmail->comment}}</td>
						<td >
							{{ datetime_in_view($hireEmail->created_at) }} 
						</td>
						<td class="text-right">
							<span>
							@if($hireEmail->status != "sent")
								<form style="display:inline-block" method="post" action="/admin/hire-email/{{$hireEmail->id}}">
								{{method_field("PUT")}}
									<button type="submit" class="btn btn-success btn-sm crude-btn">Re Queue</button> 
								</form>
							@endif
								<form name="editForm" method="get" action="/admin/hire-email/{{$hireEmail->id}}"  style="display: inline-block;">
									<button type="submit" class="btn btn-info btn-sm crude-btn"><i class="fa fa-eye"></i>  View</button> 
								</form>
							</span>
						</td>
					</tr>
				@endforeach
	    	@else
	    		<tr>
	    			<td colspan="7">No results found.</td>
	    		</tr>
	    	@endif
	   </table>
	</div>
	{{$hireEmailList->links()}}
</div>	
@endsection

