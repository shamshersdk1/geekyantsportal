@extends('layouts.admin-dashboard')
@section('main-content')
    <div class="container-fluid">
        <div class="breadcrumb-wrap">
            <div class="row">
                <div class="col-sm-8">
                    <h1>Spam</h1>
                    <ol class="breadcrumb">
                        <li><a href="/admin">Admin</a></li>
                        <li><a href="/admin/hire-email">Hire Email</a></li>
                        <li class="active">{{$hireEmail->id}}</li>
                    </ol>
                </div>
                <div class="col-sm-4">
                    
                </div>
            </div>
        </div>
        @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif
        <div class="row">
	        <div class="col-md-12" style="padding-left:30px; padding-top:20px;">
                <h4 class="admin-section-heading">Email Details</h4>
                    <div class="panel panel-default">   
                        <div class="panel-body ">   
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="info-table reduce-label-width">
                                    <label>Name : </label>
                                    <span>{{ $hireEmail->name }}</span>
                                </div>
                                <div class="info-table reduce-label-width">
                                    <label>Email : </label>
                                    <span> {{ $hireEmail->email }} </span>
                                </div>	
                                <div class="info-table reduce-label-width">
                                    <label>Company : </label>
                                    <span>{{ $hireEmail->company }}</span>
                                </div>	
                                <div class="info-table reduce-label-width">
                                    <label>Preferred Timezone : </label>
                                    <span>{{ $hireEmail->preferred_timezone }}</span>
                                </div>		
                            <div class="info-table reduce-label-width">
                                <label>Referred by :</label>
                                <span>{{ $hireEmail->referred_by }}</span>
                            </div>	
                            <div class="info-table reduce-label-width">
                                <label>Requirement :</label>
                                <span>{{ $hireEmail->requirement }}</span>
                            </div>
                                
                            <div class="info-table reduce-label-width">
                                <label>Project Type :</label>
                                <span>
                                {{ $hireEmail->project_type }} project
                                </span>
                            </div>
                            <div class="info-table reduce-label-width">
                                <label>Technologies :</label>
                                <span>
                                    @if ( !empty($technologies) )
                                        @foreach( $technologies as $tech )
                                            <div>
                                                * {{ $tech }}
                                            </div>
                                        @endforeach
                                    @endif
                                </span>
                            </div> 
                            <div class="info-table reduce-label-width">
                                <label>Documents :</label>
                                <span>
                                @if ( !empty($documents) )
                                        @foreach( $documents as $doc )
                                            <div>
                                                * {{ $doc }}
                                            </div>
                                        @endforeach
                                    @endif
                                </span>
                            </div> 
                        </div>
				    </div>
                </div>	
            </div>
@endsection