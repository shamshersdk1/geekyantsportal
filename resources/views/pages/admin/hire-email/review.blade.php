@extends('layouts.admin-dashboard')
@section('main-content')
    <div class="container-fluid">
        <div class="breadcrumb-wrap">
            <div class="row">
                <div class="col-sm-8">
                    <h1>Review Emails</h1>
                    <ol class="breadcrumb">
                        <li><a href="/admin">Admin</a></li>
                        <li><a href="/admin/hire-email">Hire Email</a></li>
                        <li class="active">Review</li>
                    </ol>
                </div>
                <div class="col-sm-4">
                    
                </div>
            </div>
        </div>
        @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
        @endif
        <div class="row">
            <div class="col-md-6" style="float:right">
                @if(!empty($hireEmail))
                    <a href="/admin/hire-email-review/{{$hireEmail->id}}?action=next" class="btn btn-info" style="float:right;margin-left:5px">Next</a>
                    @if($hireEmail->status == "rejected")
                        <a href="/admin/hire-email-review/{{$hireEmail->id}}?action=send" class="btn btn-success" style="float:right;margin-left:5px">Send & Continue</a>
                    @endif
                    <a href="/admin/hire-email-review/{{$hireEmail->id}}?action=spam" class="btn btn-danger" style="float:right;margin-left:5px">Spam & Continue</a>
                @endif
            </div>
            <h5 style="padding-left:30px">Total Unreviewed Emails: {{$count}}</h5>
        </div>
        @if(!empty($hireEmail))
            <div class="row">
                <div class="col-md-12" style="padding-left:30px; padding-top:20px;">
                    <h4 class="admin-section-heading">Email Details :</h4>
                    <div class="panel panel-default">   
                        <div class="panel-body ">   
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="info-table reduce-label-width">
                                    <label>Name : </label>
                                    <span>{{ $hireEmail->name }}</span>
                                </div>
                                <div class="info-table reduce-label-width">
                                    <label>Email : </label>
                                    <span> {{ $hireEmail->email }} </span>
                                </div>	
                                <div class="info-table reduce-label-width">
                                    <label>Company : </label>
                                    <span>{{ $hireEmail->company }}</span>
                                </div>	
                                <div class="info-table reduce-label-width">
                                    <label>Status :</label>
                                    <span style="width:5em;font-size:0.8em;float:left;margin-top:3px"
                                        @if($hireEmail->status == 'sent')
                                        class="label label-primary"
                                        @elseif($hireEmail->status == 'rejected')
                                            class="label label-danger"
                                        @else
                                            class="label label-default"
                                        @endif
                                        >{{ucfirst($hireEmail->status)}}</span>
                                </div>
                                <div class="info-table reduce-label-width">
                                    <label>Preferred Timezone : </label>
                                    <span>{{ $hireEmail->preferred_timezone }}</span>
                                </div>		
                            <div class="info-table reduce-label-width">
                                <label>Referred by :</label>
                                <span>{{ $hireEmail->referred_by }}</span>
                            </div>	
                            <div class="info-table reduce-label-width">
                                <label>Requirement :</label>
                                <span>{{ $hireEmail->requirement }}</span>
                            </div>
                                
                            <div class="info-table reduce-label-width">
                                <label>Project Type :</label>
                                <span>
                                {{ $hireEmail->project_type }} project
                                </span>
                            </div>
                            <div class="info-table reduce-label-width">
                                <label>Technologies :</label>
                                <span>
                                    @if ( !empty($technologies) )
                                    @php $technologies = json_decode($hireEmail->technologies,true); @endphp
                                        @foreach( $technologies as $tech )
                                            <div>
                                                * {{ $tech }}
                                            </div>
                                        @endforeach
                                    @endif
                                </span>
                            </div> 
                            <div class="info-table reduce-label-width">
                                <label>Documents :</label>
                                <span>
                                @if ( !empty($documents) )
                                    @php $documents = json_decode($hireEmail->documents,true); @endphp
                                        @foreach( $documents as $doc )
                                            <div>
                                                * {{ $doc }}
                                            </div>
                                        @endforeach
                                    @endif
                                </span>
                            </div> 
                        </div>
                    </div>
                </div>	
            </div>
        @else
            <div class="row">
                <div class="col-md-12" style="padding-left:30px; padding-top:20px;">
                    <h4>No Emails</h4>    
                </div>	
            </div>
        @endif
    </div>
@endsection