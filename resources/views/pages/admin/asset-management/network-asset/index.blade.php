@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid" ng-app="myApp" ng-controller="networkAssetrCtrl">
    <div class="breadcrumb-wrap">
        <div class="row">
           <div class="col-sm-8">
                <h1 class="admin-page-title">Network asset</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a  class="active">Network asset</a></li>
                </ol>
            </div>
        </div>
    </div>
    <div ng-if="error && errorMsg"class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <span>%%errorMsg%%</span><br/>
    </div>
    @if (session('message'))
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <span>{{ session('message') }}</span><br/>
        </div>
    @endif
        <table datatable="" class="table table-condensed table-hover" dt-options="dtOptions" dt-column-defs="dtColumnDefs" >
        <thead>
            <tr>
                <th>#</th>
                <th>Asset Name</th>
                <th>Is Working</th>
                <th>Mac Address</th>
                <th>Ip Address</th>
                <th>Action</th>
                
            </tr>
        </thead>
        <tbody>
                <tr ng-repeat="(i, asset) in asset.data track by $index">
                    <td>%%asset.id%%</td>
                    <td>%%asset.name%%</td>
                    <td>
                        <span ng-if="asset.is_working==1" class="label label-success custom-label">Yes</span>
                        <span ng-if="asset.is_working==0" class="label label-danger custom-label">No</span>
                   </td>
                    <td><span ng-repeat="meta in asset.metas" ng-if="meta.key === 'LAN-MAC' || meta.key === 'WIFI-MAC'"> %%meta.key%% - %%meta.value%% <br/></span></td>
                    <td><span ng-repeat="meta in asset.metas" ng-if="meta.key === 'LAN-MAC' || meta.key === 'WIFI-MAC'" >%%meta.ip_mapper.user_ip.ip_address%%<br/></span></td>
                    <td>
                        <span ng-repeat="meta in asset.metas" ng-if="meta.key === 'LAN-MAC' || meta.key === 'WIFI-MAC'"> 
                            %%meta.key%%
                            <button style="margin:2px;" class=" btn btn-sm btn-success" ng-hide="meta.ip_mapper" ng-click="assignIp(asset, meta)">Assign Ip </button>
                            <button  style="margin:2px;" ng-show="meta.ip_mapper" class="btn btn-sm btn-danger" ng-click="releaseIp(meta)">Release Ip</button>
                            <br/>
                        </span>
                    </td>
                </tr>
        </tbody>
    </table>
    </div>
    
</div>



@endsection