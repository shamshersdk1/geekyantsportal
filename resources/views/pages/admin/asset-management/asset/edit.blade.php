@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
  <div class="breadcrumb-wrap">
     <div class="row">
        <div class="col-sm-8">
           <h1>Asset</h1>
           <ol class="breadcrumb">
              <li><a href="/admin">Admin</a></li>
              <li><a href="/admin/asset-management/asset">Asset</a></li>
              <li class="active">Edit</li>
           </ol>
        </div>
        <div class="col-sm-4">
        </div>
     </div>
  </div>
  @if(!empty($errors->all()))
  <div class="alert alert-danger">
     @foreach ($errors->all() as $error)
     <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
     <span>{{ $error }}</span><br/>
     @endforeach
  </div>
  @endif
  @if (session('message'))
  <div class="alert alert-success">
     <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
     <span>{{ session('message') }}</span><br/>
  </div>
  @endif
  <div class="row">
    <div class="col-lg-7 col-md-6">
      <h4 class="admin-section-heading">Asset Details</h4>
      <div class="panel panel-default">
        <div class="panel-body">
          <form class="form-horizontal" method="post" action="/admin/asset-management/asset/{{$asset->id}}" enctype="multipart/form-data">
            <input name="_method" type="hidden" value="PUT" />
            <div class="row">
              <div class="col-lg-6 col-md-12 col-sm-6">
                <div class="m-b">
                  <label for="name">Name*:</label><br />
                  <input type="text" class="form-control" id="name" name="name" value="{{ $asset->name }}" />
                </div>
                <div class="m-b">
                  <label>Serial Number*:</label><br />
                  <input type="text" class="form-control" id="serial_number" name="serial_number"  value="{{ $asset->serial_number }}" />
                </div>

                <div class="m-b">
                  <label>Model:</label><br />
                  <input class="module-tag2" name="model" value="{{$asset->model}}" multiple="multiple" />
                </div>

                <div class="m-b">
                  <label>Make:</label><br />
                  <input class="module-tag" name="make" value="{{$asset->make}}" multiple="multiple" />
                </div>

                <div class="m-b">
                  <label>Purchase*: </label><br />
                  <div class='input-group date' id='purchase_date' style="width:100%">
                      <input type='text' id="sd" class="form-control" value="{{ $asset->purchase_date }}" autocomplete="off" name="purchase_date" />
                      <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                  </div>
                </div>

                <div class="m-b">
                  <label>Invoice number*:</label><br />
                   <input type="text" class="form-control" id="invoice_number" name="invoice_number" value="{{ $asset->invoice_number }}" />
                </div>

                <div class="m-b">
                  <label>Item Price:</label><br />
                  <input type="text" class="form-control" id="price" name="price" value="{{ $asset->price }}" />
                </div>

                <div class="m-b">
                  <label>Item Tax:</label><br />
                  <input type="text" class="form-control" id="tax" name="tax" value="{{ $asset->tax }}" />
                </div>
              </div>
              <div class="col-lg-6 col-md-12 col-sm-6">
                <div class="m-b">
                  <label for="">Category*: </label><br />
                  @if(isset($assetCategories))
                  <select id="selectid2" name="asset_category_id"  style="width: 35%">
                    @foreach($assetCategories as $x)
                    @if($asset->asset_category_id == $x->id)
                    <option value="{{$x->id}}" selected>{{$x->name}}</option>
                    @else
                    <option value="{{$x->id}}">{{$x->name}}</option>
                    @endif
                    @endforeach
                  </select>
                  @endif
                </div>
                <div class="m-b">
                  <div class="row">
                    <div class="col-md-6">
                      <label for="assignable">Is Assignable:</label><br />
                      <div>
                        <span class="form-check m-r-15">
                          <input class="form-check-input" type="radio" name="is_assignable"  id="is_assignable_yes" value="1"  <?php if($asset->is_assignable == "1") { echo 'checked'; } ?>>
                          <label class="form-check-label" for="exampleRadios1">
                             Yes
                          </label>
                        </span>
                        <span class="form-check">
                          <input class="form-check-input" type="radio" name="is_assignable"  id="is_assignable_no" value="0"  <?php if($asset->is_assignable == "0") { echo 'checked'; } ?>>
                          <label class="form-check-label" for="exampleRadios1">
                             No
                          </label>
                        </span>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <label>Is working*:</label><br />
                      <span class="form-check m-r-15">
                        <input class="form-check-input" type="radio" name="is_working"  id="is_working_yes" value="1"  <?php if($asset->is_working == "1") { echo 'checked'; } ?>>
                        <label class="form-check-label" for="exampleRadios2">
                           Yes
                        </label>
                      </span>
                      <span class="form-check">
                        <input class="form-check-input" type="radio" name="is_working"  id="is_working_no" value="0"  <?php if($asset->is_working == "0") { echo 'checked'; } ?>>
                        <label class="form-check-label" for="exampleRadios2">
                           No
                        </label>
                      </span>
                        <!-- <select class="form-control" name="is_working">
                          <option value="1">Yes</option>
                          <option value="0">No</option>
                      </select> -->
                    </div>
                  </div>
                </div>
                @if( !empty($assigned) )
                <div class="m-b">
                  <label>Assigned User:</label><br />
                  <div class="row row-sm">
                    <div class="col-sm-5">
                      <input type="text" class="form-control"  value="{{ $assigned->user->name }}" disabled="true" />
                    </div>
                    <div class="col-sm-4">
                       <input type="text" class="form-control" value="{{ date_in_view($assigned->from_date) }} " disabled />
                    </div>
                    <div class="col-sm-3">
                       <a href="/admin/asset-management/asset/{{$asset->id}}" class="btn btn-info">Change</a>
                    </div>
                  </div>
                </div>
                @endif

                <div class="m-b">
                  <label for="exampleInputFile">File input:</label><br />
                  <input type="file" id="file" class="form-control" name="file[]" multiple="true">
                  @if ( !empty($asset->files) )
                    @foreach ( $asset->files as $file )
                      <a href="{{$file->url()}}" target="_blank" class="btn btn-info btn-sm crude-btn">{{$file->name}}</a>
                      <a href="/admin/asset-management/file/delete/{{$file->id}}" style="color: red"><i class="fa fa-times fa-lg"></i></a>
                    @endforeach
                  @endif
                </div>
                <div class="m-b">
                  <label> Google Drive Link:</label><br />
                  <input type="text" class="form-control" id="google_drive_link" name="google_drive_link"  value="{{ $asset->google_drive_link }}" />
                </div>
                <div class="m-b">
                  <label>Description:</label><br />
                  <textarea class="form-control" id="description" name="description" rows="3"> {{ $asset->description }}</textarea>
                </div>
              </div>
            </div>
            <div class="text-right clearfix">
              <button type="submit" class="btn btn-success"> SAVE</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="col-lg-5 col-md-6">
      <div ng-app="myApp">
         <asset-meta asset-id="{{$asset->id}}"></asset-meta>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
   $(function () {
       $('#purchase_date').datetimepicker({
           format: 'YYYY-MM-DD'
       });
   });
</script>
<script type="text/javascript">
    $(document).ready(function() {
            $('.module-tag').select2({
                maximumSelectionSize: 1,
                @if(!empty($makeList))
                tags: {{$makeList}},
                @endif
            });
            $('.module-tag2').select2({
                maximumSelectionSize: 1,
                @if(!empty($modelList))
                tags: {{$modelList}},
                @endif
            });
    });
</script>
@endsection