@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
   <div class="breadcrumb-wrap">
      <div class="row">
         <div class="col-sm-8">
            <h1>Asset</h1>
            <ol class="breadcrumb">
               <li><a href="/admin">Admin</a></li>
               <li><a href="/admin/asset-management/asset">Asset</a></li>
               <li class="active">Add</li>
            </ol>
         </div>
         <div class="col-sm-4">
         </div>
      </div>
   </div>
   @if(!empty($errors->all()))
   <div class="alert alert-danger">
      @foreach ($errors->all() as $error)
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <span>{{ $error }}</span><br/>
      @endforeach
   </div>
   @endif
   @if (session('message'))
   <div class="alert alert-success">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <span>{{ session('message') }}</span><br/>
   </div>
   @endif
   <div class="panel panel-default">
      <div class="panel-body">
         <form class="form-horizontal" method="post" action="/admin/asset-management/asset" enctype="multipart/form-data">
            <div class="form-group col-sm-6">
               <label for="name" class="col-sm-4  control-label">Name* :</label>
               <div class="col-sm-8">
                  <input type="text" class="form-control" id="name" name="name" value="{{old('name')}}" placeholder="Asset Name"> </input>
               </div>
            </div>
            <div class="form-group col-sm-6">
               <label for="" class="col-sm-4 control-label">Category*: </label>
               <div class="col-sm-8">
                  @if(isset($assetCategories))
                  <select id="selectid2" name="asset_category_id" style="width=35%;" placeholder= "Select an option">
                     <option value=""></option>
                     @foreach($assetCategories as $x)
                     @if ( old('asset_category_id') == $x->id )
                     <option value="{{$x->id}}" selected >{{$x->name}}</option>
                     @else
                     <option value="{{$x->id}}" >{{$x->name}}</option>
                     @endif
                     @endforeach
                  </select>
                  @endif
               </div>
               <!--<div class="col-sm-4">
                  <a href="" class="btn btn-success"> Add New Client</a>
                  </div>-->
            </div>
            <div class="form-group col-sm-6" style="clear:left">
               <label class="col-sm-4 control-label">Is working* :</label>
               <div class="col-sm-8">
                  <select class="form-control" name="is_working">
                     <option value="1">Yes</option>
                     <option value="0">No</option>
                  </select>
               </div>
            </div>
            <div class="form-group col-sm-6">
               <label class="col-sm-4 control-label">Invoice number* :</label>
               <div class="col-sm-8">
                  <input type="text" class="form-control" id="invoice_number" name="invoice_number" value="{{old('invoice_number')}}" placeholder="Enter Invoice Number"> </input>
               </div>
            </div>
            <div class="form-group col-sm-6">
               <label  class="col-sm-4 control-label">Invoice Date* : </label>
               <div class="col-sm-8">
                  <div class='input-group date' id='purchase_date'>
                     <input type='text' id="sd" class="form-control" value="{{old('purchase_date') }}" autocomplete="off" name="purchase_date" />
                     <span class="input-group-addon">
                     <span class="glyphicon glyphicon-calendar"></span>
                     </span>
                  </div>
               </div>
            </div>
            <div class="form-group col-sm-6">
               <label class="col-sm-4 control-label">Serial Number* :</label>
               <div class="col-sm-8">
                  <input type="text" class="form-control" id="serial_number" name="serial_number"  value="{{old('serial_number')}}" placeholder="Enter Serial Number"> </input>
               </div>
            </div>
            <div class="form-group col-sm-6" style="clear:left">
               <label  class="col-sm-4 control-label">Item Price : </label>
               <div class="col-sm-8">
                  <input type="text" class="form-control" id="item_price" name="price"  value="{{old('price')}}" placeholder="Item Price"> </input>
               </div>
            </div>
            <div class="form-group col-sm-6">
               <label class="col-sm-4 control-label">Item Tax :</label>
               <div class="col-sm-8">
                  <input type="text" class="form-control" id="item_tax" name="tax"  value="{{old('tax')}}" placeholder="Item Tax"> </input>
               </div>
            </div>
            <div class="form-group col-sm-6" style="clear:left">
               <label  class="col-sm-4 control-label">Make : </label>
               <div class="col-sm-8">
                  <!--  <input type="text" class="form-control" id="Make" name="make"  value="{{old('make')}}" placeholder="Make"> </input>  -->
                  <input class="module-tag" name="make" multiple="multiple"/>
               </div>
            </div>
            <div class="form-group col-sm-6">
               <label class="col-sm-4 control-label">Model :</label>
               <div class="col-sm-8">
                <input class="module-tag2" name="model" multiple="multiple"/>
               </div>
            </div>
            <div class="form-group col-sm-6">
               <label class="col-sm-4 control-label">Assign User :</label>
               <div class="col-sm-8">
                  <div class="col-md-6 p-0" >
                     <select style="width=35%;" id="user_list" name="user_id" class="user_list">
                        <option value=""></option>
                        @foreach($users as $user)
                        @if ( old('user_id') == $user->id )
                        <option value="{{$user->id}}" selected >{{$user->name}}</option>
                        @else
                        <option value="{{$user->id}}" >{{$user->name}}</option>
                        @endif
                        @endforeach
                     </select>
                  </div>
                  <div class="col-md-6 " style="padding-right:0">
                     <div class='input-group date' id='datetimepickerStart'>
                        <input type='text' id="datetimeinputstart" class="form-control" value="{{old('start_date')}}" name="start_date" placeholder="From Date" autocomplete="off" />
                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                     </div>
                  </div>
               </div>
            </div>
            <div class="form-group col-sm-6">
               <label for="" class="col-sm-4 control-label">File input :</label>
               <div class="col-sm-8">
                  <input type="file" id="file" class="form-control" name="file[]" multiple="true">
               </div>
            </div>
            <div class="form-group col-sm-6" style="clear:left">
               <label class="col-sm-4 control-label"> Google Drive Link :</label>
               <div class="col-sm-8">
                  <input type="text" class="form-control" id="google_drive_link" name="google_drive_link" value="{{old('google_drive_link')}}"  placeholder="Enter google link drive"> </input>
               </div>
            </div>
            <div class="form-group col-sm-6">
               <label class="col-sm-4 control-label">Description :</label>
               <div class="col-sm-8">
                  <textarea class="form-control" id="description" name="description" rows="3" placeholder="Enter Description">{{old('description')}}</textarea>
               </div>
            </div>
            <div class="form-group">
                <label for="assignable" class="col-sm-4 control-label">Is Assignable:</label>
                    <div  class="col-sm-6">
                        <span class="form-check" style="margin: 5px;">
                            <input class="form-check-input" type="radio" name="is_assignable"  id="is_assignable_yes" value="1" checked>
                            <label class="form-check-label" for="exampleRadios1">
                                Yes
                            </label>
                        </span>
                        <span class="form-check" style="margin: 5px;">
                            <input class="form-check-input" type="radio" name="is_assignable"  id="is_assignable_no" value="0" >
                            <label class="form-check-label" for="exampleRadios1">
                                No
                            </label>
                        </span>
                    </div>
            </div>
      </div>
   </div>
   <div class="text-center" style="margin-top: 30px;">
   <button type="submit" class="btn btn-primary btn-sm crude-btn "> SAVE</button>
   </div>
   </form>
</div>
<script type="text/javascript">
   $(function () {
       $('#purchase_date').datetimepicker({
       format: 'YYYY-MM-DD'
   });

   });
</script>
<script>
   $(function () {
       $('#datetimepickerStart').datetimepicker({
           format: 'YYYY-MM-DD'
       });

       $('#datetimepickerEnd').datetimepicker({
           format: 'YYYY-MM-DD'
       });
       if($("#datetimeinputstart").val()!="")
       {
           $('#datetimepickerEnd').data("DateTimePicker").minDate($("#datetimeinputstart").val());
       }

           $('.user_list').select2({
           placeholder: 'Select an option',
           allowClear:true
       });
   });
</script>
<script type="text/javascript">
    $(document).ready(function() {
            $('.module-tag').select2({
                maximumSelectionSize: 1,
                @if(!empty($makeList))
                tags: {{$makeList}},
                @endif
            });
            $('.module-tag2').select2({
                maximumSelectionSize: 1,
                @if(!empty($modelList))
                tags: {{$modelList}},
                @endif
            });
    });
</script>
@endsection
