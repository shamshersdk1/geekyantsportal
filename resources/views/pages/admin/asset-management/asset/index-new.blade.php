@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1>Asset</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li class="active">Asset</li>
                </ol>
            </div>
            <div class="col-sm-4 text-right">
                <a href="/admin/asset-management/asset/create" class="btn btn-success btn-sm crude-btn">
                    <i class="fa fa-plus fa-fw"></i>
                    ADD
                </a>
            </div>
        </div>
    </div>
    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif
    
    <div class="row m-b">
        <div class="col-md-5 col-sm-6">
            <form action="/admin/asset-management/asset" method="GET">
                <div class="input-group">
                    <span class="input-group-addon no-bg">
                        @if( !empty($search) )
                            Showing results for
                        @else 
                            Search</label>
                        @endif
                    </span>
                    <input type="text" 
                       name="q"
                       class="form-control" 
                       placeholder="Search by name, serial number or invoice number" 
                       value="@if(isset($search)){{$search}}@endif">
                </div>
            </form>
        </div>
        <div class="col-sm-6 col-md-4 pull-right">
            @if( !empty($assetCategories) )
             <div class="input-group">
                <span class="input-group-addon no-bg">Category</span>
                <select class="form-control">
                    <option value="all">All</option>
                    @foreach( $assetCategories as $i => $assetCategory )
                    @if( ($search != '' && !empty($assetCategory['assets'])) || $search == '' )
                        <option value="{{ $assetCategory['name'] }}">{{ $assetCategory['name'] }}</option>
                    @endif
                @endforeach
                </select>
            </div>
            @endif
        </div>
    </div>
    <div id="exampleAccordion" data-children=".item">
        @if( !empty($assetCategories) )
            @foreach( $assetCategories as $i => $assetCategory )
                @if( ($search != '' && !empty($assetCategory['assets'])) || $search == '' )
                <div>
                    <div class="panel-header">
                        <h4>{{ $assetCategory['name'] }}</h4>
                    </div>
                    <div class="panel panel-default">
                    @if ( !empty($assetCategory['assets']) )
                        <table class="table table-striped">
                            <tr>
                                <th width="5%" class="text-center">Status</th>
                                <th width="15%">Name</th>
                                <th width="20%">Info</th>
                                <th width="15%">Assign</th>
                                <th width="10%">Purchase Info</th>                                    
                                <th width="25%">Additional Info</th>                                    
                                <th width="10%" class="text-right">Action</th>
                            </tr>
                            @foreach($assetCategory['assets'] as $index => $asset)
                            <tr>
                                <!-- <td>{{ $index + 1 }}</td> -->
                                <td class="text-center"> 
                                    @if($asset['is_working'] == 1)
                                        <span class="text-success" tooltip="Working" >
                                            <i class="fa fa-check"></i> 
                                        </span>
                                    @else
                                        <span class="text-danger">
                                            <i class="fa fa-ban fa-2x"></i> 
                                        </span>
                                    @endif

                                </td>
                                <td> 
                                    {{ $asset['name'] }}
                                </td>
                                <td>
                                    <div>
                                        <small>
                                            <span class="asset-info-span">Serial No.: </span>
                                            <strong>{{ $asset['serial_number'] }}</strong>
                                        </small>
                                    </div>
                                    <div>
                                        <small>
                                            <span class="asset-info-span">Make: </span>
                                            <strong>{{ $asset['serial_number'] }}</strong>
                                        </small>
                                    </div>
                                    <div>
                                        <small>
                                            <span class="asset-info-span">Model: </span>
                                            <strong>{{ $asset['serial_number'] }}</strong>
                                        </small>
                                    </div>
                                </td>
                                <td> 
                                    @if($asset['assigned_to'])
                                        <small>Assigned to</small><br />
                                        {{ $asset['assigned_to'] }} <br />
                                        <a href=""  data-toggle="modal" data-target="#assignModal" class="btn btn-danger btn-xs m-t-5">Release</a>

                                    @elseif($asset['is_working'] == 1)
                                        <button data-toggle="modal" data-target="#assignModal" class="btn btn-success btn-sm m-t-5">Assign</a>

                                    @else 
                                        <span class="label label-warning custom-label">Non Assignable</span>
                                    @endif
                                </td>

                                <td> 
                                    {{ date_in_view($asset['purchase_date']) }}<br />
                                    <small>{{ $asset['invoice_number'] }}</small>
                                </td>

                                <td>
                                    <div>
                                        <small>
                                            <span class="asset-info-span">WIFI-MAC: </span>
                                            <strong>8c:85:90:56:43:64</strong>
                                        </small>
                                    </div>
                                    <div>
                                        <small>
                                            <span class="asset-info-span">WIFI-LAN: </span>
                                            <strong>8c:85:90:56:43:64</strong>
                                        </small>
                                    </div>
                                </td>
                                
                                <td class="text-right">
                                    <div class="clearfix">
                                        <form name="showForm" method="get" action="/admin/asset-management/asset/{{$asset['id']}}" class="pull-right" style="display: inline-block; width: 80px;">
                                            <button type="submit" class="btn btn-info btn-sm m-b-5 btn-block"><i class="fa fa-eye fa-fw"></i>View</button> 
                                        </form>
                                    </div>  
                                    <div class="clearfix">
                                        <form name="editForm" method="get" action="/admin/asset-management/asset/{{$asset['id']}}/edit"  style="display: inline-block;">
                                            <button type="submit" class="btn btn-default btn-sm"><i class="fa fa-edit fa-fw"></i></button> 
                                        </form>
                                        
                                        <form name="deleteForm" method="post" action="/admin/asset-management/asset/{{$asset['id']}}"  style="display: inline-block;">
                                            <input name="_method" type="hidden" value="delete" />
                                            <button type="submit" class="btn btn-default text-danger btn-sm"><i class="fa fa-trash fa-fw"></i></button> 
                                        </form>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </table>
                        @else
                        <div class="panel-body text-center">
                            <h5>No records found</h5>
                        </div>
                        @endif
                    </div>
                </div>
                @endif
            @endforeach
        @endif
    </div>
</div>

<div class="modal fade" id="assignModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Assign / Relese Asset</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
            <div class="form-group">
                <div class="col-sm-6">
                    <label>Assign To</label>
                    <div>
                        <select class="form-control input-sm">
                            <option>Megha</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6">
                    <label>Assign from</label>
                    <div class="input-group input-group-sm">
                        <input type="" name="" class="form-control" />
                        <span class="input-group-btn">
                            <button class="btn btn-default"><i class="fa fa-calendar"></i></button>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-6">
                    <label>WIFI-MAC</label>
                    <div>
                       <a href="" class="btn btn-default btn-sm">Assign IP</a>
                    </div>
                </div>
                <div class="col-sm-6">
                    <label>LAN-MAC</label>
                    <div>
                        <a href="" class="btn btn-default btn-sm">Assign IP</a>
                    </div>
                </div>
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success">Assign</button>
      </div>
    </div>
  </div>
</div>


@endsection
