@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1>Asset</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li class="active">Asset</li>
                </ol>
            </div>
            <div class="col-sm-4 text-right">
                <a href="/admin/asset-management/asset/create" class="btn btn-success btn-sm crude-btn">
                    <i class="fa fa-plus fa-fw"></i>
                    ADD
                </a>
            </div>
        </div>
    </div>
    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif
    
    <div class="row" style="margin-bottom:1%">
        <div class="col-md-4">
            <form action="/admin/asset-management/asset" method="GET">
                @if( !empty($search) )
                    Showing results for
                @endif
                <input type="text" 
                       name="q"
                       class="form-control" 
                       placeholder="Search by name, serial number or invoice number" 
                       value="@if(isset($search)){{$search}}@endif">
            </form>
        </div>
    </div>
    <div id="exampleAccordion" data-children=".item">
        @if( !empty($assetCategories) )
            @foreach( $assetCategories as $i => $assetCategory )
                @if( ($search != '' && !empty($assetCategory['assets'])) || $search == '' )
                <div class="item">
                    <div data-toggle="collapse" class="panel" style="padding: 10px;" data-parent="#exampleAccordion" href="#exampleAccordion{{$i}}" role="button" aria-expanded="true" aria-controls="exampleAccordion{{$i}}">
                        {{ $assetCategory['name'] }} <i class="more-less glyphicon glyphicon-plus pull-right"></i>
                    </div>
                    <div id="exampleAccordion{{$i}}" class="{{ ($search != '' ) ? 'collapse in' : 'collapse'  }}" role="tabpanel" style="margin-top: 20px">
                        <div class="panel panel-default" >
                        @if ( !empty($assetCategory['assets']) )
                            <table class="table table-striped">
                                <tr>
                                    <th width="2%" class="text-center">#</th>
                                    <th width="10%" class="text-center">Name</th>
                                    <th width="15%" class="text-center">Serial Number</th>
                                    <th width="8%" class="text-center">Is Working</th>
                                    <th width="15%" class="text-center">Invoice Number</th>
                                    <th width="15%" class="text-center">Purchase Date</th>
                                    <th width="10%"class="text-center">Assigned To</th>
                                    <th width="25%" class="pull-right">Action</th>
                                </tr>
                                @foreach($assetCategory['assets'] as $index => $asset)
                                    <tr>
                                        <td class="text-center">{{ $index + 1 }}</td>
                                        <td class="text-center"> {{ $asset['name'] }} </td>
                                        <td class="text-center"> {{ $asset['serial_number'] }} </td>
                                        <td class="text-center"> 
                                            @if($asset['is_working'] == 1)
                                                <span class="label label-success custom-label">Yes</span>
                                            @else
                                                <span class="label label-danger custom-label">No</span>
                                            @endif
                                        </td>
                                        <td class="text-center"> {{ $asset['invoice_number'] }} </td>
                                
                                        <td class="text-center"> {{ date_in_view($asset['purchase_date']) }}  </td>
                                        <td class="text-center"> {{ $asset['assigned_to'] }}  </td>
                                        <td ><div class="text-right">
                                            <span>
                                                <form name="showForm" method="get" action="/admin/asset-management/asset/{{$asset['id']}}"  style="display: inline-block;">
                                                    <button type="submit" class="btn btn-info btn-sm crude-btn"><i class="fa fa-eye fa-fw"></i>View</button> 
                                                </form>
                                            <span>
                                                <form name="editForm" method="get" action="/admin/asset-management/asset/{{$asset['id']}}/edit"  style="display: inline-block;">
                                                    <button type="submit" class="btn btn-success btn-sm crude-btn"><i class="fa fa-edit fa-fw"></i>Edit</button> 
                                                </form>
                                            </span>
                                            <span>
                                                <form name="deleteForm" method="post" action="/admin/asset-management/asset/{{$asset['id']}}"  style="display: inline-block;">
                                                    <input name="_method" type="hidden" value="delete" />
                                                    <button type="submit" class="btn btn-danger btn-sm crude-btn"><i class="fa fa-trash fa-fw"></i>Delete</button> 
                                                </form>
                                            </span>
</div>
                                        </td>
                                    </tr>
                                @endforeach
                                </table>
                        @else
                            <div style="background-color: #EED4D4; padding: 20px"> No records found </div>
                        @endif
                    </div>
                    </div>
                </div>
                @endif
            @endforeach
        @endif
    </div>

</div>
@endsection
