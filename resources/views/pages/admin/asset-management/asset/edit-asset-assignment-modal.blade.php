<section>
<div class="modal fade" id="editAssetAssignmentModal" tabindex="-1" role="dialog" aria-labelledby="editRoleModalLabel">
   <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                @if(!empty($errors->all()))
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <span>{{ $error }}</span><br/>
                          @endforeach
                    </div>
                @endif
                @if (session('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ session('message') }}</span><br/>
                    </div>
                @endif
                <h4 class="modal-title" id="editRoleModalLabel">Edit Details</h4>
            </div>
            <!-- <div class="modal-body loading" style="min-height: 113px;">
                <div class="col-md-2 col-md-offset-5">
                    <i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
                </div>
            </div> -->
            <div class="modal-body content">
                <div class="row">
                    <div class="col-md-12 no-padding ma-bot-10">
                        <div class="form-group">
                            <div class="col-md-6">
                                <label for="start_date">From</label>
                                <div class='input-group date' id='datetimepickerStart_edit'>
                                    <input type='text' class="form-control" autocomplete="off" name="edit_start_date" placeholder="Start Date" required/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-6" id="end_date_div_edit">
                                <label for="end_date">To</label>
                                <div class='input-group date' id='datetimepickerEnd_edit'>
                                    <input type='text' class="form-control" name="edit_end_date" placeholder="End Date" autocomplete="off"  required/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12  ma-bot-10">
                        <div class="form-group">
                            <label for="comments">Comment</label>
                            <div  id='comments_edit' >
                                <textarea class="form-control" id='comments' name='comments' required>
</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>&nbsp;
               
                    <button type="submit" class="btn btn-primary"  onclick="updateData()">
                        Update
                    </button>
                
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function(){
            $('#datetimepickerStart_edit').datetimepicker({
                //Set max date to upcoming march
                maxDate: new Date().getMonth() <= 2 ? new Date(new Date().getFullYear()+"-03-31") : new Date(new Date().getFullYear()+1+"-03-31"),
                // minDate: new Date(new Date().getFullYear()+"-04-01"), //Set min date to April 1,
                minDate:"2015-01-01",
                format: 'YYYY-MM-DD'
            });

            $('#datetimepickerEnd_edit').datetimepicker({
                //Set max date to upcoming march
                maxDate: new Date().getMonth() <= 2 ? new Date(new Date().getFullYear()+"-03-31") : new Date(new Date().getFullYear()+1+"-03-31"),
                format: 'YYYY-MM-DD'
            });
            if($("#datetimepickerStart_edit").val()!="")
            {
                $('#datetimepickerEnd_edit').data("DateTimePicker").minDate($("#datetimepickerStart_edit").val());
            }
        });
</script>
</section>