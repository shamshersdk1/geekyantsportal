@extends('layouts.admin-dashboard')
@section('main-content')
<section>
    <div class="container-fluid" ng-app="myApp">
        <div class="breadcrumb-wrap">
            <div class="row">
                <div class="col-sm-8">
                    <h1>Asset</h1>
                    <ol class="breadcrumb">
                        <li><a href="/admin">Admin</a></li>
                        <li><a href="/admin/asset-management/asset">Asset</a></li>
                        <li class="active">{{$asset->id}}</li>
                    </ol>
                </div>
                <div class="col-sm-4 text-right">
                    <a href="/admin/asset-management/asset/{{$asset->id}}/edit" class="btn btn-info">Edit</a>
                    <button type="button" onclick="window.history.back();" class="btn btn-primary"><i class="fa fa-caret-left fa-fw"></i> Back</button>
                </div>
            </div>
        </div>
        @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif
        <div class="row">
	        <div class="col-md-5" >
                <h4 class="admin-section-heading">Asset-detail</h4>
                <div class="panel panel-default">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="info-table">
                        <label>Name : </label>
                        <span>{{$asset->name}}</span>
                    </div>
		    		<div class="info-table">
                        <label>Category : </label>
                        <span>{{$asset->assetCategory->name}}</span>
                    </div>
                    <div class="info-table">
                        <label>Purchase : </label>
                        <span>{{ date('d-m-Y', strtotime($asset->purchase_date)) }}</span>
                    </div>
		    		<div class="info-table">
                        <label>Serial Number : </label>
                        <span>{{$asset->serial_number}}</span>
                    </div>
                    <div class="info-table">
                        <label>Item Price : </label>
                        <span>{{$asset->price}}</span>
                    </div>
                    <div class="info-table">
                        <label>Item Tax : </label>
                        <span>{{$asset->tax}}</span>
                    </div>
                    <div class="info-table">
                        <label>Make : </label>
                        <span>{{$asset->make}}</span>
                    </div>
                    <div class="info-table">
                        <label>Model : </label>
                        <span>{{$asset->model}}</span>
                    </div>
					<div class="info-table">
                        <label>Is working :</label>
                        <span>{{ ($asset->is_working == 1 ) ? 'Yes' : 'No' }}</span>
                    </div>
                    <div class="info-table">
                        <label>Invoice Number :</label>
                        <span>{{$asset->invoice_number}}</span>
                    </div>
                    <div class="info-table">
                        <label>Files :</label>
                        @if ( isset($files) )
                            @foreach ( $files as $file )
                                <span>
                                    <a href="{{$file->path}}" target="_blank" class="btn btn-info btn-sm crude-btn">{{$file->name}}</a>
                                </span>
                            @endforeach
                        @else
                           <span>
                                No files uploaded
                            </span>
                        @endif
                    </div>
                    <div class="info-table">
                        <label>Google Drive Link :</label>
                        @if ( isset($asset->google_drive_link) )
                            <span>
                                <a href="{{$asset->google_drive_link}}" target="_blank" class="btn btn-info btn-xs">View</a>
                            </span>
                        @else
                           <span>
                                Google Drive Detials are not present
                            </span>
                        @endif
                    </div>
                    <div class="info-table">
                        <label>Description :</label>
                        <span>
                            {{$asset->description}}
                        </span>
                    </div>
                    <div class="info-table">
                        <label>Is Assignable :</label>
                        <span>
                            @if($asset->is_assignable == 1)
                                <span class="label label-success custom-label">Yes</span>
                            @else 
                                <span class="label label-danger custom-label">No</span>
                            @endif
                        </span>
                    </div>
                </div>
			</div>
            <div class="col-sm-7">
                <div class=""  ng-app="myApp">
                    <asset-meta asset-id="{{$asset->id}}" />
                </div>
            </div>
		</div>
        @if($asset->is_assignable)
            <div class="row">
                <div class="col-sm-12">
                    <assign-asset asset-id="{{$asset->id}}"></assign-asset>
            </div>
            </div>
        @endif
</section>
<script>
	$(function () {
        $('#datetimepickerStart').datetimepicker({
            //Set max date to upcoming march
            maxDate: new Date().getMonth() <= 2 ? new Date(new Date().getFullYear()+"-03-31") : new Date(new Date().getFullYear()+1+"-03-31"),
            // minDate: new Date(new Date().getFullYear()+"2016-04-01"), //Set min date to April 1
            minDate:"2015-01-01", //Set min date to April 1
            format: 'YYYY-MM-DD'
        });

        $('#datetimepickerEnd').datetimepicker({
            //Set max date to upcoming march
            maxDate: new Date().getMonth() <= 2 ? new Date(new Date().getFullYear()+"-03-31") : new Date(new Date().getFullYear()+1+"-03-31"),
            format: 'YYYY-MM-DD'
        });
        if($("#datetimeinputstart").val()!="")
        {
            $('#datetimepickerEnd').data("DateTimePicker").minDate($("#datetimeinputstart").val());
        }

            $('.user_list').select2({
            placeholder: 'Select an option',
            allowClear:true
        });
    });
	</script>
@endsection

@section('js')
@parent
@include('pages/admin/asset-management/asset/edit-asset-assignment-modal')
	<script>
		function updateData() {
            id =  {{  isset($assignment->id) ? $assignment->id :''  }} ;
            start_date = $('#editAssetAssignmentModal input[name="edit_start_date"]').val();
            end_date = $('#editAssetAssignmentModal input[name="edit_end_date"]').val();
            comment = $('#editAssetAssignmentModal input[name="comments"]').val();

            $.ajax({
                type: 'POST',
                url: '/admin/asset/updateUser/'+id,
                dataType: 'text',
                data: {
                    'start_date': start_date,
                    'end_date': end_date,
                    'comment': comment
                },
                beforeSend: function() {
                    $("div.loading").show();
                    $("div.content").hide();
                },
                complete: function() {
                    $("div.loading").hide();
                    $("div.content").show();
                },
                success: function(data) {
                    $('#editAssetAssignmentModal').modal('hide');
                    window.location.reload();
                },
                error: function(errorResponse){
                    $('#editAssetAssignmentModal').modal('hide');
                    console.log(errorResponse);
                    window.location.reload();
                }
            });
	}
	$(function(){
		$('#editDetailModal').on("show.bs.modal", function (e) {
			let id = $(e.relatedTarget).data('id')
			$.ajax({
				type: 'GET',
				url: '/admin/project/'+id,
				beforeSend: function() {
					$("div.loading").show();
					$("div.content").hide();
				},
				complete: function() {
					$("div.loading").hide();
					$("div.content").show();
				},
				success: function(data) {
					dat = data;
					$('#editDetailModal input[name="start_date"]').val(data.start_date);
					$('#editDetailModal input[name="end_date"]').val(data.end_date);
				}
			});
		});

		$("#editDetailModal").on("hidden.bs.modal", function(){
			$(this).find("input")
				.val('')
				.end()
		});
	});
	</script>
@endsection
