@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
   <div class="breadcrumb-wrap">
      <div class="row">
         <div class="col-sm-8">
            <h1>Category</h1>
            <ol class="breadcrumb">
               <li><a href="/admin">Admin</a></li>
               <li class="active">Category</li>
            </ol>
         </div>
         <div class="col-sm-4 text-right">
            <a href="/admin/asset-management/asset-category/create" class="btn btn-success btn-sm crude-btn">
            <i class="fa fa-plus fa-fw"></i>
            ADD CATEGORY
            </a>
         </div>
      </div>
   </div>
   @if(!empty($errors->all()))
   <div class="alert alert-danger">
      @foreach ($errors->all() as $error)
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <span>{{ $error }}</span><br/>
      @endforeach
   </div>
   @endif
   @if (session('message'))
   <div class="alert alert-success">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <span>{{ session('message') }}</span><br/>
   </div>
   @endif
   <div class="user-list-view">
      <div class="panel panel-default">
         <table class="table table-striped">
            <tr>
               <th class="text-center">#</th>
               <th class="text-center">Name</th>
               <th class="pull-right">Action</th>
            </tr>
            @if( empty($assetCategoryList) || count($assetCategoryList) == 0 )
            <tr>
               <td>
               <td class="text-center">No Records found</td>
               </td>
            </tr>
            @endif
            @if(isset($assetCategoryList) > 0)
            @foreach($assetCategoryList as $index => $assetCategory)
            <tr>
               <td class="text-center">{{ $index + 1 }}</td>
               <td class="text-center"> {{ $assetCategory->name }} </td>
               <td class="pull-right">
                  <span>
                  <form name="showForm" method="get" action="/admin/asset-management/asset-category/{{$assetCategory->id}}"  style="display: inline-block;">
                     <button type="submit" class="btn btn-info btn-sm crude-btn"><i class="fa fa-eye fa-fw"></i>View</button> 
                  </form>
                  <span>
                  <span>
                     <form name="editForm" method="get" action="/admin/asset-management/asset-category/{{$assetCategory->id}}/edit"  style="display: inline-block;">
                        <button type="submit" class="btn btn-success btn-sm crude-btn"><i class="fa fa-edit fa-fw"></i>Edit</button> 
                     </form>
                  </span>
                  <span>
                     <form name="deleteForm" method="post" action="/admin/asset-management/asset-category/{{$assetCategory->id}}"  style="display: inline-block;">
                        <input name="_method" type="hidden" value="delete" />
                        <button type="submit" class="btn btn-danger btn-sm crude-btn"><i class="fa fa-trash fa-fw"></i>Delete</button> 
                     </form>
                  </span>
               </td>
            </tr>
            @endforeach
            @endif  
         </table>
      </div>
   </div>
</div>
</div>        
@endsection