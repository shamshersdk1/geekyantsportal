@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
   <div class="breadcrumb-wrap">
      <div class="row">
         <div class="col-sm-8">
            <h1>Category</h1>
            <ol class="breadcrumb">
               <li><a href="/admin">Admin</a></li>
               <li><a href="/admin/asset-management/asset-category">Category</a></li>
               <li class="active">Edit</li>
            </ol>
         </div>
         <div class="col-sm-4">
         </div>
      </div>
   </div>
   @if(!empty($errors->all()))
   <div class="alert alert-danger">
      @foreach ($errors->all() as $error)
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <span>{{ $error }}</span><br/>
      @endforeach
   </div>
   @endif
   @if (session('message'))
   <div class="alert alert-success">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <span>{{ session('message') }}</span><br/>
   </div>
   @endif
   <div class="panel panel-default">
      <div class="panel-body">
      <form  class="form-horizontal" name="updateForm" method="post" action="/admin/asset-management/asset-category/{{$asset_category->id}}" enctype="multipart/form-data">
         <input name="_method" type="hidden" value="PUT" />
         <div class="form-group">
            <label for="name" class="col-sm-2 col-sm-offset-2 control-label">Name :</label>
            <div class="col-sm-4">
               <input type="text" class="form-control" name="name" value="{{ $asset_category->name }}">
            </div>
         </div>
         <div class="text-center" style="margin-top: 30px;">
            <button type="submit" class="btn btn-primary btn-sm crude-btn "> SAVE</button>
         </div>
      </form>
      </div>
   </div>
</div>
@endsection