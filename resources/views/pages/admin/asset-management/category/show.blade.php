@extends('layouts.admin-dashboard')
@section('main-content')
    <div class="container-fluid">
        <div class="breadcrumb-wrap">
            <div class="row">
                <div class="col-sm-8">
                    <h1>Asset</h1>
                    <ol class="breadcrumb">
                        <li><a href="/admin">Admin</a></li>
                        <li><a href="/admin/asset-management/asset-category">Category</a></li>
                        <li class="active">{{$asset_category->id}}</li>
                    </ol>
                </div>
                <div class="col-sm-4">
                    
                </div>
            </div>
        </div>
        @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif
        <div class="row">
	        <div class="col-md-12">
	            <div class="panel panel-default">
				    <div class="panel-body">
				    	<form name = "myForm" style="padding:10px;" > 
			    			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			    			<b>Name :</b> {{$asset_category->name}} <br><br>
			    		</form>
				    </div>
				</div>
			</div>	
		</div>
@endsection