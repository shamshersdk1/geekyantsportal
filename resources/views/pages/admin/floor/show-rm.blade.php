@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
   <div class="breadcrumb-wrap">
      <div class="row">
         <div class="col-sm-8">
            <h1 class="admin-page-title">Seat</h1>
            <ol class="breadcrumb">
               <li><a href="/admin">Admin</a></li>
               <li><a href="{{ url('admin/seating-arrangement') }}">Seating Arrangement</a></li>
            </ol>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-md-12">
         @if(!empty($errors->all()))
         <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <span>{{ $error }}</span><br/>
            @endforeach
         </div>
         @endif
         @if (session('message'))
         <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <span>{{ session('message') }}</span><br/>
         </div>
         @endif
      </div>
   </div>
   @foreach($floors as $floor)
      <h3>{{$floor->name}}</h3>
      <div class="row" style="margin-left: 1px; margin-right: 1px; margin-top:5px;">
         <div class="col-md-2 entry-gate">Entry gate</div>
         @foreach($floorSeatings[$floor->id]['seats'] as $row)
            <div class="col-md-5 seat" >
                  <div class="col-md-12 seat" style="margin-bottom: 20px;padding:0">
                     @foreach($row as $seat)
                        <div class="col-md-4 individual">
                           <div class="seat-wrap flex-class">
                              <div style="text-align: center;" class="seat-wrap  flex-class">
                                 <div>
                                    <div style="text-align: center;" class="number flex-class">
                                       {{$seat->name }}
                                    </div>
                                 </div>
                                 @if(!$seat->assignable)
                                    <div class="available-seat flex-class">
                                          <div>
                                             unassignable
                                          </div>
                                    </div>
                                 @endif
                                 @if($seat->assignable && !$seat->seatUser->assigned_user)
                                    <div class="available-seat flex-class">
                                       <div>
                                          empty
                                       </div>
                                    </div>
                                 @endif                        
                                 <div class="detail-seat flex-class">
                                    <div  >
                                       {{$seat->seatUser->assigned_user->name}}
                                    </div>
                                    <div  class="emp-code">
                                       {{$seat->seatUser->assigned_user->employee_id}}
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        @endforeach         
                  </div>
            
            </div>
         @endforeach
      </div>
   @endforeach
</div>
@endsection