@extends('layouts.admin-dashboard')
@section('main-content')

<section class="new-project-section">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">Floors</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
			  			<li><a href="{{ url('admin/seat-management') }}">Floors</a></li>
			  			<li class="active">Add</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>

	    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif

		<form class="form-horizontal" method="post" action="/admin/seat-management" enctype="multipart/form-data">
			<div class="panel panel-default">
				<div class="panel-body">
				    <div class="row">
				    	<div class="col-md-12">
						  	<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Name</label>
						    	<div class="col-sm-6">
						      		<input type="text" class="form-control" id="" name="name" value="{{ old('name') }}">
						    	</div>
						  	</div>
						  	<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Number of Seats</label>
						    	<div class="col-sm-6">
						      		<input type="number" class="form-control" id="" name="number_of_seats" value="{{ old('number_of_seats') }}">
						    	</div>
						  	</div>
							<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Floor Number</label>
						    	<div class="col-sm-6">
						      		<input type="number" class="form-control" id="" name="floor_number" value="{{ old('floor_number') }}">
						    	</div>
						  	</div>
							  <div class="form-group">
								{{ Form::Label('type', 'Type ',array('class' =>'col-sm-2 control-label' )) }}
									<div class="col-md-8" style="padding:0">
										<div class="col-sm-4">
										{{ Form::select('type', ['building_1' => 'building_1', 'building_2' => 'building_2'],null,['id'=>'selectid2', 'style' => 'width:150%;height:40px'])}}
										</div>
									</div>
								</div>
							</div>
					  	</div>
					</div>
				</div>
			</div>
  	  		<div class="text-center">
		  		<button type="submit" class="btn btn-success">Create Floor</button>
		  	</div>
		</form>
	</div>
</section>
@endsection
