@extends('layouts.admin-dashboard')
@section('main-content')
<script type="text/javascript">
   window.floor_details = {{ $floor_details }}
   window.users = {{ $users }}
</script>
<!--  -->   
<div class="container-fluid">
   <div class="breadcrumb-wrap">
      <div class="row">
         <div class="col-sm-8">
            <h1 class="admin-page-title">Seat</h1>
            <ol class="breadcrumb">
               <li><a href="/admin">Admin</a></li>
               <li><a href="{{ url('admin/seat-management') }}">List of Floors</a></li>
               <li>Floor {{ $floor }}({{$floor_detail->name}})</a></li>
            </ol>
         </div>
         <div class="col-sm-4 seat-arrange">
            <div>Empty Seats: {{ $empty_seats }} </div>
            <div>Occupied Seats: {{ $occupied_seats }} </div>
            <a href={{"/admin/floors-seat/".$floor_detail->id}} class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Add Seat</a>
         </div> 
      </div>
   </div>
   <div class="row">
      <div class="col-md-12">
         @if(!empty($errors->all()))
         <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <span>{{ $error }}</span><br/>
            @endforeach
         </div>
         @endif
         @if (session('message'))
         <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <span>{{ session('message') }}</span><br/>
         </div>
         @endif
      </div>
   </div>
   <!--  -->
   <div ng-app="myApp" ng-controller="seatCtrl" class="row" style="margin-left: 1px; margin-right: 1px; margin-top:5px;">
      <div class="col-md-2 entry-gate">Entry gate</div>
      <div ng-repeat="seat in floor_details track by $index" class="col-md-5 seat" >
         <div class="col-md-12 seat" style="margin-bottom: 20px;padding:0">
            <div class="col-md-4 individual" ng-repeat="individual in seat" >
               <div ng-click="editSeat(individual)" ng-if="$index%12 < 6 " class="seat-wrap  flex-class">
                  <div style="text-align: center;" class="seat-wrap  flex-class">
                     <div ng-if="individual"  >
                        <div style="text-align: center;" class="number flex-class">
                           %% individual.name %%
                        </div>
                     </div>

                     
                     <div class="available-seat flex-class" ng-if="individual.assignable == 0">
                           <div>
                              unassignable
                           </div>
                        </div>
                     <div class="available-seat flex-class" ng-if="!individual.assignable == 0 &&  !individual.user_details">
                        <div>
                           empty
                        </div>
                     </div>

               
                     
                        <div class="detail-seat flex-class" ng-if="individual.user_details.floors_seat.assignable">
                           <div  >
                              %% individual.user_details.assigned_user.name %%
                           </div>
                           <div  class="emp-code">
                              %% individual.user_details.assigned_user.employee_id %%
                           </div>
                        </div>
                        
                   
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection