@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-6">
                <h1 class="admin-page-title">Seat</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li><a href="{{ url('admin/seat-management') }}">List of Floors</a></li>
        		</ol>
            </div>
			<div class="col-sm-6">
			<div class="pull-right m-t-10">
				<a href="/admin/floors-seat/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Add Seat</a>
            </div>
			<div class="pull-right m-t-10" style="margin-right: 5px">
				<a href="/admin/seat-management/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Add Floor</a>
            </div>
			</div>
		</div>
	</div>
	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
	<div class="panel panel-default noradius">
		<table class="table table-striped">
			<th class="text-left" >#</th>
			<th class="text-left">Name</th>
			<th class="text-left" >Total Seats</th>
			<th class="text-left" >Occupied Seats</th>
			<th class="text-left"  >Type</th>
			<th class="text-left"  >Created At</th>
			<th class="text-right" >Actions</th>
			@if(count($floors) > 0)
	    		@foreach($floors as $floor)
					<tr>
						<td >
							{{ $floor->id }} 
						</td>
						<td >
							{{ $floor->name }} 
						</td>
						<td >
							<span class="label label-info custom-label">{{ $floor->number_of_seats }}</span>
						</td>
						<td >
							<span class="label label-warning custom-label">{{ $floor->getOccupiedCount() }}</span>
						</td>
						<td >
							{{ $floor->type }} 
						</td>
						<td >
							{{ date_in_view($floor->created_at) }} 
						</td>

						<td class="text-right" >
							<a href="/admin/seat-management/{{$floor->id}}/edit" class="btn btn-success crud-btn btn-sm"><i class="fa fa-pencil btn-icon-space" aria-hidden="true"></i>Edit</a>
							<a href="/admin/seat-management/{{$floor->id}}" class="btn btn-success crud-btn btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View Floor Plan</a>
						</td>
					</tr>
				@endforeach
	    	@else
	    		<tr>
	    			<td colspan="3">No results found.</td>
	    			<td></td>
	    			<td></td>
	    		</tr>
	    	@endif
	   </table>
	</div>
</div>	
@endsection

