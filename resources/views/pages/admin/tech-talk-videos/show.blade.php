@extends('layouts.admin-dashboard')
@section('main-content')
<section>
   <div class="container-fluid">
      <div class="breadcrumb-wrap">
         <div class="row">
            <div class="col-sm-6">
               <h1>Tech Talk Videos</h1>
               <ol class="breadcrumb">
                  <li><a href="/admin">Admin</a></li>
                  <li><a href="/admin/tech-talk-videos">Tech Talk Videos</a></li>
                  <li class="active">{{$techTalkVideoObj->id}}</li>
               </ol>
            </div>
            <div class="col-sm-6 text-right">
               <a href="/admin/tech-talk-videos" class="btn btn-primary"><i class="fa fa-caret-left fa-fw"></i>Back</a>
            </div>
         </div>
      </div>
      @if(!empty($errors->all()))
      <div class="alert alert-danger">
         @foreach ($errors->all() as $error)
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
         <span>{{ $error }}</span><br/>
         @endforeach
      </div>
      @endif
      @if (session('message'))
      <div class="alert alert-success">
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
         <span>{{ session('message') }}</span><br/>
      </div>
      @endif
      <div class="row cms-dev">
         <div class="col-md-12">
            <h4 >Tech Talk Videos Details</h4>
            <div class="panel panel-default">
               <div class="panel-body row">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <div class=" col-md-6">
                     <label>YouTube URL : </label>
                     <span><a href="{{$techTalkVideoObj->url}}" target="_blank">{{$techTalkVideoObj->url}}</a></span>
                  </div>
                  <div class=" col-md-6">
                     <label>Title : </label>
                     <span>{{$techTalkVideoObj->title}}</span>
                  </div>
                  <div class="col-md-6">
                     <label>Status : </label>
                     <span>{{!empty($techTalkVideoObj->status) ? 'Active' : 'Inactive' }}</span>
                  </div>
                  <div class="col-md-12">
                     <label>Description : </label>
                     <span>{{ $techTalkVideoObj->description }}</span>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>

@endsection