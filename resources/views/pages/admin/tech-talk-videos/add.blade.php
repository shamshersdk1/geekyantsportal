@extends('layouts.admin-dashboard')
@section('main-content')

<section class="new-project-section">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">Tech Talk Videos</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
			  			<li><a href="{{ url('admin/tech-talk-videos') }}">Tech Talk Videos</a></li>
			  			<li class="active">Add</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>

	    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif

		<form class="form-horizontal" method="post" action="/admin/tech-talk-videos" enctype="multipart/form-data">
			<div class="row">
		    	<div class="col-md-8 col-md-offset-2">
		    		<div class="panel panel-default">
						<div class="panel-body">
				    
						  	<div class="form-group">
						    	<label for="" class="col-sm-3 control-label">YouTube URL:</label>
						    	<div class="col-sm-8">
						      		<input type="text" class="form-control" id="" name="url" value="{{ old('url') }}" required>
						    	</div>
						  	</div>
						  	<div class="form-group">
						    	<label for="" class="col-sm-3 control-label">Title</label>
						    	<div class="col-sm-8">
						      		<input type="text" class="form-control" id="" name="title" value="{{ old('title') }}"  required>
						    	</div>
						  	</div>
						  	<div class="form-group">
						    	<label for="" class="col-sm-3 control-label">By</label>
						    	<div class="col-sm-8">
						      		<input type="text" class="form-control" id="" name="user" value="{{ old('user') }}"  required>
						    	</div>
						  	</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Status:</label>
								<div class="col-sm-4">
									<select class="form-control" name="status">
										<option value="1">Active</option>
										<option value="0">Inactive</option>
									</select>
								</div>
							</div>
						  	<div class="form-group no-margin">
						    	<label for="" class="col-sm-3 control-label">Description</label>
						    	<div class="col-sm-8">
						      		<textarea class="form-control" rows="5" name="description">{{ old('description') }}</textarea>
						    	</div>
						  	</div>
							
					  	</div>
					</div>
				</div>
			</div>
  	  		<div class="text-center">
		  		<button type="submit" class="btn btn-success">Save</button>
		  	</div>
		</form>
	</div>
</section>
@endsection
