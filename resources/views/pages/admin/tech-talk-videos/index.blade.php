@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Tech Talk Videos</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
		  			<li class="active">Tech Talk Videos</li>
        		</ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
				<a href="/admin/tech-talk-videos/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Add</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
	    	@if(!empty($errors->all()))
	            <div class="alert alert-danger">
	                @foreach ($errors->all() as $error)
	                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                    <span>{{ $error }}</span><br/>
	                  @endforeach
	            </div>
	        @endif
	        @if (session('message'))
	            <div class="alert alert-success">
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ session('message') }}</span><br/>
	            </div>
	        @endif
		  <div class="user-list-view">
            <div class="panel panel-default">
            	<table class="table table-striped">
					<tr>
						<th width="5%" class="sorting_asc">#</th>
						<th width="30%">Tech Talk Info</th>
						<th width="15%">By</th>
						<th width="10%">Status</th>
						<th width="15%">Created At</th>
						<th class="text-right sorting_asc" width="15%">Actions</th>
					</tr>
					@if(count($techTalkVideos) > 0)
						@foreach($techTalkVideos as $techTalkVideo)
							<tr>
								<td><a>{{$techTalkVideo->id}}</a></td>
								<td><strong>{{$techTalkVideo->title}}</strong><br />
									<a href="{{$techTalkVideo->url}}" target="_blank">{{$techTalkVideo->url}}</a><br />
									{{$techTalkVideo->description}}
								</td>
								<td>Megha</td>
								<td>
									<div class="onoffswitch">
										<input type="checkbox" value="{{$techTalkVideo->id}}" name="onoffswitch[]" onchange="toggle(this)"
										class="onoffswitch-checkbox" id="{{$techTalkVideo->id}}"
										<?php echo (!empty($techTalkVideo->status)&&$techTalkVideo->status==1) ? 'checked': ''; ?>>
										<label class="onoffswitch-label" for= {{$techTalkVideo->id}} >
											<span class="onoffswitch-inner"></span>
											<span class="onoffswitch-switch"></span>
										</label>
									</div>
								</td>
								<td>
									{{ datetime_in_view($techTalkVideo->created_at) }} 
								</td>
								<td class="text-right">
									<a href="/admin/tech-talk-videos/{{$techTalkVideo->id}}" class="btn btn-success crude-btn btn-sm">View</a>
									<a href="/admin/tech-talk-videos/{{$techTalkVideo->id}}/edit" class="btn btn-info btn-sm crude-btn">Edit</a>
									<form action="/admin/tech-talk-videos/{{$techTalkVideo->id}}" method="post" style="display:inline-block;">
										<input name="_method" type="hidden" value="DELETE">
										<button class="btn btn-danger btn-sm crude-btn" type="submit">Delete</button>
									</form>
								</td>
							</tr>
						@endforeach
	            	@else
	            		<tr>
	            			<td colspan="7" class="text-center">No results found.</td>
	            		</tr>
	            	@endif
            	</table>
				<div class="col-md-12">
                    <div class="pageination pull-right">
                        <nav aria-label="Page navigation">
                              <ul class="pagination">
                                {{ $techTalkVideos->render() }}
                              </ul>
                        </nav>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>
</div>

@endsection
