@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Edit Project</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/technology') }}">Technology</a></li>
                    <li class="active">Edit</li>
                </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12" style="margin-top:20px;">
    	@if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span><br/>
                  @endforeach
            </div>
        @endif
        @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
        @endif
        <form name = "myForm" method="post" action="/admin/technology/{{$technology->id}}" enctype="multipart/form-data" >
            <div class="panel panel-default">
                <div class="panel-body">
                <div class="row">
                    <div class="col-md-8 col-md-offset-1">
                        <input name="_method" type="hidden" value="PUT">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Enter Name:</label>
                            <input type="text" name="name" class="form-control" id="exampleInputEmail1" value="{{$technology->name}}">
                            <p id="name" style="color:red;"></p>
                            <label for="category">Select Category:</label>
                            <select name="category" id="category-selector" class="form-control">
                                @foreach($categories as $category)
                                    <option value="{{$category['id']}}">{{$category['name']}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div> 
                </div>
            </div>
            <div class="col-md-12 text-center">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-rotate-left fa-fw"></i> Cancel</button>
                <button type="submit" class="btn btn-success">Update</button>
            </div>
    	</form>	
	</div>		
	</div>
</div>
<script>
    $(function(){
        $("#category-selector option[value={{$technology->technology_category_id}}]").prop('selected', true);
    });
</script>
@endsection