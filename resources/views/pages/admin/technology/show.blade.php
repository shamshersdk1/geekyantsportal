@extends('layouts.admin-dashboard')
@section('main-content')
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">Technology View</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
				  		<li><a href="{{ url('admin/technology') }}">Technology</a></li>
				  		<li class="active">View</li>
		        	</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>
	    <div class="row">
	        <div class="col-md-12">
	            <div class="panel panel-default">
				    <div class="panel-body">
				    	<form name = "myForm" style="padding:10px;" > 
			    			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			    			<b>Id :</b> {{$technology->id}} <br><br>
			    			<b>Name :</b> {{$technology->name}} <br><br>
							<b>Category :</b> {{$technology->category->name}} <br><br>
			    		</form>
				    </div>
				</div>
				<!-- <a href="/admin/technology" class="btn btn-primary crud-btn">Back</a> -->
			</div>	
		</div>
	</div>
@endsection