@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
	<div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">List of Technologies</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
		  			<li><a href="{{ url('admin/technology') }}">Technology</a></li>
        		</ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <a href="/admin/technology/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i> Add new Technology</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
    	@if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span><br/>
                  @endforeach
            </div>
        @endif
        @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
        @endif
        	
            <div class="panel panel-default">
            	<!-- <div class="panel-heading" style="font-size:initial;"><b>List of Technologies</b></div> -->
            	
	            	<table class="table table-striped">
                    <thead>
	            		<th width="10%">#</th>
	            		<th width="15%">Name of Technology</th>
	            		<th width="15%">Category</th>
	            		<th class="text-right">Actions</th>
	            	</thead>
	            		@if(count($technologies) > 0)
		            		@foreach($technologies as $technology)
			            		<tr>
			            			<td class="td-text"><a>{{$technology->id}}</a></td>
			            			<td class="td-text"><a href="/admin/technology/{{$technology->id}}"> {{$technology->name}}</a></td>
			            			<td class="td-text"><a>@if(!empty($technology->category->name)){{$technology->category->name}} @else <p style="font-size:1em;color:red">Category not defined</p>@endif</a></td>
			            			<td class="text-right">
		    				    		<a href="/admin/technology/{{$technology->id}}" class="btn btn-success crud-btn btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View</a>	
		    					    	<a href="/admin/technology/{{$technology->id}}/edit" class="btn btn-info crud-btn btn-sm"><i class="fa fa-pencil btn-icon-space" aria-hidden="true"></i>Edit</a>

		    					    	<form action="/admin/technology/{{$technology->id}}" method="post" style="display:inline-block;" onsubmit="return deletetech()">
		    					    		<input name="_method" type="hidden" value="DELETE">
		    					    		<button class="btn btn-danger crud-btn btn-sm" type="submit"><i class="fa fa-trash btn-icon-space" aria-hidden="true"></i>Delete</button>
		    					    	</form>
			            			</td>
			            		</tr>
		            		@endforeach
		            	@else
		            		<tr>
		            			<td colspan="3">No results found.</td>
		            			<td></td>
		            		</tr>
		            	@endif
	            	</table>
	            
			</div>
			<!-- <a href="/admin/technology/create" class="btn btn-success btn-sm crud-btn">Add new Technology</a> -->
		</div>	
	</div>
</div>

@endsection
@section('js')
@parent
	<script>
		
		function deletetech() {
			var x = confirm("Are you sure you want to delete?");
			if (x)
				return true;
			else
				return false;
		}
	</script>
@endsection
