@extends('layouts.admin-dashboard')
    @section('main-content')
    <div class="container-fluid" ng-app="myApp">
        <div class="breadcrumb-wrap">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="admin-page-title">Reporting Manager Leave Dashboard</h1>
                    <ol class="breadcrumb">
                        <li><a href="/admin">Admin</a></li>
                        <li class="active">Reporting Manager Leave dashboard</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="row bg-white search-section-top">
            <form action="/admin/reporting-manager-leaves"  method="get" class="row">
                <div class="col-sm-4">
                    <input type="text" id="su" name="searchuser" placeholder="User's Name" value="<?=( isset( $_GET['searchuser'] ) ? $_GET['searchuser'] : '' )?>"   class="form-control searchuser" >
                </div>
                <div class="col-sm-3">
                    <div class='input-group date' id='datetimepickerStart_search'>
                        <input type='text' id="sd" class="form-control" value="<?=( isset( $_GET['start_date'] ) ? $_GET['start_date'] : '' )?>" autocomplete="off" name="start_date" placeholder="Start Date" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class='input-group date' id='datetimepickerEnd_search'>
                        <input type='text' id="ed" class="form-control" name="end_date" value="<?=( isset( $_GET['end_date'] ) ? $_GET['end_date'] : '' )?>" autocomplete="off" placeholder="End Date" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
                <div class="col-sm-2">
                    <span class="input-group-btn text-right">
                        <button type="submit" style="margin-right:15px" class="btn btn-primary btn-sm button-space">Search</button>
                        <a href="/admin/reporting-manager-leaves" class="btn btn-primary btn-sm button-space">Reset</a>
                    </span>
                </div>  
            </form>
        </div>
        <!-- ============================= admin leave Board ================================ -->
        <div class="row" ng-controller="reportingManagerLeaveCtrl">
            <div class="col-md-12">
                @if(!empty($errors->all()))
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <span>{{ $error }}</span><br/>
                          @endforeach
                    </div>
                @endif
                @if (session('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ session('message') }}</span><br/>
                    </div>
                @endif
            </div>
            <div class="col-md-12">
                <h4>List of Leaves</h4>
                <div class="panel panel-default">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="15%">Name</th>
                                    <th width="15%">Type</th>
                                    <th width="10%">From Date</th>
                                    <th width="10%">To Date</th>
                                    <th width="10%">Working Days</th>
                                    <th width="10%">Applied On</th>
                                    <th width="25%">Status</th>
                                    <th width="10%" class="text-right">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($leaves))
                                    @if(count($leaves) > 0)
                                        @foreach($leaves as $leave)
                                            @if(isset($leave->user))
                                                <tr>
                                                    <td>{{$leave->id}}</td>
                                                    <td>{{$leave->user->name}}</td>
                                                    <td>
                                                        {{ $leave->leaveType ? $leave->leaveType->title : '' }}
                                                    </td>
                                                    <td>{{date("d M Y", strtotime($leave->start_date))}}</td>
                                                    <td>{{date("d M Y", strtotime($leave->end_date))}}</td>
                                                    @if($leave->days==0.5)
                                                    <td>{{ucfirst($leave->half)}} half</td>
                                                    @else
                                                    <td>{{floor($leave->days)}}</td>
                                                    @endif
                                                    <td>{{date("d M Y", strtotime($leave->created_at))}}</td>
                                                    <td>
                                                    @if($leave->status == 'pending')
                                                        @if ( $leave->isOtherType)
                                                            <span class="label label-primary custom-label">Waiting for management team</span>
                                                        @elseif($leave->parent == 1)
                                                            @if($leave->ready == 0)
                                                                <span class="label label-info custom-label">Waiting for project managers</span>
                                                            @else
                                                                <span class="label label-warning custom-label">Waiting for your action</span>
                                                            @endif
                                                        @elseif($leave->editable == 1)
                                                            <span class="label label-warning custom-label">Waiting for your action</span>
                                                        @else
                                                            <span class="label label-info custom-label">Waiting for reporting manager</span>
                                                        @endif
                                                    @elseif($leave->status == 'approved')
                                                        <span class="label label-success custom-label">Approved</span>
                                                    @elseif($leave->status == 'rejected')
                                                        <span class="label label-danger custom-label">Rejected</span>
                                                    @elseif($leave->status == 'cancelled')
                                                        <span class="label label-primary custom-label">Cancelled</span>
                                                    @else
                                                        <span class="label label-default custom-label">Unavailable</span>
                                                    @endif
                                                    </td>
                                                    <td class="text-right">
                                                        <button class="btn btn-primary btn-sm" type="button"
                                                                    ng-click="showModal({{$leave->id}})" data-title="Algorithms"><i class="fa fa-eye"></i> View</button>
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="9">No Record found.</td>
                                        </tr>
                                    @endif
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="pull-right">
                @if(!empty($leaves)&&count($leaves)>0)
                    {{ $leaves->render() }}
                @endif
                </div>
            </div>
        </div>
    </div>

<!-- =============================Leave Modal ================================ -->
@include('pages/admin/reporting-manager-leaves/edit-leave-modal')
@include('pages/admin/reporting-manager-leaves/leave-overlap-modal')
@include('pages/admin/reporting-manager-leaves/javascript')

@endsection