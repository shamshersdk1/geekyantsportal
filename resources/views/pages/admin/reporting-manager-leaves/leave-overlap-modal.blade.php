<style>
#leaveOverlapModal .modal-dialog {
    width: 100%;
    margin: 0;
    padding: 0;
    right:0;
    bottom:0;
    display: inline-block;
}
#leaveOverlapModal .modal-body {
    height: 86vh;
    overflow-y: auto;
    overflow-x: auto;
}
#leaveOverlapModal .modal-header {
    height: 7vh;
}
#leaveOverlapModal .modal-footer {
    height: 7vh;
}
#leaveOverlapModal .modal {
    text-align: center;
}
</style>
<div class="modal fade modal-fullscreen" id="leaveOverlapModal" tabindex="-1" role="dialog" aria-labelledby="leaveOverlapModalLabel">
	<form class="approve-cancel-Form" method="post" id="reasons-form" name="modal-projects-form" enctype="multipart/form-data">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" style="float:left;display:block" id="leaveOverlapModalLabel">Leave Overlapping applied by <span id="user_name"></span></h4>
                </div>
                <div class="modal-body loading" style="min-height: 543px;">
                    <div class="col-md-2 col-md-offset-5">
                        <i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
                    </div>
                </div>
                <div class="modal-body content">
                <div class="row">
                    <div class="col-md-12 ma-bot-10" id="statusLabel">
                        <span style="margin-bottom:10px" class="status label label-warning custom-label pull-right">Waiting for your action</span><label class="pull-right" for="">Status:&nbsp</label>
                    </div>
                    <div class="col-md-3 ma-bot-10">
                        <div class="form-group ma-bot-10">
                            <label class="control-label" for="type">Leave Type</label>
                                <select class="form-control" style="width:100%" id="leave_type_edit" name="type" disabled>
                                    <option>Select Leave type</option>
                                    <option value="sick" >Sick Leave</option>
                                    <option value="unpaid" >Unpaid Leave</option>
                                    <option value="paid" >Paid Leave</option>
                                    <option value="half" >Half-day Leave</option>
                                    <option value="compoff" >Comp-Off</option>
                                </select>
                        </div>
                    </div>
                    <!-- ======================== Date Picker ======================= -->
                            <div class="form-group">
                                <div class="col-md-3">
                                    <label for="start_date">From</label>
                                    <div class='input-group date' id='datetimepickerStart_edit'>
                                        <input type='text' class="form-control" autocomplete="off" name="start_date" placeholder="Start Date" disabled/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-3" id="end_date_div_edit">
                                    <label for="end_date">To</label>
                                    <div class='input-group date' id='datetimepickerEnd_edit'>
                                        <input type='text' class="form-control" name="end_date" placeholder="End Date" autocomplete="off" disabled/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-3" id="half_day_div_edit">
                                    <div class="form-group">
                                        <label for="half">Select Half</label>
                                        <select class="form-control" name="half" disabled>
                                            <option value="first">First Half</option>
                                            <option value="second">Second Half</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 ma-bot-10">
                                <label>Duration</label>
                                <br>
                                <label id="duration-label" style="padding-top:10px"></label>
                            </div>
                    <!-- ====================== reasones ================ -->
                    <form class="approve-cancel-Form" method="post" style="display:inline">
                        <div class="col-md-12 ma-top-10">
                            <div class="form-group">
                                <label for="">Reason</label>
                                <textarea class="form-control" rows="5" placeholder="Please enter reason for applying leave" name="reason" disabled></textarea>
                            </div>
                        </div>
                    </div>
                    <label>Projects with conflicts</label>
                    <label id="rejectreasonlabel">Reason for rejection of leave</label>
                    <textarea id="rejectreasonoverlap" class="form-control" rows="3"></textarea>
                </div>
                <div class="modal-footer" style="text-align:right">
                    <span class="pull-right">
                        <button type="button" name="reject" style="margin-right:10px" class="btn btn-danger overlap-leave-reject-btn">
                            Reject
                        </button>
                        <button class="btn btn-warning" style="margin-right:10px" type="submit" name="cancel" value="cancel">Cancel</button>
                        <button class="btn btn-success" type="submit" name="approve" value="approve">Approve</button>
                    </span>
                </div>
            </div>
        </div>
    </form>
</div>