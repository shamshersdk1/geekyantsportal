<script type="text/javascript">  
     $(function() {
 
    let leaveInfo;
    function rejectLeave() {
        if($('#rejectreason').val()!="")
        {
            if(confirm("Are you sure?"))
            {
                leaveInfo.rejectReason=$('#rejectreason').val();
                $.ajax({
                    type: 'PUT',
                    url: '/admin/reporting-manager-leaves/reject/'+leaveInfo.id,
                    dataType: 'text',
                    data: leaveInfo,
                    beforeSend: function() {
                        $("div.loading").show();
                        $("div.content").hide();
                    },
                    complete: function() {
                        $("div.loading").hide();
                        $("div.content").show();
                    },
                    success: function(data) {
                        $('#editLeaveModal').modal('hide');
                        $("#leaveOverlapModal").modal('hide');
                        window.location.reload();
                    },
                    error: function(errorResponse){
                        $('#editLeaveModal').modal('hide');
                        $("#leaveOverlapModal").modal('hide');
                        window.location.reload();
                    }
                });
            }
        } else {
            alert("You must specify the reason for rejecting the leave");
        }
    }
    function rejectLeaveOverlap()
    {
        if($('#rejectreasonoverlap').val()!="")
        {
            if(confirm("Are you sure?")) {
                leaveInfo.rejectReason=$('#rejectreasonoverlap').val();
                $.ajax({
                    type: 'PUT',
                    url: '/admin/reporting-manager-leaves/reject/'+leaveInfo.id,
                    dataType: 'text',
                    data: leaveInfo,
                    beforeSend: function() {
                        $("div.loading").show();
                        $("div.content").hide();
                    },
                    complete: function() {
                        $("div.loading").hide();
                        $("div.content").show();
                    },
                    success: function(data) {
                        $('#editLeaveModal').modal('hide');
                        $("#leaveOverlapModal").modal('hide');
                        window.location.reload();
                    },
                    error: function(errorResponse){
                        $('#editLeaveModal').modal('hide');
                        $("#leaveOverlapModal").modal('hide');
                        window.location.reload();
                    }
                });
            }
        } else {
        alert("You must specify the reason for rejecting the leave");
        }
    }
        
        var rejectOverlap = rejectLeaveOverlap;
        $(".overlap-leave-reject-btn").on('click', function() {
            rejectOverlap();
        });
        var rejectLeavefunc = rejectLeave;
        $(".leave-reject-btn").on('click', function() {
            rejectLeavefunc();
        });
        $('.approve-cancel-Form').submit(function(){
            return confirm('Are you sure?');
        });

        $('.rejectform').submit(function(){
            event.preventDefault();
        });

        //Reset Edit Form
        $("#editLeaveModal").on("hidden.bs.modal", function(){
            $(this).find("input,textarea,select")
                .val('')
                .end();
        $("#editLeaveModal .status-info-label").remove();
        });

        $('.leaveDetailButton').on("click", function (e) {
            let id = this.getAttribute('data-id');
            $.ajax({
                type: 'GET',
                url: '/admin/reporting-manager-leaves/'+id,
                success: function(data) {
                    leaveInfo = data;
                    if(data.days > 1) {
                        var duration = Math.trunc(data.days) + " days";
                    } else {
                        if(data.days < 1) {
                            var duration = "Half day";
                        } else {
                            var duration = Math.trunc(data.days) + " day";
                        }
                    }
                    if(data.overlap != 1) {
                        $("div.loading").hide();
                        $("div.content").show();
                        $("#editLeaveModal").modal('show');
                        $('#editLeaveModal select[name="type"]').val(data.type);
                        $('#editLeaveModal input[name="start_date"]').val(data.start_date);
                        $('#editLeaveModal input[name="end_date"]').val(data.end_date);
                        $('#editLeaveModal textarea[name="reason"]').val(data.reason);
                        $('#editLeaveModal #duration-label').text(duration);
                        $('#editLeaveModal span.status').text(data.status.charAt(0).toUpperCase()+data.status.slice(1));
                        $('#editLeaveModal span#user_name').text(data.user.name);
                        $('#editLeaveModal span#sick_leaves').text(data.sickLeaves);
                        $('#editLeaveModal span#paid_leaves').text(data.paidLeaves);
                        $('#editLeaveModal button[name="approve"]').show();
                        $('#editLeaveModal button[name="reject"]').show();
                        $('#editLeaveModal button[name="cancel"]').show();
                        $('#editLeaveModal #rejectreason').parent().show();
                        $('#editLeaveModal #rejectreason').attr('disabled',false);
                        $('#editLeaveModal input[name="client"]').parent().hide();
                        $('#editLeaveModal span.status').removeClass("label-success");
                        $('#editLeaveModal span.status').removeClass("label-danger");
                        $('#editLeaveModal span.status').removeClass("label-primary");
                        $('#editLeaveModal span.status').removeClass("label-info");
                        $('#editLeaveModal span.status').removeClass("label-warning");
                        $('#editLeaveModal .approve-cancel-Form').attr('action','/admin/reporting-manager-leaves/'+data.id);
                        switch(data.status) {
                            case 'approved':
                                $('#editLeaveModal button[name="approve"]').hide();
                                $('#editLeaveModal button[name="reject"]').hide();
                                $('#editLeaveModal #rejectreason').parent().hide();
                                $('#editLeaveModal span.status').addClass("label-success");
                                break;
                            case 'rejected':
                                $('#editLeaveModal button[name="approve"]').hide();
                                $('#editLeaveModal button[name="reject"]').hide();
                                $('#editLeaveModal button[name="cancel"]').hide();
                                $('#editLeaveModal #rejectreason').val(data.cancellation_reason);
                                $('#editLeaveModal #rejectreason').attr('disabled',true);
                                $('#editLeaveModal span.status').addClass("label-danger");
                                break;
                            case 'cancelled':
                                $('#editLeaveModal button[name="approve"]').hide();
                                $('#editLeaveModal button[name="reject"]').hide();
                                $('#editLeaveModal button[name="cancel"]').hide();
                                $('#editLeaveModal #rejectreason').parent().hide();
                                $('#editLeaveModal span.status').addClass("label-primary");
                                break;
                            default:
                                $('#editLeaveModal input[name="client"]').parent().show();
                                $('#editLeaveModal span.status').addClass("label-info");
                                break;
                        }
                        if(data.parent == 1) {
                            if(data.status  == 'pending') {
                                $('#editLeaveModal span.status').text('Waiting for you action');
                                $('#editLeaveModal span.status').addClass("label-warning");
                            } else {
                                $('#editLeaveModal span.status').text('Approved');
                                $('#editLeaveModal span.status').addClass("label-success");
                            }
                            $('#editLeaveModal .modal-body.content').append('<label  class="manager-status-label status-info-label"> Project Managers Status</label>');
                            if(data.projectManagers.length == 0)
                            {
                                $('#editLeaveModal .manager-status-label').after('<div style="display:block;border-radius:2px" class="list-group-item status-info-label"> No Managers Allocated </div>');
                            } else {
                                data.projectManagers.forEach(function (element){
                                    $arg = '<div class="row"><div class=" col-md-12"> <div class="status-info-label list-group-item col-md-12"><span>';
                                    $arg= $arg+element.manager.name;
                                    var date = new Date(element.updated_at);
                                    $arg = $arg+'</span><spam  >';
                                    if(element.status != 'pending') {
                                        $arg = $arg+'on '+date.toDateString()+'</small>';
                                    }      
                                    $arg = $arg+'<span class="label custom-label pull-right ';
                                    switch(element.status) {
                                        case 'approved' :
                                            $arg = $arg+'label-success ">';
                                            break;
                                        case 'pending' :
                                            $arg = $arg+'label-info ">';
                                            break;
                                        case 'cancelled' :
                                            $arg = $arg+'label-primary ">';
                                            break;
                                        case 'rejected' :
                                            $arg = $arg+'label-danger ">';
                                            break;
                                        default :
                                            $arg = $arg+'label-default ">';
                                    }
                                    $arg = $arg+element.status.charAt(0).toUpperCase()+element.status.slice(1)+'</span><span > - ';
                                    $projects = "";
                                    if(element.projects) {
                                        element.projects.forEach(function (proj_name){
                                            $projects = $projects+proj_name+", ";
                                        });
                                    }
                                    if($projects.length > 0) {
                                        $projects = $projects.slice(0, -2);
                                        $arg = $arg+$projects;
                                    }
                                    $arg = $arg+'</span></div></div></div>';
                                    $('#editLeaveModal .manager-status-label').after($arg);
                                });
                            }
                            if(data.ready == 0) {
                                // $('#editLeaveModal button[name="approve"]').hide();
                                // $('#editLeaveModal button[name="reject"]').hide();
                                // $('#editLeaveModal button[name="cancel"]').hide();
                                // $('#editLeaveModal #rejectreason').parent().hide();
                                // $('#editLeaveModal input[name="client"]').parent().hide();
                                $('#editLeaveModal span.status').text('Waiting for project managers');
                                $('#editLeaveModal span.status').addClass("label-info");
                                $('#editLeaveModal span.status').removeClass("label-warning");
                            }
                        } else {
                            $('#editLeaveModal span.status').text('Waiting for you action');
                            $('#editLeaveModal span.status').addClass("label-warning");
                            if(data.status != 'pending') {
                                $('#editLeaveModal button[name="cancel"]').hide();
                            } else {
                                if(data.editable == 0) {
                                    $('#editLeaveModal button[name="approve"]').hide();
                                    $('#editLeaveModal button[name="reject"]').hide();
                                    $('#editLeaveModal button[name="cancel"]').hide();
                                    $('#editLeaveModal #rejectreason').parent().hide();
                                    $('#editLeaveModal input[name="client"]').parent().hide();
                                    $('#editLeaveModal span.status').text('Waiting for reporting manager');
                                    $('#editLeaveModal span.status').addClass("label-info");
                                    $('#editLeaveModal span.status').removeClass("label-warning");
                                }
                            }
                        }
                        if(data.type=='half') {
                            $('#end_date_div_edit').hide();
                            $('#half_day_div_edit').show();
                            $('#datetimeinputstart_edit').attr('placeholder','Date');
                            $('#editLeaveModal select[name="half"]').val(data.half);
                        }
                    } else {
                        $('.projects-table').remove();
                        $("#leaveOverlapModal .status-info-label").remove();
                        $("div.loading").hide();
                        $("div.content").show();
                        $('#leaveOverlapModal select[name="type"]').val(data.type);
                        $('#leaveOverlapModal input[name="start_date"]').val(data.start_date);
                        $('#leaveOverlapModal input[name="end_date"]').val(data.end_date);
                        $('#leaveOverlapModal textarea[name="reason"]').val(data.reason);
                        $('#leaveOverlapModal #duration-label').text(duration);
                        // $('#leaveOverlapModal span.status').text(data.status.charAt(0).toUpperCase()+data.status.slice(1));
                        $('#leaveOverlapModal span#user_name').text(data.user.name);
                        $('#leaveOverlapModal button[name="approve"]').show();
                        $('#leaveOverlapModal button[name="reject"]').show();
                        $('#leaveOverlapModal button[name="cancel"]').show();
                        $('#leaveOverlapModal #rejectreasonoverlap').show();
                        $('#leaveOverlapModal #rejectreasonlabel').show();
                        $('#leaveOverlapModal #rejectreasonoverlap').attr('disabled',false);
                        // $('#leaveOverlapModal input[name="client"]').parent().hide();
                        // $('#leaveOverlapModal span.status').removeClass("label-success");
                        // $('#leaveOverlapModal span.status').removeClass("label-danger");
                        // $('#leaveOverlapModal span.status').removeClass("label-primary");
                        // $('#leaveOverlapModal span.status').removeClass("label-info");
                        // $('#leaveOverlapModal span.status').removeClass("label-warning");
                        $('#leaveOverlapModal .approve-cancel-Form').attr('action','/admin/reporting-manager-leaves/'+data.id);
                        
                        switch(data.status) {
                            case 'approved':
                                $('#leaveOverlapModal button[name="approve"]').hide();
                                $('#leaveOverlapModal button[name="reject"]').hide();
                                $('#leaveOverlapModal #rejectreasonoverlap').parent().hide();
                                // $('#leaveOverlapModal span.status').addClass("label-success");
                                break;
                            case 'rejected':
                                $('#leaveOverlapModal button[name="approve"]').hide();
                                $('#leaveOverlapModal button[name="reject"]').hide();
                                $('#leaveOverlapModal button[name="cancel"]').hide();
                                $('#leaveOverlapModal #rejectreasonoverlap').val(data.cancellation_reason);
                                $('#leaveOverlapModal #rejectreasonoverlap').attr('disabled',true);
                                // $('#leaveOverlapModal span.status').addClass("label-danger");
                                break;
                            case 'cancelled':
                                $('#leaveOverlapModal button[name="approve"]').hide();
                                $('#leaveOverlapModal button[name="reject"]').hide();
                                $('#leaveOverlapModal button[name="cancel"]').hide();
                                $('#leaveOverlapModal #rejectreasonoverlap').parent().hide();
                                // $('#leaveOverlapModal span.status').addClass("label-primary");
                                break;
                            default:
                                $('#leaveOverlapModal input[name="client"]').parent().show();
                                // $('#leaveOverlapModal span.status').addClass("label-info");
                                break;
                        }
                        if(data.type=='half') {
                            $('#leaveOverlapModal #end_date_div_edit').hide();
                            $('#leaveOverlapModal #half_day_div_edit').show();
                            $('#leaveOverlapModal #datetimeinputstart_edit').attr('placeholder','Date');
                            $('#leaveOverlapModal select[name="half"]').val(data.half);
                        } else {
                            $('#leaveOverlapModal #end_date_div_edit').show();
                            $('#leaveOverlapModal #half_day_div_edit').hide();
                        }
                        if(data.parent == 1) {
                            $('#leaveOverlapModal span.status').text('Waiting for you action');
                            $('#leaveOverlapModal span.status').addClass("label-warning");
                            $('#leaveOverlapModal .modal-footer').before('<label style="display:block;padding-left:15px" class="manager-status-label status-info-label"> Project Managers Status</label>');
                            if(data.projectManagers.length == 0)
                            {
                                $('#leaveOverlapModal .manager-status-label').after('<li style="display:block;margin:0px 15px 0px 15px;border-radius:2px" class="list-group-item status-info-label"> No Managers Allocated </li>');
                            } else {
                                data.projectManagers.forEach(function (element){
                                    $arg = '<li style="display:block;margin:0px 15px 0px 15px;border-radius:2px" class="status-info-label list-group-item"> ';
                                    $arg= $arg+element.manager.name;
                                    var date = new Date(element.updated_at);
                                    $arg = $arg+'<small style="float:right;padding-top:1%">  on '+date.toDateString()+'</small>';
                                    $arg = $arg+'<span style="float:right" class="label custom-label ';
                                    switch(element.status) {
                                        case 'approved' :
                                            $arg = $arg+'label-success ">';
                                            break;
                                        case 'pending' :
                                            $arg = $arg+'label-info ">';
                                            break;
                                        case 'cancelled' :
                                            $arg = $arg+'label-primary ">';
                                            break;
                                        case 'rejected' :
                                            $arg = $arg+'label-danger ">';
                                            break;
                                        default :
                                            $arg = $arg+'label-default ">';
                                    }
                                    $arg = $arg+element.status.charAt(0).toUpperCase()+element.status.slice(1)+'</span><span style="display:block">';
                                    $projects = "";
                                    if(element.projects) {
                                        element.projects.forEach(function (proj_name){
                                            $projects = $projects+proj_name+", ";
                                        });
                                    }
                                    if($projects.length > 0) {
                                        $projects = $projects.slice(0, -2);
                                        $arg = $arg+$projects;
                                    }
                                    $arg = $arg+'</span></li>';
                                    $('#leaveOverlapModal .manager-status-label').after($arg);
                                });
                            }
                            if(data.ready == 0) {
                                // $('#leaveOverlapModal button[name="approve"]').hide();
                                // $('#leaveOverlapModal button[name="reject"]').hide();
                                // $('#leaveOverlapModal button[name="cancel"]').hide();
                                // $('#leaveOverlapModal #rejectreasonoverlap').hide();
                                // $('#leaveOverlapModal #rejectreasonlabel').hide();
                                // $('#leaveOverlapModal input[name="client"]').parent().hide();
                                $('#leaveOverlapModal span.status').text('Waiting for project managers');
                                $('#leaveOverlapModal span.status').addClass("label-info");
                                $('#leaveOverlapModal span.status').removeClass("label-warning");
                            }
                        } else {
                            $('#leaveOverlapModal span.status').text('Waiting for you action');
                            $('#leaveOverlapModal span.status').addClass("label-warning");
                            $('#leaveOverlapModal button[name="approve"]').show();
                            $('#leaveOverlapModal button[name="reject"]').show();
                            $('#leaveOverlapModal button[name="cancel"]').show();
                            $('#leaveOverlapModal #rejectreasonlabel').show();
                            $('#leaveOverlapModal #rejectreasonoverlap').show();
                            if(data.status != 'pending') {
                                $('#leaveOverlapModal button[name="cancel"]').hide();
                            } else {
                                if(data.editable == 0) {
                                    $('#leaveOverlapModal button[name="approve"]').hide();
                                    $('#leaveOverlapModal button[name="reject"]').hide();
                                    $('#leaveOverlapModal button[name="cancel"]').hide();
                                    $('#leaveOverlapModal #rejectreasonoverlap').hide();
                                    $('#leaveOverlapModal #rejectreasonlabel').hide();
                                    $('#leaveOverlapModal span.status').text('Waiting for reporting manager');
                                    $('#leaveOverlapModal span.status').addClass("label-info");
                                    $('#leaveOverlapModal span.status').removeClass("label-warning");
                                }
                            }
                        }
                        var x = '';
                        for(var i=0;i<Object.keys(data.overlapData).length-2;i++)
                        {
                            x += '<table class="projects-table table table-bordered table-responsive table-striped">';
                            x +='<thead><tr>';
                            x +='<th colspan="'+(data.overlapData.dates.length+1)+'" style="color:#EF3B40">Project : '+data.overlapData[i].name+'</th>';
                            x +='</tr></thead><tr><th>Dates</th>';
                            for(var l=0;l<data.overlapData.dates.length;l++)
                            {
                                if(data.overlapData.code[l]==1) {
                                    x +='<th style="background-color:grey">'+data.overlapData.dates[l]+'</th>';
                                } else {
                                    x +='<th>'+data.overlapData.dates[l]+'</th>';
                                }
                            }
                            x +='</tr>';
                                for(var j=0;j<data.overlapData[i].users.length;j++)
                                {
                                    x+='<tr>';
                                    x +='<td>'+data.overlapData[i].users[j].name+'</td>';
                                    for(var k=0;k<data.overlapData[i].users[j].dates.length;k++)
                                    {
                                        if(data.overlapData.code[k]==1) {
                                            x+='<td style="background-color:grey"></td>';
                                        } else {
                                            if(data.overlapData[i].users[j].dates[k]!=0) {
                                                x +='<td style="background-color:#EF3B40;color:white">'+data.overlapData[i].users[j].dates[k]+'</td>';
                                            } else {
                                            x +='<td></td>';
                                            }
                                        }
                                    }
                                    x+='</tr>';
                                }
                            x+='<tr><td colspan="'+(data.overlapData.dates.length+1)+'"><textarea style="width:100%" class="reason-textarea" data-projectid = "'+data.overlapData[i].id+'" rows = "3" name="reasonText" disabled>'+data.overlapData[i].reason+'</textarea></td></tr>';
                            x+='</table>';
                        }
                        
                        $("#leaveOverlapModal #rejectreasonlabel").before(x);
                        $("#leaveOverlapModal").modal('show');
                    }
                },
                error: function(errorResponse){
                    $('#editLeaveModal').modal('hide');
                    window.location.reload();
                }
            });
        });
        $( ".searchuser" ).autocomplete({
            source: {{$jsonuser}}
        });
        $('#datetimepickerStart_search').datetimepicker({
            format: 'DD-MM-YYYY'
        });

        $('#datetimepickerEnd_search').datetimepicker({
            format: 'DD-MM-YYYY',
            @if( isset( $_GET['start_date'] ))
                minDate: new Date("{{date_to_yyyymmdd($_GET['start_date'])}}"),
            @endif
        });
        @if( isset( $_GET['end_date'] ))
            $("#ed").val("{{$_GET['end_date']}}");
        @endif
        $("#datetimepickerStart_search").on("dp.change", function (e) {
            $('#datetimepickerEnd_search').data("DateTimePicker").minDate(e.date);
        });

    });
</script>