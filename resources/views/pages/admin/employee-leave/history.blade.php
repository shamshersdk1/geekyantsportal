@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Leave History</h1>
                <ol class="breadcrumb">
                    <li class="active">Leave History</li>
                </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
        </div>
    </div>
    <!-- ============================= admin leave Board ================================ -->
    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                      @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif
        </div>
        <div class="col-md-12">
            <div class="panel panel-default" style="margin-bottom:0">
                <div class="table-responsive">
                    <table class="table table-striped no-margin">
                        <thead>
                            <tr>
                                <th>Duration</th>
                                <th>Days</th>
                                <th>Type</th>
                                <th>Approved By</th>
                                <th>Status</th>
                                <th class="text-right">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($leaves) == 0)
                                <tr>
                                    <td colspan="6">
                                        No record found
                                    </td>
                                </tr>
                            @endif
                            @if(isset($leaves))
                                @foreach($leaves as $leave)
                                    <tr>
                                        <td>{{date("d M Y", strtotime($leave->start_date))}} <b>TO</b> {{date("d M Y", strtotime($leave->end_date))}}</td>
                                        @if($leave->days==0.5)
                                            <td>{{ucfirst($leave->half)}} half</td>
                                        @else
                                            <td>{{floor($leave->days)}}</td>
                                        @endif
                                        <td>
                                            @if($leave->type=='half')
                                                    {{ ucfirst($leave->type)}} Day Leave

                                            @else
                                                    {{ ucfirst($leave->type)}} Leave
                                            @endif
                                        </td>
                                        <td>{{$leave->approver['name']}}</td>
                                        <td>
                                            @if($leave->status == "approved")
                                                <span class="label label-success custom-label">Approved</span>
                                            @elseif($leave->status == "pending")
                                                <span class="label label-info custom-label">Pending</span>
                                            @elseif($leave->status == "rejected")
                                                <span class="label label-danger custom-label">Rejected</span>
                                            @elseif($leave->status == "cancelled")
                                                <span class="label label-primary custom-label">Cancelled</span>
                                            @else
                                                <span class="label label-info custom-label">No Status</span>
                                            @endif
                                        </td>
                                        <td class="text-right">
                                            <button class="btn btn-info btn-sm" type="button"
                                                data-toggle="modal" data-target="#viewLeaveModal"
                                                data-id="{{$leave->id}}" data-title="Algorithms"><i class="fa fa-eye"></i> View</button>
                                            @if(($leave->status == "pending" || $leave->status == "approved")&&($leave->start_date > date('Y-m-d')))
                                                <form action="/admin/employee-leave/status/{{$leave->id}}" style="display:inline" class="cancelForm" method="post">
                                                    <button class="btn btn-danger btn-sm" name="cancel" value="cancel">Cancel</button>
                                                </form>
                                            @else
                                                <button class="btn btn-danger btn-sm" name="cancel" value="cancel" disabled>Cancel</button>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
            {{$leaves->links()}}
        </div>
    </div>
</div>
@include('pages/admin/employee-leave/view-leave-modal')
@include('pages/admin/employee-leave/javascript')
@endsection