
<div class="modal fade" id="viewLeaveModal" tabindex="-1" role="dialog" aria-labelledby="viewLeaveModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="viewLeaveModal">Leave Details</h4>
            </div>
            <div class="modal-body loading" style="min-height: 543px;">
                <div class="col-md-2 col-md-offset-5">
                    <i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
                </div>
            </div>
            <div class="modal-body content">
                <div class="row">
                    <div class="col-md-12 ma-bot-10">
                        <span style="margin-bottom:10px" class="status label custom-label pull-right"></span><label class="pull-right" for="">Status:&nbsp</label>
                    </div>
                    <div class="col-md-6 ma-bot-10">
                        <div class="form-group ma-bot-10">
                            <label class="control-label" for="type">Leave Type</label>
                                <select class="form-control" style="width:100%" id="leave_type" name="type" disabled>
                                    <option value="sick" >Sick Leave</option>
                                    <option value="unpaid" >Unpaid Leave</option>
                                    <option value="paid" >Paid Leave</option>
                                    <option value="half" >Half-day Leave</option>
                                </select>
                        </div>
                    </div>
                    <!-- ======================== Date Picker ======================= -->
                    <div class="col-md-12 no-padding ma-bot-10">
                            <div class="form-group">
                                <div class="col-md-6">
                                    <label for="start_date">From</label>
                                    <div class='input-group date' id='datetimepickerStart'>
                                        <input type='text' class="form-control" autocomplete="off" name="start_date" placeholder="Start Date" disabled/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-6" id="end_date_div">
                                    <label for="end_date">To</label>
                                    <div class='input-group date' id='datetimepickerEnd'>
                                        <input type='text' class="form-control" name="end_date" placeholder="End Date" autocomplete="off"  disabled/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-6" id="half_day_div">
                                    <div class="form-group">
                                        <label for="half">Select Half</label>
                                        <select class="form-control" name="half" disabled>
                                            <option value="first">First Half</option>
                                            <option value="second">Second Half</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <!-- ====================== reasones ================ -->
                    <div class="col-md-12 ma-top-10">
                        <div class="form-group">
                            <label for="">Reason</label>
                            <textarea class="form-control" rows="5" placeholder="Please enter reason for applying leave" name="reason" disabled></textarea>
                        </div>
                    </div>
                    <div class="col-md-12 ma-top-10">
                        <div class="form-group">
                            <label for="">Reason for rejection</label>
                            <textarea class="form-control" rows="5" placeholder="Please enter reason for rejecting leave" name="rejectreason" id="rejectreason" disabled></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>&nbsp;
            </div>
        </div>
    </div>
</div>
