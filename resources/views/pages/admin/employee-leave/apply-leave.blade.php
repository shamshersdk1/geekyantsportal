@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="admin-page-title">Leave Request For <span>{{$user->name}}</span></h1>
                <ol class="breadcrumb">
                    <li class="active">Apply Leave</li>
                </ol>
            </div>
        </div>
    </div>
    <!-- ================= panel ================== -->
    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                      @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif
        </div>
        <!-- =================== apply form ===================== -->
        <div class="col-md-6">
            <form action="/admin/employee-leave/requestLeave" method="post">
                <div class="panel panel-default select-leave-type">
                    <div class="row">
                        <div class="col-md-12 ma-bot-10">
                            <div class="form-group ma-bot-10">
                                <label for="">Select Leave Type</label>
                                <select class="form-control" name="type">
                                    <option>Select Leave type</option>
                                    @if (old('type') == 'sick')
                                          <option value="sick" selected>Sick Leave</option>
                                    @else
                                          <option value="sick">Sick Leave</option>
                                    @endif
                                    @if (old('type') == 'paid')
                                          <option value="paid" selected>Paid Leave</option>
                                    @else
                                          <option value="paid">Paid Leave</option>
                                    @endif
                                    @if (old('type') == 'unpaid')
                                          <option value="unpaid" selected>Unpaid Leave</option>
                                    @else
                                          <option value="unpaid">Unpaid Leave</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                        <!-- ======================== Date Picker ======================= -->
                        <div class="col-md-12 no-padding ma-bot-10">
                            <div class="form-group">
                                <div class="col-md-6">
                                    <div class='input-group date' id='datetimepickerStart'>
                                        <input type='text' id="datetimeinputstart" class="form-control" value="{{old('start_date')}}" name="start_date" placeholder="Start Date" required autocomplete="off" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class='input-group date' id='datetimepickerEnd'>
                                        <input type='text' class="form-control" name="end_date" placeholder="End Date" value="{{old('end_date')}}" required autocomplete="off" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- ====================== reasones ================ -->
                        <div class="col-md-12 ma-top-10">
                            <div class="form-group">
                                <label for="">Notes</label>
                                <textarea class="form-control" rows="5" placeholder="Please enter reason for applying leave" name="reason">{{trim(old('reason'))}}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <button class="btn btn-success crud-btn" type="submit">Apply Leave</button>

            </form>
        </div>
        <div class="col-md-3 col-sm-6">

            <div class="leave-stat">
                <div class="num">
                    <span>{{$consumedSickLeaves}}</span>
                    <span class="devider">/</span>
                    <span>5</span>
                </div>
                <div class="text">
                    <div class="stat-heading"> Sick Leave</div>
                    <div class="stat-info">
                        <div><b>{{$consumedSickLeaves}}</b> Consumed</div>
                        <div><b>5</b> Available</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="leave-stat">
                <div class="num">
                    <span>{{$consumedPaidLeaves}}</span>
                    <span class="devider">/</span>
                    <span>6</span>
                </div>
                <div class="text">
                    <div class="stat-heading"> Paid Leave</div>
                    <div class="stat-info">
                        <div><b>{{$consumedPaidLeaves}}</b> Consumed</div>
                        <div><b>6</b> Available</div>
                    </div>
                </div>
            </div>
        </div>
        <!-- =================== calender ======================= -->
        <!--<div class="col-md-6">
            <div style="overflow:hidden;">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-8">
                            <div id="datetimepicker12"></div>
                        </div>
                    </div>
                </div>
            </div>
            <hr style="width: 100%; border-color: #ddd;">

            <div class="event-list">
                <div class="col-md-12 leave">
                    <span><i class="fa fa-circle fa-fw leave-1day" aria-hidden="true"></i></span>
                    <span class="date"><b>30th june</b>&#160;</span>
                    <span class="user-name"><span>Surbhi</span>, <span>Surbhi</span>, <span>Surbhi</span> &#160;</span>
                    <span class="number-leave"> 1 day leave</span>
                </div>
                <div class="col-md-12 leave">
                    <span><i class="fa fa-circle fa-fw leave-2day" aria-hidden="true"></i></span>
                    <span class="date"><b>30th june</b>&#160;</span>
                    <span class="user-name">Surbhi &#160;</span>
                    <span class="number-leave"> 1 day leave</span>
                </div>
                <div class="col-md-12 leave">
                    <span><i class="fa fa-circle fa-fw leave-3day" aria-hidden="true"></i></span>
                    <span class="date"><b>30th june</b>&#160;</span>
                    <span class="user-name">Surbhi &#160;</span>
                    <span class="number-leave"> 1 day leave</span>
                </div>
            </div>
        </div>-->
    </div>
    <!-- <section class="leave-apply-form">
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="row">
                        <form action="/admin/employee-leave/requestLeave" method="post">
                            <div class="col-md-12">
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            <label for="">Select Leave Type</label>
                                            <select class="form-control" name="type">
                                                <option>Select Leave type</option>
                                                <option value="sick">Sick Leave</option>
                                                <option value="unpaid">Unpaid Leave</option>
                                                <option value="paid">Paid Leave</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="msgof-leave">
                                            Print msg as per leave request
                                        </div>
                                    </div> -->
                            <!-- ================ from to date =============== -->
                                <!-- <div class="col-md-7">
                                    <div class="row">
                                        <div class="col-md-6 no-padding">
                                            <div class='input-group date' id='datetimepicker1'>
                                                <input type='text' class="form-control" id="txtStartDate" name="start_date" />
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-md-6 p-l-10">
                                            <div class='input-group date' id='datetimepicker1'>
                                                <input type='text' class="form-control" id="txtEndDate" name="end_date" />
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                            <!-- =========================== notes =============================== -->
                                <!-- <div class="col-md-7" style="margin-bottom: 15px;">
                                    <div class="row">
                                        <div class="col-md-12 no-padding">
                                            <label for="">Notes</label>
                                            <textarea class="form-control" rows="5" placeholder="Please enter reason for applying leave" name="reason"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-7 no-padding" >
                                    <button class="btn btn-success crud-btn" type="submit">Apply Leave</button>
                                </div> -->
                            <!-- </div>
                        </form>
                    </div>
                </div>
            </div> -->
            <!-- ================= second side =============== -->
            <!-- <div class="col-md-6">

            </div>
        </div>
    </section> -->
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#datetimepickerStart').datetimepicker({
            //Set max date to upcoming march
            maxDate: new Date().getMonth() <= 2 ? new Date(new Date().getFullYear()+"-03-31") : new Date(new Date().getFullYear()+1+"-03-31"),
            minDate: new Date().getMonth() <= 3 ? new Date(new Date().getFullYear()-1+"-04-01") : new Date(new Date().getFullYear()+"-04-01"), //Set min date to April 1
            format: 'YYYY-MM-DD'
        });

        $('#datetimepickerEnd').datetimepicker({
            //Set max date to upcoming march
            maxDate: new Date().getMonth() <= 2 ? new Date(new Date().getFullYear()+"-03-31") : new Date(new Date().getFullYear()+1+"-03-31"),
            format: 'YYYY-MM-DD'
        });
        if($("#datetimeinputstart").val()!="")
        {
            $('#datetimepickerEnd').data("DateTimePicker").minDate($("#datetimeinputstart").val());
        }
        $("#datetimepickerStart").on("dp.change", function (e) {
            $('#datetimepickerEnd').data("DateTimePicker").minDate(e.date);
        });
    })
</script>
@endsection