<script>
$(function(){
    $('.cancelForm').submit(function(){
        return confirm('Are you sure?');
    });
    $("#viewLeaveModal").on("hidden.bs.modal", function(){
        $(this).find("input,textarea,select")
            .val('')
            .end()
    });
    $('#viewLeaveModal').on("show.bs.modal", function (e) {
        let id = $(e.relatedTarget).data('id')
        $.ajax({
            type: 'GET',
            url: '/admin/employee-leave/'+id,
            beforeSend: function() {
                $("div.loading").show();
                $("div.content").hide();
            },
            complete: function() {
                $("div.loading").hide();
                $("div.content").show();
            },
            success: function(data) {
                console.log(data);
                $('#viewLeaveModal select[name="type"]').val(data.type);
                $('#viewLeaveModal input[name="start_date"]').val(data.start_date);
                $('#viewLeaveModal input[name="end_date"]').val(data.end_date);
                $('#viewLeaveModal textarea[name="reason"]').val(data.reason);
                $('#viewLeaveModal span.status').text(data.status.charAt(0).toUpperCase()+data.status.slice(1));
                $('#viewLeaveModal #rejectreason').parent().show();
                $('#viewLeaveModal span.status').removeClass("label-success");
                $('#viewLeaveModal span.status').removeClass("label-danger");
                $('#viewLeaveModal span.status').removeClass("label-primary");
                $('#viewLeaveModal span.status').removeClass("label-info");

                switch(data.status){
                    case 'approved':
                        $('#viewLeaveModal #rejectreason').parent().hide();
                        $('#viewLeaveModal span.status').addClass("label-success");
                        break;
                    case 'rejected':
                        $('#viewLeaveModal #rejectreason').val(data.cancellation_reason);
                        $('#viewLeaveModal span.status').addClass("label-danger");
                        break;
                    case 'cancelled':
                        $('#viewLeaveModal #rejectreason').parent().hide();
                        $('#viewLeaveModal span.status').addClass("label-primary");
                        break;
                    default:
                        $('#viewLeaveModal #rejectreason').parent().hide();
                        $('#viewLeaveModal span.status').addClass("label-info");
                        break;
                }
                if(data.type=='half')
                {
                    $('#end_date_div').hide();
                    $('#half_day_div').show();
                    $('#viewLeaveModal select[name="half"]').val(data.half);
                }
                else{
                    $('#end_date_div').show();
                    $('#half_day_div').hide();
                    $('#viewLeaveModal select[name="half"]').val(data.half);
                }
            }
        });
    });
});
</script>