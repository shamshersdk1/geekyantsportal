@extends('layouts.admin-dashboard')
@section('main-content')
<section>
   <div class="container-fluid">
      <div class="breadcrumb-wrap">
         <div class="row">
            <div class="col-sm-6">
               <h1>Tech Events</h1>
               <ol class="breadcrumb">
                  <li><a href="/admin">Admin</a></li>
                  <li><a href="/admin/tech-events">Tech Events</a></li>
                  <li class="active">{{$techEventObj->id}}</li>
               </ol>
            </div>
         </div>
      </div>
      @if(!empty($errors->all()))
      <div class="alert alert-danger">
         @foreach ($errors->all() as $error)
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
         <span>{{ $error }}</span><br/>
         @endforeach
      </div>
      @endif
      @if (session('message'))
      <div class="alert alert-success">
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
         <span>{{ session('message') }}</span><br/>
      </div>
      @endif
      <div class="row cms-dev">
         <div class="col-md-12">
            <h4 >CMS Developer Technology Detail</h4>
            <div class="panel panel-default">
               <div class="panel-body row">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <div class=" col-md-6">
                     <label>Title : </label>
                     <span>{{$techEventObj->title}}</span>
                  </div>
                  <div class=" col-md-6">
                     <label>Desctiption : </label>
                     <span>{{$techEventObj->description}}</span>
                  </div>
                  <div class=" col-md-6">
                     <label>Status : </label>
                     <span>{{ $techEventObj->status ? 'Active' : 'Inactive' }}</span>
                  </div>
                  <div class=" col-md-6">
                    <label>Category : </label>
                    <span>{{ ucfirst($techEventObj->category)  }}</span>
                 </div>
                  @if ( $techEventObj->image )
                  <div class="col-md-12">
                     <label>Display : </label>
                     <span><img src="{{ $techEventObj->image->path }}" style="height: 200px; width:200px;"></span>
                  </div>
                  @endif
                  @if ( $techEventObj->logoImage )
                  <div class="col-md-12">
                    <label>Logo : </label>
                    <span><img src="{{ $techEventObj->logoImage->path }}" style="height: 50px; width:50px;"></span>
                 </div>
                 @endif
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
@endsection