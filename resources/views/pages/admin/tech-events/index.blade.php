@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Tech Events</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
		  			<li class="active">Tech Events Dashboard</li>
        		</ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
				<a href="/admin/tech-events/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Add Tech Event</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
	    	@if(!empty($errors->all()))
	            <div class="alert alert-danger">
	                @foreach ($errors->all() as $error)
	                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                    <span>{{ $error }}</span><br/>
	                  @endforeach
	            </div>
	        @endif
	        @if (session('message'))
	            <div class="alert alert-success">
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ session('message') }}</span><br/>
	            </div>
			@endif
		</div>
		<div class="user-list-view">
            <div class="panel panel-default">
            	<table class="table table-striped">
					<tr>
						<th width="5%" class="sorting_asc">#</th>
						<th width="30%">Dtails</th>
						<th width="15%">Location</th>
						<th width="15%">Members</th>
						<th width="11%">End Date</th>
						<th width="10%">Status</th>
						<th class="text-right sorting_asc" width="18%">Actions</th>
					</tr>
					@if(count($techEvents) > 0)
						@foreach($techEvents as $index => $techEvent)
							<tr>
								<td class="td-text"><a>{{$index+1}}</a></td>
								<td class="td-text">
								 <h5  style="margin:0">{{$techEvent->title}}</h5>
								 <small class="text-primary" style="color: #337ab7;">{{$techEvent->description}}</small>
								</td>
								<td class="td-text">
								 <h5 style="margin:0">{{date_in_view($techEvent->start_date)}}</h5>
								 <small class="text-primary" style="color: #337ab7;">{{$techEvent->location}}</small>
								</td>
								<td><span>Sanket Sahu</span>, <span>Saurabh Sahu</span></td>
								<td class="td-text">{{date_in_view($techEvent->end_date)}}</td>
								<td>
									<div class="col-md-4">
										<div class="onoffswitch">
											<input type="checkbox" value="{{$techEvent->id}}" name="onoffswitch[]" onchange="toggle(this)"
													class="onoffswitch-checkbox" id="{{$techEvent->id}}" 
											<?php echo (!empty($techEvent->status)&&$techEvent->status==1) ? 'checked':''; ?>>
											<label class="onoffswitch-label" for= {{$techEvent->id}} >
												<span class="onoffswitch-inner"></span>
												<span class="onoffswitch-switch"></span>
											</label>
										</div>
									</div>
								</td>
							
								<td class="text-right">
									<a href="/admin/tech-events/{{$techEvent->id}}" class="btn btn-success crude-btn btn-sm">View</a>
									<a href="/admin/tech-events/{{$techEvent->id}}/edit" class="btn btn-info btn-sm crude-btn">Edit</a>
									<form action="/admin/tech-events/{{$techEvent->id}}" method="post" style="display:inline-block;">
										<input name="_method" type="hidden" value="DELETE">
										<button class="btn btn-danger btn-sm crude-btn" type="submit">Delete</button>
									</form>
								</td>
							</tr>
						@endforeach
	            	@else
	            		<tr>
	            			<td colspan="3" class="text-center">No results found.</td>
	            		</tr>
	            	@endif
            	</table>
				<div class="col-md-12">
                    <div class="pageination pull-right">
                        <nav aria-label="Page navigation">
                              <ul class="pagination">
                                {{ $techEvents->render() }}
                              </ul>
                        </nav>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>
</div>
<script>
    $(function(){
        toggle = function (item) {
			//window.location.href='users-billable/'+item.value;
			$.ajax({
				type:'POST',
				url:'/api/v1/tech-events/status',
				data:{id:item.value},
				success:function(data){
					console.log(data);
					}
			});
		}
    });
</script>
@endsection
