@extends('layouts.admin-dashboard')
@section('main-content')

<section class="new-project-section">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">Tech Events</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
			  			<li><a href="{{ url('admin/tech-events') }}">Tech Events</a></li>
			  			<li class="active">{{ $techEvent->id }}</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>

	    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif

		<form class="form-horizontal" method="post" action="/admin/tech-events/{{$techEvent->id}}" enctype="multipart/form-data">
			<input name="_method" type="hidden" value="PUT" />
			<div class="panel panel-default">
				<div class="panel-body">
				    <div class="row">
				    	<div class="col-md-12">
							<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Title</label>
						    	<div class="col-sm-6">
									  <input type="text" class="form-control" id="title" name="title" value="{{ $techEvent->title }}">
						    	</div>
							  </div>
							  <div class="form-group">
								<label class="col-sm-2 control-label">Status:</label>
								@if( $techEvent->status )
								<div class="col-sm-4">
									<select class="form-control" name="status">
										<option value="1" selected>Active</option>
										<option value="0">Inactive</option>
									</select>
								</div>
								@else
								<div class="col-sm-4">
									<select class="form-control" name="status">
										<option value="1">Active</option>
										<option value="0" selected>Inactive</option>
									</select>
								</div>
								@endif
								@if( $techEvent->category == 'global' )
								<div class="col-sm-4">
									<select class="form-control" name="category">
										<option value="global" selected>Global</option>
										<option value="upcoming">Upcoming</option>
										<option value="past">Past</option>
									</select>
								</div>
								@elseif ( $techEvent->category == 'upcoming' )
								<div class="col-sm-4">
									<select class="form-control" name="category">
										<option value="global">Global</option>
										<option value="upcoming" selected>Upcoming</option>
										<option value="past">Past</option>
									</select>
								</div>
								@else
								<div class="col-sm-4">
									<select class="form-control" name="category">
										<option value="global">Global</option>
										<option value="upcoming">Upcoming</option>
										<option value="past" selected>Past</option>
									</select>
								</div>
								@endif
							</div>
						  	<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Location</label>
						    	<div class="col-sm-6">
						      		<input type="text" class="form-control" id="location" name="location" value="{{ $techEvent->location }}">
						    	</div>
						  	</div>
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">Upload Display Image :</label>
								<div class="col-sm-4">    
									<input type="file" id="display_file" class="form-control" name="display_file" multiple="false">
								</div>
							</div>
							@if ( !empty($techEvent->image) )
							<div class="form-group">
								<div class="col-sm-2 col-sm-offset-2">
									<img src="{{ $techEvent->image->path }}" style="height: 100px; width:100px;">
								</div>
							</div>
							@endif
							<div class="form-group">
								<label class="col-sm-2 control-label">Description :</label>
								<div class="col-sm-4">
									<textarea class="form-control" id="description" name="description" rows="4" placeholder="Enter details">{{$techEvent->description}}</textarea>
								</div>
							</div>
							<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">URL</label>
						    	<div class="col-sm-6">
						      		<input type="text" class="form-control" id="url" name="url" value="{{ $techEvent->url }}">
						    	</div>
						  	</div>
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">Upload Logo Image :</label>
								<div class="col-sm-4">    
									<input type="file" id="logo_file" class="form-control" name="logo_file" multiple="false">
								</div>
							</div>
							@if ( !empty($techEvent->logoImage) )
							<div class="form-group">
								<div class="col-sm-2 col-sm-offset-2">
									<img src="{{ $techEvent->logoImage->path }}" style="height: 100px; width:100px;">
								</div>
							</div>
							@endif
							<div class="form-group">
								<div class="col-sm-3 col-sm-offset-2">
									<div class='input-group date' id='datetimepicker1'>
									<input type='text' name="start_date" placeholder = "Start Date" class="form-control" value="{{date_to_ddmmyyyy($techEvent->start_date)}}"/>
										<span class="input-group-addon">
											<span><i class="fa fa-calendar" aria-hidden="true"></i></span>
										</span>
									</div>
								</div>
								<div class="col-sm-3">
									<div class='input-group date' id='datetimepicker2'>
									<input type='text' name="end_date" placeholder = "End Date" class="form-control" value="{{date_to_ddmmyyyy($techEvent->end_date)}}"/>
										<span class="input-group-addon">
											<span><i class="fa fa-calendar" aria-hidden="true"></i></span>
										</span>
									</div>
								</div>
							</div>

					  	</div>
					</div>
				</div>
			</div>
  	  		<div class="text-center">
		  		<button type="submit" class="btn btn-success">Update Tech Event</button>
		  	</div>
		</form>
	</div>
</section>
<script type="text/javascript">
	$(function () {
		$('#datetimepicker1').datetimepicker({
			format: 'DD-MM-YYYY'
		});
		$('#datetimepicker2').datetimepicker({
			format: 'DD-MM-YYYY'
		});
	});
</script>
@endsection
