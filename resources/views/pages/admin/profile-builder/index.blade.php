@extends('layouts.admin-dashboard')
@section('page_heading','User Profile Builder')
@section('main-content')
<div class="container-fluid" ng-app="myApp" >
  <div class="breadcrumb-wrap">
    <div class="row">
      <div class="col-sm-8">
        <h1 class="admin-page-title">Profile Builder</h1>
        <ol class="breadcrumb">
          <li><a href="/admin">Admin</a></li>
          <li class="active">Profile Builder </li>
        </ol>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6">
      <h4 class="no-m-b m-t-5">{{$user->name}}</h4>
    </div>
    <div class="col-sm-6">
      <div style="display:flex; justify-content:flex-end">
        <div class="form-group " style="display:table;align-items:center; margin-right:15px;">
          <label style="padding-right:10px;display: table-cell;vertical-align: middle;">Active: </label>
          <div class="onoffswitch">
            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="active_user">
            <label class="onoffswitch-label" for="active_user">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
            </label>
          </div>
        </div>
        <div class="form-group " style="display:table;align-items:center">
          <label style="padding-right:10px;display: table-cell;vertical-align: middle;">Profile: </label>
          <div class="onoffswitch">
            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="active_profile" checked>
            <label class="onoffswitch-label" for="active_profile">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
            </label>
          </div>
        </div>
      </div>
    </div>
  </div>


  <div class="row row-sm">
    <div class="col-sm-4">
      <profile-info userId="{{$user->id}}"></profile-info>
      <div>
        <socialinfo userid="{{$user->id}}"></socialinfo>
      </div>

      <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">Achivements
                <input type="checkbox" class="pull-right"
               >
              </h4>
            </div>
            <div class="panel-body">
              <form id="form-add-achivements" class="form-add-achivements">
                <div class="input-group">
                  <input type="text" name="achivements" value="" id="new-achivements-item"  class="form-control" />
                  <span class="input-group-btn">
                  <button type="submit" id="add-achivements-item" class="btn btn-success add-achivements-item" style="padding:9px 12px"><i class="fa fa-plus"></i></button>
                  </span>
                </div>
              </form>
            </div>
            <!-- <form id="form-achivements-list"> -->
            <ul id="achivements-list" class="achivements-list list-group no-margin">
            </ul>
            <!-- </form> -->
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">Interest
                <input type="checkbox" class="pull-right"

              >
              </h4>
            </div>
            <div class="panel-body">
              <form id="form-add-interest" class="form-add-interest">
                <div class="input-group">
                  <input type="text" name="interest" value="" id="new-interest-item"  class="form-control" />
                  <span class="input-group-btn">
                  <button type="submit" id="add-interest-item" class="btn btn-success add-interest-item" style="padding:9px 12px"><i class="fa fa-plus"></i></button>
                  </span>
                </div>
              </form>
            </div>
            <!-- <form id="form-interest-list"> -->
            <ul id="interest-list" class="interest-list list-group no-margin">
            </ul>
            <!-- </form> -->
          </div>


        <div>
          <user-education userid="30"></user-education>
        </div>

    </div>
    <div class="col-sm-8">
      <div class="panel panel-default ">
        <div class="panel-heading">
          <h4 class="panel-title">About
            <input type="checkbox" class="pull-right"

            >
          </h4>
        </div>
        <textarea class="form-control" rows="4"></textarea>
      </div>
      <div class="panel panel-default">
        <div class="panel-heading">
          <h4 class="panel-title">Most Amazing Thing
            <input type="checkbox" class="pull-right"

              >
          </h4>
        </div>
        <textarea class="form-control" rows="3"></textarea>
      </div>
      <div class="row row-sm">
        <div class="col-sm-6">
          <techstack userid="{{$user->id}}"></techstack>
          
          <div class="panel panel-default">
            <project-cms user-id={{$user->id}}></project-cms>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">Tech Talks
              </h4>
            </div>
            <div class="panel-bod">
              <ul class="project-list list-group no-margin">
                <li class='list-group-item'>
                  <div class="m-b-10">
                    <input type="checkbox" checked="" class="pull-right">
                    <strong>Techtalk title</strong><br />
                    <div><small>24th Jan 2019</small></div>
                    <div class="m-t-5">Shrot description of tech talk</div>
                  </div>
                </li>
              </ul>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">Meetups
              </h4>
            </div>
            <ul class="project-list list-group no-margin">
              <li class='list-group-item'>
                <div class="m-b-10">
                  <input type="checkbox" checked="" class="pull-right">
                  <strong>React Meetup</strong><br />
                  <div><small>24th Jan 2019</small></div>
                  <div class="m-t-5">Shrot description of meetup</div>
                </div>
              </li>
            </ul>
          </div>
        </div>
            <div class="col-sm-6">
              <workshop userid="{{$user->id}}"></workshop>
              <miniproject userid="{{$user->id}}"></miniproject>
              </div>
        <div class="col-sm-6">
          <!-- Training -->
          <!-- Workshop -->
          <rnd userId="{{$user->id}}"></rnd>
          </div>

      </div>
    </div>
  </div>
  <!-- Education -->



    <div class="col-sm-12 pull-right">
      <a href="" class="btn btn-default  pull-right ">Cancel</a>
      <a href="" class="btn btn-primary  pull-right m-r-15">Save</a>
    </div>


</div>

<script type="text/javascript">

  $(document).ready(function(){
     $('.multi_select').select2();

    // Interest

    function addinterestItem() {
      var interestItem = $("#new-interest-item").val();
      $("#interest-list").append("<li class='list-group-item'>" + interestItem +
                             " <a class='interest-item-delete pull-right text-danger'>"+
                             "<i class='fa fa-trash'></i></a></li>");

     $("#new-interest-item").val("");
    }

    function deleteinterestItem(e, item) {
      e.preventDefault();
      $(item).parent().fadeOut('slow', function() {
        $(item).parent().remove();
      });
    }

    $(function() {
      $("#add-interest-item").on('click', function(e){
        e.preventDefault();
        addinterestItem()
      });

      $("#interest-list").on('click', '.interest-item-delete', function(e){
        var item = this;
        deleteinterestItem(e, item)
      })
    });

    // Achivements

    function addachivementsItem() {
      var achivementsItem = $("#new-achivements-item").val();
      $("#achivements-list").append("<li class='list-group-item'>" + achivementsItem +
                             " <a class='achivements-item-delete pull-right text-danger'>"+
                             "<i class='fa fa-trash'></i></a></li>");

     $("#new-achivements-item").val("");
    }

    function deleteachivementsItem(e, item) {
      e.preventDefault();
      $(item).parent().fadeOut('slow', function() {
        $(item).parent().remove();
      });
    }

    $(function() {
      $("#add-achivements-item").on('click', function(e){
        e.preventDefault();
        addachivementsItem()
      });

      $("#achivements-list").on('click', '.achivements-item-delete', function(e){
        var item = this;
        deleteinterestItem(e, item)
      })
    });

    // R&D

    function addresearchItem() {
      var researchItem = $("#new-research-item").val();
      $("#research-list").append("<li class='list-group-item'>" + researchItem +
                             " <a class='research-item-delete pull-right text-danger'>"+
                             "<i class='fa fa-trash'></i></a></li>");

     $("#new-research-item").val("");
    }

    function deleteresearchItem(e, item) {
      e.preventDefault();
      $(item).parent().fadeOut('slow', function() {
        $(item).parent().remove();
      });
    }

    $(function() {
      $("#add-research-item").on('click', function(e){
        e.preventDefault();
        addresearchItem()
      });

      $("#research-list").on('click', '.research-item-delete', function(e){
        var item = this;
        deleteinterestItem(e, item)
      })
    });




  });
</script>
@endsection
