@extends('layouts.admin-dashboard')
@section('page_heading','User Profile Builder')
@section('main-content')
<div class="container-fluid">
  <div class="breadcrumb-wrap">
    <div class="row">
      <div class="col-sm-8">
        <h1 class="admin-page-title">Profile Builder</h1>
        <ol class="breadcrumb">
          <li><a href="/admin">Admin</a></li>
          <li class="active">Profile Builder </li>
        </ol>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6">
      <h4 class="no-m-b m-t-5">Megha Kumari</h4>
    </div>
    <div class="col-sm-6">
      <div style="display:flex; justify-content:flex-end">
        <div class="form-group " style="display:table;align-items:center; margin-right:15px;">
          <label style="padding-right:10px;display: table-cell;vertical-align: middle;">Active: </label>
          <div class="onoffswitch">
            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="active_user" checked>
            <label class="onoffswitch-label" for="active_user">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
            </label>
          </div>
        </div>
        <div class="form-group " style="display:table;align-items:center">
          <label style="padding-right:10px;display: table-cell;vertical-align: middle;">Profile: </label>
          <div class="onoffswitch">
            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="active_profile" checked>
            <label class="onoffswitch-label" for="active_profile">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
            </label>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <div class="row row-sm">
    <div class="col-sm-4">
      <div class="panel panel-default ">
        <div class="panel-heading">
          <h4 class="panel-title">Basic info
          <input type="checkbox" checked="" class="pull-right"></h4>
        </div>
        <div class="panel-body">
          <div class="m-b">
            <label >Photo:</label>
            <div class="form-control" style="border-radius:0">
              <div class="custom-file">
                <input type="file" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01" style="border:none;width:100%">
              </div>
            </div>
          </div>
          <div class="m-b">
            <label>Title:</label>
            <input type="text" name="user_designation" class="form-control" id="" value="Software Engineer" />
          </div>
          <div class="m-b">
            <label>Office Location:</label>
            <select class="form-control">
              <option selected="selected" value="Bangalore">GeekyAnts - Bangalore, India</option>
              <option value="london">GeekyAnts - London, UK</option>
            </select>
          </div>
          <div class="m-b">
            <label>Page Title:</label>
            <input type="text" name="page_title" class="form-control" id="" value="" />
          </div>
          <div >
            <label>Profile Layout:</label>
            <select class="form-control" id="profile_type" name="profile_type">
              <option value="founder">Founder</option>
              <option value="tl">Tech Leads & Architect</option>
              <option value="dev" selected="selected">Developers & Designers</option>
              <option value="other">Other Team</option>
            </select>
          </div>
        </div>
      </div>

      <div class="panel panel-default ">
        <div class="panel-heading">
          <h4 class="panel-title">Social Info
          <input type="checkbox" checked="" class="pull-right"></h4>
        </div>
        <div class="panel-body">
          <div class="m-b">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
              <input type="text" name="" value="" class="form-control" />
              <span class="input-group-addon"><input type="checkbox" name="" /></span>
            </div>
          </div>
          <div class="m-b">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-phone"></i></span>
              <input type="text" name="" value="" class="form-control" />
              <span class="input-group-addon"><input type="checkbox" name="" /></span>
            </div>
          </div>
          <div class="m-b">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-skype"></i></span>
              <input type="text" name="" value="" class="form-control" />
              <span class="input-group-addon"><input type="checkbox" name="" /></span>
            </div>
          </div>
          <div class="m-b">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-github"></i></span>
              <input type="text" name="" value="" class="form-control" />
              <span class="input-group-addon"><input type="checkbox" name="" /></span>
            </div>
          </div>
          <div class="m-b">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa fa-stack-overflow"></i></span>
              <input type="text" name="" value="" class="form-control" />
              <span class="input-group-addon"><input type="checkbox" name="" /></span>
            </div>
          </div>
          <div class="m-b">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-linkedin"></i></span>
              <input type="text" name="" value="" class="form-control" />
              <span class="input-group-addon"><input type="checkbox" name="" /></span>
            </div>
          </div>
          <div class="m-b">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-twitter"></i></span>
              <input type="text" name="" value="" class="form-control" />
              <span class="input-group-addon"><input type="checkbox" name="" /></span>
            </div>
          </div>
          <div>
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-facebook"></i></span>
              <input type="text" name="" value="" class="form-control" />
              <span class="input-group-addon"><input type="checkbox" name="" /></span>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-sm-8">
      <div class="panel panel-default ">
        <div class="panel-heading">
          <h4 class="panel-title">About
          <input type="checkbox" checked="" class="pull-right"></h4>
        </div>
        <textarea class="form-control" rows="4"></textarea>
      </div>

      <div class="panel panel-default ">
        <div class="panel-heading">
          <h4 class="panel-title">Most Amazing Thing
          <input type="checkbox" checked="" class="pull-right"></h4>
        </div>
        <textarea class="form-control" rows="3"></textarea>
      </div>

      <div class="row row-sm">
        <div class="col-sm-6">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">Tech Stack
              <input type="checkbox" checked="" class="pull-right"></h4>
            </div>
            <div class="panel-body">
              <div class="m-b">
                <label>Tags</label>
                <select class="multi_select" multiple="multiple">
                  <option>React</option>
                  <option>React native</option>
                  <option>Laravel</option>
                  <option>php</option>
                  <option>javaScript</option>
                  <option>Python</option>
                </select>
              </div>
              <div>
                <label>Key Skills</label>
                <select class="multi_select" multiple="multiple">
                  <option>Redux</option>
                  <option>Mobex</option>
                  <option>Node</option>
                  <option>TypeScript</option>
                  <option>NextJs</option>
                </select>
              </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">Other Experience
              <input type="checkbox" checked="" class="pull-right"></h4>
            </div>
            <div class="panel-body">
              <div class="m-b">
                <label>Training</label>
                <select class="multi_select" multiple="multiple">
                  <option>React</option>
                  <option>React Native</option>
                  <option>UX</option>
                  <option>UI</option>
                </select>
              </div>
              <div class="m-b">
                <label>Workshop</label>
                <select class="multi_select" multiple="multiple">
                  <option>Frontend Workshop</option>
                  <option>React Native Workshop</option>
                  <option>React Workshop</option>
                </select>
              </div>
              <div>
                <label>Mini Projects</label>
                <select class="multi_select" multiple="multiple">
                  <option>Redux</option>
                  <option>Mobex</option>
                  <option>Node</option>
                  <option>TypeScript</option>
                  <option>NextJs</option>
                </select>
              </div>
            </div>
          </div>


          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">R&D
              <input type="checkbox" checked="" class="pull-right"></h4>
            </div>
            <div class="panel-body">
              <form id="form-add-research" class="form-add-research">
                <div class="input-group">
                  <input type="text" name="research" value="" id="new-research-item"  class="form-control" />
                  <span class="input-group-btn">
                    <button type="submit" id="add-research-item" class="btn btn-success add-research-item" style="padding:9px 12px"><i class="fa fa-plus"></i></button>
                  </span>
                </div>
              </form>
            </div>
            <!-- <form id="form-research-list"> -->
              <ul id="research-list" class="research-list list-group no-margin">
              </ul>
            <!-- </form> -->
          </div>

        </div>
        <div class="col-sm-6">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">Projects / Products
              <input type="checkbox" checked="" class="pull-right"></h4>
            </div>
            <div class="panel-body">
              <div class="m-b">
                <label>Projects</label>
                <select class="multi_select" multiple="multiple" placeholder="Projects">
                  <option>Cloud9</option>
                  <option>NOON</option>
                  <option>MPL</option>
                  <option>Spring Tree</option>
                </select>
              </div>
              <div>
                <label>Products</label>
                <select class="multi_select" multiple="multiple" placeholder="Products">
                  <option>Facebook App</option>
                  <option>E-commerce</option>
                  <option>Login Module</option>
                  <option>TypeScript</option>
                  <option>NextJs</option>
                </select>
              </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">Tech Talks / Meetups
              <input type="checkbox" checked="" class="pull-right"></h4>
            </div>
            <div class="panel-body">
              <div class="m-b">
                <label>Tech Talks</label>
                <select class="multi_select" multiple="multiple">
                  <option>Introduction of Flutter</option>
                  <option>CSS Animation</option>
                  <option>GarphQL</option>
                </select>
              </div>
              <div>
                <label>Meetup & Conferences</label>
                <select class="multi_select" multiple="multiple">
                  <option>React meetups</option>
                  <option>GraphQL meetups</option>
                  <option>React Native Meetups</option>
                </select>
              </div>
            </div>
          </div>
          
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">Achivements
              <input type="checkbox" checked="" class="pull-right"></h4>
            </div>
            <div class="panel-body">
              <form id="form-add-achivements" class="form-add-achivements">
                <div class="input-group">
                  <input type="text" name="achivements" value="" id="new-achivements-item"  class="form-control" />
                  <span class="input-group-btn">
                    <button type="submit" id="add-achivements-item" class="btn btn-success add-achivements-item" style="padding:9px 12px"><i class="fa fa-plus"></i></button>
                  </span>
                </div>
              </form>
            </div>
            <!-- <form id="form-achivements-list"> -->
              <ul id="achivements-list" class="achivements-list list-group no-margin">
              </ul>
            <!-- </form> -->
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">Interest
              <input type="checkbox" checked="" class="pull-right"></h4>
            </div>
            <div class="panel-body">
              <form id="form-add-interest" class="form-add-interest">
                <div class="input-group">
                  <input type="text" name="interest" value="" id="new-interest-item"  class="form-control" />
                  <span class="input-group-btn">
                    <button type="submit" id="add-interest-item" class="btn btn-success add-interest-item" style="padding:9px 12px"><i class="fa fa-plus"></i></button>
                  </span>
                </div>
              </form>
            </div>
            <!-- <form id="form-interest-list"> -->
              <ul id="interest-list" class="interest-list list-group no-margin">
              </ul>
            <!-- </form> -->
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Education -->
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">Eduction
      <input type="checkbox" checked="" class="pull-right"></h4>
    </div>
    <!-- <form> -->
      <table class="table table-striped table-condensed table-bordered bg-white" id="sections">
        <tr>
          <th>Degree</th>
          <th>College</th>
          <th>Location</th>
          <th>Percentage</th>
          <th>Year</th>
          <th class="text-center"><a href="#" class='addsection btn btn-success'><i class="fa fa-plus"></i></a></th>
        </tr>
        <tr class="section">
          <td><input name="degree[]" id="degree" class="form-control" value="" type="text" /></td>
          <td><input name="degree_college[]" id="degree_college" class="form-control" value="" type="text" /></td>
          <td><input name="degree_location[]" id="degree_location" class="form-control" value="" type="text" /></td>
          <td><input name="degree_percentage[]" id="degree_percentage" class="form-control" value="" type="text" /></td>
          <td><input name="degree_year[]" id="degree_year" class="form-control" value="" type="text" /></td>
          <td class="text-center"><span><button class="btn btn-default text-danger btn-sm remove"><i class="fa fa-trash"></i></button></span></td>
        </tr>
      </table>
    <!-- </form> -->
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
     $('.multi_select').select2();

    // Interest 

    function addinterestItem() {
      var interestItem = $("#new-interest-item").val();
      $("#interest-list").append("<li class='list-group-item'>" + interestItem +
                             " <a class='interest-item-delete pull-right text-danger'>"+
                             "<i class='fa fa-trash'></i></a></li>");
      
     $("#new-interest-item").val("");
    }

    function deleteinterestItem(e, item) {
      e.preventDefault();
      $(item).parent().fadeOut('slow', function() { 
        $(item).parent().remove();
      });
    }

    $(function() {
      $("#add-interest-item").on('click', function(e){
        e.preventDefault();
        addinterestItem()
      });

      $("#interest-list").on('click', '.interest-item-delete', function(e){
        var item = this; 
        deleteinterestItem(e, item)
      })
    });

    // Achivements 

    function addachivementsItem() {
      var achivementsItem = $("#new-achivements-item").val();
      $("#achivements-list").append("<li class='list-group-item'>" + achivementsItem +
                             " <a class='achivements-item-delete pull-right text-danger'>"+
                             "<i class='fa fa-trash'></i></a></li>");
      
     $("#new-achivements-item").val("");
    }

    function deleteachivementsItem(e, item) {
      e.preventDefault();
      $(item).parent().fadeOut('slow', function() { 
        $(item).parent().remove();
      });
    }

    $(function() {
      $("#add-achivements-item").on('click', function(e){
        e.preventDefault();
        addachivementsItem()
      });

      $("#achivements-list").on('click', '.achivements-item-delete', function(e){
        var item = this; 
        deleteinterestItem(e, item)
      })
    });

    // R&D 

    function addresearchItem() {
      var researchItem = $("#new-research-item").val();
      $("#research-list").append("<li class='list-group-item'>" + researchItem +
                             " <a class='research-item-delete pull-right text-danger'>"+
                             "<i class='fa fa-trash'></i></a></li>");
      
     $("#new-research-item").val("");
    }

    function deleteresearchItem(e, item) {
      e.preventDefault();
      $(item).parent().fadeOut('slow', function() { 
        $(item).parent().remove();
      });
    }

    $(function() {
      $("#add-research-item").on('click', function(e){
        e.preventDefault();
        addresearchItem()
      });

      $("#research-list").on('click', '.research-item-delete', function(e){
        var item = this; 
        deleteinterestItem(e, item)
      })
    });

    //Education

    //define template
    var template = $('#sections .section:first').clone();

    //define counter
    var sectionsCount = 1;

    //add new section
    $('body').on('click', '.addsection', function() {

        //increment
        sectionsCount++;

        //loop through each input
        var section = template.clone().find(':input').each(function(){

            //set id to store the updated section number
            var newId = this.id + sectionsCount;

            //update for label
            $(this).prev().attr('for', newId);

            //update id
            this.id = newId;

        }).end()

        //inject new section
        .appendTo('#sections');
        return false;
    });

    //remove section
    $('#sections').on('click', '.remove', function() {
        //fade out section
        $(this).parent().fadeOut(300, function(){
            //remove parent element (main section)
            $(this).parent().parent().empty();
            return false;
        });
        return false;
    });


  });
</script>
@endsection