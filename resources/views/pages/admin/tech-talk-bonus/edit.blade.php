@extends('layouts.admin-dashboard')
@section('main-content')

<section class="new-project-section">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">Tech-talk Bonuses</h1>
                    <ol class="breadcrumb">
                        <li><a href="/admin">Admin</a></li>
                        <li><a href="{{ url('admin/bonus/tech-talk') }}">Tech-talk Bonuses</a></li>
                        <li class="active">update</li>
                    </ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>

	    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif
		<form class="form-horizontal" method="post" action="/admin/bonus/tech-talk/{{$techTalkBonus->id}}" enctype="multipart/form-data">
		 {{ method_field('PUT') }}
			<div class="panel panel-default">
				<div class="panel-body">
				    <div class="row">
				    	<div class="col-md-12">

                            <div class="form-group">
								<label for="" class="col-sm-2 control-label">Topic</label>
								<div class="input-group col-sm-4">
									<input type="text" id="topic" class="form-control" placeholder="Topic" name="topic" value="{{$techTalkBonus->topic}}" required/>
								</div>
							</div>

                            <!-- ======================== Date Picker ======================= -->
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Date</label>
                                <div class='input-group date col-sm-4' id='datetimepicker6'>
                                    <input type='text' class="form-control" name="date" value="{{$techTalkBonus->date}}"/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <!-- ======================== Date Picker ======================= -->
                            <div class="form-group">
								<label for="" class="col-sm-2 control-label">Notes</label>
								<div class="input-group col-sm-4">
									<textarea name="note" id="note" class="form-control" placeholder="Notes...">{{$techTalkBonus->notes}}</textarea>
								</div>
							</div>
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">Employee Name</label>
								<div class="input-group col-sm-4">
									<select id="selectid2" name="user_id[]" multiple="true"  style="width=35%;" placeholder= "Select name">
											@foreach($userList as $user)
												@foreach($techUserObj as $techuser)

												@if( $user->id == $techuser->user_id)
												<option value="{{ $user->id}}" selected> {{$techuser->user->name}}</option>
												@endif

												@endforeach
												<option value=" {{$user->id}}"> {{$user->name}}</option>
											@endforeach
									</select>
								</div>
							</div>
					  	</div>
					</div>
				</div>
			</div>
  	  		<div class="text-center">
		  		<button type="submit" class="btn btn-success">Save</button>
		  	</div>
		</form>
	</div>
</section>
<script type="text/javascript">
    $(function () {
        $('#datetimepicker6').datetimepicker({
            format: 'YYYY-MM-DD'
        });
    });
</script>
@endsection
