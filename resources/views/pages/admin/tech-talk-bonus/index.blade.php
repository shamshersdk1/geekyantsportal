@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Tech-talk Bonuses</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/bonus/tech-talk') }}">Tech-talk Bonuses</a></li>
                </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <a href="/admin/bonus/tech-talk/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i> Add new Bonus</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
    	@if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span><br/>
                  @endforeach
            </div>
        @endif
        @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
        @endif
    <div class="user-list-view">
        <div class="panel panel-default">
            <table class="table table-striped" id="tech-talk-table">
                <thead>
                    <th>#</th>
                    <th>Topic</th>
                    <th>Talk Date</th>
                    <th>Users</th>
                    <th>Created At</th>
                    <th>Created By</th>
                    <th>Actions</th>
                </thead>
                @if(!empty($techTalkBonus))
                    @foreach($techTalkBonus as $index => $bonus)
                        <tr>
                            <td class="td-text">{{$index+1}}</td>
                             <td class="td-text">{{$bonus->topic}}</td>
                             <td class="td-text">{{date_in_view($bonus->date)}}</td>
                            <td class="td-text">
                            @foreach ($bonus->techtalkuser as $user)
                                <label>{{$user->user->name}}, </label>
                            @endforeach
                            </td>
                             <td class="td-text">{{date_in_view($bonus->created_at)}}</td>
                             <td class="td-text">{{$bonus->reviewer->name}}</td>
                            <td class="td-text">
                            @if($bonus->status=='approved' )
                            <a href="/admin/bonus/tech-talk/{{$bonus->id}}/review" class="btn btn-info btn-sm" style="display:inline-block;" > View</a>
                            @endif
                            @if($bonus->status=='pending' || $bonus->status=='inprogress')
                            <a href="/admin/bonus/tech-talk/{{$bonus->id}}/review" class="btn btn-info btn-sm" style="display:inline-block;" > Review</a>
                            @endif
                                <form action="/admin/bonus/tech-talk/{{$bonus->id}}/edit" method="put" style="display:inline-block;" >
                                @if($bonus->status=='pending')
                                    <button class="btn btn-primary  btn-sm" type="submit"><i class="fa fa-edit btn-icon-space" aria-hidden="true"></i>Edit</button>
                                @endif
                                </form>
                                @if($bonus->status=='pending' || $bonus->status=='inprogress' )

                                <form action="/admin/bonus/tech-talk/{{$bonus->id}}" method="post" style="display:inline-block;" >
                                {{ method_field('DELETE') }}
                                <button class="btn btn-danger  btn-sm" type="submit"><i class="fa fa-trash btn-icon-space" aria-hidden="true"></i>Delete</button>
                                </form>
                            @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="7">No results found.</td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
      
	</div>
</div>
<script>
		$(function(){
            $tableHeight = $( window ).height();
            $('#tech-talk-table').DataTable({
                "pageLength": 500,
                scrollY: $tableHeight - 200,
                scrollX: true,
                scrollCollapse: true,
            });
        });
</script>
@endsection
