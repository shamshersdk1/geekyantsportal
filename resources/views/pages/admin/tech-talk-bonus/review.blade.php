@extends('layouts.admin-dashboard')
@section('main-content')

    <div class="container-fluid">
        <div class="breadcrumb-wrap">
            <div class="row">
                <div class="col-sm-8">
                    <h1 class="admin-page-title">Tech-talk Bonuses</h1>
                    <ol class="breadcrumb">
                        <li><a href="/admin">Admin</a></li>
                        <li><a href="{{ url('admin/bonus/tech-talk') }}">Tech-talk Bonuses</a></li>
                        <li>Rating</li>
                    </ol>
                </div>
            </div>
        </div>
        @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif
        <div class="row">
            <div class="col-sm-2">
                <h4>Topic :</h4>
            </div>
            <div class="col-sm-2">
                <h4>{{$techTalkBonus->topic ? $techTalkBonus->topic:''}}</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2">
                <h4> Date :</h4>
            </div>
            <div class="col-sm-4">
                <h4>{{$techTalkBonus->date ? date_in_view($techTalkBonus->date) : ''}}</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2">
                <h4>Description :</h4>
            </div>
            <div class="col-sm-4">
                <h4>{{$techTalkBonus->notes ? $techTalkBonus->notes :''}}</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2">
                <h4>Status :</h4>
            </div>
            <div class="col-sm-2">
                @if($techTalkBonus->status)
                    @if($techTalkBonus->status=='pending')
                    <h4 class="label label-primary">{{$techTalkBonus->status}}</h4>
                    @elseif($techTalkBonus->status=='inprogress')
                    <h4 class="label label-warning">{{$techTalkBonus->status}}</h4>
                    @else
                    <h4 class="label label-success">{{$techTalkBonus->status}}</h4>
                    @endif
                @else
                    <h4></h4>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2">
                <h4>Created By :</h4>
            </div>
            <div class="col-sm-4">
                <h4>{{$techTalkBonus->user->name}} at {{date_in_view($techTalkBonus->created_at)}}</h4>
            </div>
        </div>
        <div class="row">
            <div class="panel panel-default">
                <table class="table table-striped">
                    <thead>
                        <th width="5%">#</th>
                        <th  width="15%">User </th>
                        <th  width="20%">Rating</th>
                        <th  width="20%">Amount</th>
                        <th  width="40%">Comment</th>

                    </thead>

                        @if($techTalkBonus->techtalkuser && count($techTalkBonus->techtalkuser)>0)
                        @foreach($techTalkBonus->techtalkuser as $index=> $techUser)
                        <form class="form-horizontal" method="post" action="/admin/bonus/tech-talk/{{$techTalkBonus->id}}/review" enctype="multipart/form-data">
                            <tr>
                                <td class="td-text">{{$index+1}}</td>
                                <td class="td-text">{{$techUser->user->name}}</td>
                                <td class="td-text">
                                    <input type="number" id="rating" class="form-control" placeholder="Rating"  style="width:100px" name="{{$techUser->user_id}}[rating]" min="0" max="10" value="{{$techUser['rating']}}" {{ $techTalkBonus->status && $techTalkBonus->status =='approved' ? 'disabled' : ''}}  />
                                </td>
                                <td class="td-text">
                                    <input type="number" id="amount" class="form-control" placeholder="amount"  style="width:100px" name="{{$techUser->user_id}}[amount]" min="0" value="{{$techUser['amount']}}"  {{ $techTalkBonus->status && $techTalkBonus->status =='approved' ? 'disabled' : ''}} />
                                </td>
                                <td class="td-text">
                                    <input type="text" id="comment" class="form-control" placeholder="comment"  style="width:350px;height:50px" name="{{$techUser->user_id}}[comment]" value="{{$techUser['comments']}}" {{ $techTalkBonus->status && $techTalkBonus->status =='approved' ? 'disabled' : ''}}  />

                                </td>

                            </tr>
                        @endforeach
                    @else
                        <tr colspan="5">No records Found</tr>
                    @endif


                </table>
         </div>
    </div>
    <div class="text-right">
        @if( $techTalkBonus->status!=='approved')
            <button type="submit" class="btn btn-success" style="margin-left:20px">Save</button>
        @endif
    </div>
    </form>
    <form  method="post" action="/admin/bonus/tech-talk/{{$techTalkBonus->id}}/save" >
        @if( $techTalkBonus->status !=='approved')
            <button type="submit" class="btn btn-warning"  >Close</button>
        @endif
    </form>

</div>

@endsection
