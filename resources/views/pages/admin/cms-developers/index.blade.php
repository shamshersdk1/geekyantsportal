@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">CMS Developers</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
		  			<li class="active">CMS Developers Dashboard</li>
        		</ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
				<a href="/admin/cms-developers/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Add Technology</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
	    	@if(!empty($errors->all()))
	            <div class="alert alert-danger">
	                @foreach ($errors->all() as $error)
	                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                    <span>{{ $error }}</span><br/>
	                  @endforeach
	            </div>
	        @endif
	        @if (session('message'))
	            <div class="alert alert-success">
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ session('message') }}</span><br/>
	            </div>
	        @endif
		<!--	<div class="row" style="margin-bottom:1%">
                <div class="col-md-3">
                    <form action="/admin/project" method="GET">
                        @if( !empty($search) )
                            Showing results for
                        @endif
                        <input type="text"
						name="searchprojects"
						class="form-control"
						placeholder="Search project by name .."
						value="@if(isset($search)){{$search}}@endif">
                    </form>
                </div>
				<div class="col-md-9">
                <ul class="btn-group" style="padding:0;float:right">
                    <a href="/admin/project?" type="button" class="btn btn-default @if (!Request::get('status')&&!Request::get('status')&&!Request::get('searchprojects') ) btn-success @endif" >In Progress</a>
                    <a href="/admin/project?@if(strpos(Request::getQueryString(), 'status') !== false)status=my @endif&@if(empty(Request::get('status')))status=my @endif" type="button" class="btn btn-default @if( preg_replace('/[\s]+.*/','',Request::get('status')) == 'my')btn-success @endif">My Projects</a>
					<a href="/admin/project?@if(strpos(Request::getQueryString(), 'status') !== false)status=completed @endif&@if(empty(Request::get('status')))status=completed @endif" type="button" class="btn btn-default @if( preg_replace('/[\s]+.*/','',Request::get('status')) == 'completed')btn-success @endif">Completed</a>
					<a href="/admin/project?@if(strpos(Request::getQueryString(), 'status') !== false)status=failed @endif&@if(empty(Request::get('status')))status=failed @endif" type="button" class="btn btn-default @if( preg_replace('/[\s]+.*/','',Request::get('status')) == 'failed')btn-success @endif">Failed</a>
					<a href="/admin/project?@if(strpos(Request::getQueryString(), 'searchprojects') !== false)searchprojects={{str_replace(' ','+',Request::get('searchprojects'))}}&@endif @if(strpos(Request::getQueryString(), 'status') !== false)status=all @endif&@if(empty(Request::get('status')))status=all @endif" type="button" class="btn btn-default @if( preg_replace('/[\s]+.*/','',Request::get('status')) == 'all')btn-success @endif @if( strpos(Request::getQueryString(), 'searchprojects') !== false) btn-success @endif">All</a>
                </ul>
                <h4 style="float:right">Filters:&nbsp&nbsp </h4>
            </div>
            </div> -->
		  <div class="user-list-view">
            <div class="panel panel-default">
            	<table class="table table-striped">
					<tr>
						<th width="5%" class="sorting_asc">#</th>
						<th width="10%" class="sorting_asc">Technology</th>
						<th width="10%">Title</th>
						<th width="12%">Description</th>
						<th width="6%">Slug</th>
						<th width="12%">Keywords</th>
						<th width="12%">Meta Description</th>
						<th width="8%">Hire Text</th>
						<th width="10%">Status</th>
						<th class="text-right sorting_asc" width="15%">Actions</th>
					</tr>
					@if(count($cmsDevelopers) > 0)
						@foreach($cmsDevelopers as $index => $cmsDeveloper)
							<tr>
								<td class="td-text"><a>{{$index+1}}</a></td>
								<td class="td-text">{{$cmsDeveloper->name}}</td>
								<td class="td-text">{{$cmsDeveloper->title}}</td>
								<td class="td-text">{{$cmsDeveloper->description}}</td>
								<td class="td-text">{{$cmsDeveloper->slug}}</td>
								<td class="td-text">{{!empty($cmsDeveloper->keywords) ? $cmsDeveloper->keywords : '' }}</td>
								<td class="td-text">{{!empty($cmsDeveloper->meta_description) ? $cmsDeveloper->meta_description : '' }}</td>
								<td class="td-text">{{!empty($cmsDeveloper->hire_text) ? $cmsDeveloper->hire_text : '' }}</td>
								<td>
									<div class="col-md-4">
										<div class="onoffswitch">
											<input type="checkbox" value="{{$cmsDeveloper->id}}" name="onoffswitch[]" onchange="toggle(this)"
											class="onoffswitch-checkbox" id="{{$cmsDeveloper->id}}"
											<?php echo (!empty($cmsDeveloper->status)&&$cmsDeveloper->status==1) ? 'checked': ''; ?>>
											<label class="onoffswitch-label" for= {{$cmsDeveloper->id}} >
												<span class="onoffswitch-inner"></span>
												<span class="onoffswitch-switch"></span>
											</label>
										</div>
									</div>
								</td>
								<td class="text-right">
									<a href="/admin/cms-developers/{{$cmsDeveloper->id}}" class="btn btn-success crude-btn btn-sm">View</a>
									<a href="/admin/cms-developers/{{$cmsDeveloper->id}}/edit" class="btn btn-info btn-sm crude-btn">Edit</a>

									<form action="/admin/cms-developers/{{$cmsDeveloper->id}}" method="post" style="display:inline-block;">
										<input name="_method" type="hidden" value="DELETE">
										<button class="btn btn-danger btn-sm crude-btn" type="submit">Delete</button>
									</form>
								</td>
							</tr>
						@endforeach
	            	@else
	            		<tr>
	            			<td colspan="3" class="text-cenbter">No results found.</td>
	            		</tr>
	            	@endif
            	</table>
				<div class="col-md-12">
                    <div class="pageination pull-right">
                        <nav aria-label="Page navigation">
                              <ul class="pagination">
                                {{ $cmsDevelopers->render() }}
                              </ul>
                        </nav>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>
</div>
<script>
    $(function(){
        toggle = function (item) {
			//window.location.href='users-billable/'+item.value;
			$.ajax({
				type:'POST',
				url:'/api/v1/cms-developers/status',
				data:{id:item.value},
				success:function(data){
					console.log(data.success);
					}
			});
		}
    });
</script>
@endsection
