@extends('layouts.admin-dashboard')
@section('main-content')
<section>
   <div class="container-fluid">
      <div class="breadcrumb-wrap">
         <div class="row">
            <div class="col-sm-6">
               <h1>Asset</h1>
               <ol class="breadcrumb">
                  <li><a href="/admin">Admin</a></li>
                  <li><a href="/admin/cms-developers">CMS Developer</a></li>
                  <li class="active">{{$cmsObj->id}}</li>
               </ol>
            </div>
            <div class="col-sm-6 text-right">
               <a href="/admin/cms-slides/create/{{$cmsObj->id}}" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Add Slides</a>
               <a href="/admin/cms-technology-developers/create/{{$cmsObj->id}}" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Add Developer</a>
               <a href="/admin/cms-developers/{{$cmsObj->id}}/edit" class="btn btn-info">Edit</a>
               <a href="/admin/cms-developers" class="btn btn-primary"><i class="fa fa-caret-left fa-fw"></i>Back</a>
            </div>
         </div>
      </div>
      @if(!empty($errors->all()))
      <div class="alert alert-danger">
         @foreach ($errors->all() as $error)
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
         <span>{{ $error }}</span><br/>
         @endforeach
      </div>
      @endif
      @if (session('message'))
      <div class="alert alert-success">
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
         <span>{{ session('message') }}</span><br/>
      </div>
      @endif
      <div class="row cms-dev">
         <div class="col-md-12">
            <h4 >CMS Developer Technology Detail</h4>
            <div class="panel panel-default">
               <div class="panel-body row">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <div class=" col-md-6">
                     <label>Name : </label>
                     <span>{{$cmsObj->name}}</span>
                  </div>
                  <div class=" col-md-6">
                     <label>Title : </label>
                     <span>{{$cmsObj->title}}</span>
                  </div>
                  <div class=" col-md-6">
                     <label>Slug : </label>
                     <span>{{ $cmsObj->slug }}</span>
                  </div>
                  <div class=" col-md-6">
                        <label>YouTube Link : </label>
                        <span>{{ $cmsObj->youtube_link ?: '' }}</span>
                     </div>
                  <div class="col-md-6">
                     <label>Keywords : </label>
                     <span>{{!empty($cmsObj->keywords) ? $cmsObj->keywords : '' }}</span>
                  </div>
                  <div class="col-md-6">
                     <label>Meta Description : </label>
                     <span>{{!empty($cmsObj->meta_description) ? $cmsObj->meta_description : '' }}</span>
                  </div>
                  <div class="col-md-6">
                     <label>Status : </label>
                     <span>{{!empty($cmsObj->status) ? 'Active' : 'Inactive' }}</span>
                  </div>
                  <div class="col-md-12">
                     <label>Description : </label>
                     <span>{{ $cmsObj->description }}</span>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-md-12">
            <h4> Developers List </h4>
            <div class="panel panel-default">
               <table class="table table-striped">
                  <tr>
                     <th width="20%" class="sorting_asc">Image</th>
                     <th width="10%">Name</th>
                     <th width="15%">About</th>
                     <th width="15%">Area of Interest</th>
                     <th width="15%">Achievement</th>
                     <th width="10%">Status</th>
                     <th class="text-right sorting_asc" width="15%">Actions</th>
                  </tr>
                  @if(count($cmsDevelopers) > 0)
                  @foreach($cmsDevelopers as $index => $cmsDeveloper)
                  <tr>
                     @if( !empty($cmsDeveloper->image->path) )
                     <td class="td-text"> <img src="{{ $cmsDeveloper->image->path }}" style="height: 200px; width:200px;"> </td>
                     @else
                     <td class="td-text"> No image uploaded </td>
                     @endif
                     <td class="td-text"> {{ $cmsDeveloper->user->name }} </td>
                     <td class="td-text"> {{ $cmsDeveloper->about }}</td>
                     <td class="td-text"> {{ $cmsDeveloper->area_of_interest }}</td>
                     <td class="td-text"> {{ $cmsDeveloper->achievement }}</td>
                     <td>
                        <div class="col-md-4">
                           <div class="onoffswitch">
                              <input type="checkbox" value="{{$cmsDeveloper->id}}" name="onoffswitch[]" onchange="toggle(this)"
                                 class="onoffswitch-checkbox" id="cmsDeveloper{{$cmsDeveloper->id}}" 
                                 <?php echo (!empty($cmsDeveloper->status)&&$cmsDeveloper->status==1) ? 'checked': ''; ?>>
                              <label class="onoffswitch-label" for= "cmsDeveloper{{$cmsDeveloper->id}}" >
                              <span class="onoffswitch-inner"></span>
                              <span class="onoffswitch-switch"></span>
                              </label>
                           </div>
                        </div>
                     </td>
                     <td class="text-right">
                        <a href="/admin/cms-technology-developers/{{$cmsDeveloper->id}}/edit" class="btn btn-info btn-sm crude-btn">Edit</a>
                     </td>
                  </tr>
                  @endforeach
                  @else
                  <tr>
                     <td colspan="3" class="text-cenbter">No results found.</td>
                  </tr>
                  @endif
               </table>
            </div>
            <!-- Slides  --> 
            <h4> Slides List </h4>
            <div class="panel panel-default">
               <table class="table table-striped">
                  <tr>
                     <th width="20%" class="sorting_asc">Image</th>
                     <th width="10%">Title</th>
                     <th width="35%">Description</th>
                     <th width="10%">User Name</th>
                     <th width="10%">Status</th>
                     <th class="text-right sorting_asc" width="15%">Actions</th>
                  </tr>
                  @if(count($cmsSlides) > 0)
                  @foreach($cmsSlides as $index => $cmsSlide)
                  <tr>
                     @if( !empty($cmsSlide->image->path) )
                     <td class="td-text"> <img src="{{ $cmsSlide->image->path }}" style="height: 200px; width:200px;"> </td>
                     @else
                     <td class="td-text"> No image uploaded </td>
                     @endif
                     <td class="td-text"> {{ $cmsSlide->title }} </td>
                     <td class="td-text"> {{ $cmsSlide->description }}</td>
                     <td class="td-text"> {{ $cmsSlide->user_name }}</td>
                     <td>
                        <div class="col-md-4">
                           <div class="onoffswitch">
                              <input type="checkbox" value="{{$cmsSlide->id}}" name="onoffswitch[]" onchange="toggle2(this)"
                                 class="onoffswitch-checkbox" id="cmsSlide{{$cmsSlide->id}}" 
                                 <?php echo (!empty($cmsSlide->status)&&$cmsSlide->status==1) ? 'checked': ''; ?>>
                              <label class="onoffswitch-label" for= "cmsSlide{{$cmsSlide->id}}" >
                              <span class="onoffswitch-inner"></span>
                              <span class="onoffswitch-switch"></span>
                              </label>
                           </div>
                        </div>
                     </td>
                     <td class="text-right">
                        <a href="/admin/cms-slides/{{$cmsSlide->id}}/edit" class="btn btn-info btn-sm crude-btn">Edit</a>
                     </td>
                  </tr>
                  @endforeach
                  @else
                  <tr>
                     <td colspan="3" class="text-cenbter">No results found.</td>
                  </tr>
                  @endif
               </table>
            </div>
         </div>
      </div>
   </div>
</section>
<script>
   $(function(){
       toggle = function (item) {
   console.log('toggle1');
   $.ajax({
   type:'POST',
   url:'/api/v1/cms-developers-technology/status',
   data:{id:item.value},
   success:function(data){
   	console.log(data.success);
   	}
   });
   }
   });
   $(function(){
       toggle2 = function (item) {
   console.log('toggle2');
   $.ajax({
   type:'POST',
   url:'/api/v1/cms-slides/status',
   data:{id:item.value},
   success:function(data){
   	console.log(data.success);
   	}
   });
   }
   });
</script>
@endsection