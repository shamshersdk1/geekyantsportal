@extends('layouts.admin-dashboard')
@section('main-content')

<section class="new-project-section">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">CMS Developers</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
			  			<li><a href="{{ url('admin/cms-developers') }}">CMS Developers</a></li>
			  			<li class="active">Add</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>

	    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif

		<form class="form-horizontal" method="post" action="/admin/cms-developers" enctype="multipart/form-data">
			<div class="panel panel-default">
				<div class="panel-body">
				    <div class="row">
				    	<div class="col-md-12">
						  	<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Name</label>
						    	<div class="col-sm-6">
						      		<input type="text" class="form-control" id="" name="name" value="{{ old('name') }}">
						    	</div>
						  	</div>
						  	<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Title</label>
						    	<div class="col-sm-6">
						      		<input type="text" class="form-control" id="" name="title" value="{{ old('title') }}">
						    	</div>
						  	</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Status:</label>
								<div class="col-sm-4">
									<select class="form-control" name="status">
										<option value="1">Active</option>
										<option value="0">Inactive</option>
									</select>
								</div>
							</div>
						  	<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Description</label>
						    	<div class="col-sm-6">
						      		<textarea class="form-control" rows="5" name="description">{{ old('description') }}</textarea>
						    	</div>
						  	</div>
							<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Slug</label>
						    	<div class="col-sm-6">
						      		<input type="text" class="form-control" id="" name="slug" value="{{ old('slug') }}">
						    	</div>
							  </div>
							  <div class="form-group">
									<label for="" class="col-sm-2 control-label">YouTube Link</label>
									<div class="col-sm-6">
										  <input type="text" class="form-control" id="" name="youtube_link" value="{{ old('youtube_link') }}">
									</div>
								</div>
							<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Keywords</label>
						    	<div class="col-sm-6">
						      		<textarea class="form-control" rows="5" name="keywords">{{ old('keywords') }}</textarea>
						    	</div>
						  	</div>
							<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Meta Description</label>
						    	<div class="col-sm-6">
						      		<textarea class="form-control" rows="5" name="meta_description">{{ old('meta_description') }}</textarea>
						    	</div>
						  	</div>
							<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Hire Text</label>
						    	<div class="col-sm-6">
						      		<textarea class="form-control" rows="5" name="hire_text">{{ old('hire_text') }}</textarea>
						    	</div>
							</div>
					  	</div>
					</div>
				</div>
			</div>
  	  		<div class="text-center">
		  		<button type="submit" class="btn btn-success">Create New CMS Developer Technology</button>
		  	</div>
		</form>
	</div>
</section>
@endsection
