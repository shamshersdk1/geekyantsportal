@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Performance Bonuses</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/bonus/performance') }}">Performance Bonuses</a></li>
                </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <a href="/admin/bonus/performance/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i> Add new Bonus</a>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <form action="/admin/bonus/performance" method="GET">
            @if( !empty($search) )
                Showing results for
            @endif
            <input type="text" name="searchuser" class="form-control" placeholder="search.." value="@if(isset($search)){{$search}}@endif">
            <input type="hidden" name="filter" value="all">
        </form>
    </div>
    <div class="row">
        <div class="col-md-12">
    	@if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span><br/>
                  @endforeach
            </div>
        @endif
        @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
        @endif  
        
    <div class="user-list-view">
        
        <div class="panel panel-default"> 
            <table class="table table-striped" id="performance-table">
                <thead>
                    <th>#</th>
                    <th>Name</th>
                    <th>Month</th>
                    <th>Amount</th>
                    <th>Status</th>
                    <th>Created By</th>
                    <th>Created At</th>
                    <th>Actions</th>
                </thead>
                @if(count($performanceBonus) > 0)
                    @foreach($performanceBonus as $index=>$bonus)
                        <tr>
                            <td class="td-text">{{$index+1}}</td>
                            @if(!empty($bonus->user->name))
                                <td class="td-text">{{$bonus->user->name}}</td>
                            @else
                                <td class="td-text"></td>
                             @endif
                            
                            
                            <td class="td-text">{{$bonus->month->formatMonth()}}</td>
                            <td class="td-text">{{($bonus->amount)}}</td>
                            <td>
                            @if($bonus->status == 'approved')
                                <label class="label label-success">{{$bonus->status}}</label>
                            @elseif($bonus->status == 'pending')
                                <label class="label label-primary">{{$bonus->status}}</label>
                            @else
                                <label class="label label-danger">{{$bonus->status}}</label>
                            @endif
                            </td>
                            <td class="td-text">{{$bonus->createdBy->name}}</td>
                            <td class="td-text">{{date_in_view($bonus->created_at)}}</td>
                            <td class="td-text">
                                <form action="/admin/bonus/performance/{{$bonus->id}}" method="get" style="display:inline-block;" >
                                    <button class="btn btn-success  btn-sm" type="submit"><i class="fa fa-eye fa-fw btn-icon-space"  aria-hidden="true"></i>View</button>
                                </form>
                                @if($bonus->status == 'pending')
                                <form action="/admin/bonus/performance/{{$bonus->id}}/approve" method="get" style="display:inline-block;" >
                                    <button class="btn btn-success  btn-sm" type="submit"><i class="fa fa-check fa-fw btn-icon-space"  aria-hidden="true"></i>Approve</button>
                                </form>
                                <form action="/admin/bonus/performance/{{$bonus->id}}/edit" method="put" style="display:inline-block;" >
                                    <button class="btn btn-primary  btn-sm" type="submit"><i class="fa fa-edit btn-icon-space" aria-hidden="true"></i>Edit</button>
                                </form>
                                <form action="/admin/bonus/performance/{{$bonus->id}}" method="post" style="display:inline-block;" >
                                {{ method_field('DELETE') }}
                                <button class="btn btn-danger  btn-sm" type="submit"><i class="fa fa-trash btn-icon-space" aria-hidden="true"></i>Delete</button>
                                </form>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="10">No results found.</td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
   
	</div>
</div>
<script>
$(function(){
            $tableHeight = $( window ).height();
            $('#performance-table').DataTable({
                "pageLength": 500,
                scrollY: $tableHeight - 200,
                scrollX: true,
                scrollCollapse: true,
            });
        });
</script>
@endsection
