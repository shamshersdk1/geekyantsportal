@extends('layouts.admin-dashboard')
@section('main-content')

<section class="new-project-section">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">Performance Bonuses</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
			  			<li><a href="{{ url('admin/bonus/performance') }}">Performance Bonuses</a></li>
			  			<li class="active">View</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>

	    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif

        <div>
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="">
                        <div>
                            <label for="" class="col-sm-2 control-label">Employee Name:</label>
                            <div class="">
                                {{$performanceObj->user->name}}
                            </div>
                        </div>
                        <br/>
                        <div>
                            <label for="" class="col-sm-2 control-label">Month:</label>
                            <div class="">
                                {{$performanceObj->month->formatMonth()}}
                            </div>
                        </div><br/>
                        <div>
                            <label for="" class="col-sm-2 control-label">Amount:</label>
                            <div class="">
                                {{$performanceObj->amount}}
                            </div>
                        </div>
                        <br/>
                        <div>
                            <label for="Name" class="col-sm-3 col-md-2 control-label">Status:</label>
                            <div class="">
                                @if($performanceObj->status == "pending")
                                    <label class="label label-warning">{{$performanceObj->status}}</label>
                                @elseif($performanceObj->status == "approved")
                                    <label class="label label-success">{{$performanceObj->status}}</label>
                                @else
                                    <label class="label label-danger">{{$performanceObj->status}}</label>
                                @endif
                            </div>
                        </div>
                        <br/>
                        <div>
                            <label for="" class="col-sm-2 control-label">Comment</label>
                            <div class="">
                                {{$performanceObj->comment}}
                            </div>
                        </div>
                        <br/>
                        <div>
                            <label for="" class="col-sm-2 control-label">Created By:</label>
                            <div class="">
                                {{$performanceObj->createdBy->name}}
                            </div>
                        </div>
                        <br/>
                        <div>
                            <label for="" class="col-sm-2 control-label">Created At:</label>
                            <div class="">
                                {{date_to_ddmmyyyy($performanceObj->created_at)}}
                            </div>
                        </div>
                        <br/>
                        <div>
                            <label for="" class="col-sm-2 control-label">Reviewed By:</label>
                            <div class="">
                                {{$performanceObj->reviewedBy->name ?? null }}
                            </div>
                        </div>
                        <br/>
                        <div>
                            <label for="" class="col-sm-2 control-label">Reviewed At:</label>
                            <div class="">
                                {{date_to_ddmmyyyy($performanceObj->reviewed_at) ?? null}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <button type="submit" class="btn btn-success">Save</button>
            </div>
        </div>
    </div>
</section>

@endsection