@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Question</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
		  			<li class="active">Question</li>
        		</ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
				<a href="/admin/feedback/question/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Add Question</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
	    	@if(!empty($errors->all()))
	            <div class="alert alert-danger">
	                @foreach ($errors->all() as $error)
	                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                    <span>{{ $error }}</span><br/>
	                  @endforeach
	            </div>
	        @endif
	        @if (session('message'))
	            <div class="alert alert-success">
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ session('message') }}</span><br/>
	            </div>
			@endif
		</div>

		<!-- <div class="col-md-12 bg-white search-section-top">
			<form class="form-group col-md-12 p-0" action="/admin/feedback/question" method="get">
			<div class="col-md-4 p-l-0">
				<div class="multipicker">
					<div class="">
						<select class="js-example-basic-multiple22 form-control" multiple="multiple" name="designation[]" placeholder="Search for Designations">
							@if(isset($designations))
								@foreach($designations as $designation)
									<?php
										$saved = false;
										foreach ($selected_designtions as $selected_designtion) {
											if ($selected_designtion == $designation->id) {
												$saved = true;
											}
										}
										?>
									@if(isset($selected_designtions))
										@if($saved)
											<option value="{{$designation->id}}" selected >{{$designation->designation}}</option>
										@else
											<option value="{{$designation->id}}">{{$designation->designation}}</option>
										@endif
									@else
										<option value="{{$designation->id}}">{{$designation->designation}}</option>
									@endif
								@endforeach
							@endif
						</select>
					</div>
				</div>
			</div>
			<div class="col-md-4 p-l-0">
					<div class="multipicker">
						<div class="">
							<select class="js-example-basic-multiple22 form-control" multiple="multiple" name="role[]" placeholder="Search for Roles">
								@if(isset($roles))
									@foreach($roles as $role)
										<?php
											$saved = false;
											foreach ($selected_roles as $selected_role) {
												if ($selected_role == $role->id) {
													$saved = true;
												}
											}
										?>
										@if(isset($selected_role))
											@if($saved)
												<option value="{{$role->id}}" selected >{{$role->name}}</option>
											@else
												<option value="{{$role->id}}">{{$role->name}}</option>
											@endif
										@else
											<option value="{{$role->id}}">{{$role->name}}</option>
										@endif
									@endforeach
								@endif
							</select>
						</div>
					</div>
				</div>
			<div class="col-md-2" style="padding-top:6px">
				<button type="submit" class="btn btn-primary btn-sm center" style="margin-left: 2px;">Search</button>
			</div>
			</form>
		</div> -->

		<div class="col-md-12">
			<h4>List of Feedback Questions</h4>
		<div class="user-list-view">
            <div class="panel panel-default">
            	<table class="table table-striped">
					<tr>
						<th width="5%" class="sorting_asc">#</th>
						<th width="25%">Question</th>
						<th width="20%">Applicable To</th>
						<th width="10%">Applicable For</th>
						<th width="10%">Reviewer Type</th>
						<th width="10%">Status</th>
						<th class="text-right sorting_asc" width="20%">Actions</th>
					</tr>
					@if(count($questions) > 0)
						@foreach($questions as $question)
							<tr>
								<td class="td-text"><a>{{$question->id}}</a></td>
								<td class="td-text">{{$question->question}}</td>
								<td class="td-text">
									@if ( $question->category_type == 'designation' )
										@if ( $question->mappedQuestion )
										@foreach( $question->mappedQuestion as $index => $mappedQuestion )
											@if( $index > 0 )
												<span>, </span>
											@endif
											<span>{{ !empty($mappedQuestion->designation->designation) ? $mappedQuestion->designation->designation : '' }} </span>
										@endforeach
										@endif
									@else
										@if ( $question->mappedQuestion )
										@foreach( $question->mappedQuestion as $index => $mappedQuestion )
											@if( $index > 0 )
												<span>, </span>
											@endif
											<span>{{ !empty($mappedQuestion->role->name) ? $mappedQuestion->role->name : '' }} </span>
										@endforeach
										@endif
									@endif
								</td>
								<td class="td-text">
									{{ ucfirst($question->category_type) }}
								</td>
								<td class="td-text">
									@if ( $question->role )
									{{ $question->role->name }}
									@endif
								</td>
								<td>
									<div class="col-md-4">
										<div class="onoffswitch">
											<input type="checkbox" value="{{$question->id}}" name="onoffswitch[]" onchange="toggle(this)"
													class="onoffswitch-checkbox" id="{{$question->id}}"
											<?php echo (!empty($question->status) && $question->status == 1) ? 'checked' : ''; ?>>
											<label class="onoffswitch-label" for= {{$question->id}} >
												<span class="onoffswitch-inner"></span>
												<span class="onoffswitch-switch"></span>
											</label>
										</div>
									</div>
								</td>
								<td class="text-right">
									<a href="/admin/feedback/question/{{$question->id}}" class="btn btn-success crude-btn btn-sm">View</a>
									<a href="/admin/feedback/question/{{$question->id}}/edit" class="btn btn-info btn-sm crude-btn">Edit</a>
									<form action="/admin/feedback/question/{{$question->id}}" method="post" style="display:inline-block;">
										<input name="_method" type="hidden" value="DELETE">
										<button class="btn btn-danger btn-sm crude-btn" type="submit">Delete</button>
									</form>
								</td>
							</tr>
				
						@endforeach
	            	@else
	            		<tr>
	            			<td colspan="3" class="text-center">No results found.</td>
	            		</tr>
	            	@endif
            	</table>
				<div class="col-md-12">
                    <div class="pageination pull-right">
                        <nav aria-label="Page navigation">
                              <ul class="pagination">
                                {{ $questions->render() }}
                              </ul>
                        </nav>
                    </div>
                </div>
			</div>
		</div>
	</div>
	</div>
</div>
</div>
<script>
    $(function(){
        toggle = function (item) {
			//window.location.href='users-billable/'+item.value;
			$.ajax({
				type:'POST',
				url:'/api/v1/question/status',
				data:{id:item.value},
				success:function(data){
					console.log(data);
					}
			});
		}
    });
</script>
@endsection