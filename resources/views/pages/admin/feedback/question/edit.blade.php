@extends('layouts.admin-dashboard')
@section('main-content')
<section class="new-project-section">
      <div class="container-fluid">
            <div class="breadcrumb-wrap">
                  <div class="row">
                        <div class="col-sm-8">
                              <h1 class="admin-page-title">Questions</h1>
                              <ol class="breadcrumb">
                                    <li><a href="/admin">Admin</a></li>
                                    <li><a href="{{ url('admin/feedback/question') }}">Question</a></li>
                                    <li class="active">{{ $questionObj->id }}</li>
                              </ol>
                        </div>
                        <div class="col-sm-4 text-right m-t-10">
                              <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i>
                                    Back</button>
                        </div>
                  </div>
            </div>
            @if(!empty($errors->all()))
            <div class="alert alert-danger">
                  @foreach ($errors->all() as $error)
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <span>{{ $error }}</span><br />
                  @endforeach
            </div>
            @endif
            @if (session('message'))
            <div class="alert alert-success">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <span>{{ session('message') }}</span><br />
            </div>
            @endif
            <form class="form-horizontal" method="post" action="/admin/feedback/question/{{$questionObj->id}}" enctype="multipart/form-data">
                  <input name="_method" type="hidden" value="PUT" />
                  <div class="panel panel-default">
                        <div class="panel-body">
                              <div class="row m-t-30">
                                    <div class="col-md-12">
                                          <div class="row">
                                                <div class="col-md-6">
                                                      <div class="form-group">
                                                            <label for="" class="col-sm-4 control-label">Question:</label>
                                                            <div class="col-sm-8">
                                                                  <div class="col-md-12">
                                                                        <input type="text" class="form-control" id="question"
                                                                              name="question" value="{{ $questionObj->question }}">
                                                                  </div>
                                                            </div>
                                                      </div>
                                                </div>
                                                <div class="col-md-6">
                                                      <div class="form-group">
                                                            <label class="col-sm-4 control-label">Status:</label>
                                                            @if( $questionObj->status )
                                                            <div class="col-sm-4">
                                                                  <div class="col-md-12">
                                                                        <select class="form-control" name="status">
                                                                              <option value="1" selected>Active</option>
                                                                              <option value="0">Inactive</option>
                                                                        </select>
                                                                  </div>
                                                            </div>
                                                            @else
                                                            <div class="col-sm-4">
                                                                  <div class="col-md-12">
                                                                        <select class="form-control" name="status">
                                                                              <option value="1">Active</option>
                                                                              <option value="0" selected>Inactive</option>
                                                                        </select>
                                                                  </div>
                                                            </div>
                                                            @endif
                                                      </div>
                                                </div>
                                                <div class="col-md-12">
                                                      <div class="form-group">
                                                            <label for="" class="col-sm-2 control-label">Reviewer:
                                                            </label>
                                                            <div class="col-sm-10">
                                                                  @foreach($roles as $index => $role)
                                                                  <div class="col-md-4">
                                                                        <input class="form-check-input" type="radio"
                                                                              name="role" value="{{$role->id}}" @if(
                                                                              $questionObj->reviewer_type == $role->id
                                                                        ) checked @elseif( $index == 0 ) checked
                                                                        @endif> {{$role->name}}
                                                                  </div>
                                                                  @endforeach
                                                            </div>
                                                      </div>
                                                </div>
                                                <div class="col-md-12">
                                                      <div class="form-group">
                                                            <label for="" class="col-sm-2 control-label">Type:</label>
                                                            <div class="col-sm-8">
                                                                  <div class="col-md-12">
                                                                        <input onchange="changeType('designation')"
                                                                              type="radio" @if( $questionObj->category_type
                                                                        == 'designation' ) checked @elseif (
                                                                        $questionObj->category_type != 'role') checked
                                                                        @endif class="form-check-input"
                                                                        id="category_type1" name="category_type"
                                                                        value="designation"> Designation </div>
                                                                  <div class="col-md-12">
                                                                        <input type="radio" onchange="changeType('role')"
                                                                              class="form-check-input" id="category_type2"
                                                                              value="role" name="category_type" @if(
                                                                              $questionObj->category_type == 'role' )
                                                                        checked @endif> Role
                                                                  </div>
                                                            </div>
                                                      </div>
                                                </div>
                                          </div>
                                          <div class="col-md-12">
                                                <div class="form-group" id="designationList">
                                                      <label class="col-sm-2 control-label"> Applicable To: </label>
                                                      <div class="col-sm-10">
                                                            @if(isset($designations))
                                                            @foreach ( $designations as $designation )
                                                            <div class="col-md-4">
                                                                  <div class="form-check form-check-inline">
                                                                        <label class="form-check-label">
                                                                              <input class="form-check-input" name="designations[]"
                                                                                    type="checkbox" value="{{$designation->id}}"
                                                                                    @if(!empty(old('designations'))&&(in_array($designation->id,
                                                                              old('designations')))) checked
                                                                              @elseif(in_array($designation->id,
                                                                              $questionDesignationArray)) checked
                                                                              @endif> {{$designation->designation}}
                                                                        </label>
                                                                  </div>
                                                            </div>
                                                            @endforeach
                                                            @endif
                                                      </div>
                                                </div>
                                          </div>
                                          <div class="col-md-12">
                                                <div class="form-group" id="roleList">
                                                      <label class="col-sm-2 control-label"> Applicable To: </label>
                                                      <div class="col-sm-10">
                                                            @if(isset($applicableToRole))
                                                            @foreach ( $applicableToRole as $role )
                                                            <div class="col-md-4">
                                                                  <div class="form-check form-check-inline">
                                                                        <label class="form-check-label">
                                                                              <input class="form-check-input" name="roles[]"
                                                                                    type="checkbox" value="{{$role->id}}"
                                                                                    @if(!empty(old('roles'))&&(in_array($role->id,
                                                                              old('roles')))) checked
                                                                              @elseif(in_array($role->id,
                                                                              $questionRoleArray)) checked @endif>
                                                                              {{$role->name}}
                                                                        </label>
                                                                  </div>
                                                            </div>
                                                            @endforeach
                                                            @endif
                                                      </div>
                                                </div>
                                          </div>
                                    </div>
                              </div>
                        </div>
                  </div>
                  <div class="text-center">
                        <button type="submit" class="btn btn-success">Update Question</button>
                  </div>
            </form>
      </div>
</section>
<script>
      $(window).load(function () {
            var x = $("input[name='category_type']:checked").val();
            if (x == 'role') {
                  $('#roleList').show()
                  $('#designationList').hide()
            } else {
                  console.log(x)
                  $('#roleList').hide()
                  $('#designationList').show()
            }
      });
      $(function () {
            changeType = function (type) {
                  console.log('changeType calledchangeType', type);
                  if (type == 'role') {
                        $('#roleList').show()
                        $('#designationList').hide()
                  } else {
                        console.log('')
                        $('#roleList').hide()
                        $('#designationList').show()
                  }
            }
      });
</script>
@endsection