@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Feedback Report</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
		  			<li class="active">Month List</li>
        		</ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
	    	@if(!empty($errors->all()))
	            <div class="alert alert-danger">
	                @foreach ($errors->all() as $error)
	                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                    <span>{{ $error }}</span><br/>
	                  @endforeach
	            </div>
	        @endif
	        @if (session('message'))
	            <div class="alert alert-success">
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ session('message') }}</span><br/>
	            </div>
			@endif
		</div>
		<div class="user-list-view">
            <div class="panel panel-default">
            	<table id="a" class="table table-striped">
                    <thead>
                        <tr>
                            <th width="5%" class="sorting_asc">#</th>
                            <th width="30%">Month</th>
                            <th width="15%">Status</th>
                            <th width="10%">Employee Visibility</th>
                            <th width="20%">Created At</th>
                            <th class="text-right" width="20%">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
					@if(count($monthList) > 0)
						@foreach($monthList as $index => $month)
							<tr>
								<td class="td-text"><a>{{$month->id}}</a></td>
                                <td class="td-text">{{ date('F', mktime(0, 0, 0, $month->month, 10)) }}, {{$month->year}}  </td>
                                @if( !$month->locked ) 
                                    <td><span class="label label-primary custom-label">Open</span></td>
                                @else
                                    <td><span class="label label-danger custom-label">Locked</span></td>
                                @endif
                                <td>
                                    <div class="col-md-4">
                                        <div class="onoffswitch">
                                            <input type="checkbox" value="{{$month->id}}" name="onoffswitch[]" onchange="toggle(this)"
                                            class="onoffswitch-checkbox" id="{{$month->id}}" 
                                            <?php echo (!empty($month->is_visible)&&$month->is_visible==1) ? 'checked':''; ?>>
                                            <label class="onoffswitch-label" for= {{$month->id}} >
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                    </div>
                                </td>
                                <td class="td-text">{{ datetime_in_view($month->created_at) }}  </td>
                                <td class="text-right">
									<a href="/admin/feedback/report/{{$month->id}}" class="btn btn-success crude-btn btn-sm">View</a>
								</td>
							</tr>
						@endforeach
	            	@else
	            		<tr>
	            			<td colspan="5" class="text-center">No results found.</td>
	            		</tr>
                    @endif
                    </tbody>
            	</table>
				<div class="col-md-12">
                    <div class="pageination pull-right">
                        <nav aria-label="Page navigation">
                              <ul class="pagination">
                                {{ $monthList->render() }}
                              </ul>
                        </nav>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>
</div>
<script>
    $(function(){
        toggle = function (item) {
			$.ajax({
				type:'POST',
				url:'/api/v1/feedback-month/visible',
				data:{id:item.value},
				success:function(data)
                {
					console.log(data);
                },
                error: function(error)
                {
                    console.log(error);
                }
			});
		}
	});
</script>
@endsection
