@extends('layouts.admin-dashboard')
@section('main-content')
<script type="text/javascript">
    window.feedbakUsers = <?php echo json_encode($users); ?>;
    window.feedbackMonth = <?php echo json_encode($monthObj); ?>;
</script>
<div class="container-fluid" ng-app="myApp" ng-controller="feedbackReportCtrl">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Feedback Report</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="/admin/feedback/report">Month List</a></li>
                    <li class="active">Report</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="row bg-white search-section-top m-b feedback-review">
        <div class="col-md-8">
            <h5>Report of the month of {{ date('F', mktime(0, 0, 0, $monthObj->month, 10)) }}, {{$monthObj->year}}</h5>
        </div>
        <div class="col-md-4">
            <button ng-if="!month.locked" class="btn btn-warning crude-btn pull-right" ng-click="regenerateAllRecords()">Regenerate All Records</button>
            <button ng-if="!month.locked" class="btn btn-danger pull-right" ng-click="lockMonth()">Lock Review</button>
            <span class="label label-danger custom-label pull-right" ng-if="month.locked">Review locked</span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="col-sm-12 text-right m-t-10">
                    <div ng-if="loading" class="loader">
                        </div>
            </div>
            <div class="user-list-view">
                <div class="panel panel-default">
                    <table class="table feedback" id="feedback">
                        <thead>
                            <th colspan="2">
                            </th>
                            <th colspan="2">
                            </th>
                        </thead>
                        <tbody ng-repeat="feedbakUser in data">
                            <tr>
                                <td colspan="2">
                                    <h4 class="panel-title">
                                        <a href="/admin/feedback/report/%%month.id%%/%%feedbakUser.user.id%%"><b>%%feedbakUser.user.name%%</b></a>
                                        <button ng-if="!month.locked" class="btn btn-warning btn-xs crude-btn"
                                            ng-click="feedbakUser.regenerateRecords( feedbakUser.user.id, month.id )">
                                            Regenerate Records
                                        </button>
                                    </h4>
                                </td>
                                <td colspan="2">
                                    <div class="pull-right">
                                        <small ng-if="!feedbakUser.sent_all && !feedbakUser.overall_completion && !month.locked">Notify all members on Slack</small>
                                        <button ng-if="!feedbakUser.sent_all && !feedbakUser.overall_completion && !month.locked " class="btn btn-info btn-xs crude-btn"
                                            ng-click="feedbakUser.notifyOnSlack(feedbakUser.id, 'all')">
                                            <i class="fa fa-bell"></i>
                                        </button>
                                        <span class="btn btn-info btn-xs crude-btn" ng-if="feedbakUser.sent_all == true">Slack
                                            notification sent to all</span>
                                    </div>
                                </td>
                            </tr>
                            <tr ng-if="feedbakUser['reviews'].length" ng-repeat="tl in feedbakUser['reviews']">
                                <td width="20%">
                                    <label class="label label-info"><span class="small label-%%tl.role_type.code%%">%%tl.role_type.name%%</span> </label> %%tl.reviewer.name%%
                                    (%%tl.project.project_name%%)
                                </td>
                                <td width="60%">
                                    <div class="progress no-margin">
                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="%%tl.reviewer_percentage%%"
                                            aria-valuemin="0" aria-valuemax="100" style="width: %%tl.reviewer_percentage%%%">
                                            <span class="sr-only">%%tl.reviewer_percentage%%% Complete (success)</span>
                                        </div>
                                    </div>
                                </td>
                                <td width="10%"><span class="badge badge-default">%%tl.reviewer_percentage%%%</span></td>
                                <td width="120px" class="text-right">
                                    <button ng-if="!tl.sent && tl.reviewer_percentage != '100' && !month.locked " class="btn btn-link btn-sm crude-btn"
                                        type="submit" ng-click="feedbakUser.notifyOnSlack(tl.id,'user', 'tl', $index)">
                                        Notify on <i class="fa fa-slack"></i>
                                    </button>
                                    <span class="btn btn-link btn-sm crude-btn" ng-if="tl.sent == true">Slack
                                        notification sent</span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('.feedback').DataTable({

            "columnDefs": [{
                "targets": [1],
                "searchable": true
            }],
        });
    });
    $(function () {
        toggleMonth = function (item) {
            var id = item.value.replace('month', '');
            $.ajax({
                type: 'POST',
                url: '/api/v1/feedback-month/toggle-lock',
                data: {
                    id: id
                },
                success: function (data) {
                    console.log(data.result);
                }
            });
        }
    });
</script>
@endsection