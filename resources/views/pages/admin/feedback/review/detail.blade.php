@extends('layouts.admin-dashboard')
@section('main-content')
<script type="text/javascript">
    window.reviews = <?php echo json_encode($reviews); ?>;
    window.month = <?php echo json_encode($reviews['month']); ?>;
</script>
<style type="text/css">
  .autosave-teaxarea {
    position: relative;
  }
  .autosave-teaxarea .saving, .saved {
    position: absolute;
    right: 0;
    bottom: 0;
    width: 20px;
    height: 20px;
    border-radius: 5px 0 5px 0;
    text-align: center;
    line-height: 20px;
    transition: all 0.2s ease-in-out; 
    -webkit-transition: all 0.2s ease-in-out; 
    -moz-transition: all 0.2s ease-in-out; 
  }

  .autosave-teaxarea .saving {
    color: #f0ad4e;
  }
  .autosave-teaxarea .saved {
    color: #1abc9c;
  }
</style>
<div class="container-fluid resource-report-index" ng-app="myApp" ng-controller="feedbackUserReviewCtrl">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Review for 
                  @if( !empty($forUser->first_name) && !empty($forUser->last_name) ) 
                  <b>{{$forUser->first_name}} {{$forUser->last_name}} </b>
                  @else
                  <b>{{$forUser->name}}</b>
                  @endif
                  <small>({{$monthName}})</small></h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="/admin/feedback/review" >Feedback</a></li>
                    <li><a>Review</a></li>
                </ol>
            </div>
            <div class="col-sm-4" ng-show="month.locked">
              <span class="label label-danger custom-label pull-right" >Review locked</span>
            </div>
            <div class="row">
              <div class="col-sm-12 m-10">
                  <label class="col-sm-1">Progress: </label>
                  <div class="col-sm-6 pull-left">
                      <div class="progress no-margin">
                          <div class="progress-bar progress-bar-success" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: %%percentage%%%">
                              <span>%%percentage%% % Complete (success)</span>
                          </div>
                      </div>
                  </div>
              </div>
            </div>
        </div>
    </div>

    <div class="row">
      <!-- Team Lead Review -->
      <div>
        <div class="col-md-6" ng-repeat="type in reviews">
          <p>Review as <label class="label label-primary" style="text-transform: capitalize">%%type.name%%</label></p>
          <ul class="list-group">

            <!-- Repeat this for Projects -->

            <ng-container ng-repeat="project in data[type.code]">
              <li class="list-group-item" style="background:#D4DCE6"><b>Project: %%project.project_name%%</b></li>
              
              <!-- Repeat this for specific projects questions -->
              <li class="list-group-item" ng-repeat="review in project.project_reviews">
                <p>%%review.question%%</p>
                <div class="m-b row">
                  <div ng-hide="review.status == 'not_applicable' || month.locked == '1'" class="col-sm-7" star-rating rating="review.rating" read-only="false" max-rating="10" click="review.saveRating(param)" mouse-hover="mouseHover1(param)" mouse-leave="mouseLeave1(param)"></div>
                  <div ng-show="review.status == 'not_applicable' || month.locked == '1'" class="col-sm-7" star-rating rating="review.rating" read-only="true" max-rating="10" click="review.saveRating(param)" mouse-hover="mouseHover1(param)" mouse-leave="mouseLeave1(param)"></div>
                  <span class="col-sm-1" ng-show="review.ratingLoading" class="saving"><i class="fa fa-spin fa-spinner"></i></span>
                  <span class="col-sm-1"></span>
                  <div class="col-sm-4 text-right">
                    <button ng-disabled="review.status == 'not_applicable' || month.locked == '1' " class="btn btn-success btn-sm" role="button" data-toggle="toggle" ng-click="review.updateNA(review)">NA</button>
                    <span ng-show="review.naLoading" class="saving"><i class="fa fa-spin fa-spinner"></i></span>
                    <button ng-disabled="review.status == 'not_applicable' || month.locked == '1'" class="btn btn-default btn-sm" role="button" data-toggle="collapse" href="#add-comment-tl%%review.id%%" aria-expanded="false" aria-controls="add-comment-tl%%review.id%%">Add Comment</button>
                  </div>
                </div>
       <!--         <div ng-class="{'collapse autosave-teaxarea': true, ' in': review.comments}" id="add-comment-tl%%review.id%%"  ng-model-options="{ allowInvalid: true, debounce: 2000 }">
                  <label>Please write your review.</label>
                  <textarea ng-disabled="review.status == 'not_applicable' || month.locked == '1'" ng-model="review.comments" ng-change="review.saveComment()" class="form-control m-b-10" id="exampleFormControlTextarea1" rows="3" col="4"></textarea>
                  <span ng-show="review.commentLoading" class="saving"><i class="fa fa-spin fa-spinner"></i></span>
                  <span ng-show="!review.commentLoading && review.comments" class="saved"><i class="fa fa-check"></i></span>
                </div> -->
                <!-- Conditional Div -->
                <div ng-model-options="{ allowInvalid: true, debounce: 2000 }" class="autosave-teaxarea" ng-if="(review.rating >0) && (review.rating < 5) || (review.rating ==10) ">
                  <label ng-if="(review.rating >0) && (review.rating < 5)">Improvements / What when wrong*</label>
                  <label ng-if="(review.rating >0) && (review.rating ==10)">Appreciation Message*</label>
                  <textarea ng-disabled="review.status == 'not_applicable' || month.locked == '1'" ng-model="review.rating_comments" ng-change="review.saveRatingComment()" class="form-control" id="exampleFormControlTextarea1" rows="3" col="4"></textarea>
                  <span class="saving" ng-show="review.ratingCommentLoading"><i class="fa fa-spin fa-spinner"></i></span>
                  <span class="saved" ng-show="!review.ratingCommentLoading && review.rating_comments"><i class="fa fa-check"></i></span>
                  <div class="col-sm-6" >
                      <input
                              type="checkbox"
                              ng-model="review.is_private"
                              ng-checked="review.is_private || review.is_private == 'true'"
                          >
                  <label class="col-sm-6">Is Private</label>
                  </div>
                </div>
                <div class="row" style="margin-top: 10px">
                    <comment-data commentable-id="review.id" commentable-type="App\Models\Admin\FeedbackUser" is-disabled="false" is-privatable="true"></comment-data>
                </div>
                
              </li>
              
            </ng-container>
          </ul>
        </div>
      </div> 
    </div>
    @endsection