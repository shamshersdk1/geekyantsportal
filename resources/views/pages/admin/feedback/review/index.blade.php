@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid resource-report-index">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Feedback</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a>Feedback Review</a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 bg-white search-section-top">
        @if ( count($feedbackMonths) > 0 )
        <div class="form-group">
            <label class="col-sm-2 control-label">Month: </label>
            <div class="col-sm-12">
                <select id="selectid3" name="month" style="width=35%;" placeholder= "Select an option">
                    <option></option>
                    @foreach($feedbackMonths as $month)
                        <option value="{{$month->id}}">{{$month->month}}, {{$month->year}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        @endif
        </div>
        <div class="col-md-12">
            <h4>List of My Users</h4>
            <div class="user-list-view">
                <div class="panel panel-default">
                    <table class="table table-striped">
                        <tr>
                            <th width="5%" class="sorting_asc">#</th>
                            <th width="40%">Name</th>
                            <th width="40%">Status</th>
                            <th class="text-right sorting_asc" width="30%">Actions</th>
                        </tr>
                        @if(count($reporteeList) > 0)
                        @foreach($reporteeList as $reportee)
                        <tr>
                            <td class="td-text"><a>{{$reportee->id}}</a></td>
                            <td class="td-text">{{$reportee->name}}</td>
                            <td class="text-right">
                                <a id="{{$reportee->id}}" class="btn btn-success crude-btn btn-sm">Review</a>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="3" class="text-center">No results found.</td>
                        </tr>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function(){
            $(".btn-success").click(function(event){
                var selected = $( "#selectid3" ).val();
                var url = '/admin/review-rm/'+this.id+'/'+selected;
                window.location.href = url;
            }); 
        });
    </script>

    @endsection