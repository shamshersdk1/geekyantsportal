@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Feedback My Review</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
		  			<li class="active">Month List</li>
        		</ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
	    	@if(!empty($errors->all()))
	            <div class="alert alert-danger">
	                @foreach ($errors->all() as $error)
	                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                    <span>{{ $error }}</span><br/>
	                  @endforeach
	            </div>
	        @endif
	        @if (session('message'))
	            <div class="alert alert-success">
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ session('message') }}</span><br/>
	            </div>
			@endif
		</div>
		<div class="user-list-view">
            <div class="panel panel-default">
                <div class="table-responisve">
                    <table id="a" class="table table-striped">
                        <thead>
                            <tr>
                                <th width="5%" class="sorting_asc">#</th>
                                <th width="30%">Month</th>
                                <th width="25%">Status</th>
                                <th width="20%">Created Date</th>
                                <th class="text-right" width="20%">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if(count($monthList) > 0)
                            @foreach($monthList as $index => $month)
                                <tr>
                                    <td class="td-text"><a>{{$month->id}}</a></td>
                                    <td class="td-text">{{ date('F', mktime(0, 0, 0, $month->month, 10)) }}, {{$month->year}}  </td>
                                    @if( !$month->locked ) 
                                        <td><span class="label label-primary custom-label">Open</span></td>
                                    @else
                                        <td><span class="label label-danger custom-label">Locked</span></td>
                                    @endif
                                    <td class="td-text">{{ datetime_in_view($month->created_at) }}  </td>
                                    <td class="text-right">
                                        <a href="/admin/feedback/my-review/{{$month->id}}" class="btn btn-success crude-btn btn-sm">View</a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="5" class="text-center">No results found.</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
				<div class="col-md-12">
                    <div class="pageination pull-right">
                        <nav aria-label="Page navigation">
                              <ul class="pagination">
                                {{ $monthList->render() }}
                              </ul>
                        </nav>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>
</div>
@endsection
