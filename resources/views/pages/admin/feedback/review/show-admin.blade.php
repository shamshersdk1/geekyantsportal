@extends('layouts.admin-dashboard')
@section('main-content')
<script type="text/javascript">
  window.reviews = <?php echo json_encode($reviews); ?>;
  window.month = <?php echo json_encode($month); ?>;
</script>
<style type="text/css">
  .autosave-teaxarea {
    position: relative;
  }

  .autosave-teaxarea .saving,
  .saved {
    position: absolute;
    right: 0;
    bottom: 0;
    width: 20px;
    height: 20px;
    border-radius: 5px 0 5px 0;
    text-align: center;
    line-height: 20px;
    transition: all 0.2s ease-in-out;
    -webkit-transition: all 0.2s ease-in-out;
    -moz-transition: all 0.2s ease-in-out;
  }

  .autosave-teaxarea .saving {
    color: #f0ad4e;
  }

  .autosave-teaxarea .saved {
    color: #1abc9c;
  }
</style>
<div class="container-fluid resource-report-index" ng-app="myApp" ng-controller="adminFeedbackReviewCtrl">
  <div class="breadcrumb-wrap">
    <div class="row">
      <div class="col-sm-8">
        <h1 class="admin-page-title">Review for
          @if( !empty($forUser->first_name) && !empty($forUser->last_name) )
         {{$forUser->first_name}} {{$forUser->last_name}} 
          @else
          {{$forUser->name}}
          @endif
          <small>({{$monthName}})</small></h1>
        <ol class="breadcrumb">
          <li><a href="/admin">Admin</a></li>
          <li><a href="/admin/feedback/report">Month List</a></li>
          <li><a href="/admin/feedback/report/{{$month->id}}">Reports</a></li>
          <li><a>{{$forUser->name}}</a></li>
        </ol>
      </div>
      <div class="col-sm-4" ng-show="month.locked">
        <span class="label label-danger custom-label pull-right">Review locked</span>
      </div>
    </div>
  </div>

    <!-- Team Lead Review -->
    <div class="panel panel-default">
      <ul class="list-group project-rating">
        <ng-container ng-repeat="row in reviews">
          <li class="list-group-item" style="background:#D4DCE6"><b>Project: %%row.project.project_name%%</b></li>
          <div ng-repeat="reviewer in row.project.reviewer">
              <li class="list-group-item"><b>Reviewer: %%reviewer.name%%</b> </li>
              <li class="list-group-item" ng-repeat="project_review in reviewer.project_reviews">
                  <p>%%project_review.question%%<span style="font-size: 11px">( as <span class="small label-%%project_review.role_type.code%%">%%project_review.role_type.name%%</span> )</span></p>
                  <div class="project-rating-star" star-rating rating="project_review.rating" read-only="true" max-rating="10"></div>

                  
                    <comment-data commentable-id="project_review.id" commentable-type="App\Models\Admin\FeedbackUser" is-disabled="true"
                      is-private="true"></comment-data>
                </li>
          </div>
        </ng-container>
      </ul>

  </div>
  @endsection