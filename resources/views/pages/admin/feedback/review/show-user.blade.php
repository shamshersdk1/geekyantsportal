@extends('layouts.admin-dashboard')
@section('main-content')
<script type="text/javascript">
  window.reviews = <?php echo json_encode($reviews); ?>;
  window.month = <?php echo json_encode($month); ?>;
</script>
<div class="container-fluid resource-report-index" ng-app="myApp" ng-controller="userFeedbackReviewCtrl">
  <div class="breadcrumb-wrap">
    <div class="row">
      <div class="col-sm-8">
        <h1 class="admin-page-title">Review for
          @if( !empty($forUser->first_name) && !empty($forUser->last_name) )
          <b>{{$forUser->first_name}} {{$forUser->last_name}} </b>
          @else
          <b>{{$forUser->name}}</b>
          @endif
          <small>({{$monthName}})</small></h1>
        <ol class="breadcrumb">
          <li><a href="/admin">Admin</a></li>
          <li><a href="/admin/feedback/my-review">My Reviews</a></li>
          <li><a>{{$monthName}}</a></li>
          <li><a>{{$forUser->name}}</a></li>
        </ol>
      </div>
      <div class="col-sm-4" ng-show="month.locked">
        <span class="label label-danger custom-label pull-right">Review locked</span>
      </div>
    </div>
  </div>

  <div class="row">
    <!-- Team Lead Review -->
    <div>
      <ul class="list-group">
        <li ng-if="reviews.length == 0" class="list-group-item">
            No records for this month
        </li>
        <ng-container ng-repeat="row in reviews">
          <li class="list-group-item" style="background:#D4DCE6"><b>Reviewer: %%row.reviewer.name%%</b></li>

          <!-- Repeat this for specific projects questions -->
          <li class="list-group-item" ng-repeat="review in row.reviewer.project_reviews">
            <p>%%review.question%%<span style="font-size: 11px">( %%review.project.project_name%% as <span class="small label-%%review.role_type.code%%">%%review.role_type.name%%</span> )</span></p>
            <div class="m-b row">
              <div class="col-sm-7" star-rating rating="review.rating" read-only="true" max-rating="10"></div>
            <!-- Conditional Div -->
            <div class="row">
              <comment-data commentable-id="review.id" commentable-type="App\Models\Admin\FeedbackUser" is-disabled="true"
                is-private="true"></comment-data>
            </div>
          </li>
        </ng-container>
      </ul>
    </div>
  </div>
  @endsection