@extends('layouts.admin-dashboard')
@section('main-content')
	<section class="sprint-dashboard">
		<div class="container-fluid">
		    <div class="breadcrumb-wrap">
		    	<div class="row">
		            <div class="col-sm-8">
		                <h1 class="admin-page-title">Sprints Dashboard</h1>
		                <ol class="breadcrumb">
		        		  	<li><a href="/admin">Admin</a></li>
				  			<li class="active">Sprint Dashboard</li>
		        		</ol>
		            </div>
		            <div class="col-sm-4 text-right m-t-10">
		                <!-- <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button> -->
		            </div>
		        </div>
		    </div>
		    <div class="row">
                <form action="/admin/project/show-sprint-dashboard" method="get">
    				<div class="col-md-4">
    					<div class="form-group">
                            <select class="form-control" name="projectId" selected = "3">
                            	<option value="" >Select</option>
                                @if(count($projects)>0)
                                    @foreach($projects as $project)

            						  	<option value={{$project->id}}>{{$project->project_name}}</option>
            						  	
                                    @endforeach
                                @endif
    						</select>
                        </div>
                    </div>
                    <div class="col-md-2  no-padding">
                        <button type="submit" class="btn btn-success" >Search </button>
                    </div>

                 </form> 
			</div>
			<div class="panel-group">
				@if(isset($projectObj))
					@foreach($projectObj as $projectDetails)
						@if(isset($projectDetails))
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headingOne">
									<h3 class="panel-title bold">
										<a role="button" data-toggle="collapse" href="#{{$projectDetails->project_name}}">{{$projectDetails->project_name}}</a>
									</h3>
								</div>
								<div id="#{{$projectDetails->project_name}}" class="panel-collapse collapse in">
									<div class="">
					                    <table class="table table-striped no-margin table-sm-heading">
		            						<tr>
						            			<th width="8%">Sprint No.</th>
						                    	<th width="16%">Sprint Title</th>
						                    	<th width="10%">Start Date</th>
						                    	<th width="10%">End Date</th>
						                    	<th width="20%">Allocated Resource</th>
						                    	<th width="8%">Allocated Hrs</th>
						                    	<th width="8%">Consume Hrs</th>
						                    	<th width="8%">Additional Hrs</th>
						                    </tr>
						                    @if(count($projectDetails->projectSprints)==0)
						                    		<tr>
						                    			<td colspan="8">
						                    				No record found
					                    				</td>
					                    			</tr>
				                    		@else

							                    @foreach($projectDetails->projectSprints as $sprint)

													@if(isset($sprint))
														<tr>
									            			<td>{{$sprint->id}}</td>
									            			<td>{{$sprint->title}}</td>
									            			<td>{{$sprint->start_date}}</td>
									            			<td>{{$sprint->end_date}}</td>
									            			<td>
									            				@if(isset($sprint->sprintResource))
																	@foreach($sprint->sprintResource as $resource)
																		@if(isset($resource->user))
																			<img src="http://ingridwu.dmmdmcfatter.com/wp-content/uploads/2015/01/placeholder.png" title="{{$resource->user->name}}" class="img-responsive" style="width: 20px; height: 20px; border: 2px solid #aaa; margin-right: 3px; margin-bottom: 3px; display:inline-block;">		
																		@endif 
																	@endforeach
																@endif
									            			</td>
									            			<td>
									            				@if(isset($allocatedHours[$sprint->id]))
															  		{{$allocatedHours[$sprint->id]}}
														  		@else
													  				0	
														  		@endif
									            			</td>
									            			<td>
									            				@if(isset($consumeHours[$sprint->id]))
																  	{{$consumeHours[$sprint->id]}}
													  			@else
																	0
														  		@endif
									            			</td>
									            			<td>
									            				@if(isset($additionalHours[$sprint->id]))
																  	{{$additionalHours[$sprint->id]}}
													  			@else
																	0
														  		@endif
									            				
									            			</td>
									            		</tr>
								            		@else
								            			<tr>
								            				<td colspan="7">
								            					<h4>No Sprint available</h4>
								            				</td>
								            			</tr>
								            		@endif
												@endforeach
											@endif	
										</table>
									</div>
								</div>
							</div>
						@endif
					@endforeach
				@endif
            </div>
		</div>
	</section>
@endsection