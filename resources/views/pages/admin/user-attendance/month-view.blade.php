@extends('layouts.admin-dashboard')
@section('page_heading','Attendance-overview')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Attendance Register</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li class="active">Attendance Register</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-8">
            <h4 class="m-b-10 m-t-10">{{$monthObj->formatMonth()}}</h4>
        </div>
        <div class="col-sm-4">
            <div>
                {{Form::open(['url' => '', 'id' => 'formId']) }}
                {{Form::token();}}
                    <div class="form-group m-b-10">
                        <div class="bg-white" style="width:100%">
                            @if(isset($monthArray))
                                @if(!$monthObj)
                                    {{Form::select('month_id', $monthArray, null, ['id'=>'selectid2', 'placeholder'=>'Select month', 'style' => 'width:35%', 'class' => ''])}}
                                @else 
                                    {{Form::select('month_id', $monthArray, $monthObj->id, ['id'=>'selectid2', 'placeholder'=>'Select month', 'style' => 'width:35%'])}}
                                @endif
                            @endif
                        </div>
                    </div>
                {{ Form::close() }}
            </div> 
        </div>
    </div> 

    <div class="">
        @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
        @endif
        @if(session()->has('error'))
        <div class="alert alert-error">
            {{ session()->get('error') }}
        </div>
        @endif
    </div>
    <div class="bg-white">
        <table class="table table-striped bg-white" id="monthly-attendance-list">
            <thead>
                <tr>
                    <th width="15%">Date</td>
                    <th width="20%" class="text-center">Present in Office Count</th>
                    <th width="15%" class="text-center">Onsite Count</th>
                    <th width="15%" class="text-center">On Leave Count</th>
                    <th width="15%" class="text-center">WFH Count</th>
                    <th width="20%" class="text-right">View Attendance</th>
                </tr>
            </thead>
            <tbody>
                @if($attendanceObj)
                    @foreach($attendanceObj as $row)
                        <tr>
                            <td>{{ date_in_view($row['date']) }} </td>
                            <td class="text-center">{{$row['no_of_present']}}</td>
                            <td class="text-center">{{$row['no_of_onsite']}}</td>
                            <td class="text-center">{{$row['no_of_leave']}}</td>
                            <td class="text-center">{{$row['no_of_wfh']}}</td>
                            <td class="text-right">
                                {{Form::open(['url' => '/admin/attendance-register/'. $monthObj->id. '/date', 'method' => 'post'])}}
                                    {{Form::token();}}
                                    {{Form::hidden('date', $row['date']) }}
                                	{{Form::button('<i class="fa fa-file fa-fw btn-icon-space"></i>  View Attendance', array('type' => 'submit', 'class' => 'btn btn-success btn-sm'));}}
                                {{Form::close()}}
                            </td>
                        </tr>
                    @endforeach
                @else
                <td colspan="6"> 
                    <div class="text-center">No Record Found</div>
                </td>
                @endif
            </tbody>
        </table>
    </div>
    <script>
        $('#selectid2').change(function(){
		var optionSelected = $("option:selected", this);
        optionValue = this.value;
        if (optionValue) { 
            window.location = "/admin/attendance-register/"+optionValue; 
        }
    });
    </script>
@endsection