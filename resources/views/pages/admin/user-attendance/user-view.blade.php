@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
  <div class="breadcrumb-wrap">
    <div class="row">
      <div class="col-sm-8">
        <h1 class="admin-page-title">Attendance Register</h1>
        <ol class="breadcrumb">
          <li><a href="/admin">Admin</a></li>
          <li><a href="/admin/users">List of Users</a></li>
          <li class="active">Attendance Register for {{$userObj->name}}</li>
        </ol>
      </div>
    </div>
  </div>
  
  <div class="m-t-15">
    @if(session()->has('message'))
      <div class="alert alert-success">
          {{ session()->get('message') }}
      </div>
    @endif
    @if(session()->has('error'))
      <div class="alert alert-error">
          {{ session()->get('error') }}
      </div>
    @endif
    
      <div class="row">
        <div class="col-sm-6 m-b">
          <h4>Attendance Summary of {{$userObj->name}} </h4>
             {{Form::open(['url' => '','id' => 'formId']) }}
          {{Form::token();}}
              <div class="form-group m-b-5 m-t-5">
                  <div style="background: white; width: 50%;">
                      @if(isset($monthArray))
                        @if(!$monthObj)
                            {{Form::select('month_id', $monthArray, null, ['id'=>'selectid2', 'placeholder'=>'Select month', 'style' => 'width:35%', 'class' => ''])}}
                        @else 
                            {{Form::select('month_id', $monthArray, $monthObj->id, ['id'=>'selectid2', 'placeholder'=>'Select month', 'style' => 'width:35%'])}}
                        @endif
                      @else
                        No Month Found
                      @endif
                  </div>
              </div>
          {{ Form::close() }}
        </div>
        <div class="col-sm-6">
          <div class="leave-number m-t-20">
              <div><small>Avg. In Time</small><h4>{{$avgInTime ? $avgInTime : 0}}</h4></div>
              <div><small>Avg. Hour Spent</small>
                <h4>
                  <?php
                  if(!is_nan($avgHoursSpent)) {
                      $minutes = $avgHoursSpent;
                      $hours = floor($minutes / 60).' Hour(s) '.($minutes -   floor($minutes / 60) * 60)." Min(s)";
                      echo $hours;
                  }
                  else {
                    echo 0;
                  }
                  ?>
                </h4>
              </div>
            </div>


        </div>
      </div>
      
    <div>
      <div class="panel panel-default">
        <table class="table bg-white" id="dt-attendance-list">
          <thead>
            <tr>
              <th width="25%">Date</td>
              <th width="25%">Timing</th>
              <th width="25%">Duration</th>
              <th width="25%" class="text-right">Status</th>
            </tr>
          </thead>
          <tbody>
            @if($attendanceObj) 
              @foreach($attendanceObj as $row)
                <tr class="{{$row['status'] == 'on_leave' ? 'warning' : ($row['status'] == 'present_onsite' ? 'info': ($row['status'] == 'present_wfh' ? 'default' : ($row['status'] == 'present_on_leave' ? 'danger' : 'default')))}}">
                  <td>{{date_in_view($row['date'])}}</td>
                  <td>
                    <strong><small>In Time:</small> </strong>{{datetime_in_view($row['in_time']) ?? null}}<br/>
                    <strong><small>Out Time:</small> </strong>{{datetime_in_view($row['out_time'] ?? null)}}
                  </td>
                  <td>
                    <strong><small>Active:</small> </strong>
                    <?php
                    if($row['active_count'] > 0) {
                        $minutes = $row['active_count'];
                        $hours = floor($minutes / 60).' Hour(s) '.($minutes -   floor($minutes / 60) * 60)." Min(s)";
                        echo $hours;
                    }
                    else {
                        echo 0;
                    }
                    ?>
                    <br/>
                    <strong><small>Total:</small></strong>
                      <?php
                      if($row['tick_count'] > 0) {
                          $minutes = $row['tick_count'];
                          $hours = floor($minutes / 60).' Hour(s) '.($minutes -   floor($minutes / 60) * 60)." Min(s)";
                          echo $hours;
                      }
                      else {
                          echo 0;
                      }
                      ?>
                  </td>
                  <td class="text-right">
                    @if($row['status'] == 'on_leave')
                        On Leave
                    @elseif($row['status'] == 'present_onsite')
                        Present Onsite
                    @elseif($row['status'] == 'present_wfh')
                        Work From Home
                    @elseif($row['status'] == 'present_in_office')
                        Present In Office
                    @elseif($row['status'] == 'present_meeting')
                        Present In Meeting
                    @elseif($row['status'] == 'present_on_leave')
                        Present On Leave
                    @elseif($row['status'] != null)
                      {{$row['status']}}
                    @else
                      Unknown
                    @endif
                  </td>
                </tr>
              @endforeach
            @else 
            <td>
              <div>No Record Found</div>
            </td>
            @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<script>
        $('#selectid2').change(function(){
		var optionSelected = $("option:selected", this);
        optionValue = this.value;
        if (optionValue) { 
            window.location = "/admin/users/{{$userObj->id}}/attendance-register/"+optionValue; 
        }
    });
    </script>
@endsection