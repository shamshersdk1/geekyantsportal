@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
  <div class="breadcrumb-wrap">
    <div class="row">
      <div class="col-sm-8">
        <h1 class="admin-page-title">Attendance Register</h1>
        <ol class="breadcrumb">
          <li><a href="/admin">Admin</a></li>
          <li><a href="/admin/attendance-register/{{$monthObj->id}}">Attendance Register</a></li>
          <li class="active">{{date_in_view($date)}}</li>
        </ol>
      </div>
    </div>
  </div>
  <div class="row">
    
  </div>
  <div class="m-t-15">
    @if(session()->has('message'))
      <div class="alert alert-success">
          {{ session()->get('message') }}
      </div>
    @endif
    @if(session()->has('error'))
      <div class="alert alert-error">
          {{ session()->get('error') }}
      </div>
    @endif
    <h4>Attendance Summary Report</h4>
    <div>
      <div class="panel panel-default">
        <table class="table bg-white" id="dt-attendance-list">
          <thead>
            <tr>
              <th width="20%">Employee</th>
              <th width="30%">Timing</th>
              <th width="30%">Duration</th>
              <th width="20%" class="text-right">Status</th>
            </tr>
          </thead>
          <tbody>
            @if($userRegisterObj) 
              @foreach($userRegisterObj as $row)
                <tr class="{{$row['status'] == 'on_leave' ? 'warning' : ($row['status'] == 'present_onsite' ? 'info': ($row['status'] == 'present_wfh' ? 'default' : ($row['status'] == 'present_on_leave' ? 'danger' : 'default')))}}">
                  <td>{{$row->user->employee_id}}<br />{{$row->user->name}}</td>
                  <td>
                    <strong><small>In Time:</small> </strong> {{datetime_in_view($row->in_time) ?? null}} 
                    <br/>
                    <strong><small>Out Time:</small> </strong> {{datetime_in_view($row->out_time) ?? null}}</td>
                  <td>
                    <strong><small>Active:</small> </strong>
                      <?php
                        if($row['active_count'] > 0) {
                            $minutes = $row['active_count'];
                            $hours = floor($minutes / 60).' Hour(s) '.($minutes -   floor($minutes / 60) * 60)." Min(s)";
                            echo $hours;
                        }
                        else {
                            echo 0;
                        }
                      ?>
                      <br/>
                    <strong><small>Total:</small> </strong>
                      <?php 
                      if($row['tick_count'] > 0) {
                          $minutes = $row['tick_count'];
                          $hours = floor($minutes / 60).' Hour(s) '.($minutes -   floor($minutes / 60) * 60)." Min(s)";
                          echo $hours;
                      }
                      else {
                          echo 0;
                      }
                      ?>
                  </td>
                  <td class="text-right">
                    @if($row->status == 'on_leave')
                      On Leave
                    @elseif($row->status == 'present_onsite')
                      Present Onsite
                    @elseif($row->status == 'present_wfh')
                      Work From Home
                    @elseif($row->status == 'present_in_office')
                      Present In office
                    @elseif($row->status == 'present_meeting')
                      Present In Meeting
                    @elseif($row->status == 'present_on_leave')
                      Present On Leave
                    @elseif($row->status != null)
                      {{$row->status}}
                    @else
                      Unknown
                    @endif
                  </td>
                </tr>
              @endforeach
            @else 
                <div>
                  No record found
                </div>
            @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection