@extends('layouts.admin-dashboard')
@section('page_heading','Attendance-overview')
@section('main-content')
<div class="container-fluid">
<style>


div.google-visualization-tooltip {
	margin-top: -60px;
	width: 100px;
	height: 48px;
	font-weight: bold;
	font-size: 14px !important;
	display: block !important
}

.google-visualization-tooltip-item-list {
	list-style-type: none;
	margin: 0;
	padding: 0em;
}

.google-visualization-tooltip-item-list .google-visualization-tooltip-item:first-child {
	margin: 0;
	font-weight: bold;
}

.google-visualization-tooltip-item-list .google-visualization-tooltip-item:first-child span {
	font-family: Arial;
	font-size: 12px !important;
	color: rgb(0, 0, 0);
	opacity: 1;
	margin: 0px;
  text-decoration: none;
  text-align:left !important;
}

.google-visualization-tooltip-item {
	margin: 0 0px;
  padding: 0 10px;
  float: left;
}

.google-visualization-tooltip-item span {
	font-size: 12px !important;
}


</style>

  <div class="breadcrumb-wrap">
    <div class="row">
      <div class="col-sm-8">
        <h1 class="admin-page-title">Attendance Overview</h1>
        <ol class="breadcrumb">
          <li><a href="/admin">Admin</a></li>
          <li class="active">Attendance Overview for {{$date}}</li>
        </ol>
      </div>
    </div>
  </div>
  <div class="m-t-15">
    @if(session()->has('message'))
      <div class="alert alert-success">
          {{ session()->get('message') }}
      </div>
    @endif
    @if(session()->has('error'))
      <div class="alert alert-error">
          {{ session()->get('error') }}
      </div>
    @endif
    <div class="row">
      <div class="col-sm-6">
        <h4>Attendance Summary Report</h4>
      </div>
      <div class="col-sm-6"></div>
    </div>

    <div>
      <div class="panel panel-default">
        <table class="table" id="dt-attendance-list" >
          <thead>
            <tr>
              <th width="5%">Employee ID</th>
              <th width="20%">Employee Details</td>
              <th width="10%">Incoming Time</th>
              <th width="20%">Primary Asset</th>
              <th width="20%">Additional Assets</th>
              <th width="25%" class="text-right">Active / Tick Counts</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($active_users as $row)

              @if(!$row['UserAttendance']->user) 
                @continue; 
              @endif

              @if($row['UserAttendance']->is_onsite || $row['UserAttendance']->is_wfh || $row['UserAttendance']->is_onsite_meeting)
                <!-- Single user -->
                <tr>
                  <td>{{$row['UserAttendance']->user->employee_id}}</td>
                  <td>{{$row['UserAttendance']->user->name}}</td>
                  <td>
                    @if($row['UserAttendance']->is_onsite)
                      <label class="label custom-label label-info">Working onsite location</label><br />
                    @endif
                    @if($row['UserAttendance']->is_wfh)
                      <label class="label custom-label label-primary">Work from home</label><br />
                    @endif
                    @if($row['UserAttendance']->is_onsite_meeting)
                      <label class="label custom-label label-warning">Out of office for Meeting</label><br />
                    @endif
                  </td>
                  <td>-</td>
                  <td>-</td>
                  <td class="text-right">-</td>
                </tr>
                <!-- END Single user -->
              @else

                <!-- Single user -->
                @if(strtotime($row['UserAttendance']->in_time) < strtotime($date." 08:00:00"))
                  <tr class="danger">
                @elseif($row['is_on_leave'])
                  <tr class="warning">
                @else
                  <tr>
                @endif
                    <td>{{$row['UserAttendance']->user->employee_id}}</td>
                    <td>{{$row['UserAttendance']->user->name}}</td>
                    <td>
                      <i class="fa fa-clock-o"></i> {{substr($row['UserAttendance']->in_time,11)}}
                    </td>
                    <td style="vertical-align: middle;">
                      @foreach($row['PrimaryAssets'] as $asset)
                        <i class="fa fa-laptop"></i> <a href="{{$asset->getURI()}}">{{$asset->name}}</a><br /><small>{{$row['Location']}}<br />
                        <?php
                        $minutes = $row['UserAttendance']->active_count;
                        $hours = floor($minutes / 60).' Hour(s) '.($minutes -   floor($minutes / 60) * 60)." Min(s)";
                        echo $hours;
                        ?>
                        </small>
                      @endforeach
                    </td>
                    <td>
                      <span style="font-size: 16px;">
                      @foreach($row['OtherAssets'] as $asset)
                        @if($asset->asset_category_id && $asset->asset_category_id==1)
                          <a href="{{$asset->getURI()}}"><i class="fa fa-laptop"></i></a>
                        @elseif($asset->asset_category_id && $assset->asset_category_id==3)
                          <a href="{{$asset->getURI()}}"><i class="fa fa-mobile"></i></a>
                        @else
                          <small>{{$asset->name}}</small>
                        @endif
                      @endforeach
                      </span>
                    </td>
                    <td class="text-right">{{$row['UserAttendance']->active_count}} / {{$row['UserAttendance']->tick_count}}</td>
                </tr>
                <!--End  Single user -->
              @endif
            @endforeach

             @foreach ($non_active_users as $user)
              <!-- Single user -->
                <tr style="background-color: #f4f4f4;">
                  <td>{{$user->employee_id}}</td>
                  <td>{{$user->name}}</td>
                  <td><label class="label label-warning custom-label">Unknow</label></td>
                  <td style="vertical-align: middle;">-</td>
                  <td>-</td>
                  <td class="text-right">
                    <!-- <small>Mark as</small><br /> -->
                    <form method='post' style="display: inline-block;" action="/admin/attendance-update" />
                      <input type="hidden" name="user_id" value="{{$user->id}}" />
                      <input type="hidden" name="date" value="{{$date}}" />
                      <input type="hidden" name="markas" value="is_wfh" />
                      <input type="submit" value="Work from home" class="btn-sm btn btn-primary" />
                    </form>
                    <form method='post' style="display: inline-block;" action="/admin/attendance-update" />
                      <input type="hidden" name="user_id" value="{{$user->id}}" />
                      <input type="hidden" name="date" value="{{$date}}" />
                      <input type="hidden" name="markas" value="is_onsite" />
                      <input type="submit" value="Onsite" class="btn-sm btn btn-info" />
                    </form>
                    <form method='post' style="display: inline-block;" action="/admin/attendance-update" />
                      <input type="hidden" name="user_id" value="{{$user->id}}" />
                      <input type="hidden" name="date" value="{{$date}}" />
                      <input type="hidden" name="markas" value="is_onsite_meeting" />
                      <input type="submit" value="Onsite Meeting" class="btn-sm btn btn-warning" />
                    </form>
                  </div>
                </td>
              </tr>
              <!--End  Single user -->
            @endforeach

            @foreach ($users_on_leave as $leave)
              <!-- Single user -->
                <tr class="info">
                  <td>{{$leave->user->employee_id}}</td>
                  <td>{{$leave->user->name}}</td>
                  <td><label class="label label-danger">Leave: {{$leave->leaveType->title}}</label></td>
                  <td style="vertical-align: middle;">-</td>
                  <td>-</td>
                  <td class="text-right">-</td>
              </tr>
              <!--End  Single user -->
            @endforeach

            </tbody>
         <tfoot>
            <tr>
              <th width="5%">Employee ID</th>
              <th width="20%">Employee Details</td>
              <th width="10%">Incoming Time</th>
              <th width="30%">Primary Asset</th>
              <th width="20%"> Additional Assets</th>
              <th width="25%" class="text-right">Active / Tick Counts</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>

<?php
/*
           
           
            <!-- Single user -->
              <tr class="danger">
              <td>Megha Kumari {{dump($user)}}</td>
              <td><i class="fa fa-clock-o"></i> 12:15 am</td>
              <td style="vertical-align: middle;">
                <i class="fa fa-laptop"></i> <a href="">Macbook Pro</a><br /><small>GeekyAnts S+ 2nd floor router 2</small>
              </td>
              <td>
                <span style="font-size: 20px; ">
                  <a href="" data-toggle="tooltip" data-placement="top"><i class="fa fa-laptop"></i></a>
                  <a href="" data-toggle="tooltip" data-placement="top"><i class="fa fa-mobile"></i></a>
                  <a href="" data-toggle="tooltip" data-placement="top"><i class="fa fa-tablet"></i></a>
                </span>
              </td>
              <td class="text-right"></td>
              <td class="text-right"><a href="/admin/attendance-overview-month" class="btn btn-info btn-xs"><i class="fa fa-file-text-o"></i></a></td>
              </tr>
              <tr class="danger">
              <td colspan="6" style="border-top:none;">
                <div id="chartcontainer2" style="height:20px;"></div>
              </td>
              </tr>

              <tr>
                <td>Varun Kumar Sahu</td>
                <td><i class="fa fa-clock-o"></i> 11:45 am</td>
                <td style="vertical-align: middle;">
                  <i class="fa fa-laptop"></i> <a href="">Macbook Pro</a><br /><small>GeekyAnts S+ 2nd floor router 2</small>
                </td>
                <td style="font-size: 20px; vertical-align: middle;">
                  <a href="" data-toggle="tooltip" data-placement="top"><i class="fa fa-laptop"></i></a>
                  <a href="" data-toggle="tooltip" data-placement="top"><i class="fa fa-mobile"></i></a>
                </td>
                <td class="text-right">    
                  <div id="piechart" style="width: 90px; height: 40px; float:right; margin:-8px -15px -17px;"></div>
                </td>
              <td class="text-right"><a href="/admin/attendance-overview-month" class="btn btn-info btn-xs"><i class="fa fa-file-text-o"></i></a></td>
              </tr>
              <tr>
              <td colspan="6" style="border-top:none;">
                <div id="chartcontainer" style="height:20px;"></div>
              </td>
              </tr>
              <!--End  Single user -->
          <tr class="update warning">
           <td>Shrutika Swaraj</td>
           <td><i class="fa fa-clock-o"></i> 10:32 am</td>
           <td style="vertical-align: middle;">
                <i class="fa fa-laptop"></i> <a href="">Macbook Pro</a><br /><small>GeekyAnts S+ 2nd floor router 2</small>
              </td>
            <td style="font-size: 20px; vertical-align: middle;">
              
            </td>
           <td class="text-right">    
                <div id="piechart1" style="width: 90px; height: 40px; float:right; margin:-8px -15px -17px;"></div>
              </td>
            <td class="text-right">
              <a href="/admin/attendance-overview-month" class="btn btn-info btn-xs"><i class="fa fa-file-text-o"></i></a><br />
              <span class="label label-warning">Sick leave</span>
            </td>
          </tr>
          <tr class="warning">
            <td colspan="6" style="border-top:none;">
              <div id="chartcontainer1" style="height:20px;"></div>
            </td>
          </tr>
          <tr>
           <td colspan="6" class="hidden">
              <div style="display: flex; justify-content: space-between;">
                <?php
                $i = 0;
                do {
                    echo '<i class="fa fa-circle" style="font-size:9px; color: #ccc;"></i> ';
                    $i++;
                } while ($i < 50);
                ?>
                <?php
                $i = 0;
                do {
                    echo '<i class="fa fa-circle" style="font-size:9px; color: #1abc9c"></i> ';
                    $i++;
                } while ($i < 50);
                ?>
                <?php
                $i = 0;
                do {
                    echo '<i class="fa fa-circle" style="font-size:9px; color: #ccc;"></i>  ';
                    $i++;
                } while ($i < 50);
                ?>
              </div>
           </td>
          </tr>
*/
          ?>
         
    <!-- Late Comer -->
    <!-- <div id="late">
      <div class="panel panel-default">
        <table class="table">
          <tbody>
            <tr>
              <th>Name</th>
              <th>Time to come</th>
              <th>Reporting Manager</th>
            </tr>
            <tr>
              <td>Shrutika</td>
              <td>12:30am</td>
              <td>Megha Kumari</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div> -->
    <!-- Leave section -->
    <!-- <div id="leavesection">
      <div class="panel panel-default">
        <table class="table">
          <tbody>
            <tr>
              <th>Name</th>
              <th>Type</th>
              <th>Action</th>
            </tr>
            <tr></tr>
          </tbody>
        </table>
      </div>
    </div> -->
        
  </div>

</div>

<?php
/*
  
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Online', 6],
          ['Offline', 3],
        ]);

        var options = {
          title: '',
          colors: ['#1abc9c', '#e6693e'],
          tooltip: { isHtml: true },    // CSS styling affects only HTML tooltips.
          legend: { position: 'none' },

        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
    </script>

<script type="text/javascript">
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip(); 
});

$(function () {
    var chart = new Highcharts.Chart({
        chart: {
            renderTo: 'chartcontainer',
            backgroundColor: null,
            type: 'spline',
            borderWidth: 0,
            margin: [2, 0, 2, 0],
            type: 'area',
            skipClone: true
        },
        title: {
                text: ''
            },
            credits: {
                enabled: false
            },
            xAxis: {
              type: 'datetime',
              labels: {
                enabled: false
              },
              title: {
                  text: null
              },
              startOnTick: false,
              endOnTick: false,
              tickPositions: []
            },
            yAxis: {
                endOnTick: false,
                startOnTick: false,
                labels: {
                    enabled: false
                },
                title: {
                    text: null
                },
                tickPositions: [0]
            },
            legend: {
                enabled: false
            },
            tooltip: {
                hideDelay: 0,
                outside: true,
                shared: true
            },
        plotOptions: {
                series: {
                    animation: false,
                    lineWidth: 1,
                    shadow: false,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    marker: {
                        radius: 1,
                        states: {
                            hover: {
                                radius: 2
                            }
                        }
                    },
                    fillOpacity: 0.25
                },
                column: {
                    negativeColor: '#910000',
                    borderColor: 'silver'
                }
            },

        series: [{
            pointStart: new Date().getTime(),
            pointInterval: 24 * 60 * 60, 
            data: [-1, 1, -1, -1, -1, 1, 1, 1, 1, 1, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 1, 1, 1, -1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, -1, -1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ],
        }]
    });

  var chart = new Highcharts.Chart({
        chart: {
            renderTo: 'chartcontainer1',
            backgroundColor: null,
            type: 'spline',
            borderWidth: 0,
            margin: [2, 0, 2, 0],
            type: 'area',
            skipClone: true
        },
        title: {
                text: ''
            },
            credits: {
                enabled: false
            },
            xAxis: {
              type: 'datetime',
              labels: {
                enabled: false
              },
              title: {
                  text: null
              },
              startOnTick: false,
              endOnTick: false,
              tickPositions: []
            },
            yAxis: {
                endOnTick: false,
                startOnTick: false,
                labels: {
                    enabled: false
                },
                title: {
                    text: null
                },
                tickPositions: [0]
            },
            legend: {
                enabled: false
            },
            tooltip: {
                hideDelay: 0,
                outside: true,
                shared: true
            },
        plotOptions: {
                series: {
                    animation: false,
                    lineWidth: 1,
                    shadow: false,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    marker: {
                        radius: 1,
                        states: {
                            hover: {
                                radius: 2
                            }
                        }
                    },
                    fillOpacity: 0.25
                },
                column: {
                    negativeColor: '#910000',
                    borderColor: 'silver'
                }
            },

        series: [{
            pointStart: new Date().getTime(),
            pointInterval: 24 * 60 * 60, 
            data: [-1, 1, -1, -1, -1, 1, 1, 1, 1, 1, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 1, 1, 1, -1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, -1, -1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ],
        }]
    });
  var chart = new Highcharts.Chart({
        chart: {
            renderTo: 'chartcontainer2',
            backgroundColor: null,
            type: 'spline',
            borderWidth: 0,
            margin: [2, 0, 2, 0],
            type: 'area',
            skipClone: true
        },
        title: {
                text: ''
            },
            credits: {
                enabled: false
            },
            xAxis: {
              type: 'datetime',
              labels: {
                enabled: false
              },
              title: {
                  text: null
              },
              startOnTick: false,
              endOnTick: false,
              tickPositions: []
            },
            yAxis: {
                endOnTick: false,
                startOnTick: false,
                labels: {
                    enabled: false
                },
                title: {
                    text: null
                },
                tickPositions: [0]
            },
            legend: {
                enabled: false
            },
            tooltip: {
                hideDelay: 0,
                outside: true,
                shared: true
            },
        plotOptions: {
                series: {
                    animation: false,
                    lineWidth: 1,
                    shadow: false,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    marker: {
                        radius: 1,
                        states: {
                            hover: {
                                radius: 2
                            }
                        }
                    },
                    fillOpacity: 0.25
                },
                column: {
                    negativeColor: '#910000',
                    borderColor: 'silver'
                }
            },

        series: [{
            pointStart: new Date().getTime(),
            pointInterval: 24 * 60 * 60, 
            data: [-1, 1, -1, -1, -1, 1, 1, 1, 1, 1, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 1, 1, 1, -1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, -1, -1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ],
        }]
    });
});

 </script>
*/
 ?>
<script type="text/javascript">
  $(document).ready(function() {
    $('#dt-attendance-list').DataTable({paging: false});
} );
</script>
@endsection