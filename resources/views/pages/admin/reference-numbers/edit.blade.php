@extends('layouts.admin-dashboard') @section('main-content')

<section class="new-project-section">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
			<div class="row">
				<div class="col-sm-8">
					<h1 class="admin-page-title">Reference Number</h1>
					<ol class="breadcrumb">
						<li>
							<a href="/admin">Admin</a>
						</li>
						<li>
							<a href="{{ url('admin/reference-number') }}">Reference Number</a>
						</li>
						<li class="active">{{ $referenceObj->id }}</li>
					</ol>
				</div>
				<div class="col-sm-4 text-right m-t-10">
					<button type="button" onclick="window.history.back();" class="btn btn-default">
						<i class="fa fa-caret-left fa-fw"></i> Back</button>
				</div>
			</div>
		</div>

		@if(!empty($errors->all()))
		<div class="alert alert-danger">
			@foreach ($errors->all() as $error)
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<span>{{ $error }}</span>
			<br/> @endforeach
		</div>
		@endif @if (session('message'))
		<div class="alert alert-success">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<span>{{ session('message') }}</span>
			<br/>
		</div>
		@endif

		<form class="form-horizontal" method="post" action="/admin/reference-number/{{$referenceObj->id}}" enctype="multipart/form-data">
			<input name="_method" type="hidden" value="PUT" />
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">Reference Number</label>
									<div class="col-sm-4">{{$referenceObj->value}}</div>
								</div>
							</div>
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">Document Type</label>
								<div class="col-sm-4">
									@if( $referenceObj->document_type == 'employee' )
									<select class="form-control" name="document_type">
										<option value="employee" selected>Employee</option>
										<option value="legals">Legals</option>
										<option value="msa">MSA</option>
										<option value="sow">SOW</option>
										<option value="others">Others</option>
									</select>
									@elseif ( $referenceObj->document_type == 'legals' )
									<select class="form-control" name="document_type">
										<option value="employee">Employee</option>
										<option value="legals" selected>Legals</option>
										<option value="msa">MSA</option>
										<option value="sow">SOW</option>
										<option value="others">Others</option>
									</select>
									@elseif ( $referenceObj->document_type == 'msa' )
									<select class="form-control" name="document_type">
										<option value="employee">Employee</option>
										<option value="legals">Legals</option>
										<option value="msa" selected>MSA</option>
										<option value="sow">SOW</option>
										<option value="others">Others</option>
									</select>
									@elseif ( $referenceObj->document_type == 'sow' )
									<select class="form-control" name="document_type">
										<option value="employee">Employee</option>
										<option value="legals">Legals</option>
										<option value="msa">MSA</option>
										<option value="sow" selected>SOW</option>
										<option value="others">Others</option>
									</select>
									@else
									<select class="form-control" name="document_type">
										<option value="employee">Employee</option>
										<option value="legals">Legals</option>
										<option value="msa">MSA</option>
										<option value="sow">SOW</option>
										<option value="others" selected>Others</option>
									</select>
									@endif
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Status:</label>
								@if( $referenceObj->status == 'new' )
								<div class="col-sm-4">
									<select class="form-control" name="status">
										<option value="new" selected>New</option>
										<option value="cancelled">Cancelled</option>
									</select>
								</div>
								@else
								<div class="col-sm-4">
									<select class="form-control" name="status">
											<option value="new">New</option>
											<option value="cancelled" selected>Cancelled</option>
									</select>
								</div>
								@endif
							</div>
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">Signed By</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" id="signed_by" name="signed_by" value="{{ $referenceObj->signed_by }}">
								</div>
							</div>
							<div class="form-group">
									<label for="" class="col-sm-2 control-label">For</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="for" name="for" value="{{ $referenceObj->for }}">
									</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Basic Info:</label>
								<div class="col-sm-4">
									<textarea class="form-control" id="info" name="info" rows="4" placeholder="Enter details">{{$referenceObj->info}}</textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="text-center">
				<button type="submit" class="btn btn-success">Update Reference</button>
			</div>
		</form>
	</div>
</section>
@endsection