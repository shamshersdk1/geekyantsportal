@extends('layouts.admin-dashboard')
@section('main-content')

<section class="new-project-section">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">Reference Number</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
			  			<li><a href="{{ url('admin/reference-number') }}">Reference Numbers</a></li>
			  			<li class="active">New</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>

	    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif

		<form class="form-horizontal" method="post" action="/admin/reference-number" enctype="multipart/form-data">
			<div class="panel panel-default">
				<div class="panel-body">
				    <div class="row">
				    	<div class="col-md-12">
							<div class="form-group">
								<label class="col-sm-3 control-label">Document Type:<span style="color: red"> *</span></label>
								<div class="col-sm-4">
									@if( !empty($documentTypes) )
									<select id="selectid3" name="document_type_id" style="width=35%;">
										@foreach($documentTypes as $x)
											<option value="{{$x->id}}" >{{$x->name}}</option>
										@endforeach
									</select>
									@endif
								</div>
							</div>
							<div class="form-group">
						    	<label for="" class="col-sm-3 control-label">Signed by:<span style="color: red"> *</span></label>
						    	<div class="col-sm-4">
						      		<input type="text" class="form-control" id="signed_by" name="signed_by" value="{{ old('signed_by') }}" required>
						    	</div>
							</div>
						  	<div class="form-group">
						    	<label for="" class="col-sm-3 control-label">For (Client/Company/Employee Name):<span style="color: red"> *</span></label>
						    	<div class="col-sm-4">
						      		<input type="text" class="form-control" id="for" name="for" value="{{ old('for') }}" required>
						    	</div>
						  	</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Description:</label>
								<div class="col-sm-4">
									<textarea class="form-control" id="info" name="info" rows="4" placeholder="Enter details">{{old('info')}}</textarea>
								</div>
							</div>
					  	</div>
					</div>
				</div>
			</div>
  	  		<div class="text-center">
		  		<button type="submit" class="btn btn-success">Generate Reference Number</button>
		  	</div>
		</form>
	</div>
</section>
@endsection
