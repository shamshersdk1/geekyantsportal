@extends('layouts.admin-dashboard')
@section('main-content')
<section>
   <div class="container-fluid">
      <div class="breadcrumb-wrap">
         <div class="row">
            <div class="col-sm-6">
               <h1>Reference Number </h1>
               <ol class="breadcrumb">
                  <li><a href="/admin">Admin</a></li>
                  <li><a href="/admin/reference-number">Reference Number</a></li>
                  <li class="active">{{$referenceObj->id}}</li>
               </ol>
            </div>
         </div>
      </div>
      @if(!empty($errors->all()))
      <div class="alert alert-danger">
         @foreach ($errors->all() as $error)
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
         <span>{{ $error }}</span><br/>
         @endforeach
      </div>
      @endif
      @if (session('message'))
      <div class="alert alert-success">
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
         <span>{{ session('message') }}</span><br/>
      </div>
      @endif
      <div class="row cms-dev">
         <div class="col-md-12">
            <h4 >Reference Number Details</h4>
            <div class="panel ">
               <div class="panel-body">
                   <div class="col-xs-3"></div>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="alert alert-info col-xs-6" >
                        <div>
                            Your Reference Number is:
                        </div>
                        <div style="margin-top:10px">
                            <strong>{{$referenceObj->value}}</strong>
                        </div>
                        <div style="margin-top:20px">
                            <label>Generated On: </label>
                            <span>{{datetime_in_view($referenceObj->created_at)}}</span>
                        </div>
                        <div>
                            <label>Document Type: </label>
                            <span>{{ $referenceObj->documentType->name}}</span>
                        </div>
                        <div>
                            <label>Signed By: </label>
                            <span>{{$referenceObj->signed_by}}</span>
                        </div>
                        <div>
                            <label>Submiited By: </label>
                            <span>{{$referenceObj->uploadedBy->name}}</span>
                        </div>
                        <div>
                            <label>For: </label>
                            <span>{{ $referenceObj->for}}</span>
                        </div>
                        <div>
                            <label>Description: </label>
                            <span>{{ $referenceObj->info}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-2"></div>
                <div class="text-center col-md-12">
                    <a href="/admin/reference-number" class="btn btn-success">Back</a>
                </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
@endsection