@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">List of Timesheet</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li><a href="{{ url('admin/users') }}">List of Timesheet</a></li>
        		</ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <span><a href="/admin/timesheet/" class="btn btn-primary">Save</a></span>
           		<span><a href="/admin/timesheet/" class="btn btn-success">Submit</a></span>
            </div>
        </div>
    </div>
</div>
	<div class="row">
        <div class="col-md-12">
			<div class="panel panel-default">
				<table class="table table-striped">
					<thead>
						<tr>
							<th width="10%" class="td-text">#</th>
							<th width="15%" class="td-text">Projects</th>
	   						<th>
	   							<div>Sunday</div>
	   							<div>30 Nov</div>
	   						</th>
							<th>
								<div>Monday</div>
								<div>1 Dec</div>
							</th>
							<th>
								<div>Tuesday</div>
								<div>2 Dec</div>
							</th>
							<th>
								<div>Wednesday</div>
								<div>3 Dec</div>
							</th>
							<th>
								<div>Thursday</div>
								<div>4 Dec</div>
							</th>
							<th>
								<div>Friday</div>
								<div>5 Dec</div>
							</th>
							<th>
								<div>Saturday</div>
								<div>6 Dec</div>
							</th>
							<th width="15%" class="text-center td-text">total hours</th>
						</tr>
					</thead>
						<tr>	
							<td width="10%" class="td-text">1</td>
							<td width="15%" class="td-text">asdfgfhj</td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td width="20%" class="text-center">0 hrs</td>
						</tr>
						<tr>	
							<td width="10%" class="td-text">2</td>
							<td width="15%" class="td-text">asdfgfhj</td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td width="15%" class="text-center td-text">0 hrs</td>
						</tr>
						<tr>	
							<td width="10%" class="td-text">3</td>
							<td width="15%" class="td-text">asdfgfhj</td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td width="15%" class="text-center td-text">0 hrs</td>
						</tr>
						<tr>	
							<td width="10%" class="td-text">4</td>
							<td width="15%" class="td-text">asdfgfhj</td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td width="15%" class="text-center td-text">0 hrs</td>
						</tr>	
						<tr>	
							<td width="10%" class="td-text">5</td>
							<td width="15%" class="td-text">asdfgfhj</td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td width="15%" class="text-center td-text">0 hrs</td>
						</tr>
						<tr>	
							<td width="10%" class="td-text">6</td>
							<td width="15%" class="td-text">asdfgfhj</td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td width="15%" class="text-center td-text">0 hrs</td>
						</tr>
						<tr>	
							<td width="10%" class="td-text">7</td>
							<td width="15%" class="td-text">asdfgfhj</td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td width="15%" class="text-center td-text">0 hrs</td>
						</tr>
						<tr>	
							<td width="10%" class="td-text">8</td>
							<td width="15%" class="td-text">asdfgfhj</td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td width="15%" class="text-center td-text">0 hrs</td>
						</tr>
						<tr>	
							<td width="10%" class="td-text">9</td>
							<td width="15%" class="td-text">asdfgfhj</td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td width="15%" class="text-center td-text">0 hrs</td>
						</tr>	
						<tr>	
							<td width="10%" class="td-text">10</td>
							<td width="15%" class="td-text">asdfgfhj</td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
							<td><input type="number" class="form-control" placeholder="0"></td>
						
							<td width="15%" class="text-center td-text">0 hrs</td>
						</tr>
				</table>
			</div>
		</div>
	</div>
@endsection
