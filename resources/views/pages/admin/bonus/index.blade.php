@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Bonuses</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/bonuses') }}">Bonuses</a></li>
                </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <a href="/admin/bonuses/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i> Add new Bonus</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
        <h4>Filters: </h4>
            <div class="form-group">
                <label for="month">Month:</label>
                <select name="month" id="month">
                    <option value="0">All</option>
                    <option value="1">January</option>
                    <option value="2">Feburary</option>
                    <option value="3">March</option>
                    <option value="4">April</option>
                    <option value="5">May</option>
                    <option value="6">June</option>
                    <option value="7">July</option>
                    <option value="8">August</option>
                    <option value="9">September</option>
                    <option value="10">October</option>
                    <option value="11">November</option>
                    <option value="12">December</option>
                </select>
                <label style="margin-left:2%" for="year">Year:</label>
                <select name="year" id="year">
                </select>
                <div style="float:right"><input id="paid" type="checkbox" @if(trim(Request::get('status'))=='paid') checked @endif> Show Paid Records</div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
    	@if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span><br/>
                  @endforeach
            </div>
        @endif
        @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
        @endif  
    <div class="user-list-view">
        <div class="panel panel-default"> 
            <table class="table table-striped">
                <thead>
                    <th>#</th>
                    <th>Name of Employee</th>
                    <th>Month</th>
                    <th>Amount</th>
                    <th>Status</th>
                    <th>Notes</th>
                    <th class="text-right">Actions</th>
                </thead>
                @if(!empty($bonuses))
                    @foreach($bonuses as $bonus)
                        <tr>
                            <td class="td-text">{{$bonus->id}}</td>
                            <td class="td-text">{{$bonus->user->name}}</td>
                            <td class="td-text">{{date("F Y", strtotime($bonus->date))}}</td>
                            <td class="td-text">{{$bonus->amount}}</td>
                            <td>
                                @if($bonus->status == "approved")
                                    <span class="label label-success custom-label">Approved</span>
                                @elseif($bonus->status == "pending")
                                    <span class="label label-info custom-label">Pending</span>
                                @elseif($bonus->status == "rejected")
                                    <span class="label label-danger custom-label">Rejected</span>
                                @else
                                    <span class="label label-info custom-label">No Status</span>
                                @endif
                            </td>
                            <td class="td-text">{{$bonus->notes}}</td>
                            <td class="text-right">
                                <a href="/admin/bonuses/{{$bonus->id}}" type="button" class="btn btn-info crud-btn btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                                @if($bonus->status == "pending")
                                    <a href="/admin/bonuses/{{$bonus->id}}/edit" type="button" class="btn btn-info crud-btn btn-sm"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
                                    <form method="POST" action="/admin/bonuses/{{$bonus->id}}" enctype="multipart/form-data" style="display:inline">
                                        {{method_field('PUT')}}
                                        <button type="submit" name="approve" value="approve" class="btn btn-success crud-btn btn-sm"><i class="fa fa-check" aria-hidden="true"></i> Approve</button>
                                    </form>
                                    <form method="POST" action="/admin/bonuses/{{$bonus->id}}" enctype="multipart/form-data" style="display:inline">
                                        {{method_field('PUT')}}
                                        <button type="submit" name="reject" value="reject" class="btn btn-danger crud-btn btn-sm"><i class="fa fa-times" aria-hidden="true"></i> Reject</button>
                                    </form>
                                @elseif($bonus->status == "approved"&&!$bonus->paid)
                                    <form method="POST" action="/admin/bonuses/{{$bonus->id}}" enctype="multipart/form-data" style="display:inline">
                                        {{method_field('PUT')}}
                                        <button type="submit" name="paid" value="paid" class="btn btn-success crud-btn btn-sm">Mark As Paid</button>
                                    </form>
                                @endif
                                <form action="/admin/bonuses/{{$bonus->id}}" method="post" style="display:inline-block;" class="deleteform">
                                    <input name="_method" type="hidden" value="DELETE">
                                    <button class="btn btn-danger crud-btn btn-sm" type="submit"><i class="fa fa-trash btn-icon-space" aria-hidden="true"></i>Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="7">No results found.</td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
        @if(!empty($bonuses))
            {{$bonuses->links()}}
        @endif
	</div>
</div>
<script>
		$(function(){
            $(".deleteform").submit(function(event){
                    return confirm('Are you sure?');
                });
            $date= new Date();
            $year=$date.getFullYear();
            $max=$date.getFullYear()+1;
            $all="All";
            $options="<option value='"+0+"'>"+ $all +"</option>";
            for($y = 2000 ; $y <=$max; $y++){
                $options += "<option value='"+$y+"'>"+ $y +"</option>";
            }
            $("#year").html($options);
            $( "#month" ).change(function() {
                $month=$("#month option:selected").val();
            window.location.href = "/admin/bonuses?month="+$month+"@if(Request::get('status')!='')&&status=paid @endif @if(Request::get('year')!='')&&year={{Request::get('year')}} @endif";
            });
            $( "#year" ).change(function() {
                $year=$("#year option:selected").val();
            window.location.href = "/admin/bonuses?year="+$year+"@if(Request::get('status')!='')&&status=paid @endif @if(Request::get('month')!='') &&month={{Request::get('month')}} @endif";
            });
            $('#paid').change(function() {
                if(this.checked)
                {
                    window.location.href = "/admin/bonuses?status=paid @if(Request::get('year')!='')&&year={{Request::get('year')}} @endif @if(Request::get('month')!='') &&month={{Request::get('month')}} @endif";
                }
                else{
                    window.location.href = "/admin/bonuses?@if(Request::get('year')!='')&&year={{Request::get('year')}} @endif @if(Request::get('month')!='') &&month={{Request::get('month')}} @endif";
                }
            });
            @if(Request::get('year')!='')
            $y={{trim(Request::get('year'))}};
            $('#year option[value="'+$y+'"]').attr("selected","true");
            @endif
            @if(Request::get('month')!='')
            $m={{trim(Request::get('month'))}};
            $('#month option[value="'+$m+'"]').attr("selected","true");
            @endif
        });
</script>
@endsection