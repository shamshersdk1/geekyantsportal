@extends('layouts.admin-dashboard')
@section('main-content')
    <div class="container-fluid mis-report">
        <div class="breadcrumb-wrap">
           <div class="row">
              <div class="col-sm-8">
                 <h1 class="admin-page-title">Paid Bonuses for {{$monthObj->formatMonth()}}</h1>
                 <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="/admin/bonus-paid">All Paid Bonuses</a></li>
                    <li class="active">{{$monthObj->formatMonth()}}</li>
                 </ol>
              </div>
           </div>
        </div>
        @if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Error!</strong> There were some errors.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
        <table class="table table-bordered table-hover bg-white">
          <tr>
              <th width="120px" class="text-center">User</th>
              <th width="120px" class="text-center">Action(s)</th>
          </tr>
          @if(count($users) == 0 )
            <tr>
               <td colspan="2" class="text-center">
                  No Bonus Record found for any User
               </td>
            </tr>

         @endif
         @if(isset($users))
            @foreach($users as $user)
               <tr>
                  <td class="text-center">{{$user->user->name}}<br/><small>{{$user->user->employee_id}}</small></div>
                  <td class="text-center">
                  <a class="btn btn-success" href="{{$monthObj->id}}/{{$user->user->id}}"><i class="fa fa-eye"></i> View</a>
                 </tr>
            @endforeach
         @endif
        </table>
    </div>
@endsection
