@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Bonus Details</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
		  			<li><a href="{{ url('admin/bonuses') }}">Bonuses</a></li>
                    <li class="active">View</li>
        		</ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
        </div>
    </div>
    <div class="">
        <div class="">
    	@if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span><br/>
                  @endforeach
            </div>
        @endif
        @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
        @endif   
            <div class="panel panel-default">
                <ul class="list-group">
                    <li class="list-group-item"> Name of Employee: {{$bonus->user->name}}</li>
                    <li class="list-group-item"> Month: {{date('F, Y',strtotime($bonus->date))}}</li>
                    <li class="list-group-item"> Amount: {{$bonus->amount}}</li>
                    <li class="list-group-item"> Status: @if($bonus->status == "approved")
                                                            <span class="label label-success custom-label">Approved</span>
                                                        @elseif($bonus->status == "pending")
                                                            <span class="label label-info custom-label">Pending</span>
                                                        @elseif($bonus->status == "rejected")
                                                            <span class="label label-danger custom-label">Rejected</span>
                                                        @else
                                                            <span class="label label-default custom-label">No Status</span>
                                                        @endif</li>
                    <li class="list-group-item"> Notes: {{$bonus->notes}}</li>
                    <li class="list-group-item"> Created By: {{$bonus->creator->name}}</li>
                    <li class="list-group-item"> Created On: {{date('j F, Y',strtotime($bonus->created_at))}} at {{date('g:i A',strtotime($bonus->created_at))}} </li>
                    @if($bonus->status != 'pending')
                        <li class="list-group-item">@if($bonus->status == 'approved')Approved By: @else Rejected By: @endif {{$bonus->approver->name}}</li>
                        <li class="list-group-item">@if($bonus->status == 'approved')Approved On: @else Rejected On: @endif{{date('j F, Y',strtotime($bonus->approved_at))}} at {{date('g:i A',strtotime($bonus->approved_at))}} </li>
                        @if($bonus->paid)
                            <li class="list-group-item"> Paid By: {{$bonus->payer->name}}</li>
                            <li class="list-group-item"> Paid On: {{date('j F, Y',strtotime($bonus->paid_at))}} at {{date('g:i A',strtotime($bonus->paid_at))}}</li>
                        @endif
                    @endif
                </ul>
            </div>
        </div>
	</div>
</div>
@endsection		