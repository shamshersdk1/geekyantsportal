
@extends('layouts.admin-dashboard')
@section('main-content')
<script>
    window.due_bonues = <?php echo json_encode($dueBonuses); ?>;
    window.userObj = <?php echo json_encode($userObj); ?>;
</script>
<div  ng-app="myApp" ng-controller="bonusDueCtrl">
<div class="container-fluid mis-report"  >
<form method="post" action="/admin/bonus-due/{{$userObj->id}}/approve">
  <div class="breadcrumb-wrap">
  <div class="row">
  <div class="col-sm-8">
  <h1 class="admin-page-title">@if($requestType=="due")List of Bonus Dues @elseif(($requestType=="all")) All Bonuses for {{$userObj->name}} @endif</h1>
  <ol class="breadcrumb">
  <li><a href="/admin">Admin</a></li>
  @if($requestType=="due")
  <li><a href="/admin/bonus-due">Due Bonus</a></li>
  <li class="active">{{$userObj->name}}</li>
  @elseif(($requestType=="all"))
  <li><a href="/admin/bonus">All Bonuses</a></li>
  <li><a href="/admin/bonus/{{$monthObj->id}}">{{$monthObj->formatMonth()}}</a></li>
  <li class="active">{{$userObj->name}}</li>
  @endif
  </ol>
  </div>

  </div>
  </div>
  @if (count($errors) > 0)
    <div class="alert alert-danger">
      <strong>Error!</strong> There were some errors.<br><br>
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif


    <div class="row m-b-10">
      <div class="col-sm-6">
        <h4 class="no-m-b m-t-5">{{$userObj->name}}</h4>
      </div>

      <div class="col-sm-6">
          @if($requestType=="due" && sizeof($dueBonuses)!=0)
        <div class="text-right">
          <h4 class="no-margin">
            <small>Total Draft Amount:</small> <big style="display: inline-block; vertical-align: middle; margin-right: 15px;"></big>%%form.draft_amount%%</big><br/>
            <small>Total Amount:</small> <big style="display: inline-block; vertical-align: middle; margin-right: 15px;"></big>%%form.amount%%</big><br/>
            <small>Approve for Payment:</small> <big style="display: inline-block; vertical-align: middle; margin-right: 15px;"></big>%%form.paid_amount%%</big>
            <!-- <button class="btn btn-success" type="submit">Approve and Next</button> -->
          </h4>

        </div>
      @endif
    </div>
    <div class="row m-b-10">
    <div class="text-right">
    <a href="#" class="btn btn-success" ng-click="form.addBonus()">Add Bonues</a>
    </div>
    </div>
    </div>
    @if($monthObj && $monthObj->bonus_pay_lock_date!=null)
  <div  class="row m-b-10">
      <div class="alert alert-warning">
          <span style="color:red;"><strong>Bonus Payment Approval is locked and will be open for Approval Beginning Next Month</strong></span>
          <a class="btn btn-primary pull-right" disabled>
            <i class="fa fa-lock"></i> &nbsp; Locked on {{datetime_in_view($monthObj->bonus_pay_lock_date)}}
          </a>
      </div>
      </div>
    @endif
    <div  class="row m-b-10">
    <div ng-if="form.errorMessage!=null" class="alert alert-danger">
      <h5><h4><strong>Error!</strong></h4> %%form.errorMessage%%</h5>
    </div>
    </div>
    </div>
    <div class="dashboard-container mis-overview">
      <div class="panel-group leave-mis" >
        <!--left section data start-->
          <div ng-if="sizeof($dueBonuses)==0" class="panel panel-default text-center"><h4>No Bonus Dues Found for this User</h4></div>
          <div >
          <div class="panel panel-default" ng-repeat="(key, value)  in data"  id="accordion_%%$index%%" ng-if="key == 'onsite' || key == 'additional' || key == 'extra' || key == 'performance' ">
            <div class="panel-heading1">
              <a class="accordion-toggle collapsed mis-accordion d-flex text-center" data-toggle="collapse" aria-expanded="true" data-target="#%%$index%%" href="#%%$index%%">
                <div style="width: 40px;" class="align-self-center align-self-stretch bg-light">
                  <i class="fa fa-minus m-t-20"></i>
                </div>
                <div class="flexbox">
                  <div>
                    <small>Bonus Type</small><h4 class="m-t-5">%%key%%</h4>
                  </div>
                  <div>
                    <div ng-if="key!='extra'">
                        <small>Day(s)</small> <h4 class="m-t-5">%%value.days%%</h4>
                    </div>
                    <div ng-if="key=='extra'">
                      <small>Hour(s)</small> <h4 class="m-t-5">%%value.hours%%</h4>
                    </div>
                  </div>
                  <div>
                    <small>Amount:</small><h4 class="m-t-5">%%value.amount%%</h4>
                  </div>
                </div>
              </a>
            </div>
            <div id="%%$index%%" class="panel-collapse collapse">
              <div class="panel-body" style="background: #D4DCE6;">
                <table class="table table-condensed bg-white no-margin">
                  <tr>
                    <th width="15%">Date</th>
                    <!-- <th width="10%">Requested For</th> -->
                    <th width="20%">Description</th>
                    <th width="10%">Draft Amount</th>
                    <th width="15%">Amount</th>
                    <th width="15%">Approved By</th>
                    <th width="15%">Approved At</th>
                    <th width="15%">Status</th>
                    <th width="15%" class="text-right">Action</th>

                  </tr>
                  <tr ng-repeat="bonus in value.bonuses">

                    <td>
                      %%bonus.date | dateFormat%%<br />
                        <label class="label label-danger" ng-if="bonus.on_leave">On Leave</label></label>
                        <label class="label label-warning" ng-if="bonus.weekend">Weekend</label></label>
                        <label class="label label-success" ng-if="bonus.holiday">Holiday</label></label>
                      </td>
                        <!-- <td>
                            <label class="label label-primary">%%bonus.type%%</label>

                        </td> -->
                    <td>
                          <small ng-show="key == 'onsite'" >Project: %%bonus.project.project_name%%</small><br/>
                          <small>Rate: %%bonus.rate%%</small><br/>
                          <small>Details : %%bonus.description%%</small><br/>
                          <small ng-hide="key == 'onsite'">Annual Gross Salary: %%bonus.annual_gross_salary%%</small>
                    </td>
                    <td>%%value.draft_amount%%</td>
                    <td>
                      <div class="input-group input-group-sm">
                          <span class="input-group-btn">
                          <input ng-disabled="bonus.payment_approved || bonus.transaction_id!=null" type='text' class="form-control" ng-model="bonus.amount" ng-change="form.update(bonus)" ng-model-options="{ debounce: 1000 }">
                            <!-- <button ng-disabled="bonus.loading || bonus.payment_approved"  ng-click="form.update(bonus)" class="btn btn-primary">Update
                            </button> -->
                          </span>
                          <span  class="input-group-addon">
                          <i ng-if="bonus.succuss" class="fa fa-check text-success"></i>
                          <i ng-if="bonus.error" class="fa fa-close text-danger"></i>
                        </span>
                        </div>
                      </div>
                      </td>
                      <td >
                    <div ng-if="key==='onsite'">
                     %%bonus.approver%%
                    </div>
                    <div ng-if="key==='extra'||'additional'">
                     %%value.approver%%
                    </div>
                  </td>
                    <td><small> %%bonus.created_at | dateToISO%%</small></td>
                    <td ng-if="!bonus.bonus_confirm && !bonus.salary_process">Bonus Approved</td>
                    <td ng-if="!bonus.salary_process && bonus.bonus_confirm">Bonus Confirmed</td>
                    <td ng-if="bonus.salary_process">Salary Process</td>
                    <td class="text-right">
                        <button ng-hide="bonus.bonus_confirm || bonus.salary_process || bonus.paid_at" ng-click="form.approvePayment(bonus)" class="btn btn-sm btn-primary" >Approve for Payment</button>
                        <span ng-show="bonus.bonus_confirm || bonus.salary_process || bonus.paid_at">Approved at %%bonus.paid_at | dateToISO%%</span>
                    </td>
                  </tr>
                  <tr ng-if="value.bonuses.length ==0">
                    <td class="text-center" colspan="5">No Bonuses due this Month</td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
        <!--left section data end-->
      </div>
    </div>
  </form>
</div>
</div>

@endsection
