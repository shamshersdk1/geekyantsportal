@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
		<div class="row">
            <div class="col-sm-8">
				<h1 class="admin-page-title">Bonus Due User</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                </ol>
			</div>
			<div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
        </div>
	</div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
    </div>
    <div class="col-sm-6">
                <div class="form-group col-md-6 ">
                    <div class="col-md-12 float-right" style="background-color: white;padding:0px;">
                    @if(isset($monthList))
                        <select id="selectid2" name="month"  placeholder= "{{$currMonth ? $currMonth->formatMonth() : 'Select Month'}}">
                            <option value=""></option>
                            @foreach($monthList as $x)
                                <option value="{{$x->id}}" >{{$x->formatMonth()}}</option>
                            @endforeach
                        </select>
                    @endif
                    </div>
                </div>
            </div>

	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel panel-default">
				<table class="table table-striped" id="bonus-table">
				<thead>
					<th class="td-text">#</th>
					<th class="td-text">Name</th>
					<th class="td-text">OnSite</th>
                    <th class="td-text">Additional</th>
                    <th class="td-text">Extra Working</th>
                    <th class="td-text">Performance</th>
                    <th class="td-text">Tech Talk</th>
                    <th class="td-text">Referral</th>
                    <th class="td-text">Total</th>
					
				</thead>
				@if(isset($users))
				@if(count($users) > 0)
					@foreach($users as $index=>$user)
						<tr>
                            <td class="td-text">{{$index + 1}}</td>
							<td class="td-text">{{$user->name}}</td>
                    		<td class="td-text">{{$user->onsiteamount}}</td>
							<td class="td-text">{{$user->additionalamount}}</td>
                            <td class="td-text">{{$user->extraworkingamount}}</td>
                            <th class="td-text">{{$user->performanceamount}}</th>
                            <th class="td-text">{{$user->techtalkamount}}</th>
                            <th class="td-text">{{$user->referralamount}}</th>
                            <td class="td-text"> {{$user->total}}</td>
						</tr>
					@endforeach
				@else
					<tr >
						<td colspan="9" class="text-center">No bonus found.</td>
					</tr>
				@endif
				@endif
				</table>
			</div>
		</div>
	</div>
</div>
<script>
	$(function () {
		$('#type option[value="{{old('type')}}"]').attr("selected",true);
    });
    $('#selectid2').change(function(){
        var optionSelected = $("option:selected", this);
        optionValue = this.value;
        if (optionValue) { 
            window.location = "/admin/bonus-due-user/"+optionValue; 
        }
    });
	$(document).ready(function() {
        var t = $('#bonus-table').DataTable( {
            pageLength:500
        } );
    });
</script>
@endsection
