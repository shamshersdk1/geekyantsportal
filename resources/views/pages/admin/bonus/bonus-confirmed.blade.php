@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-6">
                <h1 class="admin-page-title">Bonus Confimed</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li>bonus confirmed </li>
        		</ol>
            </div>
		</div>
	</div>
	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
    </div>   
    <div class="user-list-view">
        <div class="panel panel-default">
            <table class="table table-striped">
                <tr>
                    <th class="text-center">#</th>
                    <th class="text-left">Employee Id</th>
                    <th class="text-left">User</th>
                    <th class="text-center">Amount</th>
                </tr>
                @if(count($users)>0)
                    @foreach ($users as $index=>$user)
                    <tr>
                        <td class="text-center">{{ $index+1}}</td>
                        <td class="text-left"> 
                            {{$user->user->employee_id}}
                        </td>
                        <td class="text-left"> 
                            {{$user->user->name}}
                        </td>
                        <td class="text-center"> 
                            {{$amount[$user->user_id]}}
                        </td>
                    </tr>
                    @endforeach
                @else
                     <tr>
                        <td class="text-center" colspan="4">No Records found</td>
                    </tr>
                @endif
             </table>
        </div>
    </div>
</div>	
@endsection
