@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid mis-report">
  @if (count($errors) > 0)
		<div class="alert alert-danger">
			<strong>Error!</strong> There were some errors.<br><br>
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

    <div class="breadcrumb-wrap">
      <div class="row">
        <div class="col-sm-8">
          <h1 class="admin-page-title">List of Paid Bonuses for {{$userObj->name}}</h1>
          <ol class="breadcrumb">
            <li><a href="/admin">Admin</a></li>
            <li><a href="/admin/bonus-paid">All Paid Bonuses</a></li>
            <li><a href="/admin/bonus-paid/{{$monthObj->id}}">{{$monthObj->formatMonth()}}</a></li>
            <li class="active">{{$userObj->name}}</li>
          </ol>
        </div>
      </div>
    </div>
    <div class="row m-b-10">
      <div class="col-sm-6">
        <h4 class="no-m-b m-t-5">{{$userObj->name}}</h4>
      </div>
  </div>
    </div>
<div class="dashboard-container mis-overview" >
      <div class="panel-group leave-mis" >
        <!--left section data start-->
        @if(sizeof($data)==0)
          <div class="panel panel-default text-center"><h4>No Bonus Dues Found for this User</h4></div>
        @else
        @foreach($data as $index => $dataRow)
          <div class="panel panel-default" id="accordion_{{$index}}">
            <div class="panel-heading1">
              <a class="accordion-toggle collapsed mis-accordion d-flex text-center" data-toggle="collapse" data-target="#{{$index}}" href="#{{$index}}">
                <div style="width: 40px;" class="align-self-center align-self-stretch bg-light">
                  <i class="fa fa-minus m-t-20"></i>
                </div>
                <div class="flexbox">
                  <div>
                    <small>Bonus Type</small><h4 class="m-t-5">{{ucfirst($index)}}</h4>
                  </div>
                  <div>
                    @if($index!="extra")
                      <small>Day(s)</small> <h4 class="m-t-5">{{$total[$index]["days"]}}</h4>
                    @else
                      <small>Hour(s)</small> <h4 class="m-t-5">{{$total[$index]["hours"]}}</h4>
                    @endif
                    </div>
                  <div>
                    <small>Amount:</small><h4 class="m-t-5">{{$total[$index]["amount"]}}</h4>
                  </div>
                </div>
              </a>
            </div>
            <div id="{{$index}}" class="panel-collapse collapse">
              <div class="panel-body" style="background: #D4DCE6;">
                <table class="table table-condensed bg-white no-margin">
                  <tr>
                    <th width="15%">Bonus Date</th>
                    <th width="15%">Transaction Date</th>
                    <th width="15%">Amount</th>
                    <th width="15%">Approved By</th>
                    <th width="15%">Paid By</th>
                    <th width="15%">Paid At</th>
                  </tr>
                  @if($dataRow)
                  @foreach($dataRow as $row)
                  <tr>
                    <td>{{$row["bonus"]->date?date_in_view($row["bonus"]->date):''}}</td>
                    <td>{{$row["transaction"]->date?date_in_view($row["transaction"]->date):''}}</td>
                    <td>{{$row["transaction"]->amount?$row["transaction"]->amount:0}}</td>
                    <td>{{$row["bonus"]->approver?$row["bonus"]->approver->name:''}}</td>
                    <td>{{$row["bonus"]->payer?$row["bonus"]->payer->name:''}}</td>
                    <td>{{$row["bonus"]->paid_at?datetime_in_view($row["bonus"]->paid_at):''}}</td>
                  </tr>
                  @endforeach

                  @else
                  <tr>
                    <td class="text-center" colspan="5">No Bonuses due this Month</td>
                  </tr>
                  @endif
                </table>
              </div>
            </div>
          </div>
        @endforeach
        @endif
        <!--left section data end-->
      </div>

    </div>
</div>
@endsection
