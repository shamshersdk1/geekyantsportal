@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Add Employee Bonus</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
		  			<li><a href="{{ url('admin/bonuses') }}">Bonuses</a></li>
                    <li class="active">Add</li>
        		</ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
        </div>
    </div>
        <div class="row">
        <div class="col-md-12">
    	@if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span><br/>
                  @endforeach
            </div>
        @endif
        @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
        @endif   
		<form name = "myForm" method="POST" action="/admin/bonuses" enctype="multipart/form-data" >
            <div class="panel panel-default" >
                <div class="row panel-body">
                    <div class="col-md-8 col-md-offset-2">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="name">Employee :</label>
                            <input type="text" id="name" name="name" class="form-control" placeholder="Name" required value="{{ old('name') }}">
                        </div>
                        <div class="form-group">
                            <label for="month">Month:</label>
                            <select name="month" id="month">
                                <option value="1">January</option>
                                <option value="2">Feburary</option>
                                <option value="3">March</option>
                                <option value="4">April</option>
                                <option value="5">May</option>
                                <option value="6">June</option>
                                <option value="7">July</option>
                                <option value="8">August</option>
                                <option value="9">September</option>
                                <option value="10">October</option>
                                <option value="11">November</option>
                                <option value="12">December</option>
                            </select>
                            <label style="margin-left:2%" for="year">Year:</label>
                            <select name="year" id="year"></select>
                        </div>
                        <div class="form-group">
                            
                        </div>
                        <div class="form-group">
                            <label for="file">Amount:</label>
                            <input type="text" class="form-control" placeholder="0.00" name="amount" value="{{old('amount')}}" required>
                        </div>
                        <div class="form-group">
                            <label for="name">Note:</label>
                            <textarea name="note" id="note" class="form-control" placeholder="Notes..." ></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <button type="reset" class="btn btn-default"><i class="fa fa-rotate-left fa-fw"></i> Clear</button> 
                <button type="submit" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Save</button>
            </div>
		</form>
	</div>
			
	</div>
</div>
<script>
    $(function () { 
        $date= new Date();
        $month=$date.getMonth()+1;
        $year=$date.getFullYear();
        $('#month option[value="'+$month+'"]').attr("selected","true");
        $max=$date.getFullYear()+1;
        $options="";
        for($y = 2000 ; $y <=$max; $y++){
            $options += "<option value='"+$y+"'>"+ $y +"</option>";
        }
        $("#year").html($options);
        $('#year option[value="'+($max-1)+'"]').attr("selected","true");
        $("#note").text("{{old('note')}}");
        $( "#name" ).autocomplete({
            source: {{$jsonuser}}
        });
    });
</script>
@endsection