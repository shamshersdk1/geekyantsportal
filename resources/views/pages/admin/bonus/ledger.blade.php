@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Bonuses</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/bonuses') }}">Bonuses</a></li>
                    <li><a>Ledger</a></li>
                </ol>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
    	@if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span><br/>
                  @endforeach
            </div>
        @endif
        @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
        @endif
    <div class="row bg-white search-section-top"> 
        <form class="form-group col-md-12 p-0 flex-container " action="/admin/bonus-ledger" method="get" style="align-items:center">
            <div class="col-md-2 p-l-0">
             <div class='input-group date' id='year_picker'>
                    <input type='text' id="year" class="form-control" value="<?=( isset( $_GET['search_year'] ) ? $_GET['search_year'] : '' )?>" autocomplete="off" name="search_year" placeholder="Search Year" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <div class="col-md-2 p-l-0">
             <div class='input-group date' id='month_picker'>
                    <input type='text' id="month" class="form-control" value="<?=( isset( $_GET['search_month'] ) ? $_GET['search_month'] : '' )?>" autocomplete="off" name="search_month" placeholder="Search Month" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <div class="col-md-2">
                <span class="input-group-btn">
                    <button type="submit" class="btn btn-primary btn-sm center" style="margin-left: 2px;">Submit</button>
                </span>
                <span class="input-group-btn">
                    <a href="/admin/bonus-ledger" class="btn btn-primary btn-sm center">Reset</a>
                </span>
            </div>
            <div class=" pull-right"  style="margin-left:auto">
                <div class="leave-stat pull-right">
                    <div class="text">
                        <span class="stat-info">
                        Total:<b>{{number_format($total, 2)}}</b>
                        </span>
                    </div>
                </div>
            </div>
        </form> 
    </div>
    <div class="row">
        <div class="col-md-12">
            <h4>List of bonuses</h4>
        </div>    
    </div>     
    <div class="user-list-view">
        <div class="panel panel-default"> 
            <table class="table table-striped">
                <thead>
                    <th>#</th>
                    <th>Name of Employee</th>
                    <th>Date</th>
                    <th>Amount</th>
                    <th>Notes</th>
                </thead>
                @if(count($bonusList) > 0 )
                    @foreach($bonusList as $index => $bonus)
                        <tr>
                            <td class="td-text">{{$index + 1}}</td>
                            <td class="td-text">{{$bonus->user->name}}</td>
                            <td class="td-text">{{date_in_view($bonus->date)}}</td>
                            <td class="td-text">{{number_format($bonus->amount, 2)}}</td>
                            <td class="td-text">{{$bonus->notes}}</td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="7">No results found.</td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
        @if(!empty($bonuses))
            {{$bonuses->links()}}
        @endif
	</div>
</div>
<script type="text/javascript">
    $(function () {
        $('#year_picker').datetimepicker({
            viewMode: 'years',
            format: 'YYYY'
        });
        $('#month_picker').datetimepicker({
            viewMode: 'months',
            format: 'MM'
        });
    });
</script>
@endsection