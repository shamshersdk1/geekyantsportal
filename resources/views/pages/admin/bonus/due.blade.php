@extends('layouts.admin-dashboard')
@section('main-content')
    <div class="container-fluid mis-report">
        <div class="breadcrumb-wrap">
           <div class="row">
              <div class="col-sm-8">
                 <h1 class="admin-page-title">@if($requestType=="due")Bonus Dues @elseif(($requestType=="all")) All Bonuses for {{$monthObj->formatMonth()}} @endif</h1>
                 <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    @if($requestType=="due")
                    <li class="active">Due Bonus </li>
                    @elseif(($requestType=="all"))
                    <li><a href="/admin/bonus">All Bonuses</a></li>
                    <li class="active">{{$monthObj->formatMonth()}}</li>
                    @endif
                 </ol>
              </div>
           </div>
        </div>
        @if (count($errors) > 0)
            <div class="alert alert-danger">
               <strong>Error!</strong> There were some errors.<br><br>
               <ul>
                  @foreach ($errors->all() as $error)
                     <li>{{ $error }}</li>
                  @endforeach
               </ul>
            </div>
         @endif
         @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
        @endif
         <div>
         <h4>List of Bonuses for the month of {{$currMonth->formatMonth()}}</h4>
         @if($monthObj && $monthObj->bonus_pay_lock_date!=null)
            <div class="alert alert-warning">
               <span style="color:red;"><strong>Bonus Payment Approval is locked and will be open for Approval Beginning Next Month</strong></span>
               <a class="btn btn-primary pull-right" disabled>
                  <i class="fa fa-lock"></i> &nbsp; Locked on {{datetime_in_view($monthObj->bonus_pay_lock_date)}}
               </a>
            </div>
         @else
           <button name="lock" id="lock" class="btn btn-danger pull-right"><i class="fa fa-lock"></i> &nbsp; Lock</button>
         @endif
           
               <div class="col-md-3 pull-right" style="background-color: white;padding:0px;">
               @if(isset($monthList))
                  <select id="selectid2" name="month"  placeholder= "{{$currMonth ? $currMonth->formatMonth() : 'Select Month'}}">
                        <option value=""></option>
                        @foreach($monthList as $x)
                           <option value="{{$x->id}}" >{{$x->formatMonth()}}</option>
                        @endforeach
                  </select>
               @endif
               </div>
         
         
         </div>
        <table class="table table-bordered table-hover bg-white" id="bonus-table">
        <thead>
          <tr>
            <th class="text-center">Sl.No</th>
              <th class="text-center">Employee ID</th>
              <th class="text-center">Employee Name</th>
              <th class="text-center">Bonus Type</th>
              <th class="text-center">Action(s)</th>
          </tr>
         </thead>
          @if(count($bonuses) == 0 )
            <tr>
               <td colspan="2" class="text-center">
                  No Bonus Record found for any User
               </td>
            </tr>

         @endif
         @if(isset($bonuses))
            @foreach($bonuses as $index=>$bonus)
               <tr>
                  <td class="text-center">{{$index+1}}</div>
                  <td class="text-center">{{$bonus->employee_id}}</div>
                  <td class="text-center">{{$bonus->name}}</div>
                  <td class="text-center">
                  @if($bonus->additional_count > 0)
                     <span class="label label-default custom-label">{{$bonus->additional_count}} Days Additional : &#x20B9 {{$bonus->additional_amount}}</span>
                  @endif
                  @if($bonus->onsite_count > 0)
                     <span class="label label-primary custom-label">{{$bonus->onsite_count}} Days Onsite : &#x20B9 {{$bonus->onsite_amount}}</span>
                  @endif
                  @if($bonus->extra_count > 0)
                     <span class="label label-danger custom-label">Extra Hour Bonus : &#x20B9 {{$bonus->extra_amount}}</span>
                  @endif
                  @if($bonus->performance_count > 0)
                     <span class="label label-success custom-label">Performance Bonus : &#x20B9 {{$bonus->performance_amount}}</span>
                  @endif
                  @if($bonus->techtalk_count > 0)
                     <span class="label label-warning custom-label">Tech-Talk Bonus : &#x20B9 {{$bonus->techtalk_amount}}</span>
                  @endif
                  @if($bonus->referral_count > 0)
                     <span class="label label-info custom-label">Referral Bonus : &#x20B9 {{$bonus->referral_amount}}</span>
                  @endif
                  
                  </div>
                  <td class="text-center">
                  @if($requestType=="due")
                  <a class="btn btn-xs btn-success" href="@if($currMonth) /admin/bonus-due/{{$bonus->user_id}}/user @else {{$bonus->user_id}}/user @endif"><i class="fa fa-eye"></i> View</a>
                  @elseif(($requestType=="all"))
                  <a class="btn btn-success" href="{{$monthObj->id}}/{{$bonus->user_id}}"><i class="fa fa-eye"></i> View</a>
                  @endif
                 </tr>
            @endforeach
         @endif
        </table>
    </div>
    <script>
         $("#lock").click(function(){
            $confirm =  confirm('Are you sure you want to lock bonus from payment processing for this month?');
            if($confirm==true){
               window.location.href='bonus-due/lock';
            }
         });
         $('#selectid2').change(function(){
            var optionSelected = $("option:selected", this);
            optionValue = this.value;
            if (optionValue) { 
                  window.location = "/admin/bonus-due/"+optionValue; 
            }
         });
         $(function(){
            $tableHeight = $( window ).height();
            $('#bonus-table').DataTable({
                "pageLength": 500,
                scrollY: $tableHeight - 200,
                scrollX: true,
                scrollCollapse: true,
            });
        });
    </script>
@endsection
