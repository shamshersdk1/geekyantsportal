
@section('content')

<section class="forget-password">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3 forget-wrap">
				@if(!empty($errors->all()))
					<div class="alert alert-danger">
						@foreach ($errors->all() as $error)
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							<span>{{ $error }}</span><br/>
					  	@endforeach
					</div>
				@endif
				<h2 class="text-uppercase text-center forget-heading">forgot Password</h2>
				<h5 class="text-capitalize text-center forget-des">Enter your email adress and we'll email you a link to reset <br>your password.</h5>

				<form role="form" method="post" action="/password/forgot" id="floating-label" class="form-input">
				 <input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="input-group forgetpwd-inputbox">
					  	<span class="input-group-addon" id="sizing-addon2"><span class="glyphicon glyphicon-envelope"></span></span>
					  	<input type="email" name="email" class="form-control" placeholder="Email" aria-describedby="sizing-addon2">
					</div>
					<div class="forgetpwd-btn">
						<a href=""><button class="btn btn-default center-block">Send</button></a>
					</div>	
				</form>
			</div>
		</div>
	</div>
</section>

