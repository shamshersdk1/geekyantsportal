@extends('layouts.admin-dashboard')
@section('main-content')
    <div class="container-fluid">
        <div class="breadcrumb-wrap">
            <div class="flex-class align-items-center">
                <div>
                    <h1>Contact List</h1>
                    <ol class="breadcrumb">
                        <li><a href="/admin">Admin</a></li>
                        <li><a href="/admin/user-contacts">Contact List</a></li>
                        <li class="active">Add</li>
                    </ol>
                </div>
                <div>
                    
                </div>
            </div>
        </div>
        @if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span><br/>
                  @endforeach
            </div>
        @endif
        @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
        @endif
        <div style="margin-top:30px;">    
            <form class="form-horizontal" method="post" action="/admin/user-contacts" enctype="multipart/form-data">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">User* : </label>
                        <div class="col-sm-8">
                            <select class="user_id" id="user_id" name="user_id" style="width=35%;">
                                <option value=""></option>
                                @foreach($users as $user)
                                    @if ( old('user_id') == $user->id )
                                        <option value="{{$user->id}}" selected >{{$user->name}}</option>
                                    @else
                                        <option value="{{$user->id}}" >{{$user->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Mobile* :</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="mobile" name="mobile" value="{{old('mobile')}}" placeholder="Mobile Number"> </input>  
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-2 control-label">Emergency Contact Number* : </label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="emergency_contact_number" name="emergency_contact_number" value="{{old('emergency_contact_number')}}" placeholder="Emergency Contact Number"> </input>  
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Personal Email* :</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="personal_email" name="personal_email"  value="{{old('personal_email')}}" placeholder="Enter personal email"> </input> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Office Email* :</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="office_email" name="office_email"  value="{{old('office_email')}}" placeholder="Enter office email"> </input> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Skype Id :</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="skype_id" name="skype_id"  value="{{old('skype_id')}}" placeholder="Enter skype id"> </input> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Git Hub URL :</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="git_hub_url" name="git_hub_url"  value="{{old('git_hub_url')}}" placeholder="Enter github url"> </input> 
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Bit Bucket URL :</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="bit_bucket_url" name="bit_bucket_url"  value="{{old('bit_bucket_url')}}" placeholder="Enter bit bucket url"> </input> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Apple ID :</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="apple_id" name="apple_id"  value="{{old('apple_id')}}" placeholder="Enter apple id"> </input> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Address Line 1 :</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="address_line_1" name="address_line_1" value="{{old('address_line_1')}}" placeholder="Enter address line 1"> </input> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Address Line 2 :</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="address_line_2" name="address_line_2" value="{{old('address_line_2')}}" placeholder="Enter address line 2"> </input> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">City :</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="city" name="city" value="{{old('city')}}" placeholder="Enter city"> </input> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">State :</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="state" name="state" value="{{old('state')}}" placeholder="Enter state"> </input> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Pin Code :</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="pin_code" name="pin_code" value="{{old('pin_code')}}" placeholder="Enter pin code"> </input> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center" style="margin-top: 30px;">
            <button type="submit" class="btn btn-primary btn-sm crude-btn "> SAVE</button>
        </div>
        </form>
    </div>

<script type="text/javascript">
    $(function () {
        $('.user_id').select2({
            placeholder: 'Select an option',
            allowClear:true
    });

    });
</script>
@endsection
