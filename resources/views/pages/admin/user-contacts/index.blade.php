@extends('layouts.admin-dashboard')
@section('page_heading','Contact List')

@section('main-content')
<div class="container-fluid">
<div class="breadcrumb-wrap">
   <div class="flex-class justify-content-between align-items-center">
      <div>
         <h1 class="admin-page-title">Contact List</h1>
         <ol class="breadcrumb">
            <li><a href="/admin">Admin</a></li>
            <li class="active">Contact List </li>
         </ol>
      </div>
      <div>
            <a href="/admin/user-contacts/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Add User Contact</a>
      </div>
   </div>
</div>
<div class="row user-contact">
   <div class="col-md-12 ">
      <div class="panel panel-default ">
       
            <table class="table contact-list" id="conatct">
                  <thead>
                  <th>Contact</th>
                  <th>EMP CODE</th>
                  <th>Social Contact</th>
                  <th>Personal Info</th>
                  <th>Emergency Contact</th>
                  <th>Action</th>

                  </thead>
                  <tbody>
                  @if( !empty($userContactList) )
                        @foreach ( $userContactList as $userContact )
                        <tr>
                              <td>
                                    <div class="">
                                          <div class="main-contact flex-class">
                                                <div class="user-img">
                                                      <div class="img-container"
                                                      style="background:url({{!empty($userContact->profile->image) ? $userContact->profile->image : '' }});  background-size:100% 100%">
                                                      </div>
                                                </div>
                                                <div class="user-detail">
                                                      <div class="user-name">
                                                            {{!empty($userContact->user->name) ? $userContact->user->name : '' }}
                                                            <br/>
                                                            <a href="mailto:{{$userContact->office_email}}">{{$userContact->office_email}}</a>
                                                      </div>
                                                      <div class="user-number">
                                                            <i class="fa fa-phone"></i>
                                                            <a href="tel:{{$userContact->mobile}}">
                                                            {{$userContact->mobile}}</a>
                                                      </div>
                                                </div>
                                          </div>
                                    </div>
                              </td>
                              <td>
                                    {{$userContact->user->employee_id}}
                              </td>
                              <td>
                                    <div class="">
                                          @if( !empty($userContact->skype_id) )
                                          <span>
                                                <i class="fa fa-skype"></i>
                                          </span>
                                          <a href="skype:{{$userContact->skype_id}}">{{$userContact->skype_id}}</a>
                                          <br>
                                          @endif
                                          @if( !empty($userContact->apple_id) )
                                          <span>
                                                <i class="fa fa-apple"></i>
                                          </span>
                                          <span>
                                          <a href="" target="_blank">{{$userContact->apple_id}} </a>
                                          </span>
                                          <br>
                                          @endif
                                          @if( !empty($userContact->git_hub_url) )
                                          <span>
                                                <i class="fa fa-github"></i>
                                          </span>
                                          <span>
                                          <a href="{{$userContact->git_hub_url}}" target="_blank"> {{$userContact->git_hub_url}} </a>
                                          </span>
                                          <br>
                                          @endif
                                          @if( !empty($userContact->bit_bucket_url) )
                                          <span>
                                                <i class="fa fa-bitbucket" aria-hidden="true"></i>
                                          </span>
                                          <span>
                                                <a href="{{$userContact->bit_bucket_url}}" target="_blank" > {{$userContact->bit_bucket_url}} </a>
                                          </span>
                                          @endif
                                    </div>
                              </td>
                              <td>
                                    <div class="contact-wrap-heading-emer">
                                          @if ( !empty($userContact->address_line_1) )
                                          <span>
                                                {{$userContact->address_line_1}},
                                          </span>
                                          <br>
                                          @endif
                                          @if ( !empty($userContact->address_line_2) )
                                          <span>
                                                {{$userContact->address_line_2}}
                                          </span>
                                          <br>
                                          @endif
                                          @if ( !empty($userContact->city) )
                                          <span>
                                                {{$userContact->city}}
                                          </span>
                                          <br>
                                          @endif
                                          @if ( !empty($userContact->state) )
                                          <span>
                                                {{$userContact->state}}
                                          </span>
                                          @endif
                                          @if ( !empty($userContact->pin_code) || ($userContact->pin_code != 0) )
                                          <span>
                                                {{$userContact->pin_code}}
                                          </span>
                                          @endif
                                    </div>
                              </td>
                              <td>
                                    <div class="">
                                          <div class="emergency-num">
                                                <div class="num">
                                                      <i class="fa fa-phone"></i>
                                                      <a href="tel:{{$userContact->emergency_contact_number}}">
                                                      {{$userContact->emergency_contact_number}}</a>
                                                </div>
                                          </div>
                                          <div class="emergency-num">
                                                <i class="fa fa-envelope"></i>
                                                <a href="mailto:{{$userContact->personal_email}}" target="_blank"> {{$userContact->personal_email}}</a>
                                    </div>
                              </td>

                              <td >
                                    <div class="last">
                                    <div>
                                          <div class="onoffswitch">

                                                <input type="checkbox" value="{{$userContact->id}}" name="onoffswitch[]" onchange="toggle(this)"
                                                class="onoffswitch-checkbox" id="{{$userContact->id}}"
                                                <?php echo (!empty($userContact->status) && $userContact->status == 1) ? 'checked' : ''; ?>>
                                                <label class="onoffswitch-label" for= {{$userContact->id}} >
                                                      <span class="onoffswitch-inner"></span>
                                                      <span class="onoffswitch-switch"></span>
                                                </label>
                                          </div>
                                    </div>
                                          <span class="">
                                                <a href="/admin/user-contacts/{{$userContact->id}}/edit" class="btn btn-info btn-sm crude-btn">Update Info</a>
                                          </span>
                                          <span>
                                                <a id="delete-button" onclick="deleteConfirm({{$userContact->id}})" name="delete-button" class="btn btn-danger btn-sm" >Delete</a>
                                          </span>

                                    </div>
                              </td>

                        </tr>
                        @endforeach
                  @endif
                  </tbody>
            </table>
      
         </div>
      </div>
   </div>
</div>

<script type="text/javascript">
   $(function(){
             $(".select-contact").select2({
   placeholder: "All Contacts",
   allowClear: true
   });
   });
   $(document).ready(function() {
    $('#conatct').DataTable( {
        "scrollX": true,
        "pageLength": 50,
       
        "columnDefs": [
            {
                "targets": [ 1 ],
                "visible": false,
                "searchable": false
            }
        ],
        "order": [[ 1, "asc" ]]
    } );
} );
</script>
<script>
    $(function(){
        toggle = function (item) {
              	$.ajax({
				type:'POST',
				url:'../api/v1/user-contacts/status',
				data:{id:item.value},
				success:function(data){
					console.log(data.success);
					}
			});
		}
    });
</script>
<script>
$(function(){
        deleteConfirm = function (item) {
			r = confirm("Are you sure that you want to delete this user contact details?");
                  if (r == true) {
                        $.ajax({
				type:'POST',
				url:'../api/v1/user-contacts/delete',
                        data:{id:item},
				success:function(data){
					location.reload();
					}
			      });
                  }
		}
    });
</script>
@endsection