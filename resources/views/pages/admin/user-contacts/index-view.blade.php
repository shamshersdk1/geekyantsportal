@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
<div class="breadcrumb-wrap">
   <div class="row">
      <div class="col-sm-8">
         <h1 class="admin-page-title">Contact List</h1>
         <ol class="breadcrumb">
            <li><a href="/admin">Admin</a></li>
            <li class="active">Contact List </li>
         </ol>
      </div>
   </div>
</div>
<div class="row user-contact">
   <div class="col-md-12 ">
      <div class="panel panel-default ">
      <div class="table-responisve">
         <table class="table" id="conatct">
            <thead>
               <th>Contact</th>
               <th>Employee Code</th>
               <th>Social Contact</th>
               <th>Personal Info</th>
               <th>Emergency Contact</th>
            </thead>
            <tbody>
            @if( !empty($userContactList) )
                  @foreach ( $userContactList as $userContact )
                  <tr>
                        <td>
                              <div class="">
                                    <div class="main-contact flex-class">
                                          <div class="user-img">
                                                <div class="img-container"
                                                style="background:url({{!empty($userContact->profile->image) ? $userContact->profile->image : '' }});  background-size:100% 100%">
                                                </div>
                                          </div>
                                          <div class="user-detail">
                                                <div class="user-name">
                                                      {{!empty($userContact->user->name) ? $userContact->user->name : '' }}
                                                      <br/>
                                                      <a href="mailto:{{$userContact->office_email}}">{{$userContact->office_email}}</a>
                                                </div>
                                                <div class="user-number">
                                                      <i class="fa fa-phone"></i>
                                                      <a href="tel:{{$userContact->mobile}}">
                                                      {{$userContact->mobile}}</a>
                                                </div>
                                          </div>
                                    </div>
                              </div>
                        </td>
                        <td>
                              {{$userContact->user->employee_id}}
                        </td>
                        <td>
                              <div class="">
                                    @if( !empty($userContact->skype_id) )
                                    <span>
                                          <i class="fa fa-skype"></i>
                                    </span>
                                    <a href="skype:{{$userContact->skype_id}}">{{$userContact->skype_id}}</a>
                                    <br>
                                    @endif
                                    @if( !empty($userContact->apple_id) )
                                    <span>
                                          <i class="fa fa-apple"></i>
                                    </span>
                                    <span>
                                    <a href="" target="_blank">{{$userContact->apple_id}} </a>
                                    </span>
                                    <br>
                                    @endif
                                    @if( !empty($userContact->git_hub_url) )
                                    <span>
                                          <i class="fa fa-github"></i>
                                    </span>
                                    <span>
                                    <a href="{{$userContact->git_hub_url}}" target="_blank"> {{$userContact->git_hub_url}} </a>
                                    </span>
                                    <br>
                                    @endif
                                    @if( !empty($userContact->bit_bucket_url) )
                                    <span>
                                          <i class="fa fa-bitbucket" aria-hidden="true"></i>
                                    </span>
                                    <span>
                                          <a href="{{$userContact->bit_bucket_url}}" target="_blank" > {{$userContact->bit_bucket_url}} </a>
                                    </span>
                                    @endif
                              </div>
                        </td>
                        <td>
                              <div class="contact-wrap-heading-emer">
                                    @if ( !empty($userContact->address_line_1) )
                                    <span>
                                          {{$userContact->address_line_1}},
                                    </span>
                                    <br>
                                    @endif
                                    @if ( !empty($userContact->address_line_2) )
                                    <span>
                                          {{$userContact->address_line_2}}
                                    </span>
                                    <br>
                                    @endif
                                    @if ( !empty($userContact->city) )
                                    <span>
                                          {{$userContact->city}}
                                    </span>
                                    <br>
                                    @endif
                                    @if ( !empty($userContact->state) )
                                    <span>
                                          {{$userContact->state}}
                                    </span>
                                    @endif
                                    @if ( !empty($userContact->pin_code) || ($userContact->pin_code != 0) )
                                    <span>
                                          {{$userContact->pin_code}}
                                    </span>
                                    @endif
                              </div>
                        </td>
                         <td>
                              <div class="">
                                    <div class="emergency-num">
                                          <div class="num">
                                                <i class="fa fa-phone"></i>
                                                <a href="tel:{{$userContact->emergency_contact_number}}">
                                                {{$userContact->emergency_contact_number}}</a>
                                          </div>
                                    </div>
                                    <div class="emergency-num">
                                          <i class="fa fa-envelope"></i>
                                          <a href="mailto:{{$userContact->personal_email}}" target="_blank"> {{$userContact->personal_email}}</a>
                                    </spanclass>
                              </div>
                        </td>
                  </tr>
                  @endforeach
               @endif
            </tbody>
         </table>
      </div>
         </div>
      </div>
   </div>
</div>

<script type="text/javascript">
   $(function(){
             $(".select-contact").select2({
   placeholder: "All Contacts",
   allowClear: true
   });
   });
   $(document).ready(function() {
    $('#conatct').DataTable( {
        "scrollX": true,
        "pageLength": 50,
        fixedColumns:   {
            leftColumns: 1,
            rightColumns: 2
        },
        "columnDefs": [
            {
                "targets": [ 1 ],
                "visible": false,
                "searchable": false
            }
        ],
        "order": [[ 1, "asc" ]]
    } );
} );
</script>
@endsection
