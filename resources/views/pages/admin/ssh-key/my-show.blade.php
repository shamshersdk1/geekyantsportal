@extends('layouts.admin-dashboard')
@section('main-content')
<section>
   <div class="container-fluid">
      <div class="breadcrumb-wrap">
         <div class="row">
            <div class="col-sm-6">
               <h1>SSH Keys</h1>
               <ol class="breadcrumb">
                  <li><a href="/admin">Admin</a></li>
                  <li><a href="/admin/ssh-keys">SSH Keys</a></li>
                  <li class="active">{{$sshKeyObj->id}}</li>
               </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
              <a href="/admin/ssh-keys/my-ssh-key/edit" class="btn btn-primary"></i>Edit</a>
            </div>
         </div>
      </div>
      @if(!empty($errors->all()))
      <div class="alert alert-danger">
         @foreach ($errors->all() as $error)
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
         <span>{{ $error }}</span><br/>
         @endforeach
      </div>
      @endif
      @if (session('message'))
      <div class="alert alert-success">
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
         <span>{{ session('message') }}</span><br/>
      </div>
      @endif
      <div class="row cms-dev">
         <div class="col-md-12">
            <h4 >SSH Key Detail</h4>
            <div class="panel panel-default">
               <div class="panel-body row">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <div class="col-md-12">
                     <label class="col-md-2">User : </label>
                     <span class="col-md-8">{{$sshKeyObj->user->name}}</span>
                  </div>
                  <div class="col-md-12">
                     <label class="col-md-2">SSH Key : </label>
                     <div class="col-md-8" style="word-break: break-all">{{$sshKeyObj->value}}</div>
                  </div>
                  <div class=" col-md-12">
                     <label class="col-md-2">Verified : </label>
                     <span class="col-md-8">{{ $sshKeyObj->is_verified ? 'Yes' : 'No' }}</span>
                  </div>
                  <div class=" col-md-12">
                    <label class="col-md-2"> Updated On : </label>
                    <span class="col-md-8">{{ date_in_view($sshKeyObj->updated_at)  }}</span>
                 </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
@endsection