@extends('layouts.admin-dashboard')
@section('main-content')
<section>
   <div class="container-fluid">
      <div class="breadcrumb-wrap">
         <div class="row">
            <div class="col-sm-8">
               <h1>SSH Keys</h1>
               <ol class="breadcrumb">
                  <li><a href="/admin">Admin</a></li>
                  <li><a href="/admin/ssh-keys">SSH Key Details</a></li>
                  <li class="active">{{$sshKeyObj->user->name}} (#{{$sshKeyObj->id}})</li>
               </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
         </div>
      </div>
      @if(!empty($errors->all()))
      <div class="alert alert-danger">
         @foreach ($errors->all() as $error)
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
         <span>{{ $error }}</span><br/>
         @endforeach
      </div>
      @endif
      @if (session('message'))
      <div class="alert alert-success">
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
         <span>{{ session('message') }}</span><br/>
      </div>
      @endif
      <div class="row cms-dev">
         <div class="col-sm-8 col-sm-offset-2">
            
            <div class="panel panel-default">
               <div class="panel-body">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <p>
                     {{$sshKeyObj->user->name}}
                     <span class="pull-right">{{ $sshKeyObj->is_verified ? '<span class="label label-success custom-label"><i class="fa fa-check"></i> Verified</span>' : '<span class="label label-warning custom-label"><i class="fa fa-exclamation-triangle"></i> Not Verified</span>' }}</span>
                  </p>
                  <div class="well well-sm m-b-10" style="word-break: break-all">{{$sshKeyObj->value}}</div>
                  <div class="clearfix">
                     <small> Updated On : {{ date_in_view($sshKeyObj->updated_at)  }}</small>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
@endsection