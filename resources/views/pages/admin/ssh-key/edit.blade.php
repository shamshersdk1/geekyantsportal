@extends('layouts.admin-dashboard')
@section('main-content')

<section class="">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">SSH Keys</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
			  			<li><a href="{{ url('admin/ssh-keys') }}">SSH Keys</a></li>
			  			<li class="active">{{ $sshKeyObj->id }}</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>

	    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif

		<form class="form-horizontal" method="post" action="/admin/ssh-keys/{{$sshKeyObj->id}}" enctype="multipart/form-data">
			<input name="_method" type="hidden" value="PUT" />
			<div class="row">
		    	<div class="col-sm-8 col-sm-offset-2">
		    		<div class="panel panel-default">
						<div class="panel-body">
							<div class="form-group">
						    	<div class="col-sm-8">
						    		<label>User:</label>
							    	<p class="no-margin">{{$sshKeyObj->user->name}}</p>
								</div>
								<div class="col-sm-4">
									<label>Verified SSH Key: </label>
									<div>
										@if( $sshKeyObj->is_verified )
										<select class="form-control" name="is_verified">
											<option value="true" selected>Yes</option>
											<option value="false">No</option>
										</select>
										@else
										<select class="form-control" name="is_verified">
											<option value="true">Yes</option>
											<option value="false" selected>No</option>
										</select>
										@endif
									</div>
								</div>
							</div>
							<div class="row">
								<label class="col-sm-12">SSH Key:</label>
								<div class="col-sm-12">
									<textarea class="form-control" id="ssh_key" name="ssh_key" rows="10" placeholder="Enter details">{{$sshKeyObj->value}}</textarea>
								</div>
							</div>
					  	</div>
					</div>
				</div>
			</div>
  	  		<div class="text-center">
		  		<button type="submit" class="btn btn-success">Update SSH Key</button>
		  	</div>
		</form>
	</div>
</section>
<script type="text/javascript">
	$(function () {
		$('#datetimepicker1').datetimepicker({
			format: 'DD-MM-YYYY'
		});
		$('#datetimepicker2').datetimepicker({
			format: 'DD-MM-YYYY'
		});
	});
</script>
@endsection
