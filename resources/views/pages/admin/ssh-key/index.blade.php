@extends('layouts.admin-dashboard')
@section('page_heading','User SSH Keys')
@section('main-content')
<div class="container-fluid">
  <div class="breadcrumb-wrap">
    <div class="row">
      <div class="col-sm-8">
        <h1 class="admin-page-title">User SSH Keys</h1>
        <ol class="breadcrumb">
          <li><a href="/admin">Admin</a></li>
          <li class="active">SSH keys</li>
        </ol>
      </div>
    </div>
  </div>
  <div class="m-t-15">
    @if(session()->has('message'))
      <div class="alert alert-success">
          {{ session()->get('message') }}
      </div>
    @endif
    @if(session()->has('error'))
      <div class="alert alert-error">
          {{ session()->get('error') }}
      </div>
    @endif
    <div class="row">
      <div class="col-sm-6">
        <h4>List of SSH Key</h4>
      </div>
      <div class="col-sm-6"></div>
    </div>

    <div>
      <div class="panel panel-default">
        <table class="table" id="dt-ssh-key-list">
            <thead>
              <tr>
                    <th width="4%" class="sorting_asc">Employee ID</th>
                    <th width="8%">User</th>
                    <th width="58%">SSH Key</th>
                    <th width="15%" class="text-right">Verified & Updated</th>
                    <th class="text-right sorting_asc" width="15%">Actions</th>
                </tr>
            </thead>
            <tbody>
            @if(count($sshKeys) > 0)
                @foreach($sshKeys as $index => $sshKey)
                    <tr>
                        <td><a>{{$sshKey->user->employee_id}}</a></td>
                        <td>{{$sshKey->user->name}}</td>
                        <td style="word-break: break-word; white-space: normal;" >{{$sshKey->value}}</td>
                        <td class="text-right">
                            <div class="m-b-10">
                                {{$sshKey->is_verified ? '<span class="label label-success custom-label"><i class="fa fa-check"></i> Yes</span>' : '<span class="label label-warning custom-label"><i class="fa fa-exclamation-triangle"></i> No</span>' }}
                            </div>
                            {{datetime_in_view($sshKey->updated_at)}}
                        </td>
                        <td class="text-right">
                            <a href="/admin/ssh-keys/{{$sshKey->id}}" class="btn btn-info btn-sm crude-btn">View</a>
                            <a href="/admin/ssh-keys/{{$sshKey->id}}/edit" class="btn btn-primary btn-sm crude-btn">Edit</a>
                            <div style="display: inline-block;">
                              {{ Form::open(['url' => '/admin/ssh-keys/'.$sshKey->id, 'method' => 'delete']) }}
                              {{ Form::submit('Delete',['class' => 'btn btn-danger btn-sm ','onclick' => 'return confirm("Are you sure you want to delete this item?")']) }}
                              {{ Form::close() }}
                            </div>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="3" class="text-center">No results found.</td>
                </tr>
            @endif
            </tbody>
            <tfoot>
            <tr>
              <th width="4%">Employee ID</th>
              <th width="8%">User</td>
              <th width="58%">SSH Key</th>
              <th width="15%">Verified</th>
              <th width="15%" class="text-right">Active / Tick Counts</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function() {
    $('#dt-ssh-key-list').DataTable({paging: false});
    $("#delete-user-ssh-key").click(function(){
        $confirm =  confirm('Are you sure you want to lock bonus from payment processing for this month?');
        if($confirm==true){
           window.location.href='bonus-due/lock';
        }
     });
} );
</script>
@endsection