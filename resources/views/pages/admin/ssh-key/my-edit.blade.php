@extends('layouts.admin-dashboard')
@section('main-content')

<section class="new-project-section">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">SSH Keys</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
						  <li><a href="{{ url('admin/ssh-keys') }}">SSH Keys</a></li>
						  <li><a>My SSH Key</a></li>
			  			<li class="active">{{ $sshKeyObj->id }}</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>

	    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif

		<form class="form-horizontal" method="post" action="/admin/ssh-keys/my-ssh-key/edit/{{$sshKeyObj->id}}" enctype="multipart/form-data">
			<input name="_method" type="hidden" value="PUT" />
			<div class="panel panel-default">
				<div class="panel-body">
				    <div class="row">
				    	<div class="col-md-12">
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">User: </label>
									<div class="col-sm-8">{{$sshKeyObj->user->name}}</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">SSH Key: </label>
								<div class="col-sm-8">
									<textarea class="form-control" id="ssh_key" name="ssh_key" rows="10" placeholder="Enter details">{{$sshKeyObj->value}}</textarea>
								</div>
							</div>
							
					  	</div>
					</div>
				</div>
			</div>
  	  		<div class="text-center">
		  		<button type="submit" class="btn btn-success">Update SSH Key</button>
		  	</div>
		</form>
	</div>
</section>
<script type="text/javascript">
	$(function () {
		$('#datetimepicker1').datetimepicker({
			format: 'DD-MM-YYYY'
		});
		$('#datetimepicker2').datetimepicker({
			format: 'DD-MM-YYYY'
		});
	});
</script>
@endsection
