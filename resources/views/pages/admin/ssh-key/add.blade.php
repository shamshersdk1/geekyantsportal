@extends('layouts.admin-dashboard')
@section('main-content')

<section class="new-project-section">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">SSH Keys</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
			  			<li><a href="{{ url('admin/ssh-keys') }}">SSH Keys</a></li>
			  			<li class="active">Add</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>

	    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif

		<form class="form-horizontal" method="post" action="/admin/ssh-keys" enctype="multipart/form-data">
			<div class="row">
		    	<div class="col-sm-8 col-sm-offset-2">
		    		<div class="panel panel-default">
						<div class="panel-body">
							<div class="form-group">
						    	<label class="col-sm-12">User: </label>
						    	<div class="col-sm-8">
									@if(!empty($users))
										<select id="selectid3" name="user_id" style="width=35%;" placeholder= "Select an option">
											<option value=""></option>
												@foreach($users as $x)
													<option value="{{$x->id}}" >{{$x->name}}</option>
												@endforeach
										</select>
									@endif
								</div>
							</div>
							<div class="row">
								<label class="col-sm-12">SSH Key: </label>
								<div class="col-sm-12">
									<textarea class="form-control" id="ssh_key" name="ssh_key" rows="4" placeholder="Enter details">{{old('ssh_key')}}</textarea>
								</div>
							</div>
					  	</div>
					</div>
				</div>
			</div>
  	  		<div class="text-center">
		  		<button type="submit" class="btn btn-success">Add SSH Key</button>
		  	</div>
		</form>
	</div>
</section>
@endsection
