@extends('layouts.admin-dashboard')
@section('main-content')
<script type="text/javascript">
	window.users = '<?php echo json_encode($users); ?>';
</script>
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1>Assign Ip</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li class="active">Assign Ip </li>
                </ol>
            </div>
            </div>
        </div>
    </div>
    <!-- Message block -->
    @if(!empty($errors->all()))
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ $error }}</span><br/>
                @endforeach
        </div>
    @endif
    @if (session('message'))
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <span>{{ session('message') }}</span><br/>
        </div>
    @endif

    <div class="user-list-view" ng-app="myApp" ng-controller="assignIpCtrl" style="margin:10px;" >
      <div class="panel panel-default">         
         <table id="user_ip_table" class="table table-striped table-condensed1">
            <thead>
                <th class="text-left">Name</th>
                <th class="text-left">Ip Address</th>
                <th class="text-right">Actions</th>
            </thead>
            <tbody>
                <tr ng-repeat="user in users">
                    <td><span class="no-margin">%%user.name%%</span></td>
                    <td><span class="no-margin" ng-repeat="ip in user.ip_address">%%ip.ip_address%%</br></span></td>  
                    <td class="text-right">
                        <span>
                            <button class="btn btn-success crude-btn crendentail-modal" 
                                ng-click="showModal(user)">
                                <i class="fa fa-plus"></i> Assign 
                            </button>
                        </span>
                    </td>
                </tr>
            </tbody>
         </table>
      </div>
   </div>
</div>


<script>
$(function(){    
    $("td").click(function(){
    var range = document.createRange();
    range.selectNodeContents(this);  
    var sel = window.getSelection(); 
    sel.removeAllRanges(); 
    sel.addRange(range);
    document.execCommand("Copy");
});
});
</script>
@endsection
