@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Dashboard Events</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/dashboard-events') }}">Events</a></li>
                </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
				<button class="btn btn-success" type="button" data-toggle="modal" data-target="#addEventModal"><i class="fa fa-plus fa-fw"></i>Add New Event</button>
            </div>
        </div>
    </div>
    <div class="row">
            <!-- <ol class="breadcrumb">
                <h2 class="breadcrumb-head-page">Technology Add</h2>
                <li><a href="/admin">Admin</a></li>
                <li><a href="{{ url('admin/cms-technology') }}">Technology</a></li>
                <li><a href="{{ url('admin/cms-technology') }}">Add</a></li>
            </ol> -->
        <div class="col-md-12">
            @if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                    @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif   
            <div class="panel panel-default">
                <table class="table table-striped">
                    <thead >
                        <th>#</th>
                        <th>Name of Employee</th>
                        <th>Event</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th class="text-right">Actions</th>
                    </thead>
                    @if(count($events) > 0)
                        @foreach($events as $event)
                            <tr>
                                <td class="td-text">{{$event->id}}</td>
                                <td class="td-text">{{$event->user->name}}</td>
                                <td class="td-text">{{$event->title}}</td>
                                <td class="td-text">{{date('j F Y',strtotime($event->date))}}</td>
                                <td class="td-text">{{date('g:i A',strtotime($event->time))}}</td>
                                <td class="text-right">
                                    <button class="btn btn-info btn-sm" type="button"
									data-toggle="modal" data-target="#editEventModal"
									data-id="{{$event->id}}" data-title="Algorithms"><i class="fa fa-pencil fa-fw"></i>Edit</button>                                    <form action="/admin/dashboard-events/{{$event->id}}" method="post" style="display:inline-block;" class="deleteform">
                                        <input name="_method" type="hidden" value="DELETE">
                                        <button class="btn btn-danger crud-btn btn-sm" type="submit"><i class="fa fa-trash btn-icon-space" aria-hidden="true"></i>Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="6">No results found.</td>
                        </tr>
                    @endif
                </table>
            </div>
        </div>		
	</div>
</div>
<script>
		$(function(){
		$(".deleteform").submit(function(event){
                return confirm('Are you sure?');
            });
        });
</script>
@include('pages/admin/dashboard-events/modals/add-event-modal')
@include('pages/admin/dashboard-events/modals/edit-event-modal')
<script>
	let event;
	function updateEvent() {
		event.name = $('#editEventModal input[name="name"]').val();
		event.title = $('#editEventModal input[name="title"]').val();
		event.time = $('#editEventModal input[name="time"]').val();
		event.date = $('#editEventModal input[name="date"]').val();
		$.ajax({
			type: 'PUT',
			url: '/admin/dashboard-events/'+event.id,
			dataType: 'text',
			data: event,
			beforeSend: function() {
				$("div.loading").show();
				$("div.content").hide();
			},
			complete: function() {
				$("div.loading").hide();
				$("div.content").show();
			},
			success: function(data) {
				$('#editEventModal').modal('hide');
				window.location.reload();
			},
			error: function(errorResponse){
				$('#editEventModal').modal('hide');
				console.log(errorResponse);
				window.location.reload();
			}
		});
	}
	$(function(){
		$("#addEventModal").on("show.bs.modal", function(){
			$(this).find("input")
				.val('')
				.end()
		});

		$("#addEventModal").on("hidden.bs.modal", function(){
			$(this).find("input")
				.val('')
				.end()
		});
		$('#editEventModal').on("show.bs.modal", function (e) {
			let id = $(e.relatedTarget).data('id')
			$.ajax({
				type: 'GET',
				url: '/admin/dashboard-events/'+id,
				beforeSend: function() {
					$("div.loading").show();
					$("div.content").hide();
				},
				complete: function() {
					$("div.loading").hide();
					$("div.content").show();
				},
				success: function(data) {
					event = data;
					$('#editEventModal input[name="name"]').val(data.name);
					$('#editEventModal input[name="title"]').val(data.title);
					$('#editEventModal input[name="date"]').val(data.date);
					$('#editEventModal input[name="time"]').val(data.time);
				}
			});
		});

		$("#editEventModal").on("hidden.bs.modal", function(){
			$(this).find("input")
				.val('')
				.end()
		});
	});
</script>
@endsection