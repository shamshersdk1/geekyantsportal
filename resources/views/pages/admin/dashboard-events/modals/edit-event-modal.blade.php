<div class="modal fade" id="editEventModal" tabindex="-1" role="dialog" aria-labelledby="editEventModalLabel">
   <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="editEventModalLabel">Edit Event</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 ma-bot-10">
                        <div class="form-group ma-bot-10">
                            <div class="col-md-12" style="margin-bottom:3%">
                                <label for="title">Select User:</label>
                                <input type="text" name="name" class="form-control" placeholder="Select the user" value="{{ old('name') }}" readonly required>
                            </div>
                            <div class="col-md-12" style="margin-bottom:3%">
                                <label for="title">Enter Event Title:</label>
                                <input type="text" name="title" class="form-control" placeholder="Enter title" value="{{ old('title') }}" required>
                            </div>
                            <div class="col-md-6">
                                <label for="date">Date:</label>
                                <div class='input-group date' id='adddatetimepicker'>
                                    <input type='text' class="form-control" autocomplete="off" name="date" value="{{old('date')}}"  placeholder="Date" required/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label>Time:</label>
                                <input class="timeTextBox form-control" style="position:absolute;width:80%" name="time" autocomplete="off" value="{{old('time')}}" maxlength="5"/>
                                <select class="editBox form-control">        
                                    <option value="1">01:00</option>
                                    <option value="2">02:00</option>
                                    <option value="3">03:00</option>
                                    <option value="4">04:00</option>
                                    <option value="5">05:00</option>
                                    <option value="6">06:00</option>
                                    <option value="7">07:00</option>
                                    <option value="8">08:00</option>
                                    <option value="9">09:00</option>
                                    <option value="10">10:00</option>
                                    <option value="11">11:00</option>
                                    <option value="12">12:00</option>
                                    <option value="13">13:00</option>
                                    <option value="14">14:00</option>
                                    <option value="15">15:00</option>
                                    <option value="16">16:00</option>
                                    <option value="17">17:00</option>
                                    <option value="18">18:00</option>
                                    <option value="19">19:00</option>
                                    <option value="20">20:00</option>
                                    <option value="21">21:00</option>
                                    <option value="22">22:00</option>
                                    <option value="23">23:00</option>
                                    <option value="24">24:00</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>&nbsp;
                <span class="pull-right">
                    <button type="submit" class="btn btn-primary" onclick="updateEvent()">
                        Update
                    </button>
                </span>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        $('#adddatetimepicker').datetimepicker({
            format: 'YYYY-MM-DD'
        });   
        $(".editBox").change(function(){         
            $(".timeTextBox").val($(".editBox option:selected").html());
        });
    });
</script>
