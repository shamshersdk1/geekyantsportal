@extends(Request::is('admin/view/*')?'pages.admin.userView.partials.usernav':'pages.admin.userView.partials.nav')
@section('sub-content')
<style>
.panel.panel-horizontal {
    display:table;
    width:100%;
    margin-bottom:0px;
    border-style:solid;
    border-width:0px 0px 1px 0px !important;
}
.panel.panel-horizontal > .panel-heading, .panel.panel-horizontal > .panel-body{
    border-radius:0px;
    display:table-cell;
}
.panel.panel-horizontal > .panel-heading {
    width: 20%;
    background-color:#FFF;
    border-right: 1px solid #ddd;
    border-bottom:0;
}
.fab {
   width: 60px;
   height: 60px;
   background-color: #5CB85C;
   border-radius: 50%;
   box-shadow: 0 6px 10px 0 #666;
   
   font-size: 45px;
   line-height: 65px;
   color: white;
   text-align: center;
   
   position: fixed;
   right: 50px;
   bottom: 50px;
   
  transition: all 0.1s ease-in-out;
}

.fab:hover {
   box-shadow: 0 6px 14px 0 #666;
   transform: scale(1.05);
}
</style>
<script>
    $(function(){
        $(".deleteform").submit(function(event){
            return confirm('Are you sure?');
        });
    });
</script>
@if(count($appraisals) > 0)
    @foreach($appraisals as $appraisal)
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="panel panel-default panel-horizontal">
            <div class="panel-heading"@if($appraisal->is_active==1)style="border-left: 10px solid #269900"@endif>
                <h3 class="panel-title">{{$appraisal->name}}</h3>
                <small style="color:#aaa">{{date("j F Y", strtotime($appraisal->effective_date))}}</small>
                <small style="color:#aaa">@if(!empty($appraisal->end_date)) to {{date("j F Y", strtotime($appraisal->end_date))}}@endif</small>
            </div>
            <div class="panel-body appraisal-panel">
                <ul class="col-xs-8 col-sm-8 col-md-8" style="list-style-type: none;display:inline">
                    <li><strong>CTC : &#8377; {{$appraisal->annual_gross_salary+$appraisal->annual_bonus+$appraisal->monthly_var_bonus}}</strong></li>
                    <li>Annual Gross Salary: @if(empty($appraisal->annual_gross_salary)) - @else &#8377; {{$appraisal->annual_gross_salary}} @endif</li>
                    <li>Annual Bonus: @if(empty($appraisal->annual_bonus)) - @else &#8377; {{$appraisal->annual_bonus}} @endif</li>
                    <li>Monthly Variable Bonus (per annum): @if(empty($appraisal->monthly_var_bonus)) - @else &#8377; {{$appraisal->monthly_var_bonus}} @endif</li>
                </ul>
               @if(Auth::user()->isAdmin() || Auth::user()->hasRole('human-resources')|| $hasAccess )
                <div class="col-xs-4 col-sm-4 col-md-4 actionform text-right" style="float:right;padding-right:0px;margin-top:2%">
                    @if(!$appraisal->is_active)
                    <form style="display:inline" method="POST" action="/admin/users/{{$appraisal->user->id}}/appraisals/{{$appraisal->id}}">
                        {{method_field('PUT')}}
                        <button type="submit" class="btn btn-success crud-btn btn-sm" name="activate" value="activate">Set as active</button>
                    </form>
                    @endif
                    <a href="{{Request::url()}}/{{$appraisal->id}}/edit" class="btn btn-info crud-btn btn-sm"><i class="fa fa-pencil btn-icon-space" aria-hidden="true"></i>Edit</a>
                    <form action="{{Request::url()}}/{{$appraisal->id}}" method="post" style="display:inline-block;" class="deleteform">
                        <input name="_method" type="hidden" value="DELETE">
                        <button class="btn btn-danger crud-btn btn-sm" type="submit"><i class="fa fa-trash btn-icon-space" aria-hidden="true"></i>Delete</button>
                    </form>
                </div>
                @endif
            </div>
        </div>
    {{$appraisals->links()}}
    </div>
    @endforeach
    @else
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="panel panel-default panel-horizontal">
            <div class="panel-body col-sm-12">
                <ul style="list-style-type: none;display:inline">
                    <h5>No results Found</h5>
                </ul>
            </div>
        </div>
    </div>
    @endif
    @if(Request::is('admin/users/*'))
    <div class="fab">
        <a href="{{Request::url()}}/create" style="color:white"> + </a></div>
    @endif
</div>
@endsection