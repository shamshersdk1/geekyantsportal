@extends('pages.admin.userView.partials.nav')
@section('sub-content')
<script>
    $(function(){
        $('#datetimepickerStart').datetimepicker({
            format: 'YYYY-MM-DD'
        });
        $('#datetimepickerEnd').datetimepicker({
            format: 'YYYY-MM-DD'
        });
        $('#group option[value="{{old('group')}}"]').attr("selected",true);
        $('input[name="is_active"][value="{{old('is_active')}}"]').prop("checked",true);
    });
</script>
<div class="col-xs-12 col-sm-12 col-md-12">
    <form name = "myForm" method="POST" action="/admin/users/{{$id}}/appraisals" enctype="multipart/form-data" >
        <div class="panel panel-default">
            <div class="panel-heading" style="margin-bottom: 15px"><span class="mandatory">*</span> denotes mandatory field</div>
            <div class="row">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group col-md-6">
                    <label class="col-md-4" style="display:inline;padding-top:2%" align="left" for="name">Name  <span class="mandatory">*</span></label>
                    <div class="col-md-8">
                    @if($name=="")
                        <input type="text" id="name" name="name" class="form-control" placeholder="Name" required value="{{ old('name') }}">
                    @else
                        <input type="text" id="name" name="name" class="form-control" value="{{$name}}" required readonly>
                    @endif
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label class="col-md-4" style="display:inline;padding-top:2%" align="left" for="group">Payrole Group  <span class="mandatory">*</span></label>
                    <div class="col-md-8">
                        <select class="form-control" name="group" id="group" required>
                            <option value="">---Select Payrole Group---</option>
                            @foreach($payrolegroups as $group)
                                <option value="{{$group->id}}">{{$group->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label class="col-md-4" style="display:inline;padding-top:2%" align="left" for="name">Effective Date  <span class="mandatory">*</span></label>
                    <div class="col-md-8">
                        <div class='input-group date' id='datetimepickerStart'>
                        @if($name=="")
                            <input type='text' id="datetimeinputstart" class="form-control" value="{{old('start_date')}}" name="start_date" placeholder="Effective Date" required autocomplete="off" />
                        @else
                            <input type='text' id="datetimeinputstart" class="form-control" value="{{$date}}" name="start_date" placeholder="Effective Date" required readonly autocomplete="off" />
                        @endif
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label class="col-md-4" style="display:inline;padding-top:2%" align="left" for="name">Effective Till</label>
                    <div class="col-md-8">
                        <div class='input-group date' id='datetimepickerEnd'>
                            <input type='text' id="datetimeinputend" class="form-control" value="{{old('end_date')}}" name="end_date" placeholder="Effective Till" autocomplete="off" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label class="col-md-4" style="display:inline;padding-top:2%" align="left" for="annual_gross_salary">Annual Gross Salary  <span class="mandatory">*</span></label>
                    <div class="col-md-8">
                        <input type="text" name="annual_gross_salary" class="form-control" placeholder="Annual Gross Salary" required value="{{ old('annual_gross_salary') }}">
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label class="col-md-4" style="display:inline;padding-top:2%" align="left" for="monthly_var_bonus">Monthy Variable Bonus</label>
                    <div class="col-md-8">
                        <input type="text" name="monthly_var_bonus" class="form-control" placeholder="Quarterly Bonus" value="{{ old('monthly_var_bonus') }}">
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label class="col-md-4" style="display:inline;padding-top:2%" align="left" for="annual_bonus">Annual Bonus</label>
                    <div class="col-md-8">
                        <input type="text" name="annual_bonus" class="form-control" placeholder="Annual Bonus" value="{{ old('annual_bonus') }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center">
            <button type="reset" class="btn btn-default"><i class="fa fa-rotate-left fa-fw"></i> Clear</button> 
            <button type="submit" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Save</button>
        </div>
    </form>
</div>
@endsection