@extends('pages.admin.userView.partials.nav')
@section('sub-content')
<script>
    $(function () { 
        $('#datetimepickerStart').datetimepicker({
            format: 'YYYY-MM-DD'
        });
        $('#datetimepickerEnd').datetimepicker({
            format: 'YYYY-MM-DD'
        });
        $('#group option[value="{{$appraisal->payrole_group_id}}"]').attr("selected",true);
        $('input[name="is_active"][value="{{$appraisal->is_active}}"]').prop("checked",true);
    });
</script>
<div class="col-xs-12 col-sm-12 col-md-12">
    <form name = "myForm" method="POST" action="/admin/users/{{$uid}}/appraisals/{{$appraisal->id}}" enctype="multipart/form-data" class="user-list-view">
        {{method_field('PUT')}}
        <div class="panel panel-default">
            <div class="panel-heading" style="margin-bottom: 15px"><span class="mandatory">*</span> denotes mandatory field</div>
            <div class="row">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group col-md-6">
                    <label class="col-md-4" style="display:inline;padding-top:2%" align="left" for="name">Name  <span class="mandatory">*</span></label>
                    <div class="col-md-8">
                        <input type="text" id="name" name="name" class="form-control" placeholder="Name" required value="{{$appraisal->name}}">
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label class="col-md-4" style="display:inline;padding-top:2%" align="left" for="group">Payrole Group  <span class="mandatory">*</span></label>
                    <div class="col-md-8">
                        <select class="form-control" name="group" id="group" required>
                            <option value="">---Select Payrole Group---</option>
                            @foreach($payrolegroups as $group)
                                <option value="{{$group->id}}">{{$group->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label class="col-md-4" style="display:inline;padding-top:2%" align="left" for="name">Effective Date  <span class="mandatory">*</span></label>
                    <div class="col-md-8">
                        <div class='input-group date' id='datetimepickerStart'>
                            <input type='text' id="datetimeinputstart" class="form-control" value="{{$appraisal->effective_date}}" name="start_date" placeholder="Effective Date" required autocomplete="off" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label class="col-md-4" style="display:inline;padding-top:2%" align="left" for="name">Effective Till</label>
                    <div class="col-md-8">
                        <div class='input-group date' id='datetimepickerEnd'>
                            <input type='text' id="datetimeinputend" class="form-control" value="@if(!empty($appraisal->end_date)){{$appraisal->end_date}}@endif" name="end_date" placeholder="Effective Till" autocomplete="off" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label class="col-md-4" style="display:inline;padding-top:2%" align="left" for="in_hand">Annual Gross Salary  <span class="mandatory">*</span></label>
                    <div class="col-md-8">
                        <input type="text" name="annual_gross_salary" class="form-control" placeholder="In Hand" required value="{{$appraisal->annual_gross_salary}}">
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label class="col-md-4" style="display:inline;padding-top:2%" align="left" for="monthly_var_bonus">Monthly Variable Bonus</label>
                    <div class="col-md-8">
                        <input type="text" name="monthly_var_bonus" class="form-control" placeholder="Quarterly Bonus" required value="{{$appraisal->monthly_var_bonus}}">
                              
                    </div>
                  
                        
                </div>
                <div class="form-group col-md-6">
                    <label class="col-md-4" style="display:inline;padding-top:2%" align="left" for="year_end">Annual Bonus</label>
                    <div class="col-md-8">
                        <input type="text" name="annual_bonus" class="form-control" placeholder="Annual Bonus" required value="{{$appraisal->annual_bonus}}">
                        <small>Due on : {{date_in_view($annual)}}</small>
                    </div>
                </div>

          <!--        <div class="form-group col-md-6">
                 <div class="col-md-4"> <label style="display:inline;padding-top:2%" align="left" for="qtr_bonus">Bonus Reports</label></div>
         
                        @foreach($qtrArray as $index => $value)
                                   <div class="col-md-8 pull-right">
                            <label >{{date("M Y", strtotime($index))}} : {{$value}}</label>
                            </div>
                        @endforeach
            </div> -->
            </div>
        </div>
        <div class="text-center">
            <button type="submit" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Save</button>
        </div>
    </form>
</div>
@endsection