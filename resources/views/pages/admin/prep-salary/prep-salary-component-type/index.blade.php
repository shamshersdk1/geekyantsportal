@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
		<div class="row">
            <div class="col-sm-8">
				<h1 class="admin-page-title">Prepare Salary Component Type</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
					<li class="active">Prepare Salary Component Type</li>
                </ol>
			</div>
			<div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
        </div>
	</div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">Add Prepare Salary Component Type</div>
				<div class="panel-body">
					{{ Form::open(['url' => '/admin/prep-salary-component-type', 'method' => 'post']) }}
					<div class="form-group col-md-12">
                    	{{ Form::label('name', 'Name :',['class' => 'col-md-4','align' => 'right'])}}
						<div class="col-md-4" style="padding:0">
                    		{{ Form::text('name', '',['class' => 'form-control']) }}
						</div>
					</div>
					<div class="form-group col-md-12">
                    {{ Form::label('code', 'Code :',['class' => 'col-md-4','align' => 'right'])}}
						<div class="col-md-4" style="padding:0">
                    		{{ Form::text('code', '',['class' => 'form-control']) }}
						</div>
					</div>
                 
                    

                    <div class="form-group col-md-12">
                    {{ Form::label('service_class', 'Service Class :',['class' => 'col-md-4','align' => 'right'])}}
                    <div class="col-md-4" style="padding:0">
                        <div class="input-group">
                        <span class="input-group-addon">App\Services\SalaryService\</span>
                        {{ Form::text('service_class', '',['class' => 'form-control','style' => 'left-margin:0','align' => 'left']) }}
                        </div>
                        </div>
					</div>
					
					@if(isset($prepSalaryComponentTypes))
						  	  	<div class="form-group col-md-12">
									    {{ Form::label('service_class', 'Service Class Dependencies:',['class' => 'col-md-4','align' => 'right'])}}
						  	    	<div class="col-md-4" style="padding:0">						      		
										<div class="multipicker">
											<div class="">
												<select class="js-example-basic-multiple22 " multiple="multiple" name="dependencies[]">
													@foreach($prepSalaryComponentTypes as $prepSalaryComponentType)
														<?php
															$saved = false;
															foreach($selected_classes as $selected_class){
																if($selected_class == $prepSalaryComponentType->id){
																	$saved = true;
																}
															}
														?>
														@if(isset($selected_classes))
															@if($saved)
																<option value="{{$prepSalaryComponentType->id}}" selected >{{substr($prepSalaryComponentType->service_class,27)}}</option>
															@else
																<option value="{{$prepSalaryComponentType->id}}">{{substr($prepSalaryComponentType->service_class,27)}}</option>
															@endif
														@else
															<option value="{{$prepSalaryComponentType->id}}">{{substr($prepSalaryComponentType->service_class,27)}}</option>
														@endif
													@endforeach
												</select>
											</div>
										</div>
									</div>	
						  	  	</div>
					  	  	@endif
					<div class="col-md-12">
					{{ Form::submit('Save',['class' => 'btn btn-success','style' => 'display: block; margin: 0 auto']) }}
					</div>
                    {{ Form::close() }}
				</div>
				</div>
				<div class="panel panel-default">
				<table class="table table-striped">
				<thead>
					<th width="10%">#</th>
					<th width="10%">Name</th>
					<th width="15%">Code</th>
                    <th width="15%">Service Class</th>
					<th width="15%">Dependency</th>
					<th width="25%" class="text-right">Actions</th>
				</thead>
				@if(isset($prepSalaryComponentTypes))
				@if(count($prepSalaryComponentTypes) > 0)
					@foreach($prepSalaryComponentTypes as $prepSalaryComponentType)
						<tr>
							<td class="td-text">{{$prepSalaryComponentType->id}}</td>
							<td class="td-text"> {{$prepSalaryComponentType->name}}</td>
							<td class="td-text"> {{$prepSalaryComponentType->code}}</td>
                            <td class="td-text"> {{$prepSalaryComponentType->service_class}}</td>
							<td class="td-text align-middle">  
								@foreach($prepSalaryComponentType->dependsOn as $dependent)					
									{{$dependent->dependentOn ? $dependent->dependentOn->name : ''}},
								@endforeach
								</td>
							<td class="text-right">
								<a href="/admin/prep-salary-component-type/{{$prepSalaryComponentType->id}}" class="btn btn-info crud-btn btn-sm"><i class="fa fa-pencil btn-icon-space" aria-hidden="true"></i>Edit</a>
								<div style="display:inline-block;" class="deleteform">
								{{ Form::open(['url' => '/admin/prep-salary-component-type/'.$prepSalaryComponentType->id, 'method' => 'delete']) }}
								{{ Form::submit('Delete',['class' => 'btn btn-danger','onclick' => 'return confirm("Are you sure you want to delete this item?")']) }}
								{{ Form::close() }}
								</div>
							</td>
						</tr>
					@endforeach
				@else
					<tr >
						<td colspan="6" class="text-center">No prepare salary component type types added.</td>
					</tr>
				@endif
				@endif
				</table>
			</div>
		</div>
	</div>
</div>
<script>
	$(function () {
		$('#type option[value="{{old('type')}}"]').attr("selected",true);
	});
</script>
@endsection
