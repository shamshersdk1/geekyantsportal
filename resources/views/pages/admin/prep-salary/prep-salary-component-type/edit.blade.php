@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
		<div class="row">
            <div class="col-sm-8">
				<h1 class="admin-page-title">Prepare Salary Component Type</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
					<li><a href="{{ url('admin/prep-salary-component-type') }}">Prepare Salary Component Type</a></li>
					<li class="active">{{$prepSalaryComponentType->name}}</li>
                </ol>
			</div>
			<div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
        </div>
	</div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
                <div class="panel-heading">Edit Prepare Salary Component Type</div>
				<div class="panel-body">
					{{ Form::open(['url' => '/admin/prep-salary-component-type/'.$prepSalaryComponentType->id, 'method' => 'put']) }}
				    {{ Form::token() }}
					<div class="form-group col-md-12">
                    	{{ Form::label('name', 'Name :',['class' => 'col-md-4','align' => 'right'])}}
						<div class="col-md-4" style="padding:0">
                    		{{ Form::text('name', $prepSalaryComponentType->name,['class' => 'form-control']) }}
						</div>
					</div>
					<div class="form-group col-md-12">
                    {{ Form::label('code', 'Code :',['class' => 'col-md-4','align' => 'right'])}}
						<div class="col-md-4" style="padding:0">
                    		{{ Form::text('code', $prepSalaryComponentType->code,['class' => 'form-control']) }}
						</div>
					</div>
					<div class="form-group col-md-12">
                    {{ Form::label('service_class', 'Service Class :',['class' => 'col-md-4','align' => 'right'])}}
                    <div class="col-md-4" style="padding:0">
                        <div class="input-group">
                        <span class="input-group-addon">App\Services\SalaryService\</span>
                        {{ Form::text('service_class',$prepSalaryComponentType->service_class,['class' => 'form-control','style' => 'left-margin:0','align' => 'left']) }}
                        </div>
                        </div>
					</div>
					@if(isset($prepSalaryComponentTypes))
						  	  	<div class="form-group col-md-12">
									    {{ Form::label('service_class', 'Service Class Dependencies:',['class' => 'col-md-4','align' => 'right'])}}
						  	    	<div class="col-md-4" style="padding:0">						      		
										<div class="multipicker">
											<div class="">
												<select class="js-example-basic-multiple22 " multiple="multiple" name="dependencies[]">
													@foreach($prepSalaryComponentTypes as $prepSalaryComponentType)
														<?php
															$saved = false;
															foreach($selected_component_types as $selected_class){
																if($selected_class->dependent_id == $prepSalaryComponentType->id){
																	$saved = true;
																}
															}
														?>
														@if(isset($selected_component_types))
															@if($saved)
																<option value="{{$prepSalaryComponentType->id}}" selected >{{substr($prepSalaryComponentType->service_class,27)}}</option>
															@else
																<option value="{{$prepSalaryComponentType->id}}">{{substr($prepSalaryComponentType->service_class,27)}}</option>
															@endif
														@else
															<option value="{{$prepSalaryComponentType->id}}">{{substr($prepSalaryComponentType->service_class,27)}}</option>
														@endif
													@endforeach
												</select>
											</div>
										</div>
									</div>	
						  	  	</div>
					  	  	@endif

					<div class="col-md-12">
					{{ Form::submit('Update',['class' => 'btn btn-success','style' => 'display: block; margin: 0 auto']) }}
					</div>
                    {{ Form::close() }}
				</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type=“text/javascript”>
$(document).ready(function() {
       
        $(‘#selectid2’).select2({
            placeholder: ‘Select a user’,
            allowClear:true
        });
   });
</script>
@endsection
