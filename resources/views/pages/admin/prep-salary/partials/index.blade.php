@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
		<div class="row">
            <div class="col-sm-8">
				<h1 class="admin-page-title">Prepare Salary</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
					<li><a href="{{ url('admin/prep-salary') }}">Prepare Salary</a></li>
					<li class="active">View</li>
                </ol>
			</div>
        </div>
	</div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
	<div class="row">
		<table class="table table-striped" id="prep-salary-table">
			<thead>
				<th>#</th>
				<th>Employee Code</th>
				<th> Name</th>
				<th>Appraisal</th>
				<th>Appraisal Bonus</th>
				<th>Working Days</th>
				<th>Bonus</th>
				<th>Loan</th>
				<th>IT Saving</th>
				<th>VPF</th>
				<th>Food Deduction</th>
				<th>Insurance</th>
				<th>Adhoc Payment</th>
				<th>Gross Salary Paid</th>
				<th>Current Gross</th>
				<th>Gross Salary  To Be Paid</th>
				<th>TDS</th>





			</thead>
			<tbody>
				
				@if(count($prepUsers)>0)
					@foreach($prepUsers as  $index => $prepUser)
						<tr>
							<td>{{$index+1}}</td>
							<td>{{$prepUser['user']['employee_id']}}</td>
                            <td>{{$prepUser['user']['name']}}</td>
                            <td>
								@include('pages.admin.prep-salary.partials.appraisal',array('data'=>$response[$prepUser->user_id]['appraisal']))
							</td>
							<td>
								@include('pages.admin.prep-salary.partials.appraisal-bonus',array('data'=>$response[$prepUser->user_id]['appraisal-bonus']))
							</td>
							<td>
								@include('pages.admin.prep-salary.partials.working-day',array('data'=>$response[$prepUser->user_id]['working-day']))
							</td>
							<td>
								@include('pages.admin.prep-salary.partials.bonus',array('data'=>$response[$prepUser->user_id]['bonus']))
							</td>
							<td>
								@include('pages.admin.prep-salary.partials.loan',array('data'=>$response[$prepUser->user_id]['loan']))
							</td>
							<td>
								@include('pages.admin.prep-salary.partials.it-saving',array('data'=>$response[$prepUser->user_id]['it-saving']))
							</td>
							<td>
								@include('pages.admin.prep-salary.partials.vpf',array('data'=>$response[$prepUser->user_id]['vpf']))
							</td>
							<td>
								@include('pages.admin.prep-salary.partials.food-deduction',array('data'=>$response[$prepUser->user_id]['food-deduction']))
							</td>
							<td>
								@include('pages.admin.prep-salary.partials.insurance',array('data'=>$response[$prepUser->user_id]['insurance']))
							</td>
							<td>
								@include('pages.admin.prep-salary.partials.adhoc-payment',array('data'=>$response[$prepUser->user_id]['adhoc-payment']))
							</td>
							<td>
								@include('pages.admin.prep-salary.partials.gross-salary-paid',array('data'=>$response[$prepUser->user_id]['gross-paid-till-now']))
							</td>
							<td>
								@include('pages.admin.prep-salary.partials.current-gross',array('data'=>$response[$prepUser->user_id]['current-gross']))
							</td>
							<td>
								@include('pages.admin.prep-salary.partials.gross-salary-to-be-paid',array('data'=>$response[$prepUser->user_id]['gross-to-be-paid']))
							</td>
							<td>
								@include('pages.admin.prep-salary.partials.tds',array('data'=>$response[$prepUser->user_id]['tds']))
							</td>

						</tr>
					@endforeach
				@endif
			</tbody>
		</table>

	</div>
</div>
<script>
    $(document).ready(function() {
		$tableHeight = $( window ).height();
        var t = $('#prep-salary-table').DataTable( {
            pageLength:500, 
			fixedHeader: true,
			"columnDefs": [ {
				"searchable": true,
                "orderable": true,
            } ],
			scrollY: $tableHeight - 200,
            scrollX: true,
            scrollCollapse: true,
            paging:         false,
            fixedColumns:   {
                leftColumns: 3,
            },
        } );
    });
</script>
@endsection
