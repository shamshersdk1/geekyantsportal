<div class="container-fluid">
    <div class="row">
		<table class="table " >
                    @if(count($data)>0)
                        @foreach($data as $appraisal)
                            @if(isset($appraisal) && isset($appraisal['component']))
                            <tr>
                            <td>Duration</td>
                            @foreach($appraisal['component'] as $index => $component)
                                    <td>{{$index}}</td>

                            @endforeach
                        </tr>
                        <tr>
                            <td>{{$appraisal['start_date']}} <br/> {{$appraisal['end_date']}}</td>
                                @foreach($appraisal['component'] as $index => $component)
                                    <td>{{$component}}</td>

                                @endforeach
                        </tr>
                            @endif
                        @endforeach
                    @endif
            </table>
        </div>
</div>

