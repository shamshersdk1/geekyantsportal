<div class="container-fluid">
    <div class="row">
		<table class="table">
            <thead>
                <tr>
                    <td>Onsite</td>
                    <td>Additional</td>
                    <td>Extra</td>
                    <td>Tech Talk</td>
                    <td>Referral</td>
                    <td>Performance</td>
                </tr>
            </thead>
                <tr>
                    <td>{{$data['bonus']['onsite'] ? $data['bonus']['onsite'] : '-'}}</td>
                    <td>{{$data['bonus']['additional'] ? $data['bonus']['additional'] : '-'}}</td>
                    <td>{{$data['bonus']['extra'] ? $data['bonus']['extra'] : '-'}}</td>
                    <td>{{$data['bonus']['techtalk'] ? $data['bonus']['techtalk'] : '-'}}</td>
                    <td>{{$data['bonus']['referral'] ? $data['bonus']['referral'] : '-'}}</td>
                    <td>{{$data['bonus']['performance'] ? $data['bonus']['performance'] :'-'}}</td>
                </tr>                            
        </table>
	</div>
</div>

