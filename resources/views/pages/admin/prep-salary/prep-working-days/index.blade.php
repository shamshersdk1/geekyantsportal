@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
		<div class="row">
            <div class="col-sm-8">
				<h1 class="admin-page-title">Prepare Working Days</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/prep-salary') }}">Prepare Salary</a></li>
					<li class="active">{{$prepSalaryComponent->salary->month->formatMonth()}}</li>
                </ol>
			</div>
			<div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>			
        </div>
	</div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>

	<div class="row">
		<div class="col-md-12">
		
			<div class="panel panel-default">
				<div class="panel-heading">
                <span>Month: <label>{{$prepSalaryComponent->salary->month->formatMonth()}}</label></span><br>
                <span>Status: <label>
                    @if($prepSalaryComponent->salary->status == 'open')
                    <span class="label label-primary">Open<span>
                    @else
                    <span class="label label-danger">Close<span>
                    @endif    
                </label></span>
                </div>

				<div class="panel panel-default">
				<table class="table table-striped">
				<thead>
					<th width="10%">#</th>
					<th width="10%">User</th>
					<th width="15%">Working Days</th>
                    <th width="15%">Days Worked</th>
                    <th width="15%">Paid Leaves</th>
                    <th width="15%">Sick Leaves</th>
				</thead>
				@if(isset($workingDayObjs))
				@if(count($workingDayObjs) > 0)
					@foreach($workingDayObjs as $workingDayObj)
						<tr>
							<td class="td-text">{{$workingDayObj->id}}</td>
							<td class="td-text"> {{$workingDayObj->user->name}}</td>
							<td class="td-text">{{$workingDayObj->working_days}}</td>
                            <td class="td-text">{{$workingDayObj->days_worked}}</td>
							<td class="td-text">{{$workingDayObj->pl_count}}</td>
							<td class="td-text">{{$workingDayObj->sl_count}}</td>
						</tr>
					@endforeach
				@endif
				@else
					<tr >
						<td colspan="6" class="text-center">No prep working days found.</td>
					</tr>
		
				@endif
				</table>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
 $(document).ready(function() {
        $('#effectiveDate').datetimepicker({
            format: 'YYYY-MM-DD'
        });
		$('.userSelect').select2({
			placeholder: 'Select a user',
			allowClear:true
		});

        $('.userSelect1').select2({
            placeholder: 'Select a user',
            allowClear:true
        });
        $('#selectid2').select2({
            placeholder: 'Select a user',
            allowClear:true
        }); 
    });
</script>
@endsection
