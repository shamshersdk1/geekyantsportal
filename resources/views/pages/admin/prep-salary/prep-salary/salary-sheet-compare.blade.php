@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-6">
                <h1 class="admin-page-title">Salary Transaction</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="/admin/prep-salary">Prep Salary</a></li>
                    <li class="active">Salary Sheet Compare for {{$prevMonthObj->formatMonth()}} - {{$prepSalObj->month ? $prepSalObj->month->formatMonth() : ""}}</li>
                </ol>
            </div>
            <div class="col-md-2 pull-right" style="background-color: white;padding:0px;">
               @if(isset($monthList))
                  <select id="selectid2" name="month"  placeholder= "{{$prepSalObj->month ? $prepSalObj->month->formatMonth() : 'Select Month'}}">
                        <option value=""></option>
                        @foreach($monthList as $x)
                           <option value="{{$x->id}}" >{{$x->formatMonth()}}</option>
                        @endforeach
                  </select>
               @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                    @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif
        </div>
    </div>
    	<a class="btn btn-primary btn-sm crude-btn" href="{{$prepSalObj->id}}/download"><i class="fa fa-download"></i> &nbsp;Download CSV</a>
    <div class="user-list-view">
        <div class="panel panel-default">
            <table class="table table-striped" id="summary-table">
                <thead>
                    <th class="text-center">#</th>
                    <th class="text-center">Employee Id</th>
                    <th class="text-center">Name</th>
                    <th class="text-center">Bank</th>
                    @foreach($apprCompType as $type)
                        <th></th>
                        <th class="text-center">{{$type->name}}</th>
                        <th></th>
                    @endforeach
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        @foreach($apprCompType as $type)
                            <th class="text-center">{{date('F', mktime(0, 0, 0, $prevMonthObj->month, 10))}}, {{$prevMonthObj->year}}</th>
                            <th class="text-center">{{date('F', mktime(0, 0, 0, $prepSalObj->month->month, 10))}}, {{$prepSalObj->month->year}}</th>
                            <th class="text-center">Diff</th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                    @if(count($users) > 0)
                    @foreach($users as $user)
                    @if(isset($data[$user->id]))
                        <tr>
                            <td class="text-center"></td>
                            <td class="text-center">{{ $user->employee_id}}</td>
                            <td class="text-center">{{ $user->name}}</td>
                            <td class="text-center">{{$data[$user->id]['Bank'] ?? 0}}</td>
                            @foreach($apprCompType as $type) 
                                <td class="text-center">{{$data[$user->id]['previous'][$type->code] ?? 0}}</td>
                                <td class="text-center">{{$data[$user->id]['current'][$type->code] ?? 0}}</td>
                                <td class="text-center">{{$data[$user->id]['previous'][$type->code] - $data[$user->id]['current'][$type->code]}}</td>
                            @endforeach
                        </tr> 
                    @endif
                    @endforeach
                @else
                    <tr>
                        <td class="text-center">
                            No Records found
                        </td>
                    </tr>
                @endif
                
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        var t = $('#summary-table').DataTable( {
            pageLength:500,
            fixedHeader: true,
            dom: 'Bfrtip',
            buttons: [
                'csv'
            ],
            scrollY:        true,
            scrollX:        true,
            paging:         false,
            fixedColumns:   {
                leftColumns: 3,
            },
        } );
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
        $('#selectid2').change(function(){
            var optionSelected = $("option:selected", this);
            optionValue = this.value;
            if (optionValue) { 
                window.location = "/admin/prep-salary/salary-sheet-comparison/"+optionValue; 
            }
         });
    });
</script>
@endsection
