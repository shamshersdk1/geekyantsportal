@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
		<div class="row">
            <div class="col-sm-8">
				<h1 class="admin-page-title">Prepare Salary</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/prep-salary') }}">Prepare Salary</a></li>
					<li class="active">{{$prepSalarie->month->formatMonth()}}</li>
                </ol>
			</div>
			<div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
        </div>
	</div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            @foreach ($errors->all() as $error)
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>

	<div class="row">
		<div class="col-md-12">
			@if($prepSalarie->status === 'in_progress')
					<div class="alert alert-warning">
	    				<b><i class="fa fa-spinner fa-spin"></i> Please wait! Preparing the salary data. This may take sometime.</b>
    				</div>
				@endif
			<div class="panel panel-default">
				<div class="panel-heading">
					<div class="row">
						<div class="col-sm-8">
							<span>Month: <label>{{$prepSalarie->month->formatMonth()}}</label></span><br>
							<span>Status: <label>
								@if($prepSalarie->status == 'open')
								<span class="label label-primary">Open</span>
								@elseif($prepSalarie->status == 'in_progress')
								<span class="label label-primary">In Progress</span>
								@elseif($prepSalarie->status == 'processed')
								<span class="label label-primary">Processed</span>
								@elseif($prepSalarie->status == 'finalizing')
								<span class="label label-primary">Finalizing</span>
								@else
								<span class="label label-danger">Closed</span>
								@endif    
							</label></span></br>
							<span>Created At: <label>{{datetime_in_view($prepSalarie->created_at)}}</label></span><br>
						</div>
						
						<div class="col-sm-4 text-right">
						
								{{ Form::open(['url' => '/admin/prep-salary/'.$prepSalarie->id.'/salary-sheet', 'method' => 'get']) }}
								{{ Form::submit('Salary Sheet',['class' => 'btn btn-info']) }}
								{{ Form::close() }}
							
							@if($prepSalarie->checkAll() == true && $prepSalarie->status === 'processed')
							{{ Form::open(['url' => '/admin/prep-salary/'.$prepSalarie->id.'/finalize', 'method' => 'get']) }}
							{{ Form::submit('Finalize',['class' => 'btn btn-success']) }}
							{{ Form::close() }}
							@endif
							@if($prepSalarie->status === 'open')
							{{ Form::open(['url' => '/admin/prep-salary/'.$prepSalarie->id.'/generate-all', 'method' => 'get']) }}
							{{ Form::submit('Generate All',['class' => 'btn btn-primary','onclick'=>'generate(); this.disabled=true;']) }}
							{{ Form::close() }}
							@endif
							@if($prepSalarie->status === 'processed')
							{{ Form::open(['url' => '/admin/prep-salary/'.$prepSalarie->id.'/generate-all', 'method' => 'get']) }}
							{{ Form::submit('Regenerate All',['class' => 'btn btn-primary','onclick'=>'generate(); this.disabled=true;']) }}
							{{ Form::close() }}
							@endif
						</div>
					</div>
                </div>
			</div>
				<div class="panel panel-default">
				<table class="table table-striped">
				<thead>
					<th width="10%">#</th>
                    <th width="10%">Name</th>
                    <th width="10%">Status</th>
					<th width="10%">Is Generated</th>
					<th width="10%">Execution Status</th>
					<th width="25%" class="text-right">Actions</th>
				</thead>
				@if(isset($prepSalarie))
				@if(count($prepSalarie->components()) > 0)
					@foreach($prepSalarie->components as $prepSalaryComponent)
						<tr>
							<td class="td-text">{{$prepSalaryComponent->id}}</td>
                            <td class="td-text"> {{$prepSalaryComponent->type->name}}</td>
                            <td class="td-text">
                                <label>
                                    @if($prepSalaryComponent->status == 'open')
                                    <span class="label label-primary">Open<span>
                                    @else
                                    <span class="label label-danger">Close<span>
                                    @endif    
                                </label>
                            </td>
							<td class="td-text">
								@if($prepSalaryComponent->is_generated == 1)
									<span class="label label-primary">Yes<span>
								@else
                                    <span class="label label-danger">No<span>
                                @endif    
							</td>
							<td class="td-text">
								@if($prepSalarie->status != 'open')
									@if($prepSalaryComponent->totalExecutions() > 0)
										<span class="label label-default">
											Total : {{$prepSalaryComponent->totalExecutions() ?? 0}}
										</span>
									@endif
									@if($prepSalaryComponent->countExecutions('completed') > 0)
										<span class="label label-success">
											Completed : {{$prepSalaryComponent->countExecutions('completed') ?? 0}}
										</span>
									@endif
									@if($prepSalaryComponent->countExecutions('failed') > 0)
										<span class="label label-danger">
											Failed : {{$prepSalaryComponent->countExecutions('failed') ?? 0}}
										</span>
									@endif
								@endif
									
							</td>
							<td class="text-right">
								@if($prepSalaryComponent->type->code === 'user' && $prepSalaryComponent->is_generated == '1' )
								<a target="_blank" href="/admin/prep-salary/{{$prepSalaryComponent->id}}/view" class="btn btn-warning crud-btn btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View</a>
								@endif
								@if($prepSalaryComponent->type->code === 'user')
								<a target="_blank" href="/admin/prep-users/{{$prepSalarie->id}}" class="btn btn-warning btn-sm"><i class="fa fa-plus btn-icon-space" aria-hidden="true"></i>Add Users</a>
								@endif
								@if($prepSalaryComponent->type->code === 'appraisal-bonus')
								<a target="_blank" href="/admin/fixed-bonus/{{$prepSalarie->month->id}}" class="btn btn-warning btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View Fixed Bonuses</a>
								@endif
								@if($prepSalaryComponent->type->code === 'appraisal-bonus')
								<a target="_blank" href="/admin/variable-pay/{{$prepSalarie->month->id}}" class="btn btn-warning btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View Variable Pay</a>
								@endif
								@if($prepSalaryComponent->type->code == 'loan')
								<a target="_blank" href="/admin/loan-deduction/{{$prepSalarie->month->id}}" class="btn btn-warning btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View Loan Deductions</a>
								@endif
								@if($prepSalaryComponent->type->code == 'loan')
								<a target="_blank" href="/admin/loan-interest-income/{{$prepSalarie->month->id}}" class="btn btn-warning btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View Loan Interest Income</a>
								@endif
								@if($prepSalaryComponent->type->code == 'vpf')
								<a target="_blank" href="/admin/vpf-deduction/{{$prepSalarie->month->id}}" class="btn btn-warning btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View VPF Deductions</a>
								@endif
								@if($prepSalaryComponent->type->code == 'food-deduction')
								<a target="_blank" href="/admin/food-deduction/{{$prepSalarie->month->id}}" class="btn btn-warning btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View Food Deductions</a>
								@endif
								@if($prepSalaryComponent->type->code == 'insurance')
								<a target="_blank" href="/admin/insurance-deduction/{{$prepSalarie->month->id}}" class="btn btn-warning btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View Insurance Deductions</a>
								@endif
								@if($prepSalaryComponent->type->code == 'it-saving')
								<a target="_blank" href="/admin/it-saving/month/{{$prepSalarie->month->id}}" class="btn btn-warning btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View It Saving Month</a>
								@endif
								@if($prepSalaryComponent->type->code == 'working-day')
								<a target="_blank" href="/admin/user-working-day/{{$prepSalarie->month->id}}" class="btn btn-warning btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View User Working Day</a>
								@endif
								@if($prepSalaryComponent->type->code === 'tds' && $prepSalaryComponent->is_generated == '1' )
								<a target="_blank" href="/admin/prep-salary/{{$prepSalarie->id}}/tds" class="btn btn-warning crud-btn btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View</a>
								@endif
                                @if($prepSalaryComponent->status == 'open' && $prepSalarie->status == 'open' && !($prepSalaryComponent->type->code === 'user'))
								    <a href="/admin/prep-salary/{{$prepSalaryComponent->id}}/generate" onclick="myFunction({{$prepSalaryComponent->is_generated}})" class="btn btn-primary crud-btn btn-sm" ><i class="fa fa-gear btn-icon-space" aria-hidden="true"></i>Generate</a>	
								@endif
								
                            </td>
						</tr>
					@endforeach
				@else
					<tr >
						<td colspan="6" class="text-center">No prepare salary component type types added.</td>
					</tr>
				@endif
				@endif
				</table>
               
			</div>
		</div>
	</div>
</div>
<script>
	function generate() {
		window.location = '/admin/prep-salary/'+{{$prepSalarie->id}}+'/generate-all';
    }
	$(function () {
		$('#type option[value="{{old('type')}}"]').attr("selected",true);
	});
	
	function myFunction(isGenerated) {
		if(isGenerated == 1)
		{
			if(confirm("Are you sure want to regenerate, it will delete all records?")){
				return true;
			} else{
				return false;
			}
		}
	}
</script>
@endsection
