@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
		<div class="row">
            <div class="col-sm-8">
				<h1 class="admin-page-title">Prepare Salary</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/prep-salary') }}">Prepare Salary</a></li>
                    <li class="active">Payslip - {{$prepSalaryObj->month->formatMonth()}} / {{$userObj->name}}</li>
                </ol>
			</div>
			<div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
        </div>
    </div>
    
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            @foreach ($errors->all() as $error)
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>

<div class="row">
         <div class="col-sm-8">
            <div class="row">
               	<div class="col-sm-6">
					<div class="panel panel-default">
                        <div class="info-table">
							<label>Month </label>
							<span>{{$prepSalaryObj->month->formatMonth()}}</span>
						</div>
                        <div class="info-table">
							<label>Status </label>
							<span>
                                @if($prepSalaryObj->status == 'open')
                                <span class="label label-primary">Open<span>
                                @else
                                <span class="label label-danger">Close<span>
                                @endif    
                            </span>
						</div>
						<div class="info-table">
							<label>Employee Name </label>
							<span>{{$userObj->name}}</span>
						</div>
						<div class="info-table">
							<label>Employee Id </label>
							<span>{{$userObj->employee_id}}</span>
						</div>
						<div class="info-table">
							<label>Joining Date </label>
							<span>{{date_in_view($userObj->joining_date)}}</span>
						</div>
					</div>
				</div>
                <div class="col-sm-5">
					<div class="panel panel-default">
						<div class="info-table">
							<label>Monthly Gross Salary </label>
							<span>{{$salaryResponse['monthlyGrossSalary']}}</span>
						</div>
						<div class="info-table">
							<label>Total Deductions </label>
							<span>{{-$salaryResponse['totalDeductions']}}</span>
						</div>
						<div class="info-table">
							<label>Net Payable </label>
							<span>{{$salaryResponse['netPayable']}}</span>
						</div>
						
					</div>
				</div>
            </div>
        </div>
      
        @if(!count($storeSalaryObj)>0)
         <div class="col-sm-2 pull-right">
			{{ Form::open(['url' => '/admin/prep-salary/'.$prepSalaryObj->id.'/salary/'.$userObj->id, 'method' => 'get']) }}
			{{ Form::submit('Store Salary',['class' => 'btn btn-success btn-sm crude-btn']) }}
			{{ Form::close() }}
		</div>
        @endif
        <div class="col-sm-2 pull-right">
        @if(isset($userList))
            <select id="selectid2" name="user"  placeholder= "{{$userObj->name}}">
                <option value=""></option>
                @foreach($userList as $x)
                    <option value="{{$x->id}}" >{{$x->employee_id}} - {{$x->name}}</option>
                @endforeach
            </select>
        @endif
        </div>
       

       
</div>
<div class="pull-right">
        @if(count($storeSalaryObj)>0)
            <span class="label label-primary">Stored At: {{datetime_in_view($storeSalaryObj->created_at)}}</span>
        @endif
</div>


	<div class="row">
		<div class="col-md-12">
				<div class="panel panel-default">
				<table class="table table-striped">
                    <thead>
                        <th width="10%">#</th>
                        <th width="30%">Reference Type</th>
                        <th width="10%">Reference Id</th>
                        <th width="20%">Key</th>
                        <th width="10%" class="text-right">Value</th>
                    </thead>
                    <tbody>
                        <tr>
                            <th colspan="5"><b>APPRAISAL COMPONENT</b></th>
                        </tr>
                        @php
                        $flag=0;
                        $count=0;
                        @endphp
                        @if($currentGrossObj && count($currentGrossObj) > 0) 
                            @foreach ($currentGrossObj->items as $index => $item)
                                @foreach($appraisalComponentTypes as $type)
                                    @if($type->code == $item->key)
                                    <tr>
                                    @php
                                        $count = $index+1;
                                    @endphp
                                    <td>{{$index + 1}}</td>
                                    <td>App\Models\Appraisal\AppraisalComponentType</td>
                                    <td>
                                            {{$type->id}}
                                    </td>
                                    <td>{{$item->key}}</td>
                                    <td class="text-right">{{$item->value}}</td>
                                </tr>
                                @endif 
                                    @endforeach
                            @endforeach
                            <tr>
                                    <td>{{$count + 1}}</td>
                                    <td>App\Models\Appraisal\AppraisalComponentType</td>
                                    <td>{{$appraisalComponentTypes->where('code','tds')->first()->id}}</td>
                                    <td>tds</td>
                                    <td class="text-right">
                                    @if($tdsObj)
                                    {{$tdsObj->componentByKey('tds')->value}}
                                    @endif</td>
                                </tr>
                        @endif

                        <tr>
                            <th colspan="5"><b>APPRAISAL BONUS</b></th>
                        </tr>
                        @if($appraisalBonusObj && count($appraisalBonusObj) > 0) 
                            @foreach ($appraisalBonusObj as $index => $item)
                                <tr>
                                    <td>{{$index + 1}}</td>
                                    <td>App\Models\Appraisal\AppraisalBonusType</td>
                                    <td>{{$item->appraisalBonus->appraisal_bonus_type_id}}</td>
                                    <td>{{$item->appraisalBonus->appraisalBonusType->code}}</td>
                                    <td class="text-right">{{$item->value}}</td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="5" class="text-center">No Appraisal Bonus Found</td>
                            </tr>
                        @endif
                        <tr>
                            <th colspan="5"><b>BONUSES</b></th>
                        </tr>
                        @if($bonusObjs && count($bonusObjs) > 0) 
                            @foreach ($bonusObjs as $index => $item)
                                <tr>
                                    <td>{{$index + 1}}</td>
                                    <td>App\Models\Admin\Bonus</td>
                                    <td>{{$item->bonus->id}}</td>
                                    <td></td>
                                    <td class="text-right">{{$item->bonus->amount}}</td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="5" class="text-center">No Appraisal Bonus Found</td>
                            </tr>
                        @endif
                        <tr>
                            <th colspan="5"><b>VPF</b></th>
                        </tr>
                        @if($vpfObj && count($vpfObj) > 0) 
                            @foreach ($vpfObj as $index => $item)
                                <tr>
                                    <td>{{$index + 1}}</td>
                                    <td>App\Models\Admin\VpfDeduction</td>
                                    <td>{{$item->vpfs_id}}</td>
                                    <td></td>
                                    <td class="text-right">{{$item->value}}</td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="5" class="text-center">No VPF Found</td>
                            </tr>
                        @endif
                        <tr>
                            <th colspan="5"><b>INSURANCE</b></th>
                        </tr>
                        @if($insuranceObj && count($insuranceObj) > 0) 
                            @foreach ($insuranceObj as $index => $item)
                                <tr>
                                    <td>{{$index + 1}}</td>
                                    <td>App\Models\Admin\InsuranceDeduction</td>
                                    <td>{{$item->insurance_id}}</td>
                                    <td></td>
                                    <td class="text-right">{{$item->value}}</td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="5" class="text-center">No Insurance Found</td>
                            </tr>
                        @endif
                        <tr>
                            <th colspan="5"><b>LOAN EMI</b></th>
                        </tr>
                        @if($loanObj && count($loanObj) > 0) 
                            @foreach ($loanObj as $index => $item)
                                <tr>
                                    <td>{{$index + 1}}</td>
                                    <td>App\Models\LoanEmi</td>
                                    <td>{{$item->loan_id}}</td>
                                    <td></td>
                                    <td class="text-right">{{$item->emi_amount}}</td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="5" class="text-center">No Loan EMI Found</td>
                            </tr>
                        @endif
                        <tr>
                            <th colspan="5"><b>FOOD</b></th>
                        </tr>
                        @if($foodObj && count($foodObj) > 0) 
                                <tr>
                                    <td>1</td>
                                    <td>App\Models\FoodDeduction</td>
                                    <td>{{$foodObj->id}}</td>
                                    <td></td>
                                    <td class="text-right">{{$foodObj->value}}</td>
                                </tr>
                    
                        @else
                            <tr>
                                <td colspan="5" class="text-center">No Food Deduction Found</td>
                            </tr>
                        @endif
                    </tbody>
				</table>
            </div>
		</div>
	</div>
</div>
<script>
	$('#selectid2').change(function(){
        console.log('here');
        var optionSelected = $("option:selected", this);
        optionValue = this.value;
        console.log(optionValue);
        if (optionValue) { 
            window.location = "/admin/prep-salary/"+{{$prepSalaryObj->id}}+"/payslip/"+optionValue; 
        }
    });

</script>
@endsection
