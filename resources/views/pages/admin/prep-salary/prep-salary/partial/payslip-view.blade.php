<style type="text/css">
    * {
        margin:0;
        padding:0;
    }
    body {
        font-family: Arial, Helvetica, sans-serif;
        font-size: 14px;
        background: #fff;
    }
    table {
        width: 100%;
        border: solid 1px #ddd;
        border-collapse: collapse;
        margin-bottom: 20px;
    }
    table tr td, table tr th {
        border-bottom: solid 1px #ddd;
        border-right: solid 1px #ddd;
        padding: 5px;
    }
    table tr th {
        text-align: left;
    }
    p {
        font-size: 12px;
        margin-bottom: 15px;
    }
    @page {
      size: A4;
      margin: 0;
    }
    @media print {
      html, body {
        width: 210mm;
        height: 297mm;
      }
    }
</style>
<div class="main">
    <div style="text-align: right;">
        <img src="https://geekyants.com/images/logo-dark.png" width="150px" />
    </div>
    <div>
        <h1 style="font-size: 18px; text-align: center; padding: 15px 0; text-transform: uppercase; font-weight: bold;">Salary Payslip</h1>
    </div>
    <table>
        <tr>
            <th width="20%">Employee Name</th>
            <td width="40%">{{$user->name}}</td>
            <th width="20%">Employee Code</th>
            <td width="20%">{{$user->employee_id}}</td>
        </tr>
        <tr>
            <th>Designation</th>
            <td>{{$user->userDesignation->designation}}</td>
            <th>Period</th>
            <td>{{$month->formatMonth()}}</td>
        </tr>
        <tr>
            <th>PAN</th>
            <td>{{$user->userDetails->pan}}</td>
            <th>DOJ</th>
            <td>{{$user->joining_date}}</td>
        </tr>
        <tr>
            <th>PF UAN</th>
            <td>{{$user->userDetails->uan_no}}</td>
            <th>ESI No.</th>
            <td>{{$user->userDetails->esi_no}}</td>
        </tr>
        <tr>
            <th>Bank A/c. No</th>
            <td>{{$user->userDetails->bank_ac_no}}</td>
            <th>Bank IFSC</th>
            <td>{{$user->userDetails->bank_ifsc_code}}</td>
        </tr>
        <tr>
            <th>No. of Working Days</th>
            <td>{{$workingDays}}</td>
            <th>No. of Days Worked</th>
            <td>{{$days_worked}}</td>
        </tr>
    </table>
    <table style="border:none;">
        <tr>
            <td style="border:none; vertical-align: top; padding:0 5px 0 0; width: 50%">
                <table>
                    <tr>
                        <th colspan="2" style="text-align: center;">Earnings</th>
                    </tr>
                    @foreach($earning as $transaction)
                        @if(trim($transaction->reference_type) != 'App\Models\Admin\Bonus' && $transaction->amount > 0)
                            <tr>
                                 <?php
                                switch($transaction->reference_type){
                                    case 'App\Models\Appraisal\AppraisalBonus':
                                        $displaytext = $transaction->reference->appraisalBonusType->description;
                                        break;

                                    case 'App\Models\Appraisal\AppraisalComponentType': 
                                        $displaytext = $transaction->reference->name;
                                        break;
                                    case 'App\Models\Audited\AdhocPayments\AdhocPayment':
                                        $displaytext = $transaction->reference->component->description;
                                        break;
                                    default:
                                        $displaytext = $transaction->reference_type;
                                }
                                ?>
                                <td>{{$displaytext}}</td>
                                <td style="text-align: right;">{{$transaction->amount}}</td>
                            </tr>
                        @endif
                   @endforeach
                    @if(!empty($onsite_bonus))
                        <tr><td>Onsite Bonus</td><td style="text-align: right;">{{$onsite_bonus}}</td></tr>
                    @endif
                    @if(!empty($additional_bonus))
                        <tr><td>Additional Working Day Bonus</td><td style="text-align: right;">{{$additional_bonus}}</td></tr>
                    @endif
                    @if(!empty($extra_bonus))
                        <tr><td>Extra Hour Bonus</td><td style="text-align: right;">{{$extra_bonus}}</td></tr>
                    @endif
                    @if(!empty($performance_bonus))
                        <tr><td>Performance Bonus</td><td style="text-align: right;">{{$performance_bonus}}</td></tr>
                    @endif
                    @if(!empty($tech_talk_bonus))
                        <tr><td>Tech Talk Bonus</td><td style="text-align: right;">{{$tech_talk_bonus}}</td></tr>
                    @endif
                    @if(!empty($bonus))
                        <tr><td>Other Bonus</td><td style="text-align: right;">{{$bonus}}</td></tr>
                    @endif

                    <tr>
                        <th>Gross Total Earnings</th>
                        <th style="text-align: right;">{{$total_earning}}</th>
                    </tr>
                </table>
            </td>
            <td style="border:none; vertical-align: top;  padding:0 0 0 5px;  width: 50%">
                <table>
                    <tr>
                        <th colspan="2" style="text-align: center;">Deductions</th>
                    </tr>
                     @foreach($deduction as $transaction)
                        @if($transaction->amount < 0)
                            <tr>
                                <?php
                                switch($transaction->reference_type){
                                    case 'App\Models\Appraisal\AppraisalComponentType': 
                                        $displaytext = $transaction->reference->name;
                                        break;

                                    case 'App\Models\Audited\FoodDeduction\FoodDeduction':
                                        $displaytext = 'Food Deduction'; 
                                        break;

                                    case 'App\Models\Admin\Insurance\InsuranceDeduction':
                                        $displaytext = 'Insurance Deduction'; 
                                        break;

                                    case 'App\Models\Admin\VpfDeduction':
                                        $displaytext = 'VPF'; 
                                        break;

                                    case 'App\Models\LoanEmi':
                                        $displaytext = 'Loan EMI';
                                        break;
                                    case 'App\Models\Audited\AdhocPayments\AdhocPayment':
                                        $displaytext = $transaction->reference->component->description;
                                        break;

                                    default:
                                        $displaytext = $transaction->reference_type;
                                }
                                ?>
                                <td>{{$displaytext}}</td>
                                <td style="text-align: right;">{{$transaction->amount}}</td>
                            </tr>
                        @endif
                    @endforeach
                    <tr>
                        <th>Total Deductions</th>
                        <th style="text-align: right;">{{$total_deduction}}</th>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <table>
        <tr>
            <th colspan="3">Net Salary for the Month</th>
            <th style="text-align: right;">{{$net_transfer}}</th>
        </tr>
    </table>


    <div style="text-align: center; margin-top:40px;">
        <p>This is a computer generated payslip. Hence no signature is required.</p>
        @if(substr($user->employee_id,0,1) == 'B')
        <div>
            <p>
                <b>SAHU SOFT INDIA PRIVATE LIMITED</b>, CIN:  U72200BR2006PTC011902
                <br />
                Regd. Office: Amgola Road, Muzaffarpur, P.S: Kaji Mohammadpur, Bihar 842002 India
                <br />
                Corp. Office: #18, 2nd Cross Road, N S Palya, BTM Layout Stage 2, Bengaluru - 76, India
            </p>
            <p> 
                +91 80 409 74929 | 
                <a href="mailto:info@geekyants.com">info@geekyants.com</a> | 
                <a href="https://geekyants.com/" target="_blank">www.geekyants.com</a>
            </p>
        </div>

        <!-- GeekyAnts Academy -->
        @elseif(substr($user->employee_id,0,1) == 'A')
        <div>
            <p>
                <b>GEEKYANTS ACADEMY PRIVATE LIMITED</b>, CIN: U80302KA2019PTC127401
                <br />
                Corp. Office: #18, 2nd Cross Road, N S Palya, BTM Layout Stage 2, Bengaluru - 76, India
            </p>
            <p> 
                +91 80 409 74929 | 
                <a href="mailto:reachout@geekyants.academy">reachout@geekyants.academy</a> | 
                <a href="https://geekyants.academy/" target="_blank">geekyants.academy</a>
            </p>
        </div>

        @elseif(substr($user->employee_id,0,1) == 'S')
         <!-- MunshiApp -->
        <div>
            <p>
                <b>MUNSHIAPP INDIA PRIVATE LIMITED</b>, CIN: U72200KA2015PTC079406 
                <br />
                Corp. Office: #18, 2nd Cross Road, N S Palya, BTM Layout Stage 2, Bengaluru - 76, India
            </p>
            <p> 
                +91 80 409 74929 | 
                <a href="mailto:info@geekyants.com">info@geekyants.com</a>
            </p>
        </div>

        

        @elseif(substr($user->employee_id,0,1) == 'G')
         <!-- GeekyAnts -->
        <div>
            <p>
                <b>GEEKYANTS SOFTWARE PRIVATE LIMITED</b>, CIN:  U72900KA2015PTC083390
                <br />
                Corp. Office: #18, 2nd Cross Road, N S Palya, BTM Layout Stage 2, Bengaluru - 76, India
            </p>
            <p> 
                +91 80 409 74929 | 
                <a href="mailto:info@geekyants.com">info@geekyants.com</a> | 
                <a href="https://geekyants.com/" target="_blank">www.geekyants.com</a>
            </p>
        </div>
        @endif
    </div>
</div>

