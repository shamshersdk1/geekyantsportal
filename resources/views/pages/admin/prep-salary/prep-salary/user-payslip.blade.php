@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-6">
                <h1 class="admin-page-title">Prepare Salary</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li class="active">Payslips Salary</li>
                    <li class="active">{{$month->formatMonth()}}</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-10 col-lg-offset-1">
            <div class="panel panel-default">
                <div class="panel-body">@include('pages.admin.prep-salary.prep-salary.partial.payslip-view')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection