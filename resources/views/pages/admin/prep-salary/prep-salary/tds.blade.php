@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
		<div class="row">
            <div class="col-sm-6">
				<h1 class="admin-page-title">TDS View</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                      <li><a href="{{ url('admin/prep-salary') }}">Prep Salary</a></li>
                      <li><a href="{{ url('admin/prep-salary/'.$prepSalary->id) }}">{{$prepSalary->month->formatMonth()}}</a></li>
        		  	<li>Tds View</li>
                </ol>
			</div>
			
			
        </div>
	</div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
	<a class="btn btn-primary btn-sm crude-btn" href="tds/download"><i class="fa fa-download"></i> &nbsp;Download CSV</a>

	<div class="row">
		<div class="col-md-12">
		
			<div class="panel panel-default">

				<div class="panel panel-default">
				<table class="table table-striped" id="tds-table">
				@if(isset($prepTdsObjs))
				<thead>
					<th class="text-left">#</th>
					<th class="text-left">Employee Id</th>
					<th class="text-left">Employee Name</th>
                    @foreach($prepTdsObjs[0]->components as $prepTds)
						<th class="text-left">{{nameFromTDS($prepTds->key)}}</th>
                    @endforeach
				</thead>
				@foreach($prepTdsObjs as $prepTdsObj)
						<tr>
                            <td class="td-text">{{$index+1}}</td>
                            <td class="td-text"> {{$prepTdsObj->user->employee_id}}</td>
                            <td class="td-text"> {{$prepTdsObj->user->name}}</td>
                            @foreach ($prepTdsObj->components as $item)
                            <td class="td-text">{{$item->value}} </td>
                            @endforeach
						</tr>
				@endforeach
				@else
					<tr >
						<td colspan="6" class="text-center">Nothing found.</td>
					</tr>
				@endif
				</table>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
 $(document).ready(function() {
        var t = $('#tds-table').DataTable( {
            pageLength:500, 
            "columnDefs": [ {
                "searchable": true,
                "orderable": true,
                "targets": 0
            } ],
            "order": [[ 1, 'asc' ]],
            fixedHeader: true
        } );
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    });
</script>
@endsection
