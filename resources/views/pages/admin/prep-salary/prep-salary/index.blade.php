@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
		<div class="row">
            <div class="col-sm-6">
				<h1 class="admin-page-title">Prepare Salary</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
					<li class="active">Prepare Salary</li>
                </ol>
			</div>
			<div class="col-sm-6">
				<div class="form-group col-md-6 ">
				{{ Form::open(['url' => '/admin/prep-salary/create', 'method' => 'post']) }}
					<div class="col-md-6 float-right" style="background-color: white;padding:0px;">
						{{ Form::select('id', $monthList,'Select Month', array('class' => 'userSelect'  ,'placeholder' => 'Select Month'),['align' => 'left']);}}
					</div>
					{{ Form::submit('Add',['class' => 'btn btn-success btn-sm']) }}
				{{ Form::close() }}
				</div>
				<div class="col-md-6">
					<a href="/admin/prep-salary/salary-sheet-comparison" class="btn btn-warning btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>Compare Salary Sheet</a>
				</div>
			</div>
					
		</div>
	</div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>

	<div class="row">
		<div class="col-md-12">
		
			<div class="panel panel-default">
				<div class="panel panel-default">
				<table class="table table-striped" id="salary-table">
				<thead>
					<th width="10%">#</th>
					<th width="10%">Month</th>
					<th width="15%">Status</th>
                    <th width="15%">Created At</th>
					<th  class="text-right">Actions</th>
				</thead>
				@if(isset($prepSalaries))
				@if(count($prepSalaries) > 0)
					@foreach($prepSalaries as $prepSalarie)
						<tr>
							<td class="td-text">{{$prepSalarie->id}}</td>
							<td class="td-text"> {{$prepSalarie->month->formatMonth()}}</td>
							<td class="td-text"> 
								@if($prepSalarie->status == 'open')
								<span class="label label-primary">Open<span>
								@elseif($prepSalarie->status == 'in_progress')
								<span class="label label-danger">In Progress<span>
								@elseif($prepSalarie->status == 'processed')
								<span class="label label-danger">Processed<span>
								@elseif($prepSalarie->status == 'finalizing')
								<span class="label label-danger">Finalizing<span>
								@elseif($prepSalarie->status == 'discarded')
								<span class="label label-danger">Discarded<span>
								@else
								<span class="label label-warning">{{$prepSalarie->status}}<span>
								@endif
								</td>
                            <td class="td-text"> {{datetime_in_view($prepSalarie->created_at)}}</td>
							<td class="text-right">
								<div style="display:inline-block;" class="lockform">
									{{ Form::open(['url' => '/admin/prep-salary/'.$prepSalarie->id.'/salary-sheet', 'method' => 'get']) }}
									{{ Form::submit('Salary Sheet',['class' => 'btn btn-info btn-sm crude-btn']) }}
									{{ Form::close() }}
								</div>
								@if($prepSalarie->status == 'open')
								<div style="display:inline-block;" class="lockform">
									{{ Form::open(['url' => '/admin/prep-salary/'.$prepSalarie->id.'/lock', 'method' => 'get']) }}
									{{ Form::submit('Lock',['class' => 'btn btn-primary btn-sm crude-btn','onclick' => 'return confirm("Are you sure you want to Lock this item?")']) }}
									{{ Form::close() }}
								</div>
								@endif
								<a href="/admin/prep-salary/{{$prepSalarie->id}}" class="btn btn-success btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View</a>
								@if($prepSalarie->status == 'open')
								<div style="display:inline-block;" class="deleteform">
								{{ Form::open(['url' => '/admin/prep-salary/'.$prepSalarie->id, 'method' => 'delete']) }}
								{{ Form::submit('Delete',['class' => 'btn btn-danger','onclick' => 'return confirm("Are you sure you want to delete this item?")']) }}
								{{ Form::close() }}
								</div>
								@endif
								@if($prepSalarie->status == 'open' || $prepSalarie->status == 'in_progress')
								<div style="display:inline-block;" class="deleteform">
								{{ Form::open(['url' => '/admin/prep-salary/'.$prepSalarie->id.'/discard', 'method' => 'get']) }}
								{{ Form::submit('Discard',['class' => 'btn btn-primary','onclick' => 'return confirm("Are you sure you want to discard this item?")']) }}
								{{ Form::close() }}
								</div>
								@endif
							</td>
						</tr>
					@endforeach
				@else
					<tr >
						<td colspan="6" class="text-center">No prepare salary added.</td>
					</tr>
				@endif
				@endif
				</table>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
 $(document).ready(function() {
	 	var t = $('#salary-table').DataTable( {
            pageLength:500, 
            "ordering": false,
            "columnDefs": [ {
                "searchable": false,
                "targets": 0
            } ],
            fixedHeader: true
        } );
        
        $('#effectiveDate').datetimepicker({
            format: 'YYYY-MM-DD'
        });
		$('.userSelect').select2({
			placeholder: 'Select a user',
			allowClear:true
		});

        $('.userSelect1').select2({
            placeholder: 'Select a user',
            allowClear:true
        });
        $('#selectid2').select2({
            placeholder: 'Select a user',
            allowClear:true
        }); 
    });
</script>
@endsection