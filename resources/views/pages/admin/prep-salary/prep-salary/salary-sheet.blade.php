@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-6">
                <h1 class="admin-page-title">Salary Transaction</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="/admin/prep-salary">Prep Salary</a></li>
                    <li class="active">Salary Sheet for {{$prepSalObj->month ? $prepSalObj->month->formatMonth() : ""}}</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                    @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif
        </div>
    </div>
    	<a class="btn btn-primary btn-sm crude-btn" href="salary-sheet/download"><i class="fa fa-download"></i> &nbsp;Download CSV</a>
    <div class="user-list-view">
        <div class="panel panel-default">
            <table class="table table-striped" id="summary-table">
                <thead>
                    <th class="text-center">#</th>
                    <th class="text-center">Employee Id</th>
                    <th class="text-center">Name</th>
                    <th class="text-center">Bank</th>
                    <th class="text-center">Monthly Gross Salary</th>
                    <th class="text-center">Basic</th>
                    <th class="text-center">HRA</th>
                    <th class="text-center">Car Allowance</th>
                    <th class="text-center">Food Allowance</th>
                    <th class="text-center">Leave Travel Allowance</th>
                    <th class="text-center">Special Allowance</th>
                    <th class="text-center">Stipend</th>
                    <th class="text-center">PF Employee</th>
                    <th class="text-center">PF Employeer</th>
                    <th class="text-center">PF Other</th>
                    <th class="text-center">ESI</th>
                    <th class="text-center">Professional Tax</th>
                    <th class="text-center">VPF Deduction</th>
                    <th class="text-center">Medical Insurance</th>
                    <th class="text-center">Food Deduction</th>
                    <th class="text-center">Loan Deduction</th>
                    <th class="text-center">Adhoc Amount</th>
                    <th class="text-center">TDS</th>
                    <th class="text-center">Appraisal Bonus</th>
                    <th class="text-center">Onsite Bonus</th>
                    <th class="text-center">Referral Bonus</th>
                    <th class="text-center">Tech Talk Bonus</th>
                    <th class="text-center">Additional Work Day Bonus</th>
                    <th class="text-center">Performance Bonus</th>
                    <th class="text-center">User Timesheet Extra</th>
                    <th class="text-center">Total Other Bonus</th>
                    <th class="text-center">Total Deduction</th>
                    <th class="text-center">Net Payable</th>
                </thead>
                <tbody>
                @if(count($users) > 0)
                    @foreach($users as $user)
                    @if(isset($data[$user->id]))
                        <tr>
                            <td class="text-center"></td>
                            <td class="text-center">{{ $user->employee_id}}</td>
                            <td class="text-center">{{ $user->name}}</td>
                            <td class="text-center">{{$data[$user->id]['Bank'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['Monthly_Gross_Salary'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['Basic'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['HRA'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['Car_Allowance'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['Food_Allowance'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['Leave_Travel_Allowance'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['Special_Allowance'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['Stipend'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['PF_Employee'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['PF_Employeer'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['PF_Other'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['ESI'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['Professional_Tax'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['VPF_Deduction'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['Medical_Insurance'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['Food_Deduction'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['Loan_Deduction'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['Adhoc_Amount'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['TDS'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['Appraisal_Bonus'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['Onsite_Bonus'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['Referral_Bonus'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['TechTalk_Bonus'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['Additional_Work_Day_Bonus'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['Performance_Bonus'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['User_Timesheet_Extra'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['Total_Other_Bonus'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['Total_Deduction'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['Net_Payable'] ?? 0}}</td>
                        </tr> 
                    @endif
                    @endforeach
                @else
                    <tr>
                        <td colspan="6" class="text-center">
                            No Records found
                        </td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $tableHeight = $( window ).height();
        var t = $('#summary-table').DataTable( {
            pageLength:500,
            fixedHeader: true,
            dom: 'Bfrtip',
            buttons: [
                'csv'
            ],
            scrollY: $tableHeight - 200,
            scrollX: true,
            scrollCollapse: true,
            paging:         false,
            fixedColumns:   {
                leftColumns: 3,
            },
        } );
        
    });
</script>
@endsection
