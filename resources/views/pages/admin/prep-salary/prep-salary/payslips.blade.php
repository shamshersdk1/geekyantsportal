@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-6">
                <h1 class="admin-page-title">Payslip</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="/admin/bank-transfer">Bank Transfer</a></li>
                    <li class="active">Salary Payslips</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 pull-right">
            @if(isset($monthList))
                <select id="selectid4" name="month"  placeholder= "{{$monthObj->formatMonth()}}">
                    <option value=""></option>
                    @foreach($monthList as $x)
                        <option value="{{$x->id}}" >{{$x->formatMonth()}}</option>
                    @endforeach
                </select>
            @endif
        </div>
    </div>
    <div class="row">

        <div class="col-md-12">
            <h4>List of Payslip for the month of {{$monthObj->formatMonth()}}</h4>
            <div class="panel panel-default">
                    <table class="table table-striped" id="payslip-table">
                        <thead>
                        <tr>
                            <td>#</td>
                            <td> User Id </td>
                            <td> Employee ID</td>
                            <td>Name</td>
                            <td>&nbsp;</td>
                        </tr>
                        </thead>
                    @foreach($transactions as $user_transction)
                        <tr>
                            <td></td>
                            <td>{{$user_transction->user->id}}</td>
                            <td>{{$user_transction->user->employee_id}}</td>
                            <td>{{$user_transction->user->name}}</td>
                            <td>
                                <a href="/admin/payslips/{{$monthId}}/{{$user_transction->user->id}}" class="btn btn-primary" target="_blank">View Payslip</a>
                                <a href="/admin/payslips/{{$monthId}}/{{$user_transction->user->id}}/download" class="btn btn-primary">Download Payslip</a>
                                <a href="/admin/tds-breakdown/{{$monthId}}/{{$user_transction->user->id}}" class="btn btn-primary" target="_blank">View TDS Breakdown</a>
                                <a href="/admin/tds-breakdown/{{$monthId}}/{{$user_transction->user->id}}/download" class="btn btn-primary">Download TDS Breakdown</a>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        var t = $('#payslip-table').DataTable( {
            pageLength:500, 
            scrollY:        true,
            scrollX:        true,
            scrollCollapse: true,
            paging:         false,
            "columnDefs": [ {
                "searchable": false,
                "orderable": false,
                "targets": 0
            } ],
            "order": [[ 1, 'asc' ]],
            fixedHeader: true
        } );
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    });
    $('#selectid4').change(function(){
        var optionSelected = $("option:selected", this);
        optionValue = this.value;
        if (optionValue) { 
            window.location = "/admin/payslips/"+optionValue; 
        }
    });
    
</script>
@endsection
