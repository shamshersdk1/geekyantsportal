@extends('layouts.admin-dashboard')
@section('main-content')

<section class="new-project-section">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">Seats</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
			  			<li><a href="{{ url('admin/seat-management') }}">Floors</a></li>
			  			<li class="active">Add Seat</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>

	    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif

		<form class="form-horizontal" method="post" action="/admin/floors-seat" enctype="multipart/form-data">
			<div class="panel panel-default">
				<div class="panel-body">
				    <div class="row">
				    	<div class="col-md-12">
						  	<div class="form-group">
								<label for="" class="col-sm-2 control-label">Floor</label>
								<div class="col-sm-6">
									@if(isset($floor))
										<label>{{$floor->name}}</label>
										<input type="hidden" id="" name="floor_id" value="{{ $floor->id}}">
									@endif
									@if(isset($floors))
										<select id="selectid3" name="floor_id" style="width=35%;" placeholder= "Select an option">
											<option value=""></option>
												@foreach($floors as $x)
													<option value="{{$x->id}}" >{{$x->name}}</option>
												@endforeach
										</select>
									@endif
								</div>
							</div>
							<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Number of Seats to be added</label>
						    	<div class="col-sm-6">
						      		<input type="number" class="form-control" id="" name="number_of_seats" value="{{ old('number_of_seats') }}">
						    	</div>
						  	</div>
					  	</div>
					</div>
				</div>
			</div>
  	  		<div class="text-center">
		  		<button type="submit" class="btn btn-success">Create Seat</button>
		  	</div>
		</form>
	</div>
</section>
@endsection
