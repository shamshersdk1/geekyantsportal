@extends(Request::is('admin/view/*')?'pages.admin.userView.partials.usernav':'pages.admin.userView.partials.nav')
@section('sub-content')
<style>
    td{
        padding-bottom : 1% !important;
        color: #888;
    }
</style>
<div class="col-xs-12 col-sm-12 col-md-12 user-loan">
    <div class="panel panel-default" style="overflow:auto">
            <table class="table table-striped">
                <th>Amount</th>
                <th>EMI</th>
                <th class="text-right">Action</th>
                @if(count($loans) > 0)
                    @foreach($loans as $loan)
                        <tr>
                            <td class="td-text">
                                {{$loan->amount}}
                            </td>
                            <td class="td-text">
                                {{$loan->emi}}
                            </td>
                            <td class="td-text text-right">
                                    @if(Request::is('admin/users/*'))
                                    <a href="/admin/users/{{$user->id}}/loans/{{$loan->id}}" class="btn btn-info crud-btn btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View</a>    
                                    <form action="/admin/loans/{{$loan->id}}" method="post" style="display:inline-block;" class="deleteform">
                                        <input name="_method" type="hidden" value="DELETE">
                                        <button class="btn btn-danger crud-btn btn-sm" type="submit"><i class="fa fa-trash btn-icon-space" aria-hidden="true"></i>Delete</button>
                                    </form>
                                    @else
                                    <a href="/admin/view/loans/{{$loan->id}}" class="btn btn-info crud-btn btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View</a>    
                                    @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="3">No results found.</td>
                    </tr>
                @endif
        </table>
    </div>
</div>
@endsection