@extends(Request::is('admin/view/*')?'pages.admin.userView.partials.usernav':'pages.admin.userView.partials.nav')
@section('sub-content')
<div class="col-xs-12 col-sm-12 col-md-12" style="margin-top:-2%">
<h3><b style="color:#888">All Birthdays</b></h3>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
    <div class="panel panel-default" style="overflow:auto">
        <table class="table table-striped">
            <th>Name</th>
            <th class="text-right">Birthday</th>
            @if(empty($bdays))
                <tr>
                    <td>
                        Data unavailable
                    </td>
                </tr>
                @else
                @foreach($bdays as $array)
                @if($array['active']==1)
                    <tr style="border-left: 5px solid #269900">
                @else  
                    <tr>
                @endif
                    <td style="line-height:2em">
                        {{$array['user']->name}}
                    </td>
                    <td class="text-right" style="line-height:2em">
                        {{date('j M',strtotime($array['user']->dob))}}
                    </td>
                </tr>
                @endforeach
                @endif
        </table>
    </div>
</div>
@endsection