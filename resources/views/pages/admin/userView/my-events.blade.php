@extends(Request::is('admin/view/*')?'pages.admin.userView.partials.usernav':'pages.admin.userView.partials.nav')
@section('sub-content')
<div class="col-xs-12 col-sm-12 col-md-12" style="margin-top:-2%">
<h3><b style="color:#888">My Events</b></h3>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
    <div class="panel panel-default" style="overflow:auto">
        <table class="table table-striped">
            <th width="70%">Name</th>
            <th class="text-right">Date</th>
            <th class="text-right">Time</th>
           @if(empty($events))
                <tr>
                    <td colspan="3">
                        No Upcoming Events
                    </td>
                </tr>
            @else
                @foreach($events as $event)
                <tr>
                    <td>
                        {{$event->title}}
                    </td>
                    <td class="text-right">
                        {{date('j M',strtotime($event->date))}}
                    </td>
                    <td class="text-right">
                        {{date('g:i a',strtotime($event->time))}}
                    </td>
                </tr>
                @endforeach
            @endif
        </table>
    </div>
    @if(!empty($events))
        {{$events->links()}}
    @endif
</div>
@endsection