<div class="modal fade" id="editNewsModal" tabindex="-1" role="dialog" aria-labelledby="editNewsModalLabel">
   <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="editNewsModalLabel">Edit News</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 ma-bot-10">
                            <div class="form-group ma-bot-10">
                                <div class="col-md-6">
                                    <label for="title">Enter News Title:</label>
                                    <input type="text" name="title" class="form-control" placeholder="Enter news title" value="{{ old('title') }}" required>
                                </div>
                                <div class="col-md-6">
                                    <label for="date">Date:</label>
                                        <div class='input-group date' id='adddatetimepicker'>
                                        <input type='text' class="form-control" autocomplete="off" name="date" value="{{old('date')}}"  placeholder="Date" required/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 ma-top-10">
                            <div class="form-group" style="padding:0% 2.5% 0% 2.5%">
                                <label for="">Description</label>
                                <textarea class="form-control" rows="5" placeholder="Please type description" name="description" required></textarea>
                            </div>
                        </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>&nbsp;
               
                    <button type="submit" class="btn btn-primary" onclick="updateNews()">
                        Update
                    </button>
               
            </div>
        </div>
    </div>
</div>
