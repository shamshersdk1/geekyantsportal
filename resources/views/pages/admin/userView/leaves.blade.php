@extends(Request::is('admin/view/*')?'pages.admin.userView.partials.usernav':'pages.admin.userView.partials.nav')
@section('sub-content')
<div class="row apply-leave"  style="padding:0% 1.3% 0% 1.3%">
    <div class="col-xs-12 col-sm-12 col-md-5 ">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="leave-stat">
                    <div class="num">
                        <span>{{$consumedSickLeaves}}</span>
                        <span class="devider">/</span>
                        <span>{{$totalSickLeaves}}</span>
                    </div>
                    <div class="text">
                        <div class="stat-heading"> Sick Leave</div>
                        <div class="stat-info">
                            <div><b>{{$consumedSickLeaves}}</b> Consumed</div>
                            <div><b>{{$totalSickLeaves - $consumedSickLeaves}}</b> Available</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="leave-stat">
                    <div class="num">
                        <span>{{$consumedPaidLeaves}}</span>
                        <span class="devider">/</span>
                        <span>{{$totalPaidLeaves}}</span>
                    </div>
                    <div class="text">
                        <div class="stat-heading"> 
                            Paid Leave
                            
                            @if($carryForwardLeaves)
                            <span class="label label-primary custom-label pull-right carry-leaves" placement="top" tooltip="You have {{ $carryForwardLeaves }} carry forward leave(s) which has been added to your Paid leaves">
                                    <i class="fa fa-info"></i>
                                </span>
                            @endif
                        </div>
                        <div class="stat-info">
                            <div><b>{{$consumedPaidLeaves}}</b> Consumed</div>
                            <div><b>{{$totalPaidLeaves - $consumedPaidLeaves}}</b> Available</div>
                            <!-- <div><b>{{$carryForwardLeaves}}</b> Carry</div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin-top:1%">
            <div class="col-md-12">
                <form action="/admin/employee-leave/requestLeave" method="post">
                    <h5>APPLY FOR LEAVE</h5>
                    <div class="panel panel-default select-leave-type">
                        <div class="row">
                            <div class="col-md-12 ma-bot-10">
                                <div class="form-group ma-bot-10">
                                    <label for="">Select Leave Type</label>
                                    <select class="form-control" name="type">
                                        <option>Select Leave type</option>
                                        @if (old('type') == 'sick')
                                                <option value="sick" selected>Sick Leave</option>
                                        @else
                                                <option value="sick">Sick Leave</option>
                                        @endif
                                        @if (old('type') == 'paid')
                                                <option value="paid" selected>Paid Leave</option>
                                        @else
                                                <option value="paid">Paid Leave</option>
                                        @endif
                                        @if (old('type') == 'unpaid')
                                                <option value="unpaid" selected>Unpaid Leave</option>
                                        @else
                                                <option value="unpaid">Unpaid Leave</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <!-- ======================== Date Picker ======================= -->
                            <div class="col-md-12 no-padding ma-bot-10">
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <div class='input-group date' id='datetimepickerStart'>
                                            <input type='text' id="datetimeinputstart" class="form-control" value="{{old('start_date')}}" name="start_date" placeholder="Start Date" required autocomplete="off" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class='input-group date' id='datetimepickerEnd'>
                                            <input type='text' class="form-control" name="end_date" placeholder="End Date" value="{{old('end_date')}}" required autocomplete="off" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- ====================== reasones ================ -->
                            <div class="col-md-12 ma-top-10">
                                <div class="form-group">
                                    <label for="">Notes</label>
                                    <textarea class="form-control" rows="5" placeholder="Please enter reason for applying leave" name="reason">{{trim(old('reason'))}}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-success crud-btn" type="submit">Apply Leave</button>
                </form>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-7">
        <div class="panel panel-default" style="margin-bottom:0">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead >
                        <th>Duration</th>
                        <th>Days</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th class="text-right">Actions</th>
                    </thead>
                    @if(count($leaves) > 0)
                        @foreach($leaves as $leave)
                            <tr>
                                <td class="td-text">{{date('j M Y',strtotime($leave->start_date))}} to {{date('j M Y',strtotime($leave->end_date))}}</td>
                                @if($leave->days==0.5)
                                    <td>{{ucfirst($leave->half)}} half</td>
                                @else
                                    <td>{{floor($leave->days)}}</td>
                                @endif
                                <td class="td-text">
                                @if($leave->type=='half')
                                        {{ ucfirst($leave->type)}} Day Leave
                                @else
                                        {{ ucfirst($leave->type)}} Leave
                                @endif
                                </td>
                                <td >
                                    @if($leave->status == "approved")
                                        <span class="label label-success custom-label">Approved</span>
                                        <small style="display:block">by @if(!empty($leave->approver)){{$leave->approver->name}}@endif</small>
                                    @elseif($leave->status == "pending")
                                        <span class="label label-info custom-label">Pending</span>
                                    @elseif($leave->status == "rejected")
                                        <span class="label label-danger custom-label">Rejected</span>
                                    @elseif($leave->status == "cancelled")
                                        <span class="label label-primary custom-label">Cancelled</span>
                                    @else
                                        <span class="label label-info custom-label">No Status</span>
                                    @endif
                                </td>
                                <td class="text-right">
                                    @if(Request::is('admin/users/*'))
                                        <button class="btn btn-info btn-sm" type="button"
                                            data-toggle="modal" data-target="#editLeaveModal"
                                            data-id="{{$leave->id}}" data-title="Algorithms"><i class="fa fa-eye"></i> View</button>
                                    @else
                                    <button class="btn btn-info btn-sm" type="button"
                                                    data-toggle="modal" data-target="#viewLeaveModal"
                                                    data-id="{{$leave->id}}" data-title="Algorithms"><i class="fa fa-eye"></i> View</button>
                                        @if(($leave->status == "pending" || $leave->status == "approved")&&($leave->start_date > date('Y-m-d')))
                                            <form action="/admin/leave-section/status/{{$leave->id}}" style="display:inline" method="post" class="cancelForm">
                                                <button class="btn btn-danger btn-sm" name="cancel" value="cancel">Cancel</button>
                                            </form>
                                        @else
                                            <button class="btn btn-danger btn-sm" name="cancel" value="cancel" disabled>Cancel</button>
                                        @endif
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        
                    @else
                        <tr>
                            <td colspan="3">No results found.</td>
                            <td></td>
                            <td></td>
                        </tr>
                    @endif
                </table>
            </div>
        </div>
        @if(count($leaves) > 0)
            {{$leaves->links()}}
            @endif
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#datetimepickerStart').datetimepicker({
            //Set max date to upcoming march
            maxDate: new Date().getMonth() <= 2 ? new Date(new Date().getFullYear()+"-12-31") : new Date(new Date().getFullYear()+1+"-12-31"),
            minDate: new Date().getMonth() <= 3 ? new Date(new Date().getFullYear()-1+"-01-01") : new Date(new Date().getFullYear()+"-01-01"), //Set min date to April 1
            format: 'YYYY-MM-DD'
        });

        $('#datetimepickerEnd').datetimepicker({
            //Set max date to upcoming march
            maxDate: new Date().getMonth() <= 2 ? new Date(new Date().getFullYear()+"-12-31") : new Date(new Date().getFullYear()+1+"-12-31"),
            format: 'YYYY-MM-DD'
        });
        if($("#datetimeinputstart").val()!="")
        {
            $('#datetimepickerEnd').data("DateTimePicker").minDate($("#datetimeinputstart").val());
        }
        $("#datetimepickerStart").on("dp.change", function (e) {
            $('#datetimepickerEnd').data("DateTimePicker").minDate(e.date);
        });
    })
</script>


@include('pages/admin/admin-leave-section/edit-leave-modal')
@include('pages/admin/admin-leave-section/javascript')
@include('pages/admin/employee-leave/view-leave-modal')
@include('pages/admin/employee-leave/javascript')
@endsection