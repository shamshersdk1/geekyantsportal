@extends(Request::is('admin/view/*')?'pages.admin.userView.partials.usernav':'pages.admin.userView.partials.nav')
@section('sub-content')
<style>
    .profile-grid{
        display:grid;
        grid-template-columns:49% 49%;
        grid-template-rows:49% 49%;
        margin:0;
        grid-gap:2%;
        background-color:transparent;
        text-align:left;
    }
    .panel{
        margin-bottom:0;
    }
    .two-part-list {
    columns: 2;
    -webkit-columns: 2;
    -moz-columns: 2;
    }
    .fab {
       width: 60px;
       height: 60px;
       background-color: #5bc0de;
       border-radius: 50%;
       box-shadow: 0 6px 10px 0 #666;
       
       font-size: 25px;
       line-height: 65px;
       color: white;
       text-align: center;
       
       position: fixed;
       right: 50px;
       bottom: 50px;
       
      transition: all 0.1s ease-in-out;
    }

.fab:hover {
   box-shadow: 0 6px 14px 0 #666;
   transform: scale(1.05);
}
</style>
<div class="dashboard-container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="profile-grid">
            <div class="panel panel-default profile-grid-item">
                <br/>
                <h5 style="padding-left: 2%; margin-top: 0;">INTRODUCTION</h5>
                <p align="justify" style="padding:0% 2% 0% 2%;font-size:1em;color:#aaa">@if(!empty($user->profile->intro)){{$user->profile->intro}}@endif</p>
            </div>
            <div class="panel panel-default profile-grid-item">
                <br/>
                <h5 style="padding-left: 2%; margin-top: 0;">SKILLS</h5>
                @if($keywords!="")
                @foreach($keywords as $keyword)
                <p type="button" class="btn btn-primary" style="background-color:transparent;border-color:#007bb6;margin:0% 0% 2% 2%;padding:1% 3% 1% 3%;font-size:1em;color:#aaa;border-radius:25px">{{$keyword}}</p>
                @endforeach
                @endif
            </div>
            <div class="panel panel-default profile-grid-item">
                <br/>
                <h5 style="padding-left: 2%; margin-top: 0;">EXPERIENCE</h5>
                @if($skills!="")
                <ul style="list-style:none;padding:0% 2% 2% 2%">
                @foreach($skills as $skill)
                <li style="margin-bottom:1%;font-size:1em;color:#aaa;">{{$skill['skill'] }}<span class="label" style="background-color:{{$colours[rand(0,count($colours)-1)]}}">{{ $skill['percentage']}} %</span></li>
                @endforeach
                </ul>
                @endif
            </div>
            <div class="panel panel-default profile-grid-item">
                <br/>
                <h5 style="padding-left: 2%; margin-top: 0;">INTEREST & EXPERTISE</h5>
                @if($knowledge!="")
                <ul class="two-part-list" style="list-style:none;padding:0% 2% 2% 2%">
                @foreach($knowledge as $item)
                <li style="margin-bottom:1%;font-size:1em;color:#aaa;">» &nbsp{{$item}}</li>
                @endforeach
                </ul>
                @endif
            </div>
            <div class="panel panel-default profile-grid-item">
                <br/>
                <h5 style="padding-left: 2%; margin-top: 0;">Contact Info</h5>
                <div class="col-md-6">
                    <ul>
                        @if ( !empty($user->user_contact->mobile) )
                        <li class="user-number">
                                <i class="fa fa-phone"></i>
                                <a href="tel:{{$user->user_contact->mobile}}">
                                {{$user->user_contact->mobile}} </a>
                        </li>
                        @endif
                        @if ( !empty($user->user_contact->office_email) )
                        <li class="user-number">
                                <i class="fa fa-envelope"></i>
                                <a href="mailto:{{$user->user_contact->office_email}}">
                                {{$user->user_contact->office_email}} </a>
                        </li>
                        @endif
                        @if ( !empty($user->user_contact->emergency_contact_number) )
                        <li class="user-number">
                                <i class="fa fa-medkit"></i>
                                <a href="tel:{{$user->user_contact->emergency_contact_number}}">
                                {{$user->user_contact->emergency_contact_number}} </a>
                        </li>
                        @endif
                        @if ( !empty($user->user_contact->personal_email) )
                        <li class="user-number">
                                <i class="fa fa-envelope"></i>
                                <a href="mailto:{{$user->user_contact->personal_email}}">
                                {{$user->user_contact->personal_email}} </a>
                        </li>
                        @endif
                        @if ( !empty($user->user_contact->address_line_1) || !empty($user->user_contact->address_line_2) 
                        || !empty($user->user_contact->city) || !empty($user->user_contact->state) ||  !empty($user->user_contact->pin_code) )
                        <li class="user-number">
                            <div>
                               {{!empty($user->user_contact->address_line_1) ? $user->user_contact->address_line_1 : '' }}
                            </div>
                            <div>
                                {{!empty($user->user_contact->address_line_2) ? $user->user_contact->address_line_2 : '' }}
                            </div>
                            <div>
                                {{!empty($user->user_contact->city) ? $user->user_contact->city : '' }}
                            </div>
                            <div>
                                {{!empty($user->user_contact->state) ? $user->user_contact->state : '' }}
                            </div>
                            <div>
                                {{!empty($user->user_contact->pin_code) ? $user->user_contact->pin_code : '' }}
                            </div>    
                        </li>
                        @endif
                    </ul>
                </div>
                <div class="col-md-6">
                    <ul>
                        @if ( !empty($user->user_contact->skype_id) )
                        <li class="user-number">
                                <i class="fa fa-skype"></i>
                                <a href="skype:{{$user->user_contact->skype_id}}">
                                {{$user->user_contact->skype_id}} </a>
                        </li>
                        @endif
                        @if ( !empty($user->user_contact->git_hub_url) )
                        <li class="user-number">
                                <i class="fa fa-github"></i>
                                <a href="{{$user->user_contact->git_hub_url}}">
                                {{$user->user_contact->git_hub_url}} </a>
                        </li>
                        @endif
                        @if ( !empty($user->user_contact->apple_id) )
                        <li class="user-number">
                                <i class="fa fa-apple"></i>
                                <a href="">
                                {{$user->user_contact->apple_id}} </a>
                        </li>
                        @endif
                        @if ( !empty($user->user_contact->bit_bucket_url) )
                        <li class="user-number">
                                <i class="fa fa-bitbucket"></i>
                                <a href="{{$user->user_contact->bit_bucket_url}}">
                                {{$user->user_contact->bit_bucket_url}} </a>
                        </li>
                        @endif
                    </ul>
                </div>
                
            </div>  
        </div>
        </div>
    </div>
    @if(Request::is('admin/users/*'))
    <div class="fab">
        <a href="{{Request::url()}}/edit" style="color:white"><i class="fa fa-pencil"></i></a>
    </div>
    @endif  
</div>
@endsection

