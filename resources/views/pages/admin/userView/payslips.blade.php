@extends(Request::is('admin/view/*')?'pages.admin.userView.partials.usernav':'pages.admin.userView.partials.nav')
@section('sub-content')
<style>
    td{
        padding-bottom : 1% !important;
    }
</style>
<div class="col-xs-12 col-sm-12 col-md-12 payslip-user">
    <div class="panel panel-default" style="overflow:auto">
            <table class="table table-striped">
                <th>Month</th>
                <th>No. of working days</th>
                <th>No. of Days Worked</th>
                @if(Request::is('admin/users/*'))
                <th>Gross Salary</th>
                <th>Loan</th>
                <th>Total Deduction</th>
                <th>Annual Bonus</th>
                <th>Qtr Bonus</th>
                <th>Performance Bonus</th>
                @endif
                <th>Amount Payable</th>
                <th class="text-right">Action</th>
                <?php $c=0;?>
                @if(count($result) > 0)
                    @foreach($result['data'] as $data)
                        <tr>
                            <td class="td-text">
                                @if(!empty($result['payslip'][$c]['month'])){{DateTime::createFromFormat('!m',$result['payslip'][$c]['month'])->format('F')}},@endif
                                @if(!empty($result['payslip'][$c]['year'])){{$result['payslip'][$c]['year']}} &nbsp&nbsp @endif
                            </td>
                            <td class="td-text">
                                {{$data->working_day_month}}
                            </td>
                            <td class="td-text">
                                {{$data->days_worked}}
                            </td>
                            @if(Request::is('admin/users/*'))
                            <td class="td-text">
                                    {{$data->gross_salary}}
                            </td>
                            <td class="td-text">
                                    {{$data->loan}}
                            </td>
                            <td class="td-text">
                                    {{$data->total_deduction_month}}
                            </td>
                            <td class="td-text">
                                    {{$data->annual_bonus}}
                            </td>
                            <td class="td-text">
                                    {{$data->qtr_variable_bonus}}
                            </td>
                            <td class="td-text">
                                    {{$data->performance_bonus}}
                            </td>
                            @endif
                            <td class="td-text">
                                    {{$data->amount_payable}}
                            </td>
                            <td class="td-text text-right">
                                    <a target="_blank" href="/admin/employee-payslip/{{$data->id}}" class="btn btn-info crud-btn btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View</a>    
                            </td>
                        </tr>
                    <?php $c++;?>
                    @endforeach
                @else
                    <tr>
                        <td colspan="11">No results found.</td>
                    </tr>
                @endif
        </table>
    </div>
</div>
@endsection