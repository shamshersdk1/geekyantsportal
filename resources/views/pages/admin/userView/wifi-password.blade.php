@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
<div class="breadcrumb-wrap">
   <div class="row">
      <div class="col-sm-12">
         <h1 class="admin-page-title">WiFi Password</h1>
         <ol class="breadcrumb">
            <li><a href="/admin">User</a></li>
            <li class="active">Reset WiFi Password </li>
         </ol>
      </div>
   </div>
</div>
<div class="row">

    <div class="col-sm-8 col-sm-offset-2">
        <h4>Set your WiFi Password</h4>
        <div class="alert alert-info">
            <div>Set your password for "GeekyAnts WiFi" connection to use it under office premises.</div>
        </div>
            @if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                      @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif
        <div class="panel panel-default">
            <div class="panel-body">
                <form class="form-horizontal" method="POST" action="/user/wifi-password">
                    <div class="form-group m-t-15">
                        <label class="col-sm-4 control-label">Username</label>
                        <div class="col-sm-6">
                            <input type="text" name="user_name" class="form-control" disabled="disabled" value="{{$username}}"/>
                            <input type="hidden" name="username" class="form-control" value="{{$username}}"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Password</label>
                        <div class="col-sm-6">
                            <input type="password" name="password" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Re Enter Password</label>
                        <div class="col-sm-6">
                            <input type="password" name="re_password" class="form-control" />

                            <!-- <div class="small text-danger">Password didn't match</div> -->
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-6 col-sm-offset-4">
                            <button style="submit" name="Sumbit" class="btn btn-success">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
   
</div>

@endsection
