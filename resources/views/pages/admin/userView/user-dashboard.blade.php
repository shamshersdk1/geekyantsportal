@extends(Request::is('admin/view/*')?'pages.admin.userView.partials.usernav':'pages.admin.userView.partials.nav')
@section('sub-content')

<script>
$(function(){
    $('.news-grid,#carousel-left,#carousel-right').hover(
    function () {
        $('#carousel-left').css('opacity','1');
        $('#carousel-right').css('opacity','1');
    }, 
    function () {
        $('#carousel-left').css('opacity','0');
        $('#carousel-right').css('opacity','0');
    });
}); 
</script>
<div class="dashboard-container">
    <div class="row">
        <div class="col-sm-7 col-md-8">
            <div class="row">
                <div class="col-md-12 col-lg-6">
                    <div class="panel panel-default">
                        <div class="leaves-grid">
                            <div class="grid-1">{{$consumedSickLeaves}}/{{$totalSickLeaves}}
                            </div>
                            <div class="grid-2">Sick Leave</div>
                            <div class="grid-1">{{$consumedPaidLeaves}}</div>
                            <div class="grid-2">Paid Leave</div>
                            <div class="grid-1">--</div>
                            <div class="grid-2">Unpaid Leave</div>
                        </div>
                        <a href="/admin/view/leaves" class="btn btn-success apply-leave-btn">Apply Leave</a>
                    </div>
                </div>
                <div class="col-md-12 col-lg-6">
                    <div class="panel panel-default ctc-panel">
                          <div class="panel-heading"> CTC @if(!empty($user->activeAppraisal))&#8377; {{($user->activeAppraisal->in_hand + $user->activeAppraisal->qtr_bonus + $user->activeAppraisal->year_end)}} @else -- @endif
                            </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="ctc-header"> 
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="ctc-parts">
                                    In Hand
                                </div>
                            </div>
                            <div class="col-sm-7">
                                <div class="ctc-parts-value"> 
                                    :&emsp;@if(!empty($user->activeAppraisal->in_hand))&#8377; {{$user->activeAppraisal->in_hand}} @else -- @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="ctc-parts">
                                    Annual Bonus
                                </div>
                            </div>
                            <div class="col-sm-7">
                                <div class="ctc-parts-value"> 
                                    :&emsp;@if(!empty($user->activeAppraisal->year_end))&#8377; {{$user->activeAppraisal->year_end}} @else -- @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="ctc-parts" style="border-bottom: 0px solid;">
                                    Quarterly Bonus
                                </div>
                            </div>
                            <div class="col-sm-7">
                                <div class="ctc-parts-value" style="border-bottom: 0px solid;"> 
                                    :&emsp;@if(!empty($user->activeAppraisal->qtr_bonus))&#8377; {{$user->activeAppraisal->qtr_bonus}} @else -- @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-lg-6">
                    <div class="panel panel-default">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Payslips</th>
                                    <th class="text-right">
                                    @if(!empty($result))
                                    <a href="payslips">View All</a>
                                    @endif
                                    </th>
                                </tr>
                            </thead>
                            @if(!empty($result))
                            @php $c=0; @endphp
                            @foreach($result['data'] as $data)
                            <tbody>
                                <tr>
                                    <td class="td-text">
                                            @if(!empty($result['payslip'][$c]['month'])){{DateTime::createFromFormat('!m',$result['payslip'][$c]['month'])->format('F')}},@endif
                                            @if(!empty($result['payslip'][$c]['year'])){{$result['payslip'][$c]['year']}} &nbsp&nbsp @endif
                                    </td>
                                    <td class="td-text text-right">
                                            <a target="_blank" href="/admin/employee-payslip/{{$data->id}}" class="btn btn-info crud-btn btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View</a>    
                                    </td>
                                </tr>
                            </tbody>
                            @php $c++; @endphp
                            @endforeach
                            @else
                            <tbody>
                                <tr>
                                    <td colspan="2">
                                        No Data Found
                                    </td>
                                </tr>
                            </tbody>
                            @endif
                        </table>
                    </div>
                </div>
                <div class="col-md-12 col-lg-6">
                    <div class="panel panel-default">
                        <div class="ctc-header">My Upcoming Leaves</div>
                        <div class="upcoming-leaves-data-block">
                            @if(empty($nextLeave))
                            <div class="ptb3">
                                No Upcoming Leaves
                            </div>
                            @else
                            <div class="ptb3">
                                {{date('j M Y',strtotime($nextLeave->start_date))}} to {{date('j M Y',strtotime($nextLeave->end_date))}}
                            </div>
                            <div>
                                @if($nextLeave->type=='half')
                                {{ucfirst($nextLeave->type)}} Day Leave
                                @else
                                {{ucfirst($nextLeave->type)}} Leave
                                @endif
                                <div class="pull-right">
                                    <span class="label label-success leave-status-label">Approved</span>
                                    <span>
                                        by @if(!empty($nextLeave->approver)){{$nextLeave->approver->name}}@endif
                                    </span>
                                </div>
                            </div>
                            
                            <form action="/admin/leave-section/status/{{$nextLeave->id}}" method="post" class="rejectform">
                                <button class="btn btn-danger cancel-leave-btn" name="cancel" value="cancel">Cancel</button>
                            </form>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-lg-6">
                    <div class="panel panel-default grid-item">
                        <table class="table table-striped" style="flex:1">
                            <thead>
                                <tr>
                                    <th>Upcoming Events</th>
                                    <th class="text-right">
                                    @if(count($events)>0)
                                        <a href="my-events">View All</a>
                                    @endif
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($events)==0)
                                <tr>
                                    <td colspan="2">
                                        No Upcoming Events
                                    </td>
                                </tr>
                                @else
                                @foreach($events as $event)
                                <tr>
                                    <td>
                                        {{$event->title}}
                                    </td>
                                    <td class="text-right">
                                        <small style="display:block">{{date('j M',strtotime($event->date))}}</small>
                                        at <small>{{date('g:i a',strtotime($event->time))}}</small>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-12 col-lg-6">
                    <div class="panel panel-default">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Upcoming Birthdays</th>
                                    <th class="text-right">
                                    @if(!empty($bdays))
                                        <a href="birthdays">View All</a>
                                    @endif
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(empty($bdays))
                                    <tr>
                                        <td>
                                            No Upcoming Birthdays
                                        </td>
                                    </tr>
                                @else
                                @foreach($bdays as $array)
                                @if($array['active']==1)
                                    <tr style="border-left: 5px solid #269900">
                                @else  
                                    <tr>
                                @endif
                                        <td>
                                            {{$array['user']->name}}
                                        </td>
                                        <td class="text-right">
                                            {{date('j M',strtotime($array['user']->dob))}}
                                        </td>
                                    </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-5 col-md-4">
            <div class="row">
                <div class="col-sm-12">
                        <div class="panel panel-default">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>News</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="bg-white">
                                        <div id="carousel" class="carousel slide" data-ride="carousel">
                                            <!-- Wrapper for slides -->
                                            <div class="carousel-inner" role="listbox">
                                            @if(!empty($news))
                                                @foreach( $news as $newsitem )
                                                    <div class="item {{ $loop->first ? ' active' : '' }}">
                                                        <div>
                                                            <div class="edit-news-head">{{$newsitem->title}}
                                                                <button class="edit-news-btn pull-right" data-toggle="modal" data-target="#editNewsModal"
                                                                data-id="{{$newsitem->id}}" data-title="Algorithms"><i class="fa fa-pencil-square-o fa-fw"></i></button>
                                                            </div>
                                                            <div class="news-date">
                                                                {{date('j M Y',strtotime($newsitem->date))}}
                                                            </div>
                                                            <div class="news">
                                                                {{$newsitem->description}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endif
                                            </div>
                                             <!-- Controls -->
                                            <a id="carousel-left" class="left carousel-control" href="#carousel" role="button" data-slide="prev">
                                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <a id="carousel-right" class="right carousel-control" href="#carousel" role="button" data-slide="next">
                                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Holidays {{date('Y')}}</th>
                                    <th>Day</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(!empty($holidays))
                                @foreach($holidays as $holiday)
                                @if($holiday->date==$active->date)
                                    @if($holiday->type == "Others")
                     <tr style="border-left: 5px solid #00C15F; background:#FFDAB9 !important">
                        <td class="td-text">
                           <small style="font-weight:900;color:#555">
                              {{date('j M Y',strtotime($holiday->date))}} ({{$holiday->reason}}) &nbsp;*Optional*
                           </small>
                        </td>
                  @elseif($holiday->type == "Weekend")
                     <tr style="border-left: 5px solid #00C15F; background-color:#AFEEEE">
                        <td class="td-text">
                           <small style="font-weight:900;color:#555">
                              {{date('j M Y',strtotime($holiday->date))}} ({{$holiday->reason}}) &nbsp;*Weekend*
                           </small>
                        </td>
                  @else 
                     <tr style="border-left: 5px solid #00C15F">
                        <td class="td-text">
                           <small style="font-weight:900;color:#555">
                              {{date('j M Y',strtotime($holiday->date))}} ({{$holiday->reason}})
                           </small>
                        </td>
                  @endif
                        <td class="td-text">
                           <small style="font-weight:900;color:#555">
                           {{date('l',strtotime($holiday->date))}}
                           </small>
                        </td>
                     </tr>
                  @else
                  @if($holiday->type == "Others")
                     <tr class='optional' style="background:#FFDAB9 !important">
                           <td class="td-text">
                                 <small style="font-weight:900;color:#555">
                                    {{date('j M Y',strtotime($holiday->date))}} ({{$holiday->reason}}) &nbsp;*Optional*
                                 </small>
                              </td>
                  @elseif($holiday->type == "Weekend")
                     <tr class='success' style="background-color:#AFEEEE">
                           <td class="td-text">
                                 <small style="font-weight:900;color:#555">
                                    {{date('j M Y',strtotime($holiday->date))}} ({{$holiday->reason}}) &nbsp;*Weekend*
                                 </small>
                              </td>
                  @else 
                     <tr>
                           <td class="td-text">
                                 <small style="font-weight:900;color:#555">
                                    {{date('j M Y',strtotime($holiday->date))}} ({{$holiday->reason}})
                                 </small>
                              </td>
                  @endif
                     <td class="td-text">
                        <small>
                        {{date('l',strtotime($holiday->date))}}
                        </small>
                     </td>
                  </tr>
                                @endif
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="2">No results found.</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

