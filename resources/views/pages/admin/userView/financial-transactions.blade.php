
@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">{{$user->name}}'s Financial Transactions</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li><a href="{{ url('admin/users') }}">List of Users</a></li>
        		  	<li class=active>Financial Transactions</li>
        		</ol>
            </div>
		</div>
	</div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
	<!--download button-->
	<div class="row">
		<div class="col-md-12">
			@if(count($transactions) > 0)
			<div class="pull-right m-10">
				<a href="/admin/users/{{$user->id}}/financial-transactions/download" class="btn btn-success"><i class="fa fa-eye"></i> Download Financial Transactions</a>
			</div>
			@endif
		</div>
	</div>
	<!--user details-->
	<div class="row panel panel-default">
		<div class="col-sm-3 d-flex flex-column">
			<span>Employee ID: {{$user->employee_id?$user->employee_id:""}}</span>
			<span>Email: {{$user->email?$user->email:""}}</span>
			<span>Designation: {{$user->designation?$user->designation:""}}</span>
		</div>
		<div class="col-sm-3 d-flex flex-column">
			<span>Joining Date: {{$user->joining_date?$user->joining_date:""}}</span>
			<span>Confirmation Date: {{$user->confirmation_date?$user->confirmation_date:""}}</span>
			<span>Release Date: {{$user->release_date?$user->release_date:""}}</span>
		</div>
		<div class="col-sm-3 d-flex flex-column">
			<span>PAN: {{$user->pan?$user->pan:""}}</span>
			<span>PF No.: {{$user->pf_no?$user->pf_no:""}}</span>
			<span>UAN No.: {{$user->uan_no?$user->uan_no:""}}</span>
		</div>
	</div>
	<!--main panel-->
	<div class="user-list-view">
	<div class="panel panel-default ">
		<table class="table table-striped">
			<th>#</th>
			<th>Type</th>
			<th>Amount</th>
			<th>Transaction For</th>
			<th>Created At</th>
			@if(count($transactions) > 0)
                @foreach($transactions as $index => $transaction)
                <tr>
                    <td>{{$transaction->id}}</td>
                    <td>{{ucfirst($transaction->type)}}</td>
                    <td>{{$transaction->amount}}</td>
                    @if($transaction->reference_type=="App\Models\Admin\Bonus" && $transaction->reference && $transaction->reference->type)
                        <td>{{ucfirst($transaction->reference->type)}} Bonus on {{$transaction->reference->date}}</td>
	<!--renamed-->	@elseif($transaction->reference_type=="App\Models\Admin\Finance\ItSavingUserData" && $transaction->reference)
                        <td>TDS</td>
    <!--to this-->  @elseif($transaction->reference_type=="App\Models\Admin\Finance\SalaryUserDataTDS" && $transaction->reference && $transaction->reference->tds_for_the_month)
                        <td>TDS, where TDS for the month was {{$transaction->reference->tds_for_the_month}}</td>
    				@elseif($transaction->reference_type=="App\Models\Admin\Finance\PayroleGroupRuleMonth" && $transaction->reference && $transaction->reference->name && $transaction->reference->type)
                        <td>Monthly {{$transaction->reference->type}} for {{$transaction->reference->name}}</td>
    				@elseif($transaction->reference_type=="App\Models\Admin\Insurance\InsuranceDeduction" && $transaction->reference && $transaction->reference->insurance_policy_id)
                        <td>Insurance deduction for Policy {{$transaction->reference->insurance_policy_id}}</td>
    				@elseif($transaction->reference_type=="App\Models\Admin\VpfDeduction" && $transaction->reference && $transaction->reference->vpf_id)
                        <td>VPF deduction for VPF ID {{$transaction->reference->vpf_id}}</td>
    				@elseif($transaction->reference_type=="App\Models\Admin\AppraisalBonus" && $transaction->reference && $transaction->reference->type)
                        <td>{{$transaction->reference->type}} Appraisal Bonus</td>
                    @else
                        <td>{{$transaction->reference_type}} | {{$transaction->reference_id}}</td>
                    @endif
                    <td>{{datetime_in_view($transaction->created_at)}}</td>
                </tr>
				@endforeach
	    	@else
	    		<tr>
	    			<td colspan="6">No transactions found.</td>
	    		</tr>
	    	@endif
	   </table>
	</div>
	</div>
@endsection
