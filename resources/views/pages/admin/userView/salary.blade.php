@extends(Request::is('admin/view/*')?'pages.admin.userView.partials.usernav':'pages.admin.userView.partials.nav')
@section('sub-content')
<style>
    .table-striped > tbody > tr > td{
        line-height:1.6em;
    }
    .table-striped {
    border:none;
    border-collapse: collapse;
    }
    .table-striped td,.table-striped th {
        border-left: 2px solid #E1E4FA;
        border-right: 2px solid #E1E4FA;
    }

    .table-striped td:first-child,.table-striped th:first-child {
        border-left: none;
    }

    .table-striped td:last-child,.table-striped th:last-child {
        border-right: none;
    }
    .boldtext{
        font-weight:900;
    }
</style>
<div class="col-xs-12 col-sm-12 col-md-12">
    <div class="panel panel-default" style="margin-bottom:0;border-bottom:0;border-bottom-left-radius: 0;border-bottom-right-radius: 0">
		<table class="table table-striped">
            @if(!empty($data) && count($data) > 0)
                    <tr>
                @foreach($data as $index=>$value)
                @if($index!='type')
                    <th>{{ucfirst($index)}}</th>
                @else
                    <th></th>
                @endif
                    <?php $c=count($data[$index]);
                    $f=0; ?>
                @endforeach
                    </tr>
            @endif
            @for($i=0;$i<$c;$i++)
                <tr>
                @foreach($data as $index=>$value)
                @if($value[$i]=="" || $value[$i]=='-0')
                <td></td>
                @elseif($data['names'][$i]=='Gross Earning'||$data['names'][$i]=='Total Deduction'||$data['names'][$i]=='Net Salary')
                    <td class="boldtext">@if($index!='names'&&$index!='type')&#8377;@endif {{$value[$i]}}</td>
                    <?php $f++; ?>
                @else
                    <td style="color:#888">@if($index!='names'&&$index!='type')&#8377;@endif {{$value[$i]}}</td>
                @endif
                @endforeach
                </tr>
                @if($f==3)
                <!--<tr>
                    <td class="boldtext"> CTC</td>
                    <td class="boldtext">&#8377; {{($user->activeAppraisal->in_hand)/12}}</td>
                    <td class="boldtext">&#8377; {{$user->activeAppraisal->in_hand}}</td>
                    <td></td>
                </tr>-->
                <!--<tr>
                    <td class="boldtext"> Bonus</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>-->
                <!--<tr>
                    <td>Variable Performance Bonus</td>
                    <td></td>
                    <td style="color:#888">&#8377; {{$user->activeAppraisal->qtr_bonus}}</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Fixed Annual Bonus</td>
                    <td></td>
                    <td style="color:#888">&#8377; {{$user->activeAppraisal->year_end}}</td>
                    <td></td>
                </tr>
                <tr>
                    <td class="boldtext">Total Gross CTC</td>
                    <td class="boldtext">&#8377; {{($user->activeAppraisal->in_hand)/12}}</td>
                    <td class="boldtext">&#8377; {{($user->activeAppraisal->in_hand+$user->activeAppraisal->qtr_bonus+$user->activeAppraisal->year_end)}}</td>
                    <td></td>
                </tr>-->
                <?php $f++; ?>
                @endif
            @endfor
        </table>
    </div>
    <div class="panel panel-default" style="padding-top:1%;margin-top:0;border-top:0;border-top-left-radius: 0;border-top-right-radius: 0">
        <ul style="list-style-type:none;padding-left:0.7%">
            <li><b>Note: </b></li>
            <li style="color:#888">Provident Fund will be deducted as per Government Rules & Regulations.</li>
            <li style="color:#888">Professional Tax will be applicable as per Government Rules & Regulations.</li>
            <li style="color:#888">Income Tax (TDS) will be applicable as per Government Rules & Regulations.</li>
            <li style="color:#888">Health Insurance deduction will be done as per Company Policy.</li>
            <li style="color:#888">You will be eligible for Fixed Annual bonus only after you have completed 12 months from the date of confirmation. Notice period will not be part of these 12 months.</li>
        </ul>
    </div>
</div>
@endsection