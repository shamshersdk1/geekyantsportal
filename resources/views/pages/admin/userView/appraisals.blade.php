@extends('partials.admin-dashboard')
@section('main-content')
if(Request::is('admin/view/*'))
@include('pages.admin.userView.partials.usernav')
@else
@include('pages.admin.userView.partials.nav')
@endif
<style>
.panel.panel-horizontal {
    display:table;
    width:100%;
    margin-bottom:0px;
    border-style:solid;
    border-width:0px 0px 1px 0px;
}
.panel.panel-horizontal > .panel-heading, .panel.panel-horizontal > .panel-body{
    border-radius:0px;
    display:table-cell;
}
.panel.panel-horizontal > .panel-heading {
    width: 20%;
    background-color:#FFF;
    border-right: 1px solid #ddd;
    border-bottom:0;
}
</style>
<script>
		$(function(){
		$(".deleteform").submit(function(event){
                return confirm('Are you sure?');
            });
        });
</script>
@if(count($appraisals) > 0)
    @foreach($appraisals as $appraisal)
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="panel panel-default panel-horizontal">
            <div class="panel-heading"@if($appraisal->is_active==1)style="border-left: 10px solid #269900"@endif>
                <h3 class="panel-title">{{$appraisal->name}}</h3>
                <small style="color:#aaa">{{date("j F Y", strtotime($appraisal->effective_date))}}</small>
                <small style="color:#aaa">@if(!empty($appraisal->end_date)) to {{date("j F Y", strtotime($appraisal->end_date))}}@endif</small>
            </div>
            <div class="panel-body">
                <ul class="col-xs-10 col-sm-10 col-md-10" style="list-style-type: none;display:inline">
                    <li></li>
                    <li>In Hand: @if(empty($appraisal->in_hand)) - @else {{$appraisal->in_hand}} @endif</li>
                    <li>Quarterly Bonus: @if(empty($appraisal->qtr_bonus)) - @else {{$appraisal->qtr_bonus}} @endif</li>
                    <li>Annual Bonus: @if(empty($appraisal->year_end)) - @else {{$appraisal->year_end}} @endif</li>
                </ul>
                @if(Auth::user()->isAdmin())
                <div class="col-xs-2 col-sm-2 col-md-2" style="float:right;padding-right:0px;margin-top:2%">
                    <a href="{{Request::url()}}/{{$appraisal->id}}/edit" class="btn btn-info crud-btn btn-sm"><i class="fa fa-pencil btn-icon-space" aria-hidden="true"></i>Edit</a>
                    <form action="{{Request::url()}}/{{$appraisal->id}}" method="post" style="display:inline-block;" class="deleteform">
                        <input name="_method" type="hidden" value="DELETE">
                        <button class="btn btn-danger crud-btn btn-sm" type="submit"><i class="fa fa-trash btn-icon-space" aria-hidden="true"></i>Delete</button>
                    </form>
                </div>
                @endif
            </div>
        </div>
    </div>
    @endforeach
    @else
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="panel panel-default panel-horizontal">
            <div class="panel-body col-sm-12">
                <ul style="list-style-type: none;display:inline">
                    <h5>No results Found</h5>
                </ul>
            </div>
        </div>
    </div>
    @endif
    @if(Auth::user()->isAdmin())
    <div class="col-sm-4 text-right m-t-10" style="float:right">
        <a href="{{Request::url()}}/create" class="btn btn-success" style="border-radius:50%;height:3.5em;width:3.5em;margin-right:5%"><i class="fa fa-plus" style="font-size:2em;padding-top:15%"></i></a>
    </div>
    @endif
</div>
@endsection