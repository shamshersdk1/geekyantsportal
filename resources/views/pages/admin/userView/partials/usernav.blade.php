@extends('layouts.user-dashboard')
@section('main-content')
<style>
.panel.panel-horizontal {
    display:table;
    width:100%;
    margin-bottom:0px;
    border-style:solid;
    border-width:0px 0px 1px 0px;
}
.panel.panel-horizontal > .panel-heading, .panel.panel-horizontal > .panel-body{
    display:table-cell;
}
.panel.panel-horizontal > .panel-heading {
    /*width: 20%;*/
    background-color:#FFF;
    border-right: none;
    border-bottom: none;
}
.info-right-table td{
    padding:0px 15px 0px 15px !important;
}
.navbar.navbar-default{
    background-color:#FFF;
}
#options > li > a{
text-transform:none;
color:#888;
width:100%;
}
#options > li{
width:12%;
}
@media(max-width:768px){
    #options > li{
width:100%;
}
}
#options{
    width:100%;
}
#options > .active > a, #options > .active > a:hover, #options > li > a:hover{
text-decoration: none;
border-radius:0px;
border-bottom-width: 2px !important;
border-bottom-style: solid;
    border-bottom-color: #5470AC !important;
    color: #5470AC !important;
  background-color: transparent !important;

}
small{
    color:#aaa;
}
</style>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="panel panel-default panel-horizontal">
                <div class="panel-heading profile-image-container" style="@if (!empty($user->profile->image)) background: url({{$user->profile->image}}) @endif">
                </div>
                <div class="panel-body profile-details-container">
                    <ul style="list-style-type: none; padding: 0; margin: 0; display: inline-block;">
                        <li><h4 style="margin:0">{{$user->name}}</h4></li>
                        <li style="margin-bottom:2%"><small>{{empty($user->profile->title) ? "-" : $user->profile->title}}</small></li>
                        <li><small>{{$user->email}}</small></li>
                        <li><small>{{empty($user->profile->phone) ? "-" : $user->profile->phone}}</small></li>
                    </ul>
                    <table class="info-right-table" style="float:right">
                        <tr>
                            <td><small>Emp ID</small></td>
                            <td><small> : </small></td>
                            <td><small>{{empty($user->employee_id) ? "-" : $user->employee_id}}</small></td>
                        </tr>
                        <tr>
                            <td><small>DOJ</small></td>
                            <td><small> : </small></td>
                            <td><small>{{empty($user->joining_date) ? "-" : $user->joining_date}}</small></td>
                        </tr>
                        <tr>
                            <td><small>DOC</small></td>
                            <td><small> : </small></td>
                            <td><small>{{empty($user->confirmation_date) ? "-" : $user->confirmation_date}}</small></td>
                        </tr>
                        <tr>
                            <td colspan="3">
                            @if(!empty($user->profile->twitter))
                            <a href="{{$user->profile->twitter}}" style="padding-right:4%"><i class="fa fa-twitter-square fa-2x" style="color:#00aced" aria-hidden="true"></i></a>
                            @endif
                            @if(!empty($user->profile->facebook))
                            <a href="{{$user->profile->facebook}}" style="padding-right:4%"><i class="fa fa-facebook-square fa-2x" style="color:#3b5998" aria-hidden="true"></i></a>
                            @endif
                            @if(!empty($user->profile->google))
                            <a href="{{$user->profile->google}}" style="padding-right:4%"><i class="fa fa-google-plus-square fa-2x" style="color:#dd4b39" aria-hidden="true"></i></a>
                            @endif
                            @if(!empty($user->profile->linkedin))
                            <a href="{{$user->profile->linkedin}}" style="padding-right:4%"><i class="fa fa-linkedin-square fa-2x" style="color:#007bb6" aria-hidden="true"></i></a>
                            @endif
                            @if(!empty($user->profile->github))
                            <a href="{{$user->profile->github}}"><i class="fa fa-github-square fa-2x" style="color:#333" aria-hidden="true"></i></a>
                            @endif
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <ul class="nav navbar-nav profile-tabs-container" id="options">
                        <li @if(Request::is('admin/view/dashboard/*')||Request::is('admin/view/dashboard')) class="active" @endif><a href="/admin/view/dashboard"><small>Dashboard</small></a></li>
                        <li @if(Request::is('admin/view/profile/*')||Request::is('admin/view/profile')) class="active" @endif><a href="/admin/view/profile"><small>Profile</small></a></li>
                        <li @if(Request::is('admin/view/salary-breakdown/*')||Request::is('admin/view/salary-breakdown')) class="active" @endif><a href="/admin/view/salary-breakdown"><small>Salary Breakdown</small></a></li>
                        <li @if(Request::is('admin/view/payslips/*')||Request::is('admin/view/payslips')) class="active" @endif><a href="/admin/view/payslips"><small>Payslips</small></a></li>
                        <li @if(Request::is('admin/view/appraisals/*')||Request::is('admin/view/appraisals')) class="active" @endif><a href="/admin/view/appraisals"><small>Appraisals</small></a></li>
                        <li @if(Request::is('admin/view/leaves/*')||Request::is('admin/view/leaves')) class="active" @endif><a href="/admin/view/leaves"><small>Leaves</small></a></li>
                        <li @if(Request::is('admin/view/loans/*')||Request::is('admin/view/loans')) class="active" @endif><a href="/admin/view/loans"><small>Loans</small></a></li>
                        <li @if(Request::is('admin/view/bonuses/*')||Request::is('admin/view/bonuses')) class="active" @endif><a href="/admin/view/bonuses"><small>Bonus</small></a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</div>
<div class="col-md-12">
    @if(!empty($errors->all()))
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ $error }}</span><br/>
                @endforeach
        </div>
    @endif
    @if (session('message'))
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <span>{{ session('message') }}</span><br/>
        </div>
    @endif
</div>
@yield('sub-content')
@endsection
