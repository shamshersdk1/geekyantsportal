@extends('layouts.admin-dashboard')
@section('main-content')
<style>
.panel.panel-horizontal {
    display:table;
    width:100%;
    margin-bottom:0px;
    border-style:solid;
    border-width:0px 0px 1px 0px;
}
.panel.panel-horizontal > .panel-heading, .panel.panel-horizontal > .panel-body{
    display:table-cell;
}
.panel.panel-horizontal > .panel-heading {
    width: 20%;
    background-color:#FFF;
    border-right: 1px solid #ddd;
    border-bottom:0;
}
.info-right-table td{
    padding:0px 15px 0px 15px !important;
}
.navbar.navbar-default{
    background-color:#FFF;
}
#options > li > a{
text-transform:none;
color:#888;
width:100%;
}
#options > li{
width:13%;
}
@media(max-width:768px){
    #options > li{
width:100%;
}
}
#options{
    width:100%;
}
#options > .active > a, #options > .active > a:hover, #options > li > a:hover{
text-decoration: none;
border-radius:0px;
border-bottom-width: 2px !important;
border-bottom-style: solid;
border-bottom-color: #3b5998 !important;
    color: #007bb6 !important;
  background-color: transparent !important;
}
</style>
<div class="container-fluid">
    <div class="row m-t-15">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="panel panel-default panel-horizontal">
                <div class="panel-heading profile-image-container" style="@if (!empty($user->profile->image))background:url({{$user->profile->image}}); @endif background-size:100% 100%">
                </div>
                <div class="panel-body" style="padding-top:2%">
                    <ul class="col-xs-6 col-sm-6 col-md-6" style="list-style-type: none;display:inline;padding:0;margin:0">
                        <li><h4 style="margin:0">{{$user->name}}</h4></li>
                        <li style="margin-bottom:2%"><small style="color:#aaa">{{empty($user->profile->title) ? "-" : $user->profile->title}}</small></li>
                        <li><small style="color:#aaa">{{$user->email}}</small></li>
                        <li><small style="color:#aaa">{{empty($user->profile->phone) ? "-" : $user->profile->phone}}</small></li>
                        <li><small style="color:#aaa">RM : {{!empty($user->reportingManager->name) ? $user->reportingManager->name :'' }} </small></li>
                    </ul>
                    <table class="info-right-table" style="min-width:200px;float:right">
                        <tr>
                            <td><small style="color:#aaa">Emp ID</small></td>
                            <td><small style="color:#aaa"> : </small></td>
                            <td><small style="color:#aaa">{{empty($user->employee_id) ? "-" : $user->employee_id}}</small></td>
                        </tr>
                        <tr>
                            <td><small style="color:#aaa">DOJ</small></td>
                            <td><small style="color:#aaa"> : </small></td>
                            <td><small style="color:#aaa">{{empty($user->joining_date) ? "-" : $user->joining_date}}</small></td>
                        </tr>
                        <tr>
                            <td><small style="color:#aaa">DOC</small></td>
                            <td><small style="color:#aaa"> : </small></td>
                            <td><small style="color:#aaa">{{empty($user->confirmation_date) ? "-" : $user->confirmation_date}}</small></td>
                        </tr>
                        <tr>
                            <td colspan="3">
                            @if(!empty($user->profile->twitter))
                            <a href="{{$user->profile->twitter}}" style="padding-right:4%"><i class="fa fa-twitter-square fa-2x" style="color:#00aced" aria-hidden="true"></i></a>
                            @endif
                            @if(!empty($user->profile->facebook))
                            <a href="{{$user->profile->facebook}}" style="padding-right:4%"><i class="fa fa-facebook-square fa-2x" style="color:#3b5998" aria-hidden="true"></i></a>
                            @endif
                            @if(!empty($user->profile->google))
                            <a href="{{$user->profile->google}}" style="padding-right:4%"><i class="fa fa-google-plus-square fa-2x" style="color:#dd4b39" aria-hidden="true"></i></a>
                            @endif
                            @if(!empty($user->profile->linkedin))
                            <a href="{{$user->profile->linkedin}}" style="padding-right:4%"><i class="fa fa-linkedin-square fa-2x" style="color:#007bb6" aria-hidden="true"></i></a>
                            @endif
                            @if(!empty($user->profile->github))
                            <a href="{{$user->profile->github}}"><i class="fa fa-github-square fa-2x" style="color:#333" aria-hidden="true"></i></a>
                            @endif
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <ul class="nav navbar-nav col-xs-12 col-sm-12 col-md-12" id="options" style="margin-top:0">
                        <li @if(Request::is('admin/users/*/profile')||Request::is('admin/users/*/profile/*')) class="active" @endif><a href="/admin/users/{{$user->id}}/profile">Profile</a></li>
                        <li @if(Request::is('admin/users/*/salary-breakdown/*')||Request::is('admin/users/*/salary-breakdown')) class="active" @endif><a href="/admin/users/{{$user->id}}/salary-breakdown">Salary Breakdown</a></li>
                        <li @if(Request::is('admin/users/*/payslips/*')||Request::is('admin/users/*/payslips')) class="active" @endif><a href="/admin/users/{{$user->id}}/payslips">Payslips</a></li>
                        <li @if(Request::is('admin/users/*/appraisals/*')||Request::is('admin/users/*/appraisals')) class="active" @endif><a href="/admin/users/{{$user->id}}/appraisals">Appraisals</a></li>
                        <li @if(Request::is('admin/users/*/leaves/*')||Request::is('admin/users/*/leaves')) class="active" @endif><a href="/admin/users/{{$user->id}}/leaves">Leaves</a></li>
                        <li @if(Request::is('admin/users/*/loans/*')||Request::is('admin/users/*/loans')) class="active" @endif><a href="/admin/users/{{$user->id}}/loans">Loans</a></li>
                        <li @if(Request::is('admin/users/*/bonuses/*')||Request::is('admin/users/*/bonuses')) class="active" @endif><a href="/admin/users/{{$user->id}}/bonuses">Bonus</a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</div>
<div class="col-md-12">
    @if(!empty($errors->all()))
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ $error }}</span><br/>
                @endforeach
        </div>
    @endif
    @if (session('message'))
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <span>{{ session('message') }}</span><br/>
        </div>
    @endif
</div>
@yield('sub-content')
@endsection
