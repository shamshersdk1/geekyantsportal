@extends(Request::is('admin/view/*')?'pages.admin.userView.partials.usernav':'pages.admin.userView.partials.nav')
@section('sub-content')
<div class="col-xs-12 col-sm-12 col-md-12">
    @if(isset($user))
        <form role="form" method="post" action="/admin/users/{{$user->id}}/profile" id="floating-label" class="form-input" enctype="multipart/form-data">
            <div class="panel panel-default custom-space" style="margin-bottom:0;border-bottom-left-radius:0;border-bottom-right-radius:0">
                <div class="row">
                    {{ csrf_field() }}
                    {{method_field("PUT")}}   
                      <div class=col-sm-6>
                        <div class="row">
                          <div class="col-md-4">
                            <label>Title / Designation: </label>
                          </div>
                          <div class="col-md-8">
                            <div class="form-group">
                              <input type="text" name="title" class="form-control" placeholder="Designation" value="@if(!empty($user->profile)){{$user->profile->title}}@endif">
                            </div>
                          </div>
                          <div class="col-md-4">
                              <label>Google +: </label>
                          </div>
                          <div class="col-md-8">
                              <div class="form-group">
                                  <input type="text" name="google" class="form-control" placeholder="Google plus Link" value="@if(!empty($user->profile)){{$user->profile->google}}@endif">
                              </div>
                          </div>
                          <div class="col-md-4">
                              <label>Twitter : </label>
                          </div>
                          <div class="col-md-8">
                              <div class="form-group">
                                  <input type="text" name="twitter" class="form-control" placeholder="Twitter Link" value="@if(!empty($user->profile)){{$user->profile->twitter}}@endif">
                              </div>
                          </div>
                          <div class="col-md-4">
                              <label>Github: </label>
                          </div>
                          <div class="col-md-8">
                              <div class="form-group">
                                  <input type="text" name="github" class="form-control" placeholder="Github Link" value="@if(!empty($user->profile)){{$user->profile->github}}@endif">
                              </div>
                          </div> 

                        </div>
                      </div>    
                      <div class="col-sm-6">
                        <div class="row">
                            <div class="col-md-4">
                                <label>Image: </label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="file" name="image" class="form-control" value="@if(!empty($user->profile)){{$user->profile->image}}@endif">
                                </div>
                            </div>
                           
                            <div class="col-md-4">
                                <label>Facebook: </label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="text" name="facebook" class="form-control" placeholder="Facebook Link" value="@if(!empty($user->profile)){{$user->profile->facebook}}@endif">
                                </div>
                            </div> 
                             
                            <div class="col-md-4">
                                <label>LinkedIn: </label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="text" name="linkedin" class="form-control" placeholder="LinkedIn Link" value="@if(!empty($user->profile)){{$user->profile->linkedin}}@endif">
                                </div>
                            </div> 
                           
                            <div class="col-md-4">
                                <label>Stack Overflow: </label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="text" name="stack_overflow" class="form-control" placeholder="Stack Overflow Link" value="@if(!empty($user->profile)){{$user->profile->stack_overflow}}@endif">
                                </div>
                            </div>

                        </div>
                      </div>                 
                    
                            
                </div>
            </div>
            <div class="panel panel-default custom-space" style="border-top-left-radius:0;border-top-right-radius:0">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Profile Visibility : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="onoffswitch">
                                <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" <?php echo (!empty($user->profile)&&$user->profile->is_active==1)  == '1' ? 'checked':''; ?>>
                                <label class="onoffswitch-label" for="myonoffswitch">
                                    <span class="onoffswitch-inner"></span>
                                    <span class="onoffswitch-switch"></span>
                                 </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>Location: </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" name="location" class="form-control" placeholder="Location" value="@if(!empty($user->profile)){{$user->profile->location}}@endif">
                            </div>
                        </div> 
                        <div class="col-md-4">
                            <label>Keywords: </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" name="keywords" class="form-control" placeholder="Keywords" value="@if(!empty($user->profile)){{$user->profile->keywords}}@endif">
                            </div>
                        </div> 
                        <div class="col-md-4">
                            <label>Skype: </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" name="skype" class="form-control" placeholder="Skype User Id" value="@if(!empty($user->profile)){{$user->profile->skype}}@endif">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>Phone: </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" name="phone" class="form-control" placeholder="User phone number" value="@if(!empty($user->profile)){{$user->profile->phone}}@endif">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>Projects: </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <select class="form-control" id="" name="projects[]" placeholder="Projects" style="border-radius:0" multiple>
                                    @if(isset($projects) && count($projects) > 0)
                                        @foreach($projects as $project)
                                            @if( in_array($project->id, $selectedProjects) )
                                                <option  value="{{$project->id}}" selected>{{$project->project_title}}</option>
                                            @else
                                                <option  value="{{$project->id}}">{{$project->project_title}}</option>
                                            @endif
                                            
                                        @endforeach
                                    @endif
                                </select>                                
                            </div>
                        </div> 
                        

                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Slug: </label>
                            <label><small>(if left blank takes default)</small></label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" name="slug" class="form-control" value="@if(!empty($user->profile)){{$user->profile->profile_url}}@endif">
                            </div>
                        </div>
                        <div class="col-md-4" style="clear:left">
                            <label>Introduction: </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" name="intro" class="form-control" placeholder="Introduction" value="@if(!empty($user->profile)){{$user->profile->intro}}@endif">
                            </div>
                        </div> 
                        <div class="col-md-4">
                            <label>Amazing: </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" name="amazing" class="form-control" placeholder="things you have created..." value="@if(!empty($user->profile)){{$user->profile->amazing}}@endif">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>Email: </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" name="email" class="form-control" placeholder="User's Email" value="@if(!empty($user->profile)){{$user->profile->email}}@endif">
                            </div>
                        </div> 
                        <div class="col-md-4">
                            <label>Achievements: </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" name="achievements" class="form-control" placeholder="User's Achievements" value='@if(!empty($user->profile)){{$user->profile->achievements}}@endif'>
                            </div>
                        </div> 
                        <div class="col-md-4">
                            <label>Knowledge: </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" name="knowledge" class="form-control" placeholder="Knowledge" value="@if(!empty($user->profile)){{$user->profile->knowledge}}@endif">
                            </div>
                        </div>
                        <!--<div class="col-md-4">
                            <label>Expertise: </label>
                        </div>-->
                         <!--<div class="col-md-5">
                            <div class="form-group">
                                <input type="text" name="expertise" class="form-control" placeholder="Description" value="">
                            </div>
                        </div> 
                        <div class="input-group col-md-3" style="padding-right:15px">
                            <input type="text" class="form-control col-md-2" placeholder="%" style="border-radius:0">
                            <div class="input-group-btn">
                                <a onclick="addExperience(this)" style="margin-left:10px;height:2.8em" class="btn btn-default"><i class="fa fa-plus text-primary"></i></a>
                            </div>
                        </div>-->
                     

                    </div>
                  </div>
                  
                  
                        @if(!empty($expertise))
                        @foreach($expertise as $item)
                        <!-- <div class="row">
                            <div class="col-md-4">
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="experience_json[][description]" placeholder="Description" value="{{$item['skill']}}">
                                </div>
                            </div> 
                            <div class="input-group col-md-3" style="padding-right:15px">
                                <input type="text" class="form-control col-md-2" name="experience_json[][percentage]" placeholder="%" style="border-radius:0" value="{{$item['percentage']}}">
                                <div class="input-group-btn">
                                    <a onclick="removeExperience(this)" style="margin-left:10px;height:2.8em" id="{{$item['skill']}}" class="btn btn-default"><i class="fa fa-remove text-danger"></i></a>
                                </div>
                            </div>
                        </div> -->
                        @endforeach
                        @endif
                         <!--<div id="expertise">
                                
                        </div> -->
                    </div>
                </div>
            </div>
            <div class="text-center">

                <button type="submit" class="btn btn-success">Update</button>
            </div>
        </form>
    @else
        <label>Profile table is not linked</label>
    @endif
</div>
<script>
    // $(function(){
    //     var count=0;
    //     addExperience = function () {
    //         count++;
    //         var id='newSkill'+count;
    //         var html='<div class="row"><div class="col-md-4"></div><div class="col-md-5"><div class="form-group"><input type="text" class="form-control" name="experience_json[][description]" placeholder="Description"></div></div> <div class="input-group col-md-3" style="padding-right:15px"><input type="text" class="form-control col-md-2" name="experience_json[][percentage]" placeholder="%" style="border-radius:0"><div class="input-group-btn"><a onclick="removeExperience(this)" style="margin-left:10px;height:2.8em" id="'+id+'" class="btn btn-default"><i class="fa fa-remove text-danger"></i></a></div></div></div>'
            
    //         $( "#expertise" ).append(html);
		// }
    //     removeExperience = function(element) {
    //         $("[id='"+element.id+"']").parent().parent().parent().remove();
    //     }
    // });
</script>
@endsection
        