@extends('layouts.admin-dashboard')
@section('main-content')
<section class="new-project-section">
   <div class="container-fluid">
      <div class="breadcrumb-wrap">
         <div class="row">
            <div class="col-sm-8">
               <h1 class="admin-page-title">Questions</h1>
               <ol class="breadcrumb">
                  <li><a href="/admin">Admin</a></li>
                  <li><a href="{{ url('admin/question') }}">Question</a></li>
                  <li class="active">{{ $questionObj->id }}</li>
               </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
               <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
         </div>
      </div>
      @if(!empty($errors->all()))
      <div class="alert alert-danger">
         @foreach ($errors->all() as $error)
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
         <span>{{ $error }}</span><br/>
         @endforeach
      </div>
      @endif
      @if (session('message'))
      <div class="alert alert-success">
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
         <span>{{ session('message') }}</span><br/>
      </div>
      @endif
      <form class="form-horizontal" method="post" action="/admin/question/{{$questionObj->id}}" enctype="multipart/form-data">
         <input name="_method" type="hidden" value="PUT" />
         <div class="panel panel-default">
            <div class="panel-body">
               <div class="row">
                  <div class="col-md-12">
                     <div class="row">
                        <div class="col-sm-6">
                           <div class="form-group">
                              <label for="" class="col-sm-2 control-label">Question</label>
                              <div class="col-sm-6">
                                 <input type="text" class="form-control" id="question" name="question" value="{{ $questionObj->question }}">
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group">
                              <label class="col-sm-2 control-label">Status:</label>
                              @if( $questionObj->status )
                              <div class="col-sm-4">
                                 <select class="form-control" name="status">
                                    <option value="1" selected>Active</option>
                                    <option value="0">Inactive</option>
                                 </select>
                              </div>
                              @else
                              <div class="col-sm-4">
                                 <select class="form-control" name="status">
                                    <option value="1">Active</option>
                                    <option value="0" selected>Inactive</option>
                                 </select>
                              </div>
                              @endif
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group">
                              <label class="col-sm-2 control-label">Designations:</label>
                              <div class="col-sm-8">
                                 <div class="input-group col-md-10">
                                    <select class="js-example-basic-multiple22 form-control" multiple="multiple" name="designations[]">
                                       @if(isset($designations))
                                       @foreach($designations as $designation)
                                       <?php
                                          $saved = false;
                                          foreach($questionObj->feedbackDesignations as $questionDesignation){
                                          if($questionDesignation->designation_id == $designation->id){
                                          	$saved = true;
                                          }
                                          }
                                          ?>
                                       @if(isset($questionObj->feedbackDesignations))
                                       @if($saved)
                                       <option value="{{$designation->id}}" selected >{{$designation->designation}}</option>
                                       @else
                                       <option value="{{$designation->id}}">{{$designation->designation}}</option>
                                       @endif
                                       @else
                                       <option value="{{$designation->id}}">{{$designation->designation}}</option>
                                       @endif
                                       @endforeach
                                       @endif
                                    </select>
                                 </div>
                              </div>
                           </div>
                        </div>
                        @if ( count($roles) > 0 )
                        <div class="col-sm-6">
                           <div class="form-group">
                              <label class="col-sm-2 control-label">Role: </label>
                              <div class="col-sm-4">
                                 <select id="selectid3" name="role" style="width=35%;" placeholder= "Select an option">
                                    <option></option>
                                    @foreach($roles as $role)
                                    @if( $role->id == $questionObj->role_type )
                                    <option value="{{$role->id}}" selected>{{$role->name}}</option>
                                    @else
                                    <option value="{{$role->id}}">{{$role->name}}</option>
                                    @endif
                                    @endforeach
                                 </select>
                              </div>
                           </div>
                        </div>
                        @endif
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="text-center">
            <button type="submit" class="btn btn-success">Update Question</button>
         </div>
      </form>
   </div>
</section>
@endsection