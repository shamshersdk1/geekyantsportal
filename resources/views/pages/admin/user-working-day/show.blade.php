@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-6">
                <h1 class="admin-page-title">User Working Day </h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li><a href="{{ url('admin/user-working-day') }}">User Working Day</a></li>
        		  	<li>{{$month->formatMonth()}}</li>
        		</ol>
            </div>
            <div class="col-sm-6">

                @if(!$month->workingDaySetting || ($month->workingDaySetting &&  $month->workingDaySetting->value == 'open'))
                    <div class="pull-right m-t-10">
                        <span>
                            <a href="/admin/user-working-day/{{$month->id}}/regenerate" class="btn btn-warning"><i class="fa fa-gear fa-fw"></i>Regenerate</a>
                        </span>
                    </div>
                    <div class="pull-right m-t-10">
                        <span>
                            <a href="/admin/user-working-day/{{$month->id}}/status-month" class="btn btn-danger"><i class="fa fa-lock fa-fw"></i>Lock</a>
                        </span>
                    </div>
                @elseif(($month->getMonth(date("Y/m/d")) == $month->id ) && $month->workingDaySetting && $month->workingDaySetting->value == 'locked')
                    <div class="pull-right m-t-10">
                        <span>
                            <a href="/admin/user-working-day/{{$month->id}}/status-month" class="btn btn-success"><i class="fa fa-unlock fa-fw"></i>UnLock</a>
                        </span>
                    </div>
                @endif
			</div>
            @if($month && $month->workingDaySetting &&  $month->workingDaySetting->value == 'locked')
            <div class="pull pull-right">
                <span class="label label-primary">Locked at {{ $month->workingDaySetting ? datetime_in_view($month->workingDaySetting->created_at) : ''}}</span>
            </div>
        @endif
		</div>
    </div>
    
	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
    </div>
        <form method="post" action="{{$month->id}}">
            {{ csrf_field() }}
    {{ method_field('PATCH') }}
        @if(!$month->workingDaySetting ||  $month->workingDaySetting->value == 'open')
                <div class="row">
                    <div class="pull-right m-t-1 ">
                        <button type="submit" class="btn btn-success ">Save</button>
                    </div>
                </div>
        @endif
        <div class="user-list-view">
            <div class="panel panel-default">
                <table class="table table-striped" id="deduction-table">
                    <thead>
                        <th class="text-center">#</th>
                        <th class="text-center">Employee Id</th>
                        <th class="text-center">Name</th>
                        <th class="text-center">Working Days</th>
                        <th class="text-center">Loss Of Pay</th>
                        <th class="text-center">Comment</th>
                        <th class="text-center">Type</th>
                    </thead>
                    @if(count($workingDayObjs) > 0)
                        @foreach($workingDayObjs as $workingDayObj)
                            <tr>
                                <td class="text-center"></td>
                                <td class="text-left">{{ $workingDayObj->user->employee_id}}</td>
                                <td class="text-left">{{ $workingDayObj->user->name}}</td>
                                
                                <td class="text-center">
                                    @if($month->workingDaySetting && $month->workingDaySetting->value == 'locked')
                                        {{$workingDayObj->working_days}}
                                    @else
                                        <input name="working_days[{{$workingDayObj->id}}]" type="number" value="{{$workingDayObj->working_days}}"/>
                                    @endif
                                </td>
                                <td class="text-center">
                                    @if($month->workingDaySetting && $month->workingDaySetting->value == 'locked')
                                        {{$workingDayObj->lop}}
                                    @else
                                        <input name="lops[{{$workingDayObj->id}}]" type="number" value="{{$workingDayObj->lop}}"/>
                                    @endif
                                </td>
                                <td class="text-center">{{ $workingDayObj->comment}}</td>
                                <td class="text-center">{{ $workingDayObj->type}}</td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="8" class="text-center">
                                No Records found
                            </td>
                        </tr>

                    @endif
                </table>
            </div>
        </div>
    </form>
</div>
<script>
    $(document).ready(function() {
        var t = $('#deduction-table').DataTable( {
            pageLength:500, 
            scrollY:        true,
            scrollX:        true,
            scrollCollapse: true,
            paging:         false,
            "columnDefs": [ {
                "searchable": false,
                "orderable": false,
                "targets": 0
            } ],
            "order": [[ 1, 'asc' ]],
            fixedHeader: true
        } );
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    });
</script>

@endsection
