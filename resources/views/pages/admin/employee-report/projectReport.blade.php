@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="row">
        <ol class="breadcrumb">
            <h2 class="breadcrumb-head-page">Employee Reports</h2>
            <li><a href="/admin">Admin</a></li>
            <li><a href="{{ url('admin/employee-report') }}">TimeLog Reports</a></li>
        </ol>
        <div class="col-md-12">
            <div class="panel panel-default panel-white">
                
                    <table class="display table table-striped" cellpadding="10" style="width: 100%; background: #fff;">
                        <thead>
                            <tr>
                                <th class="sorting_asc" style="width:20%; font-size: 16px;">Name of Employee </th>
                                <th rowspan="2" class="sorting text-center" style="width:10%; font-size: 16px;">message</th>
                                <th rowspan="2" class="sorting text-center" style="width:10%; font-size: 16px;">time</th>
                                <!-- <th class="sorting text-center" style="width:10%; font-size: 16px;">Week 2</th>
                                <th class="sorting text-center" style="width:10%; font-size: 16px;">Week 3</th>
                                <th class="sorting text-center" style="width:10%; font-size: 16px;">Week 4</th>
                                <th class="sorting text-center" style="width:10%; font-size: 16px;">Week 5</th>
                                <th class="sorting text-center" style="width:10%; font-size: 16px;">Week 6</th>
                                <th class="sorting text-center" style="width:10%; font-size: 16px;">Week 7</th>
                                <th class="sorting text-center" style="width:10%; font-size: 16px;">Week 8</th> -->
                            </tr>
                        </thead>
                        @foreach($sprints as $show)
                        <tr>
                            <td>{{ $show['userName'] }}</td>
                            <td>
                                
                                <span><b>{{ $show['message'] }}</b> </span><br/>
                                   
                            </td>
                             <td>
                                
                                <span><b>{{$show['time']}}</b> </span><br/>
                                   
                            </td>
                            <!-- <td><span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/></td>
                            <td><span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/></td>
                            <td><span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/></td>
                            <td><span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/></td>
                            <td><span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/></td>
                            <td><span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/></td>
                            <td><span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/></td> -->
                        </tr> 
                        @endforeach
                        
                        <!-- <tr>                            <td>Abhishek</td>
                            <td>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                            </td>
                            <td><span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/></td>
                            <td><span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/></td>
                            <td><span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/></td>
                            <td><span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/></td>
                            <td><span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/></td>
                            <td><span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/></td>
                            <td><span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/></td>
                        </tr> 
                        <tr>                            <td>Abhishek</td>
                            <td>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                            </td>
                            <td><span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/></td>
                            <td><span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/></td>
                            <td><span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/></td>
                            <td><span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/></td>
                            <td><span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/></td>
                            <td><span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/></td>
                            <td><span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/>
                                <span><b>project name:</b> 40hr</span><br/></td>
                        </tr>  -->
                    </table>
            </div>
        </div>
    </div>
</div>
@endsection