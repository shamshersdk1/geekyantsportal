@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="admin-page-title">Employee Reports</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li class="active">Employee Report</li>
                </ol>
            </div>
            <!--<div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>-->
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <!-- ================= filter ============== -->
            <!--<div class="row">
                <div class="col-md-12 text-right">
                    <form class="form-inline form-filter">
                        <div class="form-group">
                            <label for="exampleInputName2">From Date</label>
                            <input type="text" class="form-control" id="exampleInputName2">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail2">To Date</label>
                            <input type="email" class="form-control" id="exampleInputEmail2">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-default">Search</button>
                        </div>
                    </form>
                </div>
            </div>-->
            <div class="panel panel-default">
                <table class="table table-striped">
                        
                            

                                <thead>
                                    @if(isset($start_date))
                                        <tr>
                                            <th>Name of Employee </th>
                                            <th width="12%">Previous Week <br>
                                                <?php $date = strtotime($start_date); $end_date = strtotime("+6 day", $date);  $week_end = date('M d, Y', $end_date); $week_start = date('M d, Y', $date); ?>{{$week_start}} <br> {{$week_end}}</th>


                                                <?php $current_start_date = strtotime("+1 day", $end_date); $current_week_start =  date('M d, Y', $current_start_date);?>
                                            <th width="12%">Current Week <br><?php  $current_end_date = strtotime("+6 day", $current_start_date);  $current_week_end = date('M d, Y', $current_end_date);   ?> {{$current_week_start}}  <br>{{$current_week_end}}</th>


                                                <?php $week1_start_date = strtotime("+1 day", $current_end_date); $week1_start =  date('M d, Y', $week1_start_date);?>
                                            <th width="12%">Week 1 <br><?php  $week1_end_date = strtotime("+6 day", $week1_start_date);  $week1_end = date('M d, Y', $week1_end_date);   ?>{{$week1_start}} <br>{{$week1_end}}</th>



                                            <?php $week2_start_date = strtotime("+1 day", $week1_end_date); $week2_start =  date('M d, Y', $week2_start_date);?>
                                            <th width="12%">Week 2 <br><?php  $week2_end_date = strtotime("+6 day", $week2_start_date);  $week2_end = date('M d, Y', $week2_end_date);   ?>{{$week2_start}}  <br>{{$week2_end}}</th>



                                            <?php $week3_start_date = strtotime("+1 day", $week2_end_date); $week3_start =  date('M d, Y', $week3_start_date);?>
                                            <th width="12%">Week 3 <br><?php  $week3_end_date = strtotime("+6 day", $week3_start_date);  $week3_end = date('M d, Y', $week3_end_date);   ?>{{$week3_start}}  <br>{{$week3_end}}</th>


                                            <?php $week4_start_date = strtotime("+1 day", $week3_end_date); $week4_start =  date('M d, Y', $week4_start_date);?>
                                            <th width="12%">Week 4 <br><?php  $week4_end_date = strtotime("+6 day", $week4_start_date);  $week4_end = date('M d, Y', $week4_end_date);   ?>{{$week4_start}}  <br>{{$week4_end}}</th>


                                            <?php $week5_start_date = strtotime("+1 day", $week4_end_date); $week5_start =  date('M d, Y', $week5_start_date);?>
                                            <th width="12%">Week 5 <br><?php  $week5_end_date = strtotime("+6 day", $week5_start_date);  $week5_end = date('M d, Y', $week5_end_date);   ?>{{$week5_start}}  <br>{{$week5_end}}</th>
                                           
                                        </tr>
                                    @endif
                                </thead>

                        @if(count($report)>0)
                            @foreach($report as $name =>$week)
                                @if(count($week)>0)
                                    <tr>                            
                                        <td> {{ $name }} </td>

                                        @foreach($week as $date =>$details)
                                            
                                                <td>
                                                    <?php
                                                        $hours = 0;
                                                        $consumed = 0;
                                                        $leave = 0;
                                                    ?> 

                                                    @if($details['allocated'])   
                                                       
                                                        @foreach($details['allocated'] as $allocated)
                                                            <?php
                                                            $hours = $hours + $allocated->hours;
                                                            // if(isset($allocate->minutes))
                                                            // $minutes = $minutes + $allocate->minutes;
                                                            ?>
                                                          
                                                           
                                                        @endforeach
                                                    @endif
                                                    @if($details['consumed']) 
                                                        <!-- consumed = {{ $details['consumed']}} -->
                                                        <?php
                                                            if(isset($details['consumed'])){
                                                                $consumed =  $details['consumed'];
                                                            }
                                                            
                                                        ?>
                                                        
                                                    @endif

                                                    @if($details['leave']) 
                                                        <!--  leave = {{ $details['leave']}}<br> -->
                                                        <?php
                                                            if(isset($details['leave'])){
                                                                $leave =  $details['leave'];
                                                            }
                                                            
                                                        ?>
                                                    @endif

                                                    @if($details['holiday']) 
                                                        <!-- holiday = {{ $details['holiday']}}<br> -->
                                                        <?php
                                                            if(isset($details['holiday'])){
                                                                $leave = $leave + $details['holiday'];
                                                            }
                                                            
                                                        ?>
                                                    @endif

                                                    <?php
                                                    $allocatedPercentage = ceil($hours/40 * 100);
                                                    
                                                    $consumePercentage = ceil($consumed/40 * 100);

                                                    $leavePercentage = ceil($leave/5 * 100 );
                                                ?>
                                                <p style="font-size: 13px; margin: 0px;"></p>
                                                <label>Allocation</label>
                                                @if($allocatedPercentage <= 100)
                                                    <div class="progress">
                                                        
                                                        <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: {{$allocatedPercentage}}%" >
                                                            {{$allocatedPercentage}} %
                                                            <!--<span class="sr-only">20% Complete (warning)</span>-->
                                                            
                                                        </div>
                                                        <div class="progress-bar progress-bar-success" style="width: {{100-$allocatedPercentage}}%">
                                                            {{100-$allocatedPercentage}} %
                                                            <!--<span class="sr-only">35% Complete (success)</span>-->
                                                        </div>
                                                    </div>
                                                <!-- ==================== red =================== -->
                                                @else
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                                            {{$allocatedPercentage}} %
                                                            <!--<span class="sr-only">10% Complete (danger)</span>-->
                                                            
                                                        </div>

                                                    </div>
                                                @endif
                                                <label>Consume Hours</label>
                                                @if($consumePercentage <= 100)
                                                    

                                                    <div class="progress">
                                                        
                                                        <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: {{$consumePercentage}}%" >
                                                            {{$consumePercentage}} %
                                                            <!--<span class="sr-only">20% Complete (warning)</span>-->
                                                            
                                                        </div>
                                                        <div class="progress-bar progress-bar-success" style="width: {{100-$consumePercentage}}%">
                                                            {{100-$consumePercentage}} %
                                                            <!--<span class="sr-only">35% Complete (success)</span>-->
                                                        </div>
                                                    </div>
                                                <!-- ==================== red =================== -->
                                                @else
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                                            {{$consumePercentage}} %
                                                            <!--<span class="sr-only">10% Complete (danger)</span>-->
                                                            
                                                        </div>

                                                    </div>
                                                @endif
                                                <label>Total Avaliable Hours</label>
                                                @if($leavePercentage <= 100)
                                                    <div class="progress">
                                                        
                                                        <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: {{$leavePercentage}}%" >
                                                            {{$leavePercentage}} %
                                                            <!--<span class="sr-only">20% Complete (warning)</span>-->
                                                            
                                                        </div>
                                                        <div class="progress-bar progress-bar-success" style="width: {{100-$leavePercentage}}%">
                                                            {{100-$leavePercentage}} %
                                                            <!--<span class="sr-only">35% Complete (success)</span>-->
                                                        </div>
                                                    </div>
                                                <!-- ==================== red =================== -->
                                                @else
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                                            {{$leavePercentage}} %
                                                            <!--<span class="sr-only">10% Complete (danger)</span>-->
                                                            
                                                        </div>

                                                    </div>
                                                @endif

                                                 
                                            </td>
                                        @endforeach
                                    </tr> 
                                @endif
                            @endforeach
                        @endif
                    </table>
            </div>
        </div>
    </div>
</div>
@endsection