@extends('layouts.admin-dashboard')
@section('main-content')
<section class="new-project-section">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">All Network Devices</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
			  			<li><a href="{{ url('admin/all-network-devices') }}">All Network Devices</a></li>
			  			<li class="active">Add</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>
	    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif
		<form class="form-horizontal" method="post" action="/admin/all-network-devices" enctype="multipart/form-data">
			<div class="panel panel-default">
				<div class="panel-body">
				    <div class="row">
				    	<div class="col-md-12">
						  	<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">User</label>
						    	<div class="col-sm-5">
						    		@if(isset($userList))
										<select id="selectid2" name="user_id" style="width=35%;" placeholder= "Select name" required>
											<option value=""></option>
											@foreach($userList as $user)
									        	<option value="{{$user->id}}" @if (old('user_id') == $user->id ) selected="selected" @endif>{{$user->name}}</option>
										    @endforeach
										</select>
									@endif
						    	</div>
						  	</div>
                            <div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Device Name</label>
						    	<div class="col-sm-5">
                                    <input id="name" class="form-control"  name="name" value="{{ old('name') }}" required/>
						    	</div>
						  	</div>
                            <div class="form-group">
						    	<label for="" class="col-sm-2 control-label">MAC Address</label>
						    	<div class="col-sm-5">
                                    <input id="mac_address" class="form-control"  name="mac_address" value="{{ old('mac_address') }}" 
										pattern="^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$" 
										oninvalid="this.setCustomValidity('Enter a Valid MAC address')"required/>
						    	</div>
						  	</div>
						</div>
					</div>
				</div>
			</div>
	</div>
	<div class="text-center">
		<button type="reset" class="btn btn-default">Clear</button>
		<button type="submit" class="btn btn-success">Save</button>
	</div>
	</form>
</section>
@endsection