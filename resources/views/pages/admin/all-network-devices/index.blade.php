@extends('layouts.admin-dashboard')
@section('main-content')
<section class="all-network-devices">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">All Network Devices</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
			  			<li class="active">All Network Devices</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
               	 <a href="/admin/all-network-devices/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i> Add Device</a>
            	</div>
	        </div>
	    </div>
	    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif
		<div class="user-list-view">
        <div class="panel panel-default"> 
            <table class="table table-striped">
                <thead>
                    <th>#</th>
                    <th>Name</th>
					<th>Device Name</th>
					<th>Mac Address</th>
					<th>IP Address</th>	
					<th>Added By</th>	
					<th>Created At</th>	
                    <th>Actions</th>
                </thead>
                @if(count($AllNetworkDevices) > 0)
                    @foreach($AllNetworkDevices as $index => $device)
                        <tr>
                            <td class="td-text">{{$device->id}}</td>
                             <td class="td-text">{{$device->user->name}}</td>
                             <td class="td-text">{{$device->name}}</td>
    						 <td class="td-text">{{$device->mac_address}}</td>
                             <td class="td-text">
								@if($device->ip)
									{{$device->ip->userIp->ip_address}}
								@endif
							 </td>
							 <td class="td-text">
								@if($device->addedBy)
									{{$device->addedBy->name}}
								@endif
							 </td>
							 <td class="td-text">{{datetime_in_view($device->created_at)}}</td>
                            <td class="td-text">
                                 <form action="/admin/all-network-devices/{{$device->id}}/edit" method="put" style="display:inline-block;" >
                                    <button class="btn btn-primary  btn-sm" type="submit"><i class="fa fa-edit btn-icon-space" aria-hidden="true"></i>Edit</button>
                                </form>
                                <form action="/admin/all-network-devices/{{$device->id}}" method="post" style="display:inline-block;" >
                                {{ method_field('DELETE') }}
                                <button class="btn btn-danger  btn-sm" type="submit"><i class="fa fa-trash btn-icon-space" aria-hidden="true"></i>Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="8">No results found.</td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
	</div>
</section>
@endsection 
