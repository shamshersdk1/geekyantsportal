@extends('layouts.admin-dashboard')
@section('main-content')
<script type="text/javascript">
  window.incident = <?php echo json_encode($incident); ?>;
  window.read_only = <?php echo json_encode($read_only); ?>;
</script>
<section class="new-project-section">
   <div class="container-fluid" ng-app="myApp" ng-controller="incidentShowCtrl">
      <div class="breadcrumb-wrap">
         <div class="flex-class justify-content-between align-items-center">
            <div>
               <h1 class="admin-page-title">Support Ticketss</h1>
               <ol class="breadcrumb">
                  <li><a href="/admin">Admin</a></li>
                  <li><a href="{{ url('admin/incident') }}">Support Tickets</a></li>
                  <li class="active">{{$incident->id}}</li>
               </ol>
            </div>
            <div>
               <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
         </div>
      </div>
      <div ng-if="!loaded" class="loader"></div>
      <div class="row" ng-if="loaded">
         <div class="col-sm-9">
            <div class="row">
               <div class="col-sm-12"  style="margin-bottom:15px">
                  <div ng-if="!readOnly">
                     <button ng-if="incidentObj.status == 'open' || incidentObj.status == 'in_progress' || incidentObj.status == 'reopen' " class="btn btn-primary" ng-click="transferIncident()" >Transfer</button>
                     <button ng-if="incidentObj.status == 'open' || incidentObj.status == 'in_progress' || incidentObj.status == 'reopen' " class="btn btn-success" ng-click="shareIncident()">Share With</button>
                     <div class="pull-right">
                        <button ng-if="incidentObj.status == 'open' || incidentObj.status == 'in_progress' || incidentObj.status == 'reopen' " class="btn btn-warning" ng-click="closeIncident()">Mark as Close</button>
                        @if ( $user->hasRole('admin') || $user->id == $incident->raised_by )
                         <button ng-if="incidentObj.status == 'closed'" class="btn btn-info" ng-click="reopenIncident()">Reopen</button>
                        @endif
                        @if( $user->hasRole('admin') )
                         <button ng-if="incidentObj.status == 'closed'"  class="btn btn-danger" ng-click="verifiedclosedIncident()">Verified Close</button>
                        @endif
                     </div>
                  </div>
               </div>
            </div>
            <div class="incident-wrap clearfix">

               <div class="row row-sm">
                  <input type="hidden" name="_token" value="9e0EVWte5bQFFg5xYDPqa5cDIxqynpmswqRRgJUv">
                  <div class="col-sm-6">
                     <div class="panel panel-default m-b-5">
                        <div class="info-table">
                           <label>Department: </label> <span> %%incidentObj.department.type%%</span>
                        </div>
                        <div class="info-table">
                           <label>Assigned To: </label>
                           <span>%%incidentObj.assignee.name%%</span>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="panel panel-default m-b-5">
                        <div class="info-table">
                           <label>Status: </label>
                           <span style="display: inline-block; text-transform: capitalize;"
                              ng-if=" incidentObj.status == 'open' || incidentObj.status == 'reopen' || incidentObj.status == 'in_progress' "
                              class="label label-primary custom-label"
                              >%%incidentObj.status%%
                           </span>
                           <span style="display: inline-block; text-transform: capitalize;"
                              ng-if=" incidentObj.status == 'closed' || incidentObj.status == 'verified_closed' "
                              class="label label-danger custom-label"
                              >%%incidentObj.status%%
                           </span>
                        </div>
                        <div class="info-table">
                           <label>Shared Users: </label>
                           <span><small>%%incidentObj.shared_with %%</small></span>
                        </div>

                     </div>
                  </div>
               </div>
               <div class="row row-sm">
                  <div class="col-sm-6">
                     <div class="panel panel-default">
                        <div class="info-table">
                           <label>Created By: </label>
                           <span>%%incidentObj.raised_by.name%%</span>
                        </div>
                        <div class="info-table">
                           <label>Created At: </label>
                           <span>
                              <small>%%incidentObj.created_at | dateToISO %%</small>
                           </span>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="panel panel-default">
                        <div class="info-table">
                           <label>Last Updated By: </label>
                           <span>
                              <span>%%incidentObj.raised_by.name%%</span>
                           </span>
                        </div>

                        <div class="info-table">
                           <label>Last Updated At: </label>
                           <span>
                               <small>%%incidentObj.updated_at | dateToISO %%</small>
                           </span>
                        </div>

                     </div>
                  </div>
               </div>
            </div>
            <div class=" row incident-msg ">
               <div class="col-sm-12">
                  <div class="panel panel-default">
                     <div class="panel-body ">
                        <h4 class="no-margin">%%incidentObj.subject%%</h4>
                        <div class="m-t-10" style="white-space: pre-line">%%incidentObj.description%%</div>
                     </div>
                  </div>
                  <div class="incident-comment">
                     <h4 class="admin-section-heading">Comments</h4>
                     <comment-data
                        commentable-id={{$incident->id}}
                        commentable-type="App\Models\Admin\Incident"
                        is-disabled="%%(incidentObj.status == 'closed' || incidentObj.status == 'verified_closed' ) ? true : false %%"
                        is-privatable="false"
                        >
                     </comment-data>
                  </div>
               </div>
            </div>
         </div>
         <activity-log reference-type="App\Models\Admin\Incident" reference-id="incidentObj.id"></activity-log>
         @if(!empty($errors->all()))
         <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <span>{{ $error }}</span><br/>
            @endforeach
         </div>
      </div>
      @endif
      @if (session('message'))
      <div class="alert alert-success">
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
         <span>{{ session('message') }}</span><br/>
      </div>
      @endif
   </div>
</section>
<div id="transfer-modal" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Transfer to:</h4>
         </div>
         <div class="modal-body">
            <div class="row">
               <div class="col-md-12 m-t-15">
                  <div class="form-group">
                     <label for="" class="col-sm-3 control-label">Department: </label>
                     <div class="col-sm-6">
                        <select class="department" name="department" >
                           <option value="%%dates[0]%%">Finance</option>
                           <option value="%%dates[1]%%">Office Admin</option>
                           <option value="%%dates[2]%%">Human Resources</option>
                        </select>
                     </div>
                  </div>
               </div>
               <div class="col-md-12 m-t-30 text-center">
                  <div class="text-center divider"><b>OR</b></div>
               </div>
               <div class="col-md-12 m-t-30" >
                  <div class="form-group">
                     <label for="" class="col-sm-3 control-label">User: </label>
                     <div class="col-sm-6">
                        <select class="user" name="user" >
                           <option value="%%dates[0]%%">Name</option>
                           <option value="%%dates[1]%%">Megha</option>
                           <option value="%%dates[2]%%">Human Resources</option>
                        </select>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="modal-footer m-t-15">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div id="share-modal" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Share With:</h4>
         </div>
         <div class="modal-body">
            <div class="row">
               <div class="col-md-12 m-t-15">
                  <div class="form-group">
                     <div class="col-sm-9">
                        <select class="share-with" name="states[]" multiple="multiple">
                           <option value="AL">Alabama</option>
                           <option value="WY">Wyoming</option>
                        </select>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="modal-footer m-t-15">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Share</button>
         </div>
      </div>
   </div>
</div>
<script>
   $(document).ready(function() {
      $('.department').select2();
      $('.user').select2();
      $('.share-with').select2();
   });
</script>
@endsection