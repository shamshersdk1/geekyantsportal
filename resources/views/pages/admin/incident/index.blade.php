@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid"  ng-app="myApp" ng-controller="incidentCtrl">
    <div class="breadcrumb-wrap">
        <div class="flex-class justify-content-between align-items-center">
            <div>
                <h1 class="admin-page-title">Support Tickets</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li class="active">Support Tickets</li>
                </ol>
            </div>
            <div>
                <a href="#" ng-click="createIncident()" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Add Ticket</a>
            </div>
        </div>
    </div>
    <div class="row bg-white search-section-top m-b">
        <div class="col-sm-6">
            <label>Deprtments: </label>
             <ui-select multiple  on-remove="departmentFilter($select.selected)" on-select="departmentFilter($select.selected)" ng-model="ctrl.multipleDemo.colors" theme="bootstrap" ng-disabled="ctrl.disabled" close-on-select="false" style="width: 300px;" title="Choose a color">
                <ui-select-match placeholder="Select department...">%%$item.type%%</ui-select-match>
                <ui-select-choices repeat="department.type as department in departments | filter:$select.search">
                   %%department.type%%
                </ui-select-choices>
              </ui-select>
        </div>
        <div class="col-sm-6">
            <label>Status:</label> 
            <select id="status_filter" multiple>
                <option value="open"> Open </option>
                <option value="in_progress"> In Progress </option>
                <option value="closed"> Closed </option>
                <option value="verified_closed"> Verified Closed </option>
            </select>
        </div>  
    </div>

    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                      @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif
        </div>
    </div>

    <table datatable="" class="table table-condensed table-hover" dt-options="dtOptions" dt-column-defs="dtColumnDefs" >
        <thead>
            <tr>
                <th width="5%">#</th>
                <th width="33%">Subject</th>
                <th width="10%">Depratment</th>
                <th width="10%">Assigned To</th>
                <th width="10%">Status</th>
                <th width="12%">Created by</th>
                <th width="12%">Last Edited by</th>
                <th class="text-right" width="10%">Action</th>
            </tr>
        </thead>
        <tbody>
            <tr ng-repeat="(i, incident) in data track by $index">
                <td>%%incident.id%%</td>
                <td style="white-space: normal;">%%incident.subject%%</td>
                <td>%%incident.department ?  incident.department.type : ""%%</td>
                <td>%%incident.assignee ? incident.assignee.name : "" %%</td>
                <td>
                    <span style="display: inline-block; text-transform: capitalize;"
                      ng-if=" incident.status == 'open' || incident.status == 'reopen' || incident.status == 'in_progress' "
                      class="label label-primary custom-label"
                      
                      >%%incident.status%%
                   </span>
                   <span style="display: inline-block; text-transform: capitalize;"
                      ng-if=" incident.status == 'closed' || incident.status == 'verified_closed' "
                      class="label label-danger custom-label"
                      >%%incident.status%%
                   </span>
                </td>
                <td>
                    <span>%%incident.raised_by ?  incident.raised_by.name : ""%%</span><br />
                    <small class="text-muted"><i class="fa fa-clock-o"></i> %%incident.created_at%%</small>
                </td>
                <td>
                    <span>%%incident.last_updated ?  incident.last_updated.user.name : ""%%</span><br />
                    <small class="text-muted"><i class="fa fa-clock-o"></i> %%incident.last_updated.created_at%%</small>
                    
                </td>
                <td class="text-right">
                    <button class="btn btn-sm btn-success" ng-click="viewDetail(incident.id)">
                        <i class="fa fa-eye"></i>
                    </button>
                    <button ng-if="incident.status == 'open'" class="btn btn-sm btn-warning" ng-click="editDetail(incident.id)">
                        <i class="fa fa-edit"></i>
                    </button>
                </td>
            </tr>
        </tbody>
    </table>
</div>
@endsection
