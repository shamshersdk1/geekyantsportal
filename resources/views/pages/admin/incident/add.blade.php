@extends('layouts.admin-dashboard')
@section('main-content')

<section class="new-project-section">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="flex-class justify-content-between align-items-center">
	            <div>
	                <h1 class="admin-page-title">Support Tickets</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
			  			<li><a href="{{ url('admin/incident') }}">Support Tickets</a></li>
			  			<li class="active">Add</li>
	        		</ol>
	            </div>
	            <div>
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>

	    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif

		<form class="form-horizontal" method="post" action="/admin/incident" enctype="multipart/form-data">
			<div class="panel panel-default">
				<div class="panel-body">
				    <div class="row">
				    	<div class="col-md-12">
				    		@if(isset($departments))
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">Department</label>
								<div class="col-sm-6">
									<select id="selectid2" name="department_id"  style="width=35%;" placeholder= "Select an option">
										<option value=""></option>
										@foreach($departments as $x)
											<option value="{{$x->id}}" >{{$x->type}}</option>
										@endforeach
									</select>
								</div>
							</div>
							@endif
							<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Subject</label>
						    	<div class="col-sm-6">
						      		<input type="text" class="form-control" id="subject" name="subject" value="{{ old('subject') }}">
						    	</div>
							</div>
							<!-- <div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Desciption</label>
						    	<div class="col-sm-6">
						      		<textarea class="form-control" id="subject" name="subject"></textarea>
						    	</div>
							</div> -->
							
					  	</div>
					</div>
				</div>
			</div>
  	  		<div class="text-center">
		  		<button type="submit" class="btn btn-success">Add Support Ticket</button>
		  	</div>
		</form>
	</div>
</section>
@endsection
