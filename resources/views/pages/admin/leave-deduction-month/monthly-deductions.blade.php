@extends('layouts.admin-dashboard')
@section('main-content')
    <div class="container-fluid mis-report">
        <form method="post" action="/admin/leave-deduction/{{$month->id}}/approve">
        <div class="breadcrumb-wrap">
           <div class="row">
              <div class="col-sm-8">
                 <h1 class="admin-page-title">Leave Deductions</h1>
                 <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="/admin/leave-deduction">Leave Deductions</a></li>
                    <li class="active">{{$month->formatMonth()}}</li>
                 </ol>
              </div>
              <div class="col-sm-4 text-right m-t-10">
                    <button class="btn btn-success pull-right" type="submit">Approve</button>
	            </div>
                <div class="col-md-12">
                    @if(!empty($errors->all()))
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <span>{{ $error }}</span><br/>
                            @endforeach
                        </div>
                    @endif
                    @if (session('message'))
                        <div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <span>{{ session('message') }}</span><br/>
                        </div>
                    @endif
                </div>
           </div>
        </div>
        <table class="table table-bordered table-hover bg-white">
          <tr>
              <th width="180px">Username</th>
              @foreach ($leaveTypes as $leaveType)
                <th class="text-center">{{$leaveType->title}}</th>
              @endforeach
          </tr>
            @if(!empty($userLeaves))
                @foreach($userLeaves as $userLeaveKey => $userLeave)
                <tr>
                    <td>{{$userLeave["username"]}}</td>
                    @foreach ($leaveTypes as $leaveType)
                        @if(!empty($userLeave[$leaveType->id]))
                          <td class="text-center">
                            <div class="input-group input-group-sm">
                              <input type="number" class="form-control" name="{{$userLeaveKey}}[{{$leaveType->id}}][value]" value="{{$userLeave[$leaveType->id]}}" />
                              <label class="input-group-addon">
                                <input type="checkbox" name="{{$userLeaveKey}}[{{$leaveType->id}}][is_checked]" value="true" checked="checked"/>
                              </label>                            
                            </div>
                          </td>
                        @else
                          <td class="text-center text-muted">-</td>
                        @endif
                    @endforeach
                </tr>
                @endforeach
                @else
                <td colspan="9" class="text-center">No Due Leave Deductions Found</td>
            @endif
        </table>
        </form>
    </div>
@endsection
