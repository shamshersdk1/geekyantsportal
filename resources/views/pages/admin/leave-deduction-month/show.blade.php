@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-6">
                <h1 class="admin-page-title">Leave Month Deduction</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li><a href="{{ url('admin/leave-deduction-month') }}">Leave Month Deduction</a></li>
        		  	<li>{{$month->formatMonth()}}</li>
        		</ol>
            </div>
            <div class="col-sm-6">

                @if(!$month->leaveSetting || ($month->leaveSetting &&  $month->leaveSetting->value == 'open'))
                    <div class="pull-right m-t-10">
                        <span>
                            <a href="/admin/leave-deduction-month/{{$month->id}}/regenerate" class="btn btn-warning"><i class="fa fa-gear fa-fw"></i>Regenerate</a>
                        </span>
                    </div>
                    <div class="pull-right m-t-10">
                        <span>
                            <a href="/admin/leave-deduction-month/{{$month->id}}/status-month" class="btn btn-danger"><i class="fa fa-lock fa-fw"></i>Lock</a>
                        </span>
                    </div>
                @endif
			</div>
		</div>
    </div>
    @if($month->leaveSetting &&  $month->leaveSetting->value == 'locked')
        <div class="pull pull-right">
            <span class="label label-primary">Locked at {{datetime_in_view($month->leaveSetting->created_at)}}</span>
        </div>
    @endif
	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
    </div>

    <form name="editForm" method="post" action="/admin/leave-deduction/{{$month->id}}/update">
        @if(!$month->leaveSetting ||  $month->leaveSetting->value == 'unlocked')
                <div class="row">
                    <div class="pull-right m-t-1 ">
                        <button style="display: none" type="submit" class="btn btn-success "><i class="fa fa-plus fa-fw"></i>Save</button>
                    </div>
                </div>
        @endif
        <div class="user-list-view">
            <div class="panel panel-default">
                <table class="table table-striped">
                    <tr>
                        <th class="text-center">#</th>
                        <th class="text-center">User</th>
                        <th class="text-center">Leave Duration</th>
                        <th class="text-center">Type</th>
                        <th class="text-center">Deducted day(s) for the month</th>
                        <th class="text-center">Amount</th>
                        <th class="text-center">Status</th>
                        <th class="pull-right">Action</th>
                    </tr>
                    @if(count($leaveDeductions) > 0)
                        @foreach($leaveDeductions as $index=>$leaveDeduction)
                            <tr>
                                <td class="text-center">{{ $index+1 }}<br></td>
                                <td class="text-center">{{ $leaveDeduction->user->name}}<br>{{ $leaveDeduction->user->employee_id}}
                                    @if(!$leaveDeduction->user->confirmation_date)
                                        <span class="label label-danger">Unconfirmed</span></td>
                                    @endif
                            </td>
                                <td class="text-center">{{ date_in_view($leaveDeduction->leave->start_date)}} to <br> {{ date_in_view($leaveDeduction->leave->end_date)}}</td>
                                <td class="text-center">
                                    @if($leaveDeduction->leave->leaveType->code =='paid')
                                     <span class="label label-primary">{{$leaveDeduction->leave->leaveType->title}}</span></td>
                                    @elseif($leaveDeduction->leave->leaveType->code =='sick')
                                     <span class="label label-warning">{{$leaveDeduction->leave->leaveType->title}}</span></td>
                                    @else
                                        <span class="label label-danger">{{$leaveDeduction->leave->leaveType->title}}</span></td>
                                     @endif
                                <td class="text-center">{{ $leaveDeduction->duration}}</td>
                                <td class="text-center">{{$leaveDeduction->amount}}</td>
                                <td class="text-center">
                                    @if($leaveDeduction  &&  $leaveDeduction->status == 'approved')
                                        <span class="label label-success custom-label">Approved</span>
                                        <br/>
                                    @elseif($leaveDeduction  &&  $leaveDeduction->status == 'ignored')
                                        <span class="label label-warning custom-label">Ignored</span>
                                        <br/>
                                    @elseif($leaveDeduction  &&  $leaveDeduction->status == 'forwarded')
                                        <span class="label label-success custom-label">Forwarded</span>
                                        <br/>
                                    @else
                                        <span class="label label-info custom-label">Pending</span>
                                    @endif

                                </td>
                                <td class="pull-right">
                                    @if(!$month->leaveSetting || $month->leaveSetting->value == 'open')
                                        @if($leaveDeduction->status != "forwarded")
                                        <a href="/admin/leave-deduction-month/{{$leaveDeduction->id}}/forward" class="btn btn-info btn-sm crude-btn"><i class="fa fa-mail-forward fa-fw"></i>Forward</a>
                                        @endif
                                        @if($leaveDeduction->status != "ignored")
                                        <a href="/admin/leave-deduction-month/{{$leaveDeduction->id}}/ignore" class="btn btn-warning btn-sm crude-btn"><i class="fa fa-info fa-fw"></i>Ignore</a>
                                        @endif
                                        <a href="/admin/leave-deduction-month/{{$leaveDeduction->id}}/delete" class="btn btn-danger btn-sm crude-btn"><i class="fa fa-trash fa-fw"></i>Delete</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5" class="text-center">
                                No Records found
                            </td>
                        </tr>

                    @endif
                </table>
            </div>
        </div>
    </form>
</div>
@endsection
