@extends('layouts.admin-dashboard')
@section('main-content')
<section>
   <div class="container-fluid">
      <div class="breadcrumb-wrap">
         <div class="row">
            <div class="col-sm-8">
               <h1>Payment Methods</h1>
               <ol class="breadcrumb">
                  <li><a href="/admin">Admin</a></li>
                  <li><a href="/admin/payment-method">Payment Methods</a></li>
                  <li class="active">{{$paymentMethodObj->id}}</li>
               </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
         </div>
      </div>
      @if(!empty($errors->all()))
      <div class="alert alert-danger">
         @foreach ($errors->all() as $error)
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
         <span>{{ $error }}</span><br/>
         @endforeach
      </div>
      @endif
      @if (session('message'))
      <div class="alert alert-success">
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
         <span>{{ session('message') }}</span><br/>
      </div>
      @endif
      <div class="row cms-dev">
         <div class="col-md-12">
            <h4 >Payment Method Details</h4>
            <div class="panel panel-default">
               <div class="panel-body row">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <div class=" col-md-10">
                     <label class=" col-md-3">Name: </label>
                     <span>{{$paymentMethodObj->name}}</span>
                  </div>
                  <div class=" col-md-10">
                     <label class=" col-md-3">Account Number: </label>
                     <span>{{ !empty($paymentMethodObj->account_number) ? $paymentMethodObj->account_number : '' }}</span>
                  </div>
                  <div class=" col-md-10">
                        <label class="col-md-3">Description: </label>
                        <span class="col-md-8" style="white-space:pre-line">{{!empty($paymentMethodObj->description)?$paymentMethodObj->description:'' }}</span>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
@endsection