@extends('layouts.admin-dashboard')
@section('main-content')

<section class="new-project-section">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">Payment Methods</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
			  			<li><a href="{{ url('admin/payment-method') }}">Payment Methods</a></li>
			  			<li class="active">Add</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>

	    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif

		<form class="form-horizontal" method="post" action="/admin/payment-method" enctype="multipart/form-data">
			
		    <div class="row">
		    	<div class="col-sm-8 col-sm-offset-2">
		    		<div class="panel panel-default">
						<div class="panel-body">
							<div class="form-group">
						    	<label class="col-sm-3 control-label">Name</label>
						    	<div class="col-sm-8">
						      		<input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
						    	</div>
							</div>
							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Account Number</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="account_number" name="account_number" value="{{ old('account_number') }}">
								</div>
							</div>
							<div class="row">
								<label for="" class="col-sm-3 control-label">Description</label>
								<div class="col-sm-8">
									<textarea class="form-control" rows="5" id="description" name="description">{{ old('description') }}</textarea>
								</div>
					  	</div>
					</div>
				</div>
				<div class="text-right">
			  		<button type="reset" class="btn btn-default"><i class="fa fa-remove"></i> Cancel</button>
			  		<button type="submit" class="btn btn-success"><i class="fa fa-plus"></i> Add Payment Method</button>
			  	</div>
			</div>
		</form>
	</div>
</section>
@endsection
