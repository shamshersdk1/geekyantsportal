@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Payment Methods</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
		  			<li class="active">Payment Methods</li>
        		</ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
				<a href="/admin/payment-method/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Add Payment Method</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
	    	@if(!empty($errors->all()))
	            <div class="alert alert-danger">
	                @foreach ($errors->all() as $error)
	                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                    <span>{{ $error }}</span><br/>
	                  @endforeach
	            </div>
	        @endif
	        @if (session('message'))
	            <div class="alert alert-success">
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ session('message') }}</span><br/>
	            </div>
			@endif
		
			<div class="user-list-view">
	            <div class="panel panel-default">
	            	<table class="table table-striped">
						<tr>
							<th width="5%" class="sorting_asc">#</th>
							<th width="20%">Name</th>
	                        <th width="25%">Account Number</th>
							<th width="30%">Description</th>
							<th class="text-right sorting_asc" width="20%">Actions</th>
						</tr>
						@if(count($paymentMethods) > 0)
							@foreach($paymentMethods as $paymentMethod)
								<tr>
									<td><a>{{$paymentMethod->id}}</a></td>
									<td>{{$paymentMethod->name}}</td>
									<td> {{!empty($paymentMethod->account_number) ? $paymentMethod->account_number : '' }}</td>
									<td>{{!empty($paymentMethod->description) ? $paymentMethod->description : '' }}</td>
	                                <td class="text-right">
										<a href="/admin/payment-method/{{$paymentMethod->id}}" class="btn btn-success crude-btn btn-sm">View</a>
										<a href="/admin/payment-method/{{$paymentMethod->id}}/edit" class="btn btn-info btn-sm crude-btn">Edit</a>
										<form action="/admin/payment-method/{{$paymentMethod->id}}" method="post" style="display:inline-block;">
											<input name="_method" type="hidden" value="DELETE">
											<button class="btn btn-danger btn-sm crude-btn" type="submit">Delete</button>
										</form>
									</td>
								</tr>
							@endforeach
		            	@else
		            		<tr>
		            			<td colspan="5" class="text-center">No results found.</td>
		            		</tr>
		            	@endif
	            	</table>
					<div class="col-md-12">
	                    <div class="pageination pull-right">
	                        <nav aria-label="Page navigation">
	                            <ul class="pagination">
	                                {{ $paymentMethods->render() }}
	                            </ul>
	                        </nav>
	                    </div>
	                </div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
@endsection
