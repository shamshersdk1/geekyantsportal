@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid" ng-app="myApp" ng-controller="techtalkIndexctrl">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-10">
                <h1>Tech Talk Participants</h1>
                <ol class="breadcrumb">
                    <li>Admin</li>
                    <li>Tech Talk Dashboard</li>
                </ol>
            </div>
            <div class="col-sm-2">
            <button type="button"  class="btn btn-success"  ng-click="addParticipants()">Add Participants</button>

            </div>
        </div>
    </div>
    <div class="row">   
        <div class="col-sm-12">
        <table class="table table-striped">
                <tr>
                    <th width="5%">Id</th>
                    <th width="11%">User name</th>
                    <th width="11%">User Id</th>
                    <th width="11%">Title</th>
                    <th width="11%">Description</th>
                    <th width="11%">Date</th>
                    <th width="11%">Status</th>
                    <th width="11%">Created At</th>
                    <th width="18%">Actions</th>
                </tr>
                <tr ng-repeat="user_data in model.data">
                    <td>%%user_data.id%%<td>
                    <td>%%user_data.name%%</td>
                    <td>%%user_data.title%%</td>
                    <td>%%user_data.description%%</td>
                    <td>%%user_data.event_date%%</td>
                    <td>%%user_data.status%%</td>
                    <td>%%user_data.created_at%%</td>
                    <td>
                        <button class=btn-info ng-click="viewParticipants(user_data.id)">View</button>
                        <button class=btn-primary ng-click="editParticipant(user_data.id)">Edit</button>
                        <button class=btn-danger ng-click="deleteParticipant(user_data.id)">Delete</button>

                    </td>

        </div>
    </div>
</div>
@endsection 
