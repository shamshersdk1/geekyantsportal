@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid resource-report-index">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Project Resource Report</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a>Project Resource Report</a></li>
                </ol>
            </div>
        </div>
    </div>

<div class="row">
    <div class="col-md-12 bg-white search-section-top">
        <form class="form-group col-md-12 p-0" action="/admin/project-resource-report" method="get">
            <div class="col-md-2 p-l-0">
                <div class='input-group date' id='datetimepickerStart_search'>
                    <input type='text' id="sd" class="form-control" value="<?=( isset( $_GET['start_date'] ) ? $_GET['start_date'] : '' )?>" autocomplete="off" name="start_date" placeholder="Start Date" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <div class="col-md-2 p-l-0">
                <div class='input-group date' id='datetimepickerEnd_search'>
                    <input type='text' id="ed" class="form-control" name="end_date" value="<?=( isset( $_GET['end_date'] ) ? $_GET['end_date'] : '' )?>" autocomplete="off" placeholder="End Date" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
          <!--  <div class="col-sm-3 ">
                <div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control" value="{{$start_date}}" placeholder="Choose Start Date" id="txtStartDate" name="start_date" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <div class="col-md-3">
                <div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control" value="{{$end_date}}" placeholder="Choose End Date" id="txtEndDate" name="end_date" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div> -->
            <div class="col-md-3 p-l-0">
                <!-- <h4 class="admin-section-heading">Search Users </h4> -->
                <div class="multipicker">
                    <div class="">
			    		<select class="js-example-basic-multiple22 form-control" multiple="multiple" name="users[]" placeholder="Search Users">
					    	@if(isset($resourceList))
                                @foreach($resourceList as $resource)
                                    <?php
					    				$saved = false;
					    				foreach($selected_users as $selected_user){
					    					if($selected_user == $resource->id){
					    						$saved = true;
					    					}
					    				}
					    			?>
					    			@if(isset($selected_users))
					    				@if($saved)
						    				<option value="{{$resource->id}}" selected >{{$resource->name}}</option>
						    			@else
						    				<option value="{{$resource->id}}">{{$resource->name}}</option>
						    			@endif
						    		@else
						    			<option value="{{$resource->id}}">{{$resource->name}}</option>
						    		@endif
					    		@endforeach
					    	@endif
					    </select>
					</div>
                </div>
		    </div>
            <div class="col-md-3 p-l-0">
                <!-- <h4 class="admin-section-heading">Search Projects </h4> -->
                <div class="multipicker"  >
                    <div class=" ">
			    		<select class="js-example-basic-multiple22 form-control" multiple="multiple" name="input_projects[]" placeholder="Search Projects ">
					    	@if(isset($projectList))
                                @foreach($projectList as $project)
                                    <?php
					    				$saved = false;
					    				foreach($selected_projects as $selected_project){
					    					if($selected_project == $project->id){
					    						$saved = true;
					    					}
					    				}
					    			?>
					    			@if(isset($selected_projects))
					    				@if($saved)
						    				<option value="{{$project->id}}" selected >{{$project->project_name}}</option>
						    			@else
						    				<option value="{{$project->id}}">{{$project->project_name}}</option>
						    			@endif
						    		@else
						    			<option value="{{$project->id}}">{{$project->project_name}}</option>
						    		@endif
					    		@endforeach
					    	@endif
                        </select>

                    </div>

                </div>

		    </div>
            <div class="col-md-2" style="padding-top:6px">
                                 <button type="submit" class="btn btn-primary btn-sm center" style="margin-left: 2px;">Search</button>

            </div>
           
            
        </form>
    </div>
    <div class="col-md-5 pull-right m-t-15">
      <div class="onleave pull-right">
      <div class="red-small-box inline"><span>On Leave</spnn></div>
       <div class="small-sat-sun-box inline"><span>Holiday</span></div>
       <div class="free-green-background inline"><span>Free</span></div>
      
            <!-- <div class="col-md-12">
                
                    <div class="col-sm-1 col-xs-1">
                        <div class="red-small-box"></div>
                    </div>
                    <div class="col-sm-4 col-xs-5">
                        <div>On Leave</div>
                    </div>
                    <div class="col-sm-1 col-xs-1">
                        <div class="small-sat-sun-box"></div>
                    </div>
                    <div class="col-sm-6 col-xs-5">
                        <div>Holiday</div>
                    </div>
                
                    <div class="col-xs-1">
                        <div class="red-small-box free-green-background"></div>
                    </div>
                    <div class="col-xs-11">
                        <div>Unassigned</div>
                    </div>
                </div> -->
    </div>
    </div>

</div>
    <div class="row m-t-15">
        <div class="col-md-12">
            <div class="panel panel-default table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th width=16%; class="td-right-text">Name</th>
                            @foreach($data['dates'] as $key => $date)
                                <th width='12%'; class="td-center-text">{{$date}}</th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data['project_name'] as $index => $project_name )
                          <tr>
                                <td class="td-right-text">
                                {{ $project_name }}
                                </td>
                                @for ($i = 0; $i < count($data['projectResourceDataList']); $i++)
                                    <td class="td-center-text  {{ $data['projectResourceDataList'][$i][$index]['users'] ? '' : 'free-green-background_td' }}">
                                        {{ $data['projectResourceDataList'][$i][$index]['users'] ? $data['projectResourceDataList'][$i][$index]['users']: 'None'}}
                                    </td>
                                @endfor
                            </tr>
                        @endforeach
                    </tbody> 
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(function(){
        
        $('#datetimepickerStart_search').datetimepicker({
            minDate: new Date().getMonth() <= 3 ? new Date(new Date().getFullYear()-1+"-04-01") : new Date(new Date().getFullYear()+"-04-01"), //Set min date to April 1
            //Set max date to upcoming march
            maxDate: new Date().getMonth() <= 2 ? new Date(new Date().getFullYear()+"-03-31") : new Date(new Date().getFullYear()+1+"-03-31"),
            format: 'YYYY-MM-DD'
        });

        $('#datetimepickerEnd_search').datetimepicker({
            //Set max date to upcoming march
            maxDate: new Date().getMonth() <= 2 ? new Date(new Date().getFullYear()+"-03-31") : new Date(new Date().getFullYear()+1+"-03-31"),
            format: 'YYYY-MM-DD'
        });

        $("#datetimepickerStart_search").on("dp.change", function (e) {
            $('#datetimepickerEnd_search').data("DateTimePicker").minDate(e.date);
        });

        $("#btnReset").click(function(){
           window.location.href='/admin/resource-report';
    }); 
    });
</script>
@endsection