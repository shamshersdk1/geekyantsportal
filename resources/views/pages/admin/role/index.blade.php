@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">List of Roles</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li><a href="{{ url('admin/role') }}">List of Roles</a></li>
        		</ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
				<button class="btn btn-success" type="button" data-toggle="modal" data-target="#addRoleModal"><i class="fa fa-plus fa-fw"></i>Add New Role</button>
            </div>
            <!--<div class="col-sm-6">
            	@if( !empty($search) )
            		Showing result for search {{$search}}
            	@endif
            </div>
            <div class="col-sm-6">
	            <form action="/admin/search-role" class="pull-right">
	                <input type="text" name="search" placeholder="search.." value="@if(isset($search)){{$search}}@endif">
	            </form>
	        </div>-->
        </div>
    </div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
	<div class="panel panel-default">
		<table class="table table-striped">
			<th>#</th>
			<th>Name of role</th>
			<th>Code</th>
			<th>Description</th>
			<th class="text-right">Actions</th>
			@if(count($roles) > 0)
	    		@foreach($roles as $role)
					<tr>
						<td class="td-text">
							{{$role->id}}
						</td>
						<td class="td-text">
						@if(!empty($role->name))
							{{$role->name}}
						@endif
						</td>
						<td class="td-text">
						@if(!empty($role->code))
							{{$role->code}}
						@endif
						</td>
						<td class="td-text">
						@if(!empty($role->description))
							{{$role->description}}
						@endif
						</td>
						<td class="text-right">
							<button class="btn btn-info btn-sm" type="button"
									data-toggle="modal" data-target="#editRoleModal"
									data-id="{{$role->id}}" data-title="Algorithms"><i class="fa fa-pencil fa-fw"></i>Edit</button>
							<form name="deleteForm" method="post" action="role/{{$role->id}}" onsubmit="return deleteuser();" style="display: inline-block;">
								<input name="_method" type="hidden" value="delete" />
								<button type="submit" class="btn btn-danger btn-sm crud-btn"><i class="fa fa-trash fa-fw"></i>Delete</button> 
							</form>
						</td>
					</tr>
				@endforeach
	    	@else
	    		<tr>
	    			<td colspan="3">No results found.</td>
	    			<td></td>
	    			<td></td>
	    		</tr>
	    	@endif
	   </table>
	</div>
	<div class="pull-right">
		{{$roles->links()}}
	</div>
	</div>
@endsection


@section('js')
	<script>
	console.log("abcd");
		function deleteuser() {
			var x = confirm("Are you sure you want to delete?");
			if (x)
				return true;
			else
				return false;
		}
</script>
@include('pages/admin/role/add-role-modal')
@include('pages/admin/role/edit-role-modal')
<script>
	let role;
	function updateRole() {
		role.name = $('#editRoleModal input[name="name"]').val();
		role.code = $('#editRoleModal input[name="code"]').val();
		role.desc = $('#editRoleModal textarea[name="desc"]').val();
		$.ajax({
			type: 'PUT',
			url: '/admin/role/'+role.id,
			dataType: 'text',
			data: role,
			beforeSend: function() {
				$("div.loading").show();
				$("div.content").hide();
			},
			complete: function() {
				$("div.loading").hide();
				$("div.content").show();
			},
			success: function(data) {
				$('#editRoleModal').modal('hide');
				window.location.reload();
			},
			error: function(errorResponse){
				$('#editRoleModal').modal('hide');
				console.log(errorResponse);
				window.location.reload();
			}
		});
	}
	$(function(){
		$("#addRoleModal").on("show.bs.modal", function(){
			$(this).find("input")
				.val('')
				.end()
		});

		$("#addRoleModal").on("hidden.bs.modal", function(){
			$(this).find("input")
				.val('')
				.end()
		});
		$('#editRoleModal').on("show.bs.modal", function (e) {
			let id = $(e.relatedTarget).data('id')
			$.ajax({
				type: 'GET',
				url: '/admin/role/'+id,
				beforeSend: function() {
					$("div.loading").show();
					$("div.content").hide();
				},
				complete: function() {
					$("div.loading").hide();
					$("div.content").show();
				},
				success: function(data) {
					role = data;
					$('#editRoleModal input[name="name"]').val(data.name);
					$('#editRoleModal input[name="code"]').val(data.code);
					$('#editRoleModal textarea[name="desc"]').val(data.description);
				}
			});
		});

		$("#editRoleModal").on("hidden.bs.modal", function(){
			$(this).find("input")
				.val('')
				.end()
		});
	});
</script>

@endsection