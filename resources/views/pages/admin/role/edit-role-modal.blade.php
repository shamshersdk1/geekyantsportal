<div class="modal fade" id="editRoleModal" tabindex="-1" role="dialog" aria-labelledby="editRoleModalLabel">
   <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="editRoleModalLabel">Edit Role</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 ma-bot-10">
                        <div class="form-group ma-bot-10" style="padding:0% 5% 0% 5%">
                            <label for="name">Role Name:</label>
                            <input type="text" name="name" class="form-control" placeholder="Name" required>
                        </div>
                    </div>
                    <div class="col-md-12 ma-bot-10">
                            <div class="form-group ma-bot-10" style="padding:0% 5% 0% 5%">
                                <label for="code">Role Code:</label>
                                <input type="text" name="code" class="form-control" placeholder="URL" required>
                            </div>
                        </div>
                        <div class="col-md-12 ma-bot-10">
                            <div class="form-group ma-bot-10" style="padding:0% 5% 0% 5%">
                                <label for="desc">Description:</label>
                                <textarea type="text" rows="5" name="desc" class="form-control" placeholder="Description"></textarea>
                            </div>
                        </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>&nbsp;
                <span class="pull-right">
                    <button type="submit" class="btn btn-primary"  onclick="updateRole()">
                        Update
                    </button>
                </span>
            </div>
        </div>
    </div>
</div>
