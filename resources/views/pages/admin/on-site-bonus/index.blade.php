@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">On Site Bonuses</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/bonus/on-site') }}">On Site Bonuses</a></li>
                </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <a href="/admin/bonus/on-site/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i> Add new Bonus</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
    	@if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span><br/>
                  @endforeach
            </div>
        @endif
        @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
        @endif  
    <div class="user-list-view">
        <div class="panel panel-default"> 
            <table class="table table-striped">
                <thead>
                    <th>#</th>
                    <th>Name</th>
                    <th>Project</th>
                    <th>Type</th>
                    <th>From Date</th>
                    <th>To Date</th>
                    <th>Amount</th>
                    <th>Created At</th>
                    <th>Created By</th>
                    <th>Actions</th>
                </thead>
                @if(count($OnSiteBonus) > 0)
                    @foreach($OnSiteBonus as $index => $bonus)
                        <tr>
                            <td class="td-text">{{$index+1}}</td>
                             <td class="td-text">{{$bonus->user->name}}</td>
                             @if(!empty($bonus->reference->project_name))
                                <td class="td-text">{{$bonus->reference->project_name}}</td>
                             @else
                                <td class="td-text"></td>
                             @endif
                             <td class="td-text">{{$bonus->type}}</td>
                             <td class="td-text">{{date_in_view($bonus->to_date)}}</td>
                              <td class="td-text">{{date_in_view($bonus->from_date)}}</td>
                             <td class="td-text">{{$bonus->amount}}</td>
                             <td class="td-text">{{date_in_view($bonus->created_at)}}</td>
                             <td class="td-text">{{$bonus->reviewer->name}}</td>
                            <td class="td-text">
                                <form action="/admin/bonus/on-site/{{$bonus->id}}/edit" method="put" style="display:inline-block;" >
                                    <button class="btn btn-primary  btn-sm" type="submit"><i class="fa fa-edit btn-icon-space" aria-hidden="true"></i>Edit</button>
                                </form>
                                <form action="/admin/bonus/on-site/{{$bonus->id}}" method="post" style="display:inline-block;" >
                                {{ method_field('DELETE') }}
                                <button class="btn btn-danger  btn-sm" type="submit"><i class="fa fa-trash btn-icon-space" aria-hidden="true"></i>Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="10">No results found.</td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
        @if(!empty($OnSiteBonus))
            {{$OnSiteBonus->links()}}
        @endif
	</div>
</div>
@endsection
