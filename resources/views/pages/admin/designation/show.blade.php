@extends('layouts.admin-dashboard')
@section('main-content')
<section>
   <div class="container-fluid">
      <div class="breadcrumb-wrap">
         <div class="row">
            <div class="col-sm-6">
               <h1>Designations</h1>
               <ol class="breadcrumb">
                  <li><a href="/admin">Admin</a></li>
                  <li><a href="/admin/designations">Designations</a></li>
                  <li class="active">{{$designationObj->id}}</li>
               </ol>
            </div>
         </div>
      </div>
      @if(!empty($errors->all()))
      <div class="alert alert-danger">
         @foreach ($errors->all() as $error)
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
         <span>{{ $error }}</span><br/>
         @endforeach
      </div>
      @endif
      @if (session('message'))
      <div class="alert alert-success">
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
         <span>{{ session('message') }}</span><br/>
      </div>
      @endif
      <div class="row cms-dev">
         <div class="col-md-12">
            <h4 >Designation Detail</h4>
            <div class="panel panel-default">
               <div class="panel-body row">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <div class=" col-md-6">
                     <label>Designation : </label>
                     <span>{{$designationObj->designation}}</span>
                  </div>
                  <div class=" col-md-6">
                     <label>Status : </label>
                     <span>{{ $designationObj->status ? 'Active' : 'Inactive' }}</span>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
@endsection