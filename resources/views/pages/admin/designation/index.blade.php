@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Designations</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
		  			<li class="active">Designation List</li>
        		</ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
				<a href="/admin/designations/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Add Desgination</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
	    	@if(!empty($errors->all()))
	            <div class="alert alert-danger">
	                @foreach ($errors->all() as $error)
	                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                    <span>{{ $error }}</span><br/>
	                  @endforeach
	            </div>
	        @endif
	        @if (session('message'))
	            <div class="alert alert-success">
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ session('message') }}</span><br/>
	            </div>
			@endif
		</div>
		<div class="user-list-view">
            <div class="panel panel-default">
            	<table class="table table-striped">
					<tr>
						<th width="5%" class="sorting_asc">#</th>
						<th width="15%">Designation</th>
                        <th width="15%">Count of Active Employee</th>
						<th width="10%">Status</th>
						<th class="text-right sorting_asc" width="18%">Actions</th>
					</tr>
					@if(count($designations) > 0)
						@foreach($designations as $desgination)
							<tr>
								<td class="td-text"><a>{{$desgination->id}}</a></td>
								<td class="td-text">{{$desgination->designation}}</td>
                                <td class="td-text">
                                    @if(isset($designation_user_count_map[$desgination->id]))
                                       {{$designation_user_count_map[$desgination->id]}}
                                    @else
                                        0
                                    @endif
                                </td>
								<td>
									<div class="col-md-4">
										<div class="onoffswitch">
											<input type="checkbox" value="{{$desgination->id}}" name="onoffswitch[]" onchange="toggle(this)"
													class="onoffswitch-checkbox" id="{{$desgination->id}}" 
											<?php echo (!empty($desgination->status)&&$desgination->status==1) ? 'checked':''; ?>>
											<label class="onoffswitch-label" for= {{$desgination->id}} >
												<span class="onoffswitch-inner"></span>
												<span class="onoffswitch-switch"></span>
											</label>
										</div>
									</div>
								</td>
							
								<td class="text-right">
									<a href="/admin/designations/{{$desgination->id}}" class="btn btn-success crude-btn btn-sm">View</a>
									<a href="/admin/designations/{{$desgination->id}}/edit" class="btn btn-info btn-sm crude-btn">Edit</a>
									<form action="/admin/designations/{{$desgination->id}}" method="post" style="display:inline-block;">
										<input name="_method" type="hidden" value="DELETE">
										<button class="btn btn-danger btn-sm crude-btn" type="submit">Delete</button>
									</form>
								</td>
							</tr>
						@endforeach
	            	@else
	            		<tr>
	            			<td colspan="3" class="text-center">No results found.</td>
	            		</tr>
	            	@endif
            	</table>
				<div class="col-md-12">
                    <div class="pageination pull-right">
                        <nav aria-label="Page navigation">
                              <ul class="pagination">
                                {{ $designations->render() }}
                              </ul>
                        </nav>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>
</div>
<script>
    $(function(){
        toggle = function (item) {
			//window.location.href='users-billable/'+item.value;
			$.ajax({
				type:'POST',
				url:'/api/v1/designations/status',
				data:{id:item.value},
				success:function(data){
					console.log(data);
					}
			});
		}
    });
</script>
@endsection
