@extends('layouts.admin-dashboard')
@section('main-content')

<section class="new-project-section">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">Designations</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
			  			<li><a href="{{ url('admin/designations') }}">Designations</a></li>
			  			<li class="active">{{ $designationObj->id }}</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>

	    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif

		<form class="form-horizontal" method="post" action="/admin/designations/{{$designationObj->id}}" enctype="multipart/form-data">
			<input name="_method" type="hidden" value="PUT" />
			<div class="panel panel-default">
				<div class="panel-body">
				    <div class="row">
				    	<div class="col-md-12">
							<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Designation</label>
						    	<div class="col-sm-6">
									  <input type="text" class="form-control" id="designation" name="designation" value="{{ $designationObj->designation }}">
						    	</div>
							  </div>
							  <div class="form-group">
								<label class="col-sm-2 control-label">Status:</label>
								@if( $designationObj->status )
								<div class="col-sm-4">
									<select class="form-control" name="status">
										<option value="1" selected>Active</option>
										<option value="0">Inactive</option>
									</select>
								</div>
								@else
								<div class="col-sm-4">
									<select class="form-control" name="status">
										<option value="1">Active</option>
										<option value="0" selected>Inactive</option>
									</select>
								</div>
								@endif
							</div>
					  	</div>
					</div>
				</div>
			</div>
  	  		<div class="text-center">
		  		<button type="submit" class="btn btn-success">Update Designation</button>
		  	</div>
		</form>
	</div>
</section>
@endsection
