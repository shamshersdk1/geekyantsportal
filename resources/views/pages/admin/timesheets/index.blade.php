@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Timesheet</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li><a href="{{ url('admin/timesheet') }}">List of Resources</a></li>
        		</ol>
            </div>
		</div>
	</div>
	<div class="panel panel-default">
		<table class="table table-striped">
			<th width="15%">Resource Name</th>
			<th width="25%">Current Projects </th>
			<th width="15%" class="td-text-center">Last Week Hours (Hours/Approved)</th>
			<th width="15%" class="td-text-center">Current Week Hours (Hours/Approved)</th>
			<th class="text-right" style="padding-right: 50px">Actions</th>
			@if(count($userDataList) > 0)
	    		@foreach($userDataList as $userData)
					<tr>
						<td class="td-text">
							{{ $userData['user_name'] }} 
						</td>
						<td class="td-text">
							{{ $userData['projects'] }} 
						</td>
						<td class="td-text-center">
							{{ $userData['hours_details']['last_week_applied_total'] }} / {{ $userData['hours_details']['last_week_approved_total'] }}
						</td>
						<td class="td-text-center">
							{{ $userData['hours_details']['current_applied_total'] }} / {{ $userData['hours_details']['current_approved_total'] }}
						</td>
						<td class="text-right">
							<button disabled href="/admin/timesheet/{{$userData['user_id']}}/notify" class="btn btn-warning crud-btn btn-sm"><i class="fa fa-bell btn-icon-space" aria-hidden="true" disabled></i>Send notification</button>
							<a href="/admin/timesheet/{{$userData['user_id']}}/show" class="btn btn-success crud-btn btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View Timesheet</a>
						</td>
					</tr>
				@endforeach
	    	@else
	    		<tr>
	    			<td colspan="3">No results found.</td>
	    			<td></td>
	    			<td></td>
	    		</tr>
	    	@endif
	   </table>
	</div>
</div>	
@endsection

