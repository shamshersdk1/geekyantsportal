<a  id="previous" class="prev btn btn-default margin-bottom-sm js-filter-date-actions" style="color: #616161;"><i class="fa fa-arrow-left" aria-hidden="true"></i></a> 
<a  id="next" class="next btn btn-default margin-bottom-sm js-filter-date-actions" style="color: #616161;"><i class="fa fa-arrow-right" aria-hidden="true"></i></a> 
<script>
function getQueryString(date)
    {
        var yyyy = date.getFullYear();
        var mm = date.getMonth()+1;
        if(mm < 10)
        {
            mm = '0'+ mm;
        }
        var dd = date.getDate();
        if(dd < 10)
        {
            dd = '0'+ dd;
        }
        return yyyy+'-'+mm+'-'+dd;
    }
$(function(){
    var flag=0;
    var range = 7;
    @if(Request::get('start_date') && Request::get('end_date'))
        var start_date = new Date('{{Request::get('start_date')}}');
        var end_date = new Date('{{Request::get('end_date')}}');
        if(start_date.getDay() == 1 && end_date.getDay() == 0)
        {
            flag = 1;
            $('#dates').val(start_date.toDateString() +" to "+ end_date.toDateString());
        }
    @endif
    if(flag == 0)
    {
        var d = new Date();
        var diff = d.getDay() - 1;
        if(diff==-1)
        {
            diff=6;
        }
        var start_date = new Date();
        var end_date = new Date();
        start_date.setDate(d.getDate() - diff);
        end_date.setDate(start_date.getDate() + (range - 1));
        $('#dates').val(start_date.toDateString() +" to "+ end_date.toDateString());
    }
    start_date.setDate(start_date.getDate() + range);
    end_date.setDate(end_date.getDate() + range);
    var message = '?start_date='+getQueryString(start_date)+'&end_date='+getQueryString(end_date);
    $('#next').prop(
        'href',
        '/admin/timesheet/' + {{$id}} + '/show'+message
    );
    start_date.setDate(start_date.getDate() - 2*range);
    end_date.setDate(end_date.getDate() - 2*range);
    var message = '?start_date='+getQueryString(start_date)+'&end_date='+getQueryString(end_date);
    $('#previous').prop(
        'href',
        '/admin/timesheet/' + {{$id}} + '/show'+message
    );
});

</script>
<br>
<br>