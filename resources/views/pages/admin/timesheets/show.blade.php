@extends('layouts.admin-dashboard')
@section('main-content')
<script type="text/javascript">
    window.dates = '<?php echo json_encode($dates); ?>';
	window.projects = {{ $projects }};
	window.verticalTotals = '<?php echo json_encode($verticalTotals); ?>';
	window.user = '<?php echo json_encode($user_name); ?>';
</script>

<!-- Header part -->
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Timesheet</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/timesheet') }}">List of Resources</a></li>
                    <li><a>{{ $user_name }}</a></li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- Navigation -->
<div class="container-fluid">
    <div class="row">
        <div class="col-md-10">       
            @include('pages.admin.timesheets.controls',array('id' => $id))
        </div>          
        <div class="col-md-2">
        <div class="onleave">
        <div class="red-small-box"><span>On Leave</span></div>
         <div class="small-sat-sun-box"><span>Holiday</span></div>
         <div class="free-green-background"><span>Free</span></div>
      </div>
        </div>
    </div>
</div>

<!-- Timesheet form  -->
<div ng-app="myApp" ng-controller="admintimesheetCtrl" class="row" style="margin-left: 1px; margin-right: 1px; margin-top:15px;">
        
        <div class="col-md-12">
            <div class="panel panel-default">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th width="15%" class="td-text">Projects</th>
                            <th ng-repeat="date in dates"> 
                                <div>%% date %%</div>
                            </th>

                            <th width="15%" class="text-center td-text">Total hours</th>
                        </tr>
                    </thead>
                        <tr ng-repeat="project in projects"> 
                            <td>
                                %%project.name%%
                            </td>
                                <td ng-repeat="d in project.data">
                                    %%d.hours | formatHours: d.hours%% / 
                                    <input ng-style=" { 'background-color' : (d.is_holiday) ? '#DFE8FB;' : (d.approver_id) ? '#eee' : (d.on_leave) ? '#FF0000' : '' }" 
                                           type="text" 
                                           class="form-control" 
                                           style="width: 55%; height: 30px; display: inline"        
                                           title="text-area" 
                                           ng-click="addTimeAprrover(project.name,d,user)"
                                           ng-value="%%(d.approver_id) ? 'd.approved_hours | formatHours: d.approved_hours' : '00.0 | formatHours: 00.0' %% " />
                                  <!--<a href="#" class="tooltip" data-tip="this is the tip!"></a></td> -->
                            <td style="text-align: center"> %%project.data | sumByRow: d.hours%% / %%project.data | sumByApprovedHours: d.approved_hours%%</td>
                        </tr>
                        <tr >
                            <td> Daily Totals </td>
                            <td ng-repeat="veritcalTotal in verticalTotals track by $index">%%veritcalTotal.total | formatHours: veritcalTotal.total %% / %%veritcalTotal.approved_total | formatHours: veritcalTotal.approved_total%%  </td>
                            <td>  </td>
                        </tr>
                </table>
            </div>
        </div>
    <script>
        $(function () {
            $('[data-toggle="popover"]').popover()
        })
    </script>
    <script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker();
            });
        </script>

<!-- Timesheet form  -->

@endsection
