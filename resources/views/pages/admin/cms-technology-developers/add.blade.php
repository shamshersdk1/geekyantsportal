@extends('layouts.admin-dashboard')
@section('main-content')

<section class="new-project-section">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">CMS Developers</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
			  			<li><a href="{{ url('admin/cms-developers') }}">CMS Developers</a></li>
			  			<li class="active">Add</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>

	    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif

		<form class="form-horizontal" method="post" action="/admin/cms-technology-developers" enctype="multipart/form-data">
			<div class="panel panel-default">
				<div class="panel-body">
				    <div class="row">
				    	<div class="col-md-12">
						  	<div class="form-group">
								<label for="" class="col-sm-2 control-label">CMS Developer Technology*: </label>
								<div class="col-sm-4" style="padding-top: 10px">
								<input type="hidden" name="cms_technology_id" value="{{ $cmsDeveloper->id }}">
									{{$cmsDeveloper->name}}
								</div>
							</div>
						  	<div class="form-group">
								<label for="" class="col-sm-2 control-label">CMS Developer*: </label>
								<div class="col-sm-4">
									@if(isset($users))
										<select id="selectid2" name="user_id" style="">
											<option value="">Select</option>
											@foreach($users as $x)
												<option value="{{$x->id}}" >{{$x->name}}</option>
											@endforeach
										</select>
									@endif
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Status:</label>
								<div class="col-sm-4">
									<select class="form-control" name="status">
										<option value="1">Active</option>
										<option value="0">Inactive</option>
									</select>
								</div>
							</div>
						  	<div class="form-group">
								<label for="" class="col-sm-2 control-label">Upload Image :</label>
								<div class="col-sm-4">    
									<input type="file" id="file" class="form-control" name="file" multiple="false">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label"><i class="fa fa-graduation-cap fa-lg admindashboard-icon"></i> About :</label>
								<div class="col-sm-4">
									<textarea class="form-control" id="about" name="about" rows="3" placeholder="Enter details">{{old('about')}}</textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label"><i class="fa fa-shield fa-lg admindashboard-icon"></i> Area of Interest :</label>
								<div class="col-sm-4">
									<textarea class="form-control" id="area_of_interest" name="area_of_interest" rows="3" placeholder="Enter area of interest">{{old('area_of_interest')}}</textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label"><i class="fa fa-server fa-lg admindashboard-icon"></i> Achievement :</label>
								<div class="col-sm-4">
									<textarea class="form-control" id="achievement" name="achievement" rows="3" placeholder="Enter Achievement">{{old('achievement')}}</textarea>
								</div>
							</div>
					  	</div>
					</div>
				</div>
			</div>
  	  		<div class="text-center">
		  		<button type="submit" class="btn btn-success">Assign New CMS Developer</button>
		  	</div>
		</form>
	</div>
</section>
@endsection