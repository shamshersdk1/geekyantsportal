@extends('layouts.admin-dashboard')
@section('main-content')

<section class="new-project-section">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">CMS Developers</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
			  			<li><a href="{{ url('admin/cms-developers') }}">CMS Developers</a></li>
			  			<li >Edit</li>
						  <li class="active">{{$cmsTechnologyDeveloper->user->name}}</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>

	    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif

		<form class="form-horizontal" method="post" action="/admin/cms-technology-developers/{{$cmsTechnologyDeveloper->id}}" enctype="multipart/form-data">
            <input name="_method" type="hidden" value="PUT" />
			<div class="panel panel-default">
				<div class="panel-body">
				    <div class="row">
				    	<div class="col-md-12">
						  	<div class="form-group">
								<label for="" class="col-sm-2 control-label">Technology*: </label>
								<div class="col-sm-4">
									@if(isset($cmsDevelopers))
										<select id="selectid2" name="cms_technology_id" style="">
											<option value="">Select</option>
											@foreach($cmsDevelopers as $x)
												@if( $cmsTechnologyDeveloper->id == $x->id )
													<option value="{{$x->id}}" selected>{{$x->name}}</option>
												@else
													<option value="{{$x->id}}">{{$x->name}}</option>
												@endif
											@endforeach
										</select>
									@endif
								</div>
							</div>
						  	<div class="form-group">
								<label class="col-sm-2 control-label">Status:</label>
								<div class="col-sm-4">
									@if($cmsTechnologyDeveloper->status  )
									<select class="form-control" name="status">
										<option value="1" selected>Active</option>
										<option value="0" >Inactive</option>
									</select>
                                    @else
                                    <select class="form-control" name="status">
										<option value="1" >Active</option>
										<option value="0" selected>Inactive</option>
									</select>
                                    @endif
								</div>
							</div>
						  	<div class="form-group">
								<label for="" class="col-sm-2 control-label">Upload Image :</label>
								<div class="col-sm-4">    
									<input type="file" id="file" class="form-control" name="file" multiple="false">
								</div>
							</div>
							@if ( !empty($cmsTechnologyDeveloper->image) )
							<div class="form-group">
								<div class="col-sm-2 col-sm-offset-2">
									<img src="/{{ $cmsTechnologyDeveloper->image->path }}" style="height: 100px; width:100px;">
								</div>
							</div>
							@endif
							<div class="form-group">
								<label class="col-sm-2 control-label"><i class="fa fa-graduation-cap fa-lg admindashboard-icon"></i> About :</label>
								<div class="col-sm-4">
									<textarea class="form-control" id="about" name="about" rows="3" placeholder="Enter details">{{ $cmsTechnologyDeveloper->about }}</textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label"><i class="fa fa-shield fa-lg admindashboard-icon"></i> Area of Interest :</label>
								<div class="col-sm-4">
									<textarea class="form-control" id="area_of_interest" name="area_of_interest" rows="3" placeholder="Enter area of interest">{{ $cmsTechnologyDeveloper->area_of_interest }}</textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label"><i class="fa fa-server fa-lg admindashboard-icon"></i> Achievement :</label>
								<div class="col-sm-4">
									<textarea class="form-control" id="achievement" name="achievement" rows="3" placeholder="Enter Achievement">{{ $cmsTechnologyDeveloper->achievement }}</textarea>
								</div>
							</div>
					  	</div>
					</div>
				</div>
			</div>
  	  		<div class="text-center">
		  		<button type="submit" class="btn btn-success">Update Developer</button>
		  	</div>
		</form>
	</div>
</section>
@endsection