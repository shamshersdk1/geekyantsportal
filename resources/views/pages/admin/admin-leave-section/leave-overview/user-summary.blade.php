@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-9">
                <h1 class="admin-page-title">Leaves Overview</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/leave-section/overview') }}">Leaves Overview</a></li>
                </ol>
            </div>
            <div class="col-sm-3">
                <div class="form-group" style="margin: 10px 0 0 0;">
                    <div class="" style="background-color: white;">
                        <select id="selectid2" name="view_type">
                            <option value="team" @if($viewType =='team') {{selected}}@endif>My Team</option>
                            <option value="all" @if($viewType =='all') {{selected}}@endif>All</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="leave-overview" style="position: relative;">
        <div class="row ">
            <div class="col-md-12">
                @if(!empty($errors->all()))
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <span>{{ $error }}</span><br/>
                        @endforeach
                    </div>
                @endif
                @if (session('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ session('message') }}</span><br/>
                    </div>
                @endif   
            </div>
        </div>

        @include('pages.partials.leave-overview')
    </div>	
</div>
<script type="text/javascript">
    $(function(){
        $tableHeight = $( window ).height();

        $("#leave-overview-summary").DataTable( {
            scrollX: true,
            scrollY: $tableHeight - 200,
            scrollCollapse: true,
            paging:  false,
            fixedColumns:   {
                leftColumns: 1
            }
        });
        $("#selectid2").change(function(){
            var date = '<?php echo $_GET["date"];?>';
            var type = '<?php echo $_GET["type"];?>';
            var viewType = $(this).val();
            console.log('viewType '+ viewType);
            
            window.location.href = "/user/leave-overview?type="+type+"&date="+date+"&view_type="+viewType;
        })
    });
</script>
@endsection