@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Leaves Overview</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/leave-section/overview') }}">Leaves Overview</a></li>
                </ol>
            </div>
        </div>
    </div>
        <div class="row leave-overview">
            <div class="col-md-12">
                @if(!empty($errors->all()))
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <span>{{ $error }}</span><br/>
                        @endforeach
                    </div>
                @endif
                @if (session('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ session('message') }}</span><br/>
                    </div>
                @endif   
            </div>
            <div class="col-md-row12 ">
                <div class=" row bg-white search-section-top">
                
                
                    <div class="col-md-4">
                    <label for="start_date">From</label>
                        <div class=' input-group date' id='datetimepickerStart'>
                       
                            <input type='text' id="datetimeinputstart" class="form-control" autocomplete="off" name="start_date"  placeholder="Start Date" required/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-4" id="end_date_div">
                    <label for="end_date">To</label>

                        <div class=' input-group date' id='datetimepickerEnd'>
                            <input type='text' id="datetimeinputend" class="form-control" name="end_date" placeholder="End Date" autocomplete="off"/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                    <a id="go-btn" class="btn btn-primary" style=" margin-top: 20px;">Go</a>

                </div>
            </div>
            <div class="col-md-12 m-t-15">
                <div class="panel panel-default overview-table">
                    <table class="table table-striped">
                        <thead >
                            @for($i=0;$i<count($dates);$i++)
                                <th @if($code[$i]==1) class="holiday" @endif >{{$dates[$i]}}</th>
                            @endfor
                        </thead>
                        <tbody>
                        @if(!empty($dataArray))
                            @foreach($dataArray as $data)
                                <tr>
                                    @for($i=0;$i<count($data['dates']);$i++)
                                        @if($code[$i]==1)
                                            <td class="holiday" width="100px"></td>
                                        @elseif($data['dates'][$i] != '0')
                                            <td style="padding:0"><div class="col-md-3 text-center name-div {{$data['dates'][$i]}}" style="padding:10px 0 0 0">{{$data['dates'][$i]}}</div><div class="col-md-9 name-div" style="padding:0 0 0 5px;"><div><small>{{$data['id']}}</small></div><div>{{$data['name']}}</div></div></td>
                                        @else
                                            <td></td>
                                        @endif
                                    @endfor
                                </tr>
                            @endforeach
                        @else
                            <tr><td colspan='{{count($dates)}}'>No leaves in the given date range</td></tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>		
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#datetimepickerStart').datetimepicker({
            format: 'YYYY-MM-DD'
        });
        $('#datetimepickerEnd').datetimepicker({
            format: 'YYYY-MM-DD'
        });
        $("#datetimepickerStart").on("dp.change", function (e) {
            $('#datetimepickerEnd').data("DateTimePicker").minDate($("#datetimeinputstart").val());
            $("#go-btn").attr('href', '?start_date='+$("#datetimeinputstart").val()+'&end_date='+$("#datetimeinputend").val());
        });
        $("#datetimepickerEnd").on("dp.change", function (e) {
            $("#go-btn").attr('href', '?start_date='+$("#datetimeinputstart").val()+'&end_date='+$("#datetimeinputend").val());
        });
        @if(Request::get('start_date') && Request::get('end_date'))
            $("#datetimeinputstart").val('{{Request::get('start_date')}}');
            $("#datetimeinputend").val('{{Request::get('end_date')}}');
        @else
            $("#datetimeinputstart").val('{{date("Y-m-d")}}');
            $("#datetimeinputend").val('{{date("Y-m-d", strtotime(date("Y-m-d") . " +30 days"))}}');
        @endif
    });
</script>
@endsection