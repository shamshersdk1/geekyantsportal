@extends('layouts.admin-dashboard')
    @section('main-content')
    <div class="container-fluid">
        <div class="breadcrumb-wrap">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="admin-page-title">Leave Report Dashboard</h1>
                    <ol class="breadcrumb">
                        <li><a href="/admin">Admin</a></li>
                        <li class="active">Leave Report dashboard</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- ============================= admin leave Board ================================ -->
        <div class="row">
            <div class="col-md-12">
                @if(!empty($errors->all()))
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <span>{{ $error }}</span><br/>
                          @endforeach
                    </div>
                @endif
                @if (session('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ session('message') }}</span><br/>
                    </div>
                @endif
            </div>
            <canvas id="graph" width="1000px" height="600px"></canvas>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script>
    <script type="text/javascript">
    var context=document.getElementById('graph').getContext('2d');
    var data={
            labels: ['Jan','Feb','Mar'],
            datasets: [{
                label: "Total Working Days",
                data: [22,19,21],
                fillColor: "transparent",
                strokeColor: "#00F",
            },
            {
                label: "Total Paid Leaves Taken",
                data:[118,100,250],
                fillColor: "transparent",
                strokeColor: "#0F0",
            },
            {
                label: "Total Sick Leaves Taken",
                data:[30,17,43],
                fillColor: "transparent",
                strokeColor: "#F00",
            },
            {
                label: "Total Unpaid Leaves Taken",
                data:[1,3,5],
                fillColor: "transparent",
                strokeColor: "#FF0",
            }]
        };
    new Chart(context).Line(data);
    </script>
@endsection
