@extends('layouts.admin-dashboard')
    @section('main-content')
    <div class="container-fluid">
        <div class="breadcrumb-wrap">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="admin-page-title">Leave Dashboard</h1>
                    <ol class="breadcrumb">
                        <li><a href="/admin">Admin</a></li>
                        <li class="active">Leave MIS REPORT</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- ============================= admin leave Board ================================ -->
        <div class="row">
            <div class="col-md-12">
                @if(!empty($errors->all()))
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <span>{{ $error }}</span><br/>
                          @endforeach
                    </div>
                @endif
                @if (session('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ session('message') }}</span><br/>
                    </div>
                @endif
            </div>
            <div class="col-md-6" >
                @if(isset($month) && isset($year))
                    <h4>MIS Report For : {{date('F', mktime(0, 0, 0, $month, 10))}}, {{$year}}</h4>
                @endif
            </div>
            <div class="col-md-6" >
                <!-- ======================== Date Picker ======================= -->
                <div class="col-md-12 no-padding ma-bot-10">
                    <form action="/admin/mis-leave-report" method="get">
                        <div class="row">
                            <div class="col-md-3">
                                <input type='text' class="form-control" id="yearPicker" name="year" value={{isset($year) ? $year : date('Y', strtotime('now'))}} placeholder="Current Year" />
                            </div>
                            <div class="form-group">
                                <div class="col-md-6">
                                    <div class='input-group date'>
                                        <input type='text' class="form-control" id='monthPicker' name="month" value={{isset($month) ? $month : date('m', strtotime('now'))}} placeholder="Select Month" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <button class="btn btn-success crud-btn" type="submit">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @if(isset($daysList) && isset($misReportList))
        <div class="container" style="margin-top: 10px;">
            @include('pages/admin/admin-leave-section/leave-mis-table')
        </div>
    @endif
    <style>
    .ui-datepicker-calendar {
        display: none;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#monthPicker').datepicker( {
                changeMonth: true,
                // changeYear: true,
                showButtonPanel: true,
                dateFormat: 'mm',
                onClose: function(dateText, inst) {
                    $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
                }
            });
        })
    </script>

@endsection