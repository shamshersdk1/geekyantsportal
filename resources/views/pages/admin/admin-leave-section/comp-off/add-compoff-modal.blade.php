
<div class="modal fade" id="addCompoffModal" tabindex="-1" role="dialog" aria-labelledby="addCompoffModalLabel">
    <form class="addForm" action="/admin/leave-section/compoff" method="POST">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="addCompoffModalLabel">Apply Leave</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6 ma-bot-10">
                            <div class="form-group ma-bot-10">
                                <label for="name">Name</label>
                                <input type='text' class="form-control searchusermodal" name="name"  placeholder="User" required />
                            </div>
                        </div>
                        <div class="col-md-6 ma-bot-10">
                            <div class="form-group ma-bot-10">
                                <label for="days">Compensation in Days</label>
                                <input type='text' class="form-control" name="days"  placeholder="No. of Days" required />
                            </div>
                        </div>
                        <!-- ====================== Notes ================ -->
                        <div class="col-md-12 ma-top-10">
                            <div class="form-group">
                                <label for="">Notes</label>
                                <textarea class="form-control" rows="5" placeholder="Please enter reason for applying leave" name="note"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>&nbsp;
                    <span class="pull-right">
                        <button type="submit" class="btn btn-primary">
                            Apply
                        </button>
                    </span>
                </div>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $( ".searchusermodal" ).autocomplete({
            source: {{$jsonuser}}
        });
        $( ".searchusermodal" ).autocomplete("option", "appendTo", ".addForm" );
    })
</script>