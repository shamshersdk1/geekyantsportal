@extends('layouts.admin-dashboard')
    @section('main-content')
    <div class="container-fluid">
        <div class="breadcrumb-wrap">
            <div class="row">
                <div class="col-sm-12">
                    @if(!empty($title))
                    <h1 class="admin-page-title">{{$title}}</h1>
                    @else
                    <h1 class="admin-page-title">Leave Dashboard</h1>
                    @endif
                    <ol class="breadcrumb">
                        <li><a href="/admin">Admin</a></li>
                        <li class="active">Leave dashboard</li>
                    </ol>
                </div>
                <!-- <div class="col-sm-4 text-right m-t-10">
                    <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
                </div> -->
            </div>
        </div>
        <!-- ============================= admin leave Board ================================ -->
        <div class="row">
            <div class="col-md-12">
                @if(!empty($errors->all()))
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <span>{{ $error }}</span><br/>
                          @endforeach
                    </div>
                @endif
                @if (session('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ session('message') }}</span><br/>
                    </div>
                @endif
            </div>
            <div class="col-md-12 ">
              <div class="row bg-white search-section-top">
                <form action="/admin/leave-section/confirmation" class="col-md-10" method="get">
                    <div class="input-group row">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <input type="text" id="su" name="searchuser" placeholder="User's Name" value="<?=( isset( $_GET['searchuser'] ) ? $_GET['searchuser'] : '' )?>"   class="form-control searchuser" >
                                </div>
                                <div class="col-md-3">
                                    <div class='input-group date' id='datetimepickerStart_search'>
                                        <input type='text' id="sd" class="form-control" value="<?=( isset( $_GET['start_date'] ) ? $_GET['start_date'] : '' )?>" autocomplete="off" name="start_date" placeholder="Start Date" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class='input-group date' id='datetimepickerEnd_search'>
                                        <input type='text' id="ed" class="form-control" name="end_date" value="<?=( isset( $_GET['end_date'] ) ? $_GET['end_date'] : '' )?>" autocomplete="off" placeholder="End Date" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn btn-primary btn-sm button-space">Search</button>
                                    </span>
                                    <span class="input-group-btn">
                                        <button id="btnReset" class="btn btn-primary btn-sm button-space">Reset</button>
                                    </span>
                                </div>  
                            </div>
                        </div>    
                    </div>
                </form>
                <div class="col-md-2 pull-right">
                <button class="btn btn-info" type="button" data-toggle="modal" data-target="#addCompoffModal">Apply New Compoff</button>
            </div>
            </div>
            </div>
           
            <div class="col-md-12 user-list-view">
                <h4>List of Pending Leaves.</h4>
                <div class="panel panel-default">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="20%">Name</th>
                                    <th width="15%">Type</th>
                                    <th width="10%">From Date</th>
                                    <th width="10%">To Date</th>
                                    <th width="10%">Working Days</th>
                                    <th width="10%">Applied On</th>
                                    <th width="10%">Status</th>
                                    <th width="20%" class="text-right">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($pendingLeaves))
                                    @if(count($pendingLeaves) > 0)
                                        @foreach($pendingLeaves as $key => $leave)
                                            @if(isset($leave->user))
                                                <tr>
                                                    <td>{{$leave->id}}</td>
                                                    <td>{{$leave->user->name}}</td>
                                                    <td>
                                                        @if($leave->type=='half')
                                                             {{ ucfirst($leave->type)}} Day Leave

                                                        @else
                                                             {{ ucfirst($leave->type)}} Leave
                                                        @endif
                                                    </td>
                                                    <td>{{date("d M Y", strtotime($leave->start_date))}}</td>
                                                    <td>{{date("d M Y", strtotime($leave->end_date))}}</td>
                                                    @if($leave->days==0.5)
                                                    <td>{{ucfirst($leave->half)}} half</td>
                                                    @else
                                                    <td>{{floor($leave->days)}}</td>
                                                    @endif
                                                    <td>{{date("d M Y - h:i a", strtotime($leave->created_at))}}</td>
                                                    <td >
                                                        @if($leave->status == "approved")
                                                            <span class="label label-success custom-label">Approved</span>
                                                        @elseif($leave->status == "pending")
                                                            <span class="label label-info custom-label">Pending</span>
                                                        @elseif($leave->status == "rejected")
                                                            <span class="label label-danger custom-label">Rejected</span>
                                                        @elseif($leave->status == "cancelled")
                                                            <span class="label label-primary custom-label">Cancelled</span>
                                                        @else
                                                            <span class="label label-info custom-label">No Status</span>
                                                        @endif
                                                    </td>
                                                    <td class="text-right">
                                                        <button class="btn btn-primary btn-sm" type="button"
                                                            data-toggle="modal" data-target="#editLeaveModal"
                                                            data-id="{{$leave->id}}" data-title="Algorithms"><i class="fa fa-eye"></i> View</button>
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="8">No Record found.</td>
                                        </tr>
                                    @endif
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('pages/admin/admin-leave-section/comp-off/add-compoff-modal')

<script>
    $(function(){
        $( ".searchuser" ).autocomplete({
            source: {{$jsonuser}}
        });
        $('#datetimepickerStart_search').datetimepicker({
            minDate: new Date(new Date().getFullYear()+"-01-01"), //Set min date to April 1st of current year
            //Set max date to upcoming march
            maxDate: new Date().getMonth() <= 2 ? new Date(new Date().getFullYear()+"-03-31") : new Date(new Date().getFullYear()+1+"-03-31"),
            format: 'YYYY-MM-DD'
        });

        $('#datetimepickerEnd_search').datetimepicker({
            //Set max date to upcoming march
            maxDate: new Date().getMonth() <= 2 ? new Date(new Date().getFullYear()+"-03-31") : new Date(new Date().getFullYear()+1+"-03-31"),
            format: 'YYYY-MM-DD'
        });

        $("#datetimepickerStart_search").on("dp.change", function (e) {
            $('#datetimepickerEnd_search').data("DateTimePicker").minDate(e.date);
        });

        $("#btnReset").click(function(){
           location.reload();
    }); 
    });
</script>

@endsection