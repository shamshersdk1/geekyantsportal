<script type="text/javascript">
    let leaveInfo;
    
    function updateLeave() {
        leaveInfo.type = $('select[name="type"]').val();
        leaveInfo.start_date=$('#editLeaveModal input[name="start_date"]').val();
        leaveInfo.end_date=$('#editLeaveModal input[name="end_date"]').val();
        leaveInfo.rejectreason=$('#rejectreason').val();
        if(leaveInfo.type=='half')
        {
            leaveInfo.half=$('#editLeaveModal select[name="half"]').val();
        }
        $.ajax({
            type: 'PUT',
            url: '/admin/leave-section/update/'+leaveInfo.id,
            dataType: 'text',
            data: leaveInfo,
            beforeSend: function() {
                $("div.loading").show();
                $("div.content").hide();
            },
            complete: function() {
                $("div.loading").hide();
                $("div.content").show();
            },
            success: function(data) {
                $('#editLeaveModal').modal('hide');
                window.location.reload();
                //call update client
            },
            error: function(errorResponse){
                $('#editLeaveModal').modal('hide');
                // console.log(errorResponse);
                window.location.reload();
            }
        });
    }
    function rejectLeave() {
        if($('#rejectreason').val()!="")
        {
            leaveInfo.rejectreason=$('#rejectreason').val();
            $.ajax({
                type: 'PUT',
                url: '/admin/leave-section/reject/'+leaveInfo.id,
                dataType: 'text',
                data: leaveInfo,
                beforeSend: function() {
                    $("div.loading").show();
                    $("div.content").hide();
                },
                complete: function() {
                    $("div.loading").hide();
                    $("div.content").show();
                },
                success: function(data) {
                    $('#rejectLeaveModal').modal('hide');
                    window.location.reload();
                },
                error: function(errorResponse){
                    $('#rejectLeaveModal').modal('hide');
                    // console.log(errorResponse);
                    window.location.reload();
                }
            });
        }
        else
        alert("You must specify the reason for rejecting the leave");
        
    }

    $(function() {

        $('.approve-cancel-Form').submit(function(){
            return confirm('Are you sure?');
        });

        $('.rejectform').submit(function(){
            event.preventDefault();
        });


        // Reset Add Form
        $("#addLeaveModal").on("show.bs.modal", function(){
            $(this).find("input,textarea,select")
                .val('')
                .end()
        });

        $("#addLeaveModal").on("hidden.bs.modal", function(){
            $(this).find("input,textarea,select")
                .val('')
                .end()
        });

        //Reset Edit Form
        $("#editLeaveModal").on("hidden.bs.modal", function(){
            $(this).find("input,textarea,select")
                .val('')
                .end()
        });

        $('#editLeaveModal').on("show.bs.modal", function (e) {
            let id = $(e.relatedTarget).data('id')
            $.ajax({
                type: 'GET',
                url: '/admin/leave-status/'+id,
                beforeSend: function() {
                    $("div.loading").show();
                    $("div.content").hide();
                },
                complete: function() {
                    $("div.loading").hide();
                    $("div.content").show();
                },
                success: function(data) {
                    leaveInfo = data;
                    if(data.days > 1) {
                        var duration = Math.trunc(data.days) + " days";
                    } else {
                        if(data.days < 1) {
                            var duration = "Half day";
                        } else {
                            var duration = Math.trunc(data.days) + " day";
                        }
                    }
                    $("#editLeaveModal .team-leads-info").remove();
                    if(data.managerInfo.length) {
                        $html = '<label class="team-leads-info" style="margin-left:15px">Team Leads Status1</label>';
                        data.managerInfo.forEach(function (info){
                            $html = $html+'<li style="display:block;margin:0px 15px 0px 15px;border-radius:2px;padding-bottom:40px" class="team-leads-info list-group-item"> ';
                            $html= $html+"<div class='col-sm-9' style='padding:0px'>"+info.name;
                            $projects = " - ";
                            if(info.projects) {
                                info.projects.forEach(function (proj_name){
                                    $projects = $projects+proj_name+", ";
                                });
                            }
                            if($projects.length > 0) {
                                $projects = $projects.slice(0, -2);
                                $html = $html+$projects+"</div>";
                            }
                            $html = $html+'<div class="col-sm-3" style="float:right;padding:0px"><span style="display:block" class="label custom-label ';
                            switch(info.status) {
                                case 'approved' :
                                $html = $html+'label-success ">';
                                break;
                                case 'pending' :
                                $html = $html+'label-info ">';
                                break;
                                case 'cancelled' :
                                $html = $html+'label-primary ">';
                                break;
                                case 'rejected' :
                                $html = $html+'label-danger ">';
                                break;
                                default :
                                $html = $html+'label-default ">';
                            }
                            $html = $html+info.status.charAt(0).toUpperCase()+info.status.slice(1)+'</span>';
                            $html = $html+'<small style="display:block;text-align:center;padding-top:1%">';
                            if(info.status != 'pending') {
                                $html = $html+'on '+info.date+'</small></span></div><span style="display:block">';
                            }      
                            $html = $html+'</li>';
                        });
                        $html = $html+'<br class="team-leads-info">';
                        $('#editLeaveModal .modal-footer').before($html);
                    }

                    $('#editLeaveModal select[name="type"]').val(data.type);
                    $('#editLeaveModal input[name="start_date"]').val(data.start_date);
                    $('#editLeaveModal input[name="end_date"]').val(data.end_date);
                    $('#editLeaveModal textarea[name="reason"]').val(data.reason);
                    $('#editLeaveModal #duration-label').text(duration);
                    $('#editLeaveModal span.status').text(data.status.charAt(0).toUpperCase()+data.status.slice(1));
                    $('#editLeaveModal span#user_name').text(data.user.name);
                    $('#editLeaveModal span#sick_leaves').text(data.sickLeaves);
                    $('#editLeaveModal span#paid_leaves').text(data.paidLeaves);
                    $('#editLeaveModal button[name="approve"]').show();
                    $('#editLeaveModal button[name="reject"]').show();
                    $('#editLeaveModal button[name="cancel"]').show();
                    $('#editLeaveModal #rejectreason').parent().show();
                    $('#editLeaveModal #rejectreason').attr('disabled',false);
                    $('#editLeaveModal input[name="client"]').parent().hide();
                    $('#editLeaveModal span.status').removeClass("label-success");
                    $('#editLeaveModal span.status').removeClass("label-danger");
                    $('#editLeaveModal span.status').removeClass("label-primary");
                    $('#editLeaveModal span.status').removeClass("label-info");

                    switch(data.status){
                        case 'approved':
                            $('#editLeaveModal button[name="approve"]').hide();
                            $('#editLeaveModal button[name="reject"]').hide();
                            $('#editLeaveModal #rejectreason').parent().hide();
                            $('#editLeaveModal span.status').addClass("label-success");
                            break;
                        case 'rejected':
                            $('#editLeaveModal button[name="approve"]').hide();
                            $('#editLeaveModal button[name="reject"]').hide();
                            $('#editLeaveModal button[name="cancel"]').hide();
                            $('#editLeaveModal #rejectreason').val(data.cancellation_reason);
                            $('#editLeaveModal #rejectreason').attr('disabled',true);
                            $('#editLeaveModal span.status').addClass("label-danger");
                            break;
                        case 'cancelled':
                            $('#editLeaveModal button[name="approve"]').hide();
                            $('#editLeaveModal button[name="reject"]').hide();
                            $('#editLeaveModal button[name="cancel"]').hide();
                            $('#editLeaveModal #rejectreason').parent().hide();
                            $('#editLeaveModal span.status').addClass("label-primary");
                            break;
                        default:
                            $('#editLeaveModal input[name="client"]').parent().show();
                            $('#editLeaveModal span.status').addClass("label-info");
                            break;
                    }
                    $('#editLeaveModal .approve-cancel-Form').attr('action','/admin/leave-section/status/'+data.id);
                    if(data.type=='half')
                    {
                        $('#end_date_div_edit').hide();
                        $('#half_day_div_edit').show();
                        $('#datetimeinputstart_edit').attr('placeholder','Date');
                        $('#editLeaveModal select[name="half"]').val(data.half);
                    }
                }
            });
        });

        //Reset reject form
        $("#rejectLeaveModal").on("hidden.bs.modal", function(){
            $(this).find("textarea")
                .val('')
                .end()
        });

        $('#rejectLeaveModal').on("show.bs.modal", function (e) {
            let id = $(e.relatedTarget).data('id');
            $.ajax({
                type: 'GET',
                url: '/admin/leave-section/'+id,
                beforeSend: function() {
                    $("div.loading").show();
                    $("div.content").hide();
                },
                complete: function() {
                    $("div.loading").hide();
                    $("div.content").show();
                },
                success: function(data) {
                    leaveInfo = data;
                    $('#seltype option[value='+data.type+']').attr("selected",true);
                    $('#rejectLeaveModal input[name="start_date"]').val(data.start_date);
                    $('#rejectLeaveModal input[name="end_date"]').val(data.end_date);
                    $('#rejectLeaveModal textarea[name="reason"]').val(data.reason);
                    $('#rejectLeaveModal span.status').text(data.status.toUpperCase());
                    $('#rejectLeaveModal textarea[name="rejectreason"]').val(data.rejectreason);
                    $('#rejectLeaveModal span#user_name').text(data.user.name);
                }
            });
        });
    });
</script>