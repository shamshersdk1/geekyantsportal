@extends('layouts.admin-dashboard')
    @section('main-content')
    <div class="container-fluid">
        <div class="breadcrumb-wrap">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="admin-page-title">Leave Dashboard</h1>
                    <ol class="breadcrumb">
                        <li><a href="/admin">Admin</a></li>
                        <li class="active">Employee Leave dashboard</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- ============================= admin leave Board ================================ -->
        <div class="row">
            <div class="col-md-12">
                @if(!empty($errors->all()))
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <span>{{ $error }}</span><br/>
                          @endforeach
                    </div>
                @endif
                @if (session('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ session('message') }}</span><br/>
                    </div>
                @endif
            </div>

            <div class="col-md-12 user-list-view">
                <h4>List of Leaves</h4>
                <div class="panel panel-default">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="10%">Name</th>
                                    <th width="10%">Total Sick Leave</th>
                                    <th width="10%">Consumed Sick Leave</th>
                                    <th width="10%">Total Paid Leave</th>
                                    <th width="10%">Consumed Paid Leave</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($userList))
                                    @if(count($userList) > 0)
                                        @foreach($userList as $index => $user)
                                            <tr>
                                                <td>{{$index}}</td>
                                                <td>{{$user['name']}}</td>
                                                <td>{{$user['sickLeave']['total']}}</td>
                                                <td>{{$user['sickLeave']['consumed']}}</td>
                                                <td>{{$user['paidLeave']['total']}}</td>
                                                <td>{{$user['paidLeave']['consumed']}}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="8">No Record found.</td>
                                        </tr>
                                    @endif
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="pull-right">
                    {{ str_replace('/?', '?', $userList->render()) }}
                </div>
            </div>
        </div>
    </div>
@endsection