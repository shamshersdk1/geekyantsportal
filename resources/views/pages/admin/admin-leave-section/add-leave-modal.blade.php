
<div class="modal fade" id="addLeaveModal" tabindex="-1" role="dialog" aria-labelledby="addLeaveModalLabel">
    <form class="addForm" action="/admin/leave-section" method="POST">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="addLeaveModalLabel">Apply Leave</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6 ma-bot-10">
                            <div class="form-group ma-bot-10">
                                <label for="employee">Name</label>
                                <input type='text' class="form-control searchusermodal" name="employee"  placeholder="User" required />
                            </div>
                        </div>
                        <div class="col-md-6 ma-bot-10">
                            <div class="form-group ma-bot-10">
                                <label for="">Select Leave Type</label>
                                <select id="leave_type" class="form-control" name="type" required>
                                    <option value="" selected>Select Leave type</option>
                                    <option value="sick" >Sick Leave</option>
                                    <option value="unpaid" >Unpaid Leave</option>
                                    <option value="paid" >Paid Leave</option>
                                    <option value="half" >Half-day Leave</option>
                                    <option value="compoff" >Comp-Off</option>
                                </select>
                            </div>
                        </div>
                        <!-- ======================== Date Picker ======================= -->
                        
                        
                        <div class="col-md-12 no-padding ma-bot-10">
                            <div class="form-group">
                                <div class="col-md-6">
                                    <label for="start_date">From</label>
                                    <div class='input-group date' id='adddatetimepickerStart'>
                                        <input type='text' id="datetimeinputstart" class="form-control" autocomplete="off" name="start_date"  placeholder="Start Date" required/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                
                                <div class="col-md-6" id="end_date_div">
                                    <label for="end_date">To</label>
                                    <div class='input-group date' id='adddatetimepickerEnd'>
                                        <input type='text' id="datetimeinputend" class="form-control" name="end_date" placeholder="End Date" autocomplete="off"/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-6" id="half_day_div">
                                    <div class="form-group">
                                        <label for="half">Select Half</label>
                                        <select class="form-control" name="half">
                                            <!-- <option value="0"> Select the half</option> -->
                                            <option value="first" id="first_half" selected>First Half</option>
                                            <option value="second">Second Half</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                           
                        <div class="col-md-6 ma-bot-10">
                            <div class="form-group ma-bot-10" style="margin-top:15px;">
                                <label for="">Status</label>
                                <select id="status" class="form-control" name="status" required>
                                    <option value="approved" selected>Approved</option>
                                    <option value="pending" >Pending</option>
                                </select>
                            </div>
                        </div>
                            <!-- ====================== reasones ================ -->
                        
                        <div class="col-md-12 ma-top-10">
                            <div class="form-group">
                                <label for="">Notes</label>
                                <textarea class="form-control" rows="5" placeholder="Please enter reason for applying leave" name="reason"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" style="margin-top:10px;" class="btn btn-default" data-dismiss="modal">Close</button>&nbsp;
                    <span class="pull-right">
                        <button type="submit" style="margin-top:10px;" class="btn btn-primary">
                            Apply
                        </button>
                    </span>
                </div>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $(".addForm").submit(function(){
            if($('#leave_type').val()=='half'){
                $("#datetimeinputend").val($("#datetimeinputstart").val());
            }
        });
        $('#adddatetimepickerStart').datetimepicker({
            minDate: new Date().getMonth() <= 3 ? new Date(new Date().getFullYear()-1+"-01-01") : new Date(new Date().getFullYear()+"-01-01"), //Set min date to April 1
            //Set max date to upcoming march
            maxDate: new Date().getMonth() <= 2 ? new Date(new Date().getFullYear()+"-12-31") : new Date(new Date().getFullYear()+1+"-12-31"),
            format: 'YYYY-MM-DD'
        });

        $('#adddatetimepickerEnd').datetimepicker({
            //Set max date to upcoming march
            maxDate: new Date().getMonth() <= 2 ? new Date(new Date().getFullYear()+"-12-31") : new Date(new Date().getFullYear()+1+"-12-31"),
            format: 'YYYY-MM-DD'
        });
        if($("#datetimeinputstart").val()!="")
        {
            $('#datetimepickerEnd').data("DateTimePicker").minDate($("#datetimeinputstart").val());
        }
        $("#adddatetimepickerStart").on("dp.change", function (e) {
            $('#adddatetimepickerEnd').data("DateTimePicker").minDate($("#datetimeinputstart").val());
        });

        $( ".searchusermodal" ).autocomplete({
            source: {{$jsonuser}}
        });
        $( ".searchusermodal" ).autocomplete("option", "appendTo", ".addForm" );
        $(document).on('change','#leave_type',function(e){
            if(e.target.value=="half")
            {
                $('#end_date_div').hide();
                $('#half_day_div').show();
                $('#datetimeinputstart').attr('placeholder','Date');
                $("#datetimeinputstart").attr('required',false);
                $("select[name='half']").attr('required',true);
            }
            else{
                $('#end_date_div').show();
                $('#half_day_div').hide();
                $('#datetimeselectstart').attr('placeholder','Start date');
                $("#datetimeinputstart").attr('required',true);
                $("select[name='half']").attr('required',false);
            }
        });
        $('#half_day_div').hide();
    })
</script>