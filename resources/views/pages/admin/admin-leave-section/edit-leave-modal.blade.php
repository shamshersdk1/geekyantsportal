
<div class="modal fade" id="editLeaveModal" tabindex="-1" role="dialog" aria-labelledby="editLeaveModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="editLeaveModal">Edit Leave applied by <span id="user_name"></span></h4>
            </div>
            <div class="modal-body loading" style="min-height: 543px;">
                <div class="col-md-2 col-md-offset-5">
                    <i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
                </div>
            </div>
            <div class="modal-body content">
                <div class="row">
                    <div class="col-md-12 ma-bot-10">
                        <div class="row">
                        <div class="col-md-6">
                            <div class="label label-warning custom-label">
                                Sick Leaves:&nbsp&nbsp<span style="font-weight: bold" id="sick_leaves"></span>
                            </div>
                            <div class="label label-primary custom-label">
                                Paid Leaves:&nbsp&nbsp<span style="font-weight: bold" id="paid_leaves"></span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <span style="margin-bottom:10px" class="status label custom-label pull-right"></span><label class="pull-right" for="">Status:&nbsp</label>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-6 ma-bot-10">
                        <div class="form-group ma-bot-10">
                            <label class="control-label" for="type">Leave Type</label>
                                <select class="form-control" style="width:100%" id="leave_type_edit" name="type">
                                    <option>Select Leave type</option>
                                    <option value="sick" >Sick Leave</option>
                                    <option value="unpaid" >Unpaid Leave</option>
                                    <option value="paid" >Paid Leave</option>
                                    <option value="half" >Half-day Leave</option>
                                    <option value="compoff" >Comp-Off</option>
                                </select>
                        </div>
                    </div>
                    <div class="col-md-6 ma-bot-10">
                        <label>Duration</label>
                        <br>
                        <label id="duration-label" style="padding-top:10px"></label>
                    </div>
                    <!-- ======================== Date Picker ======================= -->
                    <div class="col-md-12 no-padding ma-bot-10">
                            <div class="form-group">
                                <div class="col-md-6">
                                    <label for="start_date">From</label>
                                    <div class='input-group date' id='datetimepickerStart_edit'>
                                        <input type='text' class="form-control" autocomplete="off" name="start_date" placeholder="Start Date" required/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-6" id="end_date_div_edit">
                                    <label for="end_date">To</label>
                                    <div class='input-group date' id='datetimepickerEnd_edit'>
                                        <input type='text' class="form-control" name="end_date" placeholder="End Date" autocomplete="off"  required/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-6" id="half_day_div_edit">
                                    <div class="form-group">
                                        <label for="half">Select Half</label>
                                        <select class="form-control" name="half">
                                            <option value="first">First Half</option>
                                            <option value="second">Second Half</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <!-- ====================== reasones ================ -->
                <form class="approve-cancel-Form" method="post" style="display:inline">
                    <div class="col-md-12 ma-top-10">
                        <div class="form-group">
                            <label for="">Reason</label>
                            <textarea class="form-control" rows="5" placeholder="Please enter reason for applying leave" name="reason" disabled></textarea>
                        </div>
                    </div>
                    <div class="col-md-12 ma-top-10">
                        <div class="form-group">
                            <label for="client">Client to be informed <small>(Optional)</small></label>
                            <input class="form-control searchclient" placeholder="Select the client" name="client"></input>
                            <label><small id="clientemail" style="padding-left:10px"></small></label>
                        </div>
                    </div>
                    <div class="col-md-12 ma-top-10">
                        <div class="form-group">
                            <label for="">Reason for rejection</label>
                            <textarea class="form-control" rows="5" placeholder="Please enter reason for rejecting leave" name="rejectreason" id="rejectreason"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>&nbsp;
                
                    <button type="button" class="btn btn-info" onclick="updateLeave()">
                        Update
                    </button>
                
           
                    <button type="button" name="reject"  class="btn btn-warning" onclick=rejectLeave()>
                    Reject
                    </button>
            
                    <button class="btn btn-success" type="submit" name="approve" value="approve">Approve</button>
                    <button class="btn btn-danger"  type="submit" name="cancel" value="cancel">Cancel</button>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#datetimepickerStart_edit').datetimepicker({
            minDate: new Date().getMonth() <= 3 ? new Date(new Date().getFullYear()-1+"-04-01") : new Date(new Date().getFullYear()+"-04-01"), //Set min date to April 1
            //Set max date to upcoming march
            maxDate: new Date().getMonth() <= 2 ? new Date(new Date().getFullYear()+"-03-31") : new Date(new Date().getFullYear()+1+"-03-31"),
            format: 'YYYY-MM-DD'
        });

        $('#datetimepickerEnd_edit').datetimepicker({
            //Set max date to upcoming march
            maxDate: new Date().getMonth() <= 2 ? new Date(new Date().getFullYear()+"-03-31") : new Date(new Date().getFullYear()+1+"-03-31"),
            format: 'YYYY-MM-DD'
        });

        $("#datetimepickerStart_edit").on("dp.change", function (e) {
            $('#datetimepickerEnd_edit').data("DateTimePicker").minDate(e.date);
        });
        $(document).on('change','#leave_type_edit',function(e){
            if(e.target.value=="half")
            {
                $('#end_date_div_edit').hide();
                $('#half_day_div_edit').show();
                $('#datetimeinputstart_edit').attr('placeholder','Date');
            }
            else{
                $('#end_date_div_edit').show();
                $('#half_day_div_edit').hide();
                $('#datetimeinputstart_edit').attr('placeholder','Start date');
            }
        });
        if($('#leave_type_edit').val()!='half')
        {$('#half_day_div_edit').hide();}
        else{
            $('#end_date_div_edit').hide();
        }
        $( ".searchclient" ).autocomplete({
            source: {{$jsonclients}},
            select: function(event, ui) {
            event.preventDefault();
                $(".searchclient").val(ui.item.label);
                $("#clientemail").text(ui.item.value);
            },
            focus: function(event, ui) {
                event.preventDefault();
                $(".searchclient").val(ui.item.label);
            }
        });
        $( ".searchclient" ).autocomplete("option", "appendTo", "#editLeaveModal" );
    });
</script>