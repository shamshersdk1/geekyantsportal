@extends('layouts.admin-dashboard')
    @section('main-content')
    <div class="container-fluid">
        <div class="breadcrumb-wrap">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="admin-page-title">Leave Report Dashboard</h1>
                    <ol class="breadcrumb">
                        <li><a href="/admin">Admin</a></li>
                        <li class="active">Leave Report dashboard</li>
                    </ol>
                    <a href="/admin/leave-section/employee-leave-report" class="btn btn-primary crud-btn pull-right">Employee Leaves</a>
                </div>
            </div>
        </div>
        <!-- ============================= admin leave Board ================================ -->
        <div class="row">
            <div class="col-md-12">
                @if(!empty($errors->all()))
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <span>{{ $error }}</span><br/>
                          @endforeach
                    </div>
                @endif
                @if (session('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ session('message') }}</span><br/>
                    </div>
                @endif
            </div>
            <div class="col-md-12 ma-bot-10">
                <div class="panel panel-default select-leave-type">
                    <form action="/admin/leave-report" method="get">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <select class="js-example-basic-multiple22 form-control" name="user">
                                        @if(isset($allUsers))
                                                <option value ="">Select</option>
                                            @foreach($allUsers as $user)
                                                @if(isset($searchUserId))
                                                    @if($user->id == $searchUserId)
                                                        <option value= "{{$user->id }}" selected= "selected">{{$user->name}}</option>
                                                    @else
                                                        <option value= "{{$user->id }}">{{$user->name}}</option>
                                                    @endif
                                                @else
                                                    <option value= "{{$user->id }}">{{$user->name}}</option>
                                                @endif
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <!-- ======================== Date Picker ======================= -->
                                <div class="col-md-12 no-padding ma-bot-10">
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <div class='input-group date' id='datetimepickerStart'>
                                                <input type='text' class="form-control" value="{{$startDate}}" name="start_date" placeholder="From Date" />
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class='input-group date' id='datetimepickerEnd'>
                                                <input type='text' class="form-control" name="end_date" placeholder="To Date" value="{{$endDate}}"/>
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 pull-right">
                                <button class="btn btn-success crud-btn" type="submit">Apply Filter</button>
                                <a href="/admin/leave-report" class="btn btn-danger crud-btn reset">Clear</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="col-md-12">
                <h4>List of Leaves</h4>
                <div class="panel panel-default">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="10%">Name</th>
                                    <th width="30%">Type</th>
                                    <th width="10%">From Date</th>
                                    <th width="10%">To Date</th>
                                    <th width="10%">Working Days</th>
                                    <th width="15%">Status</th>
                                    <!--<th width="15%" class="text-right">Actions</th>-->
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($leaveList))
                                    @if(count($leaveList) > 0)
                                        @foreach($leaveList as $leave)
                                            @if(isset($leave->user))
                                                <tr>
                                                    <td>{{$leave->id}}</td>
                                                    <td>{{$leave->user->name}}</td>
                                                    <td>
                                                        @if($leave->type)
                                                             {{ ucfirst(trans($leave->type))}} Leave
                                                        @endif
                                                    </td>
                                                    <td>{{date("d M Y", strtotime($leave->start_date))}}</td>
                                                    <td>{{date("d M Y", strtotime($leave->end_date))}}</td>
                                                    <td>{{$leave->days}}</td>
                                                    <td >
                                                        @if($leave->status == "approved")
                                                            <span class="label label-success custom-label">Approved</span>
                                                        @elseif($leave->status == "pending")
                                                            <span class="label label-info custom-label">Pending</span>
                                                        @elseif($leave->status == "rejected")
                                                            <span class="label label-danger custom-label">Rejected</span>
                                                        @elseif($leave->status == "cancelled")
                                                            <span class="label label-primary custom-label">Cancelled</span>
                                                        @else
                                                            <span class="label label-info custom-label">No Status</span>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="8">No Record found.</td>
                                        </tr>
                                    @endif
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="pull-right">
                    {{ $leaveList->appends(Input::except('page'))->links() }}
                </div>
            </div>
            <div class="col-md-2 pull-right">
                <a href="/admin/leave-section/report-csv/?user={{ app('request')->input('user') }}&start_date={{ app('request')->input('start_date') }}&end_date={{ app('request')->input('end_date') }}" class="btn btn-info crud-btn">Export as CSV</a>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#datetimepickerStart').datetimepicker({
                format: 'YYYY-MM-DD'
            });

            $('#datetimepickerEnd').datetimepicker({
                format: 'YYYY-MM-DD'
            });

            $("#datetimepickerStart").on("dp.change", function (e) {
                $('#datetimepickerEnd').data("DateTimePicker").minDate(e.date);
            });
        })
    </script>
@endsection
