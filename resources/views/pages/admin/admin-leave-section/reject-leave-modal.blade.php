
<div class="modal fade" id="rejectLeaveModal" tabindex="-1" role="dialog" aria-labelledby="rejectLeaveModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="rejectLeaveModal">reject Leave applied by <span id="user_name"></span></h4>
            </div>
            <div class="modal-body loading">
                <div class="col-md-2 col-md-offset-5">
                    <i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
                </div>
            </div>
            <div class="modal-body content">
                <div class="row">
                    <div class="col-md-12 ma-bot-10">
                        <label for="">Status</label>: <span class="status"></span>
                    </div>
                    <div class="col-md-12 ma-bot-10">
                        <div class="form-group ma-bot-10">
                            <label for="">Select Leave Type</label>
                            <select id="seltype" disabled class="form-control" name="type">
                                <option value="sick" >Sick Leave</option>
                                <option value="unpaid" >Unpaid Leave</option>
                                <option value="paid" >Paid Leave</option>
                                <option value="paid" >Half-day Leave</option>
                            </select>
                        </div>
                    </div>
                    <!-- ======================== Date Picker ======================= -->
                    <div class="col-md-12 no-padding ma-bot-10">
                        <div class="form-group">
                            <div class="col-md-6">
                                <div class='input-group date' id='datetimepicker1'>
                                    <input type='text' class="form-control" id="sdate" name="start_date"  placeholder="Start Date" disabled/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class='input-group date' id='datetimepicker1'>
                                    <input type='text' class="form-control" id="edate" name="end_date"  placeholder="End Date" disabled/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ====================== reasones ================ -->
                    <div class="col-md-12 ma-top-10">
                        <div class="form-group">
                            <label for="">Notes</label>
                            <textarea class="form-control" rows="5" placeholder="Please enter reason for applying leave" name="reason" disabled></textarea>
                        </div>
                    </div>
                    <div class="col-md-12 ma-top-10">
                        <div class="form-group">
                            <label for="">Reason for rejection</label>
                            <textarea class="form-control" rows="5" placeholder="Please enter reason for rejecting leave" name="rejectreason" id="rejectreason"></textarea>
                        </div>
                    </div>
                    </div></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>&nbsp;
                    <span class="pull-right">
                        <button type="button" class="btn btn-primary" onclick=rejectLeave()>
                        Submit
                    </button>
                    </span>
            
            </div>
        </div>
    </div>
</div>