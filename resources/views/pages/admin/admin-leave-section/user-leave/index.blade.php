@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">User Leave Overview</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="">Leaves Overview</a></li>
                </ol>
            </div>
        </div>
    </div>
        <div class="row leave-overview">
            <div class="col-md-12">
               @if(!empty($errors->all()))
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <span>{{ $error }}</span><br/>
                        @endforeach
                    </div>
                @endif
                @if (session('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ session('message') }}</span><br/>
                    </div>
                @endif   
            </div>
            <div class="col-md-12">
                <div class="row bg-white search-section-top">
           
                <div class="col-md-8">
                    <form class="form-group col-md-12 p-0" action="/admin/attendance" method="get">
                        <div class="col-md-3 p-l-0">
                            <div class='input-group date' id='datetimepickerStart_search'>
                                <input type='text' id="sd" class="form-control" value="<?=( isset( $_GET['start_date'] ) ? $_GET['start_date'] : '' )?>" autocomplete="off" name="start_date" placeholder="Start Date" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-3 p-l-0">
                            <div class='input-group date' id='datetimepickerEnd_search'>
                                <input type='text' id="ed" class="form-control" name="end_date" value="<?=( isset( $_GET['end_date'] ) ? $_GET['end_date'] : '' )?>" autocomplete="off" placeholder="End Date" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-2" style="padding-top:6px">
                            <button type="submit" class="btn btn-primary btn-sm center" style="margin-left: 2px;">Search</button>
                        </div>
                    </form>
                </div>
                <div class="col-md-4">
                    <div class="pull-right" style="margin-right: 20px">
                        <a href="/admin/attendance/downloadExcel?@if(strpos(Request::getQueryString(), 'start_date') !== false)start_date={{str_replace(' ','+',Request::get('start_date'))}}&@endif @if(strpos(Request::getQueryString(), 'end_date') !== false)end_date={{str_replace(' ','+',Request::get('end_date'))}} @endif" class="btn btn-success center" style="margin-left: 2px;"><i class="fa fa-download fa-fw"></i>  Download</a>
                    </div>
                </div>
</div>
</div>
           
            <div class="col-md-12 m-t-15">
                <div class="panel panel-default user-leave-table">
                <table class="table table-striped "  style="width:100%">

                      
                            <thead>
                                <th class="fixed-side">Emp ID</th>
                                <th class="fixed-side second">Employee Name</th>
                                @for($i=2;$i<count($dates);$i++)
                                    <th width="30" class="{{$code[$i] ? 'holiday' : '' }}" >{{$dates[$i]}}</th>
                                @endfor
                                <th class="fixed-side-right">SL </th>
                                <th class="fixed-side-right">PL </th>
                                <th class="fixed-side-right">Other </th>

                        </thead>
                        <tbody>
                            @if(count($result_data) > 0)
                                @foreach($result_data as $index => $row_details)
                                    <tr>
                                        @for ($i = 0; $i < count($row_details); $i++)
                                            @if ( $i < 2 )
                                                <td  class="fixed-side">
                                                    {{ $row_details[$i] ? $row_details[$i] : 'None' }}
                                                </td>
                                            @elseif ( ( $i < count($row_details) - 3 ) )
                                                @if ( $row_details[$i] == 'H' )
                                                    <td class="holiday">
                                                        {{$row_details[$i] }}
                                                    </td>
                                                @elseif( $row_details[$i] == 'P')
                                                    <td class="present">
                                                        {{$row_details[$i] }}
                                                    </td>
                                                @elseif ( $row_details[$i] == 'SL' )
                                                    <td class="sl">
                                                        Sick
                                                    </td>
                                                @elseif ( $row_details[$i] == 'SL-H' )
                                                    <td class="sl">
                                                        Sick (H)
                                                    </td>
                                                @elseif ( $row_details[$i] == 'PL')
                                                    <td class="pl ">
                                                        Paid
                                                    </td>
                                                @elseif ( $row_details[$i] == 'PL-H')
                                                    <td class="pl ">
                                                        Paid (H)
                                                    </td>
                                                @else
                                                    <td class="other-leave">
                                                        {{$row_details[$i] }}
                                                    </td>
                                                @endif
                                            @else
                                                <td class="fixed-side-right">
                                                    {{$row_details[$i] }}
                                                </td>
                                            @endif
                                        @endfor
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>		
    </div>

<script type="text/javascript">
    $(function(){
        
         $('#datetimepickerStart_search').datetimepicker({
            maxDate: new Date(),
            format: 'YYYY-MM-DD',
            useCurrent:false
        });

        $('#datetimepickerEnd_search').datetimepicker({
            maxDate: new Date(),
            format: 'YYYY-MM-DD',
            useCurrent:false
        });



        $("#btnReset").click(function(){
           window.location.href='/admin/attendance';
    }); 
    $("#leave-overview").DataTable( {
        scrollX: true,
        scrollCollapse: true,
         paging:  false,
         fixedColumns:   {
            leftColumns: 2,
            rightColumns: 3
        }
      
 });

    });
</script>
@endsection