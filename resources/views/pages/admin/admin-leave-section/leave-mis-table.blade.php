<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="table-responsive">
                <table class="table table-striped">
                    @if(isset($daysList))
                        <thead>
                            <tr>
                                <th width="10%">Name</th>
                                @foreach($daysList as $day)
                                    <th>{{$day}}</th>
                                @endforeach
                                <th width="5%">Total</th>
                            </tr>
                        </thead>
                    @endif
                    <tbody>
                        @if(isset($misReportList))
                            @if(count($misReportList) > 0)
                                @foreach($misReportList as $name => $misReport)
                                    <tr>
                                        <td>{{$name}}</td>
                                        @foreach($misReport as $report)
                                            <td>{{$report}}</td>
                                        @endforeach
                                        <td>{{ array_sum($misReport) }}</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="32">No Record found.</td>
                                </tr>
                            @endif
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
