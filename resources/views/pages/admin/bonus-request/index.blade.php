@extends('layouts.admin-dashboard')
@section('main-content')
<script>
    window.monthId = <?php echo $monthId; ?>;
</script>
<div class="container-fluid"  ng-app="myApp" ng-controller="bonusRequestCtrl">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Pending Bonus Requests for {{$monthName}}</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="/admin/bonus-request">Bonus Requests</a></li>
                    <li class="active">{{$monthName}} [{{$monthId}}]</li>
                </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <span class="btn btn-success" ng-click="addBonusRequest()"><i class="fa fa-plus fa-fw"></i> Add</span>
            </div>
        </div>
    </div>

    <div class="row bg-white search-section-top m-b">
      <div class="col-sm-4">
        <div class="btn-group btn-group-sm" role="group" >
          <button type="button" class="btn btn-primary" ng-click="fetchAllBonus()">All</button>
          <button type="button" class="btn btn-default" ng-click="fetchOnsiteBonus()">Onsite</button>
          <button type="button" class="btn btn-default" ng-click="fetchAdditionalDayBonus()">Weekend / Holiday</button>
          <button type="button" class="btn btn-default" ng-click="fetchOtherBonus()">Others</button>
        </div>
      </div>
      <div class="col-sm-2">
          <ui-select ng-model="selected_user" on-select="fetchForUser($select.selected)" theme="select2" placeholder="a"
          name="name" id="name" ng-style="form.errors.selected_user && {border: '1px solid red'}">
          <ui-select-match>%%$select.selected.name%%</ui-select-match>
          <ui-select-choices repeat="user.id as user in userList | filter: $select.search">
            %%user.name%%
          </ui-select-choices>
        </ui-select>
      </div>
      <div class="col-sm-6 text-right">
        <div class="btn-group btn-group-sm no-margin" >
            <a class="btn btn-default" href="/admin/bonus-request/{{$monthId}}">
              Other <span class="label label-default">%%bonusCounts.approved + bonusCounts.rejected%%</span>
            </a>
            <a class="btn btn-default active" href="/admin/bonus-request-pending/{{$monthId}}">
              Pending <span class="label label-default">%%bonusCounts.pending%%</span>
            </a>
        </div>
      </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                      @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif
        </div>
    </div>

    <div class="row">
      <div class="col-sm-6 bonus-request-card" ng-repeat="bonusRequestObj in data" ng-if=" bonusRequestObj.status == 'pending'">
        <div class="panel panel-default">
          <div class="panel-heading bg-white">
            <div class="row row-sm">
              <div class="col-sm-5">
                 <big>%%bonusRequestObj.user.name%%</big>
              </div>
              <div class="col-sm-7 text-right">
                <div>
                 <!-- <strong
                    ng-if=" bonusRequestObj.status == 'pending'"
                    class="small text-warning"
                    tooltip= "%%bonusRequestObj.status%%"
                    ><i class="fa fa-exclamation"></i> %%bonusRequestObj.status%%
                 </strong> -->
                 <!-- <strong
                    ng-if=" bonusRequestObj.status == 'approved'"
                    class="small text-success"
                    tooltip= "%%bonusRequestObj.status%%"
                    ><i class="fa fa-check"></i> %%bonusRequestObj.status%% by %%bonusRequestObj.approved_bonus.approver.name%% at %%bonusRequestObj.approved_bonus.approved_at | dateToISO%%
                 </strong>
                 <strong
                    ng-if=" bonusRequestObj.status == 'cancelled'"
                    class="small text-danger"
                    tooltip= "%%bonusRequestObj.status%%"
                    ><i class="fa fa-warning"></i> %%bonusRequestObj.status%%
                 </strong>
                 <strong
                    ng-if=" bonusRequestObj.status == 'rejected'"
                    class="small text-danger"
                    tooltip= "%%bonusRequestObj.status%%"
                    ><i class="fa fa-ban"></i> %%bonusRequestObj.status%%
                 </strong> -->
                </div>
                <div ng-if="bonusRequestObj.status == 'pending'" style="margin: -5px 0;">
                  <button class="btn btn-default text-danger btn-sm" ng-click="rejectBonusRequest(bonusRequestObj)">Reject</button>
                  <button class="btn btn-success btn-sm" ng-click="approveBonusRequest(bonusRequestObj)">Approve</button>
                </div>
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="row row-sm">
              <div class="col-sm-4">
                <strong>%%dateWithDay(bonusRequestObj.date)%%</strong>
              </div>
              <div class="col-sm-4 text-right">
                <span style="text-transform:capitalize" class="label label-success custom-label" ng-if="bonusRequestObj.type == 'additional'">%%bonusRequestObj.sub_type%% - 
                  <span ng-show="bonusRequestObj.redeem_info.duration == '8h'">Full Day</span>
                  <span ng-show="bonusRequestObj.redeem_info.duration == '6h'">6 Hours</span>
                  <span ng-show="bonusRequestObj.redeem_info.duration == '4h'">Half Day</span>
                  <span ng-show="bonusRequestObj.redeem_info.duration == '2h'">2 Hours</span>
                </span>
                <span class="label label-primary custom-label" ng-if="bonusRequestObj.type == 'onsite'">%%bonusRequestObj.type%%</span>
                <p ng-if="bonusRequestObj.type == 'extra'">
                  <span class="label label-info custom-label">
                    %%bonusRequestObj.type%%
                  </span>
                  <span class="label label-info custom-label"> %%bonusRequestObj.redeem_info.days%% day(s)</span>
                </p>
                <span class="label label-info custom-label" ng-if="bonusRequestObj.type != 'additional' && bonusRequestObj.type != 'onsite' && bonusRequestObj.type != 'extra'">%%bonusRequestObj.type%%</span>
              </div>
              <div class="col-sm-4 text-right" style="text-transform:uppercase">
                <strong ng-if="bonusRequestObj.type == 'additional'">%%bonusRequestObj.redeem_info.redeem_type%%</strong>
                <select class="input-sm form-control" ng-model="bonusRequestObj.redeem_info.onsite_allowance_id" style="margin: -4px 0;" ng-if="bonusRequestObj.type == 'onsite'">
                    <option ng-repeat="x in bonusRequestObj.onsite_eligibility.allowanceObj" ng-value="%%x.id%%">%%x.project.project_name%% ( %%x.type%% )</option>
                </select>
              </div>
            </div>
          </div>
          <div class="row row-sm" ng-if="bonusRequestObj.title">
            <div class="col-sm-12">
               <div class="well well-sm">
                <label class="no-margin">
                  <span class="text-muted">Title:</span> 
                  %%bonusRequestObj.title%%
                </label>
              </div>
            </div>
          </div>
          <div class="row row-sm" ng-if="!bonusRequestObj.title">
            <div class="col-sm-12" >
              <ul class="list-group no-margin no-radius" ng-repeat="timesheet in bonusRequestObj.timesheet_entries track by $index">
                <li ng-show="timesheet.timesheets.length > 0" class="list-group-item bg-light">%%timesheet.project_name%%</li>
                <li ng-show="timesheet.timesheets.length > 0" class="list-group-item" ng-repeat="timesheetObj in timesheet.timesheets">
                  <div class="row row-sm">
                    <div class="col-sm-1 text-center">
                      <strong>%%timesheetObj.duration%%</strong>
                    </div>
                    <div class="col-sm-8">
                      %%timesheetObj.task%%
                    </div>
                    <div class="col-sm-3">
                      <span ng-show="timesheetObj.status == 'approved'" class="label label-success custom-label"> Approved ( <strong>%%timesheetObj.review.approved_duration%%</strong> hours )</span>
                      <span ng-show="timesheetObj.status == 'pending'" class="label label-info custom-label"> Pending</span>
                      <span ng-show="timesheetObj.status == 'rejected'" class="label label-danger custom-label"> Rejected</span>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
          </div>
          <div class="panel-body relative">
            <div class="row row-sm">
              <div class="col-sm-12">
                  <label>Notes: </label>
                  <textarea ng-model="bonusRequestObj.notes" class="form-control"></textarea>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-12" ng-if="data.length == 0">
        <div class="panel panel-default">
          <div class="panel-body text-center ">
            <h4 class="text-muted">No pending bonus requests available</h4>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection
