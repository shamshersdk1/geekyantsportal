@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid"  ng-app="myApp" ng-controller="bonusRequestCtrl">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Bonus Requests</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li class="active">Bonus Requests</li>
                </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <span class="btn btn-success" ng-click="addBonusRequest()"><i class="fa fa-plus fa-fw"></i> Add</span>
            </div>
        </div>
    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                      @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif
        </div>
    </div>
</div>
<div class="user-list-view" style="margin:10px;" >
    <div class="panel panel-default">         
       <table id="user_ip_table" class="table table-striped table-condensed1">
          <thead>
              <th class="text-left">Name</th>
              <th class="text-left">Month</th>
              <th class="text-left">Pending Request</th>
              <th class="text-left">No. of Request</th>
              <th class="text-left">Status</th>
              <th class="text-right">Actions</th>
          </thead>
          <tbody>
              @foreach($months as $month)
              <tr>
                  <td><span class="no-margin">{{$month->id}}</span></td>
                  <td><span class="no-margin">{{date('F', mktime(0, 0, 0, $month->month, 10));}}</span></td>
                  <td>{{$month->bonusRequests->count()}}</td>
                  <td>{{$month->bonusRequests->count()}}</td>
                  <td>
                      @if($month->bonusSetting && $month->bonusSetting->value == 'locked')
                          <span class="label label-danger custom-label">Locked
                          </span>
                      @else
                          <span class="label label-primary custom-label">Open</span>
                      @endif
                  </td>
                  <td class="text-right">
                      @if(!$month->bonusSetting || $month->bonusSetting->value == 'open')
                          <button class="btn btn-danger" ng-click="lockMonth({{$month->id}})">Lock Request</button>
                      @else
                          <span class="label label-primary">Locked at {{datetime_in_view($month->bonusSetting->created_at)}}</span>
                      @endif
                      <a href="/admin/bonus-request-pending/{{$month->id}}" class="btn btn-primary">View</a>
                  </td>
              </tr>
              @endforeach
          </tbody>
       </table>
    </div>
 </div>
@endsection
