@extends('layouts.admin-dashboard')
@section('main-content')
<script>
    window.monthId = <?php echo $monthId; ?>;
</script>
<div class="container-fluid"  ng-app="myApp" ng-controller="bonusRequestCtrl">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Bonus Requests for {{$monthName}}</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="/admin/bonus-request">Bonus Requests</a></li>
                    <li class="active">{{$monthName}} [{{$monthId}}]</li>
                </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <span class="btn btn-success" ng-click="addBonusRequest()"><i class="fa fa-plus fa-fw"></i> Add</span>
            </div>
        </div>
    </div>

    <div class="row bg-white search-section-top m-b">
      <div class="col-sm-4">
        <div class="btn-group btn-group-sm" role="group" >
            <button type="button" class="btn btn-primary" ng-click="fetchAllBonus()">All</button>
            <button type="button" class="btn btn-default" ng-click="fetchOnsiteBonus()">Onsite</button>
            <button type="button" class="btn btn-default" ng-click="fetchAdditionalDayBonus()">Weekend / Holiday</button>
            <button type="button" class="btn btn-default" ng-click="fetchOtherBonus()">Others</button>
        </div>
      </div>
      <div class="col-sm-8 text-right">
        <div class="btn-group btn-group-sm no-margin" >
            <!-- <button ng-class="{'btn btn-default active' : filter == 'all', 'btn btn-default': filter != 'all' }" ng-click="fetchAllBonus()">
              All <span class="label label-default">%%bonusCounts.total%%</span>
            </button> -->
            <button ng-class="{'btn btn-default active' : filter == 'approved', 'btn btn-default': filter != 'approved' }" ng-click="fetchApprovedBonus()">
              Approved <span class="label label-default">%%bonusCounts.approved%%</span>
            </button>
            <button ng-class="{'btn btn-default active' : filter == 'rejected', 'btn btn-default': filter != 'rejected' }" ng-click="fetchRejectedBonus()">
              Rejected <span class="label label-default">%%bonusCounts.rejected%%</span>
            </button>
            <!-- <button ng-class="{'btn btn-default active' : filter == 'pending', 'btn btn-default': filter != 'pending' }" ng-click="fetchPendingBonus()"> -->
            <a class="btn btn-default" href="/admin/bonus-request-pending/{{$monthId}}">
              Pending <span class="label label-default">%%bonusCounts.pending%%</span>
            </a>
        </div>
      </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                      @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif
        </div>
    </div>

    <div class="panel panel-default">
      <div class="bonus-request-card" >
        <table class="table bg-white table-striped">
          <thead>
            <tr>
              <th width="10%">User</th>
              <th width="12%">Date</th>
              <th width="15%">Type</th>
              <th width="10%">Project name</th>
              <th width="10%">Amount</th>
              <th width="15%">Notes</th>
              <th class="text-right" width="15%">Status</th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="bonusRequestObj in data">
              <td>%%bonusRequestObj.user.name%%</td>
              <td>%%dateWithDay(bonusRequestObj.date)%%</td>
              <td style="text-transform: capitalize;">
                <span class="label label-success custom-label" ng-if="bonusRequestObj.type == 'additional'">Weekend - Full Day</span>
                <span ng-if="bonusRequestObj.type == 'additional'" class="label label-default">Encash</span>
                <span class="label label-primary custom-label" ng-if="bonusRequestObj.type == 'onsite'">%%bonusRequestObj.type%% - Domestic</span>

                <span class="label label-info custom-label" ng-if="bonusRequestObj.type != 'additional' && bonusRequestObj.type != 'onsite'">%%bonusRequestObj.type%%</span>
              </td>
              <td><div ng-repeat="project in bonusRequestObj.bonus_request_projects">%%project.project.project_name%%</div></td>
              <td>%%bonusRequestObj.amount%%</td>
              <td>%%bonusRequestObj.notes%%</td>
              <td class="text-right">
                <div>
                   <strong
                      ng-if=" bonusRequestObj.status == 'pending'"
                      class="small text-warning"
                      tooltip= "%%bonusRequestObj.status%%"
                      ><i class="fa fa-exclamation"></i> %%bonusRequestObj.status%%
                   </strong>
                   <strong
                      ng-if="bonusRequestObj.status == 'approved'"
                      class="small text-success"
                      tooltip= "%%bonusRequestObj.status%%"
                      >
                        <i class="fa fa-check"></i> 
                        <span> %%bonusRequestObj.status%% by %%bonusRequestObj.approved_by.name%% </span>
                        <div>at %%bonusRequestObj.approved_bonus.approved_at | dateToISO%%</div>
                   </strong>
                   <strong
                      ng-if="bonusRequestObj.status == 'cancelled'"
                      class="small text-danger"
                      tooltip= "%%bonusRequestObj.status%%"
                      ><i class="fa fa-warning"></i> %%bonusRequestObj.status%% by %%bonusRequestObj.approved_by.name%% </span>
                        <div>at %%bonusRequestObj.approved_bonus.approved_at | dateToISO%%
                   </strong>

                   <strong
                      ng-if="bonusRequestObj.status == 'rejected'"
                      class="small text-danger"
                      tooltip= "%%bonusRequestObj.status%%"
                      ><i class="fa fa-ban"></i> 
                    %%bonusRequestObj.status%% by %%bonusRequestObj.approved_by.name%% </span>
                        <div>at %%bonusRequestObj.approved_by.updated_at | dateToISO%%
                   </strong>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
</div>
@endsection
