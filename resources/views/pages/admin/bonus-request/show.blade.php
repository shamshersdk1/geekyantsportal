@extends('layouts.admin-dashboard')
@section('main-content')
<script type="text/javascript">
  window.bonusRequest = <?php echo $bonusRequest; ?>;
</script>
<section class="new-project-section">
   <div class="container-fluid" ng-app="myApp" ng-controller="bonusRequestShowCtrl">
      <div class="breadcrumb-wrap">
         <div class="row">
            <div class="col-sm-8">
               <h1 class="admin-page-title">Bonus Requests</h1>
               <ol class="breadcrumb">
                  <li><a href="/admin">Admin</a></li>
                  <li><a href="{{ url('admin/bonus-request') }}">Bonus Request</a></li>
                  <li class="active">{{$bonusRequest->id}}</li>
               </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
               <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-sm-9">
            <div class="row">
               <div class="col-sm-12"  style="margin-bottom:15px">
                  <div>
                     <h4>Applied Bonus Details</>
                     <div class="pull-right">
                        <button  class="btn btn-success" ng-disabled="bonusRequestObj.approved_bonus" ng-click="approve(bonusRequestObj)">Approve</button>
                         <button class="btn btn-danger" ng-disabled="bonusRequestObj.status != 'pending'" ng-click="reject(bonusRequestObj)">Reject</button>
                     </div>
                  </div>
               </div>
            </div>
            <div class="incident-wrap clearfix">
               <div class="row row-sm">
                  <div class="col-sm-12">
                     <div class="panel panel-default m-b-5">
                        <div class="info-table">
                           <label>Employee Name: </label> 
                           <span>%%bonusRequestObj.user.name%%</span>
                        </div>
                        <div class="info-table">
                           <label>Date: </label>
                           <span>%%bonusRequestObj.date | date%%</span>
                        </div>
                        <div class="info-table">
                           <label>Type: </label>
                           <span style="text-transform: capitalize;">%%bonusRequestObj.type%%</span>
                        </div>
                        <div class="info-table" ng-if="bonusRequestObj.sub_type">
                           <label>Sub Type: </label>
                           <span>%%bonusRequestObj.sub_type%%</span>
                        </div>
                        <div class="info-table" ng-if="bonusRequestObj.title">
                           <label>Title: </label>
                           <span>%%bonusRequestObj.title%%</span>
                        </div>
                        <div class="info-table" ng-if="bonusRequestObj.referral">
                           <label>Referral For: </label>
                           <span>%%bonusRequestObj.referral.name%%</span>
                        </div>
                        <div class="info-table">
                           <label>Status: </label>
                           <span style="display: inline-block; text-transform: capitalize;"
                              ng-if=" bonusRequestObj.status == 'pending'"
                              class="label label-warning custom-label"
                              >%%bonusRequestObj.status%%
                           </span>
                           <span style="display: inline-block; text-transform: capitalize;"
                              ng-if=" bonusRequestObj.status == 'approved'"
                              class="label label-success custom-label"
                              >%%bonusRequestObj.status%%
                           </span>
                           <span style="display: inline-block; text-transform: capitalize;"
                              ng-if=" bonusRequestObj.status == 'cancelled'"
                              class="label label-danger custom-label"
                              >%%bonusRequestObj.status%%
                           </span>
                           <span style="display: inline-block; text-transform: capitalize;"
                              ng-if=" bonusRequestObj.status == 'rejected'"
                              class="label label-danger custom-label"
                              >%%bonusRequestObj.status%%
                           </span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row row-sm" ng-if="bonusRequestObj.approved_bonus">
                  <div class="col-sm-12">
                  <h4>Approved Bonus Details</h4>
                     <div class="panel panel-default m-b-5">
                        <div class="info-table">
                           <label>Employee Name: </label> 
                           <span>%%bonusRequestObj.approved_bonus.user.name%%</span>
                        </div>
                        <div class="info-table">
                           <label>Date: </label>
                           <span>%%bonusRequestObj.approved_bonus.date | date%%</span>
                        </div>
                        <div class="info-table">
                           <label>Type: </label>
                           <span style="text-transform: capitalize;">%%bonusRequestObj.approved_bonus.type%%</span>
                        </div>
                        <div class="info-table" ng-if="bonusRequestObj.approved_bonus.sub_type">
                           <label>Sub Type: </label>
                           <span>%%bonusRequestObj.approved_bonus.sub_type%%</span>
                        </div>
                        <div class="info-table" ng-if="bonusRequestObj.approved_bonus.title">
                           <label>Title: </label>
                           <span>%%bonusRequestObj.approved_bonus.title%%</span>
                        </div>
                        <div class="info-table" ng-if="bonusRequestObj.approved_bonus.referral">
                           <label>Referral For: </label>
                           <span>%%bonusRequestObj.approved_bonus.referral.name%%</span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <activity-log reference-type="App\Models\BonusRequest" reference-id="{{$bonusRequest->id}}"></activity-log>
         @if(!empty($errors->all()))
         <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <span>{{ $error }}</span><br/>
            @endforeach
         </div>
      </div>
      @endif
      @if (session('message'))
      <div class="alert alert-success">
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
         <span>{{ session('message') }}</span><br/>
      </div>
      @endif
   </div>
</section>
@endsection