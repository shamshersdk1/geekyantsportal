@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
	<div class="login-wrapper">
	    <div class="row">
	        <ol class="breadcrumb">
	            <li><a href="/admin">Admin</a></li>
	            <li><a href="{{ url('admin/setting') }}">Settings</a></li>
	        </ol>

			<div class="col-md-6 col-md-offset-3">
				<div class="logo-holder-panel">
					<a class="admin-login-logo" href="/"><img src="/images/logo-dark.png" class="img-responsive admin-logo-img center-block" alt=""></a>
				</div>
			</div>

			<div class="col-md-6 col-md-offset-3 panel-login-admin">
				<!-- <div class="logo-holder-panel">
					<a class="admin-login-logo" href="/"><img src="/images/logo-dark.png" class="img-responsive admin-logo-img center-block" alt=""></a>
				</div> -->
				<div class="panel panel-default">

					@if(!empty($errors->all()))
						<div class="alert alert-danger">
							@foreach ($errors->all() as $error)
								<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
								<span>{{ $error }}</span><br/>
						  	@endforeach
						</div>
					@endif
					@if (session('message'))
					    <div class="alert alert-success">
					    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					        <span>{{ session('message') }}</span><br/>
					    </div>
					@endif

					<div class="panel-body">
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<form class="form-horizontal" role="form" method="POST" action="setting">
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
									<div class="input-group custom-input-group">
									  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-user fa-lg" aria-hidden="true"></i></span>
									  	@if(isset($user->setting))
									  		<input type="email" class="form-control" name="email"  placeholder="Email User Name" value="{{$user->setting->username}}">
									  	@else
											<input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email User Name">
										@endif
									</div>

									<div class="input-group custom-input-group">
									  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
									  @if(isset($user->setting))
									  	<input type="password" class="form-control" name="password" placeholder="Password" value="{{$user->setting->password}}" id="showpwd">
									  @else
										<input type="password" class="form-control" name="password" placeholder="Password" id="showpwd">
									  @endif
									</div>
									<span>Show password</span>
									<input type="checkbox" value="Show password" id="showpwdcheckbox"/>

									<div class="sign-btn">
										
										<button type="submit" class="btn btn-primary btn-block admin-signin" >
												Save
											</button>
									</div>
								</form>
							</div>
						</div>		
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
