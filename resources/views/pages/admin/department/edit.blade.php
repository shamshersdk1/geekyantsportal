@extends('layouts.admin-dashboard')
@section('main-content')

<section class="new-project-section">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">Departments</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
			  			<li><a href="{{ url('admin/department') }}">Departments</a></li>
			  			<li class="active">{{ $departmentObj->id }}</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>

	    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif

		<form class="form-horizontal" method="post" action="/admin/department/{{$departmentObj->id}}" enctype="multipart/form-data">
			<input name="_method" type="hidden" value="PUT" />
			<div class="panel panel-default">
				<div class="panel-body">
				    <div class="row">
				    	<div class="col-md-12">
							<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Type</label>
						    	<div class="col-sm-6">
									  <input type="text" class="form-control" id="type" name="type" value="{{ $departmentObj->type }}">
						    	</div>
							  </div>
							  <div class="form-group">
						    	<label for="department_type" class="col-sm-2 control-label">Description</label>
						    	<div class="col-sm-6">
						      		<textarea class="form-control"  name="description" value="{{$departmentObj->description}}" cols="4" rows="4">{{$departmentObj->description}}</textarea>
						    	</div>
							</div>
							  <div class="form-group">
								<label class="col-sm-2 control-label">Head:</label>
								<div class="col-sm-6">
									<select id="selectid2" name="head"  style="width=35%;" placeholder= "Select an option">
										<option value=""></option>
										@foreach($users as $x)
												@if($departmentObj->head == $x->id)
													<option value="{{$x->id}}" selected>{{$x->name}}</option>
												@else
													<option value="{{$x->id}}" >{{$x->name}}</option>
									        	@endif
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
									<label class="col-sm-2 control-label">First Contact:</label>
									<div class="col-sm-6">
										<select id="selectid22" name="first_contact"  style="width=35%;" placeholder= "Select an option">
											<option value=""></option>
											@foreach($users as $y)
													@if($departmentObj->first_contact == $y->id)
														<option value="{{$y->id}}" selected>{{$y->name}}</option>
													@else
														<option value="{{$y->id}}" >{{$y->name}}</option>
													@endif
											@endforeach
										</select>
									</div>
								</div>
					  	</div>
					</div>
				</div>
			</div>
  	  		<div class="text-center">
		  		<button type="submit" class="btn btn-success">Update</button>
		  	</div>
		</form>
	</div>
</section>
@endsection
