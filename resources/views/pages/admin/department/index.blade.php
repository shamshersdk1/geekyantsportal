@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Support Departments</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
		  			<li class="active">Support Departments</li>
        		</ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
				<a href="/admin/department/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Add Department</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
	    	@if(!empty($errors->all()))
	            <div class="alert alert-danger">
	                @foreach ($errors->all() as $error)
	                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                    <span>{{ $error }}</span><br/>
	                  @endforeach
	            </div>
	        @endif
	        @if (session('message'))
	            <div class="alert alert-success">
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ session('message') }}</span><br/>
	            </div>
			@endif
		</div>
		<div class="user-list-view">
            <div class="panel panel-default">
            	<table class="table table-striped">
					<tr>
						<th width="5%" class="sorting_asc">#</th>
                        <th width="10%">Type</th>
						<th width="20%">Description</th>
                        <th width="10%">Head</th>
                        <th width="10%">First Contact</th>
						<th width="">Created At</th>
                        <th width="">Updated At</th>
						<th class="text-right sorting_asc" width="20%">Actions</th>
					</tr>
					@if(count($departments) > 0)
						@foreach($departments as $department)
							<tr>
								<td class="td-text"><a>{{$department->id}}</a></td>
                                <td class="td-text">{{$department->type}}</td>
								<td class="td-text">{{$department->description}}</td>
								<td class="td-text"> {{!empty($department->departmentHead->name) ? $department->departmentHead->name : '' }}</td>
								<td class="td-text">{{!empty($department->departmentFirstContact->name) ? $department->departmentFirstContact->name : '' }}</td>
                                <td class="td-text">{{datetime_in_view($department->created_at)}}</td>
                                <td class="td-text">{{datetime_in_view($department->updated_at)}}</td>
                                <td class="text-right">
									<a href="/admin/department/{{$department->id}}" class="btn btn-success crude-btn btn-sm">View</a>
									<a href="/admin/department/{{$department->id}}/edit" class="btn btn-info btn-sm crude-btn">Edit</a>
									<!-- <form action="/admin/department/{{$department->id}}" method="post" style="display:inline-block;">
										<input name="_method" type="hidden" value="DELETE">
										<button class="btn btn-danger btn-sm crude-btn" type="submit">Delete</button>
									</form> -->
								</td>
							</tr>
						@endforeach
	            	@else
	            		<tr>
	            			<td colspan="3" class="text-center">No results found.</td>
	            		</tr>
	            	@endif
            	</table>
				<div class="col-md-12">
                    <div class="pageination pull-right">
                        <nav aria-label="Page navigation">
                              <ul class="pagination">
                                {{ $departments->render() }}
                              </ul>
                        </nav>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>
</div>
@endsection
