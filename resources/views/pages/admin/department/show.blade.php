@extends('layouts.admin-dashboard')
@section('main-content')
<section>
   <div class="container-fluid">
      <div class="breadcrumb-wrap">
         <div class="row">
            <div class="col-sm-6">
               <h1>Departments</h1>
               <ol class="breadcrumb">
                  <li><a href="/admin">Admin</a></li>
                  <li><a href="/admin/department">Departments</a></li>
                  <li class="active">{{$departmentObj->id}}</li>
               </ol>
            </div>
         </div>
      </div>
      @if(!empty($errors->all()))
      <div class="alert alert-danger">
         @foreach ($errors->all() as $error)
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
         <span>{{ $error }}</span><br/>
         @endforeach
      </div>
      @endif
      @if (session('message'))
      <div class="alert alert-success">
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
         <span>{{ session('message') }}</span><br/>
      </div>
      @endif
      <div class="row cms-dev">
         <div class="col-md-12">
            <h4 >Department Details</h4>
            <div class="panel panel-default">
               <div class="panel-body row">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <div class=" col-md-10">
                     <label>Department Type: </label>
                     <span>{{$departmentObj->type}}</span>
                  </div>
                  <div class=" col-md-10">
                     <label>Department Head : </label>
                     <span>{{ $departmentObj->departmentHead->name}}</span>
                  </div>
                  <div class=" col-md-10">
                        <label>Department First Contact : </label>
                        <span>{{ $departmentObj->departmentFirstContact->name}}</span>
                     </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
@endsection