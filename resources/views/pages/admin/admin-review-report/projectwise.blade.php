@extends('layouts.admin-dashboard')
@section('main-content')
    <div class="container-fluid">
        <div class="breadcrumb-wrap">
           <div class="row">
              <div class="col-sm-8">
                 <h1 class="admin-page-title">Project Timesheet for the month {{ $output['month_string'] ?$output['month_string']: '' }}</h1>
                 <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="/admin/mis-review-report-project">MIS Report Project</a></li>
                    <li class="active">Project Timesheet</li>
                 </ol>
              </div>
           </div>
        </div>
        <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                      @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif
        </div>
         </div>
        <div class="m-b panel panel-default">
            <div class="panel-body">
                <a href="/admin/download-review-report-project?project_id={{$output['project_id']}}&month={{$output['month_id']}}" class="btn btn-success pull-right"><i class="fa fa-download"></i> Download</a>
                <form class="form-inline" method="get" style="display:flex;">
                    <div class="form-group" style="display:flex; padding:5px;">
                        <label for="exampleInputEmail2"  style="padding:2px;">Project</label>
                        <div style="padding:2px;">
                            @if(isset($projectList))
                                <select id="selectid2" name="project_id"  style="width=35%;" required>
                                    @foreach($projectList as $project)
                                        @if($project->id == $output['project_id'])
                                            <option value="{{$project->id}}" selected>{{$project->project_name}}</option>
                                        @else
                                            <option value="{{$project->id}}" >{{$project->project_name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            @endif
                        </div>
                    </div>
                    <div class="form-group" style="display:flex; padding:5px;">
                        <label for="exampleInputEmail2"  style="padding:2px;">Month</label>
                        <div style="padding:2px;">
                            @if(isset($monthList))
                                <select id="selectid3" name="month"  style="width=35%;" required>
                                    @foreach($monthList as $month)
                                        @if($month->id == $output['month_id'])
                                            <option value="{{$month->id}}" selected>{{date('F', mktime(0, 0, 0, $month->month, 10)).', '.$month->year}}</option>
                                        @else
                                        <option value="{{$month->id}}" >{{date('F', mktime(0, 0, 0, $month->month, 10)).', '.$month->year}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            @endif
                        </div>
                    </div>
                    <div style="padding:5px;">
                        <button type="submit" class="btn btn-success"  style="margin:2px;">Submit</button>
                    </div>
                </form>
            </div>
        </div>

        @if ($output['error'] === true && !empty($output['error_message']))
            <strong>{{$output['error_message']}}</strong>
        @endif

        @if ($output['success'] === true)
            <div class="row">
                <div class="col-sm-6">
                    <table class="table table-bordered bg-white">
                        <tr>
                            <td>Project Name</td>
                            <th>{{$output['project']->project_name}}</th>
                        </tr>
                        <tr>
                            <td>Project Manager</td>
                            <th>
                                @if($output['project']->projectManager)
                                    {{$output['project']->projectManager->name}}
                                @endif
                            </th>
                        </tr>
                        <tr>
                            <td>BD Manager</td>
                            <th>
                                @if($output['project']->bdManager)
                                    {{$output['project']->bdManager->name}}
                                @endif
                            </th>
                        </tr>
                        <tr>
                            <td>Account Manager</td>
                            <th>
                                @if($output['project']->bdManager)
                                    {{$output['project']->accountManager->name}}
                                @endif
                            </th>
                        </tr>
                        <tr>
                            <td>Start Date</td>
                            <th>{{date_in_view($output['month_start_date'])}}</th>
                        </tr>
                        <tr><td>End Date</td>
                            <th>{{date_in_view($output['month_end_date'])}}</th>
                        </tr>
                        <tr>
                            <td>Total Approved Hrs</td>
                            <th>{{$output['total_approved_hrs']}}</th>
                        </tr>
                        <tr>
                            <td>Total Employee Hrs</td>
                            <th>{{$output['total_employees_hrs']}}</th>
                        </tr>
                    </table>
                </div>
                <div class="col-sm-6">

                     <table class="table table-bordered bg-white">
                        <tr>
                            <th>Name</th>
                            <th width="120" class="text-center">Approved Hours</th>
                            <th width="120" class="text-center">Employee Hrs</th>
                            <th width="120" class="text-center">Total Extra Hrs</th>
                        </tr>
                        @foreach($result as $result)
                            <tr>
                                <td>{{$result['name']}}</td>
                                <td class="text-center">{{$result['total_hour']  ? $result['total_hour']- $result['extra_hour'] : 0}}</td>
                                <td class="text-center">{{$result['total_hour']? $result['total_hour'] : 0}}</td>
                                <td class="text-center">{{$result['extra_hour'] ? $result['extra_hour'] :0 }}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>

                <div class="col-sm-6">
                    <b>Extra Hour(s) Request</b>
                    <table class="table table-bordered table-condensed table-hover bg-white">
                        <tr>
                            <th  width="30%">Name</th>
                            <th  width="25%">Date</th>
                            <th class="text-center" width="15%">Hours</th>
                            <th  class="text-center"width="10%">Status</th>
                        </tr>

                                @if(!empty($userExtra) && count ($userExtra)>0 )
                                    @foreach($userExtra as $user_extra)
                                    <tr>

                                    <td >{{$user_extra->user->name ? $user_extra->user->name :''}}</td>
                                    <td >{{$user_extra->date ? date_in_view($user_extra->date) :''}}</td>
                                        <td class="text-center">{{$user_extra->extra_hours ? $user_extra->extra_hours : 0}}</td>
                                        <td>

                                            @if($user_extra->status && $user_extra->status==='approved')
                                                <span class="label label-success">{{$user_extra->status}}</span>
                                            @else
                                                <span class="label label-danger">{{$user_extra->status}}</span>
                                            @endIf
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                    </table>
                </div>
            </div>



            <table class="table table-bordered bg-white">
                <tr>
                    <th>Date</th>
                    <th class="text-center">Employee ID</th>
                    <th>Employee Name</th>
                    <th class="text-center">Employee Hrs</th>
                    <th class="text-center">Approved Hrs</th>
                    <th class="text-center">Extra Hrs</th>
                    <th>Task</th>
                </tr>
                @if(!empty($output['user_timesheet']))
                @foreach($output['user_timesheet'] as $timesheet)
                    <tr
                        @if(date('D', strtotime($timesheet->date)) == 'Sat' || date('D', strtotime($timesheet->date)) == 'Sun')
                            class="warning"
                        @endif
                    >
                        <td>
                            <small>{{ date('D - d M Y', strtotime($timesheet->date)) }}
                            @if(date('D', strtotime($timesheet->date)) == 'Sat' || date('D', strtotime($timesheet->date)) == 'Sun')
                                <br />(Weekend)
                            @endif</small>
                        </td>
                        <td class="text-center">
                            {{$timesheet->user->employee_id}}
                        </td>
                        <td>
                            {{$timesheet->user->name}}
                        </td>

                        <td class="text-center">
                            {{$timesheet->duration}}
                        </td>
                        <td class="text-center">
                            @if($timesheet->duration <= 8)
                            {{$timesheet->duration}}
                            @else
                            8
                            @endif

                        </td>
                        <td class="text-center">
                            @if($timesheet->duration>8)
                            {{$timesheet->duration-8}}
                            @else
                            0
                            @endif
                        </td>
                        <td>
                            @if(($timesheet->review ? $timesheet->review->approver_id : null) > 0)
                                @if($timesheet->task == $timesheet->review->approval_comments)
                                    {{$timesheet->task}}
                                @else
                                    Employee: {{$timesheet->task}}<br />
                                    <strong>Manager: {{$timesheet->review->approval_comments}}</strong>
                                @endif
                            @else
                                {{$timesheet->task}}
                            @endif
                        </td>
                    </tr>
                @endforeach
                @endif
            </table>
        @endif
@endsection
