@extends('layouts.admin-dashboard')
@section('main-content')
    <div class="container-fluid mis-report">
        <div class="breadcrumb-wrap">
           <div class="row">
              <div class="col-sm-8">
                 <h1 class="admin-page-title">MIS Report for {{$monthObj->formatMonth()}}</h1>
                 <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li class="active">MIS Report </li>
                 </ol>
              </div>
           </div>
        </div>

        @foreach($users as $user)
            @if(count($user->reportees) > 0)
                <h4>{{$user->name}}</h4>
                <table class="table table-bordered table-hover bg-white">
                    <tr>
                        <th>Team Members</th>
                        <th width="120px" class="text-center">Approved Hrs</th>
                        <th width="120px" class="text-center">Employee Hrs</th>
                        <th width="120px" class="text-center">Action(s)</th>
                    </tr>
                    @if(count($user->reportees)>0)
                        @foreach($user->reportees as $member)
                            <tr>
                                <td>{{$member->name}}</td>
                                <td width="120px" class="text-center">
                                    @if(!empty($output['user_timesheet_map'][$member->id]['grant_approved_total_time']))
                                        {{$output['user_timesheet_map'][$member->id]['grant_approved_total_time']}}</td>
                                    @endif
                                <td width="120px" class="text-center">
                                    @if(!empty($output['user_timesheet_map'][$member->id]['grant_employee_total_time']))
                                        {{$output['user_timesheet_map'][$member->id]['grant_employee_total_time']}}
                                    @endif
                                    </td>
                                <td width="120px" class="text-center"><a class="btn btn-sm btn-success" href="{{url('/admin/mis-review-report/'.$monthObj->id.'/'.$member->id . '/' .'review')}}"><i class="fa fa-file-o"></i> &nbsp; View Report</a> </td>
                            </tr>
                        @endforeach
                    @endif
                </table>
            @endif
        @endforeach
    </div>
@endsection
