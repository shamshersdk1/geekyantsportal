@extends('layouts.admin-dashboard')
@section('main-content')
    <div class="container-fluid mis-report">
        <div class="breadcrumb-wrap">
           <div class="row">
              <div class="col-sm-8">
                 <h1 class="admin-page-title">MIS Report for {{$monthObj->formatMonth()}}</h1>
                 <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li class="active">MIS Report </li>
                 </ol>
              </div>
              <div class="col-sm-4 text-right m-t-10">
                   <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
                </div>
           </div>
        </div>

       <div class="row">
            <div class="col-sm-4">
                <table class="table table-striped table-condensed table-bordered bg-white">
                    <tr><td>Name</td><th>{{$user->name}}</th></tr>
                    <tr><td>Month</td><th>{{$monthObj->formatMonth()}}</th></tr>
                    <tr><td>Start Date</td><th>{{date_in_view($monthObj->getFirstDay())}}</th></tr>
                    <tr><td>End Date</td><th>{{date_in_view($monthObj->getLastDay())}}</th></tr>
                    <tr><td>Company Working Hrs</td><th>{{$monthObj->getWorkingDays() * 8 }} hr(s)</th></tr>
                    <tr><td>Company Working Days</td><th>{{$monthObj->getWorkingDays()}} days</th></tr>
                </table>
            </div>
            <div class="col-sm-4">
                <table class="table table-striped table-condensed table-bordered bg-white">
                    <tr>
                        <td>Employee's Expected Hours <small class="text-muted">After considering leaves / holidays</small></td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>Employee's Actual Work Hours</td>
                        <th>{{$data['timesheet_overview'][0]['total_hour'] ? $data['timesheet_overview'][0]['total_hour'] : ''}}</th>
                    </tr>
                    <tr>
                        <td>Approved Hours</td>
                        <th>{{!empty($data['timesheet_overview']['total_hour']) ? $data['timesheet_overview']['total_hour']-$data['timesheet_overview']['extra_hour'] : 0}}</th>
                    </tr>
                </table>
            </div>
            <div class="col-sm-4">
                <b>Extra Hour(s) Request</b>
                <table class="table table-bordered table-condensed table-hover bg-white">
                    <tr>
                        <th width="30%">Date</th>
                        <th class="text-center" width="15%">Hours</th>
                        <th width="55%">Status</th>
                    </tr>
                    @if(!empty($data['timesheet_details']['bonuses']['extra_hours']) && count($data['timesheet_details']['bonuses']['extra_hours']) >0)
                        @foreach($data['timesheet_details']['bonuses']['extra_hours'] as $extra)
                            <tr>
                                <td>
                                    <small>{{date_in_view($extra->date)}}</small>
                                </td>
                                <td class="text-center">
                                    {{$extra->extra_hours}}
                                </td>
                                <td>

                                    @if($extra->status == 'pending')
                                        <label class="label label-warning">{{$extra->status}}</label>
                                    @else
                                        <label class="label label-success">{{$extra->status}}</label>
                                    @endif

                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="3">
                                No record found
                            </td>
                        </tr>
                    @endif
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <b>Bonus Request</b>
                <table class="table table-bordered table-condensed table-hover bg-white">
                    <tr>
                        <th>Type</th>
                        <th>Date</th>
                        <th>Status</th>
                        <th>Payment Updates</th>
                        <th>Amount</th>
                    </tr>
                    @if(!empty($data['timesheet_details']['bonus_requests']['bonus']) && count($data['timesheet_details']['bonus_requests']['bonus'])>0)
                        @foreach($data['timesheet_details']['bonus_requests']['bonus'] as $request)
                            <tr>
                                <td>
                                    {{$request['type'] }}
                                 
                                </td>
                                <td>
                                    {{$request['bonus']['date'] ? date_in_view($request['bonus']['date']) : ''}}
                                </td>
                                <td>
                                    @if($request['bonus']['status'] == 'approved')
                                        <label class="label label-success">{{$request['bonus']['status']}}</label>
                                    @elseif($request['bonus']['status'] == 'pending')
                                        <label class="label label-primary">{{$request['bonus']['status']}}</label>
                                    @else
                                        <label class="label label-danger">{{$request['bonus']['status']}}</label>
                                    @endif
                                    @if($request['bonus']['status']=='approved' || $request['bonus']['status']=='rejected')
                                        @if($request['bonus']['approver'])
                                        <small> by {{ $request['bonus']['approver']['name']}} at {{date_in_view($request['bonus']['approved_at']) ? $request['bonus']['approved_at']:''}}</small>
                                        @endif
                                    @endif
                                </td>
                                <td>
                                    @if($request['bonus']['status']=='approved' )
                                        @if($request['bonus']['transaction_id'] ==null)
                                        <label class="label label-danger">Due</label>
                                        @else
                                        <label class="label label-primary">Paid</label>
                                            @if($request['bonus']['payer'])
                                                by {{$request['bonus']['payer']['name']}} at {{ $request['bonus']['paid_at'] ? date_in_view($request['bonus']['paid_at']) : ''}}
                                            @endif
                                        @endif
                                    @endif

                                </td>
                                <td>
                                    @if($request['bonus']['amount'])
                                        {{$request['bonus']['amount']}}
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5">
                                No record found
                            </td>
                        </tr>
                    @endif
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <b>Month Report</b>
                <table class="table table-bordered table-hover bg-white">
                <thead>
                    <tr>
                        <th width="10%">Date</th>
                        <th width="30%">Timelog</th>
                        <th width="5%">Total Hrs</th>
                        <th width="5%">Auto-Approved</th>
                        <th width="30%">Additonal</th>
                    </tr>
                </thead>
                <tbody>

                     @if(!empty($data['timesheet_details']['timesheets']['days']))
                        @foreach($data['timesheet_details']['timesheets']['days'] as $day)
                        <tr @if(!empty($day))
                            @if($day['on_leave'] == 'true')
                            class = "danger"
                            @elseif($day['is_weekend'] == 'true')
                            class = "warning"
                             @elseif($day['is_holiday'] == 'true')
                            class = "info"
                        @endif>
                        <td >
                            {{date_in_view($day['date'])}}
                            @if(isset($day['on_leave']) && $day['on_leave']=='true')
                                <strong>(On Leave)</strong>
                            @endif
                            @if(isset($day['is_weekend']) && $day['is_weekend']=='true')
                                <strong>(Weekend)</strong>
                            @endif
                            @if(isset($day['is_holiday']) && $day['is_holiday']=='true')
                                <strong>(Holiday)</strong>
                            @endif
                        </td>

                        <td>
                            @if(!empty($day['projects']) && count($day['projects']) > 0)
                            @foreach($day['projects'] as $project)
                                <div>
                                    <div>
                                        <strong>{{$project['project_name']}}</strong>
                                    </div>
                                    @foreach($project['user_timesheets'] as $userTimesheeet)
                                        <div class="m-b-10">
                                            @if($userTimesheeet['status'] ==='approved')
                                            <span class="label label-success">Approved</span>&nbsp;
                                            @elseif($userTimesheeet['status'] === 'pending')
                                            <span class="label label-warning">Pending</span>&nbsp;
                                            @endif
                                            <strong>{{$userTimesheeet['duration']}}</strong> {{$userTimesheeet['task']}}

                                        </div>
                                    @endforeach
                                </div>
                            @endforeach
                            @endif
                        </td>
                        <td class="text-center">{{$day['employee_total_time'] ?? 0}}</td>

                            <td class="text-center">
                                @if(!empty($day['employee_total_time']))
                                    {{ $day['employee_total_time'] <= 8 ? $day['employee_total_time']: 8 }}
                                @endif
                            </td>

                        <td class="">
                           <div class="row">
                                <div class="col-sm-6 pull-left">

                                    @if(!empty($day['onsite']))
                                    <div class="well well-sm no-m-b">
                                                    <div class="clearfix m-b-5" >
                                                        Requested for <strong class="pull-right"> [onsite]</strong>
                                                    </div>
                                                    <div>
                                                        <textarea disabled="true" class="form-control m-b-5" value="{{$day['onsite']['notes']}}" placeholder="Additional Notes..."></textarea>
                                                    </div>
                                                    @if(!empty($day['onsite']['status']) && $day['onsite']['status'] =='pending')
                                                        <span class="label label-primary">Pending</span>
                                                    @elseif(!empty($day['onsite']['status']) && $day['onsite']['status'] =='approved')
                                                        <span class="label label-success">Approved</span>
                                                    @elseif(!empty($day['onsite']['status']) && $day['onsite']['status'] =='rejected')
                                                        <span class="label label-danger">
                                                        Rejected</span>
                                                    @endif
                                                    @if(!empty($day['onsite']['status']) && $day['onsite']['status'] =='approved')
                                                        <span class="small">by {{$day['onsite']['reviewer']['name']}} at {{date_in_view($day['onsite']['reviewed_at'])}}
                                                        </span>
                                                    @elseif(!empty($bonusRequest['status']) && $bonusRequest['status'] =='rejected')
                                                        <span class="small">by
                                                        {{$day['onsite']['reviewer']['name']}} at {{date_in_view($day['onsite']['reviewed_at'])}}</span>
                                                    @endif
                                    </div>
                                    @endif

                                    @if(!empty($day['additional']))

                                    <div class="well well-sm no-m-b">
                                                    <div class="clearfix m-b-5" >
                                                        Requested for <strong class="pull-right"> [additional]</strong>
                                                    </div>
                                                    <div>
                                                        <textarea disabled="true" class="form-control m-b-5" value="{{$day['additional']['notes']}}" placeholder="Additional Notes..."></textarea>
                                                    </div>
                                                    @if(!empty($day['additional']['status']) && $day['additional']['status'] =='pending')
                                                        <span class="label label-primary">Pending</span>
                                                    @elseif(!empty($day['additional']['status']) && $day['additional']['status'] =='approved')
                                                        <span class="label label-success">Approved</span>
                                                    @elseif(!empty($day['additional']['status']) && $day['additional']['status'] =='rejected')
                                                        <span class="label label-danger">
                                                        Rejected</span>
                                                    @endif
                                                    @if(!empty($day['additional']['status']) && $day['additional']['status'] =='approved')
                                                        <span class="small">by {{$day['additional']['reviewer']['name']}} at {{date_in_view($day['additional']['updated_at'])}}
                                                        </span>
                                                    @elseif(!empty($bonusRequest['status']) && $bonusRequest['status'] =='rejected')
                                                        <span class="small">by
                                                        {{$day['additional']['reviewer']['name']}} at {{date_in_view($day['additional']['updated_at'])}}</span>
                                                    @endif
                                    </div>


                                    @endif
                                </div>


                                <div class="col-sm-6 pull-right">
                                @if(isset($day['approve_extra_hour']) && $day['approve_extra_hour'] )
                                        <div class="input-group m-b-5 text-left">

                                            <span class="input-group-addon">
                                                Extra Hrs
                                            </span>

                                                <input class="form-control text-center" disabled="true" type="number" name="additional" value="{{$day['extra_hour']['extra_hours']}}"/>


                                        </div>
                                        <select class="form-control clearfix" disabled="true" value="{{$day['extra_hour']['project_id']}}">
                                            @foreach($day['projects'] as $project)
                                                <option value="{{$project['id']}}">{{$project['project_name']}}</option>
                                            @endforeach
                                        </select>
                                        <label class="m-t-10" >
                                        </label>
                                    @endif
                                </div>

                            </div>
                                 
                        </td>
                    </tr>
                    @endif
                    @endforeach
                    @endif
                </tbody>
            </table>
            </div>
        </div>
        <div class="clearfix text-right">
            <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
        </div>
    </div>
@endsection
