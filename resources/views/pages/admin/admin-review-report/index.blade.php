@extends('layouts.admin-dashboard')
@section('main-content')
<script type="text/javascript">
    window.userId = '<?php echo $user->id; ?>';
    window.monthId = '<?php echo $monthObj->id; ?>';
</script>

    <div class="container-fluid" ng-app="myApp" ng-controller="adminTimesheetReviewCtrl">
        <div class="breadcrumb-wrap">
           <div class="row">
              <div class="col-sm-8">
                 <h1 class="admin-page-title">Reporting for Timesheet for the month {{ $monthObj->formatMonth() }} of {{$user->name}}</h1>
                 <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="/admin/mis-review-report">MIS Report </a></li>
                    <li class="active">Timesheet of {{$user->name}} </li>
                 </ol>
              </div>
           </div>
        </div>
        @if (!empty($error) && $error === true)
            <strong>{{$error_message}}</strong>
        @endif
        @if (!empty($output['success']) && $output['success']  == 1)
            <div class="row">
                <div class="col-sm-4">
                    <table class="table table-striped table-bordered bg-white">
                        <tr><td>Name</td><th>{{$user->name}}</th></tr>
                        <tr><td>Month</td><th>{{$monthObj->formatMonth()}}</th></tr>
                        <tr><td>Start Date</td><th>{{date_in_view($monthObj->getFirstDay())}}</th></tr>
                        <tr><td>End Date</td><th>{{date_in_view($monthObj->getLastDay())}}</th></tr>
                        <tr><td>Company Working Hrs</td><th>{{$monthObj->getWorkingDays() * 40 }} hr(s)</th></tr>
                        <tr><td>Company Working Days</td><th>{{$monthObj->getWorkingDays()}} days</th></tr>
                    </table>
                </div>
                <div class="col-sm-4">
                    <table class="table table-striped table-bordered bg-white m-b-10">
                        <tr scope="col">
                            <th> Project Name </th><th> Approved Hrs</th><th> Employee Hrs </th>
                        </tr>
                        <tr ng-repeat="project in model.data.project_map">
                            <td>%%project.project.project_name%%</td>
                            <td>%%project.approved_total_time%%</td>
                            <td>%%project.employee_total_time%%</td>
                        </tr>
                        <tr>
                            <td><strong>Total</strong></td>
                            <td><strong>{{$output['grant_approved_total_time']}}</strong></td>
                            <td><strong>{{$output['grant_employee_total_time']}}</strong></td>
                        </tr>
                    </table>
                    <table class="table table-striped table-bordered bg-white m-b-5">
                        <tr>
                            <th>% Variable Pay (Approved)</th>
                            <th>% Variable Pay (Employee)</th>
                        </tr>
                        <tr>
                            <td>{{round($output['grant_approved_total_time']/($output['total_working_hrs']/100),0)}}%</td>
                            <td>{{round($output['grant_employee_total_time']/($output['total_working_hrs']/100),0)}}%</td>
                        </tr>
                    </table>
                    <i>*50% on Timesheet of Total Variable Pay</i>s
                </div>
                <div class="col-sm-4">
                    <div class="m-b panel-defaultel panel-default">
                    <div class="panel-body">
                        <form class="form-inline" method="get" style="display:flex;">
                            <div class="form-group" style="display:flex; padding:5px;">
                                <label for="exampleInputEmail2"  style="padding:2px;">Project</label>
                                <div style="padding:2px;">
                                    @if(isset($userList))
                                        <select id="selectid2" name="project_id"  style="width=35%;" required>
                                            @foreach($userList as $usr)
                                                @if($usr->employee_id == $output['employee_id'])
                                                    <option value="{{$usr->id}}" selected>{{$usr->name}}</option>
                                                @else
                                                    <option value="{{$usr->id}}" >{{$usr->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group" style="display:flex; padding:5px;">
                                <label for="exampleInputEmail2"  style="padding:2px;">Month</label>
                                <div style="padding:2px;">
                                    @if(isset($monthList))
                                        <select id="selectid3" name="month"  style="width=35%;" required>
                                            @foreach($monthList as $month)
                                                @if($month->id == $output['month_id'])
                                                    <option value="{{$month->id}}" selected>{{date('F', mktime(0, 0, 0, $month->month, 10)).', '.$month->year}}</option>
                                                @else
                                                    <option value="{{$month->id}}" >{{date('F', mktime(0, 0, 0, $month->month, 10)).', '.$month->financialYear->year}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    @endif
                                </div>
                            </div>
                            <div style="padding:5px;">
                                <button type="submit" class="btn btn-success"  style="margin:2px;">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
                <a href="/admin/mis-review-report/{{$monthObj->id}}/{{$user->id}}/review" class="btn btn-primary btn-sm">Review Timesheet</a>
                <br/>
                <!-- <button ng-if="model.data.user_monthly_timesheet.status == 'pending' && model.data.can_approve" class="btn btn-primary btn-sm" ng-click="model.reviewTimesheet()" >Approve Timesheet</button> -->
                    <span ng-if="model.data.user_monthly_timesheet.status == 'approved'">Timesheet approved by  %%model.data.user_monthly_timesheet.approver.name%% on %%model.data.user_monthly_timesheet.updated_at | dateToISO%%</span>
                    <span ng-if="model.data.extra_bonus_request">
                        <br/>
                        Extra Hour Bonus Applied for %%model.data.extra_hours%% Hour(s)<span class="label label-success">%%model.data.extra_bonus_request.status%%</span>
                    </span>
                </div>
            </div>

            <table class="table table-bordered table-hover bg-white">
                <thead>
                    <tr>
                        <th width="10%">Date</th>
                        <th width="30%">Timelog</th>
                        <th width="5%">Total Hrs</th>
                        <th width="5%">Auto-Approved</th>
                        <th width="30%">Additonal</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="day in model.data.days track by $index" ng-if="day" ng-class="{'danger' : day.on_leave !== 'false', 'warning' : day.is_weekend !=='false', 'info' : day.is_holiday !=='false'}">
                        <td>
                            %%day.date | dateToISO%% <br />
                            <span ng-if="day.on_leave !== 'false'"><strong>(On Leave)</strong></span>
                            <span ng-if="day.is_weekend !== 'false'"><strong>(Weekend)</strong></span>
                            <span ng-if="day.is_holiday !== 'false'"><strong>(Holiday)</strong></span>
                        </td>

                        <td>
                            <div ng-repeat="project in day.projects">
                                <div><strong>%%project.project_name%%</strong></div>
                                <div ng-repeat="user_timesheet in project.user_timesheets" class="m-b-10">
                                    <span ng-if="user_timesheet.review.approver_id > 0" class="label label-success">Approved</span>&nbsp;
                                    <span ng-if="user_timesheet.review.approver_id <= 0" class="label label-warning">Pending</span>&nbsp;
                                    <strong>[%%user_timesheet.duration%%]</strong> %%user_timesheet.task%%
                                </div>
                            </div>
                        </td>
                        <td class="text-center">%%day.employee_total_time%%</td>
                        <td class="text-center">%%day.approved_total_time%%</td>
                        <td class="">
                            <div class="row row-sm">
                                <div class="col-sm-6 pull-right" ng-if="day.extra_hour">
                                    <div class="input-group m-b-5 text-left">
                                        <span class="input-group-addon">
                                            Extra Hrs
                                        </span>
                                        <input ng-disabled="day.approve_extra_hour || model.data.user_monthly_timesheet.status == 'approved'" class="form-control text-center" type="number" name="additional" ng-model="day.extra_hour.extra_hours"/>

                                    </div>

                                    <select class="form-control clearfix" ng-disabled="day.approve_extra_hour || model.data.user_monthly_timesheet.status == 'approved'" ng-model="day.extra_hour.project_id">
                                        <option ng-repeat="project in day.projects" ng-value="%%project.id%%">%%project.project_name%%</option>
                                    </select>
                                    <label class="m-t-10" ng-show="day.extra_hour.project_id">
                                        <input type="checkbox" ng-disabled="!day.extra_hour.extra_hours || model.data.user_monthly_timesheet.status == 'approved'"  ng-model="day.approve_extra_hour" ng-change="model.addExtra($index)" name="approve_extra_hour"> &nbsp;Approve for payment
                                    </label>
                                    <label class="m-t-10" ng-show="day.onsite_bonus_request">
                                        Bonus Request :  Onsite
                                        <span ng-if="day.onsite_bonus_request.status == 'approved'" class="label label-success">Approved</span>&nbsp;
                                        <span ng-if="day.onsite_bonus_request.status == 'pending'" class="label label-warning">Pending</span>&nbsp;
                                    </label>
                                </div>
                                <div class="col-sm-6" ng-repeat="bonus_request in day.bonus_requests">
                                    <div class="well well-sm no-m-b">
                                        <div class="clearfix m-b-5" >
                                            Requested for <strong class="pull-right"> [%%bonus_request.type%%]
                                                <span ng-if="bonus_request.redeem_info.redeem_type == 'pl'">[PL]</span>
                                                <span ng-if="bonus_request.redeem_info.redeem_type == 'encash'">[ENCASH]</span>
                                            </strong>

                                        </div>
                                        <div class="clearfix m-b-5">
                                            <select class="input-sm form-control" ng-model="bonus_request.redeem_info.onsite_allowance_id" style="margin: -4px 0;" ng-if="bonus_request.type == 'onsite'" ng-disabled="bonus_request.status =='approved' || bonus_request.status == 'rejected'">
                                            <option ng-repeat="x in bonus_request.onsite_eligibility.allowanceObj" ng-value="%%x.id%%">%%x.project.project_name%% ( %%x.type%%)</option>
                                        </select>
                                        </div>
                                        <div>
                                            <textarea ng-disabled="bonus_request.status == 'approved' || bonus_request.status == 'rejected'" class="form-control m-b-5" ng-model="bonus_request.notes"placeholder="Additional Notes..."></textarea>
                                        </div>
                                         <div class="text-right" ng-hide="bonus_request.status == 'approved' || bonus_request.status == 'rejected' ">
                                            <a href="" class="btn btn-sm btn-default text-danger" ng-click="model.rejectBonusRequest($parent.$index, $index)" ng-disabled="bonus_request.loading">Reject</a>
                                            <a href="" class="btn btn-sm btn-success" ng-click="model.approveBonusRequest($parent.$index, $index)" ng-disabled="bonus_request.loading">Apporve</a>
                                        </div>
                                            <span ng-if="bonus_request.status == 'approved'"  class="label label-success">Approved</span>
                                            <span ng-if="bonus_request.status == 'rejected'"  class="label label-danger">
                                            Rejected</span>
                                            <span class="small" ng-if="bonus_request.status == 'approved'">by %%bonus_request.approved_bonus.approver.name%%</span>
                                            <span class="small" ng-if="bonus_request.status == 'rejected'">by %%bonus_request.approved_by.name%%</span>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    @endif
@endsection