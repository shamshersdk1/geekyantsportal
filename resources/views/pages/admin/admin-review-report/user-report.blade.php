@extends('layouts.admin-dashboard')
@section('main-content')
    <div class="container-fluid">
        <div class="breadcrumb-wrap">
           <div class="row">
              <div class="col-sm-8">
                 <h1 class="admin-page-title">Reporting for Timesheet for the month {{ $output['month_string'] }} of {{$output['user']['name'] ?? ''}}</h1>
                 <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="/admin/mis-review-report">MIS Report </a></li>
                    <li class="active">Timesheet of {{$output['user']['name'] ?? ''}} </li>
                 </ol>
              </div>
           </div>
        </div>
        <div class="m-b panel panel-default">
            <div class="panel-body">
                <form class="form-inline" method="get">
                    <div class="form-group col-sm-3 ">
                        @if($role=='admin')
                            <label for="exampleInputName2">User</label>
                            @if(isset($user))

                            <select id="selectid3" name="user"  style="width=35%;" placeholder= "Select name" required  >
                            <option value=""></option>

                                            @foreach($user as $user)
                                            @if(!empty($output['user']['name']) && $output['user']['name']==$user->name)
                                            <option value="{{$user->id}}" selected="selected">{{$user->name}}</option>
                                            @else
                                            <option value="{{$user->id}}" >{{$user->name}}</option>
                                            @endif

                                            @endforeach
                            </select>
                            @endif
                        @else

                        <label for="exampleInputName2">User</label>
                            @if(isset($user))

                            <select id="selectid3" name="user"  style="width=35%;" placeholder= "Select name" required  >
                            <option value=""></option>

                                            @foreach($parentUser as $user)
                                            @if(!empty($output['user']['name']) && $output['user']['name']==$user->name)
                                            <option value="{{$user->id}}" selected="selected">{{$user->name}}</option>
                                            @else
                                            <option value="{{$user->id}}" >{{$user->name}}</option>
                                            @endif
                                            @endforeach
                            </select>
                            @endif
                        @endif

                    </div>
                    <div class="form-group col-sm-3">
                        <label for="exampleInputEmail2">Month</label>
                                   <div>

                                   @if(isset($monthList))
                                        <select id="selectid4" name="monthId"  style="width:70%;height:40px" placeholder= "Select month" required >

                                            <option value=""></option>

                                            @foreach($monthList as $month)

                                            @if(date('F', mktime(0, 0, 0, $month->month, 10)).', '.$month->year== $output['month_string'] )
                                            <option value="{{$month->id}}" selected="selected">{{date('F', mktime(0, 0, 0, $month->month, 10)).', '.$month->year}}</option>
                                            @else
                                            <option value="{{$month->id}}" >{{date('F', mktime(0, 0, 0, $month->month, 10)).', '.$month->year}}</option>

                                            @endif
                                            @endforeach
                                        </select>

                        @endif

                                        </div>
                    </div>


                    <div class="form-group col-sm-3">
                    <button type="submit" class="btn btn-success"  style="margin-top:20px; width:100px;height:40px" >Submit</button>
                    </div>
                </form>
            </div>
        </div>
        @if ($output['error'] === true)
            <strong>{{$error_message}}</strong>
        @endif

        @if ($output['success'] == 1)
            <div class="row">
                <div class="col-lg-4">
                    <table class="table table-striped table-bordered bg-white">
                        <tr><td>Name</td><th>{{$output['user']['name']}}</th></tr>
                        <tr><td>Month</td><th>{{$output['month_string']}}</th></tr>
                        <tr><td>Start Date</td><th>{{date_in_view($output['month_start_date'])}}</th></tr>
                        <tr><td>End Date</td><th>{{date_in_view($output['month_end_date'])}}</th></tr>
                        <tr><td>Company Working Hrs</td><th>{{$output['total_working_hrs']}}</th></tr>
                        <tr><td>Company Working Days</td><th>{{$output['total_working_days']}}</th></tr>
                    </table>
                </div>
                <div class="col-lg-4">
                    <table class="table table-striped table-bordered bg-white">
                        <tr scope="col">
                            <th>Project Name</th>
                            <th>Employee Hrs</th>
                        </tr>
                        @foreach($project as $project)
                            <tr>
                                <td>{{$project->project_name ? $project->project_name : ''}}</td>
                                <td>{{$project->total}}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td><strong>Total</strong></td>
                            <td><strong>{{isset($timesheet['total_hour']) ? $timesheet['total_hour'] : ''}}</strong></td>
                        </tr>
                        <tr>
                        <td><strong>Total Approved Hours</strong></td>
                                <td><strong>{{isset($timesheet['total_hour']) && $timesheet['extra_hour'] ? $timesheet['total_hour']-$timesheet['extra_hour'] : 0}}</strong></td>
                        </tr>
                    </table>
                </div>
                <div class="col-lg-4">
                    <table class="table table-striped table-bordered bg-white">
                        <tr>
                            <th>% Variable Pay (Approved)</th>
                            <th>% Variable Pay (Employee)</th>
                        </tr>
                        <tr>
                            <td>{{round($output['grant_approved_total_time']/($output['total_working_hrs']/100),0)}}%</td>
                            <td>{{round($output['grant_employee_total_time']/($output['total_working_hrs']/100),0)}}%</td>
                        </tr>
                    </table>

                    <i>*50% on Timesheet of Total Variable Pay</i><br /><br />

                    <b>Extra Hour(s) Request</b><br /><br />
                    <table class="table table-bordered table-condensed table-hover bg-white">
                        <tr>
                            <th  width="35%">Date</th>
                            <th class="text-center" width="15%">Hours</th>
                            <th  class="text-center"width="10%">Status</th>
                        </tr>

                                @if(!empty($userExtra) && count ($userExtra)>0 )
                                    @foreach($userExtra as $user_extra)
                                    <tr>

                                        <td >{{$user_extra->date ? date_in_view($user_extra->date) :''}}</td>
                                        <td class="text-center">{{$user_extra->extra_hours ? $user_extra->extra_hours : 0}}</td>
                                        <td>

                                            @if($user_extra->status && $user_extra->status==='approved')
                                                <span class="label label-success">{{$user_extra->status}}</span>
                                            @else
                                                <span class="label label-danger">{{$user_extra->status}}</span>
                                            @endIf
                                        </td>
                                    </tr>

                                    @endforeach
                                @else
                                <td colspan="4" class="text-center">No Records Found</td>
                                @endIf

                    </table>
                </div>

            </div>
            <div class="row">
                <div class="col-sm-12">
                <b>Bonus Request</b>
                <table class="table table-bordered table-condensed table-hover bg-white">
                    <tr>
                        <th>Type </th>
                        <th>Date</th>
                        <th>Hours Approved</th>
                        <th>Status</th>
                        <th>Updated By</th>
                        <!-- <th>Amount</th> -->
                    </tr>

                    @if($data['bonuses']['bonus_requests'] && count($data['bonuses']['bonus_requests'])>0)
                        @foreach($data['bonuses']['bonus_requests'] as $bonusRequest)
                        @if($bonusRequest)
                        <tr>

                            <td>{{$bonusRequest->type ? $bonusRequest->type : ''}}</td>
                            <td>{{$bonusRequest->date ? date_in_view($bonusRequest->date):''}}</td>
                            <td>
                            @if($bonusRequest->redeem_type)
                             NA
                             @endif
                            </td>
                            <td>
                                @if($bonusRequest->status && $bonusRequest->status=='approved')
                                    <span class="label label-success">{{$bonusRequest->status}}</span>
                                    @else
                                    <span class="label label-danger">{{$bonusRequest->status}}</span>

                                @endif
                            </td>
                            <td>
                            @if(!empty($bonusRequest->approvedBy) && $bonusRequest->approvedBy->name && $bonusRequest->updated_at)
                            {{$bonusRequest->approvedBy->name }} on  {{datetime_in_view($bonusRequest->updated_at)}}
                            @endif
                            </td>
                        </tr>
                    @endif
                        @endforeach
                    @endif

                </table>
                </div>
            </div>


            <table class="table table-bordered table-hover bg-white">
                <thead>
                    <tr>
                        <th scope="col">Date</th>
                    @foreach($output['project_map'] as $project_id => $project)
                        <th scope="col">{{$project['project']->project_name}}</th>
                    @endforeach
                        <th scope="col">Employee Total Hrs</th>
                        <th scope="col">Approved Total Hrs</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($output['date_map'] as $date => $project_wise)
                    <tr @if($project_wise['leave'] !== null)
                        class = "danger"
                        @elseif($project_wise['is_weekend'] === true)
                        class = "warning"
                        @elseif($project_wise['is_holiday'] !== false)
                        class = "info"
                        @endif>
                        <td>
                            {{date_in_view($date)}}
                            @if($project_wise['leave'] !== null)
                                ({{$project_wise['leave']->type}} leave)
                            @elseif($project_wise['is_weekend'] === true)
                                <i>(Weekend)</i>
                            @elseif($project_wise['is_holiday'] !== false)
                                <strong>Holiday</strong><i>{{$project_wise['is_holiday']->reason}}</i>
                            @endif
                        </td>
                        @foreach($project_wise['projects'] as $project_id => $project_timesheet)
                            <td>
                                @if(count($project_timesheet) > 0)
                                    @foreach($project_timesheet as $timesheet)
                                        @if($timesheet->status ==='approved')
                                            <span class="label label-success">Approved</span>&nbsp;
                                        @else
                                            <span class="label label-warning">Pending</span>&nbsp;
                                        @endif
                                        {{$timesheet->task}} [{{$timesheet->duration}}]

                                        <br />
                                    @endforeach
                                @endif
                            </td>
                        @endforeach
                        <td>{{$project_wise['employee_total_time']}}</td>
                        <td>{{ $project_wise['employee_total_time']<= 8 ?$project_wise['employee_total_time']  : 8}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @endif
@endsection
