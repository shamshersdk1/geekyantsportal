@extends('layouts.admin-dashboard')
@section('main-content')
    <div class="container-fluid mis-report">
        <div class="breadcrumb-wrap">
           <div class="row">
              <div class="col-sm-8">
                 <h1 class="admin-page-title">MIS Report for {{$monthObj->formatMonth()}}</h1>
                 <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li class="active">MIS Report </li>
                 </ol>
              </div>
           </div>
        </div>

                <table class="table table-bordered table-hover bg-white">
                    <tr>
                        <th>User Name</th>
                        <th width="120px" class="text-center">Employee Hrs</th>
                        <th width="120px" class="text-center">Approved Hrs</th>
                        <th width="120px" class="text-center">Action(s)</th>
                    </tr>
                    @foreach($timesheetDetails as $timesheetDetail)
                        <tr>
                            <td>{{$timesheetDetail['name']}}</td>
                            <td width="120px" class="text-center">{{$timesheetDetail['total_hour']?$timesheetDetail['total_hour']:0}}</td>
                            <td width="120px" class="text-center">{{$timesheetDetail['total_hour']?$timesheetDetail['total_hour']-$timesheetDetail['extra_hour']:0}}</td>
                            <td width="120px" class="text-center"><a class="btn btn-sm btn-success" href="{{url('/admin/mis-review-report/'.$monthObj->id.'/'. $timesheetDetail['user_id'] . '/' .'review')}}"><i class="fa fa-file-o"></i> &nbsp; View Report</a> </td>

                        </tr>
                    @endforeach
                </table>
    </div>
@endsection
