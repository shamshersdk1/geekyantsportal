@extends('layouts.admin-dashboard')
@section('main-content')
    <div class="container-fluid mis-report">
        <div class="breadcrumb-wrap">
           <div class="row">
              <div class="col-sm-8">
                 <h1 class="admin-page-title">MIS Report Month</h1>
                 <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li class="active">MIS Report </li>
                 </ol>
              </div>
           </div>
        </div>

        
                <table class="table table-bordered table-hover bg-white">
                    <tr>
                        <th>#</th>
                        <th width="120px" class="text-center">Month</th>
                        <th width="120px" class="text-center">Timesheet Review</th>
                        <th width="120px" class="text-center">Action(s)</th>
                    </tr>
                    @foreach($months as $month)
                        <tr>
                            <td width="120px">{{$month->id}}</td>
                            <td width="120px" class="text-center">
                                {{$month->formatMonth()}}
                            </td>
                            <td>
                                @if(!$month->feedback_review_check)
                                    <span class="label label-primary custom-label">Pending Review</span>
                                @else
                                    <span class="label label-success custom-label">Completed</span>
                                @endif

                            </td>
                            
                            <td width="120px" class="text-center">
                                <a class="btn btn-sm btn-success" href="{{url('/admin/mis-review-report/'.$month->id)}}"><i class="fa fa-file-o"></i> &nbsp; Review </a>
                                    
                            </td>
                        </tr>
                    @endforeach
                </table>
    </div>
@endsection