@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-6">
                <h1 class="admin-page-title">Bank Transfer</h1>
                <ol class="breadcrumb">
                      <li><a href="/admin">Admin</a></li>
                      <li><a href="/admin/bank-transfer">Bank Transfer</a></li>
                     <li><a href="/admin/bank-transfer">{{$currMonth ? $currMonth->formatMonth() : ' '}}</a></li>
        		</ol>
            </div>
		</div>
    </div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		            @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
            @endif
            @if(session('alert-class'))
                <div class="alert alert-danger">
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ session('alert-class') }}</span><br/>
		        </div>
            @endif
	    </div>
    </div>

    @if($bankTransferObj->transferUsers->count() > 0)
    <div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel panel-default">
				<table class="table table-striped" id="bank-table">
				<thead>
                    <th class="td-text">#</th>
                    <th class="td-text">Employee Id</th>
                    <th class="td-text">User</th>
					<th class="td-text">Amount</th>
				</thead>
					@foreach($bankTransferObj->transferUsers as $index=>$bankTransfer)
						<tr>
                            <td class="td-text">{{$index + 1}}</td>
                            <td class="td-text">{{$bankTransfer->user->employee_id}}</td>
                            <td class="td-text">{{$bankTransfer->user->name}}</td>
                            <td class="td-text">{{$bankTransfer->amount}}</td>
						</tr>
					@endforeach
				</table>
			</div>
		</div>
    </div>
</div>
    @endif

    @if($bankTransferObj->holdUsers->count() > 0)
    <div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel panel-default">
				<table class="table table-striped" id="bank-table2">
				<thead>
                    <th class="td-text">#</th>
                    <th class="td-text">Employee Id</th>
                    <th class="td-text">User</th>
					<th class="td-text">Amount</th>
				</thead>
					@foreach($bankTransferObj->holdUsers as $index=>$bankTransfer)
						<tr>
                            <td class="td-text">{{$index + 1}}</td>
                            <td class="td-text">{{$bankTransfer->user->employee_id}}</td>
                            <td class="td-text">{{$bankTransfer->user->name}}</td>
                            <td class="td-text">{{$bankTransfer->amount}}</td>
						</tr>
					@endforeach
				</table>
			</div>
		</div>
    </div>
    @endif
</div>
</div>
<script>
    $(document).ready(function() {
        var t = $('#bank-table').DataTable( {
            pageLength:500, 
            "columnDefs": [ {
                "searchable": false,
                "orderable": false,
                "targets": 0
            } ],
            "order": [[ 1, 'asc' ]],
            fixedHeader: true
        } );
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();

        var t1 = $('#bank-table2').DataTable( {
            pageLength:500, 
            "columnDefs": [ {
                "searchable": false,
                "orderable": false,
                "targets": 0
            } ],
            "order": [[ 1, 'asc' ]],
            fixedHeader: true
        } );
        t1.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    });

    $('#selectid2').change(function(){
        var optionSelected = $("option:selected", this);
        optionValue = this.value;
        if (optionValue) { 
            window.location = "/admin/bank-transfer/"+optionValue; 
        }
    });
</script>
@endsection

