@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-6">
                <h1 class="admin-page-title">Bank Transfer</h1>
                <ol class="breadcrumb">
                      <li><a href="/admin">Admin</a></li>
                      <li><a href="/admin/bank-transfer">Bank Transfer</a></li>
                     <li><a href="/admin/bank-transfer">{{$bankTransfers->id}}</a></li>
        		</ol>
            </div>
           <div class="col-md-6">
                    @if($currMonth && (count($bankTransfers) > 0))

                        <div class="pull-right">
                        <a class="btn btn-primary" href="lock">
                            <i class="fa fa-lock"></i> &nbsp; Lock
                        </a>
                        </div> 
                    @endif
                </div>

		</div>
    </div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		            @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
            @endif
            @if(session('alert-class'))
                <div class="alert alert-danger">
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ session('alert-class') }}</span><br/>
		        </div>
            @endif
	    </div>
    </div>
    <div class="panel panel-default">
				<div class="panel-heading">
					<div class="row">
						<div class="col-sm-8">
							<span>Month: <label>{{$bankTransfers->month->formatMonth()}}</label></span><br>
							<span>Status: <label>
								@if($bankTransfers->status == 'open')
								<span class="label label-warning">Open</span>
								@elseif($bankTransfers->status == 'completed')
								<span class="label label-success">Completed</span>
								@elseif($bankTransfers->status == 'rejected')
								<span class="label label-danger">Rejected</span>
								@else
								@endif    
							</label></span></br>
							<span>Created At: <label>{{datetime_in_view($bankTransfers->created_at)}}</label></span><br>
						</div>
                        <form action="/admin/bank-transfer/{{$bankTransfers->id}}/view-complete/upload" method="post" style="display:inline" enctype="multipart/form-data">
                            <input id="fileupload" type="file" name="file" required class="form-control" style="border:0">
                            {{ Form::button('<i class="fa fa-upload fa-fw"></i>Upload', ['type'=> 'submit','class' => 'btn btn-success btn-sm'] )  }}
                        </form>
					</div>
                </div>
			</div>

    @if($overallStatus == 'pending')
        <a href="/admin/bank-transfer/{{$bankTransfers->id}}/complete-all" class='btn btn-warning btn-sm'><i class="fa fa-check fa-fw"></i>Complete All</a>
        <a href="/admin/bank-transfer/{{$bankTransfers->id}}/reject-all" class='btn btn-danger btn-sm'><i class="fa fa-times fa-fw"></i>Reject All</a>
        {{ Form::open(['url' => '/admin/bank-transfer/save-all', 'method' => 'post']) }}
        {{ Form::button('<i class="fa fa-save fa-fw"></i>Save All', ['type'=> 'submit','class' => 'btn btn-success btn-sm'] )  }}
    @endif
    
    <div class="row">
		<div class="col-md-12">
                <div class="row">
                    <div class="pull-right m-t-1 ">
                        
                    </div>
                </div>
			<div class="panel panel-default">
				<div class="panel panel-default">
				<table class="table table-striped" id="bank-table">
				<thead>
                    <th class="">#</th>
                    <th class="">Employee Id</th>
					<th class="">Employee Name</th>
                    <th class="">Bank Account Number</th>
                    <th class="">Amount</th>
                    <th class="">Transaction Amount</th>
                    <th class="">Mode of Transfer</th>
                    <th class="">Bank Transaction ID</th>
                    <th class="">Comment</th>
                    <th class="">Status</th>
                    <th class="">Txn Posted Date</th>
                    <th class="text-center">Actions</th>
                    
				</thead>
				@if(count($bankTransfers) > 0)
					@foreach($bankTransfers->transferUsers as $index=>$bankTransfer)
                     
                        <tr>
                            <td>{{$index+1}}</td>
                            <td class="">{{$bankTransfer->user->employee_id}}</td>
							<td class="">{{$bankTransfer->user->name}}</td>
                            <td class="">{{$bankTransfer->bank_acct_number}}</td>
                            <td class="">{{$bankTransfer->amount}} </td>
                            @if($bankTransfer->status == null || $bankTransfer->status == '' || $bankTransfer->status == 'pending')
                                <td class=""> 
                                    {{ Form::number($bankTransfer->id.'[transaction_amount]',$bankTransfer->transaction_amount, ['class' => 'form-control']) }}                  
                                </td>
                                <td class="">
                                    {{ Form::text($bankTransfer->id.'[mode_of_transfer]', $bankTransfer->mode_of_transfer ? $bankTransfer->mode_of_transfer : '', ['class' => 'form-control', 'placeholder' => 'NEFT']) }}
                                </td>
                                <td class="">
                                    {{ Form::text($bankTransfer->id.'[bank_transaction_id]', $bankTransfer->bank_transaction_id, ['class' => 'form-control']) }}
                                </td>
                                <td class="">
                                    {{Form::textarea($bankTransfer->id.'[comment]', $bankTransfer->comment , ['rows'=> 2, 'class'=>"form-control"])}}
                                </td>
                                <td>
                                    <span class="label label-primary">Pending<span>
                                </td>
                                <td class="">{{$bankTransfer->txn_posted_date}} </td>
                                <td class="text-center">
                                    @if($bankTransfer->status != 'completed' && $bankTransfer->status != 'rejected')
                                    <a href="/admin/bank-transfer/{{$bankTransfer->id}}/complete" class='btn btn-warning btn-sm'><i class="fa fa-check fa-fw"></i>Complete</a>
                                    <a href="/admin/bank-transfer/{{$bankTransfer->id}}/reject" class='btn btn-danger btn-sm'><i class="fa fa-times fa-fw"></i>Reject</a>
                                    @endif
                                </td>
                            @else 
                                <td class=""> 
                                    {{$bankTransfer->transaction_amount}}
                                </td>
                                <td class="">
                                    {{$bankTransfer->mode_of_transfer}}
                                </td>
                                <td class="">
                                    {{$bankTransfer->bank_transaction_id}}
                                </td>
                                <td class="">
                                    {{$bankTransfer->comment}}
                                </td>
                                <td>
                                    @if($bankTransfer->status == 'completed')
                                        <span class="label label-warning">Completed<span>
                                    @elseif($bankTransfer->status == 'rejected')
                                        <span class="label label-danger">Rejected<span>
                                    @endif
                                </td>
                                <td class="">{{$bankTransfer->txn_posted_date}} </td>
                                <td class="text-center">
                                    
                                </td>
                            @endif
						</tr>
                    @endforeach
				@else
					<tr >
						<td colspan="8" class="text-center">No Record Found For Selected Month.</td>
					</tr>
				@endif
				</table>
            </div>
        </div>
		</div>
        {{ Form::close() }}
	</div>
</div>
@endsection

