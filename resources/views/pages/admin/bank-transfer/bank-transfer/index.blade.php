@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-6">
                <h1 class="admin-page-title">Bank Transfer</h1>
                <ol class="breadcrumb">
                      <li><a href="/admin">Admin</a></li>
                      <li><a href="/admin/bank-transfer">Bank Transfer</a></li>
                     <li><a href="/admin/bank-transfer">{{$currMonth ? $currMonth->formatMonth() : ' '}}</a></li>
        		</ol>
            </div>
            <div class="col-sm-6">
                <div style="display:inline-block;" class="lockform">
						{{ Form::open(['url' => '/admin/payslips', 'method' => 'get']) }}
						{{ Form::submit('View Payslip',['class' => 'btn btn-warning crude-btn']) }}
						{{ Form::close() }}
					</div>
            
                <div class="form-group col-md-6 ">
                    <div class="col-md-12 float-right" style="background-color: white;padding:0px;">
                    @if(isset($monthList))
                        <select id="selectid2" name="month"  placeholder= "{{$currMonth ? $currMonth->formatMonth() : 'Select Month'}}">
                            <option value=""></option>
                            @foreach($monthList as $x)
                                <option value="{{$x->id}}" >{{$x->formatMonth()}}</option>
                            @endforeach
                        </select>
                    @endif
                    </div>
                </div>
                
            </div>

		</div>
    </div>
    <div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel panel-default">
				<table class="table table-striped" id="bank-table">
				<thead>
                    <th class="td-text">#</th>
                    <th class="td-text">Created At</th>
                    <th class="td-text">Hold Users</th>
                    <th class="td-text">Processed Users</th>
                    <th class="td-text">Status</th>
                    <th class="text-right">Action</th>
				</thead>
				@if(count($bankTransfers) > 0)
					@foreach($bankTransfers as $index=>$bankTransfer)
						<tr>
                            <td class="td-text">{{$index + 1}}</td>
                             <td class="td-text">{{datetime_in_view($bankTransfer->created_at)}}</td>
                            <td class="td-text">{{$bankTransfer->holdUsers->count()}} </td>
                            <td class="td-text">{{$bankTransfer->transferUsers->count()}} </td>
                            <td class="td-text">{{$bankTransfer->status}}</td>
                            <td class="text-right">
                                <!-- <a class="btn btn-primary btn-sm crude-btn" href="bank-transfer/{{$bankTransfer->id}}/download"><i class="fa fa-download"></i> &nbsp;Download CSV</a> -->
                                <a class="btn btn-primary btn-sm crude-btn" href="/admin/bank-transfer/{{$bankTransfer->id}}/salary-sheet"> &nbsp;Salary Sheet</a>
                                <a class="btn btn-success btn-sm crude-btn" href="/admin/bank-transfer/{{$bankTransfer->id}}/view-complete"> &nbsp;View</a>
                                @if(count($bankTransfer->holdUsers) > 0)
                                    <a class="btn btn-danger btn-sm crude-btn" href="/admin/bank-transfer/{{$bankTransfer->id}}/hold"> &nbsp;View Hold(s)</a>
                                @endif
                            </td>
						</tr>
					@endforeach
				@else
					<tr >
						<td colspan="8" class="text-center">No Record Found For Selected Month.</td>
					</tr>
				@endif
				</table>
			</div>
		</div>
	</div>
</div>
<script>
    $(document).ready(function() {
        var t = $('#bank-table').DataTable( {
            pageLength:500, 
            "columnDefs": [ {
                "searchable": false,
                "orderable": false,
                "targets": 0
            } ],
            "order": [[ 1, 'asc' ]],
            fixedHeader: true
        } );
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    });

    $('#selectid2').change(function(){
        var optionSelected = $("option:selected", this);
        optionValue = this.value;
        if (optionValue) { 
            window.location = "/admin/bank-transfer/"+optionValue; 
        }
    });
</script>
@endsection

