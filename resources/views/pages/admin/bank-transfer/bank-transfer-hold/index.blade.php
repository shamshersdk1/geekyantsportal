@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-6">
                <h1 class="admin-page-title">Bank Transfer Hold</h1>
                <ol class="breadcrumb">
                      <li><a href="/admin">Admin</a></li>
                      <li><a href="/admin/bank-transfer">Bank Transfer</a></li>
                      <li><a href="/admin/bank-transfer">Hold</a></li>
        		</ol>
            </div>
		</div>
    </div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		            @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
            @endif
            @if(session('alert-class'))
                <div class="alert alert-danger">
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ session('alert-class') }}</span><br/>
		        </div>
            @endif
	    </div>
    </div>

    <div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
                <label>Bank Transfer hold </label><br/>
                <label>TimeStamp: {{datetime_in_view($bankTransferHold->created_at)}}</label><br/>
                <label>Status: {{$bankTransferHold->status}}</label>
            </div>
        </div>
    </div>
    <div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel panel-default">
				<table class="table table-striped" id="bank-table">
				<thead>
                    <th class="td-text">#</th>
                    <th class="td-text">Employee Id</th>
					<th class="td-text">Employee Name</th>
                    <th class="td-text">Amount</th>
                    <th class="td-text">Created At</th>
                    <th class="td-text">Updated At</th>
                    <th class="td-text">Action</th>
				</thead>
				@if(count($bankTransferHold->holdUsers) > 0)
					@foreach($bankTransferHold->holdUsers as $index=>$bankTransfer)
						<tr>
                            <td class="td-text">{{$index + 1}}</td>
                            <td class="td-text">{{$bankTransfer->user->employee_id}}</td>
							<td class="td-text">{{$bankTransfer->user->name}}</td>
                            <td class="td-text">{{$bankTransfer->amount}} </td>
                            <td class="td-text">{{datetime_in_view($bankTransfer->created_at)}}</td>
                            <td class="td-text">{{datetime_in_view($bankTransfer->updated_at)}} </td>
                            <td class="td-text">
                                @if($bankTransfer->is_processed)
                                    Processed
                                @else
                                <form name="showForm" method="get" action="/admin/bank-transfer/{{$bankTransferHold->id}}/hold/{{$bankTransfer->id}}"  style="display: inline-block;">
                                    <button type="submit" class="btn btn-primary btn-sm crude-btn">Release</button> 
                                </form>
                                @endif
                            </td>
						</tr>
					@endforeach
				@else
					<tr >
						<td colspan="6" class="text-center">No Record Found For Selected Month.</td>
					</tr>
				@endif
				</table>
			</div>
		</div>
	</div>
</div>
<script>
    $(document).ready(function() {
        var t = $('#bank-table').DataTable( {
            pageLength:500, 
            "columnDefs": [ {
                "searchable": false,
                "orderable": false,
                "targets": 0
            } ],
            "order": [[ 1, 'asc' ]],
            fixedHeader: true
        } );
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    });

    $('#selectid2').change(function(){
        var optionSelected = $("option:selected", this);
        optionValue = this.value;
        if (optionValue) { 
            window.location = "/admin/bank-transfer-holds/"+optionValue; 
        }
    });
</script>
@endsection
