@extends('layouts.admin-dashboard')
@section('main-content')

<section class="new-project-section">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">Pricing</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
			  			<li><a href="{{ url('admin/project') }}">Projects</a></li>
						<li><a href="/admin/project/{{$id}}/dashboard">{{$id}}</a></li>
						<li>Resource Price</li>
						<li class="active">Update</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>

	    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif

		<form class="form-horizontal" method="post" action="/admin/project/{{$id}}/resource-price/update" enctype="multipart/form-data">
		 <input type="hidden" id="resource_id" name="resource_id" value={{$resource_price->id}}>
			<div class="panel panel-default">
				<div class="panel-body">
				    <div class="row">
				    	<div class="col-md-12">
						<div class="form-group">
								<label for="" class="col-sm-2 control-label">Project Name</label>
								<div class="col-sm-4">
									<input type="text" class="form-control" value="{{$resource_price->project->project_name}}" disabled>
								</div>	
							</div>
						  	<div class="form-group">
								<label for="" class="col-sm-2 control-label">Resource Type</label>
								<div class="col-sm-4">
									<input type="text" class="form-control" id="" name="type" value={{$resource_price->type}}>
								</div>
							</div>
							<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Hourly Rate</label>
						    	<div class="col-sm-4">
						      		<input type="number" class="form-control" id="" name="price" value={{$resource_price->price}}>
						    	</div>
						  	</div>
					  	</div>
					</div>
				</div>
			</div>
  	  		<div class="text-center">
		  		<button type="submit" class="btn btn-success">Update</button>
		  	</div>
		</form>
	</div>
</section>
@endsection
