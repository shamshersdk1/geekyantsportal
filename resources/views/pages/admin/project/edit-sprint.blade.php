
@extends('layouts.admin-dashboard')
@section('main-content')
<section class="add-sprint">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">Edit Sprint for {{$projectinfo->project_name}}</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
			  			<li><a href="{{ url('admin/project') }}">Projects</a></li>
			  			<li class="active">Edit Sprint</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>
	    <div class="col-sm-12" ng-app="sampleApp" ng-controller="myCtrl">
	        <form class="form-horizontal" action="/admin/project/{{$projectId}}/store-sprint" method='post'>
				<input type='hidden' name='project_id' value={{$projectId}}>
					<!-- <input type='hidden' name='lastSprintID' value={{$lastSprintID}}> -->
				@if(!empty($errors->all()))
		            <div class="alert alert-danger">
		                @foreach ($errors->all() as $error)
		                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                    <span>{{ $error }}</span><br/>
		                  @endforeach
		            </div>
		        @endif
		        @if (session('message'))
		            <div class="alert alert-success">
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ session('message') }}</span><br/>
		            </div>
		        @endif

			    <div class="row">
					<div class="col-sm-4">
						<h4 class="admin-section-heading">Basic Info</h4>
						<div class="panel panel-default">
							<!-- <div class="manager-name-holder project-name-holder">
				    			{{$projectinfo->project_name}}
				    		</div> -->
							<div class="info-table">
								<label>Satrt Date: </label>
								<span>{{$projectinfo->start_date}}</span>
							</div>
							<div class="info-table">
								<label>End Date: </label>
								<span>{{$projectinfo->end_date}}</span>
							</div>
							<div class="info-table">
								<label>Team Lead: </label>
								<span>{{$projectinfo->project_manager}}</span>
							</div>
							<div class="info-table">
								<label>Account Manager: </label>
								<span>{{$projectinfo->account_manager}}</span>
							</div>
							<div class="info-table">
								<label>BD Manager: </label>
								<span>{{$projectinfo->bd_manager}}</span>
							</div>
						</div>

						<h4 class="admin-section-heading">Technologies</h4>
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="tags text-center">
							      	@if(count($technologies) > 0)
						      		@foreach($technologies as $technology)
							    		<a href="javascriprt:;">{{$technology->name}}</a> 
							    	@endforeach
								    @else
								    	<h5> No technology has been added </h5>
								    @endif
							    </div>
							</div>
						</div>
					</div>

					<div class="col-sm-8">
						<div class="panel panel-default">
						  	<div class="panel-body">
						  		<div class="row">
							    	<div class="col-sm-3">
							    		<label for="text">Sprint Number</label>
								      	<input type="number" class="form-control" id="" value={{$lastSprintID}} disabled>
								    </div>
							  		<div class="col-sm-9">
								    	<label for="sprinttitle">Sprint Title</label>
								      	<input type="text" class="form-control" id="sprinttitle" name="sprint_title"  value="{{Request::old('sprint_title')}}">
							  		</div>
							  	</div>
							  	<div class="row m-t-10">
								    <div class="col-sm-12">
								    	<label for="sprinttitle">Sprint Description</label>
								      	<textarea class="form-control" rows="5" style="resize: none;" name="sprint_desc">{{Request::old('sprint_desc')}}</textarea>
							  		</div>
							  	</div>
							  	<div class="row m-t-10">
							  		<div class="col-md-6">
				                        <div class='input-group date' id='datetimepicker1'>
				                            <input type='text' class="form-control" placeholder="Chosse Start Date"  name="start_date" value="{{Request::old('start_date')}}" id="txtStartDate"/>
				                            <span class="input-group-addon">
				                                <span class="glyphicon glyphicon-calendar"></span>
				                            </span>
				                        </div>
				                    </div>
				                    <div class="col-md-6">
				                        <div class='input-group date' >
				                            <input type='text' class="form-control" placeholder="Chosse End Date"  name="end_date" value="{{Request::old('end_date')}}"/ id="txtEndDate">
				                            <span class="input-group-addon">
				                                <span class="glyphicon glyphicon-calendar"></span>
				                            </span>
				                        </div>
					                </div>
					            </div>
							</div>	
						</div>
					</div>
					
					
						
						
	<!-- ==============================================for selected user============================================== -->					
						<div class="panel panel-default">
						  	<div class="panel-body">
					    		<div class="row">
									<div ng-repeat="resource in resources" class="col-md-3" ng-if="resource.checked">

						    			<div class="sprint-grid" >
						    				<div class="sprint-img-holder">
						    					<img src="http://ingridwu.dmmdmcfatter.com/wp-content/uploads/2015/01/placeholder.png" alt="" class="img-responsive">
						    				</div>
						    				<h5 class="sprint-title"><%resource.name%></h5>
						    				<div class="caption">
						    					<p class="sprints-description">small description of project, Lorem ipsum dolor sit amet, consectetur</p>
						    					<div class="sprint-details">
						    					
						    						<div class="input-group input-group-sm">
						    							<span class="input-group-addon">
													        <input type="checkbox" name= "user[]" value='<%resource.id%>' ng-model="resource.checked">
													    </span>
						    							 <input type="text" placeholder="No. of hr" name='hrs[<%resource.id%>]' class="form-control" ng-model="resource.hrs">
						    							<span class="input-group-btn">
						    								<button type="button" class="btn btn-default"><i class="fa fa-plus-circle fa-lg" aria-hidden="true"></i></button>
						    							</span>
						    						</div>
						    					</div>
						    				</div>
						    			</div>
						    		</div>
					    		</div>
				    		</div>
			    		</div>
						<div class="text-center">
				    		<button class="btn btn-default" name="action" value="save">Save</button>
				    		<button class="btn btn-success" name="action" value="saveandgotodashboard">Save and Goto Dashboard</button>  
				    	</div>
				    	<div class="row text-center sprint-save"></div>
					
				</div>
			</form>


<!-- ==============================================for non select user============================================== -->
					<h4>Search Resource</h4>
					<label>Search: <input ng-model="searchText.name"></label>
					<div class="panel panel-default">
					  	<div class="panel-body">
				    		<div class="row">
					    		<div ng-repeat="resource in resources | filter:searchText" class="col-md-3" ng-hide="resource.checked">
					    			<div class="sprint-grid" >
					    				<div class="sprint-img-holder">
					    					<img src="http://ingridwu.dmmdmcfatter.com/wp-content/uploads/2015/01/placeholder.png" alt="" class="img-responsive">
					    				</div>
					    				<h5 class="sprint-title"><%resource.name%></h5>
					    				<div class="caption">
					    					<p class="sprints-description">small description of project, Lorem ipsum dolor sit amet, consectetur</p>
					    					<div class="sprint-details">
					    					
					    						<div class="input-group input-group-sm">
					    							<span class="input-group-addon">
												        <input type="checkbox" name= "user[]" value='<%resource.id%>' ng-model="resource.checked">
												    </span>
					    							 <input type="text" placeholder="No. of hr" name='hrs[<%resource.id%>]' class="form-control" ng-model="resource.hrs">
					    							<span class="input-group-btn">
					    								<button type="button" class="btn btn-default"><i class="fa fa-plus-circle fa-lg" aria-hidden="true"></i></button>
					    							</span>
					    						</div>
					    					</div>
					    				</div>
					    			</div>
					    		</div>
				    		</div>
				    	</div>
				    </div>

				    

					

			    	<!-- ==================== end section =============== -->
			    	
		</div>	    	
    </div>
</section>

@endsection
@section('js')

<script type="text/javascript">
	var app = angular.module('sampleApp', [], function($interpolateProvider) {
		console.log("asdfa");
	   $interpolateProvider.startSymbol('<%');
	   $interpolateProvider.endSymbol('%>');
	   
	});

    app.controller('myCtrl', function($scope, $http) {
    	$scope.searchText;
  //   	$scope.resourcechecked = function(){
		//   $scope.resources = [];
		//   angular.forEach($scope.resource, function(album){
		//     if (!!album.selected) $scope.albumNameArray.push(album.name);
		//   })
		// }
    	// $scope.resource;
    	// console.log("resournce". $scope.resource);
    	$http({
          method: 'GET',
          url: 'http://public.local.geekydev.com/admin/resourceFilter'
        }).then(function successCallback(response) {
            console.log("response",response);
            $scope.resources = response.data;
          }, function errorCallback(response) {
          	console.log("error",response);
            // called asynchronously if an error occurs
            // or server returns response with an error status.
          });
        return false;
    });
</script>

  
@endsection