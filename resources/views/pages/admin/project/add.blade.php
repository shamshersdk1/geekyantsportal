@extends('layouts.admin-dashboard')
@section('main-content')

<section class="new-project-section">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">Add new Project</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
			  			<li><a href="{{ url('admin/project') }}">Projects</a></li>
			  			<li class="active">Add</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>

	    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif

		<form class="form-horizontal" method="post" action="/admin/project" enctype="multipart/form-data">
			<div class="panel panel-default">
				<div class="panel-body">
				    <div class="row">
				    	<div class="col-md-12">
						  	<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Client Name</label>
						    	<div class="col-sm-6">
						    		@if(isset($companies))
										<select id="selectid2" name="company_id"  style="width=35%;" placeholder= "Select an option">
											<option value=""></option>
											@foreach($companies as $x)
									        	<option value="{{$x->id}}" >{{$x->name}}</option>
										    @endforeach
										</select>
									@endif
									@if(isset($company))
										<div>{{$company->name}}</div>
										<input type="hidden" name="company_id" value="{{$company->id}}">
									@endif
						    	</div>
						    	<!--<div class="col-sm-4">
						    		<a href="" class="btn btn-success"> Add New Client</a>
						    	</div>-->
						  	</div>
						  	<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Project Name</label>
						    	<div class="col-sm-6">
						      		<input type="text" class="form-control" id="" name="project_name" value="{{ old('project_name') }}">
						    	</div>
						  	</div>
						  	<div class="form-group">
								<div class="col-sm-3 col-sm-offset-2">
									<div class='input-group date' id='datetimepicker1'>
									<input type='text' name="start_date" placeholder = "Start Date" class="form-control" />
										<span class="input-group-addon">
											<span><i class="fa fa-calendar" aria-hidden="true"></i></span>
										</span>
									</div>
								</div>
								<div class="col-sm-3">
									<div class='input-group date' id='datetimepicker2'>
										<input type='text' name="end_date" placeholder = "End Date" class="form-control" />
											<span class="input-group-addon">
												<span><i class="fa fa-calendar" aria-hidden="true"></i></span>
											</span>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">B D Manager</label>
								<div class="col-sm-6">
									@if(isset($users))
										<select id="selectid3" name="bd_manager" style="width=35%;" placeholder= "Select an option">
											<option value=""></option>
												@foreach($users as $x)
													<option value="{{$x->id}}" >{{$x->name}}</option>
												@endforeach
										</select>
									@endif
								</div>
							</div>
						  	<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Account Manager</label>
				    	    	<div class="col-sm-6">						      		
				    				<select id="selectid4" name="account_manager" style="width=35%;" placeholder= "Select an option">
			    						@if(isset($users))
			    							<option value=""></option>
			    							@foreach($users as $x)
			    					        	<option value="{{$x->id}}" >{{$x->name}}</option>
			    						    @endforeach
			    						@endif
				    				</select>
				    	    	</div>
						  	</div>
						  	<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Team Lead</label>
				    	    	<div class="col-sm-6">						      		
									@if(isset($users))
										<select id="selectid22" name="project_manager_id" style="width=35%;" placeholder= "Select an option">
											<option value=""></option>
												@foreach($users as $x)
													<option value="{{$x->id}}" >{{$x->name}}</option>
												@endforeach
										</select>
									@endif
				    	    	</div>
						  	</div>
							  <div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Project Category</label>
				    	    	<div class="col-sm-6">						      		
				    				<select id="selectid2" name="project_category" class="form-control" style="">
			    						@if(isset($categories))
			    							<option value="">Select</option>
			    							@foreach($categories as $x)
			    					        	<option value="{{$x->id}}" >{{$x->name}}</option>
			    						    @endforeach
			    						@endif
				    				</select>
				    	    	</div>
						  	</div>
						  	<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Status</label>
						    	<div class="col-sm-6">
						      		<select class="form-control" name="status">
									  	<option value="open">Open</option>
									  	<option value="initial">Initial</option>
									  	<option value="sow">SOW</option>
									  	<option value="nda">NDA</option>
									  	<option value="assigned">Assigned</option>
									  	<option value="in_progress">In progress</option>
									  	<option value="completed">Completed</option>
									  	<option value="closed">Closed</option>
										<option value="free">Free</option>
									</select>
						    	</div>
						  	</div>
						  	<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Notes</label>
						    	<div class="col-sm-6">
						      		<textarea class="form-control" rows="5" name="notes">{{ old('notes') }}</textarea>
						    	</div>
						  	</div>
						  	@if(isset($publicChannels))
						  	  	<div class="form-group">
						  	    	<label for="" class="col-sm-2 control-label">Slack Public Name</label>
						  	    	<div class="col-sm-6">						      		
										<div class="multipicker">
											<div class="">
												<select class="js-example-basic-multiple22 " multiple="multiple" name="channels[]">
													@foreach($publicChannels as $publicChannel)
														<?php
															$saved = false;
															foreach($selected_channels as $selected_channel){
																if($selected_channel == $publicChannel->id){
																	$saved = true;
																}
															}
														?>
														@if(isset($selected_channels))
															@if($saved)
																<option value="{{$publicChannel->id}}" selected >{{$publicChannel->name}}</option>
															@else
																<option value="{{$publicChannel->id}}">{{$publicChannel->name}}</option>
															@endif
														@else
															<option value="{{$publicChannel->id}}">{{$publicChannel->name}}</option>
														@endif
													@endforeach
												</select>
											</div>
										</div>
									</div>	
						  	  	</div>
					  	  	@endif
					  	  	@if(isset($privateChannels))
					  	  	<div class="form-group">
						  	    	<label for="" class="col-sm-2 control-label">Slack Private Name</label>
						  	    	<div class="col-sm-6">						      		
										<div class="multipicker">
											<div class="">
												<select class="js-example-basic-multiple22 form-control" multiple="multiple" name="private_channels[]">
													@foreach($privateChannels as $privateChannel)
														<?php
															$saved = false;
															$private = false;
															$lastThreeChar = substr($privateChannel->name, -3);
															if($lastThreeChar == "-bd")
															{
																$private = true;
															}
															foreach($selected_channels as $selected_channel){
																if($selected_channel == $privateChannel->id){
																	$saved = true;
																}
															}
														?>
														@if ( $private )
															@if(isset($selected_channels))
																@if($saved)
																	<option value="{{$privateChannel->id}}" selected >{{$privateChannel->name}}</option>
																@else
																	<option value="{{$privateChannel->id}}">{{$privateChannel->name}}</option>
																@endif
															@else
																<option value="{{$privateChannel->id}}">{{$privateChannel->name}}</option>
															@endif
														@endif
													@endforeach
												</select>
											</div>
										</div>
									</div>	
						  	  	</div>  	
									  
						<!--		<div class="form-group">
					  	  	    	<label for="" class="col-sm-2 control-label">Slack Private Name</label>
					  	  	    	<div class="col-sm-6">						      		
					  	  				<select id="selectid4" name="channel_for_manager" class="form-control" style="">
					  	  					@foreach($privateChannels as $privateChannel)
					  	  						<?php
					  	  							$private = false;
					  	  							$lastThreeChar = substr($privateChannel->name, -3);
					  	  							if($lastThreeChar == "_bd")
					  	  							{
					  	  								$private = true;
					  	  							}
					  	  						?>
					  	  						@if($private)
					  	  							<option value="{{$privateChannel->name}}">{{$privateChannel->name}}</option>
					  	  						@endif
					  	  					@endforeach
					  	  				</select>
					  	  	    	</div>
					  	  	    	
					  	  	  	</div> -->
					  	  	@endif
					  	  	<div class="form-group">
					  	    	<label for="" class="col-sm-2 control-label">Email Reminder</label>
					  	    	<div class="col-sm-6">	
						  	    	<input type="radio" name="reminder" value="yes" checked> Yes
	  								<input type="radio" name="reminder" value="no"> No
	  							</div>
					  	  	</div>
					  	</div>
					</div>
				</div>
			</div>
  	  		<div class="text-center">
		  		<button type="reset" class="btn btn-default">Clear</button>
		  		<button type="submit" class="btn btn-success">Create New Project</button>
		  	</div>
		</form>
	</div>
</section>
	<script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker({
					format: 'DD-MM-YYYY'
				});
                $('#datetimepicker2').datetimepicker({
					format: 'DD-MM-YYYY'
                });
            });
	</script> 
@endsection