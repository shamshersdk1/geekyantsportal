@extends('layouts.admin-dashboard')
@section('main-content')

<section class="new-project-section">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">{{$project->project_name}}</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
			  			<li><a href="{{ url('admin/project') }}">Projects</a></li>
			  			<li class="active">Edit {{$project->project_name}}</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>

	    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif

		<form class="form-horizontal" method="post" action="/admin/project/{{$project->id}}" enctype="multipart/form-data">
			<input name="_method" type="hidden" value="PUT">
			<div class="panel panel-default">
				<div class="panel-body">
				    <div class="row">
				    	<div class="col-md-12">
						  	<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Client Name</label>
						    	<div class="col-sm-6">
									<select id="selectid2" name="company_id" style="width=35%;" placeholder= "Select an option">
										@if(isset($companies))
											@foreach($companies as $x)
												@if($project->company_id == $x->id)
													<option value="{{$x->id}}" selected>{{$x->name}}</option>
												@else
									        		<option value="{{$x->id}}" >{{$x->name}}</option>
									        	@endif

										    @endforeach
										@endif
									</select>
						    	</div>
						  	</div>
						  	<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Project Name</label>
						    	<div class="col-sm-6">
						      		<input type="text" class="form-control" id="" name="project_name" value="{{$project->project_name}}">
								</div>
						  	</div>
						  	<div class="form-group">
                                <div class="col-sm-6 col-sm-offset-2">
                                	<div class="row">
                                    	<div class="col-sm-6">
                                            <div class='input-group date' id='datetimepicker1'>
                                                <input type='text' class="form-control" placeholder="Chosse Start Date" name="start_date" value="{{date_to_ddmmyyyy($project->start_date)}}"/>
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
	                                        </div>
	                                    </div>
								  		<div class="col-md-6">
                                            <div class='input-group date' id='datetimepicker2'>
                                                <input type='text' class="form-control" placeholder="Chosse End Date" name="end_date" value="{{date_to_ddmmyyyy($project->end_date)}}"/>
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
								  		</div>
								  	</div>
								  </div>
							</div>
						  	<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">B D Manager</label>
				    	    	<div class="col-sm-6">
				    				<select id="selectid3" name="bd_manager_id" style="width=35%;" placeholder= "Select an option">
				    					@if(isset($users))
				    						<option value=""></option>
				    						@foreach($users as $x)
				    							@if((!empty($project->bdManager->name) ? $project->bdManager->name : '' ) == $x->name)
				    				        		<option value="{{$x->id}}" selected>{{$x->name}}</option>
				    				        	@else
				    				        		<option value="{{$x->id}}">{{$x->name}}</option>
				    				        	@endif
				    					    @endforeach
				    					@endif
				    				</select>
				    	    	</div>
						  	</div>
						  	<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Account Manager</label>
				    	    	<div class="col-sm-6">
				    				<select id="selectid4" name="account_manager_id" style="width=35%;" placeholder= "Select an option">
			    						@if(isset($users))
			    							<option value=""></option>
			    							@foreach($users as $x)
			    								@if((!empty($project->accountManager->name) ? $project->accountManager->name : '' ) == $x->name)
			    					        		<option value="{{$x->id}}" selected>{{$x->name}}</option>
			    					        	@else
			    					        		<option value="{{$x->id}}" >{{$x->name}}</option>
			    					        	@endif
			    						    @endforeach
			    						@endif
				    				</select>
				    	    	</div>
						  	</div>
						  	<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Team Lead</label>
				    	    	<div class="col-sm-6">
				    				<select id="selectid22" name="project_manager_id" style="width=35%;" placeholder= "Select an option">
			    						@if(isset($users))
			    							<option value=""></option>
			    							@foreach($users as $x)
			    								@if((!empty($project->projectManager->name) ? $project->projectManager->name : '' ) == $x->name)
			    					        		<option value="{{$x->id}}" selected>{{$x->name}}</option>
			    					        	@else
			    					        		<option value="{{$x->id}}" >{{$x->name}}</option>
			    					        	@endif
			    						    @endforeach
			    						@endif
				    				</select>
				    	    	</div>
						  	</div>
							<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Project Category</label>
				    	    	<div class="col-sm-6">
				    				<select id="selectid2" name="project_category" class="form-control" style="">
			    						@if(isset($categories))
			    							<option value="">Select</option>
			    							@foreach($categories as $x)
												@if($project->project_category_id == $x->id)
			    					        		<option value="{{$x->name}}" selected>{{$x->name}}</option>
			    					        	@else
			    					        		<option value="{{$x->id}}">{{$x->name}}</option>
			    					        	@endif
			    						    @endforeach
			    						@endif
				    				</select>
				    	    	</div>
						  	</div>
						  	<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Status</label>
						    	<div class="col-sm-6">
						    		<?php
$status = array('open', 'initial', 'sow', 'nda', 'assigned', 'in_progress', 'completed', 'closed', 'free');
$status_display = array('open' => 'Open', 'initial' => 'Initial', 'sow' => 'SOW', 'nda' => 'NDA', 'assigned' => 'Assigned', 'in_progress' => 'In progress', 'completed' => 'Completed', 'closed' => 'Closed', 'free' => 'Free');
?>
						      		<select class="form-control" name="status">
									  	@foreach($status as $x)
									  		@if($project->status == $x)
									  			<option value="{{$x}}" selected>{{$status_display[$x]}}</option>
									  		@else
									  			<option value="{{$x}}">{{$status_display[$x]}}</option>
									  		@endif
									  	@endforeach
									</select>
						    	</div>
						  	</div>
						  	<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Notes</label>
						    	<div class="col-sm-6">
						      		<textarea class="form-control" rows="5" name="notes">{{$project->notes}}</textarea>
						    	</div>
						  	</div>
							<!--             -->
							@if(isset($publicChannels))
						  	  	<div class="form-group">
						  	    	<label for="" class="col-sm-2 control-label">Slack Public Name</label>
						  	    	<div class="col-sm-6">
										<div class="multipicker">
											<div class="">
												<select class="js-example-basic-multiple22 " multiple="multiple" name="channels[]">
													@foreach($publicChannels as $publicChannel)
														<?php
$saved = false;
foreach ($selected_public_channels as $selected_public_channel) {
    if ($selected_public_channel['slack_id'] == $publicChannel->id) {
        $saved = true;
    }
}
?>
														@if(isset($selected_public_channels))
															@if($saved)
																<option value="{{$publicChannel->id}}" selected >{{$publicChannel->name}}</option>
															@else
																<option value="{{$publicChannel->id}}">{{$publicChannel->name}}</option>
															@endif
														@else
															<option value="{{$publicChannel->id}}">{{$publicChannel->name}}</option>
														@endif
													@endforeach
												</select>
											</div>
										</div>
									  </div>
						  	    </div>
					  	  	@endif


							<!--             -->
			  		  	  	@if(isset($privateChannels))
					  	  	<div class="form-group">
						  	    	<label for="" class="col-sm-2 control-label">Slack Private Name</label>
						  	    	<div class="col-sm-6">
										<div class="multipicker">
											<div class="">
												<select class="js-example-basic-multiple22 form-control" multiple="multiple" name="private_channels[]">
													@foreach($privateChannels as $privateChannel)
														<?php
$saved = false;
$private = false;
$lastThreeChar = substr($privateChannel->name, -3);
if ($lastThreeChar == "-bd") {
    $private = true;
}
foreach ($selected_private_channels as $selected_private_channel) {
    if ($selected_private_channel['slack_id'] == $privateChannel->id) {
        $saved = true;
    }
}
?>
														@if ( $private )
															@if(isset($selected_private_channels))
																@if($saved)
																	<option value="{{$privateChannel->id}}" selected >{{$privateChannel->name}}</option>
																@else
																	<option value="{{$privateChannel->id}}">{{$privateChannel->name}}</option>
																@endif
															@else
																<option value="{{$privateChannel->id}}">{{$privateChannel->name}}</option>
															@endif
														@endif
													@endforeach
												</select>
											</div>
										</div>
									</div>
						  	  	</div>
					  	  	@endif
			  	  	  	</div>
			    	</div>
			    </div>
			</div>
			<div class="text-center">
				<button type="button" class="btn btn-default" onclick="window.history.back();" >Cancel</button>
				<button type="submit" class="btn btn-success">Update Project</button>
			</div>
		</form>
	</div>
</section>

<script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker({
					format: 'DD-MM-YYYY'
				});
                $('#datetimepicker2').datetimepicker({
					format: 'DD-MM-YYYY'
                });
            });
	</script>
@endsection