@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Projects</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
		  			<li class="active">Project Dashboard</li>
        		</ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <a href="/admin/project/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Add new Project</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
	    	@if(!empty($errors->all()))
	            <div class="alert alert-danger">
	                @foreach ($errors->all() as $error)
	                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                    <span>{{ $error }}</span><br/>
	                  @endforeach
	            </div>
	        @endif
	        @if (session('message'))
	            <div class="alert alert-success">
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ session('message') }}</span><br/>
	            </div>
	        @endif
			<div class="row" style="margin-bottom:1%">
                <div class="col-md-3">
                    <form action="/admin/project" method="GET">
                        @if( !empty($search) )
                            Showing results for
                        @endif
                        <input type="text" 
						name="searchprojects" 
						class="form-control" 
						placeholder="Search project by name .." 
						value="@if(isset($search)){{$search}}@endif">
                    </form>
                </div>
				<div class="col-md-9">
                <ul class="btn-group" style="padding:0;float:right">
                    <a href="/admin/project?" type="button" class="btn btn-default @if (!Request::get('status')&&!Request::get('status')&&!Request::get('searchprojects') ) btn-success @endif" >In Progress</a>
                    <a href="/admin/project?@if(strpos(Request::getQueryString(), 'status') !== false)status=my @endif&@if(empty(Request::get('status')))status=my @endif" type="button" class="btn btn-default @if( preg_replace('/[\s]+.*/','',Request::get('status')) == 'my')btn-success @endif">My Projects</a>
					<a href="/admin/project?@if(strpos(Request::getQueryString(), 'status') !== false)status=completed @endif&@if(empty(Request::get('status')))status=completed @endif" type="button" class="btn btn-default @if( preg_replace('/[\s]+.*/','',Request::get('status')) == 'completed')btn-success @endif">Completed</a>
					<a href="/admin/project?@if(strpos(Request::getQueryString(), 'status') !== false)status=failed @endif&@if(empty(Request::get('status')))status=failed @endif" type="button" class="btn btn-default @if( preg_replace('/[\s]+.*/','',Request::get('status')) == 'failed')btn-success @endif">Failed</a>
					<a href="/admin/project?@if(strpos(Request::getQueryString(), 'searchprojects') !== false)searchprojects={{str_replace(' ','+',Request::get('searchprojects'))}}&@endif @if(strpos(Request::getQueryString(), 'status') !== false)status=all @endif&@if(empty(Request::get('status')))status=all @endif" type="button" class="btn btn-default @if( preg_replace('/[\s]+.*/','',Request::get('status')) == 'all')btn-success @endif @if( strpos(Request::getQueryString(), 'searchprojects') !== false) btn-success @endif">All</a>
                </ul>
                <h4 style="float:right">Filters:&nbsp&nbsp </h4>
            </div>
            </div>
		  <div class="user-list-view">
            <div class="panel panel-default">
            	<table class="table table-striped">
            		<tr>
            			<th width="5%" class="sorting_asc">#</th>
	            		<th width="20%" class="sorting_asc">Name of Project</th>
	            		<th width="10%">Team Lead</th>
						<th width="25%">Active Users</th>
						<th width="10%">Status</th>
						<th class="text-right sorting_asc" width="30%">Actions</th>
					</tr>
            		@if(count($projects) > 0)
	            		@if(isset($projects))
		            		@foreach($projects as $index => $project)
			            		<tr>
			            			<td>{{$index+ $projects->firstItem()}}</td>
			            			<td><a href="/admin/project/{{$project->id}}/dashboard"> {{$project->project_name}}</a></td>
									@if(isset($project->projectManager->name))
									<td>{{$project->projectManager->name}}</td>
									@else
									<td></td>
									@endif
									<td>{{$active_users[$index] }}</td>
									<td>
										@if($project->status == 'open')
											<span class="label label-success custom-label">Open</span>
										@elseif ($project->status == 'initial')
											<span class="label label-success custom-label">Initial</span>
										@elseif ($project->status == 'sow')
											<span class="label label-success custom-label">SOW</span>
										@elseif ($project->status == 'nda')
											<span class="label label-success custom-label">NDA</span>
										@elseif ($project->status == 'assigned')
											<span class="label label-success custom-label">Assigned</span>
										@elseif ($project->status == 'in_progress')
											<span class="label label-info custom-label">In progress</span>
										@elseif ($project->status == 'completed')
											<span class="label label-info custom-label">Completed</span>
										@elseif ($project->status == 'closed')
											<span class="label label-danger custom-label">Closed</span>
										@elseif ($project->status == 'free')
											<span class="label label-success custom-label">Free</span>
										@endif
									</td>
			            			<td class="text-right">
                                        <a href="/admin/review-report-project?project_id={{$project->id}}" class="btn btn-success crud-btn btn-sm">View Timesheet</a>  
		    				    		<a href="/admin/project/{{$project->id}}/dashboard" class="btn btn-success crud-btn btn-sm">View Dashboard</a>	
		    					    	<a href="/admin/project/{{$project->id}}/edit" class="btn btn-info btn-sm crud-btn">Edit</a>

		    					    	<form action="/admin/project/{{$project->id}}" method="post" style="display:inline-block;">
		    					    		<input name="_method" type="hidden" value="DELETE">
		    					    		<button class="btn btn-danger btn-sm crud-btn" type="submit" onclick="deletetech()">Delete</button>
		    					    	</form>
			            			</td>
			            		</tr> 
		            		@endforeach
		            	@endif
	            	@else
	            		<tr>
	            			<td colspan="3" class="text-cenbter">No results found.</td>
	            		</tr>
	            	@endif
            	</table>
				<div class="col-md-12">
                    <div class="pageination pull-right">
                        <nav aria-label="Page navigation">
                              <ul class="pagination">
                                {{ $projects->render() }}
                              </ul>
                        </nav>
                    </div>
                </div>
			</div>
		</div>	
	</div>
</div>
</div>

@endsection
@section('js')
@parent
	<script>
		
		function deletetech() {
			var x = confirm("Are you sure you want to delete?");
			if (x)
				return true;
			else
				return false;
		}
	</script>
@endsection
