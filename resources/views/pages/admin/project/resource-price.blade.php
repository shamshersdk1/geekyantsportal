@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Pricing</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="/admin/project">Projects</a></li>
                     <li><a href="/admin/project/{{$id}}/dashboard">{{$id}}</a></li>
                     <li><a href="">Resource Price</a></li>
                </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <a href="/admin/project/{{$id}}/resource-price/add" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Add</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                    @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif   
            <h4 style="margin-left:5px;">List of Resources ({{$project->project_name}})</h4>
            <div class="panel panel-default">
                <table class="table table-striped">
                    <thead >
                        <th>#</th>
                        <th>Resource Type</th>
                        <th>Price</th>
                        <th>Actions</th>
                    </thead>
                    @if(count($resource_price) > 0)
                            @foreach($resource_price as $index => $resource)
                                <tr>
                                    <td class="td-text">{{$index+1}}</td>
                                    <td class="td-text"> {{$resource->type}}</td>
                                    <td class="td-text"> &dollar; {{$resource->price}}</td>
                                    <td class="td-text">
                                        <form action="/admin/project/{{$id}}/resource-price/edit" method="post" style="display:inline-block;" >
                                            <input name="resource_id" type="hidden" value={{$resource->id}}>
                                            <button class="btn btn-primary  btn-sm" type="submit"><i class="fa fa-edit btn-icon-space" aria-hidden="true"></i>Edit</button>
                                        </form>
                                        <form action="/admin/project/{{$id}}/resource-price/remove" method="post" style="display:inline-block;" >
                                            <input name="resource_id" type="hidden" value={{$resource->id}}>
                                            <button class="btn btn-danger  btn-sm" type="submit"><i class="fa fa-trash btn-icon-space" aria-hidden="true"></i>Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="7">No results found.</td>
                            </tr>
                        @endif
                </table>
            </div>
        </div>		
	</div>
</div>
@endsection