@extends('layouts.admin-dashboard')
@section('main-content')
	<section class="sprint-dashboard">
		<div class="container-fluid">
			<div class="breadcrumb-wrap">
		    	<div class="row">
		            <div class="col-sm-12">
		                <h1 class="admin-page-title">Calendar</h1>
		                <ol class="breadcrumb">
		        		  	<li><a href="/admin">Admin</a></li>
				  			<li><a href="{{ url('admin/project/calendar') }}">Calendar</a></li>
		        		</ol>
		            </div>
		        </div>
		    </div>
		    <div class="row">
		    	<div class="col-md-12">
		    		@if(!empty($errors->all()))
					    <div class="alert alert-danger">
					        @foreach ($errors->all() as $error)
					            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					            <span>{{ $error }}</span><br/>
					          @endforeach
					    </div>
					@endif
					@if (session('message'))
					    <div class="alert alert-success">
					        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					        <span>{{ session('message') }}</span><br/>
					    </div>
					@endif
		    	</div>
		    </div>
			<!-- ================== panel ====================== -->
			<div class="panel panel-default">
				<div class="panel-heading">
				    <h3 class="panel-title">Calendar</h3>
				</div>
			  	<div class="panel-body calender-panel">
			  		 <div id="calendar"></div>
			  	</div>
			</div>

		</div>

		<!-- Context-menu -->
		<style type="text/css" media="screen">
			.context_popup{
		        width:300px;
		        position: absolute;
		        padding: 15px 20px;
		        background-color:#F2F2F2;
		        box-shadow: 10px 10px 5px #888888;
		        display: none;
		        z-index:999;
		    }
		    ._close{
		        cursor:pointer;
		        float:right;
		    }
		    .add_event_collapse_btn{
		    	margin: 10px 0px 20px 0px;
		    }
		</style>
		<div class="context_popup">
		    <span class="pull-right _close">x</span>
		    <div class="container event_details">
		    	
		    </div>
		    <a href="#demo" class="btn btn-default form-control add_event_collapse_btn" data-toggle="collapse">Add event</a>
			<div id="demo" class="collapse">
			    <form method="post" action="/admin/calendar/save">
				    <div class="form-group">
					  <label for="event_type">Select type:</label>
					  	<select class="form-control" id="event_type" name="type">
						    <option value="Weekend">Weekend</option>
						    <option value="Holiday">Holiday</option>
						    <option value="Others">Others</option>

					  	</select>
					</div>
					<div class="form-group">
					  	<label for="comment">Describe:</label>
					  	<textarea class="form-control" rows="3" id="comment" name="reason"></textarea>
					</div>
					<input type="hidden" name="date" id="date" value="test">
				    <button class="btn btn-success" type="submit">Create</button>
			    </form>
			</div>
		</div>

	</section>
	<script>
	
	//------------------------------- CONTEXT MENU ----------------------------------
	var _contextMenu = function(){
		/*
			Author: SiddharthaChowdhury,
			Date: 10 Sept 2016,
			Description: https://github.com/SiddharthaChowdhury/_context-menu,
			License: GNU
		*/
		var contextBoxClass= null;
			var clickedOnClass= null;
			var closeBtnClass= null;
			var popupBesideClass= null;
			this.config = function(obj){
				this.contextBoxClass = obj.contextBoxClass || null;
				this.clickedOnClass = obj.clickedOnClass || null;
				this.closeBtnClass = obj.closeBtnClass || null;
				this.popupBesideClass = obj.popupBesideClass;
				this.disableErrorLog = obj.disableErrorLog || false;
			}

			var check_values =  function(dis){
					var flag = 0;
					if( dis.contextBoxClass == null ){
						(!dis.disableErrorLog) ? console.log("Value for 'contextBoxClass' is "+dis.contextBoxClass+" (required)" ) : console.log();
						flag++;
					}
					else if( $('.'+dis.contextBoxClass).length == '0' ){
						(!dis.disableErrorLog) ? console.log("Class name 'contextBoxClass' is not found in the DOM.(required)" ) : console.log();
						flag++;
					}
					if( dis.clickedOnClass == null ){
						(!dis.disableErrorLog) ? console.log("Value for 'clickedOnClass' is "+dis.clickedOnClass+" (required)" ) : console.log();
						flag++;
					}
					else if( $('.'+dis.clickedOnClass).length == '0' ){
						(!dis.disableErrorLog) ? console.log("Class name 'clickedOnClass' is not found in the DOM. (required)" ) : console.log();
						flag++;
					}
					if( dis.closeBtnClass == null ){
						(!dis.disableErrorLog) ? console.log("Value for 'closeBtnClass' is "+dis.closeBtnClass+" (optional)" ) : console.log();
					}
					else if(  dis.closeBtnClass != null && $('.'+dis.closeBtnClass).length == '0'){
						(!dis.disableErrorLog) ? console.log("Class of closeBtnClass : "+dis.closeBtnClass+"  is not found in the DOM. (required)" ) : console.log();
						flag++;
					}
					if( dis.popupBesideClass == null ){
						(!dis.disableErrorLog) ? console.log("Value for 'popupBesideClass' is '"+dis.popupBesideClass+"' (optional)" ) : console.log();
					}
					else if(  dis.popupBesideClass != null && $('.'+dis.popupBesideClass).length == '0'){
						(!dis.disableErrorLog) ? console.log("Class of popupBesideClass : '"+dis.popupBesideClass+"' is not found in the DOM. (required)" ) : console.log();
						flag++;
					}
					return flag;
				}

			this.run = function(){
				if( !check_values(this) ){

					var properties = {
						contextMenu 			: $('.'+this.contextBoxClass),
						popupBesideClass 		: $('.'+this.popupBesideClass),
						popupBesideClass_str 	: '.'+this.popupBesideClass,
						clickedOnClass_str 		: '.'+this.clickedOnClass,
						closeBtnClass_str 		: '.'+this.closeBtnClass,
						contextMenu_str			: '.'+this.contextBoxClass
					};

					properties.contextMenu.hide();			

					$('body').click(function(e){  
						var target = $(e.target); 
						if(!$(target).is(properties.clickedOnClass_str) && !target.parents(properties.contextMenu_str).length) {
						    properties.contextMenu.hide();
						}
					});

					$(document).on('click', properties.clickedOnClass_str, function(e){
						e.preventDefault();
						if ( properties.popupBesideClass_str == '.undefined'){
							properties.popupBesideClass = $(this);
						}
						
						var coord = properties.popupBesideClass.offset();
						var width = properties.popupBesideClass.outerWidth()+10;
						var height = properties.popupBesideClass.outerHeight();
						properties.contextMenu.css({top: (Math.ceil(coord.top) + height - 60), left: parseInt(Math.ceil(coord.left)), position:'absolute'});
						properties.contextMenu.show();
					});

					$(document).on('click', properties.closeBtnClass_str, function(){
						properties.contextMenu.hide();
					});
				}
			}
	}
	//------------------------------- IMPLEMENTATION --------------------------------
		$(document).ready(function() {
		    // page is now ready, initialize the calendar...

		    $('#calendar').fullCalendar({
		    	events: function(start, end, timezone, callback) {
		    	       $.ajax({
		    	           url: 'calendar/events',
		    	           success: function(doc) {
		    	           	console.log("inbcalendar",doc);
		    	               var events = [];
		    	               $(doc).each(function() {
		    	               	console.log("this",$(this).attr('type'));
		    	                   events.push({
		    	                       title: $(this).attr('reason'),
		    	                       start: $(this).attr('date'), // will be parsed
		    	                       color: $(this).attr('color'),
		    	                       // editable : true
		    	                   });
		    	               });
		    	               console.log("events",events);
		    	               callback(events);
		    	           }
		    	       });
		    	   },
		    	   eventClick: function(event) {
		    	       		var result = confirm("Want to remove event?");
		    	       		var eventData = event;
		    	       		console.log(event.start._i);
							if (result) {
			    	       		$.ajax({
			    	       			type:"POST",
				    	            url: 'calendar/events/delete',
				    	            data: { title: eventData.title,
				    	            		date: event.start._i
				    	            		},
				    	            success: function(result) {
				    	            	if(result == "true"){
				    	            		console.log("true",result);
				    	            		$('#calendar').fullCalendar('refetchEvents');
				    	            	}
					    	           	console.log("cool",result);
					    	        }
		    	      		    });
							}
					        return false;
	    				}
		    });
			
			

			$('.fc-next-button').click(function(){
			    $('#calendar').fullCalendar({
			    	events: function(start, end, timezone, callback) {
			    	       $.ajax({
			    	           url: 'calendar/events',
			    	           success: function(doc) {
			    	           	console.log("inbcalendar",doc);
			    	               var events = [];
			    	               $(doc).each(function() {
			    	               	console.log("this",$(this).attr('type'));
			    	                   events.push({
			    	                       title: $(this).attr('reason'),
			    	                       start: $(this).attr('date'), // will be parsed
			    	                       color: $(this).attr('color'),
			    	                       // editable : true
			    	                   });
			    	               });
			    	               console.log("events",events);
			    	               callback(events);
			    	           }
			    	       });
			    	   },
			    	   eventClick: function(event) {
		    	       		var result = confirm("Want to remove event?");
		    	       		var eventData = event;
		    	       		console.log(event.start._i);
							if (result) {
			    	       		$.ajax({
			    	       			type:"POST",
				    	            url: 'calendar/events/delete',
				    	            data: { title: eventData.title,
				    	            		date: event.start._i
				    	            		},
				    	            success: function(result) {
				    	            	if(result == "true"){
				    	            		console.log("true",result);
				    	            		$('#calendar').fullCalendar('refetchEvents');
				    	            	}
					    	           	console.log("cool",result);
					    	        }
		    	      		    });
							}
					        return false;
		    			}
		    		});
				var active_day;
			    $('.fc-day').click(function(){
			    	active_day = $(this);
			    	console.log(active_day);
			    	console.log("here",active_day.context.attributes[1].nodeValue);
			    	var date = active_day.context.attributes[1].nodeValue;
			    	$('#date').val(date);


			    	$('.context_popup').find('.event_details').html("");
			    	if($(this).find('.events').length == 0){
			    		$('.context_popup').find('.event_details').html('<small>There are no event found on this date.</small>')
			    	}
			    	else{
			    		var data = {};
			    		$(this).find('.events').each(function(){
			    			if($(this).hasClass('red_event')){
			    				if('red' in data)
			    					data['red'].push($(this).attr('data-desc').split('||'));
			    				else{
			    					data['red'] = [];
			    					data['red'].push($(this).attr('data-desc').split('||'));
			    				}
			    			}
			    			if($(this).hasClass('blue_event')){
			    				if('blue' in data)
			    					data['blue'].push($(this).attr('data-desc').split('||'));
			    				else{
			    					data['blue'] = [];
			    					data['blue'].push($(this).attr('data-desc').split('||'));
			    				}
			    			}
			    			if($(this).hasClass('green_event')){
			    				if('green' in data)
			    					data['green'].push($(this).attr('data-desc').split('||'));
			    				else{
			    					data['green'] = [];
			    					data['green'].push($(this).attr('data-desc').split('||'));
			    				}
			    			}
			    		});
			    		var event_details = $('.context_popup').find('.event_details');
			    		$.each(data, function(index, value){
			    			event_details.append('<label style="border-radius:3px; padding:6px; background-color:'+index+'; color:#fff; position:relative; bottom:0px; left:20px;">'+value[0].length+' Events</label><hr>')
			    			var dscs = '<ul>';
			    			$.each(value[0], function(i,v){	
			    				dscs += '<li>'+v+'</li>'
			    			})
			    			dscs += '</ul>';
			    			event_details.append(dscs);
			    		})
			    		// ============ try this ===============
			    		// $(this).addClasss('bg-red');
			    	}
			    });
		    	console.log(active_day);
			});
			
			$('.fc-prev-button').click(function(){
			    $('#calendar').fullCalendar({
			    	events: function(start, end, timezone, callback) {
			    	       $.ajax({
			    	           url: 'calendar/events',
			    	           success: function(doc) {
			    	           	console.log("inbcalendar",doc);
			    	               var events = [];
			    	               $(doc).each(function() {
			    	               	console.log("this",$(this).attr('type'));
			    	                   events.push({
			    	                       title: $(this).attr('reason'),
			    	                       start: $(this).attr('date'), // will be parsed
			    	                       color: $(this).attr('color'),
			    	                       // editable : true
			    	                   });
			    	               });
			    	               console.log("events",events);
			    	               callback(events);
			    	           }
			    	       });
			    	   },
			    	   eventClick: function(event) {
		    	       		var result = confirm("Want to remove event?");
		    	       		var eventData = event;
		    	       		console.log(event.start._i);
							if (result) {
			    	       		$.ajax({
			    	       			type:"POST",
				    	            url: 'calendar/events/delete',
				    	            data: { title: eventData.title,
				    	            		date: event.start._i
				    	            		},
				    	            success: function(result) {
				    	            	if(result == "true"){
				    	            		console.log("true",result);
				    	            		$('#calendar').fullCalendar('refetchEvents');
				    	            	}
					    	           	console.log("cool",result);
					    	        }
		    	      		    });
							}
					        return false;
		    			}
		    		});
				var active_day;
			    $('.fc-day').click(function(){
			    	active_day = $(this);
			    	console.log(active_day);
			    	console.log("here",active_day.context.attributes[1].nodeValue);
			    	var date = active_day.context.attributes[1].nodeValue;
			    	$('#date').val(date);


			    	$('.context_popup').find('.event_details').html("");
			    	if($(this).find('.events').length == 0){
			    		$('.context_popup').find('.event_details').html('<small>There are no event found on this date.</small>')
			    	}
			    	else{
			    		var data = {};
			    		$(this).find('.events').each(function(){
			    			if($(this).hasClass('red_event')){
			    				if('red' in data)
			    					data['red'].push($(this).attr('data-desc').split('||'));
			    				else{
			    					data['red'] = [];
			    					data['red'].push($(this).attr('data-desc').split('||'));
			    				}
			    			}
			    			if($(this).hasClass('blue_event')){
			    				if('blue' in data)
			    					data['blue'].push($(this).attr('data-desc').split('||'));
			    				else{
			    					data['blue'] = [];
			    					data['blue'].push($(this).attr('data-desc').split('||'));
			    				}
			    			}
			    			if($(this).hasClass('green_event')){
			    				if('green' in data)
			    					data['green'].push($(this).attr('data-desc').split('||'));
			    				else{
			    					data['green'] = [];
			    					data['green'].push($(this).attr('data-desc').split('||'));
			    				}
			    			}
			    		});
			    		var event_details = $('.context_popup').find('.event_details');
			    		$.each(data, function(index, value){
			    			event_details.append('<label style="border-radius:3px; padding:6px; background-color:'+index+'; color:#fff; position:relative; bottom:0px; left:20px;">'+value[0].length+' Events</label><hr>')
			    			var dscs = '<ul>';
			    			$.each(value[0], function(i,v){	
			    				dscs += '<li>'+v+'</li>'
			    			})
			    			dscs += '</ul>';
			    			event_details.append(dscs);
			    		})
			    		// ============ try this ===============
			    		// $(this).addClasss('bg-red');
			    	}
			    });
		    	console.log(active_day);
			});

		    var active_day;
		    $('.fc-day').click(function(){
		    	active_day = $(this);
		    	console.log(active_day);
		    	console.log("here",active_day.context.attributes[1].nodeValue);
		    	var date = active_day.context.attributes[1].nodeValue;
		    	$('#date').val(date);


		    	$('.context_popup').find('.event_details').html("");
		    	if($(this).find('.events').length == 0){
		    		$('.context_popup').find('.event_details').html('<small>There are no event found on this date.</small>')
		    	}
		    	else{
		    		var data = {};
		    		$(this).find('.events').each(function(){
		    			if($(this).hasClass('red_event')){
		    				if('red' in data)
		    					data['red'].push($(this).attr('data-desc').split('||'));
		    				else{
		    					data['red'] = [];
		    					data['red'].push($(this).attr('data-desc').split('||'));
		    				}
		    			}
		    			if($(this).hasClass('blue_event')){
		    				if('blue' in data)
		    					data['blue'].push($(this).attr('data-desc').split('||'));
		    				else{
		    					data['blue'] = [];
		    					data['blue'].push($(this).attr('data-desc').split('||'));
		    				}
		    			}
		    			if($(this).hasClass('green_event')){
		    				if('green' in data)
		    					data['green'].push($(this).attr('data-desc').split('||'));
		    				else{
		    					data['green'] = [];
		    					data['green'].push($(this).attr('data-desc').split('||'));
		    				}
		    			}
		    		});
		    		var event_details = $('.context_popup').find('.event_details');
		    		$.each(data, function(index, value){
		    			event_details.append('<label style="border-radius:3px; padding:6px; background-color:'+index+'; color:#fff; position:relative; bottom:0px; left:20px;">'+value[0].length+' Events</label><hr>')
		    			var dscs = '<ul>';
		    			$.each(value[0], function(i,v){	
		    				dscs += '<li>'+v+'</li>'
		    			})
		    			dscs += '</ul>';
		    			event_details.append(dscs);
		    		})
		    		// ============ try this ===============
		    		// $(this).addClasss('bg-red');
		    	}
		    });

		    $('.record_schedule').click(function(e){
		    	e.preventDefault();
		    	var parent 		= $('.context_popup'),
		    		desc 		= parent.find('#comment').val()
		    		event_type 	= parent.find('#event_type').val();
		    	if(event_type == 'red'){
		    		if(active_day.find('label.red_event').length == 0 ){
		    			active_day.append('<label class="red_event events" data-desc="'+desc+'" style="border-radius:50%; padding:6px; background-color:red; position:relative; bottom:0px; left:20px;"></label>');
		    		}else{
		    			var data_desc = active_day.find('label.red_event').attr('data-desc');
		    			data_desc += '||'+desc;
		    			active_day.find('label.red_event').attr('data-desc', data_desc)
		    		}
		    	}else if(event_type == 'blue'){
		    		if(active_day.find('label.blue_event').length == 0 ){
		    			active_day.append('<label class="blue_event events" data-desc="'+desc+'" style="border-radius:50%; padding:6px; background-color:blue; position:relative; bottom:0px; left:20px;"></label>');
		    		}else{
		    			var data_desc = active_day.find('label.blue_event').attr('data-desc');
		    			data_desc += '||'+desc;
		    			active_day.find('label.blue_event').attr('data-desc', data_desc)
		    		}
		    	}else{
		    		if(active_day.find('label.green_event').length == 0 ){
		    			active_day.append('<label class="green_event events" data-desc="'+desc+'" style="border-radius:50%; padding:6px; background-color:green; position:relative; bottom:0px; left:20px;"></label>');
		    		}else{
		    			var data_desc = active_day.find('label.green_event').attr('data-desc');
		    			data_desc += '||'+desc;
		    			active_day.find('label.green_event').attr('data-desc', data_desc)
		    		}
		    	}
		    	parent.hide();
		    })

		    var x = new _contextMenu();              // Create instance of _context_menu
		    x.config({                               // Configure the new context menu
		    contextBoxClass : 'context_popup',
		    clickedOnClass : 'fc-day',
		    closeBtnClass : '_close',
		    // popupBesideClass : 'className',
		    disableErrorLog: true
		    })
		    x.run();

		      

		});
		// $('.red-class').click(function(){
		// 	alert("hello");
		// });
	</script>
@endsection
