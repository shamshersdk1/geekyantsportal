@extends('layouts.admin-dashboard')
@section('main-content')
<section class="project-dashboard">
   <div class="container-fluid">
      <div class="breadcrumb-wrap">
         <div class="row">
            <div class="col-sm-8">
               <h1 class="admin-page-title">{{$projectinfo->project_name}}</h1>
               <ol class="breadcrumb">
                  <li><a href="/admin">Admin</a></li>
                  <li><a href="{{ url('admin/project') }}">Projects</a></li>
               </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
               <a href="/admin/project/{{$projectinfo->id}}/edit" class="btn btn-info btn-default crud-btn">Edit</a>
               <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
         </div>
      </div>
      @if(!empty($errors->all()))
      <div class="alert alert-danger">
         @foreach ($errors->all() as $error)
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
         <span>{{ $error }}</span><br/>
         @endforeach
      </div>
      @endif
      @if (session('message'))
      <div class="alert alert-success">
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
         <span>{{ session('message') }}</span><br/>
      </div>
      @endif
      <div class="row">
         <div class="col-sm-8">
            <div class="row">
               <div class="col-sm-6">
                  <h4 class="admin-section-heading">Basic Info</h4>
                  <div class="panel panel-default">
                     <!-- <div class="manager-name-holder project-name-holder">
                        {{$projectinfo->project_name}}
                        </div> -->
                     <div class="info-table">
                        <label>Project Name: </label>
                        <span>{{$projectinfo->project_name}}</span>
                     </div>
                     <div class="info-table">
                        <label>Satrt Date: </label>
                        <span>{{date_in_view($projectinfo->start_date)}}</span>
                     </div>
                     <div class="info-table">
                        <label>End Date: </label>
                        <span>{{date_in_view($projectinfo->end_date)}}</span>
                     </div>
                     <div class="info-table">
                        <label>Category: {{$projectinfo->project_category}}</label>
                        <span></span>
                     </div>
                     <div class="info-table">
                        <label>Status: {{$projectinfo->project_status}}</label>
                        <span></span>
                     </div>
                  </div>
                  
                  @if($user->isAdmin())
                  <h4 class="admin-section-heading">Billing Schedule</h4>
                  <div class="panel panel-default">
                     <!-- <div class="manager-name-holder project-name-holder">
                        {{$projectinfo->project_name}}
                        </div> -->
                     <div class="info-table">
                        <label>Billing Type: </label>
                        <span>T & M</span>
                     </div>
                     <div class="info-table">
                        <label>Billing cycle: </label>
                        <span>Weekly</span>
                     </div>
                     <div class="info-table">
                        <label>Milestones: </label>
                        <span>
                            Sprint 1 <br />
                            Due date: 25th Nov
                        </span>
                     </div>
                  </div>
                  @endif
               </div>
               <div class="col-sm-6">
                  @if($user->isAdmin())
                  <h4 class="admin-section-heading">Client Details</h4>
                  <div class="panel panel-default">
                     <!-- <div class="manager-name-holder project-name-holder">
                        {{$projectinfo->project_name}}
                        </div> -->
                     <div class="info-table">
                        <label>Company Name: </label>
                        <span>{{$companies->name}}</span>
                     </div>
                     <div class="info-table">
                        <label>Email: </label>
                        <span>
                        @if(isset($companies))
                        {{$companies->email}}
                        @endif
                        </span>
                     </div>
                     <div class="info-table">
                        <label>Primary Contact: </label>
                        <span>
                        @if(isset($contacts))
                        @foreach($contacts as $contact)
                        {{$contact->first_name}} {{$contact->last_name}}
                        @endforeach
                        @endif
                        </span>
                     </div>
                     <div class="info-table">
                        <label>Primary Contact No.: </label>
                        <span>
                        @if(isset($companies->mobile))
                        {{$companies->mobile}}
                        @else
                        <span>
                        Not mention 
                        </span>
                        @endif
                        </span>
                     </div>
                  </div>
                  @endif    
                  @if($user->isAdmin())
                  <h4 class="admin-section-heading">Required Resource</h4>
                  <div class="panel panel-default">
                     <div class="info-table">
                        <label>Sr. Software Dev: </label>
                        <span>$30 <span class="pull-right">2</span></span>
                     </div>
                     <div class="info-table">
                        <label>QA: </label>
                        <span>$15 <span class="pull-right">1</span></span>
                     </div>
                  </div>
                  @endif
                  <h4 class="admin-section-heading">Last Month Timelog</h4>
                  <div class="panel panel-default">
                     <table class="table table-bordered bg-white">
                        <tr>
                           <th>Name</th>
                           <th width="120" class="text-center">Approved Hrs</th>
                           <th width="120" class="text-center">Employee Hrs</th>
                        </tr>
                        <tr>
                           <td>test</td>
                           <td class="text-center">50</td>
                           <td class="text-center">20</td>
                        </tr>
                     </table>
                  </div>
               </div>
            </div>
            <!--  ==================== USERS ADD ========================= -->
            <div class="row m-t-15">
               <div class="col-sm-12">
                  @if($user->isAdmin()|| $giveAccess)
                  <div class="row">
                     <div class="col-md-12">
                        <h4 class="admin-section-heading">Add Users</h4>
                        <form action="/admin/project/{{$projectinfo->id}}/addUser" method="post">
                           <div class="panel panel-default select-leave-type project-add-user">
                              <div class="row">
                                 <div class="col-md-12 ma-bot-8 p-0">
                                    <div class="form-group ma-bot-8">
                                       <div class="col-md-3 " >
                                          <select style="width:80%;" name="user_id" class="select_2_resource_type" required="true">
                                             <option value=""></option>
                                             
                                          </select>
                                       </div>
                                       <div class="col-md-3 p-0" >
                                          <select style="width:80%;" name="user_id" class="select_2_user_list" required="true">
                                             <option value=""></option>
                                             @foreach($resources as $resource)
                                             <option value="{{$resource->id}}">{{$resource->name}}</option>
                                             @endforeach
                                          </select>
                                       </div>
                                       <div class="col-md-2" style="padding-right:0">
                                          <div class='input-group date' id='datetimepickerStart'>
                                             <input type='text' id="datetimeinputstart" class="form-control" value="{{old('start_date')}}" name="start_date" placeholder="Start Date" required autocomplete="off" required="true"/>
                                             <span class="input-group-addon">
                                             <span class="glyphicon glyphicon-calendar"></span>
                                             </span>
                                          </div>
                                       </div>
                                       <div class="col-md-2 " style="padding-right:0">
                                          <div class='input-group date' id='datetimepickerEnd'>
                                             <input type='text' id="datetimeinputend" class="form-control" name="end_date" placeholder="End Date" value="{{old('end_date')}}"  autocomplete="off" />
                                             <span class="input-group-addon">
                                             <span class="glyphicon glyphicon-calendar"></span>
                                             </span>
                                          </div>
                                       </div>
                                       <div class="col-md-2  pull-right">
                                          <button class="btn btn-success crud-btn pull-right" style="margin-top: 3px;" type="submit">Add</button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
                  @endif
               </div>
               <div class="col-sm-12">
                  <div class="panel panel-default" style="margin-bottom:0">
                     <table class="table table-striped">
                        <thead>
                           <th>User Name</th>
                           <th>Start Date</th>
                           <th>End Date</th>
                           <th>Logged Time</th>
                           <th class="text-right">Actions</th>
                        </thead>
                        @if(count($projectData) > 0)
                        @foreach($projectData as $list)
                        @if($list->end_date && $list->end_date < date('Y-m-d'))
                        <tr class="danger">
                           <td class="td-text">{{$list->user_data->name}} <span class="badge badge-primary">TL</span></td>
                           <td class="td-text">{{date('j M Y',strtotime($list->start_date))}}</td>
                           <td class="td-text">{{date('j M Y',strtotime($list->end_date))}}</td>
                           <td class="td-text">20/100</td>
                           <td class="text-right">
                           </td>
                        </tr>
                        @else       
                        <tr>
                           <td class="td-text">{{$list->user_data->name}} <span class="badge badge-primary">TL</span></td>
                           <td class="td-text">{{date('j M Y',strtotime($list->start_date))}}</td>
                           <td class="td-text">
                              @if($list->end_date)
                              <span>{{date('j M Y',strtotime($list->end_date))}}</span>
                              @else    
                              <span class="label label-info custom-label" >Working</span>
                              @endif
                           </td>
                           <td class="td-text">20/100</td>
                           <td class="text-right">
                              <form action="/admin/project/release/{{$list->id}}" method="post"  style="display:inline">
                                 <button type="submit" class="btn btn-warning crud-btn btn-sm" >
                                 <i class="fa fa-trash btn-icon-space" aria-hidden="true"></i>Release</button>
                              </form>
                              <button class="btn btn-info btn-sm" type="button"
                                 data-toggle="modal" data-target="#editDetailModal"
                                 data-id="{{$list->id}}" data-title="Algorithms"><i class="fa fa-pencil fa-fw"></i>Edit</button>
                              <form action="/admin/project/delete/{{$list->id}}" method="post" style="display:inline">
                                 <input type="hidden" name="_method" value="DELETE" />
                                 <button type="submit" class="btn btn-danger crud-btn btn-sm" onclick="deleteSprint();">
                                 <i class="fa fa-trash btn-icon-space" aria-hidden="true"></i>Delete</button>
                              </form>
                           </td>
                        </tr>
                        @endif            
                        @endforeach  
                        @else
                        <tr>
                           <td colspan="3">No results found.</td>
                           <td></td>
                           <td></td>
                        </tr>
                        @endif
                     </table>
                  </div>
               </div>

                  <div class="col-md-12 m-t-15">
                     <h4>Bonus Info 
                        <div class="pull-right">
                            <a href="" class="btn btn-primary btn-sm"  data-toggle="modal" data-target="#addBonus">Add Bonus</a> 
                            <a href="" class="btn btn-white btn-sm">View All</a>
                        </div>
                    </h4>
                     <div class="panel panel-default" style="margin-bottom:0">
                        <table class="table table bg-white">
                           <thead>
                              <tr>
                                 <th>Date</th>
                                 <th>User</th>
                                 <th>Type</th>
                                 <th>Time/Day</th>
                                 <th>Amount</th>
                                 <th class="text-right">Given By</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                <td>22nd Dec</td>
                                 <td>Megha</td>
                                 <td>Onsite</td>
                                
                                 
                                 <td>10hr</td>
                                 <td>10000</td>
                                 <td class="text-right"> Pratik</td>
                              </tr>
                              <tr>
                                <td>20nd Sep</td>
                                 <td>varun</td>
                                 <td>Monthly</td>
                                
                                 
                                 <td>1 days</td>
                                 <td>10000</td>
                                 <td class="text-right"> Pratik</td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>
            </div>
         </div>
         <div class="col-sm-4">
            <h4 class="admin-section-heading">Technologies</h4>
            <div class="panel panel-default">
               <div class="panel-body">
                  <div class="tags">
                     @if(!$projectTechnologies->isEmpty())
                     @if($user->isAdmin())
                     @foreach($projectTechnologies as $projectTechnology)
                     <form style="display:inline-block;" action="/admin/project/{{$projectTechnology->id}}/{{$projectinfo->id}}/deleteTechnology" method="post">
                        <a class="label tag-label">{{$projectTechnology->name}} 
                        <input name="_method" type="submit"  value="" class="input-delete">
                        </a>
                        <input name="_method" type="hidden" value="DELETE">
                     </form>
                     @endforeach
                     @else
                     @foreach($projectTechnologies as $projectTechnology)
                     <a class="label tag-label">{{$projectTechnology->name}} 
                     <input name="_method" type="submit"  value="" class="input-delete">
                     </a>
                     <input name="_method" type="hidden" value="DELETE">
                     @endforeach
                     @endif
                     @else 
                     <h5 class="no-margin">No technology selected</h5>
                     @endif
                  </div>
               </div>
            </div>
            @if($user->isAdmin())
            <h4 class="admin-section-heading">Add Technologies</h4>
            <form action="/admin/project/{{$projectinfo->id}}/addTechnology" method="post">
               <div class="input-group">
                  <select class="js-example-basic-multiple22 form-control" multiple="multiple" name="technologies[]">
                     @if(isset($technologies))
                     @foreach($technologies as $technology)
                     <?php
                        $saved = false;
                        foreach($projectTechnologies as $projectTechnology){
                            if($projectTechnology->id == $technology->id){
                                $saved = true;
                            }
                        }
                        ?>
                     @if(isset($projectTechnologies))
                     @if($saved)
                     <option value="{{$technology->id}}" selected >{{$technology->name}}</option>
                     @else
                     <option value="{{$technology->id}}">{{$technology->name}}</option>
                     @endif
                     @else
                     <option value="{{$technology->id}}">{{$technology->name}}</option>
                     @endif
                     @endforeach
                     @endif
                  </select>
                  <span class="input-group-btn">
                  <button type="submit" class="btn btn-primary btn-sm" style="margin-left: 2px;"><i class="fa fa-plus" ></i></button>
                  </span>
               </div>
            </form>
            @endif
            <h4 class="admin-section-heading">Resource Info</h4>
             <div class="panel panel-default">
                 <!-- <div class="manager-name-holder project-name-holder">
                    {{$projectinfo->project_name}}
                    </div> -->
                 <div class="info-table">
                    <label>Sales executive / manager: </label>
                    <span>{{ !empty($projectinfo->bdManager->name) ? $projectinfo->bdManager->name : '' }}</span>
                 </div>
                 <div class="info-table">
                    <label>Project coordinator: </label>
                    <span>{{ !empty($projectinfo->accountManager->name) ? $projectinfo->accountManager->name : ''}}
                    </span>
                 </div>
                 <div class="info-table">
                    <label>Project manager: </label>
                    <span>{{ !empty($projectinfo->projectManager->name) ? $projectinfo->projectManager->name : ''}}
                    </span>
                 </div>
                 <div class="info-table">
                    <label>Code Lead: </label>
                    <span></span>
                 </div>
                 <div class="info-table">
                    <label>Delivery Lead: </label>
                    <span></span>
                 </div>
                 <div class="info-table">
                    <label>UI / UX: </label>
                    <span>By Client</span>
                 </div>
                 <div class="info-table">
                    <label>DevOp: </label>
                    <span></span>
                 </div>
                 <div class="info-table">
                    <label>Architect: </label>
                    <span></span>
                 </div>
                 <div class="info-table">
                    <label>Deployement: </label>
                    <span></span>
                 </div>
                 <div class="info-table">
                    <label>QA: </label>
                    <span></span>
                 </div>
            </div>
            <div class="clearfix">
               <h4>Project Files</h4>
               <div class="image-preview">
                  <form method="post" action="/admin/project/{{$projectinfo->id}}/upload" enctype="multipart/form-data">
                     <span id="fileselector">
                     <label class="btn btn-default" for="upload-file-selector">
                     <input type="file" name="project_file">
                     <i class="fa_icon icon-upload-alt margin-correction"></i>
                     </label>
                     </span>
                     @if($user->isAdmin())
                     <label>
                     <input type="checkbox" name="private_to_admin"> Private to Admin
                     </label>
                     @endif
                     <button class="btn btn-primary">Add</button>
                  </form>
                  @if(!$projectFiles->isEmpty())
                  <div class="panel panel-default m-t-10">
                     @foreach($projectFiles as $projectFile)
                     <div class="info-table">
                        <span class="col-sm-10">
                        <a href="/admin/file/{{$projectFile->path}}" target="_blank">{{$projectFile->path}}
                        </a>
                        </span>
                        @if($projectFile->private_to_admin == 1)
                        <div class="col-sm-2">Private</div>
                        @endif
                     </div>
                     @endforeach
                  </div>
                  @endif
               </div>
               <div class="clearfix" style="margin-top: 15px;">
                  <h4>Internal Notes</h4>
                  @if(!$projectNotes->isEmpty())
                  <div class="panel panel-default">
                     <div style="margin-left: 15px; margin-right: 15px;">
                        <div class="row">
                           <div class="col-md-12">
                              @foreach ( $projectNotes as $note)
                              <div class="info-table internal-notes">
                                 <span style="width:50%">{{$note->note}}</span>
                                 <span style="width:20%"><small class="text-primary">
                                 {{$note->user_name}}
                                 </small></span>
                                 <span style="width:25%"><small>{{datetime_in_view($note->created_at)}}</small></span>
                                 @if($note->private_to_admin == 1)
                                 <span style="width:5%"><i class="fa fa-lock"></i></span>
                                 @endif
                              </div>
                              @endforeach                           
                           </div>
                        </div>
                     </div>
                  </div>
                  @endif
               </div>
               <div class="row">
                  <div class="col-md-12">
                     <div class="form-group">
                        <form method="post" action="/admin/project/{{$projectinfo->id}}/add-comment" >
                           <textarea class="form-control" rows="5" name="comment"></textarea>
                           <div class="clearfix">
                              @if($user->isAdmin())
                              <label>
                              <input type="checkbox" name="private_to_admin"> Private to Admin
                              </label>
                              @endif
                              <button type="submit" class="m-t-10 btn btn-primary pull-right btn-sm"> <i class="fa fa-plus fa-fw"></i> Add comment 
                              </button>
                           </div>
                        </form>
                     </div>
                  </div>
                  
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!--Add Bonus Modal -->
<div class="modal fade" id="addBonus" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Add Bonus</h4>
         </div>
         <div class="modal-body row form-horizontal">
            <div class="form-group">
               <label class="col-sm-3 control-label">Bonus Type</label>
               <div class="col-sm-7">
                  <select class="form-control">
                     <option>Yearly</option>
                     <option>Monthly</option>
                     <option>Fixed</option>
                  </select>
               </div>
            </div>
            <div class="form-group">
               <label class="col-sm-3 control-label">User</label>
               <div class="col-sm-7">
                  <select class="form-control">
                     <option>User name</option>
                     <option>User</option>
                     <option>user</option>
                  </select>
               </div>
            </div>
            <div class="form-group">
               <label class="col-sm-3 control-label">Time/day</label>
               <div class="col-sm-5">
                  <select class="form-control">
                     <option>Day</option>
                     <option>Hours</option>
                  </select>
               </div>
               <div class="col-sm-2">
                  <input type="text" class="form-control" name="" >
               </div>
            </div>
            <div class="form-group">
               <label class="col-sm-3 control-label">Amount</label>
               <div class="col-sm-7">
                  <input type="text" class="form-control" name="" >
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Submit</button>
         </div>
      </div>
   </div>
</div>
@endsection
@section('js')
@parent
@include('pages/admin/dashboard/edit-detail-modal')
<script>    
   let dat;
   function updateData() {
    dat.start_date = $('#editDetailModal input[name="start_date"]').val();
    dat.end_date = $('#editDetailModal input[name="end_date"]').val();
    $.ajax({
        type: 'POST',
        url: '/admin/project/updateList/'+dat.id,
        dataType: 'text',
        data: dat,
        beforeSend: function() {
            $("div.loading").show();
            $("div.content").hide();
        },
        complete: function() {
            $("div.loading").hide();
            $("div.content").show();
        },
        success: function(data) {
            $('#editDetailModal').modal('hide');
            window.location.reload();
        },
        error: function(errorResponse){
            $('#editDetailModal').modal('hide');
            console.log(errorResponse);
            window.location.reload();
        }
    });
   }
   $(function(){    
    $('#editDetailModal').on("show.bs.modal", function (e) {
        let id = $(e.relatedTarget).data('id')
        $.ajax({
            type: 'GET',
            url: '/admin/project/'+id,
            beforeSend: function() {
                $("div.loading").show();
                $("div.content").hide();
            },
            complete: function() {
                $("div.loading").hide();
                $("div.content").show();
            },
            success: function(data) {
                dat = data;
                $('#editDetailModal input[name="start_date"]').val(data.start_date);
                $('#editDetailModal input[name="end_date"]').val(data.end_date);
            }
        });
    });
   
    $("#editDetailModal").on("hidden.bs.modal", function(){
        $(this).find("input")
            .val('')
            .end()
    });
   
   
    function deleteSprint() {
        var x = confirm("Are you sure you want to delete?");
        if(x)
            return true;
        else
            return false;
    }
    function closeSprint() {
        var x = confirm("Are you sure you want to close this sprint?");
        if(x)
            return true;
        else
            return false;
    }
    
   })
   window.onload = function() {
    @if(!($dates["start_date"] == " - " || $dates["end_date"] == " - " || empty($timesheets) || !(count($timesheets) > 0)))
        $('html, body').animate({
            scrollTop: $('#timesheet-log').offset().top
        }, 'slow');
    @endif
   }
</script>
@endsection