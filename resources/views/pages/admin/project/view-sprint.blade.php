@extends('layouts.admin-dashboard')
@section('main-content')
<section class="project-dashboard">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">Sprint View</h1>
	                <ol class="breadcrumb">
	        		  	<!-- <li><a href="/admin">Admin</a></li> -->
			  			<li><a href='/admin/project/{{$projectinfo->id}}/dashboard'>Projects</a></li>
			  			<li class="active">View</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>

    	@if(!empty($errors->all()))
		    <div class="alert alert-danger">
		        @foreach ($errors->all() as $error)
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ $error }}</span><br/>
		          @endforeach
		    </div>
		@endif
		@if (session('message'))
		    <div class="alert alert-success">
		        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		        <span></span><br/>
		    </div>
		@endif
		<div class="row">
			<div class="col-sm-4">
				<h4 class="admin-section-heading">Basic Info</h4>
				<div class="panel panel-default">
					<!-- <div class="manager-name-holder project-name-holder">
		    			{{$projectinfo->project_name}}
		    		</div> -->
					<div class="info-table">
						<label>Satrt Date: </label>
						<span>{{$projectinfo->start_date}}</span>
					</div>
					<div class="info-table">
						<label>End Date: </label>
						<span>{{$projectinfo->end_date}}</span>
					</div>
					<div class="info-table">
						<label>Team Lead: </label>
						<span>{{$projectinfo->project_manager}}</span>
					</div>
					<div class="info-table">
						<label>Account Manager: </label>
						<span>{{$projectinfo->account_manager}}</span>
					</div>
					<div class="info-table">
						<label>BD Manager: </label>
						<span>{{$projectinfo->bd_manager}}</span>
					</div>
				</div>
			</div>
			@if($user->isAdmin())
				<div class="col-sm-4">
					<h4 class="admin-section-heading">Client Details</h4>
					<div class="panel panel-default">
					  	<!-- <div class="manager-name-holder project-name-holder">
			    			{{$projectinfo->project_name}}
			    		</div> -->
						<div class="info-table">
							<label>Company Name: </label>
							<span>{{$companies->name}}</span>
						</div>
						<div class="info-table">
							<label>Email: </label>
							<span>
								@if(isset($companies))
									{{$companies->email}}
								@endif
							</span>
						</div>
						<div class="info-table">
							<label>Primary Contact: </label>
							<span>
								@if(isset($contacts))
				    				@foreach($contacts as $contact)
				    					{{$contact->first_name}} {{$contact->last_name}}
				    				@endforeach
				    			@endif
				    		</span>
						</div>
						<div class="info-table">
							<label>Primary Contact No.: </label>
							<span>
								Contact number
				    		</span>
						</div>
					</div>
				</div>
			@endif	
			<div class="col-sm-4">
				<h4 class="admin-section-heading">Technologies</h4>
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="tags">
						@if(!$projectTechnologies->isEmpty())
					      		@foreach($projectTechnologies as $projectTechnology)
						      		<!-- <form style="display:inline-block;" action="/admin/project/{{$projectTechnology->id}}/{{$projectinfo->id}}/deleteTechnology" method="post"> -->
						      			<a class="label tag-label">{{$projectTechnology->name}} 

						      			<input name="_method" type="submit"  value="" class="input-delete">
						      			</a>
						      			<input name="_method" type="hidden" value="DELETE">
						      		<!-- </form> -->
					      		@endforeach
					      	@else 
					      		<h5 class="no-margin">No technology selected</h5>
					      	@endif
					    	
					    </div>
					</div>
				</div>

				<!-- <h4 class="admin-section-heading">Add Technologies</h4>
				<form action="/admin/project/{{$projectinfo->id}}/addTechnology" method="post">
	    			<div class="input-group">
		    			<select class="js-example-basic-multiple22 form-control" multiple="multiple" name="technologies[]">
				    	@if(isset($technologies))
				    		@foreach($technologies as $technology)
				    			<?php
				    				$saved = false;
				    				foreach($projectTechnologies as $projectTechnology){
				    					if($projectTechnology->id == $technology->id){
				    						$saved = true;
				    					}
				    				}
				    			?>
				    			@if(isset($projectTechnologies))
				    				@if($saved)
					    				<option value="{{$technology->id}}" selected >{{$technology->name}}</option>
					    			@else
					    				<option value="{{$technology->id}}">{{$technology->name}}</option>
					    			@endif
					    		@else
					    			<option value="{{$technology->id}}">{{$technology->name}}</option>
					    		@endif
				    		@endforeach
				    	@endif
				    	</select>
				    	<span class="input-group-btn">
				    		<button type="submit" class="btn btn-primary btn-sm" style="margin-left: 2px;"><i class="fa fa-plus" ></i></button>
				    	</span>
		    		</div>
		    	</form> -->
			</div>
		</div>
		<!-- ======================== Sprint details ========================= -->
		@if(count($projectSprints) > 0)
			<div class="sprint-header clearfix">

				<h4 class="no-margin pull-left">
					<small style="margin-right: 50px;">{{$projectSprints['id']}}  </small> {{$projectSprints['title']}}
					@if($projectSprints['status'] == "open")
						&nbsp; <span class="label label-success label-lg">In progess</span> 
					@else
						&nbsp; <span class="label label-warning label-lg">Closed</span> 
					@endif
				</h4>
				<div class="pull-right">
					<span class="label label-warning label-lg">{{$projectSprints['start_date']}}</span> - 
					<span class="label label-success label-lg">{{$projectSprints['end_date']}}</span>
				</div>
			</div>
		@endif
		<!-- ========================= Description ===================== -->
		<div class="row">
			<div class="col-md-6 m-t-10">
				<h4 class="admin-section-heading">About Sprint</h4>
				<div class="panel panel-default">
				@if(count($projectSprints) > 0)
                    <div class="panel-body">
                    	{{$projectSprints['description']}}
                    </div>
                @endif
                </div>
                <!-- =================== timelog ============= -->
                <h4 class="admin-section-heading">Timelog History</h4>
				<div class="panel panel-default">
                    <table class="table table-striped">
                    	<thead>
                    		<tr>
                    			<th width="25%">User Name</th>
                    			<th width="35%">Message</th>
                    			<th width="20%">Time</th>
                    			<th width="20%" class="text-right">Date</th>
                    		</tr>
                    	</thead>
                    	<tbody>
                    		@if(count($timelogs)==0)
                    			<tr>
	                    			<td colspan="4">No Record Found</td>
	                    		</tr>
                    		@endif
                    		@foreach($timelogs as $timelog)
	                    		<tr>
	                    			<td>{{$timelog->user->name}}</td>
	                    			<td>{{$timelog->message}}</td>
	                    			<td>{{$timelog->minutes}}</td>
	                    			<td class="text-right">{{$timelog->current_date}}</td>
	                    		</tr>
                    		@endforeach
                    	</tbody>
                    </table>
                </div>
			</div>
			<!-- ====================== list of view resource ========= -->
			<div class="col-md-6 m-t-10">
				<h4 class="admin-section-heading">Resource Details</h4>
				<div class="panel panel-default Panel-description">
                    <table class="table table-striped">
                    	<thead>
                    		<tr>
                    			<th width="40%">List of Resource</th>
                    			<th width="30%">Allocated Time (Hrs)</th>
                    			<th width="30%" class="text-right">Consume Time(Hrs)</th>
                    		</tr>
                    	</thead>
                    	<tbody>
                    		@if(count($report)==0)
                    			<tr>
	                    			<td colspan="3">No Record Found</td>
	                    		</tr>
                    		@endif
	                    	@if(isset($report))
	                    		@foreach($report as $user)
		                    		<tr>
		                    			<td>
		                    				<img src="http://ingridwu.dmmdmcfatter.com/wp-content/uploads/2015/01/placeholder.png" alt="" width="25"> {{$user['name']}}
		                    			</td>
		                    			<td>{{$user['allocated']}}</td>
		                    			<td class="text-right">{{$user['consume']}}</td>
		                    		</tr>
	                    		@endforeach
	                		@endif
	                			
                    		
                    		<!-- <tr>
                    			<td>
                    				<img src="http://ingridwu.dmmdmcfatter.com/wp-content/uploads/2015/01/placeholder.png" alt="" width="25"> 
                    				Sachin
                    			</td>
                    			<td>100</td>
                    			<td class="text-right">3</td>
                    		</tr> -->
                    	</tbody>
                    </table>
                </div>
			</div>
		</div>
	</div>
</section>
@endsection