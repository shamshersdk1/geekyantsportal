@extends('layouts.admin-dashboard')
@section('main-content')
<script type="text/javascript">
    window.clientList = <?php echo json_encode($clientList); ?>;
    window.projectCategories = <?php echo json_encode($projectCategories); ?>;
    window.slackPublicChannels = <?php echo json_encode($slackPublicChannels); ?>;
    window.slackPrivateChannels = <?php echo json_encode($slackPrivateChannels); ?>;
    window.salesExecutiveList = <?php echo json_encode($salesExecutiveList); ?>;
    window.projectCoordinatorList = <?php echo json_encode($projectCoordinatorList); ?>;
    window.projectManagerList = <?php echo json_encode($projectManagerList); ?>;
    window.codeLeadList = <?php echo json_encode($codeLeadList); ?>;
    window.deliveryLeadList = <?php echo json_encode($deliveryLeadList); ?>;
    window.allActiveUserList = <?php echo json_encode($allActiveUserList); ?>;
    window.antManagerId = <?php echo json_encode($antManagerId); ?>;
</script>
<section class="new-project-section" >
   <div class="container-fluid"  ng-app="myApp" ng-controller="createProjectCtrl">
      <div class="breadcrumb-wrap">
         <div class="row">
               <div class="col-sm-8">
                   <h1 class="admin-page-title">Add New Project</h1>
                   <ol class="breadcrumb">
                  <li><a href="/admin">Admin</a></li>
                  <li><a href="{{ url('admin/project') }}">Projects</a></li>
                  <li class="active">Add</li>
               </ol>
               </div>
               <div class="col-sm-4 text-right m-t-10">
                   <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
               </div>
           </div>
       </div>

       @if(!empty($errors->all()))
           <div class="alert alert-danger">
               @foreach ($errors->all() as $error)
                   <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                   <span>{{ $error }}</span><br/>
                 @endforeach
           </div>
       @endif
       @if (session('message'))
           <div class="alert alert-success">
               <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
               <span>{{ session('message') }}</span><br/>
           </div>
       @endif

      <form class="form-horizontal">
          <div class="row">
            <div class="col-md-6">
               <h4>Basic Info</h4>
               <div class="panel panel-default">
                  <div class="panel-body">
                     <div class="form-group">
                        <label class="col-sm-3 control-label ">Client Name</label>
                        <div class="col-sm-9">
                            <ui-select ng-model="selected_client" on-select="select_client($select.selected)" theme="select2" name="name" id="name" ng-style="selected_client_error && {border: '1px solid red'}">
                                <ui-select-match>%%$select.selected.name%%</ui-select-match>
                                <ui-select-choices repeat="client.id as client in clientList | filter: $select.search">
                                    %%client.name%%
                                </ui-select-choices>
                            </ui-select>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-3 control-label">Project Name</label>
                        <div class="col-sm-9">
                              <input type="text" class="form-control" ng-model="project_name" ng-style="project_name_error && project_name == '' && {border: '1px solid red'}">
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                           <div class="row">
                              <div class="col-sm-6">
                                 <div class='input-group date'>
                                        <input name="date_range" date-range-picker class="form-control date-picker" type="text" ng-model="start_date.date" 
                                                min="start_date.min" options="start_date.options" ng-required="true"/>
                                    <span class="input-group-addon">
                                       <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                    </span>
                                 </div>
                              </div>
                              <div class="col-sm-6">
                                 <div class='input-group date'>
                                        <input name="date_range" date-range-picker class="form-control date-picker" type="text" ng-model="end_date.date" 
                                        min="end_date.min" options="end_date.options"/>
                                    <span class="input-group-addon">
                                       <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                    </span>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Project Category</label>
                        <div class="col-sm-9">
                                <ui-select ng-model="selected_project_category" on-select="select_project_category($select.selected)" theme="select2" name="name" id="name" ng-style="selected_project_category_error && {border: '1px solid red'}">
                                    <ui-select-match>%%$select.selected.name%%</ui-select-match>
                                    <ui-select-choices repeat="projectCategory.id as projectCategory in projectCategories | filter: $select.search">
                                        %%projectCategory.name%%
                                    </ui-select-choices>
                                </ui-select>
                            </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-3 control-label">Status</label>
                        <div class="col-sm-9">
                              <select id="selectid2" style="width=35%;" ng-model="status">
                                <option value="open">Open</option>
                                <option value="initial">Initial</option>
                                <option value="sow">SOW</option>
                                <option value="nda">NDA</option>
                                <option value="assigned">Assigned</option>
                                <option value="in_progress">In progress</option>
                                <option value="completed">Completed</option>
                                <option value="closed">Closed</option>
                                <option value="free">Free</option>
                           </select>
                           <small ng-if="status_error" style="color: red">Please select a status</small>
                        </div>
                     </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Slack Public Channel</label>
                        <div class="col-sm-9">
                            <ui-select multiple ng-model="selected_slack_public_channels" on-select="select_slack_public_channels($select.selected)" theme="select2" style="border: 1px solid #D5DDF1 !important">
                                <ui-select-match>%%$item.name%%</ui-select-match>
                                <ui-select-choices repeat="projectCategory.id as projectCategory in slackPublicChannels | filter: $select.search">
                                    %%projectCategory.name%%
                                </ui-select-choices>
                            </ui-select>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-3 control-label">Slack Private Channel</label>
                        <div class="col-sm-9">
                            <ui-select multiple ng-model="selected_slack_private_channels" on-select="select_slack_private_channels($select.selected, $item)" on-remove="deselect_slack_private_channels($item)" theme="select2" style="border: 1px solid #D5DDF1 !important">
                                <ui-select-match>%%$item.name%%</ui-select-match>
                                <ui-select-choices repeat="projectCategory.id as projectCategory in slackPrivateChannels | filter: $select.search">
                                    %%projectCategory.name%%
                                </ui-select-choices>
                            </ui-select>
                            <div ng-repeat="row in selected_private_channel_error_array track by $index">
                              <small ng-show="selected_private_channel_error_array">Bot <span style="color: red;">Ant Manager</span> is not invited in the slack channel <span style="color: red;">%%row.name%% </span><button class="btn btn-warning btn-xs" ng-click="row.reCheck()" ng-disabled="row.loading">Recheck</button><i ng-show="row.loading" class="fa fa-spinner fa-spin"></i> </small>
                              </div>
                        </div>
                     </div>
                     
                     <?php /*   {{-- Multi Select for Slack Private Channel */ ?>
                     
                     
                     <div class="form-group">
                        <label class="col-sm-3 control-label">Is Critical</label>
                        <div class="col-sm-9">  
                           <input type="radio" ng-model="is_critical" value="1"> Yes
                           <input type="radio" ng-model="is_critical" value="0"> No
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-3 control-label">Email Reminder</label>
                        <div class="col-sm-9">  
                           <input type="radio" ng-model="email_reminder" value="1"> Yes
                           <input type="radio" ng-model="email_reminder" value="0"> No
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-6">
               <h4>Resource Info</h4>
               <div class="panel panel-default">
                  <div class="panel-body">
                     <div class="form-group">
                        <label class="col-sm-3 control-label">Sales executive / manager</label>
                        <div class="col-sm-9">
                            <ui-select ng-model="selected_sales_manager" on-select="select_sales_manager($select.selected)" theme="select2" name="name" id="name" ng-style="selected_sales_manager_error && {border: '1px solid red'}">
                                <ui-select-match>%%$select.selected.name%%</ui-select-match>
                                <ui-select-choices repeat="sales_manager.id as sales_manager in salesExecutiveList | filter: $select.search">
                                    %%sales_manager.name%%
                                </ui-select-choices>
                            </ui-select>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-3 control-label">Project coordinator</label>
                        <div class="col-sm-9">
                            <ui-select ng-model="selected_project_coordinator" on-select="select_project_coordinator($select.selected)" theme="select2" name="name" id="name" ng-style="selected_project_coordinator_error && {border: '1px solid red'}">
                                <ui-select-match>%%$select.selected.name%%</ui-select-match>
                                <ui-select-choices repeat="project_coordinator.id as project_coordinator in projectCoordinatorList | filter: $select.search">
                                    %%project_coordinator.name%%
                                </ui-select-choices>
                            </ui-select>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-3 control-label">Project manager</label>
                        <div class="col-sm-9">
                            <ui-select ng-model="selected_project_manager" on-select="select_project_manager($select.selected)" theme="select2" name="name" id="name" ng-style="selected_project_manager_error && {border: '1px solid red'}">
                                <ui-select-match>%%$select.selected.name%%</ui-select-match>
                                <ui-select-choices repeat="project_manager.id as project_manager in projectManagerList | filter: $select.search">
                                    %%project_manager.name%%
                                </ui-select-choices>
                            </ui-select>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-3 control-label">Code Lead</label>
                        <div class="col-sm-9">
                            <ui-select ng-model="selected_code_lead" on-select="select_code_lead($select.selected)" theme="select2" name="name" id="name" ng-style="selected_code_lead_error && {border: '1px solid red'}">
                                <ui-select-match>%%$select.selected.name%%</ui-select-match>
                                <ui-select-choices repeat="code_lead.id as code_lead in codeLeadList | filter: $select.search">
                                    %%code_lead.name%%
                                </ui-select-choices>
                            </ui-select>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-3 control-label">Delivery Lead</label>
                        <div class="col-sm-9">
                            <ui-select ng-model="selected_delivery_lead" on-select="select_delivery_lead($select.selected)" theme="select2" name="name" id="name" ng-style="selected_delivery_lead_error && {border: '1px solid red'}">
                                <ui-select-match>%%$select.selected.name%%</ui-select-match>
                                <ui-select-choices repeat="delivery_lead.id as delivery_lead in deliveryLeadList | filter: $select.search">
                                    %%delivery_lead.name%%
                                </ui-select-choices>
                            </ui-select>
                        </div>
                     </div>
                     
                     <hr />

                     <div class="form-group">
                        <label class="col-sm-3 control-label">UI / UX</label>
                        <div class="col-sm-9">                             
                           <div class="form-inline">
                              <label>
                                 <input type="radio" ng-model="checkBoxes.ui_ux" value="geekyants">By GeekyAnts
                              </label>
                              <label>
                                 <input type="radio" ng-model="checkBoxes.ui_ux" value="client"> By Client
                              </label>
                           </div>   
                        </div>
                     </div>
                     <div class="form-group" ng-hide="checkBoxes.ui_ux == ''">
                        <label class="col-sm-3 control-label">UI / UX Lead</label>
                        <div class="col-sm-9" ng-if="checkBoxes.ui_ux == 'geekyants'">
                            <ui-select ng-model="selected_ui_ux_lead" on-select="select_ui_ux_lead($select.selected)" theme="select2" name="name" id="name" ng-style="selected_ui_ux_lead_error && {border: '1px solid red'}">
                                <ui-select-match>%%$select.selected.name%%</ui-select-match>
                                <ui-select-choices repeat="user.id as user in allActiveUserList | filter: $select.search">
                                    %%user.name%%
                                </ui-select-choices>
                            </ui-select>
                        </div>
                        <div class="col-sm-9" ng-if="checkBoxes.ui_ux == 'client'">
                            <input type="text" class="form-control" ng-model="leads.ui_ux_lead" ng-style="selected_ui_ux_lead_error && leads.ui_ux_lead == '' && {border: '1px solid red'}">
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-3 control-label">DevOp</label>
                        <div class="col-sm-9">                             
                           <div class="form-inline">
                              <label>
                                 <input type="radio" ng-model="checkBoxes.dev_op" value="geekyants" checked>By GeekyAnts
                              </label>
                              <label>
                                 <input type="radio" ng-model="checkBoxes.dev_op" value="client"> By Client
                              </label>
                           </div>   
                        </div>
                     </div>
                     <div class="form-group" ng-hide="checkBoxes.dev_op == ''">
                        <label class="col-sm-3 control-label">DevOp Lead</label>
                        <div class="col-sm-9" ng-if="checkBoxes.dev_op == 'geekyants'">
                            <ui-select ng-model="leads.dev_op_lead" on-select="select_dev_op_lead($select.selected)" theme="select2" name="name" id="name" ng-style="selected_dev_op_lead_error && {border: '1px solid red'}">
                                <ui-select-match>%%$select.selected.name%%</ui-select-match>
                                <ui-select-choices repeat="user.id as user in allActiveUserList | filter: $select.search">
                                    %%user.name%%
                                </ui-select-choices>
                            </ui-select>
                        </div>
                        <div class="col-sm-9" ng-if="checkBoxes.dev_op == 'client'">
                            <input type="text" class="form-control" ng-model="leads.dev_op_lead" ng-style="selected_dev_op_lead_error && leads.dev_op_lead == '' && {border: '1px solid red'}">
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-3 control-label">Architect</label>
                        <div class="col-sm-9">                             
                           <div class="form-inline">
                              <label>
                                 <input type="radio" ng-model="checkBoxes.architect" value="geekyants" checked>By GeekyAnts
                              </label>
                              <label>
                                 <input type="radio" ng-model="checkBoxes.architect" value="client"> By Client
                              </label>
                           </div>   
                        </div>
                     </div>
                     <div class="form-group" ng-hide="checkBoxes.architect == ''">
                        <label class="col-sm-3 control-label">Architect Lead</label>
                        <div class="col-sm-9" ng-if="checkBoxes.architect == 'geekyants'">
                            <ui-select ng-model="leads.architect_lead" on-select="select_architect($select.selected)" theme="select2" name="name" id="name" ng-style="selected_architect_error && {border: '1px solid red'}">
                                <ui-select-match>%%$select.selected.name%%</ui-select-match>
                                <ui-select-choices repeat="user.id as user in allActiveUserList | filter: $select.search">
                                    %%user.name%%
                                </ui-select-choices>
                            </ui-select>
                        </div>
                        <div class="col-sm-9" ng-if="checkBoxes.architect == 'client'">
                            <input type="text" class="form-control" ng-model="leads.architect_lead" ng-style="selected_architect_error && leads.architect_lead == '' && {border: '1px solid red'}">
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-3 control-label">Deployement</label>
                        <div class="col-sm-9">                             
                           <div class="form-inline">
                              <label>
                                 <input type="radio" ng-model="checkBoxes.deployment" value="geekyants" checked>By GeekyAnts
                              </label>
                              <label>
                                 <input type="radio" ng-model="checkBoxes.deployment" value="client"> By Client
                              </label>
                           </div>   
                        </div>
                     </div>
                     <div class="form-group" ng-hide="checkBoxes.deployment == ''">
                        <label class="col-sm-3 control-label">Deployment Lead</label>
                        <div class="col-sm-9" ng-if="checkBoxes.deployment == 'geekyants'">
                            <ui-select ng-model="leads.deployment_lead" on-select="select_deployment_lead($select.selected)" theme="select2" name="name" id="name" ng-style="selected_deployment_lead_error && {border: '1px solid red'}">
                                <ui-select-match>%%$select.selected.name%%</ui-select-match>
                                <ui-select-choices repeat="user.id as user in allActiveUserList | filter: $select.search">
                                    %%user.name%%
                                </ui-select-choices>
                            </ui-select>
                        </div>
                        <div class="col-sm-9" ng-if="checkBoxes.deployment == 'client'">
                            <input type="text" class="form-control" ng-model="leads.deployment_lead" ng-style="selected_deployment_lead_error && leads.deployment_lead == '' && {border: '1px solid red'}">
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-3 control-label">QA</label>
                        <div class="col-sm-9">                             
                           <div class="form-inline">
                              <label>
                                 <input type="radio" ng-model="checkBoxes.qa" value="geekyants" checked> By GeekyAnts
                              </label>
                              <label>
                                 <input type="radio" ng-model="checkBoxes.qa" value="client"> By Client
                              </label>
                           </div>   
                        </div>
                     </div>
                     <div class="form-group" ng-hide="checkBoxes.qa == ''">
                        <label class="col-sm-3 control-label">QA Lead</label>
                        <div class="col-sm-9" ng-if="checkBoxes.qa == 'geekyants'">
                            <ui-select ng-model="leads.qa_lead" on-select="select_qa_lead($select.selected)" theme="select2" name="name" id="name" ng-style="selected_qa_lead_error && {border: '1px solid red'}">
                                <ui-select-match>%%$select.selected.name%%</ui-select-match>
                                <ui-select-choices repeat="user.id as user in allActiveUserList | filter: $select.search">
                                    %%user.name%%
                                </ui-select-choices>
                            </ui-select>
                        </div>
                        <div class="col-sm-9" ng-if="checkBoxes.qa == 'client'">
                            <input type="text" class="form-control" ng-model="leads.qa_lead" ng-style="selected_qa_lead_error && leads.qa_lead == '' && {border: '1px solid red'}">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="text-center">
            <button type="reset" class="btn btn-default">Clear</button>
            <button class="btn btn-success" ng-click="createProject()">Create New Project</button>
         </div>
      </form>
   </div>
</section>
   <script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker({
               format: 'DD-MM-YYYY'
            });
                $('#datetimepicker2').datetimepicker({
               format: 'DD-MM-YYYY'
                });
            });
   </script> 
@endsection