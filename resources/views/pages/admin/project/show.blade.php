@extends('layouts.admin-dashboard')
@section('main-content')
<section class="project-dashboard" ng-app="myApp">
   <div class="container-fluid">
      <div class="breadcrumb-wrap">
         <div class="row">
            <div class="col-sm-8">
               <h1 class="admin-page-title">{{$projectinfo->project_name}}</h1>
               <ol class="breadcrumb">
                  <li><a href="/admin">Admin</a></li>
                  <li><a href="{{ url('admin/project') }}">Projects</a></li>
               </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
               <a href="/admin/project/{{$projectinfo->id}}/edit" class="btn btn-info btn-default crud-btn">Edit</a>
               <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
         </div>
      </div>
      @if(!empty($errors->all()))
      <div class="alert alert-danger">
         @foreach ($errors->all() as $error)
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
         <span>{{ $error }}</span><br/>
         @endforeach
      </div>
      @endif
      @if (session('message'))
      <div class="alert alert-success">
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
         <span>{{ session('message') }}</span><br/>
      </div>
      @endif
      <div class="row">
         <div class="col-sm-8">
            <div class="row">
               <div class="col-sm-6">
                   <basic-info project-id={{$projectinfo->id}}></basic-info>
                  
                  @if($user->isAdmin())
                  
                 <!-- <bill-schedule project-id="1" is-disabled="false"></bill-schedule> -->
                 <add-cron-reminder reference-id={{$projectinfo->id}} reference-type='App\Models\Admin\Project' job-handler="CronBillingScheduleReminder"></add-cron-reminder>
                  @endif
                  <required-resource project-id={{$projectinfo->id}}>  </required-resource>

               </div>
               <div class="col-sm-6">
                  @if($user->isAdmin())
                   <client-detail project-id={{$projectinfo->id}} company-id={{$projectinfo->company_id}}></client-detail>
                  @endif    
                  <!-- <required-resource project-id={{$projectinfo->id}}>  </required-resource> -->
                  <project-timelog project-id={{$projectinfo->id}}></project-timelog>
               </div>
            </div>
            <!--  ==================== USERS ADD ========================= -->
            <div class="row m-t-15">
               <div class="col-sm-12">
                  <add-project-user project-id={{$projectinfo->id}}></add-project-user>
               </div>

                  <!-- <div class="col-md-12 m-t-15">
                     <additional-workday-bonus project-id={{$projectinfo->id}}></additional-workday-bonus>
                  </div>
                   <div class="col-md-12 m-t-15">
                     <onsite-bonus project-id={{$projectinfo->id}}></onsite-bonus>
                  </div> -->
            </div>
         </div>
         <div class="col-sm-4 ">
           
                    <project-technology project-id={{$projectinfo->id}}></project-technology>
             
            <div class="clearfix m-t-10">
             <resource-info project-id={{$projectinfo->id}}></resource-info>
            </div>
            <div class="clearfix m-t-10">
               <google-drive-links reference-id={{$projectinfo->id}} reference-type="App\Models\Admin\Project"></google-drive-links>
            </div>
               <div class="clearfix" style="margin-top: 15px;">
                  <h4 class="admin-section-heading">Internal Notes</h4>
                  @if(!$projectNotes->isEmpty())
                  <div class="panel panel-default">
                     <div style="margin-left: 15px; margin-right: 15px;">
                        <div class="row">
                           <div class="col-md-12">
                              @foreach ( $projectNotes as $note)
                              <div class="info-table internal-notes">
                                 <span style="width:50%">{{$note->note}}</span>
                                 <span style="width:20%"><small class="text-primary">
                                 {{$note->user_name}}
                                 </small></span>
                                 <span style="width:25%"><small>{{datetime_in_view($note->created_at)}}</small></span>
                                 @if($note->private_to_admin == 1)
                                 <span style="width:5%"><i class="fa fa-lock"></i></span>
                                 @endif
                              </div>
                              @endforeach                           
                           </div>
                        </div>
                     </div>
                  </div>
                  @endif
               </div>
               <div class="row">
                  <div class="col-md-12">
                     <div class="form-group">
                        <form method="post" action="/admin/project/{{$projectinfo->id}}/add-comment" >
                           <textarea class="form-control" rows="5" name="comment"></textarea>
                           <div class="clearfix">
                              @if($user->isAdmin())
                              <label>
                              <input type="checkbox" name="private_to_admin" checked> Private to Admin
                              </label>
                              @endif
                              <button type="submit" class="m-t-10 btn btn-primary pull-right btn-sm"> <i class="fa fa-plus fa-fw"></i> Add comment 
                              </button>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
               <!-- <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <comment-data commentable-id="{{$projectinfo->id}}" commentable-type="App\Models\Admin\Project" is-disabled="false" is-privatable="true"></comment-data>
                        </div>
                    </div>
                </div> -->
            </div>
         </div>
      </div>
   </div>
</section>
<!--Add Bonus Modal -->
<div class="modal fade" id="addBonus" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Add Bonus</h4>
         </div>
         <div class="modal-body row form-horizontal">
            <div class="form-group">
               <label class="col-sm-3 control-label">Bonus Type</label>
               <div class="col-sm-7">
                  <select class="form-control">
                     <option>Yearly</option>
                     <option>Monthly</option>
                     <option>Fixed</option>
                  </select>
               </div>
            </div>
            <div class="form-group">
               <label class="col-sm-3 control-label">User</label>
               <div class="col-sm-7">
                  <select class="form-control">
                     <option>User name</option>
                     <option>User</option>
                     <option>user</option>
                  </select>
               </div>
            </div>
            <div class="form-group">
               <label class="col-sm-3 control-label">Time/day</label>
               <div class="col-sm-5">
                  <select class="form-control">
                     <option>Day</option>
                     <option>Hours</option>
                  </select>
               </div>
               <div class="col-sm-2">
                  <input type="text" class="form-control" name="" >
               </div>
            </div>
            <div class="form-group">
               <label class="col-sm-3 control-label">Amount</label>
               <div class="col-sm-7">
                  <input type="text" class="form-control" name="" >
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Submit</button>
         </div>
      </div>
   </div>
</div>
@endsection
@section('js')
@parent
@include('pages/admin/dashboard/edit-detail-modal')
<script>    
   let dat;
   function updateData() {
    dat.start_date = $('#editDetailModal input[name="start_date"]').val();
    dat.end_date = $('#editDetailModal input[name="end_date"]').val();
    $.ajax({
        type: 'POST',
        url: '/admin/project/updateList/'+dat.id,
        dataType: 'text',
        data: dat,
        beforeSend: function() {
            $("div.loading").show();
            $("div.content").hide();
        },
        complete: function() {
            $("div.loading").hide();
            $("div.content").show();
        },
        success: function(data) {
            $('#editDetailModal').modal('hide');
            window.location.reload();
        },
        error: function(errorResponse){
            $('#editDetailModal').modal('hide');
            console.log(errorResponse);
            window.location.reload();
        }
    });
   }
   $(function(){    
    $('#editDetailModal').on("show.bs.modal", function (e) {
        let id = $(e.relatedTarget).data('id')
        $.ajax({
            type: 'GET',
            url: '/admin/project/'+id,
            beforeSend: function() {
                $("div.loading").show();
                $("div.content").hide();
            },
            complete: function() {
                $("div.loading").hide();
                $("div.content").show();
            },
            success: function(data) {
                dat = data;
                $('#editDetailModal input[name="start_date"]').val(data.start_date);
                $('#editDetailModal input[name="end_date"]').val(data.end_date);
            }
        });
    });
   
    $("#editDetailModal").on("hidden.bs.modal", function(){
        $(this).find("input")
            .val('')
            .end()
    });
   
   
    function deleteSprint() {
        var x = confirm("Are you sure you want to delete?");
        if(x)
            return true;
        else
            return false;
    }
    function closeSprint() {
        var x = confirm("Are you sure you want to close this sprint?");
        if(x)
            return true;
        else
            return false;
    }
   // window.onload = function() {
   //  @if(!($dates["start_date"] == " - " || $dates["end_date"] == " - " || empty($timesheets) || !(count($timesheets) > 0)))
   //      $('html, body').animate({
   //          scrollTop: $('#timesheet-log').offset().top
   //      }, 'slow');
   //  @endif
   // }
</script>
@endsection