@extends('layouts.admin-dashboard')
@section('main-content')

<section class="new-project-section">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">Additional Work Days Bonuses</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
			  			<li><a href="{{ url('admin/bonus/additional-work-days') }}">Additional work day bonuses</a></li>
			  			<li class="active">Add</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>

	    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif

		<form class="form-horizontal" method="post" action="/admin/bonus/additional-work-days/{{$AdditionalWorkDaysBonus->id}}" enctype="multipart/form-data">
        {{ method_field('PUT') }}
			<div class="panel panel-default">
				<div class="panel-body">
				    <div class="row">
				    	<div class="col-md-12">
						  	<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Employee Name</label>
						    	<div class="col-sm-5">
						    		@if(isset($userList))
										<select id="selectid2" name="user_id"  style="width=35%;" placeholder= "Select name" required>
                                            @foreach($userList as $user)
                                                @if( $user->id == $userId)
                                                    <option value="{{ $user->id}}" selected> {{$user->name}}</option>
                                                @else
                                                    <option value=" {{$user->id}}"> {{$user->name}}</option>
                                                @endif
                                            @endforeach
										</select>
									@endif
						    	</div>
						  	</div>
						  	<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Project</label>
						    	<div class="col-sm-5">
						    		@if(isset($projectList))
                                        @if(isset($projectId))
										<select id="selectid3" name="project_id"  style="width=35%;" placeholder= "Select project">
                                            @foreach($projectList as $project)
                                                @if(!empty($projectId))
                                                    @if( $project->id == $projectId)
                                                        <option value="{{ $project->id}}" selected> {{$project->project_name}}</option>
                                                    @endif
                                                    <option value=" {{$project->id}}"> {{$project->project_name}}</option>
                                                @endif
                                            @endforeach
										</select>
                                        @else 
                                           <select id="selectid3" name="project_id"  style="width=35%;" placeholder= "Select project">
											<option value=""></option>
											@foreach($projectList as $x)
									        	<option value="{{$x->id}}" >{{$x->project_name}}</option>
										    @endforeach
										</select>
                                        @endif
									@endif
						    	</div>
						  	</div>
						  	<div class="form-group">
								<div class="col-sm-3 col-sm-offset-2">
									<div class='input-group date' id='datetimepicker1'>
									<input type='text' name="from_date" placeholder = "From Date" class="form-control" value="{{$AdditionalWorkDaysBonus->from_date}}" required/>
										<span class="input-group-addon">
											<span><i class="fa fa-calendar" aria-hidden="true"></i></span>
										</span>
									</div>
								</div>
								<div class="col-sm-3">
									<div class='input-group date' id='datetimepicker2'>
										<input type='text' name="to_date" placeholder = "To Date" class="form-control" value="{{$AdditionalWorkDaysBonus->to_date}}" required/>
											<span class="input-group-addon">
												<span><i class="fa fa-calendar" aria-hidden="true"></i></span>
											</span>
									</div>
								</div>
							</div>
                            	<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Amount</label>
						    	<div class="col-sm-5">
						    		<input type="number" id="Amount" class="form-control" placeholder="0.00" name="amount" value="{{$AdditionalWorkDaysBonus->amount}}" required/>
						    	</div>
						  	</div>
                            <div class="form-group row">
                                <label for="Name" class="col-sm-3 col-md-2 control-label">Type:</label>
                                <div class="col-sm-3 col-md-3" >
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="type"  value="weekend" <?php if($type == "weekend") { echo 'checked'; } ?>>
                                        <label class="form-check-label" for="exampleRadios1">
                                            Weekend
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="type"  value="national-holiday" <?php if($type == "national-holiday") { echo 'checked'; } ?>>
                                        <label class="form-check-label" for="exampleRadios1">
                                            National Holiday
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="type" value="leave-or-work-from-home"<?php if($type == "eave-or-work-from-home") { echo 'checked'; } ?>>
                                        <label class="form-check-label" for="exampleRadios2">
                                            On PL/SL/Work from Home
                                        </label>
                                    </div>
                                </div>
                            </div>
						  	<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Notes</label>
						    	<div class="col-sm-5">
						      		<textarea class="form-control" rows="5" name="notes">{{$AdditionalWorkDaysBonus->notes}}</textarea>
						    	</div>
						  	</div>
					  	</div>
					</div>
				</div>
			</div>
  	  		<div class="text-center">
		  		<button type="submit" class="btn btn-success">Save</button>
		  	</div>
		</form>
	</div>
</section>
	<script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker({
					format: 'YYYY-MM-DD'
				});
                $('#datetimepicker2').datetimepicker({
					format: 'YYYY-MM-DD'
                });
            });
	</script> 
@endsection