@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
	<div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Add Technology</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
				  	<li><a href="{{ url('admin/cms-technology') }}">Cms Technology</a></li>
				  	<li class="active">Add</li>
	        	</ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
            	<button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
	    	@if(!empty($errors->all()))
	            <div class="alert alert-danger">
	                @foreach ($errors->all() as $error)
	                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                    <span>{{ $error }}</span><br/>
	                  @endforeach
	            </div>
	        @endif
	        @if (session('message'))
	            <div class="alert alert-success">
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ session('message') }}</span><br/>
	            </div>
	        @endif
        	
            <form name = "myForm" method="post" action="/admin/cms-technology" enctype="multipart/form-data">
            	<div class="panel panel-default panel-white add-new-tech">
            		<div class="row">
		    			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			    		<div class="col-md-6">
			    			<div class="form-group">
			    			    <label for="name">Enter Name:</label>
			    			    <input type="text" name="name" class="form-control" id="exampleInputEmail1" placeholder="Name" value="{{ old('name') }}">
			    			    <p id="name" style="color:red;"></p>
			    			</div>
			    		</div>	
			    			
			    		<div class="col-md-6">
			    			<div class="form-group">
			    			    <label for="logo">Add Logo</label>
			    			    <input type="file" name="logo" id="exampleInputFile" class="form-control">
			    			    <p id="logo" style="color:red;"></p>
			    			</div>
			    		</div>
			    		<div class="col-md-6">
			    			<div class="form-group">
			    			    <label for="shortDescription">Enter Short Description:</label>
			    			    <input type="text" name="short_description" class="form-control" id="exampleInputEmail1" placeholder="Short Description" value="{{ old('short_description') }}">
			    			    <p id="short_description" style="color:red;"></p>
			    			</div>

			    			<div class="checkbox">
			    			    <label>
			    			      <input type="checkbox" name="is_featured"> Featured
			    			    </label>
			    			</div>
			    		</div>
		
			    		<div class="col-md-6">
			    			<div class="form-group">
			    			    <label for="detailedDescription">Enter Detailed Description:</label>
			    			    <input type="text" name="detailed_description" class="form-control" id="exampleInputEmail1" placeholder="Detailed Description" value="{{ old('detailed_description') }}">
			    			</div>
			    		</div>
		    		</div>
		    	</div>
		    	<div class="text-center">
		    		<button type="reset" class="btn btn-default"><i class="fa fa-rotate-left fa-fw"></i> Clear</button> 
		    		<button type="submit" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Save</button>
		    	</div>
		    </form>
		</div>
	</div>
</div>
@endsection