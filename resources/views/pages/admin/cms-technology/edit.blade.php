@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
	<div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Edit Technologie</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
			  		<li><a href="{{ url('admin/cms-technology') }}">Cms Technology</a></li>
			  		<li class="active">Edit</li>
	        	</ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
	    	@if(!empty($errors->all()))
	            <div class="alert alert-danger">
	                @foreach ($errors->all() as $error)
	                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                    <span>{{ $error }}</span><br/>
	                  @endforeach
	            </div>
	        @endif
	        @if (session('message'))
	            <div class="alert alert-success">
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ session('message') }}</span><br/>
	            </div>
	        @endif
        	
	        <form name = "myForm" style="padding:10px;" method="post" action="/admin/cms-technology/{{$technology->id}}" enctype="multipart/form-data" > 
	        	<div class="panel panel-default panel-white custom-space">
	            	<div class="row">
					    <input name="_method" type="hidden" value="PUT">
			    		<div class="col-md-6">
			    			<div class="form-group">
			    			    <label for="exampleInputEmail1">Enter Name:</label>
			    			    <input type="text" name="name" class="form-control" id="exampleInputEmail1" value="{{$technology->name}}">
			    			    <p id="name" style="color:red;"></p>
			    			</div>
			    		</div>
					    		
					    			
			    		<div class="col-md-6">
			    			<div class="form-group">
			    			    <label for="exampleInputEmail1">Enter Slug:</label>
			    			    <input type="text" name="slug" class="form-control" id="exampleInputEmail1" value="{{$technology->slug}}">
			    			    <p id="slug" style="color:red;"></p>
			    			</div>
			    		</div>
			    		<div class="col-md-6">
			    			<div class="form-group">
			    			    <label for="exampleInputEmail1">Enter Short Description:</label>
			    			    <input type="text" name="short_description" class="form-control" id="exampleInputEmail1" value="{{$technology->short_description}}">
			    			    <p id="short_description" style="color:red;"></p>
			    			</div>
			    		</div>
			    		<div class="col-md-6">
			    			<div class="form-group">
			    			    <label for="exampleInputEmail1">Enter Detailed Description:</label>
			    			    <input type="text" name="detailed_description" class="form-control" id="exampleInputEmail1" value="{{$technology->detailed_description}}">
			    			
			    			</div>
			    		</div>
			    		<div class="col-md-6">
			    			<div class="form-group">
			    			    <label for="exampleInputFile">Add Logo</label>
			    			    <input type="file" name="logo" id="exampleInputFile" value="c:/passwords.txt">
			    			    <img style="max-width:200px; max-height:200px; margin-top: 10px;" src="/images/{{$technology->logo}}">
			    			    <p id="logo" style="color:red;"></p>
			    			</div>
			    		</div>				    			
			    		<div class="col-md-12">
			    			@if($technology->is_featured == 1)
				    			<div class="checkbox">
				    			    <label>
				    			      <input type="checkbox" name="is_featured" checked> Featured
				    			    </label>
				    			</div>	
				    		@else
				    			<div class="checkbox">
				    			    <label>
				    			      <input type="checkbox" name="is_featured"> Featured
				    			    </label>
				    			</div>
				    		@endif	
			    		</div>
					</div>
				</div>
				<div class="text-center">
					<button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-rotate-left fa-fw"></i> Cancel</button>
					<button type="submit" class="btn btn-success">Update</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection