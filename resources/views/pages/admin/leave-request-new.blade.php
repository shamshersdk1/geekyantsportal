@extends('layouts.admin-dashboard')
    @section('main-content')
    <div class="container-fluid" ng-app="myApp">
        <div class="breadcrumb-wrap">
            <div class="row">
                <div class="col-sm-12">
                    @if(!empty($title))
                    <h1 class="admin-page-title">{{$title}}</h1>
                    @else
                    <h1 class="admin-page-title">Leave Dashboardss</h1>
                    @endif
                    <ol class="breadcrumb">
                        <li><a href="/admin">Admin</a></li>
                        <li class="active">Leave dashboardss </li>
                    </ol>
                </div>
                <!-- <div class="col-sm-4 text-right m-t-10">
                    <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
                </div> -->
            </div>
        </div>
        <!-- ============================= admin leave Board ================================ -->
        <div class="row" ng-controller="leaveApplyCtrl" ng-init="getUserList()">
            <div class="col-md-12">
                @if(!empty($errors->all()))
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <span>{{ $error }}</span><br/>
                          @endforeach
                    </div>
                @endif
                @if (session('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ session('message') }}</span><br/>
                    </div>
                @endif
            </div>
            <div class="col-md-12  ">
                <div class="row search-section-top bg-white">
                    <form action="/admin/leave-section/confirmation"  method="get" class="col-md-10">
                        <div class="input-group">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <input type="text" id="su" name="searchuser" placeholder="User's Name" value="<?=( isset( $_GET['searchuser'] ) ? $_GET['searchuser'] : '' )?>"   class="form-control searchuser" >
                                    </div>
                                    <div class="col-md-3">
                                        <div class='input-group date' id='datetimepickerStart_search'>
                                            <input type='text' id="sd" class="form-control" value="<?=( isset( $_GET['start_date'] ) ? $_GET['start_date'] : '' )?>" autocomplete="off" name="start_date" placeholder="Start Date" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class='input-group date' id='datetimepickerEnd_search'>
                                            <input type='text' id="ed" class="form-control" name="end_date" value="<?=( isset( $_GET['end_date'] ) ? $_GET['end_date'] : '' )?>" autocomplete="off" placeholder="End Date" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <button type="submit" class="btn btn-primary ">Search</button>
                                        <!-- <a href="/admin/leave-section/confirmation" class="btn btn-primary">Reset</a> -->
                                        <button class="btn btn-default" onclick="resetForm()">Reset</button>
                                    </div>  
                                </div>
                            </div>    
                        </div>
                    </form>
                    <div class="col-md-2">
                        <button class="btn btn-success pull-right" type="button" ng-click="openModal()">Apply New Leave</button>
                    </div>
                </div>
            </div>
            <div class="col-md-12 user-list-view">
                <div class="clearfix m-t-15">
                    <h4 class="pull-left m-t-10">List of Pending Leaves.</h4>
                    <div class="pull-right">
                        <ul class="btn-group no-margin">
                            <a href="/admin/leave-section/confirmation?@if(strpos(Request::getQueryString(), 'searchuser') !== false)searchuser={{str_replace(' ','+',Request::get('searchuser'))}}&@endif @if(strpos(Request::getQueryString(), 'status') !== false)status={{Request::get('status')}}&@endif type=" type="button" class="btn btn-default @if (!Request::get('type')) active @endif" >All</a>
                            <a href="/admin/leave-section/confirmation?@if(strpos(Request::getQueryString(), 'searchuser') !== false)searchuser={{str_replace(' ','+',Request::get('searchuser'))}}&@endif @if(strpos(Request::getQueryString(), 'status') !== false)status={{Request::get('status')}}&@endif type=sick" type="button" class="btn btn-default @if(Request::get('type') == 'sick')active @endif">Sick Leaves</a>
                            <a href="/admin/leave-section/confirmation?@if(strpos(Request::getQueryString(), 'searchuser') !== false)searchuser={{str_replace(' ','+',Request::get('searchuser'))}}&@endif @if(strpos(Request::getQueryString(), 'status') !== false)status={{Request::get('status')}}&@endif type=paid" type="button" class="btn btn-default @if(Request::get('type') == 'paid') active @endif">Paid Leaves</a>
                            <a href="/admin/leave-section/confirmation?@if(strpos(Request::getQueryString(), 'searchuser') !== false)searchuser={{str_replace(' ','+',Request::get('searchuser'))}}&@endif @if(strpos(Request::getQueryString(), 'status') !== false)status={{Request::get('status')}}&@endif type=others" type="button" class="btn btn-default @if(Request::get('type') == 'others') active @endif">Others</a>
                        </ul>
                    </div>
                </div>
                <div class="panel panel-default clearfix">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="20%">Name</th>
                                    <th width="15%">Type</th>
                                    <th width="10%">From Date</th>
                                    <th width="10%">To Date</th>
                                    <th width="10%">Working Days</th>
                                    <th width="10%">Applied On</th>
                                    <th width="10%">Status</th>
                                    <th width="20%" class="text-right">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($pendingLeaves))
                                    @if(count($pendingLeaves) > 0)
                                        @foreach($pendingLeaves as $key => $leave)
                                            @if(isset($leave->user))
                                                <tr>
                                                    <td>{{$leave->id}}</td>
                                                    <td>{{$leave->user->name}}</td>
                                                    <td>
                                                        @if($leave->leaveType && $leave->leaveType->code=='sick')
                                                            <span class="label label-danger custom-label">Sick Leave</span>
                                                        @elseif($leave->leaveType && $leave->leaveType->code=='paid')
                                                            <span class="label label-primary custom-label">Paid Leave</span>
                                                        @else
                                                            <span class="label label-success custom-label">{{ !empty($leave->leaveType->code) ? $leave->leaveType->title : '' }}</span>
                                                        @endif
                                                    </td>
                                                    <td>{{date("d M Y", strtotime($leave->start_date))}}</td>
                                                    <td>{{date("d M Y", strtotime($leave->end_date))}}</td>
                                                    @if($leave->days==0.5)
                                                    <td>{{ucfirst($leave->half)}} half</td>
                                                    @else
                                                    <td>{{floor($leave->days)}}</td>
                                                    @endif
                                                    <td>{{datetime_in_view($leave->created_at)}}</td>
                                                    <td >
                                                        @if($leave->status == "approved")
                                                            <span class="label label-success custom-label">Approved</span>
                                                        @elseif($leave->status == "pending")
                                                            <span class="label label-warning custom-label">Pending</span>
                                                        @elseif($leave->status == "rejected")
                                                            <span class="label label-danger custom-label">Rejected</span>
                                                        @elseif($leave->status == "cancelled")
                                                            <span class="label label-warning custom-label">Cancelled</span>
                                                        @else
                                                            <span class="label label-info custom-label">No Status</span>
                                                        @endif
                                                    </td>
                                                    <td class="text-right">
                                                            @if($leave->status == "pending")
                                                        <!-- <form action="/admin/leave-section/status/{{$leave->id}}" id="leavesForm{{$leave->id}}" method="post" style="display:inline">
                                                                <button class="btn btn-success btn-sm" type="submit" name="approve" value="approve">Approve</button>
                                                        </form>
                                                                <button class="btn btn-danger btn-sm" name="reject" value="reject"
                                                                    data-toggle="modal" data-target="#rejectLeaveModal"
                                                                    data-id="{{$leave->id}}">Reject</button> -->
                                                             <!--       <button class="btn btn-primary btn-sm" type="button" data-toggle="modal" data-target="#editLeaveModal" data-id="{{$leave->id}}" data-title="Algorithms"><i class="fa fa-eye"></i> View</button> -->
                                                                <button class="btn btn-primary btn-sm" type="button"
                                                                    ng-click="showModal({{$leave->id}})" data-title="Algorithms"><i class="fa fa-eye"></i> View</button> 
                                                            @else
                                                                <!-- <button class="btn btn-success btn-sm" disabled="disabled">Approve</button>
                                                                <button class="btn btn-danger btn-sm" disabled="disabled">Reject</button> -->
                                                            @endif
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="8">No Record found.</td>
                                        </tr>
                                    @endif
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="pull-right">
                    {{ $pendingLeaves->links(); }}
                </div>
            </div>
            <div class="col-md-12">
                <h4>List of Upcoming Leaves.</h4>
                <div class="panel panel-default">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="20%">Name</th>
                                    <th width="15%">Type</th>
                                    <th width="10%">From Date</th>
                                    <th width="10%">To Date</th>
                                    <th width="10%">Working Days</th>
                                    <th width="10%">Applied On</th>
                                    <th width="10%">Approved By</th>
                                    <th width="10%">Status</th>
                                    <th width="20%" class="text-right">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($upcomingLeaves))
                                    @if(count($upcomingLeaves) > 0)
                                        @foreach($upcomingLeaves as $leave)
                                            @if(isset($leave->user))
                                                <tr>
                                                    <td>{{$leave->id}}</td>
                                                    <td>{{$leave->user->name}}</td>
                                                    <td>
                                                        @if($leave->leaveType && $leave->leaveType->code=='sick')
                                                        <span class="label label-danger custom-label">Sick Leave</span>
                                                        @elseif($leave->leaveType && $leave->leaveType->code=='paid')
                                                            <span class="label label-primary custom-label">Paid Leave</span>
                                                        @else
                                                            <span class="label label-success custom-label">{{ !empty($leave->leaveType->code) ? $leave->leaveType->title : '' }}</span>
                                                        @endif
                                                    </td>
                                                    <td>{{date("d M Y", strtotime($leave->start_date))}}</td>
                                                    <td>{{date("d M Y", strtotime($leave->end_date))}}</td>
                                                    @if($leave->days==0.5)
                                                    <td>{{ucfirst($leave->half)}} half</td>
                                                    @else
                                                    <td>{{floor($leave->days)}}</td>
                                                    @endif
                                                    <td>{{datetime_in_view($leave->created_at)}}</td>
                                                    <td>@if(!empty($leave->approver)){{$leave->approver->name}}@endif</td>
                                                    <td >
                                                        @if($leave->status == "approved")
                                                            <span class="label label-success custom-label">Approved</span>
                                                        @elseif($leave->status == "pending")
                                                            <span class="label label-warning custom-label">Pending</span>
                                                        @elseif($leave->status == "rejected")
                                                            <span class="label label-danger custom-label">Rejected</span>
                                                        @elseif($leave->status == "cancelled")
                                                            <span class="label label-warning custom-label">Cancelled</span>
                                                        @else
                                                            <span class="label label-info custom-label">No Status</span>
                                                        @endif
                                                    </td>
                                                    <td class="text-right">
                                                        @if($leave->status == "pending" || $leave->status == "approved")
                                                        <button class="btn btn-primary btn-sm" type="button"
                                                                    ng-click="showModal({{$leave->id}})" data-title="Algorithms"><i class="fa fa-eye"></i> View</button> 
                                                        <!-- <button class="btn btn-primary btn-sm" type="button" data-toggle="modal" data-target="#editLeaveModal" data-id="{{$leave->id}}" data-title="Algorithms"><i class="fa fa-eye"></i> View</button> -->
                                                            <!-- <form style="display:inline" action="/admin/employee-leave/status/{{$leave->id}}" method="post" class="cancelform">
                                                                <button class="btn btn-danger btn-sm" name="cancel" value="cancel">Cancel</button>
                                                            </form> -->
                                                        @else
                                                            <!-- <button class="btn btn-danger btn-sm" name="cancel" value="cancel" disabled>Cancel</button> -->
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="10">No Record found.</td>
                                        </tr>
                                    @endif
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="pull-right">
                    {{ $upcomingLeaves->links(); }}
                </div>
            </div>
                
            <div class="col-md-12">
                <div class="clearfix m-t-15"> 
                    <h4 class="pull-left m-t-10">List of Leaves</h4>
                    <div class="pull-right">
                        <ul class="btn-group no-margin">
                            <a href="/admin/leave-section/confirmation?@if(strpos(Request::getQueryString(), 'searchuser') !== false)searchuser={{str_replace(' ','+',Request::get('searchuser'))}}&@endif @if(strpos(Request::getQueryString(), 'type') !== false)type={{Request::get('type')}}&@endif status=" type="button" class="btn btn-default @if (!Request::get('status')) active @endif" >All</a>
                            <a href="/admin/leave-section/confirmation?@if(strpos(Request::getQueryString(), 'searchuser') !== false)searchuser={{str_replace(' ','+',Request::get('searchuser'))}}&@endif @if(strpos(Request::getQueryString(), 'type') !== false)type={{Request::get('type')}}&@endif status=pending" type="button" class="btn btn-default @if(Request::get('status') == 'pending') active @endif">Pending</a>
                            <a href="/admin/leave-section/confirmation?@if(strpos(Request::getQueryString(), 'searchuser') !== false)searchuser={{str_replace(' ','+',Request::get('searchuser'))}}&@endif @if(strpos(Request::getQueryString(), 'type') !== false)type={{Request::get('type')}}&@endif status=approved" type="button" class="btn btn-default @if(Request::get('status') == 'approved') active @endif">Approved</a>
                            <a href="/admin/leave-section/confirmation?@if(strpos(Request::getQueryString(), 'searchuser') !== false)searchuser={{str_replace(' ','+',Request::get('searchuser'))}}&@endif @if(strpos(Request::getQueryString(), 'type') !== false)type={{Request::get('type')}}&@endif status=rejected" type="button" class="btn btn-default @if(Request::get('status') == 'rejected') active @endif">Rejected</a>
                            <a href="/admin/leave-section/confirmation?@if(strpos(Request::getQueryString(), 'searchuser') !== false)searchuser={{str_replace(' ','+',Request::get('searchuser'))}}&@endif @if(strpos(Request::getQueryString(), 'type') !== false)type={{Request::get('type')}}&@endif status=cancelled" type="button" class="btn btn-default @if(Request::get('status') == 'cancelled') active @endif">Cancelled</a>
                        </ul>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="20%">Name</th>
                                    <th width="15%">Type</th>
                                    <th width="10%">From Date</th>
                                    <th width="10%">To Date</th>
                                    <th width="10%">Working Days</th>
                                    <th width="10%">Applied On</th>
                                    <th width="10%">Status</th>
                                    <th width="20%" class="text-right">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($leaveList))
                                    @if(count($leaveList) > 0)
                                        @foreach($leaveList as $leave)
                                            @if(isset($leave->user))
                                                <tr>
                                                    <td>{{$leave->id}}</td>
                                                    <td>{{$leave->user->name}}</td>
                                                    <td>
                                                        @if($leave->leaveType && $leave->leaveType->code=='sick')
                                                            <span class="label label-danger custom-label">Sick Leave</span>
                                                        @elseif($leave->leaveType && $leave->leaveType->code=='paid')
                                                            <span class="label label-primary custom-label">Paid Leave</span>
                                                        @else
                                                            <span class="label label-success custom-label">{{ !empty($leave->leaveType->code) ? $leave->leaveType->title : '' }}</span>
                                                        @endif
                                                    </td>
                                                    <td>{{date("d M Y", strtotime($leave->start_date))}}</td>
                                                    <td>{{date("d M Y", strtotime($leave->end_date))}}</td>
                                                    @if($leave->days==0.5)
                                                    <td>{{ucfirst($leave->half)}} half</td>
                                                    @else
                                                    <td>{{floor($leave->days)}}</td>
                                                    @endif
                                                    <td>{{datetime_in_view($leave->created_at)}}</td>
                                                    <td >
                                                        @if($leave->status == "approved")
                                                            <span class="label label-success custom-label">Approved</span>
                                                        @elseif($leave->status == "pending")
                                                            <span class="label label-warning custom-label">Pending</span>
                                                        @elseif($leave->status == "rejected")
                                                            <span class="label label-danger custom-label">Rejected</span>
                                                        @elseif($leave->status == "cancelled")
                                                            <span class="label label-warning custom-label">Cancelled</span>
                                                        @else
                                                            <span class="label label-info custom-label">No Status</span>
                                                        @endif
                                                    </td>
                                                    <td class="text-right">
                                                        <button class="btn btn-primary btn-sm" type="button"
                                                                    ng-click="showModal({{$leave->id}})" data-title="Algorithms"><i class="fa fa-eye"></i> View</button> 
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="8">No Record found.</td>
                                        </tr>
                                    @endif
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="pull-right">
                    {{ $leaveList->links(); }}
                </div>
            </div>
        </div>
    </div>

<!-- =============================Leave Modal ================================ -->
    @include('pages/admin/admin-leave-section/edit-leave-modal')
    @include('pages/admin/admin-leave-section/javascript')
<script>
    function resetForm() {
        document.getElementById("su").value = null;
        document.getElementById("sd").value = null;
        document.getElementById("ed").value = null;
    }
    $(function(){
        $( ".searchuser" ).autocomplete({
            source: {{$jsonuser}}
        });
        $('#datetimepickerStart_search').datetimepicker({
            minDate: new Date().getMonth() <= 3 ? new Date(new Date().getFullYear()-1+"-01-01") : new Date(new Date().getFullYear()+"-01-01"), //Set min date to April 1
            //Set max date to upcoming march
            maxDate: new Date().getMonth() <= 2 ? new Date(new Date().getFullYear()+"-12-31") : new Date(new Date().getFullYear()+1+"-12-31"),
            format: 'YYYY-MM-DD'
        });

        $('#datetimepickerEnd_search').datetimepicker({
            //Set max date to upcoming march
            maxDate: new Date().getMonth() <= 2 ? new Date(new Date().getFullYear()+"-12-31") : new Date(new Date().getFullYear()+1+"-12-31"),
            format: 'YYYY-MM-DD'
        });

        $("#datetimepickerStart_search").on("dp.change", function (e) {
            $('#datetimepickerEnd_search').data("DateTimePicker").minDate(e.date);
        });

        $("#btnReset").click(function(){
           window.location.href='/admin/leave-section/confirmation';
    }); 
    });
</script>

@endsection