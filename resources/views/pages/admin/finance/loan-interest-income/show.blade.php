@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-6">
                <h1 class="admin-page-title">Loan Interest</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li>Loan Interest</li>
        		</ol>
            </div>
            <div class="col-sm-6">
                @if(!$monthObj->loanInterestSetting || ($monthObj->loanInterestSetting &&  $monthObj->loanInterestSetting->value == 'open'))
                    <div class="pull-right m-t-10">
                        <span>
                            <a href="/admin/loan-interest-income/{{$monthObj->id}}/regenerate" class="btn btn-warning"><i class="fa fa-gear fa-fw"></i>Regenerate</a>
                        </span>
                    </div>
                    <div class="pull-right m-t-10">
                        <span>
                            <a href="/admin/loan-interest-income/{{$monthObj->id}}/status-month" class="btn btn-danger"><i class="fa fa-lock fa-fw"></i>Lock</a>
                        </span>
                    </div>
                @elseif(($monthObj->getMonth(date("Y/m/d")) == $monthObj->id ) && $monthObj->loanInterestSetting && $monthObj->loanInterestSetting->value == 'locked')
                    <div class="pull-right m-t-10">
                        <span>
                            <a href="/admin/loan-interest-income/{{$monthObj->id}}/status-month" class="btn btn-success"><i class="fa fa-unlock fa-fw"></i>UnLock</a>
                        </span>
                    </div>
                @endif
                
            </div>
		</div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <h4
             class="text-warning"> Interest as per SBI rate applicable as on 01.04.2018 - 12.65%</h4
            >
        </div>
    </div> 
    @if($monthObj->loanInterestSetting &&  $monthObj->loanInterestSetting->value == 'locked')
        <div class="pull pull-right">
            <span class="label label-primary">Locked at {{datetime_in_view($monthObj->loanInterestSetting->created_at)}}</span>
        </div>
    @endif
	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
    </div>

    <form name="editForm" method="post" action="/admin/loan-interest-income/{{$monthObj->id}}/update">
        @if(!$monthObj->loanInterestSetting ||  $monthObj->loanInterestSetting->value == 'open')
            <div class="row">
                <div class="pull-right m-t-1 ">
                    <button type="submit" class="btn btn-success "><i class="fa fa-plus fa-fw"></i>Save</button>
                </div>
            </div>
        @endif
        <div class="user-list-view">
            <div class="panel panel-default">
                <table class="table table-striped" id="interest-table">
                    <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th class="text-center">Employee Id</th>
                        <th class="text-center">Name</th>
                        <th class="text-center">Total Outstanding Amount</th>
                        <th class="text-center">Interest</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($loanInterests) > 0)
                        @foreach($loanInterests as $loanInterest)
                            <tr>
                                <td class="text-center"></td>
                                <td class="text-center">{{ $loanInterest->user->employee_id}}</td>
                                <td class="text-center">{{ $loanInterest->user->name}}</td>
                                <td class="text-center">{{$loanInterest->total_outstanding_amount}}</td>
                                <td class="text-center">
                                    @if($monthObj->loanInterestSetting && $monthObj->loanInterestSetting->value == 'locked')
                                        {{$loanInterest->interest}}
                                    @else
                                        <input name="interest[{{$loanInterest->id}}]" type="number" value="{{$loanInterest->interest}}"/>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5" class="text-center">
                                No Records found
                            </td>
                        </tr>

                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </form>
</div>
<script>
    
    $(document).ready(function() {
        var t = $('#interest-table').DataTable( {
            pageLength:500, 
            scrollY:        true,
            scrollX:        true,
            scrollCollapse: true,
            paging:         false,
            "columnDefs": [ {
                "searchable": false,
                "orderable": false,
                "targets": 0
            } ],
            "order": [[ 1, 'asc' ]],
            fixedHeader: true
        } );
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    });

    $('#selectid3').change(function(){
        var optionSelected = $("option:selected", this);
        optionValue = this.value;
        if (optionValue) { 
            window.location = "/admin/loan-interest-income/"+optionValue; 
        }
    });
    
</script>
@endsection
