@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">List of Payroll</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li>List of Payrolls</li>
                </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <a href="create-payroll-csv/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Create payroll</a>
            </div>
        </div>
    </div>
    <div class="row row-sm">
        <div class="col-sm-3">
         <div class="panel panel-default">
              <div class="info-table">
                  <label>User</label>
                  <label>{{$user->name}} ({{$user->employee_id}})</label>
              </div>
              <div class="info-table">
                  <label>Month</label>
                  <label>{{$monthObj->formatMonth()}}</label>
              </div>
              <div class="info-table">
                  <label>Total Working Day in Month</label>
                  <label>22</label>
              </div>
          </div>
        </div>
      </div>
    <div class="panel panel-default"> 
       

          <table class="table table-striped">
             <tbody>
                <tr>
                   <th>#</th>
                   <th>Transaction Date</th>
                   <th>Reference</th>
                   <th>Type</th>
                   <th>Amount</th>
                   <th>Created At</th>
                </tr>
                @if(!empty($transactions))
                    @foreach($transactions as $transaction)
                    <tr>
                        <td class="td-text">{{$transaction->id}}</td>
                        <td class="td-text">{{date_in_view($transaction->date)}}</td>
                        <td class="td-text">
                          @if($transaction->reference_type == 'App\Models\Admin\Finance\PayroleGroupRuleMonth')
                              {{$transaction->reference->key->value}} 
                              @if($transaction->reference->calculated_upon == 0)
                                ({{$transaction->reference->calculation_value}} {{$transaction->reference->calculation_type}} Gross Salary)
                              @elseif($transaction->reference->calculated_upon > 0 )
                                ({{$transaction->reference->calculation_value}} {{$transaction->reference->calculation_type}} {{$transaction->reference->calc_upon->key->value}})
                              @endif
                          @elseif($transaction->reference_type == 'App\Models\Admin\Finance\ItSavingUserData')
                              TDS
                          @elseif($transaction->reference_type == 'App\Models\Admin\AppraisalBonus')
                            @if($transaction->reference->type == 'annual')
                              Annual Bonus
                            @elseif($transaction->reference->type == 'confirmation')
                              Confirmation Bonus
                            @elseif($transaction->reference->type == 'monthly_variable')
                              Monthly Variable Bonus
                            @endif
                          @else
                              {{$transaction->reference_type}}
                          @endif
                        </td>
                        <td>
                          @if($transaction->type == 'credit')
                            <span class="label label-primary custom-label">{{$transaction->type}}</span>
                          @else
                            <span class="label label-danger custom-label">{{$transaction->type}}</span>
                          @endif
                        </td>
                        <td class="td-text">{{$transaction->amount}}</td>
                        <td class="td-text">{{datetime_in_view($transaction->created_at)}}</td>
                    </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="6">No records found</td>
                    </tr>
                @endif
             </tbody>
          </table>
       </div>
    </div> 
</div>

@endsection


@section('js')
    <script>
        $(function(){
            $(".delete-form").submit(function(event){
                return confirm('Are you sure?');
            });
        });
    </script>

@endsection
