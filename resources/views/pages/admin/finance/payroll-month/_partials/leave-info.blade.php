<div class="panel panel-default">
    <div class="info-table">
        <label>Encashed PL</label>
        <span>
            2
            <span class="pull-right">
                <small><i class="fa fa-rupee text-muted"></i></small>
                2000
                <i class="fa fa-exclamation text-warning"></i>
            </span>
        </span>
    </div>
    <div class="info-table">
        <label>Additional</label>
        <span>
            2 hours
            <span class="pull-right">
                <small><i class="fa fa-rupee text-muted"></i></small>
                300
                <i class="fa fa-exclamation text-warning"></i>
            </span>
        </span>
    </div>
    <div class="info-table">
        <label>Loss of Pay</label>
        <span>
            1
            <span class="pull-right">
                <small><i class="fa fa-rupee text-muted"></i></small>
                2000
                <i class="fa fa-exclamation text-warning"></i>
            </span>
        </span>
    </div>
</div>