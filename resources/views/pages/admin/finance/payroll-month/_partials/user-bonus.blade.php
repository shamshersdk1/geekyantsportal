<div class="panel panel-default">
    @if(count($bonuses) > 0)
        @foreach($bonuses as $bonus)
        <div class="info-table">
            <label>{{ucfirst($bonus->type)}} Bonus</label>
            <span>
                <small>{{date_in_view($bonus->date)}}</small>
                <span class="pull-right">
                    <small><i class="fa fa-rupee text-muted"></i></small>
                    {{$bonus->amount}}
                    <i class="fa fa-check text-success"></i>
                </span>
            </span>
        </div>
        @endforeach
    @else
        <div class="info-table">
            <label>No bonus found for the month</label>
        </div>
    @endif
</div>
