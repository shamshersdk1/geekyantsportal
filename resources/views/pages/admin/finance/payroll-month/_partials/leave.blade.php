<div class="panel panel-default">
    <div class="info-table">
        <label>Number of working days</label>
        <span>{{$userLeave['working_days'] ? $userLeave['working_days'] : 0 }}</span>
    </div>
    <div class="info-table">
        <label>Number of days worked</label>
        <span>{{$userLeave['days_worked'] ? $userLeave['days_worked'] : 0 }}</span>
    </div>
    <div class="info-table">
        <label>PL</label>
        <span>{{$userLeave['paid_leave'] ? $userLeave['paid_leave'] : 0 }}</span>
    </div>
    <div class="info-table">
        <label>SL</label>
        <span>{{$userLeave['sick_leave'] ? $userLeave['sick_leave'] : 0 }}</span>
    </div>
</div>
