<!--
TDS deducted last month = X

A =  Annual Salary = Monthly CTC * 12 (also considering the incrment during the period) + Advance Salary

B Deduction  =
          HRA Exemption: Lower of the following: -
            = HRA
            = 40% of Basic
            = (Actual Rent to Paid for the Financial Year) - (10% of Basic)

        + Standard Deduction: 40,000/- flat
        + LTA - For which Document is Submitted (FROM EDF) 0
        + Employer PF (12%) - annual
        + Employer PF (1%) - annual
        + Professional Tax Paid - fixed 200 *12

Net taxable Salary : C = A - B

C = Salary from GeeyAnts Monthly. // NET salary

D = we need to calculate 'C' for each month (12 months) and add them up to get 'Salary from GeeyAnts for the financial Year'

E = INVESTMENTS U/S 80C, capped at Rs 1.5 Lac(max 1.5 lac)
150000
F = Other DEDUCTIONS

G = Total IT Saving = E+F

Taxable Income for the financial Year = (D) - (G) -->

<div class="panel panel-default">
      <div class="info-table">
            <label>Annual Salary:</label>
            <span>{{$tdsData['gross_annual']['gross_annual_salary'] or 0}}</span>
      </div>
      <div class="info-table">
            <label>Annual Fixed Bonus:</label>
            <span>{{$tdsData['gross_annual']['gross_annual_fixed_bonus'] or 0}}</span>
      </div>
      <div class="info-table">
            <label>Annual Monthly Bonus:</label>
            <span>{{$tdsData['gross_annual']['monthly_variable_bonus'] or 0}}</span>
      </div>
      <div class="info-table">
            <label>Confirmation Bonus:</label>
            <span>{{$tdsData['gross_annual']['confirmation_bonus'] or 0}}</span>
      </div>
      <div class="info-table">
            <label>Other Bonus:</label>
            <span>{{$tdsData['gross_annual']['other_bonuses'] or 0}}</span>
      </div>
      <div class="info-table">
            <label>HRA Exemption 1 (HRA):</label>
            <span>{{$tdsData['hra_exemption']['exemption_1'] or 0}}</span>
      </div>
      <div class="info-table">
            <label>HRA Exemption 2 (40% of basic):</label>
            <span>{{$tdsData['hra_exemption']['exemption_2'] or 0}}</span>
      </div>
      <div class="info-table">
            <label>HRA Exemption 3 (Actual Rent to Paid for the Financial Year) - (10% of Basic):</label>
            <span>{{$tdsData['hra_exemption']['exemption_3'] or 0}}</span>
      </div>
      <div class="info-table">
            <label>HRA Exemption</label>
            <span>{{$tdsData['hra_exemption']['exemption'] or 0}}</span>
      </div>
      <div class="info-table">
            <label>Standard Deduction</label>
            <span>{{$tdsData['standard_deduction'] or 0}}</span>
      </div>
      <div class="info-table">
            <label>LTA</label>
            <span>{{$tdsData['lta'] or 0}}</span>
      </div>
      <div class="info-table">
            <label>PF Other (1%)</label>
            <span>{{$tdsData['pf_other'] or 0}}</span>
      </div>
      <div class="info-table">
            <label>Annual Employeer PF (12%)</label>
            <span>{{$tdsData['pf_employeer'] or 0}}</span>
      </div>
      <div class="info-table">
            <label>Professional Tax Paid</label>
            <span>{{$tdsData['professional_tax'] or 0}}</span>
      </div>
      <div class="info-table">
            <label>Net taxable Salary</label>
            <span>{{$tdsData['net_taxable_salary'] or 0}}</span>
      </div>
      <div class="info-table">
            <label>IT Saving INVESTMENTS U/S 80C</label>
            <span>{{$tdsData['it_saving']['investment'] or 0}}</span>
      </div>
      <div class="info-table">
            <label>IT Saving Other DEDUCTIONS</label>
            <span>{{$tdsData['it_saving']['deduction'] or 0}}</span>
      </div>
      <div class="info-table">
            <label>Total IT Saving</label>
            <span>{{$tdsData['total_it_saving'] or 0}}</span>
      </div>
      <div class="info-table">
            <label>Taxable Income</label>
            <span>{{$tdsData['taxable_income'] or 0}}</span>
      </div>
      <div class="info-table">
            <label>Income Tax for the financial Year</label>
            <span>{{$tdsData['income_tax'] or 0}}</span>
      </div>
      <div class="info-table">
            <label>Edu Cess</label>
            <span>{{$tdsData['edu_cess'] or 0}}</span>
      </div>
      <div class="info-table">
            <label>Total Income Tax Payable</label>
            <span>{{$tdsData['tax_payable'] or 0}}</span>
      </div>
      <div class="info-table">
            <label>Tax already deducted till last month</label>
            <span>{{$tdsData['gross_annual']['tds_paid_till_date'] or 0}}</span>
      </div>

      <div class="info-table">
            <label>Balance Income Tax Payable</label>
            <span>{{$tdsData['balance_tax_payable'] or 0}}</span>
      </div>
      <div class="info-table">
            <label>Income Tax Payable for the month</label>
            <span>{{$tdsData['income_tax_for_the_month'] or 0}}</span>
      </div>



      <!-- <div class="info-table">
            <label>{{$tdsData['hra_exemption']['exemption_1'] or 0}}</label>
            <label>{{$tdsData['hra_exemption']['exemption_2'] or 0}}</label>
            <label>{{$tdsData['hra_exemption']['exemption_3'] or 0}}</label>
      </div>
      <div class="info-table">
            <label>HRA Exemption (Minimun of above 3):</label>
            <span>{{$tdsData['hra_exemption']['exemption'] or 0}}</span>
      </div>
      <div class="info-table">
            <label>Standard Deduction :</label>
            <span>{{$tdsData['standard_deduction'] or 0}}</span>
      </div>
      <div class="info-table">
            <label>Total Deduction from GEY:</label>
            <span>{{$tdsData['rent_paid'] or 0}}</span>
      </div>
      <div class="info-table">
            <label>Salary Income for the Year from GeekyAnts:</label>
            <span>{{$tdsData['rent_paid'] or 0}}</span>
      </div> -->

</div>
