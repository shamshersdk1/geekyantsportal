<div class="panel panel-default">
    <div class="info-table">
        <label>Feedback Score</label>
        <span>
            {{$feedbackRating}}
            <a href="" class="pull-right">View</a>
        </span>
    </div>
    <div class="info-table">
        <label>Total</label>
        <span>{{$user->activeAppraisal->monthly_var_bonus}}</span>
    </div>
    <div class="info-table">
        <label>Calculated</label>
        <span>
            {{$feedbackRating}} 
            <span class="status  pull-right">
                <!-- <i class="fa fa-check text-success"></i>  -->
                <i class="fa fa-exclamation text-warning"></i>
            </span>
        </span>
    </div>
</div>