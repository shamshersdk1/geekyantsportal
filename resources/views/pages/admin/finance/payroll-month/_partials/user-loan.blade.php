<div class="panel panel-default">
    @if(count($loans) > 0)
        @foreach($loans as $loan)
        <div class="info-table">
            <label>Loan Amount</label>
            <span>
                <small><i class="fa fa-rupee text-muted"></i></small>
                 {{$loan->amount}}
                 <i class="fa fa-check text-success pull-right"></i>
            </span>
        </div>
        <div class="info-table">
            <label>Remaining</label>
            <span>
                <small><i class="fa fa-rupee text-muted"></i></small>
                 {{$loan->remaining}}
            </span>
        </div>
        <div class="info-table">
            <label>EMI</label>
            <span>
                <small><i class="fa fa-rupee text-muted"></i></small>
                 {{$loan->emi}}
                 <i class="fa fa-exclamation text-warning pull-right"></i>
            </span>
        </div>
        @endforeach
    @else
        <div class="info-table">
            <label>No loan found for the month</label>
        </div>
    @endif
</div>
