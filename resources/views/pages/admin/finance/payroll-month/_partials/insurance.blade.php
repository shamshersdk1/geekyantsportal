@if(count($insurances) > 0)
    @foreach($insurances as $insurance)
        <div class="panel panel-default m-b-5">
            <div class="info-table">
                <label>Insurance Type</label>
                <span>{{$insurance->policy->name}}</span>
            </div>
            <div class="info-table">
                <label>Insurance Coverage</label>
                <span>{{$insurance->policy->coverage_amount}}</span>
            </div>
            <div class="info-table">
                <label>Insurance Installment</label>
                <span>{{$insurance->policy->premium_amount}}</span>
            </div>
        </div>
    @endforeach
@endif