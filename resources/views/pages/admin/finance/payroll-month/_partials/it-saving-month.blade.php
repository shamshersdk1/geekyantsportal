<div class="panel panel-default">
    @if($itSaving)
    <div class="info-table">
       <label>Rent Monthly : </label>
       <span><small>{{$itSaving->rent_monthly}}</small></span>
    </div>
    <div class="info-table">
       <label>Rent Monthly : </label>
       <span><small>{{$itSaving->rent_monthly}}</small></span>
    </div>
    <div class="info-table">
       <label>Rent Yearly : </label>
       <span><small>{{$itSaving->rent_yearly}}</small></span>
    </div>
    <div class="info-table">
       <label>pf : </label>
       <span><small>{{$itSaving->pf}}</small></span>
    </div>
    <div class="info-table">
       <label>Employees Contribution under New Pension Scheme 80CCD(1) : </label>
       <span><small>{{$itSaving->pension_scheme_1}}</small></span>
    </div>
    <div class="info-table">
       <label>Employees Contribution under New Pension Scheme 80CCD(1B) (Max Rs.50000.00 In Addition to u/s 80C) : </label>
       <span><small>{{$itSaving->pension_scheme_1b}}</small></span>
    </div>
    <div class="info-table">
       <label>Public Provident Fund (PPF) : </label>
       <span><small>{{$itSaving->ppf}}</small></span>
    </div>
    <div class="info-table">
       <label>Contribution to Certain Pension Funds : </label>
       <span><small>{{$itSaving->central_pension_fund}}</small></span>
    </div>
    <div class="info-table">
       <label>Housing Loan Repayment Principal (payable in F.Y. 2018-19) : </label>
       <span><small>{{$itSaving->housing_loan_repayment}}</small></span>
    </div>
    <div class="info-table">
       <label>Insurance Premium (LIC) : </label>
       <span><small>{{$itSaving->lic}}</small></span>
    </div>
    <div class="info-table">
       <label>Allowable Term Deposit / Fixed Deposit with Schedule Bank for 5 years & above : </label>
       <span><small>{{$itSaving->term_deposit}}</small></span>
    </div>
    <div class="info-table">
       <label>National Saving Scheme / Certificate : </label>
       <span><small>{{$itSaving->national_saving_scheme}}</small></span>
    </div>
    <div class="info-table">
       <label>Tax Saving Mutual Funds : </label>
       <span><small>{{$itSaving->tax_saving}}</small></span>
    </div>
    <div class="info-table">
       <label>Children Education Expenses / Tuition Fees : </label>
       <span><small>{{$itSaving->children_expense}}</small></span>
    </div>
    <div class="info-table">
       <label>Others, (Please specify if any) : </label>
       <span><small>{{$itSaving->other_investment}}</small></span>
    </div>
    <div class="info-table">
       <label>80D - Medical Insurance Premium (Maximum Rs. 25,000; Rs. 50,000 for senior citizens) : </label>
       <span><small>{{$itSaving->medical_insurance_premium}}</small></span>
    </div>
    <div class="info-table">
       <label>80DDB - Expenditure on Medical Treatment for specified disease : </label>
       <span><small>{{$itSaving->medical_treatment_expense}}</small></span>
    </div>
    <div class="info-table">
       <label>80E - Repayment of Interest against Educational Loan : </label>
       <span><small>{{$itSaving->educational_loan}}</small></span>
    </div>
    <div class="info-table">
       <label>80G - Donations (only for Prime Minister’s National Relief Fund, the Chief Minister’s Relief Fund or the Lieutenant Governor’s Relief Fund) : </label>
       <span><small>{{$itSaving->donation}}</small></span>
    </div>
    <div class="info-table">
       <label>80GG - Rent Paid but not in Receipt of HRA(Declaration in Form No. 10 BA required : </label>
       <span><small>{{$itSaving->rent_without_receipt}}</small></span>
    </div>
    <div class="info-table">
       <label>80U-Permanent Physical Disability (Normal Rs. 75000/- and Severe Rs.1,25,000/-) : </label>
       <span><small>{{$itSaving->physical_disablity}}</small></span>
    </div>
    <div class="info-table">
       <label>Others - (Please specify if any) : </label>
       <span><small>{{$itSaving->other_deduction}}</small></span>
    </div>
    <div class="info-table">
       <label>Salary Paid : </label>
       <span><small>{{$itSaving->salary_paid}}</small></span>
    </div>
    <div class="info-table">
       <label>TDS : </label>
       <span><small>{{$itSaving->tds}}</small></span>
    </div>
    <div class="info-table">
       <label>Form 16 / Form 12 B from previous employer : </label>
       <span><small>{{$itSaving->previous_form_16_12b}}</small></span>
    </div>
    @endif
</div>
