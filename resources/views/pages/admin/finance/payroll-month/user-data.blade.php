@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">List of Payroll</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li>List of Payrolls</li>
                </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
            <a href="/admin/payroll-month/{{$salaryData->id}}/update-status?status=hold" class="btn btn-warning btn-sm">On Hold</a>
                <a href="/admin/payroll-month/{{$salaryData->id}}/update-status?status=approve" class="btn btn-success btn-sm">Approve</a>
            </div>
        </div>
    </div>
     <div class="sclearfix">
       <div class="row row-sm">
          <input type="hidden" name="_token" value="9e0EVWte5bQFFg5xYDPqa5cDIxqynpmswqRRgJUv">
          <div class="col-sm-4">
            <h4 class="admin-section-heading">Basic Info</h4>
            <div class="panel panel-default">
              @if($user->activeAppraisal)
                  <div class="info-table">
                     <label>Name: </label> <span> {{$user->name}}</span>
                  </div>
                  <div class="info-table">
                     <label>Employee Code: </label>
                     <span>{{$user->employee_id}}</span>
                  </div>
                  <div class="info-table">
                     <label>Annual Gross Salary: </label>
                     <span>{{$user->activeAppraisal->annual_gross_salary}}</span>
                  </div>
                  <div class="info-table">
                     <label>Payroll Group : </label>
                     <span>{{$user->activeAppraisal->group->name}}</span>
                  </div>
                  <div class="info-table">
                     <label>Monthly Variable Bonus: </label>
                     <span>{{$user->activeAppraisal->monthly_var_bonus}}</span>
                  </div>
                  <div class="info-table">
                     <label>YearEnd/Annual Bonus: </label>
                     <span>{{$user->activeAppraisal->annual_bonus}}</span>
                  </div>
              @endif
            </div>

            <h4 class="admin-section-heading">Bonus</h4>
            @include('pages.admin.finance.payroll-month._partials.user-bonus')

            <h4 class="admin-section-heading">Variable Pay </h4>
            @include('pages.admin.finance.payroll-month._partials.variable')


            <h4 class="admin-section-heading">Insurance</h4>
            @include('pages.admin.finance.payroll-month._partials.insurance')

            <h4 class="admin-section-heading m-t-20">Loan</h4>
            @include('pages.admin.finance.payroll-month._partials.user-loan')

            <h4 class="admin-section-heading">Leave</h4>
            @include('pages.admin.finance.payroll-month._partials.leave')

            <h4 class="admin-section-heading">Leave Encashment / Deduction</h4>
            @include('pages.admin.finance.payroll-month._partials.leave-info')

          </div>
          <div class="col-sm-4">
            <h4 class="admin-section-heading">Payroll Group</h4>
             <div class="panel panel-default">
                @if(!empty($salaryData) && $salaryData->salaryKeys)
                    @foreach($salaryData->salaryKeys as $salaryKey)

                        @if($salaryKey->groupKey && $salaryKey->groupKey->key)
                            <div class="info-table">
                               <label>{{$salaryKey->groupKey->key->value}}: </label>
                               <span>{{$salaryKey->amount}}
                                @if($salaryKey->groupKey->name == 'basic')
                                  ({{$salaryKey->groupKey->calculation_value}}   {{$salaryKey->groupKey->calculation_type}}  Annual Gross Salary)
                                @elseif($salaryKey->groupKey->calculated_upon != '-1' && $salaryKey->groupKey->calc_upon)
                                  ({{$salaryKey->groupKey->calculation_value}}   {{$salaryKey->groupKey->calculation_type}} {{$salaryKey->groupKey->calc_upon->csvValue->value}}  )
                                @endif
                              </span>
                            </div>
                        @endif
                    @endforeach
                @endif
                <div class="info-table">
                    <label>VPF</label>
                    <span>{{$monthObj->vpfDeduction ? $monthObj->vpfDeduction->amount : 0}}</span>
                </div>
                <div class="info-table">
                    <label> Special Allowance Internal</label>
                    <span>{{$salaryData->getSpecialAllowanceInternal()}}</span>
                </div>
                <div class="info-table">
                    <label>Special Allowance</label>
                    <span>{{$salaryData->getSpecialAllowance()}}</span>
                </div>

                <div class="info-table">
                    <label>Total Monthly Deductions Internal</label>
                    <span>{{$salaryData->getTotalDeductionInternal()}}</span>
                </div>
                <div class="info-table">
                    <label>  Total Monthly Deductions</label>
                    <span>{{$salaryData->getTotalDeduction()}}</span>
                </div>
             </div>

            <h4 class="admin-section-heading">TDS Breakdown</h4>
            @include('pages.admin.finance.payroll-month._partials.it-tds-month')

          </div>
          <div class="col-sm-4">
            <h4 class="admin-section-heading">IT Saving</h4>
            @include('pages.admin.finance.payroll-month._partials.it-saving-month')
          </div>
       </div>
       <div class="panel panel-default">
          <div class="panel-body">
              <div class="row row-sm">
                  <div class="col-sm-3">
                      <label>Total Earning</label>
                      <p class="no-margin text-info"><strong>{{$salaryData->getMonthlyGrossEarning()}}/-</strong></p>
                  </div>
                  <div class="col-sm-3">
                      <label>Total Deduction</label>
                      <p class="no-margin text-info"><strong>{{$salaryData->getTotalDeduction()}}/-</strong></p>
                  </div>
                  <div class="col-sm-3">
                      <label>Net Earning</label>
                      <p class="no-margin text-info"><strong>{{$salaryData->getNetSalaryForTheMonth()}}/-</strong></p>
                  </div>

                  <div class="col-sm-3 text-right m-t-10">
                        <a href="{{Request::url()}}/tds-store/hold" class="btn btn-warning btn-sm">On Hold</a>
                        <a href="{{Request::url()}}/tds-store/approve" class="btn btn-success btn-sm">Approve</a>
                  </div>
              </div>
          </div>
      </div>
    </div>
</div>

@endsection


@section('js')
    <script>
        $(function(){
            $(".delete-form").submit(function(event){
                return confirm('Are you sure?');
            });
        });
    </script>

@endsection
