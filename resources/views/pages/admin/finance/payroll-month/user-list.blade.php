@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">List of Payroll Month</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="/admin/payroll-month">List of Payroll Months</a></li>
                    <li>{{$month->formatMonth()}}</li>
                </ol>
            </div>
        </div>
    </div>
   <div class="user-list-view">
       <div class="panel panel-default">

          <table class="table table-striped">
             <tbody>
                <tr>
                   <th>#</th>
                   <th>User</th>
                   <th>Days Worked</th>
                   <th>Annual Gross</th>
                   <th>Monthly Variable Bonus</th>
                   <th>Payroll Rule</th>
                   <th>Status</th>
                   <th>Created At</th>
                   <th class="text-right">Actions</th>
                </tr>
                @if(!empty($users))
                    @foreach($users as $key => $user)
                    <tr>
                        <td class="td-text">{{++$key}}</td>
                        <td class="td-text">
                        {{$user->user->employee_id}} | {{$user->user->name}}
                        </td>
                        <td class="td-text">
                        {{$user ->days_worked}}
                        </td>
                        <td class="td-text">{{$user->appraisal->annual_gross_salary or ''}}</td>
                        <td class="td-text">{{$user->appraisal->monthly_var_bonus or ''}}</td>
                        <td class="td-text">
                          @if($user->appraisal)
                            <a href="/admin/payroll-rule-month/{{$month->id}}/{{$user->appraisal->group->id}}" target="_blank">{{$user->appraisal->group->name}}</a>
                          @endif
                        </td>
                        <td>
                            @if($user->status == 'pending')
                                <span class="label label-primary custom-label">{{$user->status}}</span>
                            @elseif($user->status == 'completed')
                                <span class="label label-success custom-label">{{$user->status}}</span>
                            @endif
                        </td>
                        <td class="td-text">
                          {{datetime_in_view($user->created_at)}}
                        </td>
                        <td class="td-text text-right">
                            <a class="btn btn-warning btn-sm" href="/api/v1/payroll-month-data/1">Re-Calculate</a>
                            <a class="btn btn-primary btn-sm" href="/admin/payroll-month/{{$month->id}}/{{$user->id}}">View</a>
                            <a class="btn btn-danger btn-sm" href="/admin/payroll-month/{{$month->id}}/{{$user->id}}/transaction">Transaction</a>
                        </td>
                    </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="6">No records found</td>
                    </tr>
                @endif
             </tbody>
          </table>
       </div>
    </div>
</div>

@endsection


@section('js')
    <script>
        $(function(){
            $(".delete-form").submit(function(event){
                return confirm('Are you sure?');
            });
        });
    </script>

@endsection
