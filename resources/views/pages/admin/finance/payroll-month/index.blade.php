@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">List of Payroll Month</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li>List of Payroll Month</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                      @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif
        </div>
    </div>
    <div class="user-list-view">
        <div class="panel panel-default">
            <table class="table table-striped">
                <th width="5%">#</th>
                <th width="10%">Month</th>
                <th width="10%">Financial Year</th>
                <th width="10%">Payroll Rule</th>
                <th width="10%">Payroll Rule data</th>
                <th width="10%">Bonus Request</th>
                <th width="10%">Loan Request</th>
                <th width="10%">Feedback Review</th>
                <th width="10%">LOP Request</th>
                <th> Status</th>
                <th width="10%"class="text-right">Actions</th>
                @if(!empty($months))
                    @foreach($months as $month)
                        <tr>
                            <td>{{$month->id}}</td>
                            <td>{{$month->formatMonth()}}</td>
                            <td>{{$month->financialYear->year}}</td>
                            <td>
                                @if($month->payrollSetting? ($month->payrollSetting->value=='locked' ? 0 : 1) : 1)
                                    <span class="label label-danger custom-label">Pending</span>
                                @else
                                    <span class="label label-primary custom-label">Completed</span>
                                @endif
                            </td>
                            <td>
                                
                                @if($month->payrollRuleDataSetting? ($month->payrollRuleDataSetting->value=='locked' ? 0 : 1) : 1)
                                    <span class="label label-danger custom-label">Pending</span>
                                @else
                                    <span class="label label-primary custom-label">Completed</span>
                                @endif
                            </td>
                            <td>
                                @if($month->bonusSetting? ($month->bonusSetting->value=='locked' ? 0 : 1) : 1)
                                    <span class="label label-danger custom-label">Pending</span>
                                @else
                                    <span class="label label-primary custom-label">Completed</span>
                                @endif
                            </td>
                            <td>
                                @if($month->loanSetting? ($month->loanSetting->value=='locked' ? 0 : 1) : 1)
                                    <span class="label label-danger custom-label">Pending</span>
                                @else
                                    <span class="label label-primary custom-label">Completed</span>
                                @endif
                            </td>
                            <td>
                                @if($month->feedbackReviewSetting? ($month->feedbackReviewSetting->value=='locked' ? 0 : 1) : 1)
                                    <span class="label label-danger custom-label">Pending</span>
                                @else
                                    <span class="label label-primary custom-label">Completed</span>
                                @endif
                            </td>
                            <td>
                                @if($month->leaveSetting? ($month->leaveSetting->value=='locked' ? 0 : 1) : 1)
                                    <span class="label label-danger custom-label">Pending</span>
                                @else
                                    <span class="label label-primary custom-label">Completed</span>
                                @endif
                            </td>
                            <td>
                                <span class="label label-primary custom-label">Open</span>
                            </td>
                            <td class="text-right">
                                @if($month->checkAllStatus())
                                    <button class="btn btn-danger btn-sm">Lock</button>
                                    <a href="/admin/payroll-month/{{$month->id}}" class="btn btn-primary crud-btn btn-sm"></i>View</a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="4">No results found.</td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
@endsection
