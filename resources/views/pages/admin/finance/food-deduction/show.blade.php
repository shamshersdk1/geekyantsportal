@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-6">
                <h1 class="admin-page-title">Food Deduction </h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li><a href="{{ url('admin/food-deduction') }}">Food Deduction</a></li>
        		  	<li>{{$month->formatMonth()}}</li>
        		</ol>
            </div>
            <div class="col-sm-6">

                @if(!$month->monthlyFoodDeductionSetting || ($month->monthlyFoodDeductionSetting &&  $month->monthlyFoodDeductionSetting->value == 'open'))
                    <div class="pull-right m-t-10">
                        <span>
                            <a href="/admin/food-deduction/{{$month->id}}/regenerate" class="btn btn-warning"><i class="fa fa-gear fa-fw"></i>Regenerate</a>
                        </span>
                    </div>
                    <div class="pull-right m-t-10">
                        <span>
                            <a href="/admin/food-deduction/{{$month->id}}/status-month" class="btn btn-danger"><i class="fa fa-lock fa-fw"></i>Lock</a>
                        </span>
                    </div>
                @elseif(($month->getMonth(date("Y/m/d")) == $month->id ) && $month->monthlyFoodDeductionSetting && $month->monthlyFoodDeductionSetting->value == 'locked')
                    <div class="pull-right m-t-10">
                        <span>
                            <a href="/admin/food-deduction/{{$month->id}}/status-month" class="btn btn-success"><i class="fa fa-unlock fa-fw"></i>UnLock</a>
                        </span>
                    </div>
                @endif
			</div>
            @if($month && $month->monthlyFoodDeductionSetting &&  $month->monthlyFoodDeductionSetting->value == 'locked')
            <div class="pull pull-right">
                <span class="label label-primary">Locked at {{ $month->monthlyFoodDeductionSetting ? datetime_in_view($month->monthlyFoodDeductionSetting->created_at) : ''}}</span>
            </div>
        @endif
		</div>
    </div>
    
	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
    </div>
        <form method="post" action="{{$month->id}}">
            {{ csrf_field() }}
    {{ method_field('PATCH') }}
        @if(!$month->monthlyFoodDeductionSetting ||  $month->monthlyFoodDeductionSetting->value == 'open')
                <div class="row">
                    <div class="pull-right m-t-1 ">
                        <button type="submit" class="btn btn-success "><i class="fa fa-plus fa-fw"></i>Save</button>
                    </div>
                </div>
        @endif
        <div class="user-list-view">
            <div class="panel panel-default">
                <table class="table table-striped" id="deduction-table">
                    <thead>
                        <th class="text-center">#</th>
                        <th class="text-left">Employee Id</th>
                        <th class="text-left">Name</th>
                        <th class="text-left">Actual Amount(Monthly)</th>
                        <th class="text-left">Deduct Amount(Monthly)</th>
                        <th class="text-center">Action</th>
                    </thead>
                    @if(count($foodDeductionObjs) > 0)
                        @foreach($foodDeductionObjs as $foodDeductionObj)
                            <tr>
                                <td class="text-center"></td>
                                <td class="text-left">{{ $foodDeductionObj->user->employee_id}}</td>
                                <td class="text-left">{{ $foodDeductionObj->user->name}}</td>
                                <td class="text-left">{{ $foodDeductionObj->actual_amount}}</td>
                                <td class="text-left">
                                    @if($month->monthlyFoodDeductionSetting && $month->monthlyFoodDeductionSetting->value == 'locked')
                                        {{$foodDeductionObj->amount}}
                                    @else
                                        <input name="new_amount[{{$foodDeductionObj->id}}]" type="number" value="{{$foodDeductionObj->amount}}" max="0"/>
                                    @endif
                                </td>
                                <td class="pull-right">
                                    @if(!$month->monthlyFoodDeductionSetting || ($month->monthlyFoodDeductionSetting &&  $month->monthlyFoodDeductionSetting->value  == 'open'))
                                        <a href="/admin/food-deduction/{{$foodDeductionObj->id}}/delete" class="btn btn-danger btn-sm crude-btn" onClick="return confirm('Delete Food Deduction?')"><i class="fa fa-trash fa-fw"></i>Delete</a>    
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="6" class="text-center">
                                No Records found
                            </td>
                        </tr>

                    @endif
                </table>
            </div>
        </div>
    </form>
</div>
<script>
    $(document).ready(function() {
        var t = $('#deduction-table').DataTable( {
            pageLength:500, 
            "columnDefs": [ {
                "searchable": false,
                "orderable": false,
                "targets": 0
            } ],
            "order": [[ 1, 'asc' ]],
            scrollY:        true,
            scrollX:        true,
            paging:         false,
            fixedHeader: true
        } );
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    });
</script>

@endsection
