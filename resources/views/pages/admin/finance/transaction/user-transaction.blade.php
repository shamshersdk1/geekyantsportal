@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">List of Transaction</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="/admin/transaction">List Transaction</a></li>
                </ol>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                      @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif
        </div>
    </div>
    <div class="user-list-view">
        <div class="panel panel-default">
            <table class="table table-striped">
                <th width="20%">Reference</th>
                <th width="10%">Title</th>
                <th width="20%">Amount</th>
                @if(!empty($transactions))
                    @foreach($transactions as $transaction)
                        <tr>
                            <td>{{$transaction->reference->value}}</td>
                            <td>{{$transaction->title}}</td>
                            <td>@if($transaction->type =='credit')
                                    <span class="label label-primary custom-label">
                                        {{$transaction->amount}}
                                    </span>
                                @elseif($transaction->type == 'debit')
                                    <span class="label label-danger custom-label">
                                        {{$transaction->amount}}
                                    </span>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="4">No results found.</td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
@endsection
