@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">List of Transaction</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="/admin/transaction">List Transaction</a></li>
                    <li class="active">{{$month->formatMonth()}}</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                      @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif
        </div>
    </div>
    <div class="user-list-view">
        <div class="panel panel-default">
            <table class="table table-striped" id="transaction-table">
                <thead>
                <th class="text-left">#</th>
                <th class="text-left">Month</th>
                <th class="text-left">Employee Id</th>
                <th class="text-left">Employee Name</th>
                <th class="text-left">Amount</th>
                <th class="text-left">Type</th>
                <th class="text-left">Reference Type</th>
                <th class="text-left">Reference Id</th>
                <th class="text-left">Is Company Expense</th>
                </thead>
                @if(!empty($transactions))
                    @foreach($transactions as $index=>$transaction)
                        <tr>
                            <td></td>
                            <td>{{$transaction->month->formatMonth()}}</td>
                            <td>{{$transaction->user->employee_id}}</td>
                            <td>{{$transaction->user->name}}</td>
                            <td>{{$transaction->amount}}</td>
                            <td>{{$transaction->type}}</td>
                            <td>{{$transaction->reference_type}}</td>
                            <td>{{$transaction->reference_id}}</td>
                            <td>{{$transaction->is_company_expense == 1 ? "Yes" : "No"}}</td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="10">No results found.</td>
                    </tr>
                @endif
            </table>
        </div>
    </div>

<script>
$(document).ready(function() {
    var t = $('#transaction-table').DataTable( {
        pageLength:500, 
        "columnDefs": [ {
            "searchable": true,
            "orderable": true,
            "targets": 0
        } ],
        "order": [[ 1, 'desc' ]],
        fixedHeader: true
    } );
    t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
});
</script>

@endsection
