@extends('layouts.admin-dashboard')
@section('main-content')

<section class="new-project-section">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">Add Transaction</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
			  			<li><a href="{{ url('admin/transaction-summary-company') }}">Transaction Summary</a></li>
			  			<li class="active">Add</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>

	    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif

		<form class="form-horizontal" method="post" action="/admin/transaction-summary-company/store" enctype="multipart/form-data">
			<div class="panel panel-default">
				<div class="panel-body">
				    <div class="row">
				    	<div class="col-md-12">
						  	<div class="form-group">
						    	<label for="user_id" class="col-sm-2 control-label">Employee Name</label>
						    	<div class="col-sm-5">
                                        @if($users && isset($users))
										<select id="selectid2" name="user_id"  style="width=35%;" placeholder= "Select user" required>
											<option value=""></option>
											@foreach($users as $x)
									        	<option value="{{$x->id}}" >{{$x->name}}</option>
										    @endforeach
										</select>
									@endif
                                </div>
						  	</div>
						  	<div class="form-group">
                                <label for="month_id" class="col-sm-2 control-label">Month - Year</label>
                                <div class="col-sm-5">
                                        @if(isset($monthList))
										<select id="selectid3" name="month_id"  style="width=35%;" placeholder= "Select month and year" required>
											<option value=""></option>
											@foreach($monthList as $x)
									        	<option value="{{$x->id}}" >{{$x->formatMonth()}}</option>
										    @endforeach
										</select>
									@endif
                                </div>
                            </div>
						  	<div class="form-group">
						    	<label for="amount" class="col-sm-2 control-label">Amount</label>
						    	<div class="col-sm-5">
						    		<input type="number" id="amount" class="form-control" placeholder="0.00" name="amount" required/>
						    	</div>
						  	</div>
							<div class="form-group">
                                <label for="reference_type" class="col-sm-2 control-label">Reference Type</label>
                                <div class="col-sm-5">
										<select id="selectid4" name="reference_type"  style="width=35%;" placeholder= "Select reference type" required>
											<option value=""></option>
												<option value="App\Models\Appraisal\AppraisalComponentType" >Pf Employee</option>
												<option value="App\Models\Appraisal\AppraisalComponentType" >PF other</option>
												<option value="App\Models\Appraisal\AppraisalComponentType" >ESI Employee</option>
										</select>
                                </div>
                            </div>
						  	
                            <div class="form-group">
						    	<label for="reference_id" class="col-sm-2 control-label">Reference Id</label>
						    	<div class="col-sm-5">
						      		<input type="number" class="form-control" placeholder="0" name="reference_id"/>
						    	</div>
						  	</div>
					  	</div>
					</div>
				</div>
			</div>
  	  		<div class="text-center">
		  		<button type="reset" class="btn btn-default">Clear</button>
		  		<button type="submit" class="btn btn-success">Save</button>
		  	</div>
		</form>
	</div>
</section>
	<script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datetimepicker({
				format: 'YYYY-MM-DD'
			});
            $('#datetimepicker2').datetimepicker({
				format: 'YYYY-MM-DD'
            });
        });
	</script> 
@endsection