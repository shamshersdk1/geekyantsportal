@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-6">
                <h1 class="admin-page-title">Transaction Summary Company </h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li>Transaction Summary Company</a></li>
        		</ol>
            </div>
		</div>
    </div>
    <div class="row">
            <div class="col-md-2">
                {{Form::open(['url' => '/admin/transaction-summary-company/create', 'method' => 'get'])}}
                    {{Form::button('<i class="fa fa-plus fa-fw"></i>  Add Transaction', array('type' => 'submit', 'class' => 'btn btn-success'));}}
                {{Form::close()}}
            </div>
            <div class="col-md-2 pull-right">
                    <div class="form-group m-b-10">
                        <div class="bg-white" style="width:100%">
                            @if(isset($monthArray))
                                <select id="selectid2" name="month"  placeholder= "{{$monthObj ? $monthObj->formatMonth() : "All"}}">
                                    <option value=""></option>
                                    <option value="all" >All</option>
                                    @foreach($monthArray as $x)
                                        <option value="{{$x->id}}" >{{$x->formatMonth()}}</option>
                                    @endforeach
                                </select>
                            @endif
                        </div>
                    </div>                
            </div> 
        </div>
	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		            @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
    </div>
    <div class="user-list-view">
        <div class="panel panel-default">
            <table class="table table-striped" id="summary-table">
                <thead>
                    <th class="text-center" width="5%">#</th>
                    <th class="" width="7%">Employee Id</th>
                    <th class="" width="7%">Name</th>
                    <th class="" width="7%">Amount Payable</th>
                </thead>
                @if(count($transactions) > 0)
                    @foreach($transactions as $index=>$transaction)
                        
                        <tr>
                            <td class="text-center"></td>
                             <td class="text-left">{{$users->where('id',$index)->first()->employee_id}}</td>
                            <td class="text-left">{{$users->where('id',$index)->first()->name}}</td>
                            <td class="">{{$transaction}}</td>
                        </tr>
                     
                    @endforeach
                @else
                    <tr>
                        <td colspan="6" class="text-center">
                            No Records found
                        </td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        var t = $('#summary-table').DataTable( {
            pageLength:500, 
            fixedHeader: {
                header: true
            }
        } );
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    });
</script>
<script>
	$('#selectid2').change(function(){
		var optionSelected = $("option:selected", this);
        optionValue = this.value;
        console.log(optionValue);
        if (optionValue) { 
            window.location = "/admin/transaction-summary-company/"+optionValue; 
        }
    });
</script>

@endsection
