@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-6">
                <h1 class="admin-page-title">Fixed Bonuses</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li>Fixed Bonuses</li>
        		</ol>
            </div>
			
		</div>
	</div>
	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
    </div>   
    <div class="user-list-view">
        <div class="panel panel-default">
            <table class="table table-striped">
                <tr>
                    <th class="text-center">#</th>
                    <th class="text-center">Month</th>
                    <th class="text-center">Status</th>
                   
                    <th class="pull-right">Action</th>
                </tr>
                @if( empty($months) || count($months) == 0 )
                    <tr>
                        <td class="text-center" colspan="4">No Records found</td>
                        </tr>
                @endif
                @if(isset($months) > 0)
                    @foreach($months as $index=>$month)
                        <tr>
                            <td class="text-center">{{ $index+1}}</td>
                            <td class="text-center"> 
                               {{$month->formatMonth()}}
                            </td>
                            <td class="text-center">
                                @if($month->fixedBonusSetting  &&  $month->fixedBonusSetting->value == 'locked') 
                                    <span class="label label-danger custom-label">Locked</span>
                                    <br/><small>at {{datetime_in_view($month->fixedBonusSetting->created_at)}}</small>
                                @else
                                    <span class="label label-primary custom-label">OPEN</span>
                                @endif
                            </td>
                            <td class="pull-right">
                                <span>
                                    @if($month->fixedBonusSetting  &&  $month->fixedBonusSetting->value == 'locked')
                                    @else
                                    <form name="deleteForm" method="get" action="/admin/fixed-bonus/{{$month->id}}/status-month"  style="display: inline-block;">
                                        <button type="submit" class="btn btn-danger btn-sm crude-btn"><i class="fa fa-lock fa-fw"></i>Lock</button> 
                                    </form>
                                    @endif
                                </span>
                                <span>
                                <form name="showForm" method="get" action="/admin/fixed-bonus/{{$month->id}}"  style="display: inline-block;">
                                    <button type="submit" class="btn btn-primary btn-sm crude-btn"><i class="fa fa-eye fa-fw"></i>View</button> 
                                </form>
                                <span>
                                
                            </td>
                        </tr>
                    @endforeach
                @endif  
             </table>
        </div>
    </div>
</div>	
@endsection
