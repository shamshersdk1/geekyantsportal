@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-6">
                <h1 class="admin-page-title">Fixed Bonuses</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li><a href="{{ url('admin/fixed-bonus') }}">Fixed Bonuses</a></li>
        		  	<li>{{$month->formatMonth()}}</li>
        		</ol>
            </div>
            <div class="col-sm-6">

                @if(!$month->fixedBonusSetting || ($month->fixedBonusSetting &&  $month->fixedBonusSetting->value == 'open'))
                    <div class="pull-right m-t-10">
                        <span>
                            <a href="/admin/fixed-bonus/{{$month->id}}/regenerate" class="btn btn-warning"><i class="fa fa-gear fa-fw"></i>Regenerate</a>
                        </span>
                    </div>
                    <div class="pull-right m-t-10">
                        <span>
                            <a href="/admin/fixed-bonus/{{$month->id}}/status-month" class="btn btn-danger"><i class="fa fa-lock fa-fw"></i>Lock</a>
                        </span>
                    </div>
                @elseif(($month->getMonth(date("Y/m/d")) == $month->id ) && $month->fixedBonusSetting && $month->fixedBonusSetting->value == 'locked')
                    <div class="pull-right m-t-10">
                        <span>
                            <a href="/admin/fixed-bonus/{{$month->id}}/status-month" class="btn btn-success"><i class="fa fa-unlock fa-fw"></i>UnLock</a>
                        </span>
                    </div>
                @endif
			</div>
            @if($month && $month->fixedBonusSetting &&  $month->fixedBonusSetting->value == 'locked')
            <div class="pull pull-right">
                <span class="label label-primary">Locked at {{ $month->fixedBonusSetting ? datetime_in_view($month->fixedBonusSetting->created_at) : ''}}</span>
            </div>
        @endif
		</div>
    </div>
    
	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
    </div>

    <form name="editForm" method="post" action="/admin/fixed-bonus/{{$month->id}}">
    {{ csrf_field() }}
    {{ method_field('PATCH') }}
            @if(!$month->fixedBonusSetting ||  $month->fixedBonusSetting->value == 'open')
                <div class="row">
                    <div class="pull-right m-t-1 ">
                        <button type="submit" class="btn btn-success "><i class="fa fa-plus fa-fw"></i>Save</button>
                    </div>
                </div>
        @endif
        <div class="user-list-view">
            <div class="panel panel-default">
                <table class="table table-striped" id="deduction-table">
                    <thead>
                        <tr>
                            <th class="text-center">Employee Id</th>
                            <th class="text-center">Name</th>
                            @foreach($appraisalBonusTypes as $appraisalBonusType)
                            <th class="text-center">{{$appraisalBonusType->description}}</th>
                            @endforeach
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                     
                        @if(count($monthlyDeductions) > 0)
                            @foreach($monthlyDeductions as $monthlyDeduction)  
                                @if((count($monthlyDeduction->monthlyDeductionComponents)>0)?true:false)
                                <tr>
                                    <td class="text-center">{{ $monthlyDeduction->user->employee_id}}</td>
                                    <td class="text-center">{{ $monthlyDeduction->user->name}}</td>
                                    @foreach($appraisalBonusTypes as $appraisalBonusType)
                                    <td class="text-center">
                                        @if($monthlyDeduction->monthlyDeductionComponents->where('key',$appraisalBonusType->code)->first()->value)
                                            @if($month && $month->fixedBonusSetting &&  $month->fixedBonusSetting->value == 'locked')
                                            <label>{{$monthlyDeduction->monthlyDeductionComponents->where('key',$appraisalBonusType->code)->first()->value}}</label>
                                            @else
                                            <input name="new_amount[{{$monthlyDeduction->monthlyDeductionComponents->where('key',$appraisalBonusType->code)->first()->id}}][{{$appraisalBonusType->code}}]" type="number" value="{{$monthlyDeduction->monthlyDeductionComponents->where('key',$appraisalBonusType->code)->first()->value}}"/>
                                            @endif
                                        @endif
                                    </td>
                                    @endforeach
                                    
                                    <td class="pull-right">
                                        @if(!$month->fixedBonusSetting || ($month->fixedBonusSetting &&  $month->fixedBonusSetting->value == 'open'))
                                            <a href="/admin/fixed-bonus/{{$monthlyDeduction->id}}/delete" class="btn btn-danger btn-sm crude-btn" onClick="return confirm('Delete Fixed Bonus?')"><i class="fa fa-trash fa-fw"></i>Delete</a>    
                                        @endif
                                    </td>
                                </tr>
                                @endif
                            @endforeach
                        @else
                            <tr>
                                <td colspan="6" class="text-center">
                                    No Records found
                                </td>
                            </tr>

                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </form>
</div>
<script>
     $(document).ready(function() {
        $tableHeight = $( window ).height();
        var t = $('#deduction-table').DataTable( {
            "bPaginate": false,
            scrollY: $tableHeight - 200,
            scrollX: true,
            scrollCollapse: true,
            paging:         false,
            "bInfo" : false,
            order: [[ 0, 'asc' ]],
            pageLength:500, 
            columnDefs: [ {
                "searchable": true,
                "orderable": true,
                "targets": 0
            } ],
            fixedColumns:   {
            leftColumns: 2
        }
        } );
    });
</script>
@endsection