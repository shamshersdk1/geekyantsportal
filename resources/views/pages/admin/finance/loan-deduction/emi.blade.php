@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-6">
                <h1 class="admin-page-title">Loan Deduction</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li><a href="{{ url('admin/loan-deduction') }}">Loan Deduction</a></li>
        		  	<li>{{$month->formatMonth()}}</li>
        		</ol>
            </div>
            <div class="col-sm-6">

                @if(!$month->loanSetting || ($month->loanSetting &&  $month->loanSetting->value == 'open'))
                    <div class="pull-right m-t-10">
                        <span>
                            <a href="/admin/loan-deduction/{{$month->id}}/regenerate" class="btn btn-warning"><i class="fa fa-gear fa-fw"></i>Regenerate</a>
                        </span>
                    </div>
                    <div class="pull-right m-t-10">
                        <span>
                            <a href="/admin/loan-deduction/{{$month->id}}/status-month" class="btn btn-danger"><i class="fa fa-lock fa-fw"></i>Lock</a>
                        </span>
                    </div>
                @elseif(($month->getMonth(date("Y/m/d")) == $month->id ) && $month->loanSetting && $month->loanSetting->value == 'locked')
                    <div class="pull-right m-t-10">
                        <span>
                            <a href="/admin/loan-deduction/{{$month->id}}/status-month" class="btn btn-success"><i class="fa fa-unlock fa-fw"></i>UnLock</a>
                        </span>
                    </div>
                @endif
			</div>
		</div>
    </div>
    @if($month->loanSetting &&  $month->loanSetting->value == 'locked')
        <div class="pull pull-right">
            <span class="label label-primary">Locked at {{datetime_in_view($month->loanSetting->created_at)}}</span>
        </div>
    @endif
	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
    </div>

    <form name="editForm" method="post" action="/admin/loan-deduction/{{$month->id}}/update">
        @if(!$month->loanSetting ||  $month->loanSetting->value == 'open')
                <div class="row">
                    <div class="pull-right m-t-1 ">
                        <button type="submit" class="btn btn-success "><i class="fa fa-plus fa-fw"></i>Save</button>
                    </div>
                </div>
        @endif
        <div class="user-list-view">
            <div class="panel panel-default">
                <table class="table table-striped" id="deduction-table">
                    <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th class="text-center">Employee Id</th>
                        <th class="text-center">Name</th>
                        <th class="text-center">Actual Amount</th>
                        <th class="text-center">Deduct Amount</th>
                        <th class="text-center">Type</th>
                        <th class="text-center">Status</th>
                        <th class="text-center">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($loanDeductions) > 0)
                        @foreach($loanDeductions as $loanDeduction)
                            <tr>
                                <td class="text-center"></td>
                                <td class="text-center">{{ $loanDeduction->loan->user->employee_id}}</td>
                                <td class="text-center">{{ $loanDeduction->loan->user->name}}</td>
                                <td class="text-center">{{ $loanDeduction->actual_amount}}</td>
                                <td class="text-center">
                                    @if($month->loanSetting && $month->loanSetting->value == 'locked')
                                        {{$loanDeduction->amount}}
                                    @else
                                        <input name="new_amount[{{$loanDeduction->id}}]" type="number" value="{{$loanDeduction->amount}}" max="0"/>
                                    @endif
                                </td>
                                <td>{{($loanDeduction->appraisal_bonus_id === null ) ? "Against Salary" : $loanDeduction->appraisalBonus->appraisalBonusType->description}}</td>
                                <td class="text-center">
                                    @if($loanDeduction  &&  $loanDeduction->status == 'pending')
                                        <span class="label label-info custom-label">Pending</span>
                                        @if($month->loanSetting)
                                            <br/><small>at {{datetime_in_view($month->loanSetting->created_at)}}</small>
                                        @endif
                                    @elseif($loanDeduction  &&  $loanDeduction->status == 'processing')
                                        <span class="label label-warning custom-label">Processing</span>
                                    @elseif($loanDeduction  &&  $loanDeduction->status == 'paid')
                                        <span class="label label-sucess custom-label">Paid</span>
                                    @else
                                        <span class="label label-danger custom-label">rejected</span>
                                    @endif

                                </td>
                                <td class="pull-right">
                                    @if(!$month->loanSetting || $month->loanSetting->value == 'open')
                                     <a href="/admin/loan-deduction/{{$loanDeduction->id}}/delete" class="btn btn-danger btn-sm crude-btn" onClick="return confirm('Delete user loan?')"><i class="fa fa-trash fa-fw"></i>Delete</a>

                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5" class="text-center">
                                No Records found
                            </td>
                        </tr>

                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </form>
</div>
<script>
    $(document).ready(function() {
        var t = $('#deduction-table').DataTable( {
            pageLength:500, 
            scrollY:        true,
            scrollX:        true,
            scrollCollapse: true,
            paging:         false,
            "columnDefs": [ {
                "searchable": false,
                "orderable": false,
                "targets": 0
            } ],
            "order": [[ 1, 'asc' ]],
            fixedHeader: true
        } );
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    });
</script>
@endsection
