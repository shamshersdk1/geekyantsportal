@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">List of Payroll Rule Month</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="/admin/payroll-rule-month">List of Payroll Rule Month</a></li>
                    <li><a href="/admin/payroll-rule-month/{{$month->id}}">{{$month->formatMonth()}}</a></li>
                    <li>{{$groupObj->name}}</li>
                </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                      @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Add New Rule</div>
                <div class="panel-body">
                    <form action = "rules" method="POST" style="padding:10px;" >
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group col-md-6">
                            <label class="col-md-4" style="padding-top:2%" align="left" for="name">Name:</label>
                            <div class="col-md-8" style="padding:0">
                                <select name="name" id="rule-selector" class="form-control rule-name">
                                    <option value="">Select a rule name</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="col-md-4" style="padding-top:2%" align="left" for="name">Calculation Type:</label>
                            <div class="col-md-8" style="padding:0">
                                <input type="text" id="calculation_type" name="calculation_type" class="form-control" required value="{{ old('calculation_type') }}">
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="col-md-4" style="padding-top:2%" align="left" for="event">Type:</label>
                            <div class="col-md-8 col-sm-12 col-xs-12" style="padding:0px">
                                <select class="form-control col-md-12 col-sm-12 col-xs-12" style="border-radius:0px;-webkit-appearance: none" name="type" id="type">
                                    <option value="Credit">Credit</option>
                                    <option value="Debit" selected>Debit</option>
                                </select>
                                <span class="glyphicon glyphicon-triangle-bottom form-control-feedback" style="opacity:0.3;left:88%" ></span>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="col-md-4" style="padding-top:2%" align="left" for="name">Calculation Value:</label>
                            <div class="col-md-8" style="padding:0">
                                <input type="text" id="calculation_value" name="calculation_value" class="form-control" required value="{{ old('calculation_value') }}">
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="col-md-4" style="padding-top:2%" align="left" for="event">Calculated Upon:</label>
                            <div class="col-md-8 col-sm-12 col-xs-12" style="padding:0px">
                                <select class="form-control col-md-12 col-sm-12 col-xs-12" style="border-radius:0px;-webkit-appearance: none" name="calculated_upon" id="calculated_upon">
                                    <option value="-1" selected></option>
                                    <option value="0">Annual Gross Salary</option>
                                    @if(count($groupObj->rules) > 0)
                                        @foreach($groupObj->rules as $rule)
                                            <option value="{{$rule->id}}">@if(!empty($rule->csvValue)){{$rule->csvValue->value}}@else{{$rule->name}}@endif</option>
                                        @endforeach
                                    @endif
                                </select>
                                <span class="glyphicon glyphicon-triangle-bottom form-control-feedback" style="opacity:0.3;left:88%" ></span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button style="display: block; margin: 0 auto;" type="submit" class="btn btn-success">Save</button>
                        </div>
                    </form>
                </div>
                </div>
                <div class="panel panel-default">
                <table class="table table-striped">
                <thead>
                    <th width="10%">#</th>
                    <th width="15%">Rules</th>
                    <th width="10%">Type</th>
                    <th width="15%">Calculation Value</th>
                    <th width="10%">Calculation Type</th>
                    <th width="15%">Calculated Upon</th>
                    <th class="text-right">Actions</th>
                </thead>
                @if(count($groupRules) > 0)
                    @foreach($groupRules as $rule)
                        <tr>
                            <td class="td-text">{{$rule->id}}</td>
                            <td class="td-text"> @if(!empty($rule->csvValue)){{$rule->csvValue->value}} @else {{$rule->name}} @endif</td>
                            <td class="td-text"> {{ucfirst($rule->type)}}</td>
                            <td class="td-text"> {{$rule->calculation_value}}</td>
                            <td class="td-text"> {{$rule->calculation_type}}</td>
                            <td class="td-text">
                                @if($rule->calculated_upon=='-1')
                                @elseif($rule->calculated_upon=='0' )
                                    Annual Gross Salary
                                @elseif(!empty($rule->calc_upon->csvValue))
                                    {{$rule->calc_upon->csvValue->value}}
                                @endif
                            </td>
                            <td class="text-right">
                                <a href="rules/{{$rule->id}}/edit" class="btn btn-warning btn-sm"><i class="fa fa-pencil btn-icon-space" aria-hidden="true"></i>Edit</a>
                                <form action="/admin/payroll-rule-month/3/1/rule/{{$rule->id}}" method="post" style="display:inline-block;" class="deleteform">
                                    <input name="_method" type="hidden" value="DELETE">
                                    <button class="btn btn-danger btn-sm" type="submit"><i class="fa fa-trash btn-icon-space" aria-hidden="true"></i>Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="7">No rules added.</td>
                    </tr>
                @endif
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $('#type option[value="{{old('type')}}"]').attr("selected",true);
        // $( ".rule-name" ).autocomplete({
        //      source: {{$groupRules}}
        // });
        var rulesArray = {{$groupRules}};
        if(rulesArray.length > 0) {
            var option = '';
            $.each(rulesArray, function( key, value) {
                option = option + '<option value="'+value.key+'">'+value.value+'</option>';
            });
            $("#rule-selector").append(option);
        }
    });
</script>
@endsection
