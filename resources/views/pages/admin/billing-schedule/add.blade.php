@extends('layouts.admin-dashboard')
@section('main-content')

<section class="new-project-section">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">Billing Schedule</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
			  			<li><a href="{{ url('admin/project') }}">Projects</a></li>
						<li><a href="/admin/project/{{$id}}/dashboard">{{$id}}</a></li>
						<li class="active">Add bill</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>

	    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif
		<form class="form-horizontal" method="post" action="/admin/project/{{$id}}/billing/store" enctype="multipart/form-data">
			<div class="panel panel-default">
				<div class="panel-body">
				    <div class="row">
				    	<div class="col-md-12">
						   <div class="form-group">
								<label for="" class="col-sm-2 control-label">Project Name</label>
								<div class="input-group col-sm-4">
									<input type="text" class="form-control" value="{{$project->project_name}}" disabled>
								</div>	
							</div>
							 <div class="form-group">
								<label for="" class="col-sm-2 control-label">Title</label>
								<div class="input-group col-sm-4">
									<input type="text" class="form-control" name="title" value="{{ old('title') }}"/>
								</div>	
							</div>
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">Resources</label>
								<div class="input-group col-sm-4">
									<select class="js-example-basic-multiple22 form-control" multiple="multiple" name="resources[]" required>
										@foreach($resource_price as $resource_price)
											<option value="{{$resource_price->id}}" {{ (collect(old('resources'))->contains($resource_price->id)) ? 'selected':'' }}>{{$resource_price->type}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">Type</label>
								<div class="input-group col-sm-2">
									<select class="js-example-basic form-control" onchange="billingCycleOption(this);" name="type" required>
										<option value="" disabled selected>Select</option>
										<option value="fixed" {{ (old("type") == "fixed" ? "selected":"")}}>Fixed</option>
										<option value="recurring" {{ (old("type") == "recurring" ? "selected":"") }}>Recurring</option>
									</select>
								</div>
							</div>
							<div class="form-group" style="display: none;" id="amountOption" >
								<label for="" class="col-sm-2 control-label">Amount</label>
								<div class="input-group col-sm-2">
									<input type="number" id="amount" class="form-control" placeholder="0.00" name="amount" value="{{ old('amount') }}"/>
								</div>
							</div>
							<div class="form-group" style="display: none;" id="cycleOption" >
								<label for="" class="col-sm-2 control-label">Billing Cycle</label>
								<div class="input-group col-sm-2">
									<select class="js-example-basic form-control" onchange="generateOption(this);" name="cycle" id="billSelect" value={{ old('term') }}>
										<option value="" disabled selected>Select</option>
										<option value="weekly" {{ (old("cycle") == "weekly" ? "selected":"")}}>Weekly</option>
										<option value="bi-weekly" {{ (old("cycle") == "bi-weekly" ? "selected":"")}}>Bi-Weekly</option>
										<option value="monthly" {{ (old("cycle") == "monthly" ? "selected":"")}}>Monthly</option>
									</select>
								</div>
							</div>
							<div class="form-group" style="display: none;" id="reminderOption1" >
								<label for="" class="col-sm-2 control-label">Generate Invoice Every</label>
								<div class="input-group col-sm-2">
									<select class="js-example-basic form-control" name="generate_on" id="generateOn" value="{{ old('generate_on') }}">
										<option value="" disabled selected>Select Cycle</option>
										@for($i = 0; $i <= 4; $i++)
										<option value="{{jddayofweek($i)}}_{{jddayofweek($i, 1)}}">{{jddayofweek($i, 1)}}</option>
										@endfor
									</select>
								</div>
							</div>
							<div class="form-group" style="display: none;" id="reminderOption2" >
								<label for="" class="col-sm-2 control-label">Generate Invoice Every</label>
								<div class="input-group col-sm-2">
									<select class="js-example-basic form-control" name="generate_on" id="generateOn" value="{{ old('generate_on') }}">
										<option value="" disabled selected>Select Cycle</option>
										<option value="last_wd"  {{ (old("generate_on") == "last_wd" ? "selected":"")}}>Last working day of month</option>
										<option value="first_wd"  {{ (old("generate_on") == "first_wd" ? "selected":"")}}>First working day of month</option>
									</select>
								</div>
							</div>
                            <!-- ======================== Date Picker ======================= -->
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Start Date</label>
                                <div class='input-group date' id='datetimepicker6'>
                                    <input type='text' class="form-control" name="from_date" value="{{ old('from_date') }}" required/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">End Date</label>
                                <div class='input-group date' id='datetimepicker7'>
                                    <input type='text' class="form-control" id="to_date" name="to_date" value="{{ old('to_date') }}"/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <!-- ======================== Date Picker ======================= -->
							<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Notes</label>
						    	<div class="col-sm-4">
						      		<textarea class="form-control" rows="5" name="notes">{{ old('notes') }}</textarea>
						    	</div>
						  	</div>
					  	</div>
					</div>
				</div>
			</div>
  	  		<div class="text-center">
		  		<button type="submit" class="btn btn-success">Save</button>
		  	</div>
		</form>
	</div>
</section>
<script type="text/javascript">
    $(function () {
        $('#datetimepicker6').datetimepicker({
            format: 'YYYY-MM-DD'
        });
        $('#datetimepicker7').datetimepicker({
            format: 'YYYY-MM-DD'
        });
    });
    function billingCycleOption(option) {
        if (option.value == "recurring") {
            $("#cycleOption").show();
			$("#amountOption").hide();
			$("#amount").val('');
			$("#to_date").prop('required',false);
        } else {
			$("#amountOption").show();
			$("#amountOption").prop('required',false);
			$("#billSelect").val('');
            $("#cycleOption").hide();
			$("#to_date").prop('required',true);
			$("#reminderOption2").hide();
			$("#reminderOption2").val('');
			$("#reminderOption1").hide();
			$("#reminderOption1").val('');
        }
    }
	function generateOption(option){
		 if (option.value == "weekly" || option.value == "bi-weekly") {
            $("#reminderOption1").show();
			$("#reminderOption1").prop('required',true);
			$("#reminderOption2").hide();
			$("#reminderOption2").val('');
        } else {
			$("#reminderOption2").show();
			$("#reminderOption1").val('');
            $("#reminderOption1").hide();
        }
	}
</script>
@endsection
