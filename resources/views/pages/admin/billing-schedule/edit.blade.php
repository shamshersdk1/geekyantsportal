@extends('layouts.admin-dashboard')
@section('main-content')

<section class="new-project-section">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">Bills</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
			  			<li><a href="{{ url('admin/project') }}">Projects</a></li>
						<li><a href="/admin/project/{{$id}}/dashboard">{{$id}}</a></li>
						<li class="active">Update bill</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>

	    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif
		<form class="form-horizontal" method="post" action="/admin/project/{{$id}}/billing/update" enctype="multipart/form-data">
        <input type="hidden" id="billing_schedule_id" name="billing_schedule_id" value={{$billing_schedule->id}}>
			<div class="panel panel-default">
				<div class="panel-body">
				    <div class="row">
				    	<div class="col-md-12">
						<div class="form-group">
								<label for="" class="col-sm-2 control-label">Project Name</label>
								<div class="input-group col-sm-4">
									<input type="text" class="form-control" value="{{$project->project_name}}" disabled>
								</div>	
							</div>
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">Resources</label>
								<div class="input-group col-sm-4">
									@if(isset($resource_price))
                                        @if(isset($items))
											<select class="js-example-basic-multiple22 form-control selected-option" multiple="multiple" name="resources[]" required>
												@foreach($resource_price as $resource_price)
													@if(!empty($items))
														@foreach ($items as $item)
															@if( $resource_price->id == $item)
																<option value="{{ $resource_price->id}}" selected> {{ $resource_price->type}}</option>
															@endif
														@endforeach
													@endif
													<option value=" {{$resource_price->id}}"> {{ $resource_price->type}}</option>
												@endforeach
											</select>
                                        @endif
									@endif
								</div>
							</div>
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">Type</label>
								<div class="input-group col-sm-2">
									<select class="js-example-basic form-control" onchange="billingCycleOption(this);" name="type" id="type" required>
										<option value="" disabled>Select</option>
										<option value="fixed" {{ old('type', $billing_schedule->type) == 'fixed' ? 'selected' : '' }}>Fixed</option>
										<option value="recurring" {{ old('type', $billing_schedule->type) == 'recurring' ? 'selected' : '' }}>Recurring</option>
									</select>
								</div>
							</div>
							<div class="form-group" style="display: none;" id="cycleOption" >
								<label for="" class="col-sm-2 control-label">Billing Cycle</label>
								<div class="input-group col-sm-2">
									<select class="js-example-basic form-control" name="cycle" id="billSelect">
										<option value="" {{ old('cycle', $billing_schedule->term) == '' ? 'selected' : '' }} disabled>Select</option>
										<option value="weekly"{{ old('cycle', $billing_schedule->term) == 'weekly' ? 'selected' : '' }}>Weekly</option>
										<option value="bi-weekly"{{ old('cycle', $billing_schedule->term) == 'bi-weekly' ? 'selected' : '' }}>Bi-Weekly</option>
										<option value="monthly"{{ old('cycle', $billing_schedule->term) == 'monthly' ? 'selected' : '' }}>Monthly</option>
									</select>
								</div>
							</div>
                            <!-- ======================== Date Picker ======================= -->
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Start Date</label>
                                <div class='input-group date' id='datetimepicker6'>
                                    <input type='text' class="form-control" name="from_date"  value={{$billing_schedule->from_date}} required/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">End Date</label>
                                <div class='input-group date' id='datetimepicker7'>
                                    <input type='text' class="form-control" id= "to_date" name="to_date" value={{$billing_schedule->to_date}} required/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <!-- ======================== Date Picker ======================= -->
					  	</div>
					</div>
				</div>
			</div>
  	  		<div class="text-center">
		  		<button type="submit" class="btn btn-success">Save</button>
		  	</div>
		</form>
	</div>
</section>
<script type="text/javascript">
    $(function () {
        $('#datetimepicker6').datetimepicker({
            format: 'YYYY-MM-DD'
        });
        $('#datetimepicker7').datetimepicker({
            format: 'YYYY-MM-DD'
        });
		var optionType = document.getElementById('type');
		if( optionType.value == 'recurring'){
			document.getElementById("cycleOption").style.display = "block";
		}
    });
    function billingCycleOption(option) {
        if (option.value == "recurring") {
            document.getElementById("cycleOption").style.display = "block";
			document.getElementById("to_date").required = false;
        } else {
			document.getElementById("billSelect").value = "";
            document.getElementById("cycleOption").style.display = "none";
			document.getElementById("to_date").required = true;
        }
    }
</script>
@endsection
