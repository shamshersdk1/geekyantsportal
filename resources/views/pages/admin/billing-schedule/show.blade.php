@extends('layouts.admin-dashboard')
@section('main-content')

<section class="new-project-section">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">Invoices</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
			  			<li><a href="{{ url('admin/project') }}">Projects</a></li>
						<li><a href="/admin/project/{{$billingScheduleObj->project->id}}/dashboard">{{$billingScheduleObj->project->id}}</a></li>
						<li>Invoice</li>
						<li class="active">{{$billingScheduleObj->id}}</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>
            <div class="panel panel-default">
             <div class="panel-body">
                    <div class="col-md-12">
                        <label>Project : </label>
                        <span>{{$billingScheduleObj->project->project_name}}</span>
                    </div>
                    @if(!empty($billingScheduleObj->invoice_date))
                     <div class="col-md-12">
                        <label>Date : </label>
                        <span>{{date_in_view($billingScheduleObj->invoice_date)}}</span>
                    </div>
                    @endif
                    <div class="col-md-12">
                        <label>Developer : </label>
                            
                    </div>
                    <div class="col-md-12">
                        <table>
                            @if(!empty($resources) && count($resources)>0)
                                @foreach($resources as $resource)
                                    @if(isset($resource->resource->is_active) && $resource->resource->is_active)
                                        <tr>
                                            <td>
                                                <b>
                                                    @if(!empty($resource->resource->name))
                                                        {{$resource->resource->name}} 
                                                    @endif
                                                    @if(!empty($resource->resource_type->type))
                                                        ({{$resource->resource_type->type}})
                                                    @endif
                                                    @if(!empty($resource->resource_type->price))
                                                       at  ${{$resource->resource_type->price}} 
                                                    @endif
                                              </b>
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            @endif
                        </table>
                    </div>
               </div>
            </div>
	</div>
</section>
<script type="text/javascript">
    $(function () {
        $('#datetimepicker6').datetimepicker({
            format: 'YYYY-MM-DD'
        });
        $('#datetimepicker7').datetimepicker({
            format: 'YYYY-MM-DD'
        });
		var optionType = document.getElementById('type');
		if( optionType.value == 'recurring'){
			document.getElementById("cycleOption").style.display = "block";
		}
    });
    function billingCycleOption(option) {
        if (option.value == "recurring") {
            document.getElementById("cycleOption").style.display = "block";
        } else {
			document.getElementById("billSelect").value = "";
            document.getElementById("cycleOption").style.display = "none";
        }
    }
</script>
@endsection
