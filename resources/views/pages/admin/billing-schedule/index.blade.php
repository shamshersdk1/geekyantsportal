@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Billing Schedule</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="/admin/project">Projects</a></li>
                    <li><a href="/admin/project/{{$id}}/dashboard">{{$id}}</a></li>
                     <li><a href="">Bills</a></li>
                </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <a href="/admin/project/{{$id}}/billing/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Add</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                    @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif   
            <h4 style="margin-left:5px;">List of Bills ({{$project->project_name}})</h4>
            <div class="panel panel-default">
                <table class="table table-striped">
                    <thead >
                        <th>#</th>
                        <th>From Date</th>
                        <th>To Date</th>
                        <th>Status</th>
                        <th>Type</th>
                        <th>Term</th>
                        <th>Created At</th>
                        <th>Actions</th>
                    </thead>
                    @if(count($billing_schedule) > 0)
                            @foreach($billing_schedule as $index => $billing)
                                <tr>
                                    <td class="td-text">{{$index+1}}</td>
                                    <td class="td-text"> {{date_in_view($billing->from_date)}}</td>
                                    <td class="td-text">{{date_in_view($billing->to_date)}}</td>
                                    <td class="td-text">
                                        @if($billing->status == 'pending')
                                            <span class="label label-primary custom-label">Pending</span>
                                        @elseif($billing->status == 'completed')
                                            <span class="label label-success custom-label">Completed</span>
                                        @elseif($billing->status == 'rejected')
                                             <span class="label label-danger custom-label">Rejected</span>
                                        @endif
                                    </td>
                                    <td class="td-text">{{ucfirst($billing->type)}}</td>
                                    <td class="td-text">{{ucfirst($billing->term)}}</td>
                                    <td class="td-text">{{date_in_view($billing->created_at)}}</td>
                                    <td class="td-text">
                                        <form action="/admin/project/{{$id}}/billing/{{$billing->id}}" method="get" style="display:inline-block;">
                                            <button class="btn btn-info  btn-sm" type="submit"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View</button>
                                        </form>
                                        <form action="/admin/project/{{$id}}/billing/edit" method="post" style="display:inline-block;" >
                                            <input name="billing_id" type="hidden" value={{$billing->id}}>
                                            <button class="btn btn-primary  btn-sm" type="submit"><i class="fa fa-edit btn-icon-space" aria-hidden="true"></i>Edit</button>
                                        </form>
                                        <form action="/admin/project/{{$id}}/billing/remove" method="post" style="display:inline-block;" >
                                            <input name="billing_id" type="hidden" value={{$billing->id}}>
                                            <button class="btn btn-danger  btn-sm" type="submit"><i class="fa fa-trash btn-icon-space" aria-hidden="true"></i>Delete</button>
                                        </form>
                                        <form action="/admin/project/{{$id}}/billing/{{$billing->id}}/notify" method="get" style="display:inline-block;" >
                                            <button class="btn btn-warning  btn-sm" type="submit"><i class="fa fa-bell btn-icon-space" aria-hidden="true"></i>Notify</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="8">No results found.</td>
                            </tr>
                        @endif
                </table>
            </div>
        </div>		
	</div>
</div>
@endsection
