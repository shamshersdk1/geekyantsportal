@extends('layouts.admin-dashboard')
@section('main-content')
	<section class="timelog-dashboard">
		<div class="container-fluid">
			<div class="breadcrumb-wrap">
		    	<div class="row">
		            <div class="col-sm-12">
		                <h1 class="admin-page-title">Timelog Dashboard</h1>
		                <ol class="breadcrumb">
		        		  	<li><a href="/admin">Admin</a></li>
				  			<li class="active">Timelog Dashboard</li>
		        		</ol>
		            </div>
		        </div>
		    </div>
		    <div class="row">
		    	<div class="col-md-12">
						@if(!empty($errors->all()))
							<div class="alert alert-danger">
							@foreach ($errors->all() as $error)
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							<span>{{ $error }}</span><br/>
							@endforeach
							</div>
							@endif
							@if (session('message'))
							<div class="alert alert-success">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							<span>{{ session('message') }}</span><br/>
							</div>
						@endif
					<form action="/admin/timelog/dashboard/dashboard-action" method="post" onsubmit="return changeUrl();">
						<div class="panel panel-default">
							@if(!isset($timelogHistory))
								<h4><center>No Record found</center></h4>
							@endif	
					    	<table class="display table table-striped">
				    			<thead>
									<tr>
								        <th width="10%"><input type="checkbox" id="chk_all"/></th>
								        <th>Description</th>
								        <th width="20%">Project Name</th>
								        <th width="15%" style="text-align:center;">Date</th>
								        <th width="20%" style="text-align:center;">Time Spend In Min</th>
								    </tr>
								</thead>


								@if(count($timelogHistory) > 0)
									@foreach($timelogHistory as $history)
							      	<tr>
								        <td><input type="checkbox" name="index[]" value={{$history->id}}></td>
								        <td>
								        	<textarea class="form-control edit-form-control" rows="1" name=message[{{$history->id}}]>{{$history->message}}</textarea>
								        </td>
								        @if(isset($history->project->project_name))
								        	<td><textarea class="form-control edit-form-control" rows="1">{{$history->project->project_name}}</textarea></td>
								        @else
								        	<td><textarea class="form-control edit-form-control" rows="1"></textarea></td>
								        @endif
								        <td><textarea class="form-control edit-form-control" rows="1" style="text-align:center;">{{$history->current_date}}</textarea></td>
								        <td><textarea class="form-control edit-form-control" rows="1" style="text-align:center;" name= minute[{{$history->id}}]>{{$history->minutes}}</textarea></td>
							      	</tr>
							      	@endforeach
							    @else
							    	<tr>
							    		<td colspan="5">No Result found</td>
							    	</tr>
							    @endif
		        			</table>
				  		</div>
				  		<div class="row">
		    				<div class="col-md-12">
		    					<button class="btn btn-danger btn-sm" type="submit" name="action" value="delete">Delete</button>
		    					<button class="btn btn-success crud-btn btn-sm pull-right" name="action" value="save" style="margin-left:5px;">Save</button>
		    					<button class="btn btn-info crud-btn btn-sm pull-right record-btn" name="action" value="record">Record</button>
		    				</div>
		    			</div>
				  	</form>
				</div>
			</div>
		</div>
	</section>

@endsection
