@extends('layouts.admin-dashboard')
@section('main-content')
	<section class="timelog-dashboard">
		<div class="container-fluid">
			<div class="breadcrumb-wrap">
		    	<div class="row">
		            <div class="col-sm-12">
		                <h1 class="admin-page-title">Timelog History</h1>
		                <ol class="breadcrumb">
		        		  	<li><a href="/admin">Admin</a></li>
				  			<li class="active">Timelog History</li>
		        		</ol>
		            </div>
		        </div>
		    </div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<form action="/admin/timelog/history" method="post">
							<div class="panel-heading bg-light">
								<div class="row">
					    	    	<div class="col-sm-4">
					    	    		<label>Project</label> <br />
					    				<select id="selectid22" name="project" class="form-control">
					    					<option value="">Select</option>
						    				@if(isset($projects))
						    					@foreach($projects as $project)
								    				<option value="{{$project->id}}">{{$project->project_name}}</option>
								    			@endforeach
								    		@endif
					    				</select>
					    	    	</div>
					    	    	<div class="col-sm-4">
					    	    		<label>Month</label><br />
					    				<select id="selectid2" name="month" class="form-control" >
						    				<option value="">Select</option>
						    				<option value="01">January</option>
						    				<option value="02">February</option>
						    				<option value="03">March</option>
						    				<option value="04">April</option>
						    				<option value="05">May</option>
						    				<option value="06">june</option>
						    				<option value="07">july</option>
						    				<option value="08">august</option>
						    				<option value="09">september</option>
						    				<option value="10">October</option>
						    				<option value="11">November</option>
						    				<option value="12">December</option>
					    				</select>
					    	    	</div>
					    	    	<div class="col-sm-4">
					    	    		<label>&nbsp;</label><br />	
					    				<button class="btn btn-default btn-sm">Search</button>
					    	    	</div>
					    	 	</div>
					    	 </div>
						</form>
						<table class="table table-striped">
			    			<thead>
			    			
								<tr>
							        <th width="">Description</th>
							        <th width="20%">Project name</th>
							        <th width="15%">Time Spent</th>
							        <th width="15%">Date</th>
							    </tr>
							    
							</thead>
							@if(count($timelogHistory) > 0) 
									@foreach($timelogHistory as $history)
							      	<tr>
								        <td>{{$history->message}} </td>
								        @if(isset($history->project->project_name))				
								       		<td>{{$history->project->project_name}}</td>
								       	@else
								       		<td></td>
								       	@endif
								        <td>{{$history->minutes}}</td>
								        <td>{{$history->current_date}}</td>
							      	</tr>
							      	@endforeach
					      	@else
					    		<tr>
					    			<td colspan="4" class="text-center">No Result found.</td>
					    		</tr>
					    	@endif
	        			</table>
					</div>
				</div>
				
				{{ $timelogHistory->links() }}
			</div>
		</div>
	</section>




		    	    	<!--<div class="col-md-2">
	    	    			<div class="form-group">
	    	    				<label for=""></label>
                                <div class='input-group date' id='datetimepicker1'>
                                    <input type='text' class="form-control" placeholder="Chosse Start Date" id="txtStartDate" name="start_date" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
		    	    	</div>
		    	    	<div class="col-md-2">
		    	    		<div class="form-group">
                            	<label for=""></label>
                                <div class='input-group date' id='datetimepicker1'>
                                    <input type='text' class="form-control" placeholder="Chosse End Date" id="txtEndDate" name="end_date" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
		    	    	</div>-->
		    	    	<!-- <div class="col-md-2">
		    	    		<label for=""></label>
		    	    		<button type="submit" class="btn btn-info">Find</button>
		    	    	</div> -->
				
    			<!-- <div class="row pull-right">
    				<div class="col-md-12">
    					<nav aria-label="Page navigation">
						  <ul class="pagination">
						    <li>
						      <a href="#" aria-label="Previous">
						        <span aria-hidden="true">&laquo;</span>
						      </a>
						    </li>
						    <li><a href="#">1</a></li>
						    <li><a href="#">2</a></li>
						    <li><a href="#">3</a></li>
						    <li><a href="#">4</a></li>
						    <li><a href="#">5</a></li>
						    <li>
						      <a href="#" aria-label="Next">
						        <span aria-hidden="true">&raquo;</span>
						      </a>
						    </li>
						  </ul>
						</nav>
    				</div>
    			</div> -->
		  	<!-- </div> -->
		<!-- </div> -->
<!-- </div> -->
		

@endsection