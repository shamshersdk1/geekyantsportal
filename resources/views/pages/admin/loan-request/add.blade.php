@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="flex-class justify-content-between align-items-center">
            <div>
                <h1 class="admin-page-title">Request Loan</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/loan-requests') }}">My Loans</a></li>
                    <li class="active">Add</li>
                </ol>
            </div>
            <div>
                <button type="button" onclick="window.history.back();" class="btn btn-default no-margin"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
    	@if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span><br/>
                  @endforeach
            </div>
        @endif
        @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
        @endif   
		<form name = "myForm" id="loan-request-form" method="post" action="/admin/loan-requests" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token()}}">
            <div class="panel panel-default" style="padding:20px; padding-bottom:5px;">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="row">    
                            <label for="name" class="col-sm-4 text-right">Enter Amount:</label>
                            <div class="col-sm-8  form-group">
                                <input type="text" id="amount" name="amount" class="form-control" placeholder="0.00" required value="{{ old('amount') }}">
                                <div class="request-loan-note">
                                    Maximum loan amount cannot exceed more than 1 Lakh
                                </div>
                            </div>
                        </div>
                        <div class="row">    
                            <label for="name" class="col-sm-4 text-right">Select Loan Type:</label>
                            <div class="col-sm-8  form-group">
                                <select name="annual_bonus_type_id" id="loan-type-selector" class="form-control" required>
                                    @foreach($loan_types as $loan_type)
                                        <option value="{{$loan_type->id}}">{{$loan_type->description}}</option>
                                    @endforeach
                                    <option value="">Against Salary</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">    
                            <label for="name" class="col-sm-4 text-right">Reason:</label>
                            <div class="col-sm-8 form-group">
                                <textarea name="description" class="form-control" placeholder="Enter the description" rows="5" style="width:100%;">@if(!empty(old('description'))){{old('description')}}@endif</textarea>
                            </div>
                        </div>
                    </div>        
                    <div class="col-sm-6">    
                        <div class="row">    
                           <label for="name" class="col-sm-4 text-right">Enter EMI:</label>
                            <div class="col-sm-8 form-group">
                                <input type="text" id="emi" name="emi" class="form-control" placeholder="0.00" value="{{ old('emi') }}">
                                <div class="request-loan-note">
                                    Please Leave Empty If Not Applicable.
                                </div>
                                <span id="emi-error" style="color:red;display:none"><i class="fa fa-exclamation-circle"></i> Can't be greater than loan amount</span>
                            </div>    
                        </div>
                        <div class="row">
                            <label for="emi_start_date" class="col-sm-4 text-right">Emi Start Date</label>
                            <div class="col-sm-8  form-group">
                                <div class="input-group date" id="datetimepicker2" style="width:100%">
                                    <input type="text" name="emi_start_date" class="form-control"/>
                                    <span class="input-group-addon" style="position:relative">
                                    <span class="fa fa-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>

                        
                    </div>
                </div>
            </div>
            <div class="text-center">
                <button type="reset" class="btn btn-default no-margin"><i class="fa fa-rotate-left fa-fw"></i> Clear</button> 
                <button type="submit" class="btn btn-success no-margin" id="submit-btn"><i class="fa fa-plus fa-fw"></i>Save</button>
            </div>
		</form>
        </div>
	</div>
</div>
<script>
    $(function(){
        $('#datetimepicker2').datetimepicker({
            format:'YYYY-MM-DD',
            useCurrent: false,
        });
        var old_type = "{{old('type')}}";
        $("#loan-type-selector option[value='"+old_type+"']").prop('selected', true);
        $("#loan-request-form").submit(function(e){
            var emi = parseFloat($("#emi").val());
            var amount = parseFloat($("#amount").val());
            // if(amount > 100000) {
            //     $("#amount").css("border", "2px solid red");
            //     return false;
            // }
            if(emi > amount) {
                $("#emi").css("border", "2px solid red");
                $("#emi-error").show();
                return false;
            }
            return true;
        });
        $("#emi").focus(function(){
            $("#emi").css("border", "1px solid #D5DDF1");
            $("#emi-error").hide();
        });
    });
</script>
@endsection