@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Request Loan Success</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/loan-requests') }}">My Loans</a></li>
                    <li class="active">Add</li>
                    <li class="active">Success</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
    	@if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span><br/>
                  @endforeach
            </div>
        @endif
        @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
        @endif   
		<form name = "myForm" id="loan-request-form" method="get" action="/admin/loan-requests" enctype="multipart/form-data">
            <div class="panel panel-default" style="padding:20px; padding-bottom:5px;">
                <div class="row">
                    
                    <div class="col-sm-6">
                        
                        <div class="row">    
                            <label for="name" class="col-sm-4 text-left">Amount :</label>
                            <label for="name" class="col-sm-2 text-left">{{$loan->amount}}</label>
                        </div>
                        
                        <div class="row">    
                            <label for="name" class="col-sm-4 text-left">Emi :</label>
                            <label for="name" class="col-sm-2 text-left">{{$loan->emi}}</label>
                        </div>
                        <div class="row">    
                            <label for="name" class="col-sm-4 text-left">Type :</label>
                            <label for="name" class="col-sm-6 text-left">{{($loan->appraisal_bonus_id === null ) ? "Against Salary" : $loan->appraisalBonus->appraisalBonusType->description}}</label>
                        </div>
                        <div class="row">    
                            <label for="name" class="col-sm-4 text-left">Description :</label>
                            <label for="name" class="col-sm-6 text-left">{{ nl2br(e($loan->description)) }}</pre></label>
                        </div>
                        <div class="row">    
                            <label for="name" class="col-sm-4 text-left">Created At :</label>
                            <label for="name" class="col-sm-6 text-left">{{datetime_in_view($loan->created_at)}}</label>
                        </div>
                        
                        
                    </div>       
                    
                    
                </div>
                <div class="row">   
                    <label for="name" ><span class="label label-info custom-label">Pending</span>(Waiting for Reporting Manager)</label> 
                    <label for="name" class="col-sm-2 text-left">Status :</label> 
                </div> 
            
                </div>
            <div class="text-center">
                <button type="submit" class="btn btn-default" id="submit-btn"><i class="fa fa-caret-left fa-fw"></i>Back</button>
            </div>
            </div>
		</form>
        </div>
	</div>
</div>

@endsection