@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Request Loan</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    @if($loan_request->status == "pending" && $loan_request->user_id == \Auth::id())
                        <li><a href="{{ url('admin/loan-requests') }}">My Loans</a></li>
                    @else
                        <li><a href="{{ url('admin/new-loan-requests') }}">New Loan Requests</a></li>
                    @endif
                    <li class="active">Edit</li>
                </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
    	@if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span><br/>
                  @endforeach
            </div>
        @endif
        @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
        @endif   
		<form name = "myForm" method="post" action="/admin/loan-requests/{{$loan_request->id}}" enctype="multipart/form-data" >
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            {{method_field("PUT")}}
            <div class="panel panel-default" style="padding-top: 15px;">
                <div class="row">
                    <div class="col-sm-12">
                        <label for="name" class="col-sm-2">Enter Amount:</label>
                        <div class="col-sm-4  form-group">
                            <input type="text" name="amount" class="form-control" placeholder="0.00" required value="@if(!empty(old('amount'))){{old('amount')}}@elseif($loan_request->approved_amount>0){{$loan_request->approved_amount}}@else{{$loan_request->amount}}@endif">
                        </div>
                        <label for="name" class="col-sm-2">Enter EMI:</label>
                        <div class="col-sm-4 form-group">
                            <input type="text" name="emi" class="form-control" placeholder="0.00" value="@if(!empty(old('emi'))){{old('emi')}}@else{{$loan_request->emi}}@endif">
                        </div>
                        
                        <label for="name" class="col-sm-2">Select Loan Type:</label>
                        <div class="col-sm-4  form-group">
                            <select name="annual_bonus_type_id" id="loan-type-selector" class="form-control" required>                
                                @foreach($loan_types as $loan_type)
                                    @if($loan_type->id == $loan_request->appraisalBonus->appraisalBonusType->id)
                                        <option value="{{$loan_request->appraisalBonus->appraisalBonusType->id}}" selected>{{$loan_request->appraisalBonus->appraisalBonusType->description}}</option>
                                    @else
                                       <option value="{{$loan_type->id}}" >{{$loan_type->description}}</option>
                                    @endif
                                @endforeach
                                @if($loan_request->appraisalBonus->id === null)
                                    <option value="" selected>Against Salary</option>
                                @else
                                    <option value="">Against Salary</option>
                                @endif
                            </select>
                        </div>

                        <label for="emi_start_date" class="col-sm-2 text-center">Emi Start Date</label>
                        <div class="col-sm-4  form-group">
                                <div class="input-group date" id="datetimepicker2" style="width:100%">
                                    <input type="text" name="emi_start_date" class="form-control" value="@if(!empty(old('emi_start_date'))){{old('emi_start_date')}}@else{{$loan_request->emi_start_date}}@endif"/>
                                    <span class="input-group-addon" style="position:relative">
                                    <span class="fa fa-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        <label for="name" class="col-sm-2">Reason:</label>
                        <div class="col-sm-4 form-group">
                            <textarea name="description" class="form-control" placeholder="Enter the description" rows="5">@if(!empty(old('description'))){{old('description')}}@else{{$loan_request->description}}@endif</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <button type="reset" class="btn btn-default"><i class="fa fa-rotate-left fa-fw"></i> Clear</button> 
                <button type="submit" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Update</button>
            </div>
		</form>
        </div>
	</div>
</div>
<script>
    $(function(){
        var old_type = "@if(!empty(old('type'))){{old('type')}}@else{{$loan_request->type}}@endif";
        $("#loan-type-selector option[value='"+old_type+"']").prop('selected', true);
    });
</script>
@endsection