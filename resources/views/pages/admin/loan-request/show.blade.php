@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Request Loan</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="{{str_replace('/'.$loan_request->id, '', url()->current())}}">Loan Request</a></li>
                    <li class="active">View</li>
                </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
    	@if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span><br/>
                  @endforeach
            </div>
        @endif
        @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
        @endif   
        <div class="row">
	        <div class="col-md-12" style="padding-left:30px; padding-top:20px;">
                <div class="panel panel-default">   
                    <div class="panel-body ">   
                        <div class="row">
                            <div class="col-sm-6">
                                <h4>Loan Details</h4>
                            </div>
                            <div class="col-sm-3 text-center" style="float:right">
                                <label>Status :</label>
                                @if($loan_request->status == "pending")
                                    <span class="label label-info custom-label">Pending</span><br/>
                                    <small> (Waiting for Reporting Manager)</small>
                                @elseif($loan_request->status == "submitted")
                                    <span class="label label-primary custom-label">Submitted</span><br/>
                                    <small> (Waiting for Human Resources)</small>
                                @elseif($loan_request->status == "review")
                                    <span class="label label-primary custom-label">Under Review</span><br/>
                                    <small> (Waiting for Management)</small>
                                @elseif($loan_request->status == "reconcile")
                                    <span class="label label-primary custom-label">Reconcile</span><br/>
                                    <small> (Waiting for Human Resources)</small>
                                @elseif($loan_request->status == "approved")
                                    <span class="label label-success custom-label">Approved</span><br/>
                                    <small> (Waiting for Finance Team)</small>
                                @elseif($loan_request->status == "rejected")
                                    <span class="label label-danger custom-label">Rejected</span><br/>
                                @elseif($loan_request->status == "disbursed")
                                    <span class="label label-success custom-label">Disbursed</span>
                                @else
                                    <span class="label label-info custom-label">No Status</span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>User : </label>
                                        <span>{{ $loan_request->user->name }}</span>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Type : </label>
                                        <span>
                                            {{($loan_request->appraisal_bonus_id === null ) ? "Against Salary" : $loan_request->appraisalBonus->appraisalBonusType->description}}
                                        </span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>Amount : </label>
                                        <span> &#x20B9; {{ $loan_request->amount }} </span>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>EMI : </label>
                                        <span> &#x20B9; {{ $loan_request->emi }}</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>Approved Amount : </label>
                                        <span> &#x20B9; {{ $loan_request->approved_amount }} </span>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Application Date :</label>
                                        <span style="white-space:pre">{{ datetime_in_view($loan_request->created_at) }}</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>Reason :</label>
                                        <span style="white-space:pre">{{ $loan_request->description }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 text-center" style="float:right;margin-top:2em">
                                @if($file)
                                    <a href="{{$loan_request->file->url()}}" target="_blank" class="btn btn-success btn-sm">View Agreement</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8">
                        <div class="panel panel-default">
                            <div class="panel-body ">
                                @if(!empty($comments))
                                    @foreach($comments as $item)
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label>{{$item['user']}} :</label>
                                            <span>
                                                {{ $item['comment'] }}
                                            </span>
                                            <small style="float:right;padding-top:15px">{{$item['time']}}</small>
                                        </div>
                                    </div>
                                    @endforeach
                                @else
                                    <div class="row">
                                        <div class="col-sm-12">
                                            No comments
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        @if ( !empty($loan_request->loan)  )
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h4>Loan Payment Detail</h4>    
                                <div class="info-table reduce-label-width">
                                    <label class="col-sm-7"> Payment Detail : </label>
                                    <span class="col-sm-5 text-left"> {{ $loan_request->loan->payment_details }} </span>
                                </div>
                                <div class="info-table reduce-label-width">
                                    <label class="col-sm-7"> Mode of Payment : </label>
                                    <span class="col-sm-5 text-left"> {{ $loan_request->loan->payment_mode }}</span>
                                </div>
                                <div class="info-table reduce-label-width">
                                    <label class="col-sm-7"> Transaction ID : </label>
                                    <span class="col-sm-5 text-left"> {{ $loan_request->loan->transaction_id }}</span>
                                </div>
                                <div class="info-table reduce-label-width">
                                    <label class="col-sm-7"> Date of payment : </label>
                                    <span class="col-sm-5 text-left"> {{date_in_view($loan_request->loan->payment_date)}} </span>
                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h4>Loan History</h4>
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Amount</th>
                                            <th>Remaining</th>
                                            <th>Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(!empty($previous_loans))
                                            @foreach($previous_loans as $index => $loan)
                                                <tr>
                                                    <td>{{$index+1}}</td>
                                                    <td>{{$loan->amount}}</td>
                                                    <td>{{$loan->remaining}}</td>
                                                    <td>{{date_in_view($loan->application_date)}}</td>
                                                </tr>      
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="4"> No Existing loans</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                                @if(!empty($previous_loans))
                                    {{$previous_loans->render()}}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>	
    </div>
</div>	
<script>
		$(function(){
		$(".reject-btn").click(function(event){
                return confirm('Are you sure?');
            });
        });
</script>
@endsection