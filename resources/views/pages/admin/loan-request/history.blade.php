@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Loans</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li>
                        <a href="{{ url('admin/loan-applications') }}">Loan Requests</a>
                    </li>
                </ol>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                    @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif   
            <div class="panel panel-default">
                <table class="table table-striped" id="myTable">
                    <thead >
	            		<th>#</th>
                        <th>Name</th>
                        <th>Approved Amount</th>
                        <th>Amount</th>
                        <th>EMI</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th class="text-right">Action</th>
                    </thead>
                        @if(count($loan_requests) > 0)
                            @foreach($loan_requests as $index => $loan)
                                <tr>
                                    <td class="td-text">{{$loan->id}}</td>
                                    <td class="td-text">{{$loan->user->name}}</td>
                                    <td class="td-text"> &#x20B9; {{$loan->approved_amount}}</td>
                                    <td class="td-text"> &#x20B9; {{$loan->amount}}</td>
                                    <td class="td-text"> &#x20B9; {{$loan->emi}}</td>
                                    <td>{{($loan->appraisal_bonus_id === null ) ? "Against Salary" : $loan->appraisalBonus->appraisalBonusType->description}}
                                    </td>
                                    <td >
                                        @if($loan->status == "pending")
                                            <span class="label label-info custom-label">Pending</span><br/>
                                            <small> (Waiting for RM)</small>
                                        @elseif($loan->status == "submitted")
                                            <span class="label label-primary custom-label">Submitted</span><br/>
                                            <small> (Waiting for HR)</small>
                                        @elseif($loan->status == "review")
                                            <span class="label label-primary custom-label">Under Review</span><br/>
                                            <small> (Waiting for Management)</small>
                                        @elseif($loan->status == "reconcile")
                                            <span class="label label-primary custom-label">Reconcile</span><br/>
                                            <small> (Waiting for HR)</small>
                                        @elseif($loan->status == "approved")
                                            <span class="label label-success custom-label">Approved</span><br/>
                                            <small> (Waiting for A/c team)</small>
                                        @elseif($loan->status == "rejected")
                                            <span class="label label-danger custom-label">Rejected</span>
                                        @elseif($loan->status == "disbursed")
                                            <span class="label label-success custom-label">Disbursed</span><br/>
                                        @else
                                            <span class="label label-info custom-label">No Status</span>
                                        @endif
                                    </td>
                                    <!-- <td class="td-text">{{$loan->description}}</td> -->
                                    <td class="text-right">
                                        <a class="btn btn-info btn-sm" href="/admin/loan-applications/{{$loan->id}}"><i class="fa fa-eye"></i> View</a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="8">No pending loan requests</td>
                            </tr>
                        @endif
                </table>
            </div>
        </div>		
	</div>
</div>
<script>
		$(function(){
		$(".reject-btn").click(function(event){
                return confirm('Are you sure?');
            });
        });

        $(document).ready( function () {
            $('#myTable').DataTable({
                "pageLength": 500,
                "order": [[ 0, "desc" ]]
            });
            
        } );
        
</script>
@endsection