@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="flex-class justify-content-between align-items-center">
            <div>
                <h1 class="admin-page-title">Loans</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/loan-requests') }}">My Loans</a></li>
                </ol>
            </div>
            <div>
                <a href="/admin/loan-requests/create" class="btn btn-success no-margin"><i class="fa fa-plus fa-fw"></i> Request Loan</a>
            </div>
        </div>
    </div>
    @if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                    @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif   
    <div class="row">
        <div class="col-md-6">
            
            <h4 style="margin-left:5px;">List of Loan Request</h4>
            <div class="panel panel-default">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead >
                            <th class="td-text">#</th>
                            <th class="td-text">Date</th>
                            <th class="td-text">Amount</th>
                            <th class="td-text">Approved Amount</th>
                            <th class="td-text">Type</th>
                            <th class="td-text">Status</th>
                            <th class="td-text">Action</th>
                        </thead>
                            @if(count($loan_requests) > 0)
                                @foreach($loan_requests as $index => $loan)
                                    <tr>
                                        <td class="td-text">{{$index+1}}</td>
                                        <td class="td-text"> {{datemonth_in_view($loan->application_date)}}</td>
                                        <td class="td-text"> &#x20B9; {{$loan->amount}}</td>
                                        <td class="td-text"> &#x20B9; {{$loan->approved_amount}}</td>
                                        <td >{{($loan->appraisal_bonus_id == NULL) ? "Against Salary" : $loan->appraisalBonus->appraisalBonusType->description}}
                                        </td>
                                        <td >
                                            @if($loan->status == "pending")
                                                <span class="label label-info custom-label">Pending</span><br/>
                                                <small> (Waiting for RM)</small>
                                            @elseif($loan->status == "submitted")
                                                <span class="label label-primary custom-label">Submitted</span><br/>
                                                <small> (Waiting for HR)</small>
                                            @elseif($loan->status == "review")
                                                <span class="label label-primary custom-label">Under Review</span><br/>
                                                <small> (Waiting for Management)</small>
                                            @elseif($loan->status == "reconcile")
                                                <span class="label label-primary custom-label">Reconcile</span><br/>
                                                <small> (Waiting for HR)</small>
                                            @elseif($loan->status == "approved")
                                                <span class="label label-success custom-label">Approved</span><br/>
                                                <small> (Waiting for A/c team)</small>
                                            @elseif($loan->status == "rejected")
                                                <span class="label label-danger custom-label">Rejected</span>
                                            @elseif($loan->status == "disbursed")
                                                <span class="label label-success custom-label">Disbursed</span><br/>
                                            @else
                                                <span class="label label-info custom-label">No Status</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if($loan->status == "pending")
                                                <a class="btn btn-success" href="/admin/loan-requests/{{$loan->id}}/edit">Edit</a>
                                            @endif
                                            <a class="btn btn-primary" href="/admin/loan-applications/{{$loan->id}}">View</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="7">No results found.</td>
                                </tr>
                            @endif
                    </table>
                </div>
            </div>
        </div>		
	
        <div class="col-md-6">
            @if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                    @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif   
            <h4 style="margin-left:5px;">My Loan Applications</h4>
            <div class="panel panel-default">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead >
                            <th class="td-text">#</th>
                            <th class="td-text">Date</th>
                            <th class="td-text">Amount</th>
                            <th class="td-text">Remaining Amount</th>
                            <th class="td-text">Type</th>
                            <th class="td-text">Action</th>
                        </thead>
                            @if(!empty($previous_loans) && count($previous_loans) > 0)
                                @foreach($previous_loans as $index => $loan)
                                    <tr>
                                        <td class="td-text">{{$index+1}}</td>
                                        <td class="td-text"> {{datemonth_in_view($loan->application_date)}}</td>
                                        <td class="td-text"> &#x20B9; {{$loan->amount}}</td>
                                        <td class="td-text"> &#x20B9; {{$loan->outstanding_amount}}</td>
                                        <td >
                                            {{($loan->appraisal_bonus_id == NULL) ? "Against Salary" : $loan->appraisalBonus->appraisalBonusType->description}}
                                        </td>
                                
                                        <td><a class="btn btn-primary" href="/user/loans/{{$loan->id}}">View</a></td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="7">No results found.</td>
                                </tr>
                            @endif
                    </table>
                </div>
            </div>
        </div>		
    </div>
</div>
@endsection