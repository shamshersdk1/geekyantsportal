   <div class="breadcrumb-wrap">
      <div class="row">
         <div class="col-sm-8">
            <h1 class="admin-page-title">RECONCILE</h1>
            <ol class="breadcrumb">
               <li><a >Create Payslip CSV</a></li>
               <li><a >Loan Amount</a></li>
            </ol>
         </div>
      </div>
   </div>
   <ul class="progress-indicator col-sm-8">
        <li class="completed"> <span class="bubble"></span><a ng-click="gotoStep(1)">Initial</a> </li>

        <li class="completed"> <span class="bubble"></span> <a ng-click="gotoStep(2)">Loan amount</a> </li>

        <li class="completed"> <span class="bubble"></span>Reconcile</li>

        <li> <span class="bubble"></span> <a ng-click="gotoStep(4)">Accepted</a> </li>
      <li> <span class="bubble"></span>  </li>
   </ul>
   <div class="col-sm-4 pull-right">
      <a class="btn btn-primary pull-right" ng-click="nextStep()" style="margin-left:10px;">Next</a>
      <a class="btn btn-primary pull-right" ng-click="prevStep()">Previous</a>
   </div>
   <div class="row dashboard-container" style="clear:both">
   
   <div class="col-md-12 p-l-0">
      <div class="panel panel-default">
         <div class="panel-heading redBox">
           List of employee who appraisal cycle changed
         </div>
         <table class="table table-striped">
            <!-- <thead>
               <tr>
                   <th colspan="3"></th>
               </tr>
               </thead> -->
            <tbody>
                <tr>
                    <th>Name</th>
                    <th> Update On</th>
                    <th> Previous Month Net Pay</th>
                    <th> Current Month Net Pay</th>
                </tr>
                @if(!empty($appraisals))
                    @foreach($appraisals as $data)
                        <tr>
                            <td>{{$data->name}}</td>
                            <td>{{$data->updated_on}}</td>
                            <td>{{$data->previous}}</td>
                            <td>{{$data->current}}</td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="4">No Appraisal changes</td>
                    </tr>
                @endif
            </tbody>
         </table>
      </div>
   </div>
 
<div>