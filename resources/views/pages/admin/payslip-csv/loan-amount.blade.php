    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Loan Amount</h1>
                <ol class="breadcrumb">
                      <li><a href="../create-payslip-csv">Create Payslip CSV</a></li>
                      <li><a>Loan Amount</a></li>
        		</ol>
            </div>
           
        </div>
    </div>
    
    <!-- <ul class="progress-indicator col-sm-8">

        <li class="completed"> <span class="bubble"></span><a ng-click="gotoStep(1)">Initial</a> </li>

        <li class="completed"> <span class="bubble"></span>Loan amount</li>

        <li> <span class="bubble"></span> <a ng-click="gotoStep(3)">Reconcile</a> </li>

        <li> <span class="bubble"></span> <a ng-click="gotoStep(4)">Accepted</a> </li>
         <li> <span class="bubble"></span>  </li>
    </ul> -->
    <ul class="steps-wrap col-md-10">
      <li class="completed"> 1 <span>Intial</</span></li>
      <li > 2  <a ng-click="gotoStep(2)">Loan amount</a> </li>
      <li> 3<a ng-click="gotoStep(3)">Reconcile</a></li>
      <li > 4 <a ng-click="gotoStep(4)">Accepted</a></li>
    </ul>
    <div class="col-sm-4 pull-right">
        <a class="btn btn-primary pull-right" ng-click="nextStep()" style="margin-left:10px;">Next</a>
        <a class="btn btn-primary pull-right" ng-click="prevStep()">Previous</a>
    </div>
   <div class="user-list-view row">
   <div class=" col-md-12">

      <table class="table table-striped panel panel-default">
         <tbody>
            <tr>
                <th>Employee Code</th>
                <th>Employee Name</th>
                <th>Amount</th>
                <th>Date</th>
                <th>Remaining</th>
                <th>EMI</th>
                <th>Deduction Amount</th>
                <th class="text-right">Action</th>
            </tr>
            @if(!empty($loans))
                @foreach($loans as $data)
                    <tr>
                        <td>{{$data->employee_id}}</td>
                        <td>{{$data->name}}</td>
                        <td>{{$data->loan_amount}}</td>
                        <td>{{$data->date}}</td>
                        <td>{{$data->remaining}}</td>
                        <td>{{$data->emi}}</td>
                        <td><input type="text" value="{{$data->amount}}"/></td>
                        <td class="text-right">
                            <a href="/admin/payslip/8" class="btn btn-info crud-btn btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>Confirm</a>	
                            <a href="/admin/payslip/8" class="btn btn-danger btn-sm"><i class="fa fa-trash btn-icon-space" aria-hidden="true"></i>Reject</a>	
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="8">No loans for this month</td>
                </tr>
            @endif  
         </tbody>
      </table>
   </div>
</div>
<div class="user-list-view">
    <div class="panel panel-default " style="overflow-x: scroll">
        <table id="example" class="stripe  order-column table table table-striped" cellspacing="0" width="100%" height="800">
        <thead>
            <th>Employee Code</th>
            <th>Employee Name</th>
            <th>Amount</th>
            <th>Date</th>
            <th>Remaining</th>
            <th>EMI</th>
            <th>Deduction Amount</th>
            <th class="text-right">Action</th>
        </thead>
        <tr ng-repeat="loan in loans">
            <td ng-repeat="key in loan">%%key%%</td>
            <td class="text-right">
                <a class="btn btn-info crud-btn btn-sm" ng-click="confirmLoan(loan.id)"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>Confirm</a>	
                <a class="btn btn-danger btn-sm" ng-click="rejectLoan(loan.id)"><i class="fa fa-trash btn-icon-space" aria-hidden="true"></i>Reject</a>	
            </td>
        </tr>
        </table>
   </div>
   
</div>