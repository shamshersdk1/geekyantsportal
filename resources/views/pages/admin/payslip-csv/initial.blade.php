    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Create Payroll CSV</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li><a>Create Payroll CSV</a></li>
        		</ol>
            </div>
            <div class="col-sm-4 text-right">
                <a class="btn btn-success" href="/admin/create-payslip-csv/{{$id}}/download"><i class="fa fa-download">  </i>  Download</a>
            </div>
        </div>
    </div>
    <!-- <ul class="progress-indicator col-sm-8">

        <li class="completed"> <span class="bubble"></span>Intial </li>

        <li > <span class="bubble"></span> <a ng-click="gotoStep(2)">Loan amount</a> </li>

        <li> <span class="bubble"></span> <a ng-click="gotoStep(3)">Reconcile</a> </li>

        <li> <span class="bubble"></span> <a ng-click="gotoStep(4)">Accepted</a> </li>
         <li> <span class="bubble"></span>  </li>
    </ul> -->
    <ul class="steps-wrap col-md-10">
      <li class="completed"> 1 <span>Intial</</span></li>
      <li > 2  <a ng-click="gotoStep(2)">Loan amount</a> </li>
      <li> 3<a ng-click="gotoStep(3)">Reconcile</a></li>
      <li > 4 <a ng-click="gotoStep(4)">Accepted</a></li>
    </ul>
    <div class="col-sm-4 pull-right">
         <a class="btn btn-primary pull-right" ng-click="nextStep()">Next</a>
    </div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
            <div ng-if="var.error" class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert" ng-click="errorDismiss()" aria-label="close">&times;</a>
                <span>%%var.errorMessage%%</span><br/>
            </div>
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
            <div ng-if="var.message" class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" ng-click="messageDismiss()" aria-label="close">&times;</a>
                <span>%%var.messageText%%</span><br/>
            </div>
	    </div>
    </div>
 <div class="user-list-view">
    <div class="panel panel-default " style="overflow-x: scroll">
        <table id="example" class="stripe  order-column table table table-striped" cellspacing="0" width="100%" height="800">
        <thead>
            <th ng-repeat="head in headers">%%head%%</th>
        </thead>
        <tr ng-repeat="user in data" ng-init="outerIndex=$index+1">
            <td ng-repeat="val in user track by $index" ng-init="innerIndex=$index" class="valueInput">
            <input style="display:block" type="text" ng-if="editable[innerIndex]"  ng-model="data[outerIndex][innerIndex]" ng-change="update(%%user[0]%%, %%innerIndex%%, {{$id}})">
                <small ng-if="showError[outerIndex][innerIndex].code == 'error'" style="color:red"><i class="fa fa-exclamation-circle" aria-hidden="true"></i> </small>
                <small ng-if="showError[outerIndex][innerIndex].code == 'message'" style="color:green;margin-top:10px"><i class="fa fa-check" aria-hidden="true"></i>  </small>
            <label ng-if="!editable[innerIndex]">%%val%%</label>
            </td>
        </tr>
        </table>
   </div>
   
</div>

@section('js')
<script type="text/javascript">
	$(document).ready(function() {
        var table = $('#example').DataTable( {
            scrollY:        "300px",
            scrollX:        true,
            scrollCollapse: true,
            paging:         false,
            fixedColumns:   {
                leftColumns: 3
            }
        } );
    } );
    </script>
@endsection
