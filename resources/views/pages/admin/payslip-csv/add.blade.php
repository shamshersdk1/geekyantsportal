@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Create Payroll CSV</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
                      <li><a href="/admin/create-payroll-csv">Create Payroll CSV</a></li>
                      <li><a>New Payslip</a></li>
        		</ol>
            </div>
        </div>
	</div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
    </div>
   

    <div class="user-list-view">
    <div class="panel panel-default col-sm-12">
        <div class="row" style="margin:15px 0 15px 0">
            <form action = "/admin/create-payroll-csv" method="POST">
            {{csrf_field()}}
            <div class="col-sm-8 col-sm-offset-2" style="margin-bottom:15px">
                <label class="col-sm-4"> Month: </label>
                <div class="col-sm-8">
                    <select class="form-control" name="month" id="month">
                        <option value="1">January</option>
                        <option value="2">Feburary</option>
                        <option value="3">March</option>
                        <option value="4">April</option>
                        <option value="5">May</option>
                        <option value="6">June</option>
                        <option value="7">July</option>
                        <option value="8">August</option>
                        <option value="9">September</option>
                        <option value="10">October</option>
                        <option value="11">November</option>
                        <option value="12">December</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-8 col-sm-offset-2" style="margin-bottom:15px">
                <label class="col-sm-4"> Year: </label>
                <div class="col-sm-8">
                    <select class="form-control" name="year" id="year">
                    </select>
                </div>
            </div>

            <div class="col-sm-8 col-sm-offset-2">
                <label class="col-sm-4"> Total Number of Working Days :</label>
                <div class="col-sm-8">
                    <input type="number" name="days" class="form-control" value="22" style="display:inline" required>
                </div>
             </div>
           
            <div class="text-center col-sm-8 col-sm-offset-2">
                <button class="btn btn-primary" type="submit" style="margin-top:15px">Next</button>
            </div>
              </div>
            </form>
        </div>
   </div>
</div>
@endsection

@section('js')
<script>
    $(document).ready(function() {
        $date= new Date();
        $month=$date.getMonth()+1;
        $year=$date.getFullYear();
        $max=$date.getFullYear()+1;
        $options="";
        for($y = 2017 ; $y <=$max; $y++){
            $options += "<option value='"+$y+"'>"+ $y +"</option>";
        }
        $("#year").html($options);
        $('#month option[value="'+$month+'"]').prop("selected","true");
        $('#year option[value="'+($max-1)+'"]').prop("selected","true");s
    });
</script>

@endsection
