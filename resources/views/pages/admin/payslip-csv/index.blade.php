@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">List of Payroll</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li>List of Payrolls</li>
        		</ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <a href="create-payroll-csv/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Create payroll</a>
            </div>
        </div>
	</div>
   <div class="user-list-view">
   <div class="panel panel-default">
      <table class="table table-striped">
         <tbody>
            <tr>
               <th>Month</th>
               <th>Year</th>
               <th>Uploaded by</th>
               <th>Approved by</th>
               <th>Status</th>
               <th class="text-right">Actions</th>
            </tr>
            @if(!empty($csvMonths) && count($csvMonths) > 0)
                @foreach($csvMonths as $csvMonth)
                <tr>
                    <td class="td-text">
                        {{date('F', strtotime("2018-".$csvMonth->month."-01"))}}
                    </td>
                    <td class="td-text">
                        {{$csvMonth->year}}
                    </td>
                    <td class="td-text">
                        @if(!empty($csvMonth->creator)){{$csvMonth->creator->name}}@else Not available @endif
                    </td>
                    <td class="td-text">
                        @if(!empty($csvMonth->approver)){{$csvMonth->approver->name}}@else Not available @endif
                    </td>
                    <td class="td-text">
                        @if($csvMonth->status == 'pending')
                            <span class="label label-warning custom-label">Pending</span>
                        @elseif($csvMonth->status == 'approved')
                            <span class="label label-success custom-label">Approved</span>
                        @elseif($csvMonth->status == 'rejected')
                            <span class="label label-danger custom-label">Rejected</span>
                        @else
                            <span class="label label-info custom-label">{{ucfirst($csvMonth->status)}}</span>
                        @endif
                    </td>
                    <td class="td-text text-right">
                        @if($csvMonth->status != 'approved')	
                        <a href="/admin/create-payroll-csv/{{$csvMonth->id}}" class="btn btn-info crud-btn btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View</a>
                            <form class="delete-form" method="post" action="/admin/create-payroll-csv/{{$csvMonth->id}}" style="display:inline">
                            {{method_field("DELETE")}}
                                <button type="submit" id="delete-btn" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i>&nbsp; Delete</button>    
                            </form>
                        @else
                        <a class="btn btn-success btn-sm" href="/api/v1/payroll-csv-step1/{{$csvMonth->id}}/download?file={{$csvMonth->csv_path}}" download><i class="fa fa-download"></i> Download </a>
                        @endif
                    </td>
                </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="6">No records found</td>
                </tr>
            @endif
         </tbody>
      </table>
   </div>
</div>
</div>

@endsection


@section('js')
    <script>
        $(function(){
            $(".delete-form").submit(function(event){
                return confirm('Are you sure?');
            });
        });
    </script>

@endsection