@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid" ng-App="myApp">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">List of Late Comers</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="">Late to come</a></li>
                </ol>
            </div>
        </div>
    </div>
        <div class="row" ng-controller="lateListCtrl">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body row">
                        <div class="col-sm-6">
                            <label for="name" class="col-sm-3 ">Date</label>
                            <div class="col-sm-3">
                                <input date-range-picker class="date-picker form-control" type="text" ng-model="datePicker.date.selectedDate" options="datePicker.options"
                                ng-change="check()">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label for="name" class=" col-sm-3 ">Name</label>
                            <div class="col-sm-6">
                                <ui-select ng-model="selected_user" on-select="select_user($select.selected)" theme="select2" name="name" id="name" ng-style="error && {border: '1px solid red'}">
                                    <ui-select-match allow-clear="true">%%$select.selected.name%%</ui-select-match>
                                    <ui-select-choices repeat="user.id as user in userList | filter: $select.search">
                                        %%user.name%%
                                    </ui-select-choices>
                                </ui-select>
                                <div style="font-size: 12px; color: red; margin-right: 20px">
                                    %%error_message%%
                                </div>
                            </div>
                            <button class="btn btn-success btn-sm" ng-click="addToLateList()">
                                Add
                            </button>
                        </div>
                    </div>
                    <div class="panel-body1">
                        <div ng-show="loading" class="text-center" style="padding-top:20px">
                            <i class="fa fa-spinner fa-spin fa-lg"></i>
                        </div>
                        <table ng-hide="loading" class="table table-striped text-small">
                            <tr>
                                <th width="20%">Name</th>
                                <th width="20%">Reported by</th>
                                <th width="20%">Date</th>
                                <th width="20%">Created At</th>
                                <th width="25%" class="text-right">Actions</th>
                            </tr>
                            <tr ng-repeat="list in latelist">
                                <td>%%list.user.name %%</td>
                                <td>%%list.reported_by.name %%</td>
                                <td>%%filteredDate(list.date) | date : 'dd MMM yyyy' %%</td>
                                <td>%%filteredDate(list.created_at) | date : 'dd/MM/yyyy h:mm a' %%</td>
                                <td class="text-right" ng-click="delete(list)"><a  name="delete-button" class="btn btn-danger btn-sm">Delete</a></td>
                            </tr>
                            <tr ng-if="!latelist.length">
                                <td colspan="3">No Items added</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection