@extends('layouts.admin-dashboard')

@section('main-content')
	<section class="lead-dashboard-section">
		<div class="container-fluid">
            <div class="breadcrumb-wrap">
               <div class="row">
                 <div class="col-sm-8">
                    <h1 class="admin-page-title">Leads</h1>
                        <ol class="breadcrumb">
                            <li><a href="/admin">Admin</a></li>
                            <li><a>Leads</a></li>
                        </ol>
                 </div>
               </div>
        
			</div>
			<div class="user-list-view">
				<div class="panel panel-default">

					<table class="table table-striped">
							<thead>
								<th width="10%">#</th>
								<th width="15%">Name</th>
								<th width="15%">Email Address</th>
								<th class="text-right">Actions</th>
							</thead>
							<tr>
											<td class="td-text">id</td>
											<td class="td-text">Test</td>
											<td class="td-text">test@gmail.com</td>
											<td class="text-right">
												<a href="#" class="btn btn-success crud-btn btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View</a>	

												<form action="#" method="post" style="display:inline-block;" >
													<input name="_method" type="hidden" value="DELETE">
													<button class="btn btn-danger crud-btn btn-sm" type="submit"><i class="fa fa-trash btn-icon-space" aria-hidden="true"></i>Delete</button>
												</form>
											</td>
										</tr>
					</table>
				</div>

		</div>
		</div>
	</section>
@stop
@section('js')
@parent
	<script>
		
		function deleteUser() {
			var x = confirm("Are you sure you want to delete?");
			if (x)
				return true;
			else
				return false;
		}
	</script>
@endsection

