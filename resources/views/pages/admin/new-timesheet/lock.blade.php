@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">User Timesheet Lock</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/new-timesheet/lock') }}">User Timesheet Lock</a></li>
                </ol>
            </div>
        
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
    	@if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span><br/>
                  @endforeach
            </div>
        @endif
        @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
        @endif  
    <div class="user-list-view">
        <div class="panel panel-default"> 
            <table class="table table-striped">
                <thead>
                    <th>#</th>
                    <th>Name</th>
                    <th>Lock Date</th>
                    <th>Billing Date</th>
                    <th>Date Updated By</th>
                    <th>Billing Updated By</th>
                    <th>Created At</th>
                    <th>Actions</th>
                </thead>
                @if(count($lockObj) > 0)
                    @foreach($lockObj as $index => $lock)
                        <tr>
                            <td class="td-text">{{$index+1}}</td>
                            <td class="td-text">{{$lock->user->name}}</td>
                            <td class="td-text">{{date_in_view($lock->date)}}</td>
                            <td class="td-text">{{date_in_view($lock->billing_date)}}</td>
                            <td class="td-text">{{$lock->dateUpdated->name ?? ''}}</td>
                            <td class="td-text">{{$lock->billingUpdated->name ?? ''}}</td>
                            <td class="td-text">{{date_in_view($lock->created_at)}}</td>
                            <td class="td-text">
                                <form action="/admin/new-timesheet/lock/{{$lock->id}}/edit" method="put" style="display:inline-block;" >
                                    <button class="btn btn-primary  btn-sm" type="submit"><i class="fa fa-edit btn-icon-space" aria-hidden="true"></i>Edit</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="10">No results found.</td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
        @if(!empty($lockObj))
            {{$lockObj->links()}}
        @endif
	</div>
</div>
@endsection
