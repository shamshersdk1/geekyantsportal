@extends('layouts.admin-dashboard')
@section('main-content')
<script type="text/javascript">
  window.user = '<?php echo json_encode($user); ?>';
</script>
<div class="container-fluid timelog" ng-app="myApp" ng-controller="newTimesheetLeadViewCtrl">
  <div class="breadcrumb-wrap">
    <div class="row">
      <div class="col-sm-8">
        <h1 class="admin-page-title"></h1>
        <ol class="breadcrumb">
          <li><a>Timesheet</a></li>
          <li><a>Review</a></li>
        </ol>
      </div>
      <div class="col-sm-4 text-right m-t-10">
        <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
      </div>
    </div>
  </div>
  <div class="panel panel-default" style="margin: -15px -15px 15px -15px;">
    <div class="panel-body">
      <div class="row date-select">
        <div class="col-md-4">
            <label>Project</label>
            <ui-select ng-model="selectedProject" on-select="selectProject($select.selected)" theme="select2" name="project" id="project">
                <ui-select-match placeholder="All projects">%%$select.selected.name%%</ui-select-match>
                <ui-select-choices repeat="project.id as project in projectList | filter: $select.search">
                    %%project.project_name%%
                </ui-select-choices>
            </ui-select>
        </div>
        <div class="col-sm-8 ">
          <ul class="nav nav-tabs pull-right" style="margin: 30px 0 0 0;">
            <li class="active" ng-click="loadPendingList()"><a data-toggle="tab" href="#pending">Pending</a></li>
            <li ng-click="loadApprovedList()"><a data-toggle="tab" href="#approved">Approved</a></li>
            <li ng-click="loadRejectedList()"><a data-toggle="tab" href="#rejected">Rejected</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
    
  <div class="tab-content">
    <div id="pending" class="tab-pane fade in active">
      <div class="panel panel-default">
        <table class="table table-condensed">
          <thead style="width:100%; display:table;">
            <tr>
              <th width="10%">Date</th>
              <th width="10%">User</th>
              <th width="10%">Projects</th>
              <th width="8%">Total hours</th>
              <th width="40%">Task</th>
              <th width="12%" class="text-right">Action</th>
            </tr>
          </thead>
          <tbody ng-class="{overlay: dataIsLoading}" vs-repeat="{size:125}" style="height:450px; width:100%; display:block; overflow:scroll">
              <tr ng-repeat="record in timesheetHistory" style="display: table; width: 100%;"> 
                  <td width="10%">%%record.date| date: 'dd MMM yyyy' %%</td>
                  <td width="10%">%%record.user.name%%</td>
                  <td width="10%"> %%record.project.project_name%%</td>
                  <td width="8%">
                      <input disabled class="form-control m-b-5" type="text" ng-model-options="{ debounce: 1000 }" ng-model="record.duration"  style="width:70px"/>
                      <input ng-disabled="record.status != 'pending'" class="form-control" type="text" ng-model="record.review.approved_duration" style="width:70px" />
                  </td>
                  <td width="40%">
                    <textarea disabled class="form-control m-b-5"  rows="2" style="" ng-model="record.task" ng-model-options="{ debounce: 1000 }" ></textarea>
                    <textarea ng-disabled="record.status != 'pending'" class="form-control"  rows="2" style="" ng-model="record.review.approval_comments" ></textarea>
                  </td>
                  <td width="12%" ng-if="record.status == 'pending'" class="text-right">
                    <button class="btn btn-sm btn-default text-danger" ng-disabled="saving[record.id]" ng-click="reject(record)">Reject</button> 
                    <button class="btn btn-sm btn-success" ng-disabled="saving[record.id]" ng-click="approve(record)">Approve</button>
                  </td>
              </tr>
          </tbody>
        </table>
      </div>
      <div ng-show="metaData.total>0">
        <div class="row">
          <div class="col-md-12">
            <div class="pull-right">
              <ul uib-pagination total-items="metaData.total" ng-model="metaData.current_page" items-per-page="100" ng-change="changePage()"  boundary-links="false" class="no-margin"></ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="approved" class="tab-pane fade">
      <div class="panel panel-default">
        <table class="table table-condensed">
          <thead>
            <tr>
              <th width="10%">Date</th>
              <th width="10%">User</th>
              <th width="10%">Projects</th>
              <th width="8%">Total hours</th>
              <th width="40%">Task</th>
              <th width="12%"></th>
            </tr>
          </thead>
          <tbody>
            <tr ng-show="timesheetHistoryApproved.length == 0">
              <td colspan="6"> No Records found</td>
            </tr>
            <tr ng-repeat="record in timesheetHistoryApproved"> 
              <td>%%record.date| date: 'dd MMM yyyy' %%</td>
              <td>%%record.user.name%% </td>
              <td> %%record.project.project_name%%</td>
              <td>
                <input disabled class="form-control m-b-5" type="text" ng-model-options="{ debounce: 1000 }" ng-model="record.duration"  style="width:70px"/>
                <input ng-disabled="record.status != 'pending'" class="form-control" type="text" ng-model="record.review.approved_duration" style="width:70px" />
              </td>
              <td>
                <textarea disabled class="form-control m-b-5"  rows="2" style="" ng-model="record.task" ng-model-options="{ debounce: 1000 }" ></textarea>
                <textarea ng-disabled="record.status != 'pending'" class="form-control"  rows="2" style="" ng-model="record.review.approval_comments" ></textarea>
              </td>
              <td class="text-right">
                <span ng-if="record.status == 'approved'" >
                  <label class="label label-success custom-label m-b-5">Approved</label>
                  <div class="small text-muted"><strong>on %%record.updated_at | moment: 'format': 'DD MMM YYYY, HH:mm:ss'%%</strong></div>
                </span>
                <span ng-if="!saving[record.id] && !success[record.id]"></span>
                <span ng-if="saving[record.id]">Saving...</span>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <div ng-show="metaData.total>0">
        <div class="row">
          <div class="col-md-12">
            <div class="pull-right">
              <ul uib-pagination total-items="metaData.total" ng-model="metaData.current_page" items-per-page="100" ng-change="changePage()"  boundary-links="false" class="no-margin"></ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="rejected" class="tab-pane fade">
      <div class="panel panel-default">
        <table class="table table-condensed">
          <thead>
            <tr>
              <th width="10%">Date</th>
              <th width="10%">User</th>
              <th width="10%">Projects</th>
              <th width="8%">Total hours</th>
              <th width="40%">Task</th>
              <th width="12%"></th>
            </tr>
          </thead>
          <tbody>
            <tr ng-show="timesheetHistoryRejected.length == 0">
              <td colspan="6"> No Records found</td>
            </tr>
            <tr ng-repeat="record in timesheetHistoryRejected"> 
              <td>%%record.date| date: 'dd MMM yyyy' %%</td>
              <td>%%record.user.name%%</td>
              <td>%%record.project.project_name%%</td>
              <td>
                <input disabled class="form-control m-b-5" type="text" ng-model-options="{ debounce: 1000 }" ng-model="record.duration"  style="width:70px"/>
                <input ng-disabled="record.status != 'pending'" class="form-control" type="text" ng-model="record.review.approved_duration" style="width:70px" />
              </td>
              <td>
                <textarea disabled class="form-control m-b-5"  rows="2" style="" ng-model="record.task" ng-model-options="{ debounce: 1000 }" ></textarea>
                <textarea ng-disabled="record.status != 'pending'" class="form-control"  rows="2" style="" ng-model="record.review.approval_comments" ></textarea>
              </td>
              <td class="text-right">
                <span ng-if="record.status == 'rejected'">
                  <label class="label label-danger custom-label m-b-5">Rejected</label>
                  <div class="small text-muted"><strong>on %%record.updated_at | moment: 'format': 'DD MMM YYYY, HH:mm:ss' %% </strong></div>
                </span>
                <span ng-if="!saving[record.id] && !success[record.id]"></span> 
                <span ng-if="saving[record.id]">Saving...</span>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
        
      <div ng-show="metaData.total>0">
        <div class="row">
          <div class="col-md-12">
            <div class="pull-right">
              <ul uib-pagination total-items="metaData.total" ng-model="metaData.current_page" items-per-page="100" ng-change="changePage()"  boundary-links="false" class="no-margin"></ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection