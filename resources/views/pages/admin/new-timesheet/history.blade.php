@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid timelog" ng-app="myApp" ng-controller="newTimesheetHistoryCtrl">
  <div class="breadcrumb-wrap">
    <div class="flex-container align-items-center justify-content-between">
      <div>
        <h1 class="admin-page-title"></h1>
        <ol class="breadcrumb">
          <li><a>Timesheet</a></li>
          <li><a>Review</a></li>
        </ol>
      </div>
      <div>
        <button type="button" onclick="window.history.back();" class="btn btn-default no-margin"><i class="fa fa-caret-left fa-fw"></i> Back</button>
      </div>
    </div>
  </div>
  <div class="panel panel-default" >
      <div class="panel-body">
        <div class="row date-select">
          <div class="col-lg-1 col-md-1 col-xs-2"><label >Project</label></div>
          <div class="col-lg-5 col-md-5 col-xs-4">
              <ui-select ng-model="selectedProject" on-select="selectProject($select.selected)" theme="select2" name="project" id="project">
                  <ui-select-match placeholder="All projects">%%$select.selected.name%%</ui-select-match>
                  <ui-select-choices repeat="project.id as project in projectList | filter: $select.search">
                      %%project.name%%
                  </ui-select-choices>
              </ui-select>
          </div>
          <div class="col-lg-6 col-md-6 col-xs-6">
            <p  style="font-size:15px;color: #DE0000;">
              @if(Auth::user()->timesheetLock->date != null)
              Timesheet is locked till date {{date_to_ddmmyyyy(Auth::user()->timesheetLock->date) ?? null}}
              @endif
            </p>
          </div>
        </div>          
      </div>
    </div>
  <div class="panel panel-default">
    <div class="table-responsive task-table">
      <table class="table ">
        <thead>
          <tr>
            <th class="td-text" width="15%">Date</th>
            <th class="td-text" width="15%">Projects</th>
            <th class="td-text" width="10%">Total hours</th>
            <th class="td-text" width="50%">Task</th>
            <th class="td-text" width="10%">Actions</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="record in timesheetHistory"> 
            <td>%%record.date| date: 'EEE, MMMM d, yyyy' %% </td>
    
            <td>
              %%record.project.project_name%%
            </td>
            <td>
              <input ng-disabled="record.status != 'pending'" class="form-control" type="text" ng-model-options="{ debounce: 1000 }" ng-model="record.duration" ng-change="update(record)" style="width:70px"/>
            </td>
            <td>
              <textarea ng-disabled="record.status != 'pending'" class="form-control"  rows="2" style="" ng-model="record.task" ng-model-options="{ debounce: 1000 }" ng-change="update(record)"></textarea>
            </td>
            <td ng-if="record.status == 'pending' && !timesheet_error[record.id]">
                <button class="btn btn-sm btn-danger text-center" ng-click="delete(record, $index)">Delete</button>
            </td> 
            <td ng-if="saving[record.id] && !timesheet_error[record.id]">
              saving...
            </td>
            <td ng-if="timesheet_error[record.id]">
              %%error_message[record.id]%%
            </td>
            <td ng-if="success[record.id]" style="color: green">
              saved
            </td>
            <td>
              <i ng-if="record.status != 'pending'" class="fa fa-check fa-fw"></i>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    
    <div class="padder table-footer m-t-10 m-b-10" ng-show="metaData.total>0">
        <div class="row">
            <div class="col-md-12">
                <div class="pull-right">
                    <ul uib-pagination total-items="metaData.total" ng-model="metaData.current_page" items-per-page="100" ng-change="changePage()"  boundary-links="false"></ul>
                </div>
            </div>
        </div>
    </div>
  </div>
  <h1> Unconfirmed Timesheet Entries </h1>
  <div class="panel panel-default">
    <div class="table-responsive">
        <table class="table">
          <thead>
            <tr>
              <th class="td-text" width="15%">Date</th>
              <th class="td-text" width="15%">Channel</th>
              <th class="td-text" width="10%">Total hours</th>
              <th class="td-text" width="50%">Task</th>
              <th class="td-text" width="10%">Actions</th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="record in tmpTimesheetHistory"> 
              <td>%%record.date| date: 'dd MMM yyyy' %%</td>
            
              <td>
                %%record.channel_name%%
              </td>
              <td>
                <input class="form-control" type="text" ng-model-options="{ debounce: 1000 }" ng-model="record.duration" ng-change="updateTemp(record)" style="width:70px"/>
              </td>
              <td>
                <textarea class="form-control"  rows="2" style="" ng-model="record.task" ng-model-options="{ debounce: 1000 }" ng-change="updateTemp(record)"></textarea>
              </td>
              <td>
                  <button class="btn btn-sm btn-danger text-center" ng-click="deleteTemp(record, $index)">Delete</button>
              </td>
              <td ng-if="savingTemp[record.id]">
                saving...
              </td>
              <td ng-if="successTemp[record.id]" style="color: green">
                saved
              </td>
              <td>

              </td>
            </tr>
          </tbody>
        </table>
    </div>
    
    <div class="padder table-footer m-b-10" ng-show="tmpMetaData.total>0">
        <div class="row">
            <div class="col-md-12">
                <div class="pull-right">
                    <ul uib-pagination total-items="tmpMetaData.total" ng-model="tmpMetaData.current_page" items-per-page="100" ng-change="changeTempPage()"  boundary-links="false"></ul>
                </div>
            </div>
        </div>
    </div>
  </div>

</div>
@endsection