@extends('layouts.admin-dashboard')
@section('main-content')
<style>
    .panel.panel-horizontal {
        display:table;
        width:100%;
        margin-bottom:0px;
        border-style:solid;
        border-width:0px 0px 1px 0px;
    }
    .panel.panel-horizontal > .panel-heading, .panel.panel-horizontal > .panel-body{
        display:table-cell;
    }
    .panel.panel-horizontal > .panel-heading {
        /*width: 20%;*/
        background-color:#FFF;
        border-right: none;
        border-bottom: none;
    }
    .info-right-table td{
        padding:0px 15px 0px 15px !important;
    }
    .navbar.navbar-default{
        background-color:#FFF;
    }
    
    #options > li > a{
    text-transform:none;
    color:#34323b;
    width:100%;
    }
    #options > li{
    min-width:8.15%;
    }
    @media(max-width:900px){
        #options > li{
    width:100%;
    }
    }
    #options{
        width:100%;
        li{
            a{
                border: 2px solid transparent;
            }
        }
    }
    #options > .active > a, #options > .active > a:hover, #options > li > a:hover{
    text-decoration: none;
    border-radius:0px;
    border-bottom-width: 2px !important;
    border-bottom-style: solid;
    border-bottom-color: #5470AC !important;
    color: #5470AC !important;
    background-color: transparent !important;
    -webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */
    -moz-box-sizing: border-box;    /* Firefox, other Gecko */
    box-sizing: border-box;
    }
    small{
        color:#32435C !important;
        font-size: 82%;
    }
    </style>
<div class="container-fluid timelog">
  <div class="breadcrumb-wrap">
    <div class="row">
      <div class="col-sm-8">
        <h1 class="admin-page-title"></h1>
        <ol class="breadcrumb">
          <li><a>Timesheet</a></li>
          <li><a>Review</a></li>
        </ol>
      </div>
      <div class="col-sm-4 text-right m-t-10">
        <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
      </div>
    </div>
  </div>
  <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12">
          <nav class="navbar navbar-default">
              <div class="container-fluid pull-left">
                  <ul class="nav navbar-nav profile-tabs-container" id="options">
                      <li @if(Request::is('admin/new-timesheet/lead-view')) class="active" @endif><a href="/admin/new-timesheet/lead-view">Pending</a></li>
                      <li @if(Request::is('admin/new-timesheet/lead-view/approved')) class="active" @endif><a href="/admin/new-timesheet/lead-view/approved">Approved</a></li>
                  </ul>
              </div>
          </nav>
      </div>
  </div>
  <div class="panel panel-default">
    <table class="table ">
      <thead>
        <tr>
          <th class="td-text" width="10%">Date</th>
          <th class="td-text" width="10%">User</th>
          <th class="td-text" width="10%">Projects</th>
          <th class="td-text" width="10%">Total hours</th>
          <th class="td-text" width="40%">Task</th>
        </tr>
      </thead>
      <tbody>
        @if( count($userTimesheets) > 0 )
          @foreach( $userTimesheets as $userTimesheet )
          @if ( $userTimesheet->review && $userTimesheet->review->approver_id != 0 )
          <tr> 
              <td>{{ date_in_view($userTimesheet->date)}}</td>
              <td>
                {{ $userTimesheet->user->name }}
              </td>
              <td>
                {{ $userTimesheet->project->project_name }}
              </td>
              <td>
                  {{ $userTimesheet->duration }}
              </td>
              <td>
                  {{ $userTimesheet->task }}
              </td>
            </tr>
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td>
                  {{ $userTimesheet->review->approved_duration }}
              </td>
              <td>
                  {{ $userTimesheet->review->approval_comments }}
              </td>
              </tr>
            </tr>
            @endif
          @endforeach
        @endif
      </tbody>
    </table>

  </div>
</div>
@endsection