@extends('layouts.admin-dashboard')
@section('main-content')

<section class="new-project-section">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">User Timesheet Lock</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
			  			<li><a href="{{ url('admin/new-timesheet/lock') }}">User Timesheet Lock</a></li>
			  			<li class="active">Update</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>

	    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif

		<form class="form-horizontal" method="post" action="/admin/new-timesheet/lock/{{$lockObj->id}}" enctype="multipart/form-data">
        {{ method_field('PUT') }}
			<div class="panel panel-default">
				<div class="panel-body">
				    <div class="row">
				    	<div class="col-md-12">
						  	<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Employee Name</label>
						    	<div class="col-sm-5">
						    		<input type="text" id="user_id" class="form-control" name="user_id" value="{{$lockObj->user->name ?? null}}" disabled/>
						    	</div>
						  	</div>
						  	
						  	<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Lock Date</label>

								<div class="col-sm-3">
									<div class='input-group date' id='datetimepicker1'>
									<input type='text' name="date" placeholder = "Lock Date" class="form-control" value="{{date_to_ddmmyyyy($lockObj->date)}}" required/>
										<span class="input-group-addon">
											<span><i class="fa fa-calendar" aria-hidden="true"></i></span>
										</span>
									</div>
                                </div>
                              </div>
                              <div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Billing Date</label>

								<div class="col-sm-3">
									<div class='input-group date' id='datetimepicker2'>
										<input type='text' name="billing_date" placeholder = "Billing Date" class="form-control" value="{{date_to_ddmmyyyy($lockObj->billing_date)}}"/>
											<span class="input-group-addon">
												<span><i class="fa fa-calendar" aria-hidden="true"></i></span>
											</span>
									</div>
								</div>
							</div>
                            	<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Date Updated By</label>
						    	<div class="col-sm-5">
						    		<input type="text" id="dateUpdatedBy" class="form-control" name="date_updated_by" value="{{$lockObj->dateUpdated->name ?? null}}" disabled/>
						    	</div>
						  	</div>
                            <div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Billing Updated By</label>
						    	<div class="col-sm-5">
						    		<input type="text" id="billingUpdatedBy" class="form-control" name="billing_updated_by" value="{{$lockObj->billingUpdated->name ?? null}}" disabled/>
						    	</div>
						  	</div>
					  	</div>
					</div>
				</div>
			</div>
  	  		<div class="text-center">
		  		<button type="submit" class="btn btn-success">Save</button>
		  	</div>
		</form>
	</div>
</section>
	<script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker({
					format: 'DD-MM-YYYY'
				});
                $('#datetimepicker2').datetimepicker({
					format: 'DD-MM-YYYY'
                });
            });
	</script> 
@endsection