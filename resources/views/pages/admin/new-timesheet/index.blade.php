@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid timelog" ng-app="myApp" ng-controller="newTimesheetCtrl">
  <div class="breadcrumb-wrap">
    <div class="flex-container justify-content-between ">
      <div class="">
        <h1 class="admin-page-title"></h1>
        <ol class="breadcrumb">
          <li><a href="/admin">Admin</a></li>
          <li><a>Timesheet</a></li>
        </ol>
      </div>
      <div class="">
        <button type="button" style="margin-top:0;" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
      </div>
    </div>
  </div>
  <div class="panel panel-default" >
    <div class="panel-body">
      <div class="row date-select">
        <div class="col-xs-6 col-md-5">
           <select class="project" id="selectedDateID" name="selectedDate" ng-model="selectedDate">
              <!-- <option value="%%dates[0]%%">%%dates[0] | date: 'EEE'%% ,Today</option> -->
              <option value="%%dates[0]%%">Today</option>
              <option value="%%dates[1]%%">Yesterday</option>
              <option value="%%dates[2]%%">%%dates[2] | date: 'EEE, MMMM d, yyyy'%%</option>
              <option value="%%dates[3]%%">%%dates[3] | date: 'EEE, MMMM d, yyyy'%%</option>
              <option value="%%dates[4]%%">%%dates[4] | date: 'EEE, MMMM d, yyyy'%%</option>
              @for($i=5; $i<=45; $i++)
                <option value="%%dates[{{$i}}]%%">%%dates[{{$i}}] | date: 'EEE, MMMM d, yyyy'%%</option>
              @endfor
          </select>
        </div>
        <div class="col-xs-8 col-md-7">
          <div class="text-info m-t-5">
              <p class ="col-md-6" style="font-size:15px;color: #DE0000;">
                  @if(Auth::user()->timesheetLock->date != null)
                  Timesheet is locked till date {{date_to_ddmmyyyy(Auth::user()->timesheetLock->date) ?? null}}
                  @endif
                </p>
            <!-- <div style="padding: 0 0 2px 20px;"><strong>Note:</strong></div> -->
            <!-- <ul>
              <li>Please fill the actual work hours you spent on the project (Not in the office). </li>
              <li>Timelog between 9-11 hours will not be considered as extra working hours.</li>
              <li>Extra working hours needs an approval by your RM.</li>
            </ul> -->
          </div>
        </div>
      </div>
    </div>
  </div>
  <form>
    <div class="panel panel-default">
        <div class="table-responsive">
            <table class="table">
              <thead>
                <tr>
                  <th  class="td-text" width="30%">Projects</th>
                  <!-- ngRepeat: date in dates -->
                  <th  class="td-text" >Description</th>
                  <th  class="td-text" width="20%"><span class="pull-right">Total hours</span></th>

                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="i in [0,1,2,3,4]">
                  <td ng-if="projectList.length > 0">
                      <ui-select ng-model="selectedProject[i]" on-select="selectProject($select.selected, i)" theme="select2" name="name" id="name" ng-style="form.userSelectError && {border: '1px solid red'}">
                          <ui-select-match>%%$select.selected.name%%</ui-select-match>
                          <ui-select-choices repeat="project.id as project in projectList | filter: $select.search">
                              %%project.name%%
                          </ui-select-choices>
                      </ui-select>
                  </td>
                  <td>
                    <textarea class="form-control"  rows="2" style="" ng-model="tasks[i]" ng-style="taskError[i] && !tasks[i] && {border: '1px solid red'}"></textarea>
                  </td>
                  <td>
                    <span class="pull-right"><input  class="form-control" type="text" ng-model="loggedTime[i]" style="width:70px" ng-style="durationError[i] && !loggedTime[i] && {border: '1px solid red'}"/></span>
                  </td>
                </tr>
                
              </tbody>
            </table>
        </div>
    </div>
    <div ng-if="error" class="sick-leave col-md-12" style="padding:0;text-align: center">
      <p style="font-size:15px;color: #DE0000;">%% errorMessage %%</p>
    </div>
    <div class="col-sm-12 text-center">
      <button class="btn btn-success text-center" ng-click="submit()"> Submit</button>
    </div>
  </form>
</div>
<script>
  $(document).ready(function() {
    $('.project').select2();
  });
</script>
@endsection