@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Records</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
		  			<li class="active">Slack Log View</li>
        		</ol>
            </div>
        </div>
    </div>

    <div class="row">
    	@if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span><br/>
                  @endforeach
            </div>
        @endif
        @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
        @endif
        <div class="col-md-12 bg-white search-section-top ">
        <div class="row" style="width:100%">
    	<div class="col-sm-10">
            <form action="/admin/slack-access-logs"  method="get">
                <div class="input-group">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class='input-group date' id='datetimepickerStart_search'>
                                    <input type='text' id="sd" class="form-control" value="<?=( isset( $_GET['created_at'] ) ? $_GET['created_at'] : '' )?>" autocomplete="off" name="created_at" placeholder="Search By Date" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <span class="input-group-btn" style="padding-right:10px">
                                    <button type="submit" class="btn btn-primary btn-sm button-space">Search</button>
                                </span>
                            	<span class="input-group-btn">
                                    <button type="reset" class="btn btn-primary btn-sm button-space">Reset</button>
                                 </span>
                            </div>  
                        </div>
                    </div>    
                </div>
            </form>
        </div>
        </div>
        </div>
        <div class="col-sm-12 user-list-view m-t-15">
            <div class="panel panel-default">
            	<table class="table table-striped">
            		<thead>
                        <th>From</th>
                        <th>Request</th>
                        <th>Raw Response</th>
                        <th>To</th>
	                    <th>Date</th>
	                    <!-- <th class="text-right">Actions</th> -->
	                </thead>    
                    @if(!empty($logs))
                        @foreach($logs as $log)
                            <tr>
                                <td>{{$log->from}}</td>
                                <td>{{dump(json_decode($log->request, true))}}</td>
                                <td>{{dump(json_decode($log->raw_response, true))}}</td>
                                <td>{{$log->to}}</td>
                                <td>{{datetime_in_view($log->created_at)}}</td>
                                <!-- <td class="text-right"><button class="btn btn-info btn-sm" type="button"
									data-toggle="modal" data-target="#viewRecordModal"
									data-id="{{$log->id}}" data-title="Algorithms"><i class="fa fa-eye"></i>View</button>                          
								</td> -->
                            </tr>
                        @endforeach
		            @else
	            		<tr>
	            			<td colspan="6" class="text-center">No results found.</td>
	            		</tr>
	            	@endif
            	</table>
			</div>
		</div>	
		 <div class="pull-right">
            {{ $logs->links(); }}
        </div>
	</div>
</div>
<script>
	$(function() {
        $('#datetimepickerStart_search').datetimepicker({
            format: 'DD-MM-YYYY'
        });
	});
</script>
@endsection
