@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Expense Heads</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
		  			<li class="active">Expense Heads</li>
        		</ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
				<a href="/admin/expense-head/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Add Expense Head</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
	    	@if(!empty($errors->all()))
	            <div class="alert alert-danger">
	                @foreach ($errors->all() as $error)
	                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                    <span>{{ $error }}</span><br/>
	                  @endforeach
	            </div>
	        @endif
	        @if (session('message'))
	            <div class="alert alert-success">
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ session('message') }}</span><br/>
	            </div>
			@endif
		
			<div class="user-list-view">
				<div class="panel panel-default">
					<table class="table table-striped">
						<tr>
							<th width="5%" class="sorting_asc">#</th>
							<th width="15%">Title</th>
							<th width="15%">Low Cost Approver</th>
							<th width="15%">High Cost Approver</th>
							<th width="15%">Created At</th>
							<th width="15%">Updated At</th>
							<th class="text-right sorting_asc" width="20%">Actions</th>
						</tr>
						@if(count($expenseHeads) > 0)
							@foreach($expenseHeads as $expenseHead)
								<tr>
									<td class="td-text"><a>{{$expenseHead->id}}</a></td>
									<td class="td-text">{{$expenseHead->title}}</td>
									<td class="td-text"> {{!empty($expenseHead->lowCostApprover->name) ? $expenseHead->lowCostApprover->name : '' }}</td>
									<td class="td-text">{{!empty($expenseHead->highCostApprover->name) ? $expenseHead->highCostApprover->name : '' }}</td>
									<td class="td-text">{{datetime_in_view($expenseHead->created_at)}}</td>
									<td class="td-text">{{datetime_in_view($expenseHead->updated_at)}}</td>
									<td class="text-right">
										<a href="/admin/expense-head/{{$expenseHead->id}}" class="btn btn-success crude-btn btn-sm">View</a>
										<a href="/admin/expense-head/{{$expenseHead->id}}/edit" class="btn btn-info btn-sm crude-btn">Edit</a>
										<form action="/admin/expense-head/{{$expenseHead->id}}" method="post" style="display:inline-block;">
											<input name="_method" type="hidden" value="DELETE">
											<button class="btn btn-danger btn-sm crude-btn" type="submit">Delete</button>
										</form>
									</td>
								</tr>
							@endforeach
						@else
							<tr>
								<td colspan="5" class="text-center">No results found.</td>
							</tr>
						@endif
					</table>
					<div class="col-md-12">
						<div class="pageination pull-right">
							<nav aria-label="Page navigation">
									<ul class="pagination">
									{{ $expenseHeads->render() }}
									</ul>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
@endsection
