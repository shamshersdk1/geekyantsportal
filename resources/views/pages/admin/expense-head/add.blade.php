@extends('layouts.admin-dashboard')
@section('main-content')

<section class="new-project-section">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">Expense Heads</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
			  			<li><a href="{{ url('admin/expense-head') }}">Expense Heads</a></li>
			  			<li class="active">Add</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>

	    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif

		<form class="form-horizontal" method="post" action="/admin/expense-head" enctype="multipart/form-data">
			<div class="panel panel-default">
				<div class="panel-body">
				    <div class="row">
				    	<div class="col-md-12">
							<div class="form-group">
						    	<label for="department_type" class="col-sm-2 control-label">Title</label>
						    	<div class="col-sm-6">
						      		<input type="text" class="form-control" id="title" name="title" value="{{ old('title') }}">
						    	</div>
							</div>
							@if(isset($users))
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">Low Cost Approver</label>
								<div class="col-sm-6">
									<select id="selectid2" name="low_cost_approver"  style="width=35%;" placeholder= "Select an option">
										<option value=""></option>
										@foreach($users as $x)
											<option value="{{$x->id}}" >{{$x->name}}</option>
										@endforeach
									</select>
								</div>
							</div>
							@endif
							@if(isset($users))
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">High Cost Approver</label>
								<div class="col-sm-6">
									<select id="selectid22" name="high_cost_approver"  style="width=35%;" placeholder= "Select an option">
										<option value=""></option>
										@foreach($users as $y)
											<option value="{{$y->id}}" >{{$y->name}}</option>
										@endforeach
									</select>
								</div>
							</div>
							@endif
					  	</div>
					</div>
				</div>
			</div>
  	  		<div class="text-center">
		  		<button type="submit" class="btn btn-success">Add Expense Head</button>
		  	</div>
		</form>
	</div>
</section>
@endsection
