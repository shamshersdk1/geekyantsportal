@extends('layouts.admin-dashboard')
@section('main-content')

<section class="new-project-section">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">Onsite Allowance</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
			  			<li><a href="{{ url('admin/onsite-allowance') }}">Onsite Allowance</a></li>
			  			<li class="active">Add</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>

	    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif

		<form class="form-horizontal" method="post" action="/admin/onsite-allowance" enctype="multipart/form-data">
			<div class="panel panel-default">
				<div class="panel-body">
				    <div class="row">
				    	<div class="col-md-12">
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">Employee Name</label>
								<div class="input-group col-sm-4">
									@if(count($userList) > 0 )
										<select id="selectid2" name="user_id"  style="width=35%;" placeholder= "Select name" required>
											<option value=""></option>
											@foreach($userList as $user)
									        	<option value="{{$user->id}}" >{{$user->name}}</option>
										    @endforeach
										</select>
									@endif
								</div>
							</div>
						  	<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Start Date</label>
						    	<div class="input-group date col-sm-4" id="datetimepicker1" >
									<input type="text" name="start_date" class="form-control" required/>
									<span class="input-group-addon" style="position:relative">
									<span class="fa fa-calendar"></span>
									</span>
								</div>
						  	</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">End Date</label>
								<div class="input-group date col-sm-4" id="datetimepicker2" >
										<input type="text" name="end_date" class="form-control" required/>
										<span class="input-group-addon" style="position:relative">
										<span class="fa fa-calendar"></span>
										</span>
									</div>
							</div>
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">Project Name</label>
								<div class="input-group col-sm-4">
									@if(count($projectList) > 0 )
										<select id="selectid22" name="project_id"  style="width=35%;" placeholder= "Select name" required>
											<option value=""></option>
											@foreach($projectList as $project)
									        	<option value="{{$project->id}}" >{{$project->project_name}}</option>
										    @endforeach
										</select>
									@endif
								</div>
							</div>
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">Status</label>
								<div class="input-group col-sm-4">
									@if(count($projectList) > 0 )
										<select id="selectid3" name="status"  style="width=35%;" placeholder= "Select status" required>
											<option value="pending">Pending</option>
											<option value="approved">Approved</option>
											<option value="rejected">Rejected</option>
										</select>
									@endif
								</div>
							</div>
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">Type</label>
								<div class="input-group col-sm-4">
									@if(count($projectList) > 0 )
										<select id="selectid4" name="type"  style="width=35%;" placeholder= "Select type" required>
											<option value="local">Local</option>
											<option value="domestic">Domestic</option>
											<option value="international">International</option>
										</select>
									@endif
								</div>
							</div>
							<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Amount( Per Diem )</label>
						    	<div class="input-group col-sm-4">
						      		<input type="number" class="form-control" name="amount" required>
						    	</div>
						  	</div>
							<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Notes</label>
						    	<div class="input-group col-sm-4">
									<textarea class="form-control" rows="5" name="notes"></textarea>
						    	</div>
							</div>
					  	</div>
					</div>
				</div>
			</div>
  	  		<div class="text-center">
		  		<button type="submit" class="btn btn-success">Save</button>
		  	</div>
		</form>
	</div>
</section>
<script type="text/javascript">
	$(function () {
		$('#datetimepicker1').datetimepicker({
			format:'YYYY-MM-DD',
		});
		$('#datetimepicker2').datetimepicker({
			format:'YYYY-MM-DD',
		});
	});
 </script>
@endsection
