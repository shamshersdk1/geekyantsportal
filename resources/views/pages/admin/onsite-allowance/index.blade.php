@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Onsite Allowance</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
		  			<li class="active">Onsite Allowance</li>
        		</ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
				<a href="/admin/onsite-allowance/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Add Onsite Allowance</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
	    	@if(!empty($errors->all()))
	            <div class="alert alert-danger">
	                @foreach ($errors->all() as $error)
	                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                    <span>{{ $error }}</span><br/>
	                  @endforeach
	            </div>
	        @endif
	        @if (session('message'))
	            <div class="alert alert-success">
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ session('message') }}</span><br/>
	            </div>
	        @endif
		  	<div class="user-list-view">
            	<div class="panel panel-default">
            		<table class="table table-striped">
						<tr>
							<th width="5%">#</th>
							<th width="10%">User</th>
							<th width="10%">Start Date</th>
							<th width="10%">End Date</th>
							<th width="10%">Project</th>
							<th width="10%">Status</th>
							<th width="10%">Type</th>
							<th width="10%">Amount</th>
							<th width="10%">Approved By</th>
							<th width="15%">Actions</th>
						</tr>
						@if(count($onsiteAllowanceList) > 0)
							@foreach($onsiteAllowanceList as $onsiteAllowance)
								<tr>
									<td class="td-text">{{$onsiteAllowance->id}}</td>
									<td class="td-text">{{$onsiteAllowance->user ? $onsiteAllowance->user->name : ''}}</td>
									<td class="td-text">{{date_in_view($onsiteAllowance->start_date)}}</td>
									<td class="td-text">{{date_in_view($onsiteAllowance->end_date)}}</td>
									<td class="td-text">{{$onsiteAllowance->project->project_name}}</td>
									<td class="td-text">{{$onsiteAllowance->status}}</td>
									<td class="td-text">{{$onsiteAllowance->type}}</td>
									<td class="td-text">{{!empty($onsiteAllowance->amount) ? $onsiteAllowance->amount : '' }}</td>
									<td class="td-text">{{!empty($onsiteAllowance->approver->name) ? $onsiteAllowance->approver->name : '' }}</td>
									<td class="text-right">
										<a href="/admin/onsite-allowance/{{$onsiteAllowance->id}}" class="btn btn-success crude-btn btn-sm">View</a>
										<a href="/admin/onsite-allowance/{{$onsiteAllowance->id}}/edit" class="btn btn-info btn-sm crude-btn">Edit</a>
										<form action="/admin/onsite-allowance/{{$onsiteAllowance->id}}" method="post" style="display:inline-block;">
											<input name="_method" type="hidden" value="DELETE">
											<button class="btn btn-danger btn-sm crude-btn" type="submit">Delete</button>
										</form>
									</td>
								</tr>
							@endforeach
	            		@else
							<tr>
								<td colspan="10" class="text-center">No results found.</td>
							</tr>
						@endif
					</table>
					<div class="col-md-12">
                    	<div class="pageination pull-right">
                        	<nav aria-label="Page navigation">
                            	<ul class="pagination">
                                	{{ $onsiteAllowanceList->render() }}
                              	</ul>
                        	</nav>
                    	</div>
                	</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
