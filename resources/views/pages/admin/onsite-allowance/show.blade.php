@extends('layouts.admin-dashboard')
@section('main-content')
<section>
   <div class="container-fluid">
      <div class="breadcrumb-wrap">
         <div class="row">
            <div class="col-sm-6">
               <h1>Onsite Allowance</h1>
               <ol class="breadcrumb">
                  <li><a href="/admin">Admin</a></li>
                  <li><a href="/admin/onsite-allowance">Onsite Allowance</a></li>
                  <li class="active">{{$onsiteAllowanceObj->id}}</li>
               </ol>
            </div>
         </div>
      </div>
      @if(!empty($errors->all()))
      <div class="alert alert-danger">
         @foreach ($errors->all() as $error)
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
         <span>{{ $error }}</span><br/>
         @endforeach
      </div>
      @endif
      @if (session('message'))
      <div class="alert alert-success">
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
         <span>{{ session('message') }}</span><br/>
      </div>
      @endif
      <div class="row cms-dev">
         <div class="col-md-12">
            <h4 >Onsite Allowance Detail</h4>
            <div class="panel panel-default">
               <div class="panel-body row">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <div class=" col-md-6">
                     <label>Employee Name : </label>
                     <span>{{$onsiteAllowanceObj->user->name}}</span>
                  </div>
                  <div class=" col-md-6">
                     <label>Start Date : </label>
                     <span>{{date_in_view($onsiteAllowanceObj->start_date)}}</span>
                  </div>
                  <div class=" col-md-6">
                     <label>End Date : </label>
                     <span>{{date_in_view($onsiteAllowanceObj->end_date)}}</span>
                  </div>
                  <div class="col-md-6">
                     <label>Project Name : </label>
                     <span>{{$onsiteAllowanceObj->project->project_name}}</span>
                  </div>
                  <div class="col-md-6">
                     <label>Status : </label>
                     <span>{{$onsiteAllowanceObj->status}}</span>
                  </div>
                  <div class="col-md-6">
                     <label>Type : </label>
                     <span>{{$onsiteAllowanceObj->type}}</span>
                  </div>
                  <div class="col-md-6">
                    <label>Amount : </label>
                    <span>{{$onsiteAllowanceObj->amount}}</span>
                   </div>
                   <div class="col-md-6">
                        <label>Approved By : </label>
                        <span>{{!empty($onsiteAllowanceObj->approver->name) ? $onsiteAllowanceObj->approver->name : '' }}</span>
                    </div>
                    <div class="col-md-6">
                        <label>Notes : </label>
                        <span>{{!empty($onsiteAllowanceObj->notes) ? $onsiteAllowanceObj->notes : '' }}</span>
                    </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<script>
   $(function(){
       toggle = function (item) {
   console.log('toggle1');
   $.ajax({
   type:'POST',
   url:'/api/v1/cms-developers-technology/status',
   data:{id:item.value},
   success:function(data){
   	console.log(data.success);
   	}
   });
   }
   });
   $(function(){
       toggle2 = function (item) {
   console.log('toggle2');
   $.ajax({
   type:'POST',
   url:'/api/v1/cms-slides/status',
   data:{id:item.value},
   success:function(data){
   	console.log(data.success);
   	}
   });
   }
   });
</script>
@endsection