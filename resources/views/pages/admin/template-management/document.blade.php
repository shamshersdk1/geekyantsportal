@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title"></h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/template-management') }}">Template management</a></li>
                    <li class="active">Add</li>
                </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
        </div>
    </div>
    <div ng-app="myApp" ng-controller="myCtrl">
        <div class="row">
            <div class="col-md-6 ">

                <form action="/admin/template-management/download" method="post">
                    <div class="text-center panel panel-default panel-white add-new-tech">
                        <div style="display:inline-block">{{$template->name}}</div>
                        <button class="btn btn-primary" style="float:right" type="submit">Download</button>
                        <input type="hidden" name="content" value="{{$template->content}}">
                    </div>
                </form>
                    <div class="panel panel-default ">
                        <div class="panel-body">
                            @if(isset($template))
                                {{$template->content}}
                            @endif
                        </div>
                    </div>
                
            </div>
            <div class="col-md-6" >
                @if(!empty($errors->all()))
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <span>{{ $error }}</span><br/>
                          @endforeach
                    </div>
                @endif
                @if (session('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ session('message') }}</span><br/>
                    </div>
                @endif
                <form name = "myForm" method="post" action="/admin/template-management" enctype="multipart/form-data">
                    <div class="panel panel-default panel-white add-new-tech">
                        <div class="row">
                            <div class="box-body">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="col-md-12">
                                    <div class="form-group" ng-repeat="content in contents track by $index" >
                                        <input  ng-model="that[content]" class="form-control" placeholder="%%content%%">
                                    </div>
                                    <div id="formd">
                                        
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>
                </form>
                <input type="hidden" name="" id="id" value="{{$template->id}}">
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
@parent
@endsection