@extends('layouts.admin-dashboard')
@section('main-content')
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">View Template</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
				  		<li><a href="{{ url('admin/template-management') }}">Template Management</a></li>
				  		<li class="active">View</li>
		        	</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>
	    <div class="row">
	        <div class="col-md-12">
	            <div class="panel panel-default">
				    <div class="panel-body panel panel-white">
				    	<form name = "myForm"> 
				    		@if(isset($template))
				    			<input type="hidden" name="_token" value="{{ csrf_token() }}">
				    				<div class="name-view text-view"><b>Name :</b>{{$template->name}}</div>

				    			<div class="name-view text-view"><b>Content :</b> {{$template->content}} </div>
				    		@endif
				    	</form>
				    	<!-- <a href="/admin/cms-technology" class="btn btn-primary crud-btn back-btn btn-lg"><i class="fa fa-arrow-left" aria-hidden="true"></i>Back</a> -->
				    </div>
				</div>	
			</div>
		</div>
	</div>
@endsection