@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">List of Templates</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li class="active">Template-Management</li>
                </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <a href="/admin/template-management/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Create Template</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
        @if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span><br/>
                  @endforeach
            </div>
        @endif
        @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
        @endif

            <div class="panel panel-default panel-white">
                <table id="example" class="table table-striped">
                    <thead>
                        <tr>
                            <th class="sorting_asc" width="5%">#</th>
                            <th class="sorting_asc" width="70%">Name of Template</th>
                            <th class="sorting text-right" width="25%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($templates))
                            @if(count($templates) > 0)
                                @foreach($templates as $template)
                                    <tr>
                                        <td>{{$template->id}}</td>
                                        <td>{{$template->name}}</td>
                                        <td class="text-right">
                                            <button onclick="window.location='{{ url("/admin/template-management/$template->id") }}'">View</button>
                                            <button onclick="window.location='{{ url("/admin/template-management/$template->id/edit") }}'">Edit</button>
                                            <form style="display:inline-block" action="/admin/template-management/{{$template->id}}" method= "post" onsubmit="return deletetech()">
                                                <input name="_method" type="hidden" value="DELETE">
                                                <button type="submit">Delete</button>
                                            </form>
                                            <button onclick="window.location='{{ url("/admin/template-management/document/$template->id") }}'">Generate Document</button>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="3" class="text-center">No results found.</td>
                                </tr>
                            @endif
                        @endif
                    </tbody>
                </table>
            </div>
        </div>  
    </div>
</div>

@endsection
@section('js')
@parent
    <script>
        function deletetech() {
            var x = confirm("Are you sure you want to delete?");
            if (x)
                return true;
            else
                return false;
        }
    </script>
@endsection
