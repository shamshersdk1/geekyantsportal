@extends('layouts.admin-dashboard')
@section('main-content')
    <div class="container-fluid">
        <div class="breadcrumb-wrap">
            <div class="row">
                <div class="col-sm-8">
                    <h1 class="admin-page-title">View Technology</h1>
                    <ol class="breadcrumb">
                        <li><a href="/admin">Admin</a></li>
                        <li><a href="{{ url('admin/cms-technology') }}">User</a></li>
                        <li class="active">View</li>
                    </ol>
                </div>
                <div class="col-sm-4 text-right m-t-10">
                    <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
                </div>
            </div>
        </div>
        <div class="row">
             <div class="user-list-view">
                <div class="panel panel-default panel-white">
                    <table id="example" class="table table-striped">
                        <thead>
                            <tr>
                                <th class="sorting_asc" width="5%">#</th>
                                <th class="sorting_asc" width="20%">Key</th>
                                <th class="sorting_asc" width="20%">Amount</th>
                                <th class="sorting text-right" width="25%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($salaryUserData) > 0)
                                @foreach($salaryUserData as $key)
                                    <tr>
                                        <td>{{$key->id}}</td>
                                        <td>{{$key->salaryKey->value}}</td>
                                        <td>{{$key->value}}</td>
                                        <td>
                                            @if($key->type == 'credit')
                                                <span class="label label-primary custom-label">{{$key->type}}</span>
                                            @elseif($key->type == 'debit')
                                                <span class="label label-danger custom-label">{{$key->type}}</span>
                                            @endif
                                        </td>
                                        <td>{{$key->class_name}}</td>
                                        <td class="text-right">
                                            <a href="/admin/cms-technology/{{$key->id}}/edit" class="btn btn-warning btn-sm"><i class="fa fa-pencil btn-icon-space" aria-hidden="true"></i>Edit</a>

                                            <form action="/admin/cms-technology/{{$key->id}}" method="post" style="display:inline-block;" onsubmit="return deletetech()">
                                                <input name="_method" type="hidden" value="DELETE">
                                                <button class="btn btn-danger btn-sm" type="submit"><i class="fa fa-trash btn-icon-space" aria-hidden="true"></i>Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td colspan="3" class="text-center">No results found.</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                    <div class="col-md-12">
                        <div class="pageination pull-right">
                            <nav aria-label="Page navigation">
                                <ul class="pagination">
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
        </div>
        </div>
    </div>
@endsection