@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid resource-report-index">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Project Slack Report</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a>Project Slack Report</a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row user-contact">
        <div class="col-md-12">
            <div class="panel panel-default">
                <table class="table table-striped table-bordered" id="project_slack" width="100%">
                    <thead>
                        <th width="5%;" class="td-center-text">#</th>
                        <th width="25%;" class="td-center-text">Project Name</th>
                        <th width="70%;" class="td-center-text" style="word-wrap: break-word; word-break: break-all; white-space: normal;">Slack Channels</th>
                    </thead>
                    <tbody>
                        @foreach($project_details as $index => $project )
                            <tr>
                                <td class="td-center-text">
                                    {{$index+1}}
                                </td>
                                <td class="td-center-text">
                                    {{$project['name']}}
                                </td>
                                <td class="td-center-text {{ $project['slack_channels'] ? '' : 'free-green-background_td' }}" style="word-wrap: break-word; word-break: break-all; white-space: normal;">
                                    {{!empty($project['slack_channels']) ? $project['slack_channels'] : 'None' }}
                                </td>
                            </tr>
                        @endforeach
                    </tbody> 
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#project_slack').DataTable({
            "pageLength": 100,
            fixedColumns:   {
                leftColumns: 2,
                rightColumns: 1
        }
        });
    } );
</script>
@endsection