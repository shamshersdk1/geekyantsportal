@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Projects</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li class="active">Slack Channel</li>
                </ol>
            </div>
        </div>
    </div>
        <div class="user-list-view">
            <h3>Private Channels</h3>
            <div class="panel panel-default">
                        <table class="table table-striped">
                <tr>
                    <th>Slack Channel ID</th>
                    <th>Channle Name</th>
                </tr>
                @foreach($allPrivateChannels as $private)
                <tr>
                    <td>
                        {{$private->id}}
                    </td>
                    <td>
                        {{$private->name}}
                    </td>
                </tr>
                @endforeach
            </table>
            </div>
        </div>
        <div class="user-list-view">
            <h3>Public Channels</h3>
            <div class="panel panel-default">
                        <table class="table table-striped">
                <tr>
                    <th>Slack Channel ID</th>
                    <th>Channle Name</th>
                </tr>
                @foreach($slackPublicChannels as $public)
                <tr>
                    <td>
                        {{$public->id}}
                    </td>
                    <td>
                        {{$public->name}}
                    </td>
                </tr>
                @endforeach
            </table>
            </div>
        </div>
</div>

@endsection
