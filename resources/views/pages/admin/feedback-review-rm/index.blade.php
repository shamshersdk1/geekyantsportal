@extends('layouts.admin-dashboard')
@section('main-content')
<script type="text/javascript">
    window.user = '<?php echo json_encode($user); ?>';
</script>
<div class="container-fluid resource-report-index" ng-app="myApp" ng-controller="feedbackUserListCtrl">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Feedback Review</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a>Feedback Review</a></li>
                </ol>
            </div>
            <div class="col-sm-4">
                <label style="margin:0;">Month: </label>
                <select class="form-control"
                    ng-model="selectedMonth"
                    ng-change="selectMonth()"
                    ng-options="month.id as month.display_name group by month.group for month in months">
                </select>
            </div>
        </div>
    </div>

    <div class="row">
        <div ng-if="loading" class="loader"></div>
        <div ng-if="!loading" class="col-md-12">
            <h4>List of My Users</h4>
            <div class="user-list-view">
                <div class="panel panel-default">
                    <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="20%">Name</th>
                                    <th width="40%" colspan="2">Percentage Completed</th>
                                    <th class="text-right" width="20%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-show="userList.length > 0" ng-repeat="user in userList">
                                    <td>%%user.id%%</td>
                                    <td>%%user.name%%</td>
                                    <td width="60%">
                                            <div class="progress no-margin">
                                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: %%user.percentage_completed%%% ">
                                                    <span class="sr-only">%%user.percentage_completed%%% Complete (success)</span>
                                                </div>
                                            </div>
                                        </td>
                                    <td><span class="badge badge-default">%%user.percentage_completed%% %</span></td>
                                    <td>
                                        <span class="pull-right">
                                            <button class="btn btn-success crude-btn btn-sm" ng-click="review(user.id)">Review</button>
                                        </span>
                                    </td>
                                </tr>
                                <tr ng-hide="userList.length">
                                    <td colspan="3">
                                        No Records for the chosen month
                                    </td>
                                </tr>
                            </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('.project').select2();
        });
    </script>
    @endsection