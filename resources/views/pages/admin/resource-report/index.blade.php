@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid resource-report-index">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Resource Report</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a>Resource Report</a></li>
                </ol>
            </div>
        </div>
    </div>

<div class="row">
    <div class="col-md-12 bg-white search-section-top">
        <form class="form-group col-md-12 p-0" action="/admin/resource-report" method="get">
            <div class="col-md-2 p-l-0">
                <div class='input-group date' id='datetimepickerStart_search'>
                    <input type='text' id="sd" class="form-control" value="<?=( isset( $_GET['start_date'] ) ? $_GET['start_date'] : '' )?>" autocomplete="off" name="start_date" placeholder="Start Date" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <div class="col-md-2 p-l-0">
                <div class='input-group date' id='datetimepickerEnd_search'>
                    <input type='text' id="ed" class="form-control" name="end_date" value="<?=( isset( $_GET['end_date'] ) ? $_GET['end_date'] : '' )?>" autocomplete="off" placeholder="End Date" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
          <!--  <div class="col-sm-3 ">
                <div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control" value="{{$start_date}}" placeholder="Choose Start Date" id="txtStartDate" name="start_date" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <div class="col-md-3">
                <div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control" value="{{$end_date}}" placeholder="Choose End Date" id="txtEndDate" name="end_date" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div> -->
            <div class="col-md-3 p-l-0">
                <!-- <h4 class="admin-section-heading">Search Users </h4> -->
                <div class="multipicker">
                    <div class="">
			    		<select class="js-example-basic-multiple22 form-control" multiple="multiple" name="users[]" placeholder="Search Users ">
					    	@if(isset($resourceList))
                                @foreach($resourceList as $resource)
                                    <?php
					    				$saved = false;
					    				foreach($selected_users as $selected_user){
					    					if($selected_user == $resource->id){
					    						$saved = true;
					    					}
					    				}
					    			?>
					    			@if(isset($selected_users))
					    				@if($saved)
						    				<option value="{{$resource->id}}" selected >{{$resource->name}}</option>
						    			@else
						    				<option value="{{$resource->id}}">{{$resource->name}}</option>
						    			@endif
						    		@else
						    			<option value="{{$resource->id}}">{{$resource->name}}</option>
						    		@endif
					    		@endforeach
					    	@endif
					    </select>
					</div>
                </div>
		    </div>
            <div class="col-md-3 p-l-0">
                <!-- <h4 class="admin-section-heading">Search Projects </h4> -->
                <div class="multipicker"  >
                    <div class=" ">
			    		<select class="js-example-basic-multiple22 form-control" multiple="multiple" name="input_projects[]" placeholder="Search Projects">
					    	@if(isset($projectList))
                                @foreach($projectList as $project)
                                    <?php
					    				$saved = false;
					    				foreach($selected_projects as $selected_project){
					    					if($selected_project == $project->id){
					    						$saved = true;
					    					}
					    				}
					    			?>
					    			@if(isset($selected_projects))
					    				@if($saved)
						    				<option value="{{$project->id}}" selected >{{$project->project_name}}</option>
						    			@else
						    				<option value="{{$project->id}}">{{$project->project_name}}</option>
						    			@endif
						    		@else
						    			<option value="{{$project->id}}">{{$project->project_name}}</option>
						    		@endif
					    		@endforeach
					    	@endif
                        </select>

                    </div>

                </div>

		    </div>
           <div class="col-md-2" style="padding-top:6px">
                                <button type="submit" class="btn btn-primary btn-sm center" style="margin-left: 2px;">Search</button>

        </div>
            
        </form>
    </div>
    <div class="col-md-5 m-t-15">
        <div class="onleave ">
            <div class="red-small-box inline"><span>On Leave</spnn></div>
            <div class="small-sat-sun-box inline"><span>Holiday</span></div>
            <div class="free-green-background inline"><span>Free</span></div>
            <div class="small-free-project-assigned inline"><span>Free Project Assigned</span></div>
        </div>
  
       
    </div>
    <div class="col-md-7 pull-right">
     <ul class="btn-group " style="padding:0;float:right">
            <a href="/admin/resource-report?@if(strpos(Request::getQueryString(), 'searchuser') !== false)searchuser={{str_replace(' ','+',Request::get('searchuser'))}}@endif" type="button" class="btn btn-default filter-btn @if (!Request::get('filter')&&!Request::get('filter')) btn-success @endif" >All</a>
            <a href="/admin/resource-report?&start_date={{$start_date}}&end_date={{$end_date}}&filter=free" type="button" class="btn btn-default filter-btn @if( preg_replace('/[\s]+.*/','',Request::get('filter')) == 'free')btn-success @endif">Free Resources</a>
        </ul>
        <h4 style="float:right">Filters:&nbsp&nbsp </h4>
    </div>
    
    <div class="col-md-12 pull-right">
        <h6 class="pull-right clearfix" style="clear:both">Note : Report for all the project having status as " In Progress " for billable resource.</h6>
        <h6 class="pull-right" style="clear:both">Total billable resource: {{ count($resourceList) }}</h6>
    </div>
</div>
<div>
<div class="row">
        <div class="col-md-12 user-list-view">
            <div class="panel panel-default table-responsive ">
            <div class="table-wrap-resource">
                <table class="">
                    <thead>
                        <tr>
                            <th  class="td-right-text fixed-side">Name</th>
                            @foreach($data['dates'] as $key => $date)
                                <th  class="td-center-text">{{$date}}</th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data['resource_name'] as $index => $resource_name )
                          <tr>
                                <td class="td-right-text fixed-side">
                                {{ $resource_name }}
                                </td>
                                @for ($i = 0; $i < count($data['resourceDataList']); $i++)
                                    @if ( $data['resourceDataList'][$i][$index]['is_holiday']==1 )
                                        <td class="td-center-text sat-sun-background">
                                            -
                                        </td>
                                    @elseif ( $data['resourceDataList'][$i][$index]['on_leave']==1 )
                                        <td class="td-center-text on-leave-background">
                                            On Leave
                                        </td>
                                    @elseif ( $data['resourceDataList'][$i][$index]['is_free_project']==1 )
                                        <td class="td-center-text free-project-assigned">
                                            {{ $data['resourceDataList'][$i][$index]['projects'] }}
                                        </td>
                                    @else
                                        <td class="td-center-text  {{ $data['resourceDataList'][$i][$index]['projects'] ? '' : 'free-green-background_td' }}">
                                            {{ $data['resourceDataList'][$i][$index]['projects'] ? $data['resourceDataList'][$i][$index]['projects']: 'Free'}}
                                        </td>
                                    @endif
                                @endfor
                            </tr>
                        @endforeach
                    </tbody> 
                </table>
                                    </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function(){
        
        $('#datetimepickerStart_search').datetimepicker({
            minDate: new Date().getMonth() <= 3 ? new Date(new Date().getFullYear()-1+"-04-01") : new Date(new Date().getFullYear()+"-04-01"), //Set min date to April 1
            //Set max date to upcoming march
            maxDate: new Date().getMonth() <= 2 ? new Date(new Date().getFullYear()+"-03-31") : new Date(new Date().getFullYear()+1+"-03-31"),
            format: 'YYYY-MM-DD'
        });

        $('#datetimepickerEnd_search').datetimepicker({
            //Set max date to upcoming march
            maxDate: new Date().getMonth() <= 2 ? new Date(new Date().getFullYear()+"-03-31") : new Date(new Date().getFullYear()+1+"-03-31"),
            format: 'YYYY-MM-DD'
        });

        $("#datetimepickerStart_search").on("dp.change", function (e) {
            $('#datetimepickerEnd_search').data("DateTimePicker").minDate(e.date);
        });

        $("#btnReset").click(function(){
           window.location.href='/admin/resource-report';
    }); 
    });
</script>
@endsection