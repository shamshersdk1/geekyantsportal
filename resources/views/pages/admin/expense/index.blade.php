@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid" ng-app="myApp" ng-controller="expenseIndexCtrl">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Expenses</h1>
                <ol class="breadcrumb">
                <li><a href="/admin">Admin</a></li>
                <li class="active">Expenses</li>
            </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <button class="btn btn-success crude-btn" ng-click="addExpense()"><i class="fa fa-plus fa-fw"></i>Add new Expense</button>
            </div>
        </div>
    </div>
    
    @if(!empty($errors->all()))
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ $error }}</span><br/>
            @endforeach
        </div>
    @endif
    @if (session('message'))
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <span>{{ session('message') }}</span><br/>
        </div>
    @endif

    <div class="">
        <!-- <div class="row bg-white">
            <grid-view model="gridViewModel" columns="gridViewColumns"   actions="gridViewActions"></grid-view>
        </div> -->
        <table datatable="" class="table table-condensed table-hover" dt-options="dtOptions" dt-column-defs="dtColumnDefs" >
            <div class="row bg-white search-section-top m-b">
                <div class="col-sm-6">
                    <label>Payment Status:</label> 
                    <select id="payment_status" multiple style="display: inline-block; width: 200px;">
                        <option value="pending">Pending </option>
                        <option value="paid">Paid </option>
                        <option value="hold">Hold </option>
                        <option value="due_immediate_pending">Due Immediate Pending </option>
                        <option value="due_next_cycle_pending">Due Next Cycle Pending </option>
                        <option value="due_immediate_approved">Due Immediate Approved </option>
                        <option value="due_next_cycle_approved">Due Next Cycle Approved </option>
                    </select>
                </div>
            </div>
            <thead>
                <tr>
                    <th>#</th>
                    <th>Expense Head</th>
                    <th>Amount</th>
                    <th>Expense By</th>
                    <th>Payment Status</th>
                    <th>Payment Method</th>
                    <th>Invoice Number</th>
                    <th>Invoice Date</th>
                    <th>Created by</th>
                    <th class="text-right">Action</th>
                </tr>
            </thead>
            <tbody>
                <tr ng-repeat="(i, expense) in expense.data track by $index">
                    <td>%%expense.id%%</td>
                    <td>%%expense.expense_head.title%%</td>
                    <td>%%expense.amount%%</td>
                    <td>%%expense.expensor.name%%</td>
                    <td>
                        <span ng-if="expense.payment_status=='pending'" class="label label-primary custom-label">%% expense.payment_status | uppercase %%</span>
                        <span ng-if="expense.payment_status=='paid'" class="label label-success custom-label">%% expense.payment_status | uppercase %%</span>
                        <span ng-if="expense.payment_status=='hold'" class="label label-primary custom-label">%% expense.payment_status | uppercase %%</span>
                        <span ng-if="expense.payment_status=='due_next_cycle_approved' || expense.payment_status=='approved'" class="label label-success custom-label">%% expense.payment_status | uppercase %%</span>
                        <span ng-if="expense.payment_status=='due_next_cycle_pending'" class="label label-warning custom-label">%% expense.payment_status | uppercase %%</span>
                        <span ng-if="expense.payment_status=='due_immediate_approved' || expense.payment_status=='due_immediate_pending' " class="label label-danger custom-label">%% expense.payment_status | uppercase %%</span>
                    </td>
                    <td>%%expense.payment_method.name%%</td>
                    <td>%%expense.invoice_number%%</td>
                    <td>%%expense.invoice_date%%</td>
                    <td>
                        <span>%%expense.creator.name ?  expense.creator.name : ""%%</span><br />
                        <small class="text-muted"><i class="fa fa-clock-o"></i> %%expense.created_at | dateToISO%%</small>
                    </td>
                   <!--  <td>
                            <span>%%expense.last_updated ?  expense.last_updated.user.name : ""%%</span><br />
                            <small class="text-muted"><i class="fa fa-clock-o"></i> %%expense.last_updated.created_at%%</small>
                    </td> -->
                    <td class="text-right">
                        <button class="btn btn-sm btn-success" ng-click="viewDetail(expense.id)">
                            <i class="fa fa-eye"></i>
                        </button>
                        <button ng-if="expense.payment_status == 'pending'" class="btn btn-sm btn-warning" ng-click="editDetail(expense.id)">
                            <i class="fa fa-edit"></i>
                        </button>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>



@endsection