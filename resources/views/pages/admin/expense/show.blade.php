@extends('layouts.admin-dashboard')
@section('main-content')
<script type="text/javascript">
  window.expense = <?php echo json_encode($expense); ?>;
</script>
<section class="new-project-section">
   <div class="container-fluid" ng-app="myApp" ng-controller="expenseShowCtrl">
      <div class="breadcrumb-wrap">
         <div class="row">
            <div class="col-sm-8">
               <h1 class="admin-page-title">Expenses</h1>
               <ol class="breadcrumb">
                  <li><a href="/admin">Admin</a></li>
                  <li><a href="{{ url('admin/expense') }}">Expenses</a></li>
                  <li class="active">{{$expense->id}}</li>
               </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
               <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
         </div>
      </div>
      <div ng-if="!loaded" class="loader"></div>
      <div class="row" ng-if="loaded"></div>
         <div class="col-sm-9">
            <div class="row">
               <div class="col-sm-12"  style="margin-bottom:15px">
                  <div ng-if="!readOnly">
                     <button ng-if="expenseObj.payment_status != 'approved'  " class="btn btn-primary" ng-click="changeStatus()" >Change Payment Status</button>
                     <button ng-if="expenseObj.payment_status != 'approved' " class="btn btn-success" ng-click="approveExpense()">Approve</button>
                  </div>
               </div>
            </div>
            <div class="incident-wrap clearfix">
                <div class="row row-sm">
                  <input type="hidden" name="_token" value="9e0EVWte5bQFFg5xYDPqa5cDIxqynpmswqRRgJUv">
                  <div class="col-sm-6">
                     <div class="panel panel-default m-b-5">
                        <div class="info-table">
                           <label>Expense Head: </label> <span> %%expenseObj.expense_head.title%%</span>
                        </div>
                        <div class="info-table">
                           <label>PO: </label>
                           <span ng-if="expenseObj.po_id">%%expenseObj.po_id%% ( %%expenseObj.purchase_order.vendor.name %% ) </span>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="panel panel-default m-b-5">
                        <div class="info-table">
                           <label>Payment Status: </label>
                           <span ng-if="expenseObj.payment_status=='pending'" class="label label-primary custom-label">%% expenseObj.payment_status | uppercase %%</span>
                           <span ng-if="expenseObj.payment_status=='paid'" class="label label-success custom-label">%% expenseObj.payment_status | uppercase %%</span>
                           <span ng-if="expenseObj.payment_status=='hold'" class="label label-primary custom-label">%% expenseObj.payment_status | uppercase %%</span>
                           <span ng-if="expenseObj.payment_status=='due_next_cycle_approved' || expenseObj.payment_status=='approved' " class="label label-success custom-label">%% expenseObj.payment_status | uppercase %%</span>
                           <span ng-if="expenseObj.payment_status=='due_next_cycle_pending'" class="label label-warning custom-label">%% expenseObj.payment_status | uppercase %%</span>
                           <span ng-if="expenseObj.payment_status=='due_immediate_approved' || expenseObj.payment_status=='due_immediate_pending' " class="label label-danger custom-label">%% expenseObj.payment_status | uppercase %%</span>
                        </div>
                        <div class="info-table">
                           <label>Payment Method: </label>
                           <span><small>%%expenseObj.payment_method.name %%</small></span>
                        </div>
                        <div class="info-table">
                            <label>Amount: </label>
                            <span><small>%%expenseObj.amount %%</small></span>
                         </div>
                     </div>
                  </div>
               </div>
               <div class="row row-sm">
                    <div class="col-sm-6">
                        <div class="panel panel-default">
                            <div class="info-table">
                                <label>Invoice Number: </label>
                                <span>%%expenseObj.invoice_number%%</span>
                            </div>
                            <div class="info-table">
                                <label>Invoice Date: </label>
                                <span>
                                    <small>%%expenseObj.invoice_date | dateToISO %%</small>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="panel panel-default">
                            <div class="info-table">
                                <label>Expense By: </label>
                                <span>%%expenseObj.expensor.name%%</span>
                            </div>
                            <div class="info-table">
                                <label>Payment By: </label>
                                <span>
                                    %%expenseObj.payer.name %%
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="panel panel-default">
                            <div class="info-table">
                                <label>Payment Reference Number: </label>
                                <span>%%expenseObj.payment_reference_number%%</span>
                            </div>
                            <div class="info-table">
                                <label>Payment Date: </label>
                                <span>
                                    <small>%%expenseObj.payment_date | dateToISO %%</small>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="panel panel-default ">
                            <google-drive-links reference-id={{$expense->id}} reference-type="App\Models\Admin\Expense"></google-drive-links>
                        </div>
                    </div>
                  <div class="col-sm-6">
                     <div class="panel panel-default">
                        <div class="info-table">
                           <label>Created By: </label>
                           <span>%%expenseObj.creator.name%%</span>
                        </div>
                        <div class="info-table">
                           <label>Created At: </label>
                           <span>
                              <small>%%expenseObj.created_at | dateToISO %%</small>
                           </span>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6" ng-show="expenseObj.last_updated">
                     <div class="panel panel-default">
                        <div class="info-table">
                           <label>Last Updated By: </label>
                           <span>
                              <span>%%expenseObj.last_updated.user.name%%</span>
                           </span>
                        </div>

                        <div class="info-table">
                           <label>Last Updated At: </label>
                           <span>
                               <small>%%expenseObj.last_updated.created_at | dateToISO %%</small>
                           </span>
                        </div>

                     </div>
                  </div>
               </div>
            </div>
            <div class=" row incident-msg ">
               <div class="col-sm-12">
                  <div class="panel panel-default">
                     <div class="panel-body ">
                        <h4 class="no-margin">Other Info</h4>
                        <div class="m-t-10" style="white-space: pre-line">%%expenseObj.other_info%%</div>
                     </div>
                  </div>
                  <div class="incident-comment">
                     <h4 class="admin-section-heading">Comments</h4>
                     <comment-data
                        commentable-id={{$expense->id}}
                        commentable-type="App\Models\Admin\Expense"
                        is-disabled="%%(expenseObj.status == 'closed' || expenseObj.status == 'verified_closed' ) ? true : false %%"
                        is-privatable="false"
                        >
                     </comment-data>
                  </div>
               </div>
            </div>
         </div>
         <activity-log reference-type="App\Models\Admin\Expense" reference-id="expenseObj.id"></activity-log>
         @if(!empty($errors->all()))
         <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <span>{{ $error }}</span><br/>
            @endforeach
         </div>
      </div>
      @endif
      @if (session('message'))
      <div class="alert alert-success">
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
         <span>{{ session('message') }}</span><br/>
      </div>
      @endif
   </div>
</section>


<script>
   $(document).ready(function() {
      $('.department').select2();
      $('.user').select2();
      $('.share-with').select2();
   });
</script>
@endsection