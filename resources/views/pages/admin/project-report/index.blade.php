@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Project Reports</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li><a href="{{ url('admin/project-report') }}">Project Report</a></li>
        		</ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
            	<button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
                <a href="/admin/download/{{$projectinfo->id}}" class="btn btn-success"><i class="fa fa-download fa-fw" aria-hidden="true"></i> Download Report</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
    	@if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span><br/>
                  @endforeach
            </div>
        @endif
        @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
        @endif
			<div class="row">
                <!-- <form action="/admin/show-project-report" method="get">
    				<div class="col-md-6">
    					<div class="form-group">
                            <select class="form-control" name="projectId">
                                <option>option 1</option>   
            					<option>option 2</option>  	
    						</select>
                        </div>
                    </div>
                    <div class="col-md-2  no-padding">
                        <button type="submit" class="btn btn-success" >Search</button>
                    </div>
                 </form> --> 
                 <div class="col-md-12 text-right">
                    
                 </div>
			</div>
            <!-- ============= project details ==================== -->
            <div class="row">
                <div class="col-sm-4">
                    <h4 class="admin-section-heading">Basic Info</h4>
                    <div class="panel panel-default">
                        <!-- <div class="manager-name-holder project-name-holder">
                            {{$projectinfo->project_name}}
                        </div> -->
                        <div class="info-table">
                            <label>Satrt Date: </label>
                            <span>{{$projectinfo->start_date}}</span>
                        </div>
                        <div class="info-table">
                            <label>End Date: </label>
                            <span>{{$projectinfo->end_date}}</span>
                        </div>
                        <div class="info-table">
                            <label>Team Lead: </label>
                            <span>{{$projectinfo->project_manager}}</span>
                        </div>
                        <div class="info-table">
                            <label>Account Manager: </label>
                            <span>{{$projectinfo->account_manager}}</span>
                        </div>
                        <div class="info-table">
                            <label>BD Manager: </label>
                            <span>{{$projectinfo->bd_manager}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <h4 class="admin-section-heading">Client Details</h4>
                    <div class="panel panel-default">
                        <!-- <div class="manager-name-holder project-name-holder">
                            {{$projectinfo->project_name}}
                        </div> -->
                        <div class="info-table">
                            <label>Company Name: </label>
                            <span>{{$companies->name}}</span>
                        </div>
                        <div class="info-table">
                            <label>Email: </label>
                            <span>
                                @if(isset($companies))
                                    {{$companies->email}}
                                @endif
                            </span>
                        </div>
                        <div class="info-table">
                            <label>Primary Contact: </label>
                            <span>
                                @if(isset($contacts))
                                    @foreach($contacts as $contact)
                                        {{$contact->first_name}} {{$contact->last_name}}
                                    @endforeach
                                @endif
                            </span>
                        </div>
                        <div class="info-table">
                            <label>Primary Contact No.: </label>
                            <span>
                                Contact number
                            </span>
                        </div>
                    </div>
                </div>  
                <div class="col-sm-4">
                    <h4 class="admin-section-heading">Technologies</h4>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="tags">
                            @if(!$projectTechnologies->isEmpty())
                                    @foreach($projectTechnologies as $projectTechnology)
                                        <form style="display:inline-block;" >
                                            <a class="label tag-label">{{$projectTechnology->name}} 

                                            <input name="_method" type="submit"  value="" class="input-delete">
                                            </a>
                                            <input name="_method" type="hidden" value="DELETE">
                                        </form>
                                    @endforeach
                                @else 
                                    <h5 class="no-margin">No technology selected</h5>
                                @endif
                                
                            </div>
                        </div>
                    </div>

                    <!--<h4 class="admin-section-heading">Add Technologies</h4>
                    <form action="" method="post">
                        <div class="input-group">
                            <select class="js-example-basic-multiple22 form-control" multiple="multiple" name="technologies[]">
                        
                                <option>option 1</option>
                                        
                                <option>option 2</option>
                                
                                <option>option 3</option>
                                 
                            </select>
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-primary btn-sm" style="margin-left: 2px;"><i class="fa fa-plus" ></i></button>
                            </span>
                        </div>
                    </form>-->
                </div>
            </div>
        <div class="separator-h"></div>
        <!-- =========================== Additional resource ===================== -->
        
        <div class="row">
            <div class="col-md-12 m-t-10">
                <h4 class="admin-section-heading">UnKnown Sprint</h4>
                <div class="panel panel-default">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>User</th>
                                <th>Message</th>
                                <th>Consume Hours</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if(isset($unknownTimelogs))
                            @if(count($unknownTimelogs) > 0)
                                @foreach($unknownTimelogs as $unknownTimelog)
                                    <tr>
                                        <td>{{$unknownTimelog->name}}</td>
                                        <td>{{$unknownTimelog->message}}</td>
                                        <?php
                                            $hours = number_format($unknownTimelog->minutes / 60, 2, '.', ',');
                                        ?>
                                        <td>{{$hours}}</td>
                                        <td>{{$unknownTimelog->current_date}}</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                <td>No Timelog found for Unknown Sprint.</td>
                                </tr>
                            @endif
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
            
        <!-- =========================== sprint view ======================== -->
        @if(isset($data))
            @if(count($data) > 0)
                @foreach($data as $x => $sprint)
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="admin-section-heading">Sprint {{$x}}</h4>
                            <div class="panel panel-default">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>User</th>
                                            <th>Message</th>
                                            <th>Consume Hours</th>
                                            <th>Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        @if(count($sprint) > 0)
                                            @foreach($sprint as $timelog)
                                                @if($timelog->is_pre_Allocated == 1)
                                                    <tr class="unread" style="color:red;">
                                                 @else
                                                    <tr>
                                                 @endif
                                                    <td>{{$timelog->name}}</td>

                                                    <td>{{$timelog->message}} </td>
                                                    <?php
                                                       $hours = number_format($timelog->minutes / 60, 2, '.', ',');
                                                    ?>
                                                    <td>{{$hours}}</td>

                                                    <td>{{$timelog->current_date}}</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="4">No Timelog Found</td>
                                            </tr>
                                        @endif

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>       
                @endforeach
            @else
                <div>No Sprint Found</div>     
            @endif
        @endif
            
		</div>	
	</div>

</div>

@endsection
