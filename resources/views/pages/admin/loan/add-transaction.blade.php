@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Loans</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/loans') }}">Loans</a></li>
                    <li><a href="/admin/loans/{{$loan->id}}">{{$loan->id}}</a></li>
                    <li class="active">Add Transaction</li>
                </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
        @if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span><br/>
                @endforeach
            </div>
        @endif
        @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
        @endif   
        <div class="panel-body" style="padding-bottom:0" >
            <form name = "myForm">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <b>Name of Loanee:</b> {{$loan->user->name}} <br><br>
                <b>Application Date:</b> {{Carbon\Carbon::createFromFormat('Y-m-d',$loan->application_date)->toFormattedDateString()}} <br><br>
                <b>Loan Status: </b>
                @if($loan->status != "completed")
                    <span class="label label-primary custom-label">Running</span>
                @elseif($loan->status == "completed")
                    <span class="label label-success custom-label">Completed</span>
                @endif <br><br>
            </form>
        </div>
            <h4 style="padding-left:15px">Loan Transaction Details</h4 style="padding:15px">
            <form name = "myForm" method="post" action="/admin/loans/{{$loan->id}}/completeLoan" enctype="multipart/form-data" >
                <div class="panel panel-default" style="padding-top: 30px;">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label for="name" class="col-sm-4">Paid On:</label>
                                    <div class="col-sm-8  form-group">
                                        <div class='input-group date' id='paid_on'>
                                            <input type='text' id="pay_date" class="form-control" value="<?=( isset( $_GET['pay_date'] ) ? $_GET['pay_date'] : '' )?>" autocomplete="off" name="pay_date" placeholder="Paid on" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 form-group">
                                    <label for="name" class="col-sm-2">Method:</label>
                                    <div class="col-sm-8  form-group">
                                        <input type="text" name="method" class="form-control" placeholder="Payment Method" required value="{{ old('method') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 form-group">
                                    <label for="name" class="col-sm-2">Amount:</label>
                                    <div class="col-sm-8  form-group">
                                        <input type="text" name="amount" class="form-control" placeholder="Amount Paid" required value="{{ $loan->remaining }}" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 form-group">
                                    <label for="name" class="col-sm-2">Comments:</label>
                                    <div class="col-sm-8  form-group">
                                        <textarea name="comment" id="comment" rows="4" class="form-control" placeholder="Comments...">{{old('comment')}}</textarea>
                                    </div>  
                                </div>
                            </div>
                       </div>
                    </div>
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-success">Complete Loan Transaction</button>
                </div>
            </form>
        </div>
        
    </div>
</div>
<script>
    $(function(){
        $('#paid_on').datetimepicker({format: 'YYYY-MM-DD'});
    });
</script>
@endsection