@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Loans</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/loans') }}">Loans</a></li>
                    <li><a href="/admin/loans/{{$loan_transaction->loan->id}}">{{$loan_transaction->loan->id}}</a></li>
                    <li class="active">Edit / {{$loan_transaction->id}}</li>
                </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
        @if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span><br/>
                @endforeach
            </div>
        @endif
        @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
        @endif   
        <div class="panel-body" style="padding-bottom:0" >
            <form name = "myForm">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <b>Id :</b> {{$loan_transaction->id}} <br><br>
                <b>Name of Loanee:</b> {{$loan_transaction->loan->user->name}} <br><br>
                <b>Status: </b>
                @if($loan_transaction->status != "pending")
                    <span class="label label-success custom-label">Paid</span>
                @elseif($loan_transaction->status == "pending")
                    <span class="label label-info custom-label">Pending</span>
                @endif <br><br>
            </form>
        </div>
            <h4 style="padding-left:15px">Loan Transfer Details</h4 style="padding:15px">
            <form name = "myForm" method="post" action="/admin/loans/{{$loan_transaction->loan_id}}/edit/{{$loan_transaction->id}}" enctype="multipart/form-data" >
                <input name="_method" type="hidden" value="PUT">
                <div class="panel panel-default" style="padding-top: 30px;">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label for="name" class="col-sm-4">Paid On:</label>
                                    <div class="col-sm-8  form-group">
                                        <div class='input-group date' id='paid_on'>
                                            @if(!empty(old('paid_on')))
                                            <input type='text' class="form-control" name="paid_on" value="{{old('paid_on')}}"  placeholder="Payment Date" required/>
                                            @else
                                            <input type='text' class="form-control" name="paid_on" value="{{($loan_transaction->paid_on)}}"  placeholder="Payment Date" required/>
                                            @endif
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 form-group">
                                    <label for="name" class="col-sm-2">Method:</label>
                                    <div class="col-sm-8  form-group">
                                        @if(!empty(old('method')))
                                            <input type="text" name="method" class="form-control" placeholder="Payment Method" required value="{{ old('method') }}">
                                        @else
                                            <input type="text" name="method" class="form-control" placeholder="Payment Method" required value="{{ $loan_transaction->method }}">
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 form-group">
                                    <label for="name" class="col-sm-2">Amount:</label>
                                    <div class="col-sm-8  form-group">
                                        @if(!empty(old('amount')))
                                            <input type="text" name="amount" class="form-control" placeholder="Amount Paid" required value="{{ old('amount') }}">
                                        @else
                                            <input type="text" name="amount" class="form-control" placeholder="Amount Paid" required value="{{ $loan_transaction->amount }}">
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 form-group">
                                    <label for="name" class="col-sm-2">Comments:</label>
                                    <div class="col-sm-8  form-group">
                                        @if(!empty(old('comment')))
                                            <textarea name="comment" id="comment" rows="4" class="form-control" placeholder="Comments...">{{old('comment')}}</textarea>
                                        @else
                                            <textarea name="comment" id="comment" rows="4" class="form-control" placeholder="Comments...">{{$loan_transaction->comment}}</textarea>
                                        @endif
                                    </div>  
                                </div>
                            </div>
                       </div>
                    </div>
                </div>
                <div class="text-center">
                    <button type="reset" class="btn btn-default"><i class="fa fa-rotate-left fa-fw"></i> Clear</button> 
                    <button type="submit" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Save</button>
                </div>
            </form>
        </div>
        
    </div>
</div>
<script>
    $(function(){
        $('#paid_on').datetimepicker({format: 'YYYY-MM-DD'});
    });
</script>
@endsection