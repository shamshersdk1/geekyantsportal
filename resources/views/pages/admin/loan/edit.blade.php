@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Create Loan</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/loans') }}">Loans</a></li>
                    <li class="active">Edit</li>
                </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
        </div>
    </div>
        <div class="row">
            <!-- <ol class="breadcrumb">
                <h2 class="breadcrumb-head-page">Technology Add</h2>
                <li><a href="/admin">Admin</a></li>
                <li><a href="{{ url('admin/cms-technology') }}">Technology</a></li>
                <li><a href="{{ url('admin/cms-technology') }}">Add</a></li>
            </ol> -->
        <div class="col-md-12">
    	@if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span><br/>
                  @endforeach
            </div>
        @endif
        @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
        @endif   
		<form name = "myForm" method="post" action="/admin/loans" enctype="multipart/form-data" >
            <div class="panel panel-default" style="padding-top: 15px;">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        {{method_field("PUT")}}
                        <div class="form-group">
                            <label for="name">Enter Name:</label>
                            <input type="text" id="name" name="name" class="form-control" placeholder="Name" required value="{{ $loan->user->name }}">
                            <p id="name" style="color:red;"></p>
                        </div>
                        <div class="form-group">
                            <label for="name">Enter Amount:</label>
                            <input type="text" name="amount" class="form-control" placeholder="0.00" required value="{{ $loan->amount }}">
                            <p id="name" style="color:red;"></p>
                        </div>
                        <div class="form-group">
                            <label for="name">Enter EMI:</label>
                            <input type="text" name="emi" class="form-control" placeholder="0.00" required value="{{ $loan->emi }}">
                            <p id="name" style="color:red;"></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <button type="reset" class="btn btn-default"><i class="fa fa-rotate-left fa-fw"></i> Clear</button> 
                <button type="submit" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Save</button>
            </div>
		</form>
	</div>
			
	</div>
</div>
<script>
    $(function(){
        $( "#name" ).autocomplete({
            source: {{$users}}
        });
    });
</script>
@endsection