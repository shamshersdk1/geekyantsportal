@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Loans</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/loans') }}">Loans</a></li>
                    <li><a >Ledger</a></li>
                </ol>
            </div>
        </div>
    </div>
        <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                    @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif
            <div class="row">
                <div class="form-group col-md-12 p-0">
                    <div class="col-md-8">
                        <h4>List of Transactions</h4>
                    </div>
                    <div class="col-md-2 pull-right" style="margin-right:4%">
                        <div class="leave-stat">
                            <div class="text">
                                <div class="stat-heading">Balances</div>
                                <div class="stat-info">
                                    <div><b>Total disbursed:  </b>{{number_format($total_disbursed, 2)}}</div>
                                    <div><b>Total repaid:  </b> {{number_format($total_repaid, 2)}}</div>
                                    <div><b>Current balance:  </b> {{number_format($balance, 2)}}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>   
            <div class="panel panel-default">
                <table class="table table-striped">
                    <thead >
	            		<th>#</th>
                        <th>Amount</th>
                        <th>Name</th>
                        <th>Created At</th>
                        <th>Notes</th>
                    </thead>
                        @if(count($loansEmi) > 0)
                            @foreach($loansEmi as $index => $emi)
                                <tr>
                                    <td class="td-text">{{$index+1}}</td>
                                    <td class="td-text">{{number_format($emi->amount, 2)}}</td>
                                    <td class="td-text">{{$emi->loan->user->name}}</td>
                                    <td class="td-text">{{datetime_in_view($emi->created_at)}}</td>
                                    <td class="td-text">{{$emi->method}}</td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="6">No results found.</td>
                            </tr>
                        @endif
                </table>
            </div>
        </div>		
	</div>
</div>
<script>
		$(function(){
		$(".deleteform").submit(function(event){
                return confirm('Are you sure?');
            });
        });
</script>
@endsection