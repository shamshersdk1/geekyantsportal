@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Loans</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/loans/dashboard') }}">Dashboard</a></li>
                    <li><a href="{{ url('admin/loans') }}">Loans</a></li>
                </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <a href="/admin/loans/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i> Create new Loan</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                    @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif   
            <div class="panel panel-default" >
                <table class="table table-striped" id="all-loans-table">
                    <thead >
	            		<th>#</th>
                        <th width="20%">Name of loanee</th>
                        <th>Total Amount</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>Created On</th>
                        <th class="text-right">Actions</th>
                    </thead>
                        @if(count($loans) > 0)
                            @foreach($loans as $index => $loan)
                                <tr>
                                    <td class="td-text"><a>{{$index+1}}</a></td>
                                    <td class="td-text"><a href="/admin/loans/{{$loan->id}}"> {{$loan->user->name}}</a></td>
                                    <td class="td-text"><a>{{$loan->amount}}</a></td>
                                    
                                    <td>{{($loan->appraisal_bonus_id === null ) ? "Against Salary" : $loan->appraisalBonus->appraisalBonusType->description}}</td>
                                    <td>
                                        @if($loan->status != 'completed')
                                            <span class="label label-primary custom-label">Running</span>
                                        @elseif($loan->status == 'completed')
                                            <span class="label label-success custom-label">Completed</span>
                                        @endif
                                    </td>
                                    <td class="td-text"><a>{{datetime_in_view($loan->application_date)}}</a></td>
                                    <td class="text-right">
		    				    		<!-- @if($loan->status == "rejected")
                                            <a disabled class="btn btn-primary crud-btn btn-sm">View</a>
                                            <button class="btn btn-warning crud-btn btn-sm" disabled>Reject</button>
                                        @else -->
                                        
                                        <a href="/admin/loans/{{$loan->id}}" class="btn btn-primary crud-btn btn-sm">View</a>
                                        @if( $loan->remaining == 0 && $loan->status != 'completed' )
                                            <a href="/admin/loans/{{$loan->id}}/complete" class="btn btn-info crude-btn btn-sm">Complete</a>
                                        @endif    
                                            <!-- <form action = "/admin/loans/{{$loan->id}}" method = "post" style="display:inline-block;" class="deleteform">
                                                <input name="_method" type="hidden" value="PUT">
                                                <button class="btn btn-warning crud-btn btn-sm" name="status"  value="reject">Reject</button>
                                            </form>	 -->
                                        <!-- @endif
                                        <form action="/admin/loans/{{$loan->id}}" method="post" style="display:inline-block;" class="deleteform">
                                            <input name="_method" type="hidden" value="DELETE">
                                            <button class="btn btn-danger crud-btn btn-sm" type="submit"><i class="fa fa-trash btn-icon-space" aria-hidden="true"></i>Delete</button>
                                        </form> -->
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="6">No results found.</td>
                            </tr>
                        @endif
                </table>
            </div>
        </div>		
	</div>
</div>
<script>
		$(function(){
            $tableHeight = $( window ).height();
            $(".deleteform").submit(function(event){
                return confirm('Are you sure?');
            });
            $('#all-loans-table').DataTable({
                "pageLength": 500,
                "order": [[ 0, "desc" ]],
                scrollY: $tableHeight - 200,
                scrollX: true,
                scrollCollapse: true,
            });
        });
</script>
@endsection