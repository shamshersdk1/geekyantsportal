@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Create Loan</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/loans') }}">Loans</a></li>
                    <li class="active">Edit</li>
                </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
    	@if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span><br/>
                  @endforeach
            </div>
        @endif
        @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
        @endif   
		<form name = "myForm" method="post" action="/admin/loans/edit/{{$loan->id}}" enctype="multipart/form-data" >
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input name="_method" type="hidden" value="PUT" />
            <div class="panel panel-default" style="padding-top: 15px;">
                <div class="row">
                    <div class="col-sm-12">
                        <label for="name" class="col-sm-2">Enter Loanee's Name:</label>
                        <div class="col-sm-4  form-group">
                            <select id="selectid3" name="user_id" style="width=35%;" placeholder= "Select an option" required>
                                <option value=""></option>
                                    @foreach($users as $x)
                                        @if($loan->user_id == $x->id)
                                            <option value="{{$x->id}}" selected>{{$x->name}}</option>
                                        @else
                                            <option value="{{$x->id}}">{{$x->name}}</option>
                                        @endif    
                                    @endforeach
                            </select>
                        </div>
                        <label for="name" class="col-sm-2">Select Loan Type:</label>
                        <div class="col-sm-4  form-group">
                            <select name="type" id="loan-type-selector" class="form-control" required>
                                @if ( $loan->type == 'annual' )
                                    <option value="annual" selected>Against Annual Bonus</option>
                                    <option value="qtr">Against Quarterly Bonus</option>
                                    <option value="monthly">Monthly(Deducted from salary)</option>
                                @elseif ( $loan->type == 'qtr' )
                                    <option value="annual">Against Annual Bonus</option>
                                    <option value="qtr" selected>Against Quarterly Bonus</option>
                                    <option value="monthly">Monthly(Deducted from salary)</option>
                                @elseif ( $loan->type == 'monthly' )
                                    <option value="annual">Against Annual Bonus</option>
                                    <option value="qtr">Against Quarterly Bonus</option>
                                    <option value="monthly" selected>Monthly(Deducted from salary)</option>
                                @endif
                            </select>
                        </div>
                        <label for="name" class="col-sm-2">Loan Amount:</label>
                        <div class="col-sm-4  form-group">
                            <input type="text" name="amount" class="form-control" placeholder="0.00" required value="{{ $loan->amount }}">
                        </div>
                        <label for="name" class="col-sm-2">Enter EMI:</label>
                        <div class="col-sm-4 form-group">
                            <input type="text" name="emi" class="form-control" placeholder="0.00" required value="{{ $loan->emi }}">
                        </div>
                        <label for="" class="col-sm-2">Loan Application Date:</label>
                        <div class="col-sm-4  form-group">
                            <div class="input-group date datetimepicker">
                                <input type="text" name="application_date" class="form-control" placeholder="Application Date" value="{{$loan->application_date}}" required/>
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <label for="name" class="col-sm-2">Description:</label>
                        <div class="col-sm-4  form-group">
                            <textarea name="description" class="form-control" placeholder="Enter the description" rows="5">{{$loan->description}}</textarea>
                        </div>
                        <label class="col-sm-2">Agreement :</label>
                        <div class="col-sm-4  form-group">
                            <input id="agreement" type="file" name="file" class="form-control" style="border:0">
                            @if($file)
                                <a href="{{$loan->loan_request->file->url()}}" target="_blank" class="btn btn-success btn-sm">View Agreement</a>
                            @endif
                        </div>
                        <label for="payment_detail" class="col-sm-2">Payment Detail: </label>
                        <div class="col-sm-4  form-group">
                            <input type='text' class="form-control" name="payment_detail"  placeholder="Payment Detail" required value="{{$loan->payment_details}}" />
                        </div>
                        <label for="payment_mode" class="col-sm-2">Mode of Payment: </label>
                        <div class="col-sm-4  form-group">
                            <input type='text' class="form-control" name="payment_mode"  placeholder="Mode of payment" required value="{{$loan->payment_mode}}" />
                        </div>
                        <label for="transaction_id" class="col-sm-2">Transaction ID: </label>
                        <div class="col-sm-4  form-group">
                            <input type='text' class="form-control" name="transaction_id"  placeholder="Transaction ID" required value="{{$loan->transaction_id}}" />
                        </div>
                        <label for="payment_date" class="col-sm-2">Date of payment: </label>
                        <div class="col-sm-4  form-group">
                            <div class='input-group date datetimepicker' id='payment_date_datepicker' >
                                <input type='text' id="payment_date" class="form-control" autocomplete="off" name="payment_date"  placeholder="Date of Payment" required value="{{$loan->payment_date}}"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <button type="submit" class="btn btn-success"></i>Update</button>
            </div>
		</form>
        </div>
	</div>
</div>
<script>
    $(function(){
        $('.payment_date_datepicker').datetimepicker({
			format:'YYYY-MM-DD',
			useCurrent:false,
		});
        $('.datetimepicker').datetimepicker({
			format:'YYYY-MM-DD',
			useCurrent:false,
		});
        var old_type = "{{old('type')}}";
        $("#loan-type-selector option[value='"+old_type+"']").prop('selected', true);
    });
</script>
@endsection