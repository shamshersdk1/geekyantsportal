@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Apply Loan</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/loans/dashboard') }}">Dashboard </a></li>
                    <li class="active">Apply</li>
                </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
    	@if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span><br/>
                  @endforeach
            </div>
        @endif
        @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
        @endif   
		<form name = "myForm" id="loan-request-form" method="post" action="/admin/loans" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token()}}">
            <input type="hidden" name="from_admin" value="1">
            <div class="panel panel-default" style="padding:20px; padding-bottom:5px;">
                <div class="row">
                    <div class="col-sm-6">
                        <div class='row'>
                        <label for="name" class="col-sm-4 text-center" >Loanee:</label>
                        <div class="col-sm-8  form-group">
                            <select id="selectid3" name="user_id" style="width=35%;" placeholder= "Select an option" required>
                                <option value=""></option>
                                    @foreach($users as $x)
                                        <option value="{{$x->id}}" >{{$x->name}}</option>
                                    @endforeach
                            </select>
                        </div>
                        @if(!empty($users))
                            
                        @endif
                        </div>
                        <div class="row">    
                           <label for="name" class="col-sm-4 text-center">EMI:</label>
                            <div class="col-sm-8 form-group">
                                <input type="text" id="emi" name="emi" class="form-control" placeholder="0.00" required value="{{ old('emi') }}">
                                <span id="emi-error" style="color:red;display:none"><i class="fa fa-exclamation-circle"></i> Can not be greater than loan amount</span>
                            </div>
                        </div>
                        <div class="row">
                            <label for="application_date" class="col-sm-4 text-center">Application Date</label>
                            <div class="col-sm-8  form-group">
                                <div class="input-group date" id="datetimepicker1" style="width:100%">
                                    <input type="text" name="application_date" class="form-control" required />
                                    <span class="input-group-addon" style="position:relative">
                                    <span class="fa fa-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">    
                            <label for="name" class="col-sm-4 text-center">Reason:</label>
                            <div class="col-sm-8 form-group">
                                <textarea name="description" class="form-control" placeholder="Enter the description" rows="5">@if(!empty(old('description'))){{old('description')}}@endif</textarea>
                            </div>
                        </div>
                        
                        
                    </div>        
                    <div class="col-sm-6">    
                        <div class="row">    
                            <label for="name" class="col-sm-4 text-center">Amount:</label>
                            <div class="col-sm-8  form-group">
                                <input type="text" id="amount" name="amount" class="form-control" placeholder="0.00" required value="{{ old('amount') }}">
                                <div class="request-loan-note">
                                    Maximum loan amount cannot exceed more than 1 Lakh
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">    
                            <label for="name" class="col-sm-4 text-center">Loan Type:</label>
                            <div class="col-sm-8  form-group">
                                <select name="annual_bonus_type_id" id="loan-type-selector" class="form-control" required>
                                    @foreach($loan_types as $loan_type)
                                        <option value="{{$loan_type->id}}">{{$loan_type->description}}</option>
                                    @endforeach
                                    <option value="">Against Salary</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label for="emi_start_date" class="col-sm-4 text-center">Emi Start Date</label>
                            <div class="col-sm-8  form-group">
                                <div class="input-group date" id="datetimepicker2" style="width:100%">
                                    <input type="text" name="emi_start_date" class="form-control" required />
                                    <span class="input-group-addon" style="position:relative">
                                    <span class="fa fa-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
              
                        
                    </div>
                </div>
            </div>
            <div class="text-center">
                <button type="reset" class="btn btn-default"><i class="fa fa-rotate-left fa-fw"></i> Clear</button> 
                <button type="submit" class="btn btn-success" id="submit-btn"><i class="fa fa-plus fa-fw"></i>Save</button>
            </div>
		</form>
        </div>
	</div>
</div>
<script>
    $(function(){
        $('.payment_date_datepicker').datetimepicker({
			format:'DD-MM-YYYY',
			useCurrent:false,
		});
        $('.datetimepicker').datetimepicker({
			format:'DD-MM-YYYY',
			useCurrent:false,
		});
        var old_type = "{{old('type')}}";
        $("#loan-type-selector option[value='"+old_type+"']").prop('selected', true);
    });
        $('#datetimepicker2').datetimepicker({
            format:'YYYY-MM-DD',
            useCurrent: false,
        });
        $('#datetimepicker1').datetimepicker({
            format:'YYYY-MM-DD',
            useCurrent: false,
            maxDate: new Date()
       });
</script>
@endsection