@extends('layouts.admin-dashboard')
@section('main-content')
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">Loans</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
				  		<li><a href="{{ url('admin/loans') }}">Loans</a></li>
				  		<li class="active">{{$loan->id}}</li>
		        	</ol>
	            </div>
	        </div>
		</div>
		@if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span><br/>
                  @endforeach
            </div>
        @endif
        @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
        @endif
	    <div class="row">
			<div class="col-sm-4" style="margin-left:15px;">
				<h4 class="admin-section-heading">Loan Detail</h4>
				<div class="panel panel-default">
					<div class="info-table">
						<label>Employee Name: </label>
						<span>{{$loan->user->name}}</span>
					</div>
					<div class="info-table">
						<label>Loan Application Date: </label>
						<span>{{datetime_in_view($loan->created_at)}}</span>
					</div>
					<div class="info-table">
						<label>Description: </label>
						<span>{{$loan->description}}</span>
					</div>
					<div class="info-table">
						<label>Disbursed On: </label>
						<span>{{date_in_view($loan->application_date)}}</span>
					</div>
					<div class="info-table">
						<label>Loan Type: </label>
						<span>{{($loan->appraisal_bonus_id === null ) ? "Against Salary" : $loan->appraisalBonus->appraisalBonusType->description}}</span>
					</div>
					<div class="info-table">
						<label>Loan Status: </label>
						@if($loan->status != 'completed')
								<span class="label label-primary custom-label">Running</span>
						@elseif($loan->status == 'completed')
								<span class="label label-success custom-label">Completed</span>
						@endif
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<h4 class="admin-section-heading">Loan Type</h4>
				<div class="panel panel-default">
					<div class="info-table">
						<label>Loan: </label>
						<span>{{$loan->appraisal_bonus_id === NULL ? 'Against Salary' : $loan->appraisalBonus->appraisalBonusType->description}}</span>
					</div>
					@if($loan->appraisal_bonus_id === NULL)
					<div class="info-table">
						<label>EMI:</label>
						<span>{{$loan->emi}}</span>
					</div>
					<div class="info-table">
						<label>EMI Start Date: </label>
						<span>{{datemonth_in_view($loan->emi_start_date)}}</span>
					</div>
					
					@else
					<div class="info-table">
						<label>Amount: </label>
						<span>{{$loan->appraisalBonus->value}}</span>
					</div>
					@if($loan->appraisalBonus->value_date)
					<div class="info-table">
						<label>Bonus Due Date: </label>
						<span>{{datemonth_in_view($loan->appraisalBonus->value_date)}}</span>
					</div>
					@endif
					@endif
				</div>
			</div>
			<div class="col-sm-3">
				<h4 class="admin-section-heading">Amount</h4>
				<div class="panel panel-default">
					<div class="info-table">
						<label>Loan Amount: </label>
						<span>{{$loan->amount}}</span>
					</div>
					<div class="info-table">
						<label>Repayed: </label>
						<span>{{$loan->amount-$loan->remaining}}</span>
					</div>
					<div class="info-table">
						<label>Outstanding Amount: </label>
						<span>{{$remainingAmount}}</span>
					</div>
				</div>
			</div>
			<form action="/admin/loans/{{$loan->id}}/complete" method="post" style="display:inline" enctype="multipart/form-data">
			{{method_field('PUT')}}
			<div class="col-sm-2">
				<h4 class="admin-section-heading">Agreement </h4>
				<div>
					@if($loan->loan_request->file)
						<a href="{{$loan->loan_request->file->url()}}" target="_blank" class="btn btn-info btn-sm">View Agreement</a>
					@endif
				</div>
			</div>
			@if ( $loan->status != 'completed'  && $remainingAmount<=0)
				<div class="col-sm-2">
					<h4 class="admin-section-heading">Complete Loan</h4>
					<div>
						<input id="agreement" type="file" name="file" required class="form-control">
					</div>
					<div>
							<button type="submit" name="approve" value="approve" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Upload & Complete Loan</button>
					</div>
				</div>
				@else
					@if($loanCompleteFile)
					<h4 class="admin-section-heading">Closer Agreement </h4>
						<a href="{{$loanCompleteFile->url()}}" target="_blank" class="btn btn-success btn-sm">View Loan Closer Agreement</a>
					@endif
				@endif
			</form>
		</div>

		<div class="row">
			<div class="col-sm-6">
				<div class="panel panel-default">
					<h4 style="padding-left:15px">Loan EMI Transaction</h4 style="padding:15px">
					<table class="table table-striped">
						<thead style="border-top:1px solid lightgray">
							<th width="5%">#</th>
							<th width="15%">Paid On</th>
							<th width="15%">Amount</th>
							<th width="15%">Balance</th>
							<th width="10%">Status</th>
							<th width="20%">Comment</th>
							<th width="20%">Created At</th>
						</thead>
						@if(count($loanTransactions) > 0)
							@foreach($loanTransactions as $index => $loanTransaction)
								<tr>
									<td class="td-text">{{$index+1}}</td>
									<td class="td-text">{{date_in_view($loanTransaction->paid_on)}}</td>
									<td class="td-text">{{$loanTransaction->amount}}</td>
									<td class="td-text">{{$loanTransaction->balance}}</td>
									<td>
										@if($loanTransaction->status == 'paid')
											<span class="label label-success custom-label">Paid</span>
										@elseif($loanTransaction->status == 'pending')
											<span class="label label-warning custom-label">Pending</span>
										@elseif($loanTransaction->status == 'processing')
											<span class="label label-primary custom-label">Processing</span>
										@elseif($loanTransaction->status == 'rejected')
											<span class="label label-danger custom-label">Rejected</span>
										@endif
									</td>
									<td class="td-text">{{$loanTransaction->comment}}</td>
									<td class="td-text">{{datetime_in_view($loanTransaction->created_at)}}</td>
								</tr>
							@endforeach
						@else
							<tr>
								<td colspan="9">No results found.</td>							
							</tr>
						@endif
					</table>
				</div>
			</div>
		</div>
	</div>
	@endsection
<script>
	$(function(){
		$(".deleteform").submit(function(event){
			return confirm('Are you sure?');
		});
	});
</script>
