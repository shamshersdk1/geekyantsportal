<div class="modal fade" id="disburseLoanModal" tabindex="-1" role="dialog" aria-labelledby="disburseLoanModalLabel">
    <form class="disburseFrom" action="/admin/loans/{{$loan->id}}" method="POST">
      <input name="_method" type="hidden" value="PUT">
      <input type="hidden" name="status"  value="disburse">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="addLeaveModalLabel">Disburse Loan Details</h4>
                </div>
                <div class="modal-body">
                    <div class="row form-horizontal">
                        <div class="col-md-12">
                            <div class="form-group">
						    	<label for="payment_detail" class="col-sm-4">Payment Detail: </label>
						    	<div class="col-sm-6">
						      		<input type='text' class="form-control" name="payment_detail"  placeholder="Payment Detail" required />
						    	</div>
						  	</div>
                            <div class="form-group">
						    	<label for="payment_mode" class="col-sm-4">Mode of Payment: </label>
						    	<div class="col-sm-6">
						      		<input type='text' class="form-control" name="payment_mode"  placeholder="Mode of payment" required />
						    	</div>
						  	</div>
                            <div class="form-group">
                                <label for="transaction_id" class="col-sm-4">Transaction ID: </label>
						    	<div class="col-sm-6">
						      		<input type='text' class="form-control" name="transaction_id"  placeholder="Transaction ID" required />
						    	</div>
						  	</div> 
                            <div class="form-group">
                                <label for="payment_date" class="col-sm-4">Date of payment: </label>
                                <div class='input-group date col-sm-6' id='payment_date_datepicker'>
                                    <input type='text' id="payment_date" class="form-control" autocomplete="off" name="payment_date"  placeholder="Date of Payment" required/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" style="margin-top:10px;" class="btn btn-default" data-dismiss="modal">Close</button>&nbsp;
                    <span class="pull-right">
                        <button type="submit" style="margin-top:10px;" class="btn btn-info crude-btn btn-sm">
                            Disburse
                        </button>
                    </span>
                </div>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#payment_date_datepicker').datetimepicker({
            minDate: new Date().getMonth() <= 3 ? new Date(new Date().getFullYear()-1+"-01-01") : new Date(new Date().getFullYear()+"-01-01"), //Set min date to April 1
            //Set max date to upcoming march
            maxDate: new Date().getMonth() <= 2 ? new Date(new Date().getFullYear()+"-12-31") : new Date(new Date().getFullYear()+1+"-12-31"),
            format: 'YYYY-MM-DD'
        });

    })
</script>