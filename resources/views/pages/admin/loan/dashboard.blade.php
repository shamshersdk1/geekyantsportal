@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Loans</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/loans/dashboard') }}">Dashboard</a></li>
                </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                @if(Auth::user()->hasRole('account') || Auth::user()->hasRole('reporting-manager') || Auth::user()->hasRole('human-resources') || Auth::user()->hasRole('admin'))    
                    <a href="/admin/loans" class="btn btn-primary"><i class="fa fa-eye fa-fw"></i> View All Loan</a>
                @endif
                @if(Auth::user()->hasRole('human-resources') || Auth::user()->hasRole('admin'))
                    <a href="/admin/loans/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i> Apply New Loan</a>
                @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                    @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif 
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div id="container" style="width:100%; height:300px;"></div>
        </div>
        <div class="col-md-6">
            <div id="container1" style="width:100%; height:300px;"></div>
        </div>
    
        <div class="col-md-6" style="margin-top: 20px">
            <div id="container2" style="width:100%; height:300px;"></div>
        </div>
        <div class="col-md-6" style="margin-top: 20px">
             
        </div>
    </div>
</div>
    
    
<script>
        $(function () { 
            var myChart1 = Highcharts.chart('container', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Percentage of people having loan'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        }
                    }
                },
                series: [{
                    name: 'Users',
                    colorByPoint: true,
                    data: [{
                        name: 'No Loan',
                        y: {{$total_users_without_loans}}
                    }, {
                        name: 'Loan',
                        y: {{ $total_users_with_loans }} ,
                        selected: true
                    }]
                }]
            });
            var myChart2 = Highcharts.chart('container1', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Disbursed Vs Repaid'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        }
                    }
                },
                series: [{
                    name: 'Amounts',
                    colorByPoint: true,
                    data: [{
                        name: 'Total Disbursed',
                        y: {{ $total_disbursed }}
                    }, {
                        name: 'Total Repaid',
                        y: {{ $total_repaid }},
                        selected: true
                    }]
                }]
            });
            var myChart3 = Highcharts.chart('container2', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Disbursed vs Outstanding'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        }
                    }
                },
                series: [{
                    name: 'Amounts',
                    colorByPoint: true,
                    data: [{
                        name: 'Total Disbursed',
                        y: {{$total_disbursed }}
                    }, {
                        name: 'Outstanding',
                        y: {{ $balance }} ,
                        selected: true
                    }]
                }]
            });
        });
</script>
@endsection