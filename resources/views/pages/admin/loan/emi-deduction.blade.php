@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Loans</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/loans/dashboard') }}">Dashboard</a></li>
                    <li><a href="{{ url('admin/loans') }}">Loans</a></li>
                </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <!-- <a href="/admin/loans/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i> Create new Loan</a> -->
                <button class="btn btn-success btn-sm"> Accept</button>
                <button class="btn btn-danger btn-sm"> Reject</button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                    @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif   
            <div class="panel panel-default">
                <table class="table table-striped">
                    <thead >
	            		<th><input type="checkbox" />All</th>
                        <th width="20%">Name of loanee</th>
                        <th>Total Amount</th>
                        <th>Outstandig Amount</th>
                        <th>EMI</th>
                        <th>Created On</th>
                        <th>Action</th>
                    </thead>
                        @if(count($loans) > 0)
                            @foreach($loans as $index => $loan)
                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td class="td-text"><a href="/admin/loans/{{$loan->id}}"> {{$loan->user->name}}</a></td>
                                    <td class="td-text"><a>{{$loan->amount}}</a></td>
                                    <td class="td-text"><a>{{$loan->remaining}}</a></td>
                                    <td class="td-text"><input type="number" value="{{$loan->emi}}"></td>
                                    <td class="td-text"><a>{{datetime_in_view($loan->application_date)}}</a></td>
                                    <td>
                                        <button class="btn btn-success btn-sm"> Accept</button>
                                        <button class="btn btn-danger btn-sm"> Reject</button>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="6">No results found.</td>
                            </tr>
                        @endif
                </table>
            </div>
            
        </div>		
	</div>
</div>
<script>
		$(function(){
		$(".deleteform").submit(function(event){
                return confirm('Are you sure?');
            });
        });
</script>
@endsection