@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
	<div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Company</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
		  			<li><a href="{{ url('admin/lead') }}">Company</a></li>
        		</ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <a href="/admin/company/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i> Add new Company</a>
            </div>
        </div>
    </div>
    <div class="row">
    	@if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span><br/>
                  @endforeach
            </div>
        @endif
        @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
        @endif
        <div class="col-md-12 user-list-view">
            <div class="panel panel-default panel-white">
            	
	            	<table class="table table-striped">
	            		<th width="10%">#</th>
	            		<th width="20%">Name of company</th>
	            		<th class="text-right">Actions</th>
	            		@if(count($companies) > 0)
		            		@foreach($companies as $company)
			            		<tr>
			            			<td class="td-text"><a>{{$company->id}}</a></td>
			            			<td class="td-text"><a href="/admin/company/{{$company->id}}"> {{$company->name}}</a></td>
			            			<td class="text-right">
		    				    		<a href="/admin/company/{{$company->id}}" class="btn btn-success crud-btn btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View</a>	
		    					    	<a href="/admin/company/{{$company->id}}/edit" class="btn btn-info crud-btn btn-sm"><i class="fa fa-pencil btn-icon-space" aria-hidden="true"></i>Edit</a>
		    					    	<form action="/admin/company/{{$company->id}}" method="post" style="display:inline">
		    					    		<input type="hidden" name="_method" value="DELETE" />
		    					    		<button type="submit" class="btn btn-danger crud-btn btn-sm" onclick="deletetech();"><i class="fa fa-trash btn-icon-space" aria-hidden="true"></i>Delete</button>
		    					    	</form>
			            			</td>
			            		</tr>
		            		@endforeach
		            	@else
		            		<tr>
		            			<td colspan="3">No results found.</td>
		            			<td></td>
		            			<td></td>
		            		</tr>
		            	@endif
	            	</table>
	            	<div class="col-md-12">
                        <div class="pageination pull-right">
                            <nav aria-label="Page navigation">
                                  <ul class="pagination">
                                    {{ $companies->render() }}
                                  </ul>
                            </nav>
                        </div>
                    </div>
            </div>
            <!-- <button class="btn btn-success btn-lg crud-btn" onclick="window.location='{{ url("admin/company/create") }}'"><i class="fa fa-plus btn-icon-space" aria-hidden="true"></i>Add new Company</button> -->
        </div>
    </div>
</div>
@endsection
@section('js')
@parent
	<script>
		
		function deletetech() {
			var x = confirm("Are you sure you want to delete?");
			if (x)
				return true;
			else
				return false;
		}
	</script>
@endsection
