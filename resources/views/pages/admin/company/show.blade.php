@extends('layouts.admin-dashboard')
@section('main-content')
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">{{$company->name}}</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
				  		<li><a href="{{ url('admin/company') }}">Company</a></li>
				  		<li class="active">View </li>
		        	</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
					<a href="/admin/company/{{$company->id}}/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Add new Project</a>
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>
	    <div class="row">
			<div class="col-sm-4">
				<h4 class="admin-section-heading">Basic Info</h4>
				<div class="panel panel-default">
			    	@if($company->name)
			    	<div class="info-table">
						<label>Name: </label>
						<span>{{$company->name}}</span>
					</div>
					@endif
					@if(isset($company->contacts))
						@if(count($company->contacts) > 0)
							<div class="info-table">
								<label>Contact Person: </label>
								<span>
									@foreach($company->contacts as $contact)
				    					@if($contact->primary == 1)
					    					{{$contact->first_name}} {{$contact->last_name}}
					    				@endif
				    				@endforeach
								</span>
							</div>
						@endif
					@endif

					@if($company->name)
					<div class="info-table">
						<label>Email: </label>
						<span>{{$company->email}}</span>
					</div>
					@endif

					@if(($company->work_phone) || ($company->mobile))
					<div class="info-table">
						<label>Contact No.: </label>
						<span>
							{{$company->work_phone}}
							@if(($company->work_phone) && ($company->mobile) )
								<br />
							@endif
							{{$company->mobile}}
						</span>
					</div>
					@endif

					@if($company->website)
					<div class="info-table">
						<label>Website: </label>
						<span>{{$company->website}}</span>
					</div>
					@endif
				</div>
			</div>
			<div class="col-sm-4">
				<h4 class="admin-section-heading">Billing Address</h4>
				<div class="panel panel-default">
					<div class="panel-body">
						@if($company->billing_add_street)
							{{$company->billing_add_street}}<br/>
						@endif
						@if($company->billing_add_state)
							{{$company->billing_add_state}}<br />
						@endif
						@if($company->billing_add_zip_code)
							{{$company->billing_add_zip_code}}<br />
						@endif
						@if($company->billing_add_city)
							{{$company->billing_add_city}}<br />
						@endif
						@if($company->billing_add_country)
							{{$company->billing_add_country}}<br />
						@endif
						@if($company->billing_add_fax)
							<div class="m-t-10">
								<label>Fax:</label> 
								{{$company->billing_add_fax}}
							</div>
						@endif
					</div>
					
				</div>
			</div>
			<div class="col-sm-4">
				<h4 class="admin-section-heading">Shipping Address</h4>
				<div class="panel panel-default">
					<div class="panel-body">
						@if($company->shipping_add_street)
							{{$company->shipping_add_street}}<br />
						@endif
						@if($company->shipping_add_city)
							{{$company->shipping_add_city}}<br />
						@endif
						@if($company->shipping_add_state)
							{{$company->shipping_add_state}}<br />
						@endif
						@if($company->shipping_add_zip_code)
							{{$company->shipping_add_zip_code}}<br />
						@endif
						@if($company->shipping_add_country)
							{{$company->shipping_add_country}}<br />
						@endif
						@if($company->shipping_add_fax)
						<div class="m-t-10">
							<label>Fax: </label>
							<span>{{$company->shipping_add_fax}}</span>
						</div>
						@endif
					</div>
				</div>
			</div>	
		</div>
		<h4>
			List of Projects
		</h4>
		<div class="panel panel-default">
		<table class="table table-striped">
			<thead>
				<th width="15%" class="sorting_asc">#</th>
				<th width="30%" class="sorting_asc">Name of Project</th>
				<th width="30%">Team Lead</th>
				<th width="30%">Status</th>
			</thead>
			<tbody>
			@if(count($projects) > 0)
				@if(isset($projects))
					@foreach($projects as $index => $project)
						<tr>
							<td class="td-text"><a>{{$index+1}}</a></td>
							<td class="td-text"><a href="/admin/project/{{$project->id}}/dashboard"> {{$project->project_name}}</a></td>
							@if(isset($project->projectManager->name))
							<td class="td-text"><a>{{$project->projectManager->name}}</a></td>
							@else
							<td class="td-text"><a> </a></td>
							@endif
							<td class="td-text">
							@if($project->status == 'open')
								<span class="label label-success custom-label">Open</span>
							@elseif ($project->status == 'initial')
								<span class="label label-success custom-label">Initial</span>
							@elseif ($project->status == 'sow')
								<span class="label label-success custom-label">SOW</span>
							@elseif ($project->status == 'nda')
								<span class="label label-success custom-label">NDA</span>
							@elseif ($project->status == 'assigned')
								<span class="label label-success custom-label">Assigned</span>
							@elseif ($project->status == 'in_progress')
								<span class="label label-info custom-label">In progress</span>
							@elseif ($project->status == 'completed')
								<span class="label label-info custom-label">Completed</span>
							@elseif ($project->status == 'closed')
								<span class="label label-danger custom-label">Closed</span>
							@elseif ($project->status == 'free')
								<span class="label label-success custom-label">Free</span>
							@endif
						</td>
						</tr> 
					@endforeach
				@endif
			@else
				<tr>
					<td colspan="3" class="text-cenbter">No results found.</td>
				</tr>
			@endif
			</tbody>
		</table>
		<div class="col-md-12">
			<div class="pageination pull-right">
				<nav aria-label="Page navigation">
						<ul class="pagination">
						{{ $projects->render() }}
						</ul>
				</nav>
			</div>
		</div>
		</div>
	</div>
@endsection