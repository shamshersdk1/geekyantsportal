@extends('layouts.admin-dashboard')
@section('main-content')
<section class="project-dashboard" ng-app="myApp">
<div class="container-fluid">
	<div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">{{$company->name}}</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
			  		<li><a href="{{ url('admin/company') }}">Company</a></li>
			  		<li class="active">Edit {{$company->name}}</li>
	        	</ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
        </div>
    </div>
    <div class="row">
    	<div class="col-md-12">
    		@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
    	</div>
			
        <div class="col-md-12">
			<form name = "myForm" method="post" action="/admin/company/{{$company->id}}" enctype="multipart/form-data">
		    	<div class="row">
		    		<div class="col-md-12">
		    			<div class="panel panel-default">
		    				<div class="panel-body">
		    			 		<input type="hidden" name="_token" value="{{ csrf_token() }}">
			    				<input name="_method" type="hidden" value="PUT">
				    			<div class="row">
				    				<div class="col-md-6">
				    					<div class="form-group">
						    			    <label>Company Name:</label>
						    			    <input type="text" name="name" class="form-control" value="{{$company->name}}">
						    			    <span id="name" style="color:red;"></span>
						    			</div>
						    		</div>
				    				<div class="col-md-6">
				    				@if(count($company->contacts) > 0)
				    					@foreach($company->contacts as $contact)
										@if($contact->primary)
				    					<div class="form-group">
						    			    <label>Contact Person:</label>
						    			    <div class="row">
						    			    	<div class="col-sm-6">
						    			    		<input type="text" name="first_name" class="form-control" value="{{$contact->first_name}}">
						    			    	</div>
						    			    	<div class="col-sm-6">
						    			    		<input type="text" name="last_name" class="form-control" value="{{$contact->last_name}}">
						    			    	</div>
						    			    </div>
						    			</div>
										@endif
						    			@endforeach
						    		@else
    			    					<div class="form-group">
    					    			    <label>Contact Person:</label>
    					    			    <div class="row">
    					    			    	<div class="col-sm-6">
    					    			    		<input type="text" name="first_name" class="form-control" placeholder="First Name">
    					    			    	</div>
    					    			    	<div class="col-sm-6">
    					    			    		<input type="text" name="last_name" class="form-control" placeholder="Last Name">
    					    			    	</div>
    					    			    </div>
    					    			</div>
    					    		@endif
				    				</div>
				    			</div>
				    			<div class="row">
				    			    <div class="col-sm-6">
				    			    	<label>Email:</label>
				    			    	<input type="text" name="email" class="form-control" value="{{$company->email}}">
				    			    </div>
				    			    <div class="col-sm-6">
				    			    	<div class="form-group">
						    			    <label>Website:</label>
						    			    <input type="text" name="website" class="form-control" value="{{$company->website}}">
						    			</div>
				    			    </div>

				    			    <div class="col-sm-6">
				    					<div class="form-group">
						    			    <label>Phone:</label>
						    			    <input type="text" name="work_phone" class="form-control" value="{{$company->work_phone}}">
						    			</div>
						    		</div>
						    		<div class="col-sm-6">
						    			<div class="form-group">
						    				<label>Mobile:</label>
						    			    <input type="text" name="mobile" class="form-control" value="{{$company->mobile}}">
						    			</div>
						    		</div>
						    	</div>
				    		</div>
				    	</div>
				    </div>
				</div>
				<div class="form-horizontal">
					<div class="row">
						<div class="col-sm-12">
							<company-contacts company-id={{$company->id}}></company-contacts>
						</div>
					</div>
				</div>
				<div class="form-horizontal">
						<div class="row">
						<div class="col-sm-6">
					    	<h5>Billing Address</h5>
					    	<div class="panel panel-default">
					    		<div class="panel-body">
					    			<div class="form-group">
					    			    <label class="control-label col-sm-3">Street:</label>
					    			    <div class="col-sm-9">
					    			    	<input type="text" name="billing_add_street" class="form-control" value="{{$company->billing_add_street}}">
					    			    </div>
					    			</div>
					    			<div class="form-group">
					    			    <label class="control-label col-sm-3">City:</label>
					    			    <div class="col-sm-9">
					    			    	<input type="text" name="billing_add_city" class="form-control" value="{{$company->billing_add_city}}">
					    			    </div>
					    			</div>
					    			<div class="form-group">
					    			    <label class="control-label col-sm-3">State:</label>
					    			    <div class="col-sm-9">
					    			    	<input type="text" name="billing_add_state" class="form-control" value="{{$company->billing_add_state}}">
					    			    </div>
					    			</div>
					    			<div class="form-group">
					    			    <label class="control-label col-sm-3">Zip Code::</label>
					    			    <div class="col-sm-9">
					    			    	<input type="text" name="billing_add_zip_code" class="form-control" value="{{$company->billing_add_zip_code}}">
					    			    </div>
					    			</div>
					    			<div class="form-group">
					    			    <label class="control-label col-sm-3">Country:</label>
					    			    <div class="col-sm-9">
					    			    	<input type="text" name="billing_add_country" class="form-control" value="{{$company->billing_add_country}}">
					    			    </div>
					    			</div>
					    			<div class="form-group">
					    			    <label class="control-label col-sm-3">Fax:</label>
					    			    <div class="col-sm-9">
					    			    	<input type="text" name="billing_add_fax" class="form-control" value="{{$company->billing_add_fax}}">
					    			    </div>
					    			</div>
					    		</div>
				    		</div>
				    	</div>
				    	<div class="col-sm-6">
				    		<h5>Shipping Address</h5>
					    	<div class="panel panel-default">
					    		<div class="panel-body">
					    			<div class="form-group">
					    			    <label class="control-label col-sm-3">Street:</label>
					    			    <div class="col-sm-9">
					    			    	<input type="text" name="shipping_add_street" class="form-control" value="{{$company->shipping_add_street}}">
					    			    </div>
					    			</div>
					    			<div class="form-group">
					    			    <label class="control-label col-sm-3">City:</label>
					    			    <div class="col-sm-9">
					    			    	<input type="text" name="shipping_add_city" class="form-control" value="{{$company->shipping_add_city}}">
					    			    </div>
					    			</div>
					    			<div class="form-group">
					    			    <label class="control-label col-sm-3">State:</label>
					    			    <div class="col-sm-9">
					    			    	<input type="text" name="shipping_add_state" class="form-control" value="{{$company->shipping_add_state}}">
					    			    </div>
					    			</div>
					    			<div class="form-group">
					    			    <label class="control-label col-sm-3">Zip Code::</label>
					    			    <div class="col-sm-9">
					    			    	<input type="text" name="shipping_add_zip_code" class="form-control" value="{{$company->shipping_add_zip_code}}">
					    			    </div>
					    			</div>
					    			<div class="form-group">
					    			    <label class="control-label col-sm-3">Country:</label>
					    			    <div class="col-sm-9">
					    			    	<input type="text" name="shipping_add_country" class="form-control" value="{{$company->shipping_add_country}}">
					    			    </div>
					    			</div>
					    			<div class="form-group">
					    			    <label class="control-label col-sm-3">Fax:</label>
					    			    <div class="col-sm-9">
					    			    	<input type="text" name="shipping_add_fax" class="form-control" value="{{$company->shipping_add_fax}}">
					    			    </div>
					    			</div>
					    		</div>
			    			</div>
			    		</div>
			    	</div>
			    </div>
				<div class="text-center">
					<button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-rotate-left fa-fw"></i> Cancel</button>
					<button type="submit" class="btn btn-success">Update</button>
				</div>			
				<!-- <button type="submit" class="btn btn-success">Update</button> -->
			</form>
		</div>
	</div>	
</div>
</section>
@endsection
