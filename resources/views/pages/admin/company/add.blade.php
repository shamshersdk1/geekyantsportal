@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
	<div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Add Company</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<!-- <li><a href="{{ url('admin/lead') }}">Lead Dashboard</a></li> -->
        		  	<li class="active">Add</li>
        		</ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
            	<button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
       		</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
        	@if(!empty($errors->all()))
	            <div class="alert alert-danger">
	                @foreach ($errors->all() as $error)
	                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                    <span>{{ $error }}</span><br/>
	                  @endforeach
	            </div>
	        @endif
	        @if (session('message'))
	            <div class="alert alert-success">
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ session('message') }}</span><br/>
	            </div>
	        @endif
        	
            
			<form name = "myForm" method="post" action="/admin/company" enctype="multipart/form-data" class="form-horizontal">
				<div class="panel panel-default">
            		<div class="panel-body">
				    	<input type="hidden" name="_token" value="{{ csrf_token() }}">
		    			@if(isset($lead->name)) 
    			    		<input type="hidden" name="redirectedFromLead" value="true">
    			    	@endif

    			    	<div class="row">
    			    		<div class="col-md-6">
				    			<div class="form-group">
				    			    <label class="col-sm-12">Company Name:</label>
				    			    <div class="col-sm-12">
				    			    	<input type="text" name="name" class="form-control" placeholder="Name" 
				    			    	@if(isset($lead->name)) 
				    			    		value="{{$lead->name}}"
				    			    	@else 
				    			    		value="{{ old('name') }}"
				    			    	@endif 
					    			    />
					    			    <span id="name" style="color:red;"></span>
					    			</div>
				    			</div>
				    		</div>
				    		<div class="col-md-6">
				    			<div class="form-group">
				    			    <label class="col-sm-12">Contact Person:</label>
				    			    <div class="col-sm-12">
				    			    	<div class="row">
					    			    	<div class="col-sm-6">
						    			    	<input type="text" name="first_name" class="form-control" placeholder="First Name">
						    			    </div>
						    			    <div class="col-sm-6">
						    			    	<input type="text" name="last_name" class="form-control" placeholder="Last Name">
						    			    </div>
						    			</div>
						    		</div>
					    		</div>
				    		</div>
				    		<div class="col-md-6">
				    			<div class="form-group">
				    			    <label class="col-sm-12">Email:</label>
				    			    <div class="col-sm-12">
				    			    	<input type="text" name="email" class="form-control" placeholder="Email"
					    			    @if(isset($lead->email)) 
				    			    		value="{{$lead->email}}"
				    			    	@else 
				    			    		value="{{ old('email') }}"
				    			    	@endif
			    			    		/>
			    			    	</div>
				    			</div>
				    		</div>
				    		<div class="col-md-6">					    			
				    			<div class="form-group">
				    			    <label class="col-sm-12">Website:</label>
				    			    <div class="col-sm-12">
				    			    	<input type="text" name="website" class="form-control" placeholder="website" />
				    			    </div>
				    			</div>
				    		</div>
    			    	</div>  		
						<div class="row">
		    			    <div class="col-md-6">
		    			    	<div class="form-group no-m-b">
		    			    		<label class="col-sm-12">Phone:</label>
			    			    	<div class="col-sm-12">
			    			    		<input type="text" name="work_phone" class="form-control" placeholder="Work Phone">
			    			    	</div>
			    			    </div>
		    			    </div>
		    			    <div class="col-md-6">
		    			    	<div class="form-group no-m-b">
		    			    		<label class="col-sm-12">Mobile:</label>
		    			    		<div class="col-sm-12">
		    			    			<input type="text" name="mobile" class="form-control" placeholder="Mobile">
		    			    		</div>
		    			    	</div>
		    			   	</div>
		    			</div>
				    </div>
				</div>
		    			
				<div class="row">
    				<div class="col-sm-6">
		    			<h5>Billing Address</h5>
		    			<div class="panel panel-default">
		    				<div class="panel-body">
		    					<div class="form-group">
				    			    <label class="control-label col-sm-3">Street:</label>
				    			    <div class="col-sm-9">
				    			    	<input type="text" name="billing_add_street" class="form-control" />
				    			    </div>
				    			</div>
				    			<div class="form-group">
				    			    <label class="control-label col-sm-3">City :</label>
				    			    <div class="col-sm-9">
				    			    	<input type="text" name="billing_add_city" class="form-control" />
				    			    </div>
				    			</div>
				    			<div class="form-group">
				    			    <label class="control-label col-sm-3">State :</label>
				    			    <div class="col-sm-9">
				    			    	<input type="text" name="billing_add_State" class="form-control" />
				    			    </div>
				    			</div>
				    			<div class="form-group">
				    			    <label class="control-label col-sm-3">Zip Code: :</label>
				    			    <div class="col-sm-9">
				    			    	<input type="text" name="billing_add_zip_code" class="form-control" />
				    			    </div>
				    			</div>
				    			<div class="form-group">
				    			    <label class="control-label col-sm-3">Country :</label>
				    			    <div class="col-sm-9">
				    			    	<input type="text" name="billing_add_country" class="form-control" />
				    			    </div>
				    			</div>
				    			<div class="form-group no-m-b">
				    			    <label class="control-label col-sm-3">Fax :</label>
				    			    <div class="col-sm-9">
				    			    	<input type="text" name="billing_add_fax" class="form-control" />
				    			    </div>
				    			</div>
				    		</div>
				    	</div>
	    			</div>

	    			<div class="col-sm-6">
		    			<h5>Shipping Address</h5>
		    			<div class="panel panel-default">
		    				<div class="panel-body">
		    				<div class="form-group">
				    			    <label class="control-label col-sm-3">Street:</label>
				    			    <div class="col-sm-9">
				    			    	<input type="text" name="shipping_add_street" class="form-control" />
				    			    </div>
				    			</div>
				    			<div class="form-group">
				    			    <label class="control-label col-sm-3">City :</label>
				    			    <div class="col-sm-9">
				    			    	<input type="text" name="shipping_add_city" class="form-control" />
				    			    </div>
				    			</div>
				    			<div class="form-group">
				    			    <label class="control-label col-sm-3">State :</label>
				    			    <div class="col-sm-9">
				    			    	<input type="text" name="shipping_add_State" class="form-control" />
				    			    </div>
				    			</div>
				    			<div class="form-group">
				    			    <label class="control-label col-sm-3">Zip Code: :</label>
				    			    <div class="col-sm-9">
				    			    	<input type="text" name="shipping_add_zip_code" class="form-control" />
				    			    </div>
				    			</div>
				    			<div class="form-group">
				    			    <label class="control-label col-sm-3">Country :</label>
				    			    <div class="col-sm-9">
				    			    	<input type="text" name="shipping_add_country" class="form-control" />
				    			    </div>
				    			</div>
				    			<div class="form-group no-m-b">
				    			    <label class="control-label col-sm-3">Fax :</label>
				    			    <div class="col-sm-9">
				    			    	<input type="text" name="shipping_add_fax" class="form-control" />
				    			    </div>
				    			</div>
				    		</div>
				    	</div>
		    		</div>
		    	</div>

				<div class="text-center">
					<button type="reset" class="btn btn-default"><i class="fa fa-rotate-left fa-fw"></i>Clear</button>
					<button type="submit" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Save</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
