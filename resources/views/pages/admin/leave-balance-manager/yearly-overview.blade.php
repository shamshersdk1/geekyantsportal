@extends('layouts.admin-dashboard')
@section('main-content')
    <div class="container-fluid mis-report">
        <div class="breadcrumb-wrap">
           <div class="row">
              <div class="col-sm-8">
                 <h1 class="admin-page-title">Leave Balance Overview</h1>
                 <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="/admin/leave-section/leave-balance-manager">Leave Balance</a></li>
                 <li><a class="active">{{$yearObj->year}}</a></li>
                 </ol>
              </div>
              <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
           </div>
        </div>
        @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif
        <div class="row">
            <div class="col-md-12">
                    <div class="col-md-3" style="padding-top:4px;float:right;">
                    @if($till_date!=null)
                    <a class="btn btn-primary" href="{{$yearObj->id}}/download?till_date={{$till_date}}">
                    @else
                    <a class="btn btn-primary" href="{{$yearObj->id}}/download">
                    @endif
                        <i class="fa fa-download"></i> &nbsp; Download report
                    </a>
                    </div>
                
        </div>
        <!--data starts here-->
        <table class="table table-bordered table-hover bg-white" id="leaves">
          <thead>
              <th>#</th>
              <th>Emp ID</th>
              <th width="180px" class="text-left">Username</th>
              <th class="text-center">PL</th>
              <th class="text-center">SL</th>
              <th class="text-center">Optional</th>
              <th class="text-center">Marriage</th>
              <th class="text-center">Emergency</th>
              <th class="text-center">Paternity</th>
              <th class="text-center">Maternity</th>
              <th class="text-center">Compensatory Off</th>
              <th class="text-center" width="180px">Action(s)</th>
          </thead>
          <tbody>
          @if(($users))
          @foreach($users as $row)
          <tr>
                <td></td>
                <td>{{$row["employee_id"]}}</td>
                <td class="text-left">{{$row["name"]}}</div>
                   
                <td class="text-center">{{!empty($remainingData[$row['id']]['paid']) ? $remainingData[$row['id']]['paid'] : 0}}</td>
                <td class="text-center">{{!empty($remainingData[$row['id']]['sick']) ? $remainingData[$row['id']]['sick'] : 0}}</td>
                <td class="text-center">{{!empty($remainingData[$row['id']]['optional-holiday']) ? $remainingData[$row['id']]['optional-holiday'] : 0}}</td>
                <td class="text-center">{{!empty($remainingData[$row['id']]['marriage']) ? $remainingData[$row['id']]['marriage'] : 0}}</td>
                <td class="text-center">{{!empty($remainingData[$row['id']]['emergency']) ? $remainingData[$row['id']]['emergency'] : 0}}</td>
                <td class="text-center">{{!empty($remainingData[$row['id']]['paternity']) ? $remainingData[$row['id']]['paternity'] : 0}}</td>
                <td class="text-center">{{!empty($remainingData[$row['id']]['maternity']) ? $remainingData[$row['id']]['maternity'] : 0}}</td>
                <td class="text-center">{{!empty($remainingData[$row['id']]['comp-off']) ? $remainingData[$row['id']]['comp-off'] : 0}}</td>
                <td class="text-center">
                    <a class="btn btn-sm btn-success" href="{{$yearObj->id}}/{{$row['id']}}?start=sasa&edn=asdsad">
                        <i class="fa fa-file-o"></i> &nbsp; View Transaction
                    </a>
                </td>


          </tr>
          @endforeach
          @else
          <tr>
           <td class="text-center" colspan="8">No leaves found for this month</td>
          </tr>
          @endif
          </tbody>
        </table>
    </div>
    <script type="text/javascript">
    $(document).ready(function() {
        $tableHeight = $( window ).height();
        var t = $('#leaves').DataTable( {
            pageLength:500,
           scrollY: $tableHeight - 200,
           scrollX: true,
           scrollCollapse: true,
           fixedColumns:   {
               leftColumns: 3
           }
        } );
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
});
        $(function(){
            $('#datetimepickerEnd_search').datetimepicker({
                minDate: '2018-01-01',
                format: 'YYYY-MM-DD',
                useCurrent:false
            });
        });
    </script>
@endsection
