@extends('layouts.admin-dashboard')
@section('main-content')

<section class="new-project-section">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">Leave Balance Manager</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
			  			<li><a href="{{ url('admin/leave-section/leave-balance-manager') }}">Leave Balance Manager</a></li>
			  			<li class="active">Add</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>

	    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif

		<form class="form-horizontal" method="post" action="/admin/leave-section/leave-balance-manager/{{$yearObj->id}}/{{$user->id}}/balance-form/saveForm" enctype="multipart/form-data">
			<div class="panel panel-default">
				<div class="panel-body">
				    <div class="row">
				    	<div class="col-md-12">
						  	<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Employee Name</label>
						    	<div class="col-sm-5">
                                <input type="text" id="name" class="form-control" value="{{$user->name}}" name="name" disabled/>
						    	</div>
						  	</div>
						  	<div class="form-group">
                                <label for="" class="col-sm-2 control-label">Leave Type</label>
                                <div class="col-sm-5">
                                        @if(isset($leaveTypeList))
										<select id="selectid3" name="leave_type_id"  style="width=35%;" placeholder= "Select leave type" required>
											<option value=""></option>
											@foreach($leaveTypeList as $x)
									        	<option value="{{$x->id}}" >{{$x->title}}</option>
										    @endforeach
										</select>
									@endif
                                </div>
                            </div>
                                
						  	<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Days</label>
						    	<div class="col-sm-5">
						    		<input type="number" id="Amount" class="form-control" placeholder="0.00" min="0" name="days" required/>
						    	</div>
						  	</div>
                            <div class="form-group row">
                                <label for="Name" class="col-sm-3 col-md-2 control-label">Type:</label>
                                <div class="col-sm-3 col-md-3" >
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="type"  value="credit" checked>
                                        <label class="form-check-label" for="exampleRadios1">
                                            Credit
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="type"  value="debit">
                                        <label class="form-check-label" for="exampleRadios1">
                                            Debit
                                        </label>
                                    </div>
                                </div>
                            </div>
						  	<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Reason</label>
						    	<div class="col-sm-5">
						      		<textarea class="form-control" rows="5" name="reason"></textarea>
						    	</div>
						  	</div>
					  	</div>
					</div>
				</div>
			</div>
  	  		<div class="text-center">
		  		<button type="reset" class="btn btn-default">Clear</button>
		  		<button type="submit" class="btn btn-success">Save</button>
		  	</div>
		</form>
	</div>
</section>
	<script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datetimepicker({
				format: 'YYYY-MM-DD'
			});
            $('#datetimepicker2').datetimepicker({
				format: 'YYYY-MM-DD'
            });
        });
	</script> 
@endsection