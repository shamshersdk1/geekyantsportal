@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid mis-report">
  <div class="breadcrumb-wrap">
    <div class="row">
      <div class="col-sm-8">
        <h1 class="admin-page-title">{{$user->name}}'s Leave Balance Report</h1>
        <ol class="breadcrumb">
          <li><a href="/admin">Admin</a></li>
          <li><a href="/admin/leave-section/leave-balance-manager">Leave Balance Manager</a></li>
          <li><a href="/admin/leave-section/leave-balance-manager/{{$yearObj->id}}">{{$yearObj->year}}</a></li>
          <li><a class="active"></a>User Balance View</li>
        </ol>
      </div>
    </div>
  </div>
  @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif

  <div class="row mis-overview" >
        <div class="col-md-10" >
            <div class="d-flex justify-content-between align-items-center">
                <div></div>
                <div class="text-center border-right"><small>PL</small><h4>{{$userRemainingLeaveDetail['paid'] ?? 0}}</h4></div>
                <div class="text-center border-right"><small>SL</small><h4>{{$userRemainingLeaveDetail["sick"] ?? 0}}</h4></div>
                <div class="text-center border-right"><small>Optional</small><h4>{{$userRemainingLeaveDetail['optional-holiday'] ?? 0}}</h4></div>
                <div class="text-center border-right"><small>Marriage</small><h4>{{$userRemainingLeaveDetail['marriage'] ?? 0}}</h4></div>
                <div class="text-center border-right"><small>Emergency</small><h4>{{$userRemainingLeaveDetail['emergency'] ?? 0}}</h4></div>
                <div class="text-center border-right"><small>Paternity</small><h4>{{$userRemainingLeaveDetail['paternity'] ?? 0}}</h4></div>
                <div class="text-center border-right"><small>Maternity</small><h4>{{$userRemainingLeaveDetail['maternity'] ?? 0}}</h4></div>
                <div class="text-center border-right"><small>Compensatory Off</small><h4>{{$userRemainingLeaveDetail['comp-off'] ?? 0}}</h4></div>

            </div>
        </div>
        <div class="col-md-2">
        <a class="btn btn-success pull-right" href="{{$user->id}}/balance-form">
                <i class="fa fa-file-o"></i> &nbsp; Add Balance
            </a>
            @if($user->release_date!=null)
        <a class="btn btn-success pull-right" href="{{$user->id}}/settle-leaves">
                <i class="fa fa-file-o"></i> &nbsp; Settle Leaves
            </a>
            @endif
        </div>
        <div class="col-sm-12">
            <br>
            <br>
            <table class="table table-striped bg-white">
                <thead>
                    <tr>
                        <th colspan="2"><strong>Leave Balances</strong></th>
                        <th class="text-right"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>Leave Type</th>
                        <th>No. of Days</th>
                        <th>Type</th>
                        <th>Reason</th>
                        <th>Approved By</th>
                        <th>Created At</th>
                    </tr>
                    @if($leaveBalance)
                        @foreach($leaveBalance as $row)
                            <tr>
                                <td>
                                    @if($row->leaveType->code == 'paid' )
                                        <span class="label label-primary">{{$row->leaveType->title}}</span>
                                    @elseif($row->leaveType->code == 'sick' )
                                        <span class="label label-warning">{{$row->leaveType->title}}</span>
                                    @else
                                        <span class="label label-danger">{{$row->leaveType->title}}</span>
                                    @endif
                                </td>

                                <td>{{$row->days}}</td>

                                <td>
                                    @if($row->type =='credit')
                                        <span class="label label-success">{{$row->type}}</span>
                                    @else
                                        <span class="label label-danger">{{$row->type}}</span>
                                    @endif
                                </td>
                                <td>{{$row->reason}}</td>
                                <td>{{$row->balanced_by}}</td>

                                <td>{{datetime_in_view($row->created_at)}}</td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="4">
                                No leaves balanced for this year
                            </td>
                        </tr>
                    @endif
                </tbody>
            </table>
            <div class="col-sm-4 text-right m-t-10 float-right">
	                <a class="btn btn-sm btn-primary" href="{{$user->id}}/download">
                        <i class="fa fa-download"></i> &nbsp; Download Report
                    </a>
	            </div>
            <table class="table table-striped bg-white">
                <thead>
                    <tr>
                        <th colspan="2"><strong>Leave Transactions</strong></th>
                        <th class="text-right"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>Leave Type</th>
                        <th>No. of Days</th>
                        <th>Description</th>
                        <th>Transaction Date</th>
                    </tr>
                    @if($leaveCredit)
                        @foreach($leaveCredit as $row)
                            <tr>
                                <td>
                                    @if($row->leaveType->code == 'paid' )
                                    <span class="label label-primary">{{$row->leaveType->title}}</span>
                                    @elseif($row->leaveType->code == 'sick' )
                                    <span class="label label-warning">{{$row->leaveType->title}}</span>
                                    @else
                                    <span class="label label-danger">{{$row->leaveType->title}}</span>
                                    @endif
                                </td>

                                <td>{{$row->days}}</td>

                                @if($row->reference_type=="App\Models\Leave\Leave")
                                    <td>
                                        <small>{{date_to_ddmmyyyy($row->reference->start_date)}} to {{date_to_ddmmyyyy($row->reference->end_date)}}</small>
                                        &nbsp;
                                        @if($row->reference->status == "approved")
                                            <span class="label label-success">{{$row->reference->status}}</span>
                                        @elseif($row->reference->status == "pending")
                                            <span class="label label-warning">{{$row->reference->status}}</span>
                                        @else
                                                <span class="label label-danger">{{$row->reference->status}}</span>

                                    @endif
                                    </td>
                                @elseif($row->reference_type=="App\Models\Leave\LeaveCalendarYear")
                                    <td>Yearly Credit (Pro Rata Basis)</td>
                                @elseif($row->reference_type=="App\Models\Admin\LeaveBalanceManager")
                                <td>Reason : <small>{{$row->leaveBalance->reason}}</small></td>
                                @else
                                    <td>{{$row->reference_type}}</td>
                                @endif
                                <td>{{datetime_in_view($row->created_at)}}</td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="4">
                                No leaves credited for this year
                            </td>
                        </tr>
                    @endif
                </tbody>
            </table>

        </div>
    </div>
</div>
@endsection
