@extends('layouts.admin-dashboard')
@section('main-content')

<section class="new-project-section">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">Ip Address Ranges</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
                        <li><a href="{{ url('admin/ip-address-range') }}">Ip address ranges</a></li>
			  			<li class="active">Add</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>

	    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif
		<form class="form-horizontal" method="post" action="/admin/ip-address-range" enctype="multipart/form-data">
		    <div class="row">
                <div class="col-sm-6 col-sm-offset-3 m-t-15">
                	<div class="panel panel-default">
						<div class="panel-body">
                            <div class="form-group">
						    	<label for="" class="col-sm-3 control-label"> Name*</label>
						    	<div class="col-sm-8">
						    		<input type="text" id="Name" class="form-control" placeholder="Name" name="name"  value="{{ old('name')}}" required/>
						    	</div>
						  	</div>
                            <div class="form-group">
						    	<label for="" class="col-sm-3 control-label">Start Ip*</label>
						    	<div class="col-sm-8">
						    		<input type="text" id="Start_ip" class="form-control" placeholder="0.0.0.0" 
									    name="start_ip"  value="{{ old('start_ip') }}" required/>
						    	</div>
						  	</div>
                            <div class="form-group">
						    	<label for="" class="col-sm-3 control-label">End Ip*</label>
						    	<div class="col-sm-8">
						    		<input type="text" id="End_ip" class="form-control" placeholder="0.0.0.0" 
									   name="end_ip"  value="{{ old('end_ip') }}" required/>
						    	</div>
						  	</div>
                            <div class="row">
						    	<label for="" class="col-sm-3 control-label">Net Mask</label>
						    	<div class="col-sm-8">
						    		<input type="text" id="Net_mask" class="form-control" placeholder="0.0.0.0" 
									  name="net_mask"  value="{{ old('net_mask') }}"/>
						    	</div>
						  	</div>
                        </div>
					</div>
				</div>
			</div>
  	  		<div class="text-center">
		  		<button type="reset" class="btn btn-default">Clear</button>
		  		<button type="submit" class="btn btn-success">Save</button>
		  	</div>
		</form>
	</div>
</section>
@endsection