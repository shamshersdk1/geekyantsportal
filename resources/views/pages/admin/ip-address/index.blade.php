@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Ip Address Ranges</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/ip-address-range') }}">Ip address ranges</a></li>
                </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <a href="/admin/ip-address-range/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i> Add</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
    	@if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span><br/>
                  @endforeach
            </div>
        @endif
        @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
        @endif  
    <div class="user-list-view">
        <div class="panel panel-default"> 
            <table class="table table-striped">
                <thead>
                    <th>#</th>
                    <th>Name</th>
                    <th>Start Ip</th>
                    <th>End Ip</th>
                    <th>Net Mask</th>
                    <th>Company Asset</th>
                    <th class="text-right">Actions</th>
                </thead>
                @if(count($IpAddressRange) > 0)
                    @foreach($IpAddressRange as $index => $ip)
                        <tr>
                            <td>{{$index+1}}</td>
                            <td>{{$ip->name}}</td>
                            <td>{{$ip->start_ip}}</td>
                            <td>{{$ip->end_ip}}</td>
                            <td>{{$ip->net_mask_ip}}</td>
                            <td>
                            	<div class="col-md-4">
									<div class="onoffswitch">
										<input type="checkbox" value="{{$ip->id}}" name="onoffswitch[]" onchange="toggle(this)"
											class="onoffswitch-checkbox" id="{{$ip->id}}"
										<?php echo (!empty($ip->company_asset)&&$ip->company_asset==1) ? 'checked': ''; ?>>
                                        <label class="onoffswitch-label" for= {{$ip->id}} >
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
									</div>
								</div>

                            </td>
                            <td class="text-right">
                                <a href="/admin/ip-address-range/{{$ip->id}}/edit" class="btn btn-primary  btn-sm">Modify</a>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="6">No Records</td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
        @if(!empty($IpAddressRange))
            {{$IpAddressRange->links()}}
        @endif
	</div>
</div>
<script>
    $(function(){
        toggle = function (item) {
            console.log(item, "data.success");
			//window.location.href='users-billable/'+item.value;
			$.ajax({
				type:'POST',
				url:'/api/v1/ip-address-ranges/company-asset',
				data:{id:item.value},
				success:function(data){
					console.log(data.success);
					}
			});
		}
    });
</script>
@endsection
