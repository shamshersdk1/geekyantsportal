@extends('layouts.admin-dashboard')
@section('main-content')

<section class="new-project-section">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">Ip Address Ranges</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
                        <li><a href="{{ url('admin/ip-address-range') }}">Ip address ranges</a></li>
			  			<li class="active">Edit</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>

	    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif
		<form class="form-horizontal" method="post" action="/admin/ip-address-range/{{$IpAddressRange->id}}" enctype="multipart/form-data">
			{{ method_field('PUT') }}
			<div class="row">
                <div class="col-sm-6"> 
			 		@if($usedIps->count() > 0 ) 
					<table class="table table-bordered table-condensed bg-white">
						<tr class="table-dark">
							<th>User name</th>
							<th class="text-center">Ip Address</th>
							<th class="text-right">Assigned By</th>
						</tr>
						@foreach ($usedIps as $ipUser)
						<tr>
							<td>{{$ipUser->user->name}}</td>
							<td class="text-center">{{$ipUser->ip_address}}</td>
							<td class="text-right">{{$ipUser->assignee->name}}</td>
						</tr>					
						@endforeach
					</table>
					@endif
				</div>
				<div class="col-sm-6">
                    <div class="panel panel-default">
						<div class="panel-body">
                            <div class="form-group">
						    	<label for="" class="col-sm-3 control-label"> Name*</label>
						    	<div class="col-sm-8">
									@if($usedIps->count() > 0 ) 
										<input type="text" id="Name" class="form-control" placeholder="Name" name="name"  value="{{ $IpAddressRange->name}}" disabled/>	
									@else <input type="text" id="Name" class="form-control" placeholder="Name" name="name"  value="{{ $IpAddressRange->name}}" required/>
									@endif
						    	</div>
						  	</div>
                            <div class="form-group">
						    	<label for="" class="col-sm-3 control-label">Start Ip*</label>
						    	<div class="col-sm-8">
									@if($usedIps->count() > 0 ) 
										<input type="text" id="Start_ip" class="form-control" placeholder="0.0.0.0" name="start_ip"  value="{{ $IpAddressRange->start_ip}}"  disabled/>
									@else <input type="text" id="Start_ip" class="form-control" placeholder="0.0.0.0" name="start_ip"  value="{{ $IpAddressRange->start_ip}}"  required/>
									@endif
						    		
						    	</div>
						  	</div>
                            <div class="form-group">
						    	<label for="" class="col-sm-3 control-label">End Ip*</label>
						    	<div class="col-sm-8">
									@if($usedIps->count() > 0 ) 
										<input type="text" id="End_ip" class="form-control" placeholder="0.0.0.0" name="end_ip"  value="{{ $IpAddressRange->end_ip}}" disabled/>
									@else <input type="text" id="End_ip" class="form-control" placeholder="0.0.0.0" name="end_ip"  value="{{ $IpAddressRange->end_ip}}" required/>
									@endif
						    	</div>
						  	</div>
                            <div class="row">
						    	<label for="" class="col-sm-3 control-label">Net Mask</label>
						    	<div class="col-sm-8">
									@if($usedIps->count() > 0 ) 
										<input type="text" id="Net_mask" class="form-control" placeholder="0.0.0.0" name="net_mask"  value="{{ $IpAddressRange->net_mask_ip}}" disabled />
									@else <input type="text" id="Net_mask" class="form-control" placeholder="0.0.0.0" name="net_mask"  value="{{ $IpAddressRange->net_mask_ip}}" />
									@endif
						    	</div>
						  	</div>
                        </div>
                    </div>
                    <div class="text-right">
						@if($usedIps->count() == 0 ) 
				  		<button type="submit" class="btn btn-success">Save</button>
						@else 
						<form action="/admin/ip-address-range/{{$IpAddressRange->id}}" method="post" style="display:inline-block;" onsubmit="return confirm('Deleting this range will release all the ips assgined with this range. Are you sure you want to delete this range?');">
							{{ method_field('DELETE') }}
						<button class="btn btn-danger  btn-sm" type="submit"><i class="fa fa-trash btn-icon-space" aria-hidden="true"></i>Delete</button>
						</form>
						@endif
				  	</div>
                </div>
			</div>
		</form>
	</div>
</section>
@endsection