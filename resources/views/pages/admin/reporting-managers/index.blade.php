@extends('layouts.admin-dashboard')
    @section('main-content')
    <div class="container-fluid">
        <div class="breadcrumb-wrap">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="admin-page-title">Reporting Managers</h1>
                    <ol class="breadcrumb">
                        <li><a href="/admin">Admin</a></li>
                        <li class="active">Reporting Managers </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="row row-sm reprting-managers">
            @if(count($managers) > 0)
	            @foreach($managers as $manager)
                    <div class="col-md-4">
                        <div class="panel panel-default m-b-10">
                            <div class="panel-heading  bg-white">{{$manager->name}}</div>
                                @if ( count($manager->reportees) > 0 )
                                    <div class="panel-body">
                                        @foreach( $manager->reportees as $reportee )
                                            <label class="label">{{ $reportee->name }}</label>
                                        @endforeach
                                    </div>
                                @endif
                            </div>
                        </div>
                @endforeach
            @endif
        </div>
    </div>
@endsection