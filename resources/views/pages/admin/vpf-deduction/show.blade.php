@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-6">
                <h1 class="admin-page-title">VPF Deduction</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li><a href="{{ url('admin/vpf-deduction') }}">VPF Deduction</a></li>
        		  	<li>{{$month->formatMonth()}}</li>
        		</ol>
            </div>
            <div class="col-sm-6">
                
                @if(!$month->vpfSetting || ($month->vpfSetting &&  $month->vpfSetting->value == 'open')) 
                    <div class="pull-right m-t-10">
                        <span>
                            <a href="/admin/vpf-deduction/{{$month->id}}/regenerate" class="btn btn-warning"><i class="fa fa-gear fa-fw"></i>Regenerate</a>                  
                        </span>
                    </div>
                    <div class="pull-right m-t-10">
                        <span>
                            <a href="/admin/vpf-deduction/{{$month->id}}/status-month" class="btn btn-danger"><i class="fa fa-lock fa-fw"></i>Lock</a>                  
                        </span>
                    </div>
                @elseif(($month->getMonth(date("Y/m/d")) == $month->id ) && $month->vpfSetting && $month->vpfSetting->value == 'locked')
                    <div class="pull-right m-t-10">
                        <span>
                            <a href="/admin/vpf-deduction/{{$month->id}}/status-month" class="btn btn-success"><i class="fa fa-unlock fa-fw"></i>UnLock</a>
                        </span>
                    </div>
                @endif
			</div>
		</div>
    </div>
    @if($month->vpfSetting &&  $month->vpfSetting->value == 'locked') 
        <div class="pull pull-right">
            <span class="label label-primary">Locked at {{datetime_in_view($month->vpfSetting->created_at)}}</span>
        </div>
    @endif
	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
    </div>
    
    <form name="editForm" method="post" action="/admin/vpf-deduction/{{$month->id}}/update">
        @if(!$month->vpfSetting ||  $month->vpfSetting->value == 'open') 
                <div class="row">
                    <div class="pull-right m-t-1 ">
                        <button type="submit" class="btn btn-success "><i class="fa fa-plus fa-fw"></i>Save</button> 
                    </div>
                </div>   
        @endif
        <div class="user-list-view">
            <div class="panel panel-default">
                <table class="table table-striped" id="deduction-table">
                    <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th class="text-center">Employee Id</th>
                            <th class="text-center">Name</th>
                            <th class="text-center">VPF Amount</th>
                            <th class="text-center">Deduct Amount</th>
                            <th class="text-center">Created At</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($vpfDeductions) > 0)
                            @foreach($vpfDeductions as $vpfDeduction)
                                <tr>
                                    <td class="text-center"></td>
                                    <td class="text-center">{{ $vpfDeduction->user->employee_id}}</td>
                                    <td class="text-center">{{ $vpfDeduction->user->name}}</td>
                                    <td class="text-center">{{ $vpfDeduction->vpf->amount}}</td>
                                    <td class="text-center">
                                        @if($month->vpfSetting && $month->vpfSetting->value == 'locked')
                                            {{$vpfDeduction->amount}}
                                        @else
                                            <input name="new_amount[{{$vpfDeduction->id}}]" type="number" value="{{$vpfDeduction->amount}}" max="0"/>
                                        @endif
                                    </td>
                                    <td class="text-center">{{ datetime_in_view($vpfDeduction->created_at)}}</td>
                                    <td class="pull-right">
                                        @if(!$month->vpfSetting || $month->vpfSetting->value == 'open')
                                        <a href="/admin/vpf-deduction/{{$vpfDeduction->id}}/delete" onClick="return confirm('Delete VPF Deduction?')" class="btn btn-danger btn-sm crude-btn"><i class="fa fa-trash fa-fw"></i>Delete</a> 
                                        
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @else 
                            <tr>
                                <td colspan="5">
                                    <td class="text-center">No Records found</td>
                                </td>
                            </tr>

                        @endif  
                    </tbody>
                </table>
            </div>
        </div>
    </form>
</div>	
<script>
    $(document).ready(function() {
        var t = $('#deduction-table').DataTable( {
            pageLength:500, 
            "columnDefs": [ {
                "searchable": false,
                "orderable": false,
                "targets": 0
            } ],
            "order": [[ 1, 'asc' ]],
            fixedHeader: true
        } );
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    });
</script>
@endsection

