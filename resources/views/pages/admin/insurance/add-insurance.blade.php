@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid" >
    <div class="breadcrumb-wrap">
      <div class="row">
      <div class="col-sm-8">
        <h1 class="admin-page-title">Employees Insurance</h1>
        <ol class="breadcrumb">
          <li><a href="/admin">Admin</a></li>
          <li class="active">Insurance</li>
        </ol>
      </div>
    </div>
  </div>
  <div class="m-t-15">
    <form class="form-horizontal" method="post" action="/admin/insurance" enctype="multipart/form-data">
      <div class="row">
        <div class="col-lg-10 col-lg-offset-1">
          <div class="panel panel-default">
            <div class="panel-body">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <div class="col-sm-6">
                        <label>Vendor Name</label>
                      <input type="text" name="" class="form-control">
                    </div>
                    <div class="col-sm-6">
                        <label>Insurance Name</label>
                        <input type="text" name="" class="form-control">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-6">
                        <label>Policy Number</label>
                        <input type="text" name="" class="form-control">
                    </div>
                    <div class="col-sm-6">
                        <label>Insurance Type</label>
                        <select class="form-control">
                          <option>Health Insurance</option>
                          <option>LIC</option>
                          <option>Asset Isurance</option>
                          <option>Contract Isurance</option>
                        </select>
                    </div>
                    
                  </div>
                  <div class="form-group">
                    <div class="col-sm-6">
                        <div class="row row-sm">
                          <div class="col-sm-6">
                            <label>Coverage Amount</label>
                            <input type="text" name="" class="form-control">
                          </div>
                          <div class="col-sm-6">
                            <label>Terms</label>
                            <input type="text" class="form-control" name="subject" value="">
                          </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <label>Accessible for</label>
                        <select class="form-control">
                          <option value="group" name="insurance_accessibility">Group</option>
                          <option value="individual" name="insurance_accessibility">Individual</option>
                        </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-6">
                        <label>Duration</label>
                        <div class="row">
                          <div class="col-sm-5">
                            <input type="text" class="form-control" name="subject" value="">
                          </div>
                          <div class="col-sm-2 text-center m-t-5"> - To - </div>
                          <div class="col-sm-5">
                            <input type="text" class="form-control" name="subject" value="">
                          </div>
                        </div>
                    </div>

                    <!-- Show incase of individual 
                    <div class="col-sm-6">
                        <label>User</label>
                        <input type="text" class="form-control" name="subject" value="">
                    </div> -->

                    <div class="col-sm-6">
                        <div class="row row-sm">
                          <div class="col-sm-4">
                            <label>Age Group</label>
                            <div class="row row-sm">
                              <div class="col-sm-6">
                                <input type="text" class="form-control" name="subject" value="">
                              </div>
                              <div class="col-sm-6">
                                  <input type="text" class="form-control" name="subject" value="">
                              </div>
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <label>Amount <small>(Yearly)</small></label>
                            <input type="text" class="form-control" name="subject" value="" >
                          </div>
                          <div class="col-sm-2 text-right">
                            <label>Monthly</label>
                            <div><big>350 /-</big></div>
                          </div>
                          <div class="col-sm-2 text-right">
                            <div class="m-t-15"><a href="" class="btn btn-success"><i class="fa fa-plus"></i></a></div>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="text-center">
        <button type="submit" class="btn btn-success">Add Insurance</button>
      </div>
    </form>
  </div>
</div>
@endsection