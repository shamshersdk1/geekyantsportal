@extends('layouts.admin-dashboard')
@section('main-content')

<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-6">
                <h1 class="admin-page-title">Insurance Deduction</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li><a href="{{ url('admin/insurance-deduction') }}">Insurance Deduction</a></li>
        		  	<li>{{$month->formatMonth()}}</li>
        		</ol>
            </div>
            <div class="col-sm-6">
                
                @if(!$month->insuranceSetting || ($month->insuranceSetting &&  $month->insuranceSetting->value == 'open')) 
                    <div class="pull-right m-t-10">
                        <span>
                            <a href="/admin/insurance-deduction/{{$month->id}}/regenerate" class="btn btn-warning"><i class="fa fa-gear fa-fw"></i>Regenerate</a>                  
                        </span>
                    </div>
                    <div class="pull-right m-t-10">
                        <span>
                            <a href="/admin/insurance-deduction/{{$month->id}}/status-month" class="btn btn-danger"><i class="fa fa-lock fa-fw"></i>Lock</a>                  
                        </span>
                    </div>
                @elseif(($month->getMonth(date("Y/m/d")) == $month->id ) && $month->insuranceSetting && $month->insuranceSetting->value == 'locked')
                    <div class="pull-right m-t-10">
                        <span>
                            <a href="/admin/insurance-deduction/{{$month->id}}/status-month" class="btn btn-success"><i class="fa fa-unlock fa-fw"></i>UnLock</a>
                        </span>
                    </div>
                @endif
			</div>
		</div>
    </div>
    @if($month->insuranceSetting &&  $month->insuranceSetting->value == 'locked') 
        <div class="pull pull-right">
            <span class="label label-primary">Locked at {{datetime_in_view($month->insuranceSetting->created_at)}}</span>
        </div>
    @endif
	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
    </div>
    
    <form name="editForm" method="post" action="/admin/insurance-deduction/{{$month->id}}/update">
        @if(!$month->insuranceSetting ||  $month->insuranceSetting->value == 'open') 
                  
        @endif
        <div class="user-list-view">
            <div class="panel panel-default">
                <table class="table table-striped" id="deduction-table">
                    <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th class="text-left">Employee Id</th>
                            <th class="text-left">User Name</th>
                            <th class="text-left">Insurance Name</th>
                            <th class="text-left">Coverage</th>
                            <th class="text-left">Premium (p/m)</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($insuranceDeductions) > 0)
                            @foreach($insuranceDeductions as $insuranceDeduction)
                                <tr @if($insuranceDeduction->user->is_active == 0) class="danger" @endif>
                                    <td class="text-center"></td>
                                    <td class="text-left">{{ $insuranceDeduction->user->employee_id}}</td>
                                    <td class="text-left">{{ $insuranceDeduction->user->name}}</td>
                                <!-- <td class="text-center"></td> -->
                                    <td class="text-left">{{$insuranceDeduction->insurance->name}}</td>
                                    <td class="text-left">{{$insuranceDeduction->insurance->coverage_amount}}</td>
                                    <td class="text-left">{{$insuranceDeduction->amount}}</td>
                                    <td class="text-center">
                                        @if(!$month->insuranceSetting || $month->insuranceSetting->value == 'open')
                                        <a href="/admin/insurance-deduction/{{$insuranceDeduction->id}}/delete" class="btn btn-danger btn-sm crude-btn" onClick="return confirm('Delete user insurance?')"><i class="fa fa-trash fa-fw"></i>Delete</a> 
                                        
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @else 
                            <tr>
                                    <td class="text-center" colspan="5">No Records found</td>
                            </tr>
                        @endif  
                    <tbody>
                </table>
            </div>
        </div>
    </form>
</div>	
<script>
    $(document).ready(function() {
        var t = $('#deduction-table').DataTable( {
            pageLength:500, 
            "columnDefs": [ {
                "searchable": false,
                "orderable": false,
                "targets": 0
            } ],
            "order": [[ 1, 'asc' ]],
            scrollY:        true,
            scrollX:        true,
            paging:         false,
            fixedHeader: true,
            fixedColumns:   {
                leftColumns: 3,
            },
        } );
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    });
</script>
@endsection
