@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid" ng-app="myApp" ng-controller="insuranceIndexController">
    <div class="breadcrumb-wrap">
      <div class="row">
      <div class="col-sm-8">
        <h1 class="admin-page-title">Employee Insurance</h1>
        <ol class="breadcrumb">
          <li><a href="/admin">Admin</a></li>
          <li class="active">Insurance</li>
        </ol>
      </div>
      <div class="col-sm-4 text-right m-t-10">
          <a href="#" ng-click="model.addInsurance()" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Add New Policy</a>
      </div>
    </div>
</div>
  <div class="m-t-15">
    <div class="panel panel-default ">
      <table class="table table-striped">
       <tr>
        <th width="5%">#</th>
        <th width="15%">Policy</th>
        <th width="15%">Name</th>
        <th width="10%">Duration</th>
        <th width="20%">Coverage </th>
        <th width="15%">Premium</th>
        <th width="20%">Created At</th>
        <th width="10%" class="text-right">Actions</th>
            <tr ng-repeat="policy in model.data">
              <td>
                %%policy.id%%
              </td>
              <td>
                No: %%policy.policy.policy_number%%
                <br/>Name: %%policy.policy.name%%
              </td>
              <td>
                %%policy.insurable.name%%
              </td>
              <td>
                <small>Start Date : %%policy.policy.start_date | dateFormat%%</small><br />
                <small>End Date : %%policy.policy.end_date | dateFormat%%</small>
              </td>
              <td>
                <div class="m-t-5">
                  <small><label>Coverage:</label></small> <strong>%%policy.policy.coverage_amount%%/- </strong><br />
                  <small><label>Amount Paid:</label></small> <strong>%%policy.policy.amount_paid%% /- </strong>
                </div>
              </td>
              <td>
                <div class="m-t-5">
                  <small>%%policy.policy.premium_amount%%</small>
                </div>
              </td>
              <td>%%policy.created_at | dateToISO%%</td>
              <td>
                <!-- <button class="btn-primary btn-small" ng-click="model.edit(policy.id)">View</button>
                <button class="btn-warning btn-small">Edit</button> -->
              </td>
            </tr>
        <tr ng-if="!model.data.length">
          <td colspan="3">No results found.</td>
        </tr>
      </table>
    </div>
  </div>

  <!--modal -->
  <div id="insuranceModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Choose Insurance</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-8 col-sm-offset-2 m-t-15">
              <label>Select Insurance:</label>
              <select class="form-control">
                <option>Basic</option>
              </select>
              <div class="row row-sm m-t-15">
                  <div class="col-sm-6">
                    <label>Coverage:</label> <strong>1,00,000 /- </strong>
                  </div>
                  <div class="col-sm-6">
                    <label>Monthly:</label> <strong>350 /- </strong>
                  </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success" data-dismiss="modal">Add</button>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection