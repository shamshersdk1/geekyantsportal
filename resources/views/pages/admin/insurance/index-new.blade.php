@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid" >
    <div class="breadcrumb-wrap">
      <div class="row">
      <div class="col-sm-8">
        <h1 class="admin-page-title">Employees Insurance</h1>
        <ol class="breadcrumb">
          <li><a href="/admin">Admin</a></li>
          <li class="active">Insurance</li>
        </ol>
      </div>
    </div>
</div>
<div class="row bg-white search-section-top">
  <div class="col-md-8">
    <div class="btn-group" style="padding:0">
      <a href="" type="button" class="btn btn-primary ">All</a>
      <a href="" type="button" class="btn btn-default ">Approved</a>
      <a href="" type="button" class="btn btn-default ">Requested</a>
      <a href="" type="button" class="btn btn-default ">Pending</a>
    </div>
  </div>
  <div class="col-md-4">
    <label>Action</label>
    <select class="form-control">
      <option>Pending</option>
      <option>Requested for Insurance</option>
      <option>Insurance Added</option>
      <option>Requested for Removal</option>
      <option>Insurance Closed</option>
      <option>Ignore Insurance</option>
    </select>
  </div>
</div>
  <div class="m-t-15">
    <div class="panel panel-default ">
      <table class="table table-striped">
     
       <tr>
        <th class="text-center" width="5%"> <input type="checkbox" name="user" value="user"> </th>
        <th width="5%">Emp ID</th>
        <th width="15%">Name</th>
        <th width="10%">DOJ</th>
        <th width="15%">Status</th>
        <th width="20%">Insurance </th>
        <th width="20%">Last Action</th>
        <th width="10%" class="text-right">Actions</th>
        @if(count($usersList) > 0)
          @foreach($usersList as $index => $user)
            <tr>
             <td class="text-center"><input type="checkbox" name="user" value="user"> </td>
              <td>
                <a href="/admin/users/{{$user->id}}/profile">{{!empty($user->employee_id) ? $user->employee_id : "--"}}</a>
              </td>
              <td>
                <a href="/admin/users/{{$user->id}}/profile">{{$user->name}}</a><br/>
                 <small>Gender: {{$user->gender}}</small><br/>
                 <small>DOB: {{{date_to_ddmmyyyy($user->dob)}}} </small>
              </td>
              <td>
                {{empty($user->joining_date) ? "-" : $user->joining_date}}
              </td>

              <td>
                @if($user->name == 'Megha Kumari')
                  <span class="label label-success custom-label">Added</span>
                @endif
                @if($user->name == 'Apoorva Sahu')
                  <span class="label label-info custom-label">Requested to Add</span>
                @endif
                @if($user->name == 'Kumar Pratik')
                  <span class="label label-default custom-label">Pending</span>
                @endif
                @if($user->name == 'Kumar Sanket')
                  <span class="label label-warning custom-label">Ingored</span>
                @endif
                @if($user->name == 'Atul Ranjan')
                  <span class="label label-danger custom-label">Closed</span>
                @endif
                @if($user->name == 'Varun Kumar Sahu')
                  <span class="label label-primary custom-label">Requested for removal</span>
                @endif
                <div class="m-t-5">
                  <small>at 12 Jan 2019, 01:00pm</small>
                </div>
              </td>
              <td>
                @if($user->name == 'Megha Kumari' || $user->name == 'Apoorva Sahu')
                
                <div class="">
                  <small><label>Coverage:</label></small> <strong>1,00,000 /- </strong><br />
                  <small><label>Monthly:</label></small> <strong>350s /- </strong>
                </div>
                <!-- <div class="input-group input-group-sm">
                  <span class="input-group-addon">Monthly</span>
                  <input type="text" class="form-control" />
                </div> -->
                @endif
                </div>
              </td>
              <td>
                @if($user->name == 'Megha Kumari')
                  <span>Changed Insurance from Basic to Advance</span><br />
                  <small>at 2nd Feb 2019, 12pm</small>
                @endif
                @if($user->name == 'Apoorva Sahu')
                  <span>Requested for Removal</span><br />
                  <small>at 2nd Feb 2019, 12pm</small>
                @endif
                @if($user->name != 'Apoorva Sahu' && $user->name != 'Megha Kumari')
                  <span>Requested for Insurance</span><br />
                  <small>at 2nd Feb 2019, 12pm</small>
                @endif
              </td>
              
              <td class="text-right">
                @if($user->name == 'Megha Kumari')
                  <a href="" class="btn btn-info btn-sm">Request for Removal</a>
                @endif
                @if($user->name == 'Apoorva Sahu')
                  <a href="" class="btn btn-info btn-sm">Insurance Added</a>
                @endif
                @if($user->name == 'Kumar Pratik')
                  <a href="" class="btn btn-info btn-sm"  data-toggle="modal" data-target="#insuranceModal">Request to Add</a>
                @endif
                @if($user->name == 'Kumar Sanket')
                  <a href="" class="btn btn-info btn-sm">Request to Add</a>
                @endif
                @if($user->name == 'Atul Ranjan')
                  <!-- <a href="" class="btn btn-danger btn-sm">Closed</a> -->
                @endif
                @if($user->name == 'Varun Kumar Sahu')
                  <a href="" class="btn btn-info btn-sm">Close</a>
                @endif
              </td>
            </tr>
            @endforeach
        @else
        <tr>
          <td colspan="3">No results found.</td>
        </tr>
        @endif
      </table>
    </div>
  </div>

  <!--modal -->
  <div id="insuranceModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Choose Insurance</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-8 col-sm-offset-2 m-t-15">
              <label>Select Insurance:</label>
              <select class="form-control">
                <option>Basic</option>
              </select>
              <div class="row row-sm m-t-15">
                  <div class="col-sm-6">
                    <label>Coverage:</label> <strong>1,00,000 /- </strong>
                  </div>
                  <div class="col-sm-6">
                    <label>Monthly:</label> <strong>350 /- </strong>
                  </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success" data-dismiss="modal">Add</button>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection