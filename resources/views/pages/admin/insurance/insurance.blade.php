@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid" >
    <div class="breadcrumb-wrap">
      <div class="row">
      <div class="col-sm-8">
        <h1 class="admin-page-title">Insurance List</h1>
        <ol class="breadcrumb">
          <li><a href="/admin">Admin</a></li>
          <li class="active">Insurance</li>
        </ol>
      </div>
    </div>
  </div>
  <div class="m-t-15">
    <div class="panel panel-default ">
      <table class="table table-hover table-striped">
          <tr>
            <th width="40%">Insurance Name</th>
            <th>Type</th>
            <th>Effective Date</th>
            <th>Coverage Amount</th>
            <th>Monthly Deduction</th>
            <th>Assigned user</th>
            <th class="text-right">Action</th>
          </tr>
          @foreach( $insuranceData as $insurance )
          <tr>
            <td>
              <strong>{{ $insurance['name'] }}</strong><br />
              <small>{{ $insurance['number'] }}</small>
            </td>
            <td>{{ $insurance['type'] }}</td>
            <td>{{ $insurance['effective_date'] }}</td>
            <td>{{ $insurance['coverage_amount'] }}</td>
            <td>{{ $insurance['monthly_deduction'] }}</td>
            <td>{{ $insurance['assigned_user'] }}</td>
            <td class="text-right">
              <a href="{{ $insurance['url'] }}" target="_blank" class="btn btn-info btn-sm">View</a>
            </td>
          </tr>                  
          @endforeach
        </table>
    </div>
  </div>

@endsection