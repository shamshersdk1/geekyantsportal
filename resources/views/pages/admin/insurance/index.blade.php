@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid" ng-app="myApp" ng-controller="insuranceIndexController">
    <div class="breadcrumb-wrap">
      <div class="row">
      <div class="col-sm-8">
        <h1 class="admin-page-title">Insurance</h1>
        <ol class="breadcrumb">
          <li><a href="/admin">Admin1</a></li>
          <li class="active">Insurance</li>
        </ol>
      </div>
    </div>
    </div>
<div class="row bg-white search-section-top">
  <div class="col-md-8">
    <ul class="btn-group" style="padding:0">
    <a href="" type="button" class="btn btn-primary ">All</a>
    <a href="" type="button" class="btn btn-default ">Approved</a>
    <a href="" type="button" class="btn btn-default ">Requested</a>
    <a href="" type="button" class="btn btn-default ">Pending</a>
    </ul>
  </div>
  <div class="col-md-4">
  <a href="" class="btn btn-success pull-right" style="margin-left:10px" data-toggle="modal" data-target="#myModal"><i class="fa fa-edit fa-fw"></i>Approved</a>
    <a href="" class="btn btn-success pull-right" style="margin-left:10px">Requested</a>
    <a href="" class="btn btn-success pull-right" >Request for removal</a>
  </div>
</div>
  <div class="user-list-view m-t-30">
    <div class="panel panel-default ">
      <table class="table table-striped">
     
       <th> <input type="checkbox" ng-click="model.selectAll()" name="user" value="user"> </th>
        <th>Name of Employee</th>
        <th>DOJ</th>
        <th>Status</th>
        <th>Amount</th>
        <th>Coverage</th>
        <th>
      <span>Requested on </span></th>
        <th>Last  action type & at</th>
        <th class="text-right">Actions</th>
            <tr ng-repeat="premium in model.data ">
             <td><input type="checkbox" ng-model="premium.is_checked" value="user"> </td>
              <td class="td-text">
                <a href="/admin/users//profile">%%premium.user.name%%</a><br/>
                 <small>Gender: %%premium.user.gender%%</small><br/>
                 <small>DOB:  %%premium.user.dob%%</small>
                
              </td>
              <td class="td-text"><small style="color:#aaa"> 
                %%premium.user.joining_date%%
              </td>

              <td class="td-text">
                  <span class="label label-success custom-label" ng-if="premium.status =='approved'">Approved</span>
                  <span class="label label-info custom-label" ng-if="premium.status =='requested'">Requested</span>
                  <span class="label label-warning custom-label" ng-if="premium.status =='pending'">Pending</span>
                  <span class="label label-danger custom-label" ng-if="premium.status =='closed'">Closed</span>
              </td>
              <td>
                <input type="number" ng-model="premium.amount" ng-style="premium.error && !premium.amount  && {border: '1px solid red'}"/>
                <p ng-show="premium.error && !premium.amount"  class="text-danger small">
                      Please enter the amount
                </p>
              </td>
              <td class="td-text"> 1,00,000</td>
              <td class="td-text"> 
                <span ng-if="premium.date">%%premium.date | dateFormat%%</span>
              </td>
              <td class="td-text"><span>%%premium.updated_at | dateToISO%%
              Previous Amount: %%premium.last_updated.amount%%</span></td>
              
              <td>
                <a href="" class="btn btn-primary pull-right btn-sm" ng-show="premium.status =='pending'" ng-click="model.sendRequest(premium.id, $index)">Send Request</a>
                <a href="" class="btn btn-success pull-right btn-sm" ng-show="premium.status =='requested'" ng-click="model.markAsApproved(premium.id, $index)">Mark Approve</a>
                <a href="" class="btn btn-danger pull-right btn-sm" ng-show="premium.status =='approved'" ng-click="model.markAsClosed(premium.id, $index)">Mark Closed</a>
                <a href="" class="btn btn-primary pull-right btn-sm" ng-click="model.updateRecord(premium.id, $index)">Update</a>

              </td>
            </tr>
        <tr>
          <td colspan="3">No results found.</td>
          <td></td>
          <td></td>
        </tr>
      </table>
    </div>
  </div>

  <!--modal -->
  <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Insurance Amount</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-8 col-sm-offset-2">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Amount">
              <span class="input-group-addon"><i class="fa fa-plus fa-fw"></i></span>
            </div>
          </div>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">Add</button>
      </div>
    </div>

  </div>
</div>
</div>

@endsection