@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid" ng-app="myApp" ng-controller="insurancePolicyIndexController">
  <div class="breadcrumb-wrap">
    <div class="row">
    <div class="col-sm-8">
      <h1 class="admin-page-title">Insurance Policy</h1>
      <ol class="breadcrumb">
        <li><a href="/admin">Admin</a></li>
        <li class="active">Insurance</li>
      </ol>
    </div>
    <div class="col-sm-4 text-right m-t-10">
        <a href="#" ng-click="model.addNewPolicy()" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Add New Policy</a>
    </div>
  </div>
</div>
  <div class="m-t-15">
    <div class="panel panel-default ">
      <table class="table table-striped">
      <tr>
        <th width="5%">#</th>
        <th width="15%">Policy Number</th>
        <th width="15%">Duration</th>
        <th width="20%">Coverage </th>
        <th width="15%">Premium</th>
        <th width="20%">Created At</th>
        <th width="15%" class="text-right">Actions</th>
      </tr>
      <tr ng-repeat="policy in model.data">
        <td>
          %%policy.id%%
        </td>
        <td>
          %%policy.name%%<br />
          <small>%%policy.policy_number%%</small>
        </td>
        <td>
          <small>%%policy.start_date | dateFormat%% - </small><br />
          <small>%%policy.end_date | dateFormat%%</small>
        </td>
        <td>
          <div class="m-t-5">
            <small><label>Coverage:</label></small> <strong>%%policy.coverage_amount%%/- </strong><br />
            <small><label>Amount Paid:</label></small> <strong>%%policy.amount_paid%% /- </strong>
          </div>
        </td>
        <td>
          <div class="m-t-5">
            <small>%%policy.premium_amount%%</small>
          </div>
        </td>
        <td><small>%%policy.created_at | dateToISO%%</small></td>
        <td>
          <a href="/admin/insurance-policy/%%policy.id%%" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i> View</a>
          <button class="btn btn-warning btn-sm" ng-click="model.edit(policy.id)"><i class="fa fa-edit"></i> Edit</button>
        </td>
      </tr>
      <tr ng-if="!model.data.length">
        <td colspan="3">No results found.</td>
      </tr>
      </table>
    </div>
  </div>
</div>

@endsection