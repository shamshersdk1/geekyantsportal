@extends('layouts.admin-dashboard')
@section('main-content')
 <script type="text/javascript">
    window.policyId = '<?php echo json_encode($policy->id); ?>';
</script>
<div class="container-fluid" ng-app="myApp" ng-controller="viewInsurancePolicyCtrl">
    <div class="breadcrumb-wrap">
      <div class="row">
      <div class="col-sm-8">
        <h1 class="admin-page-title">Insurance Policy</h1>
        <ol class="breadcrumb">
          <li><a href="/admin">Admin</a></li>
          <li><a href="/admin/insurance-policy">Insurance Policies</a></li>
          <li class="active">%%form.data.name %%</li>
        </ol>
      </div>
    </div>
  </div>
  <div>
    <div class="m-t-15">
      <div class="panel panel-default ">
        <div class="panel-body">
          <div class="flexbox">
            <div>
              <label>Insurance:</label><br />
              <big class="no-margin text-info">%%form.data.name %%</big>
            </div>
            <div>
              <label>Policy Number:</label><br />
              <big class="no-margin text-info">%%form.data.policy_number%%</big>
            </div>
            <div>
              <label>Duration:</label><br />
              <big class="no-margin text-info" ng-if="form.data.start_date">%%form.data.start_date | dateFormat%% - %%form.data.end_date | dateFormat %%</big>
            </div>
            <div class="text-right">
              <label>
                <small>Applicable for:</small>
                <span class="label label-info custom-label" >%%form.data.applicable_for%%</span>
              </label>
              <br />
              <label class="no-margin">
                <small>Accessible to:</small>
                <span class="label label-info custom-label">%%form.data.accessible_to%%</span>
              </label>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
    <div ng-repeat="subGroup in form.data.sub_groups">
      <div class="panel panel-default m-b-10">
        <div class="panel-body">
          <div class="row row-sm">
            <div class="col-sm-6">

              <h4 class="no-top-margin">%%subGroup.name%% </h4>
              <div >
                <a href="" class="btn btn-warning btn-xs" ng-click="form.edit($index)"><i class="fa fa-edit fa-fw"></i></a>
                <a href="" ng-click="form.deleteRecord($index)" class="btn btn-default text-danger btn-xs"><i class="fa fa-trash fa-fw"></i></a>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="flexbox m-t-5">
                <div>
                  <small>Coverage:</small><br />
                  <big class="text-info">%%subGroup.coverage_amount%%</big>
                </div>
                <div>
                  <small>Amount Paid:</small><br />
                  <big class="text-info">%%subGroup.amount_paid%%</big>
                </div>
                <div>
                  <small>Premium Amount:</small><br />
                  <big class="text-info">%%subGroup.premium_amount%%</big>
                </div>
              </div>
            </div>
            <div class="col-sm-2 text-right m-t-10">
              <a data-toggle="collapse" class="show-insurance-users collapsed btn btn-success btn-sm" href="#show-%%subGroup.id%%" aria-expanded="false" aria-cotrols="show-%%subGroup.id%%">
                  <span>%%subGroup.insurances.length%%</span> %%subGroup.applicable_for%%
              </a>
              <a ng-if="subGroup.insurances.length" class="btn btn-primary btn-sm" ng-click="form.downloadReport($parent.$index, $index)">Report</a>
            </div>
          </div>
        </div>
          <div id="show-%%subGroup.id%%" class="collapse">
            <div class="panel panel-default no-margin">
              <div class="panel-body">
                <form name="insuranceAddForm[$index]" ng-submit="form.submit(insuranceAddForm[$index], $index)" novalidate>
                    <div class="row row-sm ">
                      <div class="col-sm-10">
                        <div class="loader" ng-show="form.user.loading"></div>
                        <div class="form-group no-m-b" ng-if="subGroup.applicable_for == 'user' && !form.user.loading">
                          <label>Employee</label>
                          <div>
                            <ui-select
                              ng-model="subGroup.selected_user"
                              on-select="form.select_user($select.selected, $index)"
                              theme="select2"
                              name="user"
                              id="user"
                              allow-clear="true"
                              ng-style="subGroup.error && !insuranceAddForm.user.$valid && {border: '1px solid red'}"
                              required
                            >
                              <ui-select-match allow-clear="true"
                                >%%$select.selected.name%% - %%$select.selected.employee_id%%</ui-select-match>
                              <ui-select-choices repeat="user in form.user.data | filter: $select.search">
                                %%user.name%%
                                 <div ng-bind-html="user.employee_id | highlight: $select.search"></div>
                              </ui-select-choices>
                            </ui-select>
                          </div>
                        </div>
                        <div class="loader" ng-show="form.asset.loading"></div>
                        <div class="form-group no-m-b" ng-if="subGroup.applicable_for == 'asset' && !form.asset.loading">
                          <label>Asset</label>
                          <div>
                            <ui-select
                              ng-model="form.selected_asset"
                              on-select="form.select_asset($select.selected)"
                              theme="select2"
                              name="asset"
                              id="asset"
                              allow-clear="true"
                              ng-style="subGroup.error && !insuranceAddForm.asset.$valid && {border: '1px solid red'}"
                              required
                            >
                              <ui-select-match allow-clear="true"
                                >%%$select.selected.name%%</ui-select-match
                              >
                              <ui-select-choices
                                repeat="asset in form.asset.data | filter: $select.search"
                              >
                                %%asset.name%%
                                 <div ng-bind-html="'Serial No: '+asset.serial_number | highlight: $select.search"></div>
                              <small>
                                Model: %%asset.model%%
                              </small>
                              </ui-select-choices>
                            </ui-select>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-2 m-t-15">
                        <button class="btn btn-primary m-t-10">Add</button>
                      </div>
                    </div>
                    <div ng-if="subGroup.error" class="text-danger error m-t-10">
                      %%subGroup.error_message%%
                    </div>
                    <div ng-if="subGroup.success" class="text-success m-t-10">
                      %%subGroup.message%%
                    </div>
                </form>
              </div>
              <table class="table table-striped table-condensed">
                <tr>
                  <th width="5%">#</th>
                  <th width="25%">Name</th>
                  <th width="15%">ID</th>
                  <th width="20%">Created at</th>
                  <th width="20%">Status</th>
                  <th width="15%" class="text-right">
                      Actions &nbsp;

                  </th>
                </tr>
                <tbody >
                  <tr ng-repeat="insurance in subGroup.insurances">
                    <td>
                      %%insurance.id%%
                    </td>
                    <td ng-if="insurance.insurable.is_active == 1">
                      %%insurance.insurable.name%%
                    </td>
                    <td ng-if="insurance.insurable.is_active == 0" style="background-color:#d32f2f">
                      %%insurance.insurable.name%%
                    </td>
                    <td>
                      %%insurance.insurable.employee_id%%
                    </td>
                    <td>
                      <small ng-if="insurance.created_at">%%insurance.created_at | dateToISO%%</small>
                    </td>
                    <td>
                        <span class="label label-primary custom-label" ng-if="insurance.status == 'pending'">Pending</span>
                        <span class="label label-success custom-label" ng-if="insurance.status == 'approved'">Approved</span>
                        <span class="label label-default custom-label" ng-if="insurance.status == 'closed'">Closed</span>
                    </td>
                    <td class="text-right">
                      <!-- <button class="btn btn-warning btn-sm" ng-click="form.markAsClose($index)">Close</button> -->
                      <button class="btn btn-warning btn-xs m-r-15" ng-click="form.deleteRecord($parent.$index, $index )">Remove</button>
                    </td>
                  </tr>
                    <tr ng-if="!subGroup.insurances.length">
                    <td colspan="6">No results found.</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection