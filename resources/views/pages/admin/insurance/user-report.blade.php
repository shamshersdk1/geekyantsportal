@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-6">
                <h1 class="admin-page-title">Insurance Policy User Report</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li><a href="/admin/insurance-policy">Insurance Policy</a></li>
        		</ol>
            </div>
			
		</div>
	</div>
	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
    </div>   
    <div class="row">
        <div class="col-md-6">
            <div class="user-list-view">
                <div class="panel panel-default">
                    <table class="table table-striped">
                        <caption><strong>Active User(s) without insurance policy</strong></caption>
                        <tr>
                            <th class="text-center">User</th>
                            <th class="text-center">Date</th>
                        </tr>
                        @if( empty($ActiveUsers) || count($ActiveUsers) == 0 )
                            <tr>
                                    <td class="text-center" colspan="5">No Records found</td>
                                </tr>
                        @endif
                        @if(isset($ActiveUsers) > 0)
                            @foreach($ActiveUsers as $user)
                                <tr>
                                    <td class="text-center">
                                        {{$user->employee_id}}<br>
                                        {{$user->name}}
                                    
                                    </td>
                                    <td class="text-center"> 
                                        <label><small>DOJ: {{$user->joining_date}}</small></label><br>
                                        <label><small>Confirmation Date: {{$user->confirmation_date}}</small></label>
                                    </td>
                                </tr>
                            @endforeach
                        @endif  
                    </table>
                </div>
                
            </div>
        </div>
        <div class="col-md-6">
            <div class="user-list-view">
                <div class="panel panel-default">
                    <table class="table table-striped">
                        <caption><strong>In-Active User(s) with insurance policy</strong></caption>
                        <tr>
                            <th class="text-center">User</th>
                            <th class="text-center">Duration</th>
                        </tr>
                        @if( empty($unActiveUsers) || count($unActiveUsers) == 0 )
                            <tr>
                                    <td class="text-center" colspan="5">No Records found</td>
                                </tr>
                        @endif
                        @if(isset($unActiveUsers) > 0)
                            @foreach($unActiveUsers as $unActiveUser)
                                <tr>
                                    <td class="text-center">
                                        {{$unActiveUser->employee_id}}<br>
                                        {{$unActiveUser->name}}
                                    </td>
                                    <td class="text-center"> 
                                        <label><small>DOJ: {{$unActiveUser->joining_date}} to <br>Release Date: {{$unActiveUser->release_date}}</small></label>
                                    </td>
                                </tr>
                            @endforeach
                        @endif  
                    </table>
                </div>
                
            </div>
        </div>
    </div>
</div>	
@endsection
