@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
<div class="breadcrumb-wrap">
   <div class="row">
      <div class="col-md-8">
         <h1 class="admin-page-title">Checklist</h1>
         <ol class="breadcrumb">
            <li><a href="/admin">Admin</a></li>
            <li><a href="{{ url('admin/checklist') }}">Checklist</a></li>
         </ol>
      </div>
   </div>
</div>
<div class="row">
   <!-- -->
   <div ng-app="myApp" ng-controller="checklistCtrl" class="row" style="margin-left: 1px; margin-right: 1px;">
      <div class="col-md-12 checklist">
         <div class="panel-group " id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
               <div class="panel-heading" role="tab" id="headingOne" ng-click="getUserMismatchList()">
                  <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed ">
                  Get the list of all user which are not available in the portal but added in Slack. 
                <span ng-if="user_loading" style="margin-left: 15px;"><i class="fa fa-spinner fa-spin" aria-hidden="true"></i></span>

                  </a>
               </div>
               <div id="collapseOne" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
                  <div class="panel-body p-0">
                     <table class="table table-striped" >
                        <thead>
                           <tr>
                              <th>#</th>
                              <th>User first name</th>
                              <th>User Last name</th>
                              <th>User Email</th>
                           </tr>
                        </thead>
                        <tbody>
                           <tr ng-repeat="user in user_mismatch_list">
                              <td> %% $index +1 %% </td>
                              <td> %% user.first_name %% </td>
                              <td> %% user.last_name %% </td>
                              <td> %% user.email %% </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
            <div class="panel panel-default">
               <div class="panel-heading" role="tab" id="headingTwo" ng-click="getUnassignedChannels()">
                  <a class="collapsed"  role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                  Get the list of all Slack channel which are not mapped with any project.
                  <span ng-if="channels_loading" style="margin-left: 15px;"><i class="fa fa-spinner fa-spin" aria-hidden="true"></i></span>

                  </a>
               </div>
               <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                  <div class="panel-body p-0">
                     <table class="table table-striped" >
                           <thead>
                                <tr>
                                <th>#</th>
                                <th>Slack id</th>
                                <th>Slack Channel Name</th>
                                <th>Slack Channel Normalized Name</th>
                                </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="unassigned_channel in unassigned_channels">
                            <td> %% $index +1 %%  </td>
                            <td> %% unassigned_channel.id %% </td>
                            <td> %% unassigned_channel.name %% </td>
                            <td> %% unassigned_channel.name_normalized %% </td>
                            </tr>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
            <div class="panel panel-default">
               <div class="panel-heading" role="tab" id="headingThree" ng-click="getFreeProjects()">
                  <a class="collapsed " role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree" >
                    Get the list of all project (status as in_progress) without any developers.
                    <span ng-if="projects_loading" style="margin-left: 15px;"><i class="fa fa-spinner fa-spin" aria-hidden="true"></i></span>

                  </a>
               </div>
               <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                  <div class="panel-body p-0">
                     <table class="table table-striped">
                         <thead>
                            <tr>
                            <th>#</th>
                            <th>Project ID</th>
                            <th>Project Name</th>
                            <th>Project Start Date</th>
                            </tr>
                         </thead>
                         <tbody>
                            <tr ng-repeat="free_project in free_projects">
                            <td> %% $index +1 %%  </td>
                            <td> %% free_project.id %% </td>
                            <td> %% free_project.project_name %% </td>
                            <td> %% free_project.start_date %% </td>
                            </tr>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
         <!-- -->    
      </div>
   </div>
</div>
@endsection