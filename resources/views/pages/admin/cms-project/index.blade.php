@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">List of Projects</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/cms-project') }}">Cms Projects</a></li>
                </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <a onclick="window.location='{{ url("admin/cms-project/create") }}'" class="btn btn-success"><i class="fa fa-plus fa-fw"></i> Add new Projects</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
        	@if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                      @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif
            <div class="row bg-white search-section-top" >
                <div class="col-md-3">
                    <form action="/admin/cms-project" method="GET">
                        @if( !empty($search) )
                            Showing results for
                        @endif
                        <input type="text" name="searchprojects" class="form-control" placeholder="search.." value="@if(isset($search)){{$search}}@endif">
                    </form>
                </div>
            </div>
            <div class="user-list-view m-t-15">
                <div class="panel panel-default">
                    <table class="display table table-striped">
                        <thead>
                            <tr>
                                <th width="5%">#</th>
                                <th>Name of Project</th>
                                <th class="sorting text-right" width="35%">Action</th>
                            </tr>
                        </thead>
                        @foreach($projects as $project)
                            <tr>
                                <td><a>{{$project->id}}</a></td>
                                <td><a href="/admin/cms-project/{{$project->id}}"> {{$project->project_title}}</a></td>
                                <td class="text-right">
                                    <a href="/admin/cms-project/{{$project->id}}" class="btn btn-success btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View</a>

                                <a href="/admin/cms-project/{{$project->id}}/edit" class="btn btn-info btn-sm"><i class="fa fa-pencil btn-icon-space" aria-hidden="true"></i>Edit</a>

                                    <form method="post" action="/admin/cms-project/{{$project->id}}" style="display:inline-block;">
                                        <input name="_method" type="hidden" value="DELETE">
                                        <button type="submit" class="btn btn-danger btn-sm" onclick="deletetech();"><i class="fa fa-trash btn-icon-space" aria-hidden="true"></i>Delete</button>
                                    </form>
                                    
                                </td>
                            </tr>
                        @endforeach
                    </table>
                    <div class="col-md-12">
                        <div class="pageination pull-right">
                            <nav aria-label="Page navigation">
                                <ul class="pagination">
                                    {{ $projects->render() }}
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <a class="btn btn-success crud-btn btn-lg add-newproj-btn" onclick="window.location='{{ url("admin/cms-project/create") }}'"><i class="fa fa-plus btn-icon-space" aria-hidden="true"></i>Add new Project</a> -->
        </div>
    </div>
</div>
@endsection
@section('js')
@parent
	<script>
		
		function deletetech() {
			var x = confirm("Are you sure you want to delete?");
			if (x)
				return true;
			else
				return false;
		}
	</script>
@endsection