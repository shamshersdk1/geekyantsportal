@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
	<!-- ===================== bread crumbs ==================== -->
	<div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Add Projects</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
				  	<li><a href="{{ url('admin/cms-technology') }}">Cms Project</a></li>
				  	<li class="active"><a href="{{ url('admin/cms-project/create') }}"></a>Add</li>
	        	</ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
            	<button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
        </div>
    </div>
	<!-- ============================= view design ==================== -->
    <div class="row">
    	<div class="col-md-12">
    		@if(!empty($errors->all()))
	            <div class="alert alert-danger">
	                @foreach ($errors->all() as $error)
	                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                    <span>{{ $error }}</span><br/>
	                @endforeach
	            </div>
       	 	@endif
        	@if (session('message'))
	            <div class="alert alert-success">
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ session('message') }}</span><br/>
	            </div>
        	@endif
    	</div>
	    <div class="col-md-12">
			<form name = "myForm" method="post" action="/admin/cms-project" enctype="multipart/form-data">
				<div class="panel panel-default panel-white add-new-tech">
					<div class="row">
				    	<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<div class="col-sm-6">
						<div class="form-group">
			    			    <label for="">Enter Title:</label>
			    			    <input type="text" name="project_title" class="form-control" id="" placeholder="Name" value="{{ old('project_title') }}" required>
			    			    <p id="project_title" style="color:red;"></p>
			    			</div>
							<div class="form-group">
			    			    <label for="">Enter Technology:</label>
			    			    <select name="saveTech[]" class="js-example-tags form-control" multiple="multiple">
			    			    	@foreach($technologies as $technology)
			    			    		<option value="{{$technology->id}}">{{$technology->name}}</option>
			    			    	@endforeach
			    			    </select>
			    			    <p id="project_type" style="color:red;"></p>
			    			</div>
							<div class="form-group">
			    			    <label for="">Enter Url:</label>
			    			    <input type="text" name="project_url" class="form-control" id="" placeholder="Url" value="{{ old('project_url') }}">
			    			    <p id="project_url" style="color:red;"></p>
			    			</div>
							<div class="form-group">
			    			    <label for="">Enter Project Type:</label>
			    			    <input type="radio" name="project_type" value="products" @if(old('project_type') == 'products') checked @endif required> Products &nbsp
			    			    <input type="radio" name="project_type" value="services" @if(old('project_type') == 'services') checked @endif> Services<br>
			    			    <p id="project_type" style="color:red;"></p>
			    			</div>
							<div class="form-group">
			    			    <label class="checkbox-inline">
			    			      <input type="checkbox" name="is_featured" @if(old('is_featured') == 'on') checked @endif>Featured
			    			    </label>
			    			</div>
							<div class="form-group">
			    			    <label for="exampleInputFile">Add Image</label>
			    			    <input type="file" name="project_image" id="exampleInputFile" value="{{ old('project_image') }}">
			    			    <p id="project_image" style="color:red;"></p>
			    			</div>
						</div>
						<div class="col-sm-6">
						<div class="form-group">
								<label class="">User:</label>
                             <select class="user_id" id="user_id" name="user_id" style="width=35%;" multiple=>
                                <option value=""></option>
                                @foreach($users as $user)
                                    @if ( old('user_id') == $user->id )
                                        <option value="{{$user->id}}" selected >{{$user->name}}</option>
                                    @else
                                        <option value="{{$user->id}}" >{{$user->name}}</option>
                                    @endif 
                                @endforeach
                            </select>
                            </div>
							<div class="form-group">
			    			    <label for="">Enter Full Description:</label>
			    			    <textarea type="text" name="project_desc" class="form-control" id="project-desc" rows="5" placeholder="Full Description">{{old('project_desc')}}</textarea>
			    			    <p id="project_desc" style="color:red;"></p>
			    			</div>
							<div class="form-group">
			    			    <label for="">Enter Short Description:</label>
			    			    <input type="text" name="project_short_desc" class="form-control" id="" placeholder="Short Description" value="{{ old('project_short_desc') }}">
			    			    <p id="project_short_desc" style="color:red;"></p>
			    			</div>
						</div>
						
			    		
				    </div>
				</div>

				<div class="text-center">
		    		<button type="reset" class="btn btn-default"><i class="fa fa-rotate-left fa-fw"></i> Clear</button> 
		    		<button type="submit" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Save</button>
		    	</div>
			</form>
		</div>
	</div>
	<!-- </div> -->
</div>
<script type="text/javascript">
	$(function () {
		$('#user_id').select2();
	});
</script>
@endsection