@extends('layouts.admin-dashboard')
@section('main-content')
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">View Project</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
				  		<li><a href="{{ url('admin/cms-project') }}">Cms Project</a></li>
				  		<li class="active">View</li>
		        	</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>
	    <div class="row">
			<div class="col-md-12">
	            <div class="panel panel-default">
					
					    <div class="panel-body">
					    	<form name = "myForm" style="padding:10px;" > 

					    			<input type="hidden" name="_token" value="{{ csrf_token() }}">
					    			<div class="cms-view cmsproj-view-name info-table"><b>Name :</b> {{$project->project_title}} </div>
					    			<div class="cms-view cmsproj-view-url info-table"><b>Url :</b> {{$project->project_url}}</div>
					    			<div class="cms-view cmsproj-view-des info-table"><b>Description :</b> {{$project->project_desc}}</div>	
									<div class="cms-view cmsproj-view-des info-table"><b>Short Description :</b> {{$project->project_short_desc}}</div>	

					    			<div class="cms-view cmsproj-view-project info-table"><b>Project Type :</b> {{$project->project_type}}</div>
					    			<div class="cms-view cmsproj-view-tech info-table"><b>Technologies :</b>
					    				<ul class="cav" style="display: inline-block; list-style-type:none;">
						    				@foreach($technologies as $technology)
						    					<li style="display: inline-block;">{{$technology->name}}</li> 
						    				@endforeach
					    				</ul>
					    			</div>
					    			<div class="cms-view cmsproj-view-featured info-table"><b>Featured :</b> {{$project->is_featured}}</div>
					    			<div class="cms-view cmsproj-view-img info-table"><b>Image:</b> <br/>
										<img style="max-width:200px; max-height:200px; margin-top:8px;" src="{{$project->project_image}}" />
									</div> 
									<div class="cms-view cmsproj-view-featured info-table"><b>User :</b> {{$project->user}}</div>

					    			

					    	</form>
							<!-- <a href="/admin/project" class="btn btn-primary crud-btn back-btn btn-lg"><i class="fa fa-arrow-left" aria-hidden="true"></i>Back</a> -->

					    </div>
					
				</div>
				<!-- <a href="/admin/project"><button class="btn btn-primary">Back</button></a> -->

			</div>	
		</div>
	</div>
@endsection