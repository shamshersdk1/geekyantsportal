@extends('layouts.admin-dashboard')
@section('main-content')
<script>
	$(document).ready(function(){
		$(' .js-example-basic-multiple').select2();
	});
</script>
<div class="container-fluid">
	<div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Edit Project</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
				  	<li><a href="{{ url('admin/cms-project') }}">Cms Project</a></li>
				  	<li class="active">Edit</li>
	        	</ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
        </div>
    </div>
    <div class="row">
		<div class="col-md-12">
    	@if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span><br/>
                  @endforeach
            </div>
        @endif
        @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
        @endif
        <form name = "myForm" method="post" action="/admin/cms-project/{{$project->id}}" enctype="multipart/form-data">
        	<div class="panel panel-default custom-space">
            	<div class="row">
				    <input name="_method" type="hidden" value="PUT">
	    			<input type="hidden" name="_token" value="{{ csrf_token() }}">
		    		<div class="col-md-6">	
		    			<div class="form-group">
		    			    <label for="exampleInputEmail1">Enter Title:</label>
		    			    <input type="text" name="project_title" class="form-control" id="exampleInputEmail1" value="{{$project->project_title}}">
		    			    <p id="project_title" style="color:red;"></p>
		    			</div>
		    		</div>
		    		
		    		<div class="col-md-6">
		    			<div class="form-group">
		    			    <label for="exampleInputEmail1">Enter Url:</label>
		    			    <input type="text" name="project_url" class="form-control" id="exampleInputEmail1" value="{{$project->project_url}}">
		    			    <p id="project_url" style="color:red;"></p>
		    			</div>
		    		</div>
		    		<div class="col-md-6">
						<div class="form-group">
							<label for="">Enter Full Description:</label>
							<textarea type="text" name="project_desc" class="form-control" id="project-desc" rows="5" placeholder="Full Description">{{$project->project_desc}}</textarea>
							<p id="project_desc" style="color:red;"></p>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="">Enter Short Description:</label>
							<input type="text" name="project_short_desc" class="form-control" id="" placeholder="Short Description" value="{{ $project->project_short_desc}}">
							<p id="project_short_desc" style="color:red;"></p>
						</div>
					</div>
		    		<div class="col-md-6">
		    			<div class="form-group">
		    			    <label for="exampleInputEmail1">Enter Technology:</label>&nbsp
		    			    <select name="saveTech[]" class="js-example-basic-multiple2" multiple="multiple">
		    			    	@foreach($technologies as $technology)
		    			    		<?php
		    			    			$saved = false;
		    			    			foreach($savedTechnologies as $savedTechnology){
		    			    				if($savedTechnology->id == $technology->id){
		    			    					$saved = true;
		    			    				}
		    			    			}
		    			    		?>
		    			    		@if($saved)
		    			    			<option value="{{$technology->id}}" selected="selected">{{$technology->name}}</option>
		    			    		@else
		    			    			<option value="{{$technology->id}}">{{$technology->name}}</option>
		    			    		@endif
		    			    	@endforeach
		    			    </select>
		    			    <p id="project_type" style="color:red;"></p>
		    			</div>
		    		</div>
		    		<div class="clearfix"></div>
		    		<div class="col-md-6">				    			
		    			<div class="form-group">
		    			    <label for="exampleInputEmail1">Enter Project Type:</label>&nbsp
		    			    @if($project->project_type == "products")
		    			    	<input type="radio" name="project_type" value="products" checked="checked"> Products &nbsp
		    			    	<input type="radio" name="project_type" value="services"> Services<br>
		    			    @elseif($project->project_type == "services")
		    			    	<input type="radio" name="project_type" value="products"> Products &nbsp
		    			    	<input type="radio" name="project_type" value="services" checked="checked"> Services<br>
		    			    @else
		    			    	<input type="radio" name="project_type" value="products"> Products &nbsp
		    			    	<input type="radio" name="project_type" value="services"> Services<br>
		    			    @endif
		    			    <p id="project_type" style="color:red;"></p>
		    			</div>
		    		</div>
		    		<div class="col-md-6">				    			
		    			<div class="form-group">
		    			    <label for="exampleInputEmail1">Enter Project Type:</label>&nbsp
	    			    			<div class="multipicker">
										<div class="">
											<select class="js-example-basic-multiple22 " multiple="multiple" name="channels[]">
												<option value="" selected ></option>
											</select>
										</div>
									</div>
		    			    	<p id="project_type" style="color:red;"></p>
		    			</div>
		    		</div>
					
		    		
		    		<div class="col-md-6">
		    			<div class="form-group">
		    			    <label for="exampleInputFile">Add Image</label>
		    			    <input type="file" name="project_image" id="exampleInputFile">
		    			    <img style="max-width:200px; max-height:200px;" src="{{$project->project_image}}" />
		    			</div>
		    		</div>
					
		    		<div class="col-md-12">
		    			<div class="form-group">
		    			    <label>
		    			    @if($project->is_featured == 1)
		    			    	<input type="checkbox" name="is_featured" checked> Featured
		    			    @else
		    			    	<input type="checkbox" name="is_featured" > Featured
		    			    @endif
		    			    </label>
		    			</div>
		    			<!-- <button type="submit" class="btn btn-success crud-btn back-btn">Update</button> -->
					</div>
					<!-- <div class="form-group">
								<label class="">User:</label>
                             <select class="js-example-basic-multiple22 form-control" id="user_id" multiple="multiple" name="user_id" style="width=45%;">
                                <option value=""></option>
                                @foreach($users as $user)
                                        <option value="{{$user->id}}" selected >{{$user->name}}</option>

                                @endforeach
                            </select>
							</div> -->
							<div class="col-sm-6">
					<div class="form-group">
		    			    <label for="exampleInputEmail1">Users:</label>&nbsp
	    			    			<div class="multipicker">
										<div class="">
										
											<select class="js-example-basic-multiple22 " id="user_id"  multiple="multiple" name="users[]">
											@foreach($cmsusers as $cmsuser)
											
											<option value="{{$cmsuser[0]->id}}" selected>{{$cmsuser[0]->name}}</option>
										@endforeach
										
											@foreach($users as $user)
													<option value="{{$user->id}}"  >{{$user->name}}</option>
											@endforeach
											</select>
										</div>
									</div>
		    			    	<p id="project_type" style="color:red;"></p>
		    			</div>
						<div>
				</div>
		
			</div>
			<div class="col-md-12 text-center">
				<button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-rotate-left fa-fw"></i> Cancel</button>
				<button type="submit" class="btn btn-success">Update</button>
			</div>


		</form>
			@if($errors->any())
			<h4>{{$errors->first()}}</h4>
			@endif
		</div>
			
	</div>
</div>
@endsection
@section('js')
@parent
	<script>
	function validateForm() {
		var valid = true;
	    var x = document.forms["myForm"]["project_title"].value;
	    if (x == null || x == "") {
	        document.getElementById("project_title").innerHTML = "Title should be filled";
	      valid = false;
	    }
	    var a = document.forms["myForm"]["project_url"].value;
	    if (a == null || a == "") {
	        document.getElementById("project_url").innerHTML = "Url should be filled";
	    	valid = false;
	    }
	    var b = document.forms["myForm"]["project_desc"].value;
	    if (b == null || b == "") {
	        document.getElementById("project_desc").innerHTML = " Description should be filled";
	    	valid = false;
	    }
	    var d = document.forms["myForm"]["project_type"].value;
	    if (d == null || d == "") {
	        document.getElementById("project_type").innerHTML = "Project Type should be filled";
	    	valid = false;
	    }
	    if(valid == false){
	    	return false;
	    }
	}
	</script>
@stop