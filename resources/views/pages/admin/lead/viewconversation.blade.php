@extends('layouts.admin-dashboard')
@section('main-content')
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">View Lead Dashboard</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
				  		<li class="active">View</li>
		        	</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>
	    <!-- ======================== new view =================== -->
		<div class="row">
			<div class="col-md-12 user-list-view">
	            <div class="panel panel-default">
				    <div class="view-container">
				    	<div class="lead-view-data"><b>Lead Id :</b> {{$leads->id}}</div>
				    	<div class="lead-view-data"><b>Name :</b> {{$leads->name}}</div>
				    	<div class="lead-view-data"><b>Email :</b> {{$leads->email}}</div>
				    	<div class="lead-view-data"><b>Subject :</b> {{$leads->subject}}</div>
				    	<div class="lead-view-data"><b>Message :</b> {{$leads->message}}</div>
				    </div>
				</div>
			</div>	
		</div>





		<!-- <div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="panel panel-default panel-leadmanagment">

		        	@if(!empty($errors->all()))
			            <div class="alert alert-danger">
			                @foreach ($errors->all() as $error)
			                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			                    <span>{{ $error }}</span><br/>
			                  @endforeach
			            </div>
			        @endif
			        @if (session('message'))
			            <div class="alert alert-success">
			                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			                <span>{{ session('message') }}</span><br/>
			            </div>
			        @endif
					<div class="panel-heading">

					    <h3 class="panel-title text-center">Lead Managment</h3>
					</div>
				  	<div class="panel-body">
				    	<div class="row">
				    		<div class="col-md-12">
				    			
				    			<div class="panel panel-default">

					  	<table class="table">
				   	 		<tbody>
				   	 		<tr>
				   	 			<th>Subject</th>
				   	 			<th>Message</th>
				   	 		</tr>
				   	 			
				                <tr>
				                    
				                    <td>{{$leads->id}}</td>
				                    <td><strong>{{$leads->message}}</strong></td>
				              
				                   
				                </tr>
				                

			            	</tbody>
				   	 	</table>
					</div>
						<div class="row">
							<form method="GET" action="/admin/lead">
								<button class="btn btn-info">back</button>
							</form>
						</div>

					           

				    		</div>
				    	</div>
				  	</div>
				</div>
			</div>
		</div> -->
	</div>
@stop