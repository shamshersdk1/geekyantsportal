@extends('layouts.admin-dashboard')

@section('main-content')
	<section class="lead-dashboard-section">
		<div class="container-fluid">
			<div class="breadcrumb-wrap">
		    	<div class="row">
		            <div class="col-sm-12">
		                <h1 class="admin-page-title">Lead Dashboard</h1>
		                <ol class="breadcrumb">
		        		  	<li><a href="/admin">Admin</a></li>
		        		  	<li class="active">Lead Dashboard</li>
		        		</ol>
		            </div>
		            <!-- <div class="col-sm-4 text-right m-t-10">
		                <a href="/admin/cms-technology/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i> Add new Technology</a>
		            </div> -->
		        </div>
		    </div>
		    <div class="row">
	  			<div class="col-md-12">
	  				@if(!empty($errors->all()))
			            <div class="alert alert-danger">
			                @foreach ($errors->all() as $error)
			                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			                    <span>{{ $error }}</span><br/>
			                  @endforeach
			            </div>
			        @endif
			        @if (session('message'))
			            <div class="alert alert-success">
			                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			                <span>{{ session('message') }}</span><br/>
			            </div>
			        @endif
	  			</div>
		    	<div class="col-md-12">
					<div class="lead-dashboard-filter text-right">
						<div class="btn-group" role="group" aria-label="">
							@if(isset($state))
								<form name = "myForm" method="get" style="display:inline-block;" action="/admin/lead/showstatus/all">
									<button type="submit" class="btn btn-default crud-btn btn-sm" @if($state == "all") style="background-color:#aaa;" @endif>All</button>
								</form>
								<form name = "myForm" method="get" style="display:inline-block;" action="/admin/lead/showstatus/new_lead">
				            		<button type="submit" class="btn btn-default crud-btn btn-sm" @if($state == "new_lead") style="background-color:#aaa;" @endif>New Lead</button>  
				            	</form>
								<form name = "myForm" method="get" style="display:inline-block;" action="/admin/lead/showstatus/closed_lost">
									<button type="submit" class="btn btn-default crud-btn btn-sm" @if($state == "closed_lost") style="background-color:#aaa;" @endif>Close Lost</button>
								</form>
								<form name = "myForm" method="get" style="display:inline-block;" action="/admin/lead/showstatus/closed_won">
									<button type="submit" class="btn btn-default crud-btn btn-sm" @if($state == "closed_won") style="background-color:#aaa;" @endif>Close Won</button>
								</form>
								<form name = "myForm" method="get" style="display:inline-block;" action="/admin/lead/showstatus/in_discussion">
									<button type="submit" class="btn btn-default crud-btn btn-sm" @if($state == "in_discussion") style="background-color:#aaa;" @endif>In Discussion</button>
								</form>
								
				            @endif
						</div> 
						<!-- <form name = "myForm" method="get" style="display:inline-block;" action="/admin/lead/showstatus/closed_lost">
			            					<button class="btn btn-danger">Close Lost</button>  
			            </form>
			            <form name = "myForm" method="get" style="display:inline-block;" action="/admin/lead/showstatus/closed_won">
			            					<button class="btn btn-danger">Close Won</button>  
			            </form>
			            <form name = "myForm" method="get" style="display:inline-block;" action="/admin/lead/showstatus/in_discussion">
			            					<button class="btn btn-danger">In Discussion</button>  
			            </form>		
			            <form name = "myForm" method="get" style="display:inline-block;" action="/admin/lead/showstatus/new_lead">
			            					<button class="btn btn-danger">New Lead</button>  
			            </form>	 -->
					</div>
				</div>
				<div class="col-md-12 user-list-view">
					<div class="panel panel-default">
					  	<table class="display table table-striped" cellpadding="10">
						  	<thead>
								<tr>
							        <th width="3%"><input type="checkbox" id="chk_all"/></th>
							        <th width="12%">Name</th>
							        <th width="10%">Date</th>
							        <th width="40%">Subject</th>
							        <th class="text-center" width="15%">Update</th>
							        <th class="text-center" width="20%">Action</th>
							    </tr>

							</thead>
				   	 		<tbody>
				   	 		@if(count($leads) == 0 )
				   	 		<div>
				   	 			<td colspan="6">No Record Found</td>
				   	 		</div>
				   	 		@endif
				   	 		@if(isset($leads))
					   	 		@foreach($leads as $lead)
					   	 			@if($lead->is_read == 0)
					                	<tr class="read"  style="color:#000;">
					                @else
					                	<tr class="unread" style="color:#3d3d3d;">
					                @endif
					                    <td >
					                    	<label>
									      		<input type="checkbox" class="chkbx"/>
									    	</label>
									    </td>
						                    
					                    <td>{{$lead->name}}</td>
		                    			<td>{{$lead->created_at->toDateString()}}</td>
			                    		<td><strong>{{$lead->subject}}</strong></td> 
						                <td>
						                	<form method="post" action="/admin/lead/{{$lead->id}}">
						                   		<input name="_method" type="hidden" value="put">	
						                   		<?php
						                   			$status = array("new_lead", "closed_lost", "closed_won", "in_discussion") ;
						                   		?>
						                    	<div class="input-group input-group-sm">
						                    		<select class="form-control  input-sm" name="status">
							                    		@foreach($status as $x)
								                    		@if($x == $lead->status)
																<option value="{{$x}}" selected="selected">{{$x}}</option>
															@else
																<option value="{{$x}}">{{$x}}</option>
															@endif
														@endforeach
													</select>
				                    				<span class="input-group-btn">
				                    					<button class="btn btn-default crud-btn" >update</button> 
				                    				</span>
				                    			</div>
							            	</form>	
						                    	<!-- <a href="" class="btn btn-success btn-sm"> Update</a> -->
						                    <!-- </div> -->
						                </td>
					                    <td class="text-right">
					                    	<form name = "myForm" method="post" style="display:inline-block;" action="/admin/lead/{{$lead->id}}" onsubmit="return deleteUser();">
				    					    	<input type="hidden" name="_method" value="DELETE" /> 
				            					<button class="btn btn-danger btn-sm crud-btn">Delete</button>  
						            		</form>
					                    	<!-- <a href="" class="btn btn-success btn-sm"> View All Conversation</a> -->
					                    	<form name = "myForm" method="get" style="display:inline-block;" action="/admin/lead/viewconversation/{{$lead->id}}">
						            					<button class="btn btn-success btn-sm crud-btn" >View </button>  
						            				</form>	
					                    	<a href="/admin/lead/convert-lead/{{$lead->id}}" class="btn btn-info btn-sm crud-btn"> Convert Lead</a>
					                    </td>
					                    
					                	</tr>
					            @endforeach
					        @endif

			            	</tbody>
				   	 	</table>
				   	</div>
				</div>
			</div>
		</div>
	</section>
@stop
@section('js')
@parent
	<script>
		
		function deleteUser() {
			var x = confirm("Are you sure you want to delete?");
			if (x)
				return true;
			else
				return false;
		}
	</script>
@endsection

