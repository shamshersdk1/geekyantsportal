@extends('layouts.admin-dashboard')
@section('main-content')
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-12">
	                <h1 class="admin-page-title">Lead Managment</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
					  	<li class="active">New Lead</li>
	        		</ol>
	            </div>
	            <!-- <div class="col-sm-4 text-right m-t-10">
	                <a href="/admin/cms-technology/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i> Add new Technology</a>
	            </div> -->
	        </div>
	    </div>
	    <div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="row">
		        	@if(!empty($errors->all()))
			            <div class="alert alert-danger">
			                @foreach ($errors->all() as $error)
			                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			                    <span>{{ $error }}</span><br/>
			                  @endforeach
			            </div>
			        @endif
			        @if (session('message'))
			            <div class="alert alert-success">
			                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			                <span>{{ session('message') }}</span><br/>
			            </div>
			        @endif
				</div>
			</div>
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="panel panel-default panel-leadmanagment">
						
						<!-- <div class="panel-heading">
						    <h3 class="panel-title">Lead Managment</h3>
						</div> -->
					  	<div class="panel-body custom-space">
					    	<div class="row">
					    		<div class="col-md-12">
					    			<form class="form-horizontal" method="post" action="/admin/lead">
									  	<div class="form-group">
									    	<label for="inputEmail3" class="col-sm-2 control-label">Name</label>
									    	<div class="col-sm-9">
									      		<input type="text" class="form-control" name="name" id="inputEmail3" placeholder="Name" value="{{Request::old('name')}}">
									    	</div>
									  	</div>
									  	<div class="form-group">
									    	<label for="inputEmail3" class="col-sm-2 control-label">Email</label>
									    	<div class="col-sm-9">
									      		<input type="email" class="form-control" name="email" id="inputEmail3" placeholder="Email" value="{{Request::old('email')}}">
									    	</div>
									  	</div>
									  	<div class="form-group">
									    	<label for="inputEmail3" class="col-sm-2 control-label">Subject</label>
									    	<div class="col-sm-9">
									      		<input type="text" class="form-control" name="subject" id="inputEmail3" placeholder="Subject" value="{{Request::old('subject')}}">
									    	</div>
									  	</div>
									  	<div class="form-group">
									    	<label for="source" class="col-sm-2 control-label">Source</label>
									    	<div class="col-sm-9">
									      		<input type="text" class="form-control" name="source" id="source" placeholder="Source" value="{{Request::old('source')}}">
									    	</div>
									  	</div>
									  	<div class="form-group">
									    	<label for="inputEmail3" class="col-sm-2 control-label">Message</label>
									    	<div class="col-sm-9">
									      		<textarea class="form-control" rows="5" name="message" id="comment" style="resize:none;" value="{{Request::old('message')}}"></textarea>
									    	</div>
									  	</div>
									  	<div class="col-md-6 col-md-offset-3">
									  		<button type="submit" class="btn btn-success btn-block">Send</button>
									  	</div>
									  	
									</form>
					    		</div>
					    	</div>
					  	</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop