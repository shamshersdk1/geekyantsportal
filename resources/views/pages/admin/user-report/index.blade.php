@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
        <div class="breadcrumb-wrap">
        	<div class="row">
                <div class="col-sm-8">
                    <h1 class="admin-page-title">User Report</h1>
                    <ol class="breadcrumb">
            		  	<li><a href="/admin">Admin</a></li>
            		  	<li>User Reports</li>
            		</ol>
                </div>
                <div class="col-sm-4">
                    <div class="input-group m-t-10">
                       
                    </div>
                </div>
            </div>
    	</div>
    
        <div class="row">
            <div class="col-md-12">
                @if(!empty($errors->all()))
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <span>{{ $error }}</span><br/>
                        @endforeach
                    </div>
                @endif
                @if (session('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ session('message') }}</span><br/>
                    </div>
                @endif
            </div>
        </div>

        <div class="user-list-view">
        <div class="panel panel-default">
            <table class="table table-striped" id="user-report">
                <thead>
                <tr>
                    <th class="text-left">#</th>
                    <th class="text-left">Employee Id</th>
                    <th class="text-left">Employee Name</th>
                    <th class="text-left">Email</th>
                    <th class="text-left">Status</th>
                    <th class="text-left">Joining Date</th>
                    <th class="text-left">Confimation Date</th>
                    <th class="text-left">Action</th>
                </tr>
                </thead>
                @if( empty($userList) || count($userList) == 0 )
                    <tr>
                        <td class="text-left" colspan="4">No Records found</td>
                        </tr>
                @endif
                @if(isset($userList) > 0)
                    @foreach($userList as $index=>$user)
                        <tr>
                            <td class="text-left">{{ $index+1}}</td>
                            <td class="text-left"> 
                               {{$user->employee_id}}
                            </td>
                            <td class="text-left">
                                {{$user->name}}
                            </td>
                            <td class="text-left">
                                {{$user->email}}
                            </td>
                            <td class="text-left">
                                @if($user->is_active)
                                    <span class="label label-success">Active<span>
                                @else	
                                    <span class="label label-danger">Deactivated<span>
                                @endif
                            </td>
                            <td class="text-left">
                                {{date_in_view($user->joining_date)}}
                            </td>
                            <td class="text-left">
                                {{date_in_view($user->confirmation_date)}}
                            </td>
                            <td class="pull-right">
                                
                                <span>
                                    <form name="showForm" method="get" action="/admin/user-report/{{$user->id}}"  style="display: inline-block;">
                                        <button type="submit" class="btn btn-warning btn-sm crude-btn"><i class="fa fa-eye fa-fw"></i>View</button> 
                                    </form>
                                <span>
                            </td>
                        </tr>
                    @endforeach
                @endif  
             </table>
        </div>
    </div>
    
</div>

<script>
 $(document).ready(function() {
	 $tableHeight = $( window ).height();
		var t = $('#user-report').DataTable( {
            pageLength:500, 
            "ordering": false,
			scrollY: $tableHeight - 200,
			scrollX: true,
			scrollCollapse: true,
			fixedColumns:   {
				leftColumns: 3
			}
            
        } );
});
</script>

@endsection
