@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
        <div class="breadcrumb-wrap">
        	<div class="row">
                <div class="col-sm-8">
                    <h1 class="admin-page-title">User Report</h1>
                    <ol class="breadcrumb">
            		  	<li><a href="/admin">Admin</a></li>
                        <li><a href="/admin/user-report">User Report</a></li>
            		  	<li><strong>{{$user->name}}</strong></li>
            		</ol>
                </div>
                <div class="col-sm-4">
                    <div class="col-md-12 float-right " style="background-color: white;padding:0px;">
                        @if(isset($userList))
                        <select id="selectid2" name="month"  placeholder= "{{$user ? $user->name : 'Select User'}}">
                            <option value=""></option>
                            @foreach($userList as $x)
                                <option value="{{$x->id}}" >{{$x->name}}</option>
                            @endforeach
                        </select>
                        @endif
                    </div>
                </div>
            </div>
    	</div>
    
        <div class="row">
            <div class="col-md-12">
                @if(!empty($errors->all()))
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <span>{{ $error }}</span><br/>
                        @endforeach
                    </div>
                @endif
                @if (session('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ session('message') }}</span><br/>
                    </div>
                @endif
            </div>
        </div>
    <div class="row">
        <div class="col-md-6">
            <h4>Team Structure</h4>
            <div class="panel panel-default">
                <div class="panel-body">
                    <ul class="tree">
                        @if($user->reportingManager && $user->reportingManager->reportingManager)
                            <li>
                                <input type="checkbox" checked="checked" id="{{$user->id}}" />
                                <label class="tree_label" for="{{$user->id}}">
                                    {{$user->reportingManager->reportingManager->name}}
                                    <span class="label label-success">RM</span>
                                </label>
                            </li>
                        @endif
                        @if($user->reportingManager)
                            <li>
                                <input type="checkbox" checked="checked" id="rm-{{$user->id}}" />
                                <label class="tree_label" for="rm-{{$user->id}}">
                                    <img src="{{$user->reportingManager->photo}} " />{{$user->reportingManager->name}} 
                                    <span class="label label-success">RM</span>
                                </label>
                                <ul>
                                    @if($user)
                                        <li>
                                            <input type="checkbox" checked="checked" id="user-{{$user->id}}" />
                                            <label class="tree_label" for="user-{{$user->id}}">
                                                <h4>{{$user->name}}</h4>
                                            </label>
                                            <ul>
                                                @foreach($user->reportees as $reportee)
                                                    <li>
                                                        <span class="tree_label">{{$reportee->name}}</span>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </li>
                                    @endif
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <h4>Assigned Projects</h4>
            @if(!empty($tlProjects))
                @foreach($tlProjects as $tlProject)
                    <div class="panel panel-default">
                        <div class="panel-heading bg-white clearfix">
                            <h4 class="panel-title pull-left">
                                <a href="/admin/project/{{$tlProject['project']->id}}/dashboard" target="_blank">#{{$tlProject['project']->id}}
                                {{$tlProject['project']->project_name}}</a>
                            </h4>
                            <label class="label label-primary custom-label pull-right">Team Lead</label>
                        </div>
                        <table class="table table-condensed">
                        @foreach($tlProject['resources'] as $resource)
                            <tr>
                                <td width="2%"></td>
                                <td width="50%"><label>{{$resource->resource->name}}</label></td>
                                <td width="48%">
                                    <small>{{date_in_view($resource->start_date)}}
                                    @if($resource->end_date)
                                        - {{date_in_view($resource->end_date)}}
                                    @endif
                                    </small>
                                </td>
                            </tr>
                        @endforeach
                        </table>
                    </div>
                @endforeach
            @else 
                <div class="panel panel-default">
                    <div class="panel-body text-center">
                        <h4 class="text-muted">No Project found</h4>
                    </div>
                </div>
            @endif
            @if(!empty($userProjects))
                @foreach($userProjects as $userProject)
                    <div class="panel panel-default">
                        <div class="panel-heading bg-white clearfix">
                            <h4 class="panel-title pull-left"><a href="{{$userProject->project->project_url}} ">
                                {{$userProject->project->id}} {{$userProject->project->project_name}}
                            </a>
                            </h4>
                            <label class="label label-success custom-label pull-right">Developer</label>
                        </div>
                    </div>
                @endforeach
            @else
                <div class="panel panel-default">
                    <div class="panel-body text-center">
                        <h4 class="text-muted">No Project found</h4>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>

<script>
    $('#selectid2').change(function(){
        var optionSelected = $("option:selected", this);
        optionValue = this.value;
        if (optionValue) { 
            window.location = "/admin/user-report/"+optionValue; 
        }
    });
</script>

@endsection
