@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">Request Approvals</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
		  			<li class="active">Request Approvals Dashboard</li>
        		</ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
				<a href="/admin/request-approval/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>New Request</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
	    	@if(!empty($errors->all()))
	            <div class="alert alert-danger">
	                @foreach ($errors->all() as $error)
	                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                    <span>{{ $error }}</span><br/>
	                  @endforeach
	            </div>
	        @endif
	        @if (session('message'))
	            <div class="alert alert-success">
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ session('message') }}</span><br/>
	            </div>
			@endif
		</div>
		<div class="user-list-view">
            <div class="panel panel-default">
            	<table class="table table-striped">
					<tr>
						<th width="5%" class="sorting_asc">#</th>
						<th width="16%">Requested By</th>
						<th width="16%">Description</th>
						<th width="16%">Comment</th>
						<th width="16%">Status</th>
						<th width="16%">Created Date</th>
						<th class="text-right sorting_asc" width="18%">Actions</th>
					</tr>
					@if(count($requestApprovals) > 0)
						@foreach($requestApprovals as $index => $requestApproval)
							<tr>
								<td class="td-text"><a>{{$requestApproval->id}}</a></td>
								<td class="td-text">{{$requestApproval->requestedBy->name}}</td>
								<td class="td-text">{{$requestApproval->description}}</td>
								<td class="td-text">{{$requestApproval->comment}}</td>
								<td class="td-text">
									@if($requestApproval->status == 'pending')
										<span class="label label-info custom-label">Pending</span>
									@elseif ($requestApproval->status == 'approved')
										<span class="label label-success custom-label">Approved</span>
									@else
										<span class="label label-danger custom-label">Rejected</span>
									@endif
								</td>
								<td class="td-text">{{date_in_view($requestApproval->created_at)}}</td>
								<td class="text-right">
									<a href="/admin/request-approval/{{$requestApproval->id}}" class="btn btn-success crude-btn btn-sm">View</a>
									@if( !empty($allowEdit) && $allowEdit && $requestApproval->status == 'pending' )
									<a href="/admin/request-approval/{{$requestApproval->id}}/edit" class="btn btn-primary crude-btn btn-sm">Edit</a>
									@endif
								</td>
							</tr>
						@endforeach
	            	@else
	            		<tr>
	            			<td colspan="3" class="text-center">No results found.</td>
	            		</tr>
	            	@endif
            	</table>
				<div class="col-md-12">
                    <div class="pageination pull-right">
                        <nav aria-label="Page navigation">
                              <ul class="pagination">
                                {{ $requestApprovals->render() }}
                              </ul>
                        </nav>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>
</div>
<script>
    $(function(){
        toggle = function (item) {
			//window.location.href='users-billable/'+item.value;
			$.ajax({
				type:'POST',
				url:'/api/v1/tech-events/status',
				data:{id:item.value},
				success:function(data){
					console.log(data);
					}
			});
		}
    });
</script>
@endsection
