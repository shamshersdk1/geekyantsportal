@extends('layouts.admin-dashboard')
@section('main-content')

<section class="new-project-section">
	<div class="container-fluid">
		<div class="breadcrumb-wrap">
	    	<div class="row">
	            <div class="col-sm-8">
	                <h1 class="admin-page-title">Request Approval</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/admin">Admin</a></li>
			  			<li><a href="{{ url('admin/request-approval') }}">Request Approval</a></li>
			  			<li class="active">{{ $requestApprovalObj->id }}</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        </div>
	    </div>

	    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif

		<form class="form-horizontal" method="post" action="/admin/request-approval/{{$requestApprovalObj->id}}" enctype="multipart/form-data">
			<input name="_method" type="hidden" value="PUT" />
			<div class="panel panel-default">
				<div class="panel-body">
				    <div class="row">
				    	<div class="col-md-12">
							<div class="form-group">
						    	<label for="" class="col-sm-2 control-label">Description</label>
						    	<div class="col-sm-6">
									  <input type="text" class="form-control" id="description" name="description" value="{{ $requestApprovalObj->description }}">
						    	</div>
							  </div>
							  
						  	<div class="form-group">
									<label for="" class="col-sm-2 control-label">Request to:</label>
									<div class="multipicker col-md-4">
										<div class="">
											<select class="js-example-basic-multiple22 " multiple="multiple" name="request_to[]" required>
												@foreach($userList as $user)
													<?php
														$saved = false;
														foreach($requestUsers as $requestUser){
															if($requestUser == $user->id){
																$saved = true;
															}
														}
													?>
													@if(isset($requestUsers))
														@if($saved)
															<option value="{{$user->id}}" selected >{{$user->name}}</option>
														@else
															<option value="{{$user->id}}">{{$user->name}}</option>
														@endif
													@else
														<option value="{{$user->id}}">{{$user->name}}</option>
													@endif
												@endforeach
											</select>
										</div>
									</div>
								</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Comment :</label>
								<div class="col-sm-4">
									<textarea class="form-control" id="comment" name="comment" rows="4" placeholder="Enter details">{{$requestApprovalObj->comment}}</textarea>
								</div>
							</div>
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">Notify to:</label>
								<div class="multipicker col-md-4">
									<div class="">
										<select class="js-example-basic-multiple22 " multiple="multiple" name="notify_to[]">
											@foreach($userList as $user)
												<?php
													$saved = false;
													foreach($notifyUsers as $notifyUser){
														if($notifyUser == $user->id){
															$saved = true;
														}
													}
												?>
												@if(isset($notifyUsers))
													@if($saved)
														<option value="{{$user->id}}" selected >{{$user->name}}</option>
													@else
														<option value="{{$user->id}}">{{$user->name}}</option>
													@endif
												@else
													<option value="{{$user->id}}">{{$user->name}}</option>
												@endif
											@endforeach
										</select>
									</div>
								</div>
							</div>
					  	</div>
					</div>
				</div>
			</div>
  	  		<div class="text-center">
		  		<button type="submit" class="btn btn-success">Update Request</button>
		  	</div>
		</form>
	</div>
</section>
@endsection
