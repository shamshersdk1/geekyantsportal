@extends('layouts.admin-dashboard')
@section('main-content')
<section>
   <div class="container-fluid">
      <div class="breadcrumb-wrap">
         <div class="row">
            <div class="col-sm-6">
               <h1>Request Approval</h1>
               <ol class="breadcrumb">
                  <li><a href="/admin">Admin</a></li>
                  <li><a href="/admin/request-approval">Request Approvals</a></li>
                  <li class="active">{{$requestApprovalObj->id}}</li>
               </ol>
            </div>
         </div>
      </div>
      @if(!empty($errors->all()))
      <div class="alert alert-danger">
         @foreach ($errors->all() as $error)
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
         <span>{{ $error }}</span><br/>
         @endforeach
      </div>
      @endif
      @if (session('message'))
      <div class="alert alert-success">
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
         <span>{{ session('message') }}</span><br/>
      </div>
      @endif
      <div class="row cms-dev">
         <div class="col-md-12">
            <h4 class="col-md-9">Request Approval Details</h4>
            @if( $myRequest && $requestApprovalObj->status == 'pending' && !$responded )
            <div class="col-md-3">
                <button id="{{$requestApprovalObj->id}}" name="{{$requestApprovalObj->id}}" class="btn btn-success crude-btn btn-sm" onclick="approve(this)">Approve</button>
                <button id="{{$requestApprovalObj->id}}" name="{{$requestApprovalObj->id}}" class="btn btn-danger crude-btn btn-sm" onclick="reject(this)">Reject</button>
            </div>
            @endif
            <div class="panel panel-default">
               <div class="panel-body row">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <div class=" col-md-6">
                     <label>Desctiption : </label>
                     <span>{{$requestApprovalObj->description}}</span>
                  </div>
                  <div class=" col-md-6">
                     <label>Requested By: </label>
                     <span>{{$requestApprovalObj->requestedBy->name}}</span>
                  </div>
                  <div class=" col-md-6">
                     <label>Status : </label>
                     <span>{{ ucfirst($requestApprovalObj->status)  }}</span>
                  </div>
                  <div class=" col-md-6">
                    <label>Comment : </label>
                    <span>{{ ucfirst($requestApprovalObj->comment)  }}</span>
                 </div>
               </div>
              </div>
               <div class="panel panel-default">
                  <table class="table table-striped">
                      <tr>
                          <th>Approver</th>
                          <th>Status</th>
                      </tr>
                      @foreach( $requestApprovalObj->approvalUsers as $approval )
                      <tr>
                          <td class="td-text">{{ $approval->user->name }}</td>
                          <td class="td-text">
                            @if($approval->status == 'pending')
                              <span class="label label-info custom-label">Pending</span>
                            @elseif ($approval->status == 'approved')
                              <span class="label label-success custom-label">Approved</span>
                            @else 
                              <span class="label label-danger custom-label">Rejected</span>
                            @endif
                          </td>
                      </tr>
                      @endforeach
                  </table>
               </div>
            
         </div>
      </div>
   </div>
</section>
<script>
    $(function(){
        approve = function (item) {
        var confirmed = confirm('Are you sure?');
        if ( confirmed == true )
        {
          console.log('id', item.id);
          $.ajax({
            type:'POST',
            url:'/api/v1/request-approval/approve',
            data:{id:item.id},
            success:function(data){
              window.location.reload();
              }
          });
        }
      }
    });
    $(function(){
      reject = function (item) {
      var confirmed = confirm('Are you sure?');
      if ( confirmed == true )
      {
        console.log('id', item.id);
        $.ajax({
          type:'POST',
          url:'/api/v1/request-approval/reject',
          data:{id:item.id},
          success:function(data){
            window.location.reload();
            }
        });
      }
    }
  });
</script>
@endsection