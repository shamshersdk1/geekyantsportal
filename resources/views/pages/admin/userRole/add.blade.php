@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
        	
        	@if(!empty($errors->all()))
						<div class="alert alert-danger">
							@foreach ($errors->all() as $error)
								<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
								<span>{{ $error }}</span><br/>
						  	@endforeach
						</div>
					@endif
					@if (session('message'))
					    <div class="alert alert-success">
					    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					        <span>{{ session('message') }}</span><br/>
					    </div>
					@endif
					
        	@if(!isset($userRole))
            <div class="panel panel-default" style="margin-top:20px;">
				    	<form name = "myForm" style="padding:10px;" method="post" action="/admin/user-role" enctype="multipart/form-data">
				    			<input type="hidden" name="_token" value="{{ csrf_token() }}">
				    			
				    			<div class="form-group">
				    			    <label for="Role">Role:</label>
				    			    <input type="text" name="role" class="form-control" id="role" placeholder="role">
				    			    <p id="role" style="color:red;"></p>
				    			</div>
				    			
				    			<div class="form-group">
				    			    <label for="description">Enter Short Description:</label>
				    			    <input type="text" name="description" class="form-control" id="description" placeholder="Short Description">
				    			    <p id="short_description" style="color:red;"></p>
				    			</div>
				    			
				    			<button type="submit" class="btn btn-success">Add</button>
				    	</form>

			</div>

			@else

					<form name = "myForm" style="padding:10px;" method="post" action="/admin/user-role/{{$userRole->id}}" enctype="multipart/form-data" onsubmit="return validateForm()" > 
					<input type="hidden" name="_method" value="put" /> 
						<div class="form-group">
							<label for="exampleInputEmail1">Role:</label>
							<input type="text" name="role" class="form-control" id="exampleInputEmail1" value="{{$userRole->role}}">
							<p id="role" style="color:red;"></p>
						</div>
														
						<div class="form-group">
							<label for="exampleInputEmail1">Enter Short Description:</label>
							<input type="text" name="description" class="form-control" id="exampleInputEmail1" value="{{$userRole->description}}">
							<p id="short_description" style="color:red;"></p>
						</div>
														
						<button type="submit" class="btn btn-success">Update</button>
					</form>


			@endif
		</div>
			
	</div>
</div>
@endsection
