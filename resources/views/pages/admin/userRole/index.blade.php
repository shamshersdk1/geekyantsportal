@extends('layouts.admin-dashboard')
@section('main-content')
	<div class="container">
	    <div class="row">
	        <div class="col-md-10 col-md-offset-1">
	            <div class="panel panel-default">
					
					    <div class="panel-body">
					    @if(!empty($errors->all()))
							<div class="alert alert-danger">
								@foreach ($errors->all() as $error)
									<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									<span>{{ $error }}</span><br/>
							  	@endforeach
							</div>
						@endif
						@if (session('message'))
						    <div class="alert alert-success">
						    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						        <span>{{ session('message') }}</span><br/>
						    </div>
						@endif
					    	<table class="table">
			            		<th>#</th>
			            		<th>Name of Roles</th>
			            		<th style="padding-right:75px;">Actions</th>
			            		@if(count($userRole) > 0)
				            		@foreach($userRole as $role)
					            		<tr>
					            			<td class="td-text"><a>{{$role->id}}</a></td>
					            			<td class="td-text"><a href="/admin/technology/{{$role->id}}"> {{$role->role}}</a></td>
					            			<td>
					            			<!-- <form style="display:inline-block;">
					            				<a href="" class="btn btn-success"> Edit</a>
					            			</form>
					            			<form style="display:inline-block;">
					            				<a href="" class="btn btn-danger" type="submit">Delete</a>
					            			</form> -->
					            				<div class="list-inline">			    				    		
				    					    	
				    					    	<form name = "myForm"  method="get" style="display:inline-block;" action="/admin/user-role/{{$role->id}}/edit">
				    					    	
					            					<button class="btn btn-info">Edit</button>  
					            				</form>	

				    					    	<form name = "myForm" method="post" style="display:inline-block;" action="/admin/user-role/{{$role->id}}">
				    					    	<input type="hidden" name="_method" value="DELETE" /> 
					            					<button class="btn btn-danger">Delete</button>  
					            				</form>	
					            				</div>
					            				
					            			</td>
					            		</tr>
				            		@endforeach
				            	@else
				            		<tr>
				            			<td colspan="3">No results found.</td>
				            			<td></td>
				            			<td></td>
				            		</tr>
				            	@endif
			            	</table>
					    </div>
					
				</div>

				<a href="/admin/project"><button class="btn btn-primary">Back</button></a>
				<a href="/admin/user-role/add"><button class="btn btn-success">Add new Role</button></a>
				
			</div>	
		</div>
	</div>
@endsection
