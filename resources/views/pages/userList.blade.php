@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title">List of Users</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li><a href="{{ url('admin/users') }}">List of Users</a></li>
        		</ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <a href="/admin/users/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Add new User</a>
            </div>

		</div>
	</div>
	<div class="row bg-white search-section-top" >
		<div class="col-md-3">
			<form action="/admin/users" method="GET">
				@if( !empty($search) )
            		Showing results for
            	@endif
				<input type="text" name="searchuser" class="form-control" placeholder="search.." value="@if(isset($search)){{$search}}@endif">
				<input type="hidden" name="filter" value="all">
			</form>
		</div>
		<div class="col-md-9">
                <ul class="btn-group" style="padding:0;float:right">
                    <a href="/admin/users?@if(strpos(Request::getQueryString(), 'searchuser') !== false)searchuser={{str_replace(' ','+',Request::get('searchuser'))}}@endif" type="button" class="btn btn-default @if (!Request::get('filter')&&!Request::get('filter')) btn-success @endif" >Active</a>
                    <a href="/admin/users?@if(strpos(Request::getQueryString(), 'searchuser') !== false)searchuser={{str_replace(' ','+',Request::get('searchuser'))}}&@endif @if(strpos(Request::getQueryString(), 'filter') !== false)filter=deactive @endif&@if(empty(Request::get('filter')))filter=deactive @endif" type="button" class="btn btn-default @if( preg_replace('/[\s]+.*/','',Request::get('filter')) == 'deactive')btn-success @endif">Deactivated User</a>
					<a href="/admin/users?@if(strpos(Request::getQueryString(), 'searchuser') !== false)searchuser={{str_replace(' ','+',Request::get('searchuser'))}}&@endif @if(strpos(Request::getQueryString(), 'filter') !== false)filter=unconfirmed @endif&@if(empty(Request::get('filter')))filter=unconfirmed @endif" type="button" class="btn btn-default @if( preg_replace('/[\s]+.*/','',Request::get('filter')) == 'unconfirmed')btn-success @endif">Unconfirmed</a>
                    <a href="/admin/users?@if(strpos(Request::getQueryString(), 'searchuser') !== false)searchuser={{str_replace(' ','+',Request::get('searchuser'))}}&@endif @if(strpos(Request::getQueryString(), 'filter') !== false)filter=billable @endif&@if(empty(Request::get('filter')))filter=billable @endif" type="button" class="btn btn-default @if( preg_replace('/[\s]+.*/','',Request::get('filter')) == 'billable') btn-success @endif">Billable</a>
					<a href="/admin/users?@if(strpos(Request::getQueryString(), 'searchuser') !== false)searchuser={{str_replace(' ','+',Request::get('searchuser'))}}&@endif @if(strpos(Request::getQueryString(), 'filter') !== false)filter=non_billable @endif&@if(empty(Request::get('filter')))filter=non_billable @endif" type="button" class="btn btn-default @if( preg_replace('/[\s]+.*/','',Request::get('filter')) == 'non_billable') btn-success @endif">Non Billable</a>
					<a href="/admin/users?@if(strpos(Request::getQueryString(), 'searchuser') !== false)searchuser={{str_replace(' ','+',Request::get('searchuser'))}}&@endif @if(strpos(Request::getQueryString(), 'filter') !== false)filter=admin @endif&@if(empty(Request::get('filter')))filter=admin @endif" type="button" class="btn btn-default @if(preg_replace('/[\s]+.*/','',Request::get('filter')) == 'admin') btn-success @endif">Admin</a>
					<a href="/admin/users?@if(strpos(Request::getQueryString(), 'searchuser') !== false)searchuser={{str_replace(' ','+',Request::get('searchuser'))}}&@endif @if(strpos(Request::getQueryString(), 'filter') !== false)filter=all @endif&@if(empty(Request::get('filter')))filter=all @endif" type="button" class="btn btn-default @if(preg_replace('/[\s]+.*/','',Request::get('filter')) == 'all') btn-success @endif">All</a>
                </ul>
                <h4 style="float:right">Filters:&nbsp&nbsp </h4>
            </div>
	</div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
	<div class="user-list-view">
	<div class="panel panel-default ">
		<table class="table table-striped" id="user-table">
		<thead>
			<th>#</th>
			<th>Emp	 #</th>
			<th>Name of Employee</th>
			<th>Role</th>
			<th>E-Mail</th>
			<th>Status</th>
			<th>Total Experience </th>
			<th>Billable </th>
			<th>Feedback Review </th>
			<th>Slack Timesheet Notification</th>
			<th class="text-right">Actions</th>
		</thead>
			@if(count($usersList) > 0)
	    		@foreach($usersList as $index => $user)
					<tr>
						<td class="td-text">
							{{ $index + 1 }}
						</td>
						<td class="td-text">
							<a href="/admin/users/{{$user->id}}/profile">{{!empty($user->employee_id) ? $user->employee_id : "--"}}</a>
						</td>
						<td class="td-text">
							<a href="/admin/users/{{$user->id}}/profile">{{$user->name}}</a>
						</td>
						<td class="td-text">
							{{ucfirst($user->role)}}
						</td>
						<td class="td-text">
							{{$user->email}}
						</td>
						<td class="td-text">
							@if($user->is_active == 1)
							<span class="label label-success custom-label">Active</span>@if(!empty($user->joining_date))<br><small>from {{date_in_view($user->joining_date)}}</small>@endif
							@else
							<span class="label label-danger custom-label">Deactivated</span>@if(!empty($user->release_date))<br><small>on {{date_in_view($user->release_date)}}</small>@endif
							@endif
						</td>
						@if($user->confirmation_date )
						<td class="td-text">
							{{$user->getExperience()}}
						</td>
						@elseif(!$user->confirmation_date)
						<td class="td-text">
							<span class="label label-primary custom-label">Not yet confirmed</span>
						</td>
						@endif
						<td>
								<div class="col-md-4">
								<div class="onoffswitch">

									<input type="checkbox" value="{{$user->id}}" name="onoffswitch[]" onchange="toggle(this)"
									class="onoffswitch-checkbox" id="{{$user->id}}"
									<?php echo (!empty($user->is_billable) && $user->is_billable == 1) ? 'checked' : ''; ?>>
									<label class="onoffswitch-label" for= {{$user->id}} >
										<span class="onoffswitch-inner"></span>
										<span class="onoffswitch-switch"></span>
									</label>
								</div>
							</div>
						</td>
						<td>
							<div class="col-md-4">
								<div class="onoffswitch">
									<input type="checkbox" value="feedback{{$user->id}}" name="onoffswitch[]" onchange="toggleFeedback(this)"
									class="onoffswitch-checkbox" id="feedback{{$user->id}}"
									<?php echo (!empty($user->is_feedback_review) && $user->is_feedback_review == 1) ? 'checked' : ''; ?>>
									<label class="onoffswitch-label" for= "feedback{{$user->id}}" >
										<span class="onoffswitch-inner"></span>
										<span class="onoffswitch-switch"></span>
									</label>
								</div>
							</div>
						</td>
						<td>
							<div class="col-md-4">
								<div class="onoffswitch">
									<input type="checkbox" value="notification{{$user->id}}" name="onoffswitch[]" onchange="toggleNotification(this)"
									class="onoffswitch-checkbox" id="notification{{$user->id}}"
									<?php echo (empty($user->RMNotificationReminder) || (!empty($user->RMNotificationReminder) && $user->RMNotificationReminder->value == 1)) ? 'checked' : ''; ?>>
									<label class="onoffswitch-label" for= "notification{{$user->id}}" >
										<span class="onoffswitch-inner"></span>
										<span class="onoffswitch-switch"></span>
									</label>
								</div>
							</div>
						</td>
						<td class="text-right">
							<a href="/admin/users/{{$user->id}}/financial-transactions" class="btn btn-info btn-sm crud-btn" style="display: inline-block;"><i class="fa fa-eye"></i> View Financial Transactions</a>
							<a href="/admin/users/{{$user->id}}/profile" class="btn btn-info btn-sm crud-btn" style="display: inline-block;"><i class="fa fa-eye"></i> View</a>
							<a href="users/{{$user->id}}/edit" class="btn btn-primary btn-sm crud-btn" style="display: inline-block;"><i class="fa fa-cog"></i> Settings</a>
							<a href="users/{{$user->id}}/attendance-register" class="btn btn-warning btn-sm " style="display: inline-block;"><i class="fa fa-file-pdf-o" style="margin-right:5px;" ></i> View Attendance</a>
							<a href="users/{{$user->id}}/tds-summary" class="btn btn-warning btn-sm " style="display: inline-block;"><i class="fa fa-file-pdf-o" style="margin-right:5px;" ></i>TDS Summary</a>
						</td>
					</tr>
				@endforeach
	    	@else
	    		<tr>
	    			<td colspan="3">No results found.</td>
	    			<td></td>
	    			<td></td>
	    		</tr>
	    	@endif
	   </table>
	</div>
	</div>
	<div class="pull-right">
		{{ $usersList->links() }}
	</div>
<script>
	$(document).ready(function() {
		$tableHeight = $( window ).height();
			var t = $('#user-table').DataTable( {
				pageLength:500, 
				"ordering": false,
				scrollY: $tableHeight - 200,
				scrollX: true,
				scrollCollapse: true,
				fixedColumns:   {
					leftColumns: 3
				}
			} );
	})
    $(function(){
        toggle = function (item) {
			alert(item.value);
			//window.location.href='users-billable/'+item.value;
			$.ajax({
				type:'POST',
				url:'/api/v1/user/billable',
				data:{id:item.value},
				success:function(data){
					console.log(data.success);
					}
			});
		}
	});
	$(function(){
        toggleFeedback = function (item) {
			var id  = item.value.replace('feedback','');
			$.ajax({
				type:'POST',
				url:'/api/v1/user/feedback-review',
				data:{id: id},
				success:function(data){
					console.log(data.success);
					}
			});
		}
	});
	$(function(){
        toggleNotification = function (item) {
			var id  = item.value.replace('notification','');
			$.ajax({
				type:'POST',
				url:'/api/v1/user/rm-notification',
				data:{id: id},
				success:function(data){
					console.log(data.success);
				}
			});
		}
    });
</script>
@endsection

