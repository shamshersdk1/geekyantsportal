@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-8">
                <h1 class="admin-page-title"></h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
        		  	<li><a href="{{ url('admin/users/dashboard') }}">List of Users</a></li>
					<li><a>Edit</a></li>
        		</ol>
            </div>
			<div class="col-sm-4 text-right m-t-10">
				<button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
			</div>
        </div>
    </div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        @foreach ($errors->all() as $error)
					<div class="alert alert-danger">
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
					</div>
		        @endforeach
		        
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>

    <div class="row">
	    <div class="col-md-12">
	    	@if(isset($user))
				<div class="panel panel-default custom-space">
					<div class="row">
						<form role="form" method="post" action="/admin/users/{{$user->id}}" id="floating-label" class="form-input">
							<input name="_method" type="hidden" value="put" />
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<div class="col-md-6">
								<div class="form-group">
									<input type="text" name="name" class="form-control" placeholder="Employee Name" required value="@if(!empty(old('name'))){{old('name')}}@else{{$user->name}}@endif">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
		
								    <input type="Email" name="email" class="form-control" placeholder="Employee Email" required value="{{$user->email}}" {{$user->is_active ? 'disabled':''}}>
								</div>
							</div>
							<div class="col-md-2">
								<label>Name :</label>
							</div>
							<div class="col-md-3">
								<div class="form-group">
								    <input type="text" class="form-control" placeholder="First Name" name="first_name" value="{{$user->first_name}}">
								</div>
							</div> 
							<div class="col-md-3">
								<div class="form-group">
								    <input type="text" class="form-control" placeholder="Middle Name" name="middle_name" value="{{$user->middle_name}}">
								</div>
							</div> 
							<div class="col-md-3">
								<div class="form-group">
								    <input type="text" class="form-control" placeholder="Last Name" name="last_name" value="{{$user->last_name}}">
								</div>
							</div> 
							<div class="row" style="padding-bottom: 15px;">
								<div class="col-md-12">
									<div class="col-md-2">
										<label>Access Level</label>
									</div>
									<div class="col-md-10">	
										<div class="row">
											@foreach ( $roles as $role )
												<div class="col-md-2">
													<div class="form-check form-check-inline">
														<label class="form-check-label">
															<input class="form-check-input" name="levels[]" type="checkbox" value="{{$role->id}}" @if(!empty(old('levels'))&&(in_array($role->id, old('levels')))) checked @elseif(in_array($role->id, $userRolesArray)) checked @endif>  {{$role->name}}
														</label>
													</div>
												</div>	
											@endforeach
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<label>Joining Date</label>
							</div>
							<div class="col-md-4">
								<div class="input-group date datetimepicker">
									<input type="text" name="join_date" class="form-control" placeholder="Joining Date" value="@if(!empty(old('join_date'))){{old('join_date')}}@else{{date_to_ddmmyyyy($user->joining_date)}}@endif"/>
									<span class="input-group-addon">
										<span class="fa fa-calendar"></span>
									</span>
								</div>
							</div>
							<div class="col-md-2">
								<label>Confirmation Date</label>
							</div>
							<div class="col-md-4">
								<div class="input-group date datetimepicker" id="datetimepickerEnd_search">
									<input type="text" name="confirmation_date" class="form-control" placeholder="Confirmation Date" value="@if(!empty(old('confirmation_date'))){{old('confirmation_date')}}@else{{date_to_ddmmyyyy($user->confirmation_date)}}@endif"/>
									<span class="input-group-addon">
										<span class="fa fa-calendar"></span>
									</span>
								</div>
							</div>
							<div class="col-md-2">
								<label>Release Date</label>
							</div>
							<div class="col-md-4">
								<div class="input-group date datetimepicker">
									<input type="text" name="release_date" class="form-control" placeholder="Release Date" value="@if(!empty(old('release_date'))){{old('release_date')}}@else{{date_to_ddmmyyyy($user->release_date)}}@endif"/>
									<span class="input-group-addon">
										<span class="fa fa-calendar"></span>
									</span>
								</div>
							</div>
							<div class="col-md-2">
								<label>Employee Id</label>
							</div>
							<div class="col-md-4">
								<div class="form-group">
								    <input type="text" name="employee_id" class="form-control" placeholder="Employee Id" value="@if(!empty(old('employee_id'))){{old('employee_id')}}@else{{$user->employee_id}}@endif">
								</div>
							</div> 
							<div class="col-md-2">
								<label>Date of Birth</label>
							</div>
							<div class="col-md-4">
								<div class="input-group date datetimepicker">
									<input type="text" name="date_of_birth" class="form-control" placeholder="Date of Birth" required value="@if(!empty(old('date_of_birth'))){{old('date_of_birth')}}@else{{date_to_ddmmyyyy($user->dob)}}@endif"/>
									<span class="input-group-addon">
										<span class="fa fa-calendar"></span>
									</span>
								</div>
							</div>
							<div class="col-md-2">
								<label>Reporting Manager Name:</label>
							</div>
							<div class="col-md-4">
								<div class="form-group">
								   <select id="selectid2" name="parent_id" style="width=35%;" placeholder= "Select an option">
								   	@if($parentList)
								   	<option value="{{$parentList->id}}">{{$parentList->name}}</option>
								   	@elseif(!$parentList)
								   	<option value=""></option>
								   	@endif
		                                @foreach($leaduser as $manager)
		                                	@if($manager->hasRole('reporting-manager'))
		                                		<option value="{{$manager->id}}">{{$manager->name}}</option>
		                                	@endif
		                                @endforeach
		                            </select>
								</div>
							</div>
							<div class="col-md-2">
								<label>Pan :</label>
							</div>
							<div class="col-md-4">
								<div class="form-group">
								    <input type="text" class="form-control" placeholder="" name="pan" value="{{$user->userDetails->pan}}">
								</div>
							</div> 
							<div class="col-md-2">
								<label>Bank A/c. No :</label>
							</div>
							<div class="col-md-4">
								<div class="form-group">
								    <input type="text" class="form-control" placeholder="" name="bank_ac_no" value="{{$user->userDetails->bank_ac_no}}">
								</div>
							</div> 
							<div class="col-md-2">
								<label>Bank IFSC Code :</label>
							</div>
							<div class="col-md-4">
								<div class="form-group">
								    <input type="text" class="form-control" placeholder="" name="bank_ifsc_code" value="{{$user->userDetails->bank_ifsc_code}}">
								</div>
							</div> 
							<div class="col-md-2">
								<label>PF A/c. No :</label>
							</div>
							<div class="col-md-4">
								<div class="form-group">
								    <input type="text" class="form-control" placeholder="" name="pf_no" value="{{$user->userDetails->pf_no}}">
								</div>
							</div> 
							<div class="col-md-2">
								<label>UAN :</label>
							</div>
							<div class="col-md-4">
								<div class="form-group">
								    <input type="text" class="form-control" placeholder="" name="uan_no" value="{{$user->userDetails->uan_no}}">
								</div>
							</div> 
							<div class="col-md-2">
								<label>ESI :</label>
							</div>
							<div class="col-md-4">
								<div class="form-group">
								    <input type="text" class="form-control" placeholder="" name="esi_no" value="{{$user->userDetails->esi_no}}"> 
								</div>
							</div> 
							<div class="col-md-2">
								<label>Aadhar No. :</label>
							</div>
							<div class="col-md-4">
								<div class="form-group">
								    <input type="text" class="form-control" placeholder="" name="aadhar_no" value="{{$user->userDetails->aadhar_no}}">
								</div>
							</div> 
							<div class="col-md-2">
								<label>Previous Experience :</label>
							</div>
							<div class="col-md-4">
								<div class="form-group">
								    <div class="row">
										<div class="col-sm-4">
											<select class="form-control" name="months">
												@for ($i = 0; $i <= 12 ; $i++)
													@if ( $i == $user->months )
														<option value="{{ $i }}" selected>{{ $i }}</option>
													@else
														<option value="{{ $i }}">{{ $i }}</option>
													@endif
												@endfor 
											</select>
										</div>
										<div class="col-sm-2">
											<h5 style="margin-left:-15px;">Month</h5>
										</div>
										<div class="col-sm-4">
										<select class="form-control" name="years">
												@for ($i = 0; $i <= 15 ; $i++)
													@if ( $i == $user->years )
														<option value="{{ $i }}" selected>{{ $i }}</option>
													@else
														<option value="{{ $i }}">{{ $i }}</option>
													@endif
												@endfor 
											</select>
										</div>
										<div class="col-sm-2">
											<h5 style="margin-left:-15px;">Year</h5>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<label>Gender :</label>
							</div>
							<div class="col-md-4">
								<select class="form-control" name="gender">
									<option value=""></option>
									@if ( $user->gender == 'male' )
										<option value="male" selected>Male</option>
									@else
										<option value="male">Male</option>
									@endif
									@if ( $user->gender == 'female' )
										<option value="female" selected>Female</option>
									@else
										<option value="female">Female</option>
									@endif
								</select>
							</div>
							<div class="col-md-2">
								<label>Designation:</label>
							</div>
							<div class="col-md-4">
								<div class="form-group">
								   <select id="selectid3" name="designation" style="width=35%;" placeholder= "Select an option">
										<option></option>
									   @foreach($designations as $designation)
									   		@if( $user->designation == $designation->id )
												<option value="{{$designation->id}}" selected>{{$designation->designation}}</option>
											@else
												<option value="{{$designation->id}}">{{$designation->designation}}</option>
											@endif
		                            	@endforeach
		                            </select>
								</div>
							</div>
						</div>
					</div>
					
					<div class="text-center">
						<button type="submit" class="btn btn-primary">Update</button>
					</form>
					<!-- <form name="deleteForm" method="post" action="/admin/users/{{$user->id}}" class="deleteForm" style="display: inline-block;">
						<input name="_method" type="hidden" value="delete" />
						<button type="submit" class="btn btn-danger crud-btn">Delete</button>
					</form> -->
					<form name="deleteForm" method="post" action="/admin/user-status/{{$user->id}}" style="display: inline-block;">
						<input name="_method" type="hidden" value="put" />
						<button class="{{$user->is_active? 'btn btn-danger ' : 'btn btn-success '}} " >{{$user->is_active? "De-Activate" : "Activate"}}</button>
					</form>
				</div>
            @else
                <label>Profile table is not linked</label>
            @endif
        </div>
    </div>
</div>
<script>
	// $(function(){
	// 	var d = new Date();
	// 	var strDate = d.getFullYear() + "-" + (d.getMonth()+1) + "-" + d.getDate();
	// 	$('#datetimepickerEnd_search').datetimepicker({
	// 		maxDate: strDate,
    //         format: 'DD-MM-YYYY',
    //         useCurrent:false
    //         });
    //     });
    $(function () {
		$('.datetimepicker').datetimepicker({
			format:'DD-MM-YYYY',
			useCurrent:false
		});
        <?php if (empty(old('group'))) {
            if (!empty($user->ctc->group->id)) {?>
                        $('#pgroup option[value="{{$user->ctc->group->id}}"]').attr("selected", true);
            <?php                                                                                                                                                                                                                                                                                                             }
		} else { ?>
		var val={{old('group')}};
					$('#pgroup option[value="'+val+'"]').attr("selected", true);
		<?php } ?>

		$('.deleteForm').submit(function(){
			return confirm('Are you sure you want to delete {{$user->name}}?');
		});
		@if(!empty(old('role')))
			var val = "{{old('role')}}";
		@else
			var val = "{{$user->role}}";
		@endif
		@if(!empty(old('parent_id')))
			var parent = "{{old('parent_id')}}";
		@elseif($parentList)
			var parent = "{{$parentList->id}}";
		@else
			var parent = "";
		@endif
		$("#roleSelect option[value='"+val+"']").prop('selected',true);
		$("#parentSelect option[value='"+parent+"']").prop('selected',true);
		$('.lead_list').select2({
  			placeholder: 'Select an option',
  			allowClear:true
		});
		//Education
  
    //define template
    var template = $('#sections .section:first').clone();
  
    //define counter
    var sectionsCount = 1;
  
    //add new section
    $('body').on('click', '.addsection', function() {
  
        //increment
        sectionsCount++;
  
        //loop through each input
        var section = template.clone().find(':input').each(function(){
  
            //set id to store the updated section number
            var newId = this.id + sectionsCount;
  
            //update for label
            $(this).prev().attr('for', newId);
  
            //update id
            this.id = newId;
  
        }).end()
  
        //inject new section
        .appendTo('#sections');
        return false;
    });
  
    //remove section
    $('#sections').on('click', '.remove', function() {
        //fade out section
        $(this).parent().fadeOut(300, function(){
            //remove parent element (main section)
            $(this).parent().parent().empty();
            return false;
        });
        return false;
    });
	});
</script>
@endsection
