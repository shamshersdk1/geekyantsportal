<!-- ==================== editUserList modal ========================= -->
<div class="modal-content">
  	<div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

	    <h4 class="modal-title text-center modal-heading">Edit Details of Employees</h4>
	</div>
	<div class="modal-body modal-login">
	    <div class="row">
	    	<div class="col-md-12">
	    		@if(!empty($errors->all()))
					<div class="alert alert-danger">
						@foreach ($errors->all() as $error)
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							<span>{{ $error }}</span><br/>
					  	@endforeach
					</div>
				@endif
	    	</div>
	    	<div class="col-md-6 col-md-offset-3">
	    		<form role="form" method="post" action="user-details" id="floating-label" class="form-input">
					<input name="_method" type="hidden" value="put" />
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="form-group">
						<input type="text" name="name" class="form-control" placeholder="Employee Name" required>
					</div>

	    			<div class="form-group">
					    <input type="Email" name="email" class="form-control" placeholder="Employee Email" required>
					</div>
					
					<!-- <input type="hidden" name="source" value="{{ csrf_token() }}"> -->
					<input class="btn btn-theme btn-block" type="submit" value="Save" />
					<!-- <a class="btn btn-theme btn-block login-btn-modal" type="submit">Log in</a> -->
	    		</form>
	    	</div>
	    </div>
	</div>
</div>