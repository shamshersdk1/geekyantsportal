<div>
            <div style="position: absolute; top:5px; left: 5px; right: 200px; z-index: 999;">
            <div class=' input-group'>
                <a class="btn btn-primary" href="/user/leave-overview/?type=previous&date={{$date}}&view_type={{$viewType}}">Prev</a>&nbsp;
                 <a class="btn btn-primary" href="/user/leave-overview/?type=next&date={{$date}}&view_type={{$viewType}}">Next</a>
            </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                <div class="panel panel-default">
                    <table class="table table-striped table-bordered leave-summary-overview"  id="leave-overview-summary">
                        <thead >
                            <th class="name"></th>
                            @for($i=0;$i<count($dates);$i++)
                                <th @if($code[$i]==1) class="holiday" @endif>
                                    <div class="text-center">{{$dates[$i]['day']}}</div>
                                    
                                </th>
                            @endfor
                        </thead>
                        <tbody>
                        @if(!empty($dataArray))
                            @foreach($dataArray as $data)
                                <tr>
                                    <td  class="name">{{$data['name']}}</td>
                                    @for($i=0;$i<count($data['dates']);$i++)
                                        @if($code[$i]==1)
                                            <td class="holiday"></td>
                                        @elseif($data['dates'][$i] != '0')
                                            <td style="padding:0;" class="sm">
                                                <div class="text-center name-div {{$data['dates'][$i]}}" >{{$data['dates'][$i]}}
                                                </div>
                                            </td>
                                        @else
                                            <td></td>
                                        @endif
                                    @endfor
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>