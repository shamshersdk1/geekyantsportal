@extends('layouts.admin-dashboard')

@section('body')

<div class="leave-app-container">
	{{{-- Leave type tab start --}}}
	{{{-- <div class="app-header">
		<div class="btn-left">
			
		</div>
		<div class="title">
			Leave Type
		</div>
		<div class="btn-right">
			<button class="btn btn-cancel">Cancel</button>
		</div>
	</div>
	<div class="leave-body">
		<div class="row card-row">
			<div class="col-xs-12 text-center">
				<div class="card">
					<a href="#" class="btn card-btn">
						<div class="card-inner">
							<img src="images/sick-leave.png" class="type-of-leave-icon">
							<p class="type-of-leave">Sick Leave</p>
						</div>
					</a>
				</div>
			</div>
		</div>
		<div class="row card-row">
			<div class="col-xs-12 text-center">
				<div class="card">
					<a href="#" class="btn card-btn">
						<div class="card-inner">
							<img src="images/paid-leave.png" class="type-of-leave-icon type-of-leave-icon-2">
							<p class="type-of-leave">Paid Leave</p>
						</div>
					</a>
				</div>
				<div class="card">
					<a href="#" class="btn card-btn">
						<div class="card-inner">
							<img src="images/unpaid-leave.png" class="type-of-leave-icon type-of-leave-icon-2">
							<p class="type-of-leave">Unpaid Leave</p>
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="footer">
		<button class="btn btn-block btn-success footer-btn">Next</button>
	</div> --}}}
	{{{-- Leave type tab end --}}}



	{{{-- Choose date tab start --}}}
	{{{-- <div class="app-header">
		<div class="btn-left">
			<button class="btn btn-cancel">
				<i class="fa fa-arrow-left"></i>
			</button>
		</div>
		<div class="title">
			Choose Date
		</div>
		<div class="btn-right">
			<button class="btn btn-cancel">Cancel</button>
		</div>
	</div>
	<div class="leave-body">
		<input type="text" name="daterange" value="01/01/2015 - 01/31/2015" />
	</div>
	<div class="footer">
		<button class="btn btn-block btn-success footer-btn">Next</button>
	</div> --}}}
	{{{-- Choose date tab end --}}}


	{{{-- Leave reason tab start --}}}
	{{{-- <div class="app-header">
		<div class="btn-left">
			<button class="btn btn-cancel">
				<i class="fa fa-arrow-left"></i>
			</button>
		</div>
		<div class="title">
			Reason
		</div>
		<div class="btn-right">
			<button class="btn btn-cancel">Cancel</button>
		</div>
	</div>
	<div class="leave-body">
		<textarea class="form-control reason-textarea" rows="15" id="" placeholder="Enter note"></textarea>
	</div>
	<div class="footer">
		<button class="btn btn-block btn-success footer-btn">Next</button>
	</div> --}}}
	{{{-- Leave reason tab end --}}}

	{{{-- Leave reason tab start --}}}
	<div class="app-header">
		<div class="btn-left">
			<button class="btn btn-cancel">
				<i class="fa fa-arrow-left"></i>
			</button>
		</div>
		<div class="title">
			Review
		</div>
		<div class="btn-right">
			<button class="btn btn-cancel">Cancel</button>
		</div>
	</div>
	<div class="leave-body">
		<div class="top-container">
			<div class="container-1">
				<div>
					<div class="text-center day">Friday</div>
					<div class="text-center date">10 Nov 2017</div>
				</div>
				<div>
					<i class="fa fa-calendar-o calendar-icon" aria-hidden="true"></i>
				</div>
				<div>
					<div class="text-center day">Sunday</div>
					<div class="text-center date">12 Nov 2017</div>
				</div>
			</div>
			<div class="container-2">
				<div>
					<div class="text-center day">4 Days</div>
					<div class="text-center date">Paid Leave</div>
				</div>
			</div>
		</div>
		<div class="second-container">
			<div class="reason-head">
				Reason
			</div>
			<div class="reason-text">
				This is the container that contains the reason for sick leave or paid leave or unpaid leave.
			</div>
		</div>
		<div class="bottom-container"></div>
	</div>
	<div class="footer">
		<button class="btn btn-block btn-success footer-btn">Apply</button>
	</div>
	{{{-- Leave reason tab end --}}}

</div>

<script type="text/javascript">
	$(function() {
		$('input[name="daterange"]').daterangepicker({
		    "alwaysShowCalendars": true,
		    "startDate": "11/04/2017",
		    "endDate": "11/10/2017",
		    "opens": "right"
		});
	});
</script>
@endsection