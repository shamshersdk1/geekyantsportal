@extends('layouts.mobile')

@section('title')
@endsection

@section('main-content')
    <div class="mobile-home-page">
        <div class="background">
            <div class="header-color">
                <div class="row pt20" style="padding-top: 20px;">
                    <div class="col-xs-4 text-left">  
                        <button  class="btn header-refresh-icon" id="refresh" ><i class="fa fa-refresh"></i>
                        </button>
                    </div>
                    <div class="col-xs-4 text-center">
                        <h4 class="header-title">Dashboard</h4>
                    </div>
                    <div class="col-xs-4 text-right">
                        <a href="/mobile/logout" class="header-icon"><i class="fa fa-power-off" aria-hidden="true"></i></a>
                    </div>
                </div>
                <div class="white-background">
                    <div class="row" style="padding: 20px 10px 0 10px;">
                        <div class="col-xs-4 leave-card">
                            <h5>Leave Card</h5>
                        </div>
                        <div class="col-xs-8 radio-button text-right" style="padding: 0;">
                            <div class="available-color-box"></div><span class="available-text">Available</span><div class="consumed-color-box"></div><span class="available-text">Consumed</span>
                        </div>
                        
                    </div>
                    <div class="row chart-container">
                        <div class="col-xs-4 text-center">
                            <div class="counter counter-wheel" 
                                data-style="wheel" 
                                data-max="{{$totalSickLeaves}}" 
                                data-value="{{$consumedSickLeaves}}"
                                data-count="0"  
                                data-pad="2">0
                            </div>
                            <div class="sick-leave-down">
                                <h5>Sick Leave</h5>
                                <p>{{$consumedSickLeaves}}/{{$totalSickLeaves}}</p>
                            </div>
                        </div>
                        <div class="col-xs-4 text-center">
                            <div class="counter counter-wheel" 
                                data-style="wheel" 
                                data-max="{{$totalPaidLeaves}}" 
                                data-value="{{$consumedPaidLeaves}}"
                                data-count="0"  
                                data-pad="2">0
                            </div>
                            <div class="sick-leave-down">
                                <h5>Paid Leave</h5>
                                <p>{{$consumedPaidLeaves}}/{{$totalPaidLeaves}}</p>
                            </div>
                        </div>
                        <div class="col-xs-4 text-center">
                            <div class="unpaid-leave-container">
                                <div class="numbers">{{$userUnpaidLeavesCount}}</div>
                            </div>
                            <div class="sick-leave-down">
                                <h5>Unpaid Leave</h5>
                                <p>{{$userUnpaidLeavesCount}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="dashboard-leave-details-container">
                <div class="leave-details-container">
                    <div class="history">
                        <h5>Leave History</h5>
                    </div>
                    @if(count($leaves) == 0)
                    <div class="white-sheet white-sheet-1">
                        <div class="row">
                            <div class="col-xs-12">
                                <p class="show-sick-leave"> No record found</p>
                            </div>   
                        </div>
                    </div>
                    @endif
                    @if(isset($leaves))
                        @foreach($leaves as $leave)
                            <div class="white-sheet white-sheet-1">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <p class="show-sick-leave">{{date("d M ", strtotime($leave->start_date))}} <b>-</b> {{date("d M ", strtotime($leave->end_date))}}</p>
                                    </div>
                                   @if($leave->half)
                                    <div class="col-xs-1 text-center">
                                        <p class="days-of-leave">{{$leave->days}}Days</p>
                                    </div>
                                     @endif
                                     @if(!$leave->half)
                                    <div class="col-xs-1 text-center">
                                        <p class="days-of-leave">{{(int)$leave->days}}Days</p>
                                    </div>
                                    @endif
                                    @if($leave->type)
                                        <div class="col-xs-4 text-right">
                                            <p class="leave">{{ ucfirst(trans($leave->type))}} Leave</p>
                                        </div>
                                    @endif
                                    @if($leave->status == "approved")
                                        <div class="col-xs-3 text-right">
                                            <p class="pending green">Approved</p>
                                        </div>
                                    @elseif($leave->status == "pending")
                                    <div class="col-xs-3 text-right">
                                        <p class="pending">Pending</p>
                                    </div>    
                                    @elseif($leave->status == "rejected")
                                        <div class="col-xs-3 text-right">
                                            <p class="pending red">Rejected</p>
                                        </div>
                                    @elseif($leave->status == "cancelled")
                                        <div class="col-xs-3 text-right">
                                            <p class="pending red">Cancelled</p>
                                        </div>
                                    @else
                                        <div class="col-xs-3 text-right">
                                            <p>No Status</p>
                                        </div>
                                    @endif
                                </div>
                            </div>                
                            
                        @endforeach
                    @endif
                </div>
            </div>
            <button id="apply" class="btn btn-block footer-btn crud-btn"><i></i>Apply Leave</button>
        </div>
    </div>
<script type="text/javascript">
    $(document).ready(function(){
        $("#apply").prop("disabled",false);
        $("#refresh").click(function(){
            $(this).find('i').addClass("fa fa-spinner fa-spin"); 
            setTimeout(function(){ window.location.href='/mobile/dashboard'; }, 100);

        });
        $("#apply").click(function(){
            //$(this).find('i').addClass("fa fa-spinner fa-spin");
            $(this).text($(this).text() == 'Apply Leave' ? "Processing..." : 'Apply Leave'); 
             $("#apply").prop("disabled",true);
             $("#apply").css("background", "#1ab49078");
             setTimeout(function(){ window.location.href='/mobile/dashboard/leave'; }, 100);

        });  
        $(".counter").countimator();
    });
</script>    
@endsection
