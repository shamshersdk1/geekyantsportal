@extends('layouts.plane')
@section('body')
<div class="container-fluid">
	<div class="login-wrapper">
		<div class="row">

			<div class="col-md-6 col-md-offset-3">
				<div class="logo-holder-panel">
					<a  class="admin-login-logo" ><img src="/images/logo-dark.png" class="img-responsive admin-logo-img center-block" alt=""></a>
				</div>
			</div>

			<div class="col-md-6 col-md-offset-3 panel-login-admin">
				<!-- <div class="logo-holder-panel">
					<a class="admin-login-logo" href="/"><img src="/images/logo-dark.png" class="img-responsive admin-logo-img center-block" alt=""></a>
				</div> -->
				@if(!empty($errors->all()))
					<div class="alert alert-danger">
						@foreach ($errors->all() as $error)
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							<span>{{ $error }}</span><br/>
					  	@endforeach
					</div>
				@endif
				@if (session('message'))
				    <div class="alert alert-success">
				    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				        <span>{{ session('message') }}</span><br/>
				    </div>
				@endif
				<div class="panel panel-default">
					<div class="panel-heading text-center">Login</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<!-- <div class="row">
									  <a href="{{ route('social.redirect', ['provider' => 'google']) }}" style="margin-left: 100px;" class="btn btn-lg btn-danger"><i class="fa fa-google-plus" aria-hidden="true"></i> Sign in with Google</a>
								</div> -->
								<form id="login_form" class="form-horizontal" role="form" method="POST" action="/mobile/mobile-login">
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
									<div class="input-group custom-input-group">
									  	<span class="input-group-addon" id="basic-addon1"><i class="fa fa-user fa-lg" aria-hidden="true"></i></span>
									  	<input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="User Name">
									</div>
									<div class="input-group custom-input-group">
										<span class="input-group-addon" id="basic-addon1"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
										<input type="password" class="form-control" name="password" placeholder="Password">
									</div>
									<div class="row">
										<div class="col-md-6 text-left">
											
										</div>
										<div class="col-md-6 text-right">
											
										</div>
									</div>
									<div class="sign-btn">
										<button type="submit" id="sin-in"  class="btn btn-primary btn-block admin-signin">
											SIGN IN
										</button>
									</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
$('#login_form').submit(function() {
setTimeout(function(){ $("#sin-in").text("Processing...");
	$("#sin-in").css("background", "#337ab799");
	$("#sin-in").css("border-color", "#337ab799"); 
}, 5);

});  
    });  
</script>
@endsection
