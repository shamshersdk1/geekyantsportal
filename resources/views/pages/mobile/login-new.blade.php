@extends('layouts.plane')
@section('body')
<div class="mobile-login-container">
	<div class="half first-half">
		<div class="cell">
			<div>
				<img src="images/logo-icon.png" class="logo-icon">
			</div>
			<div>
				GeekyAnts
			</div>
		</div>
	</div>
	<div class="half second-half">
		<div class="cell">
			<form class="form-horizontal" role="form" method="POST" action="/mobile/mobile-login">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="input-group custom-input-group">
				  	<span class="input-group-addon" id="basic-addon1"><i class="fa fa-user fa-lg" aria-hidden="true"></i></span>
				  	<input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="User Name">
				</div>
				<div class="input-group custom-input-group">
					<span class="input-group-addon" id="basic-addon1"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
					<input type="password" class="form-control" name="password" placeholder="Password">
				</div>
				<div class="row">
					<div class="col-md-6 text-left">
						<!-- <a href="signup" class="admin-gray-text">Sign up</a> -->
					</div>
					<div class="col-md-6 text-right">
						<!-- <a href="/password/reset" class="admin-gray-text">Forgot Password?</a> -->
					</div>
				</div>
				<div class="sign-btn">
					<button type="submit" class="btn btn-primary btn-block admin-signin"  data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing...">
						SIGN IN
					</button>
				</div>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">

$(function() {
     $('#signin').on('click', function() {
    var $this = $(this);
    $this.button('loading');
    setTimeout(function() {
       $this.button('reset');
    }, 3000)
     });
});     
</script>
@endsection
