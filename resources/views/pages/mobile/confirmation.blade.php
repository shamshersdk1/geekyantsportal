@extends('layouts.mobile')
@section('main-content')
<div class="leave-app-container">
    <div class="leave-app-container">
        <div class="app-header">
            <div class="btn-left">

            </div>
            <div class="title text-center">
                Confirmation
            </div>
            <div class="btn-right">
                
            </div>
        </div>
        <div class="leave-body-container">
            <div class="leave-body">
                <div class="confirmation-container text-center">
                    <div class="confirmation-container-cell">
                        <div class="text">
                            Your request has been submitted.
                        </div>
                        <div class="text">
                            HR department will verify.
                        </div>
                        <div class="confirm-block">
                            <div class="confirm-block-cell">
                                <i class="fa fa-check"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <button class="next done-button" id="confirm" type="button"><i></i>Done</button>
    <!-- <a href="/mobile/dashboard"><button   class="next done-button" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Processing...">Done</button></a> -->
</div>
<script type="text/javascript">
   $(document).ready(function(){
     $("#confirm").prop("disabled",false);
        $("#confirm").click(function(){
            //$(this).find('i').addClass("fa fa-spinner fa-spin"); 
            $(this).text($(this).text() == 'Done' ? "Processing..." : 'Done');
             $("#confirm").prop("disabled",true);
             $("#confirm").css("background", "#1ab49078");
             setTimeout(function(){ window.location.href='/mobile/dashboard'; }, 500);

        });
    });
</script>
@endsection   