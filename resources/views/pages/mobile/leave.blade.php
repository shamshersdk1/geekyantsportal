@extends('layouts.mobile')
@section('main-content')
    <div class="leave-app-container">
        <form id="msform" action="/mobile/dashboard/leave/requestLeave" method="post">
            <fieldset>
                    <div class="app-header">
                        <div class="btn-left">
                        </div>
                        <div class="title">
                            Leave Type        
                        </div>
                        <div class="btn-right">
                            <button class="btn btn-cancel"><a href="/mobile/dashboard">Cancel</a></button>
                        </div>
                    </div>
                    <div class="leave-body-container">
                        <div class="leave-body">
                                @if(!empty($errors->all()))
                                    <div class="alert alert-danger mt15">
                                        @foreach ($errors->all() as $error)
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            <span>{{ $error }}</span><br/>
                                          @endforeach
                                    </div>
                                @endif
                                @if (session('message'))
                                    <div class="alert alert-success mt15">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <span>{{ session('message') }}</span><br/>
                                    </div>
                                @endif
                            <div class="row card-row mt15">
                                <div class="col-xs-12 text-center">
                                    <div class="card">
                                        <label  class="card-btn">
                                            <div class="btn card-inner">
                                                <img src="/images/sick-leave.png" class="type-of-leave-icon">
                                                <p class="type-of-leave">Sick Leave</p>
                                                <input type="radio" name="type" id="inlineRadio1" value="sick" style="display: none;">
                                            </div>                     
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row card-row mt15">
                                <div class="col-xs-12 text-center">
                                    <div class="card">
                                        <label class="card-btn">
                                            <div class="btn card-inner">
                                                <img src="/images/paid-leave.png" class="type-of-leave-icon type-of-leave-icon-2">
                                                <p class="type-of-leave">Paid Leave</p>
                                                <input type="radio" name="type" id="inlineRadio2" value="paid" style="display: none;">
                                            </div>
                                        </label>     
                                    </div>
                                    <div class="card">
                                        <label  class="card-btn">
                                            <div class="btn card-inner">
                                                <img src="/images/unpaid-leave.png" class="type-of-leave-icon type-of-leave-icon-2">
                                                <p class="type-of-leave">Unpaid Leave</p>
                                                <input type="radio" name="type" id="inlineRadio3" value="unpaid" style="display: none;">
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="button" name="next" id="tabonenext" class="next action-button" disabled="true" style ="background: grey;" value="Next"/>     
            </fieldset>
            <fieldset>
                    <div class="app-header">
                        <div class="btn-left">
                            
                        </div>
                        <div class="title">
                            Choose Date
                        </div>
                        <div class="btn-right">
                            <button class="btn btn-cancel"><a href="/mobile/dashboard">Cancel</a></button>
                        </div>
                    </div>
                    <div class="leave-body-container">
                        <div class="leave-body">
                            <div class='input-group date mr15 ml15 mt15' id='datetimepickerStart'>
                                <input  id="datetimeinputstart" class="form-control" value="{{old('start_date')}}" name="start_date" placeholder="Start Date"  autocomplete="off" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                            <div class='input-group date mr15 ml15' id='datetimepickerEnd'>
                                <input  id="datetimeinputend" class="form-control" name="end_date" placeholder="End Date" value="{{old('end_date')}}"  autocomplete="off" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <input type="button" name="previous" class="previous action-button-previous" value="Back" />
                    <input type="button" name="next" id="tabtwonext"  class="next action-button" disabled="true" style ="background: grey;"  value="Next"/>
            </fieldset>
            <fieldset>
                    <div class="app-header">
                        <div class="btn-left">
                            
                        </div>
                        <div class="title">
                            Reason
                        </div>
                        <div class="btn-right">
                            <button class="btn btn-cancel"><a href="/mobile/dashboard">Cancel</a></button>
                        </div>
                    </div>
                    <div class="leave-body-container">
                        <div class="leave-body">
                            <textarea id= "message" name="reason" class="form-control reason-textarea" rows="10" id="" placeholder="Enter note" ></textarea>
                        </div>
                    </div>
                    <input type="button" name="previous" class="previous action-button-previous" value="Back"/>
                    <input type="button" id="tabthreenext" name="next" class="next action-button" disabled="true" style ="background: grey;" value="Next"/>
            </fieldset>
            <fieldset>
                    <div class="app-header">
                        <div class="btn-left">
                               
                        </div>
                        <div class="title">
                            Review 

                        </div>
                        <div class="btn-right">
                            <button class="btn btn-cancel"><a href="/mobile/dashboard">Cancel</a></button>
                        </div>
                    </div>
                    <div class="leave-body-container">
                        <div class="leave-body">
                            <div class="top-container">
                                <div class="container-1">
                                    <div>
                                        <div class="text-center day"><p id="startdayField"></p></div>
                                        <div class="text-center date"><p id="startdateField"></p></div>
                                    </div>
                                    <div>
                                        <i class="fa fa-calendar-o calendar-icon" aria-hidden="true"></i>
                                    </div>
                                    <div>
                                        <div class="text-center day"><p id="enddayField"></p></div>
                                        <div class="text-center date"><p id="enddateField"></p></div>
                                    </div>
                                </div>
                                <div class="container-2">
                                    <div>
                                        <div class="text-center day"><p id="leave-range"></p></div>
                                        <div class="text-center date"><p id="leave"></p></div>
                                    </div>
                                </div>
                            </div>
                            <div class="second-container">
                                <div class="reason-head">
                                    Reason
                                </div>
                                <div class="reason-text">
                                    <p id="reasonField"></p>
                                </div>
                            </div>
                            <div class="bottom-container"></div>
                        </div>
                    </div>
                    <input type="button" name="previous" class="previous action-button-previous" value="Back"/>
                    <button type="submit" id="apply-leave" class="btn crud-btn apply-btn-mobile"><i></i>Apply</button>
            </fieldset>
        </form>
    </div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#datetimeinputstart').blur();
        //$('#datetimeinputend').blur();

        $('#datetimeinputstart').on('cick', function(e){
          $('#datetimeinputstart').blur();  
          e.preventDefault();
        });

        // $('#datetimeinputstart').keypress(function(event) {
        //   event.preventDefault();
        //  return false;
        // });
        $('#datetimeinputend').keypress(function(event) {
          event.preventDefault();
         return false;
        });

        $("#apply-leave").prop("disabled",false);
        $('#datetimepickerStart').datetimepicker({
            //Set max date to upcoming march
            maxDate: new Date().getMonth() <= 2 ? new Date(new Date().getFullYear()+"-03-31") : new Date(new Date().getFullYear()+1+"-03-31"),
            minDate: new Date(), //Set min date to today
            format: 'YYYY-MM-DD'
        });

        $('#datetimepickerEnd').datetimepicker({
            //Set max date to upcoming march
            maxDate: new Date().getMonth() <= 2 ? new Date(new Date().getFullYear()+"-03-31") : new Date(new Date().getFullYear()+1+"-03-31"),
            format: 'YYYY-MM-DD'
        });
        if($("#datetimeinputstart").val()!="")
        {
            $('#datetimepickerEnd').data("DateTimePicker").minDate($("#datetimeinputstart").val());
        }
        if($("#datetimeinputstart").val() == "")
        {
            console.log("null start_date");
            $('#datetimeinputend').val("");
        }
        
        $("#datetimepickerStart").on("dp.change", function (e) {
            $('#datetimepickerEnd').data("DateTimePicker").minDate(e.date);
        });
   

    $('#msform input').on('change', function() {
        var first = $('input[name=type]:checked', '#msform').val();
        if(first){
            console.log("got the value");
            console.log(first);
          $("#tabonenext").prop("disabled",false);
          $("#tabonenext").css("background", "#1abc9c");
        }
    });

    $("#tabonenext").click(function() {
          var value = $('input[name=type]:checked').val();
          var final = value +" "+"leave";
          $('#leave').text(final);
          var firs = $('#datetimeinputstart').val();
          if(firs){
              $("#tabtwonext").prop("disabled",false);
              $("#tabtwonext").css("background", "#1abc9c");
            }
    });

    $("#datetimeinputstart").focus(function() {
        var firstdate = $('#datetimeinputstart').val();
        if(firstdate){
              $("#tabtwonext").prop("disabled",false);
              $("#tabtwonext").css("background", "#1abc9c");
        }
    });

    $("#tabtwonext").click(function() {
      var days = ['SUNDAY','MONDAY','TUESDAY','WEDNESDAY','THURSDAY','FRIDAY','SATURDAY'];
      var date = new Date($('#datetimeinputstart').val());
      x = date.getDay();
      day = date.getDate();
      locale = "en-us",
      month = date.toLocaleString(locale, { month: "short" });
      year = date.getFullYear();
      var data = [day,month,year].join('-');

      $('#startdateField').text(data);
      $('#startdayField').text(days[x]);
      var date1 = new Date($('#datetimeinputend').val());
      x1 = date1.getDay();
      day1 = date1.getDate();
      month1 = date1.toLocaleString(locale, { month: "short" });
      year1 = date1.getFullYear();

      var data1 = [day1,month1,year1].join('-');

      $('#enddateField').text(data1);
      $('#enddayField').text(days[x1]);
       $.ajax({
            url: '/mobile/find-days',
            type: 'GET',
            data: { start: date, end: date1}, // added data 
            success: function(res) {
                console.log(res);
                if(res > 1){
                    var d = "Days";
                  }else{
                   var d = "Day";
                  }
                 var f = res +" "+d;
                $('#leave-range').text(f);
            }
        });
       
     
      var reasonData = $("textarea#message").val();
      if(reasonData){
          $("#tabthreenext").prop("disabled",false);
          $("#tabthreenext").css("background", "#1abc9c");
      }else{
          $("#tabthreenext").prop("disabled",true);
          $("#tabthreenext").css("background", "grey");
      }

    });

    $("#tabthreenext").click(function() {
      var thought = $("textarea#message").val();
      $('#reasonField').text(thought);
      $('#leave').text(function(_, txt) {
        return txt.charAt(0).toUpperCase() + txt.slice(1).toLowerCase();
      });

    });

    
    $('#msform').submit(function() {
    setTimeout(function(){ 
        $("#apply-leave").text("Processing...");
        $("#apply-leave").css("background", "#1ab49078");}, 10);
    });  


    //jQuery time
    var current_fs, next_fs, previous_fs; //fieldsets
    var left, opacity, scale; //fieldset properties which we will animate
    var animating; //flag to prevent quick multi-click glitches

    $(".next").click(function(){
        if(animating) return false;
        animating = true;
        
        current_fs = $(this).parent();
        next_fs = $(this).parent().next();
        
        //activate next step on progressbar using the index of next_fs
        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
        
        //show the next fieldset
        next_fs.show(); 
        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
            step: function(now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale current_fs down to 80%
                scale = 1 - (1 - now) * 0.2;
                //2. bring next_fs from the right(50%)
                left = (now * 50)+"%";
                //3. increase opacity of next_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({
            'transform': 'scale('+scale+')',
            'position': 'none'
          });
                next_fs.css({'left': left, 'opacity': opacity});
            }, 
            duration: 0, 
            complete: function(){
                current_fs.hide();
                animating = false;
            }, 
            //this comes from the custom easing plugin
            easing: 'easeInOutBack'
        });
    });

    $(".previous").click(function(){
        if(animating) return false;
        animating = true;
        
        current_fs = $(this).parent();
        previous_fs = $(this).parent().prev();
        
        //de-activate current step on progressbar
        $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
        
        //show the previous fieldset
        previous_fs.show(); 
        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
            step: function(now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale previous_fs from 80% to 100%
                scale = 0.8 + (1 - now) * 0.2;
                //2. take current_fs to the right(50%) - from 0%
                left = ((1-now) * 50)+"%";
                //3. increase opacity of previous_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({'left': left,'position': 'none'});
                previous_fs.css({'transform': 'none', 'opacity': opacity});
            }, 
            duration: 00, 
            complete: function(){
                current_fs.hide();
                animating = false;
            }, 
            //this comes from the custom easing plugin
            easing: 'easeInOutBack'
        });
    });

    $(".submit").click(function(){
        return false;
    })

        $('input:radio').change(function(){
            var $this = $(this);
            $(".card-btn").removeClass('highlight');
            $this.closest('.card-btn').addClass('highlight');
        });

         $('#message').on('input', function() {
            var messageData = $("textarea#message").val();
            if(messageData){
                  $("#tabthreenext").prop("disabled",false);
                  $("#tabthreenext").css("background", "#1abc9c");
            }else{
                  $("#tabthreenext").prop("disabled",true);
                  $("#tabthreenext").css("background", "grey");
            }
         });


});


</script>
@endsection
    