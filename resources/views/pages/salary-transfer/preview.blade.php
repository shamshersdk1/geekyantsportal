@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-6">
                <h1 class="admin-page-title">Salary Transfer</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
					<li><a href="/admin/salary-transfer">Month List</a></li>
        		  	<li><a href="/admin/salary-transfer/{{$monthObj->id}}">Transaction Summary of {{$monthObj ? $monthObj->formatMonth() : "All Months"}}</a></li>
                    <li class="active">Preview</li>
        		</ol>
            </div>
		</div>
    </div>
    {{Form::open(['url' => '/admin/salary-transfer/' .($month->id ? $month->id.'/finalise' : 'finalise'), 'method' => 'POST'])}} 
	<div class="row">
        <div class="col-md-3">
            <h4>Preview for Bank Transfer</h4>
        </div>
		<div class="col-md-3 pull-right">
			{{Form::button('<i class="fa fa-check" aria-hidden="true"></i><span style="padding-left:5px">Finalise</span>', array('type' => 'submit', 'class' => 'btn btn-primary'));}}		
            {{Form::button('<i class="fa fa-caret-left fa-fw" aria-hidden="true"></i><span style="padding-left:5px">Back</span>', array('type' => 'button', 'class' => 'btn btn-default', 'onClick' => 'window.history.back();'));}}		
		</div>
    </div>
 
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		            @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @include('pages/salary-transfer/_partials.bank-hold')
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @include('pages/salary-transfer/_partials.bank-transfer')
        </div>
    </div>
    {{Form::close()}}
</div>
<script>
    $(document).ready(function() {
        
        var t = $('#transfer-table').DataTable( {
            pageLength:500, 
            fixedHeader: {
                header: true
            }
        } );
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
        var m = $('#hold-table').DataTable( {
            pageLength:500, 
            fixedHeader: {
                header: true
            }
        } );
        m.on( 'order.dt search.dt', function () {
            m.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
        
        
        
        
    });
</script>
@endsection
