@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
		<div class="row">
            <div class="col-sm-6">
				<h1 class="admin-page-title">Salary Transfer</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
					<li class="active">Month List</li>
                </ol>
			</div>
        </div>
	</div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>

	<div class="row">
            <div class="col-md-2">
                {{Form::open(['url' => '/admin/transaction-summary/create', 'method' => 'get'])}}
                    {{Form::button('<i class="fa fa-plus fa-fw"></i>  Add Transaction', array('type' => 'submit', 'class' => 'btn btn-success'));}}
                {{Form::close()}}
            </div>
            
        </div>

		{{Form::open(['url' => '/admin/salary-transfer/' . ($month->id ? $month->id.'/preview' : 'preview'), 'method' => 'POST'])}} 
	<div class="row">
        <div class="col-md-5">
            <h4>Transaction Summary for {{$month ? $month->formatMonth() : "All Months"}}</h4>
		</div>
		@if($transactions->sum('amountPayable') > 0)
		<div class="col-md-2 pull-right">
			{{Form::button('<i class="fa fa-eye" aria-hidden="true"></i><span style="padding-left:5px">Preview</span>', array('type' => 'submit', 'class' => 'btn btn-warning'));}}		
		</div>
		@endif
	</div>
	
	<div class="user-list-view">
        <div class="panel panel-default">
            <table class="table table-striped" id="summary-table">
                <thead>
					<th class="check text-center" width="7%"><input type="checkbox" id="flowcheckall" value="" />&nbsp;<small><span id="text-check">Bank Transfer</span></small></th>
                    <th class="text-center" width="5%">#</th>
                    <th class="" width="7%">Employee Id</th>
                    <th class="" width="7%">Name</th>
                    <th class="">PAN Number</th>
                    <th class="">Bank Account Number</th>
                    <th class="">Bank IFSC Code</th>
                    <th class="text-center">Net Payable</th>
                </thead>
                <tbody>
				@if(count($transactions) > 0)
					@foreach($transactions as $transaction)
                        @if($transaction->amountPayable != 0)
							<tr>
								<td class="check text-center"><input type="checkbox" id={{$transaction->user->id}} name="options[{{$transaction->user->id}}]" value={{$transaction->user->id}} />&nbsp;</td>
								<td class="text-center"></td>
								<td class="">{{ $transaction->user->employee_id}}</td>
								<td class="">{{ $transaction->user->name}}</td>
                                <td class="text-center">{{ $transaction->user->userDetails->pan ?? '-'}}</td>
                                <td class="text-center">{{ $transaction->user->userDetails->bank_ac_no ?? '-'}}</td>
                                <td class="text-center">{{ $transaction->user->userDetails->bank_ifsc_code ?? '-'}}</td>
								<td class="text-center">{{ $transaction->amountPayable ?? 0}}</td>
							</tr>
                        @endif
					@endforeach
                @else
                    <tr>
                        <td colspan="7" class="text-center">
                            No Records found
                        </td>
                    </tr>
                @endif
                </tbody>
            </table>
		</div>
	</div>
	{{Form::close()}}
</div>
	

<script>
	$('#selectid2').change(function(){
		var optionSelected = $("option:selected", this);
        optionValue = this.value;
        console.log(optionValue);
        if (optionValue) { 
            window.location = "/admin/salary-transfer/"+optionValue; 
        }
    });

	$(document).ready(function() {
        var t = $('#summary-table').DataTable( {
            pageLength:500, 
            fixedHeader: {
                header: true
            },
           dom: 'Bfrtip',
            buttons: [
                'csv'
            ]
        } );
        t.on( 'order.dt search.dt', function () {
            t.column(1, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    });

	$("#flowcheckall").click(function () {
        $('#summary-table tbody input[type="checkbox"]').prop('checked', this.checked);
    });
	
    $("#text-check").click(function () {
        source = document.getElementById('flowcheckall');
        $('#summary-table tbody input[type="checkbox"]').prop('checked', !source.checked);
        $('#flowcheckall').prop('checked', !source.checked);
    });
</script>
@endsection
