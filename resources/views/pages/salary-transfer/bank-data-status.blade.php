@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-6">
                <h1 class="admin-page-title">Salary Transfer</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
					<li><a href="/admin/salary-transfer">Month List</a></li>
        		  	<li class="active">Transfer Status</li>
        		</ol>
            </div>
		</div>
    </div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		            @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @include('pages/salary-transfer/_partials.hold-data')
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @include('pages/salary-transfer/_partials.transfer-data')
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        var m = $('#hold-table').DataTable( {
            pageLength:500, 
            fixedHeader: {
                header: true
            }
        } );
        m.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    });
</script>
@endsection
