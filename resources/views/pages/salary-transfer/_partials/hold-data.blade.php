<div class="user-list-view">
        <div class="panel panel-default">
            <table class="table table-striped" id="hold-table">
                <caption><strong>BANK TRANSFER HOLD</strong></caption>
                <thead>
                    <th class="text-center" width="5%">#</th>
					<th class="" width="7%">Employee Id</th>
                    <th class="" width="7%">Name</th>
                    <th class="text-center">Total Deduction</th>
                    <th class="text-center">Total Allowance</th>
                    <th class="text-center">Amount Payable</th>
                </thead>
                @if(count($holdUsers) > 0)
                    @foreach($holdUsers as $user)
                        <tr>
                            <td class="text-center"></td>
							<td class="">{{ $user->user->employee_id}}</td>
                            <td class="">{{ $user->user->name}}</td>
                            <td class="text-center">{{ $bankTransfer->holdUsers->where('user_id',$user->user_id)->where('amount','<', 0)->sum('amount')}}</td>
                            <td class="text-center">{{ $bankTransfer->holdUsers->where('user_id',$user->user_id)->where('amount','>', 0)->sum('amount')}}</td>
                            <td class="text-center">{{ $bankTransfer->holdUsers->where('user_id',$user->user_id)->sum('amount')}}</td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="6" class="text-center">
                            No Records found
                        </td>
                    </tr>
                @endif
            </table>
		</div>
    </div>