<div class="user-list-view">
        <h5 style="padding-left:10px">BANK TRANSFER HOLD</h5>
        <div class="panel panel-default">
            <table class="table table-striped" id="hold-table">
                <thead>
                    <th class="text-center" width="5%">#</th>
					<th class="" width="7%">Employee Id</th>
                    <th class="" width="7%">Name</th>
                    <th class="text-center">Net Payable</th>
                </thead>
                <tbody>
                @if(count($transactionsHoldUsers) > 0)
					@foreach($transactionsHoldUsers as $transactionsHoldUser)
                        @if($transactionsHoldUser->amountPayable != 0)
							<tr>
                                <input type="hidden" id="custId" name="hold[{{$transactionsHoldUser->user->id}}]" value="hold">
								<td class="text-center"></td>
								<td class="">{{ $transactionsHoldUser->user->employee_id}}</td>
								<td class="">{{ $transactionsHoldUser->user->name}}</td>
								<td class="text-center">{{ $transactionsHoldUser->amountPayable ?? 0}}</td>
							</tr>
                        @endif
					@endforeach
                @else
                    <tr>
                        <td colspan="6" class="text-center">
                            No Records found
                        </td>
                    </tr>
                @endif
                </tbody>
            </table>
		</div>
    </div>