<div class="user-list-view">
    <h5 style="padding-left:10px">BANK TRANSFER</h5>
        <div class="panel panel-default">
            <table class="table table-striped" id="transfer-table">
                <thead>
                    <th class="text-center" width="5%">#</th>
					<th class="" width="7%">Employee Id</th>
                    <th class="" width="7%">Name</th>
                    <th class="text-center">Net Payable</th>
                </thead>
                <tbody>
                @if(count($transactionsTransferUsers) > 0)
					@foreach($transactionsTransferUsers as $transactionsTransferUser)
                        @if($transactionsTransferUser->amountPayable != 0)
							<tr>
                                <input type="hidden" id="custId" name="process[{{$transactionsTransferUser->user->id}}]" value="process">
								<td class="text-center"></td>
								<td class="">{{ $transactionsTransferUser->user->employee_id}}</td>
								<td class="">{{ $transactionsTransferUser->user->name}}</td>
								<td class="text-center">{{ $transactionsTransferUser->amountPayable ?? 0}}</td>
							</tr>
                        @endif
					@endforeach
                @else
                    <tr>
                        <td colspan="6" class="text-center">
                            No Records found
                        </td>
                    </tr>
                @endif
                </tbody>
            </table>
		</div>
    </div>
    