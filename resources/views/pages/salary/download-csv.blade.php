@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-6">
                <h1 class="admin-page-title">Salary Transaction</h1>
                <ol class="breadcrumb">
                    <li><a href="/admin">Admin</a></li>
                    <li class="active">{{$monthObj ? $monthObj->formatMonth() : ""}}</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                    @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif
        </div>
    </div>
    <div class="col-md-2 pull-right">
        <div class="form-group m-b-10">
            <div class="bg-white" style="width:100%">
                @if(isset($months))
                    <select id="selectid2" name="month"  placeholder= "{{$monthObj ? $monthObj->formatMonth() : "All"}}">
                        <option value=""></option>
                        @foreach($months as $x)
                            <option value="{{$x->id}}" >{{$x->formatMonth()}}</option>
                        @endforeach
                    </select>
                @endif
            </div>
        </div>                
    </div> 
    <div class="user-list-view">
        <div class="panel panel-default">
            <table class="table table-striped" id="summary-table">
                <thead>
                    <th class="text-center" width="5%">#</th>
                    <th class="" width="7%">Employee Id</th>
                    <th class="" width="7%">Name</th>
                    @foreach ($apprCompType as $type)
                        <th>{{$type->name}}</th>
                    @endforeach
                    @foreach ($appBonusType as $bonus)
                        <th>{{$bonus->description}}</th>
                    @endforeach
                    <th>Onsite Bonus</th>
                    <th>Additional Workday Bonus</th>
                    <th>Extra Hour Bonus</th>
                    <th>Tech Talk Bonus</th>
                    <th>Performance Bonus</th>
                    <th>Referral Bonus</th>
                    <th>Other Bonus</th>
                    <th>Insurance Deduction</th>
                    <th>Food Deduction</th>
                    <th>Adhoc Payment</th>
                    <th>Loan Deduction</th>
                    <th>Vpf</th>
                </thead>
                <tbody>
                @if(count($users) > 0)
                    @foreach($users as $user)
                    @if(isset($userArray[$user->id]))
                        <tr>
                            <td class="text-center"></td>
                            <td class="">{{ $user->employee_id}}</td>
                            <td class="">{{ $user->name}}</td>
                            @foreach ($apprCompType as $type)
                                <td>{{$data[$user->id]['appraisal-comp'][$type->id] ?? 0}}</td>
                            @endforeach
                            @foreach ($appBonusType as $bonus)
                                <td>{{$data[$user->id]['appraisal-bonus'][$bonus->id] ?? 0}}</td>
                            @endforeach
                            <td>{{$data[$user->id]['onsite'] ?? 0}}</td>
                            <td>{{$data[$user->id]['additional-workday'] ?? 0}}</td>
                            <td>{{$data[$user->id]['extra-hour'] ?? 0}}</td>
                            <td>{{$data[$user->id]['tech-talk'] ?? 0}}</td>
                            <td>{{$data[$user->id]['performance'] ?? 0}}</td>
                            <td>{{$data[$user->id]['referral'] ?? 0}}</td>
                            <td>{{$data[$user->id]['other-bonus'] ?? 0}}</td>
                            <td>{{$data[$user->id]['ins-deduction'] ?? 0}}</td>
                            <td>{{$data[$user->id]['appraisal-bonus'] ?? 0}}</td>
                            <td>{{$data[$user->id]['appraisal-bonus'] ?? 0}}</td>
                            <td>{{$data[$user->id]['appraisal-bonus'] ?? 0}}</td>
                            <td>{{$data[$user->id]['appraisal-bonus'] ?? 0}}</td>
                        </tr>
                    @endif
                    @endforeach
                @else
                    <tr>
                        <td colspan="6" class="text-center">
                            No Records found
                        </td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        var t = $('#summary-table').DataTable( {
            pageLength:500, 
            fixedHeader: {
                header: true
            },
           dom: 'Bfrtip',
            buttons: [
                'csv'
            ]
        } ); 
        
    });
</script>
<script>
    $('#selectid2').change(function(){
        var optionSelected = $("option:selected", this);
        optionValue = this.value;
        console.log(optionValue);
        if(optionValue == 0) {
            window.location = "/admin/salary"; 
        }
        else if (optionValue) { 
            window.location = "/admin/salary/"+optionValue; 
        }
    });
</script>
@endsection