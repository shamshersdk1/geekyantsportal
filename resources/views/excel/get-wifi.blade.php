<table>
    <thead>
    <tr>
        <td>Mac Address</td>
        <td>Description</td>
    </tr>
    </thead>
    <tbody>
    @foreach($data as $row)
        <tr>
            <td>{{ $row['mac'] }}</td>
            <td>{{ $row['desc'] }}</td>
        </tr>
    @endforeach
    </tbody>
</table>