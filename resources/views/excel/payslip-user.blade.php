<style type="text/css">
	table,table td, table th{
		border:1px solid black;
		padding: 5px;
		border-collapse:collapse;
	}	
</style>
<table>
@if(!empty($data) && count($data) > 0)
	@foreach($data as $index=>$value)
		<tr>
			<td>{{$index}}</td>
			@if(!empty($value) && count($value) > 0)
				@foreach($value as $payslip)
					<td>{{$payslip}}</td>
				@endforeach
			@endif
		</tr>
	@endforeach
@endif
</table>