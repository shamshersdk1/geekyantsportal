$(window).load(function () {

var size = 1;
var button = 1;
var button_class = "gallery-header-center-right-links-current";
var normal_size_class = "gallery-content-center-normal";
var full_size_class = "gallery-content-center-full";
var $container = $('#gallery-content-center');
    
$container.isotope({itemSelector : 'img'});

function check_button(){
	$('.gallery-header-center-right-links').removeClass(button_class);
	if(button==1){
		$("#filter-all").addClass(button_class);
	}
	if(button==2){
		$("#filter-culture").addClass(button_class);
	}
	if(button==3){
		$("#filter-office").addClass(button_class);
	}	
	if(button==4){
		$("#filter-events").addClass(button_class);
	}	
	if(button==5){
		$("#filter-outing").addClass(button_class);
	}	
}
	
$("#filter-all").click(function() { $container.isotope({ filter: '.all' }); button = 1; check_button(); });
$("#filter-culture").click(function() {  $container.isotope({ filter: '.culture' }); button = 2; check_button();  });
$("#filter-office").click(function() {  $container.isotope({ filter: '.office' }); button = 3; check_button();  });
$("#filter-events").click(function() {  $container.isotope({ filter: '.events' }); button = 4; check_button();  });
$("#filter-outing").click(function() {  $container.isotope({ filter: '.outing' }); button = 5; check_button();  });
check_button();
check_size();

});