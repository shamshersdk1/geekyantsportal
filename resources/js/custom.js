// jQuery to collapse the navbar on scroll
function collapseNavbar() {
    if ($(".navbar").offset() && $(".navbar").offset().top > 50) {
        $(".navbar-main").addClass("top-nav-collapse");

    } else {
        $(".navbar-main").removeClass("top-nav-collapse");
    }
}

$(window).scroll(collapseNavbar);
$(document).ready(collapseNavbar);

// jQuery for page scrolling feature 
$(function () {
    $('a.page-scroll').bind('click', function (event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });
});

// Closes the Responsive Menu on Menu Item Click
$(function () {
    $('.navbar-collapse ul li a').click(function () {
        if ($(this).attr('class') != 'dropdown-toggle active' && $(this).attr('class') != 'dropdown-toggle') {
            $('.navbar-toggle:visible').click();
        }
        
    });
   
});

$(function () {
    $("[data-rel=tooltip]").tooltip({ html: true });
});


$(function () {
    // the dropdown 
    // $('.chosen').chosen({
    //     no_results_text: "Not found",
    //     allow_single_deselect: true
    // });

    // The date picker
    // $( ".datepicker-13" ).datepicker();
    $('#txtStartDate, #txtEndDate').datepicker({
        // showOn: "both",
        beforeShow: customRange,
        dateFormat: "M dd, yy",
    });

    function customRange(input) {
        if (input.id == 'txtEndDate') {
            var minDate = new Date($('#txtStartDate').val());
            minDate.setDate(minDate.getDate())
            return {
                minDate: minDate
            };
        }
        return {}
    }

});
$("#selectid2").select2();
$("#selectid22").select2();
$("#selectid3").select2();
$("#selectid4").select2();
$(".js-example-basic-multiple2").select2();
$(".js-example-basic-multiple22").select2();

$(".js-example-basic-multiple").select2();
$('.select2-container--default').css('width', '100%');
$('.js-example-basic-multiple22').css('width', '100%');

// ============= show password toggle ============================
// 
(function ($) {
    $('#showpwdcheckbox').click(function () {
        if ($("#showpwd").attr('type') == 'password')
            $("#showpwd").attr('type', 'text');
        else
            $("#showpwd").attr('type', 'password');
    });
    // ================ calender =============

}(jQuery));
// ================== multiple select ===========

$(".js-example-basic-multiple").select2();
$(".js-example-tags").select2({
    tags: true
})

// ===================== check all ===================
$('#chk_all').change(function () {
    if (this.checked)
        $('.chkbx').attr('checked', true);
    else
        $('.chkbx').attr('checked', false);

})
// ====================== calender - view ====================
$(function () {
    $('#datetimepicker12').datetimepicker({
        inline: true,
        sideBySide: true
    });
    $('.timepicker').hide();
    $('.datepicker').addClass('dateview-calender');
    var current = location.pathname;

  

});

$.sidebarMenu = function(menu) {
  var animationSpeed = 300,
    subMenuSelector = '.sidebar-submenu';

  $(menu).on('click', 'li a', function(e) {
    var $this = $(this);
    var checkElement = $this.next();

    if (checkElement.is(subMenuSelector) && checkElement.is(':visible')) {
      checkElement.slideUp(animationSpeed, function() {
        checkElement.removeClass('menu-open');
      });
      checkElement.parent("li").removeClass("active");
    }

    //If the menu is not visible
    else if ((checkElement.is(subMenuSelector)) && (!checkElement.is(':visible'))) {
      //Get the parent menu
      var parent = $this.parents('ul').first();
      //Close all open menus within the parent
      var ul = parent.find('ul:visible').slideUp(animationSpeed);
      //Remove the menu-open class from the parent
      ul.removeClass('menu-open');
      //Get the parent li
      var parent_li = $this.parent("li");

      //Open the target menu and add the menu-open class
      checkElement.slideDown(animationSpeed, function() {
        //Add the class active to the parent li
        checkElement.addClass('menu-open');
        parent.find('li.active').removeClass('active');
        parent_li.addClass('active');
      });
    }
    //if this isn't a link, prevent the page from being redirected
    if (checkElement.is(subMenuSelector)) {
      e.preventDefault();
    }
  });
}
