angular.module("myApp").service("Datatable",
    function Datatable() {

        const idx = (p, o) => p.reduce((xs, x) => (xs && xs[x]) ? xs[x] : null, o)

        function filterData(filterObj, originalData, filterArr, column) {
            if (filterArr.length) {
                filterObj[column] = filterArr;
            }
            else {
                delete filterObj[column];
            }
            let updatedData = JSON.parse(JSON.stringify(originalData));
            // console.log("filterArr", filterArr);
            // console.log("column", column);
            // console.log("filterObj", filterObj);

            if (Object.keys(filterObj).length) {
                Object.keys(filterObj).map(filterKey => {
                    let nestedKeyArray = [];
                    if (filterKey.indexOf('.') >= 0)
                        nestedKeyArray = filterKey.split('.');
                    else
                        nestedKeyArray = [filterKey];
                    updatedData = updatedData.filter((singleData) => filterObj[filterKey].indexOf(idx(nestedKeyArray, singleData)) >= 0);
                })
            }
            return updatedData;
            // $scope.data = updatedData.map((newData) => newData);
        }
        return {
            filterData: filterData
        };
    })