angular.module("myApp").service("Comment",["Restangular", "$location", "$log", function Comment(Restangular, $location, $log) {
    commentObj = {
        comments: [],
        page: 1,
        per_page: 5,
        loaded: false,
        newCommentAdded: false
    };

    function getComments(id, reference_type) {
        dataobject.loaded = false;
        Restangular.all("comments")
            .customGET("",{
                "reference_id": id,
                "reference_type": reference_type,
                "page": commentObj.page,
                "per_page": commentObj.per_page
            })
            .then(function (response) {
                commentObj.comments = response.data;
                commentObj.loaded = true;
            });
    }

    function postComment(id, reference_type, comment_text) {
        commentObj.loaded = false;
        var params = {
            reference_id: id,
            reference_type: reference_type,
            value: comment_text
        };
        Restangular.all("comments")
            .post(params)
            .then(function (response) {
                console.log(response);
                commentObj.loaded = true;
            }, function (error) {
                console.log(error);
                commentObj.loaded = true;
            }
        );
    }
    function setPage(page) {
        commentObj.page = page;
    }

    return {
        details: commentObj,
        getComments: getComments,
        postComment: postComment,
        setPage: setPage
    };
    // return {
    //     data: dataobject
    // };
}]);