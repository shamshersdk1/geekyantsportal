angular.module("myApp").service("LeaveService", [
	"Restangular",
	"$location",
	"$log",
	"$q",
	function LeaveService(Restangular, $location, $log, $q) {
		dataObject = {
			loading : false,
			optional_holidays : [],
			typesVar : [],
			types : [],
			categories : [
				{
			        code : 'paid',
			        title : 'Paid Leave'
			    },
			    {
			        code : 'sick',
			        title : 'Sick Leave'
			    },
			    {
			        code : 'optional-holiday',
			        title : 'Optional Holiday'
			    },
			    {
			        code : 'other',
			        title : 'Other'
			    }
			],
			init : function() {
				var optionalHoliday = Restangular.all('leave/optional-holiday').getList();
				var leaveTypes =Restangular.all('leave-type').getList();
				$q.all([optionalHoliday, leaveTypes, dataObject.loading = true]).then(function(
		          result
		        ) {
		          if (result[0]) {
		            dataObject.optional_holidays = result[0];
		          }
		          if (result[1]) {
		          	dataObject.types = result[1];
		          	dataObject.typesVar = result[1];
		          }
		          dataObject.loading = false;
		        });
			}
		};
		
		function getLeaveTypes() {
			return Restangular.all('leave-type').getList();
		}
		function getOptionalholidays() {
			return Restangular.all('leave/optional-holiday').getList();
		}
		
		function getUserLeaveBalance(userId) {
        };
        function applyLeave(params) {
        };

        return {
	        categories: dataObject.categories,
	        typesVar : dataObject.types,
	        optional_holidays : dataObject.optional_holidays,
	        loading : dataObject.loading,
	        init : dataObject.init,
	        getLeaveTypes : getLeaveTypes,
	        getOptionalholidays : getOptionalholidays
	    };
	}
]);
