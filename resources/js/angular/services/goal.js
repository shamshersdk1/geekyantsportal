angular.module("myApp").service("Goal", ["Restangular", "$location", "$log",
    function Goal(Restangular, $location, $log) {
        dataobject = {
            goals: [],
            userGoalItems: [],
            page: 1,
            per_page: 5,
            loaded: false,
            userGoalItemsLoaded: false,
            newGoalAdded: false,
            userGoalLoaded: false,
            userGoal: []
        };

        function getGoals(search) {
            dataobject.loaded = false;
            Restangular.all("goals")
                .customGET("filter", {
                    "name": search.text,
                    "tags[]": search.tags,
                    "page": dataobject.page,
                    "per_page": dataobject.per_page
                })
                .then(function (response) {
                    dataobject.goals = response.data;
                    dataobject.loaded = true;
                });
        }

        function updateUserGoals(params) {
            dataobject.loaded = false;
            $log.log(params);
            Restangular.one("user-goal-items", 1)
                .customPUT(params)
                .then(function (response) {
                    $log.log(response.result);
                    dataobject.loaded = true;
                });
        }

        function saveGoal(params, is_private, is_idp) {
            Restangular.all("goals")
                .post(params)
                .then(function (response) {

                    // if(response.result) {
                    //     attachGoal(1, response.result, is_private, is_idp);
                    // }
                });
        }

        function getUserGoals(id, params) {
            dataobject.userGoalItemsLoaded = false;
            Restangular.one("user-goals", id)
                .customGET("get-items", params)
                .then(function (response) {
                    dataobject.userGoalItems = response.data;
                    dataobject.userGoalItemsLoaded = true;
                });
        }

        function attachGoal(user_goal_id, goal_id, is_private, is_idp) {
            dataobject.newGoalAdded = false;
            params = {
                user_goal_id: user_goal_id,
                goal_id: goal_id,
                is_private: is_private,
                is_idp: is_idp,
            }
            Restangular.all("user-goal-items")
                .post(params)
                .then(function (response) {
                    dataobject.newGoalAdded = true;
                });
        }

        function detachGoal(user_goal_item_id) {
            dataobject.newGoalAdded = false;
            Restangular.one("user-goal-items", user_goal_item_id)
                .remove()
                .then(function (response) {
                    $log.log(response.result);
                    dataobject.newGoalAdded = true;
                });
        }

        function updateGoal(id) {
            dataobject.userGoalLoaded = false;
            Restangular.one("user-goals", id)
                .get()
                .then(function (response) {
                    dataobject.userGoal = response.data;
                    dataobject.userGoalLoaded = true;
                });
        }

        return {
            data: dataobject,
            getGoals: getGoals,
            getUserGoals: getUserGoals,
            saveGoal: saveGoal,
            attachGoal: attachGoal,
            detachGoal: detachGoal,
            updateUserGoals: updateUserGoals,
            updateGoal: updateGoal
        };
    }]);