angular.module("myApp").service("Payslip", [
	"Restangular",
	"$location",
	"$log",
	function Payslip(Restangular, $location, $log) {
		var dataObject = {
			loading: false,
			updating: false,
			error: false,
			loaded: false,
			errorMessage: null,
			code: null,
			text: null,
			payload: null,
			id: null,
			step: 1,
		};
		dataObject.id = window.id;
		function init() {
			dataObject.loading = true;
			var params = {
				id: dataObject.id
			}
			Restangular.one("payroll-csv-data")
				.get(params)
				.then(
					function (response) {
						dataObject.payload = response.data;
						angular.forEach(dataObject.payload.list, function(item) {
							console.log('Item', item);
							item.updateRecord = function() {
								console.log('updateRecord called');
								// Restangular.all("payroll-csv-data/" + dataObject.id)
								// 	.customPUT(params)
								// 	.then(
								// 		function (response) {
								// 			result.code = "message";
								// 			result.text = response.result;
								// 		},
								// 		function (errors) {
								// 			result.code = "error";
								// 			result.text = errors.data.message;
								// 		}, function() {
								// 			dataObject.updating = false;
								// 		});
							}

						});
						dataObject.loading = false;
						dataObject.loaded = true;
					},
					function (errors) {
						dataObject.loading = false;
						dataObject.errors = true;
						dataObject.errorMessage = "Unable to fetch records";
					}
					);
		}
		function updateRow(params) {
			var result = {
				code: "error",
				message: "Something went wrong"
			};
			dataObject.updating = true;
			Restangular.all("payroll-csv-data/" + dataObject.id)
				.customPUT(params)
				.then(
					function (response) {
						result.code = "message";
						result.text = response.result;
					},
					function (errors) {
						result.code = "error";
						result.text = errors.data.message;
					}, function() {
						dataObject.updating = false;
					});
				return result;
		}
		function gotoRoute($step) {
			$location.path("step" + $step);
		}

		return {
			updateRow: updateRow,
			data: dataObject,
			gotoRoute: gotoRoute,
			init: init
		};
	}
]);
