angular.module('myApp')
    .controller('feedbackUserListCtrl',["$scope", "Restangular", function ($scope, Restangular) {
        $scope.user = JSON.parse(window.user);
        $scope.months = [];
        $scope.userList = [];
        $scope.selectedMonth;
        $scope.error = false;
        $scope.loading = false;
        
        $scope.errorMessage = '';
        
        Restangular.all("feedback-month").getList({
            'pagination': 0
        })
        .then(
            function (response) {
                $scope.months = response;
                $scope.selectedMonth = $scope.months[0].id;
                $scope.getUserList();
                
            },
            function (errors) {
                console.log(errors);
            }
        );

        $scope.getUserList = function () {
            $scope.error = false;
            Restangular.all("feedback-user").getList({
                'pagination': 0,
                'feedback_month_id': $scope.selectedMonth
            })
            .then(
                function (response) {
                    $scope.loading = false;
                    $scope.userList = response;
                },
                function (errors) {
                    $scope.loading = false;
                    console.log(errors);
                }
            );
        };
        $scope.selectMonth= function () {
            $scope.loading = true;
            $scope.getUserList();
        };
        $scope.review = function (userId) {
            var path = "/admin/feedback/review/"+userId+'/'+$scope.selectedMonth;
            window.location.href =  path;
        }

    }]);