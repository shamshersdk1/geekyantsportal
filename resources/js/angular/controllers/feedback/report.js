
angular.module('myApp')
    .controller('feedbackReportCtrl', ["$scope", "$uibModal", "Restangular", function ($scope, $uibModal, Restangular) {
        $scope.data = window.feedbakUsers;
        $scope.month = window.feedbackMonth;
        $scope.loading = false;
    
        console.log($scope.data);
        $scope.form ={
            loading : false
        }
        $scope.data.forEach(function(feedback) {
            feedback.notifyOnSlack = function(id, type, role, $index){
                var url = '';
                if(type != 'all') {
                    url = 'feedback-review-notify';
                    feedback.reviews[$index].sent = true;
                } else {
                    if (confirm("Are you sure?")) {
                        url = 'feedback-review-notify-all';
                        feedback.sent_all = true;
                    } else {
                        return false;
                    }
                }
                
                Restangular.all(url).post({'feedback_user_id' : id}).then(function(response) {
                    $scope.form.loading = false;
                    feedback.sent = true;
                    //$uibModalInstance.dismiss();
                }, function(error){
                    $scope.form.loading = false;
                    console.log('feedback-review-notify ', error);
                });
            };
            feedback.regenerateRecords = function( user_id, month_id ) {
                var r = confirm("Are you sure you want to regenerate feedback records for the user?");
                if (r != true) {
                    return;
                }
                $scope.loading = true;
                var params = {
                    month_id: month_id,
                    user_id: user_id
                };
                Restangular.all('feedback-review-regenerate-user').post(params).then(function(response) {
                    window.location.reload();
                    $scope.loading = false;
                }, function(error){
                    console.log(error);
                    $scope.loading = false;
                });
            };
        });
        $scope.lockMonth = function () {
            var params = {
                id: $scope.month.id
            };
            Restangular.all('feedback-month/lock').post(params).then(
                function (response) {
                    if (response) {
                        $scope.month.locked = true;
                    }
                },
                function (errors) {
                    console.log(errors);
                }
            );
        };
        $scope.regenerateAllRecords = function() {
            var r = confirm("Are you sure you want to regenerate feedback records for all users for this month?");
            if (r != true) {
                return;
            }
            $scope.loading = true;
            var params = {
                month : $scope.month.month,
                year : $scope.month.year
            };
            Restangular.all('feedback-review-regenerate-all').post(params).then(function(response) {
                window.location.reload();
                $scope.loading = false;
            }, function(error){
                console.log(error);
                $scope.loading = false;
            });

        };
         
    }]).controller('feedbackReportModalCtrl',["$scope", "Restangular", "data", "$uibModalInstance", function ($scope, Restangular, data, $uibModalInstance) {
        $scope.form = {
            notifyOnSlack : function() {
                $scope.form.loading = true;
                var url = 'feedback-review-notify';
                if(data.type == 'all') {
                    url = 'feedback-review-notify-all';
                }
                Restangular.all(url).post({'feedback_user_id' : data.id}).then(function(response) {
                    $scope.form.loading = false;
                    $uibModalInstance.dismiss();
                }, function(error){
                    $scope.form.loading = false;
                    console.log('feedback-review-notify ', error);
                })
            }
        }
        
        $scope.cancel = function () {
            $uibModalInstance.dismiss();
        };
    }]);