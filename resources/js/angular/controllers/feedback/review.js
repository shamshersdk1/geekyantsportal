angular
    .module("myApp")
    .controller("feedbackUserReviewCtrl", ["$scope", "Restangular", function ($scope, Restangular) {
        var reviews = window.reviews;
        $scope.month = window.month;
        $scope.data = {};
        $scope.reviews = reviews.reviewer_type;
        $scope.percentage = 0;
        $scope.getProgressStatus = function () {
            $scope.error = false;
            Restangular.one("feedback-user-status").get({
                'user_id': reviews.for_user.id,
                'feedback_month_id': $scope.month.id,
            })
            .then(
                function (response) {
                    $scope.percentage = response;
                },
                function (errors) {
                    $scope.loading = false;
                    console.log(errors);
                }
            );
        };
        $scope.getProgressStatus();
        reviews.reviewer_type.forEach( function(type) {
            $scope.data[type.code] = [];
            reviews[type.code].forEach(function(project) {
                project.project_reviews.forEach(function(review) {
                    review.saveRating = function (param) {
                        review.ratingLoading = true;
                        review.rating = param;
                        if ( review.rating < 5 || review.rating > 9 )
                        {
                            review.rating_comments = '';
                            review.saveRatingComment();
                        }
                        var params = {
                            rating: review.rating,
                            id: review.id
                        };
                        Restangular.all("feedback-review")
                            .post(params)
                            .then(
                                function (response) {
                                    if (response) {
                                        review.ratingLoading = false;
                                        $scope.getProgressStatus();
                                    }
                                },
                                function (errors) {
                                    review.ratingLoading = false;
                                    console.log(errors);
                                }
                            );
                    };
                    review.saveComment = function () {
                        review.commentLoading = true;
                        const params = {
                            commentable_id: review.id,
                            commentable_type: "App\\Models\\Admin\\FeedbackUser\\General",
                            message: review.comments
                        };
                        Restangular.all("comments")
                            .post(params)
                            .then(
                                function (response) {
                                    if (response) {
                                        review.commentLoading = false;
                                    }
                                },
                                function (errors) {
                                    review.commentLoading = false;
                                    console.log(errors);
                                }
                            );
                    };
                    review.saveRatingComment = function () {
                        review.ratingCommentLoading = true;
                        if ( review.rating < 5 )
                        {
                            review.is_private = "false";
                        }
                        const params = {
                            commentable_id: review.id,
                            commentable_type: "App\\Models\\Admin\\FeedbackUser\\Restricted",
                            message: review.rating_comments,
                            is_private: review.is_private
                        };
                        Restangular.all("comments")
                            .post(params)
                            .then(
                                function (response) {
                                    if (response) {
                                        review.ratingCommentLoading = false;
                                    }
                                },
                                function (errors) {
                                    review.ratingCommentLoading = false;
                                    console.log(errors);
                                }
                            );
                    };
                    review.updateNA = function (param) {
                        var r = confirm("Are you sure you want to mark it as not applicable?");
                        if (r != true) {
                            return;
                        }
                        review.naLoading = true;
                        review.rating = param;
                        var params = {
                            id: review.id
                        };
                        Restangular.all("feedback-review-na")
                            .post(params)
                            .then(
                                function (response) {
                                    if (response) {
                                        review.naLoading = false;
                                        review.status = 'not_applicable';
                                        review.rating = null;
                                        $scope.getProgressStatus();
                                    }
                                },
                                function (errors) {
                                    review.ratingLoading = false;
                                    console.log(errors);
                                }
                            );
                    };
                });
                $scope.data[type.code].push(project);
            });
        });
        console.log($scope.data);
    }]);