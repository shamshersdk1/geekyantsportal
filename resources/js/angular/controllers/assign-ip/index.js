angular.module("myApp").controller("assignIpCtrl", [
  "$scope",
  "Restangular",
  "$uibModal",
  function($scope, Restangular, $uibModal) {
    $scope.users = JSON.parse(window.users);
    angular.element(document).ready(function() {
      dTable = $("#user_ip_table");
      dTable.DataTable({ pageLength: 500 });
    });
    $scope.showModal = function(user) {
      var modal = $uibModal.open({
        controller: "assignIpModalCtrl",
        windowClass: "bootstrap_wrap",
        templateUrl: "/views/assign-ip/modal.html",
        backdrop: "static",
        size: "md",
        resolve: {
          data: function() {
            return { user: user };
          }
        }
      });
      modal.result.then(
        function(result) {
          window.location.reload();
        },
        function() {
          //cancel
        }
      );
    };
  }
]);
