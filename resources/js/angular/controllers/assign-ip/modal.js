angular.module("myApp").controller("assignIpModalCtrl", [
  "$scope",
  "Restangular",
  "data",
  "$uibModalInstance",
  "$q",
  function($scope, Restangular, data, $uibModalInstance, $q) {
    $scope.userData = data.user;
    $scope.form = {
      loading: false,
      selected_range: "",
      showBtn: false,
      error: false,
      rangeError: false,
      rangeErrorMsg: "",
      newItem: false,
      submit: function(assginIpfrom) {
        if (!assginIpfrom.$valid) {
          $scope.form.error = true;
          return;
        }
        $scope.form.loading = true;
        var params = {
          user_id: $scope.userData.id,
          range: $scope.form.selected_range.id
        };
        Restangular.all("assign-ip")
          .post(params)
          .then(
            function(response) {
              $scope.form.loading = false;
              $scope.form.newItem = true;
              $scope.userIpList.unshift(response);
            },
            function(error) {
              $scope.form.loading = false;
              $scope.form.rangeError = true;
              $scope.form.rangeErrorMsg = error.data.message;
            }
          );
      },
      cancel: function() {
        $uibModalInstance.dismiss();
      },
      close: function() {
        $uibModalInstance.close();
      },
      releaseIp: function(index, ipRow) {
        var r = confirm("Are you sure you want to release this Ip?");
        if (r != true) {
          return;
        }
        Restangular.one("user-ip-address", ipRow.id)
          .remove()
          .then(
            function(response) {
              $scope.userIpList.splice(index, 1);
              if (index == 0) {
                $scope.form.newItem = false;
              }
            },
            function(error) {
              console.log(error);
            }
          );
      }
    };
    var param = {
      user_id: $scope.userData.id
    };
    var getIpRangeList = Restangular.all("ip-address-ranges").getList();
    var getUserIpList = Restangular.all("user-ip-address").getList(param);

    $q.all([getIpRangeList, getUserIpList, ($scope.form.loading = true)]).then(
      function(result) {
        $scope.form.loading = false;
        if (result[0]) {
          $scope.ipRangeList = result[0];
        }
        if (result[1]) {
          $scope.userIpList = result[1];
        }
      }
    );

    $scope.select_range = function(data) {
      $scope.form.showBtn = true;
      $scope.form.selected_range = data;
    };
  }
]);
