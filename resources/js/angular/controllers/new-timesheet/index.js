angular.module("myApp").controller("newTimesheetCtrl", [
  "$scope",
  "Restangular",
  function ($scope, Restangular) {
    $scope.dates = [];
    var date = new Date();
    $scope.projectList = [];
    $scope.selectedProject = [];
    $scope.tasks = [];
    $scope.loggedTime = [];
    $scope.currentDate = new Date().toISOString().slice(0, 10);
    $scope.selectedDate = $scope.currentDate;
    $scope.error = false;
    $scope.taskError = [];
    $scope.durationError = [];
    $scope.errorMessage = "";
    for (var i = 0; i < 45; i++) {
      var dateString = date.toISOString().slice(0, 10);
      $scope.dates.push(dateString);
      date.setDate(date.getDate() - 1);
    }
    Restangular.all("new-timesheet/projects")
      .getList({
        pagination: 0
      })
      .then(
        function (response) {
          $scope.projectList = response;
        },
        function (errors) {
          console.log(errors);
        }
      );
    $scope.selectProject = function (data, i) {
      $scope.selectedProject[i] = data;
    };

    $scope.submit = function () {
      $scope.error = false;
      for (i = 0; i < 45; i++) {
        if ($scope.selectedProject[i]) {
          if (!$scope.tasks[i]) {
            $scope.taskError[i] = true;
            $scope.error = true;
          } else {
            $scope.taskError[i] = false;
          }
          if (!$scope.tasks[i] || !$scope.loggedTime[i]) {
            $scope.durationError[i] = true;
            $scope.error = true;
          } else {
            $scope.durationError[i] = false;
          }
        }
      }

      if ($scope.error) {
        return;
      }
      var params = {
        date: $scope.selectedDate,
        projects: $scope.selectedProject,
        tasks: $scope.tasks,
        durations: $scope.loggedTime
      };
      Restangular.all("new-timesheet")
        .post(params)
        .then(
          function (response) {
            console.log("REST ", response);
            $scope.finalsubmit = false;
            if (response) {
              var path = "/admin/new-timesheet/history";
              window.location.href = path;
            }
          },
          function (errors) {
            console.log(errors);
            $scope.finalsubmit = false;
            $scope.error = true;
            $scope.errorMessage = errors.data.message;
          }
        );
    };
  }
]);
