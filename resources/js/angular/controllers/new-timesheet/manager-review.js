angular
  .module("myApp")
  .controller("newTimesheetLeadViewCtrl", [
    "$scope",
    "Restangular",
    function($scope, Restangular) {
      $scope.user = JSON.parse(window.user);
      var per_page = 10000;
      var page = 1;
      var project = "";

      $scope.dataIsLoading = true;
      $scope.timesheetHistory = [];
      $scope.projectList = [];
      $scope.selectedProject = "";
      $scope.success = [];
      $scope.error = false;
      $scope.taskError = false;
      $scope.durationError = false;
      $scope.saving = [];
      $scope.timesheetHistoryApproved = [];
      $scope.timesheetHistoryRejected = [];

      $scope.fetchList = function() {
        Restangular.all("new-timesheet")
          .getList({
            pagination: 1,
            page: page,
            per_page: per_page,
            relations: "user,project,review",
            project: project,
            lead_id: 1,
            status: "pending"
          })
          .then(
            function(response) {
              $scope.timesheetHistory = response;
              $scope.metaData = response.metaData;
              $scope.dataIsLoading = false;
            },
            function(errors) {
              console.log(errors);
              $scope.dataIsLoading = false;
            }
          );
      };
      $scope.fetchApprovedList = function() {
        Restangular.all("new-timesheet")
          .getList({
            pagination: 1,
            page: page,
            per_page: per_page,
            relations: "user,project,review",
            project: project,
            lead_id: 1,
            status: "approved"
          })
          .then(
            function(response) {
              $scope.timesheetHistoryApproved = response;
              $scope.metaData = response.metaData;
            },
            function(errors) {
              console.log(errors);
            }
          );
      };
      $scope.fetchRejectedList = function() {
        Restangular.all("new-timesheet")
          .getList({
            pagination: 1,
            page: page,
            per_page: per_page,
            relations: "user,project,review",
            project: project,
            lead_id: 1,
            status: "rejected"
          })
          .then(
            function(response) {
              $scope.timesheetHistoryRejected = response;
              $scope.metaData = response.metaData;
            },
            function(errors) {
              console.log(errors);
            }
          );
      };

      $scope.getAssignedProjects = function() {
        Restangular.all("new-timesheet/managed/projects")
          .getList()
          .then(
            function(response) {
              $scope.projectList = response;
            },
            function(errors) {
              console.log(errors);
            }
          );
      };

      $scope.getAssignedProjects();
      $scope.fetchList();
      $scope.fetchApprovedList();
      $scope.fetchRejectedList();

      $scope.selectProject = function(data) {
        $scope.selectedProject = data;
        project = "project_id," + data.id;
        $scope.fetchList();
      };

      $scope.update = function(data) {
        $scope.success[data.id] = false;
        $scope.saving[data.id] = false;
        if (!data.review.approved_duration || !data.review.approval_comments) {
          return;
        }

        Restangular.one("new-timesheet/approval", data.id)
          .customPUT({
            data: data
          })
          .then(
            function(response) {
              $scope.success[data.id] = true;
              $scope.saving[data.id] = false;
            },
            function(errors) {
              $scope.success[data.id] = false;
              $scope.saving[data.id] = false;
            }
          );
      };

      $scope.changePage = function() {
        page = $scope.metaData.current_page;
        $scope.fetchList();
      };

      $scope.approve = function(data) {
        $scope.success[data.id] = false;
        $scope.saving[data.id] = true;
        if (
          data.review
            ? !data.review.approved_duration || !data.review.approval_comments
            : false
        ) {
          return;
        }

        Restangular.one("new-timesheet/approval", data.id)
          .customPUT({
            data: data
          })
          .then(
            function(response) {
              $scope.success[data.id] = true;
              $scope.saving[data.id] = false;
              data.status = "approved";
              var index = $scope.timesheetHistory.indexOf(data);
              if (index !== -1) $scope.timesheetHistory.splice(index, 1);
              data.review.approver_id = $scope.user.id;
            },
            function(errors) {
              $scope.success[data.id] = false;
              $scope.saving[data.id] = false;
            }
          );
      };

      $scope.reject = function(data) {
        var r = confirm("Are you sure you want to reject this entry?");
        if (r != true) {
          return;
        }
        $scope.success[data.id] = false;
        $scope.saving[data.id] = true;
        Restangular.one("new-timesheet/reject", data.id)
          .customPUT({
            data: data
          })
          .then(
            function(response) {
              $scope.success[data.id] = true;
              $scope.saving[data.id] = false;
              data.status = "rejected";
              var index = $scope.timesheetHistory.indexOf(data);
              if (index !== -1) $scope.timesheetHistory.splice(index, 1);
              data.review.approver_id = $scope.user.id;
            },
            function(errors) {
              $scope.success[data.id] = false;
              $scope.saving[data.id] = false;
            }
          );
      };
      $scope.loadPendingList = function() {
        $scope.fetchList();
      };

      $scope.loadApprovedList = function() {
        $scope.fetchApprovedList();
      };
      $scope.loadRejectedList = function() {
        $scope.fetchRejectedList();
      };
    }
  ])
  .filter("moment", function() {
    return function(input, momentFn /*, param1, param2, ...param n */) {
      var args = Array.prototype.slice.call(arguments, 2),
        momentObj = moment(input);
      return momentObj[momentFn].apply(momentObj, args);
    };
  });
