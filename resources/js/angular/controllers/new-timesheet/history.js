angular.module('myApp').controller('newTimesheetHistoryCtrl', [
  '$scope',
  'Restangular',
  function($scope, Restangular) {
    var per_page = 100;
    var page = 1;
    var tmpPage = 1;
    var project = '';
    $scope.tmpTimesheetHistory = [];
    $scope.timesheetHistory = [];
    $scope.projectList = [];
    $scope.selectedProject = '';
    $scope.success = [];
    $scope.successTemp = [];
    $scope.error = false;
    $scope.error_message = [];
    $scope.saving = [];
    $scope.savingTemp = [];
    $scope.timesheet_error = [];

    $scope.fetchList = function() {
      Restangular.all('new-timesheet')
        .getList({
          pagination: 1,
          page: page,
          per_page: per_page,
          relations: 'project,review',
          project: project
        })
        .then(
          function(response) {
            $scope.timesheetHistory = response;
            $scope.metaData = response.metaData;
          },
          function(errors) {
            console.log(errors);
          }
        );
    };

    $scope.fetchTempList = function() {
      Restangular.all('tmp-timesheet')
        .getList({
          pagination: 1,
          page: tmpPage,
          per_page: per_page
        })
        .then(
          function(response) {
            $scope.tmpTimesheetHistory = response;
            $scope.tmpMetaData = response.metaData;
          },
          function(errors) {
            console.log(errors);
          }
        );
    };

    $scope.getAssignedProjects = function() {
      Restangular.all('new-timesheet/projects')
        .getList({
          pagination: 0
        })
        .then(
          function(response) {
            $scope.projectList = response;
          },
          function(errors) {
            console.log(errors);
          }
        );
    };

    $scope.getAssignedProjects();
    $scope.fetchList();
    $scope.fetchTempList();

    $scope.selectProject = function(data) {
      $scope.selectedProject = data;
      project = 'project_id,' + data.id;
      $scope.fetchList();
    };

    $scope.update = function(data) {
      $scope.saving[data.id] = true;
      $scope.success[data.id] = false;
      $scope.error = false;
      $scope.error_message[data.id] = '';
      Restangular.one('new-timesheet', data.id)
        .customPUT({
          data: data
        })
        .then(
          function(response) {
            $scope.saving[data.id] = false;
            $scope.success[data.id] = true;
          },
          function(errors) {
            $scope.savingTemp[data.id] = false;
            $scope.timesheet_error[data.id] = true;
            $scope.error_message[data.id] = errors.data.message;
          }
        );
    };

    $scope.changePage = function() {
      page = $scope.metaData.current_page;
      $scope.fetchList();
    };
    $scope.changeTempPage = function() {
      tmpPage = $scope.tmpMetaData.current_page;
      $scope.fetchTempList();
    };

    $scope.updateTemp = function(data) {
      $scope.savingTemp[data.id] = true;
      $scope.loading = true;
      $scope.successTemp[data.id] = false;
      $scope.error = false;
      Restangular.one('tmp-timesheet', data.id)
        .customPUT({
          data: data
        })
        .then(
          function(response) {
            $scope.savingTemp[data.id] = false;
            $scope.successTemp[data.id] = true;
          },
          function(errors) {
            $scope.savingTemp[data.id] = false;
            $scope.error = true;
            console.log($scope.error);
          }
        );
    };

    $scope.delete = function(data, index) {
      $scope.saving[data.id] = true;
      $scope.success[data.id] = false;
      $scope.error = false;
      Restangular.one('new-timesheet', data.id)
        .remove()
        .then(
          function(response) {
            $scope.saving[data.id] = false;
            $scope.success[data.id] = true;
            $scope.timesheetHistory.splice(index, 1);
          },
          function(errors) {
            $scope.savingTemp[data.id] = false;
            $scope.error = true;
          }
        );
    };

    $scope.deleteTemp = function(data, index) {
      $scope.saving[data.id] = true;
      $scope.success[data.id] = false;
      $scope.error = false;
      Restangular.one('tmp-timesheet', data.id)
        .remove()
        .then(
          function(response) {
            $scope.saving[data.id] = false;
            $scope.success[data.id] = true;
            $scope.tmpTimesheetHistory.splice(index, 1);
          },
          function(errors) {
            $scope.savingTemp[data.id] = false;
            $scope.error = true;
          }
        );
    };
  }
]);
