angular
  .module("myApp")
  .controller("sendMessageModalCtrl", ["$scope",
    "Restangular",
    "data",
    "$uibModalInstance",function(
    $scope,
    Restangular,
    data,
    $uibModalInstance
  ) {
    $scope.data = { recieved_data: data, error: false, errorMessage: "", messageSent: false, as_user: false, message: "", botname: "", sending : false };
    $scope.errorDismiss = function() {
      $scope.data.error = false;
      $scope.data.errorMessage = "";
    };
    $scope.setAsUser = function(val) {
      $scope.data.as_user = val;
    };
    $scope.messageDismiss = function() {
      $scope.data.messageSent = false;
    };
    $scope.send = function() {
        $scope.data.sending = true;
        if (!$scope.data.message) {
            $scope.data.sending = false;
            $scope.data.error = true;
            $scope.data.errorMessage = "Message can't be empty";
            return false;
        }
        var params = { user_id: $scope.data.recieved_data.user.id, message: $scope.data.message, as_user: $scope.data.as_user, botname: $scope.data.botname == "" ? "Ant-Manager" : $scope.data.botname };
        Restangular.all("slack-message")
        .post(params)
        .then(
          function(response) {
              $scope.data.sending = false;
              $scope.data.messageSent = true;
            // $uibModalInstance.close(data.id);
            // window.scrollTo(0, 0);
          },
          function(errors) {
            $scope.data.sending = false;
            $scope.data.error = true;
            $scope.data.errorMessage = errors.data.message;
          }
        );
    };
    $scope.cancel = function() {
      $uibModalInstance.dismiss();
    //   window.location.reload();
    };
  }]);