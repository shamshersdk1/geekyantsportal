angular.module('myApp')
    .controller("slackmapCtrl",["$scope", "Restangular", "$uibModal", function ($scope, Restangular, $uibModal) {
        $scope.select2Options = {
            allowClear: true
        };

        $scope.test = "Geek";
        $scope.data = {
            error : false,
            errorMessage : '',
            messageSent : false,
        };
        $scope.users = JSON.parse(window.users);
        $scope.slackUsers = JSON.parse(window.slackUsers);
        $scope.assigned = JSON.parse(window.assigned);
        $showError = [];

        $scope.users.forEach(function(element) {
            var status = {
                'code' : '',
                'error' : "",
            };
            $showError[element.id] = status;
        });
        $scope.showError = $showError;
        $scope.update = function($user_id) {
            if($scope.assigned[$user_id] == null) {
                $slack_id = null;
            } else {
                $slack_id = $scope.assigned[$user_id].id;
            }
            var params = {
                'user_id' : $user_id,
                'slack_id' : $slack_id,
            };
            Restangular.all('slack-user-map/'+$user_id).customPUT(params).then(
                function (response) {
                    $showError[$user_id].code = "message";
                    $showError[$user_id].text = response.result;
                },
                function (errors) { 
                    $showError[$user_id].code = "error";
                    $showError[$user_id].text = errors.data.message;
                }
            );
        };
        $scope.errorDismiss = function() {
            $scope.data.error = false;
            $scope.data.errorMessage = "";
        }
        $scope.messageDismiss = function() {
            $scope.data.messageSent = false;
        };
        $scope.showModal = function(user) {
            if ($scope.assigned[user.id] == null) {
                $scope.data.error = true;
                $scope.data.errorMessage = "This user doesn't have a slack id, message can't be sent";
                window.scrollTo(0,0);
                return;
            }
            var modal = $uibModal.open({
              controller: "sendMessageModalCtrl",
              windowClass: "bootstrap_wrap",
              templateUrl: "/views/send_message.html",
              backdrop: "static",
              size: "md",
              resolve: {
                data: function() {
                  return { user: user, slack_email: $scope.assigned[user.id].email };
                }
              }
            });
        };
    }])