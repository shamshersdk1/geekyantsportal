angular.module("myApp").controller("networkAssetrCtrl", [
  "$scope",
  "Restangular",
  "$uibModal",
  "DTOptionsBuilder",
  "Datatable",
  function($scope, Restangular, $uibModal, DTOptionsBuilder, Datatable) {
    $scope.filter = {};
    $scope.asset = {
      data: []
    };
    $scope.error = false;
    $scope.errorMsg = "";

    $scope.init = function() {
      var params = { pagination: 0 };
      Restangular.all("network-asset")
        .getList(params)
        .then(
          function(response) {
            $scope.asset.data = response;
            $scope.originalData = response;
          },
          function(error) {
            $scope.error = true;
            $scope.errorMsg = error.data.message;
          }
        );
    };
    $scope.dtOptions = DTOptionsBuilder.newOptions()
      .withOption("order", [[0, "DESC"]])
      .withOption("scrollY", "300px")
      .withOption("scrollX", "100%")
      .withOption("scrollCollapse", true)
      .withOption("pageLength", 1000)
      .withOption("lengthMenu", [50, 100, 150, 200, 500, 1000]);

    $scope.assignIp = function(asset, meta) {
      var params = {
        asset_id: asset.id,
        meta_id: meta.id
      };
      Restangular.all("network-asset/assign-ip")
        .post(params)
        .then(
          function(response) {
            meta.ip_mapper = response;
          },
          function(error) {
            $scope.error = true;
            $scope.errorMsg = error.data.message;
          }
        );
    };
    $scope.releaseIp = function(meta) {
      console.log(meta, "fghjk");
      Restangular.one("network-asset/release-ip", meta.ip_mapper.id)
        .get()
        .then(
          function(response) {
            meta.ip_mapper = null;
          },
          function(error) {
            $scope.error = true;
            $scope.errorMsg = error.data.message;
          }
        );
    };
    $scope.init();
  }
]);
