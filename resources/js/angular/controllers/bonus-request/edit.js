angular.module("myApp").controller("editBonusRequestModalCtrl", [
  "$scope",
  "Restangular",
  "data",
  "$uibModalInstance",
  "$q",
  function($scope, Restangular, data, $uibModalInstance, $q) {
    var id = data.id;
    $scope.showSubType = false;
    $scope.types = [
      { name: "Additional Work Day", code: "additional" },
      { name: "On Site", code: "onsite" },
      { name: "Referral", code: "referral" },
      { name: "Tech-talk", code: "techtalk" }
    ];
    $scope.select_type = function(type) {
      angular.forEach($scope.types, function(item) {
        if (type === item.code) {
          $scope.form.data.type = item;
        }
      });
      if (type == "additional") {
        $scope.sub_types = ["Weekend", "National Holiday", "Leave"];
        $scope.showSubType = true;
        $scope.form.data.title = null;
        $scope.form.data.referral_for = null;
      } else if (type == "onsite") {
        $scope.sub_types = [
          "Domestic Local",
          "Domestic On-Site",
          "International"
        ];
        $scope.showSubType = true;
        $scope.form.data.title = null;
        $scope.form.data.referral_for = null;
      } else {
        $scope.showSubType = false;
        $scope.form.data.sub_type = null;
      }
    };
    $scope.select_user = function(user) {
      $scope.form.data.user = user;
    };
    $scope.select_sub_type = function(sub_type) {
      $scope.form.data.sub_type = sub_type;
    };
    $scope.select_referral = function(user) {
      $scope.form.data.referral_for = user.id;
    };
    $scope.form = {
      submitting: false,
      error: false,
      errorMsg: "",
      data: {},
      init: function() {
        var userList = Restangular.all("user-all-active").getList();
        var oldData = Restangular.one("bonus-request", id).get();
        $q.all([userList, oldData, ($scope.form.loading = true)]).then(function(
          result
        ) {
          $scope.form.loading = false;
          if (result[0]) {
            $scope.form.users = result[0];
          }
          if (result[1]) {
            $scope.form.data = result[1];
            $scope.select_type($scope.form.data.type);
            $scope.form.data.date = new Date($scope.form.data.date);
          }
        });
      },
      submit: function($form) {
        $scope.form.submitting = true;
        if (!$form.$valid) {
          $scope.form.error = true;
          return;
        }
        var params = {
          user_id: $scope.form.data.user.id,
          date: $scope.form.data.date,
          type: $scope.form.data.type.code,
          sub_type: $scope.form.data.sub_type,
          title: $scope.form.data.title,
          referral_for: $scope.form.data.referral_for
            ? $scope.form.data.referral_for
            : null,
          status: "approved",
          id: $scope.form.data.id
        };
        Restangular.one("bonus-request", $scope.form.data.id)
          .customPUT({
            params: params
          })
          .then(
            function(response) {
              $uibModalInstance.close(response);
            },
            function(errors) {
              console.log(errors);
            }
          );
      },
      rejectBonusRequest: function(id) {
        console.log(id, "in reject form");
        Restangular.one("bonus-request/" + id + "/reject")
          .get()
          .then(
            function(response) {
              $scope.close();
            },
            function(error) {
              console.log(error);
            }
          );
      }
    };

    $scope.cancel = function() {
      $uibModalInstance.dismiss();
    };
    $scope.close = function() {
      $uibModalInstance.close();
    };
    $scope.form.init();
  }
]);
