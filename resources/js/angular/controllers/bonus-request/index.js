angular.module("myApp").controller("bonusRequestCtrl", [
  "$scope",
  "Restangular",
  "$uibModal",
  "DTOptionsBuilder",
  "Datatable",
  "filterFilter",
  "datetimeFilter",
  function(
    $scope,
    Restangular,
    $uibModal,
    DTOptionsBuilder,
    Datatable,
    filterFilter,
    datetimeFilter
  ) {
    $scope.filter = 'all';
    $scope.type = 'na';
    $scope.data = [];
    $scope.user_id = 0;
    $scope.originalData = [];
    var monthId = window.monthId;
    Restangular.all("user")
      .getList({ pagination: 0 })
      .then(
        function(response) {
          $scope.loading = false;
          $scope.userList = response;
        },
        function(errors) {
          $scope.loading = false;
          $scope.error_message = errors.data.message;
        }
      );
    
    $scope.init = function() {
      getBonusCount();
      getBonusList('all');
    };
    var getBonusCount = function () {
      let params = {
        month_id: monthId
      }
      Restangular.one("bonus-request-count").get(params).then(
        function (response) {
          $scope.bonusCounts = response;
          
        },
        function(errors) {
          console.log(errors);
        }
      )
    };
    
    var getBonusList = function(filter, type, user_id) {
      var params = { status : 'pending', monthId: monthId, filter: filter, pagination: 0, type: type, user_id: user_id };
      Restangular.all("bonus-request")
        .getList(params)
        .then(
          function(response) {
            angular.forEach(response, function(item) {
              item.created_at = datetimeFilter(item.created_at);
              item.redeem_info = JSON.parse(item.redeem_type);
              item.additional_bonus = (item.user.one_day_salary * item.working_hours) / 8;
              if ( item.bonus_request_projects && item.bonus_request_projects.length > 0 )
              {
                tempProjectIds = [];
                for ( j = 0; j < item.bonus_request_projects.length; j++ )
                {
                  tempProjectIds.push(item.bonus_request_projects[j].project_id);
                }
                item.selected_projects = tempProjectIds;
              }

            });

            $scope.data = response;
          },
          function(error) {
            console.log(error);
          }
        );
    };
      
    let getProjects = function() {
      var params = {
        pagination: 0,
        status: 'in_progress'
      };
      Restangular.all("project")
        .getList(params)
        .then(function(projectList) {
          // console.log("ProjectList-->", projectList);
          $scope.projects = projectList;
        })
        .catch(function(projectListErr) {
          console.log("ProjectList failed", projectListErr);
        });
    };
    getProjects();
    
    $scope.approveBonusRequest = function(bonusRequest) {
      Restangular.all("bonus-request-approve")
        .post(bonusRequest)
        .then(
          function(response) {
            $scope.init();
          },
          function(error) {
            console.log(error);
          }
        );
    };
    $scope.rejectBonusRequest = function(bonusRequest) {
      Restangular.all("bonus-request-reject")
        .post(bonusRequest)
        .then(
          function(response) {
            $scope.init();
          },
          function(error) {
            console.log(error);
          }
        );
    };
    $scope.addBonusRequest = function() {
      var modal = $uibModal.open({
        controller: "addBonusRequestModalCtrl",
        windowClass: "bootstrap_wrap",
        templateUrl: "/views/bonus-request/add.html",
        backdrop: "static",
        size: "md",
        resolve: {
          data: function() {
            return { month_id: monthId };
          }
        }
      });
      modal.result.then(
        function(result) {
          window.location.reload();
        },
        function() {
          //cancel
        }
      );
    };
    
    $scope.select_projects = function (bonusRequest) {
      console.log('bonusRequest',bonusRequest);
      var params = {
        bonus_request_id: bonusRequest.id,
        projects: bonusRequest.selected_projects
      };
      
      Restangular.all("bonus-request-projects")
              .post(params)
              .then(
                  function(response) {
                      console.log(response);
                  },
                  function(errors) {
                      console.log(errors);
                  }
              );
    },
    
    $scope.lockMonth = function(monthId) {
      Restangular.one("bonus-request/lock-month/" + monthId)
        .get()
        .then(
          function(response) {
            window.location.reload();
          },
          function(error) {
            console.log(error);
          }
        );
    };

    $scope.dateWithDay = function(date) {
      var d = new Date(date);
      var return_date = moment(d).format("ddd, MMM Do YYYY");
      return return_date;
    }

    $scope.fetchAllBonus = function () {
      $scope.filter = 'all';
      $scope.type = 'na';
      getBonusList($scope.filter, $scope.type);
    };

    $scope.fetchApprovedBonus = function () {
      $scope.filter = 'approved';
      getBonusList($scope.filter, $scope.type);
    };

    $scope.fetchRejectedBonus = function () {
      $scope.filter = 'rejected';
      getBonusList($scope.filter, $scope.type);
    };

    $scope.fetchPendingBonus = function () {
      $scope.filter = 'pending';
      getBonusList($scope.filter, $scope.type);
    };

    $scope.fetchOnsiteBonus = function () {
      $scope.type = 'onsite';
      getBonusList($scope.filter, $scope.type);
    };

    $scope.fetchOnsiteBonus = function () {
      $scope.type = 'onsite';
      getBonusList($scope.filter, $scope.type);
    };

    $scope.fetchAdditionalDayBonus = function () {
      $scope.type = 'additional';
      getBonusList($scope.filter, $scope.type);
    };

    $scope.fetchOtherBonus = function () {
      $scope.type = 'other';
      getBonusList($scope.filter, $scope.type);
    };

    $scope.fetchForUser = function (data) {
      console.log('data', data);
      $scope.user_id = data.id;
      getBonusList($scope.filter, $scope.type, $scope.user_id);
    };

  
    $scope.init();
  }
]);
