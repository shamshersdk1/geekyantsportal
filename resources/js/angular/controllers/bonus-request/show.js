angular.module("myApp").controller("bonusRequestShowCtrl", [
  "$scope",
  "Restangular",
  "$uibModal",
  "$rootScope",
  function($scope, Restangular, $uibModal, $rootScope) {
    $scope.loaded = false;
    var id = window.bonusRequest.id;
    $scope.init = function() {
      Restangular.one("bonus-request/" + id)
        .get()
        .then(
          function(response) {
            $scope.bonusRequestObj = response;
          },
          function(error) {
            console.log(error);
          }
        );
    };
    $scope.rejectBonusRequest = function(bonusRequest) {
      Restangular.one("bonus-request/" + bonusRequest.id + "/reject")
        .get()
        .then(
          function(response) {
            $scope.init();
          },
          function(error) {
            console.log(error);
          }
        );
    };
    $scope.approve = function(bonusRequest) {
      var modal = $uibModal.open({
        controller: "editBonusRequestModalCtrl",
        windowClass: "bootstrap_wrap",
        templateUrl: "/views/bonus-request/edit.html",
        backdrop: "static",
        size: "md",
        resolve: {
          data: function() {
            return { id: bonusRequest.id };
          }
        }
      });
      modal.result.then(
        function(result) {
          window.location.reload();
        },
        function() {
          //cancel
        }
      );
    };
    $scope.init();
  }
]);
