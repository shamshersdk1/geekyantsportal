angular.module("myApp").controller("addBonusRequestModalCtrl", [
  "$scope",
  "Restangular",
  "data",
  "$uibModalInstance",
  "$q",
  function($scope, Restangular, data, $uibModalInstance, $q) {
    $scope.selected_type = null;
    $scope.date = null;
    $scope.selected_user = null;
    $scope.selected_referral_for = null;
    $scope.selected_onsite_allowance = null;
    $scope.selected_sub_type = null;
    $scope.showSubType = false;
    $scope.user_id = data.user_id;
    $scope.name = data.name;
    $scope.error = false;
    $scope.error_message = "";

    $scope.types = [
      { name: "Additional Work Day", code: "additional" },
      { name: "On Site", code: "onsite" },
      { name: "Extra", code: "extra" },
      { name: "Performance", code: "performance" },
      { name: "Tech Talk", code: "techtalk" },
      { name: "Referral", code: "referral" }
      // { name: "Tech Talk", code: "techtalk" },
      // { name: "Refferal", code: "refferal" }
    ];
    $scope.select_type = function(type) {
      $scope.selected_type = type;
      $scope.form.data.type = $scope.selected_type.code;
      if (type.code == "additional") {
        $scope.sub_types = ["Weekend", "National Holiday", "Leave"];
        $scope.showSubType = true;
      } else if (type.code == "onsite") {
        $scope.sub_types = [
          "Domestic Local",
          "Domestic On-Site",
          "International"
        ];
        $scope.showSubType = true;
      } else {
        $scope.showSubType = false;
        $scope.selected_sub_type = null;
        $scope.form.data.sub_type = null;
      }
    };
    $scope.select_user = function(user) {
      $scope.selected_user = user;
      $scope.form.data.user_id = $scope.selected_user.id;
    };
    $scope.select_sub_type = function(sub_type) {
      $scope.selected_sub_type = sub_type;
      $scope.form.data.sub_type = $scope.selected_sub_type;
    };
    $scope.select_referral = function(user) {
      $scope.selected_referral_for = user;
      $scope.form.data.referral_for = $scope.selected_referral_for.id;
    };
    $scope.select_onsite_allowance = function(allowance) {
      $scope.onsite_allowance = allowance;
      $scope.form.data.onsite_allowance = allowance.id;
    };
    $scope.select_additional_project = function(project) {
      console.log("ap", project);
      $scope.additional_project = project;
      $scope.form.data.additional_project = project.id;
    };
    $scope.select_extra_project = function(project) {
      $scope.extra_project = project;
      $scope.form.data.extra_project = project.id;
    };
    $scope.form = {
      submitting: false,
      error: false,
      type_data: [],
      onste_allowances: [],
      init: function() {
        Restangular.all("user-all-active")
          .getList()
          .then(
            function(response) {
              $scope.form.users = response;
            },
            function(error) {
              console.log(error);
            }
          );
      },
      getUserInfo: function() {
        $scope.date = $scope.form.data.date
          ? moment($scope.form.data.date).format("YYYY-MM-DD")
          : null;
        var params = {
          date: moment($scope.form.data.date).format("YYYY-MM-DD"),
          user_id: $scope.user_id,
          type: $scope.form.data.type
        };
        console.log("inside");
        if ($scope.selected_type != null) {
          // Restangular.all("onsite-bonus/user-allowance")
          Restangular.all("bonus-request/get-user-detail")
            .post(params)
            .then(
              function(response) {
                console.log("RES ->--", response);
                if (response != null) {
                  $scope.form.type_data = response;
                }
              },
              function(error) {
                console.log(error);
              }
            );
        }
      },
      data: {
        user_id: $scope.user_id
      },
      submit: function($form) {
        $scope.error = "false";
        $scope.error_message = "";
        console.log("formss", $scope.form.data);
        $scope.form.submitting = true;
        if (!$form.$valid) {
          $scope.form.error = true;
          return;
        }
        $scope.form.data.date = moment($scope.form.data.date).format(
          "YYYY-MM-DD"
        );
        Restangular.all("bonus-request/bonus-approve-payment")
          .post($scope.form.data)
          .then(
            function(response) {
              $uibModalInstance.close(response);
            },
            function(error) {
              console.log(error);
              $scope.error = "true";
              $scope.error_message = error.data.message;
            }
          );
      }
    };

    $scope.cancel = function() {
      $uibModalInstance.dismiss();
    };
    $scope.close = function() {
      $uibModalInstance.close();
    };
    $scope.form.init();
  }
]);
