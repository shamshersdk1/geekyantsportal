angular.module('myApp')
    .controller("credentialIndexCtrl",["$scope", "Restangular", "$uibModal", function ($scope, Restangular, $uibModal) {

        $scope.credentials = JSON.parse(window.credentials);
        angular.element(document).ready(function () {
            dTable = $('#credential_table');
            dTable.DataTable({"pageLength": 1000});
        });
        $scope.showModal = function (credential) {
            var modal = $uibModal.open({
                controller: "credentialModalCtrl",
                windowClass: "bootstrap_wrap",
                templateUrl: "/views/share-credentials/share-credentials.html",
                backdrop: "static",
                size: "md",
                resolve: {
                    data: function () {
                        return { credential: credential };
                    }
                }
            });
        }; 

        $scope.delete = function(credential) {
        if (confirm("Are you sure you want to delete this credential?")) {
            Restangular.one("credential", credential.id)
            .remove()
            .then(
                function(response) {
                    $scope.message = 'Deleted Successfully';
                    window.location.reload();
                },
                function(error) {
                    $scope.error_message = errors.data.message;
                }
            );
        }
        };


    }])