angular.module('myApp')
    .controller("credentialModalCtrl",["$scope", "Restangular","data", "$uibModalInstance", function ($scope, Restangular,data,$uibModalInstance) {
    
    $scope.items = data;
    $scope.selectedUsers = [];
    var list_params = {
                credential_id: $scope.items.credential.id ? $scope.items.credential.id  : ''
            };

    Restangular.all("user").getList({'pagination' : 0})
          .then(
            function(response) {
                $scope.loading = false;
                $scope.userList = response;
              },
            function(errors) {
                $scope.loading = false;
                $scope.error_message = errors.data.message;
            }
          );        
                    
    Restangular.all('shared-users').post(list_params).then(
                function (response) {
                    for (var i = 0; i < response.length; i++) {
                        $scope.selectedUsers.push(response[i]);
                    }
                    $scope.finalsubmit = false;
                },
                function (errors) {
                    $scope.error_message = errors.data.message;
                    // $scope.errorMessage = errors.data.message.message;
                }
            );
           
    $scope.message = '';

    $scope.select_users = function (data) {
            $scope.message = '';
            var params = {
                selected_user: data,
                credential_id: $scope.items.credential.id ? $scope.items.credential.id  : ''
            };
            Restangular.all('credential-sharing').post(params).then(
                function (response) {
                    $scope.finalsubmit = false;
                    if (response.success) {
                        $scope.error_message = '';
                        $scope.message = 'Saved Successfully';
                    }
                },
                function (errors) {
                    $scope.error_message = errors.data.message;
                    // $scope.errorMessage = errors.data.message.message;
                }
            );
        }

    $scope.deselect_users = function (data) {
        $scope.message = '';
        var params = {
                deSelected_user: data,
                credential_id: $scope.items.credential.id ? $scope.items.credential.id  : ''
            };
            
            Restangular.all('credential-sharing-delete').post(params).then(
                function (response) {
                    $scope.finalsubmit = false;
                    if (response.success) {
                        $scope.error_message = '';
                        $scope.message = 'Saved Successfully';
                    }
                },
                function (errors) {
                    $scope.error_message = errors.data.message;
                    // $scope.errorMessage = errors.data.message.message;
                }
            );
            
        }

    $scope.cancel = function() {
      $uibModalInstance.dismiss();
       window.location.reload();
    }
    }]);