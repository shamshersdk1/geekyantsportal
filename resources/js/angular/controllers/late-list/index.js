angular
  .module("myApp")
  .controller("lateListCtrl", ["$scope", "Restangular",function ($scope, Restangular) {
    $scope.loading = false;
    $scope.error = false;
    $scope.error_message = null;
    Restangular.all("user").getList({
        'pagination': 0
      })
      .then(
        function (response) {
          $scope.loading = false;
          $scope.userList = response;
        },
        function (errors) {
          $scope.loading = false;
          $scope.error_message = errors.data.message;
        }
      );

    $scope.selected_user = [];
    $scope.selected_user_error = false;

    $scope.submitted = false;

    $scope.select_user = function (data) {
      $scope.selected_user = data;
      $scope.selected_user_error = false;
      $scope.error = false;
    };
    $scope.datePicker = {
      options: {
        applyClass: "btn-success",
        singleDatePicker: true,
        locale: {
          applyLabel: "Apply",
          fromLabel: "From",
          format: "D-MMM-YY", //will give you 6-Jan-17
          cancelLabel: "Cancel",
        }
      },
      date: {
        selectedDate: new Date()
      }
    };
    $scope.$watch("datePicker.date.selectedDate", function () {
      selectedDate = moment($scope.datePicker.date.selectedDate).format(
        "YYYY-MM-DD"
      );
      $scope.getLateList(selectedDate);
    });

    $scope.getLateList = function (date) {
      $scope.loading = true;
      Restangular.all("late-comers").getList({
          'pagination': 0,
          'date': date,
          'relations': 'user,reportedBy'
        })
        .then(
          function (response) {
            $scope.loading = false;
            $scope.latelist = response;
          },
          function (errors) {
            $scope.loading = false;
            $scope.error_message = errors.data.message;
          }
        );
    };

    $scope.addToLateList = function () {
      $scope.loading = true;
      date = moment($scope.datePicker.date.selectedDate).format("YYYY-MM-DD");
      var params = {
        date: date,
        user: $scope.selected_user
      };

      Restangular.all("late-comers")
        .post(params)
        .then(
          function (response) {
            $scope.loading = false;
            $scope.getLateList(date);
            $scope.selected_user = [];
            $scope.selected_user_error = false;
            $scope.error_message = null;
          },
          function (errors) {
            $scope.loading = false;
            $scope.error = true;
            $scope.error_message = errors.data.message;
          }
        );
    };
    $scope.filteredDate = function (input_date) {
      var formatted_date = new Date(input_date);
      return formatted_date;
    };
    $scope.delete = function (item) {
      Restangular.one("late-comers", item.id)
        .remove()
        .then(
          function (response) {
            $scope.latelist.splice($scope.latelist.indexOf(item), 1);
          },
          function (error) {
            alert(error.data.message);
          }
        );
    };
  }]);