angular.module("myApp").controller("expenseAddModalCtrl", [
    "$scope",
    "Restangular",
    "data",
    "$uibModalInstance",
    function($scope, Restangular, data, $uibModalInstance) {
        $scope.loaded = true;
        $scope.vendors = [];
        $scope.expenseHeads = [];
        $scope.userList = [];
        $scope.purchaseOrderList = [];
        $scope.paymentMethodList = [];
        $scope.status_list = [
            { value : "pending", name : "Pending" },
            { value : "paid", name : "Paid" },
            { value : "hold", name : "Hold" },
            { value : "due_immediate_pending", name : "Due Immediate Pending" },
            { value : "due_next_cycle_pending", name : "Due Next Cycle Pending" },
            { value : "due_immediate_approved", name : "Due Immediate Approved" },
            { value : "due_next_cycle_approved", name : "Due Next Cycle Approved" }
        ];
        
        var getVendors = function(){
            Restangular.all("vendor").getList({
                'pagination': 0
            })
            .then(
                function (response) {
                    $scope.vendors = response;
                },
                function (errors) {
                    console.log(errors);
                }
            );
        };
        var getExpenseHeads = function(){
            Restangular.all("expense-heads").getList({
                'pagination': 0
            })
            .then(
                function (response) {
                    $scope.expenseHeads = response;
                },
                function (errors) {
                    console.log(errors);
                }
            );
        };
        var getUsers = function() {
            Restangular.all("user").getList({
                'pagination': 0
              })
              .then(
                function (response) {
                  $scope.loading = false;
                  $scope.userList = response;
                },
                function (errors) {
                  $scope.loading = false;
                  $scope.error_message = errors.data.message;
                }
              );
        };
        var getPaymentMethods = function(){
            Restangular.all("payment-method").getList({
                'pagination': 0
            })
            .then(
                function (response) {
                    $scope.paymentMethodList = response;
                },
                function (errors) {
                    console.log(errors);
                }
            );
        };
        var getPurchaseOrders = function(){
            Restangular.all("purchase-orders").getList({
                'pagination': 0
            })
            .then(
                function (response) {
                    $scope.purchaseOrderList = response;
                },
                function (errors) {
                    console.log(errors);
                }
            );
        };

        getVendors();
        getExpenseHeads();
        getUsers();
        getPaymentMethods();
        getPurchaseOrders();
        $scope.form = {
            loading: false,
            po : {
                is_available: null,//yes/no
                number: null,
                submitted: false,
                loaded: false,
                loading : false,
                isValid : false,
                data : {},
                change: function(){
                    $scope.form.po.data = {};
                    $scope.form.po.number = null;
                    $scope.form.po.submitted = false;
                    $scope.form.po.loaded = false;
                    $scope.form.po.isValid = false;
                }

            },
            model: {
                po_id: null,
                expense_head_id: null,
                amount: null,
                payment_status: null,
                payment_method_id: null,
                invoice_number: null,
                invoice_date: null,
                expense_by: null,
                payment_by: null,
                payment_reference_number: null,
                payment_date: null,
                other_info: null,
            },
            errors : {
                type: false,
                vendor_id: false,
                amount: false,
                error: false
            },
            select_vendor: function(data) {
                $scope.form.model.vendor_id = data.id;
                $scope.form.errors.vendor_id = false;
            },
            select_po: function(data) {
                $scope.form.model.po_id = data.id;
                $scope.form.errors.po_id = false;
            },
            select_expense_head: function(data) {
                $scope.form.model.expense_head_id = data.id;
            },
            select_payment_method: function(data) {
                $scope.form.model.payment_method_id = data.id;
            },
            select_payment_status: function(data) {
                $scope.form.model.payment_status = data.value;
            },
            select_user: function(data) {
                $scope.form.model.expense_by = data.id
            },
            select_payment_by: function(data) {
                $scope.form.model.payment_by = data.id
            },
            getPODetail : function() {
                $scope.form.po.submitted = true;
                console.log('getPODetail called',$scope.form.po.number, $scope.form.po.is_available);
                if($scope.form.po.number != null && $scope.form.po.is_available == 'yes') {
                    
                    Restangular.one('purchase-orders', $scope.form.po.number).get().then(function(response){
                        $scope.form.po.data = response;
                        $scope.form.po.isValid = true;
                        $scope.form.po.loaded = true;
                        console.log('RES', response, $scope.form.po.isValid);
                        
                    }, function(error){
                        $scope.form.po.isValid = false;
                        $scope.form.po.loaded = true;
                        console.log('error',error);
                    });    
                }
                
            },
            submit: function ($form) {
                $scope.form.submitted = true;
                
                if(!$form.$valid) {
                    return false;
                }
                // $scope.form.model.invoice_date = moment($scope.form.model.invoice_date).format("YYYY-MM-DD");
                // $scope.form.model.payment_date = moment($scope.form.model.payment_date).format("YYYY-MM-DD");
                $scope.form.loading = true;
                $scope.form.model.po_id = $scope.form.po.number;
                var params = {
                    data : $scope.form.model
                };
                
                Restangular.all('expense').post(params).then(
                    function (response) {
                        $scope.form.loading = false;
                        $scope.close();
                    },
                    function (errors) {
                        $scope.form.loading = false;
                        $scope.form.errors.error_message =  errors.data.message;
                        console.log(errors);
                    }
                );
            },
            cancel : function() {
                $uibModalInstance.dismiss();
            },
            close : function() {
                $uibModalInstance.close();
            },
        }
        // Get existing Services name
        $scope.close = function() {
            $uibModalInstance.close();
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss();
        };
    }
  ]);
  