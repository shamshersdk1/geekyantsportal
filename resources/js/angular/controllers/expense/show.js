angular.module('myApp')
    .controller('expenseShowCtrl',["$scope", "Restangular","$uibModal", "$rootScope", function ($scope, Restangular, $uibModal, $rootScope) {
        $scope.loaded = false;
        var id = window.expense.id;
        $scope.readOnly = window.read_only;
        $scope.expenseObj = Restangular.one("expense", id);
        // $scope.departments = Restangular.all("department").getList();
        // $scope.users =
        $scope.expenseObj.get().then(
            function(response) {
                $scope.loaded = true;
                $scope.expenseObj = response;
                console.log($scope.expenseObj);
            },
            function(error) {
                $scope.loaded = true;
                console.log('error',error);
            }
        );

        $scope.changeStatus = function() {
            var modal = $uibModal.open({
              controller: "changeStatusModalCtrl",
              windowClass: "bootstrap_wrap",
              templateUrl: "/views/expense/change-status-modal.html",
              backdrop: "static",
              size: "md",
              resolve: {
                data: function() {
                  return {id: id, payment_status: $scope.expenseObj.payment_status};
                }
              }
            });
            modal.result.then(
              function(result) {
                console.log(result);
                $scope.expenseObj.payment_status = result;
                $rootScope.$broadcast('activity-log');
              },
              function() {
                //cancel
              }
            );
        };
        
        $scope.approveExpense = function() {
            var modal = $uibModal.open({
                controller: "approveExpenseModalCtrl",
                windowClass: "bootstrap_wrap",
                templateUrl: "/views/expense/approve-modal.html",
                backdrop: "static",
                size: "md",
                resolve: {
                  data: function() {
                    return {id:id};
                  }
                }
              });
              modal.result.then(
                function() {
                  $rootScope.$broadcast('activity-log');
                  $scope.expenseObj.payment_status = 'approved';
                  // console.log(result);
                },
                function() {
                  //cancel
                }
              );
        };

    }]);