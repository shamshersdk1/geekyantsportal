angular.module("myApp").controller("changeStatusModalCtrl", [
  "$scope",
  "Restangular",
  "data",
  "$uibModalInstance",
  "$q",
  function($scope, Restangular, data, $uibModalInstance, $q) {
    console.log(data);
    $scope.payment_status = data.payment_status;
    var id = data.id;
    $scope.message = '';
    $scope.error = false;
    $scope.loading = false;
    $scope.status_list = [
      { value : "paid", name : "Paid" },
      { value : "hold", name : "Hold" },
      { value : "due_immediate_pending", name : "Due Immediate Pending" },
      { value : "due_next_cycle_pending", name : "Due Next Cycle Pending" },
      { value : "due_immediate_approved", name : "Due Immediate Approved" },
      { value : "due_next_cycle_approved", name : "Due Next Cycle Approved" }
  ];



    $scope.submit = function() {
      $scope.loading = true;
      var params = {
        'id': id,
        'status': $scope.payment_status
      };
      Restangular.all('expense-update-status').post(params).then(
          function(response) {
            $scope.loading = false;
            $uibModalInstance.close(response);
          },
          function(error) {
            console.log(error);
            $scope.message = error.data.message;
            $scope.error = true;
            $scope.loading = false;
          }
        );
    };
    $scope.cancel = function() {
      $uibModalInstance.dismiss();
    };
  }]);
