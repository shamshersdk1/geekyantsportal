angular.module("myApp").controller("approveExpenseModalCtrl", [
  "$scope",
  "Restangular",
  "data",
  "$uibModalInstance",
  function($scope, Restangular, data, $uibModalInstance) {
    var id = data.id;
    $scope.message = '';
    $scope.error = false;
    $scope.loading = false;
    $scope.submit = function() {
      $scope.loading = true;
      var params = {
        'id': id,
        'status': 'approved'
      };
      Restangular.all('expense-update-status').post(params).then(
          function(response) {
            $scope.loading = false;
            $uibModalInstance.close(response);
          },
          function(error) {
            console.log(error);
            $scope.message = error.data.message;
            $scope.error = true;
            $scope.loading = false;
          }
        );
    };
    $scope.cancel = function() {
      $uibModalInstance.dismiss();
    };
  }]);
