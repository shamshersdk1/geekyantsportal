angular.module('myApp')
    .controller('expenseIndexCtrl',["$scope", "Restangular", "$uibModal","DTOptionsBuilder",
  "Datatable", function ($scope, Restangular, $uibModal, DTOptionsBuilder,
  Datatable) {

    
    $scope.filter = {};
    $scope.expense = {
      data : []
    }
    var params = {pagination : 0};
    Restangular.all('expense').getList(params).then(function(response){
      $scope.expense.data = response;
      $scope.originalData = response;
    }, function(error){

    });

    $("#payment_status").select2();
    //$("#name_filter").select2();

    $("#payment_status").change(function (event) {
      $scope.$apply(function () {
        var selectedOptions = event.val;
        $scope.expense.data = Datatable.filterData($scope.filter, $scope.originalData, selectedOptions, "payment_status");
      });
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions()
      .withOption('order', [[0, 'DESC']])
      .withOption('scrollY', '300px')
      .withOption('scrollX', '100%')
      .withOption('scrollCollapse', true)
      .withOption('pageLength', 1000)
      .withOption('lengthMenu', [50, 100, 150, 200, 500, 1000])
    
      $scope.addExpense = function() {
            var modal = $uibModal.open({
              controller: "expenseAddModalCtrl",
              windowClass: "bootstrap_wrap",
              templateUrl: "/views/expense/add-edit-modal.html",
              backdrop: "static",
              size: "lg",
              resolve: {
                data: function() {
                  return { id: 1 };
                }
              }
            });
            modal.result.then(
              function(result) {
                window.location.reload();
              },
              function() {
                //cancel
              }
            );
          };
          
          $scope.viewDetail = function(id) {
            console.log('I am here',id);
            var path = "/admin/expense/" + id;
            window.location.href = path;
          };

          $scope.editDetail = function(id) {
            var modal = $uibModal.open({
              controller: "expenseEditModalCtrl",
              windowClass: "bootstrap_wrap",
              templateUrl: "/views/expense/add-edit-modal.html",
              backdrop: "static",
              size: "lg",
              resolve: {
                data: function() {
                  return { id: id };
                }
              }
            });
            modal.result.then(
              function(result) {
                window.location.reload();
              },
              function() {
                //cancel
              }
            );
          };
          $scope.deleteDetails = function(id) {
            if (confirm("Are you sure you want to delete this vendor?")) {
              Restangular.one("vendor", id)
                .remove()
                .then(
                  function(response) {
                    window.location.reload();
                  },
                  function(error) {
                    alert(error.data.message);
                  }
                );
            }
          }

    }]);