angular.module("myApp").controller("editCmsCtrl", [
  "$scope",
  "Restangular",
  function($scope, Restangular) {
    $scope.allActiveUserList = window.allActiveUserList;

    $scope.select_user = function(data) {
      $scope.user = data;
      $scope.user_error = false;
    };
    $scope.deselect_user = function(data, item) {
      $scope.user = data;
      $scope.user_error = false;
    };

    // var params = {

    //   user: $scope.user,

    // };
    Restangular.all("project/edit")
      .post(params)
      .then(
        function(response) {
          var path = "/admin/project/" + $scope.project.id + "/dashboard";
          window.location.href = path;
        },
        function(errors) {
          console.log(errors);
        }
      );
  }
]);
