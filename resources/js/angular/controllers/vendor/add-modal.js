angular.module("myApp").controller("addVendorModalCtrl", [
    "$scope",
    "Restangular",
    "data",
    "$uibModalInstance",
    function($scope, Restangular, data, $uibModalInstance) {
        $scope.existingServices = [];
        var getServices = function(){
            Restangular.all("vendor-services").getList({})
            .then(
                function (response) {
                    for( i=0; i < response.length ; i++ )
                    {
                        $scope.existingServices.push(response[i].name);
                    }
                },
                function (errors) {
                    console.log(errors);
                }
            );
        };
        getServices();
        $scope.form = {
            loading: false,
            model: {
                type: 'corporate',
                name: null,
                address: null,
                pan: null,
                gst: null,
                landline_number: null,
                vendor_contacts : [ 
                    {name : null, mobile : null, email : null,designation : null, is_primary_contact: false}
                ],
                vendor_services : [],
                new_vendor_contact: {
                    name : null,
                    mobile : null,
                    email : null,
                    designation : null,
                    is_primary_contact: false
                }
            },
            errors : {
                error: false,
                type: false,
                name: false,
                vendor_contacts: false,
                vendor_services: false,
                error_message: null
            },
            addContact: function () {
                $scope.form.model.vendor_contacts.push($scope.form.model.new_vendor_contact);
                $scope.form.model.new_vendor_contact = {
                    name : null,
                    mobile : null,
                    email : null,
                    designation : null,
                    is_primary_contact: false
                }
            },
            remove: function(index) {
                $scope.form.model.vendor_contacts.splice(index, 1);
            },
            resetOtherPrimary: function(index) {
                for ( i = 0; i < $scope.form.model.vendor_contacts.length; i++ )
                {
                    if ( i != index )
                    {
                        $scope.form.model.vendor_contacts[i].is_primary_contact = false;
                    }
                }
            },
            submit: function () {
                $scope.form.errors.error = false;
                if ( $scope.form.model.type == null )
                {
                    $scope.form.errors.type = true;
                    $scope.form.errors.error = true;
                }
                if ( $scope.form.model.name == null )
                {
                    $scope.form.errors.name = true;
                    $scope.form.errors.error = true;
                }
                if ( $scope.form.model.vendor_contacts[0].name == null || $scope.form.model.vendor_contacts[0].mobile == null )
                {
                    $scope.form.errors.vendor_contacts = true;
                    $scope.form.errors.error = true;
                }
                if ( $scope.form.model.vendor_services.length < 1 )
                {
                    $scope.form.errors.vendor_services = true;
                    $scope.form.errors.error = true;
                }
                if ( $scope.form.errors.error  )
                {
                    return;
                }
                $scope.form.loading = true;
                var params = {
                    data : $scope.form.model
                };
                Restangular.all('vendor').post(params).then(
                    function (response) {
                        $scope.form.loading = false;
                        $scope.close();
                    },
                    function (errors) {
                        $scope.form.loading = false;
                        $scope.form.errors.error_message =  errors.data.message;
                        console.log(errors);
                    }
                );
            },
        }
        // Get existing Services name
        $scope.close = function() {
            $uibModalInstance.close();
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss();
        };
    }
  ]);
  