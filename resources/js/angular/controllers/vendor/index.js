angular.module('myApp')
    .controller('vendorIndexCtrl',["$scope", "Restangular", "$uibModal" , function ($scope, Restangular, $uibModal) {
        $scope.gridViewModel = "vendor?per_page=500";
        $scope.gridViewColumns = [
          {
            title: "Id",
            value: "%%row.id%%",
            width: 5
        },
        {
          title: "Name",
          value: "%% row.name %%",
          width: 8
        },
        {
            title: "Type",
            value: "%%row.type%%",
            value: "<span ng-class=\"{'label label-primary   custom-label' : row.type=='coporate', 'label label-danger' : row.type=='individual', 'label label-danger custom-label' : (row.status=='cancelled' || row.status=='rejected')  }\"> %% row.type | uppercase %%</span> ",
            width: 8

        },
        {
            title: "Address",
            value: "<span style='white-space: pre-wrap;'>%% row.address %%</span>",
            width: 20
        },
        {
            title: "Pan",
            value: "%% row.pan %%",
            width: 7
        },
        {
            title: "GST",
            value: "%% row.gst %%",
            width: 8
        },
        {
          title: "Services",
          value: "%% row.services_name %%",
          width: 14
      },
        {
          title: "Created At",
          value: "%% row.created_at | dateToISO %%",
          width: 15
        }
        ];
        $scope.gridViewActions = [
            {
              type: "view",
              action: "veiwDetail(row.id)"
            },
            {
              type: "edit",
              action: "editDetail(row.id)"
            },
            {
              type: "delete",
              action: "deleteDetails(row.id)"
            }
          ];
        $scope.addVendor = function() {
            var modal = $uibModal.open({
              controller: "addVendorModalCtrl",
              windowClass: "bootstrap_wrap",
              templateUrl: "/views/vendor/add-modal.html",
              backdrop: "static",
              size: "lg",
              resolve: {
                data: function() {
                  return { id: 1 };
                }
              }
            });
            modal.result.then(
              function(result) {
                window.location.reload();
              },
              function() {
                //cancel
              }
            );
          };
          
          $scope.veiwDetail = function(id) {
            var modal = $uibModal.open({
              controller: "showVendorModalCtrl",
              windowClass: "bootstrap_wrap",
              templateUrl: "/views/vendor/show-modal.html",
              backdrop: "static",
              size: "lg",
              resolve: {
                data: function() {
                  return { id: id };
                }
              }
            });
            modal.result.then(
              function(result) {
              },
              function() {
                //cancel
              }
            );
          };

          $scope.editDetail = function(id) {
            var modal = $uibModal.open({
              controller: "editVendorModalCtrl",
              windowClass: "bootstrap_wrap",
              templateUrl: "/views/vendor/add-modal.html",
              backdrop: "static",
              size: "lg",
              resolve: {
                data: function() {
                  return { id: id };
                }
              }
            });
            modal.result.then(
              function(result) {
                window.location.reload();
              },
              function() {
                //cancel
              }
            );
          };
          $scope.deleteDetails = function(id) {
            if (confirm("Are you sure you want to delete this vendor?")) {
              Restangular.one("vendor", id)
                .remove()
                .then(
                  function(response) {
                    window.location.reload();
                  },
                  function(error) {
                    alert(error.data.message);
                  }
                );
            }
          }

    }]);