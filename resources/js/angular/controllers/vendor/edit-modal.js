angular.module("myApp").controller("editVendorModalCtrl", [
    "$scope",
    "Restangular",
    "data",
    "$uibModalInstance",
    function($scope, Restangular, data, $uibModalInstance) {
        $scope.existingServices = [];
        var id = data.id;
        var vendorObj = Restangular.one("vendor", id);
        var getServices = function(){
            Restangular.all("vendor-services").getList({})
            .then(
                function (response) {
                    for( i=0; i < response.length ; i++ )
                    {
                        $scope.existingServices.push(response[i].name);
                    }
                },
                function (errors) {
                    console.log(errors);
                }
            );
        };
        getServices();
        $scope.form = {
            loading: false,
            model: {
                id: null,
                type: null,
                name: null,
                address: null,
                pan: null,
                gst: null,
                landline_number: null,
                rating: null,
                created_at: null,
                vendor_contacts : [],
                vendor_services : [],
                new_vendor_contact: {
                    name : null,
                    mobile : null,
                    email : null,
                    designation : null,
                    is_primary_contact: false
                }
            },
            errors : {
                type: false,
                name: false,
                error_message: null
            },
            addContact: function () {
                $scope.form.model.vendor_contacts.push($scope.form.model.new_vendor_contact);
                $scope.form.model.new_vendor_contact = {
                    name : null,
                    mobile : null,
                    email : null,
                    designation : null,
                    is_primary_contact: 'false'
                }
            },
            remove: function(index) {
                $scope.form.model.vendor_contacts.splice(index, 1);
            },
            resetOtherPrimary: function(index) {
                for ( i = 0; i < $scope.form.model.vendor_contacts.length; i++ )
                {
                    if ( i != index )
                    {
                        $scope.form.model.vendor_contacts[i].is_primary_contact = false;
                    }
                }
            },
            saveRating: function (param) {
                $scope.form.model.rating = param;
            },
            submit: function () {
                if ( $scope.form.model.type == null )
                {
                    $scope.form.errors.type = true;
                }
                if ( $scope.form.model.name == null )
                {
                    $scope.form.errors.name = true;
                }
                $scope.form.loading = true;
                vendorObj.customPUT({
                    data: $scope.form.model
                }).then(
                    function(response) {
                        $scope.form.loading = false;
                        $scope.close();
                    },
                    function(error) {
                        $scope.form.loading = false;
                        $scope.form.errors.error_message =  error.data.message;
                        console.log("err", error);
                    }
                  );
            },
        }
        
        vendorObj.get().then(
            function(response) {
              for( i = 0; i < response.vendor_services.length ; i++ )
              {
                $scope.form.model.vendor_services.push(response.vendor_services[i].vendor_services_name.name);
              }
              $scope.form.model.id = response.id;
              $scope.form.model.type = response.type;
              $scope.form.model.name = response.name;
              $scope.form.model.address = response.address;
              $scope.form.model.pan = response.pan;
              $scope.form.model.gst = response.gst;
              $scope.form.model.landline_number = response.landline_number;
              $scope.form.model.rating = response.rating;
              $scope.form.model.vendor_contacts = response.vendor_contacts;
              $scope.form.model.created_at = response.created_at;
              for( i = 0; i < response.vendor_contacts.length ; i++ )
              {
                 $scope.form.model.vendor_contacts[i].is_primary_contact == 1 
                 ?  $scope.form.model.vendor_contacts[i].is_primary_contact = true
                 : $scope.form.model.vendor_contacts[i].is_primary_contact = false;
              }
            },
            function(error) {
                $scope.form.errors.error_message =  error.data.message;
              console.log("err", error);
            }
          );

        $scope.filteredDate = function (input_date) {
            var formatted_date = new Date(input_date);
            return formatted_date;
        };
        
        // Get existing Services name
        $scope.close = function() {
            $uibModalInstance.close();
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss();
        };
    }
  ]);
  