angular.module("myApp").controller("adminModalCtrl", [
  "$scope",
  "Restangular",
  "data",
  "$uibInstance",
  function($scope, Restangular, data, $uibModalInstance) {
    $scope.adminError = false;
    $scope.errorMessage = "All Field is mandatory!";
    $scope.items = data;
    if ($scope.items.data.manager_comments) {
      $scope.transformedComments = $scope.items.data.manager_comments
        .split(" ")
        .join("\n");
    }
    $scope.approveChanged = function() {
      $scope.adminError = false;
    };
    $scope.saveApproveData = function(data) {
      $scope.finalsubmit = true;
      if (!$scope.transformedComments || !data.approved_hours) {
        $scope.adminError = true;
        $scope.finalsubmit = false;
      }
      if ($scope.adminError) return true;
      var params = {
        timesheet_id: data.timesheet_id,
        approved_hours: data.approved_hours,
        manager_comments: $scope.transformedComments
      };

      Restangular.all("admin/timesheet")
        .post(params)
        .then(
          function(response) {
            $scope.finalsubmit = false;
            if (response.status) {
              $uibModalInstance.close(data);
              window.location.reload();
            }
          },
          function(errors) {
            console.log("checkout");
            console.log(errors.data.message);
            $scope.finalsubmit = false;
            $scope.adminError = true;
            // $scope.errorMessage = errors.data.message.message;
          }
        );
    };
    $scope.cancel = function() {
      $uibModalInstance.dismiss();
    };
  }
]);
