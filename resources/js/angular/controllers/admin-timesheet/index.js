angular.module('myApp')
    .controller("admintimesheetCtrl", ["$scope", "Restangular", "$uibModal", function ($scope, Restangular, $uibModal) {
        $scope.dates = JSON.parse(window.dates);
        $scope.projects = window.projects;
        $scope.verticalTotals = JSON.parse(window.verticalTotals);
        $scope.user = JSON.parse(window.user);
        $scope.currentDate = new Date().toISOString().slice(0, 10);

        $scope.addTime = function (name, result) {
            $scope.list = {
                project_name: name,
                data: result
            };

            var modal = $uibModal.open({
                controller: "modalCtrl",
                windowClass: "bootstrap_wrap",
                templateUrl: "/views/add_time.html",
                backdrop: "static",
                size: "md",
                resolve: {
                    data: function () {
                        return $scope.list;
                    }
                }
            });
            modal.result.then(function (res) { }, function () { });
        };
        $scope.addTimeAprrover = function (name, result, user) {
            $scope.list = {
                project_name: name,
                approved_hours: result.approved_hours,
                data: result,
                user_name: user
            };
            var modal = $uibModal.open({
                controller: "adminModalCtrl",
                windowClass: "bootstrap_wrap",
                templateUrl: "/views/approve_time.html",
                backdrop: "static",
                size: "md",
                resolve: {
                    data: function () {
                        return $scope.list;
                    }
                }
            });
            modal.result.then(function (res) { }, function () { });
        };
        $scope.updateApproveList = function (data) {
            var params = {
                timesheet_id: data.timesheet_id,
                approved_hours: data.approved_hours
            };
            Restangular.all("admin/timesheet")
                .post(params)
                .then(function (response) { }, function (error) { });
        };
    }]);