angular.module('myApp')
    .controller('projectDetailCtrl',["$scope", "Restangular", function ($scope, Restangular) {
        $scope.dates = JSON.parse(window.dates);
        $scope.projects = window.projects[0];
        $scope.user = JSON.parse(window.user);
        $scope.currentDate = new Date().toISOString().slice(0, 10);
        $scope.existingItems = [];
        $scope.loading = [];
        for (var i = 0; i < ($scope.projects.data).length; i++) {
            $scope.existingItems[i] = $scope.projects.data[i].timesheet_details ? $scope.projects.data[i].timesheet_details : [];
            $scope.loading[$scope.projects.data[i].date] = [];
            $scope.loading[i] = false;
        }
        for (var i = 0; i < ($scope.existingItems).length; i++) {
            if (($scope.existingItems[i]).length == 0) {
                $scope.existingItems[i].push({ duration: null, reason: null });
            }
        }
        $scope.addDateRange = function (parent_index, index) {
            $scope.existingItems[parent_index].push({ duration: null, reason: null });
            $scope.form.duration = null;
            $scope.form.reason = null;
        };

        $scope.finalsubmit = false;
        $scope.form = {
            duration: null,
            reason: null
        };

        $scope.changed = function (parentIndex, index, detail) {
            $scope.loading[detail.date][index] = true;
            var params = {
                date: detail.date,
                project_id: detail.project_id,
                timesheet_details: $scope.existingItems[parentIndex]
            };

            Restangular.all('timesheet').post(params).then(

                function (response) {
                    $scope.loading[detail.date][index] = false;
                    $scope.finalsubmit = false;
                    if (response.result.success) {
                    }
                },
                function (errors) {
                    $scope.loading[detail.date][index] = false;
                    $scope.finalsubmit = false;
                    $scope.error = true;
                     $scope.errorMessage = errors.message.message;
                    //console.log(errors);
                }
            );
        };

        $scope.removeDateRange = function (parentIndex, index, detail) {
            $scope.existingItems[parentIndex].splice(index, 1);
            var params = {
                date: detail.date,
                project_id: detail.project_id,
                timesheet_details: $scope.existingItems[parentIndex]
            };
            Restangular.all('timesheet').post(params).then(
                function (response) {
                    $scope.finalsubmit = false;
                    if (response.result.success) {
                    }
                },
                function (errors) {
                    $scope.finalsubmit = false;
                    $scope.error = true;
                    $scope.errorMessage = errors.message.message;
                    //console.log(errors);
                }
            );
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss();
            window.location.reload();
        };
    }]);