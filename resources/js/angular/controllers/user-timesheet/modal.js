angular.module('myApp')
    .controller('modalCtrl',["$scope", "Restangular", "data", "$uibModalInstance",function ($scope, Restangular, data, $uibModalInstance) {
        $scope.error = false;
        $scope.errorMessage = 'Please enter valid input!';
        $scope.items = data;
        data.data.timesheet_details = data.data.timesheet_details ? data.data.timesheet_details : [];
        $scope.existingItems = data.data.timesheet_details;
        if (data.data.timesheet_details.length == 0) {
            $scope.existingItems.push({ duration: null, reason: null });
        }
        $scope.finalsubmit = false;
        $scope.form = {
            duration: null,
            reason: null
        };
        $scope.addDateRange = function (data) {
            if (!data) {
                $scope.existingItems.push({ duration: null, reason: null });
                $scope.form.duration = null;
                $scope.form.reason = null;
            }
        };
        $scope.changed = function () {
            $scope.error = false;
        };
        $scope.removeDateRange = function (index, data) {
            if (!data) {
                $scope.error = false;
                $scope.existingItems.splice(index, 1);
            }
        };
        $scope.saveData = function (data) {
            $scope.existingItems.forEach(function (item) {
                if (item.duration === null || item.duration === undefined || item.reason === null) {
                    $scope.error = true;
                    item.error = true;
                }
            });
            if ($scope.error) return true;
            $scope.finalsubmit = true;
            var params = {
                date: data.date,
                project_id: data.project_id,
                timesheet_details: $scope.existingItems
            };
            Restangular.all('timesheet').post(params).then(
                function (response) {
                    $scope.finalsubmit = false;
                    if (response.status) {
                        $uibModalInstance.close(data);
                        window.location.reload();
                    }
                },
                function (errors) {
                    $scope.finalsubmit = false;
                    $scope.error = true;
                    $scope.errorMessage = errors.data.message.message;
                }
            );
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss();
            window.location.reload();
        };
    }]);