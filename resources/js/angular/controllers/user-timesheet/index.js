angular.module('myApp')
    .controller('timesheetCtrl',["$scope", "Restangular", "$uibModal",function ($scope, Restangular, $uibModal) {
        $scope.dates = JSON.parse(window.dates);
        $scope.projects = window.projects;
        $scope.verticalTotals = JSON.parse(window.verticalTotals);
        $scope.user = JSON.parse(window.user);
        $scope.currentDate = new Date().toISOString().slice(0, 10);
        $scope.addTime = function (name, result, user) {
            $scope.list = {
                project_name: name,
                data: result,
                user_name: user
            };
            var modal = $uibModal.open({
                controller: 'modalCtrl',
                windowClass: 'bootstrap_wrap',
                templateUrl: '/views/add_time.html',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                    data: function () {
                        return $scope.list;
                    }
                }

            });
            modal.result.then(function (res) { }, function () { });
        };
        var modal = $uibModal.open({
            controller: "modalCtrl",
            windowClass: "bootstrap_wrap",
            templateUrl: "/views/add_time.html",
            backdrop: "static",
            size: "md",
            resolve: {
                data: function () {
                    return $scope.list;
                }
            }
        });
        modal.result.then(function (res) { }, function () { });


    }]);