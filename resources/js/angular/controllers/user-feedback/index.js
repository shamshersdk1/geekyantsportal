angular.module('myApp')
    .controller('userFeedbackCtrl',["$scope", "Restangular", "$uibModal",function ($scope, Restangular, $uibModal) {
        $scope.feedbackMonthDetails = JSON.parse(window.feedbackMonthDetails);
        $scope.user = JSON.parse(window.user);
        $scope.selected_month = null;
        $scope.review_details = [];
        
        $scope.loadReviews = function(month) {
            $scope.selected_month = month;
            Restangular.all("feedback-reviews-monthly").getList({
                'month_id': month.id
            })
            .then(
                function (response) {
                    $scope.review_details = response;
                    console.log($scope.review_details);
                },
                function (errors) {
                    
                }
            );
        };
    }]);