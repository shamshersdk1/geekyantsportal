angular.module("myApp").controller("payslipStep1Ctrl", [
    "$scope",
    "Restangular",
    "Payslip",
    "$log",
    "DTOptionsBuilder", "DTColumnDefBuilder",

    function($scope, Restangular, Payslip, $log, DTOptionsBuilder, DTColumnDefBuilder) {
        //function WithFixedColumnsCtrl(DTOptionsBuilder) {
        var vm = this;
        $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withOption('scrollY', '300px')
            .withOption('scrollX', '100%')
            .withOption('scrollCollapse', true)
            .withOption('paging', false)
            .withFixedColumns({
                leftColumns: 3,
                rightColumns: 0
            });
        //}

        $scope.step = 1;
        $scope.payslip = {
            data: [],
            headers: [],
            editable: [],
            loading: true,
            loaded: false,
            nextStep: function() {
                $scope.step = 2;
                Payslip.gotoRoute($scope.step);
            },
        };
        $scope.info = {
            step: 1,
            loading: false
        };

        $scope.var = {
            test: "works!",
            error: false,
            errorMessage: "",
            message: false,
            messageText: ""
        };
        Payslip.data.loaded = false;
        Payslip.init();
        $scope.PayslipData = Payslip.data;
        $scope.$watch("PayslipData.loaded", function() {


            if ($scope.PayslipData.loaded == true) {
                $scope.payslip.loading = false;
                $scope.payslip.headers = $scope.PayslipData.payload.headers;
                $scope.payslip.data = $scope.PayslipData.payload.list;
                $scope.payslip.editable = $scope.PayslipData.payload.editable;
                $scope.statusInit();
                $scope.payslip.loaded = true;
            }
        });
        $scope.errorDismiss = function() {
            $scope.var.error = false;
            $scope.var.errorMessage = "";
        };
        $scope.messageDismiss = function() {
            $scope.var.message = false;
            $scope.var.messageText = "";
        };
        $scope.statusInit = function() {
            Payslip.data.loading = true;
            Payslip.data.loaded = false;
            $scope.showError = [];
            $row = Object.keys($scope.payslip.data).length;
            $col = $scope.payslip.headers.length;
            for (var i = 1; i <= $row; i++) {
                $scope.showError[i] = [];
                for (var j = 0; j < $col; j++) {
                    $scope.showError[i][j] = { code: "", text: "" };
                }
            }
            Payslip.data.loading = false;
            Payslip.data.loaded = true;
        };
        $scope.update = function($employee_code, $index, $sl_no) {
            var params = {
                employee_code: $employee_code,
                index: $scope.payslip.headers[$index],
                value: $scope.payslip.data[$sl_no][$index],
            };
            var result = Payslip.updateRow(params);
            if ((result.code = "message")) {
                $scope.showError[$sl_no][$index].code = "message";
                $scope.showError[$sl_no][$index].text = result.text;
            } else {
                $scope.showError[$sl_no][$index].code = "error";
                $scope.showError[$sl_no][$index].text = result.text;
            }
        };
        $scope.nextStep = function() {
            $scope.step = 2;
            Payslip.gotoRoute($scope.step);
        };
    }
]);