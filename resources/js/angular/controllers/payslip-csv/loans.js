angular.module("myApp").controller("payslipcsvStep2Ctrl", [
  "$scope",
  "Restangular",
  "Payslip",
  function($scope, Restangular, Payslip) {
    $scope.info = {
      step: 2,
      loading: false
    };
    $scope.var = {
      test: "works!",
      error: false,
      errorMessage: "",
      message: false,
      messageText: ""
    };
    $scope.PayslipData = Payslip.data;
    $scope.$watch("PayslipData.loaded", function() {
      if ($scope.PayslipData.loaded == true) {
        $scope.headers = $scope.PayslipData.payload.headers;
        $scope.data = $scope.PayslipData.payload.data;
        $scope.editable = $scope.PayslipData.payload.editable;
        $scope.loans = $scope.PayslipData.payload.loans;
        $scope.appraisals = $scope.PayslipData.payload.appraisals;
        $scope.render();
      }
    });
    $scope.$watch("PayslipData.loading", function() {
      if ($scope.PayslipData.loading == true) {
        $scope.info.loading = true;
      } else {
        $scope.info.loading = false;
      }
    });
    $scope.errorDismiss = function() {
      $scope.var.error = false;
      $scope.var.errorMessage = "";
    };
    $scope.messageDismiss = function() {
      $scope.var.message = false;
      $scope.var.messageText = "";
    };
    $scope.confirmEmi = function($id, $amount) {
      var params = {
        emi_id: $id,
        amount: $amount,
      };
      // console.log(params);
      var response = Payslip.confirmEmi(params);
      if(response) {
        //show success message and disable input
      } else {
        //show error
      }
    };
  }
]);
