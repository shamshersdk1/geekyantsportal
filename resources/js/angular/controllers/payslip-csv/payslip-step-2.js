angular.module("myApp").controller("payslipStep2Ctrl", [
    "$scope",
    "Restangular",
    "Payslip",
    "$log",
    function($scope, Restangular, Payslip, $log) {
        $scope.step = 2;
        $scope.loans = {
            list: [],
            status: [],
            error: [],
            incomplete: [],
            loaded: false,
            empty: false,
            nextStep: function () {
                $scope.step = 3;
                Payslip.gotoRoute($scope.step);
            },
            prevStep: function () {
                $scope.step = 1;
                Payslip.gotoRoute($scope.step);
            },
            update: function($employee_code, $value, $index) {
                $value = $value == "0" ? "0.00" : $value;
                var params = {
                    employee_code: $employee_code,
                    index: "LOAN",
                    value: $value,
                };
                var result = Payslip.updateRow(params);
                if ((result.code = "message")) {
                    $scope.loans.error[$index] = "success";
                } else {
                    $scope.loans.error[$index] = "error";
                }
            },
            init: function() {
                var params = {
                    id: Payslip.data.id,
                };
                Restangular.one("loan")
                .get(params)
                .then(
                    function(response) {
                        console.log('loan ', response.data);
                        $scope.loans.list = response.data;
                        $scope.statusInit();
                    },
                    function(errors) {
                    }
                );
            },
        };
        $scope.loans.init();
        $scope.statusInit = function() {
            $row = Object.keys($scope.loans.list).length;
            if($row > 0) {
                for (var i = 0; i < $row; i++) {
                    if($scope.loans.list[i].status == "paid") {
                        $scope.loans.error[i] = 'success';
                    } else {
                        $scope.loans.error[i] = null;
                    }
                    $scope.loans.incomplete[i] = false;
                }
            } else {
                $scope.loans.empty = true;
            }
        };
        $scope.nextStep = function ()
        {
            $row = Object.keys($scope.loans.list).length;
            $flag=0;
            if ($row > 0) {
                for (var i = 0; i < $row; i++) {
                    $scope.loans.incomplete[i] = false;
                    if ($scope.loans.error[i] != "success") {
                        if($flag==0) {
                            alert("Approve/Reject all loans before moving on");
                        }
                        $flag=1;
                        $scope.loans.incomplete[i] = true;
                    }
                }
            }
            if($flag == 0) {
                $scope.step = 3;
                Payslip.gotoRoute($scope.step);
            }
        };
        $scope.prevStep = function ()
        {
            $scope.step = 1;
            Payslip.gotoRoute($scope.step);
        }
    }
]);
