angular.module("myApp").controller("payslipStep5Ctrl", [
    "$scope",
    "Restangular",
    "Payslip",
    "$log",
    "DTOptionsBuilder",
    "DTColumnDefBuilder",
    function ($scope, Restangular, Payslip, $log, DTOptionsBuilder, DTColumnDefBuilder) {
        $scope.step = 5;
        $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withOption('scrollY', '300px')
            .withOption('scrollX', '100%')
            .withOption('scrollCollapse', true)
            .withOption('paging', false)
            .withFixedColumns({
                leftColumns: 3,
                rightColumns: 0
            });
        $scope.payslip = {
            data: [],
            headers: [],
            editable: [],
            loading: true,
            loaded: false,
            isConfirmReady: false,
            nextStep: function () {
                $scope.step = 5;
                Payslip.gotoRoute($scope.step);
            },
            prevStep: function () {
                $scope.step = 3;
                Payslip.gotoRoute($scope.step);
            },
        };
        Payslip.data.loaded = false;
        Payslip.init();
        $scope.PayslipData = Payslip.data;
        $scope.$watch("PayslipData.loaded", function () {
            if ($scope.PayslipData.loaded == true) {
                $scope.payslip.loading = false;
                $scope.payslip.headers = $scope.PayslipData.payload.headers;
                $scope.payslip.data = $scope.PayslipData.payload.list;
                $scope.payslip.editable = $scope.PayslipData.payload.editable.fill(false, 0, Object.keys($scope.payslip.headers).length);
                $scope.payslip.loaded = true;
                $scope.payslip.isConfirmReady = true;
            }
        });
        $scope.nextStep = function () {
            if (confirm("This is irreversible, are you sure?")) {
                $scope.step = 6;
                Payslip.gotoRoute($scope.step);
            }
        };
        $scope.prevStep = function () {
            $scope.step = 4;
            Payslip.gotoRoute($scope.step);
        };
    }
]);
