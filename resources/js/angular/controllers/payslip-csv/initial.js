angular.module("myApp").controller("payslipcsvStep1Ctrl", [
  "$scope",
  "Restangular",
  "Payslip",
  function ($scope, Restangular, Payslip) {

    $scope.step = 1;

    $scope.info = {
      step: 1,
      loading: false
    };
    $scope.var = {
      test: "works!",
      error: false,
      errorMessage: "",
      message: false,
      messageText: ""
    };
    $scope.PayslipData = Payslip.data;
    $scope.$watch("PayslipData.loaded", function () {
      if ($scope.PayslipData.loaded == true) {
        $scope.headers = $scope.PayslipData.payload.headers;
        $scope.data = $scope.PayslipData.payload.data;
        $scope.editable = $scope.PayslipData.payload.editable;
        $scope.loans = $scope.PayslipData.payload.loans;
        $scope.appraisals = $scope.PayslipData.payload.appraisals;
        $scope.render();
      }
    });
    $scope.$watch("PayslipData.loading", function () {
      if ($scope.PayslipData.loading == true) {
        $scope.info.loading = true;
      } else {
        $scope.info.loading = false;
      }
    });
    $scope.errorDismiss = function () {
      $scope.var.error = false;
      $scope.var.errorMessage = "";
    };
    $scope.messageDismiss = function () {
      $scope.var.message = false;
      $scope.var.messageText = "";
    };
    $scope.render = function () {
      Payslip.data.loading = true;
      Payslip.data.loaded = false;
      $scope.showError = [];
      $row = Object.keys($scope.data).length;
      $col = $scope.headers.length;
      for (var i = 1; i <= $row; i++) {
        $scope.showError[i] = [];
        for (var j = 0; j < $col; j++) {
          $scope.showError[i][j] = { code: "", text: "" };
        }
      }
      Payslip.data.loading = false;
      Payslip.data.loaded = true;
    };
    $scope.update = function ($sl_no, $index) {
      var params = {
        sl_no: $sl_no,
        index: $scope.headers[$index],
        value: $scope.data[$sl_no][$index],
      };
      var result = Payslip.updateRow(params);
      if ((result.code = "message")) {
        $scope.showError[$sl_no][$index].code = "message";
        $scope.showError[$sl_no][$index].text = result.text;
      } else {
        $scope.showError[$sl_no][$index].code = "error";
        $scope.showError[$sl_no][$index].text = result.text;
      }
    };
    //     Restangular.all("payslip-csv/"+$id+"/update")
    //       .customPUT(params)
    //       .then(function(response) {
    //           $scope.showError[$sl_no][$index].code = "message";
    //           $scope.showError[$sl_no][$index].text = response.result;
    //         }, function(errors) {
    //             $scope.showError[$sl_no][$index].code = "error";
    //             $scope.showError[$sl_no][$index].text = errors.data.message;
    //         }
    //       );
    // };
    // $scope.nextStep = function() {
    //   Payslip.data.loaded = false;
    //   if ($scope.info.step < 3) {
    //     $scope.info.step++;
    //   }
    //   Payslip.data.loaded = true;
    // };
    // $scope.prevStep = function() {
    //   Payslip.data.loaded = false;
    //   if ($scope.info.step > 1) {
    //     $scope.info.step--;
    //   }
    //   Payslip.data.loaded = true;
    // };
    // $scope.gotoStep = function(step) {
    //   Payslip.data.loaded = false;
    //   $scope.info.step = step;
    //   Payslip.data.loaded = true;
    // };
  }
]);
