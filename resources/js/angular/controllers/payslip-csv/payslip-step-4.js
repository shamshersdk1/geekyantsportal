angular.module("myApp").controller("payslipStep4Ctrl", [
    "$scope",
    "Restangular",
    "Payslip",
    "$log",
    function($scope, Restangular, Payslip, $log) {
        $scope.step = 4;
        $scope.appraisals = {
            list: [],
            loading: false,
            loaded: false,
            empty: false,
            init: function() {
                var params = {
                    id: Payslip.data.id
                }
                Restangular.one("appraisal")
                .get(params)
                .then(
                    function(response) {
                        $scope.appraisals.list = response.data;
                        $count = Object.keys($scope.appraisals.list).length;
                        if($count <= 0) {
                            $scope.appraisals.empty = true;
                        }
                    },
                    function(errors) {
                        $log.log(errors);
                    }
                );
            },
        };
        $scope.appraisals.init();
        $scope.nextStep = function()
        {
          $scope.step = 5;
          Payslip.gotoRoute($scope.step);
        };
        $scope.prevStep = function()
        {
          $scope.step = 3;
          Payslip.gotoRoute($scope.step);
        };
    }
]);
