angular.module("myApp").controller("payslipStep6Ctrl", [
    "$scope",
    "Restangular",
    "Payslip",
    "$log",
    "DTOptionsBuilder",
    "DTColumnDefBuilder",
    function ($scope, Restangular, Payslip, $log, DTOptionsBuilder, DTColumnDefBuilder) {
        $scope.step = 6;
        $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withOption('scrollY', '300px')
            .withOption('scrollX', '100%')
            .withOption('scrollCollapse', true)
            .withOption('paging', false)
            .withFixedColumns({
                leftColumns: 3,
                rightColumns: 0
            });
        $scope.payslip = {
            data: [],
            headers: [],
            editable: [],
            loading: true,
            loaded: false,
            csv_url: null,
            downloaded: false,
            isDownloadReady: false,
        };
        Payslip.data.loaded = false;
        Payslip.init();
        $scope.PayslipData = Payslip.data;
        $scope.$watch("PayslipData.loaded", function () {
            if ($scope.PayslipData.loaded == true) {
                $scope.payslip.loading = false;
                $scope.payslip.headers = $scope.PayslipData.payload.headers;
                $scope.payslip.data = $scope.PayslipData.payload.list;
                $scope.payslip.editable = $scope.PayslipData.payload.editable.fill(false, 0, Object.keys($scope.payslip.headers).length);
                $scope.getUrl();
                $scope.payslip.loaded = true;
                $scope.payslip.isDownloadReady = true;
            }
        });
        $scope.getUrl = function () {
            Restangular.all("payroll-csv-data/" + Payslip.data.id + "/create-csv")
                .customGET()
                .then(function (response) {
                    $scope.payslip.csv_url = "/api/v1/payroll-csv-data/" + Payslip.data.id + "/download?file=" + response.data;
                });
        };
        $scope.csvdownload = function () {
            $scope.payslip.downloaded = true;
        }
    }
]);
