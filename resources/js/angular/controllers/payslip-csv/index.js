angular.module("myApp").controller("payslipcsvCtrl", [
    "$scope",
    "Restangular",
    "Payslip",
    "$location",
    "$log",
    function ($scope, Restangular, Payslip, $location, $log) {
        if(window.status == 'approved') {
            $scope.step = 6;
            $location.path("/step6");
        } else {
            $scope.step = 1;
            $location.path('/step1');
        }
    }
]);
