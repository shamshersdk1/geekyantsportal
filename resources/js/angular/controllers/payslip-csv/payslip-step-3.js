angular.module("myApp").controller("payslipStep3Ctrl", [
  "$scope",
  "Restangular",
  "Payslip",
  "$log",
  function($scope, Restangular, Payslip, $log) {
    $scope.step = 3;
    $scope.bonuses = {
        list: [],
        loading: false,
        loaded: false,
        empty: false,
        init: function() {
            var params = {
                id: Payslip.data.id
            }
            Restangular.one("bonus")
                .get(params)
                .then(
                    function(response) {
                        $scope.bonuses.list = response.data;
                        $count = Object.keys($scope.bonuses.list).length;
                        if ($count <= 0) {
                            $scope.bonuses.empty = true;
                        }
                    },
                    function(errors) {
                    $log.log(errors);
                    }
                );
        }
    };
    $scope.bonuses.init();
    $scope.nextStep = function() {
        $scope.step = 4;
        Payslip.gotoRoute($scope.step);
    };
    $scope.prevStep = function() {
        $scope.step = 2;
        Payslip.gotoRoute($scope.step);
    };
  }
]);
