angular.module('myApp')
    .controller('seatModalCtrl',["$scope", "Restangular", "data", "$uibModalInstance", "$sce", function ($scope, Restangular, data, $uibModalInstance, $sce) {
        $scope.items = data;
        $sce.trustAsHtml = $scope.items;
        $scope.selected_user = $scope.items.seat_info.user_details.assigned_user;
        $scope.error_message = '';
        $scope.select_user = function (data) {
            $scope.selected_user = data;
        }
        $scope.saveData = function () {
            var params = {
                seat_id: $scope.items.seat_info.id,
                user_id: ($scope.selected_user) ? $scope.selected_user.id : '',
                seat_name: $scope.items.seat_info.name ? $scope.items.seat_info.name : ''
            };

            Restangular.all('seats').post(params).then(
                function (response) {
                    $scope.finalsubmit = false;
                    if (response.status) {
                        $scope.error_message = '';
                        $uibModalInstance.close(data);
                        window.location.reload();
                    }
                },
                function (errors) {
                    $scope.error_message = errors.data.message;
                }
            );
        };
        $scope.releaseUser = function (data) {
            Restangular.one("seats", data).remove()
            .then(
                function(response) {
                    $scope.finalsubmit = false;
                    if (response.status) {
                        $uibModalInstance.close(data);
                        window.location.reload();
                    }
                    },
                    function(error) {
                    }
                );
            
        }

        $scope.cancel = function () {
            $uibModalInstance.dismiss();
        };
    }]);