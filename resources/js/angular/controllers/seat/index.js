angular.module('myApp')
    .controller("seatCtrl",["$scope", "Restangular", "$uibModal", function ($scope, Restangular, $uibModal) {
        $scope.floor_details = window.floor_details;
        $scope.users = window.users;
        $scope.editSeat = function (seat) {
            if(seat.assignable){
            $scope.list = {
                seat_info: seat,
                users: $scope.users
            };
            var modal = $uibModal.open({
                controller: "seatModalCtrl",
                windowClass: "bootstrap_wrap",
                templateUrl: "/views/seat-management/seat_assign.html",
                backdrop: "static",
                size: "md",
                resolve: {
                    data: function () {
                        return $scope.list;
                    }
                }
            });
            modal.result.then(function (res) {  }, function () { });
            }
        };

    }]);