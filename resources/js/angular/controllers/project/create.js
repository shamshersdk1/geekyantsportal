angular.module("myApp").controller("createProjectCtrl", [
  "$scope",
  "Restangular",
  function($scope, Restangular) {
    $scope.clientList = window.clientList;
    $scope.projectCategories = window.projectCategories;
    $scope.slackPublicChannels = window.slackPublicChannels;
    $scope.slackPrivateChannels = window.slackPrivateChannels;
    $scope.salesExecutiveList = window.salesExecutiveList;
    $scope.projectCoordinatorList = window.projectCoordinatorList;
    $scope.projectManagerList = window.projectManagerList;
    $scope.codeLeadList = window.codeLeadList;
    $scope.deliveryLeadList = window.deliveryLeadList;
    $scope.allActiveUserList = window.allActiveUserList;
    $scope.antManagerId = window.antManagerId;
    $scope.selected_client = "";
    $scope.project_name = "";
    $scope.selected_project_category = "";
    $scope.status = "open";
    $scope.email_reminder = false;
    $scope.is_critical = false;
    $scope.selected_sales_manager = "";
    $scope.selected_project_coordinator = "";
    $scope.selected_project_manager = "";
    $scope.selected_code_lead = "";
    $scope.selected_delivery_lead = "";

    $scope.checkBoxes = {
      ui_ux: "",
      dev_op: "",
      architect: "",
      deployment: "",
      qa: ""
    };
    $scope.leads = {
      ui_ux_lead: "",
      dev_op_lead: "",
      architect_lead: "",
      deployment_lead: "",
      qa_lead: ""
    };
    $scope.start_date = {
      options: {
        applyClass: "btn-success",
        singleDatePicker: true,
        locale: {
          applyLabel: "Apply",
          fromLabel: "From",
          // format: "YYYY-MM-DD", //will give you 2017-01-06
          format: "D-MMM-YY", //will give you 6-Jan-17
          // format: "D-MMMM-YY", //will give you 6-January-17
          toLabel: "To",
          cancelLabel: "Cancel",
          customRangeLabel: "Custom range"
        }
      }
    };
    $scope.end_date = {
      options: {
        applyClass: "btn-success",
        singleDatePicker: true,
        locale: {
          applyLabel: "Apply",
          fromLabel: "From",
          // format: "YYYY-MM-DD", //will give you 2017-01-06
          format: "D-MMM-YY", //will give you 6-Jan-17
          // format: "D-MMMM-YY", //will give you 6-January-17
          toLabel: "To",
          cancelLabel: "Cancel",
          customRangeLabel: "Custom range"
        },
        date: null
      }
    };

    var checkIfBotIsMember = function(channel) {
      if (channel.members.indexOf($scope.antManagerId) !== -1) {
        return true;
      } else return false;
    };

    $scope.select_client = function(data) {
      $scope.selected_client = data;
      $scope.selected_client_error = false;
    };

    $scope.select_project_category = function(data) {
      $scope.selected_project_category = data;
      $scope.selected_project_category_error = false;
    };
    $scope.select_slack_public_channels = function(data) {
      $scope.selected_slack_public_channels = data;
      $scope.selected_slack_public_channels_error = false;
    };

    $scope.selected_private_channel_error_array = [];

    $scope.select_slack_private_channels = function(data, item) {
      $scope.selected_slack_private_channels_error = false;
      var isMemeber = checkIfBotIsMember(item);
      if (!isMemeber) {
        item.loading = false;
        item.reCheck = function() {
          item.loading = true;
          Restangular.all("check-channel-bot")
            .customGET("", {
              channel_id: item.id
            })
            .then(
              function(response) {
                item.loading = false;
                if (response == true) {
                  $scope.selected_private_channel_error_array.splice(
                    $scope.selected_private_channel_error_array.indexOf(item),
                    1
                  );
                }
              },
              function(error) {
                item.loading = false;
                console.log(error);
              }
            );
        };
        $scope.selected_private_channel_error_array.push(item);

        $scope.selected_slack_private_channels_error = true;
      }
      $scope.selected_slack_private_channels = data;
    };
    $scope.deselect_slack_private_channels = function(item) {
      if ($scope.selected_private_channel_error_array.indexOf(item) !== -1) {
        $scope.selected_private_channel_error_array.splice(
          $scope.selected_private_channel_error_array.indexOf(item),
          1
        );
      }
      console.log($scope.selected_slack_private_channels);
    };

    $scope.select_sales_manager = function(data) {
      $scope.selected_sales_manager = data;
      $scope.selected_sales_manager_error = false;
    };

    $scope.select_project_coordinator = function(data) {
      $scope.selected_project_coordinator = data;
      $scope.selected_project_coordinator_error = false;
    };

    $scope.select_project_manager = function(data) {
      $scope.selected_project_manager = data;
      $scope.selected_project_manager_error = false;
    };

    $scope.select_code_lead = function(data) {
      $scope.selected_code_lead = data;
      $scope.selected_code_lead_error = false;
    };

    $scope.select_delivery_lead = function(data) {
      $scope.selected_delivery_lead = data;
      $scope.selected_delivery_lead_error = false;
    };

    $scope.select_ui_ux_lead = function(data) {
      $scope.leads.ui_ux_lead = data;
      $scope.selected_ui_ux_lead_error = false;
    };

    $scope.select_dev_op_lead = function(data) {
      $scope.leads.dev_op_lead = data;
      $scope.selected_dev_op_lead_error = false;
    };

    $scope.select_architect = function(data) {
      $scope.leads.architect_lead = data;
      $scope.selected_architect_error = false;
    };

    $scope.select_deployment_lead = function(data) {
      $scope.leads.deployment_lead = data;
      $scope.selected_deployment_lead_error = false;
    };

    $scope.select_qa_lead = function(data) {
      $scope.leads.qa_lead = data;
      $scope.selected_qa_lead_error = false;
    };

    $scope.checkForErrors = function() {
      var error = false;
      if ($scope.selected_client == "") {
        $scope.selected_client_error = true;
        error = true;
      }
      if ($scope.project_name == "") {
        $scope.project_name_error = true;
        error = true;
      }
      if ($scope.selected_project_category == "") {
        $scope.selected_project_category_error = true;
        error = true;
      }
      if ($scope.status == "") {
        $scope.status_error = true;
        error = true;
      }
      if ($scope.selected_sales_manager == "") {
        $scope.selected_sales_manager_error = true;
        error = true;
      }
      if ($scope.selected_project_coordinator == "") {
        $scope.selected_project_coordinator_error = true;
        error = true;
      }
      if ($scope.selected_project_manager == "") {
        $scope.selected_project_manager_error = true;
        error = true;
      }
      if ($scope.selected_code_lead == "") {
        $scope.selected_code_lead_error = true;
        error = true;
      }
      if ($scope.selected_delivery_lead == "") {
        $scope.selected_delivery_lead_error = true;
        error = true;
      }
      if ($scope.checkBoxes.ui_ux != "" && $scope.leads.ui_ux_lead == "") {
        $scope.selected_ui_ux_lead_error = true;
        error = true;
      }
      if ($scope.checkBoxes.dev_op != "" && $scope.leads.dev_op_lead == "") {
        $scope.selected_dev_op_lead_error = true;
        error = true;
      }
      if (
        $scope.checkBoxes.architect != "" &&
        $scope.leads.architect_lead == ""
      ) {
        $scope.selected_architect_error = true;
        error = true;
      }
      if (
        $scope.checkBoxes.deployment != "" &&
        $scope.leads.deployment_lead == ""
      ) {
        $scope.selected_deployment_lead_error = true;
        error = true;
      }
      if ($scope.checkBoxes.qa != "" && $scope.leads.qa_lead == "") {
        $scope.selected_qa_lead_error = true;
        error = true;
      }
      if ($scope.selected_private_channel_error_array.length > 0) {
        error = true;
      }
      return error;
    };

    $scope.createProject = function() {
      var endDate = "";
      if ($scope.end_date.date) {
        endDate = moment($scope.end_date.date).format("YYYY-MM-DD");
      }
      var error = $scope.checkForErrors();
      if (error) {
        return;
      } else {
        // Make post request
        var startDate = moment($scope.start_date.date).format("YYYY-MM-DD");
        var params = {
          company_id: $scope.selected_client.id,
          project_name: $scope.project_name,
          start_date: startDate,
          end_date: endDate,
          project_manager: $scope.selected_project_manager.name,
          project_manager_id: $scope.selected_project_manager.id,
          account_manager_id: $scope.selected_project_coordinator.id,
          project_category_id: $scope.selected_project_category.id,
          bd_manager_id: $scope.selected_sales_manager.id,
          code_lead_id: $scope.selected_code_lead.id,
          delivery_lead_id: $scope.selected_delivery_lead.id,
          email_reminder: $scope.email_reminder,
          is_critical: $scope.is_critical,
          status: $scope.status,
          slack_public_channels: $scope.selected_slack_public_channels,
          slack_private_channels: $scope.selected_slack_private_channels,
          project_leads: $scope.leads,
          lead_type: $scope.checkBoxes
        };
        Restangular.all("project/create")
          .post(params)
          .then(
            function(response) {
              console.log(response);
              var path = "/admin/project";
              window.location.href = path;
            },
            function(errors) {
              console.log(errors);
            }
          );
      }
    };
  }
]);
