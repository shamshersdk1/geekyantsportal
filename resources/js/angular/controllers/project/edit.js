angular.module("myApp").controller("editProjectCtrl", [
  "$scope",
  "Restangular",
  function($scope, Restangular) {
    $scope.clientList = window.clientList;
    $scope.projectCategories = window.projectCategories;
    $scope.slackPublicChannels = window.slackPublicChannels;
    $scope.slackPrivateChannels = window.slackPrivateChannels;
    $scope.salesExecutiveList = window.salesExecutiveList;
    $scope.projectCoordinatorList = window.projectCoordinatorList;
    $scope.projectManagerList = window.projectManagerList;
    $scope.codeLeadList = window.codeLeadList;
    $scope.deliveryLeadList = window.deliveryLeadList;
    $scope.allActiveUserList = window.allActiveUserList;
    $scope.project = window.project;
    $scope.selected_public_channels = window.selected_public_channels;
    $scope.selected_private_channels = window.selected_private_channels;
    $scope.antManagerId = window.antManagerId;
    $scope.selected_client = "";
    $scope.selected_project_category = "";
    $scope.email_reminder = false;
    $scope.is_critical = false;
    $scope.status_list = [
      { value: "open", name: "Open" },
      { value: "initial", name: "Initial" },
      { value: "sow", name: "SOW" },
      { value: "nda", name: "NDA" },
      { value: "assigned", name: "Assigned" },
      { value: "in_progress", name: "In progress" },
      { value: "completed", name: "Completed" },
      { value: "closed", name: "Closed" },
      { value: "free", name: "Free" }
    ];

    $scope.checkBoxes = {
      ui_ux: "",
      dev_op: "",
      architect: "",
      deployment: "",
      qa: ""
    };
    $scope.leads = {
      ui_ux_lead: "",
      dev_op_lead: "",
      architect_lead: "",
      deployment_lead: "",
      qa_lead: ""
    };

    for (var i = 0; i < $scope.project.project_leads.length; i++) {
      switch ($scope.project.project_leads[i].key) {
        case "ui_ux_lead":
          if ($scope.project.project_leads[i].is_ours == 1) {
            $scope.checkBoxes.ui_ux = "geekyants";
            $scope.leads.ui_ux_lead = Number(
              $scope.project.project_leads[i].value
            );
          } else {
            $scope.checkBoxes.ui_ux = "client";
            $scope.leads.ui_ux_lead = $scope.project.project_leads[i].value;
          }
          break;
        case "dev_op_lead":
          if ($scope.project.project_leads[i].is_ours == 1) {
            $scope.checkBoxes.dev_op = "geekyants";
            $scope.leads.dev_op_lead = Number(
              $scope.project.project_leads[i].value
            );
          } else {
            $scope.checkBoxes.dev_op = "client";
            $scope.leads.dev_op_lead = $scope.project.project_leads[i].value;
          }
          break;
        case "architect_lead":
          if ($scope.project.project_leads[i].is_ours == 1) {
            $scope.checkBoxes.architect = "geekyants";
            $scope.leads.architect_lead = Number(
              $scope.project.project_leads[i].value
            );
          } else {
            $scope.checkBoxes.architect = "client";
            $scope.leads.architect_lead = $scope.project.project_leads[i].value;
          }
          break;
        case "deployment_lead":
          if ($scope.project.project_leads[i].is_ours == 1) {
            $scope.checkBoxes.deployment = "geekyants";
            $scope.leads.deployment_lead = Number(
              $scope.project.project_leads[i].value
            );
          } else {
            $scope.checkBoxes.deployment = "client";
            $scope.leads.deployment_lead =
              $scope.project.project_leads[i].value;
          }
          break;
        case "qa_lead":
          if ($scope.project.project_leads[i].is_ours == 1) {
            $scope.checkBoxes.qa = "geekyants";
            $scope.leads.qa_lead = Number(
              $scope.project.project_leads[i].value
            );
          } else {
            $scope.checkBoxes.qa = "client";
            $scope.leads.qa_lead = $scope.project.project_leads[i].value;
          }
          break;
        default:
      }
    }

    $scope.start_date = {
      options: {
        applyClass: "btn-success",
        singleDatePicker: true,
        locale: {
          applyLabel: "Apply",
          fromLabel: "From",
          // format: "YYYY-MM-DD", //will give you 2017-01-06
          format: "D-MMM-YY", //will give you 6-Jan-17
          // format: "D-MMMM-YY", //will give you 6-January-17
          toLabel: "To",
          cancelLabel: "Cancel",
          customRangeLabel: "Custom range"
        }
      }
    };
    $scope.start_date.date = moment($scope.project.start_date);

    $scope.end_date = {
      options: {
        applyClass: "btn-success",
        singleDatePicker: true,
        locale: {
          applyLabel: "Apply",
          fromLabel: "From",
          // format: "YYYY-MM-DD", //will give you 2017-01-06
          format: "D-MMM-YY", //will give you 6-Jan-17
          // format: "D-MMMM-YY", //will give you 6-January-17
          toLabel: "To",
          cancelLabel: "Cancel",
          customRangeLabel: "Custom range"
        },
        date: null
      }
    };
    if ($scope.project.end_date)
      $scope.end_date.date = moment($scope.project.end_date);

    var checkIfBotIsMember = function(channel) {
      if (channel.members.indexOf($scope.antManagerId) !== -1) {
        return true;
      } else return false;
    };

    $scope.select_client = function(data) {
      $scope.project.company_id = data.id;
      $scope.selected_client_error = false;
    };

    $scope.select_project_category = function(data) {
      $scope.project.project_category_id = data.id;
      $scope.selected_project_category_error = false;
    };
    $scope.select_slack_public_channels = function(data) {
      $scope.selected_public_channels = data;
      $scope.selected_slack_public_channels_error = false;
    };
    $scope.deselect_slack_public_channels = function(data, item) {
      $scope.selected_public_channels = data;
      $scope.selected_slack_public_channels_error = false;
    };
    $scope.selected_private_channel_error_array = [];

    $scope.select_slack_private_channels = function(data, item) {
      $scope.selected_slack_private_channels_error = false;
      var isMemeber = checkIfBotIsMember(item);
      if (!isMemeber) {
        item.loading = false;
        item.reCheck = function() {
          item.loading = true;
          Restangular.all("check-channel-bot")
            .customGET("", {
              channel_id: item.id
            })
            .then(
              function(response) {
                item.loading = false;
                if (response == true) {
                  $scope.selected_private_channel_error_array.splice(
                    $scope.selected_private_channel_error_array.indexOf(item),
                    1
                  );
                }
              },
              function(error) {
                item.loading = false;
                console.log(error);
              }
            );
        };
        $scope.selected_private_channel_error_array.push(item);

        $scope.selected_slack_private_channels_error = true;
      }

      $scope.selected_private_channels = data;
    };

    $scope.deselect_slack_private_channels = function(data, item) {
      if ($scope.selected_private_channel_error_array.indexOf(item) !== -1) {
        $scope.selected_private_channel_error_array.splice(
          $scope.selected_private_channel_error_array.indexOf(item),
          1
        );
      }
      $scope.selected_private_channels = data;
    };

    $scope.select_sales_manager = function(data) {
      $scope.project.bd_manager_id = data.id;
      $scope.selected_sales_manager_error = false;
    };

    $scope.select_project_coordinator = function(data) {
      $scope.project.account_manager_id = data.id;
      $scope.selected_project_coordinator_error = false;
    };

    $scope.select_project_manager = function(data) {
      $scope.project.project_manager_id = data.id;
      $scope.selected_project_manager_error = false;
    };

    $scope.select_code_lead = function(data) {
      $scope.project.code_lead_id = data.id;
      $scope.selected_code_lead_error = false;
    };

    $scope.select_delivery_lead = function(data) {
      $scope.project.delivery_lead_id = data.id;
      $scope.selected_delivery_lead_error = false;
    };

    $scope.select_ui_ux_lead = function(data) {
      $scope.leads.ui_ux_lead = data.id;
      $scope.selected_ui_ux_lead_error = false;
    };

    $scope.select_dev_op_lead = function(data) {
      $scope.leads.dev_op_lead = data.id;
      $scope.selected_dev_op_lead_error = false;
    };

    $scope.select_architect = function(data) {
      $scope.leads.architect_lead = data.id;
      $scope.selected_architect_error = false;
    };

    $scope.select_deployment_lead = function(data) {
      $scope.leads.deployment_lead = data.id;
      $scope.selected_deployment_lead_error = false;
    };

    $scope.select_qa_lead = function(data) {
      $scope.leads.qa_lead = data.id;
      $scope.selected_qa_lead_error = false;
    };

    $scope.checkForErrors = function() {
      var error = false;
      if ($scope.project.company_id == "") {
        $scope.selected_client_error = true;
        error = true;
      }
      if ($scope.project.project_name == "") {
        $scope.project_name_error = true;
        error = true;
      }
      if ($scope.project.project_category_id == "") {
        $scope.selected_project_category_error = true;
        error = true;
      }
      if ($scope.project.status == "") {
        $scope.status_error = true;
        error = true;
      }
      if (
        $scope.project.bd_manager_id == "" ||
        $scope.project.bd_manager_id == null
      ) {
        $scope.selected_sales_manager_error = true;
        error = true;
      }
      if (
        $scope.project.account_manager_id == "" ||
        $scope.project.account_manager_id == null
      ) {
        $scope.selected_project_coordinator_error = true;
        error = true;
      }
      if (
        $scope.project.project_manager_id == "" ||
        $scope.project.project_manager_id == null
      ) {
        $scope.selected_project_manager_error = true;
        error = true;
      }
      if (
        $scope.project.code_lead_id == "" ||
        $scope.project.code_lead_id == null
      ) {
        $scope.selected_code_lead_error = true;
        error = true;
      }
      if (
        $scope.project.delivery_lead_id == "" ||
        $scope.project.delivery_lead_id == null
      ) {
        $scope.selected_delivery_lead_error = true;
        error = true;
      }
      if ($scope.checkBoxes.ui_ux != "" && $scope.leads.ui_ux_lead == "") {
        $scope.selected_ui_ux_lead_error = true;
        error = true;
      }
      if ($scope.checkBoxes.dev_op != "" && $scope.leads.dev_op_lead == "") {
        $scope.selected_dev_op_lead_error = true;
        error = true;
      }
      if (
        $scope.checkBoxes.architect != "" &&
        $scope.leads.architect_lead == ""
      ) {
        $scope.selected_architect_error = true;
        error = true;
      }
      if (
        $scope.checkBoxes.deployment != "" &&
        $scope.leads.deployment_lead == ""
      ) {
        $scope.selected_deployment_lead_error = true;
        error = true;
      }
      if ($scope.checkBoxes.qa != "" && $scope.leads.qa_lead == "") {
        $scope.selected_qa_lead_error = true;
        error = true;
      }
      if ($scope.selected_private_channel_error_array.length > 0) {
        error = true;
      }
      return error;
    };

    $scope.createProject = function() {
      var endDate = "";
      if ($scope.end_date.date) {
        endDate = moment($scope.end_date.date).format("YYYY-MM-DD");
      }
      var error = $scope.checkForErrors();
      if (error) {
        return;
      } else {
        // Make post request
        var startDate = moment($scope.start_date.date).format("YYYY-MM-DD");
        console.log("email", $scope.project.email_reminder);
        console.log("critical", $scope.project.is_critical);
        var params = {
          project_id: $scope.project.id,
          company_id: $scope.project.company_id,
          project_name: $scope.project.project_name,
          start_date: startDate,
          end_date: endDate,
          project_manager_id: $scope.project.project_manager_id,
          account_manager_id: $scope.project.account_manager_id,
          project_category_id: $scope.project.project_category_id,
          bd_manager_id: $scope.project.bd_manager_id,
          code_lead_id: $scope.project.code_lead_id,
          delivery_lead_id: $scope.project.delivery_lead_id,
          is_critical: $scope.project.is_critical,
          email_reminder: $scope.project.email_reminder,
          status: $scope.project.status,
          slack_public_channels: $scope.selected_public_channels,
          slack_private_channels: $scope.selected_private_channels,
          project_leads: $scope.leads,
          lead_type: $scope.checkBoxes
        };
        Restangular.all("project/edit")
          .post(params)
          .then(
            function(response) {
              var path = "/admin/project/" + $scope.project.id + "/dashboard";
              window.location.href = path;
            },
            function(errors) {
              console.log(errors);
            }
          );
      }
    };
  }
]);
