angular.module('myApp')
    .controller("adminTimesheetModifyCtrl", ["$scope", "Restangular", "$uibModal", function ($scope, Restangular, $uibModal) {
        $scope.form = {
            data : [
                {
                    'date' : '2018-01-12',
                    'project_id' : 1,
                    'hour' : 0
                }],
            addNewItem : function() {
                $scope.form.data.push({
                    'date' : '2018-01-12',
                    'project_id' : 1,
                    'hour' : 0
                })

            }
        }
    }]);