angular.module("myApp").controller("rmTimesheetReviewCtrl", [
  "$scope",
  "Restangular",
  "$uibModal",
  function($scope, Restangular, $uibModal) {
    $scope.data = [];
    $scope.extra_hour = [];
    Restangular.all("user-timesheet/get-pending-timesheet")
      .getList()
      .then(
        function(response) {
          $scope.data = response;
        },
        function(error) {}
      );
    var weekId = window.weekId;
    var userId = window.userId;
    $scope.test = "RM timesheet review";
    $scope.form = {
      approveExtraHour: function(index, date) {
        var data = $scope.data[index]["dates"][date];

        var params = { user_id: userId };

        var params = {
          user_id: data.user_id,
          date: date,
          extra_hours: data.extra,
          project_id: data.project_id,
          status: "approved"
        };

        Restangular.all("user-timesheet-extra")
          .post(params)
          .then(
            function(response) {
              $scope.data[index]["dates"][date].is_processed = true;
              $scope.data[index]["dates"][date].status = response.status;
              $scope.data[index]["dates"][date].approved_by = response.approver.name;
            },
            function(error) {
              console.log(error);
            }
          );
      },
      rejectExtraHour: function(index, date) {
        var data = $scope.data[index]["dates"][date];
        var params = { user_id: userId };
        var params = {
          user_id: data.user_id,
          date: date,
          extra_hours: data.extra,
          project_id: data.project_id,
          status: "rejected"
        };

        Restangular.all("user-timesheet-extra")
          .post(params)
          .then(
            function(response) {
              // $scope.extraHour = response;
                console.log(response);
              $scope.data[index]["dates"][date].is_processed = true;
              $scope.data[index]["dates"][date].status = response.status;
              $scope.data[index]["dates"][date].approved_by = response.approver.name;
            },
            function(error) {
              console.log(error);
              // $scope.model.data.days[index].splitting = false;
              // $scope.model.data.days[index].updating = false;
              // console.log("error", error);
            }
          );
      },
      approveAdditionalBonus: function(index, date) {
        var id = $scope.data[index]["dates"][date].additional_bonus.id;
        var notes = $scope.data[index]["dates"][date].additional_bonus.notes;
        Restangular.one("additional-work-days-bonus/" + id + "/approve")
          .get({notes:notes})
          .then(
            function(response) {
              console.log("ERR", response);
              $scope.data[index]["dates"][date].additional_bonus = response;
            },
            function(error) {
              console.log("additional-work-days-bonus", error);
            }
          );
      },
      rejectAdditionalBonus: function(index, date) {
        var id = $scope.data[index]["dates"][date].additional_bonus.id;
        var notes = $scope.data[index]["dates"][date].additional_bonus.notes;
        Restangular.one("additional-work-days-bonus/" + id + "/reject")
          .get({notes:notes})
          .then(
            function(response) {
              $scope.data[index]["dates"][date].additional_bonus = response;
            },
            function(error) {
              console.log("additional-work-days-bonus", error);
            }
          );
      },
      approveOnsiteBonus: function(index, date) {
        var id = $scope.data[index]["dates"][date].onsite_bonus.id;
        var notes = $scope.data[index]["dates"][date].onsite_bonus.notes;
        Restangular.one("onsite-bonus/" + id + "/approve")
          .get({notes:notes})
          .then(
            function(response) {
              $scope.data[index]["dates"][date].onsite_bonus = response;
            },
            function(error) {
              console.log("additional-work-days-bonus", error);
            }
          ); 
      },
      rejectOnsiteBonus: function(index, date) {
        var id = $scope.data[index]["dates"][date].onsite_bonus.id;
        var notes = $scope.data[index]["dates"][date].onsite_bonus.notes;
        Restangular.one("onsite-bonus/" + id + "/reject")
          .get({notes:notes})
          .then(
            function(response) {
              $scope.data[index]["dates"][date].onsite_bonus = response;
            },
            function(error) {
              console.log("additional-work-days-bonus", error);
            }
          );
      }
    };
    $scope.model = {
      userId: userId,
      weekId: weekId,
      loading: "false",
      data: {},
      reviewerName: "",
      status: "",

      getUserReport: function(userId, weekId) {
        $scope.model.loading = true;
        Restangular.oneUrl("get-user-weekly-timesheet/" + weekId + "/" + userId)
          .get()
          .then(
            function(response) {
              $scope.model.loading = false;
              $scope.model.data = response;
            },
            function(error) {
              $scope.model.loading = false;
              console.log("response", response);
            }
          );
      },
      approveWeeklyTimesheet: function(weekId, userId, date) {
        console.log("getLastDay!!s", date);
        var modal = $uibModal.open({
          controller: "rmTimesheetExtraHourCtrl",
          windowClass: "bootstrap_wrap",
          templateUrl: "/views/user-timesheet/extra-hour.html",
          backdrop: "static",
          size: "lg",
          resolve: {
            data: function() {
              return {
                week_id: weekId,
                user_id: userId,
                date: date
              };
            }
          }
        });
        modal.result.then(
          function(res) {
            window.location.reload();
            $scope.model.data = res;
          },
          function() {}
        );
      },
      reviewTimesheet: function(monthId, userId, date) {
        console.log("getLastDay", date);
        var modal = $uibModal.open({
          controller: "adminTimesheetExtraHourCtrl",
          windowClass: "bootstrap_wrap",
          templateUrl: "/views/user-timesheet/extra-hour.html",
          backdrop: "static",
          size: "lg",
          resolve: {
            data: function() {
              return {
                month_id: monthId,
                user_id: userId,
                date: date
              };
            }
          }
        });
        modal.result.then(
          function(res) {
            window.location.reload();
            $scope.model.data = res;
          },
          function() {}
        );
      },
      splitHours: function(index) {
        var totalApprovedHour =
          $scope.model.data.days[index].approved_total_time;
        var approvedHour = 8;
        if (totalApprovedHour > 10) {
          var extraHour = totalApprovedHour - approvedHour;
          $scope.model.data.days[index].approved_hour = approvedHour;
          $scope.model.data.days[index].extra_hour = extraHour;
        }
      },

      approveBonusRequest: function(parentIndex, index) {
        var day = $scope.model.data.days[parentIndex]["bonus_requests"][index];
        var bonusRequest = day;
        $scope.model.data.days[parentIndex]["bonus_requests"][
          index
        ].loading = true;
        Restangular.all("bonus-request-approve")
          .post(bonusRequest)
          .then(
            function(response) {
              var bonusRequest = response;
              bonusRequest.redeem_info = JSON.parse(response.redeem_type);
              $scope.model.data.days[parentIndex]["bonus_requests"][
                index
              ] = bonusRequest;
              $scope.model.data.days[parentIndex]["bonus_requests"][
                index
              ].loading = false;
            },
            function(error) {
              $scope.model.data.days[
                index
              ].additional_bonus_request.loading = false;
              console.log(error);
            }
          );
      },
      rejectBonusRequest: function(parentIndex, index) {
        var day = $scope.model.data.days[parentIndex]["bonus_requests"][index];
        $scope.model.data.days[parentIndex]["bonus_requests"][
          index
        ].loading = true;
        var bonusRequest = day;
        Restangular.all("bonus-request-reject")
          .post(bonusRequest)
          .then(
            function(response) {
              $scope.model.data.days[parentIndex]["bonus_requests"][
                index
              ] = response;
              $scope.model.data.days[parentIndex]["bonus_requests"][
                index
              ].loading = false;
              //$scope.init();
            },
            function(error) {
              $$scope.model.data.days[parentIndex]["bonus_requests"][
                index
              ].loading = false;
              console.log(error);
            }
          );
      },
      addExtra: function(index, status) {
        var day = $scope.model.data.days[index];
        $scope.model.data.days[index].updating = true;
        var extraHours = day.extra_hour.extra_hours;
        var projectId = day.extra_hour.project_id;

        if (extraHours > 0) {
          var params = {
            id: day.id,
            user_id: userId,
            date: day.date,
            extra_hours: extraHours,
            project_id: projectId,
            week_id: weekId,
            status: status
          };
          Restangular.all("user-timesheet-extra")
            .post(params)
            .then(
              function(response) {
                // $scope.extraHour = response;
                console.log(response);
                $scope.model.data.days[index].extra_hour = response;
                console.log(
                  "projects and days => ",
                  $scope.model.data.days[index]
                );

                // Restangular.one("user")
                // console.log(
                //   $scope.model.data.day.extra_hour[index].approver.name
                // );

                // $scope.model.data.days[parentIndex]["extra_hour"][
                //   index
                // ].loading = false;

                // console.log("statuss", $scope.extraHour.metaData.name);
                // console.log("statuss", $scope.extraHour.updated_at);

                // extraHour.redeem_info = JSON.parse(response.redeem_type);
                // $scope.model.data.days[parentIndex]["extra_hour"][
                //   index
                // ] = extraHour;
                // $scope.model.data.days[parentIndex]["extra_hour"][
                //   index
                // ].loading = false;
                // console.log("status", extra_hour.status);
                // console.log(response.metaData.name);
                // $scope.model.reviewerName = response.metaData.name;
                // $scope.model.status = response.metadata.status;
                // //$scope.model.data.days[index].approve_extra_hour = true;
                // $scope.model.data.days[index].updating = false;
                // $scope.model.data.days[index].splitting = false;
                // $scope.model.data.days[index].approved_total_time = 8;
                // $scope.model.data.days[index].extra_hours = extraHours;
                // $scope.model.data.days[index].extra_hour = response;
              },
              function(error) {
                console.log(error);

                $scope.model.data.days[index].splitting = false;
                $scope.model.data.days[index].updating = false;
                console.log("error", error);
              }
            );
        }
      }
    };

    $scope.model.getUserReport(userId, weekId);

    // $scope.projects = window.projects;
    // $scope.verticalTotals = JSON.parse(window.verticalTotals);
    // $scope.user = JSON.parse(window.user);
    // $scope.currentDate = new Date().toISOString().slice(0, 10);

    // $scope.addTime = function (name, result) {
    //     $scope.list = {
    //         project_name: name,
    //         data: result
    //     };

    //     var modal = $uibModal.open({
    //         controller: "modalCtrl",
    //         windowClass: "bootstrap_wrap",
    //         templateUrl: "/views/add_time.html",
    //         backdrop: "static",
    //         size: "md",
    //         resolve: {
    //             data: function () {
    //                 return $scope.list;
    //             }
    //         }
    //     });
    //     modal.result.then(function (res) { }, function () { });
    // };
    // $scope.addTimeAprrover = function (name, result, user) {
    //     $scope.list = {
    //         project_name: name,
    //         approved_hours: result.approved_hours,
    //         data: result,
    //         user_name: user
    //     };
    //     var modal = $uibModal.open({
    //         controller: "adminModalCtrl",
    //         windowClass: "bootstrap_wrap",
    //         templateUrl: "/views/approve_time.html",
    //         backdrop: "static",
    //         size: "md",
    //         resolve: {
    //             data: function () {
    //                 return $scope.list;
    //             }
    //         }
    //     });
    //     modal.result.then(function (res) { }, function () { });
    // };
    // $scope.updateApproveList = function (data) {
    //     var params = {
    //         timesheet_id: data.timesheet_id,
    //         approved_hours: data.approved_hours
    //     };
    //     Restangular.all("admin/timesheet")
    //         .post(params)
    //         .then(function (response) { }, function (error) { });
    // };
  }
]);
