angular
    .module("myApp")
    .controller("reviewGoalCtrl",["$scope", "Restangular", "$uibModal", "$log", "Goal", function ($scope, Restangular, $uibModal, $log, Goal) {
        $scope.completeLoading = false;
        $scope.userGoal = JSON.parse(window.userGoal);
        $scope.submitted = false;
        $scope.overall_feedback = '';
        $scope.display_submit = false;
        $scope.disableFields = false;
        if($scope.userGoal.status == 'completed'){
            $scope.disableFields = true;
        }
        var currentDate = new Date();
        var to_date = new Date($scope.userGoal.to_date);
        if ( to_date > currentDate )
        {
            $scope.display_submit = false;
        }
        else{
            $scope.display_submit = true;
        }
        $scope.GoalData = null;
        $scope.GoalData = Goal.data;
        $scope.goalData = Goal.data;
        $scope.$watch("GoalData.userGoalItemsLoaded", function () {
            if ($scope.GoalData.userGoalItemsLoaded == true) {
                $scope.form.data.goals = $scope.GoalData.userGoalItems;
            }
        });
        $scope.$watch("GoalData.userGoalLoaded", function () {
            if ($scope.GoalData.userGoalLoaded == true) {
                $scope.userGoal = $scope.GoalData.userGoal;
            }
        });
        $scope.$watch('userGoal', function() {
            if ( $scope.userGoal.status == 'completed' ) {
                $scope.disableFields = true;
            }
        });
        Goal.getUserGoals($scope.userGoal.id, params);
        var params = {
            start_date: moment($scope.userGoal.from_date).format("YYYY-MM-DD"),
            end_date: moment($scope.userGoal.to_date).format("YYYY-MM-DD")
        };

        $scope.form = {
            data: {
                user_goal_id: $scope.userGoal.id,
                goals: []
            },
            errors: [],
            close_others: true,
            
            store: function () {
                var result = $scope.form.validate();
                $scope.form.overall_feedback_error = result.overall_feedback_error;
                if (!result.overall_feedback_error) {
                    $scope.form.reviewUserGoals();
                }
            },
            validate: function () {
                var overall_feedback_error = false;
                if ( $scope.userGoal.overall_feedback == null || $scope.userGoal.overall_feedback == "" )
                {
                    overall_feedback_error = true;
                }
                return {
                    overall_feedback_error: overall_feedback_error
                };
            },
            reviewUserGoals: function () {
                $scope.loading = true;
                $scope.error = false;
                Restangular.one("user-goal-items", $scope.userGoal.id)
                    .customPUT({
                        overall_feedback: $scope.userGoal.overall_feedback
                    })
                    .then(function (response) {
                        window.location.reload();
                    });
            }
        };
        $scope.updateReview = function (item) {
            Restangular.one("user-goal-items/review", item.id)
                    .customPUT({
                        review: item.review_comment
                    })
                    .then(function (response) {
                    });
        };
        $scope.updatePoints = function (item) {
            Restangular.one("user-goal-items/points", item.id)
                    .customPUT({
                        points: item.points
                    })
                    .then(function (response) {
                    });
        };
        $scope.completeGoal = function (){
            $scope.completeLoading = true;
            Restangular.one("user-goals", $scope.userGoal.id)
                    .customPUT({
                        goals: $scope.form.data.goals,
                        status: 'completed'
                    })
                    .then(function (response) {
                        console.log(response);
                        $scope.completeLoading = false;
                        Goal.updateGoal($scope.userGoal.id);
                    },
                    function(errors){
                        $scope.completeLoading = false;
                        $scope.error = true;
                        $scope.success = false;
                    }
                );
            
        };
    }]);