angular
    .module("myApp")
    .controller("userGoalCtrl",["$scope", "Restangular", "Goal", function ($scope, Restangular, Goal) {
        $scope.loading = false;
        $scope.saveLoading = false;
        $scope.confirmLoading = false;
        $scope.freezeGoalLoading = false;
        $scope.userGoal = JSON.parse(window.goal);
        $scope.user = JSON.parse(window.user);
        $scope.submitted = false;
        $scope.GoalData = null;
        $scope.GoalData = Goal.data;
        $scope.collapse = '';
        $scope.disableFields = false;
        $scope.$watch("GoalData.userGoalItemsLoaded", function () {
            if ($scope.GoalData.userGoalItemsLoaded == true) {
                $scope.form.data.goals = $scope.GoalData.userGoalItems;
            }
        });
        $scope.$watch("GoalData.userGoalLoaded", function () {
            if ($scope.GoalData.userGoalLoaded == true) {
                $scope.userGoal = $scope.GoalData.userGoal;
            }
        });
        $scope.$watch('userGoal', function() {
            if ( $scope.userGoal.status == 'completed' ) {
                $scope.disableFields = true;
            }
        });
        Goal.getUserGoals($scope.userGoal.id, params);
        var params = {
            start_date: moment($scope.userGoal.from_date).format("YYYY-MM-DD"),
            end_date: moment($scope.userGoal.to_date).format("YYYY-MM-DD")
        };

        $scope.form = {
            data: {
                start_date: moment($scope.userGoal.from_date).format("YYYY-MM-DD"),
                end_date: moment($scope.userGoal.to_date).format("YYYY-MM-DD"),
                user_goal_id: $scope.userGoal.id,
                goals: [],
                newGoal: {
                    id: null,
                    title: "",
                    tags: [],
                    success_metric: "",
                    process: "",
                    is_idp: false,
                    is_open: false
                }
            },
            errors: [],
            close_others: true,
            addGoal: function (index) {
                var goal = $scope.data.goals[index] ? $scope.data.goals[index] : '';
                var length = goal.tags.length;
                tags = [];
                for (i = 0; i < length; i++) {
                    tags.push(goal.tags[i].name);
                }
                $scope.form.data.goals.push({
                    id: null,
                    title: goal.title,
                    tags: tags,
                    success_metric: goal.success_metric,
                    process: goal.process,
                    is_idp: false,
                    is_open: false
                });
            },
            createGoal: function (is_idp) {
                $scope.collapse = '';
                if (is_idp) {
                    $scope.form.data.newGoal.tags = [];
                    $scope.form.data.newGoal.success_metric = "";
                }
                var goal = $scope.form.data.newGoal;
                if (is_idp) {
                    goal.is_idp = true;
                }
                $scope.submitted = true;
                $scope.error = {
                    title: 0,
                    success_metric: 0,
                    process: 0,
                    tags: 0
                };
                if (goal.title == null || goal.title == "") {
                    $scope.error.title = 1;
                }
                if (!is_idp && (goal.success_metric == null || goal.success_metric == "")) {
                    $scope.error.success_metric = 1;
                }
                if (goal.process == null || goal.process == "") {
                    $scope.error.process = 1;
                }
                if ( !is_idp && goal.tags.length == 0) {
                    $scope.error.tags = 1;
                }
                if ($scope.error.title == 0 && $scope.error.tags == 0 && $scope.error.success_metric == 0 && $scope.error.process == 0 ) {
                    $scope.form.data.goals.push(goal);
                    $scope.collapse = 'collapse';
                    $scope.submitted = false;
                    $scope.form.data.newGoal = {
                        id: null,
                        title: "",
                        tags: [],
                        success_metric: "",
                        process: "",
                        is_idp: false,
                        is_open: false
                    };
                }
            },
            removeGoal: function (index) {
                $scope.form.data.goals.splice(index, 1);
            },
            store: function (option) {
                if (option == 'confirmed')
                {
                    if (!confirm("Accept the goals?")) 
                    {
                        return;
                    }
                }
                $scope.form.errors = [];
                $scope.form.close_others = true;
                var result = $scope.form.validate();
                $scope.form.errors = result.errors;
                if (result.count == 0) {
                    $scope.form.close_others = false;
                    $scope.form.updateUserGoals(option);
                }
            },
            validate: function () {
                var length = $scope.form.data.goals.length;
                var errors = [];
                count = 0;
                for (i = 0; i < length; i++) {
                    var goal = $scope.form.data.goals[i];
                    errors[i] = [];
                    if (goal.title == null || goal.title == "") {
                        errors[i].push("title");
                        count++;
                        $scope.form.data.goals[i].is_open = true;
                    }
                    if ( !goal.is_idp && goal.tags.length == 0) {
                        errors[i].push("tags");
                        count++;
                        $scope.form.data.goals[i].is_open = true;
                    }
                    if (!goal.is_idp && (goal.success_metric == null || goal.success_metric == "")) {
                        errors[i].push("success_metric");
                        count++;
                        $scope.form.data.goals[i].is_open = true;
                    }
                    if (goal.process == null || goal.process == "") {
                        errors[i].push("process");
                        count++;
                        $scope.form.data.goals[i].is_open = true;
                    }
                }

                return {
                    errors: errors,
                    count: count
                };
            },
            updateUserGoals: function (option) {
                if ( option == 'submitted' ){
                    $scope.saveLoading = true;
                }
                else if (option == 'confirmed') {
                    $scope.confirmLoading = true;
                }
                else if ( option == 'running' ){
                    $scope.freezeGoalLoading = true;
                }
                $scope.loading = true;
                $scope.error = false;
                Restangular.one("user-goal-items", $scope.userGoal.id)
                    .customPUT({
                        goals: $scope.form.data.goals,
                        status: option
                    })
                    .then(function (response) {
                        $scope.loading = false;
                        $scope.saveLoading = false;
                        $scope.confirmLoading = false;
                        $scope.freezeGoalLoading = false;
                        Goal.getUserGoals($scope.userGoal.id, params);
                        Goal.updateGoal($scope.userGoal.id);
                    },
                    function(errors){
                        $scope.loading = false;
                        $scope.saveLoading = false;
                        $scope.confirmLoading = false;
                        $scope.freezeGoalLoading = false;
                        $scope.error = true;
                        $scope.success = false;
                    }
                );
            }
        };
    }]);