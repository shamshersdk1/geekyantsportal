angular
    .module("myApp")
    .controller("goalCtrl",["$scope", "Restangular", "$uibModal", "$log", "Goal", "$location", function ($scope, Restangular, $uibModal, $log, Goal, $location) {
        $scope.test = "success";
        var startDate;
        var endDate;
        var userGoalId = null;
        var userId = '';
        $scope.submitted = false;
        $scope.loading = false;
        $scope.saveAsDraftLoading = false;
        $scope.setGoalLoading = false;
        $scope.data = {
            goals: [],
            tags: [],
            selected_tags: [],
            userGoalItems: [],
            newGoal: {
                title: "",
                success_metric: "",
                process: "",
                tags: []
            },
            newIdp: {
                title: "",
                process: ""
            }
        };
        Restangular.all("myUsers").getList({
                'pagination': 0
            })
            .then(
                function (response) {
                    $scope.loading = false;
                    $scope.userList = response;
                },
                function (errors) {
                    $scope.loading = false;
                    $scope.error_message = errors.data.message;
                }
            );
        $scope.select_user = function (data) {
            $scope.selected_user = data;
            $scope.form.userSelectError = false;
        };

        if (moment().quarter() == 1) {
            startDate = moment().subtract(1, "year").month("April").startOf("month");
            endDate = moment().month("March").endOf("month");
        } else {
            startDate = moment().month("April").startOf("month");
            endDate = moment().add(1, "year").month("March").endOf("month");
        }

        $scope.datePicker = {
            date: {
                startDate: startDate,
                endDate: endDate
            },
            options: {
                alwaysShowCalendars: true,
                locale: {
                    format: "D-MMM-YY", // 
                },
                ranges: {
                    "Last Year": [moment(startDate).subtract(1, "years"), moment(endDate).subtract(1, "years")]
                }
            }
        };
        $scope.form = {
            data: {
                start_date: moment($scope.datePicker.date.startDate).format(
                    "YYYY-MM-DD"
                ),
                end_date: moment($scope.datePicker.date.endDate).format("YYYY-MM-DD"),
                user_goal_id: 1,
                goals: [],
                newGoal: {
                    id: null,
                    title: "",
                    tags: [],
                    success_metric: "",
                    process: "",
                    is_idp: false,
                    is_open: false
                }
            },
            errors: [],
            close_others: true,
            addGoal: function (index) {
                var goal = $scope.data.goals[index] ? $scope.data.goals[index] : '';
                var length = goal.tags.length;
                tags = [];
                for (i = 0; i < length; i++) {
                    tags.push(goal.tags[i].name);
                }
                $scope.form.data.goals.push({
                    id: null,
                    title: goal.title,
                    tags: tags,
                    success_metric: goal.success_metric,
                    process: goal.process,
                    is_idp: false,
                    is_open: false
                });
            },
            createGoal: function (is_idp) {
                if (is_idp) {
                    $scope.form.data.newGoal.tags = [];
                    $scope.form.data.newGoal.success_metric = "";
                }
                var goal = $scope.form.data.newGoal;
                if (is_idp) {
                    goal.is_idp = true;
                }
                $scope.submitted = true;
                $scope.error = {
                    title: 0,
                    success_metric: 0,
                    process: 0,
                    tags: 0
                };
                if (goal.title == null || goal.title == "") {
                    $scope.error.title = 1;
                }
                if (!is_idp && (goal.success_metric == null || goal.success_metric == "")) {
                    $scope.error.success_metric = 1;
                }
                if (goal.process == null || goal.process == "") {
                    $scope.error.process = 1;
                }
                if (goal.tags.length == 0 && !is_idp ) {
                    $scope.error.tags = 1;
                }
                if ($scope.error.title == 0 && $scope.error.tags == 0 && $scope.error.success_metric == 0 && $scope.error.process == 0 ) {
                    $scope.form.data.goals.push(goal);
                    $scope.submitted = false;
                    $scope.form.data.newGoal = {
                        id: null,
                        title: "",
                        tags: [],
                        success_metric: "",
                        process: "",
                        is_idp: false,
                        is_open: false
                    };
                }
            },
            removeGoal: function (index) {
                $scope.form.data.goals.splice(index, 1);
            },
            store: function (option) {
                $scope.form.errors = [];
                $scope.form.close_others = true;
                var result = $scope.form.validate();
                $scope.form.errors = result.errors;
                $scope.form.userSelectError = result.userSelectError;
                if (result.count == 0) {
                    $scope.form.close_others = false;
                    $scope.form.updateUserGoals(option);
                }
            },
            validate: function () {
                var length = $scope.form.data.goals.length;
                var errors = [];
                var userSelectError = false;
                count = 0;
                for (i = 0; i < length; i++) {
                    var goal = $scope.form.data.goals[i];
                    errors[i] = [];
                    if (goal.title == null || goal.title == "") {
                        errors[i].push("title");
                        count++;
                        $scope.form.data.goals[i].is_open = true;
                    }
                    if (goal.tags.length == 0 && !goal.is_idp ) {
                        errors[i].push("tags");
                        count++;
                        $scope.form.data.goals[i].is_open = true;
                    }
                    if (!goal.is_idp && (goal.success_metric == null || goal.success_metric == "")) {
                        errors[i].push("success_metric");
                        count++;
                        $scope.form.data.goals[i].is_open = true;
                    }
                    if (goal.process == null || goal.process == "") {
                        errors[i].push("process");
                        count++;
                        $scope.form.data.goals[i].is_open = true;
                    }
                }
                if ( $scope.selected_user == null || $scope.selected_user == '' )
                {
                    userSelectError = true;
                    count++;
                }

                return {
                    errors: errors,
                    count: count,
                    userSelectError : userSelectError
                };
            },
            updateUserGoals: function (option) {
                if ( option == 'pending' )
                {
                    $scope.saveAsDraftLoading = true;
                }
                else
                {
                    $scope.setGoalLoading = true;
                }
                console.log('$scope.saveAsDraftLoading', $scope.saveAsDraftLoading);

                $scope.loading = true;
                $scope.error = false;
                var startDate = moment($scope.datePicker.date.startDate).format(
                    "YYYY-MM-DD"
                );
                var endDate = moment($scope.datePicker.date.endDate).format(
                    "YYYY-MM-DD"
                );

                var params = {
                    user_id: ($scope.selected_user) ? $scope.selected_user.id : '',
                    from_date: startDate,
                    to_date: endDate,
                    status: option
                  };
                 
                if ( userGoalId == null )
                {
                    Restangular.all("user-goals")
                    .post(params)
                    .then(
                    function(response) {
                        userGoalId = response.goalObj.id;
                        userId = response.goalObj.user_id;
                        Restangular.one("user-goal-items", userGoalId)
                            .customPUT({
                                goals: $scope.form.data.goals
                            })
                            .then(function (response) {
                                $scope.saveAsDraftLoading = false;
                                $scope.setGoalLoading = false;
                                if ( option != 'pending' )
                                {
                                    var path = "/admin/goal-user-list/"+userId+"/"+userGoalId+"/view";
                                    window.location.href =  path;
                                }
                                    
                            });
                        $scope.saveAsDraftLoading = false;
                        $scope.setGoalLoading = false;
                        $scope.success = true;
                        
                    },
                    function(errors) {
                        $scope.saveAsDraftLoading = false;
                        $scope.setGoalLoading = false;
                        $scope.error = true;
                        $scope.success = false;
                    }
                    );
                }
                else 
                {
                    Restangular.one("user-goal-items", userGoalId)
                    .customPUT({
                        goals: $scope.form.data.goals,
                        status: option,
                        flag: 'new'
                    })
                    .then(function (response) {
                        $scope.saveAsDraftLoading = false;
                        $scope.setGoalLoading = false;
                        if ( option != 'pending' )
                        {
                            var path = "/admin/goal-user-list/"+userId+"/"+userGoalId+"/view";
                            window.location.href =  path;
                        }
                    },
                    function(errors) {
                        $scope.saveAsDraftLoading = false;
                        $scope.setGoalLoading = false;
                        $scope.error = true;
                        $scope.success = false;
                    }
                );
                }
                
            }
        };

        $scope.search = {
            text: "",
            tags: []
        };
        $scope.GoalData = null;
        $scope.select2Options = {
            allowClear: true
        };

        Restangular.one("tags")
            .get()
            .then(function (response) {
                $scope.data.tags = response;
            });

        Goal.getGoals($scope.search);
        $scope.GoalData = Goal.data;

        $scope.$watch("GoalData.loaded", function () {
            if ($scope.GoalData.loaded == true) {
                $scope.data.goals = $scope.GoalData.goals;
            }
        });
        // $scope.$watch("GoalData.userGoalItemsLoaded", function () {
        //     if ($scope.GoalData.userGoalItemsLoaded == true) {
        //         $scope.form.data.goals = $scope.GoalData.userGoalItems;
        //     }
        // });

        $scope.filter = function () {
            Goal.getGoals($scope.search);
        };
        params = {
            start_date: moment($scope.datePicker.date.startDate).format(
                "YYYY-MM-DD"
            ),
            end_date: moment($scope.datePicker.date.endDate).format("YYYY-MM-DD")
        };

        Goal.getUserGoals(1, params);

        $scope.saveGoal = function (is_private) {
            var params = $scope.data.newGoal;
            Goal.saveGoal(params, is_private, false);
        };

        $scope.saveIdp = function () {
            var params = $scope.data.newIdp;
            Goal.saveGoal(params, true, true);
        };

        $scope.attachGoal = function (goal_id) {
            Goal.attachGoal(1, goal_id, false, false);
        };

        $scope.detachGoal = function (item_id) {
            Goal.detachGoal(item_id);
        };

        $scope.updateUserGoals = function () {
            params = {
                user_goal_items: $scope.data.userGoalItems
            };
            Goal.updateUserGoals(params);
        };

        $scope.$watch("GoalData.newGoalAdded", function () {
            if ($scope.GoalData.newGoalAdded == true) {
                params = {
                    start_date: moment($scope.datePicker.date.startDate)
                        .add("330", "m")
                        .format("YYYY-MM-DD"),
                    end_date: moment($scope.datePicker.date.endDate)
                        .add("330", "m")
                        .format("YYYY-MM-DD")
                };
                $scope.data.newGoal = [];
                Goal.getUserGoals(1, params);
            }
        });
    }]);