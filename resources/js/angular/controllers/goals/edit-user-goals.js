angular
    .module("myApp")
    .controller("editGoalCtrl", ["$scope", "Restangular", "$uibModal", "$log", "Goal", function ($scope, Restangular, $uibModal, $log, Goal) {
        $scope.userGoal = JSON.parse(window.userGoal);
        $scope.submitted = false;
        $scope.loading = false;
        $scope.saveAsDraftLoading = false;
        $scope.setGoalLoading = false;
        $scope.freezeGoalLoading = false;
        $scope.data = {
            goals: [],
            tags: [],
            selected_tags: [],
            userGoalItems: [],
            newGoal: {
                title: "",
                success_metric: "",
                process: "",
                tags: []
            },
            newIdp: {
                title: "",
                process: ""
            }
        };
        
        $scope.form = {
            data: {
                start_date: moment($scope.userGoal.from_date).format("YYYY-MM-DD"),
                end_date: moment($scope.userGoal.to_date).format("YYYY-MM-DD"),
                user_goal_id: $scope.userGoal.id,
                goals: [],
                newGoal: {
                    id: null,
                    title: "",
                    tags: [],
                    success_metric: "",
                    process: "",
                    is_idp: false,
                    is_open: false
                }
            },
            errors: [],
            close_others: true,
            addGoal: function (index) {
                var goal = $scope.data.goals[index] ? $scope.data.goals[index] : '';
                var length = goal.tags.length;
                tags = [];
                for (i = 0; i < length; i++) {
                    tags.push(goal.tags[i].name);
                }
                $scope.form.data.goals.push({
                    id: null,
                    title: goal.title,
                    tags: tags,
                    success_metric: goal.success_metric,
                    process: goal.process,
                    is_idp: false,
                    is_open: false
                });
            },
            createGoal: function (is_idp) {
                if (is_idp) {
                    $scope.form.data.newGoal.tags = [];
                    $scope.form.data.newGoal.success_metric = "";
                }
                var goal = $scope.form.data.newGoal;
                if (is_idp) {
                    goal.is_idp = true;
                }
                $scope.submitted = true;
                $scope.error = {
                    title: 0,
                    success_metric: 0,
                    process: 0,
                    tags: 0
                };
                if (goal.title == null || goal.title == "") {
                    $scope.error.title = 1;
                }
                if (!is_idp && (goal.success_metric == null || goal.success_metric == "")) {
                    $scope.error.success_metric = 1;
                }
                if (goal.process == null || goal.process == "") {
                    $scope.error.process = 1;
                }
                if ( !is_idp && goal.tags.length == 0) {
                    $scope.error.tags = 1;
                }
                if ($scope.error.title == 0 && $scope.error.tags == 0 && $scope.error.success_metric == 0 && $scope.error.process == 0 ) {
                    $scope.form.data.goals.push(goal);
                    $scope.submitted = false;
                    $scope.form.data.newGoal = {
                        id: null,
                        title: "",
                        tags: [],
                        success_metric: "",
                        process: "",
                        is_idp: false,
                        is_open: false
                    };
                }
            },
            removeGoal: function (index) {
                Restangular.one("user-goal-items", $scope.form.data.goals[index].id)
                    .remove()
                    .then(function (response) {
                        $scope.form.data.goals.splice(index, 1);
                    },
                    function(errors) {
                        $scope.loading = false;
                        $scope.error = true;
                        console.log(errors);
                    }
                );
                
            },
            store: function (option) {
                $scope.form.errors = [];
                $scope.form.close_others = true;
                var result = $scope.form.validate();
                $scope.form.errors = result.errors;
                if (result.count == 0) {
                    $scope.form.close_others = false;
                    $scope.form.updateUserGoals(option);
                }
            },
            validate: function () {
                var length = $scope.form.data.goals.length;
                var errors = [];
                count = 0;
                for (i = 0; i < length; i++) {
                    var goal = $scope.form.data.goals[i];
                    errors[i] = [];
                    if (goal.title == null || goal.title == "") {
                        errors[i].push("title");
                        count++;
                        $scope.form.data.goals[i].is_open = true;
                    }
                    if ( !goal.is_idp && goal.tags.length == 0) {
                        errors[i].push("tags");
                        count++;
                        $scope.form.data.goals[i].is_open = true;
                    }
                    if (!goal.is_idp && (goal.success_metric == null || goal.success_metric == "")) {
                        errors[i].push("success_metric");
                        count++;
                        $scope.form.data.goals[i].is_open = true;
                    }
                    if (goal.process == null || goal.process == "") {
                        errors[i].push("process");
                        count++;
                        $scope.form.data.goals[i].is_open = true;
                    }
                }

                return {
                    errors: errors,
                    count: count
                };
            },
            updateUserGoals: function (option) {
                $scope.loading = true;
                if ( option == 'pending' )
                {
                    $scope.saveAsDraftLoading = true;
                }
                else if ( option == 'running' )
                {
                    $scope.freezeGoalLoading = true;
                }
                else {
                    $scope.setGoalLoading = true;
                }
                Restangular.one("user-goal-items", $scope.userGoal.id)
                    .customPUT({
                        goals: $scope.form.data.goals,
                        status: option
                    })
                    .then(function (response) {
                        $scope.loading = false;
                        $scope.saveAsDraftLoading = false;
                        $scope.setGoalLoading = false;
                        $scope.freezeGoalLoading = false;
                        if ( option != 'pending' )
                            window.location.reload();
                    },
                    function(errors) {
                        $scope.loading = false;
                        $scope.saveAsDraftLoading = false;
                        $scope.setGoalLoading = false;
                        $scope.freezeGoalLoading = false;
                        $scope.error = true;
                        $scope.success = false;
                    }
                );
            }
        };

        $scope.search = {
            text: "",
            tags: []
        };
        $scope.GoalData = null;
        $scope.select2Options = {
            allowClear: true
        };

        Restangular.one("tags")
            .get()
            .then(function (response) {
                $scope.data.tags = response;
            });

        Goal.getGoals($scope.search);
        $scope.GoalData = Goal.data;

        $scope.$watch("GoalData.loaded", function () {
            if ($scope.GoalData.loaded == true) {
                $scope.data.goals = $scope.GoalData.goals;
            }
        });
        $scope.$watch("GoalData.userGoalItemsLoaded", function () {
            if ($scope.GoalData.userGoalItemsLoaded == true) {
                $scope.form.data.goals = $scope.GoalData.userGoalItems;
            }
        });

        $scope.filter = function () {
            Goal.getGoals($scope.search);
        };
        params = {
            start_date: moment($scope.userGoal.from_date).format(
                "YYYY-MM-DD"
            ),
            end_date: moment($scope.userGoal.to_date).format("YYYY-MM-DD")
        };

        Goal.getUserGoals($scope.userGoal.id, params);
        $scope.saveGoal = function (is_private) {
            var params = $scope.data.newGoal;
            Goal.saveGoal(params, is_private, false);
        };

        $scope.saveIdp = function () {
            var params = $scope.data.newIdp;
            Goal.saveGoal(params, true, true);
        };

        $scope.attachGoal = function (goal_id) {
            Goal.attachGoal(1, goal_id, false, false);
        };

        $scope.detachGoal = function (item_id) {
            Goal.detachGoal(item_id);
        };
        $scope.$watch("GoalData.userGoalItemsLoaded", function () {
            if ($scope.GoalData.userGoalItemsLoaded == true) {
                $scope.form.data.goals = $scope.GoalData.userGoalItems;
            }
        });

        $scope.updateUserGoals = function () {
            params = {
                user_goal_items: $scope.data.userGoalItems
            };
            Goal.updateUserGoals(params);
        };

        $scope.$watch("GoalData.newGoalAdded", function () {
            if ($scope.GoalData.newGoalAdded == true) {
                params = {
                    start_date: moment($scope.userGoal.from_date)
                        .add("330", "m")
                        .format("YYYY-MM-DD"),
                    end_date: moment($scope.userGoal.to_date)
                        .add("330", "m")
                        .format("YYYY-MM-DD")
                };
                $scope.data.newGoal = [];
                Goal.getUserGoals(1, params);
            }
        });
    }]);