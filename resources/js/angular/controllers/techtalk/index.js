angular.module('myApp').controller('techtalkIndexctrl',[
    "$scope","$uibModal","Restangular",function($scope,$uibModal,Restangular){

      $scope.model = {
        users : [],
        data: [],
        loading: true
      };
      Restangular.all("user")
      .getList({is_active : 1,pagination : 0})
      .then(
        function(response){
          $scope.model.users=response;
          console.log(response);
          // for(i=0;i<response.length;i++){
          //   $scope.userDetails.push(response[i]);
          //   // console.log($scope.userDetails);
          // }
        },
        function(error){
          $scope.model.loading=false;
          console.log(error);

        }
      );

      
      Restangular.all("techtalk")
      .getList()
      .then(
        function(response){
          $scope.model.loading=true;
          $scope.model.data=response;
          console.log(response);

          // for(i=0;i<response.length;i++){
          //   $scope.userDetails.push(response[i]);
          //   // console.log($scope.userDetails);
          // }
        },
        function(error){
          $scope.model.loading=false;
          console.log(error);

        }
      );
     


        $scope.addParticipants = function(){
          
          var modal = $uibModal.open({
            controller: "addTechtalkModalCtrl",
            windowClass: "bootstrap_wrap",
            templateUrl: "/views/techtalk/add-modal.html",
            backdrop: "static",
            size: "lg",
            resolve: {
              data: function() {
                return $scope.model.userList;
              }
            }
          });
          modal.result.then(
            function(result) {
              console.log('addTechtalkModalCtrl response', result);
              //window.location.reload();
            },
            function() {
              //cancel
            }
          );
          }

          $scope.viewParticipants=function(id){
            console.log("In the method");
            console.log("id"+id);
            var modal=$uibModal.open({
              controller:"showtechtalkModalCtrl",
              windowClass:"bootstrap_wrap",
              templateUrl:"/views/techtalk/show-modal.html",
              backdrop:"static",
              size:"lg",
              resolve:{
                data: function() {
                  return users;
                }
              }
              });
              modal.result.then(
                function(result) {
                },
                function() {
                }
              );
            
          }
          $scope.deleteParticipant=function(id){
            console.log(id);
            if(confirm("Are you sure want to delete this participant")){
              Restangular.one("techtalk",id)
            .remove()
            .then(
              function(response){
                    window.location.reload();
              },
              function(error){
                console.log(error);
              }
            )
            }
            
          }
          $scope.editParticipant=function(id){
            console.log("Index.js "+id);

            var modal = $uibModal.open({
              controller: "editTechtalkModelController",
              windowClass: "bootstrap_wrap",
              templateUrl: "/views/techtalk/edit-modal.html",
              backdrop: "static",
              size: "lg",
              resolve: {
                data: function() {
                  return {id:id };
                }
              }
            });
            modal.result.then(
              function(result) {
              },
              function() {
                //cancel
              }
            );
          }
    }
]);

