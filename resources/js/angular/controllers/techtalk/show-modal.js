
angular.module("myApp").controller("showtechtalkModalCtrl",[
    "$scope",
    "Restangular",
    "data",
    "$uibModalInstance",
    function($scope,Restangular,data,$uibModalInstance){
        // console.log(data);
        console.log("Into the show js"+data.id);

        $scope.model={
            data:[],
            loading:true
        };
       var techObj= Restangular.one("techtalk",data.id);
        techObj.get().then(
            function(response){
                $scope.model.data=response;
            },
            function(error){
                console.log(error);
            }
        );

        $scope.close = function() {
            console.log("in the Method")
            $uibModalInstance.close();
        };
}
]);