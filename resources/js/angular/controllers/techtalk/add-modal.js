angular.module("myApp").controller("addTechtalkModalCtrl", [
    "$scope",
    "Restangular",
    "data",
    "$uibModalInstance",
    function($scope, Restangular, data, $uibModalInstance) {
      $scope.model = {
        data : {
          employee_name:null,
          title : null,
          description : null,
        },
        onSubmit : function() {
          console.log('submit called', $scope.model.data);
          Restangular.all("techtalk")
            .post($scope.model.data)
            .then(
              function(response){
                $scope.model.loading=false;
                console.log(response);
                $uibModalInstance.close(response);
              },
              function(error){
                $scope.model.loading=false;
                console.log(error);

              }
            );
        }

      }

      // $scope.user={
      //   emp_name:'',
      //   title:'',
      //   description:'',
      //   event_date:''

      // }

      $scope.onSubmit=function(){
        // console.log($scope.user);
        console.log($scope.emp_name);
        var params = {
          name:$scope.emp_name
        };   
        console.log(params);
        Restangular.all("techtalk")
        .post(params)
        .then(
          function(response){
            $scope.model.loading=false;
            console.log(response);

          },
          function(error){
            $scope.model.loading=false;
            console.log(error);

          }
        );
      } 
      $scope.close=function(){
        $uibModalInstance.close();
       }
       $scope.cancel=function(){
        $uibModalInstance.dismiss();
       } 
    }
  ]);
