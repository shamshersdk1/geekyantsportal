angular.module("myApp").controller("bonusDueCtrl", [
  "$scope",
  "Restangular",
  "$uibModal",
  function($scope, Restangular, $uibModal) {
    $scope.data = window.due_bonues;
    $scope.user = window.userObj;
    // console.log("userObj", window.userObj);
    $scope.form = {
      amount: 0,
      draft_amount: 0,
      paid_amount: 0,
      succuss: false,
      errorMessage: null,
      submitted: false,
      amount: null,
      updateAmount: function() {
        // console.log("inside updateAmount");
        $scope.form.amount = 0;
        $scope.form.draft_amount = 0;
        $scope.form.paid_amount = 0;
        angular.forEach($scope.data, function(item) {
          // console.log("item->", item);
          angular.forEach(item.bonuses, function(bonus) {
            // console.log("bonus->", bonus);
            if (!isNaN(parseFloat(bonus.draft_amount))) {
              // console.log("draft_amount", bonus.draft_amount);
              $scope.form.draft_amount += parseFloat(bonus.draft_amount);
            }
            if (bonus.transaction_id != null) {
              bonus.payment_date = bonus.paid_at;
              if (!isNaN(parseFloat(bonus.amount))) {
                $scope.form.paid_amount += parseFloat(bonus.amount);
              }
            }
            if (!isNaN(parseFloat(bonus.amount))) {
              $scope.form.amount += parseFloat(bonus.amount);
            }
          });
        });
      },
      update: function(bonus) {
        var bonusObj = Restangular.one("bonus", bonus.id);
        bonus.loading = true;
        bonus.succuss = false;
        bonus.error = false;
        bonus.submitted = false;
        bonusObj.amount = bonus.amount;

        // if(isNaN(parseFloat(bonus.amount)))
        //   bonusObj.amount = 0;
        if (bonusObj.transaction_id == null) {
          bonusObj.put().then(
            function(response) {
              bonus.loading = false;
              bonus.succuss = true;
              bonus.submitted = true;
              $scope.form.loading = true;

              $scope.form.updateAmount();
            },
            function(error) {
              console.log(error);
              $scope.form.loading = false;
              bonus.error = true;
            }
          );
        }
      },
      approvePayment: function(bonus) {
        bonus.submitted = true;

        Restangular.one("bonus/" + bonus.id + "/approve-payment")
          .get()
          .then(
            function(response) {
              // console.log("in resp", response);
              // bonus.transaction_id = response.transaction_id;
              bonus.paid_at = response.paid_at;
              bonus.payment_approved = true;
              bonus.amount = response.amount;
              $scope.form.loading = true;
              //bonus.payment_by = response.paid_at;
              $scope.form.updateAmount();
            },
            function(error) {
              $scope.form.errorMessage = error.data.message;
            }
          );
      },
      addBonus: function() {
        // console.log("addBonus called");
        var modal = $uibModal.open({
          controller: "addBonusRequestModalCtrl",
          windowClass: "bootstrap_wrap",
          templateUrl: "/views/bonus-request/add.html",
          backdrop: "static",
          size: "md",
          resolve: {
            data: function() {
              return {
                user_id: $scope.user.id,
                name: $scope.user.name
              };
            }
          }
        });
        modal.result.then(
          function(result) {
            //window.location.reload();
            $scope.data = result;
            $scope.form.updateAmount();
          },
          function() {
            //cancel
          }
        );
      }

      // approvePayment: function(bonus) {
      //   console.log('ahgsdfhsc');
      //   bonus.submitting = true;

      //   Restangular.one("bonus/" + bonus.id + "/approve-payment")
      //     .get()
      //     .then(function(response) {
      //       bonus.payment_date = response.paid_at;
      //       bonus.payment_approved = true;
      //       //bonus.payment_by = response.paid_at;
      //     });
      // }
    };
    $scope.form.updateAmount();
  }
]);
