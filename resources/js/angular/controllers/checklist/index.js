angular.module('myApp')
    .controller('checklistCtrl',["$scope", "Restangular", "$http",function($scope, Restangular, $http) {
        $scope.user_mismatch_list = [];
        $scope.unassigned_channels = [];
        $scope.free_projects = [];
        $scope.user_loading = false;
        $scope.channels_loading = false;
        $scope.projects_loading = false;
        $scope.getUserMismatchList = function() {
                $scope.user_loading = true
                Restangular.all('user-mismatch-list').getList().then(
                    function(response) {
                        $scope.user_loading = false;
                        $scope.user_mismatch_list = response;
                    },
                    function(errors) { $scope.user_loading = false; }
                );
            }
            //   $http({
            //     method: 'GET',
            //     url: 'http://geekyants-portal.local.geekydev.com/api/v1/user-mismatch-list'
            //   }).then(
            //     function successCallback(response) {
            //       $scope.that = $scope;
            //       console.log(response);
            //       $scope.contents = Object.keys(response.data).map(function (key) {
            //         $scope.user_mismatch_list = response.data.result.mismatch_details;
            //         return response.data[key];
            //       });
            //       for (var i = 0; i < $scope.contents.length; i++) {
            //         $scope[$scope.contents[i]] = $scope.contents[i];
            //       }
            //     },
            //     function errorCallback(response) { }
            //     );
            //  }

        $scope.getUnassignedChannels = function() {
            $scope.channels_loading = true;
            Restangular.all('channels-without-projects').getList().then(
                function(response) {
                    $scope.channels_loading = false;
                    $scope.unassigned_channels = response;
                },
                function(errors) { $scope.channels_loading = false; }
            );
        }

        $scope.getFreeProjects = function() {
            $scope.projects_loading = true;
            Restangular.all('project-without-resources').getList().then(
                function(response) {
                    $scope.projects_loading = false;
                    $scope.free_projects = response;
                },
                function(errors) { $scope.projects_loading = false; }
            );
        }
    }]);