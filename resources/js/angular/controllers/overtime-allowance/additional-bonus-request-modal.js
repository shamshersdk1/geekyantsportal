angular.module("myApp").controller("additionalSelectionModalCtrl", [
  "$scope",
  "data",
  "$uibModalInstance",
  "Restangular",
  function($scope, data, $uibModalInstance,Restangular) {

    $scope.data = data;
    var params = {
      date : data.date,
      user_id : data.user_id
    };
    let preSelectedIds = [];
    console.log('KEY',data );  
    
    $scope.form = {
      loading : false,
      model : {
        project_id: null,
        project_ids: [],
        duration: '8',
        redeem_type: 'encash',
        date : data.date
      },
      projects : [],
      no_timesheet_error: false,
      error: false,
      projects : [],
      checkForTimesheet : function () {
        if ( $scope.form.model.selected_projects.length == 0 )
        {
          $scope.form.model.no_timesheet_error = true;
        }
      },
      closeModal : function() {
        $uibModalInstance.close($scope.model);
      },
      cancelModal : function(){
        $uibModalInstance.dismiss();
      },
      submit : function(form) {
        if(!form.$valid) {
          return false;
        }
        var params = $scope.form.model;
        Restangular.all('additional-work-days-bonus').post(params).then(
          function (response) {
            $uibModalInstance.close(response);
              // response.redeem_info = JSON.parse(response.redeem_type);
              // $scope.data.days[index].bonus.push(response);
              // if ( response.bonus_request_projects && response.bonus_request_projects.length > 0 )
              //   {
              //     tempProjectIds = [];
              //     for ( j = 0; j < response.bonus_request_projects.length; j++ )
              //     {
              //       tempProjectIds.push(response.bonus_request_projects[j].project_id);
              //     }
              //     $scope.model.selected_projects[response.id] = tempProjectIds;
              //   }
              // $scope.data.days[index].additionalWDActionStatus = 'success';
          },
          function (error) {
              console.log(error);
              $scope.form.error = true;
              $scope.form.error_message = error.data.message;
              //$scope.data.days[index].additionalWDActionStatus = 'error';
          }
        );
        
        
        // $scope.form.model.error = false;
        // if ($scope.model.selected_projects.length == 0)
        // {
        //   $scope.form.model.error = true;
        //   return;
        // }

        //$uibModalInstance.close($scope.model);
      }
    };
    $scope.form.loading = true;
    Restangular.all('additional-work-days-bonus/get-timesheet-detail').post(params).then(function(response){
        $scope.form.projects = response;
        $scope.form.loading = false;
    }, function(error){

    });
    
    //$scope.form.model.checkForTimesheet();
    // Restangular.all('new-timesheet/get-timesheet-project').getList(params).then(function(response){
    //   $scope.form.projects = response;
    // }, function(error){});

    $scope.save = function() {
      $scope.form.model.error = false;
      if ($scope.model.selected_projects.length == 0)
      {
        $scope.form.model.error = true;
        return;
      }
      $uibModalInstance.close($scope.model);
    };
    $scope.cancel = function() {
      $uibModalInstance.dismiss();
    };
  }
]);
