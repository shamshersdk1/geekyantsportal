angular.module("myApp").controller("overtimeallwanceCtrl", [
  "$scope",
  "Restangular",
  "$uibModal",
  function($scope, Restangular, $uibModal) {
    // $scope.data = JSON.parse(window.data);
    $scope.data = [];
    let date = new Date();
    let month = date.getMonth() + 1;
    let year = date.getFullYear();

    $scope.model = {
      loading: false,
      data: [],
      projects: [],
      calender: [],
      months: JSON.parse(window.months),
      selected_projects: [], //To determine currently selected project
      selectedMonthYear: "", //To determine currently selected month
      onsite_allowance_id: null,
      error: false,
      select_projects: function(bonus_id) {
        var params = {
          bonus_request_id: bonus_id,
          projects: $scope.model.selected_projects[bonus_id]
        };

        Restangular.all("bonus-request-projects")
          .post(params)
          .then(
            function(response) {
              console.log(response);
            },
            function(errors) {
              console.log(errors);
            }
          );
      },
      onOnSiteCheckBoxClick: function(key, index) {
        $scope.noRecordsFlag = false;
        if (key) {
          $scope.data.days[index].onSiteActionStatus = "progress";
          $scope.data.days[index].bonus.onsite_bonus_submitted = true;
          if (key.bonus.onsite_bonus) {
            var id = key.bonus.onsite_bonus.id;
            $scope.data.days[index].bonus.onsite_bonus_loading = true;

            Restangular.one("onsite-bonus", id)
              .remove()
              .then(
                function(response) {
                  $scope.data.days[index].bonus.onsite_bonus = null;
                    $scope.data.days[index].bonus.onsite_bonus_loading = false;
                    $scope.data.days[index].bonus.onsite_bonus_status = true;
                  },
                  function (errors) {
                      $scope.data.days[index].bonus.onsite_bonus_status = false;
                      $scope.data.days[index].bonus.onsite_bonus_loading = false;
                   }
              );
          } else {
            var modal = $uibModal.open({
              controller: "projectSelectionModalCtrl",
              windowClass: "bootstrap_wrap",
              templateUrl: "/views/bonus-request/onsite-project-select.html",
              backdrop: "static",
              size: "md",
              resolve: {
                data: function() {
                  return key;
                }
              }
            });
            modal.result.then(
              function(response) {
                $scope.data.days[index].bonus.onsite_bonus = response;
                $scope.data.days[index].bonus.onsite_bonus_status = true;
                $scope.data.days[index].bonus.onsite_bonus_submitted = true;
              },
              function() {
                $scope.data.days[index].additionalWDActionStatus = 'error';
                $scope.data.days[index].bonus.onsite_bonus_status = false;
                $scope.data.days[index].bonus.onsite_bonus_loading = false;
              }
            );
            $scope.data.days[index].warning_message = null;
          }
        }
      },

      onAdditionalWorkCheckBoxClick: function(key, index) {
        $scope.noRecordsFlag = false;
        if (key) {
          $scope.data.days[index].bonus.additional_bonus_submitted = true;
          if (key.bonus.additional_bonus)  
          {
            var id = key.bonus.additional_bonus.id;
            $scope.data.days[index].bonus.additional_bonus_loading = true;
            Restangular.one("additional-work-days-bonus", id)
              .remove()
              .then(
                  function (response) {
                    $scope.data.days[index].bonus.additional_bonus = null;
                    $scope.data.days[index].bonus.additional_bonus_loading = false;
                    $scope.data.days[index].bonus.additional_bonus_status = true;
                  },
                  function (errors) {
                      $scope.data.days[index].additionalWDActionStatus = 'error';
                      $scope.data.days[index].bonus.additional_bonus_status = false;
                      $scope.data.days[index].bonus.additional_bonus_loading = false;
                  }
              );
          } else {
            var additionalModal = $uibModal.open({
              controller: "additionalSelectionModalCtrl",
              windowClass: "bootstrap_wrap",
              templateUrl:
                "/views/bonus-request/additional-project-select.html",
              backdrop: "static",
              size: "md",
              resolve: {
                data: function () {
                  return key;
                }
              }
            });
            additionalModal.result.then(
              function (result) {
                $scope.data.days[index].bonus.additional_bonus = result;
                $scope.data.days[index].bonus.additional_bonus_status = true;
                $scope.data.days[index].bonus.additional_bonus_submitted = true;
              },
              function () {
                $scope.data.days[index].bonus.additional_bonus = false;
              }
            );
          }
        }
      },
      previousMonth: function() {
        month = month - 1;
        if (month == 0) {
          month = 12;
          year = year - 1;
        }
        getCalender(month, year);
      },
      nextMonth: function() {
        month = month + 1;
        if (month == 13) {
          month = 1;
          year = year + 1;
        }
        getCalender(month, year);
      }
    };
    let getProjects = function() {
      var params = {
        pagination: 0
      };
      Restangular.all("project")
        .getList(params)
        .then(function(projectList) {
          // console.log("ProjectList-->", projectList);
          $scope.model.projects = projectList;
        })
        .catch(function(projectListErr) {
          console.log("ProjectList failed", projectListErr);
        });
    };
    getProjects();
    let getCalender = function(month, year) {
      $scope.model.loading = true;
      Restangular.one("bonus/get-calender/"+month+"/"+year).get()
      .then(
          function (response) {
            $scope.data = response;
            $scope.model.loading = false;
            $scope.noRecordsFlag = true;
            $scope.data.days.map( key => {
              if ( key.bonus && key.bonus.length < 1 )
              {
                key.on_site = false;
                key.additional = false;
              }
              else if ( key.bonus && key.bonus.length > 0 ) {
                  $scope.noRecordsFlag = false;
                  key.on_site = false;
                  key.additional = false;
                  for ( i = 0; i < key.bonus.length; i++ )
                  {
                    if ( key.bonus[i].type == 'onsite' && key.bonus[i].status != 'cancelled' ) {
                      key.on_site = true;
                      if ( key.bonus[i].status == 'approved' )
                      {
                        key.on_site_approved = true;
                      }
                    } else if ( key.bonus[i].type != 'onsite' &&  key.bonus[i].status != 'cancelled' ) {
                      key.additional = true;
                      key.bonus[i].redeem_info = JSON.parse(key.bonus[i].redeem_type);
                      if ( key.bonus[i].status == 'approved' )
                      {
                        key.additional_approved = true;
                      }
                    }
                    if ( key.bonus[i].bonus_request_projects && key.bonus[i].bonus_request_projects.length > 0 )
                    {
                      tempProjectIds = [];
                      for ( j = 0; j < key.bonus[i].bonus_request_projects.length; j++ )
                      {
                        tempProjectIds.push(key.bonus[i].bonus_request_projects[j].project_id);
                      }
                      $scope.model.selected_projects[key.bonus[i].id] = tempProjectIds;
                    }
                    
                  }
                }
            });
          },
          function (errors) {
              console.log(errors);
              $scope.model.loading = false;
          }
      );
    };

    getCalender(month, year);
    $scope.model.selectedMonthYear = $scope.model.months[0];
    console.log($scope.model.selectedMonthYear);
  }
]);
