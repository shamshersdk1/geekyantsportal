angular.module("myApp").controller("projectSelectionModalCtrl", [
  "$scope",
  "Restangular",
  "data",
  "$uibModalInstance",
  function($scope, Restangular, data, $uibModalInstance) {
    $scope.data = data;
    var params = { date: data.date };

    // get the user timelog entries for the date passed
    Restangular.all("user-timesheet")
      .getList(params)
      .then(
        function(response) {
          $scope.form.user_timesheet = response;
          $scope.form.loading = false;
        },
        function(error) {
          $scope.form.loading = false;
        }
      );

    $scope.form = {
      model: {
        date: data.date,
        onsite_allowance_id: null,
        is_holiday: data.is_holiday,
        is_weekend: data.is_weekend,
        on_leave: data.on_leave,
        type: "onsite"
      },
      onsite_allowance: data.onsite_allowance,
      loading: true,
      user_timesheet: [],
      submit: function($form) {
        $scope.form.submitted = true;
        if (!$form.$valid) {
          $scope.form.error = true;
          $scope.form.submitted = false;
          return false;
        }
        var params = $scope.form.model;
        $scope.form.loading = true;
        Restangular.all("onsite-bonus")
          .post(params)
          .then(
            function(response) {
              $scope.form.loading = false;
              $uibModalInstance.close(response);

              // $scope.data.days[index].bonus.push(response);
              // if ( response.bonus_request_projects && response.bonus_request_projects.length > 0 )
              // {
              //   tempProjectIds = [];
              //   for ( j = 0; j < response.bonus_request_projects.length; j++ )
              //   {
              //     tempProjectIds.push(response.bonus_request_projects[j].project_id);
              //   }
              //   $scope.model.selected_projects[response.id] = tempProjectIds;
              // }
              // $scope.data.days[index].onSiteActionStatus = 'success';
            },
            function(errors) {
              console.log(errors);
              // $scope.data.days[index].onSiteActionStatus = 'error';
              $scope.form.loading = false;
            }
          );
      },
      cancel: function() {
        $uibModalInstance.dismiss();
      }
    };
    if (data.onsite_allowance.length == 1) {
      console.log("onsite_allowance_id", data.onsite_allowance[0].id);
      $scope.form.model.onsite_allowance_id = data.onsite_allowance[0].id;
    }
    $scope.model = {
      onsite_allowance_id: null
    };
    $scope.save = function() {
      $uibModalInstance.close($scope.model);
    };
    $scope.cancel = function() {
      $uibModalInstance.dismiss();
    };
  }
]);
