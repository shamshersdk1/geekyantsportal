angular.module("myApp").controller("createInsuranceModalCtrl", [
    "$scope",
    "Restangular",
    "data",
    "$uibModalInstance",
    function($scope, Restangular, data, $uibModalInstance) {
        $scope.options = {
            formatYear: 'yy',
            maxDate: new Date(2020, 5, 22),
            minDate: new Date(),
        };

        $scope.form = {
            error : false,
            error_message : null,
            user : {
                data : [],
                loading : false
            },
            asset : {
                data : [],
                loading : false
            },
            users : [],
            assets : [],
            startDate : {
                datepickerOption : {
                    options: {
                      applyClass: "btn-success",
                      singleDatePicker: true,
                      locale: {
                        applyLabel: "Apply",
                        fromLabel: "From",
                        // format: "YYYY-MM-DD", //will give you 2017-01-06
                        format: "D-MMM-YY", //will give you 6-Jan-17
                        // format: "D-MMMM-YY", //will give you 6-January-17
                        toLabel: "To",
                        cancelLabel: "Cancel",
                        customRangeLabel: "Custom range"
                      }
                    }
                },
            },
            insurance_policies : [],
            model : {
                type : null,
                insurance_policy_id : null,
                insurable_id : null,
                insurable_type : null
            },
            sub_group : {
                name : null,
                premium_amount : null
            },
            select_policy: function(data) {
                $scope.form.selected_policy = data;
                $scope.form.model.insurance_policy_id = $scope.form.selected_policy.id;

                //$scope.getInsura
              },
          select_user: function(data) {
            $scope.form.selected_user = data;
            $scope.form.model.insurable_id = data.id;
            $scope.form.model.insurable_type = 'App\\Models\\User';
            //$scope.getInsura
          },
          select_asset: function(data) {
            $scope.form.selected_asset = data;
            $scope.form.model.insurable_id = data.id;
            $scope.form.model.insurable_type = 'App\\Models\\Admin\\Asset';

            //$scope.getInsura
          },
            addSubGroup : function() {
                $scope.form.model.sub_groups.push($scope.form.sub_group);
                //$scope.form.itemError = false;
                // $scope.form.item = {
                //   name: null,
                //   unit_price: null,
                //   quantity: null,
                //   total: null
                // };
            },
            init: function() {
                $scope.form.loading = true;
                Restangular.all("insurance-policy")
                  .getList({'pagination': 0})
                  .then(
                    function(response) {
                      $scope.form.loading = false;
                      $scope.form.insurance_policies = response;
                    },
                    function(error) {
                      console.log(error);
                    }
                  );
            },
            submit : function($form) {
                console.log('submit called');
                $scope.form.submitted = true;
                $scope.form.itemError = false;
                // if (
                //   $scope.form.model.purchase_items[0].name == null ||
                //   $scope.form.model.purchase_items[0].unit_price == null ||
                //   $scope.form.model.purchase_items[0].quantity == null
                // ) {
                //   $scope.form.itemError = true;
                // }
                if (!$form.$valid) {
                    console.log('INVALID FORM');
                  $scope.form.error = true;
                  $scope.form.submitted = false;
                  return false;
                }
                console.log('VALID FORM');
                Restangular.all("insurance")
                  .post($scope.form.model)
                  .then(
                    function(response) {
                        $uibModalInstance.close(response);
                    },
                    function(error) {
                        $scope.form.error = true;
                        $scope.form.error_message = error.data.message;
                      console.log(error.data);
                      //$uibModalInstance.dismiss();
                    }
                  );

            },
            cancel : function() {
                $uibModalInstance.dismiss();
            },
            close : function() {
                $uibModalInstance.close();
            }

        }
        $scope.form.init();
        $scope.start_date = {
            options: {
              applyClass: "btn-success",
              singleDatePicker: true,
              locale: {
                applyLabel: "Apply",
                fromLabel: "From",
                // format: "YYYY-MM-DD", //will give you 2017-01-06
                format: "D-MMM-YY", //will give you 6-Jan-17
                // format: "D-MMMM-YY", //will give you 6-January-17
                toLabel: "To",
                cancelLabel: "Cancel",
                customRangeLabel: "Custom range"
              }
            }
          };
        $scope.close = function() {
            $uibModalInstance.close();
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss();
        };
        $scope.$watch('form.selected_policy.applicable_for', function() {
            if($scope.form.selected_policy && $scope.form.selected_policy.applicable_for == 'user') {
                $scope.form.user.loading =  true;
                Restangular.all('user').getList({pagination : 0}).then(function(response){
                    $scope.form.user.data =  response;
                    $scope.form.user.loading =  false;
                }, function(error){
                    $scope.form.user.loading =  false;
                    console.log('ERR', error);
                })
            }else if($scope.form.selected_policy && $scope.form.selected_policy.applicable_for == 'asset') {
                $scope.form.asset.loading =  true;
                Restangular.all('asset').getList({pagination : 0}).then(function(response){
                    $scope.form.asset.loading =  false;
                    $scope.form.asset.data =  response;
                }, function(error){
                    $scope.form.asset.loading =  false;
                    console.log('ERR', error);
                })
            }
        });
    }

  ]);
  