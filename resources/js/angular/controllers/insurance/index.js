angular.module('myApp')
    .controller('insuranceIndexController',["$scope", "Restangular", "$uibModal" ,"$route", function ($scope, Restangular, $uibModal, $route) {
      $scope.model = {
        data : [],
        loading : true,
        selectAll : function(){
          var items = [];
        },
        
        getData : function(status = ''){
          var params = {pagination : 0,'parent_id' : null};
          
          Restangular.all('insurance').getList(params).then(function(response){
            $scope.model.data = response;
            $scope.model.loading = false;
          }, function(error){
            $scope.model.loading = false;
          });    
        },
        addInsurance : function() {
          var modal = $uibModal.open({
          controller: "createInsuranceModalCtrl",
          windowClass: "bootstrap_wrap",
          templateUrl: "/views/insurance/create.html",
          backdrop: "static",
          size: "lg",
          resolve: {
            data: function () {
              return {};
            }
          }
        });

        modal.result.then(
          function (result) {
            //window.location.reload();
            $route.reload();
          },
          function () {
            //cancel
          }
        );
        },
        addSubGroup : function() {
          console.log('addSubGroup called');

        },
        view :function(id) {
          var modal = $uibModal.open({
          controller: "createInsurancePolicyModalCtrl",
          windowClass: "bootstrap_wrap",
          templateUrl: "/views/insurance/create.html",
          backdrop: "static",
          size: "lg",
          resolve: {
            data: function () {
              return {};
            }
          }
        });

        modal.result.then(
          function (result) {
            //window.location.reload();
          },
          function () {
            //cancel
          }
        );

        },
        edit :function(id) {
          var modal = $uibModal.open({
          controller: "editInsurancePolicyModalCtrl",
          windowClass: "bootstrap_wrap",
          templateUrl: "/views/insurance/create.html",
          backdrop: "static",
          size: "lg",
          resolve: {
            data: function () {
              return { id : id};
            }
          }
        });

        modal.result.then(
          function (result) {
            //window.location.reload();
          },
          function () {
            //cancel
          }
        );
        },
      },
     
      $scope.model.getData();
}]);