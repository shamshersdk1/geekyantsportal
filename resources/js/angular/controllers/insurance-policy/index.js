angular.module('myApp')
    .controller('insurancePolicyIndexController',["$scope", "Restangular", "$uibModal" , function ($scope, Restangular, $uibModal) {
      $scope.model = {
        data : [],
        loading : true,
        selectAll : function(){
          var items = [];
        },
        
        getData : function(status = ''){
          var params = {status : status,pagination : 0};
          Restangular.all('insurance-policy').getList(params).then(function(response){
            $scope.model.data = response;
            $scope.model.loading = false;
          }, function(error){
            $scope.model.loading = false;
          });    
        },
        addNewPolicy : function() {
          var modal = $uibModal.open({
          controller: "createInsurancePolicyModalCtrl",
          windowClass: "bootstrap_wrap",
          templateUrl: "/views/insurance-policy/create.html",
          backdrop: "static",
          size: "lg",
          resolve: {
            data: function () {
              return {};
            }
          }
        });

        modal.result.then(
          function (result) {
            window.location.reload();
          },
          function () {
            //cancel
          }
        );
        },
        addSubGroup : function() {
          console.log('addSubGroup called');

        },
        view :function(id) {
          var modal = $uibModal.open({
          controller: "createInsurancePolicyModalCtrl",
          windowClass: "bootstrap_wrap",
          templateUrl: "/views/insurance-policy/create.html",
          backdrop: "static",
          size: "lg",
          resolve: {
            data: function () {
              return {};
            }
          }
        });

        modal.result.then(
          function (result) {
            window.location.reload();
          },
          function () {
            //cancel
          }
        );

        },
        edit :function(id) {
          var modal = $uibModal.open({
          controller: "editInsurancePolicyModalCtrl",
          windowClass: "bootstrap_wrap",
          templateUrl: "/views/insurance-policy/create.html",
          backdrop: "static",
          size: "lg",
          resolve: {
            data: function () {
              return { id : id};
            }
          }
        });

        modal.result.then(
          function (result) {
            window.location.reload();
          },
          function () {
            //cancel
          }
        );
        },
      },
     
      $scope.model.getData();
}]);