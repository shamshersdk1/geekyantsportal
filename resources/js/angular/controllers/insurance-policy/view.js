angular.module("myApp").controller("viewInsurancePolicyCtrl", [
  "$scope",
  "Restangular",
  "$uibModal",
  function($scope, Restangular, $uibModal) {
    var id = window.policyId;

    var vendor_services = [];
    $scope.form = {
      loading: false,
      error: false,
      selected_user: [],
      error_message: null,
      user: {
        data: [],
        loading: false
      },
      asset: {
        data: [],
        loading: false
      },
      data: {},
      model: {
        insurance_policy_id: null,
        insurable_id: null,
        insurable_type: null
      },
      select_user: function(data, $index) {
        console.log("D", data, $index);
        $scope.form.data.sub_groups[$index].selected_user = data;
        $scope.form.data.sub_groups[$index].insurable_id = data.id;
        $scope.form.data.sub_groups[$index].insurable_type =
          "App\\Models\\User";
        //$scope.getInsura
      },
      select_asset: function(data) {
        $scope.form.selected_asset = data;
        $scope.form.model.insurable_id = data.id;
        $scope.form.model.insurable_type = "App\\Models\\Admin\\Asset";

        //$scope.getInsura
      },
      submit: function($form, $index) {
        $scope.form.data.sub_groups[$index].submitted = true;
        $scope.form.data.sub_groups[$index].success = false;
        $scope.form.data.sub_groups[$index].error = false;
        $scope.form.data.sub_groups[$index].error_message = null;
        // if (
        //   $scope.form.model.purchase_items[0].name == null ||
        //   $scope.form.model.purchase_items[0].unit_price == null ||
        //   $scope.form.model.purchase_items[0].quantity == null
        // ) {
        //   $scope.form.itemError = true;
        // }
        if (!$form.$valid) {
          $scope.form.data.sub_groups[$index].error = true;
          $scope.form.data.sub_groups[$index].submitted = false;
          return false;
        }
        var params = {
          insurance_policy_id: $scope.form.data.sub_groups[$index].id,
          insurable_id: $scope.form.data.sub_groups[$index].insurable_id,
          insurable_type: $scope.form.data.sub_groups[$index].insurable_type
        };

        // console.log('$scope.form.data.sub_groups[$index]', $scope.form.data.sub_groups[$index]);
        // return false;
        Restangular.all("insurance")
          .post(params)
          .then(
            function(response) {
              //$scope.form.data.insurances.unshift(response);
              $scope.form.data.sub_groups[$index].insurances.push(response);
              $scope.form.data.sub_groups[$index].selected_asset = null;
              $scope.form.data.sub_groups[$index].selected_user = null;
              $scope.form.data.sub_groups[$index].message =
                "Added successfully";
              $scope.form.data.sub_groups[$index].success = true;
            },
            function(error) {
              $scope.form.data.sub_groups[$index].error = true;
              $scope.form.data.sub_groups[$index].error_message =
                error.data.message;
            }
          );
      },
      markAsClose: function(index) {
        if (!confirm("Are you sure you want to close this record?"))
          return false;

        var item = $scope.form.data.insurances[index];
        var obj = Restangular.one("insurance", item.id);
        obj.status = "closed";
        obj.put().then(
          function(response) {
            $scope.form.data.insurances[index] = response;
          },
          function(error) {
            console.log("ERR", error);
          }
        );
        //console.log('markAsClose called',index);
      },
      deleteRecord: function(index, subIndex) {
        if (!confirm("Are you sure you want to delete this record?"))
          return false;

        var item = $scope.form.data.sub_groups[index].insurances[subIndex];
        // console.log("deleteRecord called", index, subIndex, item);
        // return false;

        var response = Restangular.one("insurance", item.id)
          .remove()
          .then(
            function(response) {
              $scope.form.data.sub_groups[index].insurances.splice(subIndex, 1);
            },
            function(error) {
              console.log("ERR", error);
            }
          );
      },
      edit: function($index) {
        var id = $scope.form.data.sub_groups[$index].id;
        var modal = $uibModal.open({
          controller: "editInsurancePolicyModalCtrl",
          windowClass: "bootstrap_wrap",
          templateUrl: "/views/insurance-policy/create.html",
          backdrop: "static",
          size: "lg",
          resolve: {
            data: function() {
              return { id: id };
            }
          }
        });
        modal.result.then(
          function(result) {
            $scope.form.data.sub_groups[$index] = result;
          },
          function() {
            //cancel
          }
        );
      },
      downloadReport: function($index, $subIndex) {
        //var users = $scope.form.data.sub_groups[$index].insurances;
        var users = $scope.form.data.sub_groups[$index].insurances;
        console.log("downloadReport", $index, users);
        //return false;
        var name = $scope.form.data.sub_groups[$index].name;
        var policyNumber = $scope.form.data.sub_groups[$index].policy_number;
        var coverageAmount =
          $scope.form.data.sub_groups[$index].coverage_amount;
        var premiumAmount = $scope.form.data.sub_groups[$index].premium_amount;
        var type = $scope.form.data.sub_groups[$index].applicable_for;
        var data = []; //[{'Insurance Name','Policy Number','Coverage','Name','Employee ID','Amount'}];
        if (type == "user") {
          angular.forEach(users, function(res) {
            data.push({
              InsuranceName: name,
              PolicyNumber: policyNumber,
              Coverage: coverageAmount,
              EmployeeName: res.insurable ? res.insurable.name : null,
              EID: res.insurable ? res.insurable.employee_id : null,
              "Premium(per/month)": premiumAmount
            });
          });
        } else {
          angular.forEach(users, function(res) {
            data.push({
              InsuranceName: name,
              PolicyNumber: policyNumber,
              Coverage: coverageAmount,
              Name: res.insurable.name,
              "Premium(per/month)": premiumAmount
            });
          });
        }

        var a = document.createElement("a");
        var csv = Papa.unparse(data);
        if (window.navigator.msSaveOrOpenBlob) {
          var blob = new Blob([decodeURIComponent(encodeURI(json_pre))], {
            type: "text/csv;charset=utf-8;"
          });
          navigator.msSaveBlob(blob, name + ".csv");
        } else {
          a.href = "data:attachment/csv;charset=utf-8," + encodeURI(csv);
          a.target = "_blank";
          a.download = name + ".csv";
          document.body.appendChild(a);
          a.click();
        }
        //}

        // console.log('ID', id);
        // Restangular.one('insurance-policy/download', id).get().then(function(response){
        //     //$scope.form.data.insurances.splice(index, 1);
        //     console.log('DDD', response);
        // }, function(error){
        //   console.log('ERR', error);
        // })
      }
    };
    $scope.form.loading = true;

    var vendorObj = Restangular.one("insurance-policy", id);
    vendorObj.get().then(
      function(response) {
        $scope.form.data = response;
        $scope.form.model.insurance_policy_id = response.id;
        $scope.form.loading = false;
      },
      function(error) {
        console.log("err", error);
      }
    );
    $scope.filteredDate = function(input_date) {
      var formatted_date = new Date(input_date);
      return formatted_date;
    };
    // Get existing Services name
    $scope.close = function() {
      $uibModalInstance.close();
    };

    $scope.cancel = function() {
      $uibModalInstance.dismiss();
    };

    $scope.$watch("form.data.applicable_for", function() {
      if ($scope.form.data && $scope.form.data.applicable_for == "user") {
        $scope.form.user.loading = true;
        Restangular.all("user")
          .getList({ is_active: 1, pagination: 0 })
          .then(
            function(response) {
              $scope.form.user.data = response;
              $scope.form.user.loading = false;
            },
            function(error) {
              $scope.form.user.loading = false;
              console.log("ERR", error);
            }
          );
      } else if (
        $scope.form.data &&
        $scope.form.data.applicable_for == "asset"
      ) {
        $scope.form.asset.loading = true;
        Restangular.all("asset")
          .getList({ pagination: 0 })
          .then(
            function(response) {
              $scope.form.asset.loading = false;
              $scope.form.asset.data = response;
            },
            function(error) {
              $scope.form.asset.loading = false;
              console.log("ERR", error);
            }
          );
      }
    });
  }
]);
