angular.module("myApp").controller("editInsurancePolicyModalCtrl", [
    "$scope",
    "Restangular",
    "data",
    "$uibModalInstance",
    "$q",
    function($scope, Restangular, data, $uibModalInstance, $q) {
        $scope.existingServices = [];
        var id = data.id;
        var insurancePolicy = Restangular.one("insurance-policy", id);

        $scope.form = {
            policies: [],
            startDate : {
                datepickerOption : {
                    options: {
                      applyClass: "btn-success",
                      singleDatePicker: true,
                      locale: {
                        applyLabel: "Apply",
                        fromLabel: "From",
                        // format: "YYYY-MM-DD", //will give you 2017-01-06
                        format: "D-MMM-YY", //will give you 6-Jan-17
                        // format: "D-MMMM-YY", //will give you 6-January-17
                        toLabel: "To",
                        cancelLabel: "Cancel",
                        customRangeLabel: "Custom range"
                      }

                    }
                },
            },
            vendors : [],

            model : {
                id : null,
                vendor_id : null,
                name : null,
                policy_number : null,
                description : null,
                start_date : false,
                end_date : false,
                coverage_amount : null,
                amount_paid : null,
                premium_amount : null,
                applicable_for : 'user',
                accessible_to : 'group',
                parent_id : false,
                sub_group : false,
                has_sub_group : false,
                sub_groups : []
            },
            sub_group : {
                name : null,
                premium_amount : null
            },
            select_vendor: function(data) {
                console.log('VENDOR ID',data.id);
                //$scope.form.selected_vendor = data;
                $scope.form.model.vendor_id = data.id;
              },
            select_policy: function(data) {
                $scope.form.selected_policy = data;
                $scope.form.model.parent_id = $scope.form.selected_policy.id;
              },
            addSubGroup : function() {
                $scope.form.model.sub_groups.push($scope.form.sub_group);
                //$scope.form.itemError = false;
                // $scope.form.item = {
                //   name: null,
                //   unit_price: null,
                //   quantity: null,
                //   total: null
                // };
            },

            init: function() {
                $scope.form.loading = true;
                var vendors = Restangular.all("vendor").getList({'pagination': 0});
                var insurancePolicies = Restangular.all("insurance-policy").getList({'pagination': 0,'sub_group' : true});
                
                var insurancePolicyGet = insurancePolicy.get();
                $q.all([insurancePolicyGet, vendors, insurancePolicies]).then(function(result) {
                  $scope.form.loading = false;
                  if (result[0]) {
                    $scope.form.model = result[0];
                    $scope.form.vendors = result[1];
                    $scope.form.policies = result[2];
                    $scope.form.model.start_date = new Date(result[0].start_date);
                    $scope.form.model.end_date = new Date(result[0].end_date);
                  }
                });
            },
            
            submit : function($form) {
                $scope.form.submitted = true;
                $scope.form.itemError = false;
                if (!$form.$valid) {
                  $scope.form.error = true;
                  $scope.form.submitted = false;
                  return false;
                }
                // Restangular.one("insurance-policy", id)
                //   .put($scope.form.model)
                //   .then(
                //     function(response) {
                //       //$uibModalInstance.close(response);
                //     },
                //     function(error) {
                //       console.log(error);
                //       //$uibModalInstance.dismiss();
                //     }
                //   );
                // insurancePolicy.start_date = $scope.form.model.start_date;
                // insurancePolicy.end_date = $scope.form.model.end_date;
                // insurancePolicy.start_date = $scope.form.model.start_date;
                insurancePolicy.vendor_id = $scope.form.model.vendor_id;
                insurancePolicy.name = $scope.form.model.name;
                insurancePolicy.policy_number = $scope.form.model.policy_number;
                insurancePolicy.start_date = $scope.form.model.start_date;
                insurancePolicy.end_date = $scope.form.model.end_date;
                insurancePolicy.coverage_amount = $scope.form.model.coverage_amount;
                insurancePolicy.amount_paid = $scope.form.model.amount_paid;
                insurancePolicy.premium_amount = $scope.form.model.premium_amount;
                insurancePolicy.accessible_to = $scope.form.model.accessible_to;
                insurancePolicy.applicable_for = $scope.form.model.applicable_for;
                insurancePolicy.put().then(function(response) {
                    $uibModalInstance.close(response);
                }, function(error){
                    console.log('EEE', error);
                })

            },
            cancel : function() {
                $uibModalInstance.dismiss();
            },
            close : function() {
                $uibModalInstance.close();
            }

        }
        // Restangular.one("insurance-policy", id).get().then(function(response) {
        //     console.log('get', response);
        //     $scope.form.model = response;
        //     $scope.form.model.end_date = new Date(response.end_date);
        //     $scope.form.model.start_date = new Date(response.start_date);
        // }, function(errors){
        //     console.log('ERR', errors)
        // });
        
        $scope.form.init();
        $scope.start_date = {
            options: {
              applyClass: "btn-success",
              singleDatePicker: true,
              locale: {
                applyLabel: "Apply",
                fromLabel: "From",
                // format: "YYYY-MM-DD", //will give you 2017-01-06
                format: "D-MMM-YY", //will give you 6-Jan-17
                // format: "D-MMMM-YY", //will give you 6-January-17
                toLabel: "To",
                cancelLabel: "Cancel",
                customRangeLabel: "Custom range"
              }
            }
          };
        $scope.close = function() {
            $uibModalInstance.close();
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss();
        };
    }
  ]);
  