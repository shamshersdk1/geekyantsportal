angular.module("myApp").controller("showInsuranceModalCtrl", [
    "$scope",
    "Restangular",
    "data",
    "$uibModalInstance",
    function($scope, Restangular, data, $uibModalInstance) {
        var id = data.id;
        var vendor_services = [];
        $scope.form = {
          loading: false,
          data : {}
        }
        $scope.form.loading = true;
        
        var vendorObj = Restangular.one("insurance-policy", id);
        vendorObj.get().then(
            function(response) {
              $scope.data = response;
              $scope.form.loading = false;
            },
            function(error) {
              console.log("err", error);
            }
          );
        $scope.filteredDate = function (input_date) {
          var formatted_date = new Date(input_date);
          return formatted_date;
        };  
        // Get existing Services name
        $scope.close = function() {
            $uibModalInstance.close();
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss();
        };
    }
  ]);
  