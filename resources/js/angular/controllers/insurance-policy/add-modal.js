angular.module("myApp").controller("createInsurancePolicyModalCtrl", [
    "$scope",
    "Restangular",
    "data",
    "$uibModalInstance",
    function($scope, Restangular, data, $uibModalInstance) {
        $scope.options = {
    formatYear: 'yy',
    maxDate: new Date(2020, 5, 22),
    minDate: new Date(),
  };

        $scope.form = {
            policies: [],
            startDate : {
                datepickerOption : {
                    options: {
                      applyClass: "btn-success",
                      singleDatePicker: true,
                      locale: {
                        applyLabel: "Apply",
                        fromLabel: "From",
                        // format: "YYYY-MM-DD", //will give you 2017-01-06
                        format: "D-MMM-YY", //will give you 6-Jan-17
                        // format: "D-MMMM-YY", //will give you 6-January-17
                        toLabel: "To",
                        cancelLabel: "Cancel",
                        customRangeLabel: "Custom range"
                      }
                    }
                },
            },
            vendors : [],

            model : {
                vendor_id : null,
                name : null,
                policy_number : null,
                description : null,
                start_date : false,
                end_date : false,
                coverage_amount : null,
                amount_paid : null,
                premium_amount : null,
                applicable_for : 'user',
                accessible_to : 'group',
                parent_id : false,
                sub_group : false,
                has_sub_group : false,
                sub_groups : []
            },
            sub_group : {
                name : null,
                premium_amount : null
            },
            select_vendor: function(data) {
                $scope.form.selected_vendor = data;
                $scope.form.model.vendor_id = $scope.form.selected_vendor.id;
              },
            select_policy: function(data) {
                $scope.form.selected_policy = data;
                $scope.form.model.parent_id = $scope.form.selected_policy.id;
              },
            addSubGroup : function() {
                $scope.form.model.sub_groups.push($scope.form.sub_group);
                //$scope.form.itemError = false;
                // $scope.form.item = {
                //   name: null,
                //   unit_price: null,
                //   quantity: null,
                //   total: null
                // };
            },
            init: function() {
                $scope.form.loading = true;
                Restangular.all("vendor")
                  .getList({'pagination': 0})
                  .then(
                    function(response) {
                      $scope.form.loading = false;
                      $scope.form.vendors = response;
                    },
                    function(error) {
                      console.log(error);
                    }
                  );
                Restangular.all("insurance-policy")
                  .getList({'pagination': 0,'sub_group' : false})
                  .then(
                    function(response) {
                      $scope.form.loading = false;
                      $scope.form.policies = response;
                    },
                    function(error) {
                      console.log(error);
                    }
                  );
            },
            submit : function($form) {
                $scope.form.submitted = true;
                $scope.form.itemError = false;
                if (!$form.$valid) {
                  $scope.form.error = true;
                  $scope.form.submitted = false;
                  return false;
                }
                Restangular.all("insurance-policy")
                  .post($scope.form.model)
                  .then(
                    function(response) {
                        console.log('FFF', response);
                      $uibModalInstance.close(response);
                    },
                    function(error) {
                      console.log(error);
                      //$uibModalInstance.dismiss();
                    }
                  );

            },
            cancel : function() {
                $uibModalInstance.dismiss();
            },
            close : function() {
                $uibModalInstance.close();
            }

        }
        $scope.form.init();
        $scope.start_date = {
            options: {
              applyClass: "btn-success",
              singleDatePicker: true,
              locale: {
                applyLabel: "Apply",
                fromLabel: "From",
                // format: "YYYY-MM-DD", //will give you 2017-01-06
                format: "D-MMM-YY", //will give you 6-Jan-17
                // format: "D-MMMM-YY", //will give you 6-January-17
                toLabel: "To",
                cancelLabel: "Cancel",
                customRangeLabel: "Custom range"
              }
            }
          };
        $scope.close = function() {
            $uibModalInstance.close();
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss();
        };
    }
  ]);
  