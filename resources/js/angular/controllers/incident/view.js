angular.module("myApp").controller("incidentViewModalCtrl", [
  "$scope",
  "Restangular",
  "data",
  "$uibModalInstance",
  "$q",
  function($scope, Restangular, data, $uibModalInstance) {
    var id = data.id;
    $scope.form = {
      loading: false,
      data: {},
      init: function() {
        $scope.form.loading = true;
        Restangular.one("incident", id)
          .get()
          .then(
            function(response) {
              console.log('data', response);
              $scope.form.loading = false;
              $scope.form.data = response;
            },
            function(error) {
              console.log(error);
              $scope.form.loading = false;
            }
          );
      },
      cancel: function() {
        $uibModalInstance.dismiss();
      }
    };
    $scope.form.init();
  }
]);
