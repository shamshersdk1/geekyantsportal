angular.module("myApp").controller("incidentEditModalCtrl", [
  "$scope",
  "Restangular",
  "data",
  "$uibModalInstance",
  "$q",
  function($scope, Restangular, data, $uibModalInstance, $q) {
    var id = data.id;
    var incidentObj = Restangular.one("incident", id);
    $scope.form = {
      editing: true,
      submitted: false,
      loading: false,
      itemError: false,
      error: false,
      model: {
        id : null,
        department : null,
        department_id : null,
        subject : null,
        shared_users: [],
      },
      departments : [],
      users: [],
      item: { name: null, unit_price: null, quantity: null, total: null },
      init: function() {
        var departments = Restangular.all("department").getList();
        var users = Restangular.all("user?pagination=0").getList();
        var incident = incidentObj.get();
        $q.all([departments, incident, users, ($scope.form.loading = true)]).then(
          function(result) {
            $scope.form.loading = false;
            if (result[0]) {
              $scope.form.departments = result[0];
            }
            if (result[1]) {
              
              $scope.form.model = result[1];
              $scope.form.model.shared_users = [];
              $scope.form.model.department = $scope.form.model.department;
              
              for ( i=0; i<$scope.form.model.incident_users.length; i++  )
              {
                $scope.form.model.shared_users.push($scope.form.model.incident_users[i].user_id);
              }
            }
            if (result[2]) {
              $scope.form.users = result[2];
            }
          }
        );
      },
      submit: function($form) {
        $scope.form.submitted = true;

        if (!$form.$valid) {
          $scope.form.error = true;
          $scope.form.submitted = false;
          return false;
        }
        $scope.form.loading = true;
        incidentObj.subject = $scope.form.model.subject;
        incidentObj.description = $scope.form.model.description;
        incidentObj.department_id = $scope.form.model.department.id;
        incidentObj.shared_users = $scope.form.model.shared_users;
        incidentObj.put()
          .then(
            function(response) {
              $scope.form.submitted = false;
              $scope.form.loading = false;
              $uibModalInstance.close(response);
            },
            function(error) {
              console.log(error);
              $scope.form.loading = false;
              $uibModalInstance.dismiss();
            }
          );
      },
      cancel: function() {
        $uibModalInstance.dismiss();
      },
    };
    $scope.form.init();
  }
]);
