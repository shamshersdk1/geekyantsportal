angular.module("myApp").controller("incidentCtrl", [
  "$scope",
  "Restangular",
  "$uibModal",
  "DTOptionsBuilder",
  "Datatable",
  function ($scope, Restangular, $uibModal, DTOptionsBuilder, Datatable) {
    $scope.filter = {};
    $scope.data = [];
    $scope.departments = [];

    $scope.originalData = [];
    Restangular.all('incident').getList({'per_page': 1000}).then(function (response) {
      $scope.data = response;
      $scope.originalData = response;
    }, function(error){
      console.log('error fetching incident list', error)
    });
    Restangular.all('department').getList().then(function (response) {
      $scope.departments = response;
    }, function(error){
      console.log('error fetching incident list', error)
    });
    

    //$("#dep_filter").select2();
    $("#status_filter").select2();
    
    $scope.departmentFilter = function(values) {
        var selectedOptions = [];
        angular.forEach(values, function(item){
            selectedOptions.push(item.type);
        });
        $scope.data = Datatable.filterData($scope.filter, $scope.originalData, selectedOptions, "department.type");
    }
    $("#dep_filter").change(function (event) {
      console.log('dep_filter called');
      $scope.$apply(function () {
        var selectedOptions = event.val;
        $scope.data = Datatable.filterData($scope.filter, $scope.originalData, selectedOptions, "department.type");
      });
    });

    $("#status_filter").change(function (event) {
      $scope.$apply(function () {
        var selectedOptions = event.val
        $scope.data = Datatable.filterData($scope.filter, $scope.originalData, selectedOptions, "status");
      });
    });

// >>>>>>> feat/incident-department-filter
    $scope.dtOptions = DTOptionsBuilder.newOptions()
      .withOption('order', [[0, 'DESC']])
      .withOption('scrollY', '300px')
      .withOption('scrollX', '100%')
      .withOption('scrollCollapse', true)
      .withOption('pageLength', 1000)
      .withOption('lengthMenu', [50, 100, 150, 200, 500, 1000])


    $scope.viewDetail = function (id) {
      console.log('incidentCtrl ', id);
      var path = "/admin/incident/" + id;
      window.location.href = path;
    };
    $scope.editDetail = function (id) {

      var modal = $uibModal.open({
        controller: "incidentEditModalCtrl",
        windowClass: "bootstrap_wrap",
        templateUrl: "/views/incident/create.html",
        backdrop: "static",
        size: "md",
        resolve: {
          data: function () {
            return { id: id };
          }
        }
      });
      modal.result.then(
        function (result) {
          window.location.reload();
        },
        function () {
          //cancel
        }
      );
    };
    $scope.deleteDetails = function (id) {
      if (confirm("Are you sure you want to cancel the leave!")) {
        Restangular.one("leave", id)
          .remove()
          .then(
            function (response) {
              location.reload();
            },
            function (error) {
              alert(error.data.message);
            }
          );
      }
    };
    $scope.createIncident = function () {
      var modal = $uibModal.open({
        controller: "createIncidentModalCtrl",
        windowClass: "bootstrap_wrap",
        templateUrl: "/views/incident/create.html",
        backdrop: "static",
        size: "md",
        resolve: {
          data: function () {
            return {};
          }
        }
      });
      modal.result.then(
        function (result) {
          window.location.reload();
        },
        function () {
          //cancel
        }
      );
    };
  }
]);
