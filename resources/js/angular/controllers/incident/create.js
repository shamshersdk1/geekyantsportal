angular.module("myApp").controller("createIncidentModalCtrl", [
  "$scope",
  "Restangular",
  "data",
  "$uibModalInstance",
  "$q",
  function($scope, Restangular, data, $uibModalInstance) {
    $scope.form = {
      submitted: false,
      loading: false,
      message: null,
      error: false,
      departments : [],
      users: [],
      model: {
        department : null,
        department_id: null,
        subject: null,
        description: null,
        shared_users: []
      },
      init: function() {
        $scope.form.loading = true;
        Restangular.all("department")
          .getList()
          .then(
            function(response) {
              $scope.form.loading = false;
              $scope.form.departments = response;
            },
            function(error) {
              $scope.form.loading = false;
              console.log(error);
            }
          );
          Restangular.all("user?pagination=0")
          .getList()
          .then(
            function(response) {
              $scope.form.loading = false;
              $scope.form.users = response;
            },
            function(error) {
              $scope.form.loading = false;
              console.log(error);
            }
          );
      },
      submit: function($form) {
        $scope.form.submitted = true;
        $scope.form.error = false;
        $scope.form.message = null;
        

        if (!$form.$valid) {
          $scope.form.submitted = false;
          return false;
        }
        var params = {
          department_id : $scope.form.model.department.id, 
          subject : $scope.form.model.subject,
          description:  $scope.form.model.description,
          shared_users: $scope.form.model.shared_users
        }
        $scope.form.loading = true;
        Restangular.all("incident")
          .post(params)
          .then(
            function(response) {
              $scope.form.loading = false;
              $uibModalInstance.close(response);
            },
            function(error) {
              $scope.form.error = true;
              $scope.form.loading = false;
              $scope.form.message = error.data.message;
              //$uibModalInstance.dismiss();
            }
          );
      },
      cancel: function() {
        $uibModalInstance.dismiss();
      },
      select_users: function(data) {
        $scope.form.shared_users = data;
      }
    };
    $scope.form.init();
  }
]);
