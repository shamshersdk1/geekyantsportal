angular.module("myApp").controller("transferIncidentModalCtrl", [
  "$scope",
  "Restangular",
  "data",
  "$uibModalInstance",
  function($scope, Restangular, data, $uibModalInstance) {
    var id = data.id;
    $scope.transfer_type = 'department';
    $scope.selected_department = null;
    $scope.selected_user = null;
    $scope.error = false;
    Restangular.all("department").getList().then(
      function(response) {
        $scope.departments = response;
      },
      function(error) {
        console.log(error);
      }
    );
    var params = {'pagination':0};
    
    Restangular.all("user").getList(params).then(
      function(response) {
        $scope.users = response;
      },
      function(error) {
        console.log(error);
      }
    );

    $scope.select_department = function(data) {
      $scope.selected_department = data.id;
      $scope.error = false;
    };

    $scope.select_user = function(data) {
      $scope.selected_user = data.id;
      $scope.error = false;
    };
    $scope.submit = function () {
      if ( $scope.selected_department == null && $scope.selected_user == null )
      {
        $scope.error = true;
        return;
      }
      var params = {
        'incident_id': id,
        'department_id' : $scope.selected_department,
        'user_id' : $scope.selected_user,
      }
      Restangular.all("incident-transfer").post(params).then(
        function(response) {
          $uibModalInstance.close(response);
        },
        function(error) {
          console.log(error);
        }
      );
    };
    
    $scope.close = function() {
      $uibModalInstance.close();
    };

    $scope.cancel = function() {
      $uibModalInstance.dismiss();
    };
  }
]);
