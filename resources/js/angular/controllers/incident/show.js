angular.module('myApp')
    .controller('incidentShowCtrl',["$scope", "Restangular","$uibModal", "$rootScope", function ($scope, Restangular, $uibModal, $rootScope) {
        $scope.loaded = false;
        var id = window.incident.id;
        $scope.readOnly = window.read_only;
        $scope.incidentObj = Restangular.one("incident", id);
        // $scope.departments = Restangular.all("department").getList();
        // $scope.users =
        $scope.incidentObj.get().then(
            function(response) {
                $scope.loaded = true;
                $scope.incidentObj = response;
            },
            function(error) {
                $scope.loaded = true;
                console.log('error',error);
            }
        );

        $scope.transferIncident = function() {
            var modal = $uibModal.open({
              controller: "transferIncidentModalCtrl",
              windowClass: "bootstrap_wrap",
              templateUrl: "/views/incident/transfer.html",
              backdrop: "static",
              size: "md",
              resolve: {
                data: function() {
                  return {id:id};
                }
              }
            });
            modal.result.then(
              function(result) {
                $scope.incidentObj = result;
                $rootScope.$broadcast('activity-log');
              },
              function() {
                //cancel
              }
            );
        };
        
        $scope.shareIncident = function() {
            var modal = $uibModal.open({
                controller: "shareIncidentModalCtrl",
                windowClass: "bootstrap_wrap",
                templateUrl: "/views/incident/share-modal.html",
                backdrop: "static",
                size: "md",
                resolve: {
                  data: function() {
                    return {id:id};
                  }
                }
              });
              modal.result.then(
                function(result) {
                  $rootScope.$broadcast('activity-log');
                  $scope.incidentObj = result;
                  // console.log(result);
                },
                function() {
                  //cancel
                }
              );
        };

        $scope.closeIncident = function() {
            if (confirm("Are you sure you want to close this incident?")) {
                $scope.incidentObj.status = 'closed';
                $scope.incidentObj
                  .put()
                  .then(
                    function(response) {
                      $rootScope.$broadcast('activity-log');
                    },
                    function(error) {
                      alert(error.data.message);
                    }
                  );
              }
        };

        $scope.reopenIncident = function() {
          if (confirm("Are you sure you want to reopen this incident?")) {
              $scope.incidentObj.status = 'reopen';
              $scope.incidentObj
                .put()
                .then(
                  function(response) {
                    $rootScope.$broadcast('activity-log');
                  },
                  function(error) {
                    alert(error.data.message);
                  }
                );
            }
      };

        $scope.verifiedclosedIncident = function() {
            if (confirm("Are you sure you want to verify close this incident?")) {
                $scope.incidentObj.status = 'verified_closed';
                $scope.incidentObj
                  .put()
                  .then(
                    function(response) {
                      $rootScope.$broadcast('activity-log');
                    },
                    function(error) {
                      alert(error.data.message);
                    }
                  );
              }
        };

    }]);