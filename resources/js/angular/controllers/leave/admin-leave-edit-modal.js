angular.module('myApp').controller('adminLeaveEditModalCtrl', [
  '$scope',
  'Restangular',
  'data',
  '$uibModalInstance',
  function($scope, Restangular, data, $uibModalInstance) {
    $scope.loading = true;
    // Action Loadings
    $scope.actionLoading = {
      update: false,
      cancel: false,
      reject: false,
      approve: false
    };
    $scope.leaveCategories = [
      {
        code: 'paid',
        title: 'Paid Leave'
      },
      {
        code: 'sick',
        title: 'Sick Leave'
      },
      {
        code: 'optional-holiday',
        title: 'Optional Holiday'
      },
      {
        code: 'other',
        title: 'Other'
      }
    ];
    // // Fetch List of all clients
    // Restangular.all("company")
    //   .getList()
    //   .then(
    //     function (response) {
    //       $scope.companyList = response;
    //     },
    //     function (errors) {
    //       $scope.error_message = errors.data.message;
    //     }
    //   );
    // // Date Picker
    $scope.datePicker = {
      options: {
        applyClass: 'btn-success',
        locale: {
          applyLabel: 'Apply',
          fromLabel: 'From',
          // format: "YYYY-MM-DD", //will give you 2017-01-06
          format: 'D-MMM-YY', //will give you 6-Jan-17
          // format: "D-MMMM-YY", //will give you 6-January-17
          toLabel: 'To',
          cancelLabel: 'Cancel',
          customRangeLabel: 'Custom range'
        }
      },
      date: {
        startDate: null,
        endDate: null
      }
    };
    // Getting Leave Types
    var getLeaveTypes = function() {
      Restangular.all('leave-types')
        .getList()
        .then(
          function(response) {
            $scope.leaveTypes = response;
          },
          function(errors) {
            console.log(errors);
          }
        );
    };
    getLeaveTypes();

    $scope.deduct_list = [
      {
        value: 'lop',
        name: 'Loss of Pay'
      },
      {
        value: 'sl',
        name: 'Sick Leaves'
      },
      {
        value: 'pl',
        name: 'Paid Leaves'
      },
      {
        value: 'slpl',
        name: 'Sick Leaves & Paid Leaves'
      },
      {
        value: 'other_paid',
        name: 'Other Paid'
      }
    ];

    // Initialising and assigning variables
    $scope.id = data;
    $scope.error = false;
    $scope.success = false;
    $scope.error_message = '';
    $scope.is_team_lead = false;
    $scope.lead_status = 'pending';
    $scope.label_type = '';
    $scope.start_date = null;
    $scope.end_date = null;
    $scope.selected_client = [];
    $scope.selected_client_error = false;
    $scope.deductionFlag = false;
    $scope.deductionType = null;
    $scope.deductionType_error = false;
    // Fetch leave Details
    var leaveObject = Restangular.one('leave', $scope.id);
    leaveObject.get().then(
      function(response) {
        if (response.status) {
          $scope.error = false;
          $scope.success = true;
          $scope.leave = response.data.leave;
          $scope.taken = response.data.taken;
          $scope.total = response.data.total;
          console.log('$scope.taken', $scope.leave);
          $scope.previousLeaves = response.data.previousLeaves;
          $scope.managerStatuses = response.data.managerStatuses;
          $scope.overlappingLeaves = response.data.overlappingLeaves;
          $scope.leaveOverlapping = response.data.leaveOverlapping;
          $scope.is_team_lead = response.data.leadStatuses.is_team_lead;
          $scope.lead_status = response.data.leadStatuses.lead_status;
          $scope.is_management = response.data.isManagement;
          $scope.is_other_type = response.data.isOtherType;
          $scope.datePicker.date.startDate = moment(
            response.data.leave.start_date
          );
          $scope.datePicker.date.endDate = moment(response.data.leave.end_date);
          $scope.set_label($scope.leave.status);
          $scope.created_date = new Date($scope.leave.created_at);
          $scope.loading = false;

          if ($scope.leave.leave_deduction) {
            $scope.deductionFlag = true;
            if ($scope.leave.leave_deduction.loss_of_pay > 0) {
              $scope.deductionType = 'lop';
            } else if ($scope.leave.leave_deduction.other_paid > 0) {
              $scope.deductionType = 'other_paid';
            } else if (
              $scope.leave.leave_deduction.sick_leave > 0 &&
              $scope.leave.leave_deduction.paid_leave == 0
            ) {
              $scope.deductionType = 'sl';
            } else if (
              $scope.leave.leave_deduction.sick_leave == 0 &&
              $scope.leave.leave_deduction.paid_leave > 0
            ) {
              $scope.deductionType = 'pl';
            } else if (
              $scope.leave.leave_deduction.sick_leave > 0 &&
              $scope.leave.leave_deduction.paid_leave > 0
            ) {
              $scope.deductionType = 'slpl';
            } else {
              $scope.deductionType = null;
            }
          }
        } else {
          $scope.error = true;
          $scope.success = false;
          $scope.loading = false;
          $scope.error_message = 'Something went wrong';
        }
      },
      function(errors) {
        $scope.loading = false;
        $scope.error = true;
        $scope.success = false;
        $scope.error_message = errors.data.message;
      }
    );
    $scope.select_client = function(data) {
      $scope.selected_client = data;
      $scope.selected_client_error = false;
    };

    $scope.set_label = function(status) {
      if (status == 'approved') $scope.label_type = 'label-success';
      else if (status == 'pending') $scope.label_type = 'label-info';
      else if (status == 'cancelled') $scope.label_type = 'label-primary';
      else $scope.label_type = 'label-danger';
    };

    $scope.updateLeave = function() {
      if ($scope.leave.status != 'pending') {
        $scope.actionLoading.reject = false;
        alert(
          'Leave is no longer in pending state and hence can not be modified'
        );
      }
      $scope.actionLoading.update = true;
      leaveObject = Restangular.copy($scope.leave);
      var startDate = moment($scope.datePicker.date.startDate).format(
        'YYYY-MM-DD'
      );
      var endDate = moment($scope.datePicker.date.endDate).format('YYYY-MM-DD');
      leaveObject.start_date = startDate;
      leaveObject.end_date = endDate;

      Restangular.one('leave', leaveObject.id)
        .customPUT(leaveObject, 'update')
        .then(
          function(response) {
            $scope.error = false;
            $scope.actionLoading.update = false;
            // $uibModalInstance.dismiss();
            // window.location.reload();
          },
          function(errors) {
            $scope.actionLoading.update = false;
            $scope.error = true;
            $scope.error_message = errors.data.message;
          }
        );
    };

    $scope.rejectLeave = function() {
      $scope.actionLoading.reject = true;
      if ($scope.leave.status != 'pending') {
        $scope.actionLoading.reject = false;
        alert(
          'Leave is no longer in pending state and hence can not be rejected'
        );
      }
      if (
        $scope.leave.cancellation_reason == '' ||
        $scope.leave.cancellation_reason == null
      ) {
        $scope.actionLoading.reject = false;
        alert('You must specify the reason for rejecting the leave');
      } else {
        leaveObject.cancellation_reason = $scope.leave.cancellation_reason;
        leaveObject.status = 'reject';
        Restangular.one('leave', leaveObject.id)
          .customPUT(leaveObject, 'reject')
          .then(
            function(response) {
              $scope.error = false;
              $scope.actionLoading.reject = false;
              $uibModalInstance.dismiss();
              window.location.reload();
            },
            function(errors) {
              $scope.actionLoading.reject = false;
              $scope.error = true;
              $scope.error_message = errors.data.message;
            }
          );
      }
    };

    $scope.approveLeave = function() {
      if (
        !(
          $scope.leave.leave_type.code != 'sick' ||
          $scope.leave.leave_type.code != 'paid'
        )
      ) {
        if (!$scope.deductionType) {
          $scope.deductionType_error = true;
          return;
        }
      }

      $scope.actionLoading.approve = true;
      if ($scope.leave.status != 'pending') {
        $scope.actionLoading.approve = false;
        alert(
          'Leave is no longer in pending state and hence can not be approved'
        );
      }
      leaveObject = Restangular.copy($scope.leave);
      leaveObject.status = 'approve';
      var startDate = moment($scope.datePicker.date.startDate).format(
        'YYYY-MM-DD'
      );
      var endDate = moment($scope.datePicker.date.endDate).format('YYYY-MM-DD');
      leaveObject.start_date = startDate;
      leaveObject.end_date = endDate;
      if ($scope.deductionType) {
        leaveObject.deduction_flag = true;
        leaveObject.deduction_type = $scope.deductionType;
      }
      console.log(leaveObject.deduction_type);
      Restangular.one('leave', leaveObject.id)
        .customPUT(leaveObject, 'approve')
        .then(
          function(response) {
            $scope.actionLoading.approve = false;
            $scope.error = false;
            $uibModalInstance.dismiss();
            window.location.reload();
          },
          function(errors) {
            $scope.actionLoading.approve = false;
            $scope.error = true;
            $scope.error_message = errors.data.message;
          }
        );
    };

    $scope.cancelLeave = function() {
      $scope.actionLoading.cancel = true;
      if (
        $scope.leave.status == 'rejected' ||
        $scope.leave.status == 'cancelled'
      ) {
        $scope.actionLoading.cancel = false;
        alert(
          'Leave is no longer in pending/approved state and hence can not be cancelled'
        );
      }
      leaveObject.status = 'cancelled';
      Restangular.one('leave', leaveObject.id)
        .customPUT(leaveObject, 'cancel')
        .then(
          function(response) {
            $scope.actionLoading.cancel = false;
            $uibModalInstance.dismiss();
            window.location.reload();
          },
          function(errors) {
            $scope.actionLoading.cancel = false;
            $scope.error = true;
            $scope.error_message = errors.data.message;
          }
        );
    };

    $scope.showRejectButton = function() {
      if ($scope.leave) {
        if ($scope.leave.status == 'approved') {
          return false;
        } else if ($scope.is_management && !$scope.loading) {
          return true;
        } else if ($scope.is_other_type) {
          return false;
        } else if (
          !$scope.is_team_lead &&
          $scope.leave.status == 'pending' &&
          !$scope.loading
        ) {
          return true;
        } else if (
          $scope.lead_status == 'pending' &&
          $scope.is_team_lead &&
          !$scope.loading
        ) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    };

    $scope.showApproveButton = function() {
      if ($scope.leave) {
        if ($scope.leave.status == 'approved') {
          return false;
        } else if ($scope.is_management && !$scope.loading) {
          return true;
        } else if ($scope.is_other_type) {
          return false;
        } else if (
          !$scope.is_team_lead &&
          $scope.leave.status == 'pending' &&
          !$scope.loading
        ) {
          return true;
        } else if (
          $scope.lead_status == 'pending' &&
          $scope.is_team_lead &&
          !$scope.loading
        ) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    };

    $scope.showUpdateButton = function() {
      if ($scope.leave) {
        if ($scope.is_management && !$scope.loading) {
          return true;
        } else if ($scope.is_other_type) {
          return false;
        } else if (
          $scope.leave.status == 'pending' &&
          !$scope.loading &&
          !$scope.is_team_lead
        ) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    };

    $scope.showCancelButton = function() {
      if ($scope.leave) {
        if ($scope.is_management && !$scope.loading) {
          return true;
        } else if ($scope.is_other_type) {
          return false;
        } else if (
          $scope.leave.status != 'rejected' &&
          $scope.leave.status != 'cancelled' &&
          !$scope.loading &&
          !$scope.is_team_lead
        ) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    };

    $scope.close = function() {
      $uibModalInstance.dismiss();
      window.location.reload();
    };
  }
]);
