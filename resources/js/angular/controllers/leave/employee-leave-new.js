angular.module('myApp').controller('employeeLeaveNewCtrl', [
  '$scope',
  'Restangular',
  '$location',
  '$log',
  '$uibModal',
  '$route',
  function($scope, Restangular, $location, $log, $uibModal, $route) {
    $scope.gridViewModel = 'leave?per_page=5';
    $scope.optional_leaves = window.optionalLeaves;
    $scope.user_optional_leave = window.userOptionalLeave;
    $scope.todayDate = new Date();
    $scope.x = null;
    $scope.formatDate = function(date) {
      var dateOut = new Date(date);
      return dateOut;
    };

    $scope.leaveCategories = [
      {
        code: 'paid',
        title: 'Paid Leave'
      },
      {
        code: 'sick',
        title: 'Sick Leave'
      },
      {
        code: 'optional-holiday',
        title: 'Optional Holiday'
      },
      {
        code: 'other',
        title: 'Other'
      }
    ];
    $scope.gridViewColumns = [
      {
        title: 'Duration',
        value: '%% row.start_date | date %% to %% row.end_date | date %%',
        width: 18
      },
      {
        title: 'Days',
        value: '%% row.days %%',
        width: 5
      },
      {
        title: 'Type',
        value:
          "<span ng-class=\"{'label label-primary custom-label' : row.leave_type.code=='paid', 'label label-warning custom-label' : row.leave_type.code=='sick', 'label label-success custom-label' : row.leave_type.code=='unpaid' }\"> %% row.leave_type.title | uppercase%% </span>",
        width: 5
      },
      {
        title: 'Status',
        value:
          "<span ng-class=\"{'label label-info custom-label' : row.status=='pending', 'label label-success custom-label' : row.status=='approved', 'label label-danger custom-label' : (row.status=='cancelled' || row.status=='rejected')  }\"> %% row.status | uppercase %%</span> <small ng-if='row.status ==approved' style='display:block'>%%row.approver.name%%</small>",
        width: 5
      }
    ];
    // $scope.gridViewActions = [
    //   {
    //     type: "cancel",
    //     action: "deleteDetails(row.id)"
    //   }
    // ];
    $scope.openModal = function() {
      var data = {
        leave_type_id: $scope.form.data.leave_type_id,
        leave_type: $scope.form.data.leave_type,
        start_date: new Date(),
        end_date: new Date()
      };
      var modal = $uibModal.open({
        controller: 'sickLeaveApplyModalCtrl',
        windowClass: 'bootstrap_wrap',
        templateUrl: '/views/leave/sick-leave-apply-modal.html',
        backdrop: 'static',
        size: 'md',
        resolve: {
          data: function() {
            return data;
          }
        }
      });
    };

    $scope.deleteDetails = function(id) {
      if (confirm('Are you sure you want to cancel the leave!')) {
        Restangular.one('leave', id)
          .remove()
          .then(
            function(response) {
              location.reload();
            },
            function(error) {
              alert(error.data.message);
            }
          );
      }
    };

    $scope.getWarning = function() {
      var params = {
        start_date: moment($scope.datePicker.date.startDate).format(
          'YYYY-MM-DD'
        ),
        end_date: moment($scope.datePicker.date.endDate).format('YYYY-MM-DD'),
        leave_type_id: $scope.form.data.leave_type_id
      };
      if (
        $scope.form.data.leave_type_id == null ||
        $scope.form.data.leave_type.code == 'paid'
      ) {
        Restangular.one('leave-warnings')
          .get(params)
          .then(
            function(response) {
              $scope.form.warnings = response;
            },
            function(errors) {
              $scope.form.warnings = null;
            }
          );
      }
    };

    $scope.getWorkingDays = function() {
      var params = {
        start_date: moment($scope.datePicker.date.startDate).format(
          'YYYY-MM-DD'
        ),
        end_date: moment($scope.datePicker.date.endDate).format('YYYY-MM-DD'),
        leave_type_id: $scope.form.data.leave_type_id
      };
      if ($scope.form.data.leave_type_id != null) {
        Restangular.one('leave-working-days')
          .get(params)
          .then(
            function(response) {
              $scope.form.durationLoading = false;
              $scope.form.duration = response;
            },
            function(errors) {
              $scope.form.durationLoading = false;
            }
          );
      }
    };

    $scope.datePicker = {
      options: {
        applyClass: 'btn-success',
        locale: {
          applyLabel: 'Apply',
          fromLabel: 'From',
          // format: "YYYY-MM-DD", //will give you 2017-01-06
          format: 'D-MMM-YY', //will give you 6-Jan-17
          // format: "D-MMMM-YY", //will give you 6-January-17
          toLabel: 'To',
          cancelLabel: 'Cancel',
          customRangeLabel: 'Custom range'
        },
        eventHandlers: {
          'apply.daterangepicker': function() {
            $scope.form.error_message = null;
            $scope.form.durationLoading = true;
            var params = {
              start_date: moment($scope.datePicker.date.startDate).format(
                'YYYY-MM-DD'
              ),
              end_date: moment($scope.datePicker.date.endDate).format(
                'YYYY-MM-DD'
              ),
              leave_type_id: $scope.form.data.leave_type_id
                ? $scope.form.data.leave_type_id
                : null
            };

            $scope.form.warnings = null;

            Restangular.one('leave-working-days')
              .get(params)
              .then(
                function(response) {
                  $scope.form.durationLoading = false;
                  $scope.form.duration = response;
                },
                function(errors) {
                  $scope.form.durationLoading = false;
                }
              );
            if (params) {
              Restangular.one('leave-warnings')
                .get(params)
                .then(
                  function(response) {
                    $scope.form.warnings = response;
                  },
                  function(errors) {
                    $scope.form.warnings = null;
                  }
                );
            }
          }
        }
      },
      date: { startDate: null, endDate: null },
      min: moment().format('YYYY-MM-DD')
    };

    $scope.leave = {
      loading: false,
      form: {
        type: null,
        start_date: null,
        end_date: null
      }
    };
    $scope.currentDate = Date.now();

    var getLeaveTypes = function() {
      Restangular.all('leave-types')
        .getList()
        .then(
          function(response) {
            $scope.leaveTypes = response;

            angular.forEach(response, function(item) {
              // if(item.code != 'PL')
              //   console.log('D',item);
            });
            var otherLeaveType = {
              id: 0,
              code: 'other',
              title: 'Others'
            };
            // $scope.leaveTypes.push(otherLeaveType);
            // console.log($scope.leaveTypes);
          },
          function(errors) {
            console.log(errors);
          }
        );
    };
    getLeaveTypes();

    $scope.form = {
      loading: false,
      submitting: false,
      error_message: null,
      error: false,
      success: false,
      radioModel: null,
      radioSelectionError: false,
      sickFlag: false,
      duration: null,
      durationLoading: false,
      warnings: null,
      data: {
        user_id: $scope.userId,
        start_date: null,
        end_date: null,
        type: null,
        leave_type_id: null,
        reason: null,
        leave_type: null
      },

      applyLeave: function($form) {
        if ($scope.form.duration == 0) {
          $scope.form.error = true;
          $scope.form.error_message = 'Please select the date range';
          return false;
        }
        $scope.form.submitting = true;
        if ($form.$invalid) {
          $scope.form.submitting = false;
          $scope.form.error = true;
          return false;
        }

        // console.log($scope.form, $scope.datePicker);
        // return false;
        // var startDate = $scope.form.data.start_date;
        // var startEnd = $scope.form.data.end_date;

        if ($scope.form.data.type == 'optional-holiday') {
          $scope.form.duration = 1;
          $scope.form.data.end_date = $scope.form.data.start_date;
        } else {
          $scope.form.data.start_date = moment(
            $scope.datePicker.date.startDate
          ).format('YYYY-MM-DD');
          $scope.form.data.end_date = moment(
            $scope.datePicker.date.endDate
          ).format('YYYY-MM-DD');
        }
        // if ( $scope.form.data.type == 0 && !$scope.form.radioModel )
        // {
        //   console.log('form.data.type ');
        //   $scope.form.submitting = false;
        //   $scope.form.error = true;
        //   $scope.form.radioSelectionError = true;
        //   return false;
        // }
        // $scope.form.loading = true;
        // $scope.form.error = false;
        // $scope.form.error_message = null;

        // $scope.form.data.start_date = startDate;
        // $scope.form.data.end_date = endDate;

        // if ( $scope.form.data.type == 0 ) {
        //   $scope.form.data.type = $scope.form.radioModel;
        // }
        var params = $scope.form.data;

        Restangular.all('leave')
          .post(params)
          .then(
            function(response) {
              $scope.form.loading = false;
              $scope.form.submitting = false;
              //var data = response.data;

              if (response.data.leave_overlapping) {
                $scope.openOverlappingLeaveModal(response.data);
                return false;
              } else {
                location.reload();
                $scope.form.success = true;
                $scope.form.error = false;
              }
            },
            function(errors) {
              $scope.form.loading = false;
              $scope.form.submitting = false;
              $scope.form.error = true;
              $scope.form.success = false;
              $scope.form.error_message = errors.data.message;
            }
          );
      },
      checkProjectOverlapping: function() {
        $log.log('checkProjectOverlapping called');
      }
    };

    $scope.ltchanged = function() {
      console.log('lt changed');
    };

    $scope.openOverlappingLeaveModal = function(data) {
      let leave_type = '';
      for (i = 0; i < $scope.leaveTypes.length; i++) {
        if ($scope.form.data.type == $scope.leaveTypes[i].id) {
          leave_type = $scope.leaveTypes[i].title;
        }
      }
      var data = {
        overlapping_data: data.overlapping_leaves,
        type: $scope.form.data.type,
        leave_type: leave_type,
        start_date: $scope.form.data.start_date,
        end_date: $scope.form.data.end_date,
        reason: $scope.form.data.reason,
        user_id: $scope.userId,
        leave_type_id: $scope.form.data.leave_type_id,
        leave_type: $scope.form.data.leave_type
      };
      var modal = $uibModal.open({
        controller: 'leaveOverlappingModalCtrl',
        windowClass: 'bootstrap_wrap',
        templateUrl: '/views/leave/leave-overlapping-modal-new.html',
        backdrop: 'static',
        size: 'lg',
        resolve: {
          data: function() {
            return data;
          }
        }
      });
      modal.result.then(function(res) {}, function() {});
    };
    $scope.append = function(leave_id) {
      return '/user/leaves/status/' + leave_id;
    };
    $scope.$watch(
      function($scope) {
        if ($scope.form.data.leave_type_id != null) {
          var leaveCode = $scope.leaveTypes.find(
            type => type.id == $scope.form.data.leave_type_id
          );
          $scope.form.data.leave_type = leaveCode;
        }
        if (
          $scope.form.data.leave_type != null &&
          $scope.x != $scope.form.data.leave_type
        ) {
          $scope.x = $scope.form.data.leave_type;
          if (
            $scope.datePicker.date.startDate != null &&
            $scope.datePicker.date.endDate != null
          ) {
            $scope.form.warnings = null;
            $scope.getWarning();
            $scope.getWorkingDays();
          }
        }
        return $scope.form.data.type;
      },

      function() {
        $scope.form.error_message = null;
        if ($scope.leaveCategories) {
          if (
            $scope.form.data.type == 'paid' ||
            $scope.form.data.type == 'sick' ||
            $scope.form.data.type == 'optional-holiday'
          ) {
            var index = $scope.leaveTypes.find(
              type => type.code === $scope.form.data.type
            );
            if (index) {
              $scope.form.data.leave_type_id = index.id;
              $scope.form.data.leave_type = index;
              //$scope.sdas.functu();
            }
          }
          $scope.form.sickFlag = false;
          for (i = 0; i < $scope.leaveCategories.length; i++) {
            if (
              $scope.form.data.code == $scope.leaveCategories[i].code &&
              $scope.leaveCategories[i].code == 'sick'
            ) {
              $scope.form.sickFlag = true;
            }
          }
        }
      }
    );
  }
]);
