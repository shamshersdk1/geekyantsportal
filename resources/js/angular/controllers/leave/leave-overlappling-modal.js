angular.module("myApp").controller("leaveOverlappingModalCtrl", [
  "$scope",
  "Restangular",
  "data",
  "$uibModalInstance",
  "$sce",
  function($scope, Restangular, data, $uibModalInstance, $sce) {
    $scope.data = data;
    $scope.application_reason = [];
    $scope.loading = false;
    $scope.overlap_reasons = [];

    $scope.submit = function() {
      $scope.loading = false;

      for (var i = 0; i < $scope.data.overlapping_data.projects.length; i++) {
        $scope.overlap_reasons[i] = {
          project_id: $scope.data.overlapping_data.projects[i].id,
          comment: $scope.application_reason[i]
        };
      }
      var params = {
        start_date: $scope.data.start_date,
        end_date: $scope.data.end_date,
        user_id: $scope.data.user_id,
        type: $scope.data.type,
        reason: $scope.data.reason,
        overlap_reasons: $scope.overlap_reasons,
        leave_type_id: $scope.data.leave_type_id,
        leave_type: $scope.data.leave_type
      };
      Restangular.all("leave")
        .post(params)
        .then(
          function(response) {
            if (response.status) {
              $uibModalInstance.dismiss();
              window.location.reload();
            }
          },
          function(errors) {
            $scope.error_message = errors.data.message;
          }
        );
    };

    $scope.cancel = function() {
      $uibModalInstance.dismiss();
    };
  }
]);
