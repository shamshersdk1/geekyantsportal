angular.module('myApp').controller('employeeLeaveCtrl', [
  '$scope',
  'Restangular',
  '$location',
  '$log',
  '$uibModal',
  '$route',
  function($scope, Restangular, $location, $log, $uibModal, $route) {
    $scope.gridViewModel = 'leave?per_page=5';
    $scope.gridViewColumns = [
      {
        title: 'Duration',
        value: '%% row.start_date | date %% to %% row.end_date | date %%',
        width: 18
      },
      {
        title: 'Days',
        value: '%% row.days %%',
        width: 5
      },
      {
        title: 'Type',
        value:
          "<span ng-class=\"{'label label-primary custom-label' : row.type=='paid', 'label label-warning custom-label' : row.type=='sick', 'label label-success custom-label' : row.type=='unpaid' }\"> %% row.type +' Leave' | uppercase%% </span>",
        width: 5
      },
      {
        title: 'Status',
        value:
          "<span ng-class=\"{'label label-info custom-label' : row.status=='pending', 'label label-success custom-label' : row.status=='approved', 'label label-danger custom-label' : (row.status=='cancelled' || row.status=='rejected')  }\"> %% row.status | uppercase %%</span> <small ng-if='row.status ==approved' style='display:block'>%%row.approver.name%%</small>",
        width: 5
      }
    ];
    $scope.gridViewActions = [
      {
        type: 'cancel',
        action: 'deleteDetails(row.id)'
      }
    ];
    $scope.deleteDetails = function(id) {
      if (confirm('Are you sure you want to cancel the leave!')) {
        Restangular.one('leave', id)
          .remove()
          .then(
            function(response) {
              location.reload();
            },
            function(error) {
              alert(error.data.message);
            }
          );
      }
    };

    $scope.data = {
      leaves: []
    };

    $scope.datePicker = {
      options: {
        applyClass: 'btn-success',
        locale: {
          applyLabel: 'Apply',
          fromLabel: 'From',
          // format: "YYYY-MM-DD", //will give you 2017-01-06
          format: 'D-MMM-YY', //will give you 6-Jan-17
          // format: "D-MMMM-YY", //will give you 6-January-17
          toLabel: 'To',
          cancelLabel: 'Cancel',
          customRangeLabel: 'Custom range'
        }
      },
      date: { startDate: null, endDate: null },
      min: moment().format('YYYY-MM-DD')
    };

    $scope.leave = {
      loading: false,
      form: {
        type: null,
        start_date: null,
        end_date: null
      }
    };
    $scope.currentDate = Date.now();

    $scope.leaveTypes = [
      {
        name: 'Select a leave type',
        value: null
      },
      {
        name: 'Sick Leave',
        value: 'sick'
      },
      {
        name: 'Paid Leave',
        value: 'paid'
      },
      {
        name: 'Unpaid Leave',
        value: 'unpaid'
      }
    ];

    $scope.form = {
      loading: false,
      submitting: false,
      error_message: null,
      error: false,
      success: false,
      data: {
        user_id: $scope.userId,
        start_date: null,
        end_date: null,
        type: null,
        reason: null
      },
      sick_leave_consumed: 0,
      paid_leave_consumed: 0,
      applyLeave: function($form) {
        $scope.form.submitting = true;
        if ($form.$invalid) {
          $scope.form.submitting = false;
          $scope.form.error = true;
          return false;
        }
        $scope.form.loading = true;
        $scope.form.error = false;
        $scope.form.error_message = null;
        var startDate = moment($scope.datePicker.date.startDate).format(
          'YYYY-MM-DD'
        );
        var endDate = moment($scope.datePicker.date.endDate).format(
          'YYYY-MM-DD'
        );

        $scope.form.data.start_date = startDate;
        $scope.form.data.end_date = endDate;

        var params = $scope.form.data;
        Restangular.all('leave')
          .post(params)
          .then(
            function(response) {
              $scope.form.loading = false;
              $scope.form.submitting = false;
              //var data = response.data;
              if (response.data.leave_overlapping) {
                $scope.openOverlappingLeaveModal(response.data);
                return false;
              } else {
                location.reload();
                $scope.form.success = true;
                $scope.form.error = false;
              }
            },
            function(errors) {
              $scope.form.loading = false;
              $scope.form.submitting = false;
              $scope.form.error = true;
              $scope.form.success = false;
              $scope.form.error_message = errors.data.message;
            }
          );
      },
      checkProjectOverlapping: function() {
        $log.log('checkProjectOverlapping called');
      }
    };

    $scope.openOverlappingLeaveModal = function(data) {
      var data = {
        overlapping_data: data.overlapping_leaves,
        type: $scope.form.data.type,
        start_date: $scope.form.data.start_date,
        end_date: $scope.form.data.end_date,
        reason: $scope.form.data.reason,
        user_id: $scope.userId
      };
      var modal = $uibModal.open({
        controller: 'leaveOverlappingModalCtrl',
        windowClass: 'bootstrap_wrap',
        templateUrl: '/views/leave/leave-overlapping-modal.html',
        backdrop: 'static',
        size: 'lg',
        resolve: {
          data: function() {
            return data;
          }
        }
      });
      modal.result.then(function(res) {}, function() {});
    };
    $scope.append = function(leave_id) {
      return '/user/leaves/status/' + leave_id;
    };
  }
]);
