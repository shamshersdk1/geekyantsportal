angular.module("myApp").controller("reportingManagerLeaveCtrl", [
    "$scope",
    "Restangular",
    "$location",
    "$log",
    "$uibModal",
    function ($scope, Restangular, $location, $log, $uibModal) 
    {
        $scope.showModal = function (id) 
        {
            $scope.list = id;
            var modal = $uibModal.open({
                controller: "adminLeaveEditModalCtrl",
                windowClass: "bootstrap_wrap",
                templateUrl: "/views/leave/admin-leave-edit-modal-new.html",
                backdrop: "static",
                size: "lg",
                resolve: {
                    data: function () {
                        return $scope.list;
                    }
                }
            });
        }
    }
]);
