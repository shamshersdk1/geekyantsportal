angular.module('myApp').controller('adminLeaveApplyModalCtrl', [
  '$scope',
  'Restangular',
  '$uibModalInstance',
  'LeaveService',
  function($scope, Restangular, $uibModalInstance, LeaveService) {
    $scope.loading = true;
    $scope.error = false;
    $scope.userLeaveInfo = [];
    $scope.x = null;
    $scope.leaveTypes = [];
    $scope.todayDate = new Date();
    $scope.formatDate = function(date) {
      var dateOut = new Date(date);
      return dateOut;
    };

    Restangular.all('user')
      .getList({ pagination: 0, is_active: 1 })
      .then(
        function(response) {
          $scope.loading = false;
          $scope.userList = response;
        },
        function(errors) {
          $scope.loading = false;
          $scope.error_message = errors.data.message;
        }
      );

    let getUserLeaveDetails = function(user_id) {
      let params = {
        user_id: user_id
      };
      Restangular.one('user-leave-details')
        .get(params)
        .then(
          function(response) {
            $scope.userLeaveInfo = response;
            $scope.form.leave_info_fetched = true;
          },
          function(errors) {
            console.log(errors);
          }
        );
    };

    $scope.getWarning = function() {
      var params = {
        start_date: moment($scope.datePicker.date.startDate).format(
          'YYYY-MM-DD'
        ),
        end_date: moment($scope.datePicker.date.endDate).format('YYYY-MM-DD'),
        leave_type_id: $scope.form.leave_type_id,
        user_id: $scope.form.selected_user.id
      };

      if (
        $scope.form.leave_type_id == null ||
        $scope.form.leave_type.code == 'paid'
      ) {
        Restangular.one('leave-warnings')
          .get(params)
          .then(
            function(response) {
              $scope.form.warning = response;
            },
            function(errors) {
              $scope.form.warning = null;
            }
          );
      }
    };

    $scope.datePicker = {
      options: {
        applyClass: 'btn-success',
        locale: {
          applyLabel: 'Apply',
          fromLabel: 'From',
          // format: "YYYY-MM-DD", //will give you 2017-01-06
          format: 'D-MMM-YY', //will give you 6-Jan-17
          // format: "D-MMMM-YY", //will give you 6-January-17
          toLabel: 'To',
          cancelLabel: 'Cancel',
          customRangeLabel: 'Custom range'
        },
        eventHandlers: {
          'apply.daterangepicker': function() {
            var params = {
              start_date: moment($scope.datePicker.date.startDate).format(
                'YYYY-MM-DD'
              ),
              end_date: moment($scope.datePicker.date.endDate).format(
                'YYYY-MM-DD'
              ),
              leave_type_id: $scope.form.leave_type_id,
              user_id: $scope.form.selected_user.id
            };

            Restangular.one('leave-working-days')
              .get(params)
              .then(
                function(response) {
                  $scope.form.duration = response;
                },
                function(errors) {
                  console.log(errors);
                }
              );

            Restangular.one('leave-warnings')
              .get(params)
              .then(
                function(response) {
                  $scope.form.warning = response;
                },
                function(errors) {
                  console.log(errors);
                }
              );
          }
        }
      },
      date: { startDate: new Date(), endDate: new Date() }
    };
    $scope.form = {
      leave_info_fetched: false,
      loading: false,
      submitted: false,
      selected_user: [],
      status: 'approved',
      reason: '',
      half: 'full',
      half_day_flag: false,
      leave_type_id: null,
      leave_type: null,
      radio_selection: null,
      duration: 1,
      warning: null,
      optional_leave_id: null,
      start_date: null,
      end_date: null,
      errors: {
        selected_user: false,
        reason: false,
        type: false,
        date: false
      },
      leave_categories: LeaveService.categories,
      leave_types: [],
      optional_holidays: [],
      select_user: function(data) {
        $scope.form.selected_user = data;
        $scope.form.errors.selected_user = false;
        getUserLeaveDetails($scope.form.selected_user.id);
      },
      submit: function(userForm) {
        $scope.form.submitted = true;
        if ($scope.form.selected_user.length == 0) {
          $scope.form.errors.selected_user = true;
        }
        if (userForm.$valid && !$scope.form.errors.selected_user) {
          $scope.form.loading = true;
          $scope.error = false;
          if ($scope.form.type == 'optional-holiday') {
            $scope.form.duration = 1;
            $scope.form.end_date = $scope.form.start_date;
          } else {
            var startDate = moment($scope.datePicker.date.startDate).format(
              'YYYY-MM-DD'
            );
            var endDate = moment($scope.datePicker.date.endDate).format(
              'YYYY-MM-DD'
            );
            $scope.form.start_date = startDate;
            $scope.form.end_date = endDate;
          }

          if ($scope.form.leave_type_id == 0) {
            $scope.form.leave_type_id = $scope.form.radio_selection;
          }
          //return false;
          var params = {
            user_id: $scope.form.selected_user
              ? $scope.form.selected_user.id
              : '',
            start_date: $scope.form.start_date,
            end_date: $scope.form.end_date,
            leave_type_id: $scope.form.leave_type_id,
            type: $scope.form.leave_type_id,
            half: $scope.form.half,
            status: $scope.form.status,
            reason: $scope.form.reason,
            skip_overlapping_check: true
          };

          Restangular.all('leave')
            .post(params)
            .then(
              function(response) {
                $scope.form.loading = false;
                $scope.response = response;
                if (response.status) {
                  $scope.success = true;
                  $scope.error = false;
                  $uibModalInstance.dismiss();
                  window.location.reload();
                } else {
                  $scope.error = true;
                  $scope.success = false;
                  $scope.error_message = response.data.message;
                }
              },
              function(errors) {
                $scope.form.loading = false;
                $scope.error = true;
                $scope.success = false;
                $scope.error_message = errors.data.message;
              }
            );
        }
      }
    };
    LeaveService.getLeaveTypes().then(function(response) {
      $scope.form.leave_types = response;
    });
    LeaveService.getOptionalholidays().then(function(response) {
      $scope.form.optional_holidays = response;
    });

    $scope.$watch(function($scope) {
      if ($scope.form.leave_type_id != null) {
        var leaveCode = $scope.form.leave_types.find(
          type => type.id == $scope.form.leave_type_id
        );
        $scope.form.leave_type = leaveCode;
      }
      if (
        $scope.form.leave_type != null &&
        $scope.x != $scope.form.leave_type
      ) {
        $scope.x = $scope.form.leave_type;
        $scope.getWarning();
      }

      return $scope.form.type;
    });
    $scope.$watch(
      function($scope) {
        return $scope.form.warning;
      },
      function() {}
    );
    $scope.$watch(
      function($scope) {
        return $scope.form.type;
      },
      function() {
        $scope.form.half_day_flag = false;
        checkHalfDay();
      }
    );
    $scope.$watch(
      function($scope) {
        return $scope.form.duration;
      },
      function() {
        $scope.form.half_day_flag = false;
        checkHalfDay();
      }
    );
    function checkHalfDay() {
      if ($scope.form.leave_categories.length > 0) {
        if (
          $scope.form.type == 'paid' ||
          $scope.form.type == 'sick' ||
          $scope.form.type == 'optional-holiday'
        ) {
          var index = $scope.form.leave_types.find(
            type => type.code === $scope.form.type
          );
          if (index) {
            $scope.form.leave_type_id = index.id;
          }
          if ($scope.form.type == 'sick' && $scope.form.duration == 1) {
            $scope.form.half_day_flag = true;
          }
        }
      }
    }
    // $scope.$watch(
    //   function($scope) { return $scope.form.duration },
    //   function() {
    //     $scope.form.half_day_flag = false;
    //     if ( $scope.leaveTypes && $scope.form.duration == 1 )
    //     {
    //       for ( i = 0; i < $scope.leaveTypes.length; i++ )
    //       {
    //         if ( $scope.form.leave_type_id == $scope.leaveTypes[i].id && $scope.leaveTypes[i].code == 'sick'  )
    //         {
    //           $scope.form.half_day_flag = true;
    //         }
    //       }
    //     }
    //   }
    // );

    // $scope.submitForm = function(userForm) {
    //   $scope.submitted = true;
    //   if ($scope.selected_user.length == 0) {
    //     $scope.selected_user_error = true;
    //   }
    //   if (userForm.$valid && !$scope.selected_user_error) {
    //     $scope.loading = true;
    //     $scope.error = false;
    //     var startDate = moment($scope.datePicker.date.startDate).format(
    //       "YYYY-MM-DD"
    //     );
    //     var endDate = moment($scope.datePicker.date.endDate).format(
    //       "YYYY-MM-DD"
    //     );
    //     var params = {
    //       user_id: $scope.selected_user ? $scope.selected_user.id : "",
    //       start_date: startDate,
    //       end_date: endDate,
    //       type: $scope.type,
    //       half: $scope.half,
    //       status: $scope.status,
    //       reason: $scope.reason,
    //       skip_overlapping_check: true
    //     };

    //     Restangular.all("leave")
    //       .post(params)
    //       .then(
    //         function(response) {
    //           $scope.loading = false;
    //           $scope.response = response;
    //           if (response.status) {
    //             $scope.success = true;
    //             $scope.error = false;
    //             $uibModalInstance.dismiss();
    //             window.location.reload();
    //           } else {
    //             $scope.error = true;
    //             $scope.success = false;
    //             $scope.error_message = response.data.message;
    //           }
    //         },
    //         function(errors) {
    //           $scope.loading = false;
    //           $scope.error = true;
    //           $scope.success = false;
    //           $scope.error_message = errors.data.message;
    //         }
    //       );
    //   }
    // };
    $scope.cancel = function() {
      $uibModalInstance.dismiss();
    };
  }
]);
