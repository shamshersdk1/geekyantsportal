angular.module('myApp').controller('sickLeaveApplyModalCtrl', [
  '$scope',
  'Restangular',
  'data',
  '$uibModalInstance',
  'LeaveService',
  function($scope, Restangular, data, $uibModalInstance, LeaveService) {
    $scope.data = data;
    $scope.error = false;
    $scope.error_message = '';
    $scope.loading = true;
    $scope.warning = '';

    Restangular.one('sick-leave-warnings')
      .get()
      .then(
        function(response) {
          $scope.loading = false;
          $scope.warning = response;
        },
        function(errors) {
          $scope.loading = false;
          $scope.error = true;
          $scope.error_message = errors.data.message;
        }
      );

    $scope.form = {
      loadin: false,
      leave_type_id: null,
      submitted: false,
      submitting: false,
      errors: {
        reason: false,
        type: false,
        date: false
      },
      submit: function(userForm) {
        $scope.form.loadin = true;
        $scope.form.submitted = true;
        var params = {
          user_id: '',
          start_date: '',
          end_date: '',
          leave_type_id: data.leave_type_id,
          type: '',
          half: '',
          status: '',
          reason: 'I am sick today',
          skip_overlapping_check: true
        };

        Restangular.all('sick-leave')
          .post(params)
          .then(
            function(response) {
              $scope.form.loadin = false;
              $scope.response = response;

              $scope.error = true;
              $scope.success = false;
              $scope.error_message = response;
            },
            function(errors) {
              $scope.form.loadin = false;
            }
          );
      }
    };

    $scope.cancel = function() {
      $uibModalInstance.dismiss();
      window.location.reload();
    };
  }
]);
