angular.module("myApp").controller("leaveApplyModalCtrl", [
    "$scope",
    "Restangular",
    "$location",
    "$log",
    "$uibModalInstance",
    function ($scope, Restangular, $location, $log, $uibModalInstance) {
        $log.log('====================================');
        console.log('leaveApplyModalCtrl  called');
        console.log('====================================');

        $scope.showModal = function (user) {
            if ($scope.assigned[user.id] == null) {
                $scope.data.error = true;
                $scope.data.errorMessage = "This user doesn't have a slack id, message can't be sent";
                window.scrollTo(0, 0);
                return;
            }
            var modal = $uibModal.open({
                controller: "leaveApplyModalCtrl",
                windowClass: "bootstrap_wrap",
                templateUrl: "/views/leave/modal.html",
                backdrop: "static",
                size: "md",
                resolve: {
                    data: function () {
                        return { user: user, slack_email: $scope.assigned[user.id].email };
                    }
                }
            });
        };
    }
]);
