angular.module("myApp").controller("leaveApplyCtrl", [
    "$scope",
    "Restangular",
    "$location",
    "$log",
    "$uibModal",
    function ($scope, Restangular, $location, $log, $uibModal) 
    {
        $scope.openModal = function () 
        {
            var modal = $uibModal.open({
                controller: "adminLeaveApplyModalCtrl",
                windowClass: "bootstrap_wrap",
                templateUrl: "/views/leave/admin-leave-apply-modal-new.html",
                backdrop: "static",
                size: "lg"
            });
        };

        $scope.showModal = function (id) 
        {
            $scope.list = id;
            var modal = $uibModal.open({
                controller: "adminLeaveEditModalCtrl",
                windowClass: "bootstrap_wrap",
                templateUrl: "/views/leave/admin-leave-edit-modal-new.html",
                backdrop: "static",
                size: "lg",
                resolve: {
                    data: function () {
                        return $scope.list;
                    }
                }
            });
        }
    }
]);
