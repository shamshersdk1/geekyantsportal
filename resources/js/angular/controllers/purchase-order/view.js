angular.module("myApp").controller("purchaseOrderViewModalCtrl", [
  "$scope",
  "Restangular",
  "data",
  "$uibModalInstance",
  "FileUploader",
  function($scope, Restangular, data, $uibModalInstance,FileUploader) {
    var params = {
      url: '/api/v1/file',
      autoUpload:true
    };
     
    $scope.uploader = new FileUploader(params);
    $scope.uploader.onBeforeUploadItem = function(item) {
      //item.formData.push({some: 'data'});
      console.log('onBeforeUploadItem', item);
    }
    

    // $scope.uploader.onAfterAddingFile = function(fileItem) {
    //     console.info('onAfterAddingFile', fileItem);
    //     console.log('formData',fileItem.formData);
    //     fileItem.formData = {
    //       test :" sdsd"
    //     };
    //     var reader = new FileReader();
    //     reader.onloadend = function(event) {
    //       console.log(fileItem.file.name + ': ' + event.target.result.split(',')[1]);
    //     };
    //     reader.readAsDataURL(fileItem._file);
    // };
    
    $scope.uploader.onSuccessItem = function(item, response, status, headers) {
      console.log('response', response);
      $scope.form.uploaded = true;
    }
    var id = data.id;
    $scope.form = {
      id: data.id,
      loading: false,
      uploading: false,
      uploaded: false,
      data: {},
      init: function() {
        $scope.form.loading = false;
        Restangular.one("purchase-orders", id)
          .get()
          .then(
            function(response) {
              $scope.form.loading = false;
              $scope.form.data = response;
            },
            function(error) {
              console.log(error);
              $uibModalInstance.dismiss();
            }
          );
      },
      cancel: function() {
        $uibModalInstance.dismiss();
      },
    };
    $scope.download = function() {
      var path = "/api/v1/purchase-order-download/"+id;
      window.location.href =  path;
    }
    $scope.form.init();
  }
]);
