angular.module("myApp").controller("purchaseOrderEditModalCtrl", [
  "$scope",
  "Restangular",
  "data",
  "$uibModalInstance",
  "$q",
  function($scope, Restangular, data, $uibModalInstance, $q) {
    var id = data.id;
    $scope.options = {
      formatYear: "yy",
      maxDate: new Date(2030, 5, 22),
      minDate: new Date(2018, 4, 1)
    };
    $scope.form = {
      submitted: false,
      loading: false,
      itemError: false,
      error: false,
      model: {},
      deliveryDate: {
        datepickerOptions: {
          options: {
            applyClass: "btn-success",
            singleDatePicker: true,
            locale: {
              applyLabel: "Apply",
              fromLabel: "From",
              // format: "YYYY-MM-DD", //will give you 2017-01-06
              format: "D-MMM-YY", //will give you 6-Jan-17
              // format: "D-MMMM-YY", //will give you 6-January-17
              toLabel: "To",
              cancelLabel: "Cancel",
              customRangeLabel: "Custom range"
            }
          }
        }
      },
      item: { name: null, unit_price: null, quantity: null, total: null },
      init: function() {
        var oldData = Restangular.one("purchase-orders", id).get();
        var vendorList = Restangular.all("vendor").getList({ pagination: 0 });
        var companyList = Restangular.all("company").getList({ pagination: 0 });
        $q.all([
          vendorList,
          oldData,
          companyList,
          ($scope.form.loading = true)
        ]).then(function(result) {
          $scope.form.loading = false;
          if (result[0]) {
            $scope.form.allVendors = result[0];
          }
          if (result[1]) {
            $scope.form.model = result[1];
            $scope.form.model.delivery_date = new Date(result[1].delivery_date);
            $scope.form.selected_vendor = $scope.form.model.vendor;
            $scope.form.selected_company = $scope.form.model.company;
            if ($scope.form.model.gst_included == 1) {
              $scope.form.model.gst_included = "yes";
            } else $scope.form.model.gst_included = "no";
          }
          if (result[2]) {
            $scope.form.companies = result[2];
          }
        });
      },
      submit: function($form) {
        $scope.form.submitted = true;

        if ($scope.form.model.purchase_items.length == 0) {
          $scope.form.itemError = true;
        }
        if (!$form.$valid || $scope.form.itemError) {
          $scope.form.error = true;
          $scope.form.submitted = false;
          return false;
        }
        Restangular.all("purchase-orders")
          .customPUT($scope.form.model, id)
          .then(
            function(response) {
              $scope.form.submitted = false;
              $scope.form.loading = false;
              $uibModalInstance.close(response);
            },
            function(error) {
              console.log(error);
              $uibModalInstance.dismiss();
            }
          );
      },
      cancel: function() {
        $uibModalInstance.dismiss();
      },
      select_vendor: function(data) {
        $scope.form.selected_vendor = data;
        $scope.form.model.vendor_id = $scope.form.selected_vendor.id;
      },
      select_company: function(data) {
        $scope.form.selected_company = data;
        $scope.form.model.company_id = $scope.form.selected_company.id;
      },
      addNewItem: function() {
        $scope.form.model.purchase_items.push($scope.form.item);
        $scope.form.itemError = false;
        $scope.form.item = {
          name: null,
          unit_price: null,
          quantity: null,
          total: null
        };
      },
      removeItem: function(index) {
        $scope.form.model.purchase_items.splice(index, 1);
      }
    };
    $scope.form.init();
  }
]);
