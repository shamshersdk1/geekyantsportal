angular.module("myApp").controller("createPurchaseOrderModalCtrl", [
  "$scope",
  "Restangular",
  "data",
  "$uibModalInstance",
  "$q",
  function($scope, Restangular, data, $uibModalInstance) {
    $scope.options = {
      formatYear: "yy",
      maxDate: new Date(2030, 5, 22),
      minDate: new Date()
    };
    $scope.form = {
      submitted: false,
      loading: false,
      itemError: false,
      error: false,
      selected_vendor: null,
      deliveryDate: {
        datepickerOptions: {
          options: {
            applyClass: "btn-success",
            singleDatePicker: true,
            locale: {
              applyLabel: "Apply",
              fromLabel: "From",
              // format: "YYYY-MM-DD", //will give you 2017-01-06
              format: "D-MMM-YY", //will give you 6-Jan-17
              // format: "D-MMMM-YY", //will give you 6-January-17
              toLabel: "To",
              cancelLabel: "Cancel",
              customRangeLabel: "Custom range"
            }
          }
        }
      },
      model: {
        vendor_id: null,
        company_id: null,
        reference_number: null,
        payment_terms: null,
        delivery_date: null,
        gst_included: "yes",
        status: null,
        total: null,
        purchase_items: [
          { name: null, unit_price: null, quantity: null, total: null }
        ]
      },
      item: { name: null, unit_price: null, quantity: null, total: null },
      init: function() {
        $scope.form.loading = true;
        Restangular.all("vendor")
          .getList({ pagination: 0 })
          .then(
            function(response) {
              $scope.form.loading = false;
              $scope.form.allVendors = response;
            },
            function(error) {
              console.log(error);
            }
          );
        Restangular.all("company")
          .getList({ pagination: 0 })
          .then(
            function(response) {
              $scope.form.loading = false;
              $scope.form.companies = response;
            },
            function(error) {
              console.log(error);
            }
          );
      },
      submit: function($form) {
        $scope.form.submitted = true;
        $scope.form.itemError = false;
        if (
          $scope.form.model.purchase_items[0].name == null ||
          $scope.form.model.purchase_items[0].unit_price == null ||
          $scope.form.model.purchase_items[0].quantity == null
        ) {
          $scope.form.itemError = true;
        }
        if (!$form.$valid || $scope.form.itemError) {
          $scope.form.error = true;
          $scope.form.submitted = false;
          return false;
        }
        Restangular.all("purchase-orders")
          .post($scope.form.model)
          .then(
            function(response) {
              $uibModalInstance.close(response);
            },
            function(error) {
              console.log(error);
              $uibModalInstance.dismiss();
            }
          );
      },
      cancel: function() {
        $uibModalInstance.dismiss();
      },
      select_vendor: function(data) {
        $scope.form.selected_vendor = data;
        $scope.form.model.vendor_id = $scope.form.selected_vendor.id;
      },
      select_company: function(data) {
        $scope.form.selected_company = data;
        $scope.form.model.company_id = $scope.form.selected_company.id;
      },
      addNewItem: function() {
        $scope.form.model.purchase_items.push($scope.form.item);
        $scope.form.itemError = false;
        $scope.form.item = {
          name: null,
          unit_price: null,
          quantity: null,
          total: null
        };
      },
      removeItem: function(index) {
        $scope.form.model.purchase_items.splice(index, 1);
      }
    };
    $scope.form.init();
  }
]);
