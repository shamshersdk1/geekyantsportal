angular.module("myApp").controller("purchaseOrderCtrl", [
  "$scope",
  "Restangular",
  "$uibModal",
  function($scope, Restangular, $uibModal) {
    $scope.gridViewModel = "purchase-orders";
    $scope.gridViewColumns = [
      { title: "#", value: "%% row.id %%", width: 5 },
      { title: "Vendor", value: "%% row.vendor.name %%", width: 10 },
      { title: "Company", value: "%% row.company.name %%", width: 10 },
      {
        title: "Reference Number",
        value: "%% row.reference_number %%",
        width: 10
      },
      {
        title: "Delivery Date",
        value: "%% row.delivery_date | date%%",
        width: 10
      },
      { title: "Created By", value: "%% row.created_by.name %%", width: 10 },
      {
        title: "Created At",
        value: "%%row.created_at | dateToISO %%",
        width: 20
      },
      {
        title: "Status",
        value:
          "<span ng-class=\"{'label label-primary   custom-label' : row.status=='raised', 'label label-success custom-label' : row.status=='received', 'label label-danger custom-label' : (row.status=='cancelled' || row.status=='rejected')  }\"> %% row.status | uppercase %%</span>",
        width: 10,
        condition: ""
      }
    ];
    $scope.gridViewActions = [
      { type: "view", action: "veiwDetail(row.id)" },
      { type: "edit", action: "editDetail(row.id)" },
      {
        type: "delete",
        action: "deleteDetails(row.id)",
        condition: "row.status != 'received'"
      }
    ];
    $scope.veiwDetail = function(id) {
      var modal = $uibModal.open({
        controller: "purchaseOrderViewModalCtrl",
        windowClass: "bootstrap_wrap",
        templateUrl: "/views/purchase-order/view.html",
        backdrop: "static",
        size: "lg",
        resolve: {
          data: function() {
            return { id: id };
          }
        }
      });
      modal.result.then(
        function(result) {
          //
        },
        function() {
          //cancel
        }
      );
    };
    $scope.editDetail = function(id) {
      var modal = $uibModal.open({
        controller: "purchaseOrderEditModalCtrl",
        windowClass: "bootstrap_wrap",
        templateUrl: "/views/purchase-order/create.html",
        backdrop: "static",
        size: "lg",
        resolve: {
          data: function() {
            return { id: id };
          }
        }
      });
      modal.result.then(
        function(result) {
          window.location.reload();
        },
        function() {
          //cancel
        }
      );
    };
    $scope.deleteDetails = function(id) {
      if (confirm("Are you sure you want to delete this record?")) {
        Restangular.one("purchase-orders", id)
          .remove()
          .then(
            function(response) {
              location.reload();
            },
            function(error) {
              alert(error.data.message);
            }
          );
      }
    };
    $scope.createPurchaseOrder = function() {
      var modal = $uibModal.open({
        controller: "createPurchaseOrderModalCtrl",
        windowClass: "bootstrap_wrap",
        templateUrl: "/views/purchase-order/create.html",
        backdrop: "static",
        size: "lg",
        resolve: {
          data: function() {
            return {};
          }
        }
      });
      modal.result.then(
        function(result) {
          window.location.reload();
        },
        function() {
          //cancel
        }
      );
    };
  }
]);
