angular.module('myApp')
    .controller("adminTimesheetExtraHourCtrl", ["$scope", "Restangular", "data", "$uibModalInstance", function ($scope, Restangular, data, $uibModalInstance) {
        var monthId = data.month_id;
        var userId = data.user_id;
        var date = data.date;
        console.log('user_id ', data);
        $scope.data = {
            extra_hours : 0,
            user_name : null,
            month_id : monthId,
            bonus_extra_hour : 0,
            extra_hours_request : false
        }
        // $scope.calculateTotal = function(){
        //     var sum = 0;
        //     angular.forEach($scope.form.extra_hours,function(extraHour){
        //         sum = sum + extraHour.extra_hours;
        //     });
        //     $scope.form.total_hour =sum;

        //     var days = (sum/8);
        //     $scope.form.total_days = days;
        //     $scope.form.total_days = Number((days).toFixed(1)); 
        // }
         
        $scope.form = {
            error : false,
            message : null,
            data : data,
            extra_hours : [],
            total_hour : 0,
            total_days : 0,
            loading : false,
            submitting : false,
            extra_hours_request : false,
            submit : function(form) {
                var params = {
                    extra_hours_request : $scope.form.extra_hours_request,
                    hours : $scope.form.bonus_extra_hour,
                    month_id : monthId,
                    user_id : userId,
                    type : 'extra',
                    date : date,
                    redeem_type : {
                        "redeem_type":"encash", 
                        'type': 'extra', 
                        'days' : $scope.form.total_days,
                        'hours' : $scope.form.hours
                    }
                };
                
                $scope.form.submitting = true;
                Restangular.all('user-timesheet/approve/'+data.user_id+'/'+data.month_id).post(params).then(function(response){
                    $scope.form.submitting = false;
                    $scope.form.error = false;
                    $uibModalInstance.close(response);
                    
                }, function(error){
                    $scope.form.submitting = false;
                    $scope.form.error = true;
                    $scope.form.message = error.data.message;
                })
            },
            cancel : function() {
              $uibModalInstance.dismiss();
            }
        }
        
        Restangular.one("user-timesheet-month/"+monthId+"/"+userId)
            .get() 
            .then(function (response) {
                console.log('RES', response);
                extraHour = parseInt(response.extra_hour);
                $scope.data = response;
                $scope.data.extra_hours = extraHour;
                $scope.form.bonus_extra_hour = extraHour;
            }, function (error) {
                $scope.model.loading = false;
                console.log('response', response);
            }); 
        // var params = {user_id: data.user_id, month_id :data.month_id};
        // Restangular.all('user-timesheet-extra').getList(params).then(function(response){
        //     $scope.form.extra_hours = response;
        //     $scope.calculateTotal();
        // }, function(error){
        //     console.log('Error', error);
        // });
        // Restangular.all('user-timesheet-extra').getList(params).then(function(response){
        //     $scope.form.extra_hours = response;
        //     $scope.calculateTotal();
        // }, function(error){
        //     console.log('Error', error);
        // })

    }]);