angular.module("myApp").controller("adminTimesheetReviewCtrl", [
  "$scope",
  "Restangular",
  "$uibModal",
  function($scope, Restangular, $uibModal) {
    var monthId = window.monthId;
    var userId = window.userId;
    $scope.test = "Admin review";

    $scope.model = {
      userId: userId,
      monthId: monthId,
      loading: false,
      data: {},

      getUserReport: function(userId, monthId) {
        $scope.model.loading = true;
        var params = {
          monthId: $scope.model.monthId,
          userId: $scope.model.userId
        };
        Restangular.oneUrl("get-user-timesheet/" + monthId + "/" + userId)
          .get()
          .then(
            function(response) {
              $scope.model.loading = false;
              $scope.model.data = response;
            },
            function(error) {
              $scope.model.loading = false;
              console.log("response", response);
            }
          );
      },
      approveUserTimelog: function(id) {
        // api/v1/new-timesheet/approval/44
        // PUT
      },
      reviewTimesheet: function(monthId, userId, date) {
        console.log("getLastDay", date);
        var modal = $uibModal.open({
          controller: "adminTimesheetExtraHourCtrl",
          windowClass: "bootstrap_wrap",
          templateUrl: "/views/user-timesheet/extra-hour.html",
          backdrop: "static",
          size: "lg",
          resolve: {
            data: function() {
              return {
                month_id: monthId,
                user_id: userId,
                date: date
              };
            }
          }
        });
        modal.result.then(
          function(res) {
            window.location.reload();
            $scope.model.data = res;
          },
          function() {}
        );
      },
      splitHours: function(index) {
        var totalApprovedHour =
          $scope.model.data.days[index].approved_total_time;
        var approvedHour = 8;
        if (totalApprovedHour > 10) {
          var extraHour = totalApprovedHour - approvedHour;
          $scope.model.data.days[index].approved_hour = approvedHour;
          $scope.model.data.days[index].extra_hour = extraHour;
        }
      },

      approveBonusRequest: function(parentIndex, index) {
        var day = $scope.model.data.days[parentIndex]["bonus_requests"][index];
        var bonusRequest = day;
        $scope.model.data.days[parentIndex]["bonus_requests"][
          index
        ].loading = true;
        Restangular.all("bonus-request-approve")
          .post(bonusRequest)
          .then(
            function(response) {
              var bonusRequest = response;
              bonusRequest.redeem_info = JSON.parse(response.redeem_type);
              $scope.model.data.days[parentIndex]["bonus_requests"][
                index
              ] = bonusRequest;
              $scope.model.data.days[parentIndex]["bonus_requests"][
                index
              ].loading = false;
            },
            function(error) {
              $scope.model.data.days[
                index
              ].additional_bonus_request.loading = false;
              console.log(error);
            }
          );
      },
      rejectBonusRequest: function(parentIndex, index) {
        var day = $scope.model.data.days[parentIndex]["bonus_requests"][index];
        $scope.model.data.days[parentIndex]["bonus_requests"][
          index
        ].loading = true;
        var bonusRequest = day;
        Restangular.all("bonus-request-reject")
          .post(bonusRequest)
          .then(
            function(response) {
              $scope.model.data.days[parentIndex]["bonus_requests"][
                index
              ] = response;
              $scope.model.data.days[parentIndex]["bonus_requests"][
                index
              ].loading = false;
              //$scope.init();
            },
            function(error) {
              $$scope.model.data.days[parentIndex]["bonus_requests"][
                index
              ].loading = false;
              console.log(error);
            }
          );
      },
      addExtra: function(index) { 
        var day = $scope.model.data.days[index];
        if (day.approve_extra_hour) {
          $scope.model.data.days[index].updating = true;
          var extraHours = day.extra_hour.extra_hours;
          var projectId = day.extra_hour.project_id;
          if (extraHours > 0) {
            var params = {
              id: day.id,
              user_id: userId,
              date: day.date,
              extra_hours: extraHours,
              project_id: projectId
            };
            Restangular.all("user-timesheet-extra")
              .post(params)
              .then(
                function(response) {
                  //$scope.model.data.days[index].approve_extra_hour = true;
                  $scope.model.data.days[index].updating = false;
                  $scope.model.data.days[index].splitting = false;
                  $scope.model.data.days[index].approved_total_time = 8;
                  $scope.model.data.days[index].extra_hours = extraHours;
                  $scope.model.data.days[index].extra_hour = response;
                },
                function(error) {
                  $scope.model.data.days[index].splitting = false;
                  $scope.model.data.days[index].updating = false;
                  console.log("error", error);
                }
              );
          }
        } else {
          Restangular.one("user-timesheet-extra", day.extra_hour.id)
            .remove()
            .then(
              function(response) {
                //$scope.model.data.days[index].approve_extra_hour = true;
                $scope.model.data.days[index].updating = false;
                $scope.model.data.days[index].splitting = false;
                $scope.model.data.days[index].approved_total_time = 8;
                $scope.model.data.days[index].extra_hour.extra_hours = 0;
                $scope.model.data.days[index].extra_hour.project_id = null;
              },
              function(error) {
                $scope.model.data.days[index].splitting = false;
                $scope.model.data.days[index].updating = false;
                console.log("error", error);
              }
            );
        }
      }
    };

    $scope.model.getUserReport(userId, monthId);

    // $scope.projects = window.projects;
    // $scope.verticalTotals = JSON.parse(window.verticalTotals);
    // $scope.user = JSON.parse(window.user);
    // $scope.currentDate = new Date().toISOString().slice(0, 10);

    // $scope.addTime = function (name, result) {
    //     $scope.list = {
    //         project_name: name,
    //         data: result
    //     };

    //     var modal = $uibModal.open({
    //         controller: "modalCtrl",
    //         windowClass: "bootstrap_wrap",
    //         templateUrl: "/views/add_time.html",
    //         backdrop: "static",
    //         size: "md",
    //         resolve: {
    //             data: function () {
    //                 return $scope.list;
    //             }
    //         }
    //     });
    //     modal.result.then(function (res) { }, function () { });
    // };
    // $scope.addTimeAprrover = function (name, result, user) {
    //     $scope.list = {
    //         project_name: name,
    //         approved_hours: result.approved_hours,
    //         data: result,
    //         user_name: user
    //     };
    //     var modal = $uibModal.open({
    //         controller: "adminModalCtrl",
    //         windowClass: "bootstrap_wrap",
    //         templateUrl: "/views/approve_time.html",
    //         backdrop: "static",
    //         size: "md",
    //         resolve: {
    //             data: function () {
    //                 return $scope.list;
    //             }
    //         }
    //     });
    //     modal.result.then(function (res) { }, function () { });
    // };
    // $scope.updateApproveList = function (data) {
    //     var params = {
    //         timesheet_id: data.timesheet_id,
    //         approved_hours: data.approved_hours
    //     };
    //     Restangular.all("admin/timesheet")
    //         .post(params)
    //         .then(function (response) { }, function (error) { });
    // };
  }
]);
