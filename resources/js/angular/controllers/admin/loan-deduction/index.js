angular.module('myApp')
    .controller("loanDeductionController", ["$scope", "Restangular", "$uibModal", function ($scope, Restangular, $uibModal) {
        var monthId = window.monthId;
        console.log('LOAN deduction', monthId);
        $scope.model = {
            monthId : monthId,
            loading : false,
            data : {},

            getData : function(monthId) {
                $scope.model.loading = true;
                var params = {monthId : monthId};
                Restangular.all("loan/deduction")
                .getList({month_id : monthId})
                .then(function (response) {
                    $scope.model.loading = false;
                    console.log('response', response);
                    ///$scope.model.data = response;
                }, function (error) {
                    $scope.model.loading = false;
                    console.log('response', response);
                });    
            }
            
        };
        
        $scope.model.getData(monthId);
    }]);