"use strict()";

/*Directives*/
angular
  .module("myApp")
  .directive("billSchedule", [
    function() {
      return {
        templateUrl: "/scripts/directives/bill-schedule/bill-schedule.html?v=1",
        restrict: "E",
        replace: true,
        scope: {
          projectId: "=",
          isDisabled: "@isDisabled"
        },
        controller: [
          "$scope",
          "Restangular",
          function($scope, Restangular) {
            $scope.billScheduleData;
            $scope.loadBillScheduleData = function() {
              $scope.billScheduleData;
              $scope.loading = true;
              Restangular.one("project-bill-schedule", $scope.projectId)
                .get()
                .then(
                  function(response) {
                    $scope.loading = false;
                    $scope.billScheduleData = response;
                  },
                  function(error) {
                    console.log(error);
                  }
                );
            };
            $scope.loadBillScheduleData();

            $scope.filteredDate = function(input_date) {
              var formatted_date = new Date(input_date);
              return formatted_date;
            };
          }
        ]
      };
    }]);
