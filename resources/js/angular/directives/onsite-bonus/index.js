"use strict()";

/*Directives*/
angular
  .module("myApp")
  .directive("onsiteBonus", [
    function() {
      return {
        templateUrl: "/scripts/directives/onsite-bonus/index.html?v=1",
        restrict: "E",
        replace: true,
        scope: {
          projectId: "=",
          isDisabled: "@isDisabled"
        },
        controller: [
          "$scope",
          "Restangular",
          "$uibModal",
          function($scope, Restangular, $uibModal) {
            $scope.oldbonusObjs = [];
            $scope.loading = false;
            $scope.showModal = function() {
              var modal = $uibModal.open({
                controller: "onsiteBonusaddModalCtrl",
                windowClass: "bootstrap_wrap",
                templateUrl:
                  "/scripts/directives/onsite-bonus/add-edit-modal.html",
                backdrop: "static",
                size: "md",
                resolve: {
                  data: function() {
                    return { project_id: $scope.projectId };
                  }
                }
              });
              modal.result.then(
                function(result) {
                  $scope.oldbonusObjs.push(result);
                },
                function() {
                  //
                }
              );
            };
            $scope.loadOldBonus = function() {
              $scope.oldbonusObjs = [];
              $scope.loading = true;
              Restangular.all("onsite-bonus")
                .customGET("", {
                  project_id: $scope.projectId
                })
                .then(
                  function(response) {
                    $scope.loading = false;
                    for (i = 0; i < response.length; i++) {
                      $scope.oldbonusObjs.push(response[i]);
                    }
                  },
                  function(error) {
                    console.log(error);
                  }
                );
            };
            $scope.loadOldBonus();

            $scope.removebonusObjRow = function(index, bonusRow) {
              var r = confirm("Are you sure you want to delete this entry?");
              if (r != true) {
                return;
              }
              Restangular.one("onsite-bonus", bonusRow.id)
                .remove()
                .then(
                  function(response) {
                    $scope.oldbonusObjs.splice(index, 1);
                  },
                  function(error) {
                    console.log(error);
                  }
                );
            };
            $scope.editbonusObjRow = function(index, bonusRow) {
              var modal = $uibModal.open({
                controller: "onsiteBonusmodalEditCtrl",
                windowClass: "bootstrap_wrap",
                templateUrl:
                  "/scripts/directives/onsite-bonus/add-edit-modal.html",
                backdrop: "static",
                size: "md",
                resolve: {
                  data: function() {
                    return {
                      bonus_id: bonusRow.id,
                      project_id: $scope.projectId
                    };
                  }
                }
              });
              modal.result.then(
                function(result) {
                  $scope.loadOldBonus();
                },
                function() {
                  //
                }
              );
            };
            $scope.filteredDate = function(input_date) {
              var formatted_date = new Date(input_date);
              return formatted_date;
            };
          }
        ]
      };
    }
  ])
  .controller("onsiteBonusaddModalCtrl", [
    "$scope",
    "Restangular",
    "data",
    "$uibModalInstance",
    function($scope, Restangular, data, $uibModalInstance) {
      var projectId = data.project_id;

      $scope.form = {
        fetching: true,
        loading: false,
        submitting: false,
        error: false,
        errorMessage: null,
        data: {
          from_date: null,
          to_date: null,
          amount: null,
          type: "local",
          notes: null
        },
        init: function() {
          $scope.form.loading = true;
          Restangular.one("project-info", projectId)
            .get()
            .then(
              function(response) {
                $scope.form.loading = false;
                if (response) {
                  $scope.form.project = response;
                  $scope.form.resources = $scope.form.project[0].resources;
                }
              },
              function(error) {
                console.log(error);
              }
            );
        },
        submit: function(bonusForm) {
          $scope.form.submitting = true;
          if (!bonusForm.$valid) {
            $scope.form.error = true;
            return;
          }
          $scope.loading = true;
          var params = {
            project_id: projectId,
            user_id: $scope.selected_user.id,
            type: $scope.form.data.type,
            amount: $scope.form.data.amount,
            from_date: $scope.date.startDate.format("YYYY-MM-DD HH:mm:ss"),
            to_date: $scope.date.endDate.format("YYYY-MM-DD HH:mm:ss"),
            notes: $scope.form.data.notes
          };

          Restangular.all("onsite-bonus")
            .post(params)
            .then(
              function(response) {
                $uibModalInstance.close(response);
              },
              function(error) {
                console.log(error);
                $uibModalInstance.dismiss();
              }
            );
        },
        cancel: function() {
          $uibModalInstance.dismiss();
        }
      };
      $scope.form.init();
      $scope.select_user = function(data) {
        $scope.selected_user = data;
      };

      //Daterange picker
      $scope.date = {
        startDate: moment().subtract(1, "days"),
        endDate: moment()
      };

      $scope.opts = {
        locale: {
          applyClass: "btn-green",
          applyLabel: "Apply",
          fromLabel: "From",
          format: "YYYY-MM-DD",
          toLabel: "To",
          cancelLabel: "Cancel",
          customRangeLabel: "Custom range"
        },
        ranges: {
          "Last 7 Days": [moment().subtract(6, "days"), moment()],
          "Last 30 Days": [moment().subtract(29, "days"), moment()]
        }
      };

      //Watch for date changes
      $scope.$watch("date", function(newDate) {}, false);
    }
  ])
  .controller("onsiteBonusmodalEditCtrl", [
    "$scope",
    "Restangular",
    "data",
    "$uibModalInstance",
    "$q",
    function($scope, Restangular, data, $uibModalInstance, $q) {
      var projectId = data.project_id;
      var bonusId = data.bonus_id;

      $scope.form = {
        fetching: true,
        loading: false,
        submitting: false,
        error: false,
        errorMessage: null,
        data: {
          from_date: null,
          to_date: null,
          amount: null,
          type: "weekend",
          notes: null
        },
        submit: function(bonusForm) {
          $scope.form.submitting = true;
          if (!bonusForm.$valid) {
            $scope.form.error = true;
            return;
          }
          $scope.loading = true;
          var params = {
            bonus_id: bonusId,
            project_id: projectId,
            user_id: $scope.selected_user.id,
            type: $scope.form.data.type,
            amount: $scope.form.data.amount,
            from_date: $scope.date.startDate.format("YYYY-MM-DD HH:mm:ss"),
            to_date: $scope.date.endDate.format("YYYY-MM-DD HH:mm:ss"),
            notes: $scope.form.data.notes
          };

          Restangular.all("onsite-bonus")
            .customPUT(params)
            .then(
              function(response) {
                $scope.loading = false;
                $uibModalInstance.close(response);
              },
              function(error) {
                console.log(error);
                $uibModalInstance.dismiss();
              }
            );
        },
        cancel: function() {
          $uibModalInstance.dismiss();
        }
      };

      var peojectInfo = Restangular.one("project-info", projectId).get();
      var bonus = Restangular.one("onsite-bonus", bonusId).get();

      $q.all([peojectInfo, bonus]).then(function(result) {
        $scope.form.fetching = false;
        if (result[0]) {
          $scope.form.project = result[0];
          $scope.form.resources = $scope.form.project[0].resources;
        }
        if (result[1]) {
          $scope.form.data = result[1];
        }
        $scope.date = {
          startDate: moment($scope.form.data.from_date),
          endDate: moment($scope.form.data.to_date)
        };
        $scope.selected_user = $scope.form.data.user;
      });

      $scope.select_user = function(data) {
        $scope.selected_user = data;
      };

      //Daterange picker
      $scope.opts = {
        locale: {
          applyClass: "btn-green",
          applyLabel: "Apply",
          fromLabel: "From",
          format: "YYYY-MM-DD",
          toLabel: "To",
          cancelLabel: "Cancel",
          customRangeLabel: "Custom range"
        },
        ranges: {
          "Last 7 Days": [moment().subtract(6, "days"), moment()],
          "Last 30 Days": [moment().subtract(29, "days"), moment()]
        }
      };

      //Watch for date changes
      $scope.$watch("date", function(newDate) {}, false);
    }
  ]);