"use strict()";

/*Directives*/
angular
    .module("myApp")
    .directive("projectTechnology", [
        function () {
            return {
                templateUrl: "/scripts/directives/project-technology/project-technology.html?v=1",
                restrict: "E",
                replace: true,
                scope: {
                    projectId: "=",
                    isDisabled: "@isDisabled",
                },
                controller: ["$scope",
                    "Restangular",function (
                    $scope,
                    Restangular
                ) {
                    $scope.allTechnologies = [];
                    $scope.projectTechnologies = [];
                    // get project-technology
                    $scope.getProjectTechnologies = function () {
                        $scope.listLoading = true;
                        Restangular.all("project-technology")
                            .customGET("", {
                                "project_id": $scope.projectId,
                            })
                            .then(function (response) {
                                $scope.projectTechnologies = response.data.technologies;
                            },function (error) {
                                console.log(error);
                            }
                        );
                    };
                    // All technologies
                    $scope.getAllTechnologies = function () {
                        $scope.listLoading = true;
                        Restangular.all("technology")
                            .customGET("", {})
                            .then(function (response) {
                                $scope.allTechnologies = response;
                            },function (error) {
                                console.log(error);
                            }
                        );
                    };
                    $scope.getProjectTechnologies();  
                    $scope.getAllTechnologies(); 

                    $scope.select_project_technologies = function (data) {
                        $scope.projectTechnologies = data;
                        $scope.selected_slack_public_channels_error = false;
                        var params = {
                            'project_id' : $scope.projectId,
                            'technologies' : $scope.projectTechnologies,
                        };
                        
                        Restangular.all("project-technology")
                            .post(params)
                            .then(function (response) {
                            }, function (error) {
                                console.log(error);
                            });
                    };
                }]
            };
        }
    ]);