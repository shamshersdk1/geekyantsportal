"use strict()";

/*Directives*/
angular
  .module("myApp")
  .directive("googleDriveLinks", [
    function() {
      return {
        templateUrl: "/scripts/directives/google-drive-links/index.html",
        restrict: "E",
        replace: true,
        scope: {
          referenceId: "=",
          referenceType: "@referenceType",
          isDisabled: "@isDisabled",
          mode: "@mode"
        },
        controller:["$scope", "Restangular", function($scope, Restangular) {
          $scope.oldLinks = [];
          $scope.editable = [];
          $scope.loading = false;
          $scope.error = false;
          $scope.errors = {
            'name' : false,
            'path' : false
          };
          $scope.newLinkObj = {
            name: null,
            path: null
          };
          $scope.loadOldLinks = function() {
            // $scope.oldProjectLinks = [];
            $scope.loading = true;
            Restangular.all("google-drive-link")
              .customGET("", {
                reference_id: $scope.referenceId,
                reference_type: $scope.referenceType
              })
              .then(
                function(response) {
                  $scope.loading = false;
                  $scope.oldLinks = response;
                },
                function(error) {
                  console.log(error);
                }
              );
          };
          
          $scope.loadOldLinks();

          $scope.addLink = function() {
            $scope.error = false;
            if (!$scope.newLinkObj.name) {
              $scope.errors.name = true;
              $scope.error = true;
            } else {
              $scope.errors.name = false;
            }

            if (!$scope.newLinkObj.path) {
              $scope.errors.path = true;
              $scope.error = true;
            } else {
              $scope.errors.path = false;
            }

            if ($scope.error) {
              return;
            }

            var params = {
              reference_id: $scope.referenceId,
              reference_type: $scope.referenceType,
              name: $scope.newLinkObj.name,
              path: $scope.newLinkObj.path
            };
            Restangular.all("google-drive-link")
              .post(params)
              .then(
                function(response) {
                  $scope.newLinkObj = {
                    name: "",
                    path: ""
                  };
                  $scope.loadOldLinks();
                },
                function(error) {
                  console.log(error);
                }
              );
          };
          $scope.editLink = function(index) {
            $scope.editable[index] = true;
            
          };
          $scope.saveLink = function(index, link) {
            Restangular.one("google-drive-link", link.id)
              .customPUT({
                link: link
              })
              .then(
                function(response) {
                  $scope.editable[index] = false;
                  $scope.loadOldLinks();
                },
                function(errors) {
                  console.log(errors);
                }
              );
          };

          $scope.removeLink = function(index, link) {
            var r = confirm("Are you sure you want to delete this entry?");
            if (r != true) {
              return;

            }
            Restangular.one("google-drive-link", link.id)
              .remove()
              .then(
                function(response) {
                  $scope.oldLinks.splice(index, 1);
                },
                function(error) {
                  console.log(error);
                }
              );
          };
        }]
      };
    }
  ]);
