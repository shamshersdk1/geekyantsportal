"use strict()";

/*Directives*/
angular.module("myApp").directive("assignAsset", [
  function() {
    return {
      templateUrl: "/scripts/directives/assign-asset/index.html",
      restrict: "E",
      replace: true,
      scope: {
        assetId: "="
      },
      controller: [
        "$scope",
        "Restangular",
        "$q",
        "$uibModal",
        function($scope, Restangular, $q, $uibModal) {
          var api = "asset-detail";
          $scope.form = {
            loading: false,
            error: false,
            message: null,
            submitted: false,
            assignError: false,
            assignErrorMsg: "",
            isAssigned: false,
            ipReq: false,
            data: [],
            selected_ip: {
              wifi: null,
              lan: null
            },
            wifi: null,
            lan: null,
            startDate: {
              format: "dd-MMM-yyyy",
              datepickerOptions: { maxDate: new Date(), startingDay: 0 },
              isOpen: false
            },
            model: {
              asset_id: $scope.assetId,
              user_id: null,
              from_date: null,
              to_date: null
            },
            init: function() {
              var data = Restangular.all(api).getList({
                asset_id: $scope.assetId,
                pagination: 0,
                per_page: 100
              });
              var userList = Restangular.all("user-all-active").getList();
              var asset = Restangular.one("asset", $scope.assetId).get();
              $q.all([
                data,
                userList,
                asset,
                ($scope.form.loading = true)
              ]).then(function(result) {
                $scope.form.loading = false;
                if (result[0]) {
                  $scope.form.data = result[0];
                  $scope.form.isAssigned = false;
                  angular.forEach($scope.form.data, function(item) {
                    if (item.to_date === null) {
                      $scope.form.isAssigned = true;
                    }
                  });
                }
                if (result[1]) {
                  $scope.allUsers = result[1];
                }
                if (result[2]) {
                  angular.forEach(result[2].metas, function(meta) {
                    if (meta.key === "WIFI-MAC") {
                      $scope.form.wifi = meta;
                      $scope.form.ipReq = true;
                    }
                    if (meta.key === "LAN-MAC") {
                      $scope.form.lan = meta;
                      $scope.form.ipReq = true;
                    }
                  });
                }
              });
            },
            enableEditing: function(item) {
              item.editing = !item.editing;
              item.isOpen = false;
              item.datepickerOptions = { maxDate: new Date() };
              item.update = function() {
                if (
                  typeof item.to_date == "undefined" ||
                  item.to_date == "0000-00-00" ||
                  item.to_date == null
                ) {
                  item.error = true;
                  return false;
                }
                item.put().then(
                  function(response) {
                    item.loading = false;
                    item.editing = false;
                    item.error = false;
                    item.assignee = response.assignee;
                    item.released_by = response.released_by;
                    item.user = response.user;
                    $scope.form.isAssigned = false;
                    item.assigning = false;
                    $scope.form.wifi.ip_mapper = null;
                    $scope.form.lan.ip_mapper = null;
                    $scope.form.resetForm();
                  },
                  function(error) {
                    item.loading = false;
                  }
                );
              };
            },
            releaseIP: function(meta) {
              console.log('releaseIp called', meta);
              // if (type === "wifi") {
              //   $scope.form.wifi.loading = true;
              // }
              // if (type === "lan") {
              //   $scope.form.lan.loading = true;
              // }
              Restangular.one("ip-address-mapper", meta.ip_mapper.id)
                .remove()
                .then(
                  function(response) {
                    console.log('releaseIP ', response);
                    meta.ip_mapper = null;
                    // item.loading = false;
                    // item.assigning = false;
                    // if (type === "wifi") {
                    //   $scope.form.wifi.ip_mapper = null;
                    //   $scope.form.wifi.loading = false;
                    // }
                    // if (type === "lan") {
                    //   $scope.form.lan.ip_mapper = null;
                    //   $scope.form.lan.loading = false;
                    // }
                  },
                  function(error) {
                    console.log(error);
                  }
                );
            },
            enableAssigning: function(item) {
              item.assigning = !item.assigning;
              //var params = {user_id : item.user_id};
              Restangular.all("user-ip-address/"+item.user_id+"/free-ips")
                .getList()
                .then(
                  function(response) {
                    console.log('free-ips', response);
                    $scope.loading = false;
                    $scope.form.userFreeIpList = response;
                    if ($scope.form.userFreeIpList.length == 0) {
                      $scope.form.assignError = true;
                      $scope.form.assignErrorMsg =
                        "User does not have a free ip address to assign. Allocate ip address to user and try again!";
                    }
                  },
                  function(error) {
                    console.log(error);
                  }
                );
            },
            resetForm: function() {
              $scope.form.selected_user = null;
              $scope.form.model.user_id = null;
              $scope.form.model.from_date = null;
              $scope.form.model.to_date = null;
              $scope.form.submitted = false;
              $scope.form.assignError = null;
              $scope.form.assignErrorMsg = "";
            },
            submit: function($form) {
              $scope.form.submitted = true;
              if (!$form.$valid) {
                $scope.form.error = true;
                $scope.form.submitted = false;
                return false;
              }
              Restangular.all("asset-detail")
                .post($scope.form.model)
                .then(
                  function(response) {
                    $scope.form.data.unshift(response);
                    $scope.form.submitted = false;
                    $scope.form.isAssigned = true;
                    $scope.form.resetForm();
                  },
                  function(error) {
                    $scope.form.submitted = false;
                    $scope.form.assignError = true;
                    $scope.form.assignErrorMsg = error.data.message;
                  }
                );
            },
            assignIp: function(meta) {
              if (type === "wifi") {
                $scope.form.wifi.loading = true;
                if (!$scope.form.selected_ip.wifi) {
                  $scope.form.wifi.loading = false;
                  item.error = true;
                  return;
                }
                var params = {
                  reference_type: "asset",
                  reference_id: $scope.form.wifi.id,
                  user_ip_address_id: $scope.form.selected_ip.wifi.id
                };
              }
              if (type === "lan") {
                $scope.form.lan.loading = true;
                if (!$scope.form.selected_ip.lan) {
                  $scope.form.lan.loading = false;
                  item.error = true;
                  return;
                }
                var params = {
                  reference_type: "asset",
                  reference_id: $scope.form.lan.id,
                  user_ip_address_id: $scope.form.selected_ip.lan.id
                };
              }
              Restangular.all("ip-address-mapper")
                .post(params)
                .then(
                  function(response) {
                    item.error = false;
                    if ($scope.form.selected_ip.wifi) {
                      $scope.form.wifi.loading = false;
                      $scope.form.selected_ip.wifi = null;
                      $scope.form.wifi.ip_mapper = response;
                    }
                    if ($scope.form.selected_ip.lan) {
                      $scope.form.lan.loading = false;
                      $scope.form.selected_ip.lan = null;
                      $scope.form.lan.ip_mapper = response;
                    }
                  },
                  function(error) {
                    console.log(error);
                  }
                );
            },
            select_user: function(data) {
              $scope.form.selected_user = data;
              $scope.form.model.user_id = $scope.form.selected_user.id;
            },
            select_ip: function(data, meta) {
              console.log('ip-address-mapper', $scope.form.userFreeIpList);
              meta.loading = true;
              var params = {
                  reference_type: "App\\Models\\Admin\\AssetMeta",
                  reference_id: meta.id,
                  user_ip_address_id: data.id
                };

              Restangular.all("ip-address-mapper")
                .post(params)
                .then(
                  function(response) {
                    console.log('ip-address-mapper', response);
                    meta.ip_mapper = response;
                    meta.enableAssigning = false;
                    $scope.form.userFreeIpList.splice(data, 1);
                    console.log('ip-address-mapper', $scope.form.userFreeIpList);
                    
                  },
                  function(error) {
                    console.log(error);
                  }
                );
              
              //$scope.form.selected_ip.wifi = data;
            },
            select_lan_ip: function(data, metaId) {
              $scope.form.assignIp(meta);
              $scope.form.userFreeIpList.splice(data, 1);
              //$scope.form.selected_ip.lan = data;
            },
            cancel: function(item) {
              item.assigning = false;
              $scope.form.selected_ip.wifi = null;
              $scope.form.selected_ip.lan = null;
            }
          };
          $scope.form.init();
        }
      ]
    };
  }
]);
