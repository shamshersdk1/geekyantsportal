"use strict";

angular.module("myApp").directive("techstack", [
  function() {
    return {
      templateUrl: "/scripts/directives/tech-stack/index.html",
      restrict: "E",
      replace: true,
      scope: {
        userId: "=userid"
      },
      controller: [
        "$scope",
        "Restangular",
        function($scope, Restangular) {
          $scope.form = {
            loading: false,
            error: false,
            message: null,
            submitted: false,
            data: [],
            checkbox: null,
            model: {
              title: null,
              description: null,
              user_id: $scope.userId,
              message: null,
              icon: ""
            },
            errors: {
              error: false,
              title: false,
              description: false,
              error_message: null
            },
            getMeta: function() {},
            checkboxUpdate: function() {
              var params = {
                data: $scope.form.model
              };

              var obj = Restangular.one(
                "user-visibility-setting",
                $scope.form.model.user_id
              );
              obj.status = $scope.form.checkbox ? "0" : "1";
              obj.title = "tech-stack";
              obj.put().then(
                function(response) {
                  $scope.form.checkbox = response.tech_stack ? true : false;
                },
                function(error) {
                  $scope.form.loading = false;
                  $scope.form.errors.error_message = errors.data.message;
                }
              );
            },
            init: function() {
              $scope.form.loading = true;
              $scope.form.getMeta();
              Restangular.one(
                "user-visibility-setting",
                $scope.form.model.user_id
              )
                .get()
                .then(
                  function(response) {
                    $scope.form.checkbox = response.tech_stack ? true : false;
                  },
                  function(errors) {
                    $scope.form.loading = false;
                    $scope.form.errors.error_message = errors.data.message;
                    console.log(error);
                  }
                );
            }
          };
          $scope.form.init();
        }
      ]
    };
  }
]);
