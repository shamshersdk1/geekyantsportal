"use strict()";

/*Directives*/
angular
    .module("myApp")
    .directive("resourceInfo", [
        function () {
            return {
                templateUrl: "/scripts/directives/resource-info/resource-info.html?v=1",
                restrict: "E",
                replace: true,
                scope: {
                    projectId: "=",
                    isDisabled: "@isDisabled",
                },
                controller: ["$scope","Restangular" ,function (
                    $scope,
                    Restangular
                ) {
                    $scope.projectDetails = [];
                    
                    
                    $scope.getProjectDetails = function () {
                        $scope.listLoading = true;
                        Restangular.all("project-resource-info")
                            .customGET("", {
                                "project_id": $scope.projectId,
                            })
                            .then(function (response) {
                                $scope.projectDetails = response.data;
                            },function (error) {
                                console.log(error);
                            }
                        );
                    };
                    $scope.getProjectDetails();                    
                }]
            };
        }
    ]);