"use strict()";

/*Directives*/
angular
  .module("myApp")
  .directive("clientDetail", [
    function() {
      return {
        templateUrl: "/scripts/directives/client-detail/client-detail.html?v=1",
        restrict: "E",
        replace: true,
        scope: {
          projectId: "=",
          companyId: "=",
          isDisabled: "@isDisabled"
        },
        controller: ["$scope", "Restangular", function($scope, Restangular) {
          $scope.clientData;

          $scope.loadClientData = function() {
            $scope.clientData;
            $scope.loading = true;
            Restangular.one("project-client", $scope.projectId)
              .get()
              .then(
                function(response) {
                  $scope.loading = false;
                  $scope.clientData = response;
                },
                function(error) {
                  console.log(error);
                }
              );
          };
          $scope.loadClientData();
        }]
      };
    }
  ]);
