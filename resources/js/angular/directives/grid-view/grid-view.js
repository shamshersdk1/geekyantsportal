"use strict";

/*Directives*/
angular
  .module("myApp")
  .directive("compileThis", function($compile) {
    return {
      template: "<span></span>",
      restrict: "E",
      replace: true,
      link: function(scope, iElement, iAttrs) {
        iElement.html(iAttrs.value);
        $compile(iElement)(scope);
      }
    };
  })
  .directive("gridView", [
    "$parse",
    "CONFIG",
    function($parse, CONFIG) {
      return {
        templateUrl:
          "/scripts/directives/grid-view/grid-view.html?v=" +
          window.app_version,
        restrict: "E",
        replace: true,
        scope: {
          model: "=",
          _columns: "@columns",
          _actions: "@actions",
          _showSearch: "@showSearch",
          _showDate: "@showDate",
          _showStartDate: "@showStartDate",
          _showEndDate: "@showEndDate",
          _showDateRange: "@showDateRange",
          _showDownloadCsv: "@showDownloadCsv",
          _filters: "@filters",
          control: "=?",
          extraSearchFieldsData: "=?",
          gridData: "=?"
        },
        controller: function(
          $scope,
          Restangular,
          $location,
          $parse,
          $filter,
          $log
        ) {
          $scope.dateOptions = {
            "year-format": "'yy'",
            "starting-day": 1
          };
          $scope.current_date = new Date();
          $scope.stringToDate = function (dateString) {
            let date = new Date(dateString);
            return date;
          }

          $scope.openStartDateAction = function() {
            setTimeout(function() {
              $scope.$apply(function() {
                $scope.openStartDate = true;
              });
            }, 0);
          };
          $scope.openEndDateAction = function() {
            setTimeout(function() {
              $scope.$apply(function() {
                $scope.openEndDate = true;
              });
            }, 0);
          };

          $scope.openFromDateAction = function() {
            setTimeout(function() {
              $scope.$apply(function() {
                $scope.openFromDate = true;
              });
            }, 0);
          };

          $scope.openToDateAction = function() {
            setTimeout(function() {
              $scope.$apply(function() {
                $scope.openToDate = true;
              });
            }, 0);
          };

          $scope.exportCsvAction = function() {
            makeRequest(true);
          };
          $scope.resetDownloads = function() {
            $scope.pdfLocation = null;
            $scope.csvLocation = null;
            $scope.generatingPdf = false;
            $scope.exportingCsv = false;
            $scope.noRecords = false;
          };
          $scope.filteredDate = function (input_date) {
            var formatted_date = new Date(input_date);
            return formatted_date;
          };
          $scope.columns = $scope.$parent.$eval($scope._columns);
          $scope.actions = $scope.$parent.$eval($scope._actions);
          $scope.filters = $scope.$parent.$eval($scope._filters);

          if (typeof $scope._showSearch == "undefined") {
            $scope.showSearch = true;
          } else {
            $scope.showSearch = $scope.$parent.$eval($scope._showSearch);
          }

          if (typeof $scope._showDate == "undefined") {
            $scope.showDate = false;
          } else {
            $scope.showDate = $scope.$parent.$eval($scope._showDate);
          }
          if (typeof $scope._showStartDate == "undefined") {
            $scope.showStartDate = false;
          } else {
            $scope.showStartDate = $scope.$parent.$eval($scope._showStartDate);
          }
          if (typeof $scope._showEndDate == "undefined") {
            $scope.showEndDate = false;
          } else {
            $scope.showEndDate = $scope.$parent.$eval($scope._showEndDate);
          }

          if (typeof $scope._showDateRange == "undefined") {
            $scope.showDateRange = false;
          } else {
            $scope.showDateRange = $scope.$parent.$eval($scope._showDateRange);
          }

          if (typeof $scope._showDownloadCsv == "undefined") {
            $scope.showDownloadCsv = false;
          } else {
            $scope.showDownloadCsv = $scope.$parent.$eval(
              $scope._showDownloadCsv
            );
          }

          $scope.unexpected = false;
          $scope.fetching = false;

          $scope.pagination = {};
          $scope.requestParams = {
            pageNumber: 1,
            q: "",
            perPage: 100,
            date: "",
            startDate: "",
            endDate: "",
            startTime: new Date(0, 0, 0, 0, 0),
            endTime: new Date(0, 0, 0, 0, 0)
          };
          $scope.pdfLocation = null;
          $scope.generatingPdf = false;
          $scope.noRecords = false;

          var makeRequest = _.debounce(function(exportToCsv) {
            $scope.csvLocation = null;

            var pageNumber = $scope.requestParams.pageNumber;

            var baseUrl = $scope.model;
            var sendParams = {};

            if ($scope.data && $scope.data.metaData)
              sendParams["page"] = $scope.requestParams.pageNumber;

            if ($scope.requestParams.q)
              sendParams["q"] = $scope.requestParams.q;

            if ($scope.requestParams.startDate) {
              var d = $scope.requestParams.startDate;
              sendParams["startDate"] =
                d.getFullYear() +
                "-" +
                ("00" + (d.getMonth() + 1)).slice(-2) +
                "-" +
                ("00" + d.getDate()).slice(-2);
              if ($scope.requestParams.startTime) {
                var t = $scope.requestParams.startTime;
                sendParams["startDate"] =
                  sendParams["startDate"] +
                  " " +
                  ("00" + t.getHours()).slice(-2) +
                  ":" +
                  ("00" + t.getMinutes()).slice(-2) +
                  ":00";
              } else {
                sendParams["startDate"] = sendParams["startDate"] + " 00:00:00";
              }
            }
            if ($scope.requestParams.endDate) {
              var d = $scope.requestParams.endDate;
              sendParams["endDate"] =
                d.getFullYear() +
                "-" +
                ("00" + (d.getMonth() + 1)).slice(-2) +
                "-" +
                ("00" + d.getDate()).slice(-2);
              if ($scope.requestParams.endTime) {
                var t = $scope.requestParams.endTime;
                sendParams["endDate"] =
                  sendParams["endDate"] +
                  " " +
                  ("00" + t.getHours()).slice(-2) +
                  ":" +
                  ("00" + t.getMinutes()).slice(-2) +
                  ":00";
              } else {
                sendParams["endDate"] = sendParams["endDate"] + " 00:00:00";
              }
            }

            if ($scope.showDateRange) {
              if ($scope.requestParams.fromDate) {
                var fd = $scope.requestParams.fromDate;
                sendParams["from_date"] =
                  fd.getFullYear() +
                  "-" +
                  ("00" + (fd.getMonth() + 1)).slice(-2) +
                  "-" +
                  ("00" + fd.getDate()).slice(-2);
              }

              if ($scope.requestParams.toDate) {
                var td = $scope.requestParams.toDate;

                sendParams["to_date"] =
                  td.getFullYear() +
                  "-" +
                  ("00" + (td.getMonth() + 1)).slice(-2) +
                  "-" +
                  ("00" + td.getDate()).slice(-2);
              }
            }

            if ($scope.showDate) {
              if ($scope.requestParams.date) {
                var td = $scope.requestParams.date;

                sendParams["on_date"] =
                  td.getFullYear() +
                  "-" +
                  ("00" + (td.getMonth() + 1)).slice(-2) +
                  "-" +
                  ("00" + td.getDate()).slice(-2);
              }
            }

            if ($scope.requestParams.perPage)
              sendParams["per_page"] = $scope.requestParams.perPage;

            angular.forEach($scope.extraSearchFieldsData, function(val) {
              sendParams[val.paramName] = $scope.requestParams[val.paramName];
            });

            sendParams["pagination"] = 1;

            if (exportToCsv) sendParams["export_to_csv"] = true;

            sendParams = $.extend(true, sendParams, $scope.filters);

            if (exportToCsv) {
              $scope.noRecords = false;
              $scope.exportingCsv = true;
              Restangular.all(baseUrl)
                .customGET("", sendParams)
                .finally(function() {
                  $scope.exportingCsv = false;
                })
                .then(
                  function(success) {
                    //$scope.csvLocation = "http://cosmoparking.local.geekydev.com/download?q="/*;Config.BASEURL*/+success.location;
                    $scope.csvLocation = CONFIG.DOWNLOADURL + success.location;
                  },
                  function(error) {
                    $scope.noRecords = true;
                    // ErrorHandler(error);
                  }
                );
            } else {
              $scope.loading = true;

              Restangular.all(baseUrl)
                .getList(sendParams)
                .finally(function() {
                  $scope.loading = false;
                })
                .then(
                  function(success) {
                    $scope.gridData = success;

                    $scope.qSent = sendParams["filter"];

                    $scope.data = success;
                    $scope.pagination.from =
                      (pageNumber - 1) * $scope.data.metaData.per_page + 1;

                    if (
                      pageNumber * $scope.data.metaData.per_page <
                      $scope.data.metaData.total
                    ) {
                      $scope.pagination.to =
                        pageNumber * $scope.data.metaData.per_page;
                    } else {
                      $scope.pagination.to = $scope.data.metaData.total;
                    }
                    angular.forEach($scope.data, function(o) {
                      // if(o.start_date) {
                      //  o.start_date = new Date(o.start_date * 1000);
                      //  o.start_date = $scope.getFormatedDate(o.start_date);
                      // }
                      // if(o.end_date) {
                      //  o.end_date = new Date(o.end_date * 1000);
                      //  o.end_date = $scope.getFormatedDate(o.end_date);
                      // }
                      if (o.status == 1) o.status = true;
                      if (o.enabled == 1) o.enabled = true;
                      if (o.is_available == 1) o.is_available = true;
                    });
                  },
                  function(error) {
                    $log.error("Oops", "Some error occured, please try again");
                  }
                );
            }
          }, 50);

          $scope.control = {
            remove: function(id) {
              var index = _.findIndex($scope.data, function(row) {
                return row.id == id;
              });

              if (index !== -1) $scope.data.splice(index, 1);
            }
          };

          $scope.changePage = function() {
            $scope.requestParams.pageNumber = $scope.data.metaData.current_page;
            makeRequest();
          };

          $scope.doSearch = function() {
            $scope.requestParams.pageNumber = 1;
            makeRequest();
          };

          $scope.$watch("requestParams.startDate", function(newVal, oldVal) {
            makeRequest();
          });
          $scope.$watch("requestParams.endDate", function(newVal, oldVal) {
            makeRequest();
          });
          $scope.$watch("requestParams.startTime", function(newVal, oldVal) {
            makeRequest();
          });
          $scope.$watch("requestParams.endTime", function(newVal, oldVal) {
            makeRequest();
          });

          $scope.$watch("requestParams.fromDate", function(newVal, oldVal) {
            makeRequest();
          });

          $scope.$watch("requestParams.toDate", function(newVal, oldVal) {
            makeRequest();
          });

          $scope.perPageChange = function() {
            $scope.requestParams.pageNumber = 1;
            makeRequest();
          };

          $scope.hasAction = function(action) {
            var hasIt = false;
            angular.forEach($scope.actions, function(val, key) {
              if (val.type == action) hasIt = true;
            });

            return hasIt;
          };

          function commonAction(action, row, that) {
            var actionObj = _.find($scope.actions, { type: action });

            if (!actionObj || (!actionObj.url && !actionObj.action)) {
              var l = $location.path();

              if (/list$/.test(l)) l = l.substr(0, l.length - 4);

              if (l.substr(-1) != "/") l += "/";

              $location.path(l + action + "/" + row.id);
            } else {
              if (actionObj.url) $location.path(that.$eval(actionObj.url));

              if (actionObj.action) {
                var fn = $parse(actionObj.action);
                fn($scope.$parent, { row: row });
              }
            }
          }

          $scope.addNew = function(row) {
            var actionObj = _.find($scope.actions, { type: "addNew" });
          };

          $scope.viewAction = function(row) {
            commonAction("view", row, this);
          };

          $scope.refreshAction = function(row) {
            commonAction("refresh", row, this);
          };

          $scope.downAction = function(row) {
            commonAction("down", row, this);
          };

          $scope.editAction = function(row) {
            commonAction("edit", row, this);
          };
          $scope.getFormatedDate = function(date) {
            var monthNames = [
              "January",
              "February",
              "March",
              "April",
              "May",
              "June",
              "July",
              "August",
              "September",
              "October",
              "November",
              "December"
            ];

            var d = new Date();

            var newDate = new Date(date);
            newDate.setHours(newDate.getHours());
            newDate.setMinutes(newDate.getMinutes());
            var day = ("0" + newDate.getDate()).slice(-2);
            var month = newDate.getMonth();
            var hours = newDate.getHours();
            var minutes = newDate.getMinutes();
            var meridiem = hours >= 12 ? "PM" : "AM";
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? "0" + minutes : minutes;
            newDate =
              monthNames[month] +
              " " +
              day +
              ", " +
              newDate.getFullYear() +
              " - " +
              hours +
              ":" +
              minutes +
              " " +
              meridiem;
            return newDate;
          };
          $scope.deleteAction = function(row) {
            var actionObj = _.find($scope.actions, { type: "delete" });

            if (!actionObj || (!actionObj.url && !actionObj.action)) {
              var sure = confirm("Do you really want to delete this" + "?");
              if (sure) {
                row
                  .remove()
                  .finally(function() {})
                  .then(
                    function() {
                      var index = $scope.data.indexOf(row);
                      $scope.data.splice(index, 1);

                      var msg = "Item with ID" + " " + row.id + " " + "deleted";
                      $log.info(msg);
                    },
                    function() {
                      var msg =
                        "Item with ID" +
                        " " +
                        row.id +
                        " " +
                        "could not be deleted";
                      $log.error(msg);
                    }
                  );
              }
            } else {
              commonAction("delete", row, this);
            }
          };

          $scope.starAction = function(row) {
            commonAction("star", row, this);
          };

          makeRequest();
          $scope.downloadPdf = function() {
            $scope.noRecords = false;
            $scope.generatingPdf = true;
            var startTime = null;
            var endTime = null;
            var href = window.location.href.split("?")[0];
            var day = href.substring(href.lastIndexOf("/") + 1);
            if (day == "today") {
              var d = new Date();
              startTime =
                d.getFullYear() +
                "-" +
                ("00" + (d.getMonth() + 1)).slice(-2) +
                "-" +
                ("00" + d.getDate()).slice(-2) +
                " 00:00:00";
              endTime =
                d.getFullYear() +
                "-" +
                ("00" + (d.getMonth() + 1)).slice(-2) +
                "-" +
                ("00" + d.getDate()).slice(-2) +
                " 23:59:59";
            } else {
              if (day == "tomorrow") {
                var d = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
                startTime =
                  d.getFullYear() +
                  "-" +
                  ("00" + (d.getMonth() + 1)).slice(-2) +
                  "-" +
                  ("00" + d.getDate()).slice(-2) +
                  " 00:00:00";
                endTime =
                  d.getFullYear() +
                  "-" +
                  ("00" + (d.getMonth() + 1)).slice(-2) +
                  "-" +
                  ("00" + d.getDate()).slice(-2) +
                  " 23:59:59";
              } else {
                if ($scope.requestParams.startDate) {
                  var d = $scope.requestParams.startDate;
                  startTime =
                    d.getFullYear() +
                    "-" +
                    ("00" + (d.getMonth() + 1)).slice(-2) +
                    "-" +
                    ("00" + d.getDate()).slice(-2);
                  if ($scope.requestParams.startTime) {
                    var t = $scope.requestParams.startTime;
                    startTime =
                      startTime +
                      " " +
                      ("00" + t.getHours()).slice(-2) +
                      ":" +
                      ("00" + t.getMinutes()).slice(-2) +
                      ":00";
                  } else {
                    startTime = startTime + " 00:00:00";
                  }
                }
                if ($scope.requestParams.endDate) {
                  var d = $scope.requestParams.endDate;
                  endTime =
                    d.getFullYear() +
                    "-" +
                    ("00" + (d.getMonth() + 1)).slice(-2) +
                    "-" +
                    ("00" + d.getDate()).slice(-2);
                  if ($scope.requestParams.endTime) {
                    var t = $scope.requestParams.endTime;
                    endTime =
                      endTime +
                      " " +
                      ("00" + t.getHours()).slice(-2) +
                      ":" +
                      ("00" + t.getMinutes()).slice(-2) +
                      ":00";
                  } else {
                    endTime = endTime + " 00:00:00";
                  }
                }
              }
            }
            var params = { startTime: startTime, endTime: endTime };
            Restangular.all("")
              .customGET("download-records", params)
              .finally(function() {
                $scope.loading = false;
                $scope.generatingPdf = false;
              })
              .then(
                function(success) {
                  $scope.pdfLocation = CONFIG.DOWNLOADPDFURL + success;
                },
                function(error) {
                  $scope.noRecords = true;
                  // $log.error("Oops", "Some error occured, please try again");
                }
              );
          };
        },
        link: function(scope, iElement, iAttrs) {
          scope.openModal = function(row) {
            var fn = $parse(iAttrs.openModal);
            fn(scope.$parent, { row: row });
          };

          scope.switchOnoff = function(row) {
            var fn = $parse(iAttrs.switchOnoff);
            fn(scope.$parent, { row: row });
          };
        }
      };
    }
  ]);
