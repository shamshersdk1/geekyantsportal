"use strict()";

/*Directives*/
angular
    .module("myApp")
    .directive("addCronReminder", [
        function () {
            return {
                templateUrl: "/scripts/directives/add-cron-reminder/add-cron-reminder.html?v=1",
                restrict: "E",
                replace: true,
                scope: {
                    referenceId: "=",
                    isDisabled: "@isDisabled",
                    referenceType: "@referenceType",
                    jobHandler: "@jobHandler",
                },
                controller: function (
                    $scope,
                    Restangular,
                    $uibModal
                ) {
                    $scope.billingScheduleDetails = [];
                    $scope.getBillingScheduleDetails = function () {
                        $scope.listLoading = true;
                        Restangular.all("cron")
                            .customGET("", {
                                "reference_id": $scope.referenceId,
                                "reference_type": $scope.referenceType,
                                "job_handler": $scope.jobHandler,
                            })
                            .then(function (response) {
                                $scope.billingScheduleDetails = response;
                            },function (error) {
                                console.log(error);
                            }
                        );
                    };
                    $scope.getBillingScheduleDetails();
                    
                    $scope.edit = function () {
                        $scope.list = {
                            reference_id: $scope.referenceId,
                            reference_type: $scope.referenceType,
                            job_handler: $scope.jobHandler,
                            cron_details : $scope.billingScheduleDetails,
                        };
                        var modal = $uibModal.open({
                            controller: "addCronModalCtrl",
                            windowClass: "bootstrap_wrap",
                            templateUrl: "/views/cron/modal.html",
                            backdrop: "static",
                            size: "md",
                            resolve: {
                                data: function () {
                                    return $scope.list;
                                }
                            }
                        });
                        modal.result.then(function (res) { $scope.getBillingScheduleDetails(); }, function (err) { $scope.getBillingScheduleDetails(); });
                    };
                }
            };
        }
    ]);