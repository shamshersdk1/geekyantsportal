angular.module('myApp')
    .controller('addCronModalCtrl', [ "$scope", "Restangular", "data", "$uibModalInstance" , function ($scope, Restangular, data, $uibModalInstance) {
        $scope.editFlag = data.cron_details.id ? true : false;
        $scope.repeat_list = [
            { value : "daily", name : "Daily" },
            { value : "weekly", name : "Weekly" },
            { value : "monthly", name : "Monthly" }
        ];
        $scope.event_type =  data.cron_details.event_type ? data.cron_details.event_type : 'once';
        $scope.repeat_type = data.cron_details.repeat_type ? data.cron_details.repeat_type : 'daily';
        $scope.hours = data.cron_details.hours ? data.cron_details.hours : null;
        $scope.minutes = data.cron_details.minutes ? data.cron_details.minutes : null;
        var reset = function() {
            $scope.selected_dates = {
                '1' : false, '2' : false, '3' : false, '4' : false, '5' : false, '6' : false, '7' : false, '8' : false, '9' : false, '10' : false, 
                '11' : false, '12' : false, '13' : false, '14' : false, '15' : false, '16' : false, '17' : false, '18' : false, '19' : false, '20' : false, '21' : false, 
                '22' : false, '23' : false, '24' : false, '25' : false, '26' : false, '27' : false, '28' : false, '29' : false, '30' : false, '31' : false,
            };
            $scope.selected_days = {
                'sun' : false,
                'mon' : false,
                'tue' : false,
                'wed' : false,
                'thu' : false,
                'fri' : false,
                'sat' : false,
            };
            $scope.schedule_date = '';
            $scope.ends_on = 'never';
            $scope.number_of_occurrences = null;
            $scope.ends_on_date = '';
        };
        $scope.repeatTypeChange = function () {
            reset();
        };
        $scope.selected_days = {
            'sun' : false,
            'mon' : false,
            'tue' : false,
            'wed' : false,
            'thu' : false,
            'fri' : false,
            'sat' : false,
        };
        if ( data.cron_details.day_of_week && data.cron_details.day_of_week.length > 0 )
        {
            if(data.cron_details.day_of_week.indexOf("0") !== -1) {
                $scope.selected_days.sun = true;
            }
            if(data.cron_details.day_of_week.indexOf("1") !== -1) {
                $scope.selected_days.mon = true;
            }
            if(data.cron_details.day_of_week.indexOf("2") !== -1) {
                $scope.selected_days.tue = true;
            }
            if(data.cron_details.day_of_week.indexOf("3") !== -1) {
                $scope.selected_days.wed = true;
            }
            if(data.cron_details.day_of_week.indexOf("4") !== -1) {
                $scope.selected_days.thu = true;
            }
            if(data.cron_details.day_of_week.indexOf("5") !== -1) {
                $scope.selected_days.fri = true;
            }
            if(data.cron_details.day_of_week.indexOf("6") !== -1) {
                $scope.selected_days.sat = true;
            }   
        }
        $scope.selected_dates = {
            '1' : false, '2' : false, '3' : false, '4' : false, '5' : false, '6' : false, '7' : false, '8' : false, '9' : false, '10' : false, 
            '11' : false, '12' : false, '13' : false, '14' : false, '15' : false, '16' : false, '17' : false, '18' : false, '19' : false, '20' : false, '21' : false, 
            '22' : false, '23' : false, '24' : false, '25' : false, '26' : false, '27' : false, '28' : false, '29' : false, '30' : false, '31' : false,
        };
        if ( data.cron_details.day_of_month && data.cron_details.day_of_month.length > 0 )
        {
            for(var i = 1; i < 32; i++ )
            {
                if(data.cron_details.day_of_month.indexOf(i.toString()) !== -1) {
                    $scope.selected_dates[i] = true;
                }  
            }
        }
        $scope.ends_on = 'never';
        $scope.ends_on_date = data.cron_details.ends_on ? new Date(data.cron_details.ends_on ) : '' ;
        if ( $scope.ends_on_date != '' )
        {
            $scope.ends_on = 'on_date';
        }
        $scope.number_of_occurrences = data.cron_details.occurrence_counter ? data.cron_details.occurrence_counter : null;
        if ( $scope.number_of_occurrences !== null )
        {
            $scope.ends_on = 'occurrence';
        }
        $scope.endOptionUpdated = function() {
            $scope.endsOnError = false;
            if( $scope.ends_on == 'never' )
            {
                $scope.ends_on_date = '';
                $scope.number_of_occurrences = null;
            } else if ( $scope.ends_on == 'on_date' ) {
                $scope.number_of_occurrences = null;
            } else {
                $scope.ends_on_date = '';
            }
        };
        $scope.error = false;
        $scope.openDateFlag = false;
        $scope.schedule_date = '';
        if ( data.cron_details.month_of_year )
        {
            $scope.schedule_date = new Date();
            $scope.schedule_date.setMonth(data.cron_details.month_of_year - 1);
            $scope.schedule_date.setDate(data.cron_details.day_of_month[0]);
        }
        $scope.openEndDateFlag = false;
        $scope.mytime = new Date();
        $scope.mytime.setHours($scope.hours, $scope.minutes);
        $scope.hstep = 1;
        $scope.mstep = 15;
        $scope.tomorrow = new Date();
        $scope.tomorrow.setDate($scope.tomorrow.getDate() + 1);
        $scope.dateOptions = {
            minDate: new Date()
          };
        $scope.endsOnDateOptions = {
            minDate: $scope.tomorrow
          };  
        
        $scope.openDate = function () {
            $scope.openDateFlag = true;  
        };

        $scope.openEndDate = function () {
            $scope.openEndDateFlag = true;  
        };

        $scope.validateTime = function() {
            if ( !$scope.mytime )
            {
                $scope.timeError = true;
                $scope.error = true;
            }   
        };

        $scope.validateScheduleDate = function() {
            if ( !$scope.schedule_date || $scope.schedule_date == '' )
            {
                $scope.scheduleDateError = true;
                $scope.error = true;
            }
        };

        $scope.validateEndsOn = function() {
            if ( $scope.ends_on == 'on_date' && $scope.ends_on_date == '' )
            {
                $scope.endsOnError = true;
                $scope.error = true;
            }
            if ( $scope.ends_on == 'occurrence' && !$scope.number_of_occurrences )
            {
                $scope.endsOnError = true;
                $scope.error = true;
            }
        };

        $scope.validateSelectedDays = function() {
            if ( !$scope.selected_days.sun && !$scope.selected_days.mon && !$scope.selected_days.tue && !$scope.selected_days.wed && 
                 !$scope.selected_days.thu && !$scope.selected_days.fri && !$scope.selected_days.sat )
            {
                $scope.selectedDaysError = true;
                $scope.error = true;
            }
        };

        $scope.validateDateOfMonths = function() {
            $scope.selectedDatesError = true;
            $scope.error = true;
            for(var i = 1; i < 32; i++ )
            {
                if($scope.selected_dates[i]) {
                    $scope.selectedDatesError = false;
                    $scope.error = false;
                }  
            } 
        };
        $scope.validations = function () {
            if ( $scope.event_type == 'once' )
            {
                $scope.validateTime();
                $scope.validateScheduleDate();
            }
            else
            {
                if ( $scope.repeat_type == 'daily' )
                {
                    $scope.validateTime();
                    $scope.validateEndsOn();

                } else if ( $scope.repeat_type == 'weekly' ) {
                    $scope.validateTime();
                    $scope.validateSelectedDays();
                    $scope.validateEndsOn();
                } else {
                    $scope.validateTime();
                    $scope.validateDateOfMonths();
                    $scope.validateEndsOn();
                }

            }
        };
        $scope.save = function () {
            $scope.error = false;
            $scope.validations();
            if( $scope.error )
            {
                console.log('Error');
                return;
            }
            $scope.loading = true;
            var end_date = null;
            if ( $scope.ends_on_date )
            {
                end_date = moment($scope.ends_on_date).format("YYYY-MM-DD");
            }
            var sch_date = null;
            if ( $scope.schedule_date )
            {
                sch_date = moment($scope.schedule_date).format("YYYY-MM-DD");
            }
            var minutes =  moment($scope.mytime).format("mm");
            var hours =  moment($scope.mytime).format("HH");
            var params = {
                'reference_id' : data.reference_id,
                'reference_type' : data.reference_type,
                'job_handler' : data.job_handler,
                'event_type' : $scope.event_type,
                'repeat_type' : $scope.repeat_type,
                'hours' : hours,
                'minutes' : minutes,
                'selected_days' : $scope.selected_days,
                'selected_dates' : $scope.selected_dates,
                'ends_on' : $scope.ends_on,
                'ends_on_date' :  end_date,
                'schedule_date' : sch_date,
                'number_of_occurrences' : $scope.number_of_occurrences,
            }
            console.log(params);
            if ( $scope.editFlag )
            {
                Restangular.one("cron", data.cron_details.id)
                                .customPUT({
                                    params: params
                                })
                                .then(
                                    function (response) {
                                        $scope.loading = false;
                                        var return_data = response;
                                        $uibModalInstance.close(return_data);
                                    },
                                    function (errors) {
                                        $scope.loading = false;
                                        console.log(errors);
                                    }
                                );
            }
            else
            {
                Restangular.all("cron")
                            .post(params)
                            .then(function (response) {
                                $scope.loading = false;
                                $uibModalInstance.close(12);
                            }, function (error) {
                                $scope.loading = false;
                                console.log(error);
                            });
            }
        };     

        $scope.cancel = function () {
            $uibModalInstance.dismiss(34);
        };
    }]);