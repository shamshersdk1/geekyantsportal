"use strict";

/*Directives*/
angular.module("myApp").directive("userEducation", [
  function() {
    return {
      templateUrl: "/scripts/directives/user-education/index.html",
      restrict: "E",
      replace: true,
      scope: {
        userId: "=userid"
      },
      controller: [
        "$scope",
        "Restangular",
        function($scope, Restangular) {
          $scope.form = {
            loading: false,
            error: false,
            message: null,
            submitted: false,
            data: [],
            checkbox: null,
            model: {
              uid: null,
              user_id: $scope.userId,
              message: null
            },
            errors: {
              error: false,
              title: false,
              description: false,
              error_message: null
            },
            getMeta: function() {},
            checkboxUpdate: function() {
              var params = {
                data: $scope.form.model
              };

              var obj = Restangular.one(
                "user-visibility-setting",
                $scope.form.model.user_id
              );
              obj.status = $scope.form.checkbox ? "0" : "1";
              obj.title = "education";
              obj.put().then(
                function(response) {
                  $scope.form.checkbox = response[0].education ? true : false;
                },
                function(error) {
                  $scope.form.loading = false;
                  $scope.form.errors.error_message = errors.data.message;
                  console.log("ERR", error);
                }
              );
            },
            visibilityUpdate: function($id, $index) {
              var obj = Restangular.one("user-educations", $id);
              obj.status = $scope.form.data[$index].visibility;
              obj.put().then(
                function(resp) {},
                function(error) {
                  $scope.form.loading = false;
                  $scope.form.errors.error_message = errors.data.message;
                  console.log("ERR", error);
                }
              );
            },
            init: function() {
              $scope.form.loading = true;
              $scope.form.getMeta();
              Restangular.one(
                "user-visibility-setting",
                $scope.form.model.user_id
              )
                .get()
                .then(
                  function(response) {
                    $scope.form.checkbox = response[0].education ? true : false;
                    console.log(response[0].education);
                  },
                  function(errors) {
                    $scope.form.loading = false;
                    $scope.form.errors.error_message = errors.data.message;
                    console.log(error);
                  }
                );

              Restangular.one("user-educations", $scope.form.model.user_id)
                .get()
                .then(
                  function(response) {
                    $scope.form.loading = false;
                    $scope.form.data = response;
                    console.log("first", $scope.form.data);
                    console.log($scope.form.data[0].id);
                  },
                  function(errors) {
                    $scope.form.loading = false;
                    $scope.form.errors.error_message = errors.data.message;
                    console.log(error);
                  }
                );
            }
          };
          $scope.form.init();
        }
      ]
    };
  }
]);
