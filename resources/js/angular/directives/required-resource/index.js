"use strict()";

/*Directives*/
angular
    .module("myApp")
    .directive("requiredResource", [
        function () {
            return {
                templateUrl: "/scripts/directives/required-resource/index.html?v=1",
                restrict: "E",
                replace: true,
                scope: {
                    projectId: "=",
                    isDisabled: "@isDisabled",
                },
                controller: ["$scope", "Restangular", function (
                    $scope,
                    Restangular
                ) {
                    $scope.newRequiredResources = [];
                    $scope.oldResources = [];
                    $scope.editable = [];
                    $scope.error = false;
                    $scope.resourceError = false;
                    $scope.priceError = false;
                    $scope.newResouceObj = {
                        'type' : '',
                        'price' : '',
                    };
                    
                    $scope.loadRequiredResources = function () {
                        $scope.oldResources = [];
                        $scope.listLoading = true;
                        Restangular.all("resource-price")
                            .customGET("", {
                                "project_id": $scope.projectId,
                            })
                            .then(function (response) {
                                $scope.listLoading = false;
                                for (i = 0; i < response.length; i++) {
                                    $scope.oldResources.push(response[i]);
                                }
                            },function (error) {
                                console.log(error);
                            }
                        );
                    };
                    $scope.loadRequiredResources();
                    
                    $scope.addResourceRow = function() {
                        $scope.error = false;
                        if ( !$scope.newResouceObj.type )
                        {
                            $scope.resourceError = true;
                            $scope.error = true;
                        }   else {
                            $scope.resourceError = false;
                        }
                        
                        if ( !$scope.newResouceObj.price )
                        {
                            $scope.priceError = true;
                            $scope.error = true;
                        }   else{
                            $scope.priceError = false;
                        }
                        
                        if ( $scope.error )
                        {
                            return;
                        }

                        var params = {
                            'project_id' : $scope.projectId,
                            'type' : $scope.newResouceObj.type,
                            'price' : $scope.newResouceObj.price,
                        };
                        Restangular.all("resource-price")
                            .post(params)
                            .then(function (response) {
                                $scope.newResouceObj = {
                                    'type' : '',
                                    'price' : '',
                                };
                                $scope.loadRequiredResources();
                            }, function (error) {
                                console.log(error);

                            });
                    };
                    $scope.editResourceRow = function(index) {
                        $scope.editable[index] = true;
                    }
                    $scope.saveResourceRow = function(index, resourceRow){
                        $scope.editable[index] = true;
                        Restangular.one("resource-price", resourceRow.id)
                                .customPUT({
                                    resourceRow: resourceRow
                                })
                                .then(
                                    function (response) {
                                        $scope.editable[index] = false;
                                    },
                                    function (errors) {
                                        console.log(errors);
                                    }
                                );
                    };

                    $scope.removeResourceRow = function(index, resourceRow) {
                        var r = confirm("Are you sure you want to delete this entry?");
                        if (r != true) {
                            return;
                        }
                        Restangular.one("resource-price", resourceRow.id)
                            .remove()
                            .then(
                                function(response) {
                                    $scope.oldResources.splice(index, 1);
                                },
                                function(error) {
                                    console.log(error);
                                }
                            );
                    };
                }]
            };
        }
    ]);