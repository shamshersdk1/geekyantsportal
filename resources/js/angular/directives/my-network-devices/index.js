"use strict()";

/*Directives*/
angular
  .module("myApp")
  .directive("myNetworkDevices", [
    function() {
      return {
        templateUrl: "/scripts/directives/my-network-devices/index.html",
        restrict: "E",
        replace: true,
        scope: {
          userId: "=",
          isDisabled: "@isDisabled"
        },
        controller: [
          "$scope",
          "Restangular",
          "$uibModal",
          function($scope, Restangular, $uibModal) {
            var api = "my-network-devices";

            $scope.form = {
              loading: false,
              error: false,
              data: [],
              init: function() {
                $scope.form.loading = true;
                Restangular.all(api)
                  .getList({
                    user_id: $scope.userId,
                    pagination: 0,
                    per_page: 100
                  })
                  .then(
                    function(result) {
                      $scope.form.loading = false;
                      if (result) {
                        $scope.form.data = result;
                      }
                    },
                    function(error) {
                      console.log(error);
                    }
                  );
              },
              showModal: function() {
                var modal = $uibModal.open({
                  controller: "networkDevicesAddModalCtrl",
                  windowClass: "bootstrap_wrap",
                  templateUrl:
                    "/scripts/directives/my-network-devices/add-edit-modal.html",
                  backdrop: "static",
                  size: "md",
                  resolve: {
                    data: function() {
                      return { user_id: $scope.userId };
                    }
                  }
                });
                modal.result.then(
                  function(result) {
                    $scope.form.data.push(result);
                  },
                  function() {
                    //
                  }
                );
              },
              remove: function(item) {
                var r = confirm("Are you sure you want to remove this device?");
                if (r != true) {
                  return;
                }
                Restangular.one(api, item.id)
                  .remove()
                  .then(
                    function(response) {
                      $scope.form.data.splice(item.$index, 1);
                    },
                    function(error) {
                      console.log(error);
                    }
                  );
              },
              showEditModal: function(item) {
                var modal = $uibModal.open({
                  controller: "networkDevicesEditModalCtrl",
                  windowClass: "bootstrap_wrap",
                  templateUrl:
                    "/scripts/directives/my-network-devices/add-edit-modal.html",
                  backdrop: "static",
                  size: "md",
                  resolve: {
                    data: function() {
                      return { device_id: item.id, user_id: $scope.userId };
                    }
                  }
                });
                modal.result.then(
                  function(result) {
                    $scope.form.init();
                  },
                  function() {
                    //
                  }
                );
              }
            };
            $scope.form.init();
          }
        ]
      };
    }
  ])
  .controller("networkDevicesAddModalCtrl", [
    "$scope",
    "Restangular",
    "data",
    "$uibModalInstance",
    function($scope, Restangular, data, $uibModalInstance) {
      var userId = data.user_id;

      $scope.form = {
        loading: false,
        error: false,
        AssignType: "auto",
        errorMsg: "",
        data: {
          name: null,
          mac_address: null,
          user_id: userId,
          user_ip_address_id: null
        },
        init: function() {
          $scope.form.loading = true;
          Restangular.all("user-ip-address/" + userId + "/free-ips")
            .getList()
            .then(
              function(result) {
                $scope.form.loading = false;
                if (result) {
                  $scope.form.userFreeIpList = result;
                }
              },
              function(error) {
                console.log(error);
              }
            );
        },
        cancel: function() {
          $uibModalInstance.dismiss();
        },
        submit: async function($form) {
          $scope.form.submitting = true;

          if (!$form.$valid) {
            $scope.form.error = true;
            return;
          }
          if ($scope.form.AssignType == "auto") {
            if ($scope.form.userFreeIpList.length == 0) {
              $scope.form.error = true;
              $scope.form.errorMsg =
                "No free ip available. Please release an ip and try again.";
              return;
            } else
              $scope.form.data.user_ip_address_id =
                $scope.form.userFreeIpList[0].id;
          }
          Restangular.all("my-network-devices")
            .post($scope.form.data)
            .then(
              function(response) {
                $scope.form.submitting = false;
                $uibModalInstance.close(response);
              },
              function(error) {
                $scope.form.error = true;
                $scope.form.errorMsg = error.data.message;
                return;
                // $uibModalInstance.dismiss();
              }
            );
        },
        select_ip: function(data) {
          $scope.form.selected_ip = data;
          $scope.form.data.user_ip_address_id = $scope.form.selected_ip.id;
        }
      };
      $scope.form.init();
    }
  ])
  .controller("networkDevicesEditModalCtrl", [
    "$scope",
    "Restangular",
    "data",
    "$uibModalInstance",
    "$q",
    function($scope, Restangular, data, $uibModalInstance, $q) {
      var deviceId = data.device_id;
      var userId = data.user_id;

      $scope.form = {
        loading: false,
        error: false,
        errorMsg: "",
        AssignType: "manual",
        data: {
          name: null,
          mac_address: null,
          user_id: userId,
          user_ip_address_id: null
        },
        init: function() {
          var deviceData = Restangular.one(
            "my-network-devices",
            deviceId
          ).get();
          var userFreeIpList = Restangular.all(
            "user-ip-address/" + userId + "/free-ips"
          ).getList();

          $q.all([
            deviceData,
            userFreeIpList,
            ($scope.form.loading = true)
          ]).then(function(result) {
            $scope.form.loading = false;
            if (result[0]) {
              $scope.form.data = result[0];
              if (result[0].ip != null) {
                $scope.form.selected_ip = result[0].ip.user_ip;
                $scope.form.data.user_ip_address_id = result[0].ip.user_ip.id;
              }
            }
            if (result[1]) {
              $scope.form.userFreeIpList = result[1];
            }
          });
        },
        cancel: function() {
          $uibModalInstance.dismiss();
        },
        submit: function($form) {
          $scope.form.submitting = true;
          if (!$form.$valid) {
            $scope.form.error = true;
            return;
          }
          Restangular.all("my-network-devices")
            .customPUT($scope.form.data, deviceId)
            .then(
              function(response) {
                $scope.form.submitting = false;
                $uibModalInstance.close(response);
              },
              function(error) {
                $scope.form.error = true;
                $scope.form.errorMsg =
                  "Unable to save record. Please try again.";
                return;
                // $uibModalInstance.dismiss();
              }
            );
        },
        select_ip: function(data) {
          $scope.form.selected_ip = data;
          $scope.form.data.user_ip_address_id = $scope.form.selected_ip.id;
        }
      };
      $scope.form.init();
    }
  ]);
