"use strict()";

/*Directives*/
angular
  .module("myApp")
  .directive("basicInfo", [
    function() {
      return {
        templateUrl: "/scripts/directives/basic-info/basic-info.html?v=1",
        restrict: "E",
        replace: true,
        scope: {
          projectId: "=",
          isDisabled: "@isDisabled"
        },
        controller: ["$scope", "Restangular", function($scope, Restangular) {
          $scope.projectData;
          $scope.loadProjectData = function() {
            $scope.projectData;
            $scope.loading = true;
            Restangular.one("project", $scope.projectId)
              .get()
              .then(
                function(response) {
                  $scope.loading = false;
                  $scope.projectData = response;
                },
                function(error) {
                  console.log(error);
                }
              );
          };
          $scope.loadProjectData();

          $scope.filteredDate = function(input_date) {
            var formatted_date = new Date(input_date);
            return formatted_date;
          };
        }]
      };
    }
  ]);
