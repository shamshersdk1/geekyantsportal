"use strict()";

/*Directives*/
angular.module("myApp").directive("projectCms", [
  function() {
    return {
      //template: "<h1>Made by a directive!</h1>",

      restrict: "E",
      templateUrl: "/scripts/directives/cms-project-user/index.html",
      replace: true,
      scope: {
        userId: "="
      },
      controller: [
        "$scope",
        "Restangular",
        function($scope, Restangular) {
          $scope.form = {
            loading: false,
            error: false,
            message: null,
            id: null,
            index: null,
            submitted: false,
            selected_project: null,
            data: [],
            data2: [],
            checkbox: null,
            model: {
              selectedProject: null,
              title: null,
              description: null,
              user_id: $scope.userId,
              message: null,
              project_id: null
            },
            errors: {
              error: false,
              title: false,
              description: false,
              error_message: null
            },
            select_project: function(data, obj) {
              console.log(obj);
              console.log($scope.form.data);
              var params = {
                data: $scope.form.model
              };
              Restangular.all("cms-project-user")
                .post(params)
                .then(
                  function(response) {
                    $scope.form.loading = false;

                    $scope.form.data2.push(response[0]);
                    console.log(response);
                  },
                  function(errors) {
                    $scope.form.loading = false;
                    $scope.form.errors.error_message = errors.data.message;
                    console.log(errors);
                  }
                );
              $scope.form.model.selectedProject = data;
              $scope.form.model.project_id = $scope.model.selectedProject.id;
              // console.log("hello");
            },
            getMeta: function() {},
            descClick: function(obj) {
              console.log("desc click");
              console.log(obj);
              $scope.form.id = obj.currentTarget.attributes.data.nodeValue;
              $scope.form.index =
                obj.currentTarget.attributes.data.ownerElement.id;
            },

            descClicked: function() {
              console.log($scope.form.id);
              var obj = Restangular.one(
                "cms-project-user",
                $scope.form.model.user_id
              );
              obj.description = $scope.form.data2[$scope.form.index].role;
              obj.id = $scope.form.id;
              obj.put().then(
                function(response) {
                  $scope.form.data2[$scope.form.index] = response[0];
                },
                function(error) {
                  $scope.form.loading = false;
                  $scope.form.errors.error_message = errors.data.message;
                  console.log("ERR", error);
                }
              );
            },

            checkboxUpdate: function() {
              var params = {
                data: $scope.form.model
              };

              var obj = Restangular.one(
                "user-visibility-setting",
                $scope.form.model.user_id
              );
              obj.status = $scope.form.checkbox ? "0" : "1";
              obj.title = "projects";
              obj.put().then(
                function(response) {
                  $scope.form.checkbox = response[0].projects ? true : false;
                },
                function(error) {
                  $scope.form.loading = false;
                  $scope.form.errors.error_message = errors.data.message;
                  console.log("ERR", error);
                }
              );
            },
            init: function() {
              $scope.form.loading = true;
              $scope.form.getMeta();
              Restangular.one(
                "user-visibility-setting",
                $scope.form.model.user_id
              )
                .get()
                .then(
                  function(response) {
                    $scope.form.checkbox = response[0].projects ? true : false;
                  },
                  function(errors) {
                    $scope.form.loading = false;
                    $scope.form.errors.error_message = errors.data.message;
                    console.log(error);
                  }
                );

              Restangular.one("cms-project")
                .get()
                .then(
                  function(response) {
                    console.log("recieved");
                    $scope.form.loading = false;
                    $scope.form.data = response;
                    //$scope.form.model.selectedProject = response;
                  },
                  function(errors) {
                    $scope.form.loading = false;
                    $scope.form.errors.error_message = errors.data.message;
                    console.log(error);
                  }
                );

              Restangular.one("cms-project-user", $scope.form.model.user_id)
                .get()
                .then(
                  function(response) {
                    console.log(response);
                    $scope.form.loading = false;
                    $scope.form.data2 = response;
                    console.log($scope.form.data2);
                  },
                  function(errors) {
                    $scope.form.loading = false;
                    $scope.form.errors.error_message = errors.data.message;
                    console.log(error);
                  }
                );
            },

            submit: function($form) {
              var params = {
                data: $scope.form.model
              };
              Restangular.all("cms-project-user")
                .post(params)
                .then(
                  function(response) {
                    $scope.form.loading = false;

                    $scope.form.data2.push(response[0]);
                    console.log(response);
                  },
                  function(errors) {
                    $scope.form.loading = false;
                    $scope.form.errors.error_message = errors.data.message;
                    console.log(errors);
                  }
                );
            },
            delete: function(obj) {
              Restangular.one(
                "cms-project-user",
                obj.currentTarget.attributes.data.nodeValue
              )
                .remove()
                .then(
                  function(response) {
                    delete $scope.form.data2[
                      obj.currentTarget.attributes.data.ownerElement.id
                    ];
                  },
                  function(errors) {
                    $scope.form.loading = false;
                    $scope.form.errors.error_message = errors.data.message;
                    console.log(errors);
                  }
                );
            }
          };
          $scope.form.init();
        }
      ]
    };
  }
]);
