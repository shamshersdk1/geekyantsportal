"use strict";

/*Directives*/
angular.module("myApp").directive("techstack", [
  function() {
    return {
      templateUrl: "/scripts/directives/profile-builder/tech-stack/index.html",
      restrict: "E",
      replace: true,
      scope: {
        userId: "=userid"
      },
      controller: [
        "$scope",
        "Restangular",
        function($scope, Restangular) {
          $scope.form = {
            loading: false,
            error: false,
            message: null,
            submitted: false,
            data: [],
            skills:[],
            checkbox: null,
            model: {
              user_id: $scope.userId,
              message: null
            },
            errors: {
              error: false,
              title: false,
              description: false,
              error_message: null
            },
            getMeta: function() {},
            checkboxUpdate: function() {
              var params = {
                data: $scope.form.model
              };

              var obj = Restangular.one(
                "user-visibility-setting",
                $scope.form.model.user_id
              );
              obj.status = $scope.form.checkbox ? "0" : "1";
              obj.title = "social-info";
              obj.put().then(
                function(response) {
                  $scope.form.checkbox = response.social_info ? true : false;
                },
                function(error) {
                  $scope.form.loading = false;
                  $scope.form.errors.error_message = errors.data.message;
                  console.log("ERR", error);
                }
              );
            },
            init: function() {
              $scope.form.loading = true;
              $scope.form.getMeta();
              Restangular.one(
                "user-visibility-setting",
                $scope.form.model.user_id
              )
                .get()
                .then(
                  function(response) {
                    // $scope.form.checkbox = response.social_info ? true : false;
                  },
                  function(errors) {
                    $scope.form.loading = false;
                    $scope.form.errors.error_message = errors.data.message;
                    console.log(error);
                  }
                );
                Restangular.one("skills",$scope.form.model.user_id)
                .get()
                .then(
                  function(response){
                    $scope.form.skills=response;
                    console.log("skillssss",$scope.form.skills[0].skill_name);
                  },
                  function(errors){
                     $scope.form.loading = false;
                    $scope.form.errors.error_message = errors.data.message;
                    console.log(error);
                  }
                )
              Restangular.one("tech-stack", $scope.form.model.user_id)
                .get()
                .then(
                  function(response) {
                    $scope.form.loading = false;
                    $scope.form.data = response;
                    console.log("responsesss", $scope.form.dataq);
                  },
                  function(errors) {
                    $scope.form.loading = false;
                    $scope.form.errors.error_message = errors.data.message;
                    console.log(error);
                  }
                );
            },

            addTag: function($item) {
              // console.log($item);
              const params={
                items:$item,
                id:$scope.form.model.user_id
              }
            
              Restangular.all('skills')
              .post(params)
              .then(
                function(response){
                  console.log(response);
                }
              )

            },
            submit: function($index) {
              // console.log("submit func...");


              console.log($scope.form.model.user_id);

              console.log($scope.form.data[$index].technology.id);
              console.log($scope.form.data[$index].isVisible);

              var params = {
                user_id: $scope.form.model.user_id,
                tech_id: $scope.form.data[$index].technology.id,
                isVisible: $scope.form.data[$index].isVisible
              };


              // $scope.checkvisibility[$index] = "invisible";

              // var visibility = $scope.checkvisibility[$index];
              var obj = Restangular.one(
                "tech-stack",
                $scope.form.model.user_id
              );
              obj.isVisible = $scope.form.data[$index].isVisible;
              obj.tec_id = $scope.form.data[$index].technology.id;

              obj.put().then(function(response) {});
              //   submit: function($index) {
              //     $scope.checkvisibility[$index] = "invisible";
              //     $data = JSON.stringify($scope.social_info);
              //     $id = $scope.form.model.user_id;
              //     console.log("140", $data);
              //     var params = {
              //       data: $data,
              //       id: $id
              //     };
              //     console.log("params", params);
              //     Restangular.all("social-info")
              //       .post(params)
              //       .then(
              //         function(response) {
              //           response.status
              //             ? ($scope.checkstatus[$index] = "fa fa-check")
              //             : ($scope.checkstatus[$index] = "fa fa-exclamation");
              //           $scope.checkvisibility[$index] = "visible";
              //           $scope.form.loading = false;
              //           $scope.form.model.social_json = null;
              //           // $scope.form.model.description = null;
              //           $scope.form.data.push(response);
              //           console.log($scope.form.data);
              //         },
              //         function(errors) {
              //           $scope.form.loading = false;
              //           $scope.form.errors.error_message = errors.data.message;
              //           console.log(errors);
              //         }
              //       );
            }
          };
          $scope.form.init();
        }
      ]
    };
  }
]);
