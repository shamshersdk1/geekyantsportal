"use strict";

/*Directives*/
angular.module("myApp").directive("rnd", [
  function() {
    return {
      templateUrl: "/scripts/directives/rnd/index.html?v1.2",
      restrict: "E",
      replace: true,
      scope: {
        userId: "="
      },
      controller: [
        "$scope",
        "Restangular",
        function($scope, Restangular) {
          $scope.form = {
            loading: false,
            error: false,
            message: null,
            submitted: false,
            data: [],
            checkbox: null,
            model: {
              title: null,
              description: null,
              user_id: null,
              message: null
            },
            errors: {
              error: false,
              title: false,
              description: false,
              error_message: null
            },
            getMeta: function() {},
            checkboxUpdate: function() {
              var params = {
                data: $scope.form.model
              };

              var obj = Restangular.one(
                "user-visibility-setting",
                $scope.form.model.user_id
              );
              obj.status = $scope.form.checkbox ? "0" : "1";
              obj.title = "rnd";
              obj.put().then(
                function(response) {
                  $scope.form.checkbox = response.r_and_d ? true : false;
                },
                function(error) {
                  $scope.form.loading = false;
                  $scope.form.errors.error_message = errors.data.message;
                  console.log("ERR", error);
                }
              );
            },
            init: function() {
              $scope.form.loading = true;
              $scope.form.getMeta();
              Restangular.one(
                "user-visibility-setting",
                $scope.form.model.user_id
              )
                .get()
                .then(
                  function(response) {
                    $scope.form.checkbox = response.r_and_d ? true : false;
                    console.log(response.rnd);
                  },
                  function(errors) {
                    $scope.form.loading = false;
                    $scope.form.errors.error_message = errors.data.message;
                    console.log(error);
                  }
                );

              Restangular.one("rnd", $scope.form.model.user_id)
                .get()
                .then(
                  function(response) {
                    $scope.form.loading = false;
                    $scope.form.data = response;
                  },
                  function(errors) {
                    $scope.form.loading = false;
                    $scope.form.errors.error_message = errors.data.message;
                    console.log(error);
                  }
                );
            },

            submit: function($form) {
              $scope.form.errors.error = false;
              if ($scope.form.model.title == null) {
                $scope.form.errors.title = true;
                $scope.form.errors.error = true;
              }
              if ($scope.form.model.description == null) {
                $scope.form.errors.description = true;
                $scope.form.errors.error = true;
              }
              if ($scope.form.errors.error) {
                return;
              }
              var params = {
                data: $scope.form.model
              };
              Restangular.all("rnd")
                .post(params)
                .then(
                  function(response) {
                    $scope.form.loading = false;
                    $scope.form.model.title = null;
                    $scope.form.model.description = null;
                    $scope.form.data.push(response);
                    console.log($scope.form.data);
                  },
                  function(errors) {
                    $scope.form.loading = false;
                    $scope.form.errors.error_message = errors.data.message;
                    console.log(errors);
                  }
                );
            },
            delete: function(obj) {
              console.log(obj);
              console.log(obj.currentTarget.attributes.data.nodeValue);

              var params = {
                data: $scope.form.model
              };
              Restangular.one(
                "rnd",
                obj.currentTarget.attributes.data.nodeValue
              )
                .remove()
                .then(
                  function(response) {
                    delete $scope.form.data[
                      obj.currentTarget.attributes.data.ownerElement.id
                    ];
                  },
                  function(errors) {
                    $scope.form.loading = false;
                    $scope.form.errors.error_message = errors.data.message;
                  }
                );
            }
          };
          $scope.form.init();
        }
      ]
    };
  }
]);
