"use strict";

/*Directives*/
angular.module("myApp").directive("socialinfo", [
  function() {
    return {
      templateUrl: "/scripts/directives/social-info/index.html",
      restrict: "E",
      replace: true,
      scope: {
        userId: "=userid"
      },
      controller: [
        "$scope",
        "Restangular",
        function($scope, Restangular) {
          $scope.social_info = [
            {
              type: "email",
              icon: "fa fa-envelope",
              value: "",
              visibility: false
            },
            {
              type: "phone",
              icon: "fa fa-phone",
              value: "",
              visibility: false
            },
            {
              type: "skype",
              icon: "fa fa-skype",
              value: "",
              visibility: false
            },
            {
              type: "github",
              icon: "fa fa-github",
              value: "",
              visibility: false
            },
            {
              type: "stackoverflow",
              icon: "fa fa fa-stack-overflow",
              value: "",
              visibility: false
            },
            {
              type: "linkedin",
              icon: "fa fa-linkedin",
              value: "",
              visibility: false
            },
            {
              type: "twitter",
              icon: "fa fa-twitter",
              value: "",
              visibility: false
            },
            {
              type: "facebook",
              icon: "fa fa-facebook",
              value: "",
              visibility: false
            }
          ];
          console.log("social", $scope.social_info);
          $scope.form = {
            loading: false,
            error: false,
            message: null,
            submitted: false,
            data: [],
            checkbox: null,
            model: {
              title: null,
              description: null,
              user_id: $scope.userId,
              message: null,
              icon: ""
            },
            errors: {
              error: false,
              title: false,
              description: false,
              error_message: null
            },
            getMeta: function() {},
            checkboxUpdate: function() {
              var params = {
                data: $scope.form.model
              };

              var obj = Restangular.one(
                "user-visibility-setting",
                $scope.form.model.user_id
              );
              obj.status = $scope.form.checkbox ? "0" : "1";
              obj.title = "social-info";
              obj.put().then(
                function(response) {
                  $scope.form.checkbox = response.social_info ? true : false;
                },
                function(error) {
                  $scope.form.loading = false;
                  $scope.form.errors.error_message = errors.data.message;
                  console.log("ERR", error);
                }
              );
            },
            init: function() {
              $scope.form.loading = true;
              $scope.form.getMeta();
              Restangular.one(
                "user-visibility-setting",
                $scope.form.model.user_id
              )
                .get()
                .then(
                  function(response) {
                    $scope.form.checkbox = response.social_info
                      ? true
                      : false;
                  },
                  function(errors) {
                    $scope.form.loading = false;
                    $scope.form.errors.error_message = errors.data.message;
                    console.log(error);
                  }
                );

              Restangular.one("social-info", $scope.form.model.user_id)
                .get()
                .then(
                  function(response) {
                    $scope.form.loading = false;
                    if (JSON.parse(response[0].social_json))
                      $scope.social_info = JSON.parse(response[0].social_json);
                    $scope.checkstatus = new Array(
                      $scope.social_info.length
                    ).fill("fa fa-check");
                    $scope.checkvisibility = new Array(
                      $scope.social_info.length
                    ).fill("invisible");
                  },
                  function(errors) {
                    $scope.form.loading = false;
                    $scope.form.errors.error_message = errors.data.message;
                    console.log(error);
                  }
                );
            },

            submit: function($index) {
              $scope.checkvisibility[$index] = "invisible";
              $data = JSON.stringify($scope.social_info);
              $id = $scope.form.model.user_id;
              console.log("140", $data);
              var params = {
                data: $data,
                id: $id
              };
              console.log("params", params);
              Restangular.all("social-info")
                .post(params)
                .then(
                  function(response) {
                    response.status
                      ? ($scope.checkstatus[$index] = "fa fa-check")
                      : ($scope.checkstatus[$index] = "fa fa-exclamation");
                    $scope.checkvisibility[$index] = "visible";
                    $scope.form.loading = false;
                    $scope.form.model.social_json = null;
                    // $scope.form.model.description = null;
                    $scope.form.data.push(response);
                    console.log($scope.form.data);
                  },
                  function(errors) {
                    $scope.form.loading = false;
                    $scope.form.errors.error_message = errors.data.message;
                    console.log(errors);
                  }
                );
            }
          };
          $scope.form.init();
        }
      ]
    };
  }
]);
