"use strict()";

/*Directives*/
angular
  .module("myApp")
  .directive("googleDriveLink", [
    function() {
      return {
        templateUrl: "/scripts/directives/google-drive-link/index.html",
        restrict: "E",
        replace: true,
        scope: {
          projectId: "=",
          isDisabled: "@isDisabled"
        },
        controller:["$scope", "Restangular", function($scope, Restangular) {
          $scope.newRequiredProjectLinks = [];
          $scope.oldProjectLinks = [];
          $scope.editable = [];
          $scope.error = false;
          $scope.nameError = false;
          $scope.pathError = false;
          $scope.newProjectLinkObj = {
            name: "",
            path: ""
          };
          $scope.loadRequiredProjectLinks = function() {
            $scope.oldProjectLinks = [];
            $scope.listLoading = true;
            Restangular.all("project-link")
              .customGET("", {
                project_id: $scope.projectId
              })
              .then(
                function(response) {
                  $scope.listLoading = false;
                  for (i = 0; i < response.length; i++) {
                    $scope.oldProjectLinks.push(response[i]);
                  }
                },
                function(error) {
                  console.log(error);
                }
              );
          };
          $scope.loadRequiredProjectLinks();

          $scope.addProjectLinkRow = function() {
            $scope.error = false;
            if (!$scope.newProjectLinkObj.name) {
              $scope.nameError = true;
              $scope.error = true;
            } else {
              $scope.nameError = false;
            }

            if (!$scope.newProjectLinkObj.path) {
              $scope.pathError = true;
              $scope.error = true;
            } else {
              $scope.pathError = false;
            }

            if ($scope.error) {
              return;
            }

            var params = {
              project_id: $scope.projectId,
              name: $scope.newProjectLinkObj.name,
              path: $scope.newProjectLinkObj.path
            };
            console.log("params", params);
            Restangular.all("project-link")
              .post(params)
              .then(
                function(response) {
                  $scope.newProjectLinkObj = {
                    name: "",
                    path: ""
                  };
                  $scope.loadRequiredProjectLinks();
                },
                function(error) {
                  console.log(error);
                }
              );
          };
          $scope.editProjectLinkRow = function(index) {
            console.log(index);
            $scope.editable[index] = true;
            console.log($scope.editable);
          };
          $scope.saveProjectLinkRow = function(index, ProjectLinkRow) {
            $scope.editable[index] = true;
            Restangular.one("project-link", ProjectLinkRow.id)
              .customPUT({
                ProjectLinkRow: ProjectLinkRow
              })
              .then(
                function(response) {
                  $scope.editable[index] = false;
                },
                function(errors) {
                  console.log(errors);
                }
              );
          };

          $scope.removeProjectLinkRow = function(index, ProjectLinkRow) {
            var r = confirm("Are you sure you want to delete this entry?");
            if (r != true) {
              return;

            }
            Restangular.one("project-link", ProjectLinkRow.id)
              .remove()
              .then(
                function(response) {
                  $scope.oldProjectLinks.splice(index, 1);
                },
                function(error) {
                  console.log(error);
                }
              );
          };
        }]
      };
    }
  ]);
