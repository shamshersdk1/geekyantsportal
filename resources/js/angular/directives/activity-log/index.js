"use strict()";

/*Directives*/
angular.module("myApp").directive("activityLog", [
  function() {
    return {
      templateUrl: "/scripts/directives/activity-log/index.html",
      restrict: "E",
      replace: true,
      scope: {
        referenceId: "=",
        referenceType: "@referenceType"
      },
      controller: [
        "$scope",
        "Restangular",
        "$rootScope",
        function($scope, Restangular, $rootScope) {
          var api = "activity-log";

          $scope.form = {
            loading: false,
            error: false,
            message: null,
            activities: [],
            model: {
              reference_id: $scope.referenceId,
              reference_type: $scope.referenceType,
            },
            init : function() {
              var params = {
                'reference_id' : $scope.referenceId,
                'reference_type' : $scope.referenceType,
                'pagination' : 0
              };
              Restangular.all('activity-log').getList(params).then(function(response){
                console.log('response', response);
                $scope.form.activities = response;
                angular.forEach($scope.form.activities, function(activitiy) {
                  activitiy.action_details = JSON.parse(activitiy.action_details);
                })

              }, function(error){
                console.log('error', error);
              });
            }
          }
          $scope.form.init();   
          $rootScope.$on('activity-log', function(event, data){
            console.log('broadcast activity-log');
            $scope.form.init();
          });     
        }
      ]
    };
  }
]);
