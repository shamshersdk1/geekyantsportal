"use strict()";

/*Directives*/
angular
    .module("myApp")
    .directive("addProjectUser", [
        function () {
            return {
                
                templateUrl: "/scripts/directives/add-project-user/add-project-user.html?v=1",
                restrict: "E",
                replace: true,
                scope: {
                    projectId: "=",
                    isDisabled: "@isDisabled",
                },
                
                controller: ["$scope", "Restangular", function (
                    $scope,
                    Restangular
                ) {
                    $scope.existingResources = [];
                    $scope.allTypes = [];
                    $scope.allUsers = [];
                    $scope.editable = [];
                    $scope.selected_user = '';
                    $scope.selected_type = '';
                    $scope.error = false;
                    $scope.userError = false;
                    $scope.startDateError = false;
                    $scope.edit_start_date = [];
                    $scope.edit_end_date = [];
                    $scope.today = new Date();
                    $scope.start_date = null;
                    $scope.end_date = null;
                    
                    $scope.getAllUsers = function () {
                        $scope.allUsers = [];
                        $scope.listLoading = true;
                        Restangular.all("user-all-active")
                            .customGET("", {})
                            .then(function (response) {
                                $scope.allUsers = response;
                            },function (error) {
                                console.log(error);
                            }
                        );
                    };
                    $scope.getAllTypes = function () {
                        $scope.allTypes = [];
                        $scope.listLoading = true;
                        Restangular.all("resource-price")
                            .customGET("", {
                                "project_id": $scope.projectId,
                            })
                            .then(function (response) {
                                $scope.listLoading = false;
                                for (i = 0; i < response.length; i++) {
                                    $scope.allTypes.push(response[i]);
                                }
                            },function (error) {
                                console.log(error);
                            }
                        );
                    };
                    $scope.getExistingResources = function () {
                        $scope.existingResources = [];
                        $scope.listLoading = true;
                        Restangular.all("project-resource")
                            .customGET("", {
                                "project_id": $scope.projectId,
                            })
                            .then(function (response) {
                                $scope.listLoading = false;
                                for (i = 0; i < response.length; i++) {
                                    $scope.edit_start_date[i] = new Date(response[i].start_date);
                                    $scope.edit_end_date[i] = (response[i].end_date) ? new Date(response[i].end_date) : null;
                                    $scope.existingResources.push(response[i]);
                                    
                                }
                            },function (error) {
                                console.log(error);
                            }
                        );
                    };
                    $scope.getAllTypes();
                    $scope.getAllUsers();
                    $scope.getExistingResources();

                    $scope.select_user = function (data) {
                        $scope.selected_user = data;
                        $scope.selected_user_error = false;
                    };

                    $scope.select_type = function (data) {
                        $scope.selected_type = data;
                        $scope.selected_type_error = false;
                    };

                    $scope.checkEndDateCondition = function (input_date) {
                        if ( !input_date ) {
                            return true;
                        }
                        else{
                            if ( $scope.today >= input_date )
                            {
                                return false;
                            } else {
                                return true;
                            }
                        }

                    };

                    $scope.addUser = function() {
                        $scope.error = false;
                        if ( $scope.selected_user == '' )
                        {
                            $scope.selected_user_error = true;
                            $scope.error = true;
                        }   else {
                            $scope.selected_user_error = false;
                        }
                        
                        if ( $scope.selected_type == '' )
                        {
                            $scope.selected_type_error = true;
                            $scope.error = true;
                        }   else{
                            $scope.selected_type_error = false;
                        }
                        if ( $scope.start_date == null )
                        {
                            $scope.selected_start_date_error = true;
                            $scope.error = true;
                        }   else{
                            $scope.selected_start_date_error = false;
                        }

                        if ( $scope.error )
                        { 
                            return;
                        }
                        var endDate = '';
                        if ( $scope.end_date )
                        {
                            endDate = moment($scope.end_date).format("YYYY-MM-DD");
                        }
                        var startDate = moment($scope.start_date).format("YYYY-MM-DD");
                        var params = {
                            'project_id' : $scope.projectId,
                            'user_id' : $scope.selected_user.id,
                            'resource_price_id' : $scope.selected_type.id,
                            'start_date' : startDate,
                            'end_date' : endDate
                        };
                        
                        Restangular.all("project-resource")
                            .post(params)
                            .then(function (response) {
                                $scope.selected_user = '';
                                $scope.selected_type = '';
                                $scope.start_date = null;
                                $scope.end_date = null;
                                $scope.getExistingResources();
                            }, function (error) {
                                console.log(error);

                            });
                    };
                    $scope.editResourceRow = function(index) {
                        $scope.editable[index] = true;
                    }
                    $scope.saveResourceRow = function(index, resourceRow){
                        var params = {
                            'user_id' : resourceRow.user_id,
                            'resource_price_id' : resourceRow.resource_price_id,
                            'start_date' : moment($scope.edit_start_date[index]).format("YYYY-MM-DD"),
                            'end_date' : $scope.edit_end_date[index] ? moment($scope.edit_end_date[index]).format("YYYY-MM-DD") : null ,
                        };
                        Restangular.one("project-resource", resourceRow.id)
                                .customPUT({
                                    params: params
                                })
                                .then(
                                    function (response) {
                                        $scope.editable[index] = false;
                                        $scope.getExistingResources();
                                    },
                                    function (errors) {
                                        console.log(errors);
                                    }
                                );
                    };

                    $scope.removeResourceRow = function(index, resourceRow) {
                        var r = confirm("Are you sure you want to delete this entry?");
                        if (r != true) {
                            return;
                        }
                        Restangular.one("project-resource", resourceRow.id)
                            .remove()
                            .then(
                                function(response) {
                                    $scope.existingResources.splice(index, 1);
                                },
                                function(error) {
                                    console.log(error);
                                }
                            );
                    };

                    $scope.releaseResourceRow = function(index, resourceRow) {
                        var r = confirm("Are you sure you want to release this user from the project?");
                        if (r != true) {
                            return;
                        }
                        Restangular.one("project-resource/release", resourceRow.id)
                            .customPUT()
                            .then(
                                function(response) {
                                    $scope.existingResources = [];
                                    $scope.getExistingResources();
                                },
                                function(error) {
                                    console.log(error);
                                }
                            );
                    };
                }]
            };
           
        }
       
    ]
);


    
