"use strict()";

/*Directives*/
angular.module("myApp").directive("commentData", [
  function() {
    return {
      templateUrl: "/scripts/directives/comment/comment.html?v=2",
      restrict: "E",
      replace: true,
      scope: {
        commentableId: "=",
        commentableType: "@commentableType",
        isDisabled: "@isDisabled",
        isPrivatable: "@isPrivatable"
      },
      controller: [
        "$scope",
        "Restangular",
        "$rootScope",
        function($scope, Restangular, $rootScope) {
          $scope.loadMoreFlag = false;
          $scope.commentCount = 0;
          $scope.noCommentsToLoad = false;
          $scope.showList = true;
          $scope.addLoading = false;
          $scope.listLoading = false;
          $scope.commentObj = {
            comments: [],
            page: 1,
            per_page: 5,
            loaded: false,
            newComment: "",
            newCommentAdded: false,
            isPrivate: false
          };

          $scope.loadComments = function() {
            $scope.listLoading = true;
            var params = {
              commentable_id: $scope.commentableId,
              commentable_type: $scope.commentableType,
              page: $scope.commentObj.page,
              per_page: $scope.commentObj.per_page
            };
            Restangular.all("comment")
              .getList(params)
              .then(function(response) {
                $scope.listLoading = false;
                if (response.length > 0) {
                  $scope.commentCount = response.length;
                  for (i = 0; i < response.length; i++) {
                    $scope.commentObj.comments.push(response[i]);
                  }
                } else {
                  $scope.noCommentsToLoad = true;
                }
              });
          };

          $scope.loadComments();
          $scope.loadMoreComments = function() {
            $scope.loadMoreFlag = true;
            $scope.commentObj.page = $scope.commentObj.page + 1;
            $scope.loadComments();
          };
          $scope.checkboxClicked = function(data) {
            // console.log('data', data);
          };
          $scope.postComment = function() {
            if (
              $scope.commentObj.newComment == null ||
              $scope.commentObj.newComment == ""
            ) {
              return;
            }
            $scope.addLoading = true;
            var params = {
              commentable_id: $scope.commentableId,
              commentable_type: $scope.commentableType,
              is_private: $scope.commentObj.isPrivate,
              message: $scope.commentObj.newComment
            };

            Restangular.all("comments")
              .post(params)
              .then(
                function(response) {
                  $scope.commentCount++;
                  $scope.addLoading = false;
                  $scope.commentObj.newComment = "";
                  $scope.commentObj.isPrivate = !$scope.commentObj.isPrivate;
                  $scope.showList = true;
                  $scope.commentObj.comments.unshift(response.comment);
                  $rootScope.$broadcast('activity-log');
                },
                function(error) {
                  $scope.addLoading = false;
                }
              );
          };

          $scope.toggleCommentList = function() {
            $scope.showList = !$scope.showList;
            $scope.noCommentsToLoad = false;
          };

          $scope.filteredDate = function(input_date) {
            var formatted_date = new Date(input_date);
            return formatted_date;
          };
        }
      ]
    };
  }
]);
