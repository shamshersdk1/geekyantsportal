"use strict()";

/*Directives*/
angular
  .module("myApp")
  .directive("projectTimelog", [
    function() {
      return {
        templateUrl:
          "/scripts/directives/project-timelog/project-timelog.html?v=1",
        restrict: "E",
        replace: true,
        scope: {
          projectId: "=",
          isDisabled: "@isDisabled"
        },
        controller: ["$scope", "Restangular", function($scope, Restangular) {
          $scope.timelog;
          $scope.loadTimelog = function() {
            $scope.timelog;
            $scope.loading = true;
            Restangular.one("project-timelog", $scope.projectId)
              .get()
              .then(
                function(response) {
                  $scope.loading = false;
                  $scope.timelog = response;
                },
                function(error) {
                  console.log(error);
                }
              );
          };
          $scope.loadTimelog();
        }]
      };
    }
  ]);
