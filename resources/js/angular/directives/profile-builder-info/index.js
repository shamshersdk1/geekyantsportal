"use strict()";

/*Directives*/
angular.module("myApp").directive("profileInfo", [
  function() {
    return {
      restrict: "E",
      templateUrl: "/scripts/directives/profile-builder-info/index.html",
      replace: true,
      scope: {
        userId: "=userid"
      },
      controller: [
        "$scope",
        "Restangular",
        "FileUploader",
        function($scope, Restangular, FileUploader) {
          var define = " sad";
          $scope.locationlist = [
            {
              value: "London, UK",
              location: "GeekyAnts - London, UK"
            },
            {
              value: "Bangalore, India",
              location: "GeekyAnts - Bangalore, India"
            }
          ];
          $scope.pathlist = [
            {
              path:
                "resources/views/pages/admin/profile-builder/pages/founder.blade.php",
              name: "Founder"
            },
            {
              path:
                "resources/views/pages/admin/profile-builder/pages/tech-leads-architect.blade.php",
              name: "Tech Leads & Architect"
            },
            {
              path:
                "resources/views/pages/admin/profile-builder/pages/developers-designers.blade.php",
              name: "Developers & Designers"
            },
            {
              path:
                "resources/views/pages/admin/profile-builder/pages/other-team.blade.php",
              name: "Other Team"
            }
          ];
          $scope.userSelect = null;
          $scope.pathSelect = null;
          $scope.profile_id = null;

          $scope.form = {
            loading: false,
            title: null,
            page_title: null,
            error: false,
            message: null,
            id: null,
            index: null,
            submitted: false,
            selected_project: null,
            checkbox: null,
            model: {
              selectedProject: null,
              user_id: $scope.userId,
              message: null,
              project_id: null
            },
            errors: {
              error: false,
              error_message: null
            },
            getMeta: function() {},
            getImage: function() {
              $scope.form.init();
            },
            infoUpdate: function() {
              var obj = Restangular.one(
                "profile-info",
                $scope.form.model.user_id
              );
              obj.title = $scope.form.title;
              obj.location = $scope.userSelect;
              obj.page_title = $scope.form.page_title;
              obj.page_path = $scope.pathSelect;
              obj.put().then(
                function(response) {
                  $scope.form.title = response[0]["title"];
                  $scope.form.page_title = response[0]["page_title"];
                  $scope.userSelect = response[0]["location"];
                  $scope.pathSelect = response[0]["page_path"];
                },
                function(error) {
                  $scope.form.loading = false;
                  $scope.form.errors.error_message = errors.data.message;
                  console.log("ERR", error);
                }
              );
            },

            checkboxUpdate: function() {
              var obj = Restangular.one(
                "user-visibility-setting",
                $scope.form.model.user_id
              );
              obj.status = $scope.form.checkbox ? "0" : "1";
              obj.title = "projects";
              obj.put().then(
                function(response) {
                  $scope.form.checkbox = response[0].projects ? true : false;
                },
                function(error) {
                  $scope.form.loading = false;
                  $scope.form.errors.error_message = errors.data.message;
                  console.log("ERR", error);
                }
              );
            },
            init: function() {
              $scope.form.loading = true;
              $scope.form.getMeta();
              Restangular.one(
                "user-visibility-setting",
                $scope.form.model.user_id
              )
                .get()
                .then(
                  function(response) {
                    $scope.form.checkbox = response[0].projects ? true : false;
                  },
                  function(errors) {
                    $scope.form.loading = false;
                    $scope.form.errors.error_message = errors.data.message;
                    console.log(error);
                  }
                );
              Restangular.one("profile-info", $scope.form.model.user_id)
                .get()
                .then(
                  function(response) {
                    $scope.form.title = response[0]["title"];
                    $scope.form.page_title = response[0]["page_title"];
                    $scope.userSelect = response[0]["location"];
                    $scope.pathSelect = response[0]["page_path"];
                    $scope.profile_id = response[0]["id"];
                    $scope.form.src =
                      "http://" +
                      window.location.hostname +
                      "/geekyants-portal.local.geekydev.com/storage/ProfilePicture/default.jpeg";
                    if (response[0].profile[0]) {
                      $scope.form.src =
                        "http://" +
                        window.location.hostname +
                        "/geekyants-portal.local.geekydev.com/" +
                        response[0].profile[0]["path"];
                    }
                  },
                  function(errors) {
                    $scope.form.loading = false;
                    $scope.form.errors.error_message = errors.data.message;
                    console.log(error);
                  }
                );
            }
          };
          $scope.form.init();
          $scope.uploader = new FileUploader({
            url: "/api/v1/file",
            autoUpload: true
          });
        }
      ]
    };
  }
]);
