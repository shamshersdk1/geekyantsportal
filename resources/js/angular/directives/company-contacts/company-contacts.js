"use strict()";

/*Directives*/
angular.module("myApp").directive("companyContacts", [
  function() {
    return {
      templateUrl:
        "/scripts/directives/company-contacts/company-contacts.html?v=1",
      restrict: "E",
      replace: true,
      scope: {
        companyId: "=",
        isDisabled: "@isDisabled"
      },
      controller: function($scope, Restangular, $uibModal) {
        $scope.oldcontactObjs = [];

        $scope.showModal = function() {
          var modal = $uibModal.open({
            controller: "modalCtrl",
            windowClass: "bootstrap_wrap",
            templateUrl: "/scripts/directives/company-contacts/modal.html",
            backdrop: "static",
            size: "md",
            resolve: {
              data: function() {
                return { company_id: $scope.companyId };
              }
            }
          });
          modal.result.then(
            function(result) {
              $scope.oldcontactObjs.push(result);
            },
            function() {
              //
            }
          );
        };
        $scope.loadOldContacts = function() {
          $scope.oldcontactObjs = [];
          $scope.listLoading = true;
          Restangular.all("company-contact")
            .customGET("", {
              company_id: $scope.companyId
            })
            .then(
              function(response) {
                $scope.listLoading = false;
                for (i = 0; i < response.length; i++) {
                  $scope.oldcontactObjs.push(response[i]);
                }
              },
              function(error) {
                console.log(error);
              }
            );
        };
        $scope.loadOldContacts();

        $scope.removecontactObjRow = function(index, contactRow) {
          console.log("thus");
          console.log(index, contactRow);
          var r = confirm("Are you sure you want to delete this entry?");
          if (r != true) {
            return;
          }
          Restangular.one("company-contact", contactRow.id)
            .remove()
            .then(
              function(response) {
                $scope.oldcontactObjs.splice(index, 1);
              },
              function(error) {
                console.log(error);
              }
            );
        };
        $scope.editcontactObjRow = function(index, contactRow) {
          var modal = $uibModal.open({
            controller: "editModalCtrl",
            windowClass: "bootstrap_wrap",
            templateUrl: "/scripts/directives/company-contacts/edit-modal.html",
            backdrop: "static",
            size: "md",
            resolve: {
              data: function() {
                return { contact_id: contactRow.id };
              }
            }
          });
          modal.result.then(
            function(result) {
              $scope.loadOldContacts();
            },
            function() {
              //
            }
          );
        };
      }
    };
  }
]);
