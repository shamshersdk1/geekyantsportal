angular
  .module("myApp")
  .controller("modalCtrl", function(
    $scope,
    Restangular,
    data,
    $uibModalInstance
  ) {
    $scope.items = data;
    $scope.error = false;
    $scope.firstNameError = false;
    $scope.lastNameError = false;
    $scope.typeError = false;
    $scope.emailError = false;
    $scope.contactObj = {
      first_name: "",
      last_name: "",
      type: "",
      email: "",
      mobile: ""
    };

    $scope.cancel = function() {
      $uibModalInstance.dismiss();
    };

    $scope.ok = function() {
      $scope.error = false;
      if (!$scope.contactObj.first_name) {
        $scope.firstNameError = true;
        $scope.error = true;
      } else {
        $scope.firstNameError = false;
      }

      if (!$scope.contactObj.last_name) {
        $scope.lastNameError = true;
        $scope.error = true;
      } else {
        $scope.lastNameError = false;
      }

      if (!$scope.contactObj.type) {
        $scope.typeError = true;
        $scope.error = true;
      } else {
        $scope.typeError = false;
      }

      if (!$scope.contactObj.email) {
        $scope.emailError = true;
        $scope.error = true;
      } else {
        $scope.emailError = false;
      }

      if ($scope.error) {
        return;
      }

      var params = {
        company_id: $scope.items.company_id,
        first_name: $scope.contactObj.first_name,
        last_name: $scope.contactObj.last_name,
        type: $scope.contactObj.type,
        email: $scope.contactObj.email,
        mobile: $scope.contactObj.mobile
      };

      Restangular.all("company-contact")
        .post(params)
        .then(
          function(response) {
            $uibModalInstance.close(response);
          },
          function(error) {
            console.log(error);
            $uibModalInstance.dismiss();
          }
        );
    };
  });
