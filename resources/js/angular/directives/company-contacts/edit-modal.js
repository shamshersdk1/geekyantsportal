angular
  .module("myApp")
  .controller("editModalCtrl", function(
    $scope,
    Restangular,
    data,
    $uibModalInstance
  ) {
    $scope.items = data;
    $scope.error = false;
    $scope.firstNameError = false;
    $scope.lastNameError = false;
    $scope.typeError = false;
    $scope.emailError = false;
    $scope.contactObj = {
      first_name: "",
      last_name: "",
      type: "",
      email: "",
      mobile: ""
    };
    $scope.contactData = [];

    $scope.loadContactData = function() {
      $scope.contactData;
      $scope.loading = true;
      Restangular.one("company-contact", $scope.items.contact_id)
        .get()
        .then(
          function(response) {
            $scope.loading = false;
            $scope.contactData = response;
          },
          function(error) {
            console.log(error);
          }
        );
    };
    $scope.loadContactData();

    $scope.cancel = function() {
      $uibModalInstance.dismiss();
    };

    $scope.ok = function() {
      $scope.error = false;
      if (!$scope.contactData.first_name) {
        $scope.firstNameError = true;
        $scope.error = true;
      } else {
        $scope.firstNameError = false;
      }

      if (!$scope.contactData.last_name) {
        $scope.lastNameError = true;
        $scope.error = true;
      } else {
        $scope.lastNameError = false;
      }

      if (!$scope.contactData.type) {
        $scope.typeError = true;
        $scope.error = true;
      } else {
        $scope.typeError = false;
      }

      if (!$scope.contactData.email) {
        $scope.emailError = true;
        $scope.error = true;
      } else {
        $scope.emailError = false;
      }

      if ($scope.error) {
        return;
      }

      var params = {
        contact_id: $scope.items.contact_id,
        first_name: $scope.contactData.first_name,
        last_name: $scope.contactData.last_name,
        type: $scope.contactData.type,
        email: $scope.contactData.email,
        mobile: $scope.contactData.mobile
      };

      Restangular.all("company-contact", $scope.items.contact_id)
        .customPUT(params)
        .then(
          function(response) {
            $uibModalInstance.close(response);
          },
          function(error) {
            console.log(error);
            $uibModalInstance.dismiss();
          }
        );
    };
  });
