"use strict()";

/*Directives*/
angular.module("myApp").directive("assetMeta", [
  function() {
    return {
      templateUrl: "/scripts/directives/asset-meta/index.html",
      restrict: "E",
      replace: true,
      scope: {
        assetId: "="
      },
      controller: [
        "$scope",
        "Restangular",
        function($scope, Restangular) {
          var api = "asset-meta";
          
          $scope.form = {
            loading: false,
            error: false,
            message: null,
            submitted: false,
            data: [],
            keys : [],
            model: {
              asset_id: $scope.assetId,
              key: null,
              value: null,
              validValue : true
            },
            getMeta : function() {
              Restangular.all('asset-meta/key')
                .getList()
                .then(function(response){
                  $scope.form.keys = response;
                }, function(error){
                  console.log('error', error);

                });
            },
            init: function() {
              $scope.form.loading = true;
              $scope.form.getMeta();
              Restangular.all(api)
                .getList({
                  asset_id: $scope.assetId,
                  pagination: 0,
                  per_page: 100
                })
                .then(
                  function(response) {
                    $scope.form.loading = false;
                    // angular.forEach(response, function(item) {
                    //   item.loading = false;
                    //   item.editing = false;
                    //   item.update = function() {
                    //     item.loading = true;
                    //     item.put().then(
                    //       function(response) {
                    //         console.log("updated ", response);
                    //         item.loading = false;
                    //         item.editing = false;
                    //       },
                    //       function(error) {
                    //         item.loading = false;
                    //         console.log("ERROR updating asset-meta", error);
                    //       }
                    //     );
                    //   };
                    //   item.enableEditing = function() {
                    //     item.editing = true;
                    //     return false;
                    //   };
                    // });
                    $scope.form.data = response;
                  },
                  function(error) {
                    $scope.form.loading = false;
                    console.log(error);
                  }
                );
            },
            update: function(item) {
              item.enableEditing = !item.enableEditing;
              console.log('item update');
              item.loading = false;
              item.editing = true;
              item.update = function() {
                item.loading = true;
                item.error = false;
                item.message = null;
                item.put().then(
                  function(response) {
                    $scope.form.getMeta();
                    item.loading = false;
                    item.editing = false;
                  },
                  function(error) {
                    item.loading = false;
                    item.error = true;
                    item.loading = false;
                    item.message = error.data.message;
                    console.log("ERROR updating asset-meta", error);
                  }
                );
              };
              
            },
            remove: function(itemObj, $index) {
              var confirmMess = confirm("Are you sure want to delete this entry?");
              if(confirmMess) {
                itemObj.remove().then(function(response){
                  $scope.form.data.splice($index, 1);
                }, function(error){
                  console.log('error',error);
                });
              }
            },
            resetForm: function() {
              $scope.form.model.key = null;
              $scope.form.model.value = null;
              $scope.form.submitted = false;
            },
            checkValidMac: function(value) {
              var regex = /^(([A-Fa-f0-9]{2}[:]){5}[A-Fa-f0-9]{2}[,]?)+$/i;
              return regex.test(value);
            },
            submit: function($form) {
              $scope.form.error = false;
              $scope.form.submitted = true;
              if ($form.$invalid) return false;
              $scope.form.model.validValue = true;

              if(($scope.form.model.key == 'LAN-MAC' ||  $scope.form.model.key == 'WIFI-MAC') && 
                $scope.form.checkValidMac($scope.form.model.value) == false) {
                console.log('checkValidMac false');
                $scope.form.model.validValue = false;
                return false;
              }
              
              
              Restangular.all("asset-meta")
                .post($scope.form.model)
                .then(
                  function(response) {
                    $scope.form.data.unshift(response);
                    $scope.form.resetForm();
                    $scope.form.getMeta();
                  },
                  function(error) {

                    $scope.form.error = true;
                    $scope.form.message = error.data.message;
                    console.log("error",error.data.message);
                  }
                );
            }
          };
          $scope.form.init();

          // $scope.newRequiredProjectLinks = [];
          // $scope.oldProjectLinks = [];
          // $scope.editable = [];
          // $scope.error = false;
          // $scope.nameError = false;
          // $scope.pathError = false;
          // $scope.newProjectLinkObj = {
          //   name: "",
          //   path: ""
          // };
          // $scope.loadRequiredProjectLinks = function() {
          //   $scope.oldProjectLinks = [];
          //   $scope.listLoading = true;
          //   Restangular.all("project-link")
          //     .customGET("", {
          //       project_id: $scope.projectId
          //     })
          //     .then(
          //       function(response) {
          //         $scope.listLoading = false;
          //         for (i = 0; i < response.length; i++) {
          //           $scope.oldProjectLinks.push(response[i]);
          //         }
          //       },
          //       function(error) {
          //         console.log(error);
          //       }
          //     );
          // };
          // $scope.loadRequiredProjectLinks();

          // $scope.addProjectLinkRow = function() {
          //   $scope.error = false;
          //   if (!$scope.newProjectLinkObj.name) {
          //     $scope.nameError = true;
          //     $scope.error = true;
          //   } else {
          //     $scope.nameError = false;
          //   }

          //   if (!$scope.newProjectLinkObj.path) {
          //     $scope.pathError = true;
          //     $scope.error = true;
          //   } else {
          //     $scope.pathError = false;
          //   }

          //   if ($scope.error) {
          //     return;
          //   }

          //   var params = {
          //     project_id: $scope.projectId,
          //     name: $scope.newProjectLinkObj.name,
          //     path: $scope.newProjectLinkObj.path
          //   };
          //   console.log("params", params);
          //   Restangular.all("project-link")
          //     .post(params)
          //     .then(
          //       function(response) {
          //         $scope.newProjectLinkObj = {
          //           name: "",
          //           path: ""
          //         };
          //         $scope.loadRequiredProjectLinks();
          //       },
          //       function(error) {
          //         console.log(error);
          //       }
          //     );
          // };
          // $scope.editProjectLinkRow = function(index) {
          //   console.log(index);
          //   $scope.editable[index] = true;
          //   console.log($scope.editable);
          // };
          // $scope.saveProjectLinkRow = function(index, ProjectLinkRow) {
          //   $scope.editable[index] = true;
          //   Restangular.one("project-link", ProjectLinkRow.id)
          //     .customPUT({
          //       ProjectLinkRow: ProjectLinkRow
          //     })
          //     .then(
          //       function(response) {
          //         $scope.editable[index] = false;
          //       },
          //       function(errors) {
          //         console.log(errors);
          //       }
          //     );
          // };

          // $scope.removeProjectLinkRow = function(index, ProjectLinkRow) {
          //   var r = confirm("Are you sure you want to delete this entry?");
          //   if (r != true) {
          //     return;

          //   }
          //   Restangular.one("project-link", ProjectLinkRow.id)
          //     .remove()
          //     .then(
          //       function(response) {
          //         $scope.oldProjectLinks.splice(index, 1);
          //       },
          //       function(error) {
          //         console.log(error);
          //       }
          //     );
          // };
        }
      ]
    };
  }
]);
