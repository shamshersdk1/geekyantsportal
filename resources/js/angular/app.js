var app = angular
  .module("myApp", [
    "ngAnimate",
    "ui.bootstrap",
    "restangular",
    "ui.select",
    "ngRoute",
    "datatables",
    "datatables.fixedcolumns",
    "daterangepicker",
    "angularFileUpload",
    "vs-repeat",
    "ngSanitize"
  ])
  .config([
    "$interpolateProvider",
    "RestangularProvider",
    function($interpolateProvider, RestangularProvider) {
      $interpolateProvider.startSymbol("%%");
      $interpolateProvider.endSymbol("%%");
      var apiBaseUrl = "http://geekyants-portal.local.geekydev.com/api/v1";

      RestangularProvider.setBaseUrl(apiBaseUrl);
      // RestangularProvider.setResponseExtractor(function(
      //   response,
      //   operation,
      //   what,
      //   url
      // ) {
      //   var newResponse;
      //   newResponse = response;
      //   console.log("RES ", response);
      //   if (angular.isArray(response)) {
      //     if (!response) return;
      //     for (var i = 0; i < response.length; i++) {
      //       response[i].originalData = angular.copy(response[i]);
      //     }
      //   } else {
      //     if (!response) return;
      //     response.originalData = angular.copy(response.result);
      //   }
      //   if (typeof response.meta != "undefined")
      //     newResponse.metaData = response.meta.pagination;
      //   else {
      //     newResponse.metaData = response.meta;
      //   }
      //   return newResponse;
      // });

      var env = {};
      if (window) {
        Object.assign(env, window.__env);
      }
      app.constant("__env", env);
      var apiBaseUrl = __env.apiUrl;
      RestangularProvider.setBaseUrl(apiBaseUrl);
      // RestangularProvider.setResponseExtractor(function(
      //   response,
      //   operation,
      //   what,
      //   url
      // ) {
      //   var newResponse;
      //   newResponse = response;
      //   console.log("setResponseExtractor ", response);

      //   if (angular.isArray(response)) {
      //     if (!response) return;
      //     for (var i = 0; i < response.length; i++) {
      //       response[i].originalData = angular.copy(response[i]);
      //     }
      //   } else {
      //     if (!response) return;
      //     response.originalData = angular.copy(response.result);
      //   }
      //   if (typeof response.meta != "undefined")
      //     newResponse.metaData = response.meta.pagination;
      //   else {
      //     newResponse.metaData = response.meta;
      //   }
      //   return newResponse;
      // });

      RestangularProvider.setErrorInterceptor(function(
        response,
        deferred,
        responseHandler
      ) {
        if (response.status === 498) {
          tokenException = true;
          return true;
        }
      });

      RestangularProvider.addResponseInterceptor(function(
        data,
        operation,
        what,
        url,
        response,
        deferred
      ) {
        var extractedData = {};
        extractedData = data.result;
        extractedData.metaData = data.meta ? data.meta : [];
        return extractedData;
      });
    }
  ])
  .constant("CONFIG", {
    BASEURL: "http://geekyants-portal.local.geekydev.com",
    DOWNLOADURL:
      "http://geekyants-portal.local.geekydev.com/api/v1/download?q=",
    DOWNLOADPDFURL:
      "http://geekyants-portal.local.geekydev.com/api/v1/download-pdf?q="
  })
  .filter("sumByRow", function() {
    return function(collection, column) {
      var total = 0;
      var tempTotal = "";
      var tempTotalArr = [];
      collection.forEach(function(item) {
        total += item.hours;
      });
      if (total.toString().includes(".")) {
        tempTotalArr = total.toString().split(".");
        tempTotal = tempTotalArr[0].toString().padStart(3, "0");
        total = tempTotal + "." + tempTotalArr[1].toString();
      } else {
        total = total.toString().padStart(3, "0");
        total = total + "." + "0";
      }
      return total;
    };
  })
  .filter("formatHours", function() {
    return function(collection, column) {
      var total = collection;
      var tempTotal = "";
      var tempTotalArr = [];
      if (total.toString().includes(".")) {
        tempTotalArr = total.toString().split(".");
        tempTotal = tempTotalArr[0].toString().padStart(2, "0");
        total = tempTotal + "." + tempTotalArr[1].toString();
      } else {
        total = total.toString().padStart(2, "0");
        total = total + "." + "0";
      }
      return total;
    };
  })
  .filter("sumByApprovedHours", function() {
    return function(collection, column) {
      var total = 0;
      var tempTotal = "";
      var tempTotalArr = [];
      collection.forEach(function(item) {
        if (item.approver_id != null) total += item.approved_hours;
      });
      if (total.toString().includes(".")) {
        tempTotalArr = total.toString().split(".");
        tempTotal = tempTotalArr[0].toString().padStart(3, "0");
        total = tempTotal + "." + tempTotalArr[1].toString();
      } else {
        total = total.toString().padStart(3, "0");
        total = total + "." + "0";
      }
      return total;
    };
  })
  .config([
    "$routeProvider",
    function($routeProvider) {
      $routeProvider
        .when("/step1", {
          templateUrl: "/views/payslip-csv/payroll-form.html",
          controller: "payslipStep1Ctrl"
        })
        .when("/step2", {
          templateUrl: "/views/payslip-csv/payroll-form.html",
          controller: "payslipStep2Ctrl"
        })
        .when("/step3", {
          templateUrl: "/views/payslip-csv/payroll-form.html",
          controller: "payslipStep3Ctrl"
        })
        .when("/step4", {
          templateUrl: "/views/payslip-csv/payroll-form.html",
          controller: "payslipStep4Ctrl"
        })
        .when("/step5", {
          templateUrl: "/views/payslip-csv/payroll-form.html",
          controller: "payslipStep5Ctrl"
        })
        .when("/step6", {
          templateUrl: "/views/payslip-csv/payroll-form.html",
          controller: "payslipStep6Ctrl"
        });
    }
  ])
  .config([
    "$locationProvider",
    function($locationProvider) {
      $locationProvider.hashPrefix("");
    }
  ]);
app.controller("myCtrl", function($scope, Restangular, $http) {
  $scope.id = document.getElementById("id").value;
  $http({
    method: "GET",
    url:
      "http://geekyants-portal.geekydev.com/admin/Api/get-content/" + $scope.id
  }).then(
    function successCallback(response) {
      $scope.that = $scope;
      $scope.contents = Object.keys(response.data).map(function(key) {
        return response.data[key];
      });
      for (var i = 0; i < $scope.contents.length; i++) {
        $scope[$scope.contents[i]] = $scope.contents[i];
      }
    },
    function errorCallback(response) {}
  );
});
