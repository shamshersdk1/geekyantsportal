angular.module("myApp").filter("dateFormat", function() {
  return function(input) {
    input = new Date(input).toISOString();
    var dateString = moment(input).format('YYYY-MM-DD');
    var dateStringWithTime = moment(input).format('Do, MMM Y');
    return dateStringWithTime;
  };
});
