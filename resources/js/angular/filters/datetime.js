angular.module("myApp").filter("datetime", function() {
  return function(input) {
    var t = input.split(/[- :]/);
    var dateTime = new Date(Date.UTC(t[0], t[1] - 1, t[2], t[3], t[4], t[5]));
    return dateTime;
  };
});
