(function ($) {

    $.fn.tableHeadFixer = function (param) {

        return this.each(function () {
            table.call(this);
        });

        function table() {
            /*
             This function solver z-index problem in corner cell where fix row and column at the same time,
             set corner cells z-index 1 more then other fixed cells
             */
            function setCorner() {
                var table = $(settings.table);

                if (settings.head) {
                    if (settings.left > 0) {
                        var tr = table.find("thead tr");

                        tr.each(function (k, row) {
                            solverLeftColspan(row, function (cell) {
                                $(cell).css("z-index", settings['z-index'] + 1);
                            });
                        });
                    }

                    if (settings.right > 0) {
                        var tr = table.find("thead tr");

                        tr.each(function (k, row) {
                            solveRightColspan(row, function (cell) {
                                $(cell).css("z-index", settings['z-index'] + 1);
                            });
                        });
                    }
                }

                if (settings.foot) {
                    if (settings.left > 0) {
                        var tr = table.find("tfoot tr");

                        tr.each(function (k, row) {
                            solverLeftColspan(row, function (cell) {
                                $(cell).css("z-index", settings['z-index']);
                            });
                        });
                    }

                    if (settings.right > 0) {
                        var tr = table.find("tfoot tr");

                        tr.each(function (k, row) {
                            solveRightColspan(row, function (cell) {
                                $(cell).css("z-index", settings['z-index']);
                            });
                        });
                    }
                }
            }

            // Set style of table parent
            function setParent() {
                var parent = $(settings.parent);
                var table = $(settings.table);

                parent.append(table);
                parent
                    .css({
                        'overflow-x': 'auto',
                        'overflow-y': 'auto'
                    });

                parent.scroll(function () {
                    var scrollWidth = parent[0].scrollWidth;
                    var clientWidth = parent[0].clientWidth;
                    var scrollHeight = parent[0].scrollHeight;
                    var clientHeight = parent[0].clientHeight;
                    var top = parent.scrollTop();
                    var left = parent.scrollLeft();

                    if (settings.head)
                        this.find("thead tr > *").css("top", top);

                    if (settings.foot)
                        this.find("tfoot tr > *").css("bottom", scrollHeight - clientHeight - top);

                    if (settings.left > 0)
                        settings.leftColumns.css("left", left);

                    if (settings.right > 0)
                        settings.rightColumns.css("right", scrollWidth - clientWidth - left);
                }.bind(table));
            }

            // Set table head fixed
            function fixHead() {
                var thead = $(settings.table).find("thead");
                var tr = thead.find("tr");
                var cells = thead.find("tr > *");

                setBackground(cells);
                cells.css({
                    'position': 'relative'
                });
            }

            // Set table foot fixed
            function fixFoot() {
                var tfoot = $(settings.table).find("tfoot");
                var tr = tfoot.find("tr");
                var cells = tfoot.find("tr > *");

                setBackground(cells);
                cells.css({
                    'position': 'relative'
                });
            }

            // Set table left column fixed
            function fixLeft() {
                var table = $(settings.table);

                // var fixColumn = settings.left;

                settings.leftColumns = $();

                var tr = table.find("tr");
                tr.each(function (k, row) {

                    solverLeftColspan(row, function (cell) {
                        settings.leftColumns = settings.leftColumns.add(cell);
                    });
                    // var inc = 1;

                    // for(var i = 1; i <= fixColumn; i = i + inc) {
                    // 	var nth = inc > 1 ? i - 1 : i;

                    // 	var cell = $(row).find("*:nth-child(" + nth + ")");
                    // 	var colspan = cell.prop("colspan");

                    // 	settings.leftColumns = settings.leftColumns.add(cell);

                    // 	inc = colspan;
                    // }
                });

                var column = settings.leftColumns;

                column.each(function (k, cell) {
                    var cell = $(cell);

                    setBackground(cell);
                    cell.css({
                        'position': 'relative'
                    });
                });
            }

            // Set table right column fixed
            function fixRight() {
                var table = $(settings.table);

                var fixColumn = settings.right;

                settings.rightColumns = $();

                var tr_head = table.find('thead').find("tr");
                var tr_body = table.find('tbody').find("tr");
                var fcell = null;
                tr_head.each(function (k, row) {
                    solveRightColspanHead(row, function (cell) {
                        if (k === 0) {
                            fcell = cell;
                        }
                        settings.rightColumns = settings.rightColumns.add(fcell);
                    });
                });

                tr_body.each(function (k, row) {
                    solveRightColspanBody(row, function (cell) {
                        settings.rightColumns = settings.rightColumns.add(cell);
                    });
                });

                var column = settings.rightColumns;

                column.each(function (k, cell) {
                    var cell = $(cell);

                    setBackground(cell);
                    cell.css({
                        'position': 'relative',
                        'z-index': '9999'
                    });
                });

            }

            // Set fixed cells backgrounds
            function setBackground(elements) {
                elements.each(function (k, element) {
                    var element = $(element);
                    var parent = $(element).parent();

                    var elementBackground = element.css("background-color");
                    elementBackground = (elementBackground == "transparent" || elementBackground == "rgba(0, 0, 0, 0)") ? null : elementBackground;

                    var parentBackground = parent.css("background-color");
                    parentBackground = (parentBackground == "transparent" || parentBackground == "rgba(0, 0, 0, 0)") ? null : parentBackground;

                    var background = parentBackground ? parentBackground : "white";
                    background = elementBackground ? elementBackground : background;

                    element.css("background-color", background);
                });
            }

            function solverLeftColspan(row, action) {
                var fixColumn = settings.left;
                var inc = 1;

                for (var i = 1; i <= fixColumn; i = i + inc) {
                    var nth = inc > 1 ? i - 1 : i;

                    var cell = $(row).find("> *:nth-child(" + nth + ")");
                    var colspan = cell.prop("colspan");

                    if (cell.cellPos().left < fixColumn) {
                        action(cell);
                    }

                    inc = colspan;
                }
            }

            function solveRightColspanHead(row, action) {
                var fixColumn = settings.right;
                var inc = 1;
                for (var i = 1; i <= fixColumn; i = i + inc) {
                    var nth = inc > 1 ? i - 1 : i;

                    var cell = $(row).find("> *:nth-last-child(" + nth + ")");
                    var colspan = cell.prop("colspan");

                    action(cell);

                    inc = colspan;
                }
            }

            function solveRightColspanBody(row, action) {
                var fixColumn = settings.right;
                var inc = 1;
                for (var i = 1; i <= fixColumn; i = i + inc) {
                    var nth = inc > 1 ? i - 1 : i;

                    var cell = $(row).find("> *:nth-last-child(" + nth + ")");
                    var colspan = cell.prop("colspan");
                    action(cell);
                    inc = colspan;
                }
            }

            var defaults = {
                head: true,
                foot: false,
                left: 0,
                right: 0,
                'z-index': 0
            };

            var settings = $.extend({}, defaults, param);

            settings.table = this;
            settings.parent = $(settings.table).parent();
            setParent();

            if (settings.head == true) {
                fixHead();
            }

            if (settings.foot == true) {
                fixFoot();
            }

            if (settings.left > 0) {
                fixLeft();
            }

            if (settings.right > 0) {
                fixRight();
            }

            setCorner();

            $(settings.parent).trigger("scroll");

            $(window).resize(function () {
                $(settings.parent).trigger("scroll");
            });
        }
    };

})(jQuery);

/*  cellPos jQuery plugin
 ---------------------
 Get visual position of cell in HTML table (or its block like thead).
 Return value is object with "top" and "left" properties set to row and column index of top-left cell corner.
 Example of use:
 $("#myTable tbody td").each(function(){
 $(this).text( $(this).cellPos().top +", "+ $(this).cellPos().left );
 });
 */
(function ($) {
    /* scan individual table and set "cellPos" data in the form { left: x-coord, top: y-coord } */
    function scanTable($table) {
        var m = [];
        $table.children("tr").each(function (y, row) {
            $(row).children("td, th").each(function (x, cell) {
                var $cell = $(cell),
                    cspan = $cell.attr("colspan") | 0,
                    rspan = $cell.attr("rowspan") | 0,
                    tx, ty;
                cspan = cspan ? cspan : 1;
                rspan = rspan ? rspan : 1;
                for (; m[y] && m[y][x]; ++x);  //skip already occupied cells in current row
                for (tx = x; tx < x + cspan; ++tx) {  //mark matrix elements occupied by current cell with true
                    for (ty = y; ty < y + rspan; ++ty) {
                        if (!m[ty]) {  //fill missing rows
                            m[ty] = [];
                        }
                        m[ty][tx] = true;
                    }
                }
                var pos = {top: y, left: x};
                $cell.data("cellPos", pos);
            });
        });
    };

    /* plugin */
    $.fn.cellPos = function (rescan) {
        var $cell = this.first(),
            pos = $cell.data("cellPos");
        if (!pos || rescan) {
            var $table = $cell.closest("table, thead, tbody, tfoot");
            scanTable($table);
        }
        pos = $cell.data("cellPos");
        return pos;
    }
})(jQuery);
// jQuery to collapse the navbar on scroll
function collapseNavbar() {
    if ($(".navbar").offset() && $(".navbar").offset().top > 50) {
        $(".navbar-main").addClass("top-nav-collapse");

    } else {
        $(".navbar-main").removeClass("top-nav-collapse");
    }
}

$(window).scroll(collapseNavbar);
$(document).ready(collapseNavbar);

// jQuery for page scrolling feature 
$(function () {
    $('a.page-scroll').bind('click', function (event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });
});

// Closes the Responsive Menu on Menu Item Click
$(function () {
    $('.navbar-collapse ul li a').click(function () {
        if ($(this).attr('class') != 'dropdown-toggle active' && $(this).attr('class') != 'dropdown-toggle') {
            $('.navbar-toggle:visible').click();
        }
    });
});

$(function () {
    $("[data-rel=tooltip]").tooltip({ html: true });
});


$(function () {
    // the dropdown 
    // $('.chosen').chosen({
    //     no_results_text: "Not found",
    //     allow_single_deselect: true
    // });

    // The date picker
    // $( ".datepicker-13" ).datepicker();
    $('#txtStartDate, #txtEndDate').datepicker({
        // showOn: "both",
        beforeShow: customRange,
        dateFormat: "M dd, yy",
    });

    function customRange(input) {
        if (input.id == 'txtEndDate') {
            var minDate = new Date($('#txtStartDate').val());
            minDate.setDate(minDate.getDate())
            return {
                minDate: minDate
            };
        }
        return {}
    }

});
$("#selectid2").select2();
$("#selectid22").select2();
$("#selectid3").select2();
$("#selectid4").select2();
$(".js-example-basic-multiple2").select2();
$(".js-example-basic-multiple22").select2();

$(".js-example-basic-multiple").select2();
$('.select2-container--default').css('width', '100%');
$('.js-example-basic-multiple22').css('width', '100%');

// ============= show password toggle ============================
// 
(function ($) {
    $('#showpwdcheckbox').click(function () {
        if ($("#showpwd").attr('type') == 'password')
            $("#showpwd").attr('type', 'text');
        else
            $("#showpwd").attr('type', 'password');
    });
    // ================ calender =============

}(jQuery));
// ================== multiple select ===========

$(".js-example-basic-multiple").select2();
$(".js-example-tags").select2({
    tags: true
})

// ===================== check all ===================
$('#chk_all').change(function () {
    if (this.checked)
        $('.chkbx').attr('checked', true);
    else
        $('.chkbx').attr('checked', false);

})
// ====================== calender - view ====================
$(function () {
    $('#datetimepicker12').datetimepicker({
        inline: true,
        sideBySide: true
    });
    $('.timepicker').hide();
    $('.datepicker').addClass('dateview-calender');

});
$(window).load(function () {

var size = 1;
var button = 1;
var button_class = "gallery-header-center-right-links-current";
var normal_size_class = "gallery-content-center-normal";
var full_size_class = "gallery-content-center-full";
var $container = $('#gallery-content-center');
    
$container.isotope({itemSelector : 'img'});

function check_button(){
	$('.gallery-header-center-right-links').removeClass(button_class);
	if(button==1){
		$("#filter-all").addClass(button_class);
	}
	if(button==2){
		$("#filter-culture").addClass(button_class);
	}
	if(button==3){
		$("#filter-office").addClass(button_class);
	}	
	if(button==4){
		$("#filter-events").addClass(button_class);
	}	
	if(button==5){
		$("#filter-outing").addClass(button_class);
	}	
}
	
$("#filter-all").click(function() { $container.isotope({ filter: '.all' }); button = 1; check_button(); });
$("#filter-culture").click(function() {  $container.isotope({ filter: '.culture' }); button = 2; check_button();  });
$("#filter-office").click(function() {  $container.isotope({ filter: '.office' }); button = 3; check_button();  });
$("#filter-events").click(function() {  $container.isotope({ filter: '.events' }); button = 4; check_button();  });
$("#filter-outing").click(function() {  $container.isotope({ filter: '.outing' }); button = 5; check_button();  });
check_button();
check_size();

});
/*!
 * Isotope PACKAGED v3.0.4
 *
 * Licensed GPLv3 for open source use
 * or Isotope Commercial License for commercial use
 *
 * http://isotope.metafizzy.co
 * Copyright 2017 Metafizzy
 */

!function(t,e){"function"==typeof define&&define.amd?define("jquery-bridget/jquery-bridget",["jquery"],function(i){return e(t,i)}):"object"==typeof module&&module.exports?module.exports=e(t,require("jquery")):t.jQueryBridget=e(t,t.jQuery)}(window,function(t,e){"use strict";function i(i,s,a){function u(t,e,o){var n,s="$()."+i+'("'+e+'")';return t.each(function(t,u){var h=a.data(u,i);if(!h)return void r(i+" not initialized. Cannot call methods, i.e. "+s);var d=h[e];if(!d||"_"==e.charAt(0))return void r(s+" is not a valid method");var l=d.apply(h,o);n=void 0===n?l:n}),void 0!==n?n:t}function h(t,e){t.each(function(t,o){var n=a.data(o,i);n?(n.option(e),n._init()):(n=new s(o,e),a.data(o,i,n))})}a=a||e||t.jQuery,a&&(s.prototype.option||(s.prototype.option=function(t){a.isPlainObject(t)&&(this.options=a.extend(!0,this.options,t))}),a.fn[i]=function(t){if("string"==typeof t){var e=n.call(arguments,1);return u(this,t,e)}return h(this,t),this},o(a))}function o(t){!t||t&&t.bridget||(t.bridget=i)}var n=Array.prototype.slice,s=t.console,r="undefined"==typeof s?function(){}:function(t){s.error(t)};return o(e||t.jQuery),i}),function(t,e){"function"==typeof define&&define.amd?define("ev-emitter/ev-emitter",e):"object"==typeof module&&module.exports?module.exports=e():t.EvEmitter=e()}("undefined"!=typeof window?window:this,function(){function t(){}var e=t.prototype;return e.on=function(t,e){if(t&&e){var i=this._events=this._events||{},o=i[t]=i[t]||[];return o.indexOf(e)==-1&&o.push(e),this}},e.once=function(t,e){if(t&&e){this.on(t,e);var i=this._onceEvents=this._onceEvents||{},o=i[t]=i[t]||{};return o[e]=!0,this}},e.off=function(t,e){var i=this._events&&this._events[t];if(i&&i.length){var o=i.indexOf(e);return o!=-1&&i.splice(o,1),this}},e.emitEvent=function(t,e){var i=this._events&&this._events[t];if(i&&i.length){var o=0,n=i[o];e=e||[];for(var s=this._onceEvents&&this._onceEvents[t];n;){var r=s&&s[n];r&&(this.off(t,n),delete s[n]),n.apply(this,e),o+=r?0:1,n=i[o]}return this}},t}),function(t,e){"use strict";"function"==typeof define&&define.amd?define("get-size/get-size",[],function(){return e()}):"object"==typeof module&&module.exports?module.exports=e():t.getSize=e()}(window,function(){"use strict";function t(t){var e=parseFloat(t),i=t.indexOf("%")==-1&&!isNaN(e);return i&&e}function e(){}function i(){for(var t={width:0,height:0,innerWidth:0,innerHeight:0,outerWidth:0,outerHeight:0},e=0;e<h;e++){var i=u[e];t[i]=0}return t}function o(t){var e=getComputedStyle(t);return e||a("Style returned "+e+". Are you running this code in a hidden iframe on Firefox? See http://bit.ly/getsizebug1"),e}function n(){if(!d){d=!0;var e=document.createElement("div");e.style.width="200px",e.style.padding="1px 2px 3px 4px",e.style.borderStyle="solid",e.style.borderWidth="1px 2px 3px 4px",e.style.boxSizing="border-box";var i=document.body||document.documentElement;i.appendChild(e);var n=o(e);s.isBoxSizeOuter=r=200==t(n.width),i.removeChild(e)}}function s(e){if(n(),"string"==typeof e&&(e=document.querySelector(e)),e&&"object"==typeof e&&e.nodeType){var s=o(e);if("none"==s.display)return i();var a={};a.width=e.offsetWidth,a.height=e.offsetHeight;for(var d=a.isBorderBox="border-box"==s.boxSizing,l=0;l<h;l++){var f=u[l],c=s[f],m=parseFloat(c);a[f]=isNaN(m)?0:m}var p=a.paddingLeft+a.paddingRight,y=a.paddingTop+a.paddingBottom,g=a.marginLeft+a.marginRight,v=a.marginTop+a.marginBottom,_=a.borderLeftWidth+a.borderRightWidth,I=a.borderTopWidth+a.borderBottomWidth,z=d&&r,x=t(s.width);x!==!1&&(a.width=x+(z?0:p+_));var S=t(s.height);return S!==!1&&(a.height=S+(z?0:y+I)),a.innerWidth=a.width-(p+_),a.innerHeight=a.height-(y+I),a.outerWidth=a.width+g,a.outerHeight=a.height+v,a}}var r,a="undefined"==typeof console?e:function(t){console.error(t)},u=["paddingLeft","paddingRight","paddingTop","paddingBottom","marginLeft","marginRight","marginTop","marginBottom","borderLeftWidth","borderRightWidth","borderTopWidth","borderBottomWidth"],h=u.length,d=!1;return s}),function(t,e){"use strict";"function"==typeof define&&define.amd?define("desandro-matches-selector/matches-selector",e):"object"==typeof module&&module.exports?module.exports=e():t.matchesSelector=e()}(window,function(){"use strict";var t=function(){var t=window.Element.prototype;if(t.matches)return"matches";if(t.matchesSelector)return"matchesSelector";for(var e=["webkit","moz","ms","o"],i=0;i<e.length;i++){var o=e[i],n=o+"MatchesSelector";if(t[n])return n}}();return function(e,i){return e[t](i)}}),function(t,e){"function"==typeof define&&define.amd?define("fizzy-ui-utils/utils",["desandro-matches-selector/matches-selector"],function(i){return e(t,i)}):"object"==typeof module&&module.exports?module.exports=e(t,require("desandro-matches-selector")):t.fizzyUIUtils=e(t,t.matchesSelector)}(window,function(t,e){var i={};i.extend=function(t,e){for(var i in e)t[i]=e[i];return t},i.modulo=function(t,e){return(t%e+e)%e},i.makeArray=function(t){var e=[];if(Array.isArray(t))e=t;else if(t&&"object"==typeof t&&"number"==typeof t.length)for(var i=0;i<t.length;i++)e.push(t[i]);else e.push(t);return e},i.removeFrom=function(t,e){var i=t.indexOf(e);i!=-1&&t.splice(i,1)},i.getParent=function(t,i){for(;t.parentNode&&t!=document.body;)if(t=t.parentNode,e(t,i))return t},i.getQueryElement=function(t){return"string"==typeof t?document.querySelector(t):t},i.handleEvent=function(t){var e="on"+t.type;this[e]&&this[e](t)},i.filterFindElements=function(t,o){t=i.makeArray(t);var n=[];return t.forEach(function(t){if(t instanceof HTMLElement){if(!o)return void n.push(t);e(t,o)&&n.push(t);for(var i=t.querySelectorAll(o),s=0;s<i.length;s++)n.push(i[s])}}),n},i.debounceMethod=function(t,e,i){var o=t.prototype[e],n=e+"Timeout";t.prototype[e]=function(){var t=this[n];t&&clearTimeout(t);var e=arguments,s=this;this[n]=setTimeout(function(){o.apply(s,e),delete s[n]},i||100)}},i.docReady=function(t){var e=document.readyState;"complete"==e||"interactive"==e?setTimeout(t):document.addEventListener("DOMContentLoaded",t)},i.toDashed=function(t){return t.replace(/(.)([A-Z])/g,function(t,e,i){return e+"-"+i}).toLowerCase()};var o=t.console;return i.htmlInit=function(e,n){i.docReady(function(){var s=i.toDashed(n),r="data-"+s,a=document.querySelectorAll("["+r+"]"),u=document.querySelectorAll(".js-"+s),h=i.makeArray(a).concat(i.makeArray(u)),d=r+"-options",l=t.jQuery;h.forEach(function(t){var i,s=t.getAttribute(r)||t.getAttribute(d);try{i=s&&JSON.parse(s)}catch(a){return void(o&&o.error("Error parsing "+r+" on "+t.className+": "+a))}var u=new e(t,i);l&&l.data(t,n,u)})})},i}),function(t,e){"function"==typeof define&&define.amd?define("outlayer/item",["ev-emitter/ev-emitter","get-size/get-size"],e):"object"==typeof module&&module.exports?module.exports=e(require("ev-emitter"),require("get-size")):(t.Outlayer={},t.Outlayer.Item=e(t.EvEmitter,t.getSize))}(window,function(t,e){"use strict";function i(t){for(var e in t)return!1;return e=null,!0}function o(t,e){t&&(this.element=t,this.layout=e,this.position={x:0,y:0},this._create())}function n(t){return t.replace(/([A-Z])/g,function(t){return"-"+t.toLowerCase()})}var s=document.documentElement.style,r="string"==typeof s.transition?"transition":"WebkitTransition",a="string"==typeof s.transform?"transform":"WebkitTransform",u={WebkitTransition:"webkitTransitionEnd",transition:"transitionend"}[r],h={transform:a,transition:r,transitionDuration:r+"Duration",transitionProperty:r+"Property",transitionDelay:r+"Delay"},d=o.prototype=Object.create(t.prototype);d.constructor=o,d._create=function(){this._transn={ingProperties:{},clean:{},onEnd:{}},this.css({position:"absolute"})},d.handleEvent=function(t){var e="on"+t.type;this[e]&&this[e](t)},d.getSize=function(){this.size=e(this.element)},d.css=function(t){var e=this.element.style;for(var i in t){var o=h[i]||i;e[o]=t[i]}},d.getPosition=function(){var t=getComputedStyle(this.element),e=this.layout._getOption("originLeft"),i=this.layout._getOption("originTop"),o=t[e?"left":"right"],n=t[i?"top":"bottom"],s=this.layout.size,r=o.indexOf("%")!=-1?parseFloat(o)/100*s.width:parseInt(o,10),a=n.indexOf("%")!=-1?parseFloat(n)/100*s.height:parseInt(n,10);r=isNaN(r)?0:r,a=isNaN(a)?0:a,r-=e?s.paddingLeft:s.paddingRight,a-=i?s.paddingTop:s.paddingBottom,this.position.x=r,this.position.y=a},d.layoutPosition=function(){var t=this.layout.size,e={},i=this.layout._getOption("originLeft"),o=this.layout._getOption("originTop"),n=i?"paddingLeft":"paddingRight",s=i?"left":"right",r=i?"right":"left",a=this.position.x+t[n];e[s]=this.getXValue(a),e[r]="";var u=o?"paddingTop":"paddingBottom",h=o?"top":"bottom",d=o?"bottom":"top",l=this.position.y+t[u];e[h]=this.getYValue(l),e[d]="",this.css(e),this.emitEvent("layout",[this])},d.getXValue=function(t){var e=this.layout._getOption("horizontal");return this.layout.options.percentPosition&&!e?t/this.layout.size.width*100+"%":t+"px"},d.getYValue=function(t){var e=this.layout._getOption("horizontal");return this.layout.options.percentPosition&&e?t/this.layout.size.height*100+"%":t+"px"},d._transitionTo=function(t,e){this.getPosition();var i=this.position.x,o=this.position.y,n=parseInt(t,10),s=parseInt(e,10),r=n===this.position.x&&s===this.position.y;if(this.setPosition(t,e),r&&!this.isTransitioning)return void this.layoutPosition();var a=t-i,u=e-o,h={};h.transform=this.getTranslate(a,u),this.transition({to:h,onTransitionEnd:{transform:this.layoutPosition},isCleaning:!0})},d.getTranslate=function(t,e){var i=this.layout._getOption("originLeft"),o=this.layout._getOption("originTop");return t=i?t:-t,e=o?e:-e,"translate3d("+t+"px, "+e+"px, 0)"},d.goTo=function(t,e){this.setPosition(t,e),this.layoutPosition()},d.moveTo=d._transitionTo,d.setPosition=function(t,e){this.position.x=parseInt(t,10),this.position.y=parseInt(e,10)},d._nonTransition=function(t){this.css(t.to),t.isCleaning&&this._removeStyles(t.to);for(var e in t.onTransitionEnd)t.onTransitionEnd[e].call(this)},d.transition=function(t){if(!parseFloat(this.layout.options.transitionDuration))return void this._nonTransition(t);var e=this._transn;for(var i in t.onTransitionEnd)e.onEnd[i]=t.onTransitionEnd[i];for(i in t.to)e.ingProperties[i]=!0,t.isCleaning&&(e.clean[i]=!0);if(t.from){this.css(t.from);var o=this.element.offsetHeight;o=null}this.enableTransition(t.to),this.css(t.to),this.isTransitioning=!0};var l="opacity,"+n(a);d.enableTransition=function(){if(!this.isTransitioning){var t=this.layout.options.transitionDuration;t="number"==typeof t?t+"ms":t,this.css({transitionProperty:l,transitionDuration:t,transitionDelay:this.staggerDelay||0}),this.element.addEventListener(u,this,!1)}},d.onwebkitTransitionEnd=function(t){this.ontransitionend(t)},d.onotransitionend=function(t){this.ontransitionend(t)};var f={"-webkit-transform":"transform"};d.ontransitionend=function(t){if(t.target===this.element){var e=this._transn,o=f[t.propertyName]||t.propertyName;if(delete e.ingProperties[o],i(e.ingProperties)&&this.disableTransition(),o in e.clean&&(this.element.style[t.propertyName]="",delete e.clean[o]),o in e.onEnd){var n=e.onEnd[o];n.call(this),delete e.onEnd[o]}this.emitEvent("transitionEnd",[this])}},d.disableTransition=function(){this.removeTransitionStyles(),this.element.removeEventListener(u,this,!1),this.isTransitioning=!1},d._removeStyles=function(t){var e={};for(var i in t)e[i]="";this.css(e)};var c={transitionProperty:"",transitionDuration:"",transitionDelay:""};return d.removeTransitionStyles=function(){this.css(c)},d.stagger=function(t){t=isNaN(t)?0:t,this.staggerDelay=t+"ms"},d.removeElem=function(){this.element.parentNode.removeChild(this.element),this.css({display:""}),this.emitEvent("remove",[this])},d.remove=function(){return r&&parseFloat(this.layout.options.transitionDuration)?(this.once("transitionEnd",function(){this.removeElem()}),void this.hide()):void this.removeElem()},d.reveal=function(){delete this.isHidden,this.css({display:""});var t=this.layout.options,e={},i=this.getHideRevealTransitionEndProperty("visibleStyle");e[i]=this.onRevealTransitionEnd,this.transition({from:t.hiddenStyle,to:t.visibleStyle,isCleaning:!0,onTransitionEnd:e})},d.onRevealTransitionEnd=function(){this.isHidden||this.emitEvent("reveal")},d.getHideRevealTransitionEndProperty=function(t){var e=this.layout.options[t];if(e.opacity)return"opacity";for(var i in e)return i},d.hide=function(){this.isHidden=!0,this.css({display:""});var t=this.layout.options,e={},i=this.getHideRevealTransitionEndProperty("hiddenStyle");e[i]=this.onHideTransitionEnd,this.transition({from:t.visibleStyle,to:t.hiddenStyle,isCleaning:!0,onTransitionEnd:e})},d.onHideTransitionEnd=function(){this.isHidden&&(this.css({display:"none"}),this.emitEvent("hide"))},d.destroy=function(){this.css({position:"",left:"",right:"",top:"",bottom:"",transition:"",transform:""})},o}),function(t,e){"use strict";"function"==typeof define&&define.amd?define("outlayer/outlayer",["ev-emitter/ev-emitter","get-size/get-size","fizzy-ui-utils/utils","./item"],function(i,o,n,s){return e(t,i,o,n,s)}):"object"==typeof module&&module.exports?module.exports=e(t,require("ev-emitter"),require("get-size"),require("fizzy-ui-utils"),require("./item")):t.Outlayer=e(t,t.EvEmitter,t.getSize,t.fizzyUIUtils,t.Outlayer.Item)}(window,function(t,e,i,o,n){"use strict";function s(t,e){var i=o.getQueryElement(t);if(!i)return void(u&&u.error("Bad element for "+this.constructor.namespace+": "+(i||t)));this.element=i,h&&(this.$element=h(this.element)),this.options=o.extend({},this.constructor.defaults),this.option(e);var n=++l;this.element.outlayerGUID=n,f[n]=this,this._create();var s=this._getOption("initLayout");s&&this.layout()}function r(t){function e(){t.apply(this,arguments)}return e.prototype=Object.create(t.prototype),e.prototype.constructor=e,e}function a(t){if("number"==typeof t)return t;var e=t.match(/(^\d*\.?\d*)(\w*)/),i=e&&e[1],o=e&&e[2];if(!i.length)return 0;i=parseFloat(i);var n=m[o]||1;return i*n}var u=t.console,h=t.jQuery,d=function(){},l=0,f={};s.namespace="outlayer",s.Item=n,s.defaults={containerStyle:{position:"relative"},initLayout:!0,originLeft:!0,originTop:!0,resize:!0,resizeContainer:!0,transitionDuration:"0.4s",hiddenStyle:{opacity:0,transform:"scale(0.001)"},visibleStyle:{opacity:1,transform:"scale(1)"}};var c=s.prototype;o.extend(c,e.prototype),c.option=function(t){o.extend(this.options,t)},c._getOption=function(t){var e=this.constructor.compatOptions[t];return e&&void 0!==this.options[e]?this.options[e]:this.options[t]},s.compatOptions={initLayout:"isInitLayout",horizontal:"isHorizontal",layoutInstant:"isLayoutInstant",originLeft:"isOriginLeft",originTop:"isOriginTop",resize:"isResizeBound",resizeContainer:"isResizingContainer"},c._create=function(){this.reloadItems(),this.stamps=[],this.stamp(this.options.stamp),o.extend(this.element.style,this.options.containerStyle);var t=this._getOption("resize");t&&this.bindResize()},c.reloadItems=function(){this.items=this._itemize(this.element.children)},c._itemize=function(t){for(var e=this._filterFindItemElements(t),i=this.constructor.Item,o=[],n=0;n<e.length;n++){var s=e[n],r=new i(s,this);o.push(r)}return o},c._filterFindItemElements=function(t){return o.filterFindElements(t,this.options.itemSelector)},c.getItemElements=function(){return this.items.map(function(t){return t.element})},c.layout=function(){this._resetLayout(),this._manageStamps();var t=this._getOption("layoutInstant"),e=void 0!==t?t:!this._isLayoutInited;this.layoutItems(this.items,e),this._isLayoutInited=!0},c._init=c.layout,c._resetLayout=function(){this.getSize()},c.getSize=function(){this.size=i(this.element)},c._getMeasurement=function(t,e){var o,n=this.options[t];n?("string"==typeof n?o=this.element.querySelector(n):n instanceof HTMLElement&&(o=n),this[t]=o?i(o)[e]:n):this[t]=0},c.layoutItems=function(t,e){t=this._getItemsForLayout(t),this._layoutItems(t,e),this._postLayout()},c._getItemsForLayout=function(t){return t.filter(function(t){return!t.isIgnored})},c._layoutItems=function(t,e){if(this._emitCompleteOnItems("layout",t),t&&t.length){var i=[];t.forEach(function(t){var o=this._getItemLayoutPosition(t);o.item=t,o.isInstant=e||t.isLayoutInstant,i.push(o)},this),this._processLayoutQueue(i)}},c._getItemLayoutPosition=function(){return{x:0,y:0}},c._processLayoutQueue=function(t){this.updateStagger(),t.forEach(function(t,e){this._positionItem(t.item,t.x,t.y,t.isInstant,e)},this)},c.updateStagger=function(){var t=this.options.stagger;return null===t||void 0===t?void(this.stagger=0):(this.stagger=a(t),this.stagger)},c._positionItem=function(t,e,i,o,n){o?t.goTo(e,i):(t.stagger(n*this.stagger),t.moveTo(e,i))},c._postLayout=function(){this.resizeContainer()},c.resizeContainer=function(){var t=this._getOption("resizeContainer");if(t){var e=this._getContainerSize();e&&(this._setContainerMeasure(e.width,!0),this._setContainerMeasure(e.height,!1))}},c._getContainerSize=d,c._setContainerMeasure=function(t,e){if(void 0!==t){var i=this.size;i.isBorderBox&&(t+=e?i.paddingLeft+i.paddingRight+i.borderLeftWidth+i.borderRightWidth:i.paddingBottom+i.paddingTop+i.borderTopWidth+i.borderBottomWidth),t=Math.max(t,0),this.element.style[e?"width":"height"]=t+"px"}},c._emitCompleteOnItems=function(t,e){function i(){n.dispatchEvent(t+"Complete",null,[e])}function o(){r++,r==s&&i()}var n=this,s=e.length;if(!e||!s)return void i();var r=0;e.forEach(function(e){e.once(t,o)})},c.dispatchEvent=function(t,e,i){var o=e?[e].concat(i):i;if(this.emitEvent(t,o),h)if(this.$element=this.$element||h(this.element),e){var n=h.Event(e);n.type=t,this.$element.trigger(n,i)}else this.$element.trigger(t,i)},c.ignore=function(t){var e=this.getItem(t);e&&(e.isIgnored=!0)},c.unignore=function(t){var e=this.getItem(t);e&&delete e.isIgnored},c.stamp=function(t){t=this._find(t),t&&(this.stamps=this.stamps.concat(t),t.forEach(this.ignore,this))},c.unstamp=function(t){t=this._find(t),t&&t.forEach(function(t){o.removeFrom(this.stamps,t),this.unignore(t)},this)},c._find=function(t){if(t)return"string"==typeof t&&(t=this.element.querySelectorAll(t)),t=o.makeArray(t)},c._manageStamps=function(){this.stamps&&this.stamps.length&&(this._getBoundingRect(),this.stamps.forEach(this._manageStamp,this))},c._getBoundingRect=function(){var t=this.element.getBoundingClientRect(),e=this.size;this._boundingRect={left:t.left+e.paddingLeft+e.borderLeftWidth,top:t.top+e.paddingTop+e.borderTopWidth,right:t.right-(e.paddingRight+e.borderRightWidth),bottom:t.bottom-(e.paddingBottom+e.borderBottomWidth)}},c._manageStamp=d,c._getElementOffset=function(t){var e=t.getBoundingClientRect(),o=this._boundingRect,n=i(t),s={left:e.left-o.left-n.marginLeft,top:e.top-o.top-n.marginTop,right:o.right-e.right-n.marginRight,bottom:o.bottom-e.bottom-n.marginBottom};return s},c.handleEvent=o.handleEvent,c.bindResize=function(){t.addEventListener("resize",this),this.isResizeBound=!0},c.unbindResize=function(){t.removeEventListener("resize",this),this.isResizeBound=!1},c.onresize=function(){this.resize()},o.debounceMethod(s,"onresize",100),c.resize=function(){this.isResizeBound&&this.needsResizeLayout()&&this.layout()},c.needsResizeLayout=function(){var t=i(this.element),e=this.size&&t;return e&&t.innerWidth!==this.size.innerWidth},c.addItems=function(t){var e=this._itemize(t);return e.length&&(this.items=this.items.concat(e)),e},c.appended=function(t){var e=this.addItems(t);e.length&&(this.layoutItems(e,!0),this.reveal(e))},c.prepended=function(t){var e=this._itemize(t);if(e.length){var i=this.items.slice(0);this.items=e.concat(i),this._resetLayout(),this._manageStamps(),this.layoutItems(e,!0),this.reveal(e),this.layoutItems(i)}},c.reveal=function(t){if(this._emitCompleteOnItems("reveal",t),t&&t.length){var e=this.updateStagger();t.forEach(function(t,i){t.stagger(i*e),t.reveal()})}},c.hide=function(t){if(this._emitCompleteOnItems("hide",t),t&&t.length){var e=this.updateStagger();t.forEach(function(t,i){t.stagger(i*e),t.hide()})}},c.revealItemElements=function(t){var e=this.getItems(t);this.reveal(e)},c.hideItemElements=function(t){var e=this.getItems(t);this.hide(e)},c.getItem=function(t){for(var e=0;e<this.items.length;e++){var i=this.items[e];if(i.element==t)return i}},c.getItems=function(t){t=o.makeArray(t);var e=[];return t.forEach(function(t){var i=this.getItem(t);i&&e.push(i)},this),e},c.remove=function(t){var e=this.getItems(t);this._emitCompleteOnItems("remove",e),e&&e.length&&e.forEach(function(t){t.remove(),o.removeFrom(this.items,t)},this)},c.destroy=function(){var t=this.element.style;t.height="",t.position="",t.width="",this.items.forEach(function(t){t.destroy()}),this.unbindResize();var e=this.element.outlayerGUID;delete f[e],delete this.element.outlayerGUID,h&&h.removeData(this.element,this.constructor.namespace)},s.data=function(t){t=o.getQueryElement(t);var e=t&&t.outlayerGUID;return e&&f[e]},s.create=function(t,e){var i=r(s);return i.defaults=o.extend({},s.defaults),o.extend(i.defaults,e),i.compatOptions=o.extend({},s.compatOptions),i.namespace=t,i.data=s.data,i.Item=r(n),o.htmlInit(i,t),h&&h.bridget&&h.bridget(t,i),i};var m={ms:1,s:1e3};return s.Item=n,s}),function(t,e){"function"==typeof define&&define.amd?define("isotope/js/item",["outlayer/outlayer"],e):"object"==typeof module&&module.exports?module.exports=e(require("outlayer")):(t.Isotope=t.Isotope||{},t.Isotope.Item=e(t.Outlayer))}(window,function(t){"use strict";function e(){t.Item.apply(this,arguments)}var i=e.prototype=Object.create(t.Item.prototype),o=i._create;i._create=function(){this.id=this.layout.itemGUID++,o.call(this),this.sortData={}},i.updateSortData=function(){if(!this.isIgnored){this.sortData.id=this.id,this.sortData["original-order"]=this.id,this.sortData.random=Math.random();var t=this.layout.options.getSortData,e=this.layout._sorters;for(var i in t){var o=e[i];this.sortData[i]=o(this.element,this)}}};var n=i.destroy;return i.destroy=function(){n.apply(this,arguments),this.css({display:""})},e}),function(t,e){"function"==typeof define&&define.amd?define("isotope/js/layout-mode",["get-size/get-size","outlayer/outlayer"],e):"object"==typeof module&&module.exports?module.exports=e(require("get-size"),require("outlayer")):(t.Isotope=t.Isotope||{},t.Isotope.LayoutMode=e(t.getSize,t.Outlayer))}(window,function(t,e){"use strict";function i(t){this.isotope=t,t&&(this.options=t.options[this.namespace],this.element=t.element,this.items=t.filteredItems,this.size=t.size)}var o=i.prototype,n=["_resetLayout","_getItemLayoutPosition","_manageStamp","_getContainerSize","_getElementOffset","needsResizeLayout","_getOption"];return n.forEach(function(t){o[t]=function(){return e.prototype[t].apply(this.isotope,arguments)}}),o.needsVerticalResizeLayout=function(){var e=t(this.isotope.element),i=this.isotope.size&&e;return i&&e.innerHeight!=this.isotope.size.innerHeight},o._getMeasurement=function(){this.isotope._getMeasurement.apply(this,arguments)},o.getColumnWidth=function(){this.getSegmentSize("column","Width")},o.getRowHeight=function(){this.getSegmentSize("row","Height")},o.getSegmentSize=function(t,e){var i=t+e,o="outer"+e;if(this._getMeasurement(i,o),!this[i]){var n=this.getFirstItemSize();this[i]=n&&n[o]||this.isotope.size["inner"+e]}},o.getFirstItemSize=function(){var e=this.isotope.filteredItems[0];return e&&e.element&&t(e.element)},o.layout=function(){this.isotope.layout.apply(this.isotope,arguments)},o.getSize=function(){this.isotope.getSize(),this.size=this.isotope.size},i.modes={},i.create=function(t,e){function n(){i.apply(this,arguments)}return n.prototype=Object.create(o),n.prototype.constructor=n,e&&(n.options=e),n.prototype.namespace=t,i.modes[t]=n,n},i}),function(t,e){"function"==typeof define&&define.amd?define("masonry/masonry",["outlayer/outlayer","get-size/get-size"],e):"object"==typeof module&&module.exports?module.exports=e(require("outlayer"),require("get-size")):t.Masonry=e(t.Outlayer,t.getSize)}(window,function(t,e){var i=t.create("masonry");i.compatOptions.fitWidth="isFitWidth";var o=i.prototype;return o._resetLayout=function(){this.getSize(),this._getMeasurement("columnWidth","outerWidth"),this._getMeasurement("gutter","outerWidth"),this.measureColumns(),this.colYs=[];for(var t=0;t<this.cols;t++)this.colYs.push(0);this.maxY=0,this.horizontalColIndex=0},o.measureColumns=function(){if(this.getContainerWidth(),!this.columnWidth){var t=this.items[0],i=t&&t.element;this.columnWidth=i&&e(i).outerWidth||this.containerWidth}var o=this.columnWidth+=this.gutter,n=this.containerWidth+this.gutter,s=n/o,r=o-n%o,a=r&&r<1?"round":"floor";s=Math[a](s),this.cols=Math.max(s,1)},o.getContainerWidth=function(){var t=this._getOption("fitWidth"),i=t?this.element.parentNode:this.element,o=e(i);this.containerWidth=o&&o.innerWidth},o._getItemLayoutPosition=function(t){t.getSize();var e=t.size.outerWidth%this.columnWidth,i=e&&e<1?"round":"ceil",o=Math[i](t.size.outerWidth/this.columnWidth);o=Math.min(o,this.cols);for(var n=this.options.horizontalOrder?"_getHorizontalColPosition":"_getTopColPosition",s=this[n](o,t),r={x:this.columnWidth*s.col,y:s.y},a=s.y+t.size.outerHeight,u=o+s.col,h=s.col;h<u;h++)this.colYs[h]=a;return r},o._getTopColPosition=function(t){var e=this._getTopColGroup(t),i=Math.min.apply(Math,e);return{col:e.indexOf(i),y:i}},o._getTopColGroup=function(t){if(t<2)return this.colYs;for(var e=[],i=this.cols+1-t,o=0;o<i;o++)e[o]=this._getColGroupY(o,t);return e},o._getColGroupY=function(t,e){if(e<2)return this.colYs[t];var i=this.colYs.slice(t,t+e);return Math.max.apply(Math,i)},o._getHorizontalColPosition=function(t,e){var i=this.horizontalColIndex%this.cols,o=t>1&&i+t>this.cols;i=o?0:i;var n=e.size.outerWidth&&e.size.outerHeight;return this.horizontalColIndex=n?i+t:this.horizontalColIndex,{col:i,y:this._getColGroupY(i,t)}},o._manageStamp=function(t){var i=e(t),o=this._getElementOffset(t),n=this._getOption("originLeft"),s=n?o.left:o.right,r=s+i.outerWidth,a=Math.floor(s/this.columnWidth);a=Math.max(0,a);var u=Math.floor(r/this.columnWidth);u-=r%this.columnWidth?0:1,u=Math.min(this.cols-1,u);for(var h=this._getOption("originTop"),d=(h?o.top:o.bottom)+i.outerHeight,l=a;l<=u;l++)this.colYs[l]=Math.max(d,this.colYs[l])},o._getContainerSize=function(){this.maxY=Math.max.apply(Math,this.colYs);var t={height:this.maxY};return this._getOption("fitWidth")&&(t.width=this._getContainerFitWidth()),t},o._getContainerFitWidth=function(){for(var t=0,e=this.cols;--e&&0===this.colYs[e];)t++;return(this.cols-t)*this.columnWidth-this.gutter},o.needsResizeLayout=function(){var t=this.containerWidth;return this.getContainerWidth(),t!=this.containerWidth},i}),function(t,e){"function"==typeof define&&define.amd?define("isotope/js/layout-modes/masonry",["../layout-mode","masonry/masonry"],e):"object"==typeof module&&module.exports?module.exports=e(require("../layout-mode"),require("masonry-layout")):e(t.Isotope.LayoutMode,t.Masonry)}(window,function(t,e){"use strict";var i=t.create("masonry"),o=i.prototype,n={_getElementOffset:!0,layout:!0,_getMeasurement:!0};for(var s in e.prototype)n[s]||(o[s]=e.prototype[s]);var r=o.measureColumns;o.measureColumns=function(){this.items=this.isotope.filteredItems,r.call(this)};var a=o._getOption;return o._getOption=function(t){return"fitWidth"==t?void 0!==this.options.isFitWidth?this.options.isFitWidth:this.options.fitWidth:a.apply(this.isotope,arguments)},i}),function(t,e){"function"==typeof define&&define.amd?define("isotope/js/layout-modes/fit-rows",["../layout-mode"],e):"object"==typeof exports?module.exports=e(require("../layout-mode")):e(t.Isotope.LayoutMode)}(window,function(t){"use strict";var e=t.create("fitRows"),i=e.prototype;return i._resetLayout=function(){this.x=0,this.y=0,this.maxY=0,this._getMeasurement("gutter","outerWidth")},i._getItemLayoutPosition=function(t){t.getSize();var e=t.size.outerWidth+this.gutter,i=this.isotope.size.innerWidth+this.gutter;0!==this.x&&e+this.x>i&&(this.x=0,this.y=this.maxY);var o={x:this.x,y:this.y};return this.maxY=Math.max(this.maxY,this.y+t.size.outerHeight),this.x+=e,o},i._getContainerSize=function(){return{height:this.maxY}},e}),function(t,e){"function"==typeof define&&define.amd?define("isotope/js/layout-modes/vertical",["../layout-mode"],e):"object"==typeof module&&module.exports?module.exports=e(require("../layout-mode")):e(t.Isotope.LayoutMode)}(window,function(t){"use strict";var e=t.create("vertical",{horizontalAlignment:0}),i=e.prototype;return i._resetLayout=function(){this.y=0},i._getItemLayoutPosition=function(t){t.getSize();var e=(this.isotope.size.innerWidth-t.size.outerWidth)*this.options.horizontalAlignment,i=this.y;return this.y+=t.size.outerHeight,{x:e,y:i}},i._getContainerSize=function(){return{height:this.y}},e}),function(t,e){"function"==typeof define&&define.amd?define(["outlayer/outlayer","get-size/get-size","desandro-matches-selector/matches-selector","fizzy-ui-utils/utils","isotope/js/item","isotope/js/layout-mode","isotope/js/layout-modes/masonry","isotope/js/layout-modes/fit-rows","isotope/js/layout-modes/vertical"],function(i,o,n,s,r,a){return e(t,i,o,n,s,r,a)}):"object"==typeof module&&module.exports?module.exports=e(t,require("outlayer"),require("get-size"),require("desandro-matches-selector"),require("fizzy-ui-utils"),require("isotope/js/item"),require("isotope/js/layout-mode"),require("isotope/js/layout-modes/masonry"),require("isotope/js/layout-modes/fit-rows"),require("isotope/js/layout-modes/vertical")):t.Isotope=e(t,t.Outlayer,t.getSize,t.matchesSelector,t.fizzyUIUtils,t.Isotope.Item,t.Isotope.LayoutMode)}(window,function(t,e,i,o,n,s,r){function a(t,e){return function(i,o){for(var n=0;n<t.length;n++){var s=t[n],r=i.sortData[s],a=o.sortData[s];if(r>a||r<a){var u=void 0!==e[s]?e[s]:e,h=u?1:-1;return(r>a?1:-1)*h}}return 0}}var u=t.jQuery,h=String.prototype.trim?function(t){return t.trim()}:function(t){return t.replace(/^\s+|\s+$/g,"")},d=e.create("isotope",{layoutMode:"masonry",isJQueryFiltering:!0,sortAscending:!0});d.Item=s,d.LayoutMode=r;var l=d.prototype;l._create=function(){this.itemGUID=0,this._sorters={},this._getSorters(),e.prototype._create.call(this),this.modes={},this.filteredItems=this.items,this.sortHistory=["original-order"];for(var t in r.modes)this._initLayoutMode(t)},l.reloadItems=function(){this.itemGUID=0,e.prototype.reloadItems.call(this)},l._itemize=function(){for(var t=e.prototype._itemize.apply(this,arguments),i=0;i<t.length;i++){var o=t[i];o.id=this.itemGUID++}return this._updateItemsSortData(t),t},l._initLayoutMode=function(t){var e=r.modes[t],i=this.options[t]||{};this.options[t]=e.options?n.extend(e.options,i):i,this.modes[t]=new e(this)},l.layout=function(){return!this._isLayoutInited&&this._getOption("initLayout")?void this.arrange():void this._layout()},l._layout=function(){var t=this._getIsInstant();this._resetLayout(),this._manageStamps(),this.layoutItems(this.filteredItems,t),this._isLayoutInited=!0},l.arrange=function(t){this.option(t),this._getIsInstant();var e=this._filter(this.items);this.filteredItems=e.matches,this._bindArrangeComplete(),this._isInstant?this._noTransition(this._hideReveal,[e]):this._hideReveal(e),this._sort(),this._layout()},l._init=l.arrange,l._hideReveal=function(t){this.reveal(t.needReveal),this.hide(t.needHide)},l._getIsInstant=function(){var t=this._getOption("layoutInstant"),e=void 0!==t?t:!this._isLayoutInited;return this._isInstant=e,e},l._bindArrangeComplete=function(){function t(){e&&i&&o&&n.dispatchEvent("arrangeComplete",null,[n.filteredItems])}var e,i,o,n=this;this.once("layoutComplete",function(){e=!0,t()}),this.once("hideComplete",function(){i=!0,t()}),this.once("revealComplete",function(){o=!0,t()})},l._filter=function(t){var e=this.options.filter;e=e||"*";for(var i=[],o=[],n=[],s=this._getFilterTest(e),r=0;r<t.length;r++){var a=t[r];if(!a.isIgnored){var u=s(a);u&&i.push(a),u&&a.isHidden?o.push(a):u||a.isHidden||n.push(a)}}return{matches:i,needReveal:o,needHide:n}},l._getFilterTest=function(t){return u&&this.options.isJQueryFiltering?function(e){return u(e.element).is(t)}:"function"==typeof t?function(e){return t(e.element)}:function(e){return o(e.element,t)}},l.updateSortData=function(t){
var e;t?(t=n.makeArray(t),e=this.getItems(t)):e=this.items,this._getSorters(),this._updateItemsSortData(e)},l._getSorters=function(){var t=this.options.getSortData;for(var e in t){var i=t[e];this._sorters[e]=f(i)}},l._updateItemsSortData=function(t){for(var e=t&&t.length,i=0;e&&i<e;i++){var o=t[i];o.updateSortData()}};var f=function(){function t(t){if("string"!=typeof t)return t;var i=h(t).split(" "),o=i[0],n=o.match(/^\[(.+)\]$/),s=n&&n[1],r=e(s,o),a=d.sortDataParsers[i[1]];return t=a?function(t){return t&&a(r(t))}:function(t){return t&&r(t)}}function e(t,e){return t?function(e){return e.getAttribute(t)}:function(t){var i=t.querySelector(e);return i&&i.textContent}}return t}();d.sortDataParsers={parseInt:function(t){return parseInt(t,10)},parseFloat:function(t){return parseFloat(t)}},l._sort=function(){if(this.options.sortBy){var t=n.makeArray(this.options.sortBy);this._getIsSameSortBy(t)||(this.sortHistory=t.concat(this.sortHistory));var e=a(this.sortHistory,this.options.sortAscending);this.filteredItems.sort(e)}},l._getIsSameSortBy=function(t){for(var e=0;e<t.length;e++)if(t[e]!=this.sortHistory[e])return!1;return!0},l._mode=function(){var t=this.options.layoutMode,e=this.modes[t];if(!e)throw new Error("No layout mode: "+t);return e.options=this.options[t],e},l._resetLayout=function(){e.prototype._resetLayout.call(this),this._mode()._resetLayout()},l._getItemLayoutPosition=function(t){return this._mode()._getItemLayoutPosition(t)},l._manageStamp=function(t){this._mode()._manageStamp(t)},l._getContainerSize=function(){return this._mode()._getContainerSize()},l.needsResizeLayout=function(){return this._mode().needsResizeLayout()},l.appended=function(t){var e=this.addItems(t);if(e.length){var i=this._filterRevealAdded(e);this.filteredItems=this.filteredItems.concat(i)}},l.prepended=function(t){var e=this._itemize(t);if(e.length){this._resetLayout(),this._manageStamps();var i=this._filterRevealAdded(e);this.layoutItems(this.filteredItems),this.filteredItems=i.concat(this.filteredItems),this.items=e.concat(this.items)}},l._filterRevealAdded=function(t){var e=this._filter(t);return this.hide(e.needHide),this.reveal(e.matches),this.layoutItems(e.matches,!0),e.matches},l.insert=function(t){var e=this.addItems(t);if(e.length){var i,o,n=e.length;for(i=0;i<n;i++)o=e[i],this.element.appendChild(o.element);var s=this._filter(e).matches;for(i=0;i<n;i++)e[i].isLayoutInstant=!0;for(this.arrange(),i=0;i<n;i++)delete e[i].isLayoutInstant;this.reveal(s)}};var c=l.remove;return l.remove=function(t){t=n.makeArray(t);var e=this.getItems(t);c.call(this,t);for(var i=e&&e.length,o=0;i&&o<i;o++){var s=e[o];n.removeFrom(this.filteredItems,s)}},l.shuffle=function(){for(var t=0;t<this.items.length;t++){var e=this.items[t];e.sortData.random=Math.random()}this.options.sortBy="random",this._sort(),this._layout()},l._noTransition=function(t,e){var i=this.options.transitionDuration;this.options.transitionDuration=0;var o=t.apply(this,e);return this.options.transitionDuration=i,o},l.getFilteredItemElements=function(){return this.filteredItems.map(function(t){return t.element})},d});
/*
*  __  __  _____   _____ 
* |  \/  |/ ____| / ____|
* | \  / | |  __ | (___  _ __   __ _  ___ ___    _  ___ 
* | |\/| | | |_ | \___ \| '_ \ / _` |/ __/ _ \  (_)/ __| 
* | |  | | |__| | ____) | |_) | (_| | (_|  __/_ | |\__ \
* |_|  |_|\_____||_____/| .__/ \__,_|\___\___(_)| ||___/
*                       | |                    _| |
*                       |_|                   |___,
* MG Space
*
* Copyright (c) 2016 Bryce Mullican (http://brycemullican.com)
*
* By Bryce Mullican (@BryceMullican)
* Licensed under the MIT license.
*
* @link https://github.com/Mad-Genius/mg-space
* @author Bryce Mullican
* @version 1.0.0
*/
;(function ( $, window, document, undefined ) {

    "use strict";

    // Create the defaults once
    var mgSpace = "mgSpace",
        defaults = {

            // Breakpoints at which the accordian changes # of columns
            breakpointColumns: [
                {
                    breakpoint: 0,
                    column: 1
                },
                {
                    breakpoint: 568,
                    column: 2
                },
                {
                    breakpoint: 768,
                    column: 3
                },
                {
                    breakpoint: 1200,
                    column: 4
                }
            ],

            // Default selectors
            rowWrapper: ".mg-rows",
            row: ".mg-row",
            targetWrapper: ".mg-targets",
            target: ".mg-target",
            trigger: ".mg-trigger",
            close: ".mg-close",

            // Default target padding top/bottom and row bottom margin
            rowMargin: 25, // Set to zero for gridless
            targetPadding: 120, // Padding top/bottom inside target gets divided by 2

            useHash: false, // Set to true for history
            useOnpageHash: false, // Set true for onpage history
            hashTitle: "#/item-", // Must include `#` hash symbol

            // MISC          
            useIndicator: true
        },
        shouldClick = true,
        activatingHash = false;

    // The actual plugin constructor
    function MGSpace ( element, options ) {
        var _ = this;

        _.$mgSpace = $(element);
        _.options = $.extend( {}, defaults, options );
        _._defaults = defaults;
        _._name =  mgSpace;
        _.init();

        //console.log('MADE IT HERE');
    }

    // Avoid MGSpace.prototype conflicts
    $.extend(MGSpace.prototype, {
        init: function () {
            var _ = this,
                cols = _.setColumns();

            // Set Columns
            $(_.options.rowWrapper, _.$mgSpace).attr('data-cols', cols);

            _.$rows = _.$mgSpace.find(_.options.rowWrapper).children('');
            _.$targets = _.$mgSpace.find(_.options.targetWrapper).children('');

            // Set Rows and Targets
            _.setRows($(_.$rows, _.$mgSpace));
            _.setRows($(_.$targets, _.$mgSpace));

            // Add Row Margin if Greater Than Zero (0)
            if (_.options.rowMargin > 0) {
                _.$mgSpace.prepend('<style scoped>'+_.options.row+'{margin-bottom:'+_.options.rowMargin+'px}</style>');
            }


            // NEED TO BUILD BETTER CLICK HANDLER
            _.$mgSpace.on('click.mg', _.options.trigger, function (event) {
                event.preventDefault();
            });

            // CLICK HANDLING SHOULD GO THROUGH ROW CONTROLLER
            _.$mgSpace.on('click.mg.trigger', _.options.trigger, {mgSpace:_}, _.clickHandler);
            _.$mgSpace.on('click.mg.close', _.options.close, {close:true, mgSpace:_}, _.clickHandler);

            $(window).on('resize.mg', function () {
                cols = _.setColumns();
                _.setRows($(_.options.row, _.$mgSpace));

                if (cols != $(_.options.rowWrapper).attr('data-cols')) {
                    $(_.options.rowWrapper).attr('data-cols',cols);

                    // Close
                    $(_.options.target+'-open').removeClass(_.stripDot(_.options.target)+'-open').removeAttr('style');
                    $(_.options.row+'-open').removeClass(_.stripDot(_.options.row)+'-open');
                    $('.mg-space').remove();

                    _.$mgSpace.trigger('afterCloseTarget', [_]);
                }


                if($(_.options.target+'-open').length){
                    _.resizeSpace(_.options.row+'-open');
                }                
            });

            // LOAD HASH IF EXISTS
            if (window.location.hash && _.options.useHash) {
                setTimeout(function () {
                    var sectionID =  window.location.hash.replace(_.options.hashTitle, "").split('-'),
                        rowItem = _.options.row+'[data-section="' + sectionID[0] + '"][data-id="' + sectionID[1] + '"]';

                    if (sectionID[0] == _.$mgSpace.index()) {
                         activatingHash = true;
                        _.openRow(rowItem);
                    }

                }, 400);
            }

            $(window).on('hashchange.mg', function () {
                if (window.location.hash && _.options.useOnpageHash) {
                    var sectionID =  window.location.hash.replace(_.options.hashTitle, "").split('-'),
                        rowItem = _.options.row+'[data-section="' + sectionID[0] + '"][data-id="' + sectionID[1] + '"]';

                    _.scrollToTop(rowItem);
                    activatingHash = true;

                    if (sectionID[0] == _.$mgSpace.index()) {
                        _.closeRow(300);

                        setTimeout(function () {
                            _.openRow(rowItem);
                        }, 400);
                    }
                } else {
                    _.closeRow(300);
                }               
            });            
        },

        rowController: function (element, event) {
            var _ = this,
                $rowItem = $(element).closest(_.options.row),
                itemSection = $rowItem.attr('data-section'),
                itemRow = $rowItem.attr('data-row');

            _.$mgSpace.off('click.trigger', _.options.trigger, _.clickHandler);

            if ((event.data && event.data.close) || $rowItem.hasClass(_.stripDot(_.options.row)+'-open')) {
                _.closeRow(300);
            } else {
                if ($(_.options.row+'-open[data-section="' + itemSection + '"][data-row="' + itemRow + '"]').length) {
                    // Same Row
                    //console.log('Same Row');

                    _.closeTarget(200);
                    _.resizeSpace($rowItem);
                    _.openTarget($rowItem);
                    _.scrollToTop($rowItem);                    
                } else if ($('.mg-space').hasClass('mg-space-open')) {
                    // New Row
                    //console.log('New Row');

                    _.closeTarget(200);
                    $('.mg-space').slideToggle(300, function () {
                        _.openRow($rowItem);
                    });
                } else {
                    _.openRow($rowItem);
                }
            }
        },

        openRow: function (element) {
            var _ = this;

            // Before Open Row Event Handler
            _.$mgSpace.trigger('beforeOpenRow', [_, element]);

            //console.log('Open Row');

            $(_.options.row+'-open').removeClass(_.stripDot(_.options.row)+'-open');

            _.openSpace(element);
            _.openTarget(element);
            _.scrollToTop(element); 
            
            // After Open Row Event Handler
            _.$mgSpace.trigger('afterOpenRow', [_, element]);
        },

        closeRow: function (speed) {
            var _ = this;

            //console.log('Close Row');

            _.closeTarget(speed);
            _.closeSpace(speed);
        },        

        openTarget: function (element) {
            var _ = this,
                itemSection = $(element).attr('data-section'),
                itemId = $(element).attr('data-id'),
                $itemTarget = $(_.options.target+'[data-section="' + itemSection + '"][data-id="' + itemId + '"]', _.$mgSpace);

            //console.log('Open Target');

            // Before Open Target Event Handler
            _.$mgSpace.trigger('beforeOpenTarget', [_]);

            $(element).addClass(_.stripDot(_.options.row)+'-open');

            // Offset Bug in Chrome Hack ON
            $('body').css('overflowY','scroll');

            $itemTarget.prepend('<a href="#" class="'+_.stripDot(_.options.close)+'"></a>');

            $itemTarget
                .removeAttr('style')
                .addClass(_.stripDot(_.options.target)+'-open')
                .css({
                    position: 'absolute',
                    top: $('.mg-space').position().top + $('.mg-space').parent().position().top,
                    zIndex: 2,
                    paddingTop: _.options.targetPadding/2,
                    paddingBottom: _.options.targetPadding/2
                })
                .slideDown(300, function(){
                    _.$mgSpace.on('click.mg.trigger', _.options.trigger, {mgSpace:_}, _.clickHandler);
                    $(_.options.close).fadeIn(200);

                    // After Open Target Event Handler
                    _.$mgSpace.trigger('afterOpenTarget', [_, $itemTarget]);                    
                });

            // Offset Bug in Chrome Hack OFF
            $('body').removeAttr('style');

            if (_.options.useHash && !activatingHash) {
                _.activateHash(_.options.hashTitle+itemSection+'-'+itemId);
            }

            activatingHash = false;
        },

        closeTarget: function (speed) {
            var _ = this;

            //console.log('Close Target');

            // Before Close Target Event Handler
            _.$mgSpace.trigger('beforeCloseTarget', [_]);

            $(_.options.row+'-open').removeClass(_.stripDot(_.options.row)+'-open');
            $(_.options.target+'-open').css('z-index',1);
            $(_.options.close).remove();
            $(_.options.target+'-open').slideUp(speed, function () {
                $(this).removeClass(_.stripDot(_.options.target)+'-open').removeAttr('style');
                _.$mgSpace.on('click.mg.trigger', _.options.trigger, {mgSpace:_}, _.clickHandler);


                // After Close Target Event Handler
                _.$mgSpace.trigger('afterCloseTarget', [_]);                
            });              
        },            

        openSpace: function (element) {           
            var _ = this,
                itemSection = $(element).attr('data-section'),
                itemRow = $(element).attr('data-row'),
                itemId = $(element).attr('data-id'),
                itemPosition = $(element).position(),
                $itemTarget = $(_.options.target+'[data-section="' + itemSection + '"][data-id="' + itemId + '"]', _.$mgSpace),
                targetHeight = 0;

            //console.log('Open Space');

            targetHeight = $itemTarget.css('position','fixed').show().height();

            if (!$('.mg-space[data-section="' + itemSection + '"][data-row="' + itemRow + '"]').length) {
                $('.mg-space').remove();
                $(_.options.rowWrapper).find('[data-section="' + itemSection + '"][data-row="' + itemRow + '"]').last().after('<div class="mg-space" data-section="' + itemSection + '" data-row="' + itemRow + '"><div class="mg-indicator"></div></div>');
            }

            $('.mg-space[data-section="' + itemSection + '"][data-row="' + itemRow + '"]').css({
                height: targetHeight + _.options.targetPadding,
                marginBottom: _.options.rowMargin
            }).slideDown(300, function() {
                $('.mg-space').addClass('mg-space-open');
                if (_.options.useIndicator) {
                    $('.mg-indicator').css({
                        left: itemPosition.left + parseInt($(element).css('padding-left')) + $(element).width()/2 - 10,
                    });
                    $('.mg-indicator').animate({top:-9}, 200);
                }
            });                        
        },

        closeSpace: function (speed) {
            var _ = this;

            //console.log('Close Space');            

            $('.mg-space').slideUp(speed, function () {
                $('.mg-space').removeClass('mg-space-open');
                if (_.options.useIndicator) {
                    $('.mg-indicator').css({top:1});
                }
            });            
        },

        resizeSpace: function (element) {
            var _ = this,
                itemId = $(element).attr('data-id'),
                itemSection = $(element).attr('data-section'),
                itemPosition = $(element).position(),
                $itemTarget = $(_.options.target+'[data-section="' + itemSection + '"][data-id="' + itemId + '"]'),
                itemTargetOpen = $itemTarget.hasClass(_.stripDot(_.options.target+'-open')),
                targetHeight;

            if (!itemTargetOpen) {
                $itemTarget.css('position','fixed').show();
            } else {
                $(_.options.target+'-open').css('top',$(_.options.row+'-open').offset().top+$(_.options.row+'-open').height() + _.options.rowMargin );                
            }

            targetHeight = $itemTarget.height();

            if (_.options.useIndicator && !itemTargetOpen) {
                $('.mg-indicator').css({top:1});
            }

            if (!itemTargetOpen) {
                $('.mg-space').animate({
                    height: targetHeight + _.options.targetPadding
                }, 200, function () {
                    if (_.options.useIndicator) {
                        $('.mg-indicator').css({
                            left: itemPosition.left + parseInt($(element).css('padding-left')) + $(element).width()/2 - 10,
                        });
                        $('.mg-indicator').animate({top:-9}, 200);
                    }
                });
            } else {
                $('.mg-space').css(
                    'height', targetHeight + _.options.targetPadding
                );

                if (_.options.useIndicator) {
                    $('.mg-indicator').css({
                        left: itemPosition.left + parseInt($(element).css('padding-left')) + $(element).width()/2 - 10,
                    });
                    $('.mg-indicator').animate({top:-9}, 200);
                }                                            
            }          
        },

        destroy: function () {
            var _ = this;

            // REMOVE PLUGIN
            _.$mgSpace.removeData("plugin_" + mgSpace);

            // TURN OFF EVENTS FIRST
            _.$mgSpace.off('.mg');
            $(window).off('.mg');

            // REMOVE DATA ATTRS
            $('.mg-rows').removeAttr('data-cols');
            $('.mg-row, .mg-target').removeAttr('data-id');
            $('.mg-row, .mg-target').removeAttr('data-section');
            $('.mg-row, .mg-target').removeAttr('data-row');

            // REMOVE INLINE STYLES
            $('.mg-row, .mg-target').removeAttr('style');

            // REMOVE ADDED CLASSES
            $('.mg-row, .mg-target').removeClass('mg-row mg-target');

            // REMOVE MG SPACE
            $('.mg-space').remove();
        },

        setColumns: function () {
            var _ = this,
                cols = 1;

            $.each(_.options.breakpointColumns, function( idx, val ) {
                if (_.getViewportWidth() > val.breakpoint) {
                    cols = val.column;
                }
            });

            return cols;
        },

        setRows: function (rows) {
            var _ = this,
                row = 0,
                colCount = 1,
                cols = _.setColumns(),
                parent = null,
                newParent = _.$mgSpace.index();                

            rows.each(function(idx) {
                
                if(parent === null) {
                    parent = newParent;
                }

                if(parent != newParent){
                    parent = _.$mgSpace.index();
                }

                $(this).attr('data-id', idx + 1);
                $(this).attr('data-section', parent);

                if (!$(this).parent().hasClass(_.stripDot(_.options.targetWrapper))) {
                    $(this).attr('data-row', row);
                    $(this).addClass(_.stripDot(_.options.row));
                } else {
                    $(this).addClass(_.stripDot(_.options.target));
                }

                if(colCount==cols){
                    row++;
                    colCount=0;
                }
                colCount++;
                
            });            
        },

        getViewportWidth: function () {
           var e = window, a = 'inner';
           if (!('innerWidth' in window )) {
               a = 'client';
               e = document.documentElement || document.body;
           }
           return e[ a+'Width' ];
        },

        activateHash: function (hash) {
            if(history.pushState) {
                history.pushState(null, null, window.location.origin + window.location.pathname + window.location.search + hash);
            } else {
                // Otherwise fallback to the hash update for sites that don't support the history api
                window.location.hash = hash;
            }
        },

        scrollToTop: function (element) {
            $('html, body').animate({
                scrollTop: $(element).offset().top
            }, 400);
        },

        clickHandler: function(event) {
            var _ = event.data.mgSpace;

            if (shouldClick) {
                event.stopImmediatePropagation();
                event.stopPropagation();
                event.preventDefault();

                _.rowController(this, event);
            }

        },        

        stripDot: function (string) {
            return string.replace('.', '');
        }

    }); //END $.extend

    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[ mgSpace ] = function ( options ) {
        return this.each(function() {
            if ( !$.data( this, "plugin_" + mgSpace ) ) {
                $.data( this, "plugin_" + mgSpace, new MGSpace( this, options ) );
            }
        });
    };

})( jQuery, window, document );
(function (window) {
  window.__env = window.__env || {};

  // API url
  window.__env.apiUrl = "http://geekyants-portal.local.geekydev.com/api/v1/";
  
}(this));
var app = angular
  .module(
    "myApp",
    [
      "ngAnimate",
      "ui.bootstrap",
      "restangular",
      "ui.select",
      "ngRoute",
      "datatables",
      "datatables.fixedcolumns",
      "daterangepicker",
      "angularFileUpload",
      'vs-repeat',
      "ngSanitize"
    ]).config(['$interpolateProvider', 'RestangularProvider',
      function ($interpolateProvider, RestangularProvider) {
        $interpolateProvider.startSymbol("%%");
        $interpolateProvider.endSymbol("%%");
        var apiBaseUrl = "http://geekyants-portal.local.geekydev.com/api/v1";
        
        RestangularProvider.setBaseUrl(apiBaseUrl);
        // RestangularProvider.setResponseExtractor(function(
        //   response,
        //   operation,
        //   what,
        //   url
        // ) {
        //   var newResponse;
        //   newResponse = response;
        //   console.log("RES ", response);
        //   if (angular.isArray(response)) {
        //     if (!response) return;
        //     for (var i = 0; i < response.length; i++) {
        //       response[i].originalData = angular.copy(response[i]);
        //     }
        //   } else {
        //     if (!response) return;
        //     response.originalData = angular.copy(response.result);
        //   }
        //   if (typeof response.meta != "undefined")
        //     newResponse.metaData = response.meta.pagination;
        //   else {
        //     newResponse.metaData = response.meta;
        //   }
        //   return newResponse;
        // });

        var env = {};
        if (window) {
          Object.assign(env, window.__env);
        }
        app.constant("__env", env);
        var apiBaseUrl = __env.apiUrl;
        RestangularProvider.setBaseUrl(apiBaseUrl);
        // RestangularProvider.setResponseExtractor(function(
        //   response,
        //   operation,
        //   what,
        //   url
        // ) {
        //   var newResponse;
        //   newResponse = response;
        //   console.log("setResponseExtractor ", response);

        //   if (angular.isArray(response)) {
        //     if (!response) return;
        //     for (var i = 0; i < response.length; i++) {
        //       response[i].originalData = angular.copy(response[i]);
        //     }
        //   } else {
        //     if (!response) return;
        //     response.originalData = angular.copy(response.result);
        //   }
        //   if (typeof response.meta != "undefined")
        //     newResponse.metaData = response.meta.pagination;
        //   else {
        //     newResponse.metaData = response.meta;
        //   }
        //   return newResponse;
        // });

        RestangularProvider.setErrorInterceptor(function (
          response,
          deferred,
          responseHandler
        ) {
          if (response.status === 498) {
            tokenException = true;
            return true;
          }
        });

        RestangularProvider.addResponseInterceptor(function (
          data,
          operation,
          what,
          url,
          response,
          deferred
        ) {
          var extractedData = {};
          extractedData = data.result;
          extractedData.metaData = data.meta ? data.meta : [];
          return extractedData;
        });
      }]
    )
  .constant("CONFIG", {
    BASEURL: "http://geekyants-portal.local.geekydev.com",
    DOWNLOADURL:
      "http://geekyants-portal.local.geekydev.com/api/v1/download?q=",
    DOWNLOADPDFURL:
      "http://geekyants-portal.local.geekydev.com/api/v1/download-pdf?q="
  })
  .filter("sumByRow", function () {
    return function (collection, column) {
      var total = 0;
      var tempTotal = "";
      var tempTotalArr = [];
      collection.forEach(function (item) {
        total += item.hours;
      });
      if (total.toString().includes(".")) {
        tempTotalArr = total.toString().split(".");
        tempTotal = tempTotalArr[0].toString().padStart(3, "0");
        total = tempTotal + "." + tempTotalArr[1].toString();
      } else {
        total = total.toString().padStart(3, "0");
        total = total + "." + "0";
      }
      return total;
    };
  })
  .filter("formatHours", function () {
    return function (collection, column) {
      var total = collection;
      var tempTotal = "";
      var tempTotalArr = [];
      if (total.toString().includes(".")) {
        tempTotalArr = total.toString().split(".");
        tempTotal = tempTotalArr[0].toString().padStart(2, "0");
        total = tempTotal + "." + tempTotalArr[1].toString();
      } else {
        total = total.toString().padStart(2, "0");
        total = total + "." + "0";
      }
      return total;
    };
  })
  .filter("sumByApprovedHours", function () {
    return function (collection, column) {
      var total = 0;
      var tempTotal = "";
      var tempTotalArr = [];
      collection.forEach(function (item) {
        if (item.approver_id != null) total += item.approved_hours;
      });
      if (total.toString().includes(".")) {
        tempTotalArr = total.toString().split(".");
        tempTotal = tempTotalArr[0].toString().padStart(3, "0");
        total = tempTotal + "." + tempTotalArr[1].toString();
      } else {
        total = total.toString().padStart(3, "0");
        total = total + "." + "0";
      }
      return total;
    };
  })
  .config([
    "$routeProvider",
    function ($routeProvider) {
      $routeProvider
        .when("/step1", {
          templateUrl: "/views/payslip-csv/payroll-form.html",
          controller: "payslipStep1Ctrl"
        })
        .when("/step2", {
          templateUrl: "/views/payslip-csv/payroll-form.html",
          controller: "payslipStep2Ctrl"
        })
        .when("/step3", {
          templateUrl: "/views/payslip-csv/payroll-form.html",
          controller: "payslipStep3Ctrl"
        })
        .when("/step4", {
          templateUrl: "/views/payslip-csv/payroll-form.html",
          controller: "payslipStep4Ctrl"
        })
        .when("/step5", {
          templateUrl: "/views/payslip-csv/payroll-form.html",
          controller: "payslipStep5Ctrl"
        })
        .when("/step6", {
          templateUrl: "/views/payslip-csv/payroll-form.html",
          controller: "payslipStep6Ctrl"
        });
    }
  ])
  .config([
    "$locationProvider",
    function ($locationProvider) {
      $locationProvider.hashPrefix("");
    }
  ]);
app.controller("myCtrl", function ($scope, Restangular, $http) {
  $scope.id = document.getElementById("id").value;
  $http({
    method: "GET",
    url:
      "http://geekyants-portal.geekydev.com/admin/Api/get-content/" + $scope.id
  }).then(
    function successCallback(response) {
      $scope.that = $scope;
      $scope.contents = Object.keys(response.data).map(function (key) {
        return response.data[key];
      });
      for (var i = 0; i < $scope.contents.length; i++) {
        $scope[$scope.contents[i]] = $scope.contents[i];
      }
    },
    function errorCallback(response) { }
  );
});
angular.module("myApp").filter("dateToISO", function() {
  return function(input) {
    input = new Date(input).toISOString();
    var dateString = moment(input).format('YYYY-MM-DD');
    var dateStringWithTime = moment(input).format('Do, MMM Y h:mm A');
    return dateStringWithTime;
  };
});

angular.module("myApp").filter('range', function() {
    return function(input, total) {
      total = parseInt(total);
        for (var i=0; i<total; i++) {
        input.push(i);
      }
      return input;
    };
  });
angular.module('myApp')
    .controller('timesheetCtrl',["$scope", "Restangular", "$uibModal",function ($scope, Restangular, $uibModal) {
        $scope.dates = JSON.parse(window.dates);
        $scope.projects = window.projects;
        $scope.verticalTotals = JSON.parse(window.verticalTotals);
        $scope.user = JSON.parse(window.user);
        $scope.currentDate = new Date().toISOString().slice(0, 10);
        $scope.addTime = function (name, result, user) {
            $scope.list = {
                project_name: name,
                data: result,
                user_name: user
            };
            var modal = $uibModal.open({
                controller: 'modalCtrl',
                windowClass: 'bootstrap_wrap',
                templateUrl: '/views/add_time.html',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                    data: function () {
                        return $scope.list;
                    }
                }

            });
            modal.result.then(function (res) { }, function () { });
        };
        var modal = $uibModal.open({
            controller: "modalCtrl",
            windowClass: "bootstrap_wrap",
            templateUrl: "/views/add_time.html",
            backdrop: "static",
            size: "md",
            resolve: {
                data: function () {
                    return $scope.list;
                }
            }
        });
        modal.result.then(function (res) { }, function () { });


    }]);
"use strict";

/*Directives*/
angular
  .module("myApp")
  .directive("compileThis", function($compile) {
    return {
      template: "<span></span>",
      restrict: "E",
      replace: true,
      link: function(scope, iElement, iAttrs) {
        iElement.html(iAttrs.value);
        $compile(iElement)(scope);
      }
    };
  })
  .directive("gridView", [
    "$parse",
    "CONFIG",
    function($parse, CONFIG) {
      return {
        templateUrl:
          "/scripts/directives/grid-view/grid-view.html?v=" +
          window.app_version,
        restrict: "E",
        replace: true,
        scope: {
          model: "=",
          _columns: "@columns",
          _actions: "@actions",
          _showSearch: "@showSearch",
          _showDate: "@showDate",
          _showStartDate: "@showStartDate",
          _showEndDate: "@showEndDate",
          _showDateRange: "@showDateRange",
          _showDownloadCsv: "@showDownloadCsv",
          _filters: "@filters",
          control: "=?",
          extraSearchFieldsData: "=?",
          gridData: "=?"
        },
        controller: function(
          $scope,
          Restangular,
          $location,
          $parse,
          $filter,
          $log
        ) {
          $scope.dateOptions = {
            "year-format": "'yy'",
            "starting-day": 1
          };
          $scope.current_date = new Date();
          $scope.stringToDate = function (dateString) {
            let date = new Date(dateString);
            return date;
          }

          $scope.openStartDateAction = function() {
            setTimeout(function() {
              $scope.$apply(function() {
                $scope.openStartDate = true;
              });
            }, 0);
          };
          $scope.openEndDateAction = function() {
            setTimeout(function() {
              $scope.$apply(function() {
                $scope.openEndDate = true;
              });
            }, 0);
          };

          $scope.openFromDateAction = function() {
            setTimeout(function() {
              $scope.$apply(function() {
                $scope.openFromDate = true;
              });
            }, 0);
          };

          $scope.openToDateAction = function() {
            setTimeout(function() {
              $scope.$apply(function() {
                $scope.openToDate = true;
              });
            }, 0);
          };

          $scope.exportCsvAction = function() {
            makeRequest(true);
          };
          $scope.resetDownloads = function() {
            $scope.pdfLocation = null;
            $scope.csvLocation = null;
            $scope.generatingPdf = false;
            $scope.exportingCsv = false;
            $scope.noRecords = false;
          };
          $scope.filteredDate = function (input_date) {
            var formatted_date = new Date(input_date);
            return formatted_date;
          };
          $scope.columns = $scope.$parent.$eval($scope._columns);
          $scope.actions = $scope.$parent.$eval($scope._actions);
          $scope.filters = $scope.$parent.$eval($scope._filters);

          if (typeof $scope._showSearch == "undefined") {
            $scope.showSearch = true;
          } else {
            $scope.showSearch = $scope.$parent.$eval($scope._showSearch);
          }

          if (typeof $scope._showDate == "undefined") {
            $scope.showDate = false;
          } else {
            $scope.showDate = $scope.$parent.$eval($scope._showDate);
          }
          if (typeof $scope._showStartDate == "undefined") {
            $scope.showStartDate = false;
          } else {
            $scope.showStartDate = $scope.$parent.$eval($scope._showStartDate);
          }
          if (typeof $scope._showEndDate == "undefined") {
            $scope.showEndDate = false;
          } else {
            $scope.showEndDate = $scope.$parent.$eval($scope._showEndDate);
          }

          if (typeof $scope._showDateRange == "undefined") {
            $scope.showDateRange = false;
          } else {
            $scope.showDateRange = $scope.$parent.$eval($scope._showDateRange);
          }

          if (typeof $scope._showDownloadCsv == "undefined") {
            $scope.showDownloadCsv = false;
          } else {
            $scope.showDownloadCsv = $scope.$parent.$eval(
              $scope._showDownloadCsv
            );
          }

          $scope.unexpected = false;
          $scope.fetching = false;

          $scope.pagination = {};
          $scope.requestParams = {
            pageNumber: 1,
            q: "",
            perPage: 100,
            date: "",
            startDate: "",
            endDate: "",
            startTime: new Date(0, 0, 0, 0, 0),
            endTime: new Date(0, 0, 0, 0, 0)
          };
          $scope.pdfLocation = null;
          $scope.generatingPdf = false;
          $scope.noRecords = false;

          var makeRequest = _.debounce(function(exportToCsv) {
            $scope.csvLocation = null;

            var pageNumber = $scope.requestParams.pageNumber;

            var baseUrl = $scope.model;
            var sendParams = {};

            if ($scope.data && $scope.data.metaData)
              sendParams["page"] = $scope.requestParams.pageNumber;

            if ($scope.requestParams.q)
              sendParams["q"] = $scope.requestParams.q;

            if ($scope.requestParams.startDate) {
              var d = $scope.requestParams.startDate;
              sendParams["startDate"] =
                d.getFullYear() +
                "-" +
                ("00" + (d.getMonth() + 1)).slice(-2) +
                "-" +
                ("00" + d.getDate()).slice(-2);
              if ($scope.requestParams.startTime) {
                var t = $scope.requestParams.startTime;
                sendParams["startDate"] =
                  sendParams["startDate"] +
                  " " +
                  ("00" + t.getHours()).slice(-2) +
                  ":" +
                  ("00" + t.getMinutes()).slice(-2) +
                  ":00";
              } else {
                sendParams["startDate"] = sendParams["startDate"] + " 00:00:00";
              }
            }
            if ($scope.requestParams.endDate) {
              var d = $scope.requestParams.endDate;
              sendParams["endDate"] =
                d.getFullYear() +
                "-" +
                ("00" + (d.getMonth() + 1)).slice(-2) +
                "-" +
                ("00" + d.getDate()).slice(-2);
              if ($scope.requestParams.endTime) {
                var t = $scope.requestParams.endTime;
                sendParams["endDate"] =
                  sendParams["endDate"] +
                  " " +
                  ("00" + t.getHours()).slice(-2) +
                  ":" +
                  ("00" + t.getMinutes()).slice(-2) +
                  ":00";
              } else {
                sendParams["endDate"] = sendParams["endDate"] + " 00:00:00";
              }
            }

            if ($scope.showDateRange) {
              if ($scope.requestParams.fromDate) {
                var fd = $scope.requestParams.fromDate;
                sendParams["from_date"] =
                  fd.getFullYear() +
                  "-" +
                  ("00" + (fd.getMonth() + 1)).slice(-2) +
                  "-" +
                  ("00" + fd.getDate()).slice(-2);
              }

              if ($scope.requestParams.toDate) {
                var td = $scope.requestParams.toDate;

                sendParams["to_date"] =
                  td.getFullYear() +
                  "-" +
                  ("00" + (td.getMonth() + 1)).slice(-2) +
                  "-" +
                  ("00" + td.getDate()).slice(-2);
              }
            }

            if ($scope.showDate) {
              if ($scope.requestParams.date) {
                var td = $scope.requestParams.date;

                sendParams["on_date"] =
                  td.getFullYear() +
                  "-" +
                  ("00" + (td.getMonth() + 1)).slice(-2) +
                  "-" +
                  ("00" + td.getDate()).slice(-2);
              }
            }

            if ($scope.requestParams.perPage)
              sendParams["per_page"] = $scope.requestParams.perPage;

            angular.forEach($scope.extraSearchFieldsData, function(val) {
              sendParams[val.paramName] = $scope.requestParams[val.paramName];
            });

            sendParams["pagination"] = 1;

            if (exportToCsv) sendParams["export_to_csv"] = true;

            sendParams = $.extend(true, sendParams, $scope.filters);

            if (exportToCsv) {
              $scope.noRecords = false;
              $scope.exportingCsv = true;
              Restangular.all(baseUrl)
                .customGET("", sendParams)
                .finally(function() {
                  $scope.exportingCsv = false;
                })
                .then(
                  function(success) {
                    //$scope.csvLocation = "http://cosmoparking.local.geekydev.com/download?q="/*;Config.BASEURL*/+success.location;
                    $scope.csvLocation = CONFIG.DOWNLOADURL + success.location;
                  },
                  function(error) {
                    $scope.noRecords = true;
                    // ErrorHandler(error);
                  }
                );
            } else {
              $scope.loading = true;

              Restangular.all(baseUrl)
                .getList(sendParams)
                .finally(function() {
                  $scope.loading = false;
                })
                .then(
                  function(success) {
                    $scope.gridData = success;

                    $scope.qSent = sendParams["filter"];

                    $scope.data = success;
                    $scope.pagination.from =
                      (pageNumber - 1) * $scope.data.metaData.per_page + 1;

                    if (
                      pageNumber * $scope.data.metaData.per_page <
                      $scope.data.metaData.total
                    ) {
                      $scope.pagination.to =
                        pageNumber * $scope.data.metaData.per_page;
                    } else {
                      $scope.pagination.to = $scope.data.metaData.total;
                    }
                    angular.forEach($scope.data, function(o) {
                      // if(o.start_date) {
                      //  o.start_date = new Date(o.start_date * 1000);
                      //  o.start_date = $scope.getFormatedDate(o.start_date);
                      // }
                      // if(o.end_date) {
                      //  o.end_date = new Date(o.end_date * 1000);
                      //  o.end_date = $scope.getFormatedDate(o.end_date);
                      // }
                      if (o.status == 1) o.status = true;
                      if (o.enabled == 1) o.enabled = true;
                      if (o.is_available == 1) o.is_available = true;
                    });
                  },
                  function(error) {
                    $log.error("Oops", "Some error occured, please try again");
                  }
                );
            }
          }, 50);

          $scope.control = {
            remove: function(id) {
              var index = _.findIndex($scope.data, function(row) {
                return row.id == id;
              });

              if (index !== -1) $scope.data.splice(index, 1);
            }
          };

          $scope.changePage = function() {
            $scope.requestParams.pageNumber = $scope.data.metaData.current_page;
            makeRequest();
          };

          $scope.doSearch = function() {
            $scope.requestParams.pageNumber = 1;
            makeRequest();
          };

          $scope.$watch("requestParams.startDate", function(newVal, oldVal) {
            makeRequest();
          });
          $scope.$watch("requestParams.endDate", function(newVal, oldVal) {
            makeRequest();
          });
          $scope.$watch("requestParams.startTime", function(newVal, oldVal) {
            makeRequest();
          });
          $scope.$watch("requestParams.endTime", function(newVal, oldVal) {
            makeRequest();
          });

          $scope.$watch("requestParams.fromDate", function(newVal, oldVal) {
            makeRequest();
          });

          $scope.$watch("requestParams.toDate", function(newVal, oldVal) {
            makeRequest();
          });

          $scope.perPageChange = function() {
            $scope.requestParams.pageNumber = 1;
            makeRequest();
          };

          $scope.hasAction = function(action) {
            var hasIt = false;
            angular.forEach($scope.actions, function(val, key) {
              if (val.type == action) hasIt = true;
            });

            return hasIt;
          };

          function commonAction(action, row, that) {
            var actionObj = _.find($scope.actions, { type: action });

            if (!actionObj || (!actionObj.url && !actionObj.action)) {
              var l = $location.path();

              if (/list$/.test(l)) l = l.substr(0, l.length - 4);

              if (l.substr(-1) != "/") l += "/";

              $location.path(l + action + "/" + row.id);
            } else {
              if (actionObj.url) $location.path(that.$eval(actionObj.url));

              if (actionObj.action) {
                var fn = $parse(actionObj.action);
                fn($scope.$parent, { row: row });
              }
            }
          }

          $scope.addNew = function(row) {
            var actionObj = _.find($scope.actions, { type: "addNew" });
          };

          $scope.viewAction = function(row) {
            commonAction("view", row, this);
          };

          $scope.refreshAction = function(row) {
            commonAction("refresh", row, this);
          };

          $scope.downAction = function(row) {
            commonAction("down", row, this);
          };

          $scope.editAction = function(row) {
            commonAction("edit", row, this);
          };
          $scope.getFormatedDate = function(date) {
            var monthNames = [
              "January",
              "February",
              "March",
              "April",
              "May",
              "June",
              "July",
              "August",
              "September",
              "October",
              "November",
              "December"
            ];

            var d = new Date();

            var newDate = new Date(date);
            newDate.setHours(newDate.getHours());
            newDate.setMinutes(newDate.getMinutes());
            var day = ("0" + newDate.getDate()).slice(-2);
            var month = newDate.getMonth();
            var hours = newDate.getHours();
            var minutes = newDate.getMinutes();
            var meridiem = hours >= 12 ? "PM" : "AM";
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? "0" + minutes : minutes;
            newDate =
              monthNames[month] +
              " " +
              day +
              ", " +
              newDate.getFullYear() +
              " - " +
              hours +
              ":" +
              minutes +
              " " +
              meridiem;
            return newDate;
          };
          $scope.deleteAction = function(row) {
            var actionObj = _.find($scope.actions, { type: "delete" });

            if (!actionObj || (!actionObj.url && !actionObj.action)) {
              var sure = confirm("Do you really want to delete this" + "?");
              if (sure) {
                row
                  .remove()
                  .finally(function() {})
                  .then(
                    function() {
                      var index = $scope.data.indexOf(row);
                      $scope.data.splice(index, 1);

                      var msg = "Item with ID" + " " + row.id + " " + "deleted";
                      $log.info(msg);
                    },
                    function() {
                      var msg =
                        "Item with ID" +
                        " " +
                        row.id +
                        " " +
                        "could not be deleted";
                      $log.error(msg);
                    }
                  );
              }
            } else {
              commonAction("delete", row, this);
            }
          };

          $scope.starAction = function(row) {
            commonAction("star", row, this);
          };

          makeRequest();
          $scope.downloadPdf = function() {
            $scope.noRecords = false;
            $scope.generatingPdf = true;
            var startTime = null;
            var endTime = null;
            var href = window.location.href.split("?")[0];
            var day = href.substring(href.lastIndexOf("/") + 1);
            if (day == "today") {
              var d = new Date();
              startTime =
                d.getFullYear() +
                "-" +
                ("00" + (d.getMonth() + 1)).slice(-2) +
                "-" +
                ("00" + d.getDate()).slice(-2) +
                " 00:00:00";
              endTime =
                d.getFullYear() +
                "-" +
                ("00" + (d.getMonth() + 1)).slice(-2) +
                "-" +
                ("00" + d.getDate()).slice(-2) +
                " 23:59:59";
            } else {
              if (day == "tomorrow") {
                var d = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
                startTime =
                  d.getFullYear() +
                  "-" +
                  ("00" + (d.getMonth() + 1)).slice(-2) +
                  "-" +
                  ("00" + d.getDate()).slice(-2) +
                  " 00:00:00";
                endTime =
                  d.getFullYear() +
                  "-" +
                  ("00" + (d.getMonth() + 1)).slice(-2) +
                  "-" +
                  ("00" + d.getDate()).slice(-2) +
                  " 23:59:59";
              } else {
                if ($scope.requestParams.startDate) {
                  var d = $scope.requestParams.startDate;
                  startTime =
                    d.getFullYear() +
                    "-" +
                    ("00" + (d.getMonth() + 1)).slice(-2) +
                    "-" +
                    ("00" + d.getDate()).slice(-2);
                  if ($scope.requestParams.startTime) {
                    var t = $scope.requestParams.startTime;
                    startTime =
                      startTime +
                      " " +
                      ("00" + t.getHours()).slice(-2) +
                      ":" +
                      ("00" + t.getMinutes()).slice(-2) +
                      ":00";
                  } else {
                    startTime = startTime + " 00:00:00";
                  }
                }
                if ($scope.requestParams.endDate) {
                  var d = $scope.requestParams.endDate;
                  endTime =
                    d.getFullYear() +
                    "-" +
                    ("00" + (d.getMonth() + 1)).slice(-2) +
                    "-" +
                    ("00" + d.getDate()).slice(-2);
                  if ($scope.requestParams.endTime) {
                    var t = $scope.requestParams.endTime;
                    endTime =
                      endTime +
                      " " +
                      ("00" + t.getHours()).slice(-2) +
                      ":" +
                      ("00" + t.getMinutes()).slice(-2) +
                      ":00";
                  } else {
                    endTime = endTime + " 00:00:00";
                  }
                }
              }
            }
            var params = { startTime: startTime, endTime: endTime };
            Restangular.all("")
              .customGET("download-records", params)
              .finally(function() {
                $scope.loading = false;
                $scope.generatingPdf = false;
              })
              .then(
                function(success) {
                  $scope.pdfLocation = CONFIG.DOWNLOADPDFURL + success;
                },
                function(error) {
                  $scope.noRecords = true;
                  // $log.error("Oops", "Some error occured, please try again");
                }
              );
          };
        },
        link: function(scope, iElement, iAttrs) {
          scope.openModal = function(row) {
            var fn = $parse(iAttrs.openModal);
            fn(scope.$parent, { row: row });
          };

          scope.switchOnoff = function(row) {
            var fn = $parse(iAttrs.switchOnoff);
            fn(scope.$parent, { row: row });
          };
        }
      };
    }
  ]);

"use strict";

/*Directives*/
angular
    .module("myApp")
    .directive('starRating', function () {
         return {
        scope: {
            rating: '=',
            maxRating: '@',
            readOnly: '@',
            click: "&",
            mouseHover: "&",
            mouseLeave: "&"
        },
        restrict: 'EA',
        template:
            "<div style='display: inline-block; margin: 0px; padding: 0px; cursor:pointer;' ng-repeat='idx in maxRatings track by $index'> \
                    <img ng-src='{{((hoverValue + _rating) <= $index) && \"http://www.codeproject.com/script/ratings/images/star-empty-lg.png\" || \"http://www.codeproject.com/script/ratings/images/star-fill-lg.png\"}}' \
                    ng-Click='isolatedClick($index + 1)' \
                    ng-mouseenter='isolatedMouseHover($index + 1)' \
                    ng-mouseleave='isolatedMouseLeave($index + 1)'></img> \
            </div>",
        compile:["element", "attrs", function (element, attrs) {
            if (!attrs.maxRating || (Number(attrs.maxRating) <= 0)) {
                attrs.maxRating = '5';
            };
        }],
        controller:["$scope", "$element", "$attrs", function ($scope, $element, $attrs) {
            $scope.maxRatings = [];

            for (var i = 1; i <= $scope.maxRating; i++) {
                $scope.maxRatings.push({});
            };

            $scope._rating = $scope.rating;
            
            $scope.isolatedClick = function (param) {
                if ($scope.readOnly == 'true') return;

                $scope.rating = $scope._rating = param;
                $scope.hoverValue = 0;
                $scope.click({
                    param: param
                });
            };

            $scope.isolatedMouseHover = function (param) {
                if ($scope.readOnly == 'true') return;

                $scope._rating = 0;
                $scope.hoverValue = param;
                $scope.mouseHover({
                    param: param
                });
            };

            $scope.isolatedMouseLeave = function (param) {
                if ($scope.readOnly == 'true') return;

                $scope._rating = $scope.rating;
                $scope.hoverValue = 0;
                $scope.mouseLeave({
                    param: param
                });
            };
        }]
    };
    });
"use strict()";

/*Directives*/
angular.module("myApp").directive("commentData", [
  function() {
    return {
      templateUrl: "/scripts/directives/comment/comment.html?v=2",
      restrict: "E",
      replace: true,
      scope: {
        commentableId: "=",
        commentableType: "@commentableType",
        isDisabled: "@isDisabled",
        isPrivatable: "@isPrivatable"
      },
      controller: [
        "$scope",
        "Restangular",
        "$rootScope",
        function($scope, Restangular, $rootScope) {
          $scope.loadMoreFlag = false;
          $scope.commentCount = 0;
          $scope.noCommentsToLoad = false;
          $scope.showList = true;
          $scope.addLoading = false;
          $scope.listLoading = false;
          $scope.commentObj = {
            comments: [],
            page: 1,
            per_page: 5,
            loaded: false,
            newComment: "",
            newCommentAdded: false,
            isPrivate: false
          };

          $scope.loadComments = function() {
            $scope.listLoading = true;
            var params = {
              commentable_id: $scope.commentableId,
              commentable_type: $scope.commentableType,
              page: $scope.commentObj.page,
              per_page: $scope.commentObj.per_page
            };
            Restangular.all("comment")
              .getList(params)
              .then(function(response) {
                $scope.listLoading = false;
                if (response.length > 0) {
                  $scope.commentCount = response.length;
                  for (i = 0; i < response.length; i++) {
                    $scope.commentObj.comments.push(response[i]);
                  }
                } else {
                  $scope.noCommentsToLoad = true;
                }
              });
          };

          $scope.loadComments();
          $scope.loadMoreComments = function() {
            $scope.loadMoreFlag = true;
            $scope.commentObj.page = $scope.commentObj.page + 1;
            $scope.loadComments();
          };
          $scope.checkboxClicked = function(data) {
            // console.log('data', data);
          };
          $scope.postComment = function() {
            if (
              $scope.commentObj.newComment == null ||
              $scope.commentObj.newComment == ""
            ) {
              return;
            }
            $scope.addLoading = true;
            var params = {
              commentable_id: $scope.commentableId,
              commentable_type: $scope.commentableType,
              is_private: $scope.commentObj.isPrivate,
              message: $scope.commentObj.newComment
            };

            Restangular.all("comments")
              .post(params)
              .then(
                function(response) {
                  $scope.commentCount++;
                  $scope.addLoading = false;
                  $scope.commentObj.newComment = "";
                  $scope.commentObj.isPrivate = !$scope.commentObj.isPrivate;
                  $scope.showList = true;
                  $scope.commentObj.comments.unshift(response.comment);
                  $rootScope.$broadcast('activity-log');
                },
                function(error) {
                  $scope.addLoading = false;
                }
              );
          };

          $scope.toggleCommentList = function() {
            $scope.showList = !$scope.showList;
            $scope.noCommentsToLoad = false;
          };

          $scope.filteredDate = function(input_date) {
            var formatted_date = new Date(input_date);
            return formatted_date;
          };
        }
      ]
    };
  }
]);

"use strict()";

/*Directives*/
angular
    .module("myApp")
    .directive("requiredResource", [
        function () {
            return {
                templateUrl: "/scripts/directives/required-resource/index.html?v=1",
                restrict: "E",
                replace: true,
                scope: {
                    projectId: "=",
                    isDisabled: "@isDisabled",
                },
                controller: ["$scope", "Restangular", function (
                    $scope,
                    Restangular
                ) {
                    $scope.newRequiredResources = [];
                    $scope.oldResources = [];
                    $scope.editable = [];
                    $scope.error = false;
                    $scope.resourceError = false;
                    $scope.priceError = false;
                    $scope.newResouceObj = {
                        'type' : '',
                        'price' : '',
                    };
                    
                    $scope.loadRequiredResources = function () {
                        $scope.oldResources = [];
                        $scope.listLoading = true;
                        Restangular.all("resource-price")
                            .customGET("", {
                                "project_id": $scope.projectId,
                            })
                            .then(function (response) {
                                $scope.listLoading = false;
                                for (i = 0; i < response.length; i++) {
                                    $scope.oldResources.push(response[i]);
                                }
                            },function (error) {
                                console.log(error);
                            }
                        );
                    };
                    $scope.loadRequiredResources();
                    
                    $scope.addResourceRow = function() {
                        $scope.error = false;
                        if ( !$scope.newResouceObj.type )
                        {
                            $scope.resourceError = true;
                            $scope.error = true;
                        }   else {
                            $scope.resourceError = false;
                        }
                        
                        if ( !$scope.newResouceObj.price )
                        {
                            $scope.priceError = true;
                            $scope.error = true;
                        }   else{
                            $scope.priceError = false;
                        }
                        
                        if ( $scope.error )
                        {
                            return;
                        }

                        var params = {
                            'project_id' : $scope.projectId,
                            'type' : $scope.newResouceObj.type,
                            'price' : $scope.newResouceObj.price,
                        };
                        Restangular.all("resource-price")
                            .post(params)
                            .then(function (response) {
                                $scope.newResouceObj = {
                                    'type' : '',
                                    'price' : '',
                                };
                                $scope.loadRequiredResources();
                            }, function (error) {
                                console.log(error);

                            });
                    };
                    $scope.editResourceRow = function(index) {
                        $scope.editable[index] = true;
                    }
                    $scope.saveResourceRow = function(index, resourceRow){
                        $scope.editable[index] = true;
                        Restangular.one("resource-price", resourceRow.id)
                                .customPUT({
                                    resourceRow: resourceRow
                                })
                                .then(
                                    function (response) {
                                        $scope.editable[index] = false;
                                    },
                                    function (errors) {
                                        console.log(errors);
                                    }
                                );
                    };

                    $scope.removeResourceRow = function(index, resourceRow) {
                        var r = confirm("Are you sure you want to delete this entry?");
                        if (r != true) {
                            return;
                        }
                        Restangular.one("resource-price", resourceRow.id)
                            .remove()
                            .then(
                                function(response) {
                                    $scope.oldResources.splice(index, 1);
                                },
                                function(error) {
                                    console.log(error);
                                }
                            );
                    };
                }]
            };
        }
    ]);
"use strict()";

/*Directives*/
angular
    .module("myApp")
    .directive("addProjectUser", [
        function () {
            return {
                
                templateUrl: "/scripts/directives/add-project-user/add-project-user.html?v=1",
                restrict: "E",
                replace: true,
                scope: {
                    projectId: "=",
                    isDisabled: "@isDisabled",
                },
                
                controller: ["$scope", "Restangular", function (
                    $scope,
                    Restangular
                ) {
                    $scope.existingResources = [];
                    $scope.allTypes = [];
                    $scope.allUsers = [];
                    $scope.editable = [];
                    $scope.selected_user = '';
                    $scope.selected_type = '';
                    $scope.error = false;
                    $scope.userError = false;
                    $scope.startDateError = false;
                    $scope.edit_start_date = [];
                    $scope.edit_end_date = [];
                    $scope.today = new Date();
                    $scope.start_date = null;
                    $scope.end_date = null;
                    
                    $scope.getAllUsers = function () {
                        $scope.allUsers = [];
                        $scope.listLoading = true;
                        Restangular.all("user-all-active")
                            .customGET("", {})
                            .then(function (response) {
                                $scope.allUsers = response;
                            },function (error) {
                                console.log(error);
                            }
                        );
                    };
                    $scope.getAllTypes = function () {
                        $scope.allTypes = [];
                        $scope.listLoading = true;
                        Restangular.all("resource-price")
                            .customGET("", {
                                "project_id": $scope.projectId,
                            })
                            .then(function (response) {
                                $scope.listLoading = false;
                                for (i = 0; i < response.length; i++) {
                                    $scope.allTypes.push(response[i]);
                                }
                            },function (error) {
                                console.log(error);
                            }
                        );
                    };
                    $scope.getExistingResources = function () {
                        $scope.existingResources = [];
                        $scope.listLoading = true;
                        Restangular.all("project-resource")
                            .customGET("", {
                                "project_id": $scope.projectId,
                            })
                            .then(function (response) {
                                $scope.listLoading = false;
                                for (i = 0; i < response.length; i++) {
                                    $scope.edit_start_date[i] = new Date(response[i].start_date);
                                    $scope.edit_end_date[i] = (response[i].end_date) ? new Date(response[i].end_date) : null;
                                    $scope.existingResources.push(response[i]);
                                    
                                }
                            },function (error) {
                                console.log(error);
                            }
                        );
                    };
                    $scope.getAllTypes();
                    $scope.getAllUsers();
                    $scope.getExistingResources();

                    $scope.select_user = function (data) {
                        $scope.selected_user = data;
                        $scope.selected_user_error = false;
                    };

                    $scope.select_type = function (data) {
                        $scope.selected_type = data;
                        $scope.selected_type_error = false;
                    };

                    $scope.checkEndDateCondition = function (input_date) {
                        if ( !input_date ) {
                            return true;
                        }
                        else{
                            if ( $scope.today >= input_date )
                            {
                                return false;
                            } else {
                                return true;
                            }
                        }

                    };

                    $scope.addUser = function() {
                        $scope.error = false;
                        if ( $scope.selected_user == '' )
                        {
                            $scope.selected_user_error = true;
                            $scope.error = true;
                        }   else {
                            $scope.selected_user_error = false;
                        }
                        
                        if ( $scope.selected_type == '' )
                        {
                            $scope.selected_type_error = true;
                            $scope.error = true;
                        }   else{
                            $scope.selected_type_error = false;
                        }
                        if ( $scope.start_date == null )
                        {
                            $scope.selected_start_date_error = true;
                            $scope.error = true;
                        }   else{
                            $scope.selected_start_date_error = false;
                        }

                        if ( $scope.error )
                        { 
                            return;
                        }
                        var endDate = '';
                        if ( $scope.end_date )
                        {
                            endDate = moment($scope.end_date).format("YYYY-MM-DD");
                        }
                        var startDate = moment($scope.start_date).format("YYYY-MM-DD");
                        var params = {
                            'project_id' : $scope.projectId,
                            'user_id' : $scope.selected_user.id,
                            'resource_price_id' : $scope.selected_type.id,
                            'start_date' : startDate,
                            'end_date' : endDate
                        };
                        
                        Restangular.all("project-resource")
                            .post(params)
                            .then(function (response) {
                                $scope.selected_user = '';
                                $scope.selected_type = '';
                                $scope.start_date = null;
                                $scope.end_date = null;
                                $scope.getExistingResources();
                            }, function (error) {
                                console.log(error);

                            });
                    };
                    $scope.editResourceRow = function(index) {
                        $scope.editable[index] = true;
                    }
                    $scope.saveResourceRow = function(index, resourceRow){
                        var params = {
                            'user_id' : resourceRow.user_id,
                            'resource_price_id' : resourceRow.resource_price_id,
                            'start_date' : moment($scope.edit_start_date[index]).format("YYYY-MM-DD"),
                            'end_date' : $scope.edit_end_date[index] ? moment($scope.edit_end_date[index]).format("YYYY-MM-DD") : null ,
                        };
                        Restangular.one("project-resource", resourceRow.id)
                                .customPUT({
                                    params: params
                                })
                                .then(
                                    function (response) {
                                        $scope.editable[index] = false;
                                        $scope.getExistingResources();
                                    },
                                    function (errors) {
                                        console.log(errors);
                                    }
                                );
                    };

                    $scope.removeResourceRow = function(index, resourceRow) {
                        var r = confirm("Are you sure you want to delete this entry?");
                        if (r != true) {
                            return;
                        }
                        Restangular.one("project-resource", resourceRow.id)
                            .remove()
                            .then(
                                function(response) {
                                    $scope.existingResources.splice(index, 1);
                                },
                                function(error) {
                                    console.log(error);
                                }
                            );
                    };

                    $scope.releaseResourceRow = function(index, resourceRow) {
                        var r = confirm("Are you sure you want to release this user from the project?");
                        if (r != true) {
                            return;
                        }
                        Restangular.one("project-resource/release", resourceRow.id)
                            .customPUT()
                            .then(
                                function(response) {
                                    $scope.existingResources = [];
          