(function ($) {

    $.fn.tableHeadFixer = function (param) {

        return this.each(function () {
            table.call(this);
        });

        function table() {
            /*
             This function solver z-index problem in corner cell where fix row and column at the same time,
             set corner cells z-index 1 more then other fixed cells
             */
            function setCorner() {
                var table = $(settings.table);

                if (settings.head) {
                    if (settings.left > 0) {
                        var tr = table.find("thead tr");

                        tr.each(function (k, row) {
                            solverLeftColspan(row, function (cell) {
                                $(cell).css("z-index", settings['z-index'] + 1);
                            });
                        });
                    }

                    if (settings.right > 0) {
                        var tr = table.find("thead tr");

                        tr.each(function (k, row) {
                            solveRightColspan(row, function (cell) {
                                $(cell).css("z-index", settings['z-index'] + 1);
                            });
                        });
                    }
                }

                if (settings.foot) {
                    if (settings.left > 0) {
                        var tr = table.find("tfoot tr");

                        tr.each(function (k, row) {
                            solverLeftColspan(row, function (cell) {
                                $(cell).css("z-index", settings['z-index']);
                            });
                        });
                    }

                    if (settings.right > 0) {
                        var tr = table.find("tfoot tr");

                        tr.each(function (k, row) {
                            solveRightColspan(row, function (cell) {
                                $(cell).css("z-index", settings['z-index']);
                            });
                        });
                    }
                }
            }

            // Set style of table parent
            function setParent() {
                var parent = $(settings.parent);
                var table = $(settings.table);

                parent.append(table);
                parent
                    .css({
                        'overflow-x': 'auto',
                        'overflow-y': 'auto'
                    });

                parent.scroll(function () {
                    var scrollWidth = parent[0].scrollWidth;
                    var clientWidth = parent[0].clientWidth;
                    var scrollHeight = parent[0].scrollHeight;
                    var clientHeight = parent[0].clientHeight;
                    var top = parent.scrollTop();
                    var left = parent.scrollLeft();

                    if (settings.head)
                        this.find("thead tr > *").css("top", top);

                    if (settings.foot)
                        this.find("tfoot tr > *").css("bottom", scrollHeight - clientHeight - top);

                    if (settings.left > 0)
                        settings.leftColumns.css("left", left);

                    if (settings.right > 0)
                        settings.rightColumns.css("right", scrollWidth - clientWidth - left);
                }.bind(table));
            }

            // Set table head fixed
            function fixHead() {
                var thead = $(settings.table).find("thead");
                var tr = thead.find("tr");
                var cells = thead.find("tr > *");

                setBackground(cells);
                cells.css({
                    'position': 'relative'
                });
            }

            // Set table foot fixed
            function fixFoot() {
                var tfoot = $(settings.table).find("tfoot");
                var tr = tfoot.find("tr");
                var cells = tfoot.find("tr > *");

                setBackground(cells);
                cells.css({
                    'position': 'relative'
                });
            }

            // Set table left column fixed
            function fixLeft() {
                var table = $(settings.table);

                // var fixColumn = settings.left;

                settings.leftColumns = $();

                var tr = table.find("tr");
                tr.each(function (k, row) {

                    solverLeftColspan(row, function (cell) {
                        settings.leftColumns = settings.leftColumns.add(cell);
                    });
                    // var inc = 1;

                    // for(var i = 1; i <= fixColumn; i = i + inc) {
                    // 	var nth = inc > 1 ? i - 1 : i;

                    // 	var cell = $(row).find("*:nth-child(" + nth + ")");
                    // 	var colspan = cell.prop("colspan");

                    // 	settings.leftColumns = settings.leftColumns.add(cell);

                    // 	inc = colspan;
                    // }
                });

                var column = settings.leftColumns;

                column.each(function (k, cell) {
                    var cell = $(cell);

                    setBackground(cell);
                    cell.css({
                        'position': 'relative'
                    });
                });
            }

            // Set table right column fixed
            function fixRight() {
                var table = $(settings.table);

                var fixColumn = settings.right;

                settings.rightColumns = $();

                var tr_head = table.find('thead').find("tr");
                var tr_body = table.find('tbody').find("tr");
                var fcell = null;
                tr_head.each(function (k, row) {
                    solveRightColspanHead(row, function (cell) {
                        if (k === 0) {
                            fcell = cell;
                        }
                        settings.rightColumns = settings.rightColumns.add(fcell);
                    });
                });

                tr_body.each(function (k, row) {
                    solveRightColspanBody(row, function (cell) {
                        settings.rightColumns = settings.rightColumns.add(cell);
                    });
                });

                var column = settings.rightColumns;

                column.each(function (k, cell) {
                    var cell = $(cell);

                    setBackground(cell);
                    cell.css({
                        'position': 'relative',
                        'z-index': '9999'
                    });
                });

            }

            // Set fixed cells backgrounds
            function setBackground(elements) {
                elements.each(function (k, element) {
                    var element = $(element);
                    var parent = $(element).parent();

                    var elementBackground = element.css("background-color");
                    elementBackground = (elementBackground == "transparent" || elementBackground == "rgba(0, 0, 0, 0)") ? null : elementBackground;

                    var parentBackground = parent.css("background-color");
                    parentBackground = (parentBackground == "transparent" || parentBackground == "rgba(0, 0, 0, 0)") ? null : parentBackground;

                    var background = parentBackground ? parentBackground : "white";
                    background = elementBackground ? elementBackground : background;

                    element.css("background-color", background);
                });
            }

            function solverLeftColspan(row, action) {
                var fixColumn = settings.left;
                var inc = 1;

                for (var i = 1; i <= fixColumn; i = i + inc) {
                    var nth = inc > 1 ? i - 1 : i;

                    var cell = $(row).find("> *:nth-child(" + nth + ")");
                    var colspan = cell.prop("colspan");

                    if (cell.cellPos().left < fixColumn) {
                        action(cell);
                    }

                    inc = colspan;
                }
            }

            function solveRightColspanHead(row, action) {
                var fixColumn = settings.right;
                var inc = 1;
                for (var i = 1; i <= fixColumn; i = i + inc) {
                    var nth = inc > 1 ? i - 1 : i;

                    var cell = $(row).find("> *:nth-last-child(" + nth + ")");
                    var colspan = cell.prop("colspan");

                    action(cell);

                    inc = colspan;
                }
            }

            function solveRightColspanBody(row, action) {
                var fixColumn = settings.right;
                var inc = 1;
                for (var i = 1; i <= fixColumn; i = i + inc) {
                    var nth = inc > 1 ? i - 1 : i;

                    var cell = $(row).find("> *:nth-last-child(" + nth + ")");
                    var colspan = cell.prop("colspan");
                    action(cell);
                    inc = colspan;
                }
            }

            var defaults = {
                head: true,
                foot: false,
                left: 0,
                right: 0,
                'z-index': 0
            };

            var settings = $.extend({}, defaults, param);

            settings.table = this;
            settings.parent = $(settings.table).parent();
            setParent();

            if (settings.head == true) {
                fixHead();
            }

            if (settings.foot == true) {
                fixFoot();
            }

            if (settings.left > 0) {
                fixLeft();
            }

            if (settings.right > 0) {
                fixRight();
            }

            setCorner();

            $(settings.parent).trigger("scroll");

            $(window).resize(function () {
                $(settings.parent).trigger("scroll");
            });
        }
    };

})(jQuery);

/*  cellPos jQuery plugin
 ---------------------
 Get visual position of cell in HTML table (or its block like thead).
 Return value is object with "top" and "left" properties set to row and column index of top-left cell corner.
 Example of use:
 $("#myTable tbody td").each(function(){
 $(this).text( $(this).cellPos().top +", "+ $(this).cellPos().left );
 });
 */
(function ($) {
    /* scan individual table and set "cellPos" data in the form { left: x-coord, top: y-coord } */
    function scanTable($table) {
        var m = [];
        $table.children("tr").each(function (y, row) {
            $(row).children("td, th").each(function (x, cell) {
                var $cell = $(cell),
                    cspan = $cell.attr("colspan") | 0,
                    rspan = $cell.attr("rowspan") | 0,
                    tx, ty;
                cspan = cspan ? cspan : 1;
                rspan = rspan ? rspan : 1;
                for (; m[y] && m[y][x]; ++x);  //skip already occupied cells in current row
                for (tx = x; tx < x + cspan; ++tx) {  //mark matrix elements occupied by current cell with true
                    for (ty = y; ty < y + rspan; ++ty) {
                        if (!m[ty]) {  //fill missing rows
                            m[ty] = [];
                        }
                        m[ty][tx] = true;
                    }
                }
                var pos = {top: y, left: x};
                $cell.data("cellPos", pos);
            });
        });
    };

    /* plugin */
    $.fn.cellPos = function (rescan) {
        var $cell = this.first(),
            pos = $cell.data("cellPos");
        if (!pos || rescan) {
            var $table = $cell.closest("table, thead, tbody, tfoot");
            scanTable($table);
        }
        pos = $cell.data("cellPos");
        return pos;
    }
})(jQuery);
// jQuery to collapse the navbar on scroll
function collapseNavbar() {
    if ($(".navbar").offset() && $(".navbar").offset().top > 50) {
        $(".navbar-main").addClass("top-nav-collapse");

    } else {
        $(".navbar-main").removeClass("top-nav-collapse");
    }
}

$(window).scroll(collapseNavbar);
$(document).ready(collapseNavbar);

// jQuery for page scrolling feature 
$(function () {
    $('a.page-scroll').bind('click', function (event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });
});

// Closes the Responsive Menu on Menu Item Click
$(function () {
    $('.navbar-collapse ul li a').click(function () {
        if ($(this).attr('class') != 'dropdown-toggle active' && $(this).attr('class') != 'dropdown-toggle') {
            $('.navbar-toggle:visible').click();
        }
    });
});

$(function () {
    $("[data-rel=tooltip]").tooltip({ html: true });
});


$(function () {
    // the dropdown 
    // $('.chosen').chosen({
    //     no_results_text: "Not found",
    //     allow_single_deselect: true
    // });

    // The date picker
    // $( ".datepicker-13" ).datepicker();
    $('#txtStartDate, #txtEndDate').datepicker({
        // showOn: "both",
        beforeShow: customRange,
        dateFormat: "M dd, yy",
    });

    function customRange(input) {
        if (input.id == 'txtEndDate') {
            var minDate = new Date($('#txtStartDate').val());
            minDate.setDate(minDate.getDate())
            return {
                minDate: minDate
            };
        }
        return {}
    }

});
$("#selectid2").select2();
$("#selectid22").select2();
$("#selectid3").select2();
$("#selectid4").select2();
$(".js-example-basic-multiple2").select2();
$(".js-example-basic-multiple22").select2();

$(".js-example-basic-multiple").select2();
$('.select2-container--default').css('width', '100%');
$('.js-example-basic-multiple22').css('width', '100%');

// ============= show password toggle ============================
// 
(function ($) {
    $('#showpwdcheckbox').click(function () {
        if ($("#showpwd").attr('type') == 'password')
            $("#showpwd").attr('type', 'text');
        else
            $("#showpwd").attr('type', 'password');
    });
    // ================ calender =============

}(jQuery));
// ================== multiple select ===========

$(".js-example-basic-multiple").select2();
$(".js-example-tags").select2({
    tags: true
})

// ===================== check all ===================
$('#chk_all').change(function () {
    if (this.checked)
        $('.chkbx').attr('checked', true);
    else
        $('.chkbx').attr('checked', false);

})
// ====================== calender - view ====================
$(function () {
    $('#datetimepicker12').datetimepicker({
        inline: true,
        sideBySide: true
    });
    $('.timepicker').hide();
    $('.datepicker').addClass('dateview-calender');

});
$(window).load(function () {

var size = 1;
var button = 1;
var button_class = "gallery-header-center-right-links-current";
var normal_size_class = "gallery-content-center-normal";
var full_size_class = "gallery-content-center-full";
var $container = $('#gallery-content-center');
    
$container.isotope({itemSelector : 'img'});

function check_button(){
	$('.gallery-header-center-right-links').removeClass(button_class);
	if(button==1){
		$("#filter-all").addClass(button_class);
	}
	if(button==2){
		$("#filter-culture").addClass(button_class);
	}
	if(button==3){
		$("#filter-office").addClass(button_class);
	}	
	if(button==4){
		$("#filter-events").addClass(button_class);
	}	
	if(button==5){
		$("#filter-outing").addClass(button_class);
	}	
}
	
$("#filter-all").click(function() { $container.isotope({ filter: '.all' }); button = 1; check_button(); });
$("#filter-culture").click(function() {  $container.isotope({ filter: '.culture' }); button = 2; check_button();  });
$("#filter-office").click(function() {  $container.isotope({ filter: '.office' }); button = 3; check_button();  });
$("#filter-events").click(function() {  $container.isotope({ filter: '.events' }); button = 4; check_button();  });
$("#filter-outing").click(function() {  $container.isotope({ filter: '.outing' }); button = 5; check_button();  });
check_button();
check_size();

});
/*!
 * Isotope PACKAGED v3.0.4
 *
 * Licensed GPLv3 for open source use
 * or Isotope Commercial License for commercial use
 *
 * http://isotope.metafizzy.co
 * Copyright 2017 Metafizzy
 */

!function(t,e){"function"==typeof define&&define.amd?define("jquery-bridget/jquery-bridget",["jquery"],function(i){return e(t,i)}):"object"==typeof module&&module.exports?module.exports=e(t,require("jquery")):t.jQueryBridget=e(t,t.jQuery)}(window,function(t,e){"use strict";function i(i,s,a){function u(t,e,o){var n,s="$()."+i+'("'+e+'")';return t.each(function(t,u){var h=a.data(u,i);if(!h)return void r(i+" not initialized. Cannot call methods, i.e. "+s);var d=h[e];if(!d||"_"==e.charAt(0))return void r(s+" is not a valid method");var l=d.apply(h,o);n=void 0===n?l:n}),void 0!==n?n:t}function h(t,e){t.each(function(t,o){var n=a.data(o,i);n?(n.option(e),n._init()):(n=new s(o,e),a.data(o,i,n))})}a=a||e||t.jQuery,a&&(s.prototype.option||(s.prototype.option=function(t){a.isPlainObject(t)&&(this.options=a.extend(!0,this.options,t))}),a.fn[i]=function(t){if("string"==typeof t){var e=n.call(arguments,1);return u(this,t,e)}return h(this,t),this},o(a))}function o(t){!t||t&&t.bridget||(t.bridget=i)}var n=Array.prototype.slice,s=t.console,r="undefined"==typeof s?function(){}:function(t){s.error(t)};return o(e||t.jQuery),i}),function(t,e){"function"==typeof define&&define.amd?define("ev-emitter/ev-emitter",e):"object"==typeof module&&module.exports?module.exports=e():t.EvEmitter=e()}("undefined"!=typeof window?window:this,function(){function t(){}var e=t.prototype;return e.on=function(t,e){if(t&&e){var i=this._events=this._events||{},o=i[t]=i[t]||[];return o.indexOf(e)==-1&&o.push(e),this}},e.once=function(t,e){if(t&&e){this.on(t,e);var i=this._onceEvents=this._onceEvents||{},o=i[t]=i[t]||{};return o[e]=!0,this}},e.off=function(t,e){var i=this._events&&this._events[t];if(i&&i.length){var o=i.indexOf(e);return o!=-1&&i.splice(o,1),this}},e.emitEvent=function(t,e){var i=this._events&&this._events[t];if(i&&i.length){var o=0,n=i[o];e=e||[];for(var s=this._onceEvents&&this._onceEvents[t];n;){var r=s&&s[n];r&&(this.off(t,n),delete s[n]),n.apply(this,e),o+=r?0:1,n=i[o]}return this}},t}),function(t,e){"use strict";"function"==typeof define&&define.amd?define("get-size/get-size",[],function(){return e()}):"object"==typeof module&&module.exports?module.exports=e():t.getSize=e()}(window,function(){"use strict";function t(t){var e=parseFloat(t),i=t.indexOf("%")==-1&&!isNaN(e);return i&&e}function e(){}function i(){for(var t={width:0,height:0,innerWidth:0,innerHeight:0,outerWidth:0,outerHeight:0},e=0;e<h;e++){var i=u[e];t[i]=0}return t}function o(t){var e=getComputedStyle(t);return e||a("Style returned "+e+". Are you running this code in a hidden iframe on Firefox? See http://bit.ly/getsizebug1"),e}function n(){if(!d){d=!0;var e=document.createElement("div");e.style.width="200px",e.style.padding="1px 2px 3px 4px",e.style.borderStyle="solid",e.style.borderWidth="1px 2px 3px 4px",e.style.boxSizing="border-box";var i=document.body||document.documentElement;i.appendChild(e);var n=o(e);s.isBoxSizeOuter=r=200==t(n.width),i.removeChild(e)}}function s(e){if(n(),"string"==typeof e&&(e=document.querySelector(e)),e&&"object"==typeof e&&e.nodeType){var s=o(e);if("none"==s.display)return i();var a={};a.width=e.offsetWidth,a.height=e.offsetHeight;for(var d=a.isBorderBox="border-box"==s.boxSizing,l=0;l<h;l++){var f=u[l],c=s[f],m=parseFloat(c);a[f]=isNaN(m)?0:m}var p=a.paddingLeft+a.paddingRight,y=a.paddingTop+a.paddingBottom,g=a.marginLeft+a.marginRight,v=a.marginTop+a.marginBottom,_=a.borderLeftWidth+a.borderRightWidth,I=a.borderTopWidth+a.borderBottomWidth,z=d&&r,x=t(s.width);x!==!1&&(a.width=x+(z?0:p+_));var S=t(s.height);return S!==!1&&(a.height=S+(z?0:y+I)),a.innerWidth=a.width-(p+_),a.innerHeight=a.height-(y+I),a.outerWidth=a.width+g,a.outerHeight=a.height+v,a}}var r,a="undefined"==typeof console?e:function(t){console.error(t)},u=["paddingLeft","paddingRight","paddingTop","paddingBottom","marginLeft","marginRight","marginTop","marginBottom","borderLeftWidth","borderRightWidth","borderTopWidth","borderBottomWidth"],h=u.length,d=!1;return s}),function(t,e){"use strict";"function"==typeof define&&define.amd?define("desandro-matches-selector/matches-selector",e):"object"==typeof module&&module.exports?module.exports=e():t.matchesSelector=e()}(window,function(){"use strict";var t=function(){var t=window.Element.prototype;if(t.matches)return"matches";if(t.matchesSelector)return"matchesSelector";for(var e=["webkit","moz","ms","o"],i=0;i<e.length;i++){var o=e[i],n=o+"MatchesSelector";if(t[n])return n}}();return function(e,i){return e[t](i)}}),function(t,e){"function"==typeof define&&define.amd?define("fizzy-ui-utils/utils",["desandro-matches-selector/matches-selector"],function(i){return e(t,i)}):"object"==typeof module&&module.exports?module.exports=e(t,require("desandro-matches-selector")):t.fizzyUIUtils=e(t,t.matchesSelector)}(window,function(t,e){var i={};i.extend=function(t,e){for(var i in e)t[i]=e[i];return t},i.modulo=function(t,e){return(t%e+e)%e},i.makeArray=function(t){var e=[];if(Array.isArray(t))e=t;else if(t&&"object"==typeof t&&"number"==typeof t.length)for(var i=0;i<t.length;i++)e.push(t[i]);else e.push(t);return e},i.removeFrom=function(t,e){var i=t.indexOf(e);i!=-1&&t.splice(i,1)},i.getParent=function(t,i){for(;t.parentNode&&t!=document.body;)if(t=t.parentNode,e(t,i))return t},i.getQueryElement=function(t){return"string"==typeof t?document.querySelector(t):t},i.handleEvent=function(t){var e="on"+t.type;this[e]&&this[e](t)},i.filterFindElements=function(t,o){t=i.makeArray(t);var n=[];return t.forEach(function(t){if(t instanceof HTMLElement){if(!o)return void n.push(t);e(t,o)&&n.push(t);for(var i=t.querySelectorAll(o),s=0;s<i.length;s++)n.push(i[s])}}),n},i.debounceMethod=function(t,e,i){var o=t.prototype[e],n=e+"Timeout";t.prototype[e]=function(){var t=this[n];t&&clearTimeout(t);var e=arguments,s=this;this[n]=setTimeout(function(){o.apply(s,e),delete s[n]},i||100)}},i.docReady=function(t){var e=document.readyState;"complete"==e||"interactive"==e?setTimeout(t):document.addEventListener("DOMContentLoaded",t)},i.toDashed=function(t){return t.replace(/(.)([A-Z])/g,function(t,e,i){return e+"-"+i}).toLowerCase()};var o=t.console;return i.htmlInit=function(e,n){i.docReady(function(){var s=i.toDashed(n),r="data-"+s,a=document.querySelectorAll("["+r+"]"),u=document.querySelectorAll(".js-"+s),h=i.makeArray(a).concat(i.makeArray(u)),d=r+"-options",l=t.jQuery;h.forEach(function(t){var i,s=t.getAttribute(r)||t.getAttribute(d);try{i=s&&JSON.parse(s)}catch(a){return void(o&&o.error("Error parsing "+r+" on "+t.className+": "+a))}var u=new e(t,i);l&&l.data(t,n,u)})})},i}),function(t,e){"function"==typeof define&&define.amd?define("outlayer/item",["ev-emitter/ev-emitter","get-size/get-size"],e):"object"==typeof module&&module.exports?module.exports=e(require("ev-emitter"),require("get-size")):(t.Outlayer={},t.Outlayer.Item=e(t.EvEmitter,t.getSize))}(window,function(t,e){"use strict";function i(t){for(var e in t)return!1;return e=null,!0}function o(t,e){t&&(this.element=t,this.layout=e,this.position={x:0,y:0},this._create())}function n(t){return t.replace(/([A-Z])/g,function(t){return"-"+t.toLowerCase()})}var s=document.documentElement.style,r="string"==typeof s.transition?"transition":"WebkitTransition",a="string"==typeof s.transform?"transform":"WebkitTransform",u={WebkitTransition:"webkitTransitionEnd",transition:"transitionend"}[r],h={transform:a,transition:r,transitionDuration:r+"Duration",transitionProperty:r+"Property",transitionDelay:r+"Delay"},d=o.prototype=Object.create(t.prototype);d.constructor=o,d._create=function(){this._transn={ingProperties:{},clean:{},onEnd:{}},this.css({position:"absolute"})},d.handleEvent=function(t){var e="on"+t.type;this[e]&&this[e](t)},d.getSize=function(){this.size=e(this.element)},d.css=function(t){var e=this.element.style;for(var i in t){var o=h[i]||i;e[o]=t[i]}},d.getPosition=function(){var t=getComputedStyle(this.element),e=this.layout._getOption("originLeft"),i=this.layout._getOption("originTop"),o=t[e?"left":"right"],n=t[i?"top":"bottom"],s=this.layout.size,r=o.indexOf("%")!=-1?parseFloat(o)/100*s.width:parseInt(o,10),a=n.indexOf("%")!=-1?parseFloat(n)/100*s.height:parseInt(n,10);r=isNaN(r)?0:r,a=isNaN(a)?0:a,r-=e?s.paddingLeft:s.paddingRight,a-=i?s.paddingTop:s.paddingBottom,this.position.x=r,this.position.y=a},d.layoutPosition=function(){var t=this.layout.size,e={},i=this.layout._getOption("originLeft"),o=this.layout._getOption("originTop"),n=i?"paddingLeft":"paddingRight",s=i?"left":"right",r=i?"right":"left",a=this.position.x+t[n];e[s]=this.getXValue(a),e[r]="";var u=o?"paddingTop":"paddingBottom",h=o?"top":"bottom",d=o?"bottom":"top",l=this.position.y+t[u];e[h]=this.getYValue(l),e[d]="",this.css(e),this.emitEvent("layout",[this])},d.getXValue=function(t){var e=this.layout._getOption("horizontal");return this.layout.options.percentPosition&&!e?t/this.layout.size.width*100+"%":t+"px"},d.getYValue=function(t){var e=this.layout._getOption("horizontal");return this.layout.options.percentPosition&&e?t/this.layout.size.height*100+"%":t+"px"},d._transitionTo=function(t,e){this.getPosition();var i=this.position.x,o=this.position.y,n=parseInt(t,10),s=parseInt(e,10),r=n===this.position.x&&s===this.position.y;if(this.setPosition(t,e),r&&!this.isTransitioning)return void this.layoutPosition();var a=t-i,u=e-o,h={};h.transform=this.getTranslate(a,u),this.transition({to:h,onTransitionEnd:{transform:this.layoutPosition},isCleaning:!0})},d.getTranslate=function(t,e){var i=this.layout._getOption("originLeft"),o=this.layout._getOption("originTop");return t=i?t:-t,e=o?e:-e,"translate3d("+t+"px, "+e+"px, 0)"},d.goTo=function(t,e){this.setPosition(t,e),this.layoutPosition()},d.moveTo=d._transitionTo,d.setPosition=function(t,e){this.position.x=parseInt(t,10),this.position.y=parseInt(e,10)},d._nonTransition=function(t){this.css(t.to),t.isCleaning&&this._removeStyles(t.to);for(var e in t.onTransitionEnd)t.onTransitionEnd[e].call(this)},d.transition=function(t){if(!parseFloat(this.layout.options.transitionDuration))return void this._nonTransition(t);var e=this._transn;for(var i in t.onTransitionEnd)e.onEnd[i]=t.onTransitionEnd[i];for(i in t.to)e.ingProperties[i]=!0,t.isCleaning&&(e.clean[i]=!0);if(t.from){this.css(t.from);var o=this.element.offsetHeight;o=null}this.enableTransition(t.to),this.css(t.to),this.isTransitioning=!0};var l="opacity,"+n(a);d.enableTransition=function(){if(!this.isTransitioning){var t=this.layout.options.transitionDuration;t="number"==typeof t?t+"ms":t,this.css({transitionProperty:l,transitionDuration:t,transitionDelay:this.staggerDelay||0}),this.element.addEventListener(u,this,!1)}},d.onwebkitTransitionEnd=function(t){this.ontransitionend(t)},d.onotransitionend=function(t){this.ontransitionend(t)};var f={"-webkit-transform":"transform"};d.ontransitionend=function(t){if(t.target===this.element){var e=this._transn,o=f[t.propertyName]||t.propertyName;if(delete e.ingProperties[o],i(e.ingProperties)&&this.disableTransition(),o in e.clean&&(this.element.style[t.propertyName]="",delete e.clean[o]),o in e.onEnd){var n=e.onEnd[o];n.call(this),delete e.onEnd[o]}this.emitEvent("transitionEnd",[this])}},d.disableTransition=function(){this.removeTransitionStyles(),this.element.removeEventListener(u,this,!1),this.isTransitioning=!1},d._removeStyles=function(t){var e={};for(var i in t)e[i]="";this.css(e)};var c={transitionProperty:"",transitionDuration:"",transitionDelay:""};return d.removeTransitionStyles=function(){this.css(c)},d.stagger=function(t){t=isNaN(t)?0:t,this.staggerDelay=t+"ms"},d.removeElem=function(){this.element.parentNode.removeChild(this.element),this.css({display:""}),this.emitEvent("remove",[this])},d.remove=function(){return r&&parseFloat(this.layout.options.transitionDuration)?(this.once("transitionEnd",function(){this.removeElem()}),void this.hide()):void this.removeElem()},d.reveal=function(){delete this.isHidden,this.css({display:""});var t=this.layout.options,e={},i=this.getHideRevealTransitionEndProperty("visibleStyle");e[i]=this.onRevealTransitionEnd,this.transition({from:t.hiddenStyle,to:t.visibleStyle,isCleaning:!0,onTransitionEnd:e})},d.onRevealTransitionEnd=function(){this.isHidden||this.emitEvent("reveal")},d.getHideRevealTransitionEndProperty=function(t){var e=this.layout.options[t];if(e.opacity)return"opacity";for(var i in e)return i},d.hide=function(){this.isHidden=!0,this.css({display:""});var t=this.layout.options,e={},i=this.getHideRevealTransitionEndProperty("hiddenStyle");e[i]=this.onHideTransitionEnd,this.transition({from:t.visibleStyle,to:t.hiddenStyle,isCleaning:!0,onTransitionEnd:e})},d.onHideTransitionEnd=function(){this.isHidden&&(this.css({display:"none"}),this.emitEvent("hide"))},d.destroy=function(){this.css({position:"",left:"",right:"",top:"",bottom:"",transition:"",transform:""})},o}),function(t,e){"use strict";"function"==typeof define&&define.amd?define("outlayer/outlayer",["ev-emitter/ev-emitter","get-size/get-size","fizzy-ui-utils/utils","./item"],function(i,o,n,s){return e(t,i,o,n,s)}):"object"==typeof module&&module.exports?module.exports=e(t,require("ev-emitter"),require("get-size"),require("fizzy-ui-utils"),require("./item")):t.Outlayer=e(t,t.EvEmitter,t.getSize,t.fizzyUIUtils,t.Outlayer.Item)}(window,function(t,e,i,o,n){"use strict";function s(t,e){var i=o.getQueryElement(t);if(!i)return void(u&&u.error("Bad element for "+this.constructor.namespace+": "+(i||t)));this.element=i,h&&(this.$element=h(this.element)),this.options=o.extend({},this.constructor.defaults),this.option(e);var n=++l;this.element.outlayerGUID=n,f[n]=this,this._create();var s=this._getOption("initLayout");s&&this.layout()}function r(t){function e(){t.apply(this,arguments)}return e.prototype=Object.create(t.prototype),e.prototype.constructor=e,e}function a(t){if("number"==typeof t)return t;var e=t.match(/(^\d*\.?\d*)(\w*)/),i=e&&e[1],o=e&&e[2];if(!i.length)return 0;i=parseFloat(i);var n=m[o]||1;return i*n}var u=t.console,h=t.jQuery,d=function(){},l=0,f={};s.namespace="outlayer",s.Item=n,s.defaults={containerStyle:{position:"relative"},initLayout:!0,originLeft:!0,originTop:!0,resize:!0,resizeContainer:!0,transitionDuration:"0.4s",hiddenStyle:{opacity:0,transform:"scale(0.001)"},visibleStyle:{opacity:1,transform:"scale(1)"}};var c=s.prototype;o.extend(c,e.prototype),c.option=function(t){o.extend(this.options,t)},c._getOption=function(t){var e=this.constructor.compatOptions[t];return e&&void 0!==this.options[e]?this.options[e]:this.options[t]},s.compatOptions={initLayout:"isInitLayout",horizontal:"isHorizontal",layoutInstant:"isLayoutInstant",originLeft:"isOriginLeft",originTop:"isOriginTop",resize:"isResizeBound",resizeContainer:"isResizingContainer"},c._create=function(){this.reloadItems(),this.stamps=[],this.stamp(this.options.stamp),o.extend(this.element.style,this.options.containerStyle);var t=this._getOption("resize");t&&this.bindResize()},c.reloadItems=function(){this.items=this._itemize(this.element.children)},c._itemize=function(t){for(var e=this._filterFindItemElements(t),i=this.constructor.Item,o=[],n=0;n<e.length;n++){var s=e[n],r=new i(s,this);o.push(r)}return o},c._filterFindItemElements=function(t){return o.filterFindElements(t,this.options.itemSelector)},c.getItemElements=function(){return this.items.map(function(t){return t.element})},c.layout=function(){this._resetLayout(),this._manageStamps();var t=this._getOption("layoutInstant"),e=void 0!==t?t:!this._isLayoutInited;this.layoutItems(this.items,e),this._isLayoutInited=!0},c._init=c.layout,c._resetLayout=function(){this.getSize()},c.getSize=function(){this.size=i(this.element)},c._getMeasurement=function(t,e){var o,n=this.options[t];n?("string"==typeof n?o=this.element.querySelector(n):n instanceof HTMLElement&&(o=n),this[t]=o?i(o)[e]:n):this[t]=0},c.layoutItems=function(t,e){t=this._getItemsForLayout(t),this._layoutItems(t,e),this._postLayout()},c._getItemsForLayout=function(t){return t.filter(function(t){return!t.isIgnored})},c._layoutItems=function(t,e){if(this._emitCompleteOnItems("layout",t),t&&t.length){var i=[];t.forEach(function(t){var o=this._getItemLayoutPosition(t);o.item=t,o.isInstant=e||t.isLayoutInstant,i.push(o)},this),this._processLayoutQueue(i)}},c._getItemLayoutPosition=function(){return{x:0,y:0}},c._processLayoutQueue=function(t){this.updateStagger(),t.forEach(function(t,e){this._positionItem(t.item,t.x,t.y,t.isInstant,e)},this)},c.updateStagger=function(){var t=this.options.stagger;return null===t||void 0===t?void(this.stagger=0):(this.stagger=a(t),this.stagger)},c._positionItem=function(t,e,i,o,n){o?t.goTo(e,i):(t.stagger(n*this.stagger),t.moveTo(e,i))},c._postLayout=function(){this.resizeContainer()},c.resizeContainer=function(){var t=this._getOption("resizeContainer");if(t){var e=this._getContainerSize();e&&(this._setContainerMeasure(e.width,!0),this._setContainerMeasure(e.height,!1))}},c._getContainerSize=d,c._setContainerMeasure=function(t,e){if(void 0!==t){var i=this.size;i.isBorderBox&&(t+=e?i.paddingLeft+i.paddingRight+i.borderLeftWidth+i.borderRightWidth:i.paddingBottom+i.paddingTop+i.borderTopWidth+i.borderBottomWidth),t=Math.max(t,0),this.element.style[e?"width":"height"]=t+"px"}},c._emitCompleteOnItems=function(t,e){function i(){n.dispatchEvent(t+"Complete",null,[e])}function o(){r++,r==s&&i()}var n=this,s=e.length;if(!e||!s)return void i();var r=0;e.forEach(function(e){e.once(t,o)})},c.dispatchEvent=function(t,e,i){var o=e?[e].concat(i):i;if(this.emitEvent(t,o),h)if(this.$element=this.$element||h(this.element),e){var n=h.Event(e);n.type=t,this.$element.trigger(n,i)}else this.$element.trigger(t,i)},c.ignore=function(t){var e=this.getItem(t);e&&(e.isIgnored=!0)},c.unignore=function(t){var e=this.getItem(t);e&&delete e.isIgnored},c.stamp=function(t){t=this._find(t),t&&(this.stamps=this.stamps.concat(t),t.forEach(this.ignore,this))},c.unstamp=function(t){t=this._find(t),t&&t.forEach(function(t){o.removeFrom(this.stamps,t),this.unignore(t)},this)},c._find=function(t){if(t)return"string"==typeof t&&(t=this.element.querySelectorAll(t)),t=o.makeArray(t)},c._manageStamps=function(){this.stamps&&this.stamps.length&&(this._getBoundingRect(),this.stamps.forEach(this._manageStamp,this))},c._getBoundingRect=function(){var t=this.element.getBoundingClientRect(),e=this.size;this._boundingRect={left:t.left+e.paddingLeft+e.borderLeftWidth,top:t.top+e.paddingTop+e.borderTopWidth,right:t.right-(e.paddingRight+e.borderRightWidth),bottom:t.bottom-(e.paddingBottom+e.borderBottomWidth)}},c._manageStamp=d,c._getElementOffset=function(t){var e=t.getBoundingClientRect(),o=this._boundingRect,n=i(t),s={left:e.left-o.left-n.marginLeft,top:e.top-o.top-n.marginTop,right:o.right-e.right-n.marginRight,bottom:o.bottom-e.bottom-n.marginBottom};return s},c.handleEvent=o.handleEvent,c.bindResize=function(){t.addEventListener("resize",this),this.isResizeBound=!0},c.unbindResize=function(){t.removeEventListener("resize",this),this.isResizeBound=!1},c.onresize=function(){this.resize()},o.debounceMethod(s,"onresize",100),c.resize=function(){this.isResizeBound&&this.needsResizeLayout()&&this.layout()},c.needsResizeLayout=function(){var t=i(this.element),e=this.size&&t;return e&&t.innerWidth!==this.size.innerWidth},c.addItems=function(t){var e=this._itemize(t);return e.length&&(this.items=this.items.concat(e)),e},c.appended=function(t){var e=this.addItems(t);e.length&&(this.layoutItems(e,!0),this.reveal(e))},c.prepended=function(t){var e=this._itemize(t);if(e.length){var i=this.items.slice(0);this.items=e.concat(i),this._resetLayout(),this._manageStamps(),this.layoutItems(e,!0),this.reveal(e),this.layoutItems(i)}},c.reveal=function(t){if(this._emitCompleteOnItems("reveal",t),t&&t.length){var e=this.updateStagger();t.forEach(function(t,i){t.stagger(i*e),t.reveal()})}},c.hide=function(t){if(this._emitCompleteOnItems("hide",t),t&&t.length){var e=this.updateStagger();t.forEach(function(t,i){t.stagger(i*e),t.hide()})}},c.revealItemElements=function(t){var e=this.getItems(t);this.reveal(e)},c.hideItemElements=function(t){var e=this.getItems(t);this.hide(e)},c.getItem=function(t){for(var e=0;e<this.items.length;e++){var i=this.items[e];if(i.element==t)return i}},c.getItems=function(t){t=o.makeArray(t);var e=[];return t.forEach(function(t){var i=this.getItem(t);i&&e.push(i)},this),e},c.remove=function(t){var e=this.getItems(t);this._emitCompleteOnItems("remove",e),e&&e.length&&e.forEach(function(t){t.remove(),o.removeFrom(this.items,t)},this)},c.destroy=function(){var t=this.element.style;t.height="",t.position="",t.width="",this.items.forEach(function(t){t.destroy()}),this.unbindResize();var e=this.element.outlayerGUID;delete f[e],delete this.element.outlayerGUID,h&&h.removeData(this.element,this.constructor.namespace)},s.data=function(t){t=o.getQueryElement(t);var e=t&&t.outlayerGUID;return e&&f[e]},s.create=function(t,e){var i=r(s);return i.defaults=o.extend({},s.defaults),o.extend(i.defaults,e),i.compatOptions=o.extend({},s.compatOptions),i.namespace=t,i.data=s.data,i.Item=r(n),o.htmlInit(i,t),h&&h.bridget&&h.bridget(t,i),i};var m={ms:1,s:1e3};return s.Item=n,s}),function(t,e){"function"==typeof define&&define.amd?define("isotope/js/item",["outlayer/outlayer"],e):"object"==typeof module&&module.exports?module.exports=e(require("outlayer")):(t.Isotope=t.Isotope||{},t.Isotope.Item=e(t.Outlayer))}(window,function(t){"use strict";function e(){t.Item.apply(this,arguments)}var i=e.prototype=Object.create(t.Item.prototype),o=i._create;i._create=function(){this.id=this.layout.itemGUID++,o.call(this),this.sortData={}},i.updateSortData=function(){if(!this.isIgnored){this.sortData.id=this.id,this.sortData["original-order"]=this.id,this.sortData.random=Math.random();var t=this.layout.options.getSortData,e=this.layout._sorters;for(var i in t){var o=e[i];this.sortData[i]=o(this.element,this)}}};var n=i.destroy;return i.destroy=function(){n.apply(this,arguments),this.css({display:""})},e}),function(t,e){"function"==typeof define&&define.amd?define("isotope/js/layout-mode",["get-size/get-size","outlayer/outlayer"],e):"object"==typeof module&&module.exports?module.exports=e(require("get-size"),require("outlayer")):(t.Isotope=t.Isotope||{},t.Isotope.LayoutMode=e(t.getSize,t.Outlayer))}(window,function(t,e){"use strict";function i(t){this.isotope=t,t&&(this.options=t.options[this.namespace],this.element=t.element,this.items=t.filteredItems,this.size=t.size)}var o=i.prototype,n=["_resetLayout","_getItemLayoutPosition","_manageStamp","_getContainerSize","_getElementOffset","needsResizeLayout","_getOption"];return n.forEach(function(t){o[t]=function(){return e.prototype[t].apply(this.isotope,arguments)}}),o.needsVerticalResizeLayout=function(){var e=t(this.isotope.element),i=this.isotope.size&&e;return i&&e.innerHeight!=this.isotope.size.innerHeight},o._getMeasurement=function(){this.isotope._getMeasurement.apply(this,arguments)},o.getColumnWidth=function(){this.getSegmentSize("column","Width")},o.getRowHeight=function(){this.getSegmentSize("row","Height")},o.getSegmentSize=function(t,e){var i=t+e,o="outer"+e;if(this._getMeasurement(i,o),!this[i]){var n=this.getFirstItemSize();this[i]=n&&n[o]||this.isotope.size["inner"+e]}},o.getFirstItemSize=function(){var e=this.isotope.filteredItems[0];return e&&e.element&&t(e.element)},o.layout=function(){this.isotope.layout.apply(this.isotope,arguments)},o.getSize=function(){this.isotope.getSize(),this.size=this.isotope.size},i.modes={},i.create=function(t,e){function n(){i.apply(this,arguments)}return n.prototype=Object.create(o),n.prototype.constructor=n,e&&(n.options=e),n.prototype.namespace=t,i.modes[t]=n,n},i}),function(t,e){"function"==typeof define&&define.amd?define("masonry/masonry",["outlayer/outlayer","get-size/get-size"],e):"object"==typeof module&&module.exports?module.exports=e(require("outlayer"),require("get-size")):t.Masonry=e(t.Outlayer,t.getSize)}(window,function(t,e){var i=t.create("masonry");i.compatOptions.fitWidth="isFitWidth";var o=i.prototype;return o._resetLayout=function(){this.getSize(),this._getMeasurement("columnWidth","outerWidth"),this._getMeasurement("gutter","outerWidth"),this.measureColumns(),this.colYs=[];for(var t=0;t<this.cols;t++)this.colYs.push(0);this.maxY=0,this.horizontalColIndex=0},o.measureColumns=function(){if(this.getContainerWidth(),!this.columnWidth){var t=this.items[0],i=t&&t.element;this.columnWidth=i&&e(i).outerWidth||this.containerWidth}var o=this.columnWidth+=this.gutter,n=this.containerWidth+this.gutter,s=n/o,r=o-n%o,a=r&&r<1?"round":"floor";s=Math[a](s),this.cols=Math.max(s,1)},o.getContainerWidth=function(){var t=this._getOption("fitWidth"),i=t?this.element.parentNode:this.element,o=e(i);this.containerWidth=o&&o.innerWidth},o._getItemLayoutPosition=function(t){t.getSize();var e=t.size.outerWidth%this.columnWidth,i=e&&e<1?"round":"ceil",o=Math[i](t.size.outerWidth/this.columnWidth);o=Math.min(o,this.cols);for(var n=this.options.horizontalOrder?"_getHorizontalColPosition":"_getTopColPosition",s=this[n](o,t),r={x:this.columnWidth*s.col,y:s.y},a=s.y+t.size.outerHeight,u=o+s.col,h=s.col;h<u;h++)this.colYs[h]=a;return r},o._getTopColPosition=function(t){var e=this._getTopColGroup(t),i=Math.min.apply(Math,e);return{col:e.indexOf(i),y:i}},o._getTopColGroup=function(t){if(t<2)return this.colYs;for(var e=[],i=this.cols+1-t,o=0;o<i;o++)e[o]=this._getColGroupY(o,t);return e},o._getColGroupY=function(t,e){if(e<2)return this.colYs[t];var i=this.colYs.slice(t,t+e);return Math.max.apply(Math,i)},o._getHorizontalColPosition=function(t,e){var i=this.horizontalColIndex%this.cols,o=t>1&&i+t>this.cols;i=o?0:i;var n=e.size.outerWidth&&e.size.outerHeight;return this.horizontalColIndex=n?i+t:this.horizontalColIndex,{col:i,y:this._getColGroupY(i,t)}},o._manageStamp=function(t){var i=e(t),o=this._getElementOffset(t),n=this._getOption("originLeft"),s=n?o.left:o.right,r=s+i.outerWidth,a=Math.floor(s/this.columnWidth);a=Math.max(0,a);var u=Math.floor(r/this.columnWidth);u-=r%this.columnWidth?0:1,u=Math.min(this.cols-1,u);for(var h=this._getOption("originTop"),d=(h?o.top:o.bottom)+i.outerHeight,l=a;l<=u;l++)this.colYs[l]=Math.max(d,this.colYs[l])},o._getContainerSize=function(){this.maxY=Math.max.apply(Math,this.colYs);var t={height:this.maxY};return this._getOption("fitWidth")&&(t.width=this._getContainerFitWidth()),t},o._getContainerFitWidth=function(){for(var t=0,e=this.cols;--e&&0===this.colYs[e];)t++;return(this.cols-t)*this.columnWidth-this.gutter},o.needsResizeLayout=function(){var t=this.containerWidth;return this.getContainerWidth(),t!=this.containerWidth},i}),function(t,e){"function"==typeof define&&define.amd?define("isotope/js/layout-modes/masonry",["../layout-mode","masonry/masonry"],e):"object"==typeof module&&module.exports?module.exports=e(require("../layout-mode"),require("masonry-layout")):e(t.Isotope.LayoutMode,t.Masonry)}(window,function(t,e){"use strict";var i=t.create("masonry"),o=i.prototype,n={_getElementOffset:!0,layout:!0,_getMeasurement:!0};for(var s in e.prototype)n[s]||(o[s]=e.prototype[s]);var r=o.measureColumns;o.measureColumns=function(){this.items=this.isotope.filteredItems,r.call(this)};var a=o._getOption;return o._getOption=function(t){return"fitWidth"==t?void 0!==this.options.isFitWidth?this.options.isFitWidth:this.options.fitWidth:a.apply(this.isotope,arguments)},i}),function(t,e){"function"==typeof define&&define.amd?define("isotope/js/layout-modes/fit-rows",["../layout-mode"],e):"object"==typeof exports?module.exports=e(require("../layout-mode")):e(t.Isotope.LayoutMode)}(window,function(t){"use strict";var e=t.create("fitRows"),i=e.prototype;return i._resetLayout=function(){this.x=0,this.y=0,this.maxY=0,this._getMeasurement("gutter","outerWidth")},i._getItemLayoutPosition=function(t){t.getSize();var e=t.size.outerWidth+this.gutter,i=this.isotope.size.innerWidth+this.gutter;0!==this.x&&e+this.x>i&&(this.x=0,this.y=this.maxY);var o={x:this.x,y:this.y};return this.maxY=Math.max(this.maxY,this.y+t.size.outerHeight),this.x+=e,o},i._getContainerSize=function(){return{height:this.maxY}},e}),function(t,e){"function"==typeof define&&define.amd?define("isotope/js/layout-modes/vertical",["../layout-mode"],e):"object"==typeof module&&module.exports?module.exports=e(require("../layout-mode")):e(t.Isotope.LayoutMode)}(window,function(t){"use strict";var e=t.create("vertical",{horizontalAlignment:0}),i=e.prototype;return i._resetLayout=function(){this.y=0},i._getItemLayoutPosition=function(t){t.getSize();var e=(this.isotope.size.innerWidth-t.size.outerWidth)*this.options.horizontalAlignment,i=this.y;return this.y+=t.size.outerHeight,{x:e,y:i}},i._getContainerSize=function(){return{height:this.y}},e}),function(t,e){"function"==typeof define&&define.amd?define(["outlayer/outlayer","get-size/get-size","desandro-matches-selector/matches-selector","fizzy-ui-utils/utils","isotope/js/item","isotope/js/layout-mode","isotope/js/layout-modes/masonry","isotope/js/layout-modes/fit-rows","isotope/js/layout-modes/vertical"],function(i,o,n,s,r,a){return e(t,i,o,n,s,r,a)}):"object"==typeof module&&module.exports?module.exports=e(t,require("outlayer"),require("get-size"),require("desandro-matches-selector"),require("fizzy-ui-utils"),require("isotope/js/item"),require("isotope/js/layout-mode"),require("isotope/js/layout-modes/masonry"),require("isotope/js/layout-modes/fit-rows"),require("isotope/js/layout-modes/vertical")):t.Isotope=e(t,t.Outlayer,t.getSize,t.matchesSelector,t.fizzyUIUtils,t.Isotope.Item,t.Isotope.LayoutMode)}(window,function(t,e,i,o,n,s,r){function a(t,e){return function(i,o){for(var n=0;n<t.length;n++){var s=t[n],r=i.sortData[s],a=o.sortData[s];if(r>a||r<a){var u=void 0!==e[s]?e[s]:e,h=u?1:-1;return(r>a?1:-1)*h}}return 0}}var u=t.jQuery,h=String.prototype.trim?function(t){return t.trim()}:function(t){return t.replace(/^\s+|\s+$/g,"")},d=e.create("isotope",{layoutMode:"masonry",isJQueryFiltering:!0,sortAscending:!0});d.Item=s,d.LayoutMode=r;var l=d.prototype;l._create=function(){this.itemGUID=0,this._sorters={},this._getSorters(),e.prototype._create.call(this),this.modes={},this.filteredItems=this.items,this.sortHistory=["original-order"];for(var t in r.modes)this._initLayoutMode(t)},l.reloadItems=function(){this.itemGUID=0,e.prototype.reloadItems.call(this)},l._itemize=function(){for(var t=e.prototype._itemize.apply(this,arguments),i=0;i<t.length;i++){var o=t[i];o.id=this.itemGUID++}return this._updateItemsSortData(t),t},l._initLayoutMode=function(t){var e=r.modes[t],i=this.options[t]||{};this.options[t]=e.options?n.extend(e.options,i):i,this.modes[t]=new e(this)},l.layout=function(){return!this._isLayoutInited&&this._getOption("initLayout")?void this.arrange():void this._layout()},l._layout=function(){var t=this._getIsInstant();this._resetLayout(),this._manageStamps(),this.layoutItems(this.filteredItems,t),this._isLayoutInited=!0},l.arrange=function(t){this.option(t),this._getIsInstant();var e=this._filter(this.items);this.filteredItems=e.matches,this._bindArrangeComplete(),this._isInstant?this._noTransition(this._hideReveal,[e]):this._hideReveal(e),this._sort(),this._layout()},l._init=l.arrange,l._hideReveal=function(t){this.reveal(t.needReveal),this.hide(t.needHide)},l._getIsInstant=function(){var t=this._getOption("layoutInstant"),e=void 0!==t?t:!this._isLayoutInited;return this._isInstant=e,e},l._bindArrangeComplete=function(){function t(){e&&i&&o&&n.dispatchEvent("arrangeComplete",null,[n.filteredItems])}var e,i,o,n=this;this.once("layoutComplete",function(){e=!0,t()}),this.once("hideComplete",function(){i=!0,t()}),this.once("revealComplete",function(){o=!0,t()})},l._filter=function(t){var e=this.options.filter;e=e||"*";for(var i=[],o=[],n=[],s=this._getFilterTest(e),r=0;r<t.length;r++){var a=t[r];if(!a.isIgnored){var u=s(a);u&&i.push(a),u&&a.isHidden?o.push(a):u||a.isHidden||n.push(a)}}return{matches:i,needReveal:o,needHide:n}},l._getFilterTest=function(t){return u&&this.options.isJQueryFiltering?function(e){return u(e.element).is(t)}:"function"==typeof t?function(e){return t(e.element)}:function(e){return o(e.element,t)}},l.updateSortData=function(t){
var e;t?(t=n.makeArray(t),e=this.getItems(t)):e=this.items,this._getSorters(),this._updateItemsSortData(e)},l._getSorters=function(){var t=this.options.getSortData;for(var e in t){var i=t[e];this._sorters[e]=f(i)}},l._updateItemsSortData=function(t){for(var e=t&&t.length,i=0;e&&i<e;i++){var o=t[i];o.updateSortData()}};var f=function(){function t(t){if("string"!=typeof t)return t;var i=h(t).split(" "),o=i[0],n=o.match(/^\[(.+)\]$/),s=n&&n[1],r=e(s,o),a=d.sortDataParsers[i[1]];return t=a?function(t){return t&&a(r(t))}:function(t){return t&&r(t)}}function e(t,e){return t?function(e){return e.getAttribute(t)}:function(t){var i=t.querySelector(e);return i&&i.textContent}}return t}();d.sortDataParsers={parseInt:function(t){return parseInt(t,10)},parseFloat:function(t){return parseFloat(t)}},l._sort=function(){if(this.options.sortBy){var t=n.makeArray(this.options.sortBy);this._getIsSameSortBy(t)||(this.sortHistory=t.concat(this.sortHistory));var e=a(this.sortHistory,this.options.sortAscending);this.filteredItems.sort(e)}},l._getIsSameSortBy=function(t){for(var e=0;e<t.length;e++)if(t[e]!=this.sortHistory[e])return!1;return!0},l._mode=function(){var t=this.options.layoutMode,e=this.modes[t];if(!e)throw new Error("No layout mode: "+t);return e.options=this.options[t],e},l._resetLayout=function(){e.prototype._resetLayout.call(this),this._mode()._resetLayout()},l._getItemLayoutPosition=function(t){return this._mode()._getItemLayoutPosition(t)},l._manageStamp=function(t){this._mode()._manageStamp(t)},l._getContainerSize=function(){return this._mode()._getContainerSize()},l.needsResizeLayout=function(){return this._mode().needsResizeLayout()},l.appended=function(t){var e=this.addItems(t);if(e.length){var i=this._filterRevealAdded(e);this.filteredItems=this.filteredItems.concat(i)}},l.prepended=function(t){var e=this._itemize(t);if(e.length){this._resetLayout(),this._manageStamps();var i=this._filterRevealAdded(e);this.layoutItems(this.filteredItems),this.filteredItems=i.concat(this.filteredItems),this.items=e.concat(this.items)}},l._filterRevealAdded=function(t){var e=this._filter(t);return this.hide(e.needHide),this.reveal(e.matches),this.layoutItems(e.matches,!0),e.matches},l.insert=function(t){var e=this.addItems(t);if(e.length){var i,o,n=e.length;for(i=0;i<n;i++)o=e[i],this.element.appendChild(o.element);var s=this._filter(e).matches;for(i=0;i<n;i++)e[i].isLayoutInstant=!0;for(this.arrange(),i=0;i<n;i++)delete e[i].isLayoutInstant;this.reveal(s)}};var c=l.remove;return l.remove=function(t){t=n.makeArray(t);var e=this.getItems(t);c.call(this,t);for(var i=e&&e.length,o=0;i&&o<i;o++){var s=e[o];n.removeFrom(this.filteredItems,s)}},l.shuffle=function(){for(var t=0;t<this.items.length;t++){var e=this.items[t];e.sortData.random=Math.random()}this.options.sortBy="random",this._sort(),this._layout()},l._noTransition=function(t,e){var i=this.options.transitionDuration;this.options.transitionDuration=0;var o=t.apply(this,e);return this.options.transitionDuration=i,o},l.getFilteredItemElements=function(){return this.filteredItems.map(function(t){return t.element})},d});
/*
*  __  __  _____   _____ 
* |  \/  |/ ____| / ____|
* | \  / | |  __ | (___  _ __   __ _  ___ ___    _  ___ 
* | |\/| | | |_ | \___ \| '_ \ / _` |/ __/ _ \  (_)/ __| 
* | |  | | |__| | ____) | |_) | (_| | (_|  __/_ | |\__ \
* |_|  |_|\_____||_____/| .__/ \__,_|\___\___(_)| ||___/
*                       | |                    _| |
*                       |_|                   |___,
* MG Space
*
* Copyright (c) 2016 Bryce Mullican (http://brycemullican.com)
*
* By Bryce Mullican (@BryceMullican)
* Licensed under the MIT license.
*
* @link https://github.com/Mad-Genius/mg-space
* @author Bryce Mullican
* @version 1.0.0
*/
;(function ( $, window, document, undefined ) {

    "use strict";

    // Create the defaults once
    var mgSpace = "mgSpace",
        defaults = {

            // Breakpoints at which the accordian changes # of columns
            breakpointColumns: [
                {
                    breakpoint: 0,
                    column: 1
                },
                {
                    breakpoint: 568,
                    column: 2
                },
                {
                    breakpoint: 768,
                    column: 3
                },
                {
                    breakpoint: 1200,
                    column: 4
                }
            ],

            // Default selectors
            rowWrapper: ".mg-rows",
            row: ".mg-row",
            targetWrapper: ".mg-targets",
            target: ".mg-target",
            trigger: ".mg-trigger",
            close: ".mg-close",

            // Default target padding top/bottom and row bottom margin
            rowMargin: 25, // Set to zero for gridless
            targetPadding: 120, // Padding top/bottom inside target gets divided by 2

            useHash: false, // Set to true for history
            useOnpageHash: false, // Set true for onpage history
            hashTitle: "#/item-", // Must include `#` hash symbol

            // MISC          
            useIndicator: true
        },
        shouldClick = true,
        activatingHash = false;

    // The actual plugin constructor
    function MGSpace ( element, options ) {
        var _ = this;

        _.$mgSpace = $(element);
        _.options = $.extend( {}, defaults, options );
        _._defaults = defaults;
        _._name =  mgSpace;
        _.init();

        //console.log('MADE IT HERE');
    }

    // Avoid MGSpace.prototype conflicts
    $.extend(MGSpace.prototype, {
        init: function () {
            var _ = this,
                cols = _.setColumns();

            // Set Columns
            $(_.options.rowWrapper, _.$mgSpace).attr('data-cols', cols);

            _.$rows = _.$mgSpace.find(_.options.rowWrapper).children('');
            _.$targets = _.$mgSpace.find(_.options.targetWrapper).children('');

            // Set Rows and Targets
            _.setRows($(_.$rows, _.$mgSpace));
            _.setRows($(_.$targets, _.$mgSpace));

            // Add Row Margin if Greater Than Zero (0)
            if (_.options.rowMargin > 0) {
                _.$mgSpace.prepend('<style scoped>'+_.options.row+'{margin-bottom:'+_.options.rowMargin+'px}</style>');
            }


            // NEED TO BUILD BETTER CLICK HANDLER
            _.$mgSpace.on('click.mg', _.options.trigger, function (event) {
                event.preventDefault();
            });

            // CLICK HANDLING SHOULD GO THROUGH ROW CONTROLLER
            _.$mgSpace.on('click.mg.trigger', _.options.trigger, {mgSpace:_}, _.clickHandler);
            _.$mgSpace.on('click.mg.close', _.options.close, {close:true, mgSpace:_}, _.clickHandler);

            $(window).on('resize.mg', function () {
                cols = _.setColumns();
                _.setRows($(_.options.row, _.$mgSpace));

                if (cols != $(_.options.rowWrapper).attr('data-cols')) {
                    $(_.options.rowWrapper).attr('data-cols',cols);

                    // Close
                    $(_.options.target+'-open').removeClass(_.stripDot(_.options.target)+'-open').removeAttr('style');
                    $(_.options.row+'-open').removeClass(_.stripDot(_.options.row)+'-open');
                    $('.mg-space').remove();

                    _.$mgSpace.trigger('afterCloseTarget', [_]);
                }


                if($(_.options.target+'-open').length){
                    _.resizeSpace(_.options.row+'-open');
                }                
            });

            // LOAD HASH IF EXISTS
            if (window.location.hash && _.options.useHash) {
                setTimeout(function () {
                    var sectionID =  window.location.hash.replace(_.options.hashTitle, "").split('-'),
                        rowItem = _.options.row+'[data-section="' + sectionID[0] + '"][data-id="' + sectionID[1] + '"]';

                    if (sectionID[0] == _.$mgSpace.index()) {
                         activatingHash = true;
                        _.openRow(rowItem);
                    }

                }, 400);
            }

            $(window).on('hashchange.mg', function () {
                if (window.location.hash && _.options.useOnpageHash) {
                    var sectionID =  window.location.hash.replace(_.options.hashTitle, "").split('-'),
                        rowItem = _.options.row+'[data-section="' + sectionID[0] + '"][data-id="' + sectionID[1] + '"]';

                    _.scrollToTop(rowItem);
                    activatingHash = true;

                    if (sectionID[0] == _.$mgSpace.index()) {
                        _.closeRow(300);

                        setTimeout(function () {
                            _.openRow(rowItem);
                        }, 400);
                    }
                } else {
                    _.closeRow(300);
                }               
            });            
        },

        rowController: function (element, event) {
            var _ = this,
                $rowItem = $(element).closest(_.options.row),
                itemSection = $rowItem.attr('data-section'),
                itemRow = $rowItem.attr('data-row');

            _.$mgSpace.off('click.trigger', _.options.trigger, _.clickHandler);

            if ((event.data && event.data.close) || $rowItem.hasClass(_.stripDot(_.options.row)+'-open')) {
                _.closeRow(300);
            } else {
                if ($(_.options.row+'-open[data-section="' + itemSection + '"][data-row="' + itemRow + '"]').length) {
                    // Same Row
                    //console.log('Same Row');

                    _.closeTarget(200);
                    _.resizeSpace($rowItem);
                    _.openTarget($rowItem);
                    _.scrollToTop($rowItem);                    
                } else if ($('.mg-space').hasClass('mg-space-open')) {
                    // New Row
                    //console.log('New Row');

                    _.closeTarget(200);
                    $('.mg-space').slideToggle(300, function () {
                        _.openRow($rowItem);
                    });
                } else {
                    _.openRow($rowItem);
                }
            }
        },

        openRow: function (element) {
            var _ = this;

            // Before Open Row Event Handler
            _.$mgSpace.trigger('beforeOpenRow', [_, element]);

            //console.log('Open Row');

            $(_.options.row+'-open').removeClass(_.stripDot(_.options.row)+'-open');

            _.openSpace(element);
            _.openTarget(element);
            _.scrollToTop(element); 
            
            // After Open Row Event Handler
            _.$mgSpace.trigger('afterOpenRow', [_, element]);
        },

        closeRow: function (speed) {
            var _ = this;

            //console.log('Close Row');

            _.closeTarget(speed);
            _.closeSpace(speed);
        },        

        openTarget: function (element) {
            var _ = this,
                itemSection = $(element).attr('data-section'),
                itemId = $(element).attr('data-id'),
                $itemTarget = $(_.options.target+'[data-section="' + itemSection + '"][data-id="' + itemId + '"]', _.$mgSpace);

            //console.log('Open Target');

            // Before Open Target Event Handler
            _.$mgSpace.trigger('beforeOpenTarget', [_]);

            $(element).addClass(_.stripDot(_.options.row)+'-open');

            // Offset Bug in Chrome Hack ON
            $('body').css('overflowY','scroll');

            $itemTarget.prepend('<a href="#" class="'+_.stripDot(_.options.close)+'"></a>');

            $itemTarget
                .removeAttr('style')
                .addClass(_.stripDot(_.options.target)+'-open')
                .css({
                    position: 'absolute',
                    top: $('.mg-space').position().top + $('.mg-space').parent().position().top,
                    zIndex: 2,
                    paddingTop: _.options.targetPadding/2,
                    paddingBottom: _.options.targetPadding/2
                })
                .slideDown(300, function(){
                    _.$mgSpace.on('click.mg.trigger', _.options.trigger, {mgSpace:_}, _.clickHandler);
                    $(_.options.close).fadeIn(200);

                    // After Open Target Event Handler
                    _.$mgSpace.trigger('afterOpenTarget', [_, $itemTarget]);                    
                });

            // Offset Bug in Chrome Hack OFF
            $('body').removeAttr('style');

            if (_.options.useHash && !activatingHash) {
                _.activateHash(_.options.hashTitle+itemSection+'-'+itemId);
            }

            activatingHash = false;
        },

        closeTarget: function (speed) {
            var _ = this;

            //console.log('Close Target');

            // Before Close Target Event Handler
            _.$mgSpace.trigger('beforeCloseTarget', [_]);

            $(_.options.row+'-open').removeClass(_.stripDot(_.options.row)+'-open');
            $(_.options.target+'-open').css('z-index',1);
            $(_.options.close).remove();
            $(_.options.target+'-open').slideUp(speed, function () {
                $(this).removeClass(_.stripDot(_.options.target)+'-open').removeAttr('style');
                _.$mgSpace.on('click.mg.trigger', _.options.trigger, {mgSpace:_}, _.clickHandler);


                // After Close Target Event Handler
                _.$mgSpace.trigger('afterCloseTarget', [_]);                
            });              
        },            

        openSpace: function (element) {           
            var _ = this,
                itemSection = $(element).attr('data-section'),
                itemRow = $(element).attr('data-row'),
                itemId = $(element).attr('data-id'),
                itemPosition = $(element).position(),
                $itemTarget = $(_.options.target+'[data-section="' + itemSection + '"][data-id="' + itemId + '"]', _.$mgSpace),
                targetHeight = 0;

            //console.log('Open Space');

            targetHeight = $itemTarget.css('position','fixed').show().height();

            if (!$('.mg-space[data-section="' + itemSection + '"][data-row="' + itemRow + '"]').length) {
                $('.mg-space').remove();
                $(_.options.rowWrapper).find('[data-section="' + itemSection + '"][data-row="' + itemRow + '"]').last().after('<div class="mg-space" data-section="' + itemSection + '" data-row="' + itemRow + '"><div class="mg-indicator"></div></div>');
            }

            $('.mg-space[data-section="' + itemSection + '"][data-row="' + itemRow + '"]').css({
                height: targetHeight + _.options.targetPadding,
                marginBottom: _.options.rowMargin
            }).slideDown(300, function() {
                $('.mg-space').addClass('mg-space-open');
                if (_.options.useIndicator) {
                    $('.mg-indicator').css({
                        left: itemPosition.left + parseInt($(element).css('padding-left')) + $(element).width()/2 - 10,
                    });
                    $('.mg-indicator').animate({top:-9}, 200);
                }
            });                        
        },

        closeSpace: function (speed) {
            var _ = this;

            //console.log('Close Space');            

            $('.mg-space').slideUp(speed, function () {
                $('.mg-space').removeClass('mg-space-open');
                if (_.options.useIndicator) {
                    $('.mg-indicator').css({top:1});
                }
            });            
        },

        resizeSpace: function (element) {
            var _ = this,
                itemId = $(element).attr('data-id'),
                itemSection = $(element).attr('data-section'),
                itemPosition = $(element).position(),
                $itemTarget = $(_.options.target+'[data-section="' + itemSection + '"][data-id="' + itemId + '"]'),
                itemTargetOpen = $itemTarget.hasClass(_.stripDot(_.options.target+'-open')),
                targetHeight;

            if (!itemTargetOpen) {
                $itemTarget.css('position','fixed').show();
            } else {
                $(_.options.target+'-open').css('top',$(_.options.row+'-open').offset().top+$(_.options.row+'-open').height() + _.options.rowMargin );                
            }

            targetHeight = $itemTarget.height();

            if (_.options.useIndicator && !itemTargetOpen) {
                $('.mg-indicator').css({top:1});
            }

            if (!itemTargetOpen) {
                $('.mg-space').animate({
                    height: targetHeight + _.options.targetPadding
                }, 200, function () {
                    if (_.options.useIndicator) {
                        $('.mg-indicator').css({
                            left: itemPosition.left + parseInt($(element).css('padding-left')) + $(element).width()/2 - 10,
                        });
                        $('.mg-indicator').animate({top:-9}, 200);
                    }
                });
            } else {
                $('.mg-space').css(
                    'height', targetHeight + _.options.targetPadding
                );

                if (_.options.useIndicator) {
                    $('.mg-indicator').css({
                        left: itemPosition.left + parseInt($(element).css('padding-left')) + $(element).width()/2 - 10,
                    });
                    $('.mg-indicator').animate({top:-9}, 200);
                }                                            
            }          
        },

        destroy: function () {
            var _ = this;

            // REMOVE PLUGIN
            _.$mgSpace.removeData("plugin_" + mgSpace);

            // TURN OFF EVENTS FIRST
            _.$mgSpace.off('.mg');
            $(window).off('.mg');

            // REMOVE DATA ATTRS
            $('.mg-rows').removeAttr('data-cols');
            $('.mg-row, .mg-target').removeAttr('data-id');
            $('.mg-row, .mg-target').removeAttr('data-section');
            $('.mg-row, .mg-target').removeAttr('data-row');

            // REMOVE INLINE STYLES
            $('.mg-row, .mg-target').removeAttr('style');

            // REMOVE ADDED CLASSES
            $('.mg-row, .mg-target').removeClass('mg-row mg-target');

            // REMOVE MG SPACE
            $('.mg-space').remove();
        },

        setColumns: function () {
            var _ = this,
                cols = 1;

            $.each(_.options.breakpointColumns, function( idx, val ) {
                if (_.getViewportWidth() > val.breakpoint) {
                    cols = val.column;
                }
            });

            return cols;
        },

        setRows: function (rows) {
            var _ = this,
                row = 0,
                colCount = 1,
                cols = _.setColumns(),
                parent = null,
                newParent = _.$mgSpace.index();                

            rows.each(function(idx) {
                
                if(parent === null) {
                    parent = newParent;
                }

                if(parent != newParent){
                    parent = _.$mgSpace.index();
                }

                $(this).attr('data-id', idx + 1);
                $(this).attr('data-section', parent);

                if (!$(this).parent().hasClass(_.stripDot(_.options.targetWrapper))) {
                    $(this).attr('data-row', row);
                    $(this).addClass(_.stripDot(_.options.row));
                } else {
                    $(this).addClass(_.stripDot(_.options.target));
                }

                if(colCount==cols){
                    row++;
                    colCount=0;
                }
                colCount++;
                
            });            
        },

        getViewportWidth: function () {
           var e = window, a = 'inner';
           if (!('innerWidth' in window )) {
               a = 'client';
               e = document.documentElement || document.body;
           }
           return e[ a+'Width' ];
        },

        activateHash: function (hash) {
            if(history.pushState) {
                history.pushState(null, null, window.location.origin + window.location.pathname + window.location.search + hash);
            } else {
                // Otherwise fallback to the hash update for sites that don't support the history api
                window.location.hash = hash;
            }
        },

        scrollToTop: function (element) {
            $('html, body').animate({
                scrollTop: $(element).offset().top
            }, 400);
        },

        clickHandler: function(event) {
            var _ = event.data.mgSpace;

            if (shouldClick) {
                event.stopImmediatePropagation();
                event.stopPropagation();
                event.preventDefault();

                _.rowController(this, event);
            }

        },        

        stripDot: function (string) {
            return string.replace('.', '');
        }

    }); //END $.extend

    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[ mgSpace ] = function ( options ) {
        return this.each(function() {
            if ( !$.data( this, "plugin_" + mgSpace ) ) {
                $.data( this, "plugin_" + mgSpace, new MGSpace( this, options ) );
            }
        });
    };

})( jQuery, window, document );
(function (window) {
  window.__env = window.__env || {};

  // API url
  window.__env.apiUrl = window.api_url;
  
}(this));
var app = angular
  .module("myApp", [
    "ngAnimate",
    "ui.bootstrap",
    "restangular",
    "ui.select",
    "ngRoute",
    "datatables",
    "datatables.fixedcolumns",
    "daterangepicker",
    "angularFileUpload",
    "vs-repeat",
    "ngSanitize"
  ])
  .config([
    "$interpolateProvider",
    "RestangularProvider",
    function($interpolateProvider, RestangularProvider) {
      $interpolateProvider.startSymbol("%%");
      $interpolateProvider.endSymbol("%%");
      var apiBaseUrl = "http://geekyants-portal.local.geekydev.com/api/v1";

      RestangularProvider.setBaseUrl(apiBaseUrl);
      // RestangularProvider.setResponseExtractor(function(
      //   response,
      //   operation,
      //   what,
      //   url
      // ) {
      //   var newResponse;
      //   newResponse = response;
      //   console.log("RES ", response);
      //   if (angular.isArray(response)) {
      //     if (!response) return;
      //     for (var i = 0; i < response.length; i++) {
      //       response[i].originalData = angular.copy(response[i]);
      //     }
      //   } else {
      //     if (!response) return;
      //     response.originalData = angular.copy(response.result);
      //   }
      //   if (typeof response.meta != "undefined")
      //     newResponse.metaData = response.meta.pagination;
      //   else {
      //     newResponse.metaData = response.meta;
      //   }
      //   return newResponse;
      // });

      var env = {};
      if (window) {
        Object.assign(env, window.__env);
      }
      app.constant("__env", env);
      var apiBaseUrl = __env.apiUrl;
      RestangularProvider.setBaseUrl(apiBaseUrl);
      // RestangularProvider.setResponseExtractor(function(
      //   response,
      //   operation,
      //   what,
      //   url
      // ) {
      //   var newResponse;
      //   newResponse = response;
      //   console.log("setResponseExtractor ", response);

      //   if (angular.isArray(response)) {
      //     if (!response) return;
      //     for (var i = 0; i < response.length; i++) {
      //       response[i].originalData = angular.copy(response[i]);
      //     }
      //   } else {
      //     if (!response) return;
      //     response.originalData = angular.copy(response.result);
      //   }
      //   if (typeof response.meta != "undefined")
      //     newResponse.metaData = response.meta.pagination;
      //   else {
      //     newResponse.metaData = response.meta;
      //   }
      //   return newResponse;
      // });

      RestangularProvider.setErrorInterceptor(function(
        response,
        deferred,
        responseHandler
      ) {
        if (response.status === 498) {
          tokenException = true;
          return true;
        }
      });

      RestangularProvider.addResponseInterceptor(function(
        data,
        operation,
        what,
        url,
        response,
        deferred
      ) {
        var extractedData = {};
        extractedData = data.result;
        extractedData.metaData = data.meta ? data.meta : [];
        return extractedData;
      });
    }
  ])
  .constant("CONFIG", {
    BASEURL: "http://geekyants-portal.local.geekydev.com",
    DOWNLOADURL:
      "http://geekyants-portal.local.geekydev.com/api/v1/download?q=",
    DOWNLOADPDFURL:
      "http://geekyants-portal.local.geekydev.com/api/v1/download-pdf?q="
  })
  .filter("sumByRow", function() {
    return function(collection, column) {
      var total = 0;
      var tempTotal = "";
      var tempTotalArr = [];
      collection.forEach(function(item) {
        total += item.hours;
      });
      if (total.toString().includes(".")) {
        tempTotalArr = total.toString().split(".");
        tempTotal = tempTotalArr[0].toString().padStart(3, "0");
        total = tempTotal + "." + tempTotalArr[1].toString();
      } else {
        total = total.toString().padStart(3, "0");
        total = total + "." + "0";
      }
      return total;
    };
  })
  .filter("formatHours", function() {
    return function(collection, column) {
      var total = collection;
      var tempTotal = "";
      var tempTotalArr = [];
      if (total.toString().includes(".")) {
        tempTotalArr = total.toString().split(".");
        tempTotal = tempTotalArr[0].toString().padStart(2, "0");
        total = tempTotal + "." + tempTotalArr[1].toString();
      } else {
        total = total.toString().padStart(2, "0");
        total = total + "." + "0";
      }
      return total;
    };
  })
  .filter("sumByApprovedHours", function() {
    return function(collection, column) {
      var total = 0;
      var tempTotal = "";
      var tempTotalArr = [];
      collection.forEach(function(item) {
        if (item.approver_id != null) total += item.approved_hours;
      });
      if (total.toString().includes(".")) {
        tempTotalArr = total.toString().split(".");
        tempTotal = tempTotalArr[0].toString().padStart(3, "0");
        total = tempTotal + "." + tempTotalArr[1].toString();
      } else {
        total = total.toString().padStart(3, "0");
        total = total + "." + "0";
      }
      return total;
    };
  })
  .config([
    "$routeProvider",
    function($routeProvider) {
      $routeProvider
        .when("/step1", {
          templateUrl: "/views/payslip-csv/payroll-form.html",
          controller: "payslipStep1Ctrl"
        })
        .when("/step2", {
          templateUrl: "/views/payslip-csv/payroll-form.html",
          controller: "payslipStep2Ctrl"
        })
        .when("/step3", {
          templateUrl: "/views/payslip-csv/payroll-form.html",
          controller: "payslipStep3Ctrl"
        })
        .when("/step4", {
          templateUrl: "/views/payslip-csv/payroll-form.html",
          controller: "payslipStep4Ctrl"
        })
        .when("/step5", {
          templateUrl: "/views/payslip-csv/payroll-form.html",
          controller: "payslipStep5Ctrl"
        })
        .when("/step6", {
          templateUrl: "/views/payslip-csv/payroll-form.html",
          controller: "payslipStep6Ctrl"
        });
    }
  ])
  .config([
    "$locationProvider",
    function($locationProvider) {
      $locationProvider.hashPrefix("");
    }
  ]);
app.controller("myCtrl", function($scope, Restangular, $http) {
  $scope.id = document.getElementById("id").value;
  $http({
    method: "GET",
    url:
      "http://geekyants-portal.geekydev.com/admin/Api/get-content/" + $scope.id
  }).then(
    function successCallback(response) {
      $scope.that = $scope;
      $scope.contents = Object.keys(response.data).map(function(key) {
        return response.data[key];
      });
      for (var i = 0; i < $scope.contents.length; i++) {
        $scope[$scope.contents[i]] = $scope.contents[i];
      }
    },
    function errorCallback(response) {}
  );
});

angular.module("myApp").filter("dateToISO", function() {
  return function(input) {
    input = new Date(input).toISOString();
    var dateString = moment(input).format('YYYY-MM-DD');
    var dateStringWithTime = moment(input).format('Do, MMM Y h:mm A');
    return dateStringWithTime;
  };
});

angular.module("myApp").filter("dateFormat", function() {
  return function(input) {
    input = new Date(input).toISOString();
    var dateString = moment(input).format('YYYY-MM-DD');
    var dateStringWithTime = moment(input).format('Do, MMM Y');
    return dateStringWithTime;
  };
});

angular.module("myApp").filter('range', function() {
    return function(input, total) {
      total = parseInt(total);
        for (var i=0; i<total; i++) {
        input.push(i);
      }
      return input;
    };
  });
angular.module('myApp')
    .controller('timesheetCtrl',["$scope", "Restangular", "$uibModal",function ($scope, Restangular, $uibModal) {
        $scope.dates = JSON.parse(window.dates);
        $scope.projects = window.projects;
        $scope.verticalTotals = JSON.parse(window.verticalTotals);
        $scope.user = JSON.parse(window.user);
        $scope.currentDate = new Date().toISOString().slice(0, 10);
        $scope.addTime = function (name, result, user) {
            $scope.list = {
                project_name: name,
                data: result,
                user_name: user
            };
            var modal = $uibModal.open({
                controller: 'modalCtrl',
                windowClass: 'bootstrap_wrap',
                templateUrl: '/views/add_time.html',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                    data: function () {
                        return $scope.list;
                    }
                }

            });
            modal.result.then(function (res) { }, function () { });
        };
        var modal = $uibModal.open({
            controller: "modalCtrl",
            windowClass: "bootstrap_wrap",
            templateUrl: "/views/add_time.html",
            backdrop: "static",
            size: "md",
            resolve: {
                data: function () {
                    return $scope.list;
                }
            }
        });
        modal.result.then(function (res) { }, function () { });


    }]);
"use strict";

/*Directives*/
angular
  .module("myApp")
  .directive("compileThis", function($compile) {
    return {
      template: "<span></span>",
      restrict: "E",
      replace: true,
      link: function(scope, iElement, iAttrs) {
        iElement.html(iAttrs.value);
        $compile(iElement)(scope);
      }
    };
  })
  .directive("gridView", [
    "$parse",
    "CONFIG",
    function($parse, CONFIG) {
      return {
        templateUrl:
          "/scripts/directives/grid-view/grid-view.html?v=" +
          window.app_version,
        restrict: "E",
        replace: true,
        scope: {
          model: "=",
          _columns: "@columns",
          _actions: "@actions",
          _showSearch: "@showSearch",
          _showDate: "@showDate",
          _showStartDate: "@showStartDate",
          _showEndDate: "@showEndDate",
          _showDateRange: "@showDateRange",
          _showDownloadCsv: "@showDownloadCsv",
          _filters: "@filters",
          control: "=?",
          extraSearchFieldsData: "=?",
          gridData: "=?"
        },
        controller: function(
          $scope,
          Restangular,
          $location,
          $parse,
          $filter,
          $log
        ) {
          $scope.dateOptions = {
            "year-format": "'yy'",
            "starting-day": 1
          };
          $scope.current_date = new Date();
          $scope.stringToDate = function (dateString) {
            let date = new Date(dateString);
            return date;
          }

          $scope.openStartDateAction = function() {
            setTimeout(function() {
              $scope.$apply(function() {
                $scope.openStartDate = true;
              });
            }, 0);
          };
          $scope.openEndDateAction = function() {
            setTimeout(function() {
              $scope.$apply(function() {
                $scope.openEndDate = true;
              });
            }, 0);
          };

          $scope.openFromDateAction = function() {
            setTimeout(function() {
              $scope.$apply(function() {
                $scope.openFromDate = true;
              });
            }, 0);
          };

          $scope.openToDateAction = function() {
            setTimeout(function() {
              $scope.$apply(function() {
                $scope.openToDate = true;
              });
            }, 0);
          };

          $scope.exportCsvAction = function() {
            makeRequest(true);
          };
          $scope.resetDownloads = function() {
            $scope.pdfLocation = null;
            $scope.csvLocation = null;
            $scope.generatingPdf = false;
            $scope.exportingCsv = false;
            $scope.noRecords = false;
          };
          $scope.filteredDate = function (input_date) {
            var formatted_date = new Date(input_date);
            return formatted_date;
          };
          $scope.columns = $scope.$parent.$eval($scope._columns);
          $scope.actions = $scope.$parent.$eval($scope._actions);
          $scope.filters = $scope.$parent.$eval($scope._filters);

          if (typeof $scope._showSearch == "undefined") {
            $scope.showSearch = true;
          } else {
            $scope.showSearch = $scope.$parent.$eval($scope._showSearch);
          }

          if (typeof $scope._showDate == "undefined") {
            $scope.showDate = false;
          } else {
            $scope.showDate = $scope.$parent.$eval($scope._showDate);
          }
          if (typeof $scope._showStartDate == "undefined") {
            $scope.showStartDate = false;
          } else {
            $scope.showStartDate = $scope.$parent.$eval($scope._showStartDate);
          }
          if (typeof $scope._showEndDate == "undefined") {
            $scope.showEndDate = false;
          } else {
            $scope.showEndDate = $scope.$parent.$eval($scope._showEndDate);
          }

          if (typeof $scope._showDateRange == "undefined") {
            $scope.showDateRange = false;
          } else {
            $scope.showDateRange = $scope.$parent.$eval($scope._showDateRange);
          }

          if (typeof $scope._showDownloadCsv == "undefined") {
            $scope.showDownloadCsv = false;
          } else {
            $scope.showDownloadCsv = $scope.$parent.$eval(
              $scope._showDownloadCsv
            );
          }

          $scope.unexpected = false;
          $scope.fetching = false;

          $scope.pagination = {};
          $scope.requestParams = {
            pageNumber: 1,
            q: "",
            perPage: 100,
            date: "",
            startDate: "",
            endDate: "",
            startTime: new Date(0, 0, 0, 0, 0),
            endTime: new Date(0, 0, 0, 0, 0)
          };
          $scope.pdfLocation = null;
          $scope.generatingPdf = false;
          $scope.noRecords = false;

          var makeRequest = _.debounce(function(exportToCsv) {
            $scope.csvLocation = null;

            var pageNumber = $scope.requestParams.pageNumber;

            var baseUrl = $scope.model;
            var sendParams = {};

            if ($scope.data && $scope.data.metaData)
              sendParams["page"] = $scope.requestParams.pageNumber;

            if ($scope.requestParams.q)
              sendParams["q"] = $scope.requestParams.q;

            if ($scope.requestParams.startDate) {
              var d = $scope.requestParams.startDate;
              sendParams["startDate"] =
                d.getFullYear() +
                "-" +
                ("00" + (d.getMonth() + 1)).slice(-2) +
                "-" +
                ("00" + d.getDate()).slice(-2);
              if ($scope.requestParams.startTime) {
                var t = $scope.requestParams.startTime;
                sendParams["startDate"] =
                  sendParams["startDate"] +
                  " " +
                  ("00" + t.getHours()).slice(-2) +
                  ":" +
                  ("00" + t.getMinutes()).slice(-2) +
                  ":00";
              } else {
                sendParams["startDate"] = sendParams["startDate"] + " 00:00:00";
              }
            }
            if ($scope.requestParams.endDate) {
              var d = $scope.requestParams.endDate;
              sendParams["endDate"] =
                d.getFullYear() +
                "-" +
                ("00" + (d.getMonth() + 1)).slice(-2) +
                "-" +
                ("00" + d.getDate()).slice(-2);
              if ($scope.requestParams.endTime) {
                var t = $scope.requestParams.endTime;
                sendParams["endDate"] =
                  sendParams["endDate"] +
                  " " +
                  ("00" + t.getHours()).slice(-2) +
                  ":" +
                  ("00" + t.getMinutes()).slice(-2) +
                  ":00";
              } else {
                sendParams["endDate"] = sendParams["endDate"] + " 00:00:00";
              }
            }

            if ($scope.showDateRange) {
              if ($scope.requestParams.fromDate) {
                var fd = $scope.requestParams.fromDate;
                sendParams["from_date"] =
                  fd.getFullYear() +
                  "-" +
                  ("00" + (fd.getMonth() + 1)).slice(-2) +
                  "-" +
                  ("00" + fd.getDate()).slice(-2);
              }

              if ($scope.requestParams.toDate) {
                var td = $scope.requestParams.toDate;

                sendParams["to_date"] =
                  td.getFullYear() +
                  "-" +
                  ("00" + (td.getMonth() + 1)).slice(-2) +
                  "-" +
                  ("00" + td.getDate()).slice(-2);
              }
            }

            if ($scope.showDate) {
              if ($scope.requestParams.date) {
                var td = $scope.requestParams.date;

                sendParams["on_date"] =
                  td.getFullYear() +
                  "-" +
                  ("00" + (td.getMonth() + 1)).slice(-2) +
                  "-" +
                  ("00" + td.getDate()).slice(-2);
              }
            }

            if ($scope.requestParams.perPage)
              sendParams["per_page"] = $scope.requestParams.perPage;

            angular.forEach($scope.extraSearchFieldsData, function(val) {
              sendParams[val.paramName] = $scope.requestParams[val.paramName];
            });

            sendParams["pagination"] = 1;

            if (exportToCsv) sendParams["export_to_csv"] = true;

            sendParams = $.extend(true, sendParams, $scope.filters);

            if (exportToCsv) {
              $scope.noRecords = false;
              $scope.exportingCsv = true;
              Restangular.all(baseUrl)
                .customGET("", sendParams)
                .finally(function() {
                  $scope.exportingCsv = false;
                })
                .then(
                  function(success) {
                    //$scope.csvLocation = "http://cosmoparking.local.geekydev.com/download?q="/*;Config.BASEURL*/+success.location;
                    $scope.csvLocation = CONFIG.DOWNLOADURL + success.location;
                  },
                  function(error) {
                    $scope.noRecords = true;
                    // ErrorHandler(error);
                  }
                );
            } else {
              $scope.loading = true;

              Restangular.all(baseUrl)
                .getList(sendParams)
                .finally(function() {
                  $scope.loading = false;
                })
                .then(
                  function(success) {
                    $scope.gridData = success;

                    $scope.qSent = sendParams["filter"];

                    $scope.data = success;
                    $scope.pagination.from =
                      (pageNumber - 1) * $scope.data.metaData.per_page + 1;

                    if (
                      pageNumber * $scope.data.metaData.per_page <
                      $scope.data.metaData.total
                    ) {
                      $scope.pagination.to =
                        pageNumber * $scope.data.metaData.per_page;
                    } else {
                      $scope.pagination.to = $scope.data.metaData.total;
                    }
                    angular.forEach($scope.data, function(o) {
                      // if(o.start_date) {
                      //  o.start_date = new Date(o.start_date * 1000);
                      //  o.start_date = $scope.getFormatedDate(o.start_date);
                      // }
                      // if(o.end_date) {
                      //  o.end_date = new Date(o.end_date * 1000);
                      //  o.end_date = $scope.getFormatedDate(o.end_date);
                      // }
                      if (o.status == 1) o.status = true;
                      if (o.enabled == 1) o.enabled = true;
                      if (o.is_available == 1) o.is_available = true;
                    });
                  },
                  function(error) {
                    $log.error("Oops", "Some error occured, please try again");
                  }
                );
            }
          }, 50);

          $scope.control = {
            remove: function(id) {
              var index = _.findIndex($scope.data, function(row) {
                return row.id == id;
              });

              if (index !== -1) $scope.data.splice(index, 1);
            }
          };

          $scope.changePage = function() {
            $scope.requestParams.pageNumber = $scope.data.metaData.current_page;
            makeRequest();
          };

          $scope.doSearch = function() {
            $scope.requestParams.pageNumber = 1;
            makeRequest();
          };

          $scope.$watch("requestParams.startDate", function(newVal, oldVal) {
            makeRequest();
          });
          $scope.$watch("requestParams.endDate", function(newVal, oldVal) {
            makeRequest();
          });
          $scope.$watch("requestParams.startTime", function(newVal, oldVal) {
            makeRequest();
          });
          $scope.$watch("requestParams.endTime", function(newVal, oldVal) {
            makeRequest();
          });

          $scope.$watch("requestParams.fromDate", function(newVal, oldVal) {
            makeRequest();
          });

          $scope.$watch("requestParams.toDate", function(newVal, oldVal) {
            makeRequest();
          });

          $scope.perPageChange = function() {
            $scope.requestParams.pageNumber = 1;
            makeRequest();
          };

          $scope.hasAction = function(action) {
            var hasIt = false;
            angular.forEach($scope.actions, function(val, key) {
              if (val.type == action) hasIt = true;
            });

            return hasIt;
          };

          function commonAction(action, row, that) {
            var actionObj = _.find($scope.actions, { type: action });

            if (!actionObj || (!actionObj.url && !actionObj.action)) {
              var l = $location.path();

              if (/list$/.test(l)) l = l.substr(0, l.length - 4);

              if (l.substr(-1) != "/") l += "/";

              $location.path(l + action + "/" + row.id);
            } else {
              if (actionObj.url) $location.path(that.$eval(actionObj.url));

              if (actionObj.action) {
                var fn = $parse(actionObj.action);
                fn($scope.$parent, { row: row });
              }
            }
          }

          $scope.addNew = function(row) {
            var actionObj = _.find($scope.actions, { type: "addNew" });
          };

          $scope.viewAction = function(row) {
            commonAction("view", row, this);
          };

          $scope.refreshAction = function(row) {
            commonAction("refresh", row, this);
          };

          $scope.downAction = function(row) {
            commonAction("down", row, this);
          };

          $scope.editAction = function(row) {
            commonAction("edit", row, this);
          };
          $scope.getFormatedDate = function(date) {
            var monthNames = [
              "January",
              "February",
              "March",
              "April",
              "May",
              "June",
              "July",
              "August",
              "September",
              "October",
              "November",
              "December"
            ];

            var d = new Date();

            var newDate = new Date(date);
            newDate.setHours(newDate.getHours());
            newDate.setMinutes(newDate.getMinutes());
            var day = ("0" + newDate.getDate()).slice(-2);
            var month = newDate.getMonth();
            var hours = newDate.getHours();
            var minutes = newDate.getMinutes();
            var meridiem = hours >= 12 ? "PM" : "AM";
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? "0" + minutes : minutes;
            newDate =
              monthNames[month] +
              " " +
              day +
              ", " +
              newDate.getFullYear() +
              " - " +
              hours +
              ":" +
              minutes +
              " " +
              meridiem;
            return newDate;
          };
          $scope.deleteAction = function(row) {
            var actionObj = _.find($scope.actions, { type: "delete" });

            if (!actionObj || (!actionObj.url && !actionObj.action)) {
              var sure = confirm("Do you really want to delete this" + "?");
              if (sure) {
                row
                  .remove()
                  .finally(function() {})
                  .then(
                    function() {
                      var index = $scope.data.indexOf(row);
                      $scope.data.splice(index, 1);

                      var msg = "Item with ID" + " " + row.id + " " + "deleted";
                      $log.info(msg);
                    },
                    function() {
                      var msg =
                        "Item with ID" +
                        " " +
                        row.id +
                        " " +
                        "could not be deleted";
                      $log.error(msg);
                    }
                  );
              }
            } else {
              commonAction("delete", row, this);
            }
          };

          $scope.starAction = function(row) {
            commonAction("star", row, this);
          };

          makeRequest();
          $scope.downloadPdf = function() {
            $scope.noRecords = false;
            $scope.generatingPdf = true;
            var startTime = null;
            var endTime = null;
            var href = window.location.href.split("?")[0];
            var day = href.substring(href.lastIndexOf("/") + 1);
            if (day == "today") {
              var d = new Date();
              startTime =
                d.getFullYear() +
                "-" +
                ("00" + (d.getMonth() + 1)).slice(-2) +
                "-" +
                ("00" + d.getDate()).slice(-2) +
                " 00:00:00";
              endTime =
                d.getFullYear() +
                "-" +
                ("00" + (d.getMonth() + 1)).slice(-2) +
                "-" +
                ("00" + d.getDate()).slice(-2) +
                " 23:59:59";
            } else {
              if (day == "tomorrow") {
                var d = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
                startTime =
                  d.getFullYear() +
                  "-" +
                  ("00" + (d.getMonth() + 1)).slice(-2) +
                  "-" +
                  ("00" + d.getDate()).slice(-2) +
                  " 00:00:00";
                endTime =
                  d.getFullYear() +
                  "-" +
                  ("00" + (d.getMonth() + 1)).slice(-2) +
                  "-" +
                  ("00" + d.getDate()).slice(-2) +
                  " 23:59:59";
              } else {
                if ($scope.requestParams.startDate) {
                  var d = $scope.requestParams.startDate;
                  startTime =
                    d.getFullYear() +
                    "-" +
                    ("00" + (d.getMonth() + 1)).slice(-2) +
                    "-" +
                    ("00" + d.getDate()).slice(-2);
                  if ($scope.requestParams.startTime) {
                    var t = $scope.requestParams.startTime;
                    startTime =
                      startTime +
                      " " +
                      ("00" + t.getHours()).slice(-2) +
                      ":" +
                      ("00" + t.getMinutes()).slice(-2) +
                      ":00";
                  } else {
                    startTime = startTime + " 00:00:00";
                  }
                }
                if ($scope.requestParams.endDate) {
                  var d = $scope.requestParams.endDate;
                  endTime =
                    d.getFullYear() +
                    "-" +
                    ("00" + (d.getMonth() + 1)).slice(-2) +
                    "-" +
                    ("00" + d.getDate()).slice(-2);
                  if ($scope.requestParams.endTime) {
                    var t = $scope.requestParams.endTime;
                    endTime =
                      endTime +
                      " " +
                      ("00" + t.getHours()).slice(-2) +
                      ":" +
                      ("00" + t.getMinutes()).slice(-2) +
                      ":00";
                  } else {
                    endTime = endTime + " 00:00:00";
                  }
                }
              }
            }
            var params = { startTime: startTime, endTime: endTime };
            Restangular.all("")
              .customGET("download-records", params)
              .finally(function() {
                $scope.loading = false;
                $scope.generatingPdf = false;
              })
              .then(
                function(success) {
                  $scope.pdfLocation = CONFIG.DOWNLOADPDFURL + success;
                },
                function(error) {
                  $scope.noRecords = true;
                  // $log.error("Oops", "Some error occured, please try again");
                }
              );
          };
        },
        link: function(scope, iElement, iAttrs) {
          scope.openModal = function(row) {
            var fn = $parse(iAttrs.openModal);
            fn(scope.$parent, { row: row });
          };

          scope.switchOnoff = function(row) {
            var fn = $parse(iAttrs.switchOnoff);
            fn(scope.$parent, { row: row });
          };
        }
      };
    }
  ]);

"use strict";

/*Directives*/
angular
    .module("myApp")
    .directive('starRating', function () {
         return {
        scope: {
            rating: '=',
            maxRating: '@',
            readOnly: '@',
            click: "&",
            mouseHover: "&",
            mouseLeave: "&"
        },
        restrict: 'EA',
        template:
            "<div style='display: inline-block; margin: 0px; padding: 0px; cursor:pointer;' ng-repeat='idx in maxRatings track by $index'> \
                    <img ng-src='{{((hoverValue + _rating) <= $index) && \"http://www.codeproject.com/script/ratings/images/star-empty-lg.png\" || \"http://www.codeproject.com/script/ratings/images/star-fill-lg.png\"}}' \
                    ng-Click='isolatedClick($index + 1)' \
                    ng-mouseenter='isolatedMouseHover($index + 1)' \
                    ng-mouseleave='isolatedMouseLeave($index + 1)'></img> \
            </div>",
        compile:["element", "attrs", function (element, attrs) {
            if (!attrs.maxRating || (Number(attrs.maxRating) <= 0)) {
                attrs.maxRating = '5';
            };
        }],
        controller:["$scope", "$element", "$attrs", function ($scope, $element, $attrs) {
            $scope.maxRatings = [];

            for (var i = 1; i <= $scope.maxRating; i++) {
                $scope.maxRatings.push({});
            };

            $scope._rating = $scope.rating;
            
            $scope.isolatedClick = function (param) {
                if ($scope.readOnly == 'true') return;

                $scope.rating = $scope._rating = param;
                $scope.hoverValue = 0;
                $scope.click({
                    param: param
                });
            };

            $scope.isolatedMouseHover = function (param) {
                if ($scope.readOnly == 'true') return;

                $scope._rating = 0;
                $scope.hoverValue = param;
                $scope.mouseHover({
                    param: param
                });
            };

            $scope.isolatedMouseLeave = function (param) {
                if ($scope.readOnly == 'true') return;

                $scope._rating = $scope.rating;
                $scope.hoverValue = 0;
                $scope.mouseLeave({
                    param: param
                });
            };
        }]
    };
    });
"use strict()";

/*Directives*/
angular.module("myApp").directive("commentData", [
  function() {
    return {
      templateUrl: "/scripts/directives/comment/comment.html?v=2",
      restrict: "E",
      replace: true,
      scope: {
        commentableId: "=",
        commentableType: "@commentableType",
        isDisabled: "@isDisabled",
        isPrivatable: "@isPrivatable"
      },
      controller: [
        "$scope",
        "Restangular",
        "$rootScope",
        function($scope, Restangular, $rootScope) {
          $scope.loadMoreFlag = false;
          $scope.commentCount = 0;
          $scope.noCommentsToLoad = false;
          $scope.showList = true;
          $scope.addLoading = false;
          $scope.listLoading = false;
          $scope.commentObj = {
            comments: [],
            page: 1,
            per_page: 5,
            loaded: false,
            newComment: "",
            newCommentAdded: false,
            isPrivate: false
          };

          $scope.loadComments = function() {
            $scope.listLoading = true;
            var params = {
              commentable_id: $scope.commentableId,
              commentable_type: $scope.commentableType,
              page: $scope.commentObj.page,
              per_page: $scope.commentObj.per_page
            };
            Restangular.all("comment")
              .getList(params)
              .then(function(response) {
                $scope.listLoading = false;
                if (response.length > 0) {
                  $scope.commentCount = response.length;
                  for (i = 0; i < response.length; i++) {
                    $scope.commentObj.comments.push(response[i]);
                  }
                } else {
                  $scope.noCommentsToLoad = true;
                }
              });
          };

          $scope.loadComments();
          $scope.loadMoreComments = function() {
            $scope.loadMoreFlag = true;
            $scope.commentObj.page = $scope.commentObj.page + 1;
            $scope.loadComments();
          };
          $scope.checkboxClicked = function(data) {
            // console.log('data', data);
          };
          $scope.postComment = function() {
            if (
              $scope.commentObj.newComment == null ||
              $scope.commentObj.newComment == ""
            ) {
              return;
            }
            $scope.addLoading = true;
            var params = {
              commentable_id: $scope.commentableId,
              commentable_type: $scope.commentableType,
              is_private: $scope.commentObj.isPrivate,
              message: $scope.commentObj.newComment
            };

            Restangular.all("comments")
              .post(params)
              .then(
                function(response) {
                  $scope.commentCount++;
                  $scope.addLoading = false;
                  $scope.commentObj.newComment = "";
                  $scope.commentObj.isPrivate = !$scope.commentObj.isPrivate;
                  $scope.showList = true;
                  $scope.commentObj.comments.unshift(response.comment);
                  $rootScope.$broadcast('activity-log');
                },
                function(error) {
                  $scope.addLoading = false;
                }
              );
          };

          $scope.toggleCommentList = function() {
            $scope.showList = !$scope.showList;
            $scope.noCommentsToLoad = false;
          };

          $scope.filteredDate = function(input_date) {
            var formatted_date = new Date(input_date);
            return formatted_date;
          };
        }
      ]
    };
  }
]);

"use strict()";

/*Directives*/
angular
    .module("myApp")
    .directive("requiredResource", [
        function () {
            return {
                templateUrl: "/scripts/directives/required-resource/index.html?v=1",
                restrict: "E",
                replace: true,
                scope: {
                    projectId: "=",
                    isDisabled: "@isDisabled",
                },
                controller: ["$scope", "Restangular", function (
                    $scope,
                    Restangular
                ) {
                    $scope.newRequiredResources = [];
                    $scope.oldResources = [];
                    $scope.editable = [];
                    $scope.error = false;
                    $scope.resourceError = false;
                    $scope.priceError = false;
                    $scope.newResouceObj = {
                        'type' : '',
                        'price' : '',
                    };
                    
                    $scope.loadRequiredResources = function () {
                        $scope.oldResources = [];
                        $scope.listLoading = true;
                        Restangular.all("resource-price")
                            .customGET("", {
                                "project_id": $scope.projectId,
                            })
                            .then(function (response) {
                                $scope.listLoading = false;
                                for (i = 0; i < response.length; i++) {
                                    $scope.oldResources.push(response[i]);
                                }
                            },function (error) {
                                console.log(error);
                            }
                        );
                    };
                    $scope.loadRequiredResources();
                    
                    $scope.addResourceRow = function() {
                        $scope.error = false;
                        if ( !$scope.newResouceObj.type )
                        {
                            $scope.resourceError = true;
                            $scope.error = true;
                        }   else {
                            $scope.resourceError = false;
                        }
                        
                        if ( !$scope.newResouceObj.price )
                        {
                            $scope.priceError = true;
                            $scope.error = true;
                        }   else{
                            $scope.priceError = false;
                        }
                        
                        if ( $scope.error )
                        {
                            return;
                        }

                        var params = {
                            'project_id' : $scope.projectId,
                            'type' : $scope.newResouceObj.type,
                            'price' : $scope.newResouceObj.price,
                        };
                        Restangular.all("resource-price")
                            .post(params)
                            .then(function (response) {
                                $scope.newResouceObj = {
                                    'type' : '',
                                    'price' : '',
                                };
                                $scope.loadRequiredResources();
                            }, function (error) {
                                console.log(error);

                            });
                    };
                    $scope.editResourceRow = function(index) {
                        $scope.editable[index] = true;
                    }
                    $scope.saveResourceRow = function(index, resourceRow){
                        $scope.editable[index] = true;
                        Restangular.one("resource-price", resourceRow.id)
                                .customPUT({
                                    resourceRow: resourceRow
                                })
                                .then(
                                    function (response) {
                                        $scope.editable[index] = false;
                                    },
                                    function (errors) {
                                        console.log(errors);
                                    }
                                );
                    };

                    $scope.removeResourceRow = function(index, resourceRow) {
                        var r = confirm("Are you sure you want to delete this entry?");
                        if (r != true) {
                            return;
                        }
                        Restangular.one("resource-price", resourceRow.id)
                            .remove()
                            .then(
                                function(response) {
                                    $scope.oldResources.splice(index, 1);
                                },
                                function(error) {
                                    console.log(error);
                                }
                            );
                    };
                }]
            };
        }
    ]);
"use strict()";

/*Directives*/
angular
    .module("myApp")
    .directive("addProjectUser", [
        function () {
            return {
                
                templateUrl: "/scripts/directives/add-project-user/add-project-user.html?v=1",
                restrict: "E",
                replace: true,
                scope: {
                    projectId: "=",
                    isDisabled: "@isDisabled",
                },
                
                controller: ["$scope", "Restangular", function (
                    $scope,
                    Restangular
                ) {
                    $scope.existingResources = [];
                    $scope.allTypes = [];
                    $scope.allUsers = [];
                    $scope.editable = [];
                    $scope.selected_user = '';
                    $scope.selected_type = '';
                    $scope.error = false;
                    $scope.userError = false;
                    $scope.startDateError = false;
                    $scope.edit_start_date = [];
                    $scope.edit_end_date = [];
                    $scope.today = new Date();
                    $scope.start_date = null;
                    $scope.end_date = null;
                    
                    $scope.getAllUsers = function () {
                        $scope.allUsers = [];
                        $scope.listLoading = true;
                        Restangular.all("user-all-active")
                            .customGET("", {})
                            .then(function (response) {
                                $scope.allUsers = response;
                            },function (error) {
                                console.log(error);
                            }
                        );
                    };
                    $scope.getAllTypes = function () {
                        $scope.allTypes = [];
                        $scope.listLoading = true;
                        Restangular.all("resource-price")
                            .customGET("", {
                                "project_id": $scope.projectId,
                            })
                            .then(function (response) {
                                $scope.listLoading = false;
                                for (i = 0; i < response.length; i++) {
                                    $scope.allTypes.push(response[i]);
                                }
                            },function (error) {
                                console.log(error);
                            }
                        );
                    };
                    $scope.getExistingResources = function () {
                        $scope.existingResources = [];
                        $scope.listLoading = true;
                        Restangular.all("project-resource")
                            .customGET("", {
                                "project_id": $scope.projectId,
                            })
                            .then(function (response) {
                                $scope.listLoading = false;
                                for (i = 0; i < response.length; i++) {
                                    $scope.edit_start_date[i] = new Date(response[i].start_date);
                                    $scope.edit_end_date[i] = (response[i].end_date) ? new Date(response[i].end_date) : null;
                                    $scope.existingResources.push(response[i]);
                                    
                                }
                            },function (error) {
                                console.log(error);
                            }
                        );
                    };
                    $scope.getAllTypes();
                    $scope.getAllUsers();
                    $scope.getExistingResources();

                    $scope.select_user = function (data) {
                        $scope.selected_user = data;
                        $scope.selected_user_error = false;
                    };

                    $scope.select_type = function (data) {
                        $scope.selected_type = data;
                        $scope.selected_type_error = false;
                    };

                    $scope.checkEndDateCondition = function (input_date) {
                        if ( !input_date ) {
                            return true;
                        }
                        else{
                            if ( $scope.today >= input_date )
                            {
                                return false;
                            } else {
                                return true;
                            }
                        }

                    };

                    $scope.addUser = function() {
                        $scope.error = false;
                        if ( $scope.selected_user == '' )
                        {
                            $scope.selected_user_error = true;
                            $scope.error = true;
                        }   else {
                            $scope.selected_user_error = false;
                        }
                        
                        if ( $scope.selected_type == '' )
                        {
                            $scope.selected_type_error = true;
                            $scope.error = true;
                        }   else{
                            $scope.selected_type_error = false;
                        }
                        if ( $scope.start_date == null )
                        {
                            $scope.selected_start_date_error = true;
                            $scope.error = true;
                        }   else{
                            $scope.selected_start_date_error = false;
                        }

                        if ( $scope.error )
                        { 
                            return;
                        }
                        var endDate = '';
                        if ( $scope.end_date )
                        {
                            endDate = moment($scope.end_date).format("YYYY-MM-DD");
                        }
                        var startDate = moment($scope.start_date).format("YYYY-MM-DD");
                        var params = {
                            'project_id' : $scope.projectId,
                            'user_id' : $scope.selected_user.id,
                            'resource_price_id' : $scope.selected_type.id,
                            'start_date' : startDate,
                            'end_date' : endDate
                        };
                        
                        Restangular.all("project-resource")
                            .post(params)
                            .then(function (response) {
                                $scope.selected_user = '';
                                $scope.selected_type = '';
                                $scope.start_date = null;
                                $scope.end_date = null;
                                $scope.getExistingResources();
                            }, function (error) {
                                console.log(error);

                            });
                    };
                    $scope.editResourceRow = function(index) {
                        $scope.editable[index] = true;
                    }
                    $scope.saveResourceRow = function(index, resourceRow){
                        var params = {
                            'user_id' : resourceRow.user_id,
                            'resource_price_id' : resourceRow.resource_price_id,
                            'start_date' : moment($scope.edit_start_date[index]).format("YYYY-MM-DD"),
                            'end_date' : $scope.edit_end_date[index] ? moment($scope.edit_end_date[index]).format("YYYY-MM-DD") : null ,
                        };
                        Restangular.one("project-resource", resourceRow.id)
                                .customPUT({
                                    params: params
                                })
                                .then(
                                    function (response) {
                                        $scope.editable[index] = false;
                                        $scope.getExistingResources();
                                    },
                                    function (errors) {
                                        console.log(errors);
                                    }
                                );
                    };

                    $scope.removeResourceRow = function(index, resourceRow) {
                        var r = confirm("Are you sure you want to delete this entry?");
                        if (r != true) {
                            return;
                        }
                        Restangular.one("project-resource", resourceRow.id)
                            .remove()
                            .then(
                                function(response) {
                                    $scope.existingResources.splice(index, 1);
                                },
                                function(error) {
                                    console.log(error);
                                }
                            );
                    };

                    $scope.releaseResourceRow = function(index, resourceRow) {
                        var r = confirm("Are you sure you want to release this user from the project?");
                        if (r != true) {
                            return;
                        }
                        Restangular.one("project-resource/release", resourceRow.id)
                            .customPUT()
                            .then(
                                function(response) {
                                    $scope.existingResources = [];
                                    $scope.getExistingResources();
                                },
                                function(error) {
                                    console.log(error);
                                }
                            );
                    };
                }]
            };
           
        }
       
    ]
);


    

"use strict()";

/*Directives*/
angular
    .module("myApp")
    .directive("resourceInfo", [
        function () {
            return {
                templateUrl: "/scripts/directives/resource-info/resource-info.html?v=1",
                restrict: "E",
                replace: true,
                scope: {
                    projectId: "=",
                    isDisabled: "@isDisabled",
                },
                controller: ["$scope","Restangular" ,function (
                    $scope,
                    Restangular
                ) {
                    $scope.projectDetails = [];
                    
                    
                    $scope.getProjectDetails = function () {
                        $scope.listLoading = true;
                        Restangular.all("project-resource-info")
                            .customGET("", {
                                "project_id": $scope.projectId,
                            })
                            .then(function (response) {
                                $scope.projectDetails = response.data;
                            },function (error) {
                                console.log(error);
                            }
                        );
                    };
                    $scope.getProjectDetails();                    
                }]
            };
        }
    ]);
"use strict()";

/*Directives*/
angular
    .module("myApp")
    .directive("projectTechnology", [
        function () {
            return {
                templateUrl: "/scripts/directives/project-technology/project-technology.html?v=1",
                restrict: "E",
                replace: true,
                scope: {
                    projectId: "=",
                    isDisabled: "@isDisabled",
                },
                controller: ["$scope",
                    "Restangular",function (
                    $scope,
                    Restangular
                ) {
                    $scope.allTechnologies = [];
                    $scope.projectTechnologies = [];
                    // get project-technology
                    $scope.getProjectTechnologies = function () {
                        $scope.listLoading = true;
                        Restangular.all("project-technology")
                            .customGET("", {
                                "project_id": $scope.projectId,
                            })
                            .then(function (response) {
                                $scope.projectTechnologies = response.data.technologies;
                            },function (error) {
                                console.log(error);
                            }
                        );
                    };
                    // All technologies
                    $scope.getAllTechnologies = function () {
                        $scope.listLoading = true;
                        Restangular.all("technology")
                            .customGET("", {})
                            .then(function (response) {
                                $scope.allTechnologies = response;
                            },function (error) {
                                console.log(error);
                            }
                        );
                    };
                    $scope.getProjectTechnologies();  
                    $scope.getAllTechnologies(); 

                    $scope.select_project_technologies = function (data) {
                        $scope.projectTechnologies = data;
                        $scope.selected_slack_public_channels_error = false;
                        var params = {
                            'project_id' : $scope.projectId,
                            'technologies' : $scope.projectTechnologies,
                        };
                        
                        Restangular.all("project-technology")
                            .post(params)
                            .then(function (response) {
                            }, function (error) {
                                console.log(error);
                            });
                    };
                }]
            };
        }
    ]);
"use strict()";

/*Directives*/
angular
    .module("myApp")
    .directive("addCronReminder", [
        function () {
            return {
                templateUrl: "/scripts/directives/add-cron-reminder/add-cron-reminder.html?v=1",
                restrict: "E",
                replace: true,
                scope: {
                    referenceId: "=",
                    isDisabled: "@isDisabled",
                    referenceType: "@referenceType",
                    jobHandler: "@jobHandler",
                },
                controller: function (
                    $scope,
                    Restangular,
                    $uibModal
                ) {
                    $scope.billingScheduleDetails = [];
                    $scope.getBillingScheduleDetails = function () {
                        $scope.listLoading = true;
                        Restangular.all("cron")
                            .customGET("", {
                                "reference_id": $scope.referenceId,
                                "reference_type": $scope.referenceType,
                                "job_handler": $scope.jobHandler,
                            })
                            .then(function (response) {
                                $scope.billingScheduleDetails = response;
                            },function (error) {
                                console.log(error);
                            }
                        );
                    };
                    $scope.getBillingScheduleDetails();
                    
                    $scope.edit = function () {
                        $scope.list = {
                            reference_id: $scope.referenceId,
                            reference_type: $scope.referenceType,
                            job_handler: $scope.jobHandler,
                            cron_details : $scope.billingScheduleDetails,
                        };
                        var modal = $uibModal.open({
                            controller: "addCronModalCtrl",
                            windowClass: "bootstrap_wrap",
                            templateUrl: "/views/cron/modal.html",
                            backdrop: "static",
                            size: "md",
                            resolve: {
                                data: function () {
                                    return $scope.list;
                                }
                            }
                        });
                        modal.result.then(function (res) { $scope.getBillingScheduleDetails(); }, function (err) { $scope.getBillingScheduleDetails(); });
                    };
                }
            };
        }
    ]);
"use strict()";

/*Directives*/
angular.module("myApp").directive("activityLog", [
  function() {
    return {
      templateUrl: "/scripts/directives/activity-log/index.html",
      restrict: "E",
      replace: true,
      scope: {
        referenceId: "=",
        referenceType: "@referenceType"
      },
      controller: [
        "$scope",
        "Restangular",
        "$rootScope",
        function($scope, Restangular, $rootScope) {
          var api = "activity-log";

          $scope.form = {
            loading: false,
            error: false,
            message: null,
            activities: [],
            model: {
              reference_id: $scope.referenceId,
              reference_type: $scope.referenceType,
            },
            init : function() {
              var params = {
                'reference_id' : $scope.referenceId,
                'reference_type' : $scope.referenceType,
                'pagination' : 0
              };
              Restangular.all('activity-log').getList(params).then(function(response){
                console.log('response', response);
                $scope.form.activities = response;
                angular.forEach($scope.form.activities, function(activitiy) {
                  activitiy.action_details = JSON.parse(activitiy.action_details);
                })

              }, function(error){
                console.log('error', error);
              });
            }
          }
          $scope.form.init();   
          $rootScope.$on('activity-log', function(event, data){
            console.log('broadcast activity-log');
            $scope.form.init();
          });     
        }
      ]
    };
  }
]);

angular.module('myApp')
    .controller('modalCtrl',["$scope", "Restangular", "data", "$uibModalInstance",function ($scope, Restangular, data, $uibModalInstance) {
        $scope.error = false;
        $scope.errorMessage = 'Please enter valid input!';
        $scope.items = data;
        data.data.timesheet_details = data.data.timesheet_details ? data.data.timesheet_details : [];
        $scope.existingItems = data.data.timesheet_details;
        if (data.data.timesheet_details.length == 0) {
            $scope.existingItems.push({ duration: null, reason: null });
        }
        $scope.finalsubmit = false;
        $scope.form = {
            duration: null,
            reason: null
        };
        $scope.addDateRange = function (data) {
            if (!data) {
                $scope.existingItems.push({ duration: null, reason: null });
                $scope.form.duration = null;
                $scope.form.reason = null;
            }
        };
        $scope.changed = function () {
            $scope.error = false;
        };
        $scope.removeDateRange = function (index, data) {
            if (!data) {
                $scope.error = false;
                $scope.existingItems.splice(index, 1);
            }
        };
        $scope.saveData = function (data) {
            $scope.existingItems.forEach(function (item) {
                if (item.duration === null || item.duration === undefined || item.reason === null) {
                    $scope.error = true;
                    item.error = true;
                }
            });
            if ($scope.error) return true;
            $scope.finalsubmit = true;
            var params = {
                date: data.date,
                project_id: data.project_id,
                timesheet_details: $scope.existingItems
            };
            Restangular.all('timesheet').post(params).then(
                function (response) {
                    $scope.finalsubmit = false;
                    if (response.status) {
                        $uibModalInstance.close(data);
                        window.location.reload();
                    }
                },
                function (errors) {
                    $scope.finalsubmit = false;
                    $scope.error = true;
                    $scope.errorMessage = errors.data.message.message;
                }
            );
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss();
            window.location.reload();
        };
    }]);
angular.module('myApp')
    .controller("admintimesheetCtrl", ["$scope", "Restangular", "$uibModal", function ($scope, Restangular, $uibModal) {
        $scope.dates = JSON.parse(window.dates);
        $scope.projects = window.projects;
        $scope.verticalTotals = JSON.parse(window.verticalTotals);
        $scope.user = JSON.parse(window.user);
        $scope.currentDate = new Date().toISOString().slice(0, 10);

        $scope.addTime = function (name, result) {
            $scope.list = {
                project_name: name,
                data: result
            };

            var modal = $uibModal.open({
                controller: "modalCtrl",
                windowClass: "bootstrap_wrap",
                templateUrl: "/views/add_time.html",
                backdrop: "static",
                size: "md",
                resolve: {
                    data: function () {
                        return $scope.list;
                    }
                }
            });
            modal.result.then(function (res) { }, function () { });
        };
        $scope.addTimeAprrover = function (name, result, user) {
            $scope.list = {
                project_name: name,
                approved_hours: result.approved_hours,
                data: result,
                user_name: user
            };
            var modal = $uibModal.open({
                controller: "adminModalCtrl",
                windowClass: "bootstrap_wrap",
                templateUrl: "/views/approve_time.html",
                backdrop: "static",
                size: "md",
                resolve: {
                    data: function () {
                        return $scope.list;
                    }
                }
            });
            modal.result.then(function (res) { }, function () { });
        };
        $scope.updateApproveList = function (data) {
            var params = {
                timesheet_id: data.timesheet_id,
                approved_hours: data.approved_hours
            };
            Restangular.all("admin/timesheet")
                .post(params)
                .then(function (response) { }, function (error) { });
        };
    }]);
angular.module("myApp").controller("adminModalCtrl", [
  "$scope",
  "Restangular",
  "data",
  "$uibInstance",
  function($scope, Restangular, data, $uibModalInstance) {
    $scope.adminError = false;
    $scope.errorMessage = "All Field is mandatory!";
    $scope.items = data;
    if ($scope.items.data.manager_comments) {
      $scope.transformedComments = $scope.items.data.manager_comments
        .split(" ")
        .join("\n");
    }
    $scope.approveChanged = function() {
      $scope.adminError = false;
    };
    $scope.saveApproveData = function(data) {
      $scope.finalsubmit = true;
      if (!$scope.transformedComments || !data.approved_hours) {
        $scope.adminError = true;
        $scope.finalsubmit = false;
      }
      if ($scope.adminError) return true;
      var params = {
        timesheet_id: data.timesheet_id,
        approved_hours: data.approved_hours,
        manager_comments: $scope.transformedComments
      };

      Restangular.all("admin/timesheet")
        .post(params)
        .then(
          function(response) {
            $scope.finalsubmit = false;
            if (response.status) {
              $uibModalInstance.close(data);
              window.location.reload();
            }
          },
          function(errors) {
            console.log("checkout");
            console.log(errors.data.message);
            $scope.finalsubmit = false;
            $scope.adminError = true;
            // $scope.errorMessage = errors.data.message.message;
          }
        );
    };
    $scope.cancel = function() {
      $uibModalInstance.dismiss();
    };
  }
]);

angular.module('myApp')
    .controller('checklistCtrl',["$scope", "Restangular", "$http",function($scope, Restangular, $http) {
        $scope.user_mismatch_list = [];
        $scope.unassigned_channels = [];
        $scope.free_projects = [];
        $scope.user_loading = false;
        $scope.channels_loading = false;
        $scope.projects_loading = false;
        $scope.getUserMismatchList = function() {
                $scope.user_loading = true
                Restangular.all('user-mismatch-list').getList().then(
                    function(response) {
                        $scope.user_loading = false;
                        $scope.user_mismatch_list = response;
                    },
                    function(errors) { $scope.user_loading = false; }
                );
            }
            //   $http({
            //     method: 'GET',
            //     url: 'http://geekyants-portal.local.geekydev.com/api/v1/user-mismatch-list'
            //   }).then(
            //     function successCallback(response) {
            //       $scope.that = $scope;
            //       console.log(response);
            //       $scope.contents = Object.keys(response.data).map(function (key) {
            //         $scope.user_mismatch_list = response.data.result.mismatch_details;
            //         return response.data[key];
            //       });
            //       for (var i = 0; i < $scope.contents.length; i++) {
            //         $scope[$scope.contents[i]] = $scope.contents[i];
            //       }
            //     },
            //     function errorCallback(response) { }
            //     );
            //  }

        $scope.getUnassignedChannels = function() {
            $scope.channels_loading = true;
            Restangular.all('channels-without-projects').getList().then(
                function(response) {
                    $scope.channels_loading = false;
                    $scope.unassigned_channels = response;
                },
                function(errors) { $scope.channels_loading = false; }
            );
        }

        $scope.getFreeProjects = function() {
            $scope.projects_loading = true;
            Restangular.all('project-without-resources').getList().then(
                function(response) {
                    $scope.projects_loading = false;
                    $scope.free_projects = response;
                },
                function(errors) { $scope.projects_loading = false; }
            );
        }
    }]);
angular.module('myApp')
    .controller("slackmapCtrl",["$scope", "Restangular", "$uibModal", function ($scope, Restangular, $uibModal) {
        $scope.select2Options = {
            allowClear: true
        };

        $scope.test = "Geek";
        $scope.data = {
            error : false,
            errorMessage : '',
            messageSent : false,
        };
        $scope.users = JSON.parse(window.users);
        $scope.slackUsers = JSON.parse(window.slackUsers);
        $scope.assigned = JSON.parse(window.assigned);
        $showError = [];

        $scope.users.forEach(function(element) {
            var status = {
                'code' : '',
                'error' : "",
            };
            $showError[element.id] = status;
        });
        $scope.showError = $showError;
        $scope.update = function($user_id) {
            if($scope.assigned[$user_id] == null) {
                $slack_id = null;
            } else {
                $slack_id = $scope.assigned[$user_id].id;
            }
            var params = {
                'user_id' : $user_id,
                'slack_id' : $slack_id,
            };
            Restangular.all('slack-user-map/'+$user_id).customPUT(params).then(
                function (response) {
                    $showError[$user_id].code = "message";
                    $showError[$user_id].text = response.result;
                },
                function (errors) { 
                    $showError[$user_id].code = "error";
                    $showError[$user_id].text = errors.data.message;
                }
            );
        };
        $scope.errorDismiss = function() {
            $scope.data.error = false;
            $scope.data.errorMessage = "";
        }
        $scope.messageDismiss = function() {
            $scope.data.messageSent = false;
        };
        $scope.showModal = function(user) {
            if ($scope.assigned[user.id] == null) {
                $scope.data.error = true;
                $scope.data.errorMessage = "This user doesn't have a slack id, message can't be sent";
                window.scrollTo(0,0);
                return;
            }
            var modal = $uibModal.open({
              controller: "sendMessageModalCtrl",
              windowClass: "bootstrap_wrap",
              templateUrl: "/views/send_message.html",
              backdrop: "static",
              size: "md",
              resolve: {
                data: function() {
                  return { user: user, slack_email: $scope.assigned[user.id].email };
                }
              }
            });
        };
    }])
angular
  .module("myApp")
  .controller("sendMessageModalCtrl", ["$scope",
    "Restangular",
    "data",
    "$uibModalInstance",function(
    $scope,
    Restangular,
    data,
    $uibModalInstance
  ) {
    $scope.data = { recieved_data: data, error: false, errorMessage: "", messageSent: false, as_user: false, message: "", botname: "", sending : false };
    $scope.errorDismiss = function() {
      $scope.data.error = false;
      $scope.data.errorMessage = "";
    };
    $scope.setAsUser = function(val) {
      $scope.data.as_user = val;
    };
    $scope.messageDismiss = function() {
      $scope.data.messageSent = false;
    };
    $scope.send = function() {
        $scope.data.sending = true;
        if (!$scope.data.message) {
            $scope.data.sending = false;
            $scope.data.error = true;
            $scope.data.errorMessage = "Message can't be empty";
            return false;
        }
        var params = { user_id: $scope.data.recieved_data.user.id, message: $scope.data.message, as_user: $scope.data.as_user, botname: $scope.data.botname == "" ? "Ant-Manager" : $scope.data.botname };
        Restangular.all("slack-message")
        .post(params)
        .then(
          function(response) {
              $scope.data.sending = false;
              $scope.data.messageSent = true;
            // $uibModalInstance.close(data.id);
            // window.scrollTo(0, 0);
          },
          function(errors) {
            $scope.data.sending = false;
            $scope.data.error = true;
            $scope.data.errorMessage = errors.data.message;
          }
        );
    };
    $scope.cancel = function() {
      $uibModalInstance.dismiss();
    //   window.location.reload();
    };
  }]);
angular.module("myApp").controller("payslipcsvCtrl", [
    "$scope",
    "Restangular",
    "Payslip",
    "$location",
    "$log",
    function ($scope, Restangular, Payslip, $location, $log) {
        if(window.status == 'approved') {
            $scope.step = 6;
            $location.path("/step6");
        } else {
            $scope.step = 1;
            $location.path('/step1');
        }
    }
]);

angular.module("myApp").controller("payslipcsvStep1Ctrl", [
  "$scope",
  "Restangular",
  "Payslip",
  function ($scope, Restangular, Payslip) {

    $scope.step = 1;

    $scope.info = {
      step: 1,
      loading: false
    };
    $scope.var = {
      test: "works!",
      error: false,
      errorMessage: "",
      message: false,
      messageText: ""
    };
    $scope.PayslipData = Payslip.data;
    $scope.$watch("PayslipData.loaded", function () {
      if ($scope.PayslipData.loaded == true) {
        $scope.headers = $scope.PayslipData.payload.headers;
        $scope.data = $scope.PayslipData.payload.data;
        $scope.editable = $scope.PayslipData.payload.editable;
        $scope.loans = $scope.PayslipData.payload.loans;
        $scope.appraisals = $scope.PayslipData.payload.appraisals;
        $scope.render();
      }
    });
    $scope.$watch("PayslipData.loading", function () {
      if ($scope.PayslipData.loading == true) {
        $scope.info.loading = true;
      } else {
        $scope.info.loading = false;
      }
    });
    $scope.errorDismiss = function () {
      $scope.var.error = false;
      $scope.var.errorMessage = "";
    };
    $scope.messageDismiss = function () {
      $scope.var.message = false;
      $scope.var.messageText = "";
    };
    $scope.render = function () {
      Payslip.data.loading = true;
      Payslip.data.loaded = false;
      $scope.showError = [];
      $row = Object.keys($scope.data).length;
      $col = $scope.headers.length;
      for (var i = 1; i <= $row; i++) {
        $scope.showError[i] = [];
        for (var j = 0; j < $col; j++) {
          $scope.showError[i][j] = { code: "", text: "" };
        }
      }
      Payslip.data.loading = false;
      Payslip.data.loaded = true;
    };
    $scope.update = function ($sl_no, $index) {
      var params = {
        sl_no: $sl_no,
        index: $scope.headers[$index],
        value: $scope.data[$sl_no][$index],
      };
      var result = Payslip.updateRow(params);
      if ((result.code = "message")) {
        $scope.showError[$sl_no][$index].code = "message";
        $scope.showError[$sl_no][$index].text = result.text;
      } else {
        $scope.showError[$sl_no][$index].code = "error";
        $scope.showError[$sl_no][$index].text = result.text;
      }
    };
    //     Restangular.all("payslip-csv/"+$id+"/update")
    //       .customPUT(params)
    //       .then(function(response) {
    //           $scope.showError[$sl_no][$index].code = "message";
    //           $scope.showError[$sl_no][$index].text = response.result;
    //         }, function(errors) {
    //             $scope.showError[$sl_no][$index].code = "error";
    //             $scope.showError[$sl_no][$index].text = errors.data.message;
    //         }
    //       );
    // };
    // $scope.nextStep = function() {
    //   Payslip.data.loaded = false;
    //   if ($scope.info.step < 3) {
    //     $scope.info.step++;
    //   }
    //   Payslip.data.loaded = true;
    // };
    // $scope.prevStep = function() {
    //   Payslip.data.loaded = false;
    //   if ($scope.info.step > 1) {
    //     $scope.info.step--;
    //   }
    //   Payslip.data.loaded = true;
    // };
    // $scope.gotoStep = function(step) {
    //   Payslip.data.loaded = false;
    //   $scope.info.step = step;
    //   Payslip.data.loaded = true;
    // };
  }
]);

angular.module("myApp").controller("payslipStep1Ctrl", [
    "$scope",
    "Restangular",
    "Payslip",
    "$log",
    "DTOptionsBuilder", "DTColumnDefBuilder",

    function($scope, Restangular, Payslip, $log, DTOptionsBuilder, DTColumnDefBuilder) {
        //function WithFixedColumnsCtrl(DTOptionsBuilder) {
        var vm = this;
        $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withOption('scrollY', '300px')
            .withOption('scrollX', '100%')
            .withOption('scrollCollapse', true)
            .withOption('paging', false)
            .withFixedColumns({
                leftColumns: 3,
                rightColumns: 0
            });
        //}

        $scope.step = 1;
        $scope.payslip = {
            data: [],
            headers: [],
            editable: [],
            loading: true,
            loaded: false,
            nextStep: function() {
                $scope.step = 2;
                Payslip.gotoRoute($scope.step);
            },
        };
        $scope.info = {
            step: 1,
            loading: false
        };

        $scope.var = {
            test: "works!",
            error: false,
            errorMessage: "",
            message: false,
            messageText: ""
        };
        Payslip.data.loaded = false;
        Payslip.init();
        $scope.PayslipData = Payslip.data;
        $scope.$watch("PayslipData.loaded", function() {


            if ($scope.PayslipData.loaded == true) {
                $scope.payslip.loading = false;
                $scope.payslip.headers = $scope.PayslipData.payload.headers;
                $scope.payslip.data = $scope.PayslipData.payload.list;
                $scope.payslip.editable = $scope.PayslipData.payload.editable;
                $scope.statusInit();
                $scope.payslip.loaded = true;
            }
        });
        $scope.errorDismiss = function() {
            $scope.var.error = false;
            $scope.var.errorMessage = "";
        };
        $scope.messageDismiss = function() {
            $scope.var.message = false;
            $scope.var.messageText = "";
        };
        $scope.statusInit = function() {
            Payslip.data.loading = true;
            Payslip.data.loaded = false;
            $scope.showError = [];
            $row = Object.keys($scope.payslip.data).length;
            $col = $scope.payslip.headers.length;
            for (var i = 1; i <= $row; i++) {
                $scope.showError[i] = [];
                for (var j = 0; j < $col; j++) {
                    $scope.showError[i][j] = { code: "", text: "" };
                }
            }
            Payslip.data.loading = false;
            Payslip.data.loaded = true;
        };
        $scope.update = function($employee_code, $index, $sl_no) {
            var params = {
                employee_code: $employee_code,
                index: $scope.payslip.headers[$index],
                value: $scope.payslip.data[$sl_no][$index],
            };
            var result = Payslip.updateRow(params);
            if ((result.code = "message")) {
                $scope.showError[$sl_no][$index].code = "message";
                $scope.showError[$sl_no][$index].text = result.text;
            } else {
                $scope.showError[$sl_no][$index].code = "error";
                $scope.showError[$sl_no][$index].text = result.text;
            }
        };
        $scope.nextStep = function() {
            $scope.step = 2;
            Payslip.gotoRoute($scope.step);
        };
    }
]);
angular.module("myApp").controller("payslipStep2Ctrl", [
    "$scope",
    "Restangular",
    "Payslip",
    "$log",
    function($scope, Restangular, Payslip, $log) {
        $scope.step = 2;
        $scope.loans = {
            list: [],
            status: [],
            error: [],
            incomplete: [],
            loaded: false,
            empty: false,
            nextStep: function () {
                $scope.step = 3;
                Payslip.gotoRoute($scope.step);
            },
            prevStep: function () {
                $scope.step = 1;
                Payslip.gotoRoute($scope.step);
            },
            update: function($employee_code, $value, $index) {
                $value = $value == "0" ? "0.00" : $value;
                var params = {
                    employee_code: $employee_code,
                    index: "LOAN",
                    value: $value,
                };
                var result = Payslip.updateRow(params);
                if ((result.code = "message")) {
                    $scope.loans.error[$index] = "success";
                } else {
                    $scope.loans.error[$index] = "error";
                }
            },
            init: function() {
                var params = {
                    id: Payslip.data.id,
                };
                Restangular.one("loan")
                .get(params)
                .then(
                    function(response) {
                        console.log('loan ', response.data);
                        $scope.loans.list = response.data;
                        $scope.statusInit();
                    },
                    function(errors) {
                    }
                );
            },
        };
        $scope.loans.init();
        $scope.statusInit = function() {
            $row = Object.keys($scope.loans.list).length;
            if($row > 0) {
                for (var i = 0; i < $row; i++) {
                    if($scope.loans.list[i].status == "paid") {
                        $scope.loans.error[i] = 'success';
                    } else {
                        $scope.loans.error[i] = null;
                    }
                    $scope.loans.incomplete[i] = false;
                }
            } else {
                $scope.loans.empty = true;
            }
        };
        $scope.nextStep = function ()
        {
            $row = Object.keys($scope.loans.list).length;
            $flag=0;
            if ($row > 0) {
                for (var i = 0; i < $row; i++) {
                    $scope.loans.incomplete[i] = false;
                    if ($scope.loans.error[i] != "success") {
                        if($flag==0) {
                            alert("Approve/Reject all loans before moving on");
                        }
                        $flag=1;
                        $scope.loans.incomplete[i] = true;
                    }
                }
            }
            if($flag == 0) {
                $scope.step = 3;
                Payslip.gotoRoute($scope.step);
            }
        };
        $scope.prevStep = function ()
        {
            $scope.step = 1;
            Payslip.gotoRoute($scope.step);
        }
    }
]);

angular.module("myApp").controller("payslipStep3Ctrl", [
  "$scope",
  "Restangular",
  "Payslip",
  "$log",
  function($scope, Restangular, Payslip, $log) {
    $scope.step = 3;
    $scope.bonuses = {
        list: [],
        loading: false,
        loaded: false,
        empty: false,
        init: function() {
            var params = {
                id: Payslip.data.id
            }
            Restangular.one("bonus")
                .get(params)
                .then(
                    function(response) {
                        $scope.bonuses.list = response.data;
                        $count = Object.keys($scope.bonuses.list).length;
                        if ($count <= 0) {
                            $scope.bonuses.empty = true;
                        }
                    },
                    function(errors) {
                    $log.log(errors);
                    }
                );
        }
    };
    $scope.bonuses.init();
    $scope.nextStep = function() {
        $scope.step = 4;
        Payslip.gotoRoute($scope.step);
    };
    $scope.prevStep = function() {
        $scope.step = 2;
        Payslip.gotoRoute($scope.step);
    };
  }
]);

angular.module("myApp").controller("payslipStep4Ctrl", [
    "$scope",
    "Restangular",
    "Payslip",
    "$log",
    function($scope, Restangular, Payslip, $log) {
        $scope.step = 4;
        $scope.appraisals = {
            list: [],
            loading: false,
            loaded: false,
            empty: false,
            init: function() {
                var params = {
                    id: Payslip.data.id
                }
                Restangular.one("appraisal")
                .get(params)
                .then(
                    function(response) {
                        $scope.appraisals.list = response.data;
                        $count = Object.keys($scope.appraisals.list).length;
                        if($count <= 0) {
                            $scope.appraisals.empty = true;
                        }
                    },
                    function(errors) {
                        $log.log(errors);
                    }
                );
            },
        };
        $scope.appraisals.init();
        $scope.nextStep = function()
        {
          $scope.step = 5;
          Payslip.gotoRoute($scope.step);
        };
        $scope.prevStep = function()
        {
          $scope.step = 3;
          Payslip.gotoRoute($scope.step);
        };
    }
]);

angular.module("myApp").controller("payslipStep5Ctrl", [
    "$scope",
    "Restangular",
    "Payslip",
    "$log",
    "DTOptionsBuilder",
    "DTColumnDefBuilder",
    function ($scope, Restangular, Payslip, $log, DTOptionsBuilder, DTColumnDefBuilder) {
        $scope.step = 5;
        $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withOption('scrollY', '300px')
            .withOption('scrollX', '100%')
            .withOption('scrollCollapse', true)
            .withOption('paging', false)
            .withFixedColumns({
                leftColumns: 3,
                rightColumns: 0
            });
        $scope.payslip = {
            data: [],
            headers: [],
            editable: [],
            loading: true,
            loaded: false,
            isConfirmReady: false,
            nextStep: function () {
                $scope.step = 5;
                Payslip.gotoRoute($scope.step);
            },
            prevStep: function () {
                $scope.step = 3;
                Payslip.gotoRoute($scope.step);
            },
        };
        Payslip.data.loaded = false;
        Payslip.init();
        $scope.PayslipData = Payslip.data;
        $scope.$watch("PayslipData.loaded", function () {
            if ($scope.PayslipData.loaded == true) {
                $scope.payslip.loading = false;
                $scope.payslip.headers = $scope.PayslipData.payload.headers;
                $scope.payslip.data = $scope.PayslipData.payload.list;
                $scope.payslip.editable = $scope.PayslipData.payload.editable.fill(false, 0, Object.keys($scope.payslip.headers).length);
                $scope.payslip.loaded = true;
                $scope.payslip.isConfirmReady = true;
            }
        });
        $scope.nextStep = function () {
            if (confirm("This is irreversible, are you sure?")) {
                $scope.step = 6;
                Payslip.gotoRoute($scope.step);
            }
        };
        $scope.prevStep = function () {
            $scope.step = 4;
            Payslip.gotoRoute($scope.step);
        };
    }
]);

angular.module("myApp").controller("payslipStep6Ctrl", [
    "$scope",
    "Restangular",
    "Payslip",
    "$log",
    "DTOptionsBuilder",
    "DTColumnDefBuilder",
    function ($scope, Restangular, Payslip, $log, DTOptionsBuilder, DTColumnDefBuilder) {
        $scope.step = 6;
        $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withOption('scrollY', '300px')
            .withOption('scrollX', '100%')
            .withOption('scrollCollapse', true)
            .withOption('paging', false)
            .withFixedColumns({
                leftColumns: 3,
                rightColumns: 0
            });
        $scope.payslip = {
            data: [],
            headers: [],
            editable: [],
            loading: true,
            loaded: false,
            csv_url: null,
            downloaded: false,
            isDownloadReady: false,
        };
        Payslip.data.loaded = false;
        Payslip.init();
        $scope.PayslipData = Payslip.data;
        $scope.$watch("PayslipData.loaded", function () {
            if ($scope.PayslipData.loaded == true) {
                $scope.payslip.loading = false;
                $scope.payslip.headers = $scope.PayslipData.payload.headers;
                $scope.payslip.data = $scope.PayslipData.payload.list;
                $scope.payslip.editable = $scope.PayslipData.payload.editable.fill(false, 0, Object.keys($scope.payslip.headers).length);
                $scope.getUrl();
                $scope.payslip.loaded = true;
                $scope.payslip.isDownloadReady = true;
            }
        });
        $scope.getUrl = function () {
            Restangular.all("payroll-csv-data/" + Payslip.data.id + "/create-csv")
                .customGET()
                .then(function (response) {
                    $scope.payslip.csv_url = "/api/v1/payroll-csv-data/" + Payslip.data.id + "/download?file=" + response.data;
                });
        };
        $scope.csvdownload = function () {
            $scope.payslip.downloaded = true;
        }
    }
]);

angular.module("myApp").controller("payslipcsvStep2Ctrl", [
  "$scope",
  "Restangular",
  "Payslip",
  function($scope, Restangular, Payslip) {
    $scope.info = {
      step: 2,
      loading: false
    };
    $scope.var = {
      test: "works!",
      error: false,
      errorMessage: "",
      message: false,
      messageText: ""
    };
    $scope.PayslipData = Payslip.data;
    $scope.$watch("PayslipData.loaded", function() {
      if ($scope.PayslipData.loaded == true) {
        $scope.headers = $scope.PayslipData.payload.headers;
        $scope.data = $scope.PayslipData.payload.data;
        $scope.editable = $scope.PayslipData.payload.editable;
        $scope.loans = $scope.PayslipData.payload.loans;
        $scope.appraisals = $scope.PayslipData.payload.appraisals;
        $scope.render();
      }
    });
    $scope.$watch("PayslipData.loading", function() {
      if ($scope.PayslipData.loading == true) {
        $scope.info.loading = true;
      } else {
        $scope.info.loading = false;
      }
    });
    $scope.errorDismiss = function() {
      $scope.var.error = false;
      $scope.var.errorMessage = "";
    };
    $scope.messageDismiss = function() {
      $scope.var.message = false;
      $scope.var.messageText = "";
    };
    $scope.confirmEmi = function($id, $amount) {
      var params = {
        emi_id: $id,
        amount: $amount,
      };
      // console.log(params);
      var response = Payslip.confirmEmi(params);
      if(response) {
        //show success message and disable input
      } else {
        //show error
      }
    };
  }
]);

angular.module("myApp").controller("payslipcsvStep3Ctrl", [
  "$scope",
  "Restangular",
  "Payslip",
  function($scope, Restangular, Payslip) {
    $scope.info = {
      step: 3,
      loading: false
    };
    $scope.var = {
      test: "works!",
      error: false,
      errorMessage: "",
      message: false,
      messageText: ""
    };
    $scope.PayslipData = Payslip.data;
    $scope.$watch("PayslipData.loaded", function() {
      if ($scope.PayslipData.loaded == true) {
        $scope.headers = $scope.PayslipData.payload.headers;
        $scope.data = $scope.PayslipData.payload.data;
        $scope.editable = $scope.PayslipData.payload.editable;
        $scope.loans = $scope.PayslipData.payload.loans;
        $scope.appraisals = $scope.PayslipData.payload.appraisals;
        $scope.render();
      }
    });
    $scope.$watch("PayslipData.loading", function() {
      if ($scope.PayslipData.loading == true) {
        $scope.info.loading = true;
      } else {
        $scope.info.loading = false;
      }
    });
    $scope.errorDismiss = function() {
      $scope.var.error = false;
      $scope.var.errorMessage = "";
    };
    $scope.messageDismiss = function() {
      $scope.var.message = false;
      $scope.var.messageText = "";
    };
    $scope.render = function() {
      Payslip.data.loading = true;
      Payslip.data.loaded = false;
      $scope.showError = [];
      $row = Object.keys($scope.data).length;
      $col = $scope.headers.length;
      for (var i = 1; i <= $row; i++) {
        $scope.showError[i] = [];
        for (var j = 0; j < $col; j++) {
          $scope.showError[i][j] = { code: "", text: "" };
        }
      }
      Payslip.data.loading = false;
      Payslip.data.loaded = true;
    };
    $scope.update = function($sl_no, $index, $id) {
      var params = {
        sl_no: $sl_no,
        index: $scope.headers[$index],
        value: $scope.data[$sl_no][$index],
        id: $id
      };
      var result = Payslip.updateRow(params);
      if ((result.code = "message")) {
        $scope.showError[$sl_no][$index].code = "message";
        $scope.showError[$sl_no][$index].text = result.text;
      } else {
        $scope.showError[$sl_no][$index].code = "error";
        $scope.showError[$sl_no][$index].text = result.text;
      }
    };
    //     Restangular.all("payslip-csv/"+$id+"/update")
    //       .customPUT(params)
    //       .then(function(response) {
    //           $scope.showError[$sl_no][$index].code = "message";
    //           $scope.showError[$sl_no][$index].text = response.result;
    //         }, function(errors) {
    //             $scope.showError[$sl_no][$index].code = "error";
    //             $scope.showError[$sl_no][$index].text = errors.data.message;
    //         }
    //       );
    // };
    // $scope.nextStep = function() {
    //   Payslip.data.loaded = false;
    //   Payslip.data.step++;
    //   Payslip.gotoRoute();
    //   Payslip.data.loaded = true;
    // };
    // $scope.prevStep = function() {
    //   Payslip.data.loaded = false;
    //   Payslip.data.step--; 
    //   Payslip.gotoRoute();
    //   Payslip.data.loaded = true;
    // };
    // $scope.gotoStep = function(step) {
    //   Payslip.data.loaded = false;
    //   $scope.info.step = step;
    //   Payslip.data.loaded = true;
    // };
  }
]);

angular.module('myApp')
    .controller('projectDetailCtrl',["$scope", "Restangular", function ($scope, Restangular) {
        $scope.dates = JSON.parse(window.dates);
        $scope.projects = window.projects[0];
        $scope.user = JSON.parse(window.user);
        $scope.currentDate = new Date().toISOString().slice(0, 10);
        $scope.existingItems = [];
        $scope.loading = [];
        for (var i = 0; i < ($scope.projects.data).length; i++) {
            $scope.existingItems[i] = $scope.projects.data[i].timesheet_details ? $scope.projects.data[i].timesheet_details : [];
            $scope.loading[$scope.projects.data[i].date] = [];
            $scope.loading[i] = false;
        }
        for (var i = 0; i < ($scope.existingItems).length; i++) {
            if (($scope.existingItems[i]).length == 0) {
                $scope.existingItems[i].push({ duration: null, reason: null });
            }
        }
        $scope.addDateRange = function (parent_index, index) {
            $scope.existingItems[parent_index].push({ duration: null, reason: null });
            $scope.form.duration = null;
            $scope.form.reason = null;
        };

        $scope.finalsubmit = false;
        $scope.form = {
            duration: null,
            reason: null
        };

        $scope.changed = function (parentIndex, index, detail) {
            $scope.loading[detail.date][index] = true;
            var params = {
                date: detail.date,
                project_id: detail.project_id,
                timesheet_details: $scope.existingItems[parentIndex]
            };

            Restangular.all('timesheet').post(params).then(

                function (response) {
                    $scope.loading[detail.date][index] = false;
                    $scope.finalsubmit = false;
                    if (response.result.success) {
                    }
                },
                function (errors) {
                    $scope.loading[detail.date][index] = false;
                    $scope.finalsubmit = false;
                    $scope.error = true;
                     $scope.errorMessage = errors.message.message;
                    //console.log(errors);
                }
            );
        };

        $scope.removeDateRange = function (parentIndex, index, detail) {
            $scope.existingItems[parentIndex].splice(index, 1);
            var params = {
                date: detail.date,
                project_id: detail.project_id,
                timesheet_details: $scope.existingItems[parentIndex]
            };
            Restangular.all('timesheet').post(params).then(
                function (response) {
                    $scope.finalsubmit = false;
                    if (response.result.success) {
                    }
                },
                function (errors) {
                    $scope.finalsubmit = false;
                    $scope.error = true;
                    $scope.errorMessage = errors.message.message;
                    //console.log(errors);
                }
            );
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss();
            window.location.reload();
        };
    }]);
angular.module("myApp").controller("employeeLeaveCtrl", [
  "$scope",
  "Restangular",
  "$location",
  "$log",
  "$uibModal",
  "$route",
  function($scope, Restangular, $location, $log, $uibModal, $route) {
    $scope.gridViewModel = "leave?per_page=5";
    $scope.gridViewColumns = [
      {
        title: "Duration",
        value: "%% row.start_date | date %% to %% row.end_date | date %%",
        width: 18
      },
      {
        title: "Days",
        value: "%% row.days %%",
        width: 5
      },
      {
        title: "Type",
        value:
          "<span ng-class=\"{'label label-primary custom-label' : row.type=='paid', 'label label-warning custom-label' : row.type=='sick', 'label label-success custom-label' : row.type=='unpaid' }\"> %% row.type +' Leave' | uppercase%% </span>",
        width: 5
      },
      {
        title: "Status",
        value:
          "<span ng-class=\"{'label label-info custom-label' : row.status=='pending', 'label label-success custom-label' : row.status=='approved', 'label label-danger custom-label' : (row.status=='cancelled' || row.status=='rejected')  }\"> %% row.status | uppercase %%</span> <small ng-if='row.status ==approved' style='display:block'>%%row.approver.name%%</small>",
        width: 5
      }
    ];
    $scope.gridViewActions = [
      {
        type: "cancel",
        action: "deleteDetails(row.id)"
      }
    ];
    $scope.deleteDetails = function(id) {
      if (confirm("Are you sure you want to cancel the leave!")) {
        Restangular.one("leave", id)
          .remove()
          .then(
            function(response) {
              location.reload();
            },
            function(error) {
              alert(error.data.message);
            }
          );
      }
    };

    $scope.data = {
      leaves: []
    };

    $scope.datePicker = {
      options: {
        applyClass: "btn-success",
        locale: {
          applyLabel: "Apply",
          fromLabel: "From",
          // format: "YYYY-MM-DD", //will give you 2017-01-06
          format: "D-MMM-YY", //will give you 6-Jan-17
          // format: "D-MMMM-YY", //will give you 6-January-17
          toLabel: "To",
          cancelLabel: "Cancel",
          customRangeLabel: "Custom range"
        }
      },
      date: { startDate: null, endDate: null },
      min: moment().format("YYYY-MM-DD")
    };

    $scope.leave = {
      loading: false,
      form: {
        type: null,
        start_date: null,
        end_date: null
      }
    };
    $scope.currentDate = Date.now();

    $scope.leaveTypes = [
      {
        name: "Select a leave type",
        value: null
      },
      {
        name: "Sick Leave",
        value: "sick"
      },
      {
        name: "Paid Leave",
        value: "paid"
      },
      {
        name: "Unpaid Leave",
        value: "unpaid"
      }
    ];

    $scope.form = {
      loading: false,
      submitting: false,
      error_message: null,
      error: false,
      success: false,
      data: {
        user_id: $scope.userId,
        start_date: null,
        end_date: null,
        type: null,
        reason: null
      },
      sick_leave_consumed: 0,
      paid_leave_consumed: 0,
      applyLeave: function($form) {
        $scope.form.submitting = true;
        if ($form.$invalid) {
          $scope.form.submitting = false;
          $scope.form.error = true;
          return false;
        }
        $scope.form.loading = true;
        $scope.form.error = false;
        $scope.form.error_message = null;
        var startDate = moment($scope.datePicker.date.startDate).format(
          "YYYY-MM-DD"
        );
        var endDate = moment($scope.datePicker.date.endDate).format(
          "YYYY-MM-DD"
        );

        $scope.form.data.start_date = startDate;
        $scope.form.data.end_date = endDate;

        var params = $scope.form.data;
        Restangular.all("leave")
          .post(params)
          .then(
            function(response) {
              $scope.form.loading = false;
              $scope.form.submitting = false;
              //var data = response.data;
              if (response.data.leave_overlapping) {
                $scope.openOverlappingLeaveModal(response.data);
                return false;
              } else {
                location.reload();
                $scope.form.success = true;
                $scope.form.error = false;
              }
            },
            function(errors) {
              $scope.form.loading = false;
              $scope.form.submitting = false;
              $scope.form.error = true;
              $scope.form.success = false;
              $scope.form.error_message = errors.data.message;
            }
          );
      },
      checkProjectOverlapping: function() {
        $log.log("checkProjectOverlapping called");
      }
    };

    $scope.openOverlappingLeaveModal = function(data) {
      var data = {
        overlapping_data: data.overlapping_leaves,
        type: $scope.form.data.type,
        start_date: $scope.form.data.start_date,
        end_date: $scope.form.data.end_date,
        reason: $scope.form.data.reason,
        user_id: $scope.userId
      };
      var modal = $uibModal.open({
        controller: "leaveOverlappingModalCtrl",
        windowClass: "bootstrap_wrap",
        templateUrl: "/views/leave/leave-overlapping-modal.html",
        backdrop: "static",
        size: "lg",
        resolve: {
          data: function() {
            return data;
          }
        }
      });
      modal.result.then(function(res) {}, function() {});
    };
    $scope.append = function(leave_id) {
      return "/user/leaves/status/" + leave_id;
    };
  }
]);

angular
  .module("myApp")
  .controller("leaveOverlappingModalCtrl",["$scope",
    "Restangular",
    "data",
    "$uibModalInstance",
    "$sce", function(
    $scope,
    Restangular,
    data,
    $uibModalInstance,
    $sce
  ) {
    $scope.data = data;
    $scope.application_reason = [];
    $scope.loading = false;
    $scope.overlap_reasons = [];
    
    $scope.submit = function() {
      $scope.loading = false;
      
      for (var i = 0; i < $scope.data.overlapping_data.projects.length; i++) 
      {
        $scope.overlap_reasons[i] = {
                project_id: $scope.data.overlapping_data.projects[i].id,
                comment: $scope.application_reason[i]
            };
      }
      var params = {
        start_date: $scope.data.start_date,
        end_date: $scope.data.end_date,
        user_id: $scope.data.user_id,
        type: $scope.data.type,
        reason: $scope.data.reason,
        overlap_reasons: $scope.overlap_reasons
      };
      Restangular.all("leave-new")
        .post(params)
        .then(
          function(response) {
            if (response.status) {
                $uibModalInstance.dismiss();
                window.location.reload();
            }
          },
          function(errors) {
            $scope.error_message = errors.data.message;
          }
        );
    };
   

    $scope.cancel = function() {
      $uibModalInstance.dismiss();
    };
  }]);

angular.module("myApp").controller("leaveApplyCtrl", [
    "$scope",
    "Restangular",
    "$location",
    "$log",
    "$uibModal",
    function ($scope, Restangular, $location, $log, $uibModal) 
    {
        $scope.openModal = function () 
        {
            var modal = $uibModal.open({
                controller: "adminLeaveApplyModalCtrl",
                windowClass: "bootstrap_wrap",
                templateUrl: "/views/leave/admin-leave-apply-modal-new.html",
                backdrop: "static",
                size: "lg"
            });
        };

        $scope.showModal = function (id) 
        {
            $scope.list = id;
            var modal = $uibModal.open({
                controller: "adminLeaveEditModalCtrl",
                windowClass: "bootstrap_wrap",
                templateUrl: "/views/leave/admin-leave-edit-modal-new.html",
                backdrop: "static",
                size: "lg",
                resolve: {
                    data: function () {
                        return $scope.list;
                    }
                }
            });
        }
    }
]);

angular.module("myApp").controller("leaveApplyModalCtrl", [
    "$scope",
    "Restangular",
    "$location",
    "$log",
    "$uibModalInstance",
    function ($scope, Restangular, $location, $log, $uibModalInstance) {
        $log.log('====================================');
        console.log('leaveApplyModalCtrl  called');
        console.log('====================================');

        $scope.showModal = function (user) {
            if ($scope.assigned[user.id] == null) {
                $scope.data.error = true;
                $scope.data.errorMessage = "This user doesn't have a slack id, message can't be sent";
                window.scrollTo(0, 0);
                return;
            }
            var modal = $uibModal.open({
                controller: "leaveApplyModalCtrl",
                windowClass: "bootstrap_wrap",
                templateUrl: "/views/leave/modal.html",
                backdrop: "static",
                size: "md",
                resolve: {
                    data: function () {
                        return { user: user, slack_email: $scope.assigned[user.id].email };
                    }
                }
            });
        };
    }
]);

angular.module("myApp").controller("overtimeallwanceCtrl", [
  "$scope",
  "Restangular",
  "$uibModal",
  function($scope, Restangular, $uibModal) {
    // $scope.data = JSON.parse(window.data);
    $scope.data = [];
    let date = new Date();
    let month = date.getMonth() + 1;
    let year = date.getFullYear();

    $scope.model = {
      loading: false,
      data: [],
      projects: [],
      calender: [],
      months: JSON.parse(window.months),
      selected_projects: [], //To determine currently selected project
      selectedMonthYear: "", //To determine currently selected month
      onsite_allowance_id: null,
      error: false,
      select_projects: function (bonus_id) {
        var params = {
          bonus_request_id: bonus_id,
          projects: $scope.model.selected_projects[bonus_id]
        };
        
        Restangular.all("bonus-request-projects")
                .post(params)
                .then(
                    function(response) {
                        console.log(response);
                    },
                    function(errors) {
                        console.log(errors);
                    }
                );
      },
      onOnSiteCheckBoxClick: function(key, index) {
        $scope.noRecordsFlag = false;
        if (key) {
          $scope.data.days[index].onSiteActionStatus = 'progress';
            if ( key.on_site ) {
              var bonus = key.bonus.find( bonus => bonus.type ==='onsite' && bonus.status !='cancelled' );
              var bonusIndex = key.bonus.indexOf(bonus);
              Restangular.one("bonus/delete-bonus-request", bonus.id)
              .remove()
              .then(
                  function (response) {
                    $scope.data.days[index].bonus.splice(bonusIndex,1);
                    $scope.data.days[index].onSiteActionStatus = 'success';
                  },
                  function (errors) {
                      console.log(errors);
                      $scope.data.days[index].onSiteActionStatus = 'error';
                  }
              );
              
            } 
            else {
                var modal = $uibModal.open({
                  controller: "projectSelectionModalCtrl",
                  windowClass: "bootstrap_wrap",
                  templateUrl: "/views/bonus-request/onsite-project-select.html",
                  backdrop: "static",
                  size: "md",
                  resolve: {
                    data: function () {
                      return { 
                        date: key.date,
                        onsite_allowance : key.onsite_allowance,
                        is_holiday: key.is_holiday,
                        is_weekend: key.is_weekend,
                        on_leave: key.on_leave
                       };
                    }
                  }
                });
                modal.result.then(
                  function (response) {
                    $scope.data.days[index].bonus.push(response);
                    if ( response.bonus_request_projects && response.bonus_request_projects.length > 0 )
                    {
                      tempProjectIds = [];
                      for ( j = 0; j < response.bonus_request_projects.length; j++ )
                      {
                        tempProjectIds.push(response.bonus_request_projects[j].project_id);
                      }
                      $scope.model.selected_projects[response.id] = tempProjectIds;
                    }
                    $scope.data.days[index].onSiteActionStatus = 'success';
                  },
                  function () {
                    $scope.data.days[index].on_site = !$scope.data.days[index].on_site;
                    $scope.data.days[index].onSiteActionStatus = null;
                  }
                );
              $scope.data.days[index].warning_message = null;
            }
        }
      },
      
      onAdditionalWorkCheckBoxClick: function(key, index) {
        $scope.noRecordsFlag = false;
        if (key) {
          $scope.data.days[index].additionalWDActionStatus = 'progress';
          if (key.additional )  
          {
            var bonus = key.bonus.find( bonus => bonus.type ==='additional' && bonus.status !='cancelled' );
            var bonusIndex = key.bonus.indexOf(bonus);
              Restangular.one("bonus/delete-bonus-request", bonus.id)
              .remove()
              .then(
                  function (response) {
                    $scope.data.days[index].bonus.splice(bonusIndex,1);
                    $scope.data.days[index].additionalWDActionStatus = 'success';
                  },
                  function (errors) {
                      console.log(errors);
                      $scope.data.days[index].additionalWDActionStatus = 'error';
                  }
              );
          } 
          else {
            var additionalModal = $uibModal.open({
              controller: "additionalSelectionModalCtrl",
              windowClass: "bootstrap_wrap",
              templateUrl: "/views/bonus-request/additional-project-select.html",
              backdrop: "static",
              size: "md",
              resolve: {
                data: function () {
                  return { obj: key };
                }
              }
            });
            additionalModal.result.then(
              function (result) {
                let params = {
                  date: key.date,
                  type: "additional",
                  is_holiday: $scope.data.days[index].is_holiday,
                  is_weekend: $scope.data.days[index].is_weekend,
                  on_leave: $scope.data.days[index].on_leave,
                  onsite_allowance_id: null,
                  project_id: result.selected_projects,
                  duration: result.worked_for,
                  redeem: result.redeem
                };
                Restangular.all('bonus/apply-on-site-bonus').post(params).then(
                  function (response) {
                      response.redeem_info = JSON.parse(response.redeem_type);
                      $scope.data.days[index].bonus.push(response);
                      if ( response.bonus_request_projects && response.bonus_request_projects.length > 0 )
                        {
                          tempProjectIds = [];
                          for ( j = 0; j < response.bonus_request_projects.length; j++ )
                          {
                            tempProjectIds.push(response.bonus_request_projects[j].project_id);
                          }
                          $scope.model.selected_projects[response.id] = tempProjectIds;
                        }
                      $scope.data.days[index].additionalWDActionStatus = 'success';
                  },
                  function (errors) {
                      console.log(errors);
                      $scope.data.days[index].additionalWDActionStatus = 'error';
                  }
                );

              },
              function () {
                $scope.data.days[index].additional = !$scope.data.days[index].additional;
                $scope.data.days[index].additionalWDActionStatus = null;
              }
            );
          }
        }
      },
      previousMonth : function() {
        month = month - 1;
        if( month == 0 )
        {
          month = 12;
          year = year - 1;
        }
        getCalender(month, year);
      },
      nextMonth : function() {
        month = month + 1;
        if( month == 13 )
        {
          month = 1;
          year = year + 1;
        }
        getCalender(month, year);
      }
    };
    let getProjects = function() {
      var params = {
        pagination: 0,
      };
      Restangular.all("project")
        .getList(params)
        .then(function(projectList) {
          // console.log("ProjectList-->", projectList);
          $scope.model.projects = projectList;
        })
        .catch(function(projectListErr) {
          console.log("ProjectList failed", projectListErr);
        });
    };
    getProjects();
    let getCalender = function(month, year) {
      $scope.model.loading = true;
      Restangular.one("bonus/get-calender/"+month+"/"+year).get()
      .then(
          function (response) {
            $scope.data = response;
            $scope.model.loading = false;
            $scope.noRecordsFlag = true;
            $scope.data.days.map( key => {
              if ( key.bonus && key.bonus.length < 1 )
              {
                key.on_site = false;
                key.additional = false;
              }
              else if ( key.bonus && key.bonus.length > 0 ) {
                  $scope.noRecordsFlag = false;
                  key.on_site = false;
                  key.additional = false;
                  for ( i = 0; i < key.bonus.length; i++ )
                  {
                    if ( key.bonus[i].type == 'onsite' && key.bonus[i].status != 'cancelled' ) {
                      key.on_site = true;
                      if ( key.bonus[i].status == 'approved' )
                      {
                        key.on_site_approved = true;
                      }
                    } else if ( key.bonus[i].type != 'onsite' &&  key.bonus[i].status != 'cancelled' ) {
                      key.additional = true;
                      key.bonus[i].redeem_info = JSON.parse(key.bonus[i].redeem_type);
                      console.log('key.bonus[i].redeem_info',key.bonus[i].redeem_info);
                      if ( key.bonus[i].status == 'approved' )
                      {
                        key.additional_approved = true;
                      }
                    }
                    if ( key.bonus[i].bonus_request_projects && key.bonus[i].bonus_request_projects.length > 0 )
                    {
                      tempProjectIds = [];
                      for ( j = 0; j < key.bonus[i].bonus_request_projects.length; j++ )
                      {
                        tempProjectIds.push(key.bonus[i].bonus_request_projects[j].project_id);
                      }
                      $scope.model.selected_projects[key.bonus[i].id] = tempProjectIds;
                    }
                    
                  }
                }
            });
          },
          function (errors) {
              console.log(errors);
              $scope.model.loading = false;
          }
      );
    };

    getCalender(month, year);
    $scope.model.selectedMonthYear = $scope.model.months[0];
    console.log($scope.model.selectedMonthYear);
  }

]);

angular.module("myApp").service("Payslip", [
	"Restangular",
	"$location",
	"$log",
	function Payslip(Restangular, $location, $log) {
		var dataObject = {
			loading: false,
			updating: false,
			error: false,
			loaded: false,
			errorMessage: null,
			code: null,
			text: null,
			payload: null,
			id: null,
			step: 1,
		};
		dataObject.id = window.id;
		function init() {
			dataObject.loading = true;
			var params = {
				id: dataObject.id
			}
			Restangular.one("payroll-csv-data")
				.get(params)
				.then(
					function (response) {
						dataObject.payload = response.data;
						angular.forEach(dataObject.payload.list, function(item) {
							console.log('Item', item);
							item.updateRecord = function() {
								console.log('updateRecord called');
								// Restangular.all("payroll-csv-data/" + dataObject.id)
								// 	.customPUT(params)
								// 	.then(
								// 		function (response) {
								// 			result.code = "message";
								// 			result.text = response.result;
								// 		},
								// 		function (errors) {
								// 			result.code = "error";
								// 			result.text = errors.data.message;
								// 		}, function() {
								// 			dataObject.updating = false;
								// 		});
							}

						});
						dataObject.loading = false;
						dataObject.loaded = true;
					},
					function (errors) {
						dataObject.loading = false;
						dataObject.errors = true;
						dataObject.errorMessage = "Unable to fetch records";
					}
					);
		}
		function updateRow(params) {
			var result = {
				code: "error",
				message: "Something went wrong"
			};
			dataObject.updating = true;
			Restangular.all("payroll-csv-data/" + dataObject.id)
				.customPUT(params)
				.then(
					function (response) {
						result.code = "message";
						result.text = response.result;
					},
					function (errors) {
						result.code = "error";
						result.text = errors.data.message;
					}, function() {
						dataObject.updating = false;
					});
				return result;
		}
		function gotoRoute($step) {
			$location.path("step" + $step);
		}

		return {
			updateRow: updateRow,
			data: dataObject,
			gotoRoute: gotoRoute,
			init: init
		};
	}
]);

angular.module("myApp").service("Datatable",
    function Datatable() {

        const idx = (p, o) => p.reduce((xs, x) => (xs && xs[x]) ? xs[x] : null, o)

        function filterData(filterObj, originalData, filterArr, column) {
            if (filterArr.length) {
                filterObj[column] = filterArr;
            }
            else {
                delete filterObj[column];
            }
            let updatedData = JSON.parse(JSON.stringify(originalData));
            // console.log("filterArr", filterArr);
            // console.log("column", column);
            // console.log("filterObj", filterObj);

            if (Object.keys(filterObj).length) {
                Object.keys(filterObj).map(filterKey => {
                    let nestedKeyArray = [];
                    if (filterKey.indexOf('.') >= 0)
                        nestedKeyArray = filterKey.split('.');
                    else
                        nestedKeyArray = [filterKey];
                    updatedData = updatedData.filter((singleData) => filterObj[filterKey].indexOf(idx(nestedKeyArray, singleData)) >= 0);
                })
            }
            return updatedData;
            // $scope.data = updatedData.map((newData) => newData);
        }
        return {
            filterData: filterData
        };
    })
$(function() {

    var slicked;

    if ($('.default').length) {
        $('.mg-space-init').mgSpace();
        
        $('.mg-space-init').on('beforeChange', function(event, mgSpace, trigger, rowItem){
          //$(rowItem).addClass('event-fired');
          console.log('Fire Before Everything');
        });

        $('.mg-space-init').on('afterChange', function(event, mgSpace, trigger, rowItem){
          //$(rowItem).addClass('event-fired').siblings().removeClass('event-fired');
          console.log('Fire After Everything');
        });        

        $('.mg-space-init').on('beforeOpenRow', function(event, mgSpace, rowItem){
          console.log('Fire Before Open Row');
        });

        $('.mg-space-init').on('afterOpenRow', function(event, mgSpace, rowItem){
          console.log('Fire After Open Row');
        });
    }

    if($('.no-grid').length){
        $('.no-grid').mgSpace({
            rowMargin:0, 
            targetPadding:90,
            breakpointColumns: [
                {
                    breakpoint: 0,
                    column: 1
                },
                {
                    breakpoint: 568,
                    column: 2
                },
                {
                    breakpoint: 768,
                    column: 3
                }                
            ]
        });
    }

    if($('.mg-history').length){
        $('.mg-space-init').mgSpace({useHash: true,useOnpageHash: true});
    }

    if($('.mg-space-multiple').length){
        $('.mg-space-init4').mgSpace({useHash: true});
    }

    if ($('.slick').length) {
        $('.mg-space-init').mgSpace();

        $('.mg-space-init').on('afterOpenTarget', function(event, mgSpace, itemTarget){
            slicked = itemTarget['selector'];
            
            var cols = mgSpace.setColumns();

            //For responsive slick you must wrap in screen width tests and set them seperately
            //you can not use slicks "responsive" option.
            $(slicked +' .multiple-items').slick({
              infinite: true,
              slidesToShow: cols,
              slidesToScroll: 1,           
            });                

        });

        $('.mg-space-init').on('afterCloseTarget', function(event, mgSpace){
            if ($('.slick-initialized').length) {
                $(slicked +' .multiple-items').slick("destroy", true);
            }
        });                
    }    

    // Chrome Dev Tools Screen Sizing Clone
    function gvwh(){var i=window,t="inner";return"innerWidth"in window||(t="client",i=document.documentElement||document.body),i[t+"Width"]+"px"+' &times; '+i[t+"Height"]+"px"}$("body").append('<div id="size" style="position:fixed;bottom:0;right:0;background:#ddd;padding:0 6px;font-size:16px;font-family:sans-serif;z-index:9999"></div>'),$("#size").append(gvwh()),$(window).resize(function(){$("#size").html(gvwh())});   
}); // END DOC READY
/*!
 * Masonry PACKAGED v4.2.0
 * Cascading grid layout library
 * http://masonry.desandro.com
 * MIT License
 * by David DeSandro
 */

!function(t,e){"function"==typeof define&&define.amd?define("jquery-bridget/jquery-bridget",["jquery"],function(i){return e(t,i)}):"object"==typeof module&&module.exports?module.exports=e(t,require("jquery")):t.jQueryBridget=e(t,t.jQuery)}(window,function(t,e){"use strict";function i(i,r,a){function h(t,e,n){var o,r="$()."+i+'("'+e+'")';return t.each(function(t,h){var u=a.data(h,i);if(!u)return void s(i+" not initialized. Cannot call methods, i.e. "+r);var d=u[e];if(!d||"_"==e.charAt(0))return void s(r+" is not a valid method");var l=d.apply(u,n);o=void 0===o?l:o}),void 0!==o?o:t}function u(t,e){t.each(function(t,n){var o=a.data(n,i);o?(o.option(e),o._init()):(o=new r(n,e),a.data(n,i,o))})}a=a||e||t.jQuery,a&&(r.prototype.option||(r.prototype.option=function(t){a.isPlainObject(t)&&(this.options=a.extend(!0,this.options,t))}),a.fn[i]=function(t){if("string"==typeof t){var e=o.call(arguments,1);return h(this,t,e)}return u(this,t),this},n(a))}function n(t){!t||t&&t.bridget||(t.bridget=i)}var o=Array.prototype.slice,r=t.console,s="undefined"==typeof r?function(){}:function(t){r.error(t)};return n(e||t.jQuery),i}),function(t,e){"function"==typeof define&&define.amd?define("ev-emitter/ev-emitter",e):"object"==typeof module&&module.exports?module.exports=e():t.EvEmitter=e()}("undefined"!=typeof window?window:this,function(){function t(){}var e=t.prototype;return e.on=function(t,e){if(t&&e){var i=this._events=this._events||{},n=i[t]=i[t]||[];return-1==n.indexOf(e)&&n.push(e),this}},e.once=function(t,e){if(t&&e){this.on(t,e);var i=this._onceEvents=this._onceEvents||{},n=i[t]=i[t]||{};return n[e]=!0,this}},e.off=function(t,e){var i=this._events&&this._events[t];if(i&&i.length){var n=i.indexOf(e);return-1!=n&&i.splice(n,1),this}},e.emitEvent=function(t,e){var i=this._events&&this._events[t];if(i&&i.length){var n=0,o=i[n];e=e||[];for(var r=this._onceEvents&&this._onceEvents[t];o;){var s=r&&r[o];s&&(this.off(t,o),delete r[o]),o.apply(this,e),n+=s?0:1,o=i[n]}return this}},t}),function(t,e){"use strict";"function"==typeof define&&define.amd?define("get-size/get-size",[],function(){return e()}):"object"==typeof module&&module.exports?module.exports=e():t.getSize=e()}(window,function(){"use strict";function t(t){var e=parseFloat(t),i=-1==t.indexOf("%")&&!isNaN(e);return i&&e}function e(){}function i(){for(var t={width:0,height:0,innerWidth:0,innerHeight:0,outerWidth:0,outerHeight:0},e=0;u>e;e++){var i=h[e];t[i]=0}return t}function n(t){var e=getComputedStyle(t);return e||a("Style returned "+e+". Are you running this code in a hidden iframe on Firefox? See http://bit.ly/getsizebug1"),e}function o(){if(!d){d=!0;var e=document.createElement("div");e.style.width="200px",e.style.padding="1px 2px 3px 4px",e.style.borderStyle="solid",e.style.borderWidth="1px 2px 3px 4px",e.style.boxSizing="border-box";var i=document.body||document.documentElement;i.appendChild(e);var o=n(e);r.isBoxSizeOuter=s=200==t(o.width),i.removeChild(e)}}function r(e){if(o(),"string"==typeof e&&(e=document.querySelector(e)),e&&"object"==typeof e&&e.nodeType){var r=n(e);if("none"==r.display)return i();var a={};a.width=e.offsetWidth,a.height=e.offsetHeight;for(var d=a.isBorderBox="border-box"==r.boxSizing,l=0;u>l;l++){var c=h[l],f=r[c],m=parseFloat(f);a[c]=isNaN(m)?0:m}var p=a.paddingLeft+a.paddingRight,g=a.paddingTop+a.paddingBottom,y=a.marginLeft+a.marginRight,v=a.marginTop+a.marginBottom,_=a.borderLeftWidth+a.borderRightWidth,z=a.borderTopWidth+a.borderBottomWidth,E=d&&s,b=t(r.width);b!==!1&&(a.width=b+(E?0:p+_));var x=t(r.height);return x!==!1&&(a.height=x+(E?0:g+z)),a.innerWidth=a.width-(p+_),a.innerHeight=a.height-(g+z),a.outerWidth=a.width+y,a.outerHeight=a.height+v,a}}var s,a="undefined"==typeof console?e:function(t){console.error(t)},h=["paddingLeft","paddingRight","paddingTop","paddingBottom","marginLeft","marginRight","marginTop","marginBottom","borderLeftWidth","borderRightWidth","borderTopWidth","borderBottomWidth"],u=h.length,d=!1;return r}),function(t,e){"use strict";"function"==typeof define&&define.amd?define("desandro-matches-selector/matches-selector",e):"object"==typeof module&&module.exports?module.exports=e():t.matchesSelector=e()}(window,function(){"use strict";var t=function(){var t=window.Element.prototype;if(t.matches)return"matches";if(t.matchesSelector)return"matchesSelector";for(var e=["webkit","moz","ms","o"],i=0;i<e.length;i++){var n=e[i],o=n+"MatchesSelector";if(t[o])return o}}();return function(e,i){return e[t](i)}}),function(t,e){"function"==typeof define&&define.amd?define("fizzy-ui-utils/utils",["desandro-matches-selector/matches-selector"],function(i){return e(t,i)}):"object"==typeof module&&module.exports?module.exports=e(t,require("desandro-matches-selector")):t.fizzyUIUtils=e(t,t.matchesSelector)}(window,function(t,e){var i={};i.extend=function(t,e){for(var i in e)t[i]=e[i];return t},i.modulo=function(t,e){return(t%e+e)%e},i.makeArray=function(t){var e=[];if(Array.isArray(t))e=t;else if(t&&"object"==typeof t&&"number"==typeof t.length)for(var i=0;i<t.length;i++)e.push(t[i]);else e.push(t);return e},i.removeFrom=function(t,e){var i=t.indexOf(e);-1!=i&&t.splice(i,1)},i.getParent=function(t,i){for(;t!=document.body;)if(t=t.parentNode,e(t,i))return t},i.getQueryElement=function(t){return"string"==typeof t?document.querySelector(t):t},i.handleEvent=function(t){var e="on"+t.type;this[e]&&this[e](t)},i.filterFindElements=function(t,n){t=i.makeArray(t);var o=[];return t.forEach(function(t){if(t instanceof HTMLElement){if(!n)return void o.push(t);e(t,n)&&o.push(t);for(var i=t.querySelectorAll(n),r=0;r<i.length;r++)o.push(i[r])}}),o},i.debounceMethod=function(t,e,i){var n=t.prototype[e],o=e+"Timeout";t.prototype[e]=function(){var t=this[o];t&&clearTimeout(t);var e=arguments,r=this;this[o]=setTimeout(function(){n.apply(r,e),delete r[o]},i||100)}},i.docReady=function(t){var e=document.readyState;"complete"==e||"interactive"==e?setTimeout(t):document.addEventListener("DOMContentLoaded",t)},i.toDashed=function(t){return t.replace(/(.)([A-Z])/g,function(t,e,i){return e+"-"+i}).toLowerCase()};var n=t.console;return i.htmlInit=function(e,o){i.docReady(function(){var r=i.toDashed(o),s="data-"+r,a=document.querySelectorAll("["+s+"]"),h=document.querySelectorAll(".js-"+r),u=i.makeArray(a).concat(i.makeArray(h)),d=s+"-options",l=t.jQuery;u.forEach(function(t){var i,r=t.getAttribute(s)||t.getAttribute(d);try{i=r&&JSON.parse(r)}catch(a){return void(n&&n.error("Error parsing "+s+" on "+t.className+": "+a))}var h=new e(t,i);l&&l.data(t,o,h)})})},i}),function(t,e){"function"==typeof define&&define.amd?define("outlayer/item",["ev-emitter/ev-emitter","get-size/get-size"],e):"object"==typeof module&&module.exports?module.exports=e(require("ev-emitter"),require("get-size")):(t.Outlayer={},t.Outlayer.Item=e(t.EvEmitter,t.getSize))}(window,function(t,e){"use strict";function i(t){for(var e in t)return!1;return e=null,!0}function n(t,e){t&&(this.element=t,this.layout=e,this.position={x:0,y:0},this._create())}function o(t){return t.replace(/([A-Z])/g,function(t){return"-"+t.toLowerCase()})}var r=document.documentElement.style,s="string"==typeof r.transition?"transition":"WebkitTransition",a="string"==typeof r.transform?"transform":"WebkitTransform",h={WebkitTransition:"webkitTransitionEnd",transition:"transitionend"}[s],u={transform:a,transition:s,transitionDuration:s+"Duration",transitionProperty:s+"Property",transitionDelay:s+"Delay"},d=n.prototype=Object.create(t.prototype);d.constructor=n,d._create=function(){this._transn={ingProperties:{},clean:{},onEnd:{}},this.css({position:"absolute"})},d.handleEvent=function(t){var e="on"+t.type;this[e]&&this[e](t)},d.getSize=function(){this.size=e(this.element)},d.css=function(t){var e=this.element.style;for(var i in t){var n=u[i]||i;e[n]=t[i]}},d.getPosition=function(){var t=getComputedStyle(this.element),e=this.layout._getOption("originLeft"),i=this.layout._getOption("originTop"),n=t[e?"left":"right"],o=t[i?"top":"bottom"],r=this.layout.size,s=-1!=n.indexOf("%")?parseFloat(n)/100*r.width:parseInt(n,10),a=-1!=o.indexOf("%")?parseFloat(o)/100*r.height:parseInt(o,10);s=isNaN(s)?0:s,a=isNaN(a)?0:a,s-=e?r.paddingLeft:r.paddingRight,a-=i?r.paddingTop:r.paddingBottom,this.position.x=s,this.position.y=a},d.layoutPosition=function(){var t=this.layout.size,e={},i=this.layout._getOption("originLeft"),n=this.layout._getOption("originTop"),o=i?"paddingLeft":"paddingRight",r=i?"left":"right",s=i?"right":"left",a=this.position.x+t[o];e[r]=this.getXValue(a),e[s]="";var h=n?"paddingTop":"paddingBottom",u=n?"top":"bottom",d=n?"bottom":"top",l=this.position.y+t[h];e[u]=this.getYValue(l),e[d]="",this.css(e),this.emitEvent("layout",[this])},d.getXValue=function(t){var e=this.layout._getOption("horizontal");return this.layout.options.percentPosition&&!e?t/this.layout.size.width*100+"%":t+"px"},d.getYValue=function(t){var e=this.layout._getOption("horizontal");return this.layout.options.percentPosition&&e?t/this.layout.size.height*100+"%":t+"px"},d._transitionTo=function(t,e){this.getPosition();var i=this.position.x,n=this.position.y,o=parseInt(t,10),r=parseInt(e,10),s=o===this.position.x&&r===this.position.y;if(this.setPosition(t,e),s&&!this.isTransitioning)return void this.layoutPosition();var a=t-i,h=e-n,u={};u.transform=this.getTranslate(a,h),this.transition({to:u,onTransitionEnd:{transform:this.layoutPosition},isCleaning:!0})},d.getTranslate=function(t,e){var i=this.layout._getOption("originLeft"),n=this.layout._getOption("originTop");return t=i?t:-t,e=n?e:-e,"translate3d("+t+"px, "+e+"px, 0)"},d.goTo=function(t,e){this.setPosition(t,e),this.layoutPosition()},d.moveTo=d._transitionTo,d.setPosition=function(t,e){this.position.x=parseInt(t,10),this.position.y=parseInt(e,10)},d._nonTransition=function(t){this.css(t.to),t.isCleaning&&this._removeStyles(t.to);for(var e in t.onTransitionEnd)t.onTransitionEnd[e].call(this)},d.transition=function(t){if(!parseFloat(this.layout.options.transitionDuration))return void this._nonTransition(t);var e=this._transn;for(var i in t.onTransitionEnd)e.onEnd[i]=t.onTransitionEnd[i];for(i in t.to)e.ingProperties[i]=!0,t.isCleaning&&(e.clean[i]=!0);if(t.from){this.css(t.from);var n=this.element.offsetHeight;n=null}this.enableTransition(t.to),this.css(t.to),this.isTransitioning=!0};var l="opacity,"+o(a);d.enableTransition=function(){if(!this.isTransitioning){var t=this.layout.options.transitionDuration;t="number"==typeof t?t+"ms":t,this.css({transitionProperty:l,transitionDuration:t,transitionDelay:this.staggerDelay||0}),this.element.addEventListener(h,this,!1)}},d.onwebkitTransitionEnd=function(t){this.ontransitionend(t)},d.onotransitionend=function(t){this.ontransitionend(t)};var c={"-webkit-transform":"transform"};d.ontransitionend=function(t){if(t.target===this.element){var e=this._transn,n=c[t.propertyName]||t.propertyName;if(delete e.ingProperties[n],i(e.ingProperties)&&this.disableTransition(),n in e.clean&&(this.element.style[t.propertyName]="",delete e.clean[n]),n in e.onEnd){var o=e.onEnd[n];o.call(this),delete e.onEnd[n]}this.emitEvent("transitionEnd",[this])}},d.disableTransition=function(){this.removeTransitionStyles(),this.element.removeEventListener(h,this,!1),this.isTransitioning=!1},d._removeStyles=function(t){var e={};for(var i in t)e[i]="";this.css(e)};var f={transitionProperty:"",transitionDuration:"",transitionDelay:""};return d.removeTransitionStyles=function(){this.css(f)},d.stagger=function(t){t=isNaN(t)?0:t,this.staggerDelay=t+"ms"},d.removeElem=function(){this.element.parentNode.removeChild(this.element),this.css({display:""}),this.emitEvent("remove",[this])},d.remove=function(){return s&&parseFloat(this.layout.options.transitionDuration)?(this.once("transitionEnd",function(){this.removeElem()}),void this.hide()):void this.removeElem()},d.reveal=function(){delete this.isHidden,this.css({display:""});var t=this.layout.options,e={},i=this.getHideRevealTransitionEndProperty("visibleStyle");e[i]=this.onRevealTransitionEnd,this.transition({from:t.hiddenStyle,to:t.visibleStyle,isCleaning:!0,onTransitionEnd:e})},d.onRevealTransitionEnd=function(){this.isHidden||this.emitEvent("reveal")},d.getHideRevealTransitionEndProperty=function(t){var e=this.layout.options[t];if(e.opacity)return"opacity";for(var i in e)return i},d.hide=function(){this.isHidden=!0,this.css({display:""});var t=this.layout.options,e={},i=this.getHideRevealTransitionEndProperty("hiddenStyle");e[i]=this.onHideTransitionEnd,this.transition({from:t.visibleStyle,to:t.hiddenStyle,isCleaning:!0,onTransitionEnd:e})},d.onHideTransitionEnd=function(){this.isHidden&&(this.css({display:"none"}),this.emitEvent("hide"))},d.destroy=function(){this.css({position:"",left:"",right:"",top:"",bottom:"",transition:"",transform:""})},n}),function(t,e){"use strict";"function"==typeof define&&define.amd?define("outlayer/outlayer",["ev-emitter/ev-emitter","get-size/get-size","fizzy-ui-utils/utils","./item"],function(i,n,o,r){return e(t,i,n,o,r)}):"object"==typeof module&&module.exports?module.exports=e(t,require("ev-emitter"),require("get-size"),require("fizzy-ui-utils"),require("./item")):t.Outlayer=e(t,t.EvEmitter,t.getSize,t.fizzyUIUtils,t.Outlayer.Item)}(window,function(t,e,i,n,o){"use strict";function r(t,e){var i=n.getQueryElement(t);if(!i)return void(h&&h.error("Bad element for "+this.constructor.namespace+": "+(i||t)));this.element=i,u&&(this.$element=u(this.element)),this.options=n.extend({},this.constructor.defaults),this.option(e);var o=++l;this.element.outlayerGUID=o,c[o]=this,this._create();var r=this._getOption("initLayout");r&&this.layout()}function s(t){function e(){t.apply(this,arguments)}return e.prototype=Object.create(t.prototype),e.prototype.constructor=e,e}function a(t){if("number"==typeof t)return t;var e=t.match(/(^\d*\.?\d*)(\w*)/),i=e&&e[1],n=e&&e[2];if(!i.length)return 0;i=parseFloat(i);var o=m[n]||1;return i*o}var h=t.console,u=t.jQuery,d=function(){},l=0,c={};r.namespace="outlayer",r.Item=o,r.defaults={containerStyle:{position:"relative"},initLayout:!0,originLeft:!0,originTop:!0,resize:!0,resizeContainer:!0,transitionDuration:"0.4s",hiddenStyle:{opacity:0,transform:"scale(0.001)"},visibleStyle:{opacity:1,transform:"scale(1)"}};var f=r.prototype;n.extend(f,e.prototype),f.option=function(t){n.extend(this.options,t)},f._getOption=function(t){var e=this.constructor.compatOptions[t];return e&&void 0!==this.options[e]?this.options[e]:this.options[t]},r.compatOptions={initLayout:"isInitLayout",horizontal:"isHorizontal",layoutInstant:"isLayoutInstant",originLeft:"isOriginLeft",originTop:"isOriginTop",resize:"isResizeBound",resizeContainer:"isResizingContainer"},f._create=function(){this.reloadItems(),this.stamps=[],this.stamp(this.options.stamp),n.extend(this.element.style,this.options.containerStyle);var t=this._getOption("resize");t&&this.bindResize()},f.reloadItems=function(){this.items=this._itemize(this.element.children)},f._itemize=function(t){for(var e=this._filterFindItemElements(t),i=this.constructor.Item,n=[],o=0;o<e.length;o++){var r=e[o],s=new i(r,this);n.push(s)}return n},f._filterFindItemElements=function(t){return n.filterFindElements(t,this.options.itemSelector)},f.getItemElements=function(){return this.items.map(function(t){return t.element})},f.layout=function(){this._resetLayout(),this._manageStamps();var t=this._getOption("layoutInstant"),e=void 0!==t?t:!this._isLayoutInited;this.layoutItems(this.items,e),this._isLayoutInited=!0},f._init=f.layout,f._resetLayout=function(){this.getSize()},f.getSize=function(){this.size=i(this.element)},f._getMeasurement=function(t,e){var n,o=this.options[t];o?("string"==typeof o?n=this.element.querySelector(o):o instanceof HTMLElement&&(n=o),this[t]=n?i(n)[e]:o):this[t]=0},f.layoutItems=function(t,e){t=this._getItemsForLayout(t),this._layoutItems(t,e),this._postLayout()},f._getItemsForLayout=function(t){return t.filter(function(t){return!t.isIgnored})},f._layoutItems=function(t,e){if(this._emitCompleteOnItems("layout",t),t&&t.length){var i=[];t.forEach(function(t){var n=this._getItemLayoutPosition(t);n.item=t,n.isInstant=e||t.isLayoutInstant,i.push(n)},this),this._processLayoutQueue(i)}},f._getItemLayoutPosition=function(){return{x:0,y:0}},f._processLayoutQueue=function(t){this.updateStagger(),t.forEach(function(t,e){this._positionItem(t.item,t.x,t.y,t.isInstant,e)},this)},f.updateStagger=function(){var t=this.options.stagger;return null===t||void 0===t?void(this.stagger=0):(this.stagger=a(t),this.stagger)},f._positionItem=function(t,e,i,n,o){n?t.goTo(e,i):(t.stagger(o*this.stagger),t.moveTo(e,i))},f._postLayout=function(){this.resizeContainer()},f.resizeContainer=function(){var t=this._getOption("resizeContainer");if(t){var e=this._getContainerSize();e&&(this._setContainerMeasure(e.width,!0),this._setContainerMeasure(e.height,!1))}},f._getContainerSize=d,f._setContainerMeasure=function(t,e){if(void 0!==t){var i=this.size;i.isBorderBox&&(t+=e?i.paddingLeft+i.paddingRight+i.borderLeftWidth+i.borderRightWidth:i.paddingBottom+i.paddingTop+i.borderTopWidth+i.borderBottomWidth),t=Math.max(t,0),this.element.style[e?"width":"height"]=t+"px"}},f._emitCompleteOnItems=function(t,e){function i(){o.dispatchEvent(t+"Complete",null,[e])}function n(){s++,s==r&&i()}var o=this,r=e.length;if(!e||!r)return void i();var s=0;e.forEach(function(e){e.once(t,n)})},f.dispatchEvent=function(t,e,i){var n=e?[e].concat(i):i;if(this.emitEvent(t,n),u)if(this.$element=this.$element||u(this.element),e){var o=u.Event(e);o.type=t,this.$element.trigger(o,i)}else this.$element.trigger(t,i)},f.ignore=function(t){var e=this.getItem(t);e&&(e.isIgnored=!0)},f.unignore=function(t){var e=this.getItem(t);e&&delete e.isIgnored},f.stamp=function(t){t=this._find(t),t&&(this.stamps=this.stamps.concat(t),t.forEach(this.ignore,this))},f.unstamp=function(t){t=this._find(t),t&&t.forEach(function(t){n.removeFrom(this.stamps,t),this.unignore(t)},this)},f._find=function(t){return t?("string"==typeof t&&(t=this.element.querySelectorAll(t)),t=n.makeArray(t)):void 0},f._manageStamps=function(){this.stamps&&this.stamps.length&&(this._getBoundingRect(),this.stamps.forEach(this._manageStamp,this))},f._getBoundingRect=function(){var t=this.element.getBoundingClientRect(),e=this.size;this._boundingRect={left:t.left+e.paddingLeft+e.borderLeftWidth,top:t.top+e.paddingTop+e.borderTopWidth,right:t.right-(e.paddingRight+e.borderRightWidth),bottom:t.bottom-(e.paddingBottom+e.borderBottomWidth)}},f._manageStamp=d,f._getElementOffset=function(t){var e=t.getBoundingClientRect(),n=this._boundingRect,o=i(t),r={left:e.left-n.left-o.marginLeft,top:e.top-n.top-o.marginTop,right:n.right-e.right-o.marginRight,bottom:n.bottom-e.bottom-o.marginBottom};return r},f.handleEvent=n.handleEvent,f.bindResize=function(){t.addEventListener("resize",this),this.isResizeBound=!0},f.unbindResize=function(){t.removeEventListener("resize",this),this.isResizeBound=!1},f.onresize=function(){this.resize()},n.debounceMethod(r,"onresize",100),f.resize=function(){this.isResizeBound&&this.needsResizeLayout()&&this.layout()},f.needsResizeLayout=function(){var t=i(this.element),e=this.size&&t;return e&&t.innerWidth!==this.size.innerWidth},f.addItems=function(t){var e=this._itemize(t);return e.length&&(this.items=this.items.concat(e)),e},f.appended=function(t){var e=this.addItems(t);e.length&&(this.layoutItems(e,!0),this.reveal(e))},f.prepended=function(t){var e=this._itemize(t);if(e.length){var i=this.items.slice(0);this.items=e.concat(i),this._resetLayout(),this._manageStamps(),this.layoutItems(e,!0),this.reveal(e),this.layoutItems(i)}},f.reveal=function(t){if(this._emitCompleteOnItems("reveal",t),t&&t.length){var e=this.updateStagger();t.forEach(function(t,i){t.stagger(i*e),t.reveal()})}},f.hide=function(t){if(this._emitCompleteOnItems("hide",t),t&&t.length){var e=this.updateStagger();t.forEach(function(t,i){t.stagger(i*e),t.hide()})}},f.revealItemElements=function(t){var e=this.getItems(t);this.reveal(e)},f.hideItemElements=function(t){var e=this.getItems(t);this.hide(e)},f.getItem=function(t){for(var e=0;e<this.items.length;e++){var i=this.items[e];if(i.element==t)return i}},f.getItems=function(t){t=n.makeArray(t);var e=[];return t.forEach(function(t){var i=this.getItem(t);i&&e.push(i)},this),e},f.remove=function(t){var e=this.getItems(t);this._emitCompleteOnItems("remove",e),e&&e.length&&e.forEach(function(t){t.remove(),n.removeFrom(this.items,t)},this)},f.destroy=function(){var t=this.element.style;t.height="",t.position="",t.width="",this.items.forEach(function(t){t.destroy()}),this.unbindResize();var e=this.element.outlayerGUID;delete c[e],delete this.element.outlayerGUID,u&&u.removeData(this.element,this.constructor.namespace)},r.data=function(t){t=n.getQueryElement(t);var e=t&&t.outlayerGUID;return e&&c[e]},r.create=function(t,e){var i=s(r);return i.defaults=n.extend({},r.defaults),n.extend(i.defaults,e),i.compatOptions=n.extend({},r.compatOptions),i.namespace=t,i.data=r.data,i.Item=s(o),n.htmlInit(i,t),u&&u.bridget&&u.bridget(t,i),i};var m={ms:1,s:1e3};return r.Item=o,r}),function(t,e){"function"==typeof define&&define.amd?define(["outlayer/outlayer","get-size/get-size"],e):"object"==typeof module&&module.exports?module.exports=e(require("outlayer"),require("get-size")):t.Masonry=e(t.Outlayer,t.getSize)}(window,function(t,e){var i=t.create("masonry");i.compatOptions.fitWidth="isFitWidth";var n=i.prototype;return n._resetLayout=function(){this.getSize(),this._getMeasurement("columnWidth","outerWidth"),this._getMeasurement("gutter","outerWidth"),this.measureColumns(),this.colYs=[];for(var t=0;t<this.cols;t++)this.colYs.push(0);this.maxY=0,this.horizontalColIndex=0},n.measureColumns=function(){if(this.getContainerWidth(),!this.columnWidth){var t=this.items[0],i=t&&t.element;this.columnWidth=i&&e(i).outerWidth||this.containerWidth}var n=this.columnWidth+=this.gutter,o=this.containerWidth+this.gutter,r=o/n,s=n-o%n,a=s&&1>s?"round":"floor";r=Math[a](r),this.cols=Math.max(r,1)},n.getContainerWidth=function(){var t=this._getOption("fitWidth"),i=t?this.element.parentNode:this.element,n=e(i);this.containerWidth=n&&n.innerWidth},n._getItemLayoutPosition=function(t){t.getSize();var e=t.size.outerWidth%this.columnWidth,i=e&&1>e?"round":"ceil",n=Math[i](t.size.outerWidth/this.columnWidth);n=Math.min(n,this.cols);for(var o=this.options.horizontalOrder?"_getHorizontalColPosition":"_getTopColPosition",r=this[o](n,t),s={x:this.columnWidth*r.col,y:r.y},a=r.y+t.size.outerHeight,h=n+r.col,u=r.col;h>u;u++)this.colYs[u]=a;return s},n._getTopColPosition=function(t){var e=this._getTopColGroup(t),i=Math.min.apply(Math,e);return{col:e.indexOf(i),y:i}},n._getTopColGroup=function(t){if(2>t)return this.colYs;for(var e=[],i=this.cols+1-t,n=0;i>n;n++)e[n]=this._getColGroupY(n,t);return e},n._getColGroupY=function(t,e){if(2>e)return this.colYs[t];var i=this.colYs.slice(t,t+e);return Math.max.apply(Math,i)},n._getHorizontalColPosition=function(t,e){var i=this.horizontalColIndex%this.cols,n=t>1&&i+t>this.cols;i=n?0:i;var o=e.size.outerWidth&&e.size.outerHeight;return this.horizontalColIndex=o?i+t:this.horizontalColIndex,{col:i,y:this._getColGroupY(i,t)}},n._manageStamp=function(t){var i=e(t),n=this._getElementOffset(t),o=this._getOption("originLeft"),r=o?n.left:n.right,s=r+i.outerWidth,a=Math.floor(r/this.columnWidth);a=Math.max(0,a);var h=Math.floor(s/this.columnWidth);h-=s%this.columnWidth?0:1,h=Math.min(this.cols-1,h);for(var u=this._getOption("originTop"),d=(u?n.top:n.bottom)+i.outerHeight,l=a;h>=l;l++)this.colYs[l]=Math.max(d,this.colYs[l])},n._getContainerSize=function(){this.maxY=Math.max.apply(Math,this.colYs);var t={height:this.maxY};return this._getOption("fitWidth")&&(t.width=this._getContainerFitWidth()),t},n._getContainerFitWidth=function(){for(var t=0,e=this.cols;--e&&0===this.colYs[e];)t++;return(this.cols-t)*this.columnWidth-this.gutter},n.needsResizeLayout=function(){var t=this.containerWidth;return this.getContainerWidth(),t!=this.containerWidth},i});
/*! jquery-countimator 14-10-2015 */
!function(a,b){function c(c,k){function l(){function d(){k.animateOnInit&&k.animateOnAppear&&i(c)&&(a(b).off("scroll touchmove",d),w.call(B))}var e,f,g,h=(o(),p()),j=n();h||(e=r(),e&&("number"!=typeof k.value?k.value=e.nodeValue:k.count=e.nodeValue)),j||(f=s(),f&&(k.max=f.nodeValue)),g=C.find("script[type*='text/x-']"),g.length&&(k.template=g.html(),g.remove()),a(b).on("resize",function(){q.call(B)}),a(b).on("scroll touchmove",d),k.animateOnInit?k.animateOnAppear&&i(c)?(k.count="number"==typeof h?h:0,w.call(B)):u.call(this):(k.count=o(),u.call(this)),q.call(this)}function m(){var a=parseFloat(k.min);return isNaN(a)?0:a}function n(){var a=parseFloat(k.max);return isNaN(a)?0:a}function o(){var a=(n(),m()),b=(p(),parseFloat(k.value));return isNaN(b)&&(b=a),b}function p(){var a=(n(),m()),b=parseFloat(k.count);return isNaN(b)&&(b=a),b}function q(){}function r(a){var b=C.find(k.countSelector)[0];return b||(b=C.find("*").last().siblings().addBack()[0]),h(b||c)[0]}function s(a){var b=C.find(k.maxSelector)[0];return b?h(b)[0]:null}function t(a){var b=k.decimals,c=k.decimalDelimiter,d=k.thousandDelimiter,e=f(a,b,c,d);return e=g(e,k.pad)}function u(){var e,f,g,h,i,j,l,q,u,v=n(),w=m(),x=o(),y=p(),z=t(y),A=t(x),B=t(v),C=t(w),D=k.engine||"undefined"!=typeof b.Handlebars?b.Handlebars:null,E=k.template;try{g=a(k.template),E=g.length&&g[0].innerHTML||E}catch(F){}D&&E?(h=D.compile(E),h&&(i=a.extend({},k,{count:z,value:A,max:B,min:C}),e=h(i)),f=document.createElement("div"),f.innerHTML=e,j=f.childNodes,a(c).contents().remove(),a(c).append(j)):(l=r(),l&&(l.nodeValue=z),q=s(),q&&(q.nodeValue=B),l||q||(c.innerHTML=z)),k.style&&(u=a.fn[d].getStyle(k.style),u&&u.render&&u.render.call(c,y,k))}function v(a){k.value=a,D||w()}function w(){D||(z=(new Date).getTime(),A=p(),D=!0,"function"==typeof k.start&&k.start.call(c),j(x))}function x(){var a=k.duration,b=(n(),o()),d=(new Date).getTime(),e=z+a,f=Math.min((a-(e-d))/a,1),g=A+f*(b-A);k.count=g,u.call(this),"function"==typeof k.step&&k.step.call(c,g,k),1>f&&D?j(x):y.call(this)}function y(){D=!1,"function"==typeof k.complete&&k.complete.call(c)}var z,A,B=this,C=a(c),D=!1;k=a.extend({},e,k,C.data()),this.resize=function(){q.call(this)},this.animate=function(a){v.call(this,a)},this.setOptions=function(b){var c=this.getOptions();a.extend(!0,k,b),k.value!==c.value&&w()},this.getOptions=function(){return a.extend(!0,{},k)},l.call(this)}var d="countimator",e={count:0,value:null,min:null,max:0,duration:1e3,countSelector:".counter-count",maxSelector:".counter-max",template:null,engine:null,animateOnInit:!0,animateOnAppear:!0,decimals:0,decimalDelimiter:".",thousandDelimiter:null,pad:!1,style:null,start:function(){},step:function(a){},complete:function(){}},f=function(a,b,c,d){b=isNaN(b=Math.abs(b))?2:b,c="undefined"==typeof c?".":c,d="undefined"==typeof d?",":d,d="string"==typeof d?d:"";var e=0>a?"-":"",f=Math.abs(+a||0).toFixed(b),g=String(parseInt(f)),h=g.length>3?g.length%3:0;return e+(h?g.substr(0,h)+d:"")+g.substr(h).replace(/(\d{3})(?=\d)/g,"$1"+d)+(b?c+Math.abs(f-g).toFixed(b).slice(2):"")},g=function(a,b){for(var c=""+a;c.length<b;)c="0"+c;return c},h=function(b){return a(b).contents().filter(function(){return 3===this.nodeType})},i=function(c){var d=a(c),e=a(b),f=e.scrollTop(),g=f+e.height(),h=d.offset().top,i=h+d.height();return g>=i&&h>=f},j=b.mozRequestAnimationFrame||b.webkitRequestAnimationFrame||b.msRequestAnimationFrame||b.oRequestAnimationFrame||function(a){b.setTimeout(a,1e3/60)};a.fn[d]=function(b){return this.each(function(){var e=a.extend(!0,{},b),f=a(this).data(d);return f?f.setOptions(e):a(this).data(d,new c(this,e)),a(this)})},function(){var b={};a.fn[d].registerStyle=function(a,c){b[a]=c},a.fn[d].getStyle=function(a){return b[a]}}()}(jQuery,window);
/*! jquery-countimator 14-10-2015 */
!function(a,b){function c(a,b,c,d,e,f){f="boolean"==typeof f?f:!1,d-=Math.PI/2,e-=Math.PI/2;var g,h,i,j,k="M "+a+", "+b;return e-d===2*Math.PI?k+=" m -"+c+", 0 a "+c+","+c+" 0 1,0 "+2*c+",0 a "+c+","+c+" 0 1,0 -"+2*c+",0":(g=a+Math.cos(d)*c,h=b+Math.sin(d)*c,i=a+Math.cos(e)*c,j=b+Math.sin(e)*c,k+=" L"+g+","+h+" A"+c+","+c+" 0 "+(e-d>Math.PI?1:0)+","+(f?0:1)+" "+i+","+j+" Z"),k}var d={render:function(b,d){var e=200,f=d.min?parseFloat(d.min):0,g=d.max?parseFloat(d.max):0,h=e,i=e,j=e,k=(b-f)/(g-f),l=k*Math.PI*2,m=c(h,i,j+1,0,l),n=a(this).find("> .counter-wheel-graphics"),o=a(this).find("> .counter-wheel-content");b=Math.min(g,Math.max(f,b)),o.length||a(this).prepend(a(this).wrapInner('<div class="counter-wheel-content"></div>')),n.length||(n=a('<svg preserveAspectRatio="none" class="counter-wheel-graphics" viewBox="0 0 '+2*e+" "+2*e+'"><path class="counter-wheel-highlight" d="M0 0" fill="teal"/></svg>'),a(this).prepend(n)),n.find("path").attr("d",m)}};if(!a.fn.countimator)throw"Include countimator script before style-plugin";a.fn.countimator.registerStyle("wheel",d)}(jQuery,window);
angular.module('myApp')
    .controller("seatCtrl",["$scope", "Restangular", "$uibModal", function ($scope, Restangular, $uibModal) {
        $scope.floor_details = window.floor_details;
        $scope.users = window.users;
        $scope.editSeat = function (seat) {
            $scope.list = {
                seat_info: seat,
                users: $scope.users
            };
            var modal = $uibModal.open({
                controller: "seatModalCtrl",
                windowClass: "bootstrap_wrap",
                templateUrl: "/views/seat-management/seat_assign.html",
                backdrop: "static",
                size: "md",
                resolve: {
                    data: function () {
                        return $scope.list;
                    }
                }
            });
            modal.result.then(function (res) {  }, function () { });
        };

    }]);
angular.module('myApp')
    .controller('seatModalCtrl',["$scope", "Restangular", "data", "$uibModalInstance", "$sce", function ($scope, Restangular, data, $uibModalInstance, $sce) {
        $scope.items = data;
        $sce.trustAsHtml = $scope.items;
        $scope.selected_user = $scope.items.seat_info.user_details.assigned_user;
        $scope.error_message = '';
        $scope.select_user = function (data) {
            $scope.selected_user = data;
        }
        $scope.saveData = function () {
            var params = {
                seat_id: $scope.items.seat_info.id,
                user_id: ($scope.selected_user) ? $scope.selected_user.id : '',
                seat_name: $scope.items.seat_info.name ? $scope.items.seat_info.name : ''
            };

            Restangular.all('seats').post(params).then(
                function (response) {
                    $scope.finalsubmit = false;
                    if (response.status) {
                        $scope.error_message = '';
                        $uibModalInstance.close(data);
                        window.location.reload();
                    }
                },
                function (errors) {
                    $scope.error_message = errors.data.message;
                }
            );
        };
        $scope.releaseUser = function (data) {
            Restangular.one("seats", data).remove()
            .then(
                function(response) {
                    $scope.finalsubmit = false;
                    if (response.status) {
                        $uibModalInstance.close(data);
                        window.location.reload();
                    }
                    },
                    function(error) {
                    }
                );
            
        }

        $scope.cancel = function () {
            $uibModalInstance.dismiss();
        };
    }]);
angular.module('myApp')
    .controller("credentialModalCtrl",["$scope", "Restangular","data", "$uibModalInstance", function ($scope, Restangular,data,$uibModalInstance) {
    
    $scope.items = data;
    $scope.selectedUsers = [];
    var list_params = {
                credential_id: $scope.items.credential.id ? $scope.items.credential.id  : ''
            };

    Restangular.all("user").getList({'pagination' : 0})
          .then(
            function(response) {
                $scope.loading = false;
                $scope.userList = response;
              },
            function(errors) {
                $scope.loading = false;
                $scope.error_message = errors.data.message;
            }
          );        
                    
    Restangular.all('shared-users').post(list_params).then(
                function (response) {
                    for (var i = 0; i < response.length; i++) {
                        $scope.selectedUsers.push(response[i]);
                    }
                    $scope.finalsubmit = false;
                },
                function (errors) {
                    $scope.error_message = errors.data.message;
                    // $scope.errorMessage = errors.data.message.message;
                }
            );
           
    $scope.message = '';

    $scope.select_users = function (data) {
            $scope.message = '';
            for (var i = data.length - 1; i < data.length; i++) {
                $scope.selectedUsers.push(data[i]);
            }
            var params = {
                selected_users: ($scope.selectedUsers) ? $scope.selectedUsers : '',
                credential_id: $scope.items.credential.id ? $scope.items.credential.id  : ''
            };
            Restangular.all('credential-sharing').post(params).then(
                function (response) {
                    $scope.finalsubmit = false;
                    if (response.success) {
                        $scope.error_message = '';
                        $scope.message = 'Saved Successfully';
                    }
                },
                function (errors) {
                    $scope.error_message = errors.data.message;
                    // $scope.errorMessage = errors.data.message.message;
                }
            );
        }

    $scope.deselect_users = function (data) {
        $scope.message = '';
        $scope.selectedUsers = $scope.selectedUsers.filter(function(item){ item.id !== data.id});
        var params = {
                selected_users: ($scope.selectedUsers) ? $scope.selectedUsers : '',
                credential_id: $scope.items.credential.id ? $scope.items.credential.id  : ''
            };
            
            Restangular.all('credential-sharing').post(params).then(
                function (response) {
                    $scope.finalsubmit = false;
                    if (response.success) {
                        $scope.error_message = '';
                        $scope.message = 'Saved Successfully';
                    }
                },
                function (errors) {
                    $scope.error_message = errors.data.message;
                    // $scope.errorMessage = errors.data.message.message;
                }
            );
            
        }

    $scope.cancel = function() {
      $uibModalInstance.dismiss();
    //   window.location.reload();
    }
    }]);
angular.module('myApp')
    .controller("credentialIndexCtrl",["$scope", "Restangular", "$uibModal", function ($scope, Restangular, $uibModal) {

        $scope.credentials = JSON.parse(window.credentials);
        angular.element(document).ready(function () {
            dTable = $('#credential_table');
            dTable.DataTable({"pageLength": 1000});
        });
        $scope.showModal = function (credential) {
            var modal = $uibModal.open({
                controller: "credentialModalCtrl",
                windowClass: "bootstrap_wrap",
                templateUrl: "/views/share-credentials/share-credentials.html",
                backdrop: "static",
                size: "md",
                resolve: {
                    data: function () {
                        return { credential: credential };
                    }
                }
            });
        }; 

        $scope.delete = function(credential) {
        if (confirm("Are you sure you want to delete this credential?")) {
            Restangular.one("credential", credential.id)
            .remove()
            .then(
                function(response) {
                    $scope.message = 'Deleted Successfully';
                    window.location.reload();
                },
                function(error) {
                    $scope.error_message = errors.data.message;
                }
            );
        }
        };


    }])
angular.module("myApp").controller("adminLeaveApplyModalCtrl", [
  "$scope",
  "Restangular",
  "$uibModalInstance",
  function($scope, Restangular, $uibModalInstance) {
    $scope.loading = true;
    $scope.error = false;
    $scope.userLeaveInfo = [];
    Restangular.all("user")
      .getList({ pagination: 0 })
      .then(
        function(response) {
          $scope.loading = false;
          $scope.userList = response;
        },
        function(errors) {
          $scope.loading = false;
          $scope.error_message = errors.data.message;
        }
      );

    var getLeaveTypes = function() {
      Restangular.all("leave-types")
        .getList()
        .then(
          function(response) {
            $scope.leaveTypes = response;
            var otherLeaveType = {
              id: 0,
              code: "other",
              title: "Others"
            };
            $scope.leaveTypes.push(otherLeaveType);
            console.log($scope.leaveTypes);
          },
          function(errors) {
            console.log(errors);
          }
        );
    };
    getLeaveTypes();

    let getUserLeaveDetails = function(user_id) {
      let params = {
        user_id: user_id
      }
      Restangular.one("user-leave-details").get(params).then(
        function (response) {
          $scope.userLeaveInfo = response;
          $scope.form.leave_info_fetched = true;
        },
        function(errors) {
          console.log(errors);
        }
      )
    };

    $scope.datePicker = {
      options: {
        applyClass: "btn-success",
        locale: {
          applyLabel: "Apply",
          fromLabel: "From",
          // format: "YYYY-MM-DD", //will give you 2017-01-06
          format: "D-MMM-YY", //will give you 6-Jan-17
          // format: "D-MMMM-YY", //will give you 6-January-17
          toLabel: "To",
          cancelLabel: "Cancel",
          customRangeLabel: "Custom range"
        },
        eventHandlers: { 'apply.daterangepicker' : function () {
          console.log($scope.datePicker.date.startDate,$scope.datePicker.date.endDate);
          var params = {
            start_date: moment($scope.datePicker.date.startDate).format("YYYY-MM-DD"),
            end_date: moment($scope.datePicker.date.endDate).format("YYYY-MM-DD")
          }
          
          Restangular.one("leave-working-days").get(params).then(
            function (response) {
              $scope.form.duration = response;
            },
            function(errors) {
              console.log(errors);
            }
          )
          } 
        }
      },
      date: { startDate: new Date(), endDate: new Date() }
    };

    $scope.form = {
      leave_info_fetched: false,
      loading: false,
      submitted: false,
      selected_user: [],
      status: "approved",
      reason: "",
      half: "full",
      half_day_flag: false,
      leave_type_id: null,
      radio_selection: null,
      duration: 1,
      errors: {
        selected_user: false,
        reason: false,
        type: false,
        date: false
      },
      select_user: function(data) {
        $scope.form.selected_user = data;
        $scope.form.errors.selected_user = false;
        getUserLeaveDetails($scope.form.selected_user.id);
      },
      submit: function(userForm) {
        
        $scope.form.submitted = true;
        if ($scope.form.selected_user.length == 0) {
          $scope.form.errors.selected_user = true;
        }
        if (userForm.$valid && !$scope.form.errors.selected_user) {
          $scope.form.loading = true;
          $scope.error = false;
          var startDate = moment($scope.datePicker.date.startDate).format(
            "YYYY-MM-DD"
          );
          var endDate = moment($scope.datePicker.date.endDate).format(
            "YYYY-MM-DD"
          );
          if ( $scope.form.leave_type_id == 0 ) {
            $scope.form.leave_type_id = $scope.form.radio_selection;
          }
          var params = {
            user_id: $scope.form.selected_user ? $scope.form.selected_user.id : "",
            start_date: startDate,
            end_date: endDate,
            type: $scope.form.leave_type_id,
            half: $scope.form.half,
            status: $scope.form.status,
            reason: $scope.form.reason,
            skip_overlapping_check: true
          };

          Restangular.all("leave-new")
            .post(params)
            .then(
              function(response) {
                $scope.form.loading = false;
                $scope.response = response;
                if (response.status) {
                  $scope.success = true;
                  $scope.error = false;
                  $uibModalInstance.dismiss();
                  window.location.reload();
                } else {
                  $scope.error = true;
                  $scope.success = false;
                  $scope.error_message = response.data.message;
                }
              },
              function(errors) {
                $scope.form.loading = false;
                $scope.error = true;
                $scope.success = false;
                $scope.error_message = errors.data.message;
              }
            );
        }
      }
    };

    $scope.$watch(
      function($scope) { return $scope.form.leave_type_id },
      function() {
        $scope.form.half_day_flag = false;
        if ( $scope.leaveTypes && $scope.form.duration == 1 )
        {
          for ( i = 0; i < $scope.leaveTypes.length; i++ )
          {
            if ( $scope.form.leave_type_id == $scope.leaveTypes[i].id && $scope.leaveTypes[i].code == 'sick'  )
            {
              $scope.form.half_day_flag = true;
            }
          }
        }
      }
    );

    $scope.$watch(
      function($scope) { return $scope.form.duration },
      function() {
        $scope.form.half_day_flag = false;
        if ( $scope.leaveTypes && $scope.form.duration == 1 )
        {
          for ( i = 0; i < $scope.leaveTypes.length; i++ )
          {
            if ( $scope.form.leave_type_id == $scope.leaveTypes[i].id && $scope.leaveTypes[i].code == 'sick'  )
            {
              $scope.form.half_day_flag = true;
            }
          }
        }
      }
    );


    // $scope.submitForm = function(userForm) {
    //   $scope.submitted = true;
    //   if ($scope.selected_user.length == 0) {
    //     $scope.selected_user_error = true;
    //   }
    //   if (userForm.$valid && !$scope.selected_user_error) {
    //     $scope.loading = true;
    //     $scope.error = false;
    //     var startDate = moment($scope.datePicker.date.startDate).format(
    //       "YYYY-MM-DD"
    //     );
    //     var endDate = moment($scope.datePicker.date.endDate).format(
    //       "YYYY-MM-DD"
    //     );
    //     var params = {
    //       user_id: $scope.selected_user ? $scope.selected_user.id : "",
    //       start_date: startDate,
    //       end_date: endDate,
    //       type: $scope.type,
    //       half: $scope.half,
    //       status: $scope.status,
    //       reason: $scope.reason,
    //       skip_overlapping_check: true
    //     };

    //     Restangular.all("leave")
    //       .post(params)
    //       .then(
    //         function(response) {
    //           $scope.loading = false;
    //           $scope.response = response;
    //           if (response.status) {
    //             $scope.success = true;
    //             $scope.error = false;
    //             $uibModalInstance.dismiss();
    //             window.location.reload();
    //           } else {
    //             $scope.error = true;
    //             $scope.success = false;
    //             $scope.error_message = response.data.message;
    //           }
    //         },
    //         function(errors) {
    //           $scope.loading = false;
    //           $scope.error = true;
    //           $scope.success = false;
    //           $scope.error_message = errors.data.message;
    //         }
    //       );
    //   }
    // };
    $scope.cancel = function() {
      $uibModalInstance.dismiss();
    };
  }
]);

angular
  .module("myApp")
  .controller("adminLeaveEditModalCtrl", ["$scope",
    "Restangular",
    "data",
    "$uibModalInstance",
    function (
      $scope,
      Restangular,
      data,
      $uibModalInstance
    ) {
      $scope.loading = true;
      // Action Loadings
      $scope.actionLoading = {
        update: false,
        cancel: false,
        reject: false,
        approve: false
      };
      // Fetch List of all clients
      Restangular.all("company")
        .getList()
        .then(
          function (response) {
            $scope.companyList = response;
          },
          function (errors) {
            $scope.error_message = errors.data.message;
          }
        );
      // Date Picker
      $scope.datePicker = {
        options: {
          applyClass: "btn-success",
          locale: {
            applyLabel: "Apply",
            fromLabel: "From",
            // format: "YYYY-MM-DD", //will give you 2017-01-06
            format: "D-MMM-YY", //will give you 6-Jan-17
            // format: "D-MMMM-YY", //will give you 6-January-17
            toLabel: "To",
            cancelLabel: "Cancel",
            customRangeLabel: "Custom range"
          }
        },
        date: {
          startDate: null,
          endDate: null
        }
      };
      // Getting Leave Types
      var getLeaveTypes = function () {
        Restangular.all("leave-types")
          .getList()
          .then(
            function (response) {
              $scope.leaveTypes = response;
            },
            function (errors) {
              console.log(errors);
            }
          );
      };
      getLeaveTypes();

      $scope.deduct_list = [{
          value: "lop",
          name: "Loss of Pay"
        },
        {
          value: "sl",
          name: "Sick Leaves"
        },
        {
          value: "pl",
          name: "Paid Leaves"
        },
        {
          value: "slpl",
          name: "Sick Leaves & Paid Leaves"
        },
        {
          value: "other_paid",
          name: "Other Paid"
        }
      ];

      // Initialising and assigning variables
      $scope.id = data;
      $scope.error = false;
      $scope.success = false;
      $scope.error_message = "";
      $scope.is_team_lead = false;
      $scope.lead_status = 'pending';
      $scope.label_type = "";
      $scope.start_date = null;
      $scope.end_date = null;
      $scope.selected_client = [];
      $scope.selected_client_error = false;
      $scope.deductionFlag = false;
      $scope.deductionType = null;
      $scope.deductionType_error = false;
      // Fetch leave Details
      var leaveObject = Restangular.one("leave-new", $scope.id);
      leaveObject.get().then(
        function (response) {
          if (response.status) {
            $scope.error = false;
            $scope.success = true;
            $scope.leave = response.data.leave;
            console.log('$scope.leave', $scope.leave);
            $scope.previousLeaves = response.data.previousLeaves;
            $scope.managerStatuses = response.data.managerStatuses;
            $scope.overlappingLeaves = response.data.overlappingLeaves;
            $scope.leaveOverlapping = response.data.leaveOverlapping;
            $scope.is_team_lead = response.data.leadStatuses.is_team_lead;
            $scope.lead_status = response.data.leadStatuses.lead_status;
            $scope.is_management = response.data.isManagement;
            $scope.is_other_type = response.data.isOtherType;
            $scope.datePicker.date.startDate = moment(
              response.data.leave.start_date
            );
            $scope.datePicker.date.endDate = moment(
              response.data.leave.end_date
            );
            $scope.set_label($scope.leave.status);
            $scope.created_date = new Date($scope.leave.created_at);
            $scope.loading = false;

            if ($scope.leave.leave_deduction) {
              $scope.deductionFlag = true;
              if ($scope.leave.leave_deduction.loss_of_pay > 0) {
                $scope.deductionType = 'lop';
              } else if ($scope.leave.leave_deduction.other_paid > 0) {
                $scope.deductionType = 'other_paid';
              } else if ($scope.leave.leave_deduction.sick_leave > 0 && $scope.leave.leave_deduction.paid_leave == 0) {
                $scope.deductionType = 'sl';
              } else if ($scope.leave.leave_deduction.sick_leave == 0 && $scope.leave.leave_deduction.paid_leave > 0) {
                $scope.deductionType = 'pl';
              } else if ($scope.leave.leave_deduction.sick_leave > 0 && $scope.leave.leave_deduction.paid_leave > 0) {
                $scope.deductionType = 'slpl';
              } else {
                $scope.deductionType = null;
              }
            }

          } else {
            $scope.error = true;
            $scope.success = false;
            $scope.loading = false;
            $scope.error_message = "Something went wrong";
          }
        },
        function (errors) {
          $scope.loading = false;
          $scope.error = true;
          $scope.success = false;
          $scope.error_message = errors.data.message;
        }
      );
      $scope.select_client = function (data) {
        $scope.selected_client = data;
        $scope.selected_client_error = false;
      };

      $scope.set_label = function (status) {
        if (status == "approved") $scope.label_type = "label-success";
        else if (status == "pending") $scope.label_type = "label-info";
        else if (status == "cancelled") $scope.label_type = "label-primary";
        else $scope.label_type = "label-danger";
      };

      $scope.updateLeave = function () {
        if ($scope.leave.status != 'pending') {
          $scope.actionLoading.reject = false;
          alert("Leave is no longer in pending state and hence can not be modified");
        }
        $scope.actionLoading.update = true;
        leaveObject = Restangular.copy($scope.leave);
        var startDate = moment($scope.datePicker.date.startDate).format(
          "YYYY-MM-DD"
        );
        var endDate = moment($scope.datePicker.date.endDate).format("YYYY-MM-DD");
        leaveObject.start_date = startDate;
        leaveObject.end_date = endDate;

        Restangular.one("leave-new", leaveObject.id)
          .customPUT(leaveObject, "update")
          .then(
            function (response) {
              $scope.error = false;
              $scope.actionLoading.update = false;
              // $uibModalInstance.dismiss();
              // window.location.reload();
            },
            function (errors) {
              $scope.actionLoading.update = false;
              $scope.error = true;
              $scope.error_message = errors.data.message;
            }
          );
      };

      $scope.rejectLeave = function () {
        $scope.actionLoading.reject = true;
        if ($scope.leave.status != 'pending') {
          $scope.actionLoading.reject = false;
          alert("Leave is no longer in pending state and hence can not be rejected");
        }
        if (
          $scope.leave.cancellation_reason == "" ||
          $scope.leave.cancellation_reason == null
        ) {
          $scope.actionLoading.reject = false;
          alert("You must specify the reason for rejecting the leave");
        } else {
          leaveObject.cancellation_reason = $scope.leave.cancellation_reason;
          leaveObject.status = "reject";
          Restangular.one("leave-new", leaveObject.id)
            .customPUT(leaveObject, "reject")
            .then(
              function (response) {
                $scope.error = false
                $scope.actionLoading.reject = false;
                $uibModalInstance.dismiss();
                window.location.reload();
              },
              function (errors) {
                $scope.actionLoading.reject = false;
                $scope.error = true;
                $scope.error_message = errors.data.message;
              }
            );
        }
      };

      $scope.approveLeave = function () {
        if ( !($scope.leave.leave_type.code != 'sick' || $scope.leave.leave_type.code != 'paid') )
        {
         if (!$scope.deductionType ) {
          $scope.deductionType_error = true;
          return;
           }
        }
        
        $scope.actionLoading.approve = true;
        if ($scope.leave.status != 'pending') {
          $scope.actionLoading.approve = false;
          alert("Leave is no longer in pending state and hence can not be approved");
        }
        leaveObject = Restangular.copy($scope.leave);
        leaveObject.status = "approve";
        var startDate = moment($scope.datePicker.date.startDate).format(
          "YYYY-MM-DD"
        );
        var endDate = moment($scope.datePicker.date.endDate).format("YYYY-MM-DD");
        leaveObject.start_date = startDate;
        leaveObject.end_date = endDate;
        if ($scope.deductionType) {
          leaveObject.deduction_flag = true;
          leaveObject.deduction_type = $scope.deductionType;
        }
        console.log(leaveObject.deduction_type);
        Restangular.one("leave-new", leaveObject.id)
          .customPUT(leaveObject, "approve")
          .then(
            function (response) {
              $scope.actionLoading.approve = false;
              $scope.error = false;
              $uibModalInstance.dismiss();
              window.location.reload();
            },
            function (errors) {
              $scope.actionLoading.approve = false;
              $scope.error = true;
              $scope.error_message = errors.data.message;
            }
          );
      };

      $scope.cancelLeave = function () {
        $scope.actionLoading.cancel = true;
        if ($scope.leave.status == 'rejected' || $scope.leave.status == 'cancelled') {
          $scope.actionLoading.cancel = false;
          alert("Leave is no longer in pending/approved state and hence can not be cancelled");
        }
        leaveObject.status = "cancelled";
        Restangular.one("leave-new", leaveObject.id)
          .customPUT(leaveObject, "cancel")
          .then(
            function (response) {
              $scope.actionLoading.cancel = false;
              $uibModalInstance.dismiss();
              window.location.reload();
            },
            function (errors) {
              $scope.actionLoading.cancel = false;
              $scope.error = true;
              $scope.error_message = errors.data.message;
            }
          );
      };

      $scope.showRejectButton = function () {
        if ($scope.leave) {
          if ($scope.is_management && !$scope.loading) {
            return true;
          } else if ($scope.is_other_type) {
            return false;
          } else if (!$scope.is_team_lead && $scope.leave.status == 'pending' && !$scope.loading) {
            return true;
          } else if ($scope.lead_status == 'pending' && $scope.is_team_lead && !$scope.loading) {
            return true;
          } else {
            return false;
          }
        } else {
          return false;
        }

      };

      $scope.showApproveButton = function () {
        if ($scope.leave) {
          if ($scope.is_management && !$scope.loading) {
            return true;
          } else if ($scope.is_other_type) {
            return false;
          } else if (!$scope.is_team_lead && $scope.leave.status == 'pending' && !$scope.loading) {
            return true;
          } else if ($scope.lead_status == 'pending' && $scope.is_team_lead && !$scope.loading) {
            return true;
          } else {
            return false
          }
        } else {
          return false;
        }


      };

      $scope.showUpdateButton = function () {
        if ($scope.leave) {
          if ($scope.is_management && !$scope.loading) {
            return true;
          } else if ($scope.is_other_type) {
            return false;
          } else if ($scope.leave.status == 'pending' && !$scope.loading && !$scope.is_team_lead) {
            return true;
          } else {
            return false;
          }
        } else {
          return false;
        }

      };

      $scope.showCancelButton = function () {
        if ($scope.leave) {
          if ($scope.is_management && !$scope.loading) {
            return true;
          } else if ($scope.is_other_type) {
            return false;
          } else if ($scope.leave.status != 'rejected' && $scope.leave.status != 'cancelled' && !$scope.loading && !$scope.is_team_lead) {
            return true;
          } else {
            return false;
          }
        } else {
          return false;
        }


      };



      $scope.close = function () {
        $uibModalInstance.dismiss()
        window.location.reload();
      };
    }
  ]);
angular.module("myApp").controller("reportingManagerLeaveCtrl", [
    "$scope",
    "Restangular",
    "$location",
    "$log",
    "$uibModal",
    function ($scope, Restangular, $location, $log, $uibModal) 
    {
        $scope.showModal = function (id) 
        {
            $scope.list = id;
            var modal = $uibModal.open({
                controller: "adminLeaveEditModalCtrl",
                windowClass: "bootstrap_wrap",
                templateUrl: "/views/leave/admin-leave-edit-modal-new.html",
                backdrop: "static",
                size: "lg",
                resolve: {
                    data: function () {
                        return $scope.list;
                    }
                }
            });
        }
    }
]);

angular
  .module("myApp")
  .controller("lateListCtrl", ["$scope", "Restangular",function ($scope, Restangular) {
    $scope.loading = false;
    $scope.error = false;
    $scope.error_message = null;
    Restangular.all("user").getList({
        'pagination': 0
      })
      .then(
        function (response) {
          $scope.loading = false;
          $scope.userList = response;
        },
        function (errors) {
          $scope.loading = false;
          $scope.error_message = errors.data.message;
        }
      );

    $scope.selected_user = [];
    $scope.selected_user_error = false;

    $scope.submitted = false;

    $scope.select_user = function (data) {
      $scope.selected_user = data;
      $scope.selected_user_error = false;
      $scope.error = false;
    };
    $scope.datePicker = {
      options: {
        applyClass: "btn-success",
        singleDatePicker: true,
        locale: {
          applyLabel: "Apply",
          fromLabel: "From",
          format: "D-MMM-YY", //will give you 6-Jan-17
          cancelLabel: "Cancel",
        }
      },
      date: {
        selectedDate: new Date()
      }
    };
    $scope.$watch("datePicker.date.selectedDate", function () {
      selectedDate = moment($scope.datePicker.date.selectedDate).format(
        "YYYY-MM-DD"
      );
      $scope.getLateList(selectedDate);
    });

    $scope.getLateList = function (date) {
      $scope.loading = true;
      Restangular.all("late-comers").getList({
          'pagination': 0,
          'date': date,
          'relations': 'user,reportedBy'
        })
        .then(
          function (response) {
            $scope.loading = false;
            $scope.latelist = response;
          },
          function (errors) {
            $scope.loading = false;
            $scope.error_message = errors.data.message;
          }
        );
    };

    $scope.addToLateList = function () {
      $scope.loading = true;
      date = moment($scope.datePicker.date.selectedDate).format("YYYY-MM-DD");
      var params = {
        date: date,
        user: $scope.selected_user
      };

      Restangular.all("late-comers")
        .post(params)
        .then(
          function (response) {
            $scope.loading = false;
            $scope.getLateList(date);
            $scope.selected_user = [];
            $scope.selected_user_error = false;
            $scope.error_message = null;
          },
          function (errors) {
            $scope.loading = false;
            $scope.error = true;
            $scope.error_message = errors.data.message;
          }
        );
    };
    $scope.filteredDate = function (input_date) {
      var formatted_date = new Date(input_date);
      return formatted_date;
    };
    $scope.delete = function (item) {
      Restangular.one("late-comers", item.id)
        .remove()
        .then(
          function (response) {
            $scope.latelist.splice($scope.latelist.indexOf(item), 1);
          },
          function (error) {
            alert(error.data.message);
          }
        );
    };
  }]);
angular
    .module("myApp")
    .controller("goalCtrl",["$scope", "Restangular", "$uibModal", "$log", "Goal", "$location", function ($scope, Restangular, $uibModal, $log, Goal, $location) {
        $scope.test = "success";
        var startDate;
        var endDate;
        var userGoalId = null;
        var userId = '';
        $scope.submitted = false;
        $scope.loading = false;
        $scope.saveAsDraftLoading = false;
        $scope.setGoalLoading = false;
        $scope.data = {
            goals: [],
            tags: [],
            selected_tags: [],
            userGoalItems: [],
            newGoal: {
                title: "",
                success_metric: "",
                process: "",
                tags: []
            },
            newIdp: {
                title: "",
                process: ""
            }
        };
        Restangular.all("myUsers").getList({
                'pagination': 0
            })
            .then(
                function (response) {
                    $scope.loading = false;
                    $scope.userList = response;
                },
                function (errors) {
                    $scope.loading = false;
                    $scope.error_message = errors.data.message;
                }
            );
        $scope.select_user = function (data) {
            $scope.selected_user = data;
            $scope.form.userSelectError = false;
        };

        if (moment().quarter() == 1) {
            startDate = moment().subtract(1, "year").month("April").startOf("month");
            endDate = moment().month("March").endOf("month");
        } else {
            startDate = moment().month("April").startOf("month");
            endDate = moment().add(1, "year").month("March").endOf("month");
        }

        $scope.datePicker = {
            date: {
                startDate: startDate,
                endDate: endDate
            },
            options: {
                alwaysShowCalendars: true,
                locale: {
                    format: "D-MMM-YY", // 
                },
                ranges: {
                    "Last Year": [moment(startDate).subtract(1, "years"), moment(endDate).subtract(1, "years")]
                }
            }
        };
        $scope.form = {
            data: {
                start_date: moment($scope.datePicker.date.startDate).format(
                    "YYYY-MM-DD"
                ),
                end_date: moment($scope.datePicker.date.endDate).format("YYYY-MM-DD"),
                user_goal_id: 1,
                goals: [],
                newGoal: {
                    id: null,
                    title: "",
                    tags: [],
                    success_metric: "",
                    process: "",
                    is_idp: false,
                    is_open: false
                }
            },
            errors: [],
            close_others: true,
            addGoal: function (index) {
                var goal = $scope.data.goals[index] ? $scope.data.goals[index] : '';
                var length = goal.tags.length;
                tags = [];
                for (i = 0; i < length; i++) {
                    tags.push(goal.tags[i].name);
                }
                $scope.form.data.goals.push({
                    id: null,
                    title: goal.title,
                    tags: tags,
                    success_metric: goal.success_metric,
                    process: goal.process,
                    is_idp: false,
                    is_open: false
                });
            },
            createGoal: function (is_idp) {
                if (is_idp) {
                    $scope.form.data.newGoal.tags = [];
                    $scope.form.data.newGoal.success_metric = "";
                }
                var goal = $scope.form.data.newGoal;
                if (is_idp) {
                    goal.is_idp = true;
                }
                $scope.submitted = true;
                $scope.error = {
                    title: 0,
                    success_metric: 0,
                    process: 0,
                    tags: 0
                };
                if (goal.title == null || goal.title == "") {
                    $scope.error.title = 1;
                }
                if (!is_idp && (goal.success_metric == null || goal.success_metric == "")) {
                    $scope.error.success_metric = 1;
                }
                if (goal.process == null || goal.process == "") {
                    $scope.error.process = 1;
                }
                if (goal.tags.length == 0 && !is_idp ) {
                    $scope.error.tags = 1;
                }
                if ($scope.error.title == 0 && $scope.error.tags == 0 && $scope.error.success_metric == 0 && $scope.error.process == 0 ) {
                    $scope.form.data.goals.push(goal);
                    $scope.submitted = false;
                    $scope.form.data.newGoal = {
                        id: null,
                        title: "",
                        tags: [],
                        success_metric: "",
                        process: "",
                        is_idp: false,
                        is_open: false
                    };
                }
            },
            removeGoal: function (index) {
                $scope.form.data.goals.splice(index, 1);
            },
            store: function (option) {
                $scope.form.errors = [];
                $scope.form.close_others = true;
                var result = $scope.form.validate();
                $scope.form.errors = result.errors;
                $scope.form.userSelectError = result.userSelectError;
                if (result.count == 0) {
                    $scope.form.close_others = false;
                    $scope.form.updateUserGoals(option);
                }
            },
            validate: function () {
                var length = $scope.form.data.goals.length;
                var errors = [];
                var userSelectError = false;
                count = 0;
                for (i = 0; i < length; i++) {
                    var goal = $scope.form.data.goals[i];
                    errors[i] = [];
                    if (goal.title == null || goal.title == "") {
                        errors[i].push("title");
                        count++;
                        $scope.form.data.goals[i].is_open = true;
                    }
                    if (goal.tags.length == 0 && !goal.is_idp ) {
                        errors[i].push("tags");
                        count++;
                        $scope.form.data.goals[i].is_open = true;
                    }
                    if (!goal.is_idp && (goal.success_metric == null || goal.success_metric == "")) {
                        errors[i].push("success_metric");
                        count++;
                        $scope.form.data.goals[i].is_open = true;
                    }
                    if (goal.process == null || goal.process == "") {
                        errors[i].push("process");
                        count++;
                        $scope.form.data.goals[i].is_open = true;
                    }
                }
                if ( $scope.selected_user == null || $scope.selected_user == '' )
                {
                    userSelectError = true;
                    count++;
                }

                return {
                    errors: errors,
                    count: count,
                    userSelectError : userSelectError
                };
            },
            updateUserGoals: function (option) {
                if ( option == 'pending' )
                {
                    $scope.saveAsDraftLoading = true;
                }
                else
                {
                    $scope.setGoalLoading = true;
                }
                console.log('$scope.saveAsDraftLoading', $scope.saveAsDraftLoading);

                $scope.loading = true;
                $scope.error = false;
                var startDate = moment($scope.datePicker.date.startDate).format(
                    "YYYY-MM-DD"
                );
                var endDate = moment($scope.datePicker.date.endDate).format(
                    "YYYY-MM-DD"
                );

                var params = {
                    user_id: ($scope.selected_user) ? $scope.selected_user.id : '',
                    from_date: startDate,
                    to_date: endDate,
                    status: option
                  };
                 
                if ( userGoalId == null )
                {
                    Restangular.all("user-goals")
                    .post(params)
                    .then(
                    function(response) {
                        userGoalId = response.goalObj.id;
                        userId = response.goalObj.user_id;
                        Restangular.one("user-goal-items", userGoalId)
                            .customPUT({
                                goals: $scope.form.data.goals
                            })
                            .then(function (response) {
                                $scope.saveAsDraftLoading = false;
                                $scope.setGoalLoading = false;
                                if ( option != 'pending' )
                                {
                                    var path = "/admin/goal-user-list/"+userId+"/"+userGoalId+"/view";
                                    window.location.href =  path;
                                }
                                    
                            });
                        $scope.saveAsDraftLoading = false;
                        $scope.setGoalLoading = false;
                        $scope.success = true;
                        
                    },
                    function(errors) {
                        $scope.saveAsDraftLoading = false;
                        $scope.setGoalLoading = false;
                        $scope.error = true;
                        $scope.success = false;
                    }
                    );
                }
                else 
                {
                    Restangular.one("user-goal-items", userGoalId)
                    .customPUT({
                        goals: $scope.form.data.goals,
                        status: option,
                        flag: 'new'
                    })
                    .then(function (response) {
                        $scope.saveAsDraftLoading = false;
                        $scope.setGoalLoading = false;
                        if ( option != 'pending' )
                        {
                            var path = "/admin/goal-user-list/"+userId+"/"+userGoalId+"/view";
                            window.location.href =  path;
                        }
                    },
                    function(errors) {
                        $scope.saveAsDraftLoading = false;
                        $scope.setGoalLoading = false;
                        $scope.error = true;
                        $scope.success = false;
                    }
                );
                }
                
            }
        };

        $scope.search = {
            text: "",
            tags: []
        };
        $scope.GoalData = null;
        $scope.select2Options = {
            allowClear: true
        };

        Restangular.one("tags")
            .get()
            .then(function (response) {
                $scope.data.tags = response;
            });

        Goal.getGoals($scope.search);
        $scope.GoalData = Goal.data;

        $scope.$watch("GoalData.loaded", function () {
            if ($scope.GoalData.loaded == true) {
                $scope.data.goals = $scope.GoalData.goals;
            }
        });
        // $scope.$watch("GoalData.userGoalItemsLoaded", function () {
        //     if ($scope.GoalData.userGoalItemsLoaded == true) {
        //         $scope.form.data.goals = $scope.GoalData.userGoalItems;
        //     }
        // });

        $scope.filter = function () {
            Goal.getGoals($scope.search);
        };
        params = {
            start_date: moment($scope.datePicker.date.startDate).format(
                "YYYY-MM-DD"
            ),
            end_date: moment($scope.datePicker.date.endDate).format("YYYY-MM-DD")
        };

        Goal.getUserGoals(1, params);

        $scope.saveGoal = function (is_private) {
            var params = $scope.data.newGoal;
            Goal.saveGoal(params, is_private, false);
        };

        $scope.saveIdp = function () {
            var params = $scope.data.newIdp;
            Goal.saveGoal(params, true, true);
        };

        $scope.attachGoal = function (goal_id) {
            Goal.attachGoal(1, goal_id, false, false);
        };

        $scope.detachGoal = function (item_id) {
            Goal.detachGoal(item_id);
        };

        $scope.updateUserGoals = function () {
            params = {
                user_goal_items: $scope.data.userGoalItems
            };
            Goal.updateUserGoals(params);
        };

        $scope.$watch("GoalData.newGoalAdded", function () {
            if ($scope.GoalData.newGoalAdded == true) {
                params = {
                    start_date: moment($scope.datePicker.date.startDate)
                        .add("330", "m")
                        .format("YYYY-MM-DD"),
                    end_date: moment($scope.datePicker.date.endDate)
                        .add("330", "m")
                        .format("YYYY-MM-DD")
                };
                $scope.data.newGoal = [];
                Goal.getUserGoals(1, params);
            }
        });
    }]);
angular.module("myApp").service("Goal", ["Restangular", "$location", "$log",
    function Goal(Restangular, $location, $log) {
        dataobject = {
            goals: [],
            userGoalItems: [],
            page: 1,
            per_page: 5,
            loaded: false,
            userGoalItemsLoaded: false,
            newGoalAdded: false,
            userGoalLoaded: false,
            userGoal: []
        };

        function getGoals(search) {
            dataobject.loaded = false;
            Restangular.all("goals")
                .customGET("filter", {
                    "name": search.text,
                    "tags[]": search.tags,
                    "page": dataobject.page,
                    "per_page": dataobject.per_page
                })
                .then(function (response) {
                    dataobject.goals = response.data;
                    dataobject.loaded = true;
                });
        }

        function updateUserGoals(params) {
            dataobject.loaded = false;
            $log.log(params);
            Restangular.one("user-goal-items", 1)
                .customPUT(params)
                .then(function (response) {
                    $log.log(response.result);
                    dataobject.loaded = true;
                });
        }

        function saveGoal(params, is_private, is_idp) {
            Restangular.all("goals")
                .post(params)
                .then(function (response) {

                    // if(response.result) {
                    //     attachGoal(1, response.result, is_private, is_idp);
                    // }
                });
        }

        function getUserGoals(id, params) {
            dataobject.userGoalItemsLoaded = false;
            Restangular.one("user-goals", id)
                .customGET("get-items", params)
                .then(function (response) {
                    dataobject.userGoalItems = response.data;
                    dataobject.userGoalItemsLoaded = true;
                });
        }

        function attachGoal(user_goal_id, goal_id, is_private, is_idp) {
            dataobject.newGoalAdded = false;
            params = {
                user_goal_id: user_goal_id,
                goal_id: goal_id,
                is_private: is_private,
                is_idp: is_idp,
            }
            Restangular.all("user-goal-items")
                .post(params)
                .then(function (response) {
                    dataobject.newGoalAdded = true;
                });
        }

        function detachGoal(user_goal_item_id) {
            dataobject.newGoalAdded = false;
            Restangular.one("user-goal-items", user_goal_item_id)
                .remove()
                .then(function (response) {
                    $log.log(response.result);
                    dataobject.newGoalAdded = true;
                });
        }

        function updateGoal(id) {
            dataobject.userGoalLoaded = false;
            Restangular.one("user-goals", id)
                .get()
                .then(function (response) {
                    dataobject.userGoal = response.data;
                    dataobject.userGoalLoaded = true;
                });
        }

        return {
            data: dataobject,
            getGoals: getGoals,
            getUserGoals: getUserGoals,
            saveGoal: saveGoal,
            attachGoal: attachGoal,
            detachGoal: detachGoal,
            updateUserGoals: updateUserGoals,
            updateGoal: updateGoal
        };
    }]);
angular
    .module("myApp")
    .controller("editGoalCtrl", ["$scope", "Restangular", "$uibModal", "$log", "Goal", function ($scope, Restangular, $uibModal, $log, Goal) {
        $scope.userGoal = JSON.parse(window.userGoal);
        $scope.submitted = false;
        $scope.loading = false;
        $scope.saveAsDraftLoading = false;
        $scope.setGoalLoading = false;
        $scope.freezeGoalLoading = false;
        $scope.data = {
            goals: [],
            tags: [],
            selected_tags: [],
            userGoalItems: [],
            newGoal: {
                title: "",
                success_metric: "",
                process: "",
                tags: []
            },
            newIdp: {
                title: "",
                process: ""
            }
        };
        
        $scope.form = {
            data: {
                start_date: moment($scope.userGoal.from_date).format("YYYY-MM-DD"),
                end_date: moment($scope.userGoal.to_date).format("YYYY-MM-DD"),
                user_goal_id: $scope.userGoal.id,
                goals: [],
                newGoal: {
                    id: null,
                    title: "",
                    tags: [],
                    success_metric: "",
                    process: "",
                    is_idp: false,
                    is_open: false
                }
            },
            errors: [],
            close_others: true,
            addGoal: function (index) {
                var goal = $scope.data.goals[index] ? $scope.data.goals[index] : '';
                var length = goal.tags.length;
                tags = [];
                for (i = 0; i < length; i++) {
                    tags.push(goal.tags[i].name);
                }
                $scope.form.data.goals.push({
                    id: null,
                    title: goal.title,
                    tags: tags,
                    success_metric: goal.success_metric,
                    process: goal.process,
                    is_idp: false,
                    is_open: false
                });
            },
            createGoal: function (is_idp) {
                if (is_idp) {
                    $scope.form.data.newGoal.tags = [];
                    $scope.form.data.newGoal.success_metric = "";
                }
                var goal = $scope.form.data.newGoal;
                if (is_idp) {
                    goal.is_idp = true;
                }
                $scope.submitted = true;
                $scope.error = {
                    title: 0,
                    success_metric: 0,
                    process: 0,
                    tags: 0
                };
                if (goal.title == null || goal.title == "") {
                    $scope.error.title = 1;
                }
                if (!is_idp && (goal.success_metric == null || goal.success_metric == "")) {
                    $scope.error.success_metric = 1;
                }
                if (goal.process == null || goal.process == "") {
                    $scope.error.process = 1;
                }
                if ( !is_idp && goal.tags.length == 0) {
                    $scope.error.tags = 1;
                }
                if ($scope.error.title == 0 && $scope.error.tags == 0 && $scope.error.success_metric == 0 && $scope.error.process == 0 ) {
                    $scope.form.data.goals.push(goal);
                    $scope.submitted = false;
                    $scope.form.data.newGoal = {
                        id: null,
                        title: "",
                        tags: [],
                        success_metric: "",
                        process: "",
                        is_idp: false,
                        is_open: false
                    };
                }
            },
            removeGoal: function (index) {
                Restangular.one("user-goal-items", $scope.form.data.goals[index].id)
                    .remove()
                    .then(function (response) {
                        $scope.form.data.goals.splice(index, 1);
                    },
                    function(errors) {
                        $scope.loading = false;
                        $scope.error = true;
                        console.log(errors);
                    }
                );
                
            },
            store: function (option) {
                $scope.form.errors = [];
                $scope.form.close_others = true;
                var result = $scope.form.validate();
                $scope.form.errors = result.errors;
                if (result.count == 0) {
                    $scope.form.close_others = false;
                    $scope.form.updateUserGoals(option);
                }
            },
            validate: function () {
                var length = $scope.form.data.goals.length;
                var errors = [];
                count = 0;
                for (i = 0; i < length; i++) {
                    var goal = $scope.form.data.goals[i];
                    errors[i] = [];
                    if (goal.title == null || goal.title == "") {
                        errors[i].push("title");
                        count++;
                        $scope.form.data.goals[i].is_open = true;
                    }
                    if ( !goal.is_idp && goal.tags.length == 0) {
                        errors[i].push("tags");
                        count++;
                        $scope.form.data.goals[i].is_open = true;
                    }
                    if (!goal.is_idp && (goal.success_metric == null || goal.success_metric == "")) {
                        errors[i].push("success_metric");
                        count++;
                        $scope.form.data.goals[i].is_open = true;
                    }
                    if (goal.process == null || goal.process == "") {
                        errors[i].push("process");
                        count++;
                        $scope.form.data.goals[i].is_open = true;
                    }
                }

                return {
                    errors: errors,
                    count: count
                };
            },
            updateUserGoals: function (option) {
                $scope.loading = true;
                if ( option == 'pending' )
                {
                    $scope.saveAsDraftLoading = true;
                }
                else if ( option == 'running' )
                {
                    $scope.freezeGoalLoading = true;
                }
                else {
                    $scope.setGoalLoading = true;
                }
                Restangular.one("user-goal-items", $scope.userGoal.id)
                    .customPUT({
                        goals: $scope.form.data.goals,
                        status: option
                    })
                    .then(function (response) {
                        $scope.loading = false;
                        $scope.saveAsDraftLoading = false;
                        $scope.setGoalLoading = false;
                        $scope.freezeGoalLoading = false;
                        if ( option != 'pending' )
                            window.location.reload();
                    },
                    function(errors) {
                        $scope.loading = false;
                        $scope.saveAsDraftLoading = false;
                        $scope.setGoalLoading = false;
                        $scope.freezeGoalLoading = false;
                        $scope.error = true;
                        $scope.success = false;
                    }
                );
            }
        };

        $scope.search = {
            text: "",
            tags: []
        };
        $scope.GoalData = null;
        $scope.select2Options = {
            allowClear: true
        };

        Restangular.one("tags")
            .get()
            .then(function (response) {
                $scope.data.tags = response;
            });

        Goal.getGoals($scope.search);
        $scope.GoalData = Goal.data;

        $scope.$watch("GoalData.loaded", function () {
            if ($scope.GoalData.loaded == true) {
                $scope.data.goals = $scope.GoalData.goals;
            }
        });
        $scope.$watch("GoalData.userGoalItemsLoaded", function () {
            if ($scope.GoalData.userGoalItemsLoaded == true) {
                $scope.form.data.goals = $scope.GoalData.userGoalItems;
            }
        });

        $scope.filter = function () {
            Goal.getGoals($scope.search);
        };
        params = {
            start_date: moment($scope.userGoal.from_date).format(
                "YYYY-MM-DD"
            ),
            end_date: moment($scope.userGoal.to_date).format("YYYY-MM-DD")
        };

        Goal.getUserGoals($scope.userGoal.id, params);
        $scope.saveGoal = function (is_private) {
            var params = $scope.data.newGoal;
            Goal.saveGoal(params, is_private, false);
        };

        $scope.saveIdp = function () {
            var params = $scope.data.newIdp;
            Goal.saveGoal(params, true, true);
        };

        $scope.attachGoal = function (goal_id) {
            Goal.attachGoal(1, goal_id, false, false);
        };

        $scope.detachGoal = function (item_id) {
            Goal.detachGoal(item_id);
        };
        $scope.$watch("GoalData.userGoalItemsLoaded", function () {
            if ($scope.GoalData.userGoalItemsLoaded == true) {
                $scope.form.data.goals = $scope.GoalData.userGoalItems;
            }
        });

        $scope.updateUserGoals = function () {
            params = {
                user_goal_items: $scope.data.userGoalItems
            };
            Goal.updateUserGoals(params);
        };

        $scope.$watch("GoalData.newGoalAdded", function () {
            if ($scope.GoalData.newGoalAdded == true) {
                params = {
                    start_date: moment($scope.userGoal.from_date)
                        .add("330", "m")
                        .format("YYYY-MM-DD"),
                    end_date: moment($scope.userGoal.to_date)
                        .add("330", "m")
                        .format("YYYY-MM-DD")
                };
                $scope.data.newGoal = [];
                Goal.getUserGoals(1, params);
            }
        });
    }]);
angular
    .module("myApp")
    .controller("reviewGoalCtrl",["$scope", "Restangular", "$uibModal", "$log", "Goal", function ($scope, Restangular, $uibModal, $log, Goal) {
        $scope.completeLoading = false;
        $scope.userGoal = JSON.parse(window.userGoal);
        $scope.submitted = false;
        $scope.overall_feedback = '';
        $scope.display_submit = false;
        $scope.disableFields = false;
        if($scope.userGoal.status == 'completed'){
            $scope.disableFields = true;
        }
        var currentDate = new Date();
        var to_date = new Date($scope.userGoal.to_date);
        if ( to_date > currentDate )
        {
            $scope.display_submit = false;
        }
        else{
            $scope.display_submit = true;
        }
        $scope.GoalData = null;
        $scope.GoalData = Goal.data;
        $scope.goalData = Goal.data;
        $scope.$watch("GoalData.userGoalItemsLoaded", function () {
            if ($scope.GoalData.userGoalItemsLoaded == true) {
                $scope.form.data.goals = $scope.GoalData.userGoalItems;
            }
        });
        $scope.$watch("GoalData.userGoalLoaded", function () {
            if ($scope.GoalData.userGoalLoaded == true) {
                $scope.userGoal = $scope.GoalData.userGoal;
            }
        });
        $scope.$watch('userGoal', function() {
            if ( $scope.userGoal.status == 'completed' ) {
                $scope.disableFields = true;
            }
        });
        Goal.getUserGoals($scope.userGoal.id, params);
        var params = {
            start_date: moment($scope.userGoal.from_date).format("YYYY-MM-DD"),
            end_date: moment($scope.userGoal.to_date).format("YYYY-MM-DD")
        };

        $scope.form = {
            data: {
                user_goal_id: $scope.userGoal.id,
                goals: []
            },
            errors: [],
            close_others: true,
            
            store: function () {
                var result = $scope.form.validate();
                $scope.form.overall_feedback_error = result.overall_feedback_error;
                if (!result.overall_feedback_error) {
                    $scope.form.reviewUserGoals();
                }
            },
            validate: function () {
                var overall_feedback_error = false;
                if ( $scope.userGoal.overall_feedback == null || $scope.userGoal.overall_feedback == "" )
                {
                    overall_feedback_error = true;
                }
                return {
                    overall_feedback_error: overall_feedback_error
                };
            },
            reviewUserGoals: function () {
                $scope.loading = true;
                $scope.error = false;
                Restangular.one("user-goal-items", $scope.userGoal.id)
                    .customPUT({
                        overall_feedback: $scope.userGoal.overall_feedback
                    })
                    .then(function (response) {
                        window.location.reload();
                    });
            }
        };
        $scope.updateReview = function (item) {
            Restangular.one("user-goal-items/review", item.id)
                    .customPUT({
                        review: item.review_comment
                    })
                    .then(function (response) {
                    });
        };
        $scope.updatePoints = function (item) {
            Restangular.one("user-goal-items/points", item.id)
                    .customPUT({
                        points: item.points
                    })
                    .then(function (response) {
                    });
        };
        $scope.completeGoal = function (){
            $scope.completeLoading = true;
            Restangular.one("user-goals", $scope.userGoal.id)
                    .customPUT({
                        goals: $scope.form.data.goals,
                        status: 'completed'
                    })
                    .then(function (response) {
                        console.log(response);
                        $scope.completeLoading = false;
                        Goal.updateGoal($scope.userGoal.id);
                    },
                    function(errors){
                        $scope.completeLoading = false;
                        $scope.error = true;
                        $scope.success = false;
                    }
                );
            
        };
    }]);
angular
    .module("myApp")
    .controller("userGoalCtrl",["$scope", "Restangular", "Goal", function ($scope, Restangular, Goal) {
        $scope.loading = false;
        $scope.saveLoading = false;
        $scope.confirmLoading = false;
        $scope.freezeGoalLoading = false;
        $scope.userGoal = JSON.parse(window.goal);
        $scope.user = JSON.parse(window.user);
        $scope.submitted = false;
        $scope.GoalData = null;
        $scope.GoalData = Goal.data;
        $scope.collapse = '';
        $scope.disableFields = false;
        $scope.$watch("GoalData.userGoalItemsLoaded", function () {
            if ($scope.GoalData.userGoalItemsLoaded == true) {
                $scope.form.data.goals = $scope.GoalData.userGoalItems;
            }
        });
        $scope.$watch("GoalData.userGoalLoaded", function () {
            if ($scope.GoalData.userGoalLoaded == true) {
                $scope.userGoal = $scope.GoalData.userGoal;
            }
        });
        $scope.$watch('userGoal', function() {
            if ( $scope.userGoal.status == 'completed' ) {
                $scope.disableFields = true;
            }
        });
        Goal.getUserGoals($scope.userGoal.id, params);
        var params = {
            start_date: moment($scope.userGoal.from_date).format("YYYY-MM-DD"),
            end_date: moment($scope.userGoal.to_date).format("YYYY-MM-DD")
        };

        $scope.form = {
            data: {
                start_date: moment($scope.userGoal.from_date).format("YYYY-MM-DD"),
                end_date: moment($scope.userGoal.to_date).format("YYYY-MM-DD"),
                user_goal_id: $scope.userGoal.id,
                goals: [],
                newGoal: {
                    id: null,
                    title: "",
                    tags: [],
                    success_metric: "",
                    process: "",
                    is_idp: false,
                    is_open: false
                }
            },
            errors: [],
            close_others: true,
            addGoal: function (index) {
                var goal = $scope.data.goals[index] ? $scope.data.goals[index] : '';
                var length = goal.tags.length;
                tags = [];
                for (i = 0; i < length; i++) {
                    tags.push(goal.tags[i].name);
                }
                $scope.form.data.goals.push({
                    id: null,
                    title: goal.title,
                    tags: tags,
                    success_metric: goal.success_metric,
                    process: goal.process,
                    is_idp: false,
                    is_open: false
                });
            },
            createGoal: function (is_idp) {
                $scope.collapse = '';
                if (is_idp) {
                    $scope.form.data.newGoal.tags = [];
                    $scope.form.data.newGoal.success_metric = "";
                }
                var goal = $scope.form.data.newGoal;
                if (is_idp) {
                    goal.is_idp = true;
                }
                $scope.submitted = true;
                $scope.error = {
                    title: 0,
                    success_metric: 0,
                    process: 0,
                    tags: 0
                };
                if (goal.title == null || goal.title == "") {
                    $scope.error.title = 1;
                }
                if (!is_idp && (goal.success_metric == null || goal.success_metric == "")) {
                    $scope.error.success_metric = 1;
                }
                if (goal.process == null || goal.process == "") {
                    $scope.error.process = 1;
                }
                if ( !is_idp && goal.tags.length == 0) {
                    $scope.error.tags = 1;
                }
                if ($scope.error.title == 0 && $scope.error.tags == 0 && $scope.error.success_metric == 0 && $scope.error.process == 0 ) {
                    $scope.form.data.goals.push(goal);
                    $scope.collapse = 'collapse';
                    $scope.submitted = false;
                    $scope.form.data.newGoal = {
                        id: null,
                        title: "",
                        tags: [],
                        success_metric: "",
                        process: "",
                        is_idp: false,
                        is_open: false
                    };
                }
            },
            removeGoal: function (index) {
                $scope.form.data.goals.splice(index, 1);
            },
            store: function (option) {
                if (option == 'confirmed')
                {
                    if (!confirm("Accept the goals?")) 
                    {
                        return;
                    }
                }
                $scope.form.errors = [];
                $scope.form.close_others = true;
                var result = $scope.form.validate();
                $scope.form.errors = result.errors;
                if (result.count == 0) {
                    $scope.form.close_others = false;
                    $scope.form.updateUserGoals(option);
                }
            },
            validate: function () {
                var length = $scope.form.data.goals.length;
                var errors = [];
                count = 0;
                for (i = 0; i < length; i++) {
                    var goal = $scope.form.data.goals[i];
                    errors[i] = [];
                    if (goal.title == null || goal.title == "") {
                        errors[i].push("title");
                        count++;
                        $scope.form.data.goals[i].is_open = true;
                    }
                    if ( !goal.is_idp && goal.tags.length == 0) {
                        errors[i].push("tags");
                        count++;
                        $scope.form.data.goals[i].is_open = true;
                    }
                    if (!goal.is_idp && (goal.success_metric == null || goal.success_metric == "")) {
                        errors[i].push("success_metric");
                        count++;
                        $scope.form.data.goals[i].is_open = true;
                    }
                    if (goal.process == null || goal.process == "") {
                        errors[i].push("process");
                        count++;
                        $scope.form.data.goals[i].is_open = true;
                    }
                }

                return {
                    errors: errors,
                    count: count
                };
            },
            updateUserGoals: function (option) {
                if ( option == 'submitted' ){
                    $scope.saveLoading = true;
                }
                else if (option == 'confirmed') {
                    $scope.confirmLoading = true;
                }
                else if ( option == 'running' ){
                    $scope.freezeGoalLoading = true;
                }
                $scope.loading = true;
                $scope.error = false;
                Restangular.one("user-goal-items", $scope.userGoal.id)
                    .customPUT({
                        goals: $scope.form.data.goals,
                        status: option
                    })
                    .then(function (response) {
                        $scope.loading = false;
                        $scope.saveLoading = false;
                        $scope.confirmLoading = false;
                        $scope.freezeGoalLoading = false;
                        Goal.getUserGoals($scope.userGoal.id, params);
                        Goal.updateGoal($scope.userGoal.id);
                    },
                    function(errors){
                        $scope.loading = false;
                        $scope.saveLoading = false;
                        $scope.confirmLoading = false;
                        $scope.freezeGoalLoading = false;
                        $scope.error = true;
                        $scope.success = false;
                    }
                );
            }
        };
    }]);
angular.module("myApp").service("Comment",["Restangular", "$location", "$log", function Comment(Restangular, $location, $log) {
    commentObj = {
        comments: [],
        page: 1,
        per_page: 5,
        loaded: false,
        newCommentAdded: false
    };

    function getComments(id, reference_type) {
        dataobject.loaded = false;
        Restangular.all("comments")
            .customGET("",{
                "reference_id": id,
                "reference_type": reference_type,
                "page": commentObj.page,
                "per_page": commentObj.per_page
            })
            .then(function (response) {
                commentObj.comments = response.data;
                commentObj.loaded = true;
            });
    }

    function postComment(id, reference_type, comment_text) {
        commentObj.loaded = false;
        var params = {
            reference_id: id,
            reference_type: reference_type,
            value: comment_text
        };
        Restangular.all("comments")
            .post(params)
            .then(function (response) {
                console.log(response);
                commentObj.loaded = true;
            }, function (error) {
                console.log(error);
                commentObj.loaded = true;
            }
        );
    }
    function setPage(page) {
        commentObj.page = page;
    }

    return {
        details: commentObj,
        getComments: getComments,
        postComment: postComment,
        setPage: setPage
    };
    // return {
    //     data: dataobject
    // };
}]);
angular.module("myApp").controller("newTimesheetCtrl", [
  "$scope",
  "Restangular",
  function($scope, Restangular) {
    $scope.dates = [];
    var date = new Date();
    $scope.projectList = [];
    $scope.selectedProject = [];
    $scope.tasks = [];
    $scope.loggedTime = [];
    $scope.currentDate = new Date().toISOString().slice(0, 10);
    $scope.selectedDate = $scope.currentDate;
    $scope.error = false;
    $scope.taskError = [];
    $scope.durationError = [];
    $scope.errorMessage = "";
    for (var i = 0; i < 45; i++) {
      var dateString = date.toISOString().slice(0, 10);
      $scope.dates.push(dateString);
      date.setDate(date.getDate() - 1);
    }
    Restangular.all("new-timesheet/projects")
      .getList({
        pagination: 0
      })
      .then(
        function(response) {
          $scope.projectList = response;
        },
        function(errors) {
          console.log(errors);
        }
      );
    $scope.selectProject = function(data, i) {
      $scope.selectedProject[i] = data;
    };

    $scope.submit = function() {
      $scope.error = false;
      for (i = 0; i < 45; i++) {
        if ($scope.selectedProject[i]) {
          if (!$scope.tasks[i]) {
            $scope.taskError[i] = true;
            $scope.error = true;
          } else {
            $scope.taskError[i] = false;
          }
          if (!$scope.tasks[i] || !$scope.loggedTime[i]) {
            $scope.durationError[i] = true;
            $scope.error = true;
          } else {
            $scope.durationError[i] = false;
          }
        }
      }

      if ($scope.error) {
        return;
      }
      var params = {
        date: $scope.selectedDate,
        projects: $scope.selectedProject,
        tasks: $scope.tasks,
        durations: $scope.loggedTime
      };
      Restangular.all("new-timesheet")
        .post(params)
        .then(
          function(response) {
            console.log("REST ", response);
            $scope.finalsubmit = false;
            if (response) {
              var path = "/admin/new-timesheet/history";
              window.location.href = path;
            }
          },
          function(errors) {
            console.log(errors);
            $scope.finalsubmit = false;
            $scope.error = true;
            $scope.errorMessage = errors.data.message;
          }
        );
    };
  }
]);

angular.module('myApp')
    .controller('newTimesheetHistoryCtrl',["$scope", "Restangular", function ($scope, Restangular) {
        var per_page = 100;
        var page = 1;
        var tmpPage = 1;
        var project = '';
        $scope.tmpTimesheetHistory = [];
        $scope.timesheetHistory = [];
        $scope.projectList = [];
        $scope.selectedProject = '';
        $scope.success = [];
        $scope.successTemp = [];
        $scope.error = false;
        $scope.saving = [];
        $scope.savingTemp = [];

        $scope.fetchList = function(){
            Restangular.all("new-timesheet").getList({
                'pagination': 1,
                'page': page,
                'per_page': per_page,
                'relations': 'project,review',
                'project': project,
            })
            .then(
                function (response) {
                    $scope.timesheetHistory = response;
                    $scope.metaData = response.metaData;
                },
                function (errors) {
                    console.log(errors);
                }
            );
        };

        $scope.fetchTempList = function(){
            Restangular.all("tmp-timesheet").getList({
                'pagination': 1,
                'page': tmpPage,
                'per_page': per_page
            })
            .then(
                function (response) {
                    $scope.tmpTimesheetHistory = response;
                    $scope.tmpMetaData = response.metaData;
                },
                function (errors) {
                    console.log(errors);
                }
            );
        };

        $scope.getAssignedProjects = function () {
            Restangular.all("new-timesheet/projects").getList({
                'pagination': 0
            })
            .then(
                function (response) {
                    $scope.projectList = response;
                },
                function (errors) {
                    console.log(errors);
                }
            );
        };

        $scope.getAssignedProjects();
        $scope.fetchList();
        $scope.fetchTempList();

        $scope.selectProject = function (data) {
            $scope.selectedProject = data;
            project = 'project_id,'+data.id;
            $scope.fetchList();
        };

        $scope.update = function (data) {
            $scope.saving[data.id] = true;
            $scope.success[data.id] = false;
            $scope.error = false;
            Restangular.one("new-timesheet", data.id)
            .customPUT({
                data: data
            })
            .then(
                function (response) {
                    $scope.saving[data.id] = false;
                    $scope.success[data.id] = true;
                },
                function (errors) {
                    $scope.savingTemp[data.id] = false;
                    $scope.error = true;
                }
            );
        };
        
        $scope.changePage = function () {
            page = $scope.metaData.current_page;
            $scope.fetchList();
        };
        $scope.changeTempPage = function () {
            tmpPage = $scope.tmpMetaData.current_page;
            $scope.fetchTempList();
        };

        $scope.updateTemp = function (data) {
            $scope.savingTemp[data.id] = true;
            $scope.loading = true;
            $scope.successTemp[data.id] = false;
            $scope.error = false;
            Restangular.one("tmp-timesheet", data.id)
            .customPUT({
                data: data
            })
            .then(
                function (response) {
                    $scope.savingTemp[data.id] = false;
                    $scope.successTemp[data.id] = true;
                },
                function (errors) {
                    $scope.savingTemp[data.id] = false;
                    $scope.error = true;
                    console.log($scope.error);
                }
            );
        };

        $scope.delete = function (data, index) {
            $scope.saving[data.id] = true;
            $scope.success[data.id] = false;
            $scope.error = false;
            Restangular.one("new-timesheet", data.id)
            .remove()
            .then(
                function (response) {
                    $scope.saving[data.id] = false;
                    $scope.success[data.id] = true;
                    $scope.timesheetHistory.splice(index, 1);
                },
                function (errors) {
                    $scope.savingTemp[data.id] = false;
                    $scope.error = true;
                }
            );
        };   

        $scope.deleteTemp = function (data, index) {
            $scope.saving[data.id] = true;
            $scope.success[data.id] = false;
            $scope.error = false;
            Restangular.one("tmp-timesheet", data.id)
            .remove()
            .then(
                function (response) {
                    $scope.saving[data.id] = false;
                    $scope.success[data.id] = true;
                    $scope.tmpTimesheetHistory.splice(index, 1);
                    
                },
                function (errors) {
                    $scope.savingTemp[data.id] = false;
                    $scope.error = true;
                }
            );
        };      

    }]);
angular.module('myApp')
    .controller('newTimesheetLeadViewCtrl', ["$scope", "Restangular", function ($scope, Restangular) {
        $scope.user = JSON.parse(window.user);
        var per_page = 10000;
        var page = 1;
        var project = '';

        $scope.dataIsLoading = true;
        $scope.timesheetHistory = [];
        $scope.projectList = [];
        $scope.selectedProject = '';
        $scope.success = [];
        $scope.error = false;
        $scope.taskError = false;
        $scope.durationError = false;
        $scope.saving = [];
        $scope.timesheetHistoryApproved = [];
        $scope.timesheetHistoryRejected = [];

        $scope.fetchList = function () {
            Restangular.all("new-timesheet").getList({
                'pagination': 1,
                'page': page,
                'per_page': per_page,
                'relations': 'user,project,review',
                'project': project,
                'lead_id': 1,
                'status': 'pending',
            })
                .then(
                    function (response) {
                        $scope.timesheetHistory = response;
                        $scope.metaData = response.metaData;
                        $scope.dataIsLoading = false;
                    },
                    function (errors) {
                        console.log(errors);
                        $scope.dataIsLoading = false;
                    }
                );
        };
        $scope.fetchApprovedList = function () {
            Restangular.all("new-timesheet").getList({
                'pagination': 1,
                'page': page,
                'per_page': per_page,
                'relations': 'user,project,review',
                'project': project,
                'lead_id': 1,
                'status': 'approved',
            })
                .then(
                    function (response) {
                        $scope.timesheetHistoryApproved = response;
                        $scope.metaData = response.metaData;
                    },
                    function (errors) {
                        console.log(errors);
                    }
                );
        };
        $scope.fetchRejectedList = function () {
            Restangular.all("new-timesheet").getList({
                'pagination': 1,
                'page': page,
                'per_page': per_page,
                'relations': 'user,project,review',
                'project': project,
                'lead_id': 1,
                'status': 'rejected',
            })
                .then(
                    function (response) {
                        $scope.timesheetHistoryRejected = response;
                        $scope.metaData = response.metaData;
                    },
                    function (errors) {
                        console.log(errors);
                    }
                );
        };

        $scope.getAssignedProjects = function () {
            Restangular.all("new-timesheet/managed/projects").getList()
                .then(
                    function (response) {
                        $scope.projectList = response;
                    },
                    function (errors) {
                        console.log(errors);
                    }
                );
        };

        $scope.getAssignedProjects();
        $scope.fetchList();
        $scope.fetchApprovedList();
        $scope.fetchRejectedList();

        $scope.selectProject = function (data) {
            $scope.selectedProject = data;
            project = 'project_id,' + data.id;
            $scope.fetchList();
        };

        $scope.update = function (data) {
            $scope.success[data.id] = false;
            $scope.saving[data.id] = false;
            if (!data.review.approved_duration || !data.review.approval_comments) {
                return;
            }

            Restangular.one("new-timesheet/approval", data.id)
                .customPUT({
                    data: data
                })
                .then(
                    function (response) {
                        $scope.success[data.id] = true;
                        $scope.saving[data.id] = false;
                    },
                    function (errors) {
                        $scope.success[data.id] = false;
                        $scope.saving[data.id] = false;
                    }
                );
        };

        $scope.changePage = function () {
            page = $scope.metaData.current_page;
            $scope.fetchList();
        };

        $scope.approve = function (data) {
            $scope.success[data.id] = false;
            $scope.saving[data.id] = true;
            if (!data.review.approved_duration || !data.review.approval_comments) {
                return;
            }

            Restangular.one("new-timesheet/approval", data.id)
                .customPUT({
                    data: data
                })
                .then(
                    function (response) {
                        $scope.success[data.id] = true;
                        $scope.saving[data.id] = false;
                        data.status = 'approved';
                        var index = $scope.timesheetHistory.indexOf(data);
                        if (index !== -1) $scope.timesheetHistory.splice(index, 1);
                        data.review.approver_id = $scope.user.id;
                    },
                    function (errors) {
                        $scope.success[data.id] = false;
                        $scope.saving[data.id] = false;
                    }
                );
        };

        $scope.reject = function (data) {
            var r = confirm("Are you sure you want to reject this entry?");
            if (r != true) {
                return;
            }
            $scope.success[data.id] = false;
            $scope.saving[data.id] = true;
            Restangular.one("new-timesheet/reject", data.id)
                .customPUT({
                    data: data
                })
                .then(
                    function (response) {
                        $scope.success[data.id] = true;
                        $scope.saving[data.id] = false;
                        data.status = 'rejected';
                        var index = $scope.timesheetHistory.indexOf(data);
                        if (index !== -1) $scope.timesheetHistory.splice(index, 1);
                        data.review.approver_id = $scope.user.id;
                    },
                    function (errors) {
                        $scope.success[data.id] = false;
                        $scope.saving[data.id] = false;
                    }
                );
        };
        $scope.loadPendingList = function () {
            $scope.fetchList();
        };

        $scope.loadApprovedList = function () {
            $scope.fetchApprovedList();
        };
        $scope.loadRejectedList = function () {
            $scope.fetchRejectedList();
        };
    }])
    .filter('moment', function () {
        return function (input, momentFn /*, param1, param2, ...param n */) {
            var args = Array.prototype.slice.call(arguments, 2),
                momentObj = moment(input);
            return momentObj[momentFn].apply(momentObj, args);
        };
    });
angular.module('myApp')
    .controller('feedbackUserListCtrl',["$scope", "Restangular", function ($scope, Restangular) {
        $scope.user = JSON.parse(window.user);
        $scope.months = [];
        $scope.userList = [];
        $scope.selectedMonth;
        $scope.error = false;
        $scope.loading = false;
        
        $scope.errorMessage = '';
        
        Restangular.all("feedback-month").getList({
            'pagination': 0
        })
        .then(
            function (response) {
                $scope.months = response;
                $scope.selectedMonth = $scope.months[0].id;
                $scope.getUserList();
                
            },
            function (errors) {
                console.log(errors);
            }
        );

        $scope.getUserList = function () {
            $scope.error = false;
            Restangular.all("feedback-user").getList({
                'pagination': 0,
                'feedback_month_id': $scope.selectedMonth
            })
            .then(
                function (response) {
                    $scope.loading = false;
                    $scope.userList = response;
                },
                function (errors) {
                    $scope.loading = false;
                    console.log(errors);
                }
            );
        };
        $scope.selectMonth= function () {
            $scope.loading = true;
            $scope.getUserList();
        };
        $scope.review = function (userId) {
            var path = "/admin/feedback/review/"+userId+'/'+$scope.selectedMonth;
            window.location.href =  path;
        }

    }]);
angular
    .module("myApp")
    .controller("feedbackUserReviewCtrl", ["$scope", "Restangular", function ($scope, Restangular) {
        var reviews = window.reviews;
        $scope.month = window.month;
        $scope.data = {};
        $scope.reviews = reviews.reviewer_type;
        $scope.percentage = 0;
        $scope.getProgressStatus = function () {
            $scope.error = false;
            Restangular.one("feedback-user-status").get({
                'user_id': reviews.for_user.id,
                'feedback_month_id': $scope.month.id,
            })
            .then(
                function (response) {
                    $scope.percentage = response;
                },
                function (errors) {
                    $scope.loading = false;
                    console.log(errors);
                }
            );
        };
        $scope.getProgressStatus();
        reviews.reviewer_type.forEach( function(type) {
            $scope.data[type.code] = [];
            reviews[type.code].forEach(function(project) {
                project.project_reviews.forEach(function(review) {
                    review.saveRating = function (param) {
                        review.ratingLoading = true;
                        review.rating = param;
                        if ( review.rating < 5 || review.rating > 9 )
                        {
                            review.rating_comments = '';
                            review.saveRatingComment();
                        }
                        var params = {
                            rating: review.rating,
                            id: review.id
                        };
                        Restangular.all("feedback-review")
                            .post(params)
                            .then(
                                function (response) {
                                    if (response) {
                                        review.ratingLoading = false;
                                        $scope.getProgressStatus();
                                    }
                                },
                                function (errors) {
                                    review.ratingLoading = false;
                                    console.log(errors);
                                }
                            );
                    };
                    review.saveComment = function () {
                        review.commentLoading = true;
                        const params = {
                            commentable_id: review.id,
                            commentable_type: "App\\Models\\Admin\\FeedbackUser\\General",
                            message: review.comments
                        };
                        Restangular.all("comments")
                            .post(params)
                            .then(
                                function (response) {
                                    if (response) {
                                        review.commentLoading = false;
                                    }
                                },
                                function (errors) {
                                    review.commentLoading = false;
                                    console.log(errors);
                                }
                            );
                    };
                    review.saveRatingComment = function () {
                        review.ratingCommentLoading = true;
                        if ( review.rating < 5 )
                        {
                            review.is_private = "false";
                        }
                        const params = {
                            commentable_id: review.id,
                            commentable_type: "App\\Models\\Admin\\FeedbackUser\\Restricted",
                            message: review.rating_comments,
                            is_private: review.is_private
                        };
                        Restangular.all("comments")
                            .post(params)
                            .then(
                                function (response) {
                                    if (response) {
                                        review.ratingCommentLoading = false;
                                    }
                                },
                                function (errors) {
                                    review.ratingCommentLoading = false;
                                    console.log(errors);
                                }
                            );
                    };
                    review.updateNA = function (param) {
                        var r = confirm("Are you sure you want to mark it as not applicable?");
                        if (r != true) {
                            return;
                        }
                        review.naLoading = true;
                        review.rating = param;
                        var params = {
                            id: review.id
                        };
                        Restangular.all("feedback-review-na")
                            .post(params)
                            .then(
                                function (response) {
                                    if (response) {
                                        review.naLoading = false;
                                        review.status = 'not_applicable';
                                        review.rating = null;
                                        $scope.getProgressStatus();
                                    }
                                },
                                function (errors) {
                                    review.ratingLoading = false;
                                    console.log(errors);
                                }
                            );
                    };
                });
                $scope.data[type.code].push(project);
            });
        });
        console.log($scope.data);
    }]);

angular.module('myApp')
    .controller('feedbackReportCtrl', ["$scope", "$uibModal", "Restangular", function ($scope, $uibModal, Restangular) {
        $scope.data = window.feedbakUsers;
        $scope.month = window.feedbackMonth;
        $scope.loading = false;
    
        console.log($scope.data);
        $scope.form ={
            loading : false
        }
        $scope.data.forEach(function(feedback) {
            feedback.notifyOnSlack = function(id, type, role, $index){
                var url = '';
                if(type != 'all') {
                    url = 'feedback-review-notify';
                    feedback.reviews[$index].sent = true;
                } else {
                    if (confirm("Are you sure?")) {
                        url = 'feedback-review-notify-all';
                        feedback.sent_all = true;
                    } else {
                        return false;
                    }
                }
                
                Restangular.all(url).post({'feedback_user_id' : id}).then(function(response) {
                    $scope.form.loading = false;
                    feedback.sent = true;
                    //$uibModalInstance.dismiss();
                }, function(error){
                    $scope.form.loading = false;
                    console.log('feedback-review-notify ', error);
                });
            };
            feedback.regenerateRecords = function( user_id, month_id ) {
                var r = confirm("Are you sure you want to regenerate feedback records for the user?");
                if (r != true) {
                    return;
                }
                $scope.loading = true;
                var params = {
                    month_id: month_id,
                    user_id: user_id
                };
                Restangular.all('feedback-review-regenerate-user').post(params).then(function(response) {
                    window.location.reload();
                    $scope.loading = false;
                }, function(error){
                    console.log(error);
                    $scope.loading = false;
                });
            };
        });
        $scope.lockMonth = function () {
            var params = {
                id: $scope.month.id
            };
            Restangular.all('feedback-month/lock').post(params).then(
                function (response) {
                    if (response) {
                        $scope.month.locked = true;
                    }
                },
                function (errors) {
                    console.log(errors);
                }
            );
        };
        $scope.regenerateAllRecords = function() {
            var r = confirm("Are you sure you want to regenerate feedback records for all users for this month?");
            if (r != true) {
                return;
            }
            $scope.loading = true;
            var params = {
                month : $scope.month.month,
                year : $scope.month.year
            };
            Restangular.all('feedback-review-regenerate-all').post(params).then(function(response) {
                window.location.reload();
                $scope.loading = false;
            }, function(error){
                console.log(error);
                $scope.loading = false;
            });

        };
         
    }]).controller('feedbackReportModalCtrl',["$scope", "Restangular", "data", "$uibModalInstance", function ($scope, Restangular, data, $uibModalInstance) {
        $scope.form = {
            notifyOnSlack : function() {
                $scope.form.loading = true;
                var url = 'feedback-review-notify';
                if(data.type == 'all') {
                    url = 'feedback-review-notify-all';
                }
                Restangular.all(url).post({'feedback_user_id' : data.id}).then(function(response) {
                    $scope.form.loading = false;
                    $uibModalInstance.dismiss();
                }, function(error){
                    $scope.form.loading = false;
                    console.log('feedback-review-notify ', error);
                })
            }
        }
        
        $scope.cancel = function () {
            $uibModalInstance.dismiss();
        };
    }]);
angular
    .module("myApp")
    .controller("adminFeedbackReviewCtrl",["$scope", "Restangular", function ($scope, Restangular) {
        $scope.reviews = window.reviews;
        $scope.month = window.month;
        console.log(reviews);
    }]);
angular
    .module("myApp")
    .controller("userFeedbackReviewCtrl",["$scope", "Restangular", function ($scope, Restangular) {
        $scope.reviews = window.reviews;
        $scope.month = window.month;
        console.log(reviews);
        $scope.oneAtATime = true;


  $scope.status = {
    isCustomHeaderOpen: false,
    isFirstOpen: true,
    isFirstDisabled: false
  };
  $scope.status2 = {
    isCustomHeaderOpen: false,
    isFirstOpen: true,
    isFirstDisabled: false
  };

    }]);


    
angular
    .module("myApp")
    .controller("createProjectCtrl",["$scope", "Restangular", function ($scope, Restangular) {
        $scope.clientList = window.clientList;
        $scope.projectCategories = window.projectCategories;
        $scope.slackPublicChannels = window.slackPublicChannels;
        $scope.slackPrivateChannels = window.slackPrivateChannels;
        $scope.salesExecutiveList = window.salesExecutiveList;
        $scope.projectCoordinatorList = window.projectCoordinatorList;
        $scope.projectManagerList = window.projectManagerList;
        $scope.codeLeadList = window.codeLeadList;
        $scope.deliveryLeadList = window.deliveryLeadList;
        $scope.allActiveUserList = window.allActiveUserList;
        $scope.antManagerId = window.antManagerId;
        $scope.selected_client = '';
        $scope.project_name = '';
        $scope.selected_project_category = '';
        $scope.status = 'open';
        $scope.email_reminder = 'true';
        $scope.selected_sales_manager = '';
        $scope.selected_project_coordinator = '';
        $scope.selected_project_manager = '';
        $scope.selected_code_lead = '';
        $scope.selected_delivery_lead = '';
        
        $scope.checkBoxes = {
            ui_ux : '',
            dev_op : '',
            architect : '',
            deployment : '',
            qa : ''
        };
        $scope.leads = {
            ui_ux_lead : '',
            dev_op_lead : '',
            architect_lead : '',
            deployment_lead : '',
            qa_lead : ''
        };
        $scope.start_date = {
            options: {
              applyClass: "btn-success",
              singleDatePicker: true,
              locale: {
                applyLabel: "Apply",
                fromLabel: "From",
                // format: "YYYY-MM-DD", //will give you 2017-01-06
                format: "D-MMM-YY", //will give you 6-Jan-17
                // format: "D-MMMM-YY", //will give you 6-January-17
                toLabel: "To",
                cancelLabel: "Cancel",
                customRangeLabel: "Custom range"
              }
            }
          };
          $scope.end_date = {
            options: {
              applyClass: "btn-success",
              singleDatePicker: true,
              locale: {
                applyLabel: "Apply",
                fromLabel: "From",
                // format: "YYYY-MM-DD", //will give you 2017-01-06
                format: "D-MMM-YY", //will give you 6-Jan-17
                // format: "D-MMMM-YY", //will give you 6-January-17
                toLabel: "To",
                cancelLabel: "Cancel",
                customRangeLabel: "Custom range"
              },
              date: null
            }
          };

        var checkIfBotIsMember = function (channel) {
            if(channel.members.indexOf($scope.antManagerId) !== -1) {
                return true;
            }
            else 
                return false;
        };

        $scope.select_client = function (data) {
            $scope.selected_client = data;
            $scope.selected_client_error = false;
        };

        $scope.select_project_category = function (data) {
            $scope.selected_project_category = data;
            $scope.selected_project_category_error = false;
        };
        $scope.select_slack_public_channels = function (data) {
            $scope.selected_slack_public_channels = data;
            $scope.selected_slack_public_channels_error = false;
        };

        $scope.selected_private_channel_error_array = [];

        $scope.select_slack_private_channels = function (data, item) {
            $scope.selected_slack_private_channels_error = false;
            var isMemeber = checkIfBotIsMember(item);
            if ( !isMemeber )
            {
                item.loading = false;
                item.reCheck = function () {
                    item.loading = true;
                    Restangular.all("check-channel-bot")
                    .customGET("", {
                        "channel_id": item.id,
                    })
                    .then(function (response) {
                        item.loading = false;
                        if (response == true )
                        {
                            $scope.selected_private_channel_error_array.splice( $scope.selected_private_channel_error_array.indexOf(item), 1 );
                        }
                    },function (error) {
                        item.loading = false;
                        console.log(error);
                    }
                );
                };
                $scope.selected_private_channel_error_array.push(item);

                $scope.selected_slack_private_channels_error = true; 
            }
            $scope.selected_slack_private_channels = data;
            
        };
        $scope.deselect_slack_private_channels = function(item){
            if ( $scope.selected_private_channel_error_array.indexOf(item) !== -1 )
            {
                $scope.selected_private_channel_error_array.splice( $scope.selected_private_channel_error_array.indexOf(item), 1 );
            }
            console.log($scope.selected_slack_private_channels);
        };
        
        $scope.select_sales_manager = function (data) {
            $scope.selected_sales_manager = data;
            $scope.selected_sales_manager_error = false;
        };

        $scope.select_project_coordinator = function (data) {
            $scope.selected_project_coordinator = data;
            $scope.selected_project_coordinator_error = false;
        };

        $scope.select_project_manager = function (data) {
            $scope.selected_project_manager = data;
            $scope.selected_project_manager_error = false;
        };

        $scope.select_code_lead = function (data) {
            $scope.selected_code_lead = data;
            $scope.selected_code_lead_error = false;
        };

        $scope.select_delivery_lead = function (data) {
            $scope.selected_delivery_lead = data;
            $scope.selected_delivery_lead_error = false;
        };

        $scope.select_ui_ux_lead = function (data) {
            $scope.leads.ui_ux_lead = data;
            $scope.selected_ui_ux_lead_error = false;
        };

        $scope.select_dev_op_lead = function (data) {
            $scope.leads.dev_op_lead = data;
            $scope.selected_dev_op_lead_error = false;
        };

        $scope.select_architect = function (data) {
            $scope.leads.architect_lead = data;
            $scope.selected_architect_error = false;
        };

        $scope.select_deployment_lead = function (data) {
            $scope.leads.deployment_lead = data;
            $scope.selected_deployment_lead_error = false;
        };

        $scope.select_qa_lead = function (data) {
            $scope.leads.qa_lead = data;
            $scope.selected_qa_lead_error = false;
        };

        $scope.checkForErrors = function() {
            var error = false;
            if ( $scope.selected_client == '' )
            {
                $scope.selected_client_error = true;
                error = true;
            }
            if ( $scope.project_name == '' )
            {
                $scope.project_name_error = true;
                error = true;
            }
            if ( $scope.selected_project_category == '' )
            {
                $scope.selected_project_category_error = true;
                error = true;
            }
            if ( $scope.status == '' )
            {
                $scope.status_error = true;
                error = true;
            }
            if ( $scope.selected_sales_manager == '' )
            {
                $scope.selected_sales_manager_error = true;
                error = true;
            }
            if ( $scope.selected_project_coordinator == '' )
            {
                $scope.selected_project_coordinator_error = true;
                error = true;
            }
            if ( $scope.selected_project_manager == '' )
            {
                $scope.selected_project_manager_error = true;
                error = true;
            }
            if ( $scope.selected_code_lead == '' )
            {
                $scope.selected_code_lead_error = true;
                error = true;
            }
            if ( $scope.selected_delivery_lead == '' )
            {
                $scope.selected_delivery_lead_error = true;
                error = true;
            }
            if ($scope.checkBoxes.ui_ux != '' && $scope.leads.ui_ux_lead == '' )
            {
                $scope.selected_ui_ux_lead_error = true;
                error = true;
            }
            if ($scope.checkBoxes.dev_op != '' && $scope.leads.dev_op_lead == '' )
            {
                $scope.selected_dev_op_lead_error = true;
                error = true;
            }
            if ($scope.checkBoxes.architect != '' && $scope.leads.architect_lead == '' )
            {
                $scope.selected_architect_error= true;
                error = true;
            }
            if ($scope.checkBoxes.deployment != '' && $scope.leads.deployment_lead == '' )
            {
                $scope.selected_deployment_lead_error = true;
                error = true;
            }
            if ($scope.checkBoxes.qa != '' && $scope.leads.qa_lead == '' )
            {
                $scope.selected_qa_lead_error = true;
                error = true;
            }
            if ($scope.selected_private_channel_error_array.length > 0  )
            {
                error = true;
            }
            return error;

        };

        $scope.createProject = function () {
            var endDate = '';
            if ( $scope.end_date.date )
            {
                endDate = moment($scope.end_date.date).format("YYYY-MM-DD");
            }
            var error = $scope.checkForErrors();
            if ( error )
            {
                return;
            }
            else {
                // Make post request
                var startDate = moment($scope.start_date.date).format(
                "YYYY-MM-DD"
                );
                var params = {
                    'company_id' : $scope.selected_client.id,
                    'project_name' : $scope.project_name,
                    'start_date' : startDate,
                    'end_date' : endDate,
                    'project_manager' : $scope.selected_project_manager.name,
                    'project_manager_id' : $scope.selected_project_manager.id,
                    'account_manager_id' : $scope.selected_project_coordinator.id,
                    'project_category_id' : $scope.selected_project_category.id,
                    'bd_manager_id' : $scope.selected_sales_manager.id,
                    'code_lead_id' : $scope.selected_code_lead.id,
                    'delivery_lead_id' : $scope.selected_delivery_lead.id,
                    'email_reminder' : $scope.email_reminder,
                    'status' : $scope.status,
                    'slack_public_channels': $scope.selected_slack_public_channels,
                    'slack_private_channels': $scope.selected_slack_private_channels,
                    'project_leads': $scope.leads,
                    'lead_type': $scope.checkBoxes
                };
                Restangular.all("project/create")
                .post(params)
                .then(
                    function(response) {
                        console.log(response);
                        var path = "/admin/project";
                        window.location.href =  path;
                    },
                    function(errors) {
                        console.log(errors);
                    }
                );
            }
            
        };
    }]);

angular
    .module("myApp")
    .controller("editProjectCtrl",["$scope", "Restangular", function ($scope, Restangular) {
        $scope.clientList = window.clientList;
        $scope.projectCategories = window.projectCategories;
        $scope.slackPublicChannels = window.slackPublicChannels;
        $scope.slackPrivateChannels = window.slackPrivateChannels;
        $scope.salesExecutiveList = window.salesExecutiveList;
        $scope.projectCoordinatorList = window.projectCoordinatorList;
        $scope.projectManagerList = window.projectManagerList;
        $scope.codeLeadList = window.codeLeadList;
        $scope.deliveryLeadList = window.deliveryLeadList;
        $scope.allActiveUserList = window.allActiveUserList;
        $scope.project = window.project;
        $scope.selected_public_channels = window.selected_public_channels;
        $scope.selected_private_channels = window.selected_private_channels;
        $scope.antManagerId = window.antManagerId;
        $scope.selected_client = '';
        $scope.selected_project_category = '';
        $scope.email_reminder = 'true';
        $scope.status_list = [
            { value : "open", name : "Open" },
            { value : "initial", name : "Initial" },
            { value : "sow", name : "SOW" },
            { value : "nda", name : "NDA" },
            { value : "assigned", name : "Assigned" },
            { value : "in_progress", name : "In progress" },
            { value : "completed", name : "Completed" },
            { value : "closed", name : "Closed" },
            { value : "free", name : "Free" }
        ];
        
        $scope.checkBoxes = {
            ui_ux : '',
            dev_op : '',
            architect : '',
            deployment : '',
            qa : ''
        };
        $scope.leads = {
            ui_ux_lead : '',
            dev_op_lead : '',
            architect_lead : '',
            deployment_lead : '',
            qa_lead : ''
        };

        for (var i = 0; i < $scope.project.project_leads.length; i++) {
            switch ($scope.project.project_leads[i].key) {
                case 'ui_ux_lead':
                    if ( $scope.project.project_leads[i].is_ours == 1) {
                        $scope.checkBoxes.ui_ux = 'geekyants';
                        $scope.leads.ui_ux_lead = Number($scope.project.project_leads[i].value);
                    }
                    else {
                        $scope.checkBoxes.ui_ux = 'client';
                        $scope.leads.ui_ux_lead = $scope.project.project_leads[i].value;
                    }
                    break;
                case 'dev_op_lead':
                    if ( $scope.project.project_leads[i].is_ours == 1) {
                        $scope.checkBoxes.dev_op = 'geekyants';
                        $scope.leads.dev_op_lead = Number($scope.project.project_leads[i].value);
                    }
                    else {
                        $scope.checkBoxes.dev_op = 'client';
                        $scope.leads.dev_op_lead = $scope.project.project_leads[i].value;
                    }
                    break;
                case 'architect_lead':
                    if ( $scope.project.project_leads[i].is_ours == 1) {
                        $scope.checkBoxes.architect = 'geekyants';
                        $scope.leads.architect_lead = Number($scope.project.project_leads[i].value);
                    }
                    else {
                        $scope.checkBoxes.architect = 'client';  
                        $scope.leads.architect_lead = $scope.project.project_leads[i].value;  
                    }
                    break;
                case 'deployment_lead':
                    if ( $scope.project.project_leads[i].is_ours == 1) {
                        $scope.checkBoxes.deployment = 'geekyants';
                        $scope.leads.deployment_lead = Number($scope.project.project_leads[i].value);
                    }
                    else {
                        $scope.checkBoxes.deployment = 'client';  
                        $scope.leads.deployment_lead = $scope.project.project_leads[i].value;  
                    }
                    break;
                case 'qa_lead':
                    if ( $scope.project.project_leads[i].is_ours == 1) {
                        $scope.checkBoxes.qa = 'geekyants';
                        $scope.leads.qa_lead = Number($scope.project.project_leads[i].value);
                    }
                    else {
                        $scope.checkBoxes.qa = 'client';
                        $scope.leads.qa_lead = $scope.project.project_leads[i].value;
                    }
                    break;
                default:
            }
        }

        $scope.start_date = {
            options: {
              applyClass: "btn-success",
              singleDatePicker: true,
              locale: {
                applyLabel: "Apply",
                fromLabel: "From",
                // format: "YYYY-MM-DD", //will give you 2017-01-06
                format: "D-MMM-YY", //will give you 6-Jan-17
                // format: "D-MMMM-YY", //will give you 6-January-17
                toLabel: "To",
                cancelLabel: "Cancel",
                customRangeLabel: "Custom range"
              }
            }
          };
          $scope.start_date.date = moment($scope.project.start_date);
          
          $scope.end_date = {
            options: {
              applyClass: "btn-success",
              singleDatePicker: true,
              locale: {
                applyLabel: "Apply",
                fromLabel: "From",
                // format: "YYYY-MM-DD", //will give you 2017-01-06
                format: "D-MMM-YY", //will give you 6-Jan-17
                // format: "D-MMMM-YY", //will give you 6-January-17
                toLabel: "To",
                cancelLabel: "Cancel",
                customRangeLabel: "Custom range"
              },
              date: null
            }
          };
        if ( $scope.project.end_date )
            $scope.end_date.date = moment($scope.project.end_date);

        var checkIfBotIsMember = function (channel) {
            if(channel.members.indexOf($scope.antManagerId) !== -1) {
                return true;
            }
            else 
                return false;
        };

        $scope.select_client = function (data) {
            $scope.project.company_id = data.id;
            $scope.selected_client_error = false;
        };

        $scope.select_project_category = function (data) {
            $scope.project.project_category_id = data.id;
            $scope.selected_project_category_error = false;
        };
        $scope.select_slack_public_channels = function (data) {
            $scope.selected_public_channels = data;
            $scope.selected_slack_public_channels_error = false;
        };
        $scope.deselect_slack_public_channels = function (data, item) {
            $scope.selected_public_channels = data;
            $scope.selected_slack_public_channels_error = false;
        };
        $scope.selected_private_channel_error_array = [];

        $scope.select_slack_private_channels = function (data, item) {
            $scope.selected_slack_private_channels_error = false;
            var isMemeber = checkIfBotIsMember(item);
            if ( !isMemeber )
            {
                item.loading = false;
                item.reCheck = function () {
                    item.loading = true;
                    Restangular.all("check-channel-bot")
                    .customGET("", {
                        "channel_id": item.id,
                    })
                    .then(function (response) {
                        item.loading = false;
                        if (response == true )
                        {
                            $scope.selected_private_channel_error_array.splice( $scope.selected_private_channel_error_array.indexOf(item), 1 );
                        }
                    },function (error) {
                        item.loading = false;
                        console.log(error);
                    }
                );
                };
                $scope.selected_private_channel_error_array.push(item);

                $scope.selected_slack_private_channels_error = true; 
            }
            
            $scope.selected_private_channels = data;
        };

        $scope.deselect_slack_private_channels = function(data, item) {
            if ( $scope.selected_private_channel_error_array.indexOf(item) !== -1 )
            {
                $scope.selected_private_channel_error_array.splice( $scope.selected_private_channel_error_array.indexOf(item), 1 );
            }
            $scope.selected_private_channels = data;
        };

        $scope.select_sales_manager = function (data) {
            $scope.project.bd_manager_id = data.id;
            $scope.selected_sales_manager_error = false;
        };

        $scope.select_project_coordinator = function (data) {
            $scope.project.account_manager_id = data.id;
            $scope.selected_project_coordinator_error = false;
        };

        $scope.select_project_manager = function (data) {
            $scope.project.project_manager_id = data.id;
            $scope.selected_project_manager_error = false;
        };

        $scope.select_code_lead = function (data) {
            $scope.project.code_lead_id = data.id;
            $scope.selected_code_lead_error = false;
        };

        $scope.select_delivery_lead = function (data) {
            $scope.project.delivery_lead_id = data.id;
            $scope.selected_delivery_lead_error = false;
        };

        $scope.select_ui_ux_lead = function (data) {
            $scope.leads.ui_ux_lead = data.id;
            $scope.selected_ui_ux_lead_error = false;
        };

        $scope.select_dev_op_lead = function (data) {
            $scope.leads.dev_op_lead = data.id;
            $scope.selected_dev_op_lead_error = false;
        };

        $scope.select_architect = function (data) {
            $scope.leads.architect_lead = data.id;
            $scope.selected_architect_error = false;
        };

        $scope.select_deployment_lead = function (data) {
            $scope.leads.deployment_lead = data.id;
            $scope.selected_deployment_lead_error = false;
        };

        $scope.select_qa_lead = function (data) {
            $scope.leads.qa_lead = data.id;
            $scope.selected_qa_lead_error = false;
        };

        $scope.checkForErrors = function() {
            var error = false;
            if ( $scope.project.company_id == '' )
            {
                $scope.selected_client_error = true;
                error = true;
            }
            if ( $scope.project.project_name == '' )
            {
                $scope.project_name_error = true;
                error = true;
            }
            if ( $scope.project.project_category_id == '' )
            {
                $scope.selected_project_category_error = true;
                error = true;
            }
            if ( $scope.project.status == '' )
            {
                $scope.status_error = true;
                error = true;
            }
            if ( $scope.project.bd_manager_id == '' || $scope.project.bd_manager_id == null )
            {
                $scope.selected_sales_manager_error = true;
                error = true;
            }
            if ( $scope.project.account_manager_id == '' || $scope.project.account_manager_id == null )
            {
                $scope.selected_project_coordinator_error = true;
                error = true;
            }
            if ( $scope.project.project_manager_id == '' || $scope.project.project_manager_id == null )
            {
                $scope.selected_project_manager_error = true;
                error = true;
            }
            if ( $scope.project.code_lead_id == '' || $scope.project.code_lead_id == null )
            {
                $scope.selected_code_lead_error = true;
                error = true;
            }
            if ( $scope.project.delivery_lead_id == '' || $scope.project.delivery_lead_id == null )
            {
                $scope.selected_delivery_lead_error = true;
                error = true;
            }
            if ($scope.checkBoxes.ui_ux != '' && $scope.leads.ui_ux_lead == '' )
            {
                $scope.selected_ui_ux_lead_error = true;
                error = true;
            }
            if ($scope.checkBoxes.dev_op != '' && $scope.leads.dev_op_lead == '' )
            {
                $scope.selected_dev_op_lead_error = true;
                error = true;
            }
            if ($scope.checkBoxes.architect != '' && $scope.leads.architect_lead == '' )
            {
                $scope.selected_architect_error= true;
                error = true;
            }
            if ($scope.checkBoxes.deployment != '' && $scope.leads.deployment_lead == '' )
            {
                $scope.selected_deployment_lead_error = true;
                error = true;
            }
            if ($scope.checkBoxes.qa != '' && $scope.leads.qa_lead == '' )
            {
                $scope.selected_qa_lead_error = true;
                error = true;
            }
            if ($scope.selected_private_channel_error_array.length > 0  )
            {
                error = true;
            }
            return error;

        };

        $scope.createProject = function () {
            var endDate = '';
            if ( $scope.end_date.date )
            {
                endDate = moment($scope.end_date.date).format("YYYY-MM-DD");
            }
            var error = $scope.checkForErrors();
            if ( error )
            {
                return;
            }
            else {
                // Make post request
                var startDate = moment($scope.start_date.date).format(
                "YYYY-MM-DD"
                );
                var params = {
                    'project_id' : $scope.project.id,
                    'company_id' : $scope.project.company_id,
                    'project_name' : $scope.project.project_name,
                    'start_date' : startDate,
                    'end_date' : endDate,
                    'project_manager_id' : $scope.project.project_manager_id,
                    'account_manager_id' : $scope.project.account_manager_id,
                    'project_category_id' : $scope.project.project_category_id,
                    'bd_manager_id' : $scope.project.bd_manager_id,
                    'code_lead_id' : $scope.project.code_lead_id,
                    'delivery_lead_id' : $scope.project.delivery_lead_id,
                    'email_reminder' : $scope.project.email_reminder,
                    'status' : $scope.project.status,
                    'slack_public_channels': $scope.selected_public_channels,
                    'slack_private_channels': $scope.selected_private_channels,
                    'project_leads': $scope.leads,
                    'lead_type': $scope.checkBoxes
                };
                Restangular.all("project/edit")
                .post(params)
                .then(
                    function(response) {
                        var path = "/admin/project/"+$scope.project.id+"/dashboard";
                        window.location.href =  path;
                    },
                    function(errors) {
                        console.log(errors);
                    }
                );
            }
            
        };
    }]);

angular.module("myApp").controller("incidentCtrl", [
  "$scope",
  "Restangular",
  "$uibModal",
  "DTOptionsBuilder",
  "Datatable",
  function ($scope, Restangular, $uibModal, DTOptionsBuilder, Datatable) {
    $scope.filter = {};
    $scope.data = [];
    $scope.departments = [];

    $scope.originalData = [];
    Restangular.all('incident').getList({'per_page': 1000}).then(function (response) {
      $scope.data = response;
      $scope.originalData = response;
    }, function(error){
      console.log('error fetching incident list', error)
    });
    Restangular.all('department').getList().then(function (response) {
      $scope.departments = response;
    }, function(error){
      console.log('error fetching incident list', error)
    });
    

    //$("#dep_filter").select2();
    $("#status_filter").select2();
    
    $scope.departmentFilter = function(values) {
        var selectedOptions = [];
        angular.forEach(values, function(item){
            selectedOptions.push(item.type);
        });
        $scope.data = Datatable.filterData($scope.filter, $scope.originalData, selectedOptions, "department.type");
    }
    $("#dep_filter").change(function (event) {
      console.log('dep_filter called');
      $scope.$apply(function () {
        var selectedOptions = event.val;
        $scope.data = Datatable.filterData($scope.filter, $scope.originalData, selectedOptions, "department.type");
      });
    });

    $("#status_filter").change(function (event) {
      $scope.$apply(function () {
        var selectedOptions = event.val
        $scope.data = Datatable.filterData($scope.filter, $scope.originalData, selectedOptions, "status");
      });
    });

// >>>>>>> feat/incident-department-filter
    $scope.dtOptions = DTOptionsBuilder.newOptions()
      .withOption('order', [[0, 'DESC']])
      .withOption('scrollY', '300px')
      .withOption('scrollX', '100%')
      .withOption('scrollCollapse', true)
      .withOption('pageLength', 1000)
      .withOption('lengthMenu', [50, 100, 150, 200, 500, 1000])


    $scope.viewDetail = function (id) {
      console.log('incidentCtrl ', id);
      var path = "/admin/incident/" + id;
      window.location.href = path;
    };
    $scope.editDetail = function (id) {

      var modal = $uibModal.open({
        controller: "incidentEditModalCtrl",
        windowClass: "bootstrap_wrap",
        templateUrl: "/views/incident/create.html",
        backdrop: "static",
        size: "md",
        resolve: {
          data: function () {
            return { id: id };
          }
        }
      });
      modal.result.then(
        function (result) {
          window.location.reload();
        },
        function () {
          //cancel
        }
      );
    };
    $scope.deleteDetails = function (id) {
      if (confirm("Are you sure you want to cancel the leave!")) {
        Restangular.one("leave", id)
          .remove()
          .then(
            function (response) {
              location.reload();
            },
            function (error) {
              alert(error.data.message);
            }
          );
      }
    };
    $scope.createIncident = function () {
      var modal = $uibModal.open({
        controller: "createIncidentModalCtrl",
        windowClass: "bootstrap_wrap",
        templateUrl: "/views/incident/create.html",
        backdrop: "static",
        size: "md",
        resolve: {
          data: function () {
            return {};
          }
        }
      });
      modal.result.then(
        function (result) {
          window.location.reload();
        },
        function () {
          //cancel
        }
      );
    };
  }
]);

angular.module("myApp").controller("createIncidentModalCtrl", [
  "$scope",
  "Restangular",
  "data",
  "$uibModalInstance",
  "$q",
  function($scope, Restangular, data, $uibModalInstance) {
    $scope.form = {
      submitted: false,
      loading: false,
      message: null,
      error: false,
      departments : [],
      users: [],
      model: {
        department : null,
        department_id: null,
        subject: null,
        description: null,
        shared_users: []
      },
      init: function() {
        $scope.form.loading = true;
        Restangular.all("department")
          .getList()
          .then(
            function(response) {
              $scope.form.loading = false;
              $scope.form.departments = response;
            },
            function(error) {
              $scope.form.loading = false;
              console.log(error);
            }
          );
          Restangular.all("user?pagination=0")
          .getList()
          .then(
            function(response) {
              $scope.form.loading = false;
              $scope.form.users = response;
            },
            function(error) {
              $scope.form.loading = false;
              console.log(error);
            }
          );
      },
      submit: function($form) {
        $scope.form.submitted = true;
        $scope.form.error = false;
        $scope.form.message = null;
        

        if (!$form.$valid) {
          $scope.form.submitted = false;
          return false;
        }
        var params = {
          department_id : $scope.form.model.department.id, 
          subject : $scope.form.model.subject,
          description:  $scope.form.model.description,
          shared_users: $scope.form.model.shared_users
        }
        $scope.form.loading = true;
        Restangular.all("incident")
          .post(params)
          .then(
            function(response) {
              $scope.form.loading = false;
              $uibModalInstance.close(response);
            },
            function(error) {
              $scope.form.error = true;
              $scope.form.loading = false;
              $scope.form.message = error.data.message;
              //$uibModalInstance.dismiss();
            }
          );
      },
      cancel: function() {
        $uibModalInstance.dismiss();
      },
      select_users: function(data) {
        $scope.form.shared_users = data;
      }
    };
    $scope.form.init();
  }
]);

angular.module("myApp").controller("incidentViewModalCtrl", [
  "$scope",
  "Restangular",
  "data",
  "$uibModalInstance",
  "$q",
  function($scope, Restangular, data, $uibModalInstance) {
    var id = data.id;
    $scope.form = {
      loading: false,
      data: {},
      init: function() {
        $scope.form.loading = true;
        Restangular.one("incident", id)
          .get()
          .then(
            function(response) {
              console.log('data', response);
              $scope.form.loading = false;
              $scope.form.data = response;
            },
            function(error) {
              console.log(error);
              $scope.form.loading = false;
            }
          );
      },
      cancel: function() {
        $uibModalInstance.dismiss();
      }
    };
    $scope.form.init();
  }
]);

angular.module("myApp").controller("incidentEditModalCtrl", [
  "$scope",
  "Restangular",
  "data",
  "$uibModalInstance",
  "$q",
  function($scope, Restangular, data, $uibModalInstance, $q) {
    var id = data.id;
    var incidentObj = Restangular.one("incident", id);
    $scope.form = {
      editing: true,
      submitted: false,
      loading: false,
      itemError: false,
      error: false,
      model: {
        id : null,
        department : null,
        department_id : null,
        subject : null,
        shared_users: [],
      },
      departments : [],
      users: [],
      item: { name: null, unit_price: null, quantity: null, total: null },
      init: function() {
        var departments = Restangular.all("department").getList();
        var users = Restangular.all("user?pagination=0").getList();
        var incident = incidentObj.get();
        $q.all([departments, incident, users, ($scope.form.loading = true)]).then(
          function(result) {
            $scope.form.loading = false;
            if (result[0]) {
              $scope.form.departments = result[0];
            }
            if (result[1]) {
              
              $scope.form.model = result[1];
              $scope.form.model.shared_users = [];
              $scope.form.model.department = $scope.form.model.department;
              
              for ( i=0; i<$scope.form.model.incident_users.length; i++  )
              {
                $scope.form.model.shared_users.push($scope.form.model.incident_users[i].user_id);
              }
            }
            if (result[2]) {
              $scope.form.users = result[2];
            }
          }
        );
      },
      submit: function($form) {
        $scope.form.submitted = true;

        if (!$form.$valid) {
          $scope.form.error = true;
          $scope.form.submitted = false;
          return false;
        }
        $scope.form.loading = true;
        incidentObj.subject = $scope.form.model.subject;
        incidentObj.description = $scope.form.model.description;
        incidentObj.department_id = $scope.form.model.department.id;
        incidentObj.shared_users = $scope.form.model.shared_users;
        incidentObj.put()
          .then(
            function(response) {
              $scope.form.submitted = false;
              $scope.form.loading = false;
              $uibModalInstance.close(response);
            },
            function(error) {
              console.log(error);
              $scope.form.loading = false;
              $uibModalInstance.dismiss();
            }
          );
      },
      cancel: function() {
        $uibModalInstance.dismiss();
      },
    };
    $scope.form.init();
  }
]);

angular.module('myApp')
    .controller("adminTimesheetReviewCtrl", ["$scope", "Restangular", "$uibModal", function ($scope, Restangular, $uibModal) {
        var monthId = window.monthId;
        var userId = window.userId;
        $scope.test = "Admin review";

        
        $scope.model = {
            userId : userId,
            monthId : monthId,
            loading : false,
            data : {},

            getUserReport : function(userId, monthId) {
                console.log('U', userId);
                console.log('M', monthId);
                $scope.model.loading = true;
                var params = {monthId : $scope.model.monthId, userId: $scope.model.userId};
                Restangular.oneUrl("get-user-timesheet/"+monthId+"/"+userId)
                .get() 
                .then(function (response) {
                    $scope.model.loading = false;
                    $scope.model.data = response;
                }, function (error) {
                    $scope.model.loading = false;
                    console.log('response', response);
                });    
            },
            approveUserTimelog : function(id) {
                // api/v1/new-timesheet/approval/44
                // PUT

            },
            reviewTimesheet : function(monthId, userId, date) {
                console.log('getLastDay',date);
                var modal = $uibModal.open({
                    controller: "adminTimesheetExtraHourCtrl",
                    windowClass: "bootstrap_wrap",
                    templateUrl: "/views/user-timesheet/extra-hour.html",
                    backdrop: "static",
                    size: "lg",
                    resolve: {
                        data: function () {
                            return {
                                "month_id" : monthId,
                                "user_id" : userId,
                                "date" : date
                            }
                        }
                    }
                });
                modal.result.then(function (res) { 
                    window.location.reload();
                    $scope.model.data = res;
                }, function () { });
            },
            splitHours : function(index) {

                var totalApprovedHour = $scope.model.data.days[index].approved_total_time;
                console.log('splitHours called',totalApprovedHour)
                var approvedHour = 8;
                if(totalApprovedHour > 10) {

                    var extraHour = totalApprovedHour - approvedHour;
                    $scope.model.data.days[index].approved_hour = approvedHour;
                    $scope.model.data.days[index].extra_hour = extraHour;
                }
                
            },

            approveBonusRequest : function(parentIndex, index) {
                var day = $scope.model.data.days[parentIndex]['bonus_requests'][index];
                var bonusRequest = day;
                $scope.model.data.days[parentIndex]['bonus_requests'][index].loading = true;
                Restangular.all("bonus-request-approve")
                    .post(bonusRequest)
                    .then(function(response) {
                        $scope.model.data.days[parentIndex]['bonus_requests'][index] = response;
                        $scope.model.data.days[parentIndex]['bonus_requests'][index].loading = false;
                },
                function(error) {
                    $scope.model.data.days[index].additional_bonus_request.loading = false;
                    console.log(error);
                });
            },
            rejectBonusRequest : function(parentIndex, index) {
                var day = $scope.model.data.days[parentIndex]['bonus_requests'][index];
                $scope.model.data.days[parentIndex]['bonus_requests'][index].loading = true;
                var bonusRequest = day;
                Restangular.all("bonus-request-reject")
                    .post(bonusRequest)
                    .then(
                      function(response) {
                        $scope.model.data.days[parentIndex]['bonus_requests'][index] = response;
                        $scope.model.data.days[parentIndex]['bonus_requests'][index].loading = false;
                        //$scope.init();
                      },
                      function(error) {
                        $$scope.model.data.days[parentIndex]['bonus_requests'][index].loading = false;
                        console.log(error);
                      }
                    );
            },
            addExtra : function(index) {
                
                var day = $scope.model.data.days[index];
                console.log('DAY ', day.approve_extra_hour);
                if(day.approve_extra_hour) {

                    
                    $scope.model.data.days[index].updating = true;
                    var extraHours = day.extra_hour.extra_hours;
                    var projectId = day.extra_hour.project_id;
                    if(extraHours > 0) {
                        var params = {id: day.id, user_id: userId,date : day.date, extra_hours : extraHours, project_id : projectId};
                        Restangular.all('user-timesheet-extra').post(params).then(function(response){
                            //$scope.model.data.days[index].approve_extra_hour = true;
                            $scope.model.data.days[index].updating = false;
                            $scope.model.data.days[index].splitting = false;
                            $scope.model.data.days[index].approved_total_time = 8;
                            $scope.model.data.days[index].extra_hours = extraHours;
                            $scope.model.data.days[index].extra_hour = response;
                        }, function(error){
                            $scope.model.data.days[index].splitting = false;
                            $scope.model.data.days[index].updating = false;
                            console.log('error', error);
                        })
                    }
                } else {
                    console.log('day.extra_hour delete');
                    Restangular.one('user-timesheet-extra', day.extra_hour.id ).remove().then(function(response){
                            //$scope.model.data.days[index].approve_extra_hour = true;
                            $scope.model.data.days[index].updating = false;
                            $scope.model.data.days[index].splitting = false;
                            $scope.model.data.days[index].approved_total_time = 8;
                            $scope.model.data.days[index].extra_hour.extra_hours = 0;
                            $scope.model.data.days[index].extra_hour.project_id = null;
                        }, function(error){
                            $scope.model.data.days[index].splitting = false;
                            $scope.model.data.days[index].updating = false;
                            console.log('error', error);
                        })
                }
                
            }
            
        };
        
        $scope.model.getUserReport(userId, monthId);

        // $scope.projects = window.projects;
        // $scope.verticalTotals = JSON.parse(window.verticalTotals);
        // $scope.user = JSON.parse(window.user);
        // $scope.currentDate = new Date().toISOString().slice(0, 10);

        // $scope.addTime = function (name, result) {
        //     $scope.list = {
        //         project_name: name,
        //         data: result
        //     };

        //     var modal = $uibModal.open({
        //         controller: "modalCtrl",
        //         windowClass: "bootstrap_wrap",
        //         templateUrl: "/views/add_time.html",
        //         backdrop: "static",
        //         size: "md",
        //         resolve: {
        //             data: function () {
        //                 return $scope.list;
        //             }
        //         }
        //     });
        //     modal.result.then(function (res) { }, function () { });
        // };
        // $scope.addTimeAprrover = function (name, result, user) {
        //     $scope.list = {
        //         project_name: name,
        //         approved_hours: result.approved_hours,
        //         data: result,
        //         user_name: user
        //     };
        //     var modal = $uibModal.open({
        //         controller: "adminModalCtrl",
        //         windowClass: "bootstrap_wrap",
        //         templateUrl: "/views/approve_time.html",
        //         backdrop: "static",
        //         size: "md",
        //         resolve: {
        //             data: function () {
        //                 return $scope.list;
        //             }
        //         }
        //     });
        //     modal.result.then(function (res) { }, function () { });
        // };
        // $scope.updateApproveList = function (data) {
        //     var params = {
        //         timesheet_id: data.timesheet_id,
        //         approved_hours: data.approved_hours
        //     };
        //     Restangular.all("admin/timesheet")
        //         .post(params)
        //         .then(function (response) { }, function (error) { });
        // };
    }]);
angular.module('myApp')
    .controller("adminTimesheetExtraHourCtrl", ["$scope", "Restangular", "data", "$uibModalInstance", function ($scope, Restangular, data, $uibModalInstance) {
        var monthId = data.month_id;
        var userId = data.user_id;
        var date = data.date;
        console.log('user_id ', data);
        $scope.data = {
            extra_hours : 0,
            user_name : null,
            month_id : monthId,
            bonus_extra_hour : 0,
            extra_hours_request : false
        }
        // $scope.calculateTotal = function(){
        //     var sum = 0;
        //     angular.forEach($scope.form.extra_hours,function(extraHour){
        //         sum = sum + extraHour.extra_hours;
        //     });
        //     $scope.form.total_hour =sum;

        //     var days = (sum/8);
        //     $scope.form.total_days = days;
        //     $scope.form.total_days = Number((days).toFixed(1)); 
        // }
         
        $scope.form = {
            error : false,
            message : null,
            data : data,
            extra_hours : [],
            total_hour : 0,
            total_days : 0,
            loading : false,
            submitting : false,
            extra_hours_request : false,
            submit : function(form) {
                var params = {
                    extra_hours_request : $scope.form.extra_hours_request,
                    hours : $scope.form.bonus_extra_hour,
                    month_id : monthId,
                    user_id : userId,
                    type : 'extra',
                    date : date,
                    redeem_type : {
                        "redeem_type":"encash", 
                        'type': 'extra', 
                        'days' : $scope.form.total_days,
                        'hours' : $scope.form.hours
                    }
                };
                
                $scope.form.submitting = true;
                Restangular.all('user-timesheet/approve/'+data.user_id+'/'+data.month_id).post(params).then(function(response){
                    $scope.form.submitting = false;
                    $scope.form.error = false;
                    $uibModalInstance.close(response);
                    
                }, function(error){
                    $scope.form.submitting = false;
                    $scope.form.error = true;
                    $scope.form.message = error.data.message;
                })
            },
            cancel : function() {
              $uibModalInstance.dismiss();
            }
        }
        
        Restangular.one("user-timesheet-month/"+monthId+"/"+userId)
            .get() 
            .then(function (response) {
                console.log('RES', response);
                extraHour = parseInt(response.extra_hour);
                $scope.data = response;
                $scope.data.extra_hours = extraHour;
                $scope.form.bonus_extra_hour = extraHour;
            }, function (error) {
                $scope.model.loading = false;
                console.log('response', response);
            }); 
        // var params = {user_id: data.user_id, month_id :data.month_id};
        // Restangular.all('user-timesheet-extra').getList(params).then(function(response){
        //     $scope.form.extra_hours = response;
        //     $scope.calculateTotal();
        // }, function(error){
        //     console.log('Error', error);
        // });
        // Restangular.all('user-timesheet-extra').getList(params).then(function(response){
        //     $scope.form.extra_hours = response;
        //     $scope.calculateTotal();
        // }, function(error){
        //     console.log('Error', error);
        // })

    }]);
"use strict()";

/*Directives*/
angular
  .module("myApp")
  .directive("basicInfo", [
    function() {
      return {
        templateUrl: "/scripts/directives/basic-info/basic-info.html?v=1",
        restrict: "E",
        replace: true,
        scope: {
          projectId: "=",
          isDisabled: "@isDisabled"
        },
        controller: ["$scope", "Restangular", function($scope, Restangular) {
          $scope.projectData;
          $scope.loadProjectData = function() {
            $scope.projectData;
            $scope.loading = true;
            Restangular.one("project", $scope.projectId)
              .get()
              .then(
                function(response) {
                  $scope.loading = false;
                  $scope.projectData = response;
                },
                function(error) {
                  console.log(error);
                }
              );
          };
          $scope.loadProjectData();

          $scope.filteredDate = function(input_date) {
            var formatted_date = new Date(input_date);
            return formatted_date;
          };
        }]
      };
    }
  ]);

"use strict()";

/*Directives*/
angular
  .module("myApp")
  .directive("clientDetail", [
    function() {
      return {
        templateUrl: "/scripts/directives/client-detail/client-detail.html?v=1",
        restrict: "E",
        replace: true,
        scope: {
          projectId: "=",
          companyId: "=",
          isDisabled: "@isDisabled"
        },
        controller: ["$scope", "Restangular", function($scope, Restangular) {
          $scope.clientData;

          $scope.loadClientData = function() {
            $scope.clientData;
            $scope.loading = true;
            Restangular.one("project-client", $scope.projectId)
              .get()
              .then(
                function(response) {
                  $scope.loading = false;
                  $scope.clientData = response;
                },
                function(error) {
                  console.log(error);
                }
              );
          };
          $scope.loadClientData();
        }]
      };
    }
  ]);

"use strict()";

/*Directives*/
angular
  .module("myApp")
  .directive("projectTimelog", [
    function() {
      return {
        templateUrl:
          "/scripts/directives/project-timelog/project-timelog.html?v=1",
        restrict: "E",
        replace: true,
        scope: {
          projectId: "=",
          isDisabled: "@isDisabled"
        },
        controller: ["$scope", "Restangular", function($scope, Restangular) {
          $scope.timelog;
          $scope.loadTimelog = function() {
            $scope.timelog;
            $scope.loading = true;
            Restangular.one("project-timelog", $scope.projectId)
              .get()
              .then(
                function(response) {
                  $scope.loading = false;
                  $scope.timelog = response;
                },
                function(error) {
                  console.log(error);
                }
              );
          };
          $scope.loadTimelog();
        }]
      };
    }
  ]);

"use strict()";

/*Directives*/
angular
  .module("myApp")
  .directive("googleDriveLink", [
    function() {
      return {
        templateUrl: "/scripts/directives/google-drive-link/index.html",
        restrict: "E",
        replace: true,
        scope: {
          projectId: "=",
          isDisabled: "@isDisabled"
        },
        controller:["$scope", "Restangular", function($scope, Restangular) {
          $scope.newRequiredProjectLinks = [];
          $scope.oldProjectLinks = [];
          $scope.editable = [];
          $scope.error = false;
          $scope.nameError = false;
          $scope.pathError = false;
          $scope.newProjectLinkObj = {
            name: "",
            path: ""
          };
          $scope.loadRequiredProjectLinks = function() {
            $scope.oldProjectLinks = [];
            $scope.listLoading = true;
            Restangular.all("project-link")
              .customGET("", {
                project_id: $scope.projectId
              })
              .then(
                function(response) {
                  $scope.listLoading = false;
                  for (i = 0; i < response.length; i++) {
                    $scope.oldProjectLinks.push(response[i]);
                  }
                },
                function(error) {
                  console.log(error);
                }
              );
          };
          $scope.loadRequiredProjectLinks();

          $scope.addProjectLinkRow = function() {
            $scope.error = false;
            if (!$scope.newProjectLinkObj.name) {
              $scope.nameError = true;
              $scope.error = true;
            } else {
              $scope.nameError = false;
            }

            if (!$scope.newProjectLinkObj.path) {
              $scope.pathError = true;
              $scope.error = true;
            } else {
              $scope.pathError = false;
            }

            if ($scope.error) {
              return;
            }

            var params = {
              project_id: $scope.projectId,
              name: $scope.newProjectLinkObj.name,
              path: $scope.newProjectLinkObj.path
            };
            console.log("params", params);
            Restangular.all("project-link")
              .post(params)
              .then(
                function(response) {
                  $scope.newProjectLinkObj = {
                    name: "",
                    path: ""
                  };
                  $scope.loadRequiredProjectLinks();
                },
                function(error) {
                  console.log(error);
                }
              );
          };
          $scope.editProjectLinkRow = function(index) {
            console.log(index);
            $scope.editable[index] = true;
            console.log($scope.editable);
          };
          $scope.saveProjectLinkRow = function(index, ProjectLinkRow) {
            $scope.editable[index] = true;
            Restangular.one("project-link", ProjectLinkRow.id)
              .customPUT({
                ProjectLinkRow: ProjectLinkRow
              })
              .then(
                function(response) {
                  $scope.editable[index] = false;
                },
                function(errors) {
                  console.log(errors);
                }
              );
          };

          $scope.removeProjectLinkRow = function(index, ProjectLinkRow) {
            var r = confirm("Are you sure you want to delete this entry?");
            if (r != true) {
              return;

            }
            Restangular.one("project-link", ProjectLinkRow.id)
              .remove()
              .then(
                function(response) {
                  $scope.oldProjectLinks.splice(index, 1);
                },
                function(error) {
                  console.log(error);
                }
              );
          };
        }]
      };
    }
  ]);

"use strict()";

/*Directives*/
angular
  .module("myApp")
  .directive("billSchedule", [
    function() {
      return {
        templateUrl: "/scripts/directives/bill-schedule/bill-schedule.html?v=1",
        restrict: "E",
        replace: true,
        scope: {
          projectId: "=",
          isDisabled: "@isDisabled"
        },
        controller: [
          "$scope",
          "Restangular",
          function($scope, Restangular) {
            $scope.billScheduleData;
            $scope.loadBillScheduleData = function() {
              $scope.billScheduleData;
              $scope.loading = true;
              Restangular.one("project-bill-schedule", $scope.projectId)
                .get()
                .then(
                  function(response) {
                    $scope.loading = false;
                    $scope.billScheduleData = response;
                  },
                  function(error) {
                    console.log(error);
                  }
                );
            };
            $scope.loadBillScheduleData();

            $scope.filteredDate = function(input_date) {
              var formatted_date = new Date(input_date);
              return formatted_date;
            };
          }
        ]
      };
    }]);

"use strict()";

/*Directives*/
angular
  .module("myApp")
  .directive("additionalWorkdayBonus", [
    function() {
      return {
        templateUrl:
          "/scripts/directives/additional-workday-bonus/index.html?v=1",
        restrict: "E",
        replace: true,
        scope: {
          projectId: "=",
          isDisabled: "@isDisabled"
        },
        controller: [
          "$scope",
          "Restangular",
          "$uibModal",
          function($scope, Restangular, $uibModal) {
            $scope.oldbonusObjs = [];
            $scope.loading = false;
            $scope.showModal = function() {
              var modal = $uibModal.open({
                controller: "additionalBonusmodalAddCtrl",
                windowClass: "bootstrap_wrap",
                templateUrl:
                  "/scripts/directives/additional-workday-bonus/add-edit-modal.html",
                backdrop: "static",
                size: "md",
                resolve: {
                  data: function() {
                    return { project_id: $scope.projectId };
                  }
                }
              });
              modal.result.then(
                function(result) {
                  $scope.oldbonusObjs.push(result);
                },
                function() {
                  //
                }
              );
            };
            $scope.loadOldBonus = function() {
              $scope.oldbonusObjs = [];
              $scope.loading = true;
              Restangular.all("additional-workday-bonus")
                .customGET("", {
                  project_id: $scope.projectId
                })
                .then(
                  function(response) {
                    $scope.loading = false;
                    for (i = 0; i < response.length; i++) {
                      $scope.oldbonusObjs.push(response[i]);
                    }
                  },
                  function(error) {
                    console.log(error);
                  }
                );
            };
            $scope.loadOldBonus();

            $scope.removebonusObjRow = function(index, bonusRow) {
              var r = confirm("Are you sure you want to delete this entry?");
              if (r != true) {
                return;
              }
              Restangular.one("additional-workday-bonus", bonusRow.id)
                .remove()
                .then(
                  function(response) {
                    $scope.oldbonusObjs.splice(index, 1);
                  },
                  function(error) {
                    console.log(error);
                  }
                );
            };
            $scope.editbonusObjRow = function(index, bonusRow) {
              var modal = $uibModal.open({
                controller: "additionalBonusmodalEditCtrl",
                windowClass: "bootstrap_wrap",
                templateUrl:
                  "/scripts/directives/additional-workday-bonus/add-edit-modal.html",
                backdrop: "static",
                size: "md",
                resolve: {
                  data: function() {
                    return {
                      bonus_id: bonusRow.id,
                      project_id: $scope.projectId
                    };
                  }
                }
              });
              modal.result.then(
                function(result) {
                  $scope.loadOldBonus();
                },
                function() {
                  //
                }
              );
            };
            $scope.filteredDate = function(input_date) {
              var formatted_date = new Date(input_date);
              return formatted_date;
            };
          }
        ]
      };
    }
  ])
  .controller("additionalBonusmodalAddCtrl", [
    "$scope",
    "Restangular",
    "data",
    "$uibModalInstance",
    function($scope, Restangular, data, $uibModalInstance) {
      var projectId = data.project_id;

      $scope.form = {
        fetching: true,
        loading: false,
        submitting: false,
        error: false,
        errorMessage: null,
        data: {
          from_date: null,
          to_date: null,
          amount: null,
          type: "weekend",
          notes: null
        },
        init: function() {
          $scope.form.loading = true;
          Restangular.one("project-info", projectId)
            .get()
            .then(
              function(response) {
                $scope.form.loading = false;
                if (response) {
                  $scope.form.project = response;
                  $scope.form.resources = $scope.form.project[0].resources;
                }
              },
              function(error) {
                console.log(error);
              }
            );
        },
        submit: function(bonusForm) {
          $scope.form.submitting = true;
          if (!bonusForm.$valid) {
            $scope.form.error = true;
            return;
          }
          $scope.loading = true;
          var params = {
            project_id: projectId,
            user_id: $scope.selected_user.id,
            type: $scope.form.data.type,
            amount: $scope.form.data.amount,
            from_date: $scope.date.startDate.format("YYYY-MM-DD HH:mm:ss"),
            to_date: $scope.date.endDate.format("YYYY-MM-DD HH:mm:ss"),
            notes: $scope.form.data.notes
          };

          Restangular.all("additional-workday-bonus")
            .post(params)
            .then(
              function(response) {
                $uibModalInstance.close(response);
              },
              function(error) {
                console.log(error);
                $uibModalInstance.dismiss();
              }
            );
        },
        cancel: function() {
          $uibModalInstance.dismiss();
        }
      };
      $scope.form.init();
      $scope.select_user = function(data) {
        $scope.selected_user = data;
      };

      //Daterange picker
      $scope.date = {
        startDate: moment().subtract(1, "days"),
        endDate: moment()
      };

      $scope.opts = {
        locale: {
          applyClass: "btn-green",
          applyLabel: "Apply",
          fromLabel: "From",
          format: "YYYY-MM-DD",
          toLabel: "To",
          cancelLabel: "Cancel",
          customRangeLabel: "Custom range"
        },
        ranges: {
          "Last 7 Days": [moment().subtract(6, "days"), moment()],
          "Last 30 Days": [moment().subtract(29, "days"), moment()]
        }
      };

      //Watch for date changes
      $scope.$watch("date", function(newDate) {}, false);
    }
  ])
  .controller("additionalBonusmodalEditCtrl", [
    "$scope",
    "Restangular",
    "data",
    "$uibModalInstance",
    "$q",
    function($scope, Restangular, data, $uibModalInstance, $q) {
      var projectId = data.project_id;
      var bonusId = data.bonus_id;

      $scope.form = {
        fetching: true,
        loading: false,
        submitting: false,
        error: false,
        errorMessage: null,
        data: {
          from_date: null,
          to_date: null,
          amount: null,
          type: "weekend",
          notes: null
        },
        submit: function(bonusForm) {
          $scope.form.submitting = true;
          if (!bonusForm.$valid) {
            $scope.form.error = true;
            return;
          }
          $scope.loading = true;
          var params = {
            bonus_id: bonusId,
            project_id: projectId,
            user_id: $scope.selected_user.id,
            type: $scope.form.data.type,
            amount: $scope.form.data.amount,
            from_date: $scope.date.startDate.format("YYYY-MM-DD HH:mm:ss"),
            to_date: $scope.date.endDate.format("YYYY-MM-DD HH:mm:ss"),
            notes: $scope.form.data.notes
          };
          Restangular.all("additional-workday-bonus")
            .customPUT(params)
            .then(
              function(response) {
                $scope.loading = false;
                $uibModalInstance.close(response);
              },
              function(error) {
                console.log(error);
                $uibModalInstance.dismiss();
              }
            );
        },
        cancel: function() {
          $uibModalInstance.dismiss();
        }
      };

      var peojectInfo = Restangular.one("project-info", projectId).get();
      var bonus = Restangular.one("additional-workday-bonus", bonusId).get();

      $q.all([peojectInfo, bonus]).then(function(result) {
        $scope.form.fetching = false;
        if (result[0]) {
          $scope.form.project = result[0];
          $scope.form.resources = $scope.form.project[0].resources;
        }
        if (result[1]) {
          $scope.form.data = result[1];
        }
        $scope.date = {
          startDate: moment($scope.form.data.from_date),
          endDate: moment($scope.form.data.to_date)
        };
        $scope.selected_user = $scope.form.data.user;
      });

      $scope.select_user = function(data) {
        console.log(data, "here in select user");
        $scope.selected_user = data;
      };

      //Daterange picker
      $scope.opts = {
        locale: {
          applyClass: "btn-green",
          applyLabel: "Apply",
          fromLabel: "From",
          format: "YYYY-MM-DD",
          toLabel: "To",
          cancelLabel: "Cancel",
          customRangeLabel: "Custom range"
        },
        ranges: {
          "Last 7 Days": [moment().subtract(6, "days"), moment()],
          "Last 30 Days": [moment().subtract(29, "days"), moment()]
        }
      };

      //Watch for date changes
      $scope.$watch("date", function(newDate) {}, false);
    }
  ]);

"use strict()";

/*Directives*/
angular
  .module("myApp")
  .directive("onsiteBonus", [
    function() {
      return {
        templateUrl: "/scripts/directives/onsite-bonus/index.html?v=1",
        restrict: "E",
        replace: true,
        scope: {
          projectId: "=",
          isDisabled: "@isDisabled"
        },
        controller: [
          "$scope",
          "Restangular",
          "$uibModal",
          function($scope, Restangular, $uibModal) {
            $scope.oldbonusObjs = [];
            $scope.loading = false;
            $scope.showModal = function() {
              var modal = $uibModal.open({
                controller: "onsiteBonusaddModalCtrl",
                windowClass: "bootstrap_wrap",
                templateUrl:
                  "/scripts/directives/onsite-bonus/add-edit-modal.html",
                backdrop: "static",
                size: "md",
                resolve: {
                  data: function() {
                    return { project_id: $scope.projectId };
                  }
                }
              });
              modal.result.then(
                function(result) {
                  $scope.oldbonusObjs.push(result);
                },
                function() {
                  //
                }
              );
            };
            $scope.loadOldBonus = function() {
              $scope.oldbonusObjs = [];
              $scope.loading = true;
              Restangular.all("onsite-bonus")
                .customGET("", {
                  project_id: $scope.projectId
                })
                .then(
                  function(response) {
                    $scope.loading = false;
                    for (i = 0; i < response.length; i++) {
                      $scope.oldbonusObjs.push(response[i]);
                    }
                  },
                  function(error) {
                    console.log(error);
                  }
                );
            };
            $scope.loadOldBonus();

            $scope.removebonusObjRow = function(index, bonusRow) {
              var r = confirm("Are you sure you want to delete this entry?");
              if (r != true) {
                return;
              }
              Restangular.one("onsite-bonus", bonusRow.id)
                .remove()
                .then(
                  function(response) {
                    $scope.oldbonusObjs.splice(index, 1);
                  },
                  function(error) {
                    console.log(error);
                  }
                );
            };
            $scope.editbonusObjRow = function(index, bonusRow) {
              var modal = $uibModal.open({
                controller: "onsiteBonusmodalEditCtrl",
                windowClass: "bootstrap_wrap",
                templateUrl:
                  "/scripts/directives/onsite-bonus/add-edit-modal.html",
                backdrop: "static",
                size: "md",
                resolve: {
                  data: function() {
                    return {
                      bonus_id: bonusRow.id,
                      project_id: $scope.projectId
                    };
                  }
                }
              });
              modal.result.then(
                function(result) {
                  $scope.loadOldBonus();
                },
                function() {
                  //
                }
              );
            };
            $scope.filteredDate = function(input_date) {
              var formatted_date = new Date(input_date);
              return formatted_date;
            };
          }
        ]
      };
    }
  ])
  .controller("onsiteBonusaddModalCtrl", [
    "$scope",
    "Restangular",
    "data",
    "$uibModalInstance",
    function($scope, Restangular, data, $uibModalInstance) {
      var projectId = data.project_id;

      $scope.form = {
        fetching: true,
        loading: false,
        submitting: false,
        error: false,
        errorMessage: null,
        data: {
          from_date: null,
          to_date: null,
          amount: null,
          type: "local",
          notes: null
        },
        init: function() {
          $scope.form.loading = true;
          Restangular.one("project-info", projectId)
            .get()
            .then(
              function(response) {
                $scope.form.loading = false;
                if (response) {
                  $scope.form.project = response;
                  $scope.form.resources = $scope.form.project[0].resources;
                }
              },
              function(error) {
                console.log(error);
              }
            );
        },
        submit: function(bonusForm) {
          $scope.form.submitting = true;
          if (!bonusForm.$valid) {
            $scope.form.error = true;
            return;
          }
          $scope.loading = true;
          var params = {
            project_id: projectId,
            user_id: $scope.selected_user.id,
            type: $scope.form.data.type,
            amount: $scope.form.data.amount,
            from_date: $scope.date.startDate.format("YYYY-MM-DD HH:mm:ss"),
            to_date: $scope.date.endDate.format("YYYY-MM-DD HH:mm:ss"),
            notes: $scope.form.data.notes
          };

          Restangular.all("onsite-bonus")
            .post(params)
            .then(
              function(response) {
                $uibModalInstance.close(response);
              },
              function(error) {
                console.log(error);
                $uibModalInstance.dismiss();
              }
            );
        },
        cancel: function() {
          $uibModalInstance.dismiss();
        }
      };
      $scope.form.init();
      $scope.select_user = function(data) {
        $scope.selected_user = data;
      };

      //Daterange picker
      $scope.date = {
        startDate: moment().subtract(1, "days"),
        endDate: moment()
      };

      $scope.opts = {
        locale: {
          applyClass: "btn-green",
          applyLabel: "Apply",
          fromLabel: "From",
          format: "YYYY-MM-DD",
          toLabel: "To",
          cancelLabel: "Cancel",
          customRangeLabel: "Custom range"
        },
        ranges: {
          "Last 7 Days": [moment().subtract(6, "days"), moment()],
          "Last 30 Days": [moment().subtract(29, "days"), moment()]
        }
      };

      //Watch for date changes
      $scope.$watch("date", function(newDate) {}, false);
    }
  ])
  .controller("onsiteBonusmodalEditCtrl", [
    "$scope",
    "Restangular",
    "data",
    "$uibModalInstance",
    "$q",
    function($scope, Restangular, data, $uibModalInstance, $q) {
      var projectId = data.project_id;
      var bonusId = data.bonus_id;

      $scope.form = {
        fetching: true,
        loading: false,
        submitting: false,
        error: false,
        errorMessage: null,
        data: {
          from_date: null,
          to_date: null,
          amount: null,
          type: "weekend",
          notes: null
        },
        submit: function(bonusForm) {
          $scope.form.submitting = true;
          if (!bonusForm.$valid) {
            $scope.form.error = true;
            return;
          }
          $scope.loading = true;
          var params = {
            bonus_id: bonusId,
            project_id: projectId,
            user_id: $scope.selected_user.id,
            type: $scope.form.data.type,
            amount: $scope.form.data.amount,
            from_date: $scope.date.startDate.format("YYYY-MM-DD HH:mm:ss"),
            to_date: $scope.date.endDate.format("YYYY-MM-DD HH:mm:ss"),
            notes: $scope.form.data.notes
          };

          Restangular.all("onsite-bonus")
            .customPUT(params)
            .then(
              function(response) {
                $scope.loading = false;
                $uibModalInstance.close(response);
              },
              function(error) {
                console.log(error);
                $uibModalInstance.dismiss();
              }
            );
        },
        cancel: function() {
          $uibModalInstance.dismiss();
        }
      };

      var peojectInfo = Restangular.one("project-info", projectId).get();
      var bonus = Restangular.one("onsite-bonus", bonusId).get();

      $q.all([peojectInfo, bonus]).then(function(result) {
        $scope.form.fetching = false;
        if (result[0]) {
          $scope.form.project = result[0];
          $scope.form.resources = $scope.form.project[0].resources;
        }
        if (result[1]) {
          $scope.form.data = result[1];
        }
        $scope.date = {
          startDate: moment($scope.form.data.from_date),
          endDate: moment($scope.form.data.to_date)
        };
        $scope.selected_user = $scope.form.data.user;
      });

      $scope.select_user = function(data) {
        $scope.selected_user = data;
      };

      //Daterange picker
      $scope.opts = {
        locale: {
          applyClass: "btn-green",
          applyLabel: "Apply",
          fromLabel: "From",
          format: "YYYY-MM-DD",
          toLabel: "To",
          cancelLabel: "Cancel",
          customRangeLabel: "Custom range"
        },
        ranges: {
          "Last 7 Days": [moment().subtract(6, "days"), moment()],
          "Last 30 Days": [moment().subtract(29, "days"), moment()]
        }
      };

      //Watch for date changes
      $scope.$watch("date", function(newDate) {}, false);
    }
  ]);
angular.module('myApp')
    .controller('addCronModalCtrl', [ "$scope", "Restangular", "data", "$uibModalInstance" , function ($scope, Restangular, data, $uibModalInstance) {
        $scope.editFlag = data.cron_details.id ? true : false;
        $scope.repeat_list = [
            { value : "daily", name : "Daily" },
            { value : "weekly", name : "Weekly" },
            { value : "monthly", name : "Monthly" }
        ];
        $scope.event_type =  data.cron_details.event_type ? data.cron_details.event_type : 'once';
        $scope.repeat_type = data.cron_details.repeat_type ? data.cron_details.repeat_type : 'daily';
        $scope.hours = data.cron_details.hours ? data.cron_details.hours : null;
        $scope.minutes = data.cron_details.minutes ? data.cron_details.minutes : null;
        var reset = function() {
            $scope.selected_dates = {
                '1' : false, '2' : false, '3' : false, '4' : false, '5' : false, '6' : false, '7' : false, '8' : false, '9' : false, '10' : false, 
                '11' : false, '12' : false, '13' : false, '14' : false, '15' : false, '16' : false, '17' : false, '18' : false, '19' : false, '20' : false, '21' : false, 
                '22' : false, '23' : false, '24' : false, '25' : false, '26' : false, '27' : false, '28' : false, '29' : false, '30' : false, '31' : false,
            };
            $scope.selected_days = {
                'sun' : false,
                'mon' : false,
                'tue' : false,
                'wed' : false,
                'thu' : false,
                'fri' : false,
                'sat' : false,
            };
            $scope.schedule_date = '';
            $scope.ends_on = 'never';
            $scope.number_of_occurrences = null;
            $scope.ends_on_date = '';
        };
        $scope.repeatTypeChange = function () {
            reset();
        };
        $scope.selected_days = {
            'sun' : false,
            'mon' : false,
            'tue' : false,
            'wed' : false,
            'thu' : false,
            'fri' : false,
            'sat' : false,
        };
        if ( data.cron_details.day_of_week && data.cron_details.day_of_week.length > 0 )
        {
            if(data.cron_details.day_of_week.indexOf("0") !== -1) {
                $scope.selected_days.sun = true;
            }
            if(data.cron_details.day_of_week.indexOf("1") !== -1) {
                $scope.selected_days.mon = true;
            }
            if(data.cron_details.day_of_week.indexOf("2") !== -1) {
                $scope.selected_days.tue = true;
            }
            if(data.cron_details.day_of_week.indexOf("3") !== -1) {
                $scope.selected_days.wed = true;
            }
            if(data.cron_details.day_of_week.indexOf("4") !== -1) {
                $scope.selected_days.thu = true;
            }
            if(data.cron_details.day_of_week.indexOf("5") !== -1) {
                $scope.selected_days.fri = true;
            }
            if(data.cron_details.day_of_week.indexOf("6") !== -1) {
                $scope.selected_days.sat = true;
            }   
        }
        $scope.selected_dates = {
            '1' : false, '2' : false, '3' : false, '4' : false, '5' : false, '6' : false, '7' : false, '8' : false, '9' : false, '10' : false, 
            '11' : false, '12' : false, '13' : false, '14' : false, '15' : false, '16' : false, '17' : false, '18' : false, '19' : false, '20' : false, '21' : false, 
            '22' : false, '23' : false, '24' : false, '25' : false, '26' : false, '27' : false, '28' : false, '29' : false, '30' : false, '31' : false,
        };
        if ( data.cron_details.day_of_month && data.cron_details.day_of_month.length > 0 )
        {
            for(var i = 1; i < 32; i++ )
            {
                if(data.cron_details.day_of_month.indexOf(i.toString()) !== -1) {
                    $scope.selected_dates[i] = true;
                }  
            }
        }
        $scope.ends_on = 'never';
        $scope.ends_on_date = data.cron_details.ends_on ? new Date(data.cron_details.ends_on ) : '' ;
        if ( $scope.ends_on_date != '' )
        {
            $scope.ends_on = 'on_date';
        }
        $scope.number_of_occurrences = data.cron_details.occurrence_counter ? data.cron_details.occurrence_counter : null;
        if ( $scope.number_of_occurrences !== null )
        {
            $scope.ends_on = 'occurrence';
        }
        $scope.endOptionUpdated = function() {
            $scope.endsOnError = false;
            if( $scope.ends_on == 'never' )
            {
                $scope.ends_on_date = '';
                $scope.number_of_occurrences = null;
            } else if ( $scope.ends_on == 'on_date' ) {
                $scope.number_of_occurrences = null;
            } else {
                $scope.ends_on_date = '';
            }
        };
        $scope.error = false;
        $scope.openDateFlag = false;
        $scope.schedule_date = '';
        if ( data.cron_details.month_of_year )
        {
            $scope.schedule_date = new Date();
            $scope.schedule_date.setMonth(data.cron_details.month_of_year - 1);
            $scope.schedule_date.setDate(data.cron_details.day_of_month[0]);
        }
        $scope.openEndDateFlag = false;
        $scope.mytime = new Date();
        $scope.mytime.setHours($scope.hours, $scope.minutes);
        $scope.hstep = 1;
        $scope.mstep = 15;
        $scope.tomorrow = new Date();
        $scope.tomorrow.setDate($scope.tomorrow.getDate() + 1);
        $scope.dateOptions = {
            minDate: new Date()
          };
        $scope.endsOnDateOptions = {
            minDate: $scope.tomorrow
          };  
        
        $scope.openDate = function () {
            $scope.openDateFlag = true;  
        };

        $scope.openEndDate = function () {
            $scope.openEndDateFlag = true;  
        };

        $scope.validateTime = function() {
            if ( !$scope.mytime )
            {
                $scope.timeError = true;
                $scope.error = true;
            }   
        };

        $scope.validateScheduleDate = function() {
            if ( !$scope.schedule_date || $scope.schedule_date == '' )
            {
                $scope.scheduleDateError = true;
                $scope.error = true;
            }
        };

        $scope.validateEndsOn = function() {
            if ( $scope.ends_on == 'on_date' && $scope.ends_on_date == '' )
            {
                $scope.endsOnError = true;
                $scope.error = true;
            }
            if ( $scope.ends_on == 'occurrence' && !$scope.number_of_occurrences )
            {
                $scope.endsOnError = true;
                $scope.error = true;
            }
        };

        $scope.validateSelectedDays = function() {
            if ( !$scope.selected_days.sun && !$scope.selected_days.mon && !$scope.selected_days.tue && !$scope.selected_days.wed && 
                 !$scope.selected_days.thu && !$scope.selected_days.fri && !$scope.selected_days.sat )
            {
                $scope.selectedDaysError = true;
                $scope.error = true;
            }
        };

        $scope.validateDateOfMonths = function() {
            $scope.selectedDatesError = true;
            $scope.error = true;
            for(var i = 1; i < 32; i++ )
            {
                if($scope.selected_dates[i]) {
                    $scope.selectedDatesError = false;
                    $scope.error = false;
                }  
            } 
        };
        $scope.validations = function () {
            if ( $scope.event_type == 'once' )
            {
                $scope.validateTime();
                $scope.validateScheduleDate();
            }
            else
            {
                if ( $scope.repeat_type == 'daily' )
                {
                    $scope.validateTime();
                    $scope.validateEndsOn();

                } else if ( $scope.repeat_type == 'weekly' ) {
                    $scope.validateTime();
                    $scope.validateSelectedDays();
                    $scope.validateEndsOn();
                } else {
                    $scope.validateTime();
                    $scope.validateDateOfMonths();
                    $scope.validateEndsOn();
                }

            }
        };
        $scope.save = function () {
            $scope.error = false;
            $scope.validations();
            if( $scope.error )
            {
                console.log('Error');
                return;
            }
            $scope.loading = true;
            var end_date = null;
            if ( $scope.ends_on_date )
            {
                end_date = moment($scope.ends_on_date).format("YYYY-MM-DD");
            }
            var sch_date = null;
            if ( $scope.schedule_date )
            {
                sch_date = moment($scope.schedule_date).format("YYYY-MM-DD");
            }
            var minutes =  moment($scope.mytime).format("mm");
            var hours =  moment($scope.mytime).format("HH");
            var params = {
                'reference_id' : data.reference_id,
                'reference_type' : data.reference_type,
                'job_handler' : data.job_handler,
                'event_type' : $scope.event_type,
                'repeat_type' : $scope.repeat_type,
                'hours' : hours,
                'minutes' : minutes,
                'selected_days' : $scope.selected_days,
                'selected_dates' : $scope.selected_dates,
                'ends_on' : $scope.ends_on,
                'ends_on_date' :  end_date,
                'schedule_date' : sch_date,
                'number_of_occurrences' : $scope.number_of_occurrences,
            }
            console.log(params);
            if ( $scope.editFlag )
            {
                Restangular.one("cron", data.cron_details.id)
                                .customPUT({
                                    params: params
                                })
                                .then(
                                    function (response) {
                                        $scope.loading = false;
                                        var return_data = response;
                                        $uibModalInstance.close(return_data);
                                    },
                                    function (errors) {
                                        $scope.loading = false;
                                        console.log(errors);
                                    }
                                );
            }
            else
            {
                Restangular.all("cron")
                            .post(params)
                            .then(function (response) {
                                $scope.loading = false;
                                $uibModalInstance.close(12);
                            }, function (error) {
                                $scope.loading = false;
                                console.log(error);
                            });
            }
        };     

        $scope.cancel = function () {
            $uibModalInstance.dismiss(34);
        };
    }]);
"use strict()";

/*Directives*/
angular.module("myApp").directive("companyContacts", [
  function() {
    return {
      templateUrl:
        "/scripts/directives/company-contacts/company-contacts.html?v=1",
      restrict: "E",
      replace: true,
      scope: {
        companyId: "=",
        isDisabled: "@isDisabled"
      },
      controller: function($scope, Restangular, $uibModal) {
        $scope.oldcontactObjs = [];

        $scope.showModal = function() {
          var modal = $uibModal.open({
            controller: "modalCtrl",
            windowClass: "bootstrap_wrap",
            templateUrl: "/scripts/directives/company-contacts/modal.html",
            backdrop: "static",
            size: "md",
            resolve: {
              data: function() {
                return { company_id: $scope.companyId };
              }
            }
          });
          modal.result.then(
            function(result) {
              $scope.oldcontactObjs.push(result);
            },
            function() {
              //
            }
          );
        };
        $scope.loadOldContacts = function() {
          $scope.oldcontactObjs = [];
          $scope.listLoading = true;
          Restangular.all("company-contact")
            .customGET("", {
              company_id: $scope.companyId
            })
            .then(
              function(response) {
                $scope.listLoading = false;
                for (i = 0; i < response.length; i++) {
                  $scope.oldcontactObjs.push(response[i]);
                }
              },
              function(error) {
                console.log(error);
              }
            );
        };
        $scope.loadOldContacts();

        $scope.removecontactObjRow = function(index, contactRow) {
          console.log("thus");
          console.log(index, contactRow);
          var r = confirm("Are you sure you want to delete this entry?");
          if (r != true) {
            return;
          }
          Restangular.one("company-contact", contactRow.id)
            .remove()
            .then(
              function(response) {
                $scope.oldcontactObjs.splice(index, 1);
              },
              function(error) {
                console.log(error);
              }
            );
        };
        $scope.editcontactObjRow = function(index, contactRow) {
          var modal = $uibModal.open({
            controller: "editModalCtrl",
            windowClass: "bootstrap_wrap",
            templateUrl: "/scripts/directives/company-contacts/edit-modal.html",
            backdrop: "static",
            size: "md",
            resolve: {
              data: function() {
                return { contact_id: contactRow.id };
              }
            }
          });
          modal.result.then(
            function(result) {
              $scope.loadOldContacts();
            },
            function() {
              //
            }
          );
        };
      }
    };
  }
]);

angular
  .module("myApp")
  .controller("modalCtrl", function(
    $scope,
    Restangular,
    data,
    $uibModalInstance
  ) {
    $scope.items = data;
    $scope.error = false;
    $scope.firstNameError = false;
    $scope.lastNameError = false;
    $scope.typeError = false;
    $scope.emailError = false;
    $scope.contactObj = {
      first_name: "",
      last_name: "",
      type: "",
      email: "",
      mobile: ""
    };

    $scope.cancel = function() {
      $uibModalInstance.dismiss();
    };

    $scope.ok = function() {
      $scope.error = false;
      if (!$scope.contactObj.first_name) {
        $scope.firstNameError = true;
        $scope.error = true;
      } else {
        $scope.firstNameError = false;
      }

      if (!$scope.contactObj.last_name) {
        $scope.lastNameError = true;
        $scope.error = true;
      } else {
        $scope.lastNameError = false;
      }

      if (!$scope.contactObj.type) {
        $scope.typeError = true;
        $scope.error = true;
      } else {
        $scope.typeError = false;
      }

      if (!$scope.contactObj.email) {
        $scope.emailError = true;
        $scope.error = true;
      } else {
        $scope.emailError = false;
      }

      if ($scope.error) {
        return;
      }

      var params = {
        company_id: $scope.items.company_id,
        first_name: $scope.contactObj.first_name,
        last_name: $scope.contactObj.last_name,
        type: $scope.contactObj.type,
        email: $scope.contactObj.email,
        mobile: $scope.contactObj.mobile
      };

      Restangular.all("company-contact")
        .post(params)
        .then(
          function(response) {
            $uibModalInstance.close(response);
          },
          function(error) {
            console.log(error);
            $uibModalInstance.dismiss();
          }
        );
    };
  });

angular
  .module("myApp")
  .controller("editModalCtrl", function(
    $scope,
    Restangular,
    data,
    $uibModalInstance
  ) {
    $scope.items = data;
    $scope.error = false;
    $scope.firstNameError = false;
    $scope.lastNameError = false;
    $scope.typeError = false;
    $scope.emailError = false;
    $scope.contactObj = {
      first_name: "",
      last_name: "",
      type: "",
      email: "",
      mobile: ""
    };
    $scope.contactData = [];

    $scope.loadContactData = function() {
      $scope.contactData;
      $scope.loading = true;
      Restangular.one("company-contact", $scope.items.contact_id)
        .get()
        .then(
          function(response) {
            $scope.loading = false;
            $scope.contactData = response;
          },
          function(error) {
            console.log(error);
          }
        );
    };
    $scope.loadContactData();

    $scope.cancel = function() {
      $uibModalInstance.dismiss();
    };

    $scope.ok = function() {
      $scope.error = false;
      if (!$scope.contactData.first_name) {
        $scope.firstNameError = true;
        $scope.error = true;
      } else {
        $scope.firstNameError = false;
      }

      if (!$scope.contactData.last_name) {
        $scope.lastNameError = true;
        $scope.error = true;
      } else {
        $scope.lastNameError = false;
      }

      if (!$scope.contactData.type) {
        $scope.typeError = true;
        $scope.error = true;
      } else {
        $scope.typeError = false;
      }

      if (!$scope.contactData.email) {
        $scope.emailError = true;
        $scope.error = true;
      } else {
        $scope.emailError = false;
      }

      if ($scope.error) {
        return;
      }

      var params = {
        contact_id: $scope.items.contact_id,
        first_name: $scope.contactData.first_name,
        last_name: $scope.contactData.last_name,
        type: $scope.contactData.type,
        email: $scope.contactData.email,
        mobile: $scope.contactData.mobile
      };

      Restangular.all("company-contact", $scope.items.contact_id)
        .customPUT(params)
        .then(
          function(response) {
            $uibModalInstance.close(response);
          },
          function(error) {
            console.log(error);
            $uibModalInstance.dismiss();
          }
        );
    };
  });

"use strict()";

/*Directives*/
angular.module("myApp").directive("assetMeta", [
  function() {
    return {
      templateUrl: "/scripts/directives/asset-meta/index.html",
      restrict: "E",
      replace: true,
      scope: {
        assetId: "="
      },
      controller: [
        "$scope",
        "Restangular",
        function($scope, Restangular) {
          var api = "asset-meta";
          
          $scope.form = {
            loading: false,
            error: false,
            message: null,
            submitted: false,
            data: [],
            keys : [],
            model: {
              asset_id: $scope.assetId,
              key: null,
              value: null,
              validValue : true
            },
            getMeta : function() {
              Restangular.all('asset-meta/key')
                .getList()
                .then(function(response){
                  $scope.form.keys = response;
                }, function(error){
                  console.log('error', error);

                });
            },
            init: function() {
              $scope.form.loading = true;
              $scope.form.getMeta();
              Restangular.all(api)
                .getList({
                  asset_id: $scope.assetId,
                  pagination: 0,
                  per_page: 100
                })
                .then(
                  function(response) {
                    $scope.form.loading = false;
                    // angular.forEach(response, function(item) {
                    //   item.loading = false;
                    //   item.editing = false;
                    //   item.update = function() {
                    //     item.loading = true;
                    //     item.put().then(
                    //       function(response) {
                    //         console.log("updated ", response);
                    //         item.loading = false;
                    //         item.editing = false;
                    //       },
                    //       function(error) {
                    //         item.loading = false;
                    //         console.log("ERROR updating asset-meta", error);
                    //       }
                    //     );
                    //   };
                    //   item.enableEditing = function() {
                    //     item.editing = true;
                    //     return false;
                    //   };
                    // });
                    $scope.form.data = response;
                  },
                  function(error) {
                    $scope.form.loading = false;
                    console.log(error);
                  }
                );
            },
            update: function(item) {
              item.enableEditing = !item.enableEditing;
              console.log('item update');
              item.loading = false;
              item.editing = true;
              item.update = function() {
                item.loading = true;
                item.error = false;
                item.message = null;
                item.put().then(
                  function(response) {
                    $scope.form.getMeta();
                    item.loading = false;
                    item.editing = false;
                  },
                  function(error) {
                    item.loading = false;
                    item.error = true;
                    item.loading = false;
                    item.message = error.data.message;
                    console.log("ERROR updating asset-meta", error);
                  }
                );
              };
              
            },
            remove: function(itemObj, $index) {
              var confirmMess = confirm("Are you sure want to delete this entry?");
              if(confirmMess) {
                itemObj.remove().then(function(response){
                  $scope.form.data.splice($index, 1);
                }, function(error){
                  console.log('error',error);
                });
              }
            },
            resetForm: function() {
              $scope.form.model.key = null;
              $scope.form.model.value = null;
              $scope.form.submitted = false;
            },
            checkValidMac: function(value) {
              var regex = /^(([A-Fa-f0-9]{2}[:]){5}[A-Fa-f0-9]{2}[,]?)+$/i;
              return regex.test(value);
            },
            submit: function($form) {
              $scope.form.error = false;
              $scope.form.submitted = true;
              if ($form.$invalid) return false;
              $scope.form.model.validValue = true;

              if(($scope.form.model.key == 'LAN-MAC' ||  $scope.form.model.key == 'WIFI-MAC') && 
                $scope.form.checkValidMac($scope.form.model.value) == false) {
                console.log('checkValidMac false');
                $scope.form.model.validValue = false;
                return false;
              }
              
              
              Restangular.all("asset-meta")
                .post($scope.form.model)
                .then(
                  function(response) {
                    $scope.form.data.unshift(response);
                    $scope.form.resetForm();
                    $scope.form.getMeta();
                  },
                  function(error) {

                    $scope.form.error = true;
                    $scope.form.message = error.data.message;
                    console.log("error",error.data.message);
                  }
                );
            }
          };
          $scope.form.init();

          // $scope.newRequiredProjectLinks = [];
          // $scope.oldProjectLinks = [];
          // $scope.editable = [];
          // $scope.error = false;
          // $scope.nameError = false;
          // $scope.pathError = false;
          // $scope.newProjectLinkObj = {
          //   name: "",
          //   path: ""
          // };
          // $scope.loadRequiredProjectLinks = function() {
          //   $scope.oldProjectLinks = [];
          //   $scope.listLoading = true;
          //   Restangular.all("project-link")
          //     .customGET("", {
          //       project_id: $scope.projectId
          //     })
          //     .then(
          //       function(response) {
          //         $scope.listLoading = false;
          //         for (i = 0; i < response.length; i++) {
          //           $scope.oldProjectLinks.push(response[i]);
          //         }
          //       },
          //       function(error) {
          //         console.log(error);
          //       }
          //     );
          // };
          // $scope.loadRequiredProjectLinks();

          // $scope.addProjectLinkRow = function() {
          //   $scope.error = false;
          //   if (!$scope.newProjectLinkObj.name) {
          //     $scope.nameError = true;
          //     $scope.error = true;
          //   } else {
          //     $scope.nameError = false;
          //   }

          //   if (!$scope.newProjectLinkObj.path) {
          //     $scope.pathError = true;
          //     $scope.error = true;
          //   } else {
          //     $scope.pathError = false;
          //   }

          //   if ($scope.error) {
          //     return;
          //   }

          //   var params = {
          //     project_id: $scope.projectId,
          //     name: $scope.newProjectLinkObj.name,
          //     path: $scope.newProjectLinkObj.path
          //   };
          //   console.log("params", params);
          //   Restangular.all("project-link")
          //     .post(params)
          //     .then(
          //       function(response) {
          //         $scope.newProjectLinkObj = {
          //           name: "",
          //           path: ""
          //         };
          //         $scope.loadRequiredProjectLinks();
          //       },
          //       function(error) {
          //         console.log(error);
          //       }
          //     );
          // };
          // $scope.editProjectLinkRow = function(index) {
          //   console.log(index);
          //   $scope.editable[index] = true;
          //   console.log($scope.editable);
          // };
          // $scope.saveProjectLinkRow = function(index, ProjectLinkRow) {
          //   $scope.editable[index] = true;
          //   Restangular.one("project-link", ProjectLinkRow.id)
          //     .customPUT({
          //       ProjectLinkRow: ProjectLinkRow
          //     })
          //     .then(
          //       function(response) {
          //         $scope.editable[index] = false;
          //       },
          //       function(errors) {
          //         console.log(errors);
          //       }
          //     );
          // };

          // $scope.removeProjectLinkRow = function(index, ProjectLinkRow) {
          //   var r = confirm("Are you sure you want to delete this entry?");
          //   if (r != true) {
          //     return;

          //   }
          //   Restangular.one("project-link", ProjectLinkRow.id)
          //     .remove()
          //     .then(
          //       function(response) {
          //         $scope.oldProjectLinks.splice(index, 1);
          //       },
          //       function(error) {
          //         console.log(error);
          //       }
          //     );
          // };
        }
      ]
    };
  }
]);

angular.module("myApp").controller("assignIpModalCtrl", [
  "$scope",
  "Restangular",
  "data",
  "$uibModalInstance",
  "$q",
  function($scope, Restangular, data, $uibModalInstance, $q) {
    $scope.userData = data.user;
    $scope.form = {
      loading: false,
      selected_range: "",
      showBtn: false,
      error: false,
      rangeError: false,
      rangeErrorMsg: "",
      newItem: false,
      submit: function(assginIpfrom) {
        if (!assginIpfrom.$valid) {
          $scope.form.error = true;
          return;
        }
        $scope.form.loading = true;
        var params = {
          user_id: $scope.userData.id,
          range: $scope.form.selected_range.id
        };
        Restangular.all("assign-ip")
          .post(params)
          .then(
            function(response) {
              $scope.form.loading = false;
              $scope.form.newItem = true;
              $scope.userIpList.unshift(response);
            },
            function(error) {
              $scope.form.loading = false;
              $scope.form.rangeError = true;
              $scope.form.rangeErrorMsg = error.data.message;
            }
          );
      },
      cancel: function() {
        $uibModalInstance.dismiss();
      },
      close: function() {
        $uibModalInstance.close();
      },
      releaseIp: function(index, ipRow) {
        var r = confirm("Are you sure you want to release this Ip?");
        if (r != true) {
          return;
        }
        Restangular.one("user-ip-address", ipRow.id)
          .remove()
          .then(
            function(response) {
              $scope.userIpList.splice(index, 1);
              if (index == 0) {
                $scope.form.newItem = false;
              }
            },
            function(error) {
              console.log(error);
            }
          );
      }
    };
    var param = {
      user_id: $scope.userData.id
    };
    var getIpRangeList = Restangular.all("ip-address-ranges").getList();
    var getUserIpList = Restangular.all("user-ip-address").getList(param);

    $q.all([getIpRangeList, getUserIpList, ($scope.form.loading = true)]).then(
      function(result) {
        $scope.form.loading = false;
        if (result[0]) {
          $scope.ipRangeList = result[0];
        }
        if (result[1]) {
          $scope.userIpList = result[1];
        }
      }
    );

    $scope.select_range = function(data) {
      $scope.form.showBtn = true;
      $scope.form.selected_range = data;
    };
  }
]);

angular.module("myApp").controller("assignIpCtrl", [
  "$scope",
  "Restangular",
  "$uibModal",
  function($scope, Restangular, $uibModal) {
    $scope.users = JSON.parse(window.users);
    angular.element(document).ready(function() {
      dTable = $("#user_ip_table");
      dTable.DataTable({ pageLength: 500 });
    });
    $scope.showModal = function(user) {
      var modal = $uibModal.open({
        controller: "assignIpModalCtrl",
        windowClass: "bootstrap_wrap",
        templateUrl: "/views/assign-ip/modal.html",
        backdrop: "static",
        size: "md",
        resolve: {
          data: function() {
            return { user: user };
          }
        }
      });
      modal.result.then(
        function(result) {
          window.location.reload();
        },
        function() {
          //cancel
        }
      );
    };
  }
]);

"use strict()";

/*Directives*/
angular.module("myApp").directive("assignAsset", [
  function() {
    return {
      templateUrl: "/scripts/directives/assign-asset/index.html",
      restrict: "E",
      replace: true,
      scope: {
        assetId: "="
      },
      controller: [
        "$scope",
        "Restangular",
        "$q",
        "$uibModal",
        function($scope, Restangular, $q, $uibModal) {
          var api = "asset-detail";
          $scope.form = {
            loading: false,
            error: false,
            message: null,
            submitted: false,
            assignError: false,
            assignErrorMsg: "",
            isAssigned: false,
            ipReq: false,
            data: [],
            selected_ip: {
              wifi: null,
              lan: null
            },
            wifi: null,
            lan: null,
            startDate: {
              format: "dd-MMM-yyyy",
              datepickerOptions: { maxDate: new Date(), startingDay: 0 },
              isOpen: false
            },
            model: {
              asset_id: $scope.assetId,
              user_id: null,
              from_date: null,
              to_date: null
            },
            init: function() {
              var data = Restangular.all(api).getList({
                asset_id: $scope.assetId,
                pagination: 0,
                per_page: 100
              });
              var userList = Restangular.all("user-all-active").getList();
              var asset = Restangular.one("asset", $scope.assetId).get();
              $q.all([
                data,
                userList,
                asset,
                ($scope.form.loading = true)
              ]).then(function(result) {
                $scope.form.loading = false;
                if (result[0]) {
                  $scope.form.data = result[0];
                  $scope.form.isAssigned = false;
                  angular.forEach($scope.form.data, function(item) {
                    if (item.to_date === null) {
                      $scope.form.isAssigned = true;
                    }
                  });
                }
                if (result[1]) {
                  $scope.allUsers = result[1];
                }
                if (result[2]) {
                  angular.forEach(result[2].metas, function(meta) {
                    if (meta.key === "WIFI-MAC") {
                      $scope.form.wifi = meta;
                      $scope.form.ipReq = true;
                    }
                    if (meta.key === "LAN-MAC") {
                      $scope.form.lan = meta;
                      $scope.form.ipReq = true;
                    }
                  });
                }
              });
            },
            enableEditing: function(item) {
              item.editing = !item.editing;
              item.isOpen = false;
              item.datepickerOptions = { maxDate: new Date() };
              item.update = function() {
                if (
                  typeof item.to_date == "undefined" ||
                  item.to_date == "0000-00-00" ||
                  item.to_date == null
                ) {
                  item.error = true;
                  return false;
                }
                item.put().then(
                  function(response) {
                    item.loading = false;
                    item.editing = false;
                    item.error = false;
                    item.assignee = response.assignee;
                    item.released_by = response.released_by;
                    item.user = response.user;
                    $scope.form.isAssigned = false;
                    item.assigning = false;
                    $scope.form.wifi.ip_mapper = null;
                    $scope.form.lan.ip_mapper = null;
                    $scope.form.resetForm();
                  },
                  function(error) {
                    item.loading = false;
                  }
                );
              };
            },
            releaseIP: function(meta) {
              console.log('releaseIp called', meta);
              // if (type === "wifi") {
              //   $scope.form.wifi.loading = true;
              // }
              // if (type === "lan") {
              //   $scope.form.lan.loading = true;
              // }
              Restangular.one("ip-address-mapper", meta.ip_mapper.id)
                .remove()
                .then(
                  function(response) {
                    console.log('releaseIP ', response);
                    meta.ip_mapper = null;
                    // item.loading = false;
                    // item.assigning = false;
                    // if (type === "wifi") {
                    //   $scope.form.wifi.ip_mapper = null;
                    //   $scope.form.wifi.loading = false;
                    // }
                    // if (type === "lan") {
                    //   $scope.form.lan.ip_mapper = null;
                    //   $scope.form.lan.loading = false;
                    // }
                  },
                  function(error) {
                    console.log(error);
                  }
                );
            },
            enableAssigning: function(item) {
              item.assigning = !item.assigning;
              //var params = {user_id : item.user_id};
              Restangular.all("user-ip-address/"+item.user_id+"/free-ips")
                .getList()
                .then(
                  function(response) {
                    console.log('free-ips', response);
                    $scope.loading = false;
                    $scope.form.userFreeIpList = response;
                    if ($scope.form.userFreeIpList.length == 0) {
                      $scope.form.assignError = true;
                      $scope.form.assignErrorMsg =
                        "User does not have a free ip address to assign. Allocate ip address to user and try again!";
                    }
                  },
                  function(error) {
                    console.log(error);
                  }
                );
            },
            resetForm: function() {
              $scope.form.selected_user = null;
              $scope.form.model.user_id = null;
              $scope.form.model.from_date = null;
              $scope.form.model.to_date = null;
              $scope.form.submitted = false;
              $scope.form.assignError = null;
              $scope.form.assignErrorMsg = "";
            },
            submit: function($form) {
              $scope.form.submitted = true;
              if (!$form.$valid) {
                $scope.form.error = true;
                $scope.form.submitted = false;
                return false;
              }
              Restangular.all("asset-detail")
                .post($scope.form.model)
                .then(
                  function(response) {
                    $scope.form.data.unshift(response);
                    $scope.form.submitted = false;
                    $scope.form.isAssigned = true;
                    $scope.form.resetForm();
                  },
                  function(error) {
                    $scope.form.submitted = false;
                    $scope.form.assignError = true;
                    $scope.form.assignErrorMsg = error.data.message;
                  }
                );
            },
            assignIp: function(meta) {
              if (type === "wifi") {
                $scope.form.wifi.loading = true;
                if (!$scope.form.selected_ip.wifi) {
                  $scope.form.wifi.loading = false;
                  item.error = true;
                  return;
                }
                var params = {
                  reference_type: "asset",
                  reference_id: $scope.form.wifi.id,
                  user_ip_address_id: $scope.form.selected_ip.wifi.id
                };
              }
              if (type === "lan") {
                $scope.form.lan.loading = true;
                if (!$scope.form.selected_ip.lan) {
                  $scope.form.lan.loading = false;
                  item.error = true;
                  return;
                }
                var params = {
                  reference_type: "asset",
                  reference_id: $scope.form.lan.id,
                  user_ip_address_id: $scope.form.selected_ip.lan.id
                };
              }
              Restangular.all("ip-address-mapper")
                .post(params)
                .then(
                  function(response) {
                    item.error = false;
                    if ($scope.form.selected_ip.wifi) {
                      $scope.form.wifi.loading = false;
                      $scope.form.selected_ip.wifi = null;
                      $scope.form.wifi.ip_mapper = response;
                    }
                    if ($scope.form.selected_ip.lan) {
                      $scope.form.lan.loading = false;
                      $scope.form.selected_ip.lan = null;
                      $scope.form.lan.ip_mapper = response;
                    }
                  },
                  function(error) {
                    console.log(error);
                  }
                );
            },
            select_user: function(data) {
              $scope.form.selected_user = data;
              $scope.form.model.user_id = $scope.form.selected_user.id;
            },
            select_ip: function(data, meta) {
              console.log('ip-address-mapper', $scope.form.userFreeIpList);
              meta.loading = true;
              var params = {
                  reference_type: "App\\Models\\Admin\\AssetMeta",
                  reference_id: meta.id,
                  user_ip_address_id: data.id
                };

              Restangular.all("ip-address-mapper")
                .post(params)
                .then(
                  function(response) {
                    console.log('ip-address-mapper', response);
                    meta.ip_mapper = response;
                    meta.enableAssigning = false;
                    $scope.form.userFreeIpList.splice(data, 1);
                    console.log('ip-address-mapper', $scope.form.userFreeIpList);
                    
                  },
                  function(error) {
                    console.log(error);
                  }
                );
              
              //$scope.form.selected_ip.wifi = data;
            },
            select_lan_ip: function(data, metaId) {
              $scope.form.assignIp(meta);
              $scope.form.userFreeIpList.splice(data, 1);
              //$scope.form.selected_ip.lan = data;
            },
            cancel: function(item) {
              item.assigning = false;
              $scope.form.selected_ip.wifi = null;
              $scope.form.selected_ip.lan = null;
            }
          };
          $scope.form.init();
        }
      ]
    };
  }
]);

angular.module('myApp')
    .controller('vendorIndexCtrl',["$scope", "Restangular", "$uibModal" , function ($scope, Restangular, $uibModal) {
        $scope.gridViewModel = "vendor?per_page=500";
        $scope.gridViewColumns = [
          {
            title: "Id",
            value: "%%row.id%%",
            width: 5
        },
        {
          title: "Name",
          value: "%% row.name %%",
          width: 8
        },
        {
            title: "Type",
            value: "%%row.type%%",
            value: "<span ng-class=\"{'label label-primary   custom-label' : row.type=='coporate', 'label label-danger' : row.type=='individual', 'label label-danger custom-label' : (row.status=='cancelled' || row.status=='rejected')  }\"> %% row.type | uppercase %%</span> ",
            width: 8

        },
        {
            title: "Address",
            value: "<span style='white-space: pre-wrap;'>%% row.address %%</span>",
            width: 20
        },
        {
            title: "Pan",
            value: "%% row.pan %%",
            width: 7
        },
        {
            title: "GST",
            value: "%% row.gst %%",
            width: 8
        },
        {
          title: "Services",
          value: "%% row.services_name %%",
          width: 14
      },
        {
          title: "Created At",
          value: "%% row.created_at | dateToISO %%",
          width: 15
        }
        ];
        $scope.gridViewActions = [
            {
              type: "view",
              action: "veiwDetail(row.id)"
            },
            {
              type: "edit",
              action: "editDetail(row.id)"
            },
            {
              type: "delete",
              action: "deleteDetails(row.id)"
            }
          ];
        $scope.addVendor = function() {
            var modal = $uibModal.open({
              controller: "addVendorModalCtrl",
              windowClass: "bootstrap_wrap",
              templateUrl: "/views/vendor/add-modal.html",
              backdrop: "static",
              size: "lg",
              resolve: {
                data: function() {
                  return { id: 1 };
                }
              }
            });
            modal.result.then(
              function(result) {
                window.location.reload();
              },
              function() {
                //cancel
              }
            );
          };
          
          $scope.veiwDetail = function(id) {
            var modal = $uibModal.open({
              controller: "showVendorModalCtrl",
              windowClass: "bootstrap_wrap",
              templateUrl: "/views/vendor/show-modal.html",
              backdrop: "static",
              size: "lg",
              resolve: {
                data: function() {
                  return { id: id };
                }
              }
            });
            modal.result.then(
              function(result) {
              },
              function() {
                //cancel
              }
            );
          };

          $scope.editDetail = function(id) {
            var modal = $uibModal.open({
              controller: "editVendorModalCtrl",
              windowClass: "bootstrap_wrap",
              templateUrl: "/views/vendor/add-modal.html",
              backdrop: "static",
              size: "lg",
              resolve: {
                data: function() {
                  return { id: id };
                }
              }
            });
            modal.result.then(
              function(result) {
                window.location.reload();
              },
              function() {
                //cancel
              }
            );
          };
          $scope.deleteDetails = function(id) {
            if (confirm("Are you sure you want to delete this vendor?")) {
              Restangular.one("vendor", id)
                .remove()
                .then(
                  function(response) {
                    window.location.reload();
                  },
                  function(error) {
                    alert(error.data.message);
                  }
                );
            }
          }

    }]);
angular.module("myApp").controller("addVendorModalCtrl", [
    "$scope",
    "Restangular",
    "data",
    "$uibModalInstance",
    function($scope, Restangular, data, $uibModalInstance) {
        $scope.existingServices = [];
        var getServices = function(){
            Restangular.all("vendor-services").getList({})
            .then(
                function (response) {
                    for( i=0; i < response.length ; i++ )
                    {
                        $scope.existingServices.push(response[i].name);
                    }
                },
                function (errors) {
                    console.log(errors);
                }
            );
        };
        getServices();
        $scope.form = {
            loading: false,
            model: {
                type: 'corporate',
                name: null,
                address: null,
                pan: null,
                gst: null,
                landline_number: null,
                vendor_contacts : [ 
                    {name : null, mobile : null, email : null,designation : null, is_primary_contact: false}
                ],
                vendor_services : [],
                new_vendor_contact: {
                    name : null,
                    mobile : null,
                    email : null,
                    designation : null,
                    is_primary_contact: false
                }
            },
            errors : {
                error: false,
                type: false,
                name: false,
                vendor_contacts: false,
                vendor_services: false,
                error_message: null
            },
            addContact: function () {
                $scope.form.model.vendor_contacts.push($scope.form.model.new_vendor_contact);
                $scope.form.model.new_vendor_contact = {
                    name : null,
                    mobile : null,
                    email : null,
                    designation : null,
                    is_primary_contact: false
                }
            },
            remove: function(index) {
                $scope.form.model.vendor_contacts.splice(index, 1);
            },
            resetOtherPrimary: function(index) {
                for ( i = 0; i < $scope.form.model.vendor_contacts.length; i++ )
                {
                    if ( i != index )
                    {
                        $scope.form.model.vendor_contacts[i].is_primary_contact = false;
                    }
                }
            },
            submit: function () {
                $scope.form.errors.error = false;
                if ( $scope.form.model.type == null )
                {
                    $scope.form.errors.type = true;
                    $scope.form.errors.error = true;
                }
                if ( $scope.form.model.name == null )
                {
                    $scope.form.errors.name = true;
                    $scope.form.errors.error = true;
                }
                if ( $scope.form.model.vendor_contacts[0].name == null || $scope.form.model.vendor_contacts[0].mobile == null )
                {
                    $scope.form.errors.vendor_contacts = true;
                    $scope.form.errors.error = true;
                }
                if ( $scope.form.model.vendor_services.length < 1 )
                {
                    $scope.form.errors.vendor_services = true;
                    $scope.form.errors.error = true;
                }
                if ( $scope.form.errors.error  )
                {
                    return;
                }
                $scope.form.loading = true;
                var params = {
                    data : $scope.form.model
                };
                Restangular.all('vendor').post(params).then(
                    function (response) {
                        $scope.form.loading = false;
                        $scope.close();
                    },
                    function (errors) {
                        $scope.form.loading = false;
                        $scope.form.errors.error_message =  errors.data.message;
                        console.log(errors);
                    }
                );
            },
        }
        // Get existing Services name
        $scope.close = function() {
            $uibModalInstance.close();
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss();
        };
    }
  ]);
  
angular.module("myApp").controller("showVendorModalCtrl", [
    "$scope",
    "Restangular",
    "data",
    "$uibModalInstance",
    function($scope, Restangular, data, $uibModalInstance) {
        var id = data.id;
        var vendor_services = [];
        $scope.form = {
          loading: false,
          data : {}
        }
        $scope.form.loading = true;
        
        var vendorObj = Restangular.one("vendor", id);
        vendorObj.get().then(
            function(response) {
              $scope.vendorDetails = response;
              $scope.form.loading = false;
            },
            function(error) {
              console.log("err", error);
            }
          );
        $scope.filteredDate = function (input_date) {
          var formatted_date = new Date(input_date);
          return formatted_date;
        };  
        // Get existing Services name
        $scope.close = function() {
            $uibModalInstance.close();
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss();
        };
    }
  ]);
  
angular.module("myApp").controller("editVendorModalCtrl", [
    "$scope",
    "Restangular",
    "data",
    "$uibModalInstance",
    function($scope, Restangular, data, $uibModalInstance) {
        $scope.existingServices = [];
        var id = data.id;
        var vendorObj = Restangular.one("vendor", id);
        var getServices = function(){
            Restangular.all("vendor-services").getList({})
            .then(
                function (response) {
                    for( i=0; i < response.length ; i++ )
                    {
                        $scope.existingServices.push(response[i].name);
                    }
                },
                function (errors) {
                    console.log(errors);
                }
            );
        };
        getServices();
        $scope.form = {
            loading: false,
            model: {
                id: null,
                type: null,
                name: null,
                address: null,
                pan: null,
                gst: null,
                landline_number: null,
                rating: null,
                created_at: null,
                vendor_contacts : [],
                vendor_services : [],
                new_vendor_contact: {
                    name : null,
                    mobile : null,
                    email : null,
                    designation : null,
                    is_primary_contact: false
                }
            },
            errors : {
                type: false,
                name: false,
                error_message: null
            },
            addContact: function () {
                $scope.form.model.vendor_contacts.push($scope.form.model.new_vendor_contact);
                $scope.form.model.new_vendor_contact = {
                    name : null,
                    mobile : null,
                    email : null,
                    designation : null,
                    is_primary_contact: 'false'
                }
            },
            remove: function(index) {
                $scope.form.model.vendor_contacts.splice(index, 1);
            },
            resetOtherPrimary: function(index) {
                for ( i = 0; i < $scope.form.model.vendor_contacts.length; i++ )
                {
                    if ( i != index )
                    {
                        $scope.form.model.vendor_contacts[i].is_primary_contact = false;
                    }
                }
            },
            saveRating: function (param) {
                $scope.form.model.rating = param;
            },
            submit: function () {
                if ( $scope.form.model.type == null )
                {
                    $scope.form.errors.type = true;
                }
                if ( $scope.form.model.name == null )
                {
                    $scope.form.errors.name = true;
                }
                $scope.form.loading = true;
                vendorObj.customPUT({
                    data: $scope.form.model
                }).then(
                    function(response) {
                        $scope.form.loading = false;
                        $scope.close();
                    },
                    function(error) {
                        $scope.form.loading = false;
                        $scope.form.errors.error_message =  error.data.message;
                        console.log("err", error);
                    }
                  );
            },
        }
        
        vendorObj.get().then(
            function(response) {
              for( i = 0; i < response.vendor_services.length ; i++ )
              {
                $scope.form.model.vendor_services.push(response.vendor_services[i].vendor_services_name.name);
              }
              $scope.form.model.id = response.id;
              $scope.form.model.type = response.type;
              $scope.form.model.name = response.name;
              $scope.form.model.address = response.address;
              $scope.form.model.pan = response.pan;
              $scope.form.model.gst = response.gst;
              $scope.form.model.landline_number = response.landline_number;
              $scope.form.model.rating = response.rating;
              $scope.form.model.vendor_contacts = response.vendor_contacts;
              $scope.form.model.created_at = response.created_at;
              for( i = 0; i < response.vendor_contacts.length ; i++ )
              {
                 $scope.form.model.vendor_contacts[i].is_primary_contact == 1 
                 ?  $scope.form.model.vendor_contacts[i].is_primary_contact = true
                 : $scope.form.model.vendor_contacts[i].is_primary_contact = false;
              }
            },
            function(error) {
                $scope.form.errors.error_message =  error.data.message;
              console.log("err", error);
            }
          );

        $scope.filteredDate = function (input_date) {
            var formatted_date = new Date(input_date);
            return formatted_date;
        };
        
        // Get existing Services name
        $scope.close = function() {
            $uibModalInstance.close();
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss();
        };
    }
  ]);
  
angular.module("myApp").controller("purchaseOrderCtrl", [
  "$scope",
  "Restangular",
  "$uibModal",
  function($scope, Restangular, $uibModal) {
    $scope.gridViewModel = "purchase-orders";
    $scope.gridViewColumns = [
      { title: "#", value: "%% row.id %%", width: 2 },
      { title: "Vendor Name", value: "%% row.vendor.name %%", width: 5 },
      {
        title: "Reference Number",
        value: "%% row.reference_number %%",
        width: 7
      },
      { title: "Payment Terms", value: "%% row.payment_terms %%", width: 6 },
      {
        title: "Delivery Date",
        value: "%% row.delivery_date | date%%",
        width: 5
      },
      { title: "Created By", value: "%% row.created_by.name %%", width: 5 },
      {
        title: "Status",
        value:
          "<span ng-class=\"{'label label-primary   custom-label' : row.status=='raised', 'label label-success custom-label' : row.status=='received', 'label label-danger custom-label' : (row.status=='cancelled' || row.status=='rejected')  }\"> %% row.status | uppercase %%</span>",
        width: 5,
        condition: ""
      },
      {
        title: "Created At",
        value: "%%row.created_at | dateToISO %%",
        width: 9
      }
    ];
    $scope.gridViewActions = [
      { type: "view", action: "veiwDetail(row.id)" },
      { type: "edit", action: "editDetail(row.id)" },
      {
        type: "delete",
        action: "deleteDetails(row.id)",
        condition: "row.status != 'received'"
      }
    ];
    $scope.veiwDetail = function(id) {
      var modal = $uibModal.open({
        controller: "purchaseOrderViewModalCtrl",
        windowClass: "bootstrap_wrap",
        templateUrl: "/views/purchase-order/view.html",
        backdrop: "static",
        size: "lg",
        resolve: {
          data: function() {
            return { id: id };
          }
        }
      });
      modal.result.then(
        function(result) {
          //
        },
        function() {
          //cancel
        }
      );
    };
    $scope.editDetail = function(id) {
      var modal = $uibModal.open({
        controller: "purchaseOrderEditModalCtrl",
        windowClass: "bootstrap_wrap",
        templateUrl: "/views/purchase-order/create.html",
        backdrop: "static",
        size: "lg",
        resolve: {
          data: function() {
            return { id: id };
          }
        }
      });
      modal.result.then(
        function(result) {
          window.location.reload();
        },
        function() {
          //cancel
        }
      );
    };
    $scope.deleteDetails = function(id) {
      if (confirm("Are you sure you want to delete this record?")) {
        Restangular.one("purchase-orders", id)
          .remove()
          .then(
            function(response) {
              location.reload();
            },
            function(error) {
              alert(error.data.message);
            }
          );
      }
    };
    $scope.createPurchaseOrder = function() {
      var modal = $uibModal.open({
        controller: "createPurchaseOrderModalCtrl",
        windowClass: "bootstrap_wrap",
        templateUrl: "/views/purchase-order/create.html",
        backdrop: "static",
        size: "lg",
        resolve: {
          data: function() {
            return {};
          }
        }
      });
      modal.result.then(
        function(result) {
          window.location.reload();
        },
        function() {
          //cancel
        }
      );
    };
  }
]);

angular.module("myApp").controller("createPurchaseOrderModalCtrl", [
  "$scope",
  "Restangular",
  "data",
  "$uibModalInstance",
  "$q",
  function($scope, Restangular, data, $uibModalInstance) {
    $scope.options = {
      formatYear: 'yy',
      maxDate: new Date(2030, 5, 22),
      minDate: new Date(),
    };
    $scope.form = {
      submitted: false,
      loading: false,
      itemError: false,
      error: false,
      selected_vendor : null,
      deliveryDate : {
        datepickerOptions : {
          options: {
              applyClass: "btn-success",
              singleDatePicker: true,
              locale: {
                applyLabel: "Apply",
                fromLabel: "From",
                // format: "YYYY-MM-DD", //will give you 2017-01-06
                format: "D-MMM-YY", //will give you 6-Jan-17
                // format: "D-MMMM-YY", //will give you 6-January-17
                toLabel: "To",
                cancelLabel: "Cancel",
                customRangeLabel: "Custom range"
              }

            }
        }
      },
      model: {
        vendor_id: null,
        reference_number: null,
        payment_terms: null,
        delivery_date: null,
        gst_included: "yes",
        status: null,
        total: null,
        purchase_items: [
          { name: null, unit_price: null, quantity: null, total: null }
        ]
      },
      item: { name: null, unit_price: null, quantity: null, total: null },
      init: function() {
        $scope.form.loading = true;
        Restangular.all("vendor")
          .getList({'pagination': 0})
          .then(
            function(response) {
              $scope.form.loading = false;
              $scope.form.allVendors = response;
            },
            function(error) {
              console.log(error);
            }
          );
      },
      submit: function($form) {
        $scope.form.submitted = true;
        $scope.form.itemError = false;
        if (
          $scope.form.model.purchase_items[0].name == null ||
          $scope.form.model.purchase_items[0].unit_price == null ||
          $scope.form.model.purchase_items[0].quantity == null
        ) {
          $scope.form.itemError = true;
        }
        if (!$form.$valid || $scope.form.itemError) {
          $scope.form.error = true;
          $scope.form.submitted = false;
          return false;
        }
        Restangular.all("purchase-orders")
          .post($scope.form.model)
          .then(
            function(response) {
              $uibModalInstance.close(response);
            },
            function(error) {
              console.log(error);
              $uibModalInstance.dismiss();
            }
          );
      },
      cancel: function() {
        $uibModalInstance.dismiss();
      },
      select_vendor: function(data) {
        $scope.form.selected_vendor = data;
        $scope.form.model.vendor_id = $scope.form.selected_vendor.id;
      },
      addNewItem: function() {
        $scope.form.model.purchase_items.push($scope.form.item);
        $scope.form.itemError = false;
        $scope.form.item = {
          name: null,
          unit_price: null,
          quantity: null,
          total: null
        };
      },
      removeItem: function(index) {
        $scope.form.model.purchase_items.splice(index, 1);
      }
    };
    $scope.form.init();
  }
]);

angular.module("myApp").controller("purchaseOrderEditModalCtrl", [
  "$scope",
  "Restangular",
  "data",
  "$uibModalInstance",
  "$q",
  function($scope, Restangular, data, $uibModalInstance, $q) {
    var id = data.id;
    $scope.options = {
      formatYear: 'yy',
      maxDate: new Date(2030, 5, 22),
      minDate: new Date(2018, 4, 1),
    };
    $scope.form = {
      submitted: false,
      loading: false,
      itemError: false,
      error: false,
      model: {},
      deliveryDate : {
        datepickerOptions : {
          options: {
              applyClass: "btn-success",
              singleDatePicker: true,
              locale: {
                applyLabel: "Apply",
                fromLabel: "From",
                // format: "YYYY-MM-DD", //will give you 2017-01-06
                format: "D-MMM-YY", //will give you 6-Jan-17
                // format: "D-MMMM-YY", //will give you 6-January-17
                toLabel: "To",
                cancelLabel: "Cancel",
                customRangeLabel: "Custom range"
              }
            }
        }
      },
      item: { name: null, unit_price: null, quantity: null, total: null },
      init: function() {
        var oldData = Restangular.one("purchase-orders", id).get();
        var vendorList = Restangular.all("vendor").getList({'pagination': 0});
        $q.all([vendorList, oldData, ($scope.form.loading = true)]).then(
          function(result) {
            $scope.form.loading = false;
            if (result[0]) {
              $scope.form.allVendors = result[0];
            }
            if (result[1]) {
              $scope.form.model = result[1];
              $scope.form.model.delivery_date = new Date(result[1].delivery_date);
              $scope.form.selected_vendor = $scope.form.model.vendor;
              if ($scope.form.model.gst_included == 1) {
                $scope.form.model.gst_included = "yes";
              } else $scope.form.model.gst_included = "no";
            }
          }
        );
      },
      submit: function($form) {
        $scope.form.submitted = true;

        if ($scope.form.model.purchase_items.length == 0) {
          $scope.form.itemError = true;
        }
        if (!$form.$valid || $scope.form.itemError) {
          $scope.form.error = true;
          $scope.form.submitted = false;
          return false;
        }
        Restangular.all("purchase-orders")
          .customPUT($scope.form.model, id)
          .then(
            function(response) {
              $scope.form.submitted = false;
              $scope.form.loading = false;
              $uibModalInstance.close(response);
            },
            function(error) {
              console.log(error);
              $uibModalInstance.dismiss();
            }
          );
      },
      cancel: function() {
        $uibModalInstance.dismiss();
      },
      select_vendor: function(data) {
        $scope.form.selected_vendor = data;
        $scope.form.model.vendor_id = $scope.form.selected_vendor.id;
      },
      addNewItem: function() {
        $scope.form.model.purchase_items.push($scope.form.item);
        $scope.form.itemError = false;
        $scope.form.item = {
          name: null,
          unit_price: null,
          quantity: null,
          total: null
        };
      },
      removeItem: function(index) {
        $scope.form.model.purchase_items.splice(index, 1);
      }
    };
    $scope.form.init();
  }
]);

angular.module("myApp").controller("purchaseOrderViewModalCtrl", [
  "$scope",
  "Restangular",
  "data",
  "$uibModalInstance",
  "FileUploader",
  function($scope, Restangular, data, $uibModalInstance,FileUploader) {
    var params = {
      url: '/api/v1/file',
      autoUpload:true
    };
     
    $scope.uploader = new FileUploader(params);
    $scope.uploader.onBeforeUploadItem = function(item) {
      //item.formData.push({some: 'data'});
      console.log('onBeforeUploadItem', item);
    }
    

    // $scope.uploader.onAfterAddingFile = function(fileItem) {
    //     console.info('onAfterAddingFile', fileItem);
    //     console.log('formData',fileItem.formData);
    //     fileItem.formData = {
    //       test :" sdsd"
    //     };
    //     var reader = new FileReader();
    //     reader.onloadend = function(event) {
    //       console.log(fileItem.file.name + ': ' + event.target.result.split(',')[1]);
    //     };
    //     reader.readAsDataURL(fileItem._file);
    // };
    
    $scope.uploader.onSuccessItem = function(item, response, status, headers) {
      console.log('response', response);
      $scope.form.uploaded = true;
    }
    var id = data.id;
    $scope.form = {
      id: data.id,
      loading: false,
      uploading: false,
      uploaded: false,
      data: {},
      init: function() {
        $scope.form.loading = false;
        Restangular.one("purchase-orders", id)
          .get()
          .then(
            function(response) {
              $scope.form.loading = false;
              $scope.form.data = response;
            },
            function(error) {
              console.log(error);
              $uibModalInstance.dismiss();
            }
          );
      },
      cancel: function() {
        $uibModalInstance.dismiss();
      },
    };
    $scope.download = function() {
      var path = "/api/v1/purchase-order-download/"+id;
      window.location.href =  path;
    }
    $scope.form.init();
  }
]);

"use strict()";

/*Directives*/
angular
  .module("myApp")
  .directive("myNetworkDevices", [
    function() {
      return {
        templateUrl: "/scripts/directives/my-network-devices/index.html",
        restrict: "E",
        replace: true,
        scope: {
          userId: "=",
          isDisabled: "@isDisabled"
        },
        controller: [
          "$scope",
          "Restangular",
          "$uibModal",
          function($scope, Restangular, $uibModal) {
            var api = "my-network-devices";

            $scope.form = {
              loading: false,
              error: false,
              data: [],
              init: function() {
                $scope.form.loading = true;
                Restangular.all(api)
                  .getList({
                    user_id: $scope.userId,
                    pagination: 0,
                    per_page: 100
                  })
                  .then(
                    function(result) {
                      $scope.form.loading = false;
                      if (result) {
                        $scope.form.data = result;
                      }
                    },
                    function(error) {
                      console.log(error);
                    }
                  );
              },
              showModal: function() {
                var modal = $uibModal.open({
                  controller: "networkDevicesAddModalCtrl",
                  windowClass: "bootstrap_wrap",
                  templateUrl:
                    "/scripts/directives/my-network-devices/add-edit-modal.html",
                  backdrop: "static",
                  size: "md",
                  resolve: {
                    data: function() {
                      return { user_id: $scope.userId };
                    }
                  }
                });
                modal.result.then(
                  function(result) {
                    $scope.form.data.push(result);
                  },
                  function() {
                    //
                  }
                );
              },
              remove: function(item) {
                var r = confirm("Are you sure you want to remove this device?");
                if (r != true) {
                  return;
                }
                Restangular.one(api, item.id)
                  .remove()
                  .then(
                    function(response) {
                      $scope.form.data.splice(item.$index, 1);
                    },
                    function(error) {
                      console.log(error);
                    }
                  );
              },
              showEditModal: function(item) {
                var modal = $uibModal.open({
                  controller: "networkDevicesEditModalCtrl",
                  windowClass: "bootstrap_wrap",
                  templateUrl:
                    "/scripts/directives/my-network-devices/add-edit-modal.html",
                  backdrop: "static",
                  size: "md",
                  resolve: {
                    data: function() {
                      return { device_id: item.id, user_id: $scope.userId };
                    }
                  }
                });
                modal.result.then(
                  function(result) {
                    $scope.form.init();
                  },
                  function() {
                    //
                  }
                );
              }
            };
            $scope.form.init();
          }
        ]
      };
    }
  ])
  .controller("networkDevicesAddModalCtrl", [
    "$scope",
    "Restangular",
    "data",
    "$uibModalInstance",
    function($scope, Restangular, data, $uibModalInstance) {
      var userId = data.user_id;

      $scope.form = {
        loading: false,
        error: false,
        AssignType: "auto",
        errorMsg: "",
        data: {
          name: null,
          mac_address: null,
          user_id: userId,
          user_ip_address_id: null
        },
        init: function() {
          $scope.form.loading = true;
          Restangular.all("user-ip-address/" + userId + "/free-ips")
            .getList()
            .then(
              function(result) {
                $scope.form.loading = false;
                if (result) {
                  $scope.form.userFreeIpList = result;
                }
              },
              function(error) {
                console.log(error);
              }
            );
        },
        cancel: function() {
          $uibModalInstance.dismiss();
        },
        submit: async function($form) {
          $scope.form.submitting = true;

          if (!$form.$valid) {
            $scope.form.error = true;
            return;
          }
          if ($scope.form.AssignType == "auto") {
            if ($scope.form.userFreeIpList.length == 0) {
              $scope.form.error = true;
              $scope.form.errorMsg =
                "No free ip available. Please release an ip and try again.";
              return;
            } else
              $scope.form.data.user_ip_address_id =
                $scope.form.userFreeIpList[0].id;
          }
          Restangular.all("my-network-devices")
            .post($scope.form.data)
            .then(
              function(response) {
                $scope.form.submitting = false;
                $uibModalInstance.close(response);
              },
              function(error) {
                $scope.form.error = true;
                $scope.form.errorMsg = error.data.message;
                return;
                // $uibModalInstance.dismiss();
              }
            );
        },
        select_ip: function(data) {
          $scope.form.selected_ip = data;
          $scope.form.data.user_ip_address_id = $scope.form.selected_ip.id;
        }
      };
      $scope.form.init();
    }
  ])
  .controller("networkDevicesEditModalCtrl", [
    "$scope",
    "Restangular",
    "data",
    "$uibModalInstance",
    "$q",
    function($scope, Restangular, data, $uibModalInstance, $q) {
      var deviceId = data.device_id;
      var userId = data.user_id;

      $scope.form = {
        loading: false,
        error: false,
        errorMsg: "",
        AssignType: "manual",
        data: {
          name: null,
          mac_address: null,
          user_id: userId,
          user_ip_address_id: null
        },
        init: function() {
          var deviceData = Restangular.one(
            "my-network-devices",
            deviceId
          ).get();
          var userFreeIpList = Restangular.all(
            "user-ip-address/" + userId + "/free-ips"
          ).getList();

          $q.all([
            deviceData,
            userFreeIpList,
            ($scope.form.loading = true)
          ]).then(function(result) {
            $scope.form.loading = false;
            if (result[0]) {
              $scope.form.data = result[0];
              if (result[0].ip != null) {
                $scope.form.selected_ip = result[0].ip.user_ip;
                $scope.form.data.user_ip_address_id = result[0].ip.user_ip.id;
              }
            }
            if (result[1]) {
              $scope.form.userFreeIpList = result[1];
            }
          });
        },
        cancel: function() {
          $uibModalInstance.dismiss();
        },
        submit: function($form) {
          $scope.form.submitting = true;
          if (!$form.$valid) {
            $scope.form.error = true;
            return;
          }
          Restangular.all("my-network-devices")
            .customPUT($scope.form.data, deviceId)
            .then(
              function(response) {
                $scope.form.submitting = false;
                $uibModalInstance.close(response);
              },
              function(error) {
                $scope.form.error = true;
                $scope.form.errorMsg =
                  "Unable to save record. Please try again.";
                return;
                // $uibModalInstance.dismiss();
              }
            );
        },
        select_ip: function(data) {
          $scope.form.selected_ip = data;
          $scope.form.data.user_ip_address_id = $scope.form.selected_ip.id;
        }
      };
      $scope.form.init();
    }
  ]);

angular.module('myApp')
    .controller('expenseIndexCtrl',["$scope", "Restangular", "$uibModal","DTOptionsBuilder",
  "Datatable", function ($scope, Restangular, $uibModal, DTOptionsBuilder,
  Datatable) {

    
    $scope.filter = {};
    $scope.expense = {
      data : []
    }
    var params = {pagination : 0};
    Restangular.all('expense').getList(params).then(function(response){
      $scope.expense.data = response;
      $scope.originalData = response;
    }, function(error){

    });

    $("#payment_status").select2();
    //$("#name_filter").select2();

    $("#payment_status").change(function (event) {
      $scope.$apply(function () {
        var selectedOptions = event.val;
        $scope.expense.data = Datatable.filterData($scope.filter, $scope.originalData, selectedOptions, "payment_status");
      });
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions()
      .withOption('order', [[0, 'DESC']])
      .withOption('scrollY', '300px')
      .withOption('scrollX', '100%')
      .withOption('scrollCollapse', true)
      .withOption('pageLength', 1000)
      .withOption('lengthMenu', [50, 100, 150, 200, 500, 1000])
    
      $scope.addExpense = function() {
            var modal = $uibModal.open({
              controller: "expenseAddModalCtrl",
              windowClass: "bootstrap_wrap",
              templateUrl: "/views/expense/add-edit-modal.html",
              backdrop: "static",
              size: "lg",
              resolve: {
                data: function() {
                  return { id: 1 };
                }
              }
            });
            modal.result.then(
              function(result) {
                window.location.reload();
              },
              function() {
                //cancel
              }
            );
          };
          
          $scope.viewDetail = function(id) {
            console.log('I am here',id);
            var path = "/admin/expense/" + id;
            window.location.href = path;
          };

          $scope.editDetail = function(id) {
            var modal = $uibModal.open({
              controller: "expenseEditModalCtrl",
              windowClass: "bootstrap_wrap",
              templateUrl: "/views/expense/add-edit-modal.html",
              backdrop: "static",
              size: "lg",
              resolve: {
                data: function() {
                  return { id: id };
                }
              }
            });
            modal.result.then(
              function(result) {
                window.location.reload();
              },
              function() {
                //cancel
              }
            );
          };
          $scope.deleteDetails = function(id) {
            if (confirm("Are you sure you want to delete this vendor?")) {
              Restangular.one("vendor", id)
                .remove()
                .then(
                  function(response) {
                    window.location.reload();
                  },
                  function(error) {
                    alert(error.data.message);
                  }
                );
            }
          }

    }]);
angular.module("myApp").controller("expenseAddModalCtrl", [
    "$scope",
    "Restangular",
    "data",
    "$uibModalInstance",
    function($scope, Restangular, data, $uibModalInstance) {
        $scope.loaded = true;
        $scope.vendors = [];
        $scope.expenseHeads = [];
        $scope.userList = [];
        $scope.purchaseOrderList = [];
        $scope.paymentMethodList = [];
        $scope.status_list = [
            { value : "pending", name : "Pending" },
            { value : "paid", name : "Paid" },
            { value : "hold", name : "Hold" },
            { value : "due_immediate_pending", name : "Due Immediate Pending" },
            { value : "due_next_cycle_pending", name : "Due Next Cycle Pending" },
            { value : "due_immediate_approved", name : "Due Immediate Approved" },
            { value : "due_next_cycle_approved", name : "Due Next Cycle Approved" }
        ];
        
        var getVendors = function(){
            Restangular.all("vendor").getList({
                'pagination': 0
            })
            .then(
                function (response) {
                    $scope.vendors = response;
                },
                function (errors) {
                    console.log(errors);
                }
            );
        };
        var getExpenseHeads = function(){
            Restangular.all("expense-heads").getList({
                'pagination': 0
            })
            .then(
                function (response) {
                    $scope.expenseHeads = response;
                },
                function (errors) {
                    console.log(errors);
                }
            );
        };
        var getUsers = function() {
            Restangular.all("user").getList({
                'pagination': 0
              })
              .then(
                function (response) {
                  $scope.loading = false;
                  $scope.userList = response;
                },
                function (errors) {
                  $scope.loading = false;
                  $scope.error_message = errors.data.message;
                }
              );
        };
        var getPaymentMethods = function(){
            Restangular.all("payment-method").getList({
                'pagination': 0
            })
            .then(
                function (response) {
                    $scope.paymentMethodList = response;
                },
                function (errors) {
                    console.log(errors);
                }
            );
        };
        var getPurchaseOrders = function(){
            Restangular.all("purchase-orders").getList({
                'pagination': 0
            })
            .then(
                function (response) {
                    $scope.purchaseOrderList = response;
                },
                function (errors) {
                    console.log(errors);
                }
            );
        };

        getVendors();
        getExpenseHeads();
        getUsers();
        getPaymentMethods();
        getPurchaseOrders();
        $scope.form = {
            loading: false,
            po : {
                is_available: null,//yes/no
                number: null,
                submitted: false,
                loaded: false,
                loading : false,
                isValid : false,
                data : {},
                change: function(){
                    $scope.form.po.data = {};
                    $scope.form.po.number = null;
                    $scope.form.po.submitted = false;
                    $scope.form.po.loaded = false;
                    $scope.form.po.isValid = false;
                }

            },
            model: {
                po_id: null,
                expense_head_id: null,
                amount: null,
                payment_status: null,
                payment_method_id: null,
                invoice_number: null,
                invoice_date: null,
                expense_by: null,
                payment_by: null,
                payment_reference_number: null,
                payment_date: null,
                other_info: null,
            },
            errors : {
                type: false,
                vendor_id: false,
                amount: false,
                error: false
            },
            select_vendor: function(data) {
                $scope.form.model.vendor_id = data.id;
                $scope.form.errors.vendor_id = false;
            },
            select_po: function(data) {
                $scope.form.model.po_id = data.id;
                $scope.form.errors.po_id = false;
            },
            select_expense_head: function(data) {
                $scope.form.model.expense_head_id = data.id;
            },
            select_payment_method: function(data) {
                $scope.form.model.payment_method_id = data.id;
            },
            select_payment_status: function(data) {
                $scope.form.model.payment_status = data.value;
            },
            select_user: function(data) {
                $scope.form.model.expense_by = data.id
            },
            select_payment_by: function(data) {
                $scope.form.model.payment_by = data.id
            },
            getPODetail : function() {
                $scope.form.po.submitted = true;
                console.log('getPODetail called',$scope.form.po.number, $scope.form.po.is_available);
                if($scope.form.po.number != null && $scope.form.po.is_available == 'yes') {
                    
                    Restangular.one('purchase-orders', $scope.form.po.number).get().then(function(response){
                        $scope.form.po.data = response;
                        $scope.form.po.isValid = true;
                        $scope.form.po.loaded = true;
                        console.log('RES', response, $scope.form.po.isValid);
                        
                    }, function(error){
                        $scope.form.po.isValid = false;
                        $scope.form.po.loaded = true;
                        console.log('error',error);
                    });    
                }
                
            },
            submit: function ($form) {
                $scope.form.submitted = true;
                
                if(!$form.$valid) {
                    return false;
                }
                // $scope.form.model.invoice_date = moment($scope.form.model.invoice_date).format("YYYY-MM-DD");
                // $scope.form.model.payment_date = moment($scope.form.model.payment_date).format("YYYY-MM-DD");
                $scope.form.loading = true;
                $scope.form.model.po_id = $scope.form.po.number;
                var params = {
                    data : $scope.form.model
                };
                
                Restangular.all('expense').post(params).then(
                    function (response) {
                        $scope.form.loading = false;
                        $scope.close();
                    },
                    function (errors) {
                        $scope.form.loading = false;
                        $scope.form.errors.error_message =  errors.data.message;
                        console.log(errors);
                    }
                );
            },
            cancel : function() {
                $uibModalInstance.dismiss();
            },
            close : function() {
                $uibModalInstance.close();
            },
        }
        // Get existing Services name
        $scope.close = function() {
            $uibModalInstance.close();
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss();
        };
    }
  ]);
  
angular.module("myApp").controller("expenseEditModalCtrl", [
    "$scope",
    "Restangular",
    "data",
    "$uibModalInstance",
    function($scope, Restangular, data, $uibModalInstance) {
        $scope.existingServices = [];
        var id = data.id;
        var expenseObj = Restangular.one("expense", id);
        $scope.loaded = false;
        $scope.vendors = [];
        $scope.expenseHeads = [];
        $scope.userList = [];
        $scope.purchaseOrderList = [];
        $scope.paymentMethodList = [];
        $scope.status_list = [
            { value : "pending", name : "Pending" },
            { value : "paid", name : "Paid" },
            { value : "hold", name : "Hold" },
            { value : "due_immediate_pending", name : "Due Immediate Pending" },
            { value : "due_next_cycle_pending", name : "Due Next Cycle Pending" },
            { value : "due_immediate_approved", name : "Due Immediate Approved" },
            { value : "due_next_cycle_approved", name : "Due Next Cycle Approved" }
        ];
        
        var getVendors = function(){
            Restangular.all("vendor").getList({})
            .then(
                function (response) {
                    $scope.vendors = response;
                },
                function (errors) {
                    console.log(errors);
                }
            );
        };
        var getExpenseHeads = function(){
            Restangular.all("expense-heads").getList({})
            .then(
                function (response) {
                    $scope.expenseHeads = response;
                },
                function (errors) {
                    console.log(errors);
                }
            );
        };
        var getUsers = function() {
            Restangular.all("user").getList({
                'pagination': 0
              })
              .then(
                function (response) {
                  $scope.loading = false;
                  $scope.userList = response;
                },
                function (errors) {
                  $scope.loading = false;
                  $scope.error_message = errors.data.message;
                }
              );
        };
        var getPaymentMethods = function(){
            Restangular.all("payment-method").getList({
                'pagination': 0
            })
            .then(
                function (response) {
                    $scope.paymentMethodList = response;
                },
                function (errors) {
                    console.log(errors);
                }
            );
        };
        var getPurchaseOrders = function(){
            Restangular.all("purchase-orders").getList({
                'pagination': 0
            })
            .then(
                function (response) {
                    $scope.purchaseOrderList = response;
                },
                function (errors) {
                    console.log(errors);
                }
            );
        };
        getVendors();
        getExpenseHeads();
        getUsers();
        getPaymentMethods();
        getPurchaseOrders();
        
        $scope.form = {
            loading: false,
            po : {
                is_available: null,//yes/no
                number: null,
                submitted: false,
                loaded: false,
                loading : false,
                isValid : false,
                data : {},
                change: function(){
                    $scope.form.po.data = {};
                    $scope.form.po.number = null;
                    $scope.form.po.submitted = false;
                    $scope.form.po.loaded = false;
                    $scope.form.po.isValid = false;
                }

            },
            model: {
                po_id: null,
                expense_head_id: null,
                amount: null,
                payment_status: null,
                payment_method_id: null,
                invoice_number: null,
                invoice_date: null,
                expense_by: null,
                payment_by: null,
                payment_reference_number: null,
                payment_date: null,
                other_info: null,
            },
            errors : {
                type: false,
                vendor_id: false,
                amount: false,
                error: false
            },
            select_vendor: function(data) {
                $scope.form.model.vendor_id = data.id;
                $scope.form.errors.vendor_id = false;
            },
            select_po: function(data) {
                $scope.form.model.po_id = data.id;
                $scope.form.errors.po_id = false;
            },
            select_expense_head: function(data) {
                $scope.form.model.expense_head_id = data.id;
            },
            select_payment_method: function(data) {
                $scope.form.model.payment_method_id = data.id;
            },
            select_payment_status: function(data) {
                $scope.form.model.payment_status = data.value;
            },
            select_user: function(data) {
                $scope.form.model.expense_by = data.id
            },
            select_payment_by: function(data) {
                $scope.form.model.payment_by = data.id
            },
            getPODetail : function() {
                $scope.form.po.submitted = true;
                if($scope.form.po.number != null && $scope.form.po.is_available == 'yes') {
                    
                    Restangular.one('purchase-orders', $scope.form.po.number).get().then(function(response){
                        $scope.form.po.data = response;
                        $scope.form.po.isValid = true;
                        $scope.form.po.loaded = true;
                        
                        
                    }, function(error){
                        $scope.form.po.isValid = false;
                        $scope.form.po.loaded = true;
                        console.log('error',error);
                    });    
                }
                
            },
            submit: function ($form) {
                $scope.form.submitted = true;
                
                if(!$form.$valid) {
                    return false;
                }
                // $scope.form.model.invoice_date = moment($scope.form.model.invoice_date).format("YYYY-MM-DD");
                // $scope.form.model.payment_date = moment($scope.form.model.payment_date).format("YYYY-MM-DD");
                $scope.form.loading = true;
                $scope.form.model.po_id = $scope.form.po.number;
                var params = {
                    data : $scope.form.model
                };
                
                expenseObj.customPUT({
                    data: $scope.form.model
                }).then(
                    function (response) {
                        $scope.form.loading = false;
                        $scope.close();
                    },
                    function (errors) {
                        $scope.form.loading = false;
                        $scope.form.errors.error_message =  errors.data.message;
                        console.log(errors);
                    }
                );
            },
            cancel : function() {
                $uibModalInstance.dismiss();
            },
            close : function() {
                $uibModalInstance.close();
            },
        }

        
        expenseObj.get().then(
            function(response) {
                $scope.form.model = response;
                $scope.form.model.payment_date = new Date(response.payment_date);
                $scope.form.model.invoice_date = new Date(response.invoice_date);
                if( $scope.form.model.po_id )
                {
                    $scope.form.po.number = $scope.form.model.po_id; 
                    $scope.form.po.is_available = 'yes';
                    $scope.form.getPODetail();
                    console.log($scope.form.po);
                } else {
                    $scope.form.po.is_available = 'no';
                }
                $scope.loaded = true;
            },
            function(error) {
                $scope.loaded = true;
                $scope.form.errors.error_message =  error.data.message;
              console.log("err", error);
            }
          );

        $scope.filteredDate = function (input_date) {
            var formatted_date = new Date(input_date);
            return formatted_date;
        };
        
        // Get existing Services name
        $scope.close = function() {
            $uibModalInstance.close();
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss();
        };
    }
  ]);
  
angular.module('myApp')
    .controller('expenseShowCtrl',["$scope", "Restangular","$uibModal", "$rootScope", function ($scope, Restangular, $uibModal, $rootScope) {
        $scope.loaded = false;
        var id = window.expense.id;
        $scope.readOnly = window.read_only;
        $scope.expenseObj = Restangular.one("expense", id);
        // $scope.departments = Restangular.all("department").getList();
        // $scope.users =
        $scope.expenseObj.get().then(
            function(response) {
                $scope.loaded = true;
                $scope.expenseObj = response;
                console.log($scope.expenseObj);
            },
            function(error) {
                $scope.loaded = true;
                console.log('error',error);
            }
        );

        $scope.changeStatus = function() {
            var modal = $uibModal.open({
              controller: "changeStatusModalCtrl",
              windowClass: "bootstrap_wrap",
              templateUrl: "/views/expense/change-status-modal.html",
              backdrop: "static",
              size: "md",
              resolve: {
                data: function() {
                  return {id: id, payment_status: $scope.expenseObj.payment_status};
                }
              }
            });
            modal.result.then(
              function(result) {
                console.log(result);
                $scope.expenseObj.payment_status = result;
                $rootScope.$broadcast('activity-log');
              },
              function() {
                //cancel
              }
            );
        };
        
        $scope.approveExpense = function() {
            var modal = $uibModal.open({
                controller: "approveExpenseModalCtrl",
                windowClass: "bootstrap_wrap",
                templateUrl: "/views/expense/approve-modal.html",
                backdrop: "static",
                size: "md",
                resolve: {
                  data: function() {
                    return {id:id};
                  }
                }
              });
              modal.result.then(
                function() {
                  $rootScope.$broadcast('activity-log');
                  $scope.expenseObj.payment_status = 'approved';
                  // console.log(result);
                },
                function() {
                  //cancel
                }
              );
        };

    }]);
angular.module('myApp')
    .controller('incidentShowCtrl',["$scope", "Restangular","$uibModal", "$rootScope", function ($scope, Restangular, $uibModal, $rootScope) {
        $scope.loaded = false;
        var id = window.incident.id;
        $scope.readOnly = window.read_only;
        $scope.incidentObj = Restangular.one("incident", id);
        // $scope.departments = Restangular.all("department").getList();
        // $scope.users =
        $scope.incidentObj.get().then(
            function(response) {
                $scope.loaded = true;
                $scope.incidentObj = response;
            },
            function(error) {
                $scope.loaded = true;
                console.log('error',error);
            }
        );

        $scope.transferIncident = function() {
            var modal = $uibModal.open({
              controller: "transferIncidentModalCtrl",
              windowClass: "bootstrap_wrap",
              templateUrl: "/views/incident/transfer.html",
              backdrop: "static",
              size: "md",
              resolve: {
                data: function() {
                  return {id:id};
                }
              }
            });
            modal.result.then(
              function(result) {
                $scope.incidentObj = result;
                $rootScope.$broadcast('activity-log');
              },
              function() {
                //cancel
              }
            );
        };
        
        $scope.shareIncident = function() {
            var modal = $uibModal.open({
                controller: "shareIncidentModalCtrl",
                windowClass: "bootstrap_wrap",
                templateUrl: "/views/incident/share-modal.html",
                backdrop: "static",
                size: "md",
                resolve: {
                  data: function() {
                    return {id:id};
                  }
                }
              });
              modal.result.then(
                function(result) {
                  $rootScope.$broadcast('activity-log');
                  $scope.incidentObj = result;
                  // console.log(result);
                },
                function() {
                  //cancel
                }
              );
        };

        $scope.closeIncident = function() {
            if (confirm("Are you sure you want to close this incident?")) {
                $scope.incidentObj.status = 'closed';
                $scope.incidentObj
                  .put()
                  .then(
                    function(response) {
                      $rootScope.$broadcast('activity-log');
                    },
                    function(error) {
                      alert(error.data.message);
                    }
                  );
              }
        };

        $scope.reopenIncident = function() {
          if (confirm("Are you sure you want to reopen this incident?")) {
              $scope.incidentObj.status = 'reopen';
              $scope.incidentObj
                .put()
                .then(
                  function(response) {
                    $rootScope.$broadcast('activity-log');
                  },
                  function(error) {
                    alert(error.data.message);
                  }
                );
            }
      };

        $scope.verifiedclosedIncident = function() {
            if (confirm("Are you sure you want to verify close this incident?")) {
                $scope.incidentObj.status = 'verified_closed';
                $scope.incidentObj
                  .put()
                  .then(
                    function(response) {
                      $rootScope.$broadcast('activity-log');
                    },
                    function(error) {
                      alert(error.data.message);
                    }
                  );
              }
        };

    }]);
angular.module("myApp").controller("transferIncidentModalCtrl", [
  "$scope",
  "Restangular",
  "data",
  "$uibModalInstance",
  function($scope, Restangular, data, $uibModalInstance) {
    var id = data.id;
    $scope.transfer_type = 'department';
    $scope.selected_department = null;
    $scope.selected_user = null;
    $scope.error = false;
    Restangular.all("department").getList().then(
      function(response) {
        $scope.departments = response;
      },
      function(error) {
        console.log(error);
      }
    );
    var params = {'pagination':0};
    
    Restangular.all("user").getList(params).then(
      function(response) {
        $scope.users = response;
      },
      function(error) {
        console.log(error);
      }
    );

    $scope.select_department = function(data) {
      $scope.selected_department = data.id;
      $scope.error = false;
    };

    $scope.select_user = function(data) {
      $scope.selected_user = data.id;
      $scope.error = false;
    };
    $scope.submit = function () {
      if ( $scope.selected_department == null && $scope.selected_user == null )
      {
        $scope.error = true;
        return;
      }
      var params = {
        'incident_id': id,
        'department_id' : $scope.selected_department,
        'user_id' : $scope.selected_user,
      }
      Restangular.all("incident-transfer").post(params).then(
        function(response) {
          $uibModalInstance.close(response);
        },
        function(error) {
          console.log(error);
        }
      );
    };
    
    $scope.close = function() {
      $uibModalInstance.close();
    };

    $scope.cancel = function() {
      $uibModalInstance.dismiss();
    };
  }
]);

angular.module("myApp").controller("shareIncidentModalCtrl", [
  "$scope",
  "Restangular",
  "data",
  "$uibModalInstance",
  "$q",
  function($scope, Restangular, data, $uibModalInstance, $q) {
    var id = data.id;
    var incidentObj = Restangular.one("incident", id);
    $scope.form = {
      model: {
        shared_users: []
      },
      departments : [],
      users: [],
      init: function() {
        var users = Restangular.all("user?pagination=0").getList();
        var incident = incidentObj.get();
        $q.all([incident, users, ($scope.form.loading = true)]).then(
          function(result) {
            $scope.form.loading = false;
            if (result[0]) {
              $scope.form.model = result[0];
              $scope.form.model.shared_users = [];
              for ( i=0; i<$scope.form.model.incident_users.length; i++  )
              {
                $scope.form.model.shared_users.push($scope.form.model.incident_users[i].user_id);
              }
            }
            if (result[1]) {
              $scope.form.users = result[1];
            }
          }
        );
      },
      submit: function($form) {
        $scope.form.submitted = true;

        if (!$form.$valid) {
          $scope.form.error = true;
          $scope.form.submitted = false;
          return false;
        }
        $scope.form.loading = true;
        incidentObj.subject = $scope.form.model.subject;
        incidentObj.description = $scope.form.model.description;
        incidentObj.department_id = $scope.form.model.department.id;
        incidentObj.shared_users = $scope.form.model.shared_users;
        incidentObj.put()
          .then(
            function(response) {
              $scope.form.submitted = false;
              $scope.form.loading = false;
              $uibModalInstance.close(response);
            },
            function(error) {
              console.log(error);
              $scope.form.loading = false;
            }
          );
      },
      cancel: function() {
        $uibModalInstance.dismiss();
      },
    };
    $scope.form.init();
  }
]);

angular.module('myApp')
    .controller('userFeedbackCtrl',["$scope", "Restangular", "$uibModal",function ($scope, Restangular, $uibModal) {
        $scope.feedbackMonthDetails = JSON.parse(window.feedbackMonthDetails);
        $scope.user = JSON.parse(window.user);
        $scope.selected_month = null;
        $scope.review_details = [];
        
        $scope.loadReviews = function(month) {
            $scope.selected_month = month;
            Restangular.all("feedback-reviews-monthly").getList({
                'month_id': month.id
            })
            .then(
                function (response) {
                    $scope.review_details = response;
                    console.log($scope.review_details);
                },
                function (errors) {
                    
                }
            );
        };
    }]);
angular.module("myApp").controller("approveExpenseModalCtrl", [
  "$scope",
  "Restangular",
  "data",
  "$uibModalInstance",
  function($scope, Restangular, data, $uibModalInstance) {
    var id = data.id;
    $scope.message = '';
    $scope.error = false;
    $scope.loading = false;
    $scope.submit = function() {
      $scope.loading = true;
      var params = {
        'id': id,
        'status': 'approved'
      };
      Restangular.all('expense-update-status').post(params).then(
          function(response) {
            $scope.loading = false;
            $uibModalInstance.close(response);
          },
          function(error) {
            console.log(error);
            $scope.message = error.data.message;
            $scope.error = true;
            $scope.loading = false;
          }
        );
    };
    $scope.cancel = function() {
      $uibModalInstance.dismiss();
    };
  }]);

angular.module("myApp").controller("changeStatusModalCtrl", [
  "$scope",
  "Restangular",
  "data",
  "$uibModalInstance",
  "$q",
  function($scope, Restangular, data, $uibModalInstance, $q) {
    console.log(data);
    $scope.payment_status = data.payment_status;
    var id = data.id;
    $scope.message = '';
    $scope.error = false;
    $scope.loading = false;
    $scope.status_list = [
      { value : "paid", name : "Paid" },
      { value : "hold", name : "Hold" },
      { value : "due_immediate_pending", name : "Due Immediate Pending" },
      { value : "due_next_cycle_pending", name : "Due Next Cycle Pending" },
      { value : "due_immediate_approved", name : "Due Immediate Approved" },
      { value : "due_next_cycle_approved", name : "Due Next Cycle Approved" }
  ];



    $scope.submit = function() {
      $scope.loading = true;
      var params = {
        'id': id,
        'status': $scope.payment_status
      };
      Restangular.all('expense-update-status').post(params).then(
          function(response) {
            $scope.loading = false;
            $uibModalInstance.close(response);
          },
          function(error) {
            console.log(error);
            $scope.message = error.data.message;
            $scope.error = true;
            $scope.loading = false;
          }
        );
    };
    $scope.cancel = function() {
      $uibModalInstance.dismiss();
    };
  }]);

"use strict()";

/*Directives*/
angular
  .module("myApp")
  .directive("googleDriveLinks", [
    function() {
      return {
        templateUrl: "/scripts/directives/google-drive-links/index.html",
        restrict: "E",
        replace: true,
        scope: {
          referenceId: "=",
          referenceType: "@referenceType",
          isDisabled: "@isDisabled",
          mode: "@mode"
        },
        controller:["$scope", "Restangular", function($scope, Restangular) {
          $scope.oldLinks = [];
          $scope.editable = [];
          $scope.loading = false;
          $scope.error = false;
          $scope.errors = {
            'name' : false,
            'path' : false
          };
          $scope.newLinkObj = {
            name: null,
            path: null
          };
          $scope.loadOldLinks = function() {
            // $scope.oldProjectLinks = [];
            $scope.loading = true;
            Restangular.all("google-drive-link")
              .customGET("", {
                reference_id: $scope.referenceId,
                reference_type: $scope.referenceType
              })
              .then(
                function(response) {
                  $scope.loading = false;
                  $scope.oldLinks = response;
                },
                function(error) {
                  console.log(error);
                }
              );
          };
          
          $scope.loadOldLinks();

          $scope.addLink = function() {
            $scope.error = false;
            if (!$scope.newLinkObj.name) {
              $scope.errors.name = true;
              $scope.error = true;
            } else {
              $scope.errors.name = false;
            }

            if (!$scope.newLinkObj.path) {
              $scope.errors.path = true;
              $scope.error = true;
            } else {
              $scope.errors.path = false;
            }

            if ($scope.error) {
              return;
            }

            var params = {
              reference_id: $scope.referenceId,
              reference_type: $scope.referenceType,
              name: $scope.newLinkObj.name,
              path: $scope.newLinkObj.path
            };
            Restangular.all("google-drive-link")
              .post(params)
              .then(
                function(response) {
                  $scope.newLinkObj = {
                    name: "",
                    path: ""
                  };
                  $scope.loadOldLinks();
                },
                function(error) {
                  console.log(error);
                }
              );
          };
          $scope.editLink = function(index) {
            $scope.editable[index] = true;
            
          };
          $scope.saveLink = function(index, link) {
            Restangular.one("google-drive-link", link.id)
              .customPUT({
                link: link
              })
              .then(
                function(response) {
                  $scope.editable[index] = false;
                  $scope.loadOldLinks();
                },
                function(errors) {
                  console.log(errors);
                }
              );
          };

          $scope.removeLink = function(index, link) {
            var r = confirm("Are you sure you want to delete this entry?");
            if (r != true) {
              return;

            }
            Restangular.one("google-drive-link", link.id)
              .remove()
              .then(
                function(response) {
                  $scope.oldLinks.splice(index, 1);
                },
                function(error) {
                  console.log(error);
                }
              );
          };
        }]
      };
    }
  ]);

angular.module("myApp").controller("networkAssetrCtrl", [
  "$scope",
  "Restangular",
  "$uibModal",
  "DTOptionsBuilder",
  "Datatable",
  function($scope, Restangular, $uibModal, DTOptionsBuilder, Datatable) {
    $scope.filter = {};
    $scope.asset = {
      data: []
    };
    $scope.error = false;
    $scope.errorMsg = "";

    $scope.init = function() {
      var params = { pagination: 0 };
      Restangular.all("network-asset")
        .getList(params)
        .then(
          function(response) {
            $scope.asset.data = response;
            $scope.originalData = response;
          },
          function(error) {
            $scope.error = true;
            $scope.errorMsg = error.data.message;
          }
        );
    };
    $scope.dtOptions = DTOptionsBuilder.newOptions()
      .withOption("order", [[0, "DESC"]])
      .withOption("scrollY", "300px")
      .withOption("scrollX", "100%")
      .withOption("scrollCollapse", true)
      .withOption("pageLength", 1000)
      .withOption("lengthMenu", [50, 100, 150, 200, 500, 1000]);

    $scope.assignIp = function(asset, meta) {
      var params = {
        asset_id: asset.id,
        meta_id: meta.id
      };
      Restangular.all("network-asset/assign-ip")
        .post(params)
        .then(
          function(response) {
            meta.ip_mapper = response;
          },
          function(error) {
            $scope.error = true;
            $scope.errorMsg = error.data.message;
          }
        );
    };
    $scope.releaseIp = function(meta) {
      console.log(meta, "fghjk");
      Restangular.one("network-asset/release-ip", meta.ip_mapper.id)
        .get()
        .then(
          function(response) {
            meta.ip_mapper = null;
          },
          function(error) {
            $scope.error = true;
            $scope.errorMsg = error.data.message;
          }
        );
    };
    $scope.init();
  }
]);

angular.module("myApp").controller("bonusRequestCtrl", [
  "$scope",
  "Restangular",
  "$uibModal",
  "DTOptionsBuilder",
  "Datatable",
  "filterFilter",
  "datetimeFilter",
  function(
    $scope,
    Restangular,
    $uibModal,
    DTOptionsBuilder,
    Datatable,
    filterFilter,
    datetimeFilter
  ) {
    $scope.filter = 'all';
    $scope.type = 'na';
    $scope.data = [];
    $scope.user_id = 0;
    $scope.originalData = [];
    var monthId = window.monthId;
    Restangular.all("user")
      .getList({ pagination: 0 })
      .then(
        function(response) {
          $scope.loading = false;
          $scope.userList = response;
        },
        function(errors) {
          $scope.loading = false;
          $scope.error_message = errors.data.message;
        }
      );
    
    $scope.init = function() {
      getBonusCount();
      getBonusList('all');
    };
    var getBonusCount = function () {
      let params = {
        month_id: monthId
      }
      Restangular.one("bonus-request-count").get(params).then(
        function (response) {
          $scope.bonusCounts = response;
          
        },
        function(errors) {
          console.log(errors);
        }
      )
    };
    
    var getBonusList = function(filter, type, user_id) {
      var params = { status : 'pending', monthId: monthId, filter: filter, pagination: 0, type: type, user_id: user_id };
      Restangular.all("bonus-request")
        .getList(params)
        .then(
          function(response) {
            angular.forEach(response, function(item) {
              item.created_at = datetimeFilter(item.created_at);
              item.redeem_info = JSON.parse(item.redeem_type);
              item.additional_bonus = (item.user.one_day_salary * item.working_hours) / 8;
              if ( item.bonus_request_projects && item.bonus_request_projects.length > 0 )
              {
                tempProjectIds = [];
                for ( j = 0; j < item.bonus_request_projects.length; j++ )
                {
                  tempProjectIds.push(item.bonus_request_projects[j].project_id);
                }
                item.selected_projects = tempProjectIds;
              }

            });

            $scope.data = response;
          },
          function(error) {
            console.log(error);
          }
        );
    };
      
    let getProjects = function() {
      var params = {
        pagination: 0,
        status: 'in_progress'
      };
      Restangular.all("project")
        .getList(params)
        .then(function(projectList) {
          // console.log("ProjectList-->", projectList);
          $scope.projects = projectList;
        })
        .catch(function(projectListErr) {
          console.log("ProjectList failed", projectListErr);
        });
    };
    getProjects();
    
    $scope.approveBonusRequest = function(bonusRequest) {
      Restangular.all("bonus-request-approve")
        .post(bonusRequest)
        .then(
          function(response) {
            $scope.init();
          },
          function(error) {
            console.log(error);
          }
        );
    };
    $scope.rejectBonusRequest = function(bonusRequest) {
      Restangular.all("bonus-request-reject")
        .post(bonusRequest)
        .then(
          function(response) {
            $scope.init();
          },
          function(error) {
            console.log(error);
          }
        );
    };
    $scope.addBonusRequest = function() {
      var modal = $uibModal.open({
        controller: "addBonusRequestModalCtrl",
        windowClass: "bootstrap_wrap",
        templateUrl: "/views/bonus-request/add.html",
        backdrop: "static",
        size: "md",
        resolve: {
          data: function() {
            return { month_id: monthId };
          }
        }
      });
      modal.result.then(
        function(result) {
          window.location.reload();
        },
        function() {
          //cancel
        }
      );
    };
    
    $scope.select_projects = function (bonusRequest) {
      console.log('bonusRequest',bonusRequest);
      var params = {
        bonus_request_id: bonusRequest.id,
        projects: bonusRequest.selected_projects
      };
      
      Restangular.all("bonus-request-projects")
              .post(params)
              .then(
                  function(response) {
                      console.log(response);
                  },
                  function(errors) {
                      console.log(errors);
                  }
              );
    },
    
    $scope.lockMonth = function(monthId) {
      Restangular.one("bonus-request/lock-month/" + monthId)
        .get()
        .then(
          function(response) {
            window.location.reload();
          },
          function(error) {
            console.log(error);
          }
        );
    };

    $scope.dateWithDay = function(date) {
      var d = new Date(date);
      var return_date = moment(d).format("ddd, MMM Do YYYY");
      return return_date;
    }

    $scope.fetchAllBonus = function () {
      $scope.filter = 'all';
      $scope.type = 'na';
      getBonusList($scope.filter, $scope.type);
    };

    $scope.fetchApprovedBonus = function () {
      $scope.filter = 'approved';
      getBonusList($scope.filter, $scope.type);
    };

    $scope.fetchRejectedBonus = function () {
      $scope.filter = 'rejected';
      getBonusList($scope.filter, $scope.type);
    };

    $scope.fetchPendingBonus = function () {
      $scope.filter = 'pending';
      getBonusList($scope.filter, $scope.type);
    };

    $scope.fetchOnsiteBonus = function () {
      $scope.type = 'onsite';
      getBonusList($scope.filter, $scope.type);
    };

    $scope.fetchOnsiteBonus = function () {
      $scope.type = 'onsite';
      getBonusList($scope.filter, $scope.type);
    };

    $scope.fetchAdditionalDayBonus = function () {
      $scope.type = 'additional';
      getBonusList($scope.filter, $scope.type);
    };

    $scope.fetchOtherBonus = function () {
      $scope.type = 'other';
      getBonusList($scope.filter, $scope.type);
    };

    $scope.fetchForUser = function (data) {
      console.log('data', data);
      $scope.user_id = data.id;
      getBonusList($scope.filter, $scope.type, $scope.user_id);
    };

  
    $scope.init();
  }
]);

angular.module("myApp").filter("datetime", function() {
  return function(input) {
    var t = input.split(/[- :]/);
    var dateTime = new Date(Date.UTC(t[0], t[1] - 1, t[2], t[3], t[4], t[5]));
    return dateTime;
  };
});

angular.module("myApp").controller("addBonusRequestModalCtrl", [
  "$scope",
  "Restangular",
  "data",
  "$uibModalInstance",
  "$q",
  function($scope, Restangular, data, $uibModalInstance, $q) {
    $scope.selected_type = null;
    $scope.selected_user = null;
    $scope.selected_referral_for = null;
    $scope.selected_sub_type = null;
    $scope.showSubType = false;
    $scope.types = [
      { name: "Additional Work Day", code: "additional" },
      { name: "On Site", code: "onsite" },
      { name: "Referral", code: "referral" },
      { name: "Tech-talk", code: "techtalk" }
    ];
    $scope.select_type = function(type) {
      $scope.selected_type = type;
      $scope.form.data.type = $scope.selected_type.code;
      if (type.code == "additional") {
        $scope.sub_types = ["Weekend", "National Holiday", "Leave"];
        $scope.showSubType = true;
      } else if (type.code == "onsite") {
        $scope.sub_types = [
          "Domestic Local",
          "Domestic On-Site",
          "International"
        ];
        $scope.showSubType = true;
      } else {
        $scope.showSubType = false;
        $scope.selected_sub_type = null;
        $scope.form.data.sub_type = null;
      }
    };
    $scope.select_user = function(user) {
      $scope.selected_user = user;
      $scope.form.data.user_id = $scope.selected_user.id;
    };
    $scope.select_sub_type = function(sub_type) {
      $scope.selected_sub_type = sub_type;
      $scope.form.data.sub_type = $scope.selected_sub_type;
    };
    $scope.select_referral = function(user) {
      $scope.selected_referral_for = user;
      $scope.form.data.referral_for = $scope.selected_referral_for.id;
    };
    $scope.form = {
      submitting: false,
      error: false,
      init: function() {
        Restangular.all("user-all-active")
          .getList()
          .then(
            function(response) {
              $scope.form.users = response;
            },
            function(error) {
              console.log(error);
            }
          );
      },
      data: {},
      submit: function($form) {
        $scope.form.submitting = true;
        if (!$form.$valid) {
          $scope.form.error = true;
          return;
        }
        $scope.form.data.month_id = data.month_id;
        Restangular.all("bonus-request")
          .post($scope.form.data)
          .then(
            function(response) {
              $uibModalInstance.close(response);
            },
            function(error) {
              console.log(error);
            }
          );
      }
    };

    $scope.cancel = function() {
      $uibModalInstance.dismiss();
    };
    $scope.close = function() {
      $uibModalInstance.close();
    };
    $scope.form.init();
  }
]);

angular.module("myApp").controller("editBonusRequestModalCtrl", [
  "$scope",
  "Restangular",
  "data",
  "$uibModalInstance",
  "$q",
  function($scope, Restangular, data, $uibModalInstance, $q) {
    var id = data.id;
    $scope.showSubType = false;
    $scope.types = [
      { name: "Additional Work Day", code: "additional" },
      { name: "On Site", code: "onsite" },
      { name: "Referral", code: "referral" },
      { name: "Tech-talk", code: "techtalk" }
    ];
    $scope.select_type = function(type) {
      angular.forEach($scope.types, function(item) {
        if (type === item.code) {
          $scope.form.data.type = item;
        }
      });
      if (type == "additional") {
        $scope.sub_types = ["Weekend", "National Holiday", "Leave"];
        $scope.showSubType = true;
        $scope.form.data.title = null;
        $scope.form.data.referral_for = null;
      } else if (type == "onsite") {
        $scope.sub_types = [
          "Domestic Local",
          "Domestic On-Site",
          "International"
        ];
        $scope.showSubType = true;
        $scope.form.data.title = null;
        $scope.form.data.referral_for = null;
      } else {
        $scope.showSubType = false;
        $scope.form.data.sub_type = null;
      }
    };
    $scope.select_user = function(user) {
      $scope.form.data.user = user;
    };
    $scope.select_sub_type = function(sub_type) {
      $scope.form.data.sub_type = sub_type;
    };
    $scope.select_referral = function(user) {
      $scope.form.data.referral_for = user.id;
    };
    $scope.form = {
      submitting: false,
      error: false,
      errorMsg: "",
      data: {},
      init: function() {
        var userList = Restangular.all("user-all-active").getList();
        var oldData = Restangular.one("bonus-request", id).get();
        $q.all([userList, oldData, ($scope.form.loading = true)]).then(function(
          result
        ) {
          $scope.form.loading = false;
          if (result[0]) {
            $scope.form.users = result[0];
          }
          if (result[1]) {
            $scope.form.data = result[1];
            $scope.select_type($scope.form.data.type);
            $scope.form.data.date = new Date($scope.form.data.date);
          }
        });
      },
      submit: function($form) {
        $scope.form.submitting = true;
        if (!$form.$valid) {
          $scope.form.error = true;
          return;
        }
        var params = {
          user_id: $scope.form.data.user.id,
          date: $scope.form.data.date,
          type: $scope.form.data.type.code,
          sub_type: $scope.form.data.sub_type,
          title: $scope.form.data.title,
          referral_for: $scope.form.data.referral_for
            ? $scope.form.data.referral_for
            : null,
          status: "approved",
          id: $scope.form.data.id
        };
        Restangular.one("bonus-request", $scope.form.data.id)
          .customPUT({
            params: params
          })
          .then(
            function(response) {
              $uibModalInstance.close(response);
            },
            function(errors) {
              console.log(errors);
            }
          );
      },
      rejectBonusRequest: function(id) {
        console.log(id, "in reject form");
        Restangular.one("bonus-request/" + id + "/reject")
          .get()
          .then(
            function(response) {
              $scope.close();
            },
            function(error) {
              console.log(error);
            }
          );
      }
    };

    $scope.cancel = function() {
      $uibModalInstance.dismiss();
    };
    $scope.close = function() {
      $uibModalInstance.close();
    };
    $scope.form.init();
  }
]);

angular.module("myApp").controller("bonusRequestShowCtrl", [
  "$scope",
  "Restangular",
  "$uibModal",
  "$rootScope",
  function($scope, Restangular, $uibModal, $rootScope) {
    $scope.loaded = false;
    var id = window.bonusRequest.id;
    $scope.init = function() {
      Restangular.one("bonus-request/" + id)
        .get()
        .then(
          function(response) {
            $scope.bonusRequestObj = response;
          },
          function(error) {
            console.log(error);
          }
        );
    };
    $scope.rejectBonusRequest = function(bonusRequest) {
      Restangular.one("bonus-request/" + bonusRequest.id + "/reject")
        .get()
        .then(
          function(response) {
            $scope.init();
          },
          function(error) {
            console.log(error);
          }
        );
    };
    $scope.approve = function(bonusRequest) {
      var modal = $uibModal.open({
        controller: "editBonusRequestModalCtrl",
        windowClass: "bootstrap_wrap",
        templateUrl: "/views/bonus-request/edit.html",
        backdrop: "static",
        size: "md",
        resolve: {
          data: function() {
            return { id: bonusRequest.id };
          }
        }
      });
      modal.result.then(
        function(result) {
          window.location.reload();
        },
        function() {
          //cancel
        }
      );
    };
    $scope.init();
  }
]);

angular.module("myApp").controller("employeeLeaveNewCtrl", [
  "$scope",
  "Restangular",
  "$location",
  "$log",
  "$uibModal",
  "$route",
  function($scope, Restangular, $location, $log, $uibModal, $route) {
    $scope.gridViewModel = "leave-new?per_page=5";
    $scope.gridViewColumns = [
      {
        title: "Duration",
        value: "%% row.start_date | date %% to %% row.end_date | date %%",
        width: 18
      },
      {
        title: "Days",
        value: "%% row.days %%",
        width: 5
      },
      {
        title: "Type",
        value:
          "<span ng-class=\"{'label label-primary custom-label' : row.leave_type.code=='paid', 'label label-warning custom-label' : row.leave_type.code=='sick', 'label label-success custom-label' : row.leave_type.code=='unpaid' }\"> %% row.leave_type.title | uppercase%% </span>",
        width: 5
      },
      {
        title: "Status",
        value:
          "<span ng-class=\"{'label label-info custom-label' : row.status=='pending', 'label label-success custom-label' : row.status=='approved', 'label label-danger custom-label' : (row.status=='cancelled' || row.status=='rejected')  }\"> %% row.status | uppercase %%</span> <small ng-if='row.status ==approved' style='display:block'>%%row.approver.name%%</small>",
        width: 5
      }
    ];
    // $scope.gridViewActions = [
    //   {
    //     type: "cancel",
    //     action: "deleteDetails(row.id)"
    //   }
    // ];
    $scope.deleteDetails = function(id) {
      if (confirm("Are you sure you want to cancel the leave!")) {
        Restangular.one("leave", id)
          .remove()
          .then(
            function(response) {
              location.reload();
            },
            function(error) {
              alert(error.data.message);
            }
          );
      }
    };
    
    $scope.datePicker = {
      options: {
        applyClass: "btn-success",
        locale: {
          applyLabel: "Apply",
          fromLabel: "From",
          // format: "YYYY-MM-DD", //will give you 2017-01-06
          format: "D-MMM-YY", //will give you 6-Jan-17
          // format: "D-MMMM-YY", //will give you 6-January-17
          toLabel: "To",
          cancelLabel: "Cancel",
          customRangeLabel: "Custom range"
        },
        eventHandlers: { 'apply.daterangepicker' : function () {
          $scope.form.error_message = null;
          $scope.form.durationLoading = true;
          var params = {
            start_date: moment($scope.datePicker.date.startDate).format("YYYY-MM-DD"),
            end_date: moment($scope.datePicker.date.endDate).format("YYYY-MM-DD")
          }
          
          Restangular.one("leave-working-days").get(params).then(
            function (response) {
              $scope.form.durationLoading = false;
              $scope.form.duration = response;
            },
            function(errors) {
              $scope.form.durationLoading = false;
              console.log(errors);
            }
          )
          } 
        }
      },
      date: { startDate: null, endDate: null },
      min: moment().format("YYYY-MM-DD")
    };

    $scope.leave = {
      loading: false,
      form: {
        type: null,
        start_date: null,
        end_date: null
      }
    };
    $scope.currentDate = Date.now();
  
    var getLeaveTypes = function() {
      Restangular.all('leave-types').getList().then(
        function(response) {
          $scope.leaveTypes = response;
          var otherLeaveType = {
            id: 0,
            code: 'other',
            title: 'Others'
          };
          $scope.leaveTypes.push(otherLeaveType);
          console.log($scope.leaveTypes);
        },
        function(errors) {
          console.log(errors);
        }
      )
    };
    getLeaveTypes();
    
    $scope.form = {
      loading: false,
      submitting: false,
      error_message: null,
      error: false,
      success: false,
      radioModel : null,
      radioSelectionError : false,
      sickFlag : false,
      duration: null,
      durationLoading: false,
      data: {
        user_id: $scope.userId,
        start_date: null,
        end_date: null,
        type: null,
        reason: null
      },
      
      applyLeave: function($form) {
        if ( $scope.form.duration == 0 )
        {
          $scope.form.error = true;
          $scope.form.error_message = 'Leave duration is 0 day';
          return false;
        }
        $scope.form.submitting = true;
        if ($form.$invalid) {
          $scope.form.submitting = false;
          $scope.form.error = true;
          return false;
        }
        if ( $scope.form.data.type == 0 && !$scope.form.radioModel )
        {
          $scope.form.submitting = false;
          $scope.form.error = true;
          $scope.form.radioSelectionError = true;
          return false;
        }
        $scope.form.loading = true;
        $scope.form.error = false;
        $scope.form.error_message = null;
        var startDate = moment($scope.datePicker.date.startDate).format(
          "YYYY-MM-DD"
        );
        var endDate = moment($scope.datePicker.date.endDate).format(
          "YYYY-MM-DD"
        );
        
        $scope.form.data.start_date = startDate;
        $scope.form.data.end_date = endDate;

        if ( $scope.form.data.type == 0 ) {
          $scope.form.data.type = $scope.form.radioModel;
        }
        var params = $scope.form.data;
        
        Restangular.all("leave-new")
          .post(params)
          .then(
            function(response) {
              $scope.form.loading = false;
              $scope.form.submitting = false;
              //var data = response.data;
              if (response.data.leave_overlapping) {
                $scope.openOverlappingLeaveModal(response.data);
                return false;
              } else {
                location.reload();
                $scope.form.success = true;
                $scope.form.error = false;
              }
            },
            function(errors) {
              $scope.form.loading = false;
              $scope.form.submitting = false;
              $scope.form.error = true;
              $scope.form.success = false;
              $scope.form.error_message = errors.data.message;
            }
          );
      },
      checkProjectOverlapping: function() {
        $log.log("checkProjectOverlapping called");
      }
    };

    $scope.openOverlappingLeaveModal = function(data) {
      let leave_type = '';
      for ( i = 0; i < $scope.leaveTypes.length; i++ )
      {
        if ( $scope.form.data.type == $scope.leaveTypes[i].id)
        {
          leave_type = $scope.leaveTypes[i].title;
        }
      }
      var data = {
        overlapping_data: data.overlapping_leaves,
        type: $scope.form.data.type,
        leave_type: leave_type,
        start_date: $scope.form.data.start_date,
        end_date: $scope.form.data.end_date,
        reason: $scope.form.data.reason,
        user_id: $scope.userId
      };
      var modal = $uibModal.open({
        controller: "leaveOverlappingModalCtrl",
        windowClass: "bootstrap_wrap",
        templateUrl: "/views/leave/leave-overlapping-modal-new.html",
        backdrop: "static",
        size: "lg",
        resolve: {
          data: function() {
            return data;
          }
        }
      });
      modal.result.then(function(res) {}, function() {});
    };
    $scope.append = function(leave_id) {
      return "/user/leaves/status/" + leave_id;
    };
    $scope.$watch(
      function($scope) { return $scope.form.data.type },
      function() {
        $scope.form.error_message = null;
        if ( $scope.leaveTypes )
        {
          $scope.form.sickFlag = false;
          for ( i = 0; i < $scope.leaveTypes.length; i++ )
          {
            if ( $scope.form.data.type == $scope.leaveTypes[i].id && $scope.leaveTypes[i].code == 'sick' )
            {
              $scope.form.sickFlag = true;
            }
          }
        }
      }
    );
  }
]);

angular.module("myApp").controller("projectSelectionModalCtrl", [
  "$scope",
  "Restangular",
  "data",
  "$uibModalInstance",
  function($scope, Restangular, data, $uibModalInstance) {
    $scope.data = data;
    var params = {date : data.date};
    
    // get the user timelog entries for the date passed
    Restangular.all('user-timesheet').getList(params).then(function(response){
        $scope.form.user_timesheet = response;
        $scope.form.loading = false;
    }, function(error){
        $scope.form.loading = false;
    });

    $scope.form = {
        model : {
            date : data.date,
            onsite_allowance_id  : null,
            is_holiday: data.is_holiday,
            is_weekend: data.is_weekend,
            on_leave: data.on_leave,
            type: "onsite"
        },
        onsite_allowance  : data.onsite_allowance,
        loading : true,
        user_timesheet : [],
        submit : function($form) {
            $scope.form.submitted = true;
            if (!$form.$valid) {
              $scope.form.error = true;
              $scope.form.submitted = false;
              return false;
            }
            var params = $scope.form.model;
            $scope.form.loading = true;
            Restangular.all('bonus/apply-on-site-bonus').post(params).then(
              function (response) {
                  $scope.form.loading = false;
                  console.log('REE', response);

                  $uibModalInstance.close(response);


                  // $scope.data.days[index].bonus.push(response);
                  // if ( response.bonus_request_projects && response.bonus_request_projects.length > 0 )
                  // {
                  //   tempProjectIds = [];
                  //   for ( j = 0; j < response.bonus_request_projects.length; j++ )
                  //   {
                  //     tempProjectIds.push(response.bonus_request_projects[j].project_id);
                  //   }
                  //   $scope.model.selected_projects[response.id] = tempProjectIds;
                  // }
                  // $scope.data.days[index].onSiteActionStatus = 'success';
              },
              function (errors) {
                  console.log(errors);
                  // $scope.data.days[index].onSiteActionStatus = 'error';
                  $scope.form.loading = false;
              }
            );
        },
        cancel : function() {
          $uibModalInstance.dismiss();
        }

    };
    if(data.onsite_allowance.length == 1) {
        console.log('onsite_allowance_id', data.onsite_allowance[0].id);
        $scope.form.model.onsite_allowance_id = data.onsite_allowance[0].id;
    }
    $scope.model = {
      onsite_allowance_id: null
    };
    $scope.save = function() {
      $uibModalInstance.close($scope.model);
    };
    $scope.cancel = function() {
      $uibModalInstance.dismiss();
    };
  }
]);

angular.module("myApp").controller("additionalSelectionModalCtrl", [
  "$scope",
  "data",
  "$uibModalInstance",
  "Restangular",
  function($scope, data, $uibModalInstance,Restangular) {
    $scope.data = data;
    var params = {date : data.date};
    // Restangular.all('get-user-timelog-project').getList(params).then(function(response){
    //   console.log('RES',response)
    // }, function(error){

//    })
    let preSelectedIds = [];
    angular.forEach(data.obj.assigned_projects, function(item) {
      preSelectedIds.push(item.id);
      });
    
    $scope.model = {
      selected_projects: preSelectedIds,
      worked_for: '8h',
      redeem: 'encash',
      error: false,
      no_timesheet_error: false,
      checkForTimesheet : function () {
        if ( $scope.model.selected_projects.length == 0 )
        {
          $scope.model.no_timesheet_error = true;
        }
      }
    };
    $scope.model.checkForTimesheet();
    $scope.save = function() {
      $scope.model.error = false;
      if ($scope.model.selected_projects.length == 0)
      {
        $scope.model.error = true;
        return;
      }
      $uibModalInstance.close($scope.model);
    };
    $scope.cancel = function() {
      $uibModalInstance.dismiss();
    };
  }
]);

angular.module('myApp')
    .controller('insurancePolicyIndexController',["$scope", "Restangular", "$uibModal" , function ($scope, Restangular, $uibModal) {
      $scope.model = {
        data : [],
        loading : true,
        selectAll : function(){
          var items = [];
        },
        
        getData : function(status = ''){
          var params = {status : status,pagination : 0};
          Restangular.all('insurance-policy').getList(params).then(function(response){
            $scope.model.data = response;
            $scope.model.loading = false;
          }, function(error){
            $scope.model.loading = false;
          });    
        },
        addNewPolicy : function() {
          var modal = $uibModal.open({
          controller: "createInsurancePolicyModalCtrl",
          windowClass: "bootstrap_wrap",
          templateUrl: "/views/insurance-policy/create.html",
          backdrop: "static",
          size: "lg",
          resolve: {
            data: function () {
              return {};
            }
          }
        });

        modal.result.then(
          function (result) {
            window.location.reload();
          },
          function () {
            //cancel
          }
        );
        },
        addSubGroup : function() {
          console.log('addSubGroup called');

        },
        view :function(id) {
          var modal = $uibModal.open({
          controller: "createInsurancePolicyModalCtrl",
          windowClass: "bootstrap_wrap",
          templateUrl: "/views/insurance-policy/create.html",
          backdrop: "static",
          size: "lg",
          resolve: {
            data: function () {
              return {};
            }
          }
        });

        modal.result.then(
          function (result) {
            window.location.reload();
          },
          function () {
            //cancel
          }
        );

        },
        edit :function(id) {
          var modal = $uibModal.open({
          controller: "editInsurancePolicyModalCtrl",
          windowClass: "bootstrap_wrap",
          templateUrl: "/views/insurance-policy/create.html",
          backdrop: "static",
          size: "lg",
          resolve: {
            data: function () {
              return { id : id};
            }
          }
        });

        modal.result.then(
          function (result) {
            window.location.reload();
          },
          function () {
            //cancel
          }
        );
        },
      },
     
      $scope.model.getData();
}]);
angular.module("myApp").controller("createInsurancePolicyModalCtrl", [
    "$scope",
    "Restangular",
    "data",
    "$uibModalInstance",
    function($scope, Restangular, data, $uibModalInstance) {
        $scope.options = {
    formatYear: 'yy',
    maxDate: new Date(2020, 5, 22),
    minDate: new Date(),
  };

        $scope.form = {
            policies: [],
            startDate : {
                datepickerOption : {
                    options: {
                      applyClass: "btn-success",
                      singleDatePicker: true,
                      locale: {
                        applyLabel: "Apply",
                        fromLabel: "From",
                        // format: "YYYY-MM-DD", //will give you 2017-01-06
                        format: "D-MMM-YY", //will give you 6-Jan-17
                        // format: "D-MMMM-YY", //will give you 6-January-17
                        toLabel: "To",
                        cancelLabel: "Cancel",
                        customRangeLabel: "Custom range"
                      }
                    }
                },
            },
            vendors : [],

            model : {
                vendor_id : null,
                name : null,
                policy_number : null,
                description : null,
                start_date : false,
                end_date : false,
                coverage_amount : null,
                amount_paid : null,
                premium_amount : null,
                applicable_for : 'user',
                accessible_to : 'group',
                parent_id : false,
                sub_group : false,
                has_sub_group : false,
                sub_groups : []
            },
            sub_group : {
                name : null,
                premium_amount : null
            },
            select_vendor: function(data) {
                $scope.form.selected_vendor = data;
                $scope.form.model.vendor_id = $scope.form.selected_vendor.id;
              },
            select_policy: function(data) {
                $scope.form.selected_policy = data;
                $scope.form.model.parent_id = $scope.form.selected_policy.id;
              },
            addSubGroup : function() {
                $scope.form.model.sub_groups.push($scope.form.sub_group);
                //$scope.form.itemError = false;
                // $scope.form.item = {
                //   name: null,
                //   unit_price: null,
                //   quantity: null,
                //   total: null
                // };
            },
            init: function() {
                $scope.form.loading = true;
                Restangular.all("vendor")
                  .getList({'pagination': 0})
                  .then(
                    function(response) {
                      $scope.form.loading = false;
                      $scope.form.vendors = response;
                    },
                    function(error) {
                      console.log(error);
                    }
                  );
                Restangular.all("insurance-policy")
                  .getList({'pagination': 0,'sub_group' : false})
                  .then(
                    function(response) {
                      $scope.form.loading = false;
                      $scope.form.policies = response;
                    },
                    function(error) {
                      console.log(error);
                    }
                  );
            },
            submit : function($form) {
                $scope.form.submitted = true;
                $scope.form.itemError = false;
                if (!$form.$valid) {
                  $scope.form.error = true;
                  $scope.form.submitted = false;
                  return false;
                }
                Restangular.all("insurance-policy")
                  .post($scope.form.model)
                  .then(
                    function(response) {
                        console.log('FFF', response);
                      $uibModalInstance.close(response);
                    },
                    function(error) {
                      console.log(error);
                      //$uibModalInstance.dismiss();
                    }
                  );

            },
            cancel : function() {
                $uibModalInstance.dismiss();
            },
            close : function() {
                $uibModalInstance.close();
            }

        }
        $scope.form.init();
        $scope.start_date = {
            options: {
              applyClass: "btn-success",
              singleDatePicker: true,
              locale: {
                applyLabel: "Apply",
                fromLabel: "From",
                // format: "YYYY-MM-DD", //will give you 2017-01-06
                format: "D-MMM-YY", //will give you 6-Jan-17
                // format: "D-MMMM-YY", //will give you 6-January-17
                toLabel: "To",
                cancelLabel: "Cancel",
                customRangeLabel: "Custom range"
              }
            }
          };
        $scope.close = function() {
            $uibModalInstance.close();
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss();
        };
    }
  ]);
  
angular.module("myApp").controller("editInsurancePolicyModalCtrl", [
    "$scope",
    "Restangular",
    "data",
    "$uibModalInstance",
    "$q",
    function($scope, Restangular, data, $uibModalInstance, $q) {
        $scope.existingServices = [];
        var id = data.id;
        var insurancePolicy = Restangular.one("insurance-policy", id);

        $scope.form = {
            policies: [],
            startDate : {
                datepickerOption : {
                    options: {
                      applyClass: "btn-success",
                      singleDatePicker: true,
                      locale: {
                        applyLabel: "Apply",
                        fromLabel: "From",
                        // format: "YYYY-MM-DD", //will give you 2017-01-06
                        format: "D-MMM-YY", //will give you 6-Jan-17
                        // format: "D-MMMM-YY", //will give you 6-January-17
                        toLabel: "To",
                        cancelLabel: "Cancel",
                        customRangeLabel: "Custom range"
                      }

                    }
                },
            },
            vendors : [],

            model : {
                id : null,
                vendor_id : null,
                name : null,
                policy_number : null,
                description : null,
                start_date : false,
                end_date : false,
                coverage_amount : null,
                amount_paid : null,
                premium_amount : null,
                applicable_for : 'user',
                accessible_to : 'group',
                parent_id : false,
                sub_group : false,
                has_sub_group : false,
                sub_groups : []
            },
            sub_group : {
                name : null,
                premium_amount : null
            },
            select_vendor: function(data) {
                console.log('VENDOR ID',data.id);
                //$scope.form.selected_vendor = data;
                $scope.form.model.vendor_id = data.id;
              },
            select_policy: function(data) {
                $scope.form.selected_policy = data;
                $scope.form.model.parent_id = $scope.form.selected_policy.id;
              },
            addSubGroup : function() {
                $scope.form.model.sub_groups.push($scope.form.sub_group);
                //$scope.form.itemError = false;
                // $scope.form.item = {
                //   name: null,
                //   unit_price: null,
                //   quantity: null,
                //   total: null
                // };
            },

            init: function() {
                $scope.form.loading = true;
                var vendors = Restangular.all("vendor").getList({'pagination': 0});
                var insurancePolicies = Restangular.all("insurance-policy").getList({'pagination': 0,'sub_group' : true});
                
                var insurancePolicyGet = insurancePolicy.get();
                $q.all([insurancePolicyGet, vendors, insurancePolicies]).then(function(result) {
                  $scope.form.loading = false;
                  if (result[0]) {
                    $scope.form.model = result[0];
                    $scope.form.vendors = result[1];
                    $scope.form.policies = result[2];
                    $scope.form.model.start_date = new Date(result[0].start_date);
                    $scope.form.model.end_date = new Date(result[0].end_date);
                  }
                });
            },
            
            submit : function($form) {
                $scope.form.submitted = true;
                $scope.form.itemError = false;
                if (!$form.$valid) {
                  $scope.form.error = true;
                  $scope.form.submitted = false;
                  return false;
                }
                // Restangular.one("insurance-policy", id)
                //   .put($scope.form.model)
                //   .then(
                //     function(response) {
                //       //$uibModalInstance.close(response);
                //     },
                //     function(error) {
                //       console.log(error);
                //       //$uibModalInstance.dismiss();
                //     }
                //   );
                // insurancePolicy.start_date = $scope.form.model.start_date;
                // insurancePolicy.end_date = $scope.form.model.end_date;
                // insurancePolicy.start_date = $scope.form.model.start_date;
                insurancePolicy.vendor_id = $scope.form.model.vendor_id;
                insurancePolicy.name = $scope.form.model.name;
                insurancePolicy.policy_number = $scope.form.model.policy_number;
                insurancePolicy.start_date = $scope.form.model.start_date;
                insurancePolicy.end_date = $scope.form.model.end_date;
                insurancePolicy.coverage_amount = $scope.form.model.coverage_amount;
                insurancePolicy.amount_paid = $scope.form.model.amount_paid;
                insurancePolicy.premium_amount = $scope.form.model.premium_amount;
                insurancePolicy.accessible_to = $scope.form.model.accessible_to;
                insurancePolicy.applicable_for = $scope.form.model.applicable_for;
                insurancePolicy.put().then(function(response) {
                    $uibModalInstance.close(response);
                }, function(error){
                    console.log('EEE', error);
                })

            },
            cancel : function() {
                $uibModalInstance.dismiss();
            },
            close : function() {
                $uibModalInstance.close();
            }

        }
        // Restangular.one("insurance-policy", id).get().then(function(response) {
        //     console.log('get', response);
        //     $scope.form.model = response;
        //     $scope.form.model.end_date = new Date(response.end_date);
        //     $scope.form.model.start_date = new Date(response.start_date);
        // }, function(errors){
        //     console.log('ERR', errors)
        // });
        
        $scope.form.init();
        $scope.start_date = {
            options: {
              applyClass: "btn-success",
              singleDatePicker: true,
              locale: {
                applyLabel: "Apply",
                fromLabel: "From",
                // format: "YYYY-MM-DD", //will give you 2017-01-06
                format: "D-MMM-YY", //will give you 6-Jan-17
                // format: "D-MMMM-YY", //will give you 6-January-17
                toLabel: "To",
                cancelLabel: "Cancel",
                customRangeLabel: "Custom range"
              }
            }
          };
        $scope.close = function() {
            $uibModalInstance.close();
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss();
        };
    }
  ]);
  
angular.module("myApp").controller("showInsuranceModalCtrl", [
    "$scope",
    "Restangular",
    "data",
    "$uibModalInstance",
    function($scope, Restangular, data, $uibModalInstance) {
        var id = data.id;
        var vendor_services = [];
        $scope.form = {
          loading: false,
          data : {}
        }
        $scope.form.loading = true;
        
        var vendorObj = Restangular.one("insurance-policy", id);
        vendorObj.get().then(
            function(response) {
              $scope.data = response;
              $scope.form.loading = false;
            },
            function(error) {
              console.log("err", error);
            }
          );
        $scope.filteredDate = function (input_date) {
          var formatted_date = new Date(input_date);
          return formatted_date;
        };  
        // Get existing Services name
        $scope.close = function() {
            $uibModalInstance.close();
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss();
        };
    }
  ]);
  
angular.module("myApp").controller("viewInsurancePolicyCtrl", [
  "$scope",
  "Restangular",
  "$uibModal",
  function($scope, Restangular, $uibModal) {
    var id = window.policyId;

    var vendor_services = [];
    $scope.form = {
      loading: false,
      error: false,
      selected_user: [],
      error_message: null,
      user: {
        data: [],
        loading: false
      },
      asset: {
        data: [],
        loading: false
      },
      data: {},
      model: {
        insurance_policy_id: null,
        insurable_id: null,
        insurable_type: null
      },
      select_user: function(data, $index) {
        console.log("D", data, $index);
        $scope.form.data.sub_groups[$index].selected_user = data;
        $scope.form.data.sub_groups[$index].insurable_id = data.id;
        $scope.form.data.sub_groups[$index].insurable_type =
          "App\\Models\\User";
        //$scope.getInsura
      },
      select_asset: function(data) {
        $scope.form.selected_asset = data;
        $scope.form.model.insurable_id = data.id;
        $scope.form.model.insurable_type = "App\\Models\\Admin\\Asset";

        //$scope.getInsura
      },
      submit: function($form, $index) {
        $scope.form.data.sub_groups[$index].submitted = true;
        $scope.form.data.sub_groups[$index].success = false;
        $scope.form.data.sub_groups[$index].error = false;
        $scope.form.data.sub_groups[$index].error_message = null;
        // if (
        //   $scope.form.model.purchase_items[0].name == null ||
        //   $scope.form.model.purchase_items[0].unit_price == null ||
        //   $scope.form.model.purchase_items[0].quantity == null
        // ) {
        //   $scope.form.itemError = true;
        // }
        if (!$form.$valid) {
          $scope.form.data.sub_groups[$index].error = true;
          $scope.form.data.sub_groups[$index].submitted = false;
          return false;
        }
        var params = {
          insurance_policy_id: $scope.form.data.sub_groups[$index].id,
          insurable_id: $scope.form.data.sub_groups[$index].insurable_id,
          insurable_type: $scope.form.data.sub_groups[$index].insurable_type
        };

        // console.log('$scope.form.data.sub_groups[$index]', $scope.form.data.sub_groups[$index]);
        // return false;
        Restangular.all("insurance")
          .post(params)
          .then(
            function(response) {
              //$scope.form.data.insurances.unshift(response);
              $scope.form.data.sub_groups[$index].insurances.push(response);
              $scope.form.data.sub_groups[$index].selected_asset = null;
              $scope.form.data.sub_groups[$index].selected_user = null;
              $scope.form.data.sub_groups[$index].message =
                "Added successfully";
              $scope.form.data.sub_groups[$index].success = true;
            },
            function(error) {
              $scope.form.data.sub_groups[$index].error = true;
              $scope.form.data.sub_groups[$index].error_message =
                error.data.message;
            }
          );
      },
      markAsClose: function(index) {
        if (!confirm("Are you sure you want to close this record?"))
          return false;

        var item = $scope.form.data.insurances[index];
        var obj = Restangular.one("insurance", item.id);
        obj.status = "closed";
        obj.put().then(
          function(response) {
            $scope.form.data.insurances[index] = response;
          },
          function(error) {
            console.log("ERR", error);
          }
        );
        //console.log('markAsClose called',index);
      },
      deleteRecord: function(index, subIndex) {
        if (!confirm("Are you sure you want to delete this record?"))
          return false;

        var item = $scope.form.data.sub_groups[index].insurances[subIndex];
        // console.log("deleteRecord called", index, subIndex, item);
        // return false;

        var response = Restangular.one("insurance", item.id)
          .remove()
          .then(
            function(response) {
              $scope.form.data.sub_groups[index].insurances.splice(subIndex, 1);
            },
            function(error) {
              console.log("ERR", error);
            }
          );
      },
      edit: function($index) {
        var id = $scope.form.data.sub_groups[$index].id;
        var modal = $uibModal.open({
          controller: "editInsurancePolicyModalCtrl",
          windowClass: "bootstrap_wrap",
          templateUrl: "/views/insurance-policy/create.html",
          backdrop: "static",
          size: "lg",
          resolve: {
            data: function() {
              return { id: id };
            }
          }
        });
        modal.result.then(
          function(result) {
            $scope.form.data.sub_groups[$index] = result;
          },
          function() {
            //cancel
          }
        );
      },
      downloadReport: function($index, $subIndex) {
        //var users = $scope.form.data.sub_groups[$index].insurances;
        var users = $scope.form.data.sub_groups[$index].insurances;
        console.log("downloadReport", $index, users);
        //return false;
        var name = $scope.form.data.sub_groups[$index].name;
        var policyNumber = $scope.form.data.sub_groups[$index].policy_number;
        var coverageAmount =
          $scope.form.data.sub_groups[$index].coverage_amount;
        var premiumAmount = $scope.form.data.sub_groups[$index].premium_amount;
        var type = $scope.form.data.sub_groups[$index].applicable_for;
        var data = []; //[{'Insurance Name','Policy Number','Coverage','Name','Employee ID','Amount'}];
        if (type == "user") {
          angular.forEach(users, function(res) {
            data.push({
              InsuranceName: name,
              PolicyNumber: policyNumber,
              Coverage: coverageAmount,
              EmployeeName: res.insurable ? res.insurable.name : null,
              EID: res.insurable ? res.insurable.employee_id : null,
              "Premium(per/month)": premiumAmount
            });
          });
        } else {
          angular.forEach(users, function(res) {
            data.push({
              InsuranceName: name,
              PolicyNumber: policyNumber,
              Coverage: coverageAmount,
              Name: res.insurable.name,
              "Premium(per/month)": premiumAmount
            });
          });
        }

        var a = document.createElement("a");
        var csv = Papa.unparse(data);
        if (window.navigator.msSaveOrOpenBlob) {
          var blob = new Blob([decodeURIComponent(encodeURI(json_pre))], {
            type: "text/csv;charset=utf-8;"
          });
          navigator.msSaveBlob(blob, name + ".csv");
        } else {
          a.href = "data:attachment/csv;charset=utf-8," + encodeURI(csv);
          a.target = "_blank";
          a.download = name + ".csv";
          document.body.appendChild(a);
          a.click();
        }
        //}

        // console.log('ID', id);
        // Restangular.one('insurance-policy/download', id).get().then(function(response){
        //     //$scope.form.data.insurances.splice(index, 1);
        //     console.log('DDD', response);
        // }, function(error){
        //   console.log('ERR', error);
        // })
      }
    };
    $scope.form.loading = true;

    var vendorObj = Restangular.one("insurance-policy", id);
    vendorObj.get().then(
      function(response) {
        $scope.form.data = response;
        $scope.form.model.insurance_policy_id = response.id;
        $scope.form.loading = false;
      },
      function(error) {
        console.log("err", error);
      }
    );
    $scope.filteredDate = function(input_date) {
      var formatted_date = new Date(input_date);
      return formatted_date;
    };
    // Get existing Services name
    $scope.close = function() {
      $uibModalInstance.close();
    };

    $scope.cancel = function() {
      $uibModalInstance.dismiss();
    };

    $scope.$watch("form.data.applicable_for", function() {
      if ($scope.form.data && $scope.form.data.applicable_for == "user") {
        $scope.form.user.loading = true;
        Restangular.all("user")
          .getList({ is_active: 1, pagination: 0 })
          .then(
            function(response) {
              $scope.form.user.data = response;
              $scope.form.user.loading = false;
            },
            function(error) {
              $scope.form.user.loading = false;
              console.log("ERR", error);
            }
          );
      } else if (
        $scope.form.data &&
        $scope.form.data.applicable_for == "asset"
      ) {
        $scope.form.asset.loading = true;
        Restangular.all("asset")
          .getList({ pagination: 0 })
          .then(
            function(response) {
              $scope.form.asset.loading = false;
              $scope.form.asset.data = response;
            },
            function(error) {
              $scope.form.asset.loading = false;
              console.log("ERR", error);
            }
          );
      }
    });
  }
]);

angular.module('myApp')
    .controller('insuranceIndexController',["$scope", "Restangular", "$uibModal" ,"$route", function ($scope, Restangular, $uibModal, $route) {
      $scope.model = {
        data : [],
        loading : true,
        selectAll : function(){
          var items = [];
        },
        
        getData : function(status = ''){
          var params = {pagination : 0,'parent_id' : null};
          
          Restangular.all('insurance').getList(params).then(function(response){
            $scope.model.data = response;
            $scope.model.loading = false;
          }, function(error){
            $scope.model.loading = false;
          });    
        },
        addInsurance : function() {
          var modal = $uibModal.open({
          controller: "createInsuranceModalCtrl",
          windowClass: "bootstrap_wrap",
          templateUrl: "/views/insurance/create.html",
          backdrop: "static",
          size: "lg",
          resolve: {
            data: function () {
              return {};
            }
          }
        });

        modal.result.then(
          function (result) {
            //window.location.reload();
            $route.reload();
          },
          function () {
            //cancel
          }
        );
        },
        addSubGroup : function() {
          console.log('addSubGroup called');

        },
        view :function(id) {
          var modal = $uibModal.open({
          controller: "createInsurancePolicyModalCtrl",
          windowClass: "bootstrap_wrap",
          templateUrl: "/views/insurance/create.html",
          backdrop: "static",
          size: "lg",
          resolve: {
            data: function () {
              return {};
            }
          }
        });

        modal.result.then(
          function (result) {
            //window.location.reload();
          },
          function () {
            //cancel
          }
        );

        },
        edit :function(id) {
          var modal = $uibModal.open({
          controller: "editInsurancePolicyModalCtrl",
          windowClass: "bootstrap_wrap",
          templateUrl: "/views/insurance/create.html",
          backdrop: "static",
          size: "lg",
          resolve: {
            data: function () {
              return { id : id};
            }
          }
        });

        modal.result.then(
          function (result) {
            //window.location.reload();
          },
          function () {
            //cancel
          }
        );
        },
      },
     
      $scope.model.getData();
}]);
angular.module("myApp").controller("createInsuranceModalCtrl", [
    "$scope",
    "Restangular",
    "data",
    "$uibModalInstance",
    function($scope, Restangular, data, $uibModalInstance) {
        $scope.options = {
            formatYear: 'yy',
            maxDate: new Date(2020, 5, 22),
            minDate: new Date(),
        };

        $scope.form = {
            error : false,
            error_message : null,
            user : {
                data : [],
                loading : false
            },
            asset : {
                data : [],
                loading : false
            },
            users : [],
            assets : [],
            startDate : {
                datepickerOption : {
                    options: {
                      applyClass: "btn-success",
                      singleDatePicker: true,
                      locale: {
                        applyLabel: "Apply",
                        fromLabel: "From",
                        // format: "YYYY-MM-DD", //will give you 2017-01-06
                        format: "D-MMM-YY", //will give you 6-Jan-17
                        // format: "D-MMMM-YY", //will give you 6-January-17
                        toLabel: "To",
                        cancelLabel: "Cancel",
                        customRangeLabel: "Custom range"
                      }
                    }
                },
            },
            insurance_policies : [],
            model : {
                type : null,
                insurance_policy_id : null,
                insurable_id : null,
                insurable_type : null
            },
            sub_group : {
                name : null,
                premium_amount : null
            },
            select_policy: function(data) {
                $scope.form.selected_policy = data;
                $scope.form.model.insurance_policy_id = $scope.form.selected_policy.id;

                //$scope.getInsura
              },
          select_user: function(data) {
            $scope.form.selected_user = data;
            $scope.form.model.insurable_id = data.id;
            $scope.form.model.insurable_type = 'App\\Models\\User';
            //$scope.getInsura
          },
          select_asset: function(data) {
            $scope.form.selected_asset = data;
            $scope.form.model.insurable_id = data.id;
            $scope.form.model.insurable_type = 'App\\Models\\Admin\\Asset';

            //$scope.getInsura
          },
            addSubGroup : function() {
                $scope.form.model.sub_groups.push($scope.form.sub_group);
                //$scope.form.itemError = false;
                // $scope.form.item = {
                //   name: null,
                //   unit_price: null,
                //   quantity: null,
                //   total: null
                // };
            },
            init: function() {
                $scope.form.loading = true;
                Restangular.all("insurance-policy")
                  .getList({'pagination': 0})
                  .then(
                    function(response) {
                      $scope.form.loading = false;
                      $scope.form.insurance_policies = response;
                    },
                    function(error) {
                      console.log(error);
                    }
                  );
            },
            submit : function($form) {
                console.log('submit called');
                $scope.form.submitted = true;
                $scope.form.itemError = false;
                // if (
                //   $scope.form.model.purchase_items[0].name == null ||
                //   $scope.form.model.purchase_items[0].unit_price == null ||
                //   $scope.form.model.purchase_items[0].quantity == null
                // ) {
                //   $scope.form.itemError = true;
                // }
                if (!$form.$valid) {
                    console.log('INVALID FORM');
                  $scope.form.error = true;
                  $scope.form.submitted = false;
                  return false;
                }
                console.log('VALID FORM');
                Restangular.all("insurance")
                  .post($scope.form.model)
                  .then(
                    function(response) {
                        $uibModalInstance.close(response);
                    },
                    function(error) {
                        $scope.form.error = true;
                        $scope.form.error_message = error.data.message;
                      console.log(error.data);
                      //$uibModalInstance.dismiss();
                    }
                  );

            },
            cancel : function() {
                $uibModalInstance.dismiss();
            },
            close : function() {
                $uibModalInstance.close();
            }

        }
        $scope.form.init();
        $scope.start_date = {
            options: {
              applyClass: "btn-success",
              singleDatePicker: true,
              locale: {
                applyLabel: "Apply",
                fromLabel: "From",
                // format: "YYYY-MM-DD", //will give you 2017-01-06
                format: "D-MMM-YY", //will give you 6-Jan-17
                // format: "D-MMMM-YY", //will give you 6-January-17
                toLabel: "To",
                cancelLabel: "Cancel",
                customRangeLabel: "Custom range"
              }
            }
          };
        $scope.close = function() {
            $uibModalInstance.close();
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss();
        };
        $scope.$watch('form.selected_policy.applicable_for', function() {
            if($scope.form.selected_policy && $scope.form.selected_policy.applicable_for == 'user') {
                $scope.form.user.loading =  true;
                Restangular.all('user').getList({pagination : 0}).then(function(response){
                    $scope.form.user.data =  response;
                    $scope.form.user.loading =  false;
                }, function(error){
                    $scope.form.user.loading =  false;
                    console.log('ERR', error);
                })
            }else if($scope.form.selected_policy && $scope.form.selected_policy.applicable_for == 'asset') {
                $scope.form.asset.loading =  true;
                Restangular.all('asset').getList({pagination : 0}).then(function(response){
                    $scope.form.asset.loading =  false;
                    $scope.form.asset.data =  response;
                }, function(error){
                    $scope.form.asset.loading =  false;
                    console.log('ERR', error);
                })
            }
        });
    }

  ]);
  
angular.module("myApp").controller("editInsuranceModalCtrl", [
    "$scope",
    "Restangular",
    "data",
    "$uibModalInstance",
    function($scope, Restangular, data, $uibModalInstance) {

    }
  ]); 
angular.module("myApp").controller("showInsuranceModalCtrl", [
    "$scope",
    "Restangular",
    "data",
    "$uibModalInstance",
    function($scope, Restangular, data, $uibModalInstance) {
        var id = data.id;
        var vendor_services = [];
        $scope.form = {
          loading: false,
          data : {}
        }
        $scope.form.loading = true;
        
        var vendorObj = Restangular.one("insurance-policy", id);
        vendorObj.get().then(
            function(response) {
              $scope.data = response;
              $scope.form.loading = false;
            },
            function(error) {
              console.log("err", error);
            }
          );
        $scope.filteredDate = function (input_date) {
          var formatted_date = new Date(input_date);
          return formatted_date;
        };  
        // Get existing Services name
        $scope.close = function() {
            $uibModalInstance.close();
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss();
        };
    }
  ]);
  
angular.module('myApp')
    .controller("loanDeductionController", ["$scope", "Restangular", "$uibModal", function ($scope, Restangular, $uibModal) {
        var monthId = window.monthId;
        
        console.log('LOAN deduction', monthId);
        $scope.model = {
            monthId : monthId,
            loading : false,
            data : {},

            getData : function(monthId) {
                $scope.model.loading = true;
                var params = {monthId : monthId};
                Restangular.all("loan/deduction")
                .getList({month_id : monthId})
                .then(function (response) {
                    $scope.model.loading = false;
                    console.log('response', response);
                    ///$scope.model.data = response;
                }, function (error) {
                    $scope.model.loading = false;
                    console.log('response', response);
                });    
            }
            
        };
        
        $scope.model.getData(monthId);
    }]);
"use strict";

/*Directives*/
angular.module("myApp").directive("workshop", [
  function() {
    return {
      templateUrl: "/scripts/directives/workshop/index.html",
      restrict: "E",
      replace: true,
      scope: {
        userId: "=userid"
      },
      controller: [
        "$scope",
        "Restangular",
        function($scope, Restangular) {
          $scope.form = {
            loading: false,
            error: false,
            message: null,
            submitted: false,
            data: [],
            checkbox: null,
            model: {
              title: null,
              description: null,
              user_id: $scope.userId,
              message: null
            },
            errors: {
              error: false,
              title: false,
              description: false,
              error_message: null
            },
            getMeta: function() {},
            checkboxUpdate: function() {
              var params = {
                data: $scope.form.model
              };

              var obj = Restangular.one(
                "user-visibility-setting",
                $scope.form.model.user_id
              );
              obj.status = $scope.form.checkbox ? "0" : "1";
              obj.title = "workshop";
              obj.put().then(
                function(response) {
                  $scope.form.checkbox = response[0].workshop ? true : false;
                },
                function(error) {
                  $scope.form.loading = false;
                  $scope.form.errors.error_message = errors.data.message;
                  console.log("ERR", error);
                }
              );
            },
            init: function() {
              console.log("WORK", $scope.form.model.user_id);
              $scope.form.loading = true;
              Restangular.one("user-visibility-setting", $scope.form.model.user_id)
                .get()
                .then(
                  function(response) {
                    $scope.form.checkbox = response.workshop ? true : false;
                    
                  },
                  function(errors) {
                    $scope.form.loading = false;
                    $scope.form.errors.error_message = errors.data.message;
                  }
                );

              Restangular.one("workshop", $scope.form.model.user_id)
                .get()
                .then(
                  function(response) {
                    $scope.form.loading = false;
                    $scope.form.data = response;
                    console.log($scope.form.data[0].id);
                  },
                  function(errors) {
                    $scope.form.loading = false;
                    $scope.form.errors.error_message = errors.data.message;
                    console.log(error);
                  }
                );
            },

            submit: function($form) {
              $scope.form.errors.error = false;
              if ($scope.form.model.title == null) {
                $scope.form.errors.title = true;
                $scope.form.errors.error = true;
              }
              if ($scope.form.model.description == null) {
                $scope.form.errors.description = true;
                $scope.form.errors.error = true;
              }
              if ($scope.form.errors.error) {
                return;
              }
              var params = {
                data: $scope.form.model
              };
              Restangular.all("workshop")
                .post(params)
                .then(
                  function(response) {
                    $scope.form.loading = false;
                    $scope.form.model.title = null;
                    $scope.form.model.description = null;
                    $scope.form.data.push(response);
                    console.log($scope.form.data);
                  },
                  function(errors) {
                    $scope.form.loading = false;
                    $scope.form.errors.error_message = errors.data.message;
                    console.log(errors);
                  }
                );
            },
            delete: function(obj) {
              console.log(obj);
              console.log(obj.currentTarget.attributes.data.nodeValue);

              var params = {
                data: $scope.form.model
              };
              Restangular.one(
                "workshop",
                obj.currentTarget.attributes.data.nodeValue
              )
                .remove()
                .then(
                  function(response) {
                    delete $scope.form.data[
                      obj.currentTarget.attributes.data.ownerElement.id
                    ];
                  },
                  function(errors) {
                    $scope.form.loading = false;
                    $scope.form.errors.error_message = errors.data.message;
                    console.log(errors);
                  }
                );
            }
          };
          $scope.form.init();
        }
      ]
    };
  }
]);

"use strict";

/*Directives*/
angular.module("myApp").directive("miniproject", [
  function() {
    return {
      templateUrl: "/scripts/directives/miniproject/index.html?v1.2",
      restrict: "E",
      replace: true,
      scope: {
        userId: "=userid"
      },
      controller: [
        "$scope",
        "Restangular",
        function($scope, Restangular) {
          console.log("miniProject called");
          $scope.form = {
            loading: false,
            error: false,
            message: null,
            submitted: false,
            data: [],
            checkbox: null,
            model: {
              title: null,
              description: null,
              user_id: $scope.userId,
              message: null
            },
            errors: {
              error: false,
              title: false,
              description: false,
              error_message: null
            },
            getMeta: function() {},
            checkboxUpdate: function() {
              var params = {
                data: $scope.form.model
              };

              var obj = Restangular.one(
                "user-visibility-setting",
                $scope.form.model.user_id
              );
              obj.status = $scope.form.checkbox ? "0" : "1";
              obj.title = "miniproject";
              obj.put().then(
                function(response) {
                  $scope.form.checkbox = response.mini_project
                    ? true
                    : false;
                },
                function(error) {
                  $scope.form.loading = false;
                  $scope.form.errors.error_message = errors.data.message;
                  console.log("ERR", error);
                }
              );
            },
            init: function() {
              $scope.form.loading = true;
              $scope.form.getMeta();
              Restangular.one(
                "user-visibility-setting",
                $scope.form.model.user_id
              )
                .get()
                .then(
                  function(response) {
                    $scope.form.checkbox = response.mini_project
                      ? true
                      : false;
                  },
                  function(errors) {
                    $scope.form.loading = false;
                    $scope.form.errors.error_message = errors.data.message;
                    console.log(error);
                  }
                );

              Restangular.one("miniproject", $scope.form.model.user_id)
                .get()
                .then(
                  function(response) {
                    $scope.form.loading = false;
                    $scope.form.data = response;
                  },
                  function(errors) {
                    $scope.form.loading = false;
                    $scope.form.errors.error_message = errors.data.message;
                    console.log(error);
                  }
                );
            },

            submit: function($form) {
              $scope.form.errors.error = false;
              if ($scope.form.model.title == null) {
                $scope.form.errors.title = true;
                $scope.form.errors.error = true;
              }
              if ($scope.form.model.description == null) {
                $scope.form.errors.description = true;
                $scope.form.errors.error = true;
              }
              if ($scope.form.errors.error) {
                return;
              }
              var params = {
                data: $scope.form.model
              };
              Restangular.all("miniproject")
                .post(params)
                .then(
                  function(response) {
                    $scope.form.loading = false;
                    $scope.form.model.title = null;
                    $scope.form.model.description = null;
                    $scope.form.data.push(response);
                    console.log($scope.form.data);
                  },
                  function(errors) {
                    $scope.form.loading = false;
                    $scope.form.errors.error_message = errors.data.message;
                    console.log(errors);
                  }
                );
            },
            delete: function(obj) {
              console.log(obj);
              console.log(obj.currentTarget.attributes.data.nodeValue);

              var params = {
                data: $scope.form.model
              };
              Restangular.one(
                "miniproject",
                obj.currentTarget.attributes.data.nodeValue
              )
                .remove()
                .then(
                  function(response) {
                    delete $scope.form.data[
                      obj.currentTarget.attributes.data.ownerElement.id
                    ];
                  },
                  function(errors) {
                    $scope.form.loading = false;
                    $scope.form.errors.error_message = errors.data.message;
                    console.log(errors);
                  }
                );
            }
          };
          $scope.form.init();
        }
      ]
    };
  }
]);

"use strict";

/*Directives*/
angular.module("myApp").directive("socialinfo", [
  function() {
    return {
      templateUrl: "/scripts/directives/social-info/index.html",
      restrict: "E",
      replace: true,
      scope: {
        userId: "=userid"
      },
      controller: [
        "$scope",
        "Restangular",
        function($scope, Restangular) {
          $scope.social_info = [
            {
              type: "email",
              icon: "fa fa-envelope",
              value: "",
              visibility: false
            },
            {
              type: "phone",
              icon: "fa fa-phone",
              value: "",
              visibility: false
            },
            {
              type: "skype",
              icon: "fa fa-skype",
              value: "",
              visibility: false
            },
            {
              type: "github",
              icon: "fa fa-github",
              value: "",
              visibility: false
            },
            {
              type: "stackoverflow",
              icon: "fa fa fa-stack-overflow",
              value: "",
              visibility: false
            },
            {
              type: "linkedin",
              icon: "fa fa-linkedin",
              value: "",
              visibility: false
            },
            {
              type: "twitter",
              icon: "fa fa-twitter",
              value: "",
              visibility: false
            },
            {
              type: "facebook",
              icon: "fa fa-facebook",
              value: "",
              visibility: false
            }
          ];
          console.log("social", $scope.social_info);
          $scope.form = {
            loading: false,
            error: false,
            message: null,
            submitted: false,
            data: [],
            checkbox: null,
            model: {
              title: null,
              description: null,
              user_id: $scope.userId,
              message: null,
              icon: ""
            },
            errors: {
              error: false,
              title: false,
              description: false,
              error_message: null
            },
            getMeta: function() {},
            checkboxUpdate: function() {
              var params = {
                data: $scope.form.model
              };

              var obj = Restangular.one(
                "user-visibility-setting",
                $scope.form.model.user_id
              );
              obj.status = $scope.form.checkbox ? "0" : "1";
              obj.title = "social-info";
              obj.put().then(
                function(response) {
                  $scope.form.checkbox = response.social_info ? true : false;
                },
                function(error) {
                  $scope.form.loading = false;
                  $scope.form.errors.error_message = errors.data.message;
                  console.log("ERR", error);
                }
              );
            },
            init: function() {
              $scope.form.loading = true;
              $scope.form.getMeta();
              Restangular.one(
                "user-visibility-setting",
                $scope.form.model.user_id
              )
                .get()
                .then(
                  function(response) {
                    $scope.form.checkbox = response.social_info
                      ? true
                      : false;
                  },
                  function(errors) {
                    $scope.form.loading = false;
                    $scope.form.errors.error_message = errors.data.message;
                    console.log(error);
                  }
                );

              Restangular.one("social-info", $scope.form.model.user_id)
                .get()
                .then(
                  function(response) {
                    $scope.form.loading = false;
                    if (JSON.parse(response[0].social_json))
                      $scope.social_info = JSON.parse(response[0].social_json);
                    $scope.checkstatus = new Array(
                      $scope.social_info.length
                    ).fill("fa fa-check");
                    $scope.checkvisibility = new Array(
                      $scope.social_info.length
                    ).fill("invisible");
                  },
                  function(errors) {
                    $scope.form.loading = false;
                    $scope.form.errors.error_message = errors.data.message;
                    console.log(error);
                  }
                );
            },

            submit: function($index) {
              $scope.checkvisibility[$index] = "invisible";
              $data = JSON.stringify($scope.social_info);
              $id = $scope.form.model.user_id;
              console.log("140", $data);
              var params = {
                data: $data,
                id: $id
              };
              console.log("params", params);
              Restangular.all("social-info")
                .post(params)
                .then(
                  function(response) {
                    response.status
                      ? ($scope.checkstatus[$index] = "fa fa-check")
                      : ($scope.checkstatus[$index] = "fa fa-exclamation");
                    $scope.checkvisibility[$index] = "visible";
                    $scope.form.loading = false;
                    $scope.form.model.social_json = null;
                    // $scope.form.model.description = null;
                    $scope.form.data.push(response);
                    console.log($scope.form.data);
                  },
                  function(errors) {
                    $scope.form.loading = false;
                    $scope.form.errors.error_message = errors.data.message;
                    console.log(errors);
                  }
                );
            }
          };
          $scope.form.init();
        }
      ]
    };
  }
]);

"use strict";

/*Directives*/
angular.module("myApp").directive("rnd", [
  function() {
    return {
      templateUrl: "/scripts/directives/rnd/index.html?v1.2",
      restrict: "E",
      replace: true,
      scope: {
        userId: "="
      },
      controller: [
        "$scope",
        "Restangular",
        function($scope, Restangular) {
          $scope.form = {
            loading: false,
            error: false,
            message: null,
            submitted: false,
            data: [],
            checkbox: null,
            model: {
              title: null,
              description: null,
              user_id: null,
              message: null
            },
            errors: {
              error: false,
              title: false,
              description: false,
              error_message: null
            },
            getMeta: function() {},
            checkboxUpdate: function() {
              var params = {
                data: $scope.form.model
              };

              var obj = Restangular.one(
                "user-visibility-setting",
                $scope.form.model.user_id
              );
              obj.status = $scope.form.checkbox ? "0" : "1";
              obj.title = "rnd";
              obj.put().then(
                function(response) {
                  $scope.form.checkbox = response.r_and_d ? true : false;
                },
                function(error) {
                  $scope.form.loading = false;
                  $scope.form.errors.error_message = errors.data.message;
                  console.log("ERR", error);
                }
              );
            },
            init: function() {
              $scope.form.loading = true;
              $scope.form.getMeta();
              Restangular.one(
                "user-visibility-setting",
                $scope.form.model.user_id
              )
                .get()
                .then(
                  function(response) {
                    $scope.form.checkbox = response.r_and_d ? true : false;
                    console.log(response.rnd);
                  },
                  function(errors) {
                    $scope.form.loading = false;
                    $scope.form.errors.error_message = errors.data.message;
                    console.log(error);
                  }
                );

              Restangular.one("rnd", $scope.form.model.user_id)
                .get()
                .then(
                  function(response) {
                    $scope.form.loading = false;
                    $scope.form.data = response;
                  },
                  function(errors) {
                    $scope.form.loading = false;
                    $scope.form.errors.error_message = errors.data.message;
                    console.log(error);
                  }
                );
            },

            submit: function($form) {
              $scope.form.errors.error = false;
              if ($scope.form.model.title == null) {
                $scope.form.errors.title = true;
                $scope.form.errors.error = true;
              }
              if ($scope.form.model.description == null) {
                $scope.form.errors.description = true;
                $scope.form.errors.error = true;
              }
              if ($scope.form.errors.error) {
                return;
              }
              var params = {
                data: $scope.form.model
              };
              Restangular.all("rnd")
                .post(params)
                .then(
                  function(response) {
                    $scope.form.loading = false;
                    $scope.form.model.title = null;
                    $scope.form.model.description = null;
                    $scope.form.data.push(response);
                    console.log($scope.form.data);
                  },
                  function(errors) {
                    $scope.form.loading = false;
                    $scope.form.errors.error_message = errors.data.message;
                    console.log(errors);
                  }
                );
            },
            delete: function(obj) {
              console.log(obj);
              console.log(obj.currentTarget.attributes.data.nodeValue);

              var params = {
                data: $scope.form.model
              };
              Restangular.one(
                "rnd",
                obj.currentTarget.attributes.data.nodeValue
              )
                .remove()
                .then(
                  function(response) {
                    delete $scope.form.data[
                      obj.currentTarget.attributes.data.ownerElement.id
                    ];
                  },
                  function(errors) {
                    $scope.form.loading = false;
                    $scope.form.errors.error_message = errors.data.message;
                  }
                );
            }
          };
          $scope.form.init();
        }
      ]
    };
  }
]);

"use strict()";

/*Directives*/
angular.module("myApp").directive("profileInfo", [
  function() {
    return {
      restrict: "E",
      templateUrl: "/scripts/directives/profile-builder-info/index.html",
      replace: true,
      scope: {
        userId: "=userid"
      },
      controller: [
        "$scope",
        "Restangular",
        "FileUploader",
        function($scope, Restangular, FileUploader) {
          var define = " sad";
          $scope.locationlist = [
            {
              value: "London, UK",
              location: "GeekyAnts - London, UK"
            },
            {
              value: "Bangalore, India",
              location: "GeekyAnts - Bangalore, India"
            }
          ];
          $scope.pathlist = [
            {
              path:
                "resources/views/pages/admin/profile-builder/pages/founder.blade.php",
              name: "Founder"
            },
            {
              path:
                "resources/views/pages/admin/profile-builder/pages/tech-leads-architect.blade.php",
              name: "Tech Leads & Architect"
            },
            {
              path:
                "resources/views/pages/admin/profile-builder/pages/developers-designers.blade.php",
              name: "Developers & Designers"
            },
            {
              path:
                "resources/views/pages/admin/profile-builder/pages/other-team.blade.php",
              name: "Other Team"
            }
          ];
          $scope.userSelect = null;
          $scope.pathSelect = null;
          $scope.profile_id = null;

          $scope.form = {
            loading: false,
            title: null,
            page_title: null,
            error: false,
            message: null,
            id: null,
            index: null,
            submitted: false,
            selected_project: null,
            checkbox: null,
            model: {
              selectedProject: null,
              user_id: $scope.userId,
              message: null,
              project_id: null
            },
            errors: {
              error: false,
              error_message: null
            },
            getMeta: function() {},
            getImage: function() {
              $scope.form.init();
            },
            infoUpdate: function() {
              console.log("infoUpdate called");
              var obj = Restangular.one(
                "profile-info",
                $scope.form.model.user_id
              );
              obj.title = $scope.form.title;
              obj.location = $scope.userSelect;
              obj.page_title = $scope.form.page_title;
              obj.page_path = $scope.pathSelect;
              obj.put().then(
                function(response) {
                  console.log('RE', response);
                  $scope.form.title = response[0]["title"];
                  $scope.form.page_title = response[0]["page_title"];
                  $scope.userSelect = response[0]["location"];
                  $scope.pathSelect = response[0]["page_path"];
                },
                function(error) {
                  $scope.form.loading = false;
                  $scope.form.errors.error_message = errors.data.message;
                  console.log("ERR", error);
                }
              );
            },

            checkboxUpdate: function() {
              var obj = Restangular.one(
                "user-visibility-setting",
                $scope.form.model.user_id
              );
              obj.status = $scope.form.checkbox ? "0" : "1";
              obj.title = "projects";
              obj.put().then(
                function(response) {
                  $scope.form.checkbox = response.projects ? true : false;
                },
                function(error) {
                  $scope.form.loading = false;
                  $scope.form.errors.error_message = errors.data.message;
                  console.log("ERR", error);
                }
              );
            },
            init: function() {
              $scope.form.loading = true;
              $scope.form.getMeta();
              Restangular.one(
                "user-visibility-setting",
                $scope.form.model.user_id
              )
                .get()
                .then(
                  function(response) {
                    $scope.form.checkbox = response.projects ? true : false;
                  },
                  function(errors) {
                    $scope.form.loading = false;
                    $scope.form.errors.error_message = errors.data.message;
                    console.log(error);
                  }
                );
              Restangular.one("profile-info", $scope.form.model.user_id)
                .get()
                .then(
                  function(response) {
                    console.log('title', response);
                    $scope.form.title = response.title;
                    $scope.form.page_title = response.page_title;
                    $scope.userSelect = response.location;
                    $scope.pathSelect = response.page_path;
                    $scope.profile_id = response.id;
                    $scope.form.src =
                      "http://" +
                      window.location.hostname +
                      "/geekyants-portal.local.geekydev.com/storage/ProfilePicture/default.jpeg";
                    if (response[0].profile[0]) {
                      $scope.form.src =
                        "http://" +
                        window.location.hostname +
                        "/geekyants-portal.local.geekydev.com/" +
                        response[0].profile[0]["path"];
                    }
                  },
                  function(errors) {
                    $scope.form.loading = false;
                    $scope.form.errors.error_message = errors.data.message;
                    console.log(error);
                  }
                );
            }
          };
          $scope.form.init();
          $scope.uploader = new FileUploader({
            url: "/api/v1/file",
            autoUpload: true
          });
        }
      ]
    };
  }
]);

"use strict()";

/*Directives*/
angular.module("myApp").directive("tech-stack", [
  function() {
    return {
      templateUrl: "/scripts/directives/user-profile/tech-stack/index.html",
      restrict: "E",
      replace: true,
      scope: {
        assetId: "="
      },
      controller: [
        "$scope",
        "Restangular",
        function($scope, Restangular) {
          var api = "skill";

          $scope.form = {
            loading: false,
            error: false,
            message: null,
            submitted: false,
            data: [],
            keys: [],
            model: {
              asset_id: $scope.assetId,
              key: null,
              value: null,
              validValue: true
            },
            getMeta: function() {
              Restangular.all("asset-meta/key")
                .getList()
                .then(
                  function(response) {
                    $scope.form.keys = response;
                  },
                  function(error) {
                    console.log("error", error);
                  }
                );
            },
            init: function() {
              $scope.form.loading = true;
              $scope.form.getMeta();
              Restangular.all(api)
                .getList({
                  asset_id: $scope.assetId,
                  pagination: 0,
                  per_page: 100
                })
                .then(
                  function(response) {
                    $scope.form.loading = false;
                    // angular.forEach(response, function(item) {
                    //   item.loading = false;
                    //   item.editing = false;
                    //   item.update = function() {
                    //     item.loading = true;
                    //     item.put().then(
                    //       function(response) {
                    //         console.log("updated ", response);
                    //         item.loading = false;
                    //         item.editing = false;
                    //       },
                    //       function(error) {
                    //         item.loading = false;
                    //         console.log("ERROR updating asset-meta", error);
                    //       }
                    //     );
                    //   };
                    //   item.enableEditing = function() {
                    //     item.editing = true;
                    //     return false;
                    //   };
                    // });
                    $scope.form.data = response;
                  },
                  function(error) {
                    $scope.form.loading = false;
                    console.log(error);
                  }
                );
            },
            update: function(item) {
              item.enableEditing = !item.enableEditing;
              console.log("item update");
              item.loading = false;
              item.editing = true;
              item.update = function() {
                item.loading = true;
                item.error = false;
                item.message = null;
                item.put().then(
                  function(response) {
                    $scope.form.getMeta();
                    item.loading = false;
                    item.editing = false;
                  },
                  function(error) {
                    item.loading = false;
                    item.error = true;
                    item.loading = false;
                    item.message = error.data.message;
                    console.log("ERROR updating asset-meta", error);
                  }
                );
              };
            },
            remove: function(itemObj, $index) {
              var confirmMess = confirm(
                "Are you sure want to delete this entry?"
              );
              if (confirmMess) {
                itemObj.remove().then(
                  function(response) {
                    $scope.form.data.splice($index, 1);
                  },
                  function(error) {
                    console.log("error", error);
                  }
                );
              }
            },
            resetForm: function() {
              $scope.form.model.key = null;
              $scope.form.model.value = null;
              $scope.form.submitted = false;
            },
            checkValidMac: function(value) {
              var regex = /^(([A-Fa-f0-9]{2}[:]){5}[A-Fa-f0-9]{2}[,]?)+$/i;
              return regex.test(value);
            },
            submit: function($form) {
              $scope.form.error = false;
              $scope.form.submitted = true;
              if ($form.$invalid) return false;
              $scope.form.model.validValue = true;

              if (
                ($scope.form.model.key == "LAN-MAC" ||
                  $scope.form.model.key == "WIFI-MAC") &&
                $scope.form.checkValidMac($scope.form.model.value) == false
              ) {
                console.log("checkValidMac false");
                $scope.form.model.validValue = false;
                return false;
              }

              Restangular.all("asset-meta")
                .post($scope.form.model)
                .then(
                  function(response) {
                    $scope.form.data.unshift(response);
                    $scope.form.resetForm();
                    $scope.form.getMeta();
                  },
                  function(error) {
                    $scope.form.error = true;
                    $scope.form.message = error.data.message;
                    console.log("error", error.data.message);
                  }
                );
            }
          };
          $scope.form.init();

          // $scope.newRequiredProjectLinks = [];
          // $scope.oldProjectLinks = [];
          // $scope.editable = [];
          // $scope.error = false;
          // $scope.nameError = false;
          // $scope.pathError = false;
          // $scope.newProjectLinkObj = {
          //   name: "",
          //   path: ""
          // };
          // $scope.loadRequiredProjectLinks = function() {
          //   $scope.oldProjectLinks = [];
          //   $scope.listLoading = true;
          //   Restangular.all("project-link")
          //     .customGET("", {
          //       project_id: $scope.projectId
          //     })
          //     .then(
          //       function(response) {
          //         $scope.listLoading = false;
          //         for (i = 0; i < response.length; i++) {
          //           $scope.oldProjectLinks.push(response[i]);
          //         }
          //       },
          //       function(error) {
          //         console.log(error);
          //       }
          //     );
          // };
          // $scope.loadRequiredProjectLinks();

          // $scope.addProjectLinkRow = function() {
          //   $scope.error = false;
          //   if (!$scope.newProjectLinkObj.name) {
          //     $scope.nameError = true;
          //     $scope.error = true;
          //   } else {
          //     $scope.nameError = false;
          //   }

          //   if (!$scope.newProjectLinkObj.path) {
          //     $scope.pathError = true;
          //     $scope.error = true;
          //   } else {
          //     $scope.pathError = false;
          //   }

          //   if ($scope.error) {
          //     return;
          //   }

          //   var params = {
          //     project_id: $scope.projectId,
          //     name: $scope.newProjectLinkObj.name,
          //     path: $scope.newProjectLinkObj.path
          //   };
          //   console.log("params", params);
          //   Restangular.all("project-link")
          //     .post(params)
          //     .then(
          //       function(response) {
          //         $scope.newProjectLinkObj = {
          //           name: "",
          //           path: ""
          //         };
          //         $scope.loadRequiredProjectLinks();
          //       },
          //       function(error) {
          //         console.log(error);
          //       }
          //     );
          // };
          // $scope.editProjectLinkRow = function(index) {
          //   console.log(index);
          //   $scope.editable[index] = true;
          //   console.log($scope.editable);
          // };
          // $scope.saveProjectLinkRow = function(index, ProjectLinkRow) {
          //   $scope.editable[index] = true;
          //   Restangular.one("project-link", ProjectLinkRow.id)
          //     .customPUT({
          //       ProjectLinkRow: ProjectLinkRow
          //     })
          //     .then(
          //       function(response) {
          //         $scope.editable[index] = false;
          //       },
          //       function(errors) {
          //         console.log(errors);
          //       }
          //     );
          // };

          // $scope.removeProjectLinkRow = function(index, ProjectLinkRow) {
          //   var r = confirm("Are you sure you want to delete this entry?");
          //   if (r != true) {
          //     return;

          //   }
          //   Restangular.one("project-link", ProjectLinkRow.id)
          //     .remove()
          //     .then(
          //       function(response) {
          //         $scope.oldProjectLinks.splice(index, 1);
          //       },
          //       function(error) {
          //         console.log(error);
          //       }
          //     );
          // };
        }
      ]
    };
  }
]);

"use strict()";

/*Directives*/
angular.module("myApp").directive("projectCms", [
  function() {
    return {
      //template: "<h1>Made by a directive!</h1>",

      restrict: "E",
      templateUrl: "/scripts/directives/cms-project-user/index.html",
      replace: true,
      scope: {
        userId: "="
      },
      controller: [
        "$scope",
        "Restangular",
        function($scope, Restangular) {
          $scope.form = {
            loading: false,
            error: false,
            message: null,
            id: null,
            index: null,
            submitted: false,
            selected_project: null,
            data: [],
            data2: [],
            checkbox: null,
            model: {
              selectedProject: null,
              title: null,
              description: null,
              user_id: $scope.userId,
              message: null,
              project_id: null
            },
            errors: {
              error: false,
              title: false,
              description: false,
              error_message: null
            },
            select_project: function(data, obj) {
              console.log(obj);
              console.log($scope.form.data);
              var params = {
                data: $scope.form.model
              };
              Restangular.all("cms-project-user")
                .post(params)
                .then(
                  function(response) {
                    $scope.form.loading = false;

                    $scope.form.data2.push(response[0]);
                    console.log(response);
                  },
                  function(errors) {
                    $scope.form.loading = false;
                    $scope.form.errors.error_message = errors.data.message;
                    console.log(errors);
                  }
                );
              $scope.form.model.selectedProject = data;
              $scope.form.model.project_id = $scope.model.selectedProject.id;
              // console.log("hello");
            },
            getMeta: function() {},
            descClick: function(obj) {
              console.log("desc click");
              console.log(obj);
              $scope.form.id = obj.currentTarget.attributes.data.nodeValue;
              $scope.form.index =
                obj.currentTarget.attributes.data.ownerElement.id;
            },

            descClicked: function() {
              console.log($scope.form.id);
              var obj = Restangular.one(
                "cms-project-user",
                $scope.form.model.user_id
              );
              obj.description = $scope.form.data2[$scope.form.index].role;
              obj.id = $scope.form.id;
              obj.put().then(
                function(response) {
                  $scope.form.data2[$scope.form.index] = response[0];
                },
                function(error) {
                  $scope.form.loading = false;
                  $scope.form.errors.error_message = errors.data.message;
                  console.log("ERR", error);
                }
              );
            },

            checkboxUpdate: function() {
              var params = {
                data: $scope.form.model
              };

              var obj = Restangular.one(
                "user-visibility-setting",
                $scope.form.model.user_id
              );
              obj.status = $scope.form.checkbox ? "0" : "1";
              obj.title = "projects";
              obj.put().then(
                function(response) {
                  $scope.form.checkbox = response[0].projects ? true : false;
                },
                function(error) {
                  $scope.form.loading = false;
                  $scope.form.errors.error_message = errors.data.message;
                  console.log("ERR", error);
                }
              );
            },
            init: function() {
              $scope.form.loading = true;
              $scope.form.getMeta();
              Restangular.one(
                "user-visibility-setting",
                $scope.form.model.user_id
              )
                .get()
                .then(
                  function(response) {
                    $scope.form.checkbox = response[0].projects ? true : false;
                  },
                  function(errors) {
                    $scope.form.loading = false;
                    $scope.form.errors.error_message = errors.data.message;
                    console.log(error);
                  }
                );

              Restangular.one("cms-project")
                .get()
                .then(
                  function(response) {
                    console.log("recieved");
                    $scope.form.loading = false;
                    $scope.form.data = response;
                    //$scope.form.model.selectedProject = response;
                  },
                  function(errors) {
                    $scope.form.loading = false;
                    $scope.form.errors.error_message = errors.data.message;
                    console.log(error);
                  }
                );

              Restangular.one("cms-project-user", $scope.form.model.user_id)
                .get()
                .then(
                  function(response) {
                    console.log(response);
                    $scope.form.loading = false;
                    $scope.form.data2 = response;
                    console.log($scope.form.data2);
                  },
                  function(errors) {
                    $scope.form.loading = false;
                    $scope.form.errors.error_message = errors.data.message;
                    console.log(error);
                  }
                );
            },

            submit: function($form) {
              var params = {
                data: $scope.form.model
              };
              Restangular.all("cms-project-user")
                .post(params)
                .then(
                  function(response) {
                    $scope.form.loading = false;

                    $scope.form.data2.push(response[0]);
                    console.log(response);
                  },
                  function(errors) {
                    $scope.form.loading = false;
                    $scope.form.errors.error_message = errors.data.message;
                    console.log(errors);
                  }
                );
            },
            delete: function(obj) {
              Restangular.one(
                "cms-project-user",
                obj.currentTarget.attributes.data.nodeValue
              )
                .remove()
                .then(
                  function(response) {
                    delete $scope.form.data2[
                      obj.currentTarget.attributes.data.ownerElement.id
                    ];
                  },
                  function(errors) {
                    $scope.form.loading = false;
                    $scope.form.errors.error_message = errors.data.message;
                    console.log(errors);
                  }
                );
            }
          };
          $scope.form.init();
        }
      ]
    };
  }
]);

//# sourceMappingURL=custom.js.map
