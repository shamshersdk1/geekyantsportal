<?php

namespace App\Events\Loan;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\InteractsWithSockets;

use Log;

class LoanRequestReconciled
{
    use InteractsWithSockets, SerializesModels;

    public $loan_request_id;
    public $role;
    public $comments;
    public $status;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($loan_request_id, $status, $role, $comments)
    {
        $this->loan_request_id = $loan_request_id;
        $this->role = $role;
        $this->comments = $comments;
        $this->status = $status;

    }
}
