<?php

namespace App\Events\Loan;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\InteractsWithSockets;

use Log;

class LoanRequestDisbursed
{
    use InteractsWithSockets, SerializesModels;

    public $loan_request_id;
    public $role;
    public $status;
    public $comments;
    public $data_array;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($loan_request_id, $status, $role, $comments, $data_array)
    {
        $this->loan_request_id = $loan_request_id;
        $this->role = $role;
        $this->status = $status;
        $this->comments = $comments;
        $this->data_array = $data_array;
    }
}
