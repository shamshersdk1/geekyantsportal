<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Log;

class ActivityLog
{
    use InteractsWithSockets, SerializesModels;

    public $reference_id;
    public $reference_type;
    public $action;

    public function __construct($referenceId, $referenceType, $action)
    {
        $this->reference_id = $referenceId;
        $this->reference_type = $referenceType;
        $this->action = $action;
    }
    
}
