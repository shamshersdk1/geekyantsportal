<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Log;

class LoanEmiPaid
{
    use InteractsWithSockets, SerializesModels;

    public $payslipDataObj;

    public function __construct($payslipDataObj)
    {
        $this->payslipDataObj = $payslipDataObj;
    }
    
}
