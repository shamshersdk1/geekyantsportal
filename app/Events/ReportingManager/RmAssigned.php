<?php

namespace App\Events\ReportingManager;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class RmAssigned
{
    use InteractsWithSockets, SerializesModels;

    public $userId;
    public $rmId;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($userId, $rmId)
    {
        $this->userId = $userId;
        $this->rmId = $rmId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
