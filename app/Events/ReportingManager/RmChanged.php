<?php

namespace App\Events\ReportingManager;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class RmChanged
{
    use InteractsWithSockets, SerializesModels;

    public $userId;
    public $oldRmId;
    public $newRmId;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($userId, $oldRmId, $newRmId)
    {
        $this->userId = $userId;
        $this->oldRmId = $oldRmId;
        $this->newRmId = $newRmId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
