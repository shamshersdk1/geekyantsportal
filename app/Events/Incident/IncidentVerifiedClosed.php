<?php

namespace App\Events\Incident;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class IncidentVerifiedClosed
{
    use InteractsWithSockets, SerializesModels;

    public $incidentId;
    
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($incidentId)
    {
        $this->incidentId = $incidentId;
    }
}
