<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Log;

class LoanCompleted
{
    use InteractsWithSockets, SerializesModels;

    public $loanId;

    public function __construct($loanId)
    {
        $this->loanId = $loanId;
    }
    
}
