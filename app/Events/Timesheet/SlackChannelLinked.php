<?php

namespace App\Events\Timesheet;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SlackChannelLinked
{
    use InteractsWithSockets, SerializesModels;

    public $slackChannelId;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($slackChannelId)
    {
        $this->slackChannelId = $slackChannelId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
