<?php

namespace App\Events\Late;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Log;

class LateMarked
{
    use InteractsWithSockets, SerializesModels;

    public $userId;
    public $reportedById;
    public $date;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($userId, $reportedById, $date)
    {
        $this->userId = $userId;
        $this->reportedById = $reportedById;
        $this->date = $date;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
