<?php

namespace App\Events\BonusRequest;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class BonusRequestRejected
{
    use InteractsWithSockets, SerializesModels;

    public $bonusRequestId;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($bonusRequestId)
    {
        $this->bonusRequestId = $bonusRequestId;
    }
}
