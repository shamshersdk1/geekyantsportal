<?php

namespace App\Events\BonusRequest;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class BonusRequestRaised
{
    use InteractsWithSockets, SerializesModels;

    public $bonusRequestId;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($bonusRequestId)
    {
        \Log::info('EVENT BonusRequestRaised ');
        $this->bonusRequestId = $bonusRequestId;
    }
}
