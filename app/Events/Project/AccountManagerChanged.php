<?php

namespace App\Events\Project;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class AccountManagerChanged
{
    use InteractsWithSockets, SerializesModels;

    public $oldUserId;
    public $newUserId;
    public $projectId;
    
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($oldUserId, $newUserId, $projectId)
    {
        $this->oldUserId = $oldUserId;
        $this->newUserId = $newUserId;
        $this->projectId = $projectId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
