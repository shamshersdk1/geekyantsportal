<?php

namespace App\Events\Project;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class DeveloperAssigned
{
    use InteractsWithSockets, SerializesModels;

    public $userId;
    public $projectId;
    public $startDate;
    public $endDate;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($userId, $projectId, $startDate, $endDate)
    {
        $this->userId = $userId;
        $this->projectId = $projectId;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
