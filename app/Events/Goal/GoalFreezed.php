<?php

namespace App\Events\Goal;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Log;

class GoalFreezed
{
    use InteractsWithSockets, SerializesModels;

    public $goalId;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($goalId)
    {
        $this->goalId = $goalId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
