<?php

namespace App\Events\Comment;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Log;

class CommentStored
{
    use InteractsWithSockets, SerializesModels;

    public $commentId;

    public function __construct($commentId)
    {
        $this->commentId = $commentId;
    }
    
}
