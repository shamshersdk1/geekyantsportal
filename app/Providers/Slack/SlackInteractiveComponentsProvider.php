<?php

namespace App\Providers\Slack;

use Illuminate\Support\ServiceProvider;

use Config;

class SlackInteractiveComponentsProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $services = Config::get('slack.interactivecomponents');

        foreach ($services as $service) 
        {
            $this->app->bind("App\Services\SlackService\SlackListnerService\InteractiveComponentInterface", "App\Services\SlackService\SlackListnerService\InteractiveComponent\\{$service}");
        }
    }
}
