<?php namespace App\Providers\Slack;

use Illuminate\Support\ServiceProvider;

use Config;

class SlackServiceProvider extends ServiceProvider {

    public function register() {
        $services = Config::get('slack.slashcommands');

        foreach ($services as $service) {
            $this->app->bind("App\Services\SlackService\SlackListnerService\SlashCommandInterface", "App\Services\SlackService\SlackListnerService\SlashCommand\\{$service}");
        }
    }
}