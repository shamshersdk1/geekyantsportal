<?php namespace App\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{

    /**
     * The event handler mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\Leave\LeaveApplied' => [
            'App\Listeners\LeaveApplied\EmailRM',
            'App\Listeners\LeaveApplied\EmailTL',
            'App\Listeners\LeaveApplied\SlackRM',
            'App\Listeners\LeaveApplied\SlackTL',
        ],
        'App\Events\Leave\LeaveCancelled' => [
            'App\Listeners\LeaveCancelled\EmailUser',
            'App\Listeners\LeaveCancelled\EmailRM',
            'App\Listeners\LeaveCancelled\EmailTL',
            'App\Listeners\LeaveCancelled\SlackRM',
            'App\Listeners\LeaveCancelled\SlackAdmin',
            'App\Listeners\LeaveCancelled\SlackTL',
            'App\Listeners\LeaveCancelled\SlackUser',
            'App\Listeners\LeaveCancelled\RemoveLeaveEventFromCalendar',
        ],
        'App\Events\Leave\LeaveApproved' => [
            'App\Listeners\LeaveApproved\EmailUser',
            'App\Listeners\LeaveApproved\EmailRM',
            'App\Listeners\LeaveApproved\EmailTL',
            'App\Listeners\LeaveApproved\SlackRM',
            'App\Listeners\LeaveApproved\SlackAdmin',
            'App\Listeners\LeaveApproved\SlackTL',
            'App\Listeners\LeaveApproved\SlackUser',
            'App\Listeners\LeaveApproved\SaveLeaveToCalendar',
        ],
        'App\Events\Leave\LeaveRejected' => [
            'App\Listeners\LeaveRejected\EmailUser',
            'App\Listeners\LeaveRejected\EmailRM',
            'App\Listeners\LeaveRejected\EmailTL',
            'App\Listeners\LeaveRejected\SlackRM',
            'App\Listeners\LeaveRejected\SlackTL',
            'App\Listeners\LeaveRejected\SlackUser',
        ],
        'App\Events\Late\LateMarked' => [
            'App\Listeners\LateMarked\SlackUser',
            'App\Listeners\LateMarked\SlackAdmin',
        ],
        'App\Events\Late\LateUnmarked' => [
            'App\Listeners\LateUnmarked\SlackUser',
            'App\Listeners\LateUnmarked\SlackAdmin',
        ],
        'App\Events\LoanDisbursed' => [
            'App\Listeners\Loan\NotifyLoanDisburse',
        ],
        'App\Events\LoanCompleted' => [
            'App\Listeners\Loan\NotifyLoanComplete',
        ],
        'App\Events\LoanEmiPaid' => [
            'App\Listeners\UpdateLoanEmi',
        ],
        'App\Events\Goal\GoalSet' => [
            'App\Listeners\Goal\SlackUser',
        ],
        'App\Events\Goal\GoalConfirmed' => [
            'App\Listeners\Goal\SlackRM',
        ],
        'App\Events\Goal\GoalFreezed' => [
            'App\Listeners\Goal\GoalFreezed\SlackRM',
            'App\Listeners\Goal\GoalFreezed\SlackUser',
        ],
        'App\Events\RequestApproval\RequestRaised' => [
            'App\Listeners\RequestApproval\RequestRaised\SlackRequestors',
        ],
        'App\Events\RequestApproval\RequestApproved' => [
            'App\Listeners\RequestApproval\RequestApproved\SlackUser',
            'App\Listeners\RequestApproval\RequestApproved\SlackNotifyTo',
        ],
        'App\Events\RequestApproval\RequestRejected' => [
            'App\Listeners\RequestApproval\RequestRejected\SlackUser',
            'App\Listeners\RequestApproval\RequestRejected\SlackNotifyTo',
        ],
        'App\Events\SshKey\SshKeyEntered' => [
            'App\Listeners\SshKey\SshKeyEntered\SlackUser',
        ],
        'App\Events\Timesheet\SlackChannelLinked' => [
            'App\Listeners\Timesheet\SlackChannelLinked\TempToTimesheet',
        ],
        'App\Events\Project\AccountManagerAssigned' => [
            'App\Listeners\Project\AccountManagerAssigned\SlackNewManager',
        ],
        'App\Events\Project\BdManagerAssigned' => [
            'App\Listeners\Project\BdManagerAssigned\SlackNewManager',
        ],
        'App\Events\Project\ProjectManagerAssigned' => [
            'App\Listeners\Project\ProjectManagerAssigned\SlackNewManager',
        ],
        'App\Events\Project\AccountManagerChanged' => [
            'App\Listeners\Project\AccountManagerChanged\SlackNewManager',
            'App\Listeners\Project\AccountManagerChanged\SlackOldManager',
        ],
        'App\Events\Project\BdManagerChanged' => [
            'App\Listeners\Project\BdManagerChanged\SlackNewManager',
            'App\Listeners\Project\BdManagerChanged\SlackOldManager',
        ],
        'App\Events\Project\ProjectManagerChanged' => [
            'App\Listeners\Project\ProjectManagerChanged\SlackNewManager',
            'App\Listeners\Project\ProjectManagerChanged\SlackOldManager',
        ],
        'App\Events\ReportingManager\RmAssigned' => [
            'App\Listeners\RmAssigned\SlackNewRM',
            'App\Listeners\RmAssigned\SlackUser',
        ],
        'App\Events\ReportingManager\RmChanged' => [
            'App\Listeners\RmChanged\SlackNewRM',
            'App\Listeners\RmChanged\SlackOldRM',
            'App\Listeners\RmChanged\SlackUser',
        ],
        'App\Events\Project\DeveloperAssigned' => [
            'App\Listeners\Project\DeveloperAssigned\SlackDeveloper',
            'App\Listeners\Project\DeveloperAssigned\SlackChannel',
        ],
        'App\Events\Project\DeveloperRemoved' => [
            'App\Listeners\Project\DeveloperRemoved\SlackDeveloper',
            'App\Listeners\Project\DeveloperRemoved\SlackChannel',
        ],
        'App\Events\Incident\IncidentCreated' => [
            'App\Listeners\Incident\NotifyIncidentCreated',
        ],
        'App\Events\Incident\IncidentClosed' => [
            'App\Listeners\Incident\NotifyIncidentClosed',
        ],
        'App\Events\Incident\IncidentReopened' => [
            'App\Listeners\Incident\NotifyIncidentReopened',
        ],
        'App\Events\Incident\IncidentShared' => [
            'App\Listeners\Incident\NotifyIncidentShared',
        ],
        'App\Events\Incident\IncidentTransferred' => [
            'App\Listeners\Incident\NotifyIncidentTransferred',
        ],
        'App\Events\Incident\IncidentVerifiedClosed' => [
            'App\Listeners\Incident\NotifyIncidentVerifiedClosed',
        ],
        'App\Events\Comment\CommentStored' => [
            'App\Listeners\Comment\CommentStoredAction',
        ],
        'App\Events\User\UserCreated' => [
            'App\Listeners\User\UserCreatedHandle',
        ],
        'App\Events\User\UserUpdated' => [
            'App\Listeners\User\UserUpdatedHandle',
        ],
        'App\Events\Loan\LoanRequested' => [
            'App\Listeners\Loan\LoanRequested\UpdateLoanRequestActivities',
            'App\Listeners\Loan\LoanRequested\SendNotifications',
        ],
        'App\Events\Loan\LoanRequestSubmitted' => [
            'App\Listeners\Loan\LoanRequestSubmitted\UpdateLoanRequestActivities',
            'App\Listeners\Loan\LoanRequestSubmitted\SendNotifications',
        ],
        'App\Events\Loan\LoanRequestReviewed' => [
            'App\Listeners\Loan\LoanRequestReviewed\UpdateLoanRequestActivities',
            'App\Listeners\Loan\LoanRequestReviewed\SendNotifications',
        ],
        'App\Events\Loan\LoanRequestReconciled' => [
            'App\Listeners\Loan\LoanRequestReconciled\UpdateLoanRequestActivities',
            'App\Listeners\Loan\LoanRequestReconciled\SendNotifications',
        ],
        'App\Events\Loan\LoanRequestApproved' => [
            'App\Listeners\Loan\LoanRequestApproved\UpdateLoanRequestActivities',
            'App\Listeners\Loan\LoanRequestApproved\SendNotifications',
            'App\Listeners\Loan\LoanRequestApproved\UpdateLoanTable',
        ],
        'App\Events\Loan\LoanRequestDisbursed' => [
            'App\Listeners\Loan\LoanRequestDisbursed\UpdateLoanRequestActivities',
            'App\Listeners\Loan\LoanRequestDisbursed\SendNotifications',
            'App\Listeners\Loan\LoanRequestDisbursed\UpdateLoanTable',
        ],
        'App\Events\Loan\LoanRequestRejected' => [
            'App\Listeners\Loan\LoanRequestRejected\UpdateLoanRequestActivities',
            'App\Listeners\Loan\LoanRequestRejected\SendNotifications',
        ],
        'App\Events\BonusRequest\BonusRequestRaised' => [
            'App\Listeners\BonusRequest\NotifyBonusRequestRaised',
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }

}
