<?php namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler {

	private $sentryID;
	/**
	 * A list of the exception types that should not be reported.
	 *
	 * @var array
	 */
	protected $dontReport = [
		'Symfony\Component\HttpKernel\Exception\HttpException'
	];

	/**
	 * Report or log an exception.
	 *
	 * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
	 *
	 * @param  \Exception  $e
	 * @return void
	 */
	public function report(Exception $exception)
	{	
		if ( !empty(env('SENTRY_SERVER')) && env('SENTRY_SERVER') == 'production' && $this->shouldReport($exception) ) {
			
	        app('sentry')->captureException($exception);
	    }

	    parent::report($exception);
	}

	/**
	 * Render an exception into an HTTP response.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Exception  $e
	 * @return \Illuminate\Http\Response
	 */
	public function render($request, Exception $e)
	{
		if ($e instanceof ModelNotFoundException) {
			return response()->view('errors.not-found', [], 404);
		}

		if ($this->isHttpException($e)) {
			return response()->view('errors.not-found', [], 404);
		} else {
			// Custom error 500 view on production
			if (app()->environment() == 'production') {
				response()->view('errors.not-found', [], 404);
			}
			return parent::render($request, $e);
		}
		// return parent::render($request, $e);
	}

}
