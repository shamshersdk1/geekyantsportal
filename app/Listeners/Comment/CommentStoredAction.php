<?php

namespace App\Listeners\Comment;

use App\Events\Comment\CommentStored;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;
use App\Models\Comment;
use App\Models\Activity;
use App\Services\IncidentService;

class CommentStoredAction
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  CommentStored  $event
     * @return void
     */
    public function handle(CommentStored $event)
    {
        $comment_id = $event->commentId;
        $commentObj = Comment::find($comment_id);
        if ( $commentObj )
        {
            if ( $commentObj->commentable_type == 'App\Models\Admin\Incident' )
            {
                $data['action'] = 'comment';
                $data['reference_id'] = $commentObj->commentable_id;
                $data['reference_type'] = $commentObj->commentable_type;
                $data['user_id'] = $commentObj->user_id;
                Activity::storeData($data);

                $commentCount = Comment::where('commentable_type',$commentObj->commentable_type)->where('commentable_id',$commentObj->commentable_id)->count();
                if ( $commentCount == 1 )
                {
                    IncidentService::changeIncidentStatus($commentObj->commentable_id);
                }
                IncidentService::notifyCommentAdded($commentObj->id);
            }
            if ( $commentObj->commentable_type == 'App\Models\Admin\Expense' )
            {
                $data['action'] = 'comment';
                $data['reference_id'] = $commentObj->commentable_id;
                $data['reference_type'] = $commentObj->commentable_type;
                $data['user_id'] = $commentObj->user_id;
                Activity::storeData($data);
            }
        }
    }
}