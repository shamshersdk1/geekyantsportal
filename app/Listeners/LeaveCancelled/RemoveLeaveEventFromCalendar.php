<?php

namespace App\Listeners\LeaveCancelled;

use App\Events\Leave\LeaveCancelled;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\CalendarEvent;
use App\Jobs\GoogleCalendarEvents\DeleteCalendarEventJob;
use Log;

class RemoveLeaveEventFromCalendar
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }
    /**
     * Handle the event.
     *
     * @param  LeaveApplied  $event
     * @return void
     */
    public function handle(LeaveCancelled $event)
    {
        try 
        {
            $job = (new DeleteCalendarEventJob($event->leaveId))->onQueue('calendar-event');
            dispatch($job);
        } catch (Exception $e) {
            \Log::info('Unable to process CalendarEvent Leave#'.$this->leaveId);
            \Log::info(json_encode($e->getMessage()));
        }
    }
}
