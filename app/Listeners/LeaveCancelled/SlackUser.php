<?php

namespace App\Listeners\LeaveCancelled;

use App\Events\Leave\LeaveCancelled;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;
use App\Models\Admin\Leave;
use App\Jobs\Slack\SlackReminder\NotifyUser;

class SlackUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  LeaveCancelled  $event
     * @return void
     */
    public function handle(LeaveCancelled $event)
    {
        $leave = Leave::find($event->leaveId);
        if ( !empty($leave) )
        {
            $job = (new NotifyUser($leave->id))->onQueue('leave-reminder');
            dispatch($job);
        }
    }
}
