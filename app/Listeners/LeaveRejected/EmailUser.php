<?php

namespace App\Listeners\LeaveRejected;

use App\Events\Leave\LeaveRejected;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;
use App\Jobs\SendEmailJob;

class EmailUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  LeaveApplied  $event
     * @return void
     */
    public function handle(LeaveRejected $event)
    {
        $job = (new SendEmailJob($event->leaveId, 'notify','USER'))->onQueue('leave-email');
        dispatch($job);
    }
}
