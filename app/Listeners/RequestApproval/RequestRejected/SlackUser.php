<?php

namespace App\Listeners\RequestApproval\RequestRejected;

use App\Events\RequestApproval\RequestRejected;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;
use App\Jobs\Slack\SlackReminder\RequestApproval\NotifyUser;

class SlackUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  RequestRejected  $event
     * @return void
     */
    public function handle(RequestRejected $event)
    {
      $job = (new NotifyUser($event->requestId))->onQueue('request-approval');
      dispatch($job);
    }
}
