<?php

namespace App\Listeners\RequestApproval\RequestRejected;

use App\Events\RequestApproval\RequestRejected;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;
use App\Jobs\Slack\SlackReminder\RequestApproval\Notify;

class SlackNotifyTo
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  RequestRejected  $event
     * @return void
     */
    public function handle(RequestRejected $event)
    {
      $job = (new Notify($event->requestId))->onQueue('request-approval');
      dispatch($job);
    }
}
