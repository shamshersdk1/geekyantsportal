<?php

namespace App\Listeners\RequestApproval\RequestRaised;

use App\Events\RequestApproval\RequestRaised;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;
use App\Jobs\Slack\SlackReminder\RequestApproval\NotifyUser;

class SlackUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  RequestRaised  $event
     * @return void
     */
    public function handle(RequestRaised $event)
    {
      $job = (new NotifyUser($event->requestId))->onQueue('request-approval');
      dispatch($job);
    }
}
