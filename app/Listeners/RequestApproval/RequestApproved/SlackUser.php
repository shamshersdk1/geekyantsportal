<?php

namespace App\Listeners\RequestApproval\RequestApproved;

use App\Events\RequestApproval\RequestApproved;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;
use App\Jobs\Slack\SlackReminder\RequestApproval\NotifyUser;

class SlackUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  RequestApproved  $event
     * @return void
     */
    public function handle(RequestApproved $event)
    {
      $job = (new NotifyUser($event->requestId))->onQueue('request-approval');
      dispatch($job);
    }
}
