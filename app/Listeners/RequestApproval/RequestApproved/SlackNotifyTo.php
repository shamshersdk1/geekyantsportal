<?php

namespace App\Listeners\RequestApproval\RequestApproved;

use App\Events\RequestApproval\RequestApproved;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;
use App\Jobs\Slack\SlackReminder\RequestApproval\Notify;

class SlackNotifyTo
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  RequestApproved  $event
     * @return void
     */
    public function handle(RequestApproved $event)
    {
      $job = (new Notify($event->requestId))->onQueue('request-approval');
      dispatch($job);
    }
}
