<?php

namespace App\Listeners\LeaveApplied;

use App\Events\Leave\LeaveApplied;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;
use App\Models\Admin\Leave;
use App\Jobs\Slack\SlackReminder\NotifyTL;

class SlackTL
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  LeaveApplied  $event
     * @return void
     */
    public function handle(LeaveApplied $event)
    {
        $leave = Leave::find($event->leaveId);
        if ( !empty($leave) )
        {
            $job = (new NotifyTL($leave->id))->onQueue('leave-reminder');
            dispatch($job);
        }
    }
}
