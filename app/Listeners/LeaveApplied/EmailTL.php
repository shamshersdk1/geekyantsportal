<?php

namespace App\Listeners\LeaveApplied;

use App\Events\Leave\LeaveApplied;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;
use App\Jobs\SendEmailJob;

class EmailTL
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  LeaveApplied  $event
     * @return void
     */
    public function handle(LeaveApplied $event)
    {
        $job = (new SendEmailJob($event->leaveId, 'request','TL'))->onQueue('leave-email');
        dispatch($job);
    }
}
