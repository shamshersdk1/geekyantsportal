<?php

namespace App\Listeners\Timesheet\SlackChannelLinked;

use App\Events\Timesheet\SlackChannelLinked;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\Timesheet\MoveTempRecordsToTimesheetTable;

class TempToTimesheet
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  SshKeyEntered  $event
     * @return void
     */
    public function handle(SlackChannelLinked $event)
    {
      $job = (new MoveTempRecordsToTimesheetTable($event->slackChannelId))->onQueue('slack-channel-link');
      dispatch($job);
    }
}
