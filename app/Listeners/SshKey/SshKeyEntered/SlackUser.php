<?php

namespace App\Listeners\SshKey\SshKeyEntered;

use App\Events\SshKey\SshKeyEntered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;
use App\Jobs\Slack\SlackReminder\SshKey\NotifyUser;

class SlackUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  SshKeyEntered  $event
     * @return void
     */
    public function handle(SshKeyEntered $event)
    {
      $job = (new NotifyUser($event->sshKeyId))->onQueue('ssh-notification');
      dispatch($job);
    }
}
