<?php

namespace App\Listeners\Goal\GoalFreezed;

use App\Events\Goal\GoalFreezed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;
use App\Models\Admin\UserGoal;
use App\Jobs\Slack\SlackReminder\Goal\NotifyUser;

class SlackUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  GoalFreezed  $event
     * @return void
     */
    public function handle(GoalFreezed $event)
    {
        $goal = UserGoal::find($event->goalId);
        if ( !empty($goal) )
        {
            $job = (new NotifyUser($goal->id))->onQueue('goal-reminder');
            dispatch($job);
        }
    }
}
