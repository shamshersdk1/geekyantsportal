<?php

namespace App\Listeners\Goal;

use App\Events\Goal\GoalConfirmed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;
use App\Models\Admin\UserGoal;
use App\Jobs\Slack\SlackReminder\Goal\NotifyRM;

class SlackRM
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  GoalConfirmed  $event
     * @return void
     */
    public function handle(GoalConfirmed $event)
    {
        $goal = UserGoal::find($event->goalId);
        if ( !empty($goal) )
        {
            $job = (new NotifyRM($goal->id))->onQueue('goal-reminder');
            dispatch($job);
        }
    }
}