<?php

namespace App\Listeners\Goal;

use App\Events\Goal\GoalSet;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;
use App\Models\Admin\UserGoal;
use App\Jobs\Slack\SlackReminder\Goal\NotifyUser;

class SlackUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  GoalSet  $event
     * @return void
     */
    public function handle(GoalSet $event)
    {
        $goal = UserGoal::find($event->goalId);
        if ( !empty($goal) )
        {
            $job = (new NotifyUser($goal->id))->onQueue('goal-reminder');
            dispatch($job);
        }
    }
}
