<?php

namespace App\Listeners\User;

use App\Events\User\UserCreated;
use App\Services\UserService;

class UserCreatedHandle
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  UserCreated  $event
     * @return void
     */
    public function handle(UserCreated $event)
    {
        // UserService::updateProRataUserLeaveBalance($event->userId);
        $year = date('Y');
        UserService::updateUserLeaveBalance($event->userId, $year);

    }
}
