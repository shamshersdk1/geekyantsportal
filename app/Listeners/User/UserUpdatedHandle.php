<?php

namespace App\Listeners\User;

use App\Events\User\UserUpdated;
use App\Services\UserService;

class UserUpdatedHandle
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  UserUpdated  $event
     * @return void
     */
    public function handle(UserUpdated $event)
    {
        $year = date('Y');
        UserService::updateUserLeaveBalance($event->userId, $year);
    }
}
