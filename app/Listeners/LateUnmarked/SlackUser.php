<?php

namespace App\Listeners\LateUnmarked;

use App\Events\Late\LateUnmarked;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;
use App\Models\Admin\LateComer;
use App\Jobs\Slack\SlackReminder\Late\NotifyUser;

class SlackUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  LateUnmarked  $event
     * @return void
     */
    public function handle(LateUnmarked $event)
    {
      $isMarked = false;
      $job = (new NotifyUser($event->userId, $event->reportedById, $isMarked, $event->date))->onQueue('late-comer-reminder');
      dispatch($job);
    }
}
