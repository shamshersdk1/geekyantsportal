<?php

namespace App\Listeners;

use App\Events\LoanEmiPaid;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\LoanPayment;
use Log;


class UpdateLoanEmi
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LoanEmiPaid  $event
     * @return void
     */
    public function handle(LoanEmiPaid $event)
    {
        $job = (new LoanPayment($event->payslipDataObj))->onQueue('loan-emi-save');
        dispatch($job);   
    }
}
