<?php

namespace App\Listeners\RmChanged;

use App\Events\ReportingManager\RmChanged;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\Slack\SlackReminder\RM\NotifyUser;

class SlackUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  RmChanged  $event
     * @return void
     */
    public function handle(RmChanged $event)
    {
        $job = (new NotifyUser($event->userId, $event->newRmId))->onQueue('reporting-manager-change-notification');
        dispatch($job);
    }
}
