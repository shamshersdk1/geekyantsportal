<?php

namespace App\Listeners\Incident;

use App\Events\Incident\IncidentClosed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Services\IncidentService;

class NotifyIncidentClosed
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  IncidentClosed  $event
     * @return void
     */
    public function handle(IncidentClosed $event)
    {
      IncidentService::incidentClosed($event->incidentId);
    }
}
