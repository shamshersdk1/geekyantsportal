<?php

namespace App\Listeners\Incident;

use App\Events\Incident\IncidentShared;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Services\IncidentService;

class NotifyIncidentShared
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  IncidentShared  $event
     * @return void
     */
    public function handle(IncidentShared $event)
    {
      IncidentService::incidentShared($event->incidentId);
    }
}
