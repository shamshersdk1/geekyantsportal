<?php

namespace App\Listeners\Incident;

use App\Events\Incident\IncidentCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Services\IncidentService;

class NotifyIncidentCreated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  IncidentCreated  $event
     * @return void
     */
    public function handle(IncidentCreated $event)
    {
      IncidentService::incidentCreated($event->incidentId);
    }
}
