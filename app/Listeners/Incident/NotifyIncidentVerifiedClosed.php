<?php

namespace App\Listeners\Incident;

use App\Events\Incident\IncidentVerifiedClosed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Services\IncidentService;

class NotifyIncidentVerifiedClosed
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  IncidentVerifiedClosed  $event
     * @return void
     */
    public function handle(IncidentVerifiedClosed $event)
    {
      IncidentService::incidentVerifiedClosed($event->incidentId);
    }
}
