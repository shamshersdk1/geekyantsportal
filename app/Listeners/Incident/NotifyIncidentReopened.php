<?php

namespace App\Listeners\Incident;

use App\Events\Incident\IncidentReopened;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Services\IncidentService;

class NotifyIncidentReopened
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  IncidentReopened  $event
     * @return void
     */
    public function handle(IncidentReopened $event)
    {
      IncidentService::incidentReopened($event->incidentId);
    }
}
