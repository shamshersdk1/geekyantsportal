<?php

namespace App\Listeners\Incident;

use App\Events\Incident\IncidentTransferred;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Services\IncidentService;

class NotifyIncidentTransferred
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  IncidentTransferred  $event
     * @return void
     */
    public function handle(IncidentTransferred $event)
    {
      IncidentService::incidentTransferred($event->incidentId);
    }
}
