<?php

namespace App\Listeners\RmAssigned;

use App\Events\ReportingManager\RmAssigned;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\Slack\SlackReminder\RM\NotifyNewRM;

class SlackNewRM
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  RmAssigned  $event
     * @return void
     */
    public function handle(RmAssigned $event)
    {
        $job = (new NotifyNewRM($event->userId, $event->rmId))->onQueue('reporting-manager-change-notification');
        dispatch($job);
    }
}
