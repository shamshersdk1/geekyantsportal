<?php

namespace App\Listeners\LeaveApproved;

use App\Events\Leave\LeaveApproved;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;

use App\Jobs\SendEmailJob;

class EmailUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  LeaveApplied  $event
     * @return void
     */
    public function handle(LeaveApproved $event)
    {
        $job = (new SendEmailJob($event->leaveId, 'notify','USER'))->onQueue('leave-email');
        dispatch($job);
    }
}
