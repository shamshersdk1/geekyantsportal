<?php

namespace App\Listeners\LeaveApproved;

use App\Events\Leave\LeaveApproved;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\CalendarEvent;
use App\Jobs\GoogleCalendarEvents\CreateCalendarEventJob;
use Log;

class SaveLeaveToCalendar
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  LeaveApplied  $event
     * @return void
     */
    public function handle(LeaveApproved $event)
    {
        try 
        {
            $job = (new CreateCalendarEventJob($event->leaveId))->onQueue('calendar-event');
            dispatch($job);
        } catch (Exception $e) {
            \Log::info('Unable to process CalendarEvent Leave#'.$this->leaveId);
            \Log::info(json_encode($e->getMessage()));
        }
    }
}
