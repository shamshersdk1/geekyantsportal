<?php

namespace App\Listeners\LeaveApproved;

use App\Events\Leave\LeaveApproved;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;
use App\Models\Admin\Leave;
use App\Jobs\Slack\SlackReminder\NotifyAdmin;

class SlackAdmin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  LeaveApplied  $event
     * @return void
     */
    public function handle(LeaveApproved $event)
    {
        $leave = Leave::find($event->leaveId);
        if ( !empty($leave) )
        {
            $job = (new NotifyAdmin($leave->id))->onQueue('leave-reminder');
            dispatch($job);
        }
    }
}
