<?php

namespace App\Listeners\Loan\LoanRequestRejected;

use App\Events\Loan\LoanRequestRejected;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Loan;
use App\Services\LoanRequestService;



class SendNotifications
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  LoanRequestRejected  $event
     * @return void
     */
    public function handle(LoanRequestRejected $event)
    {
        LoanRequestService::loanRejected($event->loan_request_id);
    }
}
