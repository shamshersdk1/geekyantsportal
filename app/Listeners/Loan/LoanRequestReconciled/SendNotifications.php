<?php

namespace App\Listeners\Loan\LoanRequestReconciled;

use App\Events\Loan\LoanRequestReconciled;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Loan;
use App\Services\LoanRequestService;


class SendNotifications
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  LoanRequestReconciled  $event
     * @return void
     */
    public function handle(LoanRequestReconciled $event)
    {
        LoanRequestService::loanReconciled($event->loan_request_id);
    }
}
