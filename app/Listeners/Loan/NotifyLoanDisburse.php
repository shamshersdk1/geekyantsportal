<?php

namespace App\Listeners\Loan;

use App\Events\LoanDisbursed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;
use App\Models\Loan;
use App\Jobs\Slack\SlackReminder\Loan\Notify;

class NotifyLoanDisburse
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  LoanDisbursed  $event
     * @return void
     */
    public function handle(LoanDisbursed $event)
    {
        $loan = Loan::find($event->loanId);
        if ( !empty($loan) )
        {
            $job = (new Notify($loan->id))->onQueue('loan-notification');
            dispatch($job);
        }
    }
}
