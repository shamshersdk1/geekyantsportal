<?php

namespace App\Listeners\Loan\LoanRequested;

use App\Events\Loan\LoanRequested;
use App\Models\Loan;
use App\Services\LoanRequestService;

class SendNotifications
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  LoanRequested  $event
     * @return void
     */
    public function handle(LoanRequested $event)
    {
        LoanRequestService::loanRequested($event->loan_request_id);
    }
}
