<?php

namespace App\Listeners\Loan\LoanRequested;

use App\Events\Loan\LoanRequested;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Loan;
use App\Services\LoanRequestService;


class UpdateLoanRequestActivities
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  LoanRequested  $event
     * @return void
     */
    public function handle(LoanRequested $event)
    {
        LoanRequestService::updateLoanRequestActivities($event->loan_request_id,  $event->status,$event->role, $event->comments);
    }
}
