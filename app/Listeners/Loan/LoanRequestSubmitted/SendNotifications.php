<?php

namespace App\Listeners\Loan\LoanRequestSubmitted;

use App\Events\Loan\LoanRequestSubmitted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Loan;
use App\Services\LoanRequestService;


class SendNotifications
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  LoanRequestSubmitted  $event
     * @return void
     */
    public function handle(LoanRequestSubmitted $event)
    {
        LoanRequestService::loanSubmitted($event->loan_request_id);
    }
}
