<?php

namespace App\Listeners\Loan\LoanRequestDisbursed;

use App\Events\Loan\LoanRequestDisbursed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Loan;
use App\Services\LoanRequestService;


class UpdateLoanRequestActivities
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  LoanRequestDisbursed  $event
     * @return void
     */
    public function handle(LoanRequestDisbursed $event)
    {
        LoanRequestService::updateLoanRequestActivities($event->loan_request_id, $event->status, $event->role, $event->comments);
    }
}
