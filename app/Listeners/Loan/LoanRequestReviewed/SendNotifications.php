<?php

namespace App\Listeners\Loan\LoanRequestReviewed;

use App\Events\Loan\LoanRequestReviewed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Loan;
use App\Services\LoanRequestService;


class SendNotifications
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  LoanRequestReviewed  $event
     * @return void
     */
    public function handle(LoanRequestReviewed $event)
    {
        LoanRequestService::loanReviewed($event->loan_request_id);
    }
}
