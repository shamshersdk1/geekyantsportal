<?php

namespace App\Listeners\Loan\LoanRequestApproved;

use App\Events\Loan\LoanRequestApproved;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Loan;
use App\Services\LoanRequestService;


class UpdateLoanRequestActivities
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  LoanRequestApproved  $event
     * @return void
     */
    public function handle(LoanRequestApproved $event)
    {
        LoanRequestService::updateLoanRequestActivities($event->loan_request_id, $event->status,$event->role, $event->comments);
    }
}
