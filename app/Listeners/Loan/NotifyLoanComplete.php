<?php

namespace App\Listeners\Loan;

use App\Events\LoanCompleted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;
use App\Models\Loan;
use App\Jobs\Slack\SlackReminder\Loan\Notify;

class NotifyLoanComplete
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  LoanCompleted  $event
     * @return void
     */
    public function handle(LoanCompleted $event)
    {
        $loan = Loan::find($event->loanId);
        if ( !empty($loan) )
        {
            $job = (new Notify($loan->id))->onQueue('loan-notification');
            dispatch($job);
        }
    }
}
