<?php

namespace App\Listeners\LateMarked;

use App\Events\Late\LateMarked;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;
use App\Models\Admin\LateComer;
use App\Jobs\Slack\SlackReminder\Late\NotifyAdmin;

class SlackAdmin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  LateMarked  $event
     * @return void
     */
    public function handle(LateMarked $event)
    {
      $isMarked = true;
      $job = (new NotifyAdmin($event->userId, $event->reportedById, $isMarked, $event->date))->onQueue('late-comer-reminder');
      dispatch($job);
    }
}
