<?php

namespace App\Listeners;

use App\Events\ActivityLog;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Activity;
use Log;


class ActivityLog
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LoanEmiPaid  $event
     * @return void
     */
    public function handle(ActivityLog $event)
    {
        $data['reference_type'] = $event->reference_type
        $data['reference_id'] = $event->reference_id
        $data['action'] = $event->action
        $data['user_id'] = \Auth::User()->id;
        Activity::storeData($data);
    }
}
