<?php

namespace App\Listeners\Project\BdManagerChanged;

use App\Events\Project\BdManagerChanged;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\Slack\SlackReminder\Project\NotifyOldManager;

class SlackOldManager
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  BdManagerChanged  $event
     * @return void
     */
    public function handle(BdManagerChanged $event)
    {
        $type = 'bd-manager';
        $job = (new NotifyOldManager($event->oldUserId, $event->projectId, $type))->onQueue('project-manager-change-notification');
        dispatch($job);
    }
}
