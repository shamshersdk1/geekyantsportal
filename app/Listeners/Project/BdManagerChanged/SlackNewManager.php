<?php

namespace App\Listeners\Project\BdManagerChanged;

use App\Events\Project\BdManagerChanged;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\Slack\SlackReminder\Project\NotifyNewManager;

class SlackNewManager
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  BdManagerChanged  $event
     * @return void
     */
    public function handle(BdManagerChanged $event)
    {
        $type = 'sales-manager';
        $job = (new NotifyNewManager($event->newUserId, $event->projectId, $type))->onQueue('project-manager-change-notification');
        dispatch($job);
    }
}
