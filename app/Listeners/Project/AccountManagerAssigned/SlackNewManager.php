<?php

namespace App\Listeners\Project\AccountManagerAssigned;

use App\Events\Project\AccountManagerAssigned;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\Slack\SlackReminder\Project\NotifyNewManager;

class SlackNewManager
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  AccountManagerAssigned  $event
     * @return void
     */
    public function handle(AccountManagerAssigned $event)
    {
        $type = 'account-manager';
        $job = (new NotifyNewManager($event->userId, $event->projectId, $type))->onQueue('project-manager-change-notification');
        dispatch($job);
    }
}
