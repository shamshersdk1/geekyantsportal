<?php

namespace App\Listeners\Project\DeveloperAssigned;

use App\Events\Project\DeveloperAssigned;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\Slack\SlackReminder\Project\NotifyChannel;

class SlackChannel
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  DeveloperAssigned  $event
     * @return void
     */
    public function handle(DeveloperAssigned $event)
    {
        $type = 'assigned';
        $job = (new NotifyChannel($event->userId, $event->projectId, $type, $event->startDate, $event->endDate))->onQueue('project-manager-change-notification');
        dispatch($job);
    }
}
