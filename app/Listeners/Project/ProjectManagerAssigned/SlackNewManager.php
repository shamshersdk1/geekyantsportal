<?php

namespace App\Listeners\Project\ProjectManagerAssigned;

use App\Events\Project\ProjectManagerAssigned;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\Slack\SlackReminder\Project\NotifyNewManager;

class SlackNewManager
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  ProjectManagerAssigned  $event
     * @return void
     */
    public function handle(ProjectManagerAssigned $event)
    {
        $type = 'project-manager';
        $job = (new NotifyNewManager($event->userId, $event->projectId, $type))->onQueue('project-manager-change-notification');
        dispatch($job);
    }
}
