<?php

namespace App\Listeners\Project\AccountManagerChanged;

use App\Events\Project\AccountManagerChanged;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\Slack\SlackReminder\Project\NotifyOldManager;

class SlackOldManager
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  AccountManagerChanged  $event
     * @return void
     */
    public function handle(AccountManagerChanged $event)
    {
        $type = 'account-manager';
        $job = (new NotifyOldManager($event->oldUserId, $event->projectId, $type))->onQueue('project-manager-change-notification');
        dispatch($job);
    }
}
