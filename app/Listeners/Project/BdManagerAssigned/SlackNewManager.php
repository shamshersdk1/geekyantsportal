<?php

namespace App\Listeners\Project\BdManagerAssigned;

use App\Events\Project\BdManagerAssigned;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\Slack\SlackReminder\Project\NotifyNewManager;

class SlackNewManager
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  BdManagerAssigned  $event
     * @return void
     */
    public function handle(BdManagerAssigned $event)
    {
        $type = 'sales-manager';
        $job = (new NotifyNewManager($event->userId, $event->projectId, $type))->onQueue('project-manager-change-notification');
        dispatch($job);
    }
}
