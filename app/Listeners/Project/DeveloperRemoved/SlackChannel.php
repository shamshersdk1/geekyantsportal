<?php

namespace App\Listeners\Project\DeveloperRemoved;

use App\Events\Project\DeveloperRemoved;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\Slack\SlackReminder\Project\NotifyChannel;

class SlackChannel
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  DeveloperRemoved  $event
     * @return void
     */
    public function handle(DeveloperRemoved $event)
    {
        $type = 'removed';
        $job = (new NotifyChannel($event->userId, $event->projectId, $type, 'null', 'null'))->onQueue('project-manager-change-notification');
        dispatch($job);
    }
}
