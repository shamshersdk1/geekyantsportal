<?php

namespace App\Listeners\Project\ProjectManagerChanged;

use App\Events\Project\ProjectManagerChanged;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\Slack\SlackReminder\Project\NotifyNewManager;

class SlackNewManager
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  ProjectManagerChanged  $event
     * @return void
     */
    public function handle(ProjectManagerChanged $event)
    {
        $type = 'project-manager';
        $job = (new NotifyNewManager($event->newUserId, $event->projectId, $type))->onQueue('project-manager-change-notification');
        dispatch($job);
    }
}
