<?php

namespace App\Listeners\BonusRequest;

use App\Events\BonusRequest\BonusRequestRaised;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Services\BonusService;

class NotifyBonusRequestRaised
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  BonusRequestRaised  $event
     * @return void
     */
    public function handle(BonusRequestRaised $event)
    {
        \Log::info('handle BonusRequestRaised called');
        BonusService::bonusRequestRaised($event->bonusRequestId);
    }
}
