<?php

namespace App\Listeners\BonusRequest;

use App\Events\BonusRequest\BonusRequestApproved;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Services\BonusService;

class NotifyBonusRequestRejected
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  BonusRequestApproved  $event
     * @return void
     */
    public function handle(BonusRequestApproved $event)
    {
      BonusService::bonusRequestApproved($event->bonusRequestId);
    }
}
