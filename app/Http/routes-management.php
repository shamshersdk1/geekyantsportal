<?php
            Route::group(['roles' => ['management']], function () {
                // ---------------------------------------------------------------
                // ------------------------- Loan Request -------------------------
                Route::get('reviewed-loan-requests', 'LoanRequestController@getReviewedRequests');
                Route::get('reviewed-loan-requests/{id}', 'LoanRequestController@showReviewedRequest');
            });