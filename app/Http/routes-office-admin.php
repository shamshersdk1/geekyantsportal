<?php

Route::group(['roles' => ['office-admin', 'system-admin']], function () {
    // ------------------------- Asset Management -------------------------
    Route::resource('asset-management/asset-category', 'AssetCategoryController');
    Route::resource('asset-management/asset', 'AssetController');
    Route::post('asset-management/asset/{id}/assignUser', 'AssetController@assignUser');
    Route::post('asset-management/asset/release/{id}', 'AssetController@releaseUser');
    Route::delete('asset-management/asset/delete/{id}', 'AssetController@destroyUser');
    Route::post('asset-management/asset/updateUser/{id}', 'AssetController@updateUserAssignment');
    Route::get('asset-management/file/delete/{id}', 'FileController@destroy');
    Route::get('asset-management/network-asset', 'AssetController@networkAsset');

    //Route::resource('ip-address-mapper', 'IpAddressMapperController');
    // ---------------------------------------------------------------
    //Purchase Orders
    Route::resource('purchase-orders', 'PurchaseOrderController');
    Route::resource('vendor', 'VendorController');
    Route::get('expense', 'ExpenseController@index');
    Route::get('expense/{id}', 'ExpenseController@show');

});
