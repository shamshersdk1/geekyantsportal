<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidateAppraisalBonusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(!$this->user()->hasRole('admin')){
            return false;
        }
        return true;    
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'appraisal_id'=>'required|exists:appraisals,id',
            'appraisal_bonus_type_id'=>'required|exists:appraisal_bonus_type,id',
            'value' =>'required',
            'value_date'=>'required|date'
        ];
    }
}
