<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Redirect;

class ValidateServerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(!$this->user()->hasRole('admin') &&  !$this->user()->hasRole('system-admin')){
            return false;
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'name' => 'required|unique:servers',
                'city' => 'required',
                'country' => 'required',
                'primary_ip' => 'required|unique:servers|ip',
                'hostname' => 'required|unique:servers',
                'provider_id' => 'required|exists:server_providers,id',
                'type_id' => 'required|exists:server_type,id',
                'provider_account_id' => 'required|exists:server_provider_accounts,id',
    
        ];
    }
}
