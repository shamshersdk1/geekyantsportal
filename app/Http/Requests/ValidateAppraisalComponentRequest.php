<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidateAppraisalComponentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(!$this->user()->hasRole('admin')){
            return false;
        }
        return true;    
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'appraisal_id'=>'required|exists:appraisals,id',
            'component_id'=>'required|exists:appraisal_component_types,id',
            'value' =>'required',

        ];
    }
}
