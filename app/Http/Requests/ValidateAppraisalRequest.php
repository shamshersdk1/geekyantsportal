<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Appraisal\AppraisalComponentType;
use App\Models\Appraisal\AppraisalBonusType;

class ValidateAppraisalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if($this->user()->hasRole('admin') || $this->user()->hasRole('human-resources')){
            return true;
        }
        return false;    
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules=[
            'user_id' => 'required | exists:users,id',
            'effective_date' => 'required | date',
            'type_id' => 'required | exists:appraisal_types,id',
            'component.*'=>'numeric',

        ];

        $appraisalBonus=$this->request->get('bonus');
            if(count($appraisalBonus) > 0){
                foreach($appraisalBonus as $key => $bonus){
                   
                    $rules['bonus.' . $key . '.value'] ='numeric';
                    $rules['bonus.' . $key . '.value_date'] ='date';

                }
            }
           
      
        return $rules;
        }
        public function messages()
        {
          $messages = [];
          $appraisalComponent=$this->request->get('component');
            if(count($appraisalComponent)>0){
                foreach($appraisalComponent as $key => $val)
                {
                  $appraisalComponent=AppraisalComponentType::find($key);
                  if($appraisalComponent){
                      $messages['component.'.$key.'.integer'] = 'Value must be integer for component '. $appraisalComponent->code;
                    //   $messages['component.'.'.exists'] = 'Component doesnot exists';
                  }
                }
            }
            $appraisalBonus=$this->request->get('bonus');
            if(count($appraisalBonus) > 0){
                foreach($appraisalBonus as $key => $bonus){
                    $appraisalBonusType=AppraisalBonusType::find($key);
                    if($appraisalBonus){
                        $messages['bonus.'.$key. '.value'.'.integer'] = 'Value must be integer for bonus '. $appraisalBonusType->description;
                        $messages['bonus.'.$key. '.value_date'.'.date'] = 'Value date must be of type date '. $appraisalBonusType->description;

                    }  
                }
            }
          
          return $messages;
        }
}
