<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Input;

class ValidateServerAccessRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(!$this->user()->hasRole('admin') &&  !$this->user()->hasRole('system-admin')){
            return false;
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        return [
                    'user_id' => 'required|exists:users,id',
                    'server_id' => 'required|exists:servers,id',
                    'account_id' => 'required|exists:server_accounts,id',
    
        ];
    }
    public function validationData($keys=null) 
    {
        $data = parent::all();
        $data['server_id'] = $this->route('serverId');
        return $data;
}
}

