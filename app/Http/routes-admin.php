<?php

Route::group(['roles' => ['admin']], function () {

    Route::get('it-saving/{financialId}/create/{userId?}', 'ItSavingController@create');
    Route::get('it-saving/{financialId}/create/{userId}/copy', 'ItSavingController@copyFromPrevious');
    Route::get('it-saving/month/', 'ItSavingController@monthUserList');
    Route::get('it-saving/month/{monthId}', 'ItSavingController@monthUserList');
    Route::get('it-saving/month/{monthId}/regenerate', 'ItSavingController@regenerate');
    Route::get('it-saving/{financial_year_id}', 'ItSavingController@userList');
    Route::get('it-saving/month/{monthId}/status-month', 'ItSavingController@lockMonth');

    Route::get('salary-review', 'SalaryReviewController@indexMis');
    Route::get('salary-review/{id}', 'SalaryReviewController@index');
    // Route::get('review-report', 'AdminReviewReport@index');
    // Route::get('mis-review-report', 'AdminReviewReport@MisReport');
    Route::get('mis-review-report-project', 'AdminReviewReport@MisProjectReport');
    Route::get('review-report-project', 'AdminReviewReport@ProjectReport');
    Route::get('download-review-report-project', 'AdminReviewReport@DownloadProjectReport');

    //goal-report
    Route::get('goal-report', 'GoalController@goalReport');
    Route::get('goal-report/month', 'GoalController@monthlyResult');
    Route::get('slack-channels', 'ProjectController@showSlackChannel');

    // Roles

    Route::resource('role', 'RoleController');

    // ----------- TO BE DECIDED. Currently in admin -------------------
    Route::get('download/{id}', 'ProjectReportController@download');
    Route::get('file/{filename}', function ($filename) {
        $path = '/uploads/project-files/' . $filename;
        if (!Storage::exists($path)) {
            abort(404);
        }
        $content = Storage::get($path);
        $type = Storage::mimeType($path);
        return response($content, 200)->header('Content-Type', $type);
    });
    //Route::get('test', 'TestController@index');
    // Reporting Managers
    Route::get('reporting-managers', 'UsersListController@getReportingManagers');

    Route::get('users/{userId}/tds-summary/{yearId?}', 'Tds\TdsController@index');

    Route::get('setting', 'SettingController@showSetting');
    Route::post('setting', 'SettingController@saveSetting');
    //TechTalk
    Route::get('tech-talk', 'TeckTalkController@index');
    //Meetups
    Route::get('meetups', 'MeetupsController@index');
    Route::get('meetups/create', 'MeetupsController@create');
    Route::get('meetups/view', 'MeetupsController@show');

    // Route::get('insurance', 'Admin\InsuranceController@index');
    //profile-builder
    Route::get('profile-builder', 'ProfileBuilderController@index');

    Route::get('profile-builder/{id}', 'ProfileBuilderController@show');

    // Route::get('insurance', 'Admin\InsuranceController@index');

    // Route::get('insurance/{id}', 'InsuranceController@show');
    // Route::get('insurance-list', 'InsuranceController@list');
    // Route::get('add-insurance', 'InsuranceController@add');

    // Bad Leads Filter
    Route::resource('bad-leads-filter', 'BadLeadsFilterController');
    // Hire Email
    Route::resource('hire-email', 'HireEmailController');
    Route::get('hire-email-review', 'HireEmailController@review');
    Route::get('hire-email-review/{id}', 'HireEmailController@reviewAction');
    //leads
    Route::get('lead/showstatus/{status}', 'LeadController@showstatus');
    Route::get('lead/viewconversation/{id}', 'LeadController@displayConversation');
    Route::get('lead/convert-lead/{id}', 'LeadController@convertLead');
    Route::resource('lead', 'LeadController');
    // Route::resource('project', 'ProjectController');
    Route::get('show-project-report/{id}', 'ProjectReportController@showProjectDetails');
    Route::resource('new-employee-report', 'EmployeeReportController@NewEmployeeReport');
    Route::resource('employee-report', 'EmployeeReportController');

    /*
    Project Quotation routes
     */
    Route::resource('project-quotation', 'ProjectQuotationController');
    Route::get('project-quotation/{quotationId}/{milestoneId}/show-milestone', 'ProjectQuotationController@showMilestone');
    Route::get('project-quotation/{quotaionId}/add-milestone', 'ProjectQuotationController@addMilestone');
    Route::delete('project-quotation/{milestoneId}/delete-milestone', 'ProjectQuotationController@deleteMilestone');
    Route::get('project-quotation/{quotationId}/{milestoneId}/edit-milestone', 'ProjectQuotationController@editMilestone');
    Route::post('project-quotation/{milestoneId}/update-milestone', 'ProjectQuotationController@updateMilestone');
    Route::post('project-quotation/store-milestone', 'ProjectQuotationController@storeMilestone');
    Route::get('project-quotation/get-sprints/{id}', 'ProjectQuotationController@getSprints');
    Route::get('project-quotation/get-milestone-sprints/{id}', 'ProjectQuotationController@getMilestoneSprints');
    Route::post('project-quotation/map-sprints', 'ProjectQuotationController@mapSprints');

    Route::get('template-management/document/{id}', 'TemplateManagementController@document');
    Route::get('Api/get-content/{id}', 'TemplateManagementController@contentApi');
    Route::post('template-management/download', 'TemplateManagementController@download');
    Route::resource('template-management', 'TemplateManagementController');
    Route::resource('new-timesheet/lock', 'Timesheet\UserTimesheetLockController');

    // Route::resource('users', 'UsersListController');
    Route::resource('events', 'EventController');
    Route::resource('user-events', 'UserEventController');
    Route::resource('user-events-attachment', 'UserEventAttachmentController');

    // Company Routes
    Route::resource('company', 'CompanyController');
    Route::get('company/{id}/create', 'CompanyController@createNewProject');

    Route::get('profile-status/{id}', 'ProfileController@toggleStatus');
    Route::get('attachments/download/{id}', 'UserEventAttachmentController@download');

    Route::get('news/{id}', 'NewsController@show');
    Route::post('news', 'NewsController@store');
    Route::put('news/{id}', 'NewsController@update');

    // ---------------------------------------------------------------
    // ------------------------- Dashboard Events -------------------------
    Route::get('dashboard-events', 'DashboardEventController@index');
    Route::post('dashboard-events', 'DashboardEventController@store');
    Route::get('dashboard-events/{id}', 'DashboardEventController@show');
    Route::delete('dashboard-events/{id}', 'DashboardEventController@destroy');
    Route::put('dashboard-events/{id}', 'DashboardEventController@update');

    Route::get('timelog/dashboard', 'TimelogController@dashboard');
    Route::get('timelog/history', 'TimelogController@history');
    Route::post('timelog/history', 'TimelogController@filter');
    Route::post('timelog/dashboard/dashboard-action', 'TimelogController@dashboardAction');

    //Employee Payslip
    // Route::get('employee-payslip/{id}', 'EmployeePayslipController@viewPayslip');

    //
    Route::get('employee-leave/history', 'EmployeeLeaveController@history');
    Route::get('employee-leave/apply', 'EmployeeLeaveController@apply');
    Route::get('employee-leave/{id}', 'EmployeeLeaveController@getLeave');

    // Route::post('employee-leave/requestLeave', 'EmployeeLeaveController@requestLeave');
    Route::post('employee-leave/status/{id}', 'EmployeeLeaveController@cancelLeave');

    Route::get('payroll', 'PayrollController@index');
    Route::post('payroll/group', 'PayrollController@groupStore');
    Route::delete('payroll/group/{id}', 'PayrollController@groupDelete');
    Route::get('payroll/group/{id}/rules', 'PayrollController@rules');
    Route::get('payroll/group/{groupid}/rules/{id}/edit', 'PayrollController@editRule');
    Route::put('payroll/group/{groupid}/rules/{id}', 'PayrollController@updateRule');
    Route::post('payroll/group/{id}/rules', 'PayrollController@storeRule');
    Route::delete('payroll/rule/{id}', 'PayrollController@deleteRule');
    Route::get('payroll/user/{id}/download', 'PayrollController@download');
    Route::get('payroll/user/{id}/viewpdf', 'PayrollController@viewpdf');

    Route::resource('payroll-rules-csv', 'PayrollRuleForCsvController');

    Route::get('logs', 'LogController@index');
    Route::get('logs/{id}', 'LogController@show');

    Route::resource('slack-access-logs', 'SlackAccessLogController');

    Route::resource('skill', 'UserTechnologiesController');
    Route::post('skill/accept/{id}', 'UserTechnologiesController@accept');
    Route::post('skill/reject/{id}', 'UserTechnologiesController@reject');
    Route::get('resources', 'ResourceController@index');
    Route::get('resources/my-resource', 'ResourceController@myResource');

    Route::get('dashboard-events', 'DashboardEventController@index');
    Route::post('dashboard-events', 'DashboardEventController@store');
    Route::get('dashboard-events/{id}', 'DashboardEventController@show');
    Route::delete('dashboard-events/{id}', 'DashboardEventController@destroy');
    Route::put('dashboard-events/{id}', 'DashboardEventController@update');

    // TIMESHEET
    Route::get('timesheet/all', 'TimesheetController@showAll');
    Route::get('leads', function () {
        return View::make('pages/admin/leads/index');
    });

    //MIS Leave
    Route::get('leave-section/mis-leave-report', 'LeaveMisController@index');
    Route::get('leave-section/mis-leave-report/{month_id}/{userId}', 'LeaveMisController@yearOverview');
    Route::get('leave-section/mis-leave-report/{month_id}', 'LeaveMisController@monthOverview');

    //leave Deductions
    Route::get('leave-deduction', 'LeaveDeductionMonthController@index');
    Route::get('leave-deduction/{month_id}', 'LeaveDeductionMonthController@monthDeductions');
    Route::post('leave-deduction/{month_id}/approve', 'LeaveDeductionMonthController@approveMonthDeductions');
    //Leave Balance Manager
    // Route::get('leave-section/leave-balance-manager', 'LeaveBalanceManagerController@yearlyOverview');
    // Route::get('leave-section/leave-balance-manager/{userId}', 'LeaveBalanceManagerController@userLeaveOverview');
    // Route::get('leave-section/leave-balance-manager/{userId}/balance-form', 'LeaveBalanceManagerController@balanceForm');
    // Route::post('leave-section/leave-balance-manager/{userId}/balance-form/saveForm', 'LeaveBalanceManagerController@saveForm');

    Route::get('checklist', 'ChecklistController@index');

    Route::resource('slack-user-map', 'SlackMapController');
    Route::get('loans/dashboard', 'LoanController@viewDashboard');
    Route::get('feedback/report', 'FeedbackReviewController@months');
    Route::get('feedback/report/{id}', 'FeedbackReviewController@report');
    Route::get('feedback/report/summary/{month_id}', 'FeedbackReviewController@showSummary');
    Route::get('feedback/report/{month_id}/{user_id}', 'FeedbackReviewController@showAdmin');
    //Department
    Route::resource('department', 'DepartmentController');
    // Expense Head
    Route::resource('expense-head', 'ExpenseHeadController');

    // Payment Methods
    Route::resource('payment-method', 'PaymentMethodController');
    Route::resource('all-network-devices', 'MyNetworkDeviceController');

    Route::resource('leave-deduction-month', 'LeaveDeductionMonthController');
    Route::get('leave-deduction-month/{monthId}/regenerate', 'LeaveDeductionMonthController@regenerate');
    Route::get('leave-deduction-month/{monthId}/status-month', 'LeaveDeductionMonthController@monthStatus');
    Route::get('leave-deduction-month/{monthId}/ignore', 'LeaveDeductionMonthController@ignore');
    Route::get('leave-deduction-month/{monthId}/forward', 'LeaveDeductionMonthController@forward');
    Route::get('leave-deduction-month/{monthId}/delete', 'LeaveDeductionMonthController@delete');

    Route::get('prep-salary/salary-sheet-comparison/{monthId?}', 'PrepSalary\PrepSalaryController@salarySheetCompare');
    Route::get('prep-salary/salary-sheet-comparison/{prepSalId}/download', 'PrepSalary\PrepSalaryController@salarySheetCompareDownload');

    Route::get('prep-salary/{id}/discard', 'PrepSalary\PrepSalaryController@discardPrepSalary');
    Route::get('prep-salary/{id}/salary-sheet', 'PrepSalary\PrepSalaryController@prepSalarySheet');
    Route::get('prep-salary/{id}/finalize', 'PrepSalary\PrepSalaryController@finalize');
    Route::get('prep-salary/{id}/payslip/{userId?}', 'PrepSalary\PrepSalaryController@generatePayslip');
    Route::get('prep-salary/{id}/salary/{userId}', 'PrepSalary\PrepSalaryController@salaryStore');
    Route::get('prep-salary/{id}/lock', 'PrepSalary\PrepSalaryController@lockMonth');
    Route::post('prep-salary/create', 'PrepSalary\PrepSalaryController@storeSalary');
    Route::resource('prep-salary', 'PrepSalary\PrepSalaryController');
    Route::resource('prep-salary-component-type', 'PrepSalary\PrepSalaryComponentTypeController');
    Route::get('prep-salary/{id}/generate', 'PrepSalary\PrepSalaryController@generate');
    Route::get('prep-salary/{id}/view', 'PrepSalary\PrepSalaryController@getView');
    Route::get('prep-salary/{id}/download', 'PrepSalary\PrepSalaryController@downloadCSV');
    Route::get('prep-salary/{id}/tds', 'PrepSalary\PrepSalaryController@viewTds');
    Route::get('prep-salary/{id}/tds/download', 'PrepSalary\PrepSalaryController@downloadCSVTds');
    Route::get('prep-salary/{id}/salary-sheet/download', 'PrepSalary\PrepSalaryController@prepSalarySheetDownload');
    Route::get('prep-users/{id}', 'PrepSalary\PrepSalaryController@getUsers');
    Route::post('prep-users/{id}', 'PrepSalary\PrepSalaryController@setUsers');
    Route::get('prep-users/{prepSalaryId}/{monthId}/status-month', 'PrepSalary\PrepSalaryController@statusMonth');
    Route::get('prep-salary/{id}/generate-all', 'PrepSalary\PrepSalaryController@generateAll');

    Route::get('prep-salary/{prepSalaryId}/{prepComponentId}/working-day', 'PrepSalary\PrepWorkingDayController@show');

    //Bank Transfer Hold
    Route::get('bank-transfer/{bankTransferId}/hold', 'BankTransfer\BankTransferController@getHolds');
    Route::get('bank-transfer/{bankTransferId}/hold/{bankHoldId}', 'BankTransfer\BankTransferController@release');

    //Bank Transfer / Hold
    Route::get('bank-transfer/{monthId?}', 'BankTransfer\BankTransferController@index');
    Route::put('bank-transfer/{bankTransferUserId}/save', 'BankTransfer\BankTransferController@saveTransfer');
    Route::post('bank-transfer/save-all', 'BankTransfer\BankTransferController@saveAllTransfer');
    Route::get('bank-transfer/{bankTransferUserId}/complete', 'BankTransfer\BankTransferController@completeTransfer');
    Route::get('bank-transfer/{bankTransferUserId}/complete-all', 'BankTransfer\BankTransferController@completeAllTransfer');
    Route::get('bank-transfer/{bankTransferUserId}/reject', 'BankTransfer\BankTransferController@rejectTransfer');
    Route::get('bank-transfer/{bankTransferUserId}/reject-all', 'BankTransfer\BankTransferController@rejectAllTransfer');
    Route::get('bank-transfer/{bankTransferId}/view-complete', 'BankTransfer\BankTransferController@complete');
    Route::post('bank-transfer/{bankTransferId}/view-complete/upload', 'BankTransfer\BankTransferController@upload');
    Route::get('bank-transfer/{monthId}/download', 'BankTransfer\BankTransferController@downloadCSV');
    Route::resource('bank-transfer', 'BankTransfer\BankTransferController');
    Route::get('bank-transfer/{monthId}/lock', 'BankTransfer\BankTransferController@lock');
    Route::get('bank-transfer/{id}/salary-sheet', 'BankTransfer\BankTransferController@showSalarySheet');
    Route::get('bank-transfer/{id}/salary-sheet/download', 'BankTransfer\BankTransferController@downloadSalarySheet');
    //Salary
    Route::get('salary/{monthId?}', 'Salary\SalaryTransferController@downloadCSV');
    // Route::get('salary-transfer', 'Salary\SalaryTransferController@index');
    Route::get('salary-transfer/{monthId?}', 'Salary\SalaryTransferController@index');
    Route::post('salary-transfer/preview', 'Salary\SalaryTransferController@preview');
    Route::post('salary-transfer/{monthId}/preview', 'Salary\SalaryTransferController@preview');
    Route::post('salary-transfer/finalise', 'Salary\SalaryTransferController@finaliseTransfer');
    Route::get('salary-transfer/{monthId}/bank-data', 'Salary\SalaryTransferController@getBankData');

    //Transaction summary
    Route::post('transaction-summary/store', 'Finance\TransactionController@store');
    Route::get('transaction-summary/create', 'Finance\TransactionController@create');
    Route::get('transaction-summary/{id?}', 'Finance\TransactionController@summary');

    //Transaction Summary Company
    Route::post('transaction-summary-company/store', 'Finance\TransactionCompanyController@store');
    Route::get('transaction-summary-company/create', 'Finance\TransactionCompanyController@create');
    Route::get('transaction-summary-company/{id?}', 'Finance\TransactionCompanyController@summary');

    Route::get('bonus-confirmed', 'BonusController@getConfirmedBonus');
    Route::get('audit', 'AuditController@index');
    Route::get('audit/report', 'AuditController@report');

    //Policy
    Route::get('policy/update-status/{id}', 'Policy\PolicyController@updateStatus');
    Route::resource('policy', 'Policy\PolicyController');
    //------------------------------------------------------------------
    require_once "routes-system-admin.php";
    //------------------------------------------------------------------

    require_once "routes-event-manager.php";

    //------------------------------------------------------------------
    require_once "routes-system-admin.php";
    //------------------------------------------------------------------

    require_once "routes-event-manager.php";

    require_once "routes-ref-generator.php";

    require_once "routes-rm.php";

    // Common routes for Account and HR

    require_once "routes-account.php";

    require_once "routes-hr.php";

    require_once "routes-office-admin.php";

    require_once "routes-cms.php";

    require_once "routes-calender.php";

    require_once "routes-management.php";

});
