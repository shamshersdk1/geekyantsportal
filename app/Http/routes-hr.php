<?php
Route::group(['roles' => ['human-resources']], function () {
    // ------------------------- USER CRUD -------------------------
    Route::resource('late-comers', 'LateComerController');
    Route::resource('users', 'UsersListController');
    Route::get('users/{id}/financial-transactions', 'UserViewController@showFinancialTransactions');
    Route::get('users/{id}/financial-transactions/download', 'UserViewController@downloadFinancialTransactions');
    Route::get('users/{id}/profile', 'UserViewController@showProfile');
    Route::get('users/{id}/profile/edit', 'UserViewController@editProfile');
    Route::put('user-status/{id}', 'UsersListController@toggle');
    Route::delete('users/{techid}/{userid}/deleteTechnology', 'UsersListController@deleteTechnology');
    Route::post('users/{id}/addTechnology', 'UsersListController@addTechnology');
    Route::get('user-profile/create/{id}', 'UsersListController@addUserProfile');
    Route::get('search-user', 'UsersListController@search');
    Route::get('users/{id}/salary-breakdown', 'UserViewController@showSalary');
    Route::get('users/{id}/payslips', 'UserViewController@showPayslips');
    Route::get('users/{id}/leaves', 'UserViewController@showLeaves');
    Route::get('users/{id}/loans', 'UserViewController@showLoans');
    Route::get('users/{id}/loans/{lid}', 'UserViewController@showEMIadmin');
    Route::get('users/{id}/bonuses', 'UserViewController@showBonuses');
    Route::resource('users/{id}/appraisals', 'AppraisalController');

    //Leave Overview
    Route::get('leave-section/leave-report', 'LeaveReportController@yearList');
    Route::get('leave-section/leave-report/{yearId}', 'LeaveReportController@yearlyOverview');
    Route::get('leave-section/leave-report/{yearId}/download', 'LeaveReportController@downloadLeaveOverviewCSV');

    //Leave Balance Manager
    Route::get('leave-section/leave-balance-manager', 'LeaveBalanceManagerController@yearList');
    Route::get('leave-section/leave-balance-manager/{yearId}', 'LeaveBalanceManagerController@yearlyOverview');
    Route::get('leave-section/leave-balance-manager/{yearId}/download', 'LeaveBalanceManagerController@downloadLeaveOverviewCSV');
    Route::get('leave-section/leave-balance-manager/{yearId}/{userId}', 'LeaveBalanceManagerController@userLeaveOverview');
    Route::get('leave-section/leave-balance-manager/{yearId}/{userId}/download', 'LeaveBalanceManagerController@downloadUserLeaveTransactionCSV'); //resign
    Route::get('leave-section/leave-balance-manager/{yearId}/{userId}/settle-leaves', 'LeaveBalanceManagerController@settleUserLeaves'); //resign
    Route::get('leave-section/leave-balance-manager/{yearId}/{userId}/balance-form', 'LeaveBalanceManagerController@balanceForm');
    Route::post('leave-section/leave-balance-manager/{yearId}/{userId}/balance-form/saveForm', 'LeaveBalanceManagerController@saveForm');

    //all bonus
    Route::get('bonus', 'BonusController@allBonuses');
    Route::get('bonus/{monthId}', 'BonusController@allBonusesMonth');
    Route::get('bonus/{monthId}/{userId}', 'BonusController@allBonusesMonthUser');

    //due bonus
    Route::get('bonus-due/lock', 'BonusController@lockBonusApproval');
    Route::post('bonus-due/{userId}/approve', 'BonusController@userDueBonusesApprove');
    Route::get('bonus-due/{monthId?}', 'BonusController@dueBonuses');
    Route::get('bonus-due/{userId}/user', 'BonusController@userDueBonuses');
    Route::get('bonus-due-user/{monthId?}', 'BonusController@dueBonusesUser');

    //paid bonus
    Route::get('bonus-paid', 'BonusController@paidBonuses');
    Route::get('bonus-paid/{monthId}', 'BonusController@paidBonusesMonth');
    Route::get('bonus-paid/{monthId}/{userId}', 'BonusController@paidBonusesMonthUser');

    /* Leaves */
    Route::get('leave-section/confirmation', 'AdminLeaveRequestController@index');
    Route::get('leave-section/filter/{type}', 'AdminLeaveRequestController@index');
    Route::post('leave-section/status/{id}', 'AdminLeaveRequestController@leaveStatus');
    Route::put('leave-section/update/{id}', 'AdminLeaveRequestController@updateLeave');
    Route::put('leave-section/reject/{id}', 'AdminLeaveRequestController@rejectLeave');
    Route::post('leave-section', 'AdminLeaveRequestController@createNewLeave');
    Route::resource('leave-section/compoff', 'CompOffController');
    Route::get('leave-report', 'AdminLeaveReportController@index');
    Route::get('leave-section/report-csv', 'AdminLeaveReportController@exportCsv');
    Route::get('leave-section/employee-leave-report', 'AdminLeaveReportController@employeeLeaveList');
    Route::get('leave-section/mis-report', 'AdminLeaveReportController@generateLeaveMisReport');
    Route::get('leave-section/overview', 'LeaveOverviewController@index');
    Route::get('leave-status/{id}', 'AdminLeaveRequestController@getLeave');
    Route::get('leave-section/user-leave-report', 'UserLeaveReportController@index');
    Route::get('leave-section/{id}', 'AdminLeaveRequestController@getLeave');

    // Seat Management
    Route::resource('seat-management', 'FloorController');
    Route::resource('floors-seat', 'FloorsSeatController');

    //Attendance Report
    Route::get('attendance', 'UserLeaveReportController@index');
    Route::get('attendance/downloadExcel', 'UserLeaveReportController@downloadExcel');

    //Loan Requests
    Route::get('submitted-loan-requests', 'LoanRequestController@getSubmittedRequests');
    Route::get('reconciled-loan-requests', 'LoanRequestController@getReconciledRequests');
    Route::get('submitted-loan-requests/{id}', 'LoanRequestController@showSubmittedRequest');
    Route::get('reconciled-loan-requests/{id}', 'LoanRequestController@showReconciledRequest');
    Route::get('calender-event', 'CalendarEventController@index');
    // Designation
    Route::resource('designations', 'DesignationController');
    // Insurance Policy Managers

    // For Attendance Reporting
    Route::get('users/{userId}/attendance-register/{monthId?}', 'UserAttendaneController@userView');

    Route::post('attendance-register/{monthId}/date', 'UserAttendaneController@dateView');
    Route::get('attendance-register/{monthId?}', 'UserAttendaneController@monthView');

    Route::get('attendance-overview', 'UserAttendaneController@index');
    Route::post('attendance-update', 'UserAttendaneController@markAs');
    Route::get('attendance-overview-month', 'UserAttendaneController@view');

    //Appraisal
    Route::resource('appraisal/appraisal-bonus-type', 'Appraisal\AppraisalBonusTypeController');
    Route::resource('appraisal/appraisal-type', 'Appraisal\AppraisalTypeController');
    Route::resource('appraisal/appraisal-component-type', 'Appraisal\AppraisalComponentTypeController');
    Route::get('appraisal/add', 'Appraisal\AppraisalController@addAppraisal');
    Route::post('appraisal/add', 'Appraisal\AppraisalController@storeAppraisal');
    Route::resource('appraisal', 'Appraisal\AppraisalController');

    //insurance
    Route::get('insurance-policy-user-report', 'Insurance\InsurancePolicyController@userReport');

    //Prepare Salary
    Route::get('payslips/{monthId?}', 'PrepSalary\PrepSalaryController@showPayslips');
    Route::get('payslips/{monthId}/{user_id}', 'PrepSalary\PrepSalaryController@showUserPayslip');
    Route::get('payslips/{monthId}/{user_id}/download', 'PrepSalary\PrepSalaryController@downloadUserPayslip');

    //TDS Breakdown
    Route::get('tds-breakdown/{monthId}/{user_id}', 'Tds\TdsController@showUserTDS');
    Route::get('tds-breakdown/{monthId}/{user_id}/download', 'Tds\TdsController@downloadUserTDS');
});
