<?php
            Route::group(['roles' => ['cms-manager']], function () {
                // ------------------------- CMS Management -------------------------
                Route::resource('cms-project', 'CmsProjectController');
                Route::resource('cms-technology', 'CmsTechnologyController');
                Route::resource('cms-developers', 'CmsDevelopersController');
                Route::resource('cms-technology-developers', 'CmsTechnologyDevelopersController');
                Route::get('cms-technology-developers/create/{id}', 'CmsTechnologyDevelopersController@create');
                Route::resource('cms-slides', 'CmsSlideController');
                Route::get('cms-slides/create/{id}', 'CmsSlideController@create');
                Route::resource('tech-talk-videos','TechTalkVideoController');
                // ---------------------------------------------------------------
            });