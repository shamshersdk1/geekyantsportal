<?php 
namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel {

	/**
	 * The application's global HTTP middleware stack.
	 *
	 * @var array
	 */
	protected $middleware = [
		'Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode',
		'Illuminate\Cookie\Middleware\EncryptCookies',
		'Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse',
		'Illuminate\Session\Middleware\StartSession',
		'Illuminate\View\Middleware\ShareErrorsFromSession',
		'App\Http\Middleware\HttpsProtocol',
	];

	/**
	 * The application's route middleware.
	 *
	 * @var array
	 */
	protected $routeMiddleware = [
		'auth' => 'App\Http\Middleware\Authenticate',
		'admin' => 'App\Http\Middleware\AdminMiddleware',
		'session' => 'App\Http\Middleware\AuthenticateSession',
		//'auth' => \Illuminate\Auth\Middleware\Authenticate::class,
		'auth.basic' => 'Illuminate\Auth\Middleware\AuthenticateWithBasicAuth',
		'guest' => 'App\Http\Middleware\RedirectIfAuthenticated',
		'mobile' => 'App\Http\Middleware\MobileMiddleware',
		'not.mobile' => 'App\Http\Middleware\NotMobileMiddleware',
		'mobile.auth' => 'App\Http\Middleware\MobileAuthenticate',
		'capture.route' => 'App\Http\Middleware\CaptureRouteMiddleware',
		'roleCheck' => 'App\Http\Middleware\Role',
		'api_token'=>'App\Http\Middleware\ApiToken'
	];

}
