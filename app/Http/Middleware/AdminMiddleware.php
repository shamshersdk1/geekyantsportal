<?php
namespace App\Http\Middleware;
use Closure;
use Redirect;
use Illuminate\Http\Request;
class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       
        $userObject = \Auth::user();
        $giveAccess = false;
        $count = false;
        $case = 0;
        if ($userObject->role == "admin") {
            $case = 1;
        } 
        if ( count( $userObject->roles) > 0 && $userObject->role !== "admin" ) {
            $case = 2;
            foreach ($userObject->roles as $role) {
                if (strpos($request->route()->getPath(), 'admin/'.$role->code) !== false) {
                    $case = 3;
                }
            }
        }
        switch ($case) {
            case 1:
                return $next($request);
                break;
            case 2:
                return Redirect::to('/admin/dashboard');
                break;
            case 3:
                return $next($request);
                break;
            default:
                return Redirect::to('/user');
        }
    }
}
