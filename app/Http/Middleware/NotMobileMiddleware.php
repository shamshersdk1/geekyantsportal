<?php

namespace App\Http\Middleware;

use Closure;
use Redirect;
use Illuminate\Http\Request;
use App\Helpers\Sys;

class NotMobileMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        \Log::info("NotMobileMiddleware called");  
        $userObject = \Auth::user();
        $isMobile = Sys::checkIsMobile();

        if ($isMobile ){ // not mobile 
            \Log::info("web url hitting from mobile");  
            return redirect::to('/mobile');      
        }else{ // if mobile
            \Log::info("web url hitting from web");
            return $next($request);
        }
    }
}
