<?php

namespace App\Http\Middleware;

use Closure;

class ApiToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if($request->api_token!=env('TOKEN_DATA_BLR')){
            return response()->json('UnAuthorized access',401);
            }
        return $next($request);
    }
}
