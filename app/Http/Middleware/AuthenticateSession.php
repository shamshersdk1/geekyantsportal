<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AuthenticateSession 
{

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @param  string|null  $guard
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{

		$request->user = \Auth::check() ? \Auth::user() : NULL;
		if($request->user->is_active == 0){
			Auth::logout();
		}

		return $next($request);
	}
}