    <?php
//  -- SYSTEM ADMIN --
Route::group(['roles' => ['system-admin']], function () {

    // ------------------------- Credentials -------------------------
    Route::post('shared-credential/new', 'CredentialController@updateCredential');
    Route::post('shared-credential/share', 'CredentialController@shareCredential');

    // --------------------------------------------------------------
    // SSH KEY
    Route::resource('ssh-keys', 'SshKeyController');
    Route::resource('ip-address-range', 'IpAddressRangeController');
    Route::resource('assign-ip-address', 'UserIpAddressController');

    //Server Management
    Route::resource('server-management/server-type', 'ServerManagement\ServerTypeController');
    Route::resource('server-management/server-provider', 'ServerManagement\ServerProvidersController');
    Route::resource('server-management/server-provider/{id}/server-acccount', 'ServerManagement\ServerProviderAccountsController');
    Route::resource('server-management/server', 'ServerManagement\ServerController');
    Route::get('server-management/server/create/{providerId}', 'ServerManagement\ServerController@create');
    Route::get('server-management/server/edit/{id}/{providerId}', 'ServerManagement\ServerController@edit');
    Route::resource('server-management/server/{serverId}/manage-access', 'ServerManagement\ServerAccessController');
    Route::post('server-management/server/{serverId}/add-server-account', 'ServerManagement\ServerAccessController@saveServerAccount');
    Route::delete('server-management/server/{serverId}/{accountId}/delete-account', 'ServerManagement\ServerAccessController@deleteAccount');
    Route::delete('server-management/server/{serverId}/{serverAccountAccessId}/delete', 'ServerManagement\ServerAccessController@destroy');
    Route::get('server-management/server/{serverId}/{serverAccountAccessId}/edit', 'ServerManagement\ServerAccessController@edit');
    Route::post('server-management/server/{serverId}/{serverAccountAccessId}/update', 'ServerManagement\ServerAccessController@update');
    Route::get('server-management/server/{serverId}/manage', 'ServerManagement\ServerController@manage');
    Route::get('server-management/server/{serverId}/delete', 'ServerManagement\ServerController@destroy');
    Route::post('server-management/server/{serverId}/add-note', 'ServerManagement\ServerNoteController@add');
    Route::post('server-management/server/{serverId}/add-meta', 'ServerManagement\ServermetaController@add');
    Route::post('server-management/server/{serverId}/delete-meta/{metaId}', 'ServerManagement\ServermetaController@delete');
});
