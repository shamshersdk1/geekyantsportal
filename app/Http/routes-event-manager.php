<?php
//  -- EVENT MANAGER --
            Route::group(['roles' => ['event-manager']], function () {
                Route::resource('tech-events', 'TechEventsController');
            });