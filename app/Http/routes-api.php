<?php
/**
 *|---------------------------------------------------------------------------
 *| Leave Api Routes
 *|---------------------------------------------------------------------------
 */
Route::group(['prefix' => '/api/v1/', 'namespace' => 'Api\V1'], function () {

    // user leave api
    //Route::resource('leave', 'LeaveApiController'); // api
    Route::resource('leave/optional-holiday', 'Leave\UserLeaveController@getOptionalHolidays'); // api
    Route::resource('leave', 'Leave\UserLeaveController'); // api
    Route::post('sick-leave', 'Leave\UserLeaveController@applySick'); // api
    Route::get('sick-leave-warnings', 'Leave\UserLeaveController@sickLeaveWarning'); // api

    Route::get('leave-type', 'Leave\LeaveTypeController@index'); // api
    Route::resource('user', 'UserController'); // api
    Route::get('myUsers', 'UserController@getMyUsers'); // api
    Route::post('user/billable', 'UserController@updateBillable');
    Route::post('user/feedback-review', 'UserController@updateFeedbackReview');
    Route::post('user/rm-notification', 'UserController@updateRMNotificationReminder');
    Route::get('user/leaves/check', 'LeaveApiController@checkLeaveProjects'); // api
    Route::get('user-all-active', 'UserController@getAllActiveUsers'); // api
    Route::post('user/leaves/overlapping-request/leave', 'LeaveApiController@saveOverlapLeave'); //api

    Route::post('user/leaves/overlapping-request/reasons', 'LeaveApiController@saveOverlapRequest'); //api

    Route::post('leave-request', 'LeaveApiController@leaveRequest'); //api

    //Route::post('user/ap', 'UserApController@store');
    //Route::post('binding', 'IpMacBindingController@store');

    // admin leave api
    // Route::post('leave', 'LeaveApiController@store');
    Route::post('leave/balance', 'LeaveApiController@getBalance');
    //Route::get('leave/balance', 'LeaveApiController@getBalance');
    // Route::get('leave/{id}', 'LeaveApiController@getLeaveDetails');
    Route::put('leave/{id}/reject', 'LeaveApiController@rejectLeave');
    Route::put('leave-new/{id}/reject', 'Leave\UserLeaveController@rejectLeave');
    Route::put('leave/{id}/approve', 'LeaveApiController@approveLeave');
    Route::put('leave-new/{id}/approve', 'Leave\UserLeaveController@approveLeave');
    Route::put('leave/{id}/cancel', 'LeaveApiController@cancelLeave');
    Route::put('leave-new/{id}/cancel', 'Leave\UserLeaveController@cancelLeave');
    Route::put('leave/{id}/update', 'LeaveApiController@updateLeave');
    Route::put('leave-new/{id}/update', 'Leave\UserLeaveController@updateLeave');

    Route::post('timesheet', 'TimesheetApiController@store');
    Route::post('admin/timesheet', 'AdminTimesheetApiController@store');
    Route::post('timesheet_detail', 'TimesheetDetailApiController@store');
    Route::delete('timesheet_detail/{id}', 'TimesheetDetailApiController@delete');
    Route::post('timesheet/update-data', 'TimesheetApiController@updateTimesheetDetails');
    Route::get('user-timesheet', 'UserTimesheetController@index');
    Route::get('get-user-timesheet/{monthId}/{userId}', 'UserTimesheetController@getUserTimesheet');
    Route::get('get-user-weekly-timesheet/{weekId}/{userId}', 'UserTimesheetController@getUserWeeklyTimesheet');

    Route::post('slack-message', 'Slack\SlackMessageApiController@sendMessageToUser');
    Route::post('slack-message-on-channel', 'Slack\SlackMessageApiController@sendMessageToChannel');
    Route::get('/user-mismatch-list', 'ChecklistApiController@userMismatchList');
    Route::resource('/portal-from-slack', 'Slack\SlashCommandController');
    Route::resource('/slack-interactive-components', 'Slack\SlackInteractiveComponentsController');
    // Route::post('/assign-project-from-slack', "Slack\InteractiveMessageController@setProject");

    Route::resource('slack-user-map', 'AdminSlackMapController');
    Route::get('user-mismatch-list', 'ChecklistApiController@userMismatchList');
    Route::get('project-without-resources', 'ChecklistApiController@projectWithoutResources');
    Route::get('channels-without-projects', 'ChecklistApiController@channelsWithoutProjects');

    Route::resource('payroll-csv-data', 'PayslipCsvDataController');
    Route::get('loan/deduction', 'LoanController@deduction');
    Route::resource('loan', 'LoanController');

    Route::get('bonus/{monthYear}/get-user-bonus/{projectId}', 'BonusController@fetchUserBonus');

    Route::resource('bonus-request', 'BonusRequestController');
    Route::get('bonus-request-count', 'BonusRequestController@getCounts');
    Route::post('bonus-request-reject', 'BonusRequestController@markAsRejected');
    Route::post('bonus-request-approve', 'BonusRequestController@markAsApproved');
    Route::get('bonus-request/lock-month/{id}', 'BonusRequestController@lockMonth');
    Route::post('bonus-request/bonus-approve-payment', 'BonusRequestController@bonusApprovePayment');
    Route::post('bonus-request/get-user-detail', 'BonusRequestController@getUserDetails');

    // bonus-request-projects
    Route::post('bonus-request-projects', 'BonusController@addProjectsToBonus');
    Route::get('bonus/get-calender/{month}/{year}', 'BonusController@getCalender');
    Route::post('bonus/apply-on-site-bonus', 'BonusController@addOnSiteBonus');
    Route::delete('bonus/delete-on-site-bonus/{id}', 'BonusController@deleteOnSiteBonus');
    Route::delete('bonus/delete-bonus-request/{id}', 'BonusController@deleteBonusRequest');
    Route::post('bonus/apply-additional-working-bonus', 'BonusController@addAdditionalWorkingBonus');
    Route::delete('bonus/delete-additional-working-bonus/{id}', 'BonusController@deleteAdditionalWorkingBonus');
    Route::get('bonus/{id}/approve-payment', 'BonusController@approvePayment');
    Route::resource('bonus', 'BonusController');
    Route::resource('appraisal', 'AppraisalController');
    Route::get('payroll-csv-data/{id}/create-csv', 'PayslipCsvDataController@createCsv');
    Route::get('payroll-csv-data/{id}/download', 'PayslipCsvDataController@download');

    //Onsite Bonus
    Route::get('onsite-bonus', 'BonusRequest\OnsiteBonusController@index');
    Route::post('onsite-bonus', 'BonusRequest\OnsiteBonusController@store');
    Route::delete('onsite-bonus/{id}', 'BonusRequest\OnsiteBonusController@delete');

    Route::get('onsite-bonus/{id}/approve', 'BonusRequest\OnsiteBonusController@approve');
    Route::get('onsite-bonus/{id}/reject', 'BonusRequest\OnsiteBonusController@reject');
    Route::get('onsite-bonus/{id}/cancel', 'BonusRequest\OnsiteBonusController@cancel');

    // CMS DEVELOPER TECHNOLOGY
    Route::post('cms-developers/status', 'CmsDeveloperApiController@updateStatus');
    Route::post('cms-developers-technology/status', 'CmsDeveloperApiController@updateDeveloperStatus');
    Route::post('cms-slides/status', 'CmsDeveloperApiController@updateSlideStatus');
    // Seat
    // Route::post('seats', 'SeatsApiController@store');
    // Route::post('seats/release', 'SeatsApiController@release');

    Route::resource('seats', 'SeatsApiController');
    // Attendance
    Route::post('attendance', 'AttendanceApiController@store');

    Route::resource('user-tasks', 'UserTaskController');
    Route::resource('shared-tasks', 'SharedTaskController');

    Route::post('portal-from-slack-test', function () {
        \Log::info("test reply worked!!!");
        return "worked";
    });
    // User Contact Status
    Route::post('user-contacts/status', 'UserContactApiController@updateStatus');
    Route::post('user-contacts/delete', 'UserContactApiController@deleteUserContact');
    // Credential Sharing
    Route::post('shared-users', 'CredentialApiController@getSharedUsers');
    Route::post('credential-sharing', 'CredentialApiController@saveSharedUsers');
    Route::post('credential-sharing-delete', 'CredentialApiController@deleteSharedUser');
    Route::delete('credential/{id}', 'CredentialApiController@deleteCredential');

    Route::get('goals/filter', 'GoalController@filterGoals');
    Route::resource('goals', 'GoalController');
    Route::resource('user-goal-items', 'UserGoalItemController');
    Route::resource('tags', 'TagController');
    Route::get('user-goals/{id}/get-items', 'UserGoalController@getItems');
    Route::resource('user-goals', 'UserGoalController');
    Route::put('user-goal-items/review/{id}', 'UserGoalItemController@updateReview');
    Route::put('user-goal-items/points/{id}', 'UserGoalItemController@updatePoints');
    Route::resource('comment', 'CommentController');
    Route::get('comments', 'CommentController@getComments');
    Route::post('comments', 'CommentController@store');

    //Route::resource('user', 'UserController');

    // Company

    Route::get('company', 'CompanyController@index');
    // Late comers
    Route::resource('late-comers', 'LateComerController');

    // Tech Event Status
    Route::post('tech-events/status', 'TechEventController@updateStatus');
    // Reference Number Status
    Route::post('reference-number/cancel', 'ReferenceNumberController@cancel');
    Route::post('request-approval/approve', 'RequestApprovalController@approve');
    Route::post('request-approval/reject', 'RequestApprovalController@reject');

    Route::get('new-timesheet/projects', 'Timesheet\UserTimesheetController@getProjects');
    Route::resource('new-timesheet', 'Timesheet\UserTimesheetController');
    Route::resource('tmp-timesheet', 'TmpTimesheetController');
    Route::get('new-timesheet/managed/projects', 'Timesheet\UserTimesheetController@getManagedProjects');
    Route::put('new-timesheet/approval/{id}', 'Timesheet\UserTimesheetController@updateApproveDetails');
    Route::put('new-timesheet/reject/{id}', 'Timesheet\UserTimesheetController@updateRejectDetails');
    Route::get('new-timesheet/get-timesheet-project', 'Timesheet\UserTimesheetController@getProjectByTimesheet');
    Route::post('user-timesheet/approve-weekly/{userId}/{weekId}', 'Timesheet\UserTimesheetWeekController@approve');
    Route::get('user-timesheet-month/{monthId}/{userId}', 'Timesheet\UserTimesheetExtraController@getTimesheet');
    Route::get('user-timesheet-week/{weekId}/{userId}', 'Timesheet\UserTimesheetWeekController@getTimesheet');
    Route::get('user-timesheet/get-pending-timesheet', 'Timesheet\UserTimesheetController@getPendingTimesheet');
    Route::resource('user-timesheet-extra', 'Timesheet\UserTimesheetExtraController');

    Route::get('additional-work-days-bonus/{id}', 'BonusRequest\AdditionalWorkingDayBonusController@show');
    Route::delete('additional-work-days-bonus/{id}', 'BonusRequest\AdditionalWorkingDayBonusController@delete');
    Route::post('additional-work-days-bonus', 'BonusRequest\AdditionalWorkingDayBonusController@store');
    Route::post('additional-work-days-bonus/get-timesheet-detail', 'BonusRequest\AdditionalWorkingDayBonusController@getTimesheetDetail');

    Route::get('additional-work-days-bonus/{id}/approve', 'BonusRequest\AdditionalWorkingDayBonusController@approve');
    // Route::post('user-timesheet-extra', 'Timesheet\UserTimesheetExtraController@store');
    Route::get('additional-work-days-bonus/{id}/approve', 'BonusRequest\AdditionalWorkingDayBonusController@approve');
    Route::get('additional-work-days-bonus/{id}/reject', 'BonusRequest\AdditionalWorkingDayBonusController@reject');

    Route::resource('project', 'ProjectController');
    Route::post('project/create', 'ProjectController@createProject');
    Route::post('project/edit', 'ProjectController@editProject');
    Route::get('project-resource-info', 'ProjectController@projectResourceInfo');
    Route::get('project-technology', 'ProjectController@projectTechnologies');
    Route::post('project-technology', 'ProjectController@setProjectTechnologies');
    Route::get('project-info/{id}', 'ProjectController@projectInfo');

    // Designation
    Route::resource('designations', 'DesignationController');
    Route::post('designations/status', 'DesignationController@updateStatus');
    // Feedback Question
    Route::resource('question', 'FeedbackQuestionController');
    Route::post('question/status', 'FeedbackQuestionController@updateStatus');
    // Feedback Month
    Route::post('feedback-month/visible', 'FeedbackMonthController@updateVisibility');
    Route::resource('feedback-month', 'FeedbackMonthController');
    Route::get('feedback-user', 'FeedbackUserController@getUserList');
    Route::get('feedback-user-status', 'FeedbackUserController@getProgressStatus');
    //Feedback Review
    Route::post('feedback-review-notify', 'FeedbackReviewController@sendNotificationSingle');
    Route::post('feedback-review-notify-all', 'FeedbackReviewController@sendNotificationAll');

    Route::post('feedback-review', 'FeedbackReviewController@updateRating');
    Route::post('feedback-review-na', 'FeedbackReviewController@updateNotAppicable');

    Route::post('feedback-month/lock', 'FeedbackMonthController@lockMonth');
    Route::post('feedback-month/toggle-lock', 'FeedbackMonthController@toggleLock');
    Route::post('feedback-review-regenerate-user', 'FeedbackReviewController@regenerateRecordsUser');
    Route::post('feedback-review-regenerate-all', 'FeedbackReviewController@regenerateRecordsAll');
    //Bill-Schedule-Cron
    Route::get('project-bill-schedule/{project_id}', 'CronApiController@ShowBillSchedule');
    Route::post('project/{id}/bill-schedule', 'CronApiController@store');
    Route::post('project/{id}/bill-schedule/update/{bill_schedule_id}', 'CronApiController@update');
    Route::get('project/{id}/bill-schedule/{bill_schedule_id}', 'CronApiController@show');

    // Resource Price
    Route::resource('resource-price', 'ResourcePriceController');

    // Project Resource Controller
    Route::resource('project-resource', 'ProjectResourceController');
    Route::put('project-resource/release/{id}', 'ProjectResourceController@releaseUser');
    // Technologies
    Route::get('technology', 'TechnologyController@index');
    // Resource Price
    Route::resource('resource-price', 'ResourcePriceController');

    // Resource Price
    Route::resource('resource-price', 'ResourcePriceController');

    //project
    Route::get('project-client/{id}', 'ProjectController@clientDetail');
    Route::get('project-timelog/{id}', 'ProjectController@timelog');
    //project files
    Route::resource('project-link', 'ProjectFileController');
    //bonus
    Route::resource('additional-workday-bonus', 'AdditionalWorkDaysBonusController');
    Route::put('additional-workday-bonus', 'AdditionalWorkDaysBonusController@updateBonus');

    // Route::resource('onsite-bonus', 'OnsiteBonusController');
    // Route::put('onsite-bonus', 'OnsiteBonusController@updateBonus');
    //Route::resource('onsite-bonus', 'OnsiteBonusController');
    Route::put('onsite-bonus', 'OnsiteBonusController@updateBonus');
    Route::post('onsite-bonus/user-allowance', 'OnsiteBonusController@userOnSite');
    // CRON
    Route::resource('cron', 'CronController');
    // Check slack member channel
    Route::get('check-channel-bot', 'ProjectController@checkBotInChannel');

    //contact
    Route::resource('company-contact', 'ContactController');
    Route::put('company-contact', 'ContactController@updateContact');

    Route::get('asset-meta/key', 'AssetMetaController@getMeta');
    Route::resource('asset-meta', 'AssetMetaController');
    Route::get('asset/{id}', 'AssetController@show');
    Route::get('asset', 'AssetController@index');

    //Ip Address Range
    Route::resource('ip-address-ranges', 'IpAddressRangeController');
    Route::post('assign-ip', 'IpAddressRangeController@assignIp');
    Route::post('ip-address-ranges/company-asset', 'IpAddressRangeController@updateCompanyAssetStatus');

    Route::resource('user-ip-address', 'UserIpAddressController');
    Route::get('user-ip-address/{userId}/free-ips', 'UserIpAddressController@freeUserIp');
    Route::resource('asset-detail', 'AssetAssignmentDetailController');
    // Vendor
    Route::resource('vendor', 'VendorController');
    Route::get('vendor-services', 'VendorController@getServices');
    Route::post('incident-transfer', 'IncidentController@transferIncident');
    Route::resource('incident', 'IncidentController');
    Route::resource('incident-view', 'IncidentController');
    Route::resource('department', 'DepartmentController');
    Route::get('purchase-order-download/{id}', 'PurchaseOrderController@download');
    Route::resource('purchase-orders', 'PurchaseOrderController');
    Route::resource('purchase-order-view', 'PurchaseOrderController@show');

    Route::resource('my-network-devices', 'MyNetworkDeviceController');
    // Expense
    Route::get('expense-heads', 'ExpenseController@getExpenseHeads');
    Route::resource('expense', 'ExpenseController');
    Route::post('expense-update-status', 'ExpenseController@updateStatus');

    Route::resource('ip-address-mapper', 'IpAddressMapperController');
    Route::resource('activity-log', 'ActivityController');
    Route::post('file', 'FileController@store');

    Route::get('payment-method', 'PaymentMethodController@index');

    Route::get('feedback-reviews-monthly', 'FeedbackUserController@getMonthlyUserReviews');
    // Google Drive Link
    Route::resource('google-drive-link', 'GoogleDriveLinkController');

    Route::get('network-asset', 'AssetController@networkAsset');
    Route::post('network-asset/assign-ip', 'AssetController@assignIpNetworkAsset');
    Route::get('network-asset/release-ip/{ip_mapper_id}', 'AssetController@releaseIpNetworkAsset');

    Route::get('leave-types', 'Leave\UserLeaveController@getLeaveTypes');
    Route::get('leave-working-days', 'Leave\UserLeaveController@getWorkingDays');
    Route::get('leave-warnings', 'Leave\UserLeaveController@getWarnings');

    Route::get('user-leave-details', 'Leave\UserLeaveController@getUserLeaveDetails');
    Route::resource('insurance', 'Insurance\InsuranceController');
    Route::resource('insurance-policy', 'Insurance\InsurancePolicyController');
    Route::get('insurance-policy/download/{id}', 'Insurance\InsurancePolicyController@downloadCSV');
    // Route::put('insurance-premium/requested/{id}','Insurance\InsurancePremiumController@markAsRequested');
    // Route::put('insurance-premium/approved/{id}','Insurance\InsurancePremiumController@markAsApproved');
    // Route::put('insurance-premium/close/{id}','Insurance\InsurancePremiumController@markAsClosed');
    // Route::put('insurance-premium/update-record/{id}','Insurance\InsurancePremiumController@updateRecord');

    Route::resource('notify', 'notifyController');

    Route::post('user_ap_log/{api_token}', 'UserApLogController@store')->middleware('api_token');
    Route::post('user_ip_mac_log/{api_token}', 'IpMacBindingController@store')->middleware('api_token');
    //Profile Builder
    //Route::resource('training', 'User\TrainingController');
    //Route::resource('workshop', 'User\WorkshopController');
    //Route::resource('miniproject', 'User\MiniprojectController');
    //Route::resource('tech-stack', 'User\TechStackController');

    Route::resource('rnd', 'User\RnDController');
    Route::get('social-info/{id}', 'ProfileController@show');
    Route::post('social-info', 'ProfileController@updateSocialInfo');

    Route::resource('user-visibility-setting', 'UserVisibilityController');

    Route::post('profile-info/{id}', 'ProfileInfoController@imageStore');
    Route::resource('profile-info', 'ProfileInfoController');
    // Route::get('cms-project-user/get-projects/{id}', 'CmsProjectUserController@getServices');

    Route::get('cms-project', 'CmsProjectController@index');
    Route::resource('cms-project-user', 'CmsProjectUserController');
    Route::resource('skills', 'SkillsController');

    //PayRoll
    Route::get('payroll-month-data/{id}', 'PayRoll\PayRollMonthController@recalculateSalaryData');
    //profile-builder

    Route::get('user-educations/{userId}', 'UserEducationController@getUserEducation');
    //Route::post('education/{id}', 'EducationController@updateStatus');
    Route::resource('user-educations', 'UserEducationController');

    Route::post('server-auth-check', 'ServerManagement\ServerAuthCheckController@authCheck');

});
