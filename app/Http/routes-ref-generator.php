<?php
//  -- REFERENCE NUMBER --
Route::group(['roles' => ['reference-generator']], function () {
    Route::resource('reference-number', 'ReferenceNumberController');
});