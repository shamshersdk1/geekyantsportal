<?php Route::group(['roles' => ['account']], function () {

    Route::group(['roles' => ['human-resources', 'reporting-manager', 'management']], function () {
        Route::group(['roles' => ['user']], function () {
            Route::get('loan-requests/success/{id}', 'LoanRequestController@success');
            Route::get('loan-requests', 'LoanRequestController@index');
            Route::resource('loan-requests', 'LoanRequestController');
            Route::get('loan-applications/{id}', 'LoanRequestController@showLoanDetail');
            Route::resource('request-approval', 'RequestApprovalController');
            Route::get('my-requests', 'RequestApprovalController@myRequests');
            Route::get('approval-requests', 'RequestApprovalController@approvalRequests');
            Route::get('db-file/{file_id}', 'FileController@streamFile');
        });
        Route::get('loans/emi-month', 'LoanController@emiMonth');
        Route::get('loans/emi-month/{monthId}', 'LoanController@emiDeduction');
        Route::put('loans/{id}/complete', 'LoanController@completeLoan');
        Route::resource('loans', 'LoanController');

        // Route::get('loans/{id}/complete', 'LoanController@completeLoanShow');
        // Route::put('loans/{id}/complete', 'LoanController@completeLoanUpdate');
        // Route::get('loans/{id}/completeLoan', 'LoanController@completeLoan');
        // Route::post('loans/{id}/completeLoan', 'LoanController@completeLoanTransaction');
        // Route::get('loans/edit/{id}', 'LoanController@editLoan');
        // Route::put('loans/edit/{id}', 'LoanController@updateLoan');
        // Route::post('loans/{id}/upload', 'LoanController@uploadFile');

        // Route::resource('loan-requests', 'LoanRequestController');
        // Route::get('new-loan-requests', 'LoanRequestController@getRequests');
        // Route::get('new-loan-requests/{id}', 'LoanRequestController@showRequest');
        Route::put('new-loan-requests/{id}', 'LoanRequestController@action');
        Route::get('loan-applications', 'LoanRequestController@history');

    });

    // Insurance Policy Managers
    Route::get('insurance-policy', 'Insurance\InsurancePolicyController@index');
    Route::get('insurance-policy/{id}', 'Insurance\InsurancePolicyController@show');

    // Insurance Deduction
    Route::get('insurance-deduction', 'Insurance\InsuranceDeductionController@monthList');
    Route::get('insurance-deduction/{monthId}', 'Insurance\InsuranceDeductionController@monthlyDeduction');
    Route::get('insurance-deduction/{monthId}/status-month', 'Insurance\InsuranceDeductionController@monthStatusDeduction');
    Route::get('insurance-deduction/{monthId}/regenerate', 'Insurance\InsuranceDeductionController@regenerate');
    Route::get('insurance-deduction/{deductionId}/delete', 'Insurance\InsuranceDeductionController@deleteRecord');

    // Route::get('loans-ledger', 'LoanController@loanLedger');

    Route::get('loans/{loan_id}/edit/{loan_transaction_id}', 'LoanTransactionController@editTransaction');
    Route::put('loans/{loan_id}/edit/{loan_transaction_id}', 'LoanTransactionController@updateTransaction');
    Route::get('loans/{loan_id}/create', 'LoanTransactionController@addTransaction');
    Route::post('loans/{loan_id}', 'LoanTransactionController@saveTransaction');

    Route::resource('emis', 'LoanEmiController');
    // Route::get('emis/new/{id}', 'LoanEmiController@newEmi');
    // Route::post('emis/new/{id}', 'LoanEmiController@storeNewEmi');
    /** Payslip */

    Route::get('upload-payslip', 'PayslipController@index'); //current by default
    Route::get('upload-payslip/create', 'PayslipController@uploadPayslip'); //current by default
    Route::post('upload-payslip', 'PayslipController@store'); //current by default
    Route::delete('payslip/{id}', 'PayslipController@delete');

    Route::get('payslip/pending', 'PayslipController@pendingPayslip');
    Route::post('payslip/pending/approve/{id}', 'PayslipController@approvePayslip');
    Route::get('payslip/pending/reject/{id}', 'PayslipController@rejectPayslip');
    Route::get('payslip/month/download/{id}', 'PayslipController@downloadPayslipMonth');
    // Route::resource('payslip', 'PayslipController');

    Route::get('payslip/rejected', 'PayslipController@rejectedPayslip');

    Route::get('payslip/approved', 'PayslipController@approvedPayslip');

    Route::get('payslip/{id}', 'PayslipController@show');
    Route::get('payslip/approved/download/{id}', 'PayslipController@download');
    Route::get('payslip/approved/view/{id}', 'PayslipController@viewPayslip');

    Route::get('create-payroll-csv/{id}/download', 'PayslipCsvController@download');
    Route::get('create-payroll-csv/{id}/loan-amount', 'PayslipCsvController@loan');
    Route::get('create-payroll-csv/{id}/reconcile', 'PayslipCsvController@reconcile');
    Route::get('create-payroll-csv/{monthId}/review', 'PayslipCsvController@reviewMonth');
    Route::get('create-payroll-csv/{monthId}/review/{id}', 'PayslipCsvController@reviewMonthUser');
    Route::resource('create-payroll-csv', 'PayslipCsvController');
    Route::get('payroll-rule-month', 'Finance\PayrollRuleForCsvMonthController@index');
    Route::get('payroll-rule-month/{id}', 'Finance\PayrollRuleForCsvMonthController@showGroup');
    Route::get('payroll-rule-month/{monthId}/{groupId}', 'Finance\PayrollRuleForCsvMonthController@showGroupRule');
    Route::get('payroll-rule-month/{monthId}/{groupId}/rules/{id}/edit', 'Finance\PayrollRuleForCsvMonthController@editGroupRule');
    Route::get('payroll-month/{id}/update-status', 'Finance\PayrollMonthController@TDSStore');
    Route::get('payroll-month', 'Finance\PayrollMonthController@index');
    Route::get('payroll-month/{id}', 'Finance\PayrollMonthController@showUsers');
    Route::get('user-salary-review', 'SalaryReviewController@view');
    Route::get('payroll-month/{monthId}/{id}', 'Finance\PayrollMonthController@showSalaryData');
    Route::get('payroll-month/{monthId}/{id}/transaction', 'Finance\PayrollMonthController@showTransaction');
    Route::resource('loan-deduction', 'Finance\LoanDeductionController');
    Route::get('loan-interest-income', 'Finance\LoanInterestController@index');
    Route::get('loan-interest-income/{monthId}', 'Finance\LoanInterestController@show');
    Route::post('loan-interest-income/{monthId}/update', 'Finance\LoanInterestController@update');
    Route::get('loan-interest-income/{monthId}/regenerate', 'Finance\LoanInterestController@regenerate');
    Route::get('loan-interest-income/{monthId}/status-month', 'Finance\LoanInterestController@monthStatusDeduction');
    Route::get('loan-deduction/{monthId}/status-month', 'Finance\LoanDeductionController@monthStatusDeduction');
    Route::get('loan-deduction/{monthId}/regenerate', 'Finance\LoanDeductionController@regenerate');
    Route::post('loan-deduction/{monthId}/update', 'Finance\LoanDeductionController@updateData');
    Route::get('loan-deduction/{monthId}/delete', 'Finance\LoanDeductionController@deleteRecord');

    // ------------------------- Projects -------------------------
    Route::get('project/create-new', 'ProjectController@createNew');
    Route::get('project/{id}/edit-new', 'ProjectController@editNew');
    Route::resource('project', 'ProjectController');
    Route::get('project/create/{id}', 'ProjectController@createConvert');
    Route::post('project/updateList/{id}', 'ProjectController@updateList');
    Route::post('project/{id}/addTechnology', 'ProjectController@addTechnology');
    Route::delete('project/{techid}/{projectid}/deleteTechnology', 'ProjectController@deleteTechnology');
    Route::post('project/{id}/addUser', 'ProjectController@addUser');
    Route::delete('project/{userid}/{projectid}/deleteUser', 'ProjectController@deleteUser');
    Route::delete('project/delete/{resourceid}', 'ProjectController@destroyResouce');
    Route::post('project/release/{resourceid}', 'ProjectController@releaseResource');
    Route::get('show-project-report/{id}', 'ProjectReportController@showProjectDetails');
    Route::get('project/{id}/dashboard', 'ProjectController@viewDashboard');
    Route::get('project/{id}/dashboard-new', 'ProjectController@viewDashboardNew');
    Route::post('project/{id}/add-comment', 'ProjectController@addComment');
    Route::post('project/{id}/upload', 'ProjectController@uploadFile');
    Route::get('project/{id}/download-report', 'ProjectController@generateReportCsv');
    Route::get('project/{id}/download-log', 'ProjectController@generateLogCsv');
    // -------------------------------------------------------------
    // ------------------------- Project & Resource Reports -------------------------
    Route::get('resource-report', 'ResourceReportController@index');
    Route::get('resource-hours-report', 'ResourceReportController@resourceAllocationReportWithHours');
    Route::get('project-resource-report', 'ProjectResourceReportController@index');
    // ------------------------- Project Slack Reports -------------------------
    Route::get('project-slack-report', 'ProjectController@projectSlackReport');

    //project-files
    Route::get('project/{id}/project-file/add', 'ProjectController@addProjectLinks');
    Route::post('project/{id}/project-file/create', 'ProjectController@storeProjectLinks');

    //Resource-price
    Route::get('project/{id}/resource-price', 'ResourcePriceController@index');
    Route::get('project/{id}/resource-price/add', 'ResourcePriceController@add');
    Route::post('project/{id}/resource-price/create', 'ResourcePriceController@create');
    Route::post('project/{id}/resource-price/remove', 'ResourcePriceController@remove');
    Route::post('project/{id}/resource-price/edit', 'ResourcePriceController@edit');
    Route::post('project/{id}/resource-price/update', 'ResourcePriceController@update');

    //billing-reminder
    //Route::post('project/{id}/billing/remove', 'BillingScheduleReminderController@destroy');
    Route::get('billing-schedule-reminder/{id}', 'BillingScheduleReminderController@show');
    Route::get('transaction', 'Finance\TransactionController@index');
    Route::get('transaction/{monthId}', 'Finance\TransactionController@showUserList');
    Route::get('transaction/{monthId}/{userId}', 'Finance\TransactionController@showUserTransaction');

    Route::resource('onsite-allowance', 'OnsiteAllowanceController');

    Route::resource('vpf', 'VpfController');

    Route::get('vpf/approve/{id}', 'VpfController@approve');
    Route::get('vpf/rejected/{id}', 'VpfController@reject');

    Route::get('vpf-deduction', 'VpfController@getVpfDeductions');
    Route::get('vpf-deduction/{monthId}', 'VpfController@viewDeductions');
    Route::get('vpf-deduction/{monthId}/regenerate', 'VpfController@regenerate');
    Route::post('vpf-deduction/{monthId}/update', 'VpfController@storeData');
    Route::get('vpf-deduction/{vpfId}/delete', 'VpfController@deleteRecord');
    Route::get('vpf-deduction/{monthId}/status-month', 'VpfController@monthStatus');

    //appraisal bonus
    Route::get('appraisal-bonus', 'AppraisalController@getAppraisalBonuses');
    Route::get('appraisal-bonus/{monthId}', 'AppraisalController@viewAppraisals');
    Route::get('appraisal-bonus/{monthId}/regenerate', 'AppraisalController@regenerate');
    Route::get('appraisal-bonus/{monthId}/{userId}/addAnnualBonus', 'AppraisalController@addAnnualBonus');
    Route::post('appraisal-bonus/{monthId}/update', 'AppraisalController@storeData');
    Route::get('appraisal-bonus/{appraisalId}/delete', 'AppraisalController@deleteRecord');
    Route::get('appraisal-bonus/{monthId}/status-month', 'AppraisalController@monthStatus');

    //fixed bonus
    Route::resource('fixed-bonus', 'Finance\FixedBonusController');
    Route::get('fixed-bonus/{monthId}/delete', 'Finance\FixedBonusController@destroy');
    Route::get('fixed-bonus/{monthId}/status-month', 'Finance\FixedBonusController@lockMonth');
    Route::get('fixed-bonus/{monthId}/regenerate', 'Finance\FixedBonusController@regenerate');

    //Variable Pay
    Route::resource('variable-pay', 'Finance\VariablePayController');
    Route::get('variable-pay/{monthId}/{variablePayId}/delete', 'Finance\VariablePayController@destroy');
    Route::get('variable-pay/{monthId}/status-month', 'Finance\VariablePayController@lockMonth');
    Route::get('variable-pay/{monthId}/regenerate', 'Finance\VariablePayController@regenerate');

    //Food Deduction
    Route::resource('food-deduction', 'Finance\FoodDeductionController');
    Route::get('food-deduction/{monthId}/delete', 'Finance\FoodDeductionController@destroy');
    Route::get('food-deduction/{monthId}/status-month', 'Finance\FoodDeductionController@lockMonth');
    Route::get('food-deduction/{monthId}/regenerate', 'Finance\FoodDeductionController@regenerate');

    //Adhoc Payments
    Route::resource('adhoc-payment-component', 'AdhocPayment\AdhocPaymentComponentController');
    Route::resource('adhoc-payments', 'AdhocPayment\AdhocPaymentsController');
    Route::get('adhoc-payments/{monthId}/status-month', 'AdhocPayment\AdhocPaymentsController@lockMonth');

    //User Working Day
    Route::resource('user-working-day', 'WorkingDay\UserWorkingDayController');
    Route::get('user-working-day/{monthId}/status-month', 'WorkingDay\UserWorkingDayController@lockMonth');
    Route::get('user-working-day/{monthId}/regenerate', 'WorkingDay\UserWorkingDayController@regenerate');

});
