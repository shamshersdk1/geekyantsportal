<?php
if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
    // Ignores notices and reports all other kinds... and warnings
    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
    // error_reporting(E_ALL ^ E_WARNING); // Maybe this is enough
}

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
 */

// Route::get('policies', function () {
//     return View::make('pages/policies');
// });

Route::get('policies', 'Admin\Policy\PolicyController@showActivePolices');
Route::get('update-profile', 'UpdateProfileController@index');

Route::get('get-wifi-mac-csv', 'IpAddressMapperController@getWifiMacCSV');
Route::get('get-user-asset-ip', 'IpAddressMapperController@getUserAssetIp');
Route::get('get-user-asset-ip/check', 'IpAddressMapperController@getLastUpdatedAt');
Route::post('gateway-request', 'GatewayRequestController@store');

Route::group(['middleware' => ['session', 'roleCheck']], function () {

    Route::get('/', 'LoginController@showLogin');
    Route::get('/home', function () {return View::make('pages/home');});
    Route::post('/save-notes', 'Api\V1\Slack\SlashCommandController@saveNotes');
    Route::post('api/v1/send-slack-message-user', 'Api\V1\Slack\ExternalSlackController@sendMessageUser');
    Route::post('api/v1/send-slack-message-channel', 'Api\V1\Slack\ExternalSlackController@sendMessageChannel');
    Route::post('api/v1/send-follow-up-reminder', 'Api\V1\Slack\ExternalSlackController@sendFollowUpMessage');
    Route::post('api/v1/send-assigned-to-msg', 'Api\V1\Slack\ExternalSlackController@sendAssignedToMessage');

    require_once "routes-api.php";

    /**
     *|---------------------------------------------------------------------------
     *| Google Authentication Routes
     *|---------------------------------------------------------------------------
     */
    $s = 'social.';
    Route::get('/social/redirect/{provider}', ['as' => $s . 'redirect', 'uses' => 'Auth\SocialController@getSocialRedirect']);
    Route::get('/social/handle/{provider}', ['as' => $s . 'handle', 'uses' => 'Auth\SocialController@getSocialHandle']);

    Route::get('images/{filename}', function ($filename) {
        $path = '/uploads/technology/' . $filename;
        if (!Storage::exists($path)) {
            abort(404);
        }
        $content = Storage::get($path);
        $type = Storage::mimeType($path);
        return response($content, 200)->header('Content-Type', $type);
    });

    /*
    Login and Sign Routes
     */
    Route::get('admin', 'LoginController@showLogin');
    Route::post('login', 'LoginController@login');
    //Route::get('test', 'TestController@index');

    Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'auth'], function () {

        Route::group([
            'middleware' => ['admin', 'capture.route'],
        ],
            function () {
            }
        );

        //For both admin and user

        //Sprints for a individual user
    });

    Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'auth'], function () {

        // Route::get('seats/{id}/show', 'SeatController@show');
        Route::post('employee-leave/requestLeave', 'EmployeeLeaveController@requestLeave');
        Route::put('users/{id}/profile', 'ProfileController@update');
        Route::get('employee-payslip/{id}', 'EmployeePayslipController@viewPayslip');

        Route::get('dashboard', 'AdminController@dashboard');
        Route::get('new-dashboard', 'AdminController@dashboard');
        Route::get('rm-dashboard', 'rmController@dashboard');

        Route::get('logout', 'AdminController@dologout');

        Route::get('super-admin-switch', 'RoleController@switchToSuperAdmin');
        // User Contacts
        Route::resource('user-contacts', 'UserContactController');
        // Shared Credentials
        Route::resource('shared-credential', 'CredentialController');
        Route::post('shared-credential/new', 'CredentialController@updateCredential');
        Route::post('shared-credential/share', 'CredentialController@shareCredential');
        // My SSH Key
        Route::get('ssh-keys/my-ssh-key/view', 'SshKeyController@showMyRequest');
        Route::get('ssh-keys/my-ssh-key/edit', 'SshKeyController@getMyRequest');
        Route::get('ssh-keys/my-ssh-key/add', 'SshKeyController@createMyRequest');
        Route::post('ssh-keys/my-ssh-key/add', 'SshKeyController@addMyRequest');
        Route::put('ssh-keys/my-ssh-key/edit/{id}', 'SshKeyController@updateMyRequest');

        require_once "routes-admin.php";
    });

    require_once "routes-user.php";

    Route::get('/{slug}', 'ProfileController@view');
    Route::get('/{technology}', 'HomeController@show');

});

Route::get('admin/contact', function () {
    return View::make('pages/admin/contact');
});
