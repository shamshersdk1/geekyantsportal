<?php
        Route::group(['roles' => ['calender']], function () {
                // ---------------------------------------------------------------
                // ------------------------- Calendar -------------------------
                Route::get('calendar', 'CalendarController@show');
                Route::get('calendar/events', 'CalendarController@events');
                Route::post('calendar/events/delete', 'CalendarController@deleteEvent');
                Route::post('calendar/save', 'CalendarController@save');
            });