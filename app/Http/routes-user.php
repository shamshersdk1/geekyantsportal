<?php
Route::group(['namespace' => 'UserView', 'middleware' => 'auth', 'roles' => ['user']], function () {

    Route::get('user/appraisals', 'AppraisalController@index');
    Route::get('user', 'UserViewController@redirectToDashboard');
    Route::get('user/dashboard', 'UserViewController@showDashboard');
    Route::get('user/salary-details', 'UserViewController@showSalaryDetails');
    Route::get('user/profile', 'UserViewController@showProfile');
    Route::get('user/profile/edit', 'UserViewController@editProfile');
    Route::get('user/salary-breakdown', 'UserViewController@showSalary');
    Route::get('user/payslips', 'UserViewController@showPayslips');
    Route::get('user/payslip/{monthId}', 'UserViewController@showUserPayslip');
    Route::get('user/payslip/{monthId}/download', 'UserViewController@downloadUserPayslip');
    Route::get('user/tds-breakdown/{monthId}', 'UserViewController@showUserTDS');
    Route::get('user/tds-breakdown/{monthId}/download', 'UserViewController@downloadUserTDS');
    Route::get('user/leaves', 'UserLeaveController@indexNew');
    Route::get('user/leaves-new', 'UserLeaveController@indexNew');
    Route::post('user/leaves/status/{id}', 'UserLeaveController@leaveStatus');
    Route::post('user/leaves/requestLeave', '\App\Http\Controllers\Admin\EmployeeLeaveController@requestLeave');

    // Route::get('user/leaves/check', 'UserLeaveController@checkLeaveProjects'); // api
    // Route::post('user/leaves/overlapping-request/leave', 'UserLeaveController@saveOverlapLeave'); //api
    // Route::post('user/leaves/overlapping-request/reasons', 'UserLeaveController@saveOverlapRequest'); //api

    Route::get('user/loans', 'UserViewController@showLoans');
    Route::get('user/birthdays', 'UserViewController@showBdays');
    Route::get('user/my-events', 'UserViewController@showMyEvents');
    Route::get('user/loans/{id}', 'UserViewController@showEMI');
    //Route::get('user/bonuses', 'UserViewController@showBonuses');
    Route::get('user/bonuses', 'UserOvertimeController@index');
    // Route::get('user/timesheet', 'UserViewController@showTimesheet');
    Route::get('user/timesheet', function () {
        return Redirect::to('/admin/new-timesheet');
    });
    Route::get('user/assets', 'UserViewController@showAssets');
    Route::get('user/feedback', 'UserViewController@showFeedback');
    // Route::get('user/timesheet/detail', 'UserViewController@showDetailTimesheet');
    Route::get('user/timesheet/detail', function () {
        return Redirect::to('/admin/new-timesheet');
    });
    Route::post('user/timesheet/save', 'UserViewController@saveTimesheet');
    Route::resource('user/skill', 'UserViewController@showSkills');
    Route::resource('user/skill/store', 'UserViewController@saveSkills');

    // Project wise weekly timesheet
    // Route::get('user/timesheet/{id}', 'UserViewController@projectDetailTimesheet');
    Route::get('user/timesheet/{id}', function () {
        return Redirect::to('/admin/new-timesheet');
    });
});

Route::group(['roles' => ['user']], function () {

    // Route::resource('admin/loan-requests', 'Admin\LoanRequestController');
    Route::get('admin/loan-requests/{id}/delete', 'Admin\LoanRequestController@deleteRequest');
    Route::get('admin/loan-applications/{id}/file/{file_id}', 'Admin\LoanRequestController@streamFile');
    // Route::get('loans/{id}/file/{file_id}', 'LoanController@streamFile');
    Route::get('admin/user-tasks', 'Admin\UserTaskController@index');
    Route::resource('admin/shared-task', 'Admin\SharedTaskController');
    Route::get('admin/shared-task/detach/{id}', 'Admin\SharedTaskController@detachUser');

    Route::get('admin/goal-user-view', 'Admin\GoalController@myGoals');
    Route::get('admin/goal-user-view/{id}', 'Admin\GoalController@goalUserView');

    Route::get('admin/new-timesheet', 'Admin\NewTimesheetController@create');
    Route::get('admin/new-timesheet/history', 'Admin\NewTimesheetController@timesheetHistory');
    Route::get('admin/new-timesheet/lead-view', 'Admin\NewTimesheetController@leadView');
    Route::get('admin/new-timesheet/remind-me', 'Admin\NewTimesheetController@remindMe');
    Route::get('admin/new-timesheet/remind-me', 'Admin\NewTimesheetController@remindMe');
    Route::get('admin/new-timesheet/review', 'Admin\AdminReviewReport@UserReport');
    // Question
    //Route::resource('admin/question', 'Admin\FeedbackQuestionController');
    // ------------------------- Feedback -------------------------
    Route::get('admin/feedback/my-review', 'Admin\FeedbackReviewController@showUserMonths');
    Route::get('admin/feedback/my-review/{month_id}', 'Admin\FeedbackReviewController@showUser');
    // --------------------------------------------------------------
    // Incidents
    Route::resource('admin/incident', 'Admin\IncidentController');

    Route::resource('admin/my-network-devices', 'Admin\MyNetworkDeviceController');
    Route::get('user/it-saving/{id}/copy','ItSavingController@copyFromPrevious');
    Route::resource('user/it-saving', 'ItSavingController');
    Route::resource('admin/it-saving', 'Admin\ItSavingController');
    Route::get('admin/it-saving/{financial_year_id}/{user_id}', 'Admin\ItSavingController@show');

    Route::get('admin/bonus-request', 'User\BonusRequestController@month');
    Route::get('admin/bonus-request-pending/{monthId}', 'User\BonusRequestController@monthBonusesPending');
    Route::get('admin/bonus-request/{monthId}', 'User\BonusRequestController@monthBonuses');
    Route::get('admin/bonus-request/view/{id}', 'User\BonusRequestController@showBonusRequest');
    Route::get('user/wifi-password', 'UserView\WifiPasswordController@index');
    Route::post('user/wifi-password', 'UserView\WifiPasswordController@updatePassword');
    Route::get('user/leave-overview', 'Admin\LeaveOverviewController@leaveSummary');

});
