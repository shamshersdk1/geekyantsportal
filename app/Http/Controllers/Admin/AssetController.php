<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Asset;
use App\Models\Admin\AssetAssignmentDetail;
use App\Models\Admin\AssetCategory;
use App\Models\Admin\File;
use App\Models\User;
use App\Services\AssetService;
use App\Services\FileService;
use Auth;
use Exception;
use Illuminate\Http\Request;
use Redirect;
use Validator;

class AssetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!empty($request->q)) {
            $search = $request['q'];
        } else {
            $search = '';
        }

        $assetCategories = AssetCategory::with('assets')->get();

        $assetCategories = AssetService::getAssignedUsers($assetCategories, $search);
        // echo "<pre>";
        // print_r($assetCategories);
        // die;
        return View('pages/admin/asset-management/asset/index', ['assetCategories' => $assetCategories, 'search' => $search]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $assetCategories = AssetCategory::all();
        $makeList = Asset::whereNotNull('make')->select('make')->distinct()->pluck('make')->toArray();
        $makeList = '["' . implode('","', $makeList) . '"]';

        $modelList = Asset::whereNotNull('model')->select('model')->distinct()->pluck('model')->toArray();
        $modelList = '["' . implode('","', $modelList) . '"]';
        $users = User::where('is_active', 1)->get();
        return View('pages/admin/asset-management/asset/add', ['assetCategories' => $assetCategories, 'users' => $users, 'makeList' => $makeList, 'modelList' => $modelList]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $reference_type = '';
        $prefix = '';
        $reference_id = '';
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'asset_category_id' => 'required',
            'purchase_date' => 'required',
            'serial_number' => 'required',
            'is_working' => 'required',
            'invoice_number' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors(['Please provide all the mandatory fields']);
        }
        if (!empty($request->user_id) && empty($request->start_date)) {
            return Redirect::back()->withInput()->withErrors(['Please select start date for assignment']);
        }

        $asset = new Asset();

        $asset->name = $request->name;
        $asset->asset_category_id = $request->asset_category_id;
        $asset->purchase_date = $request->purchase_date;
        $asset->serial_number = !empty($request->serial_number) ? $request->serial_number : null;
        $asset->price = !empty($request->price) ? $request->price : null;
        $asset->tax = !empty($request->tax) ? $request->tax : null;
        $asset->make = !empty($request->make) ? $request->make : null;
        $asset->model = !empty($request->model) ? $request->model : null;
        $asset->is_working = $request->is_working;
        $asset->invoice_number = $request->invoice_number;
        $asset->google_drive_link = !empty($request->google_drive_link) ? $request->google_drive_link : '';
        $asset->description = !empty($request->description) ? $request->description : '';
        $asset->is_assignable = !empty($request->is_assignable) ? $request->is_assignable : '';
        if (!$asset->save()) {
            $errors = $asset->getErrors();
            $messages = $errors->messages();
            return Redirect::back()->withInput()->withErrors($messages);
        }
        if (!empty($request->user_id) && !empty($request->start_date)) {
            $asset_new = Asset::where('serial_number', $request->serial_number)
                ->where('invoice_number', $request->invoice_number)
                ->where('asset_category_id', $request->asset_category_id)
                ->where('name', $request->name)->first();
            $assignee = Auth::user();
            $asset_assignment_obj = new AssetAssignmentDetail();
            $asset_assignment_obj->user_id = $request->user_id;
            $asset_assignment_obj->asset_id = $asset_new->id;
            $asset_assignment_obj->from_date = $request->start_date;
            $asset_assignment_obj->assignee_id = $assignee->id;
            $asset_assignment_obj->save();
        }

        if ($request->hasFile('file')) {
            $reference_type = 'App\Models\Admin\Asset';
            $prefix = 'asset';
            // print_r($reference_type);
            $asset_new = Asset::where('serial_number', $request->serial_number)
                ->where('invoice_number', $request->invoice_number)
                ->where('asset_category_id', $request->asset_category_id)
                ->where('name', $request->name)->first();
            $reference_id = $asset_new->id;
            foreach ($request->file as $single_file) {
                $fileSize = $single_file->getClientSize();
                if ($fileSize > 20000000) {
                    return Redirect::back()->withInput()->withErrors(['Filesize exceeds 20MB']);
                }
                $content = file_get_contents($single_file->getRealPath());
                $type = $single_file->extension();
                // $fileName = FileService::getFileName($type);
                $originalName = $single_file->getClientOriginalName();
                $store = FileService::uploadFileInDB($content, $originalName, $type, $reference_id, $reference_type);
                // $upload = FileService::uploadFile($single_file, $fileName, $reference_id, $reference_type, $originalName, $prefix);
            }
        }

        return redirect('admin/asset-management/asset/' . $asset->id)->with('message', 'Asset added successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $asset = Asset::with('assetCategory')->find($id);
        $files = [];
        $assignment_details = [];
        $reference_type = 'App\Models\Admin\Asset';
        $files = File::where('reference_type', $reference_type)
            ->where('reference_id', $asset->id)->get();

        $users = User::where('is_active', 1)->get();
        $assignment_details = AssetAssignmentDetail::with('user', 'asset', 'assignee')
            ->where('asset_id', $id)->get();

        if (isset($asset)) {
            return View('pages/admin/asset-management/asset/show', ['asset' => $asset, 'files' => $files, 'users' => $users, 'assignment_details' => $assignment_details]);
        }
        return Redirect::back()->withInput()->withErrors(['Something went wrong']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $asset = Asset::with('assetCategory')->find($id);
        $assetCategories = AssetCategory::all();
        $assigned = AssetAssignmentDetail::currentuser($id);
        $makeList = Asset::whereNotNull('make')->select('make')->distinct()->pluck('make')->toArray();
        $makeList = '["' . implode('","', $makeList) . '"]';

        $modelList = Asset::whereNotNull('model')->select('model')->distinct()->pluck('model')->toArray();
        $modelList = '["' . implode('","', $modelList) . '"]';
        $files = [];
        $reference_type = 'App\Models\Admin\Asset';
        $files = File::where('reference_type', $reference_type)
            ->where('reference_id', $asset->id)->get();
        if (isset($asset)) {
            return View('pages/admin/asset-management/asset/edit', ['asset' => $asset, 'assetCategories' => $assetCategories, 'files' => $files,
                'assigned' => $assigned, 'makeList' => $makeList, 'modelList' => $modelList]);
        }
        return Redirect::back()->withInput()->withErrors(['Something went wrong']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'asset_category_id' => 'required',
            'purchase_date' => 'required',
            'serial_number' => 'required',
            'is_working' => 'required',
            'invoice_number' => 'required',
        ]);
        $asset = Asset::find($id);

        if (isset($asset)) {
            $asset->name = $request->name;
            $asset->asset_category_id = $request->asset_category_id;
            $asset->purchase_date = $request->purchase_date;
            $asset->serial_number = $request->serial_number;
            $asset->is_working = $request->is_working;
            $asset->invoice_number = $request->invoice_number;
            $asset->google_drive_link = !empty($request->google_drive_link) ? $request->google_drive_link : '';
            $asset->description = !empty($request->description) ? $request->description : '';
            $asset->price = !empty($request->price) ? $request->price : null;
            $asset->tax = !empty($request->tax) ? $request->tax : null;
            $asset->make = !empty($request->make) ? $request->make : null;
            $asset->model = !empty($request->model) ? $request->model : null;
            $asset->is_assignable = !empty($request->is_assignable) ? $request->is_assignable : '';
            if ($asset->save()) {
                if ($request->hasFile('file')) {
                    $reference_type = 'App\Models\Admin\Asset';
                    $prefix = 'asset';
                    $reference_id = $asset->id;
                    foreach ($request->file as $single_file) {
                        $fileSize = $single_file->getClientSize();
                        if ($fileSize > 20000000) {
                            return Redirect::back()->withInput()->withErrors(['Filesize exceeds 20MB']);
                        }
                        $content = file_get_contents($single_file->getRealPath());
                        $type = $single_file->extension();
                        $originalName = $single_file->getClientOriginalName();
                        // $fileName = FileService::getFileName($type);   
                        $store = FileService::uploadFileInDB($content, $originalName, $type, $reference_id, $reference_type);
                        // $upload = FileService::uploadFile($single_file, $fileName, $reference_id, $reference_type, $originalName, $prefix);
                    }
                }
                return Redirect::back()->with('message', 'Successfully updated');
            } else {
                return Redirect::back()->withErrors($asset->getErrors());
            }

        }
        return redirect::back()->withInput()->withErrors(['Something went wrong']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $asset = Asset::find($id);
        if ($asset) {
            if ($asset->delete()) {
                return Redirect::back()->with('message', 'Successfully Deleted');
            } else {
                return Redirect::back()->withErrors($asset->getErrors());
            }
        } else {
            abort(404);
        }

    }
    /**
     * Assign asset to a user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function assignUser(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'start_date' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors(['Please provide all the mandatory fields']);
        }
        $assigned_flag = AssetAssignmentDetail::checkAvailability($id, $request->start_date);

        if (!$assigned_flag) {
            $assignee = Auth::user();
            $asset_assignment_obj = new AssetAssignmentDetail();
            $asset_assignment_obj->user_id = $request->user_id;
            $asset_assignment_obj->asset_id = $id;
            $asset_assignment_obj->from_date = $request->start_date;
            $asset_assignment_obj->to_date = !empty($request->end_date) ? $request->end_date : '';
            $asset_assignment_obj->assignee_id = $assignee->id;

            if ($asset_assignment_obj->save()) {
                return redirect::back()->with('message', 'Asset assigned to user');
            } else {
                return redirect::back()->withErrors($asset_assignment_obj->getErrors());
            }
        } else {
            return Redirect::back()->withInput()->withErrors(['Asset is already assigned']);
        }
    }
    /**
     * Release asset from a user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function releaseUser($id)
    {
        $assignedUser = AssetAssignmentDetail::find($id);
        $end_date = date('Y-m-d', strtotime("-1 days"));
        if ($assignedUser) {
            try {
                $assignedUser->to_date = $end_date;
                $assignedUser->save();
                return redirect::back()->with('message', 'Asset released from user');
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        } else {
            abort(404);
        }
    }
    /**
     * Delete user asset allocation record.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyUser($id)
    {
        $assignedUser = AssetAssignmentDetail::find($id);
        if ($assignedUser) {
            $assignedUser->delete();
            return redirect::back()->with('message', 'User asset record deleted');
        } else {
            abort(404);
        }
    }

    public function updateUserAssignment(Request $request, $id)
    {
        $assignedUser = AssetAssignmentDetail::find($id);
        if ($assignedUser) {
            $assignedUser->from_date = $request['start_date'];
            $assignedUser->to_date = $request['end_date'];
            $assignedUser->comments = $request['comment'];
            $assignedUser->save();
            return redirect::back()->with('message', 'User asset record edited');
        } else {
            abort(404);
        }

    }
    public function networkAsset()
    {
        return View('pages/admin/asset-management/network-asset/index');
    }
}
