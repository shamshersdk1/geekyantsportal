<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Admin\CmsDeveloper;
use App\Models\Admin\CmsSlide;
use App\Models\User;
use App\Models\Admin\File;
use App\Services\FileService;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Redirect;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use Config;
use Exception;
use DB;
use Auth;
use Validator;

class CmsSlideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $cmsDeveloper = CmsDeveloper::find($id);
        return View('pages/admin/cms-slides/add', ['cmsDeveloper' => $cmsDeveloper ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $flag = 0;

        $validator = Validator::make($request->all(), [
            'cms_technology_id' => 'required',
            'title' => 'required',
			'description' => 'required',
            'user_name' => 'required',
            'status' => 'required'
        ]);
        

        if ($validator->fails()) {
            return redirect::back()->withErrors($validator->errors())->withInput($request->input());
        }

        $cmsSlide = new CmsSlide();
        $cmsSlide->cms_technology_id = $request->cms_technology_id;
        $cmsSlide->title = $request->title;
        $cmsSlide->description = $request->description;
        $cmsSlide->user_name = $request->user_name;
        $cmsSlide->status = $request->status;
        if($cmsSlide->save()){
            $flag = 1;
        }

        if ( $request->hasFile('file') )
        {
            $reference_type = 'App\Models\Admin\CmsSlide';
            $prefix = 'cms';
            $cmsObjNew = CmsSlide::where('cms_technology_id',$request->cms_technology_id)
                                ->where('user_name',$request->user_name)
                                ->where('title',$request->title)
                                ->where('description',$request->description)
                                ->first();

            $reference_id = $cmsObjNew->id;
            $single_file = $request->file;
            $type = $single_file->extension();
            $fileName = FileService::getFileName($type);
            $originalName = $single_file->getClientOriginalName();
            $upload = FileService::uploadFile($single_file, $fileName, $reference_id, $reference_type, $originalName, $prefix);
        }
        

        if( $flag == 1 ){
            return redirect('/admin/cms-developers')->with('message', 'Successfully Added developer');
        }
        else {
            return redirect::back()->withErrors($cmsObj->getErrors())->withInput();
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cmsSlide =  CmsSlide::with(['image' => function($query) {
                            $query->where('reference_type', '=', 'App\Models\Admin\CmsSlide');
                            }],'cms_developer')
                            ->where('id',$id)->first();
        $cmsDevelopers = CmsDeveloper::where('status',1)->get();
        
        if( $cmsSlide && $cmsDevelopers ){
            
            return View('pages/admin/cms-slides/edit', ['cmsSlide' => $cmsSlide, 'cmsDevelopers' => $cmsDevelopers]);
        }
        else {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $flag = 0;

        $validator = Validator::make($request->all(), [
            'title' => 'required',
			'description' => 'required',
            'user_name' => 'required',
            'status' => 'required'
        ]);
        

        if ($validator->fails()) {
            return redirect::back()->withErrors($validator->errors())->withInput($request->input());
        }
        
        $cmsSlide = CmsSlide::find($id);
        $cmsSlide->title = $request->title;
        $cmsSlide->description = $request->description;
        $cmsSlide->user_name = $request->user_name;
        $cmsSlide->status = $request->status;
        if($cmsSlide->save()){
            $flag = 1;
        }

        if ( $request->hasFile('file') )
        {
            $reference_type = 'App\Models\Admin\CmsSlide';
            $prefix = 'cms';
            
            $file = File::where('reference_type',$reference_type)->where('reference_id',$id)->first();

            if( isset($file) ){
                $file->delete();
            }

            $reference_id = $id;
            $single_file = $request->file;
            $type = $single_file->extension();
            $fileName = FileService::getFileName($type);
            $originalName = $single_file->getClientOriginalName();
            $upload = FileService::uploadFile($single_file, $fileName, $reference_id, $reference_type, $originalName, $prefix);
        }

        return redirect('admin/cms-developers')->with('message', 'Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
    /**
     * Show details of the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }


    

    


}
