<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\OnsiteAllowance;
use App\Models\Admin\Project;
use App\Models\User;
use Illuminate\Http\Request;
use Redirect;

class OnsiteAllowanceController extends Controller
{

    public function index()
    {
        $onsiteAllowanceList = OnsiteAllowance::orderBy('id', 'DESC')->paginate(100);
        return View('pages/admin/onsite-allowance/index', compact('onsiteAllowanceList'));
    }

    public function create()
    {
        $userList = User::where('is_active', 1)->get();
        $projectList = Project::where('status', 'in_progress')->get();
        return View('pages/admin/onsite-allowance/add', compact('userList', 'projectList'));
    }

    public function store(Request $request)
    {
        $userObj = \Auth::user();
        if ($request->end_date < $request->start_date) {
            return redirect::back()->withErrors('Start Date should be less than end date')->withInput();
        }
        $onsiteAllowanceObj = new OnsiteAllowance();
        $onsiteAllowanceObj->user_id = $request->user_id;
        $onsiteAllowanceObj->start_date = $request->start_date;
        $onsiteAllowanceObj->end_date = $request->end_date;
        $onsiteAllowanceObj->project_id = $request->project_id;
        $onsiteAllowanceObj->status = $request->status;
        $onsiteAllowanceObj->type = $request->type;
        $onsiteAllowanceObj->amount = $request->amount;
        $onsiteAllowanceObj->notes = !empty($request->notes) ? $request->notes : null;
        if ($onsiteAllowanceObj->status != 'pending') {
            $onsiteAllowanceObj->approver_id = $userObj->id;
        }
        if ($onsiteAllowanceObj->save()) {
            return redirect('admin/onsite-allowance')->with('message', 'Saved successfully');
        } else {
            return redirect::back()->withErrors($onsiteAllowanceObj->getErrors())->withInput();
        }
    }

    public function show($id)
    {
        $onsiteAllowanceObj = OnsiteAllowance::find($id);
        return View('pages/admin/onsite-allowance/show', compact('onsiteAllowanceObj'));
    }

    public function edit($id)
    {
        $onsiteAllowanceObj = OnsiteAllowance::find($id);
        $userList = User::where('is_active', 1)->get();
        $projectList = Project::where('status', 'in_progress')->get();
        if ($onsiteAllowanceObj) {
            return View('pages/admin/onsite-allowance/edit', compact('onsiteAllowanceObj', 'userList', 'projectList'));
        } else {
            return redirect('admin/onsite-allowance')->withError('message', 'Invalid request');
        }
    }

    public function update(Request $request, $id)
    {
        $userObj = \Auth::user();
        if ($request->end_date < $request->start_date) {
            return redirect::back()->withErrors('Start Date should be less than end date')->withInput();
        }
        $onsiteAllowanceObj = OnsiteAllowance::find($id);
        $onsiteAllowanceObj->user_id = $request->user_id;
        $onsiteAllowanceObj->start_date = $request->start_date;
        $onsiteAllowanceObj->end_date = $request->end_date;
        $onsiteAllowanceObj->project_id = $request->project_id;
        $onsiteAllowanceObj->status = $request->status;
        $onsiteAllowanceObj->type = $request->type;
        $onsiteAllowanceObj->amount = $request->amount;
        $onsiteAllowanceObj->notes = !empty($request->notes) ? $request->notes : null;
        if ($onsiteAllowanceObj->status != 'pending') {
            $onsiteAllowanceObj->approver_id = $userObj->id;
        }
        if ($onsiteAllowanceObj->save()) {
            return redirect('admin/onsite-allowance')->with('message', 'Saved successfully');
        } else {
            return redirect::back()->withErrors($onsiteAllowanceObj->getErrors())->withInput();
        }
    }

    public function destroy($id)
    {
        $onsiteAllowanceObj = OnsiteAllowance::find($id);
        if ($onsiteAllowanceObj->delete()) {
            return redirect('/admin/onsite-allowance')->with('message', 'Successfully Deleted');
        } else {
            return redirect::back()->withErrors($onsiteAllowanceObj->getErrors());
        }
    }

}
