<?php namespace App\Http\Controllers\Admin;

use App\Models\Profile;
use App\Models\Project;
use App\Models\User;
use App\Models\Setting;
use MandrillMail;
//use Weblee\Mandrill\Mail;
use Mail;
use App\Http\Requests;
use Redirect;
use Illuminate\Http\Request;
use Auth;
use App\Http\Controllers\Controller;

class SettingController extends Controller {
	
	public function showSetting() {

		$userId = Auth::user()->id;


		$password = Auth::user()->password;	

		if($userId){
			$user = User::with('setting')->find($userId);

			if(isset($user->setting)) {
				if(isset($user->setting->password)) {
					$user->setting->password = $this->decrypt($user->setting->password,$password);
				}
			}

			return view('pages.admin.setting',compact('user'));
		}			
	}

	
	public function saveSetting(Request $request) {
		

        $email = $request->email;
        $password = $request->password;

        $passKey = Auth::user()->password;

		$key = $passKey; //'password to (en/de)crypt';
		$string = $password;

		$iv = mcrypt_create_iv(
		    mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC),
		    MCRYPT_DEV_URANDOM
		);

		$encrypted = base64_encode(
		    $iv .
		    mcrypt_encrypt(
		        MCRYPT_RIJNDAEL_128,
		        hash('sha256', $key, true),
		        $string,
		        MCRYPT_MODE_CBC,
		        $iv
		    )
		);
		$data['password'] = $encrypted;
		$data['username'] = $email;

		$setting = Setting::saveData($data);	

		if($setting){
			return Redirect::back()->with('message', 'Successfully saved your email setting.');
		} else {
			return Redirect::back()->withErrors(['Something went wrong please try again']);
		}	
	}

	public function decrypt($encrypted,$key) {
		
        $data = base64_decode($encrypted);
		$iv = substr($data, 0, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC));

		$decrypted = rtrim(
		    mcrypt_decrypt(
		        MCRYPT_RIJNDAEL_128,
		        hash('sha256', $key, true),
		        substr($data, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC)),
		        MCRYPT_MODE_CBC,
		        $iv
		    ),
		    "\0"
		);

		return $decrypted;
	}
}
