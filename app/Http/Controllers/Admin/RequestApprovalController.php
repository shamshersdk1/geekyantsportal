<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Admin\TechEvent;
use App\Models\User;
use App\Models\Admin\RequestApproval;
use App\Models\Admin\RequestApproveUser;

use App\Http\Controllers\Controller;

use Redirect;
use Config;
use Auth;

class RequestApprovalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $requestApprovals = RequestApproval::orderBy('created_at','DESC')->paginate(20);
        return View('pages/admin/request-approval/index', ['requestApprovals' => $requestApprovals ] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $userList = User::where('is_active',1)->get();
        $requestUsers = [];
        $notifyUsers = [];
        return View('pages/admin/request-approval/add',['userList' => $userList, 'requestUsers' => $requestUsers, 'notifyUsers' => $notifyUsers]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        if ( empty($request->description) || empty($request->request_to) || empty($request->comment) )
        {
            return Redirect::back()->withInput()->withErrors('All fields are mandatory');
        }
        $input = $request->all();
        $response = RequestApproval::saveData($input);
        
        if ( $response['status'] )
        {
            return redirect('/admin/request-approval/'.$response['message'])->with('message', 'Successfully raised request');
        }
        else
        {
            return Redirect::back()->withInput()->withErrors($response['message']);
        }

    }
    /**
     * Show details of the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();
        $requestApprovalObj = RequestApproval::find($id);
        $myRequest = RequestApproval::isMyRequest($id);
        $responded = RequestApproval::isResponded($id, $user->id);
        return View('pages/admin/request-approval/show', ['requestApprovalObj' => $requestApprovalObj, 'myRequest' => $myRequest, 'responded' => $responded ] );
        
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $requestApprovalObj = RequestApproval::find($id);
        $userList = User::where('is_active',1)->get();
        $requestUsers = [];
        $notifyUsers = !empty($requestApprovalObj->notify_to) ? json_decode($requestApprovalObj->notify_to) : [];
        $requestUsersArray = $requestApprovalObj->approvalUsers;
        foreach( $requestUsersArray as $requestUser )
        {
            $requestUsers[] = $requestUser->user_id;
        }
        
        if($requestApprovalObj){

            return View('pages/admin/request-approval/edit', ['requestApprovalObj' => $requestApprovalObj, 'userList' => $userList, 'requestUsers' => $requestUsers, 'notifyUsers' => $notifyUsers]);
        }
        else {
            return Redirect::back()->withInput()->withErrors('Tech event not found');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ( empty($request->description) || empty($request->request_to) || empty($request->comment) )
        {
            return Redirect::back()->withInput()->withErrors('All fields are mandatory');
        }

        $input = $request->all();
        $response = RequestApproval::updateData($id, $input);
        
        if ( $response['status'] )
        {
            return redirect('/admin/request-approval/'.$response['message'])->with('message', 'Successfully raised request');
        }
        else
        {
            return Redirect::back()->withInput()->withErrors($response['message']);
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response = TechEvent::deleteRecord($id);
        if ( $response['status'] )
        {
            return redirect('/admin/tech-events')->with('message','Successfully Deleted');
        }
        else
        {
            return redirect::back()->withErrors($response['message']);
        }
    }

    public function myRequests()
    {
        $user = Auth::user();
        $requestApprovals = RequestApproval::where('user_id',$user->id)->orderBy('created_at','DESC')->paginate(20);
        $allowEdit = true;
        return View('pages/admin/request-approval/index', ['requestApprovals' => $requestApprovals,'allowEdit' => $allowEdit ] );
    }

    public function approvalRequests()
    {
        $user = Auth::user();
        
        $requestIds = RequestApproveUser::where('user_id',$user->id)->distinct()->select('request_id')->pluck('request_id')->toArray();
        $requestApprovals = RequestApproval::whereIn('id',$requestIds)->orderBy('created_at','DESC')->paginate(20);
        
        return View('pages/admin/request-approval/index', ['requestApprovals' => $requestApprovals ] );
    }
    
}
