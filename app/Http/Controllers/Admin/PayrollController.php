<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Admin\Payslip;
use App\Models\Admin\PayslipMonth;
use App\Models\Admin\PayslipData;
use App\Models\User;
use App\Models\Admin\CmsProject;
use App\Models\Admin\ProfileProject;
use App\Models\Admin\PayroleGroup;
use App\Models\Admin\PayroleGroupRule;
use App\Models\Admin\Appraisal;

use App\Services\CtcService;

use Redirect;
use Response;
use View;
use PDF;
use Excel;
use Validator;

class PayrollController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->user) {
            return Redirect::back()->with('message', 'Invalid Link Followed!');
        }

        $groups = PayroleGroup::paginate(50);
        return view('pages/admin/payroll/group/index', [
                                                    'groups'   => $groups,
                                                    'user'     => $request->user
                                                ]);
    }

    public function groupStore(Request $request)
    {
        if (!$request->user) {
            return Redirect::back()->with('message', 'Invalid Link Followed!');
        }


        $saveGroup = PayroleGroup::saveData($request);

        if (!$saveGroup['status']) {
            return Redirect::back()->withErrors($saveGroup['log']);
        }

        return Redirect::back()->with('message', $saveGroup['message']);
    }

    public function groupDelete(Request $request, $id)
    {
        if (!$request->user) {
            return Redirect::back()->with('message', 'Invalid Link Followed!');
        }


        $groupObj = PayroleGroup::find($id);
        if (!$groupObj) {
            return Redirect::back()->withErrors('Group not found');
        }
        $usersCtc = Appraisal::where('payrole_group_id', $groupObj->id)->get();
        if (count($usersCtc) > 0) {
            return Redirect::back()->withErrors(['Cannot delete this group, first edit the users appraisals']);
        }

        if (!$groupObj->delete()) {
            return Redirect::back()->withErrors($groupObj->getErrors());
        }

        return Redirect::back()->with('message', 'Suceessfully deleted');
    }

    public function rules(Request $request, $id)
    {
        if (!$request->user) {
            return Redirect::back()->with('message', 'Invalid Link Followed!');
        }

        $groupRules = PayroleGroupRule::getRules();
        $groupObj = PayroleGroup::with('rules')->find($id);
        
        if (!$groupObj) {
            return Redirect::back()->withErrors('Group not found');
        }
        return view('pages/admin/payroll/group/rule', compact('groupObj', 'groupRules'));
    }

    public function storeRule(Request $request, $id)
    {
        if (!$request->user) {
            return Redirect::back()->with('message', 'Invalid Link Followed!');
        }

        $saveRules = PayroleGroupRule::saveData($request, $id);

        if (!$saveRules['status']) {
            return Redirect::back()->withErrors($saveRules['errors'])->withInput();
        }

        return Redirect::to('/admin/payroll/group/'.$id.'/rules')->with('message', $saveRules['message']);
    }
    public function updateRule(Request $request, $gid, $id)
    {
        if (!$request->user) {
            return Redirect::back()->with('message', 'Invalid Link Followed!');
        }

        $saveRules = PayroleGroupRule::updateData($request, $id);

        if (!$saveRules['status']) {
            return Redirect::back()->withErrors($saveRules['errors'])->withInput();
        }

        return Redirect::to('/admin/payroll/group/'.$gid.'/rules')->with('message', $saveRules['message']);
    }

    public function deleteRule($id)
    {
        $rule=PayroleGroupRule::find($id);
        $rule->delete();
        return Redirect::back()->with('message', 'Rule successfully deleted');
    }
    public function editRule($gid, $id)
    {
        $rule=PayroleGroupRule::find($id);
        $groupObj=$rule->group;
        $groupRules = PayroleGroupRule::getRules();
        return view('pages.admin.payroll.group.editrule', compact('rule', 'groupObj','groupRules'));

    }

    public function download(Request $request, $userId)
    {
        if (!$request->user) {
            return Redirect::back()->with('message', 'Invalid Link Followed!');
        }


        $resultObj = new CtcService;
        $result = $resultObj->downloadUserPayslip($userId);

        if (!$result['status'] || empty($result['data'])) {
            return Redirect::back()->withErrors($result['log']);
        }
        Excel::create('New file', function ($excel) use ($result) {

            $excel->sheet('New sheet', function ($sheet) use ($result) {

                $sheet->loadView('excel.payslip-user', ['data' => $result['data']]);
            });
        })->export('xls');
    }
    public function viewpdf(Request $request, $userId)
    {
        if (!$request->user) {
            return Redirect::back()->with('message', 'Invalid Link Followed!');
        }


        $resultObj = new CtcService;
        $result = $resultObj->downloadUserPayslip($userId);

        if (!$result['status'] || empty($result['data'])) {
            return Redirect::back()->withErrors($result['log']);
        }
        $data=$result['data'];
        return view('pdf.payslip-user', compact('data'));
        // $pdf = PDF::loadView('excel.payslip-user', compact('data'))->setPaper('A4', 'landscape');
        // return $pdf->stream('payslip.pdf', array('Attachment'=>0));
    }
}
