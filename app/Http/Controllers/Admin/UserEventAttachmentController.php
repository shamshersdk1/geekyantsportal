<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserEventAttachment;
use Redirect;
use Response;

class UserEventAttachmentController extends Controller
{
    public function destroy($id)
    {
        $uea=UserEventAttachment::find($id);
        if ($uea) {
            $uea->delete();
            return redirect::back()->with('message', 'Attachment deleted');
        }
        return redirect::back();
    }
    public function index()
    {
        return redirect::back();
    }public function show()
    {
        return redirect::back();
    }
    public function download($id)
    {
        $attachment=UserEventAttachment::find($id);
        $filename=$attachment->attachment;
        $file=storage_path('Events/'.str_replace(' ', '_', $attachment->userevent->event->name).'/'.$attachment->userevent->user->id).'/'.$filename;
        return Response::download($file, $filename);
    }
}
