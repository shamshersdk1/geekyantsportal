<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\PayslipDataItem;
use App\Models\Admin\PayslipDataMaster;
use App\Models\Admin\PayslipDataMonth;
use App\Models\User;
use App\Services\PayslipDataService;
use Auth;
use Illuminate\Http\Request;
use Redirect;

class PayslipDataController extends Controller
{
    public function index()
    {
        $month = date('m');
        $year = date('Y');
        $obj = PayslipDataMonth::whereMonth('date', $month)->whereYear('date', $year)->first();
        if (!$obj) {
            $payslipDataMonth = new PayslipDataMonth();
            $payslipDataMonth->date = date('Y-m-d');
            $payslipDataMonth->is_locked = false;
            $payslipDataMonth->save();
        }
        $payslipMonths = PayslipDataMonth::all();
        $selectedMonth = PayslipDataMonth::orderBy('id', 'DESC')->first();

        return View('pages/admin/payslip-data/index', ['payslipMonths' => $payslipMonths, 'selectedMonth' => $selectedMonth]);
    }

    public function getPayslipMonthMaster($id)
    {
        $authUser = Auth::user();
        $payslipDataMonth = PayslipDataMonth::find($id);
        if (!$payslipDataMonth) {
            return redirect('/admin/payslip-data/');
        }
        $users = User::where('is_active', 1)->get();
        $masterListExists = PayslipDataMaster::where('payslip_data_month_id', $id)->first();
        if (!$masterListExists) {
            PayslipDataService::createRecords($id);
        } else if (!$payslipDataMonth->is_locked) {
            foreach ($users as $user) {
                $payslipObj = PayslipDataMaster::where('payslip_data_month_id', $id)->where('user_id', $user->id)->first();
                if (!$payslipObj) {
                    $payslipDataObj = new PayslipDataMaster();
                    $payslipDataObj->user_id = $user->id;
                    $payslipDataObj->payslip_data_month_id = $id;
                    $payslipDataObj->created_by = $authUser->id;
                    $payslipDataObj->save();
                }
            }
        }
        $payslipMonthMasterList = PayslipDataMaster::where('payslip_data_month_id', $id)->orderBy('id', 'ASC')->get();

        $missingUsersIds = [];
        $missingUsers = [];

        $currentMonth = date('Y-m-d');
        $lastMonth = date("Y-m-d", strtotime("first day of previous month"));

        // return View('pages/admin/payslip-data/master-create',['users' => $users, 'payslipMonth' => $payslipMonth,'payslipMonthMaster' => $payslipMonthMaster]);
        return View('pages/admin/payslip-data/master', compact('payslipMonthMasterList', 'payslipDataMonth', 'currentMonth', 'lastMonth', 'missingUsers'));
    }

    public function getPayslipMonthItem($id)
    {
        $payslipDataMonth = PayslipDataMonth::find($id);
        if (!$payslipDataMonth) {
            return redirect('/admin/payslip-data/');
        }
        $authUser = Auth::user();
        $users = User::where('is_active', 1)->get();
        $itemListExists = PayslipDataItem::where('payslip_data_month_id', $id)->first();
        if (!$itemListExists) {
            PayslipDataService::createItemRecords($id);
        } else if (!$payslipDataMonth->is_locked) {
            foreach ($users as $user) {
                $payslipObj = PayslipDataItem::where('payslip_data_month_id', $id)->where('user_id', $user->id)->first();
                if (!$payslipObj) {
                    $payslipDataObj = new PayslipDataItem();
                    $payslipDataObj->user_id = $user->id;
                    $payslipDataObj->payslip_data_month_id = $id;
                    $payslipDataObj->created_by = $authUser->id;
                    $payslipDataObj->save();
                }
            }
        }
        $payslipMonthItemList = PayslipDataItem::where('payslip_data_month_id', $id)->orderBy('id', 'ASC')->get();

        return View('pages/admin/payslip-data/item', compact('payslipMonthItemList', 'payslipDataMonth'));
    }

    public function savePayslipMonthMaster(Request $request)
    {
        $user = Auth::user();
        if (!$user) {
            return Redirect::back()->withErrors(['Invalid Access']);
        }
        $input = $request->all();
        PayslipDataService::updateMasterDetails($input, $user->id);
        return redirect('/admin/payslip-data/data-master/' . $input['payslip_data_month_id']);
    }

    public function savePayslipMonthItem(Request $request)
    {
        $user = Auth::user();
        if (!$user) {
            return Redirect::back()->withErrors(['Invalid Access']);
        }
        $input = $request->all();
        PayslipDataService::updateItemDetails($input, $user->id);
        return redirect('/admin/payslip-data/data-item/' . $input['payslip_data_month_id']);
    }
    public function lock($id)
    {
        $user = Auth::user();
        if (!$user || !$user->isAdmin()) {
            return Redirect::back()->withErrors(['Invalid Access']);
        }

        $payslipDataMonth = PayslipDataMonth::find($id);
        if ($payslipDataMonth) {
            $payslipDataMonth->is_locked = 1;
            $payslipDataMonth->save();
        }
        return redirect('/admin/payslip-data/data-master/' . $id);
    }
}
