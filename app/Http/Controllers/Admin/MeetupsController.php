<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Redirect;
use Config;
use Exception;
use DB;
use Auth;
use Validator;

class MeetupsController extends Controller
{
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return View('pages/admin/cms-meetups/index');
    }

       /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();
        $user = Auth::user();
            return View('pages/admin/cms-meetups/add',['users' => $users]);
       
        
    }
    public function show()
    {
        
        return View('pages/admin/cms-meetups/view');
    }



    
}
