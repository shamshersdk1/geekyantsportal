<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\News;
use Log;
use Redirect;

class NewsController extends Controller
{
    public function store(Request $request)
    {
        $response=News::saveData($request, 0);
        if ($response['status']) {
            return redirect::back()->with('message', $response['message']);
        } else {
            return redirect::back()->withErrors($response['error']);
        }
    }
    public function update(Request $request, $id)
    {
        $response=News::saveData($request, $id);
        if ($response['status']) {
            return redirect::back()->with('message', $response['message']);
        } else {
            return redirect::back()->withErrors($response['error']);
        }
    }
    public function show(Request $request, $id)
    {
        if (!$request->user) {
            return response("Users only", 401);
        }

        if (! $request->user->isAdmin()) {
            return response("Admins only", 401);
        }

        $news = News::find($id);
        if (!$news) {
            return response()->json("News not found", 400);
        }
        return response()->json($news);
    }
}
