<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\TeckTalkService;

use Log;
use Redirect;

class TeckTalkController extends Controller {

	public function index() 
	{	
		$techtalks = TeckTalkService::getData();
		return View('pages/admin/tech-talks/index', compact('techtalks'));
	}
 }

    
