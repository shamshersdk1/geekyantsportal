<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\IpAddressMapper;
use App\Models\Admin\MyNetworkDevice;
use App\Models\Admin\UserIpAddress;
use App\Models\User;
use DB;
use Illuminate\Http\Request;

class MyNetworkDeviceController extends Controller
{
    public function index()
    {
        $AllNetworkDevices = MyNetworkDevice::with('ip.userIp')->orderBy('id', 'DESC')->get();
        return view('pages.admin.all-network-devices.index', compact('AllNetworkDevices'));
    }
    public function create()
    {
        $userList = User::Where('is_active', 1)->orderBy('name', 'asc')->get();
        return view('pages.admin.all-network-devices.add', compact('userList'));
    }
    public function store(Request $request)
    {
        $data = $request->all();
        if (empty($data)) {
            return redirect()->back()->withErrors('Empty Data sent')->withInput();
        }

        $res = UserIpAddress::userFreeIp($data['user_id']);
        if (!$res['status']) {
            return redirect()->back()->withErrors('User has no free Ip available to assign to the device.Please assign ips to user')->withInput();
        }
        $data['user_ip_address_id'] = $res['result']->id;

        $isUsed = MyNetworkDevice::where('mac_address', $data['mac_address'])->first();
        $isUsedInAsset = AssetMeta::where('value', $data['mac_address'])->first();
        if ($isUsed || $isUsedInAsset) {
            return redirect()->back()->withErrors('Mac Address is already in use')->withInput();
        }

        $myDeviceObj = new MyNetworkDevice;
        $response = MyNetworkDevice::saveData($data, $myDeviceObj);
        if (!$response['status']) {
            return redirect()->back()->withErrors($response['message'])->withInput();
        }
        return redirect('/admin/all-network-devices')->with('message', 'Saved!');
    }
    public function edit($id)
    {
        $userList = User::Where('is_active', 1)->orderBy('name', 'asc')->get();
        $userDeviceObj = MyNetworkDevice::find($id);
        $userFreeIpList = [];
        $UserIpAddress = UserIpAddress::where('user_id', $userDeviceObj->user_id)->get();
        foreach ($UserIpAddress as $userIp) {
            $isUsedInMyDevice = IpAddressMapper::where('user_ip_address_id', $userIp->id)->with('userIp')->first();
            if (!$isUsedInMyDevice) {
                array_push($userFreeIpList, $userIp);
            }
        }
        return view('pages.admin.all-network-devices.edit', compact('userList', 'userDeviceObj', 'userFreeIpList'));
    }
    public function update(Request $request, $id)
    {
        $data = $request->all();
        if (empty($data)) {
            return redirect()->back()->withErrors('Empty Data sent')->withInput();
        }
        $myDeviceObj = MyNetworkDevice::find($id);

        $response = MyNetworkDevice::saveData($data, $myDeviceObj);
        if (!$response['status']) {
            return redirect()->back()->withErrors($response['message'])->withInput();
        }
        return redirect('/admin/all-network-devices')->with('message', 'Saved!');

    }
    public function destroy($id)
    {
        DB::beginTransaction();
        $myDeviceObj = MyNetworkDevice::where('id', $id)->delete();
        if (!$myDeviceObj) {
            DB::rollback();
            return redirect()->back()->withErrors("Unable to delete device");
        }
        $IpAddressMapperObj = IpAddressMapper::where('reference_type', 'App\Models\Admin\MyNetworkDevice')->where('reference_id', $id)->first();
        if ($IpAddressMapperObj) {
            if (!$IpAddressMapperObj->delete()) {
                DB::rollback();
                return redirect()->back()->withErrors("Unable to release ip from the device");
            }
        }
        DB::commit();
        return redirect('/admin/all-network-devices')->with('message', 'Successfully Deleted!');
    }
}
