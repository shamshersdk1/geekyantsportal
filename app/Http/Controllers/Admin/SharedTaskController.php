<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\User;
use App\Models\Admin\SharedTask;
use App\Models\Admin\UserTask;

use Redirect;
use Auth;
use Input;
use Carbon\Carbon;

class SharedTaskController extends Controller
{
    
    public function index()
    {
        return Redirect::back();
    }

    public function show($id)
    {
        return Redirect::back();
    }
    public function store(Request $request)
    {
        if(empty($request->new_developers) || !(Auth::check())) {
            return Redirect::back()->withErrors(["Invalid request"]);    
        }
        $owner_id = Auth::id();
        foreach($request->new_developers as $user_id)
        {
            $user = User::find($user_id);
            if(!$user) {
                continue;
            }
            $shared = new SharedTask();
            $shared->user_id = $user_id;
            $shared->owner_id = $owner_id;
            $shared->created_by = $owner_id;
            $shared->is_removed = false;
            $shared->save();
        }
        return Redirect::back()->with('message', "Added Successfully");
    }

    public function detachUser($id)
    {
        if(Auth::check()) {
            $shared = SharedTask::where('owner_id', Auth::id())->where('user_id', $id)->delete();
            return Redirect::back()->with('message', "Removed successfully");
        }
        return Redirect::back()->withErrors(["You need to login first"]);
    }
    
}
