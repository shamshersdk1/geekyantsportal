<?php

namespace App\Http\Controllers\Admin;

use App\Events\Loan\LoanRequested;
use App\Http\Controllers\Controller;
use App\Jobs\Loan\LoanApproveJob;
use App\Jobs\Loan\LoanDisburseJob;
use App\Jobs\Loan\LoanRejectJob;
use App\Models\Admin\File;
use App\Models\Admin\LoanRequest;
use App\Models\Appraisal\AppraisalBonusType;
use App\Models\Loan;
use App\Models\LoanEmi;
use App\Models\LoanTransaction;
use App\Models\User;
use App\Services\FileService;
use App\Services\LoanService;
use Auth;
use Illuminate\Http\Request;
use Log;
use Redirect;

class LoanController extends Controller
{
    public function index()
    {
        $loans = Loan::get();
        return view('pages.admin.loan.index', compact('loans'));
    }
    public function create()
    {
        if (Auth::User() && (!Auth::User()->hasRole('human-resources') && !Auth::User()->hasRole('admin'))) {
            return Redirect('/admin/loans');
        }
        $loan_types = AppraisalBonusType::where('is_loanable', 1)->get();
        if (!count($loan_types) > 0) {
            return Redirect::back()->withInput()->withErrors("Loan Types not found");
        }
        $jsonuser = User::getUsers();
        $users = User::where('is_active', 1)->get();
        return view('pages.admin.loan.add', compact('jsonuser', 'users', 'loan_types'));
    }
    public function store(Request $request)
    {
        $response['status'] = false;
        $input = $request->all();

        if (!$input) {
            return Redirect::to("admin/loans")->withErrors('Empty Input');
        }
        $role = isset($input['from_admin']) ? 'Admin' : 'User';
        $response = LoanService::loanEligibleCheck($input['user_id'], $input['annual_bonus_type_id'], $input['amount']);
        if ($response['status']) {
            return Redirect::back()->withInput()->withErrors($response['error']);
        }
        $emi = $input['emi'];
        if ($response['appraisal_bonus_id'] != null) {
            $emi = $input['amount'];
        }
        $loanRequest = LoanRequest::create(['user_id' => $input['user_id'], 'amount' => $input['amount'], 'appraisal_bonus_id' => $response['appraisal_bonus_id'] ? $response['appraisal_bonus_id'] : null, 'description' => $input['description'], 'emi' => $emi, 'emi_start_date' => $input['emi_start_date'], 'application_date' => $input['application_date']]);
        if (!$loanRequest->validate($input)) {
            return Redirect::back()->withInput()->withErrors($loanRequest->getError());
        }
        event(new LoanRequested($loanRequest->id, 'approved', $role, $loanRequest->description));
        return Redirect::to("admin/loans")->with('message', 'Saved Successfully');

    }
    public function update(Request $request, $id)
    {
        if (empty($request) || empty($id)) {
            return Redirect::back()->withErrors(['Something went wrong']);
        }
        $loan = Loan::find($id);
        if ($request->status == 'approve') {
            $loan->status = 'approved';
            $loan->approved_by = \Auth::id();
            if ($loan->save()) {
                $job = (new LoanApproveJob($loan))->onQueue('loan-approve');
                dispatch($job);
            }
        } elseif ($request->status == 'reject') {
            $loan->status = 'rejected';
            if ($loan->save()) {
                $job = (new LoanRejectJob($loan))->onQueue('loan-reject');
                dispatch($job);
            }
        } elseif ($request->status == 'disburse') {
            $date = date('Y-m-d');
            $loan->status = 'disbursed';
            if (empty($request->payment_detail) || empty($request->payment_mode) || empty($request->transaction_id) || empty($request->payment_date)) {
                return Redirect::back()->withErrors(['All fields are mandatory']);
            }
            $loan->payment_details = $request->payment_detail;
            $loan->payment_mode = $request->payment_mode;
            $loan->transaction_id = $request->transaction_id;
            $loan->payment_date = $request->payment_date;
            if ($loan->save()) {
                $job = (new LoanDisburseJob($loan))->onQueue('loan-disburse');
                dispatch($job);
            }
            $loan_transaction = new LoanTransaction();
            $loan_transaction->loan_id = $loan->id;
            $loan_transaction->amount = -($loan->amount);
            $loan_transaction->paid_on = $date;
            $loan_transaction->status = 'paid';
            $loan_transaction->method = 'Loan Disbursed';
            if (!$loan_transaction->save()) {
                \Log::info($loan_transaction->getErrors());
            }

        } elseif ($request->status == 'cancel') {
            $loan->status = 'cancelled';
            $loan->save();
        } elseif ($request->status == 'complete') {
            $loan->status = 'completed';
            $loan->save();
        } else {
            return Redirect::back()->withErrors(['Invalid Action']);
        }
        return Redirect::back()->with('message', 'Updated successfully');
    }
    public function show($id)
    {
        $loan = Loan::with('loan_transactions', 'loan_request')->find($id);
        if ($loan) {
            if ($loan->loan_request) {
                $file = File::where("reference_type", "App\Models\Admin\LoanRequest")->where("reference_id", $loan->loan_request->id)->first();
            }
            $loanCompleteFile = File::where("reference_type", "App\Models\Admin\Loan")->where("reference_id", $loan->id)->first();
            $loanTransactions = LoanEmi::where('loan_id', $id)->orderBy('paid_on', 'DESC')->get();
            //$loanInterests = LoanInterest::where('loan_id', $id)->orderBy('created_at', 'ASC')->get();
            $remainingAmount = $loan->amount - abs($loan->emis()->where('status','paid')->sum('amount'));
            return view('pages.admin.loan.show', compact('remainingAmount','loan', 'loanTransactions', 'file','loanCompleteFile'));
        } else {
            return Redirect::back()->withErrors(['Invalid Link followed']);
        }
    }
    public function destroy($id)
    {
        $loan = Loan::find($id);
        if ($loan) {
            foreach ($loan->emis as $emi) {
                $emi->delete();
            }
            $loan->delete();
            return redirect::back()->with('message', 'Successfully Deleted');
        }

        return redirect::back()->withErrors('Could not delete');
    }

    public function changeStatus($id, Request $request)
    {
        $loan = Loan::find($id);
        if ($loan) {
            if ($request->approve) {
                $loan->status = 'approved';
                $loan->approved_by = \Auth::id();
                $loan->save();
                return Redirect::back()->with('message', 'Loan approved');
            } elseif ($request->reject) {
                $loan->status = 'rejected';
                $loan->approved_by = \Auth::id();
                $loan->save();
                return Redirect::back()->with('message', 'Loan rejected');
            } else {
                return Redirect::back()->withErrors(['Invalid link followed']);
            }
        } else {
            return Redirect::back()->withErrors(['Invalid link followed']);
        }
    }

    public function loanLedger()
    {
        $loansEmi = LoanTransaction::with('loan')->orderBy('created_at', 'ASC')->get();
        $balance = 0;
        $total_disbursed = 0;
        $total_repaid = 0;
        foreach ($loansEmi as $emi) {
            $balance = $balance + $emi->amount;
            if ($emi->amount > 0) {
                $total_repaid = $total_repaid + $emi->amount;
            } else {
                $total_disbursed = $total_disbursed + $emi->amount;
            }
        }
        return view('pages.admin.loan.ledger', compact('loansEmi', 'balance', 'total_disbursed', 'total_repaid'));

    }
    public function viewDashboard()
    {
        $total_users = User::where('is_active', 1)->distinct('id')->count();
        $total_users_with_loans = Loan::where('status', '!=', 'completed')->distinct('user_id')->count();
        $total_users_without_loans = $total_users - $total_users_with_loans;
        $total_disbursed = LoanTransaction::where('type', 'debit')->sum('amount');
        $total_repaid = LoanTransaction::where('type', 'credit')->sum('amount');
        $balance = $total_repaid - $total_disbursed;
        if ($balance < 0) {
            $balance = -$balance;
        }

        return view('pages.admin.loan.dashboard', compact('total_users_with_loans', 'total_users_without_loans', 'total_disbursed', 'total_repaid', 'balance'));
    }

    public function uploadFile(Request $request, $id)
    {
        $loan = Loan::find($id);
        if ($loan && $loan->status == "approved") {
            $file = $request->file;
            $fileSize = $file->getClientSize();
            if ($fileSize > 20000000) {
                return Redirect::back()->withInput()->withErrors(['Filesize exceeds 20MB']);
            }
            $type = $file->extension();
            $fileName = FileService::getFileName($type);
            $originalName = $file->getClientOriginalName();
            $upload = FileService::uploadFileInStorage($file, $fileName, $id, "App\Models\Loan", $originalName, "Loan");
            return Redirect::back()->with('message', 'File Added');
        } else {
            return Redirect::back()->withErrors(['Invalid link followed']);
        }
    }

    public function streamFile($id, $file_id)
    {
        $file = File::find($file_id);
        if ($file && $file->reference_id == $id) {
            $path = $file->path;
            return response()->file($path);
        } else {
            return Redirect::back()->withErrors(['Invalid link followed']);
        }
    }

    public function editLoan($id)
    {
        if (Auth::User() && (!Auth::User()->hasRole('human-resources') && !Auth::User()->hasRole('admin'))) {
            return Redirect('/admin/loans');
        }

        $loan = Loan::find($id);
        $file = null;
        if (empty($loan)) {
            return Redirect::back()->withErrors(['Loan not found']);
        }
        if ($loan->loan_request_id) {
            $file = File::where("reference_type", "App\Models\Admin\LoanRequest")->where("reference_id", $loan->loan_request_id)->first();
        }
        $users = User::where('is_active', 1)->get();
        return view('pages.admin.loan.editLoan', compact('users', 'loan', 'file'));
    }

    public function updateLoan(Request $request, $id)
    {
        $input = $request->all();
        $file_id = null;

        if ($request->file) {
            $file = $request->file;
            $fileSize = $file->getClientSize();
            if ($fileSize > 20000000) {
                return Redirect::back()->withInput()->withErrors(['Filesize exceeds 20MB']);
            }
            $type = $file->extension();
            $fileName = FileService::getFileName($type);
            $originalName = $file->getClientOriginalName();
            $uploadedFile = FileService::uploadFileInStorage($file, $fileName, 1, "App\Models\Admin\LoanRequest", $originalName, "LoanRequest");
            $file_id = $uploadedFile->id;
        }

        $response = LoanService::updateLoanRequest($id, $input, $file_id);
        if (!$response['status']) {
            return Redirect::back()->withErrors($response['message'])->withInput();
        }
        return Redirect::back()->with('message', 'Loan request updated successfully');
    }

    public function completeLoanShow($id)
    {
        if (Auth::User() && (!Auth::User()->hasRole('human-resources') && !Auth::User()->hasRole('admin'))) {
            return Redirect('/admin/loans');
        }

        $loan = Loan::find($id);
        $file = null;
        if (empty($loan)) {
            return Redirect::back()->withErrors(['Loan not found']);
        }
        if ($loan->loan_request_id) {
            $file = File::where("reference_type", "App\Models\Admin\LoanRequest")->where("reference_id", $loan->loan_request_id)->first();
        }
        $users = User::where('is_active', 1)->get();
        return view('pages.admin.loan.complete-loan', compact('users', 'loan', 'file'));
    }

    public function completeLoanUpdate($id)
    {
        if (Auth::User() && (!Auth::User()->hasRole('human-resources') && !Auth::User()->hasRole('admin'))) {
            return Redirect('/admin/loans');
        }

        $loanObj = Loan::find($id);

        if (empty($loanObj)) {
            return Redirect::back()->withErrors(['Loan not found']);
        }
        $status = 'completed';
        $response = LoanService::updateLoanStatus($id, $status);
        if ($response['status']) {
            return Redirect::to('/admin/loans/' . $id)->with('message', 'Loan request updated successfully');
        } else {
            return Redirect::back()->withErrors($response['message']);
        }

        return view('pages.admin.loan.complete-loan', compact('users', 'loan', 'file'));
    }

    public function completeLoan($id, Request $request)
    {
        if(!$request->file)
        {
            return Redirect::back()->withErrors("Please upload file first.");
        }
        $loan = Loan::find($id);
        if($loan)
        {
            $remainingAmount = $loan->emis()->where('status','paid')->sum('amount')+$loan->amount;
            if($remainingAmount > 0)
            {
                return Redirect::back()->withErrors("Loan has remaining amount.");
            }
            $response = Loan::uploadFile($request,$id);
            if(!$response['status'])
            {
               return Redirect::back()->withErrors($response['message']);
            }
            $loan->status = "completed";
            if(!$loan->update())
            {
                return Redirect::back()->withErrors("Something went wrong!");
            }
        }
        return Redirect::back()->withMessage("Loan Completed.");
    }
    public function completeLoanTransaction($id, Request $request)
    {
        $input = $request->all();
        $response = LoanService::completeLoanPayment($id, $input);
        if ($response['status']) {
            return Redirect::to('/admin/loans/' . $id)->with('message', 'Loan request updated successfully');
        } else {
            return Redirect::back()->withErrors($response['message']);
        }
        return view('pages.admin.loan.add-transaction', compact('loan'));
    }
    public function emiMonth()
    {
        $loans = Loan::where('remaining', '>', 0)->get();
        return view('pages.admin.loan.emi-deduction', compact('loans'));
    }
}
