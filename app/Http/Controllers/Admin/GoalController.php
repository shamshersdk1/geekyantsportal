<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Goal;
use App\Models\Admin\Tag;
use App\Models\Admin\UserGoal;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Redirect;
use Validator;
use View;

class GoalController extends Controller
{
    public function index()
    {
        $goals = Goal::getPublicGoals();
        return View::make('pages/admin/goals/index', compact('goals'));
    }

    public function create()
    {
        $tags = Tag::pluck('name')->toArray();
        $tags = '["' . implode('","', $tags) . '"]';
        return View::make('pages/admin/goals/create', compact('tags'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'process' => 'required',
        ]);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator->errors());
            return $response;
        }
        $response = Goal::saveGoal($request->toArray());
        if ($response['status']) {
            return Redirect::to('admin/goals')->with('message', "Saved successfully");
        } else {
            return Redirect::back()->withErrors($response['message']);
        }
    }

    public function show($id)
    {
        $goal = Goal::with('tags')->find($id);
        if ($goal) {
            return View::make('pages/admin/goals/view-goal', compact('goal'));
        } else {
            return Redirect::back()->withErrors(["Invalid link followed"]);
        }
    }

    public function edit($id)
    {
        $goal = Goal::with('tags')->find($id);
        $tags = Tag::pluck('name')->toArray();
        $tags = '["' . implode('","', $tags) . '"]';
        if ($goal->tags) {
            $selected = $goal->tags()->pluck('name')->toArray();
            $selected = '["' . implode('","', $selected) . '"]';
        }
        if ($goal) {
            return View::make('pages/admin/goals/edit', compact('goal', 'selected', 'tags'));
        } else {
            return Redirect::back()->withErrors(["Invalid link followed"]);
        }
    }

    public function update(Request $request, $id)
    {
        $response = Goal::updateGoal($request->toArray(), $id);
        if ($response['status']) {
            return Redirect::to('admin/goals')->with('message', "Updated successfully");
        } else {
            return Redirect::back()->withErrors($response['message'])->withInput();
        }
    }

    public function destroy($id)
    {
        return Redirect::back()->withErrors(['Invalid link followed']);
    }
    public function deleteGoal($id)
    {
        $goal = Goal::find($id);
        if ($goal) {
            $goal->delete();
            return Redirect::back()->with('message', "Deleted successfully");
        } else {
            return Redirect::back()->withErrors(["Invalid link followed"]);
        }
    }

    public function myGoals()
    {
        if (Auth::check()) {
            $user = Auth::user();
            $goals = UserGoal::where('user_id', $user->id)->where('status', '!=', 'pending')->get();
            return View::make('pages/admin/goals/my-goals', compact('goals', 'user'));

        }
    }

    public function goalUserView($id)
    {
        $manager_flag = false;
        if (Auth::check()) {
            $user = Auth::user();
            $goal = UserGoal::find($id);
            if ($goal) {
                return View::make('pages/admin/goals/user-view', compact('goal', 'user', 'manager_flag'));
            } else {
                return Redirect::back()->withErrors(["Goal not found"]);
            }
        }
    }

    public function userList()
    {
        $user = Auth::user();
        // $users = User::where('is_active', 1)->orderBy('employee_id')->paginate(20);
        $users_list = UserGoal::distinct()->select('user_id')->pluck('user_id')->toArray();
        $users = User::whereIn('id', $users_list)->where('parent_id', $user->id)->orderBy('employee_id')->paginate(20);
        return View::make('pages/admin/goals/user-list', compact('users'));
    }

    public function assignGoal()
    {
        return View::make('pages/admin/goals/goal-assignment');
    }

    public function userGoalView($user_id, $goal_id)
    {
        $manager_flag = true;
        if (Auth::check()) {
            $user = Auth::user();
            $goal = UserGoal::find($goal_id);
            if ($goal) {
                return View::make('pages/admin/goals/user-view', compact('goal', 'user', 'manager_flag'));
            } else {
                return Redirect::back()->withErrors(["Goal not found"]);
            }
        }

    }
    public function goalReport()
    {
        $user_goals = UserGoal::All();
        return View::make('pages/admin/goals/goal-report', compact('user_goals'));
    }
    public function monthlyResult()
    {
        $monthId = Input::get('id');
        if (!empty($monthId)) {
            $user_goals = UserGoal::whereMonth('from_date', '=', $monthId)->get();
            return View::make('pages/admin/goals/goal-report', compact('user_goals'));
        }
        $user_goals = UserGoal::All();
        return View::make('pages/admin/goals/goal-report', compact('user_goals'));
    }
}
