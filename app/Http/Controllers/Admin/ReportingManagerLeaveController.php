<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Admin\Client;
use App\Models\Admin\Leave;
use App\Models\Admin\ProjectResource;
use App\Models\Admin\Project;
use App\Models\ProjectManagerLeaveApproval;

use App\Services\SlackService\SlackMessageService\Reminder\Leave as SlackLeaveReminder;

use Input;
use Redirect;
use Illuminate\Pagination\LengthAwarePaginator;

class ReportingManagerLeaveController extends Controller
{
    public function index(Request $request)
    {
        $message = [];
        $resources = \Auth::user()->projectResources()->where(function ($query){
            $query->where('project_resources.end_date', null)->orWhere('project_resources.end_date', '>=', date('Y-m-d'));
        })->where('project_resources.start_date', '<=', date('Y-m-d'))->pluck('user_id')->toArray();
        $users = User::where('parent_id', \Auth::id())->pluck('id')->toArray();
        $users = array_unique(array_merge($users, $resources));
        if(!empty($request->searchuser) && $request->searchuser != '') {
            $searchedUser = User::where('name', str_replace('+', '_', $request->searchuser))->first();
            if(empty($searchedUser)) {
                $users = [''];
                $message[] = 'Select a valid user';
            } else {
                if ( in_array($searchedUser->id, $users) )
                {
                    $users = [$searchedUser->id];
                }
                else 
                {
                    $users = [''];
                    $message[] = 'Invalid Request';
                }
            }    
        }
        if((!empty($request->start_date) && $request->start_date != '') && (!empty($request->end_date) && $request->end_date != '') && ($request->start_date <= $request->end_date)) {
            $pendingLeaves = Leave::where('end_date', '>=', date_to_yyyymmdd($request->start_date))->where('start_date', '<=', date_to_yyyymmdd($request->end_date))->whereIn('user_id', $users)->where('status', 'pending')->orderBy('start_date')->get();
            $leaves = Leave::where('end_date', '>=', date_to_yyyymmdd($request->start_date))->where('start_date', '<=', date_to_yyyymmdd($request->end_date))->whereIn('user_id', $users)->whereNotIn('status', ['pending'])->orderByRaw("FIELD(status, 'approved')")->orderBy('start_date', 'ASC')->get()->reverse();
        } else {
            if((!empty($request->start_date)||!empty($request->end_date))||($request->start_date > $request->end_date)) {
                $message[] = 'Dates are invalid';
            }
            $pendingLeaves = Leave::whereIn('user_id', $users)->where('status', 'pending')->orderBy('start_date')->get();
            $leaves = Leave::whereIn('user_id', $users)->whereNotIn('status', ['pending'])->orderByRaw("FIELD(status, 'approved')")->orderBy('start_date', 'ASC')->get()->reverse();
        }
        $leaves = $pendingLeaves->merge($leaves)->all();
        foreach($leaves as $leave)
        {
            if($leave->user->parent_id == \Auth::id()) {
                $leave->parent = 1;
                if(empty($leave->projectManagerApprovals) || $leave->projectManagerApprovals()->where('status', 'pending')->count() == 0) {
                    $leave->ready = 1;
                } else {
                    $leave->ready = 0;
                }
            } else {  
                $leave->parent = 0;
                if(!empty($leave->projectManagerApprovals)) {
                    $projManagerApprovalObj = $leave->projectManagerApprovals()->where('user_id', \Auth::id())->first();
                     if(!empty($projManagerApprovalObj) && $projManagerApprovalObj->status == 'pending') {
                        $leave->editable = 1;
                     } else {
                         $leave->editable = 0;
                     }
                } else {
                    $leave->editable = 0;
                }
            }
            if ( $leave->isLeaveType('paid') || $leave->isLeaveType('sick') ) {
                $leave->isOtherType = false;
            } else {
                $leave->isOtherType = true;
            }
        }
        $page = Input::get('page', 1); 
        $perPage = 10; 
        $offset = ($page * $perPage) - $perPage;
        
        $leaves =  new LengthAwarePaginator(
            array_slice($leaves, $offset, $perPage, true), 
            count($leaves), 
            $perPage, 
            $page, 
            ['path' => $request->url(), 'query' => $request->query()]
        );
        $jsonuser = User::getUsers();
        $jsonclients=Client::getClients();
        if(empty($message)) {
            return view('pages.admin.reporting-manager-leaves.index', compact('leaves', 'jsonclients', 'jsonuser'));
        } else {
            return view('pages.admin.reporting-manager-leaves.index', compact('leaves', 'jsonclients', 'jsonuser'))->withErrors($message);
        }
    }
    public function update(Request $request, $id)
    {
        $leave = Leave::find($id);
        if ($leave){
            if($leave->user->parent_id == \Auth::id()) {
                $leave->approver_id=\Auth::id();
                $leave->action_date=date('Y-m-d');
                if ($request->approve) {
                    $leave->status = "approved";
                    if ($leave->save()) {
                        if (!empty($request->client)) {
                            $client=Client::where('name', $request->client)->first();
                            if(!empty($client) && !empty($client->email)) {
                                Leave::notifyClient($client->email, $leave);
                            }
                        }
                        Leave::notifyLeaveAction($leave->id);
                        return redirect::back()->with('message', 'Leave Approved');
                    } else {
                        return redirect::back()->withErrors('Unable to save');
                    }
                } elseif ($request->cancel) {
                    $leave->status = "cancelled";
                    if ($leave->save()) {
                        Leave::notifyLeaveAction($leave->id);
                        return redirect::back()->with('message', 'Leave Cancelled');
                    } else {
                        return redirect::back()->withErrors('Unable to save');
                    }
                }
            } else {
                try {
                    $approval = $leave->projectManagerApprovals()->where('user_id', \Auth::id())->first();
                } catch(Exception $e) {
                    $approval = null;
                }
                if ($approval != null) {
                    if ($request->approve) {
                        $approval->status = "approved";
                        if($approval->save()) {
                            SlackLeaveReminder::remindReportingManager($id);
                            return redirect::back()->with('message', 'Request approved');
                        } else {
                            return redirect::back()->withErrors('Unable to save');
                        }
                    } elseif ($request->cancel) {
                        SlackLeaveReminder::remindReportingManager($id);
                        $approval->status = "cancelled";
                        if ($approval->save()) {
                            return redirect::back()->with('message', 'Request Cancelled');
                        } else {
                            return redirect::back()->withErrors('Unable to save');
                        }
                    } 
                } else {
                    return redirect::back()->withErrors('Something went wrong');
                }
            }
        } else {
            return redirect::back()->withErrors('Something went wrong');
        }
    }
    public function getInfo($id)
    {
        $leave = Leave::with(['user','overlapRequests'])->where('id', $id)->first();
        $resources = \Auth::user()->projectResources()->where(function ($query){
            $query->where('project_resources.end_date', null)->orWhere('project_resources.end_date', '>', date('Y-m-d'));
        })->where('project_resources.start_date', '<=', date('Y-m-d'))->pluck('user_id')->toArray();
        if($leave && ($leave->user->parent_id == \Auth::id() || array_search($leave->user->id, $resources) !== false)) {
            if($leave->user->parent_id == \Auth::id()) {
                $leave->parent = 1;
                $leave->editable = 0;
                $leave->projectManagers = $leave->projectManagerApprovals()->with('manager')->get();
                $projectIds = ProjectResource::getActiveProjectIdArray($leave->user->id, $leave->start_date, $leave->end_date);
                if(!empty($leave->projectManagers && !empty($projectIds))) {
                    foreach($leave->projectManagers as $key => $pmanager)
                    {
                        $projects = Project::where('project_manager_id', $pmanager->user_id)->whereIn('id', $projectIds)->pluck('project_name')->toArray();
                        if($projects) {
                            $leave->projectManagers[$key]->projects = $projects;
                        }
                    }
                }
                $leave->overlap = 0;
                if(count($leave->overlapRequests) > 0) {
                    $leave->overlap = 1;
                    $result = ProjectManagerLeaveApproval::getOverlappingLeaves($leave->user_id, $leave->overlapRequests()->pluck('project_id')->toArray(), $leave->start_date, $leave->end_date, $leave->id);
                    $data = $result['data'];
                    if($result['count']>0) {
                        $data['dates'] = $result['dates'];
                        $data['code'] = $result['code'];
                    }
                    $leave->overlapData = $data;
                }   
                if(empty($leave->projectManagerApprovals) || $leave->projectManagerApprovals()->where('status', 'pending')->count() == 0) {
                    $leave->ready = 1;
                } else {
                    $leave->ready = 0;
                }
            } else {
                $leave->parent = 0;
                $leave->ready = 0;
                $leave->overlap = 0;
                if(count($leave->overlapRequests) > 0) {
                    foreach($leave->overlapRequests as $overlap)
                    {
                        if($overlap->project->projectManager->id == \Auth::id()) {
                            $leave->overlap = 1;
                            $result = ProjectManagerLeaveApproval::getOverlappingLeaves($leave->user_id, [$overlap->project->id], $leave->start_date, $leave->end_date, $leave->id);
                            $data = $result['data'];
                            if($result['count']>0) {
                                $data['dates'] = $result['dates'];
                                $data['code'] = $result['code'];
                            }
                            $leave->overlapData = $data;
                            break;
                        }
                    }
                }
                if(!empty($leave->projectManagerApprovals)) {
                    $projManagerApprovalObj = $leave->projectManagerApprovals()->where('user_id', \Auth::id())->first();
                     if(!empty($projManagerApprovalObj) && $projManagerApprovalObj->status == 'pending') {
                        $leave->editable = 1;
                     } else {
                         $leave->editable = 0;
                     }
                } else {
                    $leave->editable = 0;
                }
            }
            return response()->json($leave);
        } else {
            return response()->json('Leave not found', 404);
        }
    }
    public function reject(Request $request, $id)
    {
        if(empty($request->rejectReason)) {
            return redirect::back()->withErrors('Rejection reason not specified');
        }
        $leave = Leave::find($id);
        $resources = \Auth::user()->projectResources()->where(function ($query){
            $query->where('project_resources.end_date', null)->orWhere('project_resources.end_date', '>', date('Y-m-d'));
        })->where('project_resources.start_date', '<=', date('Y-m-d'))->pluck('user_id')->toArray();
        if ($leave){
            if($leave->user->parent_id == \Auth::id()) {
                $leave->approver_id=\Auth::id();
                $leave->action_date=date('Y-m-d');
                $leave->status = "rejected";
                $leave->cancellation_reason = $request->rejectReason;
                if ($leave->save()) {
                    Leave::notifyLeaveAction($leave->id);
                    return redirect::back()->with('message', 'Leave Rejected');
                } else {
                    return redirect::back()->withErrors('Unable to save');
                }
            } else {
                try {
                    $approval = $leave->projectManagerApprovals()->where('user_id', \Auth::id())->first();
                } catch(\Exception $e) {
                    $approval = null;
                }
                if ($approval != null) {
                    $approval->status = "rejected";
                    $approval->rejection_reason = $request->rejectReason;
                    if ($approval->save()) {
                        SlackLeaveReminder::remindReportingManager($id);
                        return redirect::back()->with('message', 'Request Rejected');
                    } else {
                        return redirect::back()->withErrors('Unable to save');
                    }
                } else {
                    return redirect::back()->withErrors('Something went wrong');
                }
            }
        } else {
            return redirect::back()->withErrors('Something went wrong');
        }
    }
}
