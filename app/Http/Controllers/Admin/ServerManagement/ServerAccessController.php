<?php
namespace App\Http\Controllers\Admin\ServerManagement;

use App\Http\Controllers\Controller;
use App\Http\Requests\ValidateServerAccessRequest;
use App\Models\Audited\Server\Account;
use App\Models\Audited\Server\AccountAccess;
use App\Models\Audited\Server\Server;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;

class ServerAccessController extends Controller
{

    public function index($id)
    {

        $serverObj = Server::with('provider', 'type', 'account', 'accountAccess')->find($id);

        if (!$serverObj) {
            return redirect('admin/server-management/server')->withErrors('Server with requested Id not found !')->withInput();
        }
        $users = User::where('is_active', 1)->orderBy('name', 'asc')->get();
        $serverAccountAccess = AccountAccess::with('account', 'user')->where('server_id', $id)->get()->groupBy('account_id');

        return view('pages.server-management.server-access.index', compact('users', 'serverObj', 'serverAccountAccess'));
    }

    public function store(ValidateServerAccessRequest $request, $id)
    {
        $data = $request->all();
        // Is This required since validation is being checked in Model

        // if(!$data['user_id']){
        //         return redirect()->back()->withErrors('User Not Selected !')->withInput();
        // }

        // if(!$data['account_id']){
        //         return redirect()->back()->withErrors('Server Account Not Selected !')->withInput();
        // }

        $data['server_id'] = $id;

        if (isset($data['user_id']) && isset($data['account_id'])) {
            $serverAccess = AccountAccess::where('user_id', $data['user_id'])->where('server_id', $id)->where('account_id', $data['account_id'])->first();
            if ($serverAccess) {
                return redirect()->back()->withErrors('Same User Cannot be allocated to same account')->withInput();
            }

        }
        $serverAccountAccess = new AccountAccess();

        $serverAccountAccess->server_id = $id;
        $serverAccountAccess->user_id = $data['user_id'];
        $serverAccountAccess->account_id = $data['account_id'];

        if (!$serverAccountAccess->save()) {
            return redirect()->back()->withErrors('Unable To Save !')->withInput();
        } else {
            return redirect()->back()->with('message', 'Saved Successfully');
        }

    }
    public function destroy($serverId, $serverAccountAccessId)
    {

        $serverAccountObj = AccountAccess::find($serverAccountAccessId);

        if (!$serverAccountObj) {
            return redirect()->back()->withErrors('Id Not found')->withInput();
        }
        if (!$serverAccountObj->delete()) {
            return redirect()->back()->withErrors('Unable To delete')->withInput();
        } else {
            return redirect()->back()->with('message', 'Deleted Successfully');

        }

    }

    public function edit($id, $serverAccountAccessId)
    {

        $serverObj = Server::with('provider', 'type', 'account', 'accountAccess')->find($id);

        if (!$serverObj) {
            return redirect('admin/server-management/server')->withErrors('Server with requested Id not found !')->withInput();
        }
        $users = User::where('is_active', 1)->orderBy('name', 'asc')->get();

        $serverAccountAccess = AccountAccess::with('user', 'account')->find($serverAccountAccessId);
        if (!$serverAccountAccess) {
            return redirect('admin/server-management/server/' . $id . '/manage-access')->withErrors('Server Access Id Not Found!')->withInput();
        }
        return view('pages.server-management.server-access.edit', compact('users', 'serverAccount', 'serverObj', 'serverAccountAccess'));
    }

    public function update($serverId, $serverAccountAccessId, ValidateServerAccessRequest $request)
    {
        $serverAccountAccess = AccountAccess::find($serverAccountAccessId);

        $data = $request->all();
        $data['server_id'] = $serverId;

        if ($serverAccountAccess) {

            if (!$serverAccountAccess->validate($data)) {
                $errors = $serverAccountAccess->getError();
                return redirect()->back()->withErrors($errors)->withInput();
            }
            $serverAccountAccess->user_id = $data['user_id'];
            $serverAccountAccess->account_id = $data['account_id'];

            $serverAccess = AccountAccess::where('user_id', $request->user_id)->where('server_id', $serverId)->where('account_id', $request->account_id)->first();

            if ($serverAccess) {
                return redirect()->back()->withErrors('User With the account already present')->withInput();
            }
            if (!$serverAccountAccess->save()) {
                return redirect()->back()->withErrors('Unable To Update!')->withInput();
            } else {

                return redirect('admin/server-management/server/' . $serverId . '/manage-access')->with('message', 'Updated Successfully');
            }
        } else {
            return redirect()->back()->withErrors('Access account not found!')->withInput();
        }
    }

    public function saveServerAccount($serverId, Request $request)
    {
        $data = $request->all();
        $data['server_id'] = $serverId;

        $serverAccount = new Account();

        try {
            $serverAccount->username = $data['username'];
            $serverAccount->server_id = $data['server_id'];

            if (!$serverAccount->save()) {
                return redirect()->back()->withErrors('Unable To Save!')->withInput();
            }
            return redirect()->back()->with('message', 'Server Account Saved Successfully');
        } catch (Exception $e) {
            return redirect()->back()->withErrors('Account name exist!')->withInput();
        }

    }
    public function deleteAccount($serverId, $accountId)
    {
        $accountObj = Account::find($accountId);
        if (!$accountObj) {
            return redirect()->back()->withErrors('Account Id not found!')->withInput();
        }

        if (!$accountObj->delete()) {
            return redirect()->back()->withErrors('Account cannot be  deleted')->withInput();
        }

        return redirect()->back()->with('message', 'Server Account Deleted Successfully');

    }

}
