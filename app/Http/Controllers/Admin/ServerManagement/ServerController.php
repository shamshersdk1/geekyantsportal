<?php

namespace App\Http\Controllers\Admin\ServerManagement;

use App\Http\Controllers\Controller;
use App\Models\Audited\Server\Provider;
use App\Models\Audited\Server\ProviderAccount;
use App\Models\Audited\Server\Server;
use App\Models\Audited\Server\ServerType;
use Illuminate\Http\Request;
use Redirect;
use  App\Http\Requests\ValidateServerRequest;

class ServerController extends Controller
{
    public function index()
    {
        $serverObj = Server::paginate(50);
        $providerList = Provider::all();

        return view('pages.server-management.server.index', compact('serverObj', 'providerList'));
    }
    public function create($providerId = null)
    {
        $providerObj = null;
        if ($providerId != null) {
            $providerObj = Provider::find($providerId);
            if ($providerObj == null) {
                return redirect('/admin/server-management/server')->withErrors('Provider not found!');
            }
        }

        $providerList = Provider::all();
        if (count($providerList) == 0) {
            return Redirect::back()->withErrors('No Provider Available!')->withInput();
        }

        $typeList = ServerType::all();
        if (count($typeList) == 0) {
            return Redirect::back()->withErrors('Server Type not Available!')->withInput();
        }

        if ($providerObj == null) {
            $accountList = ProviderAccount::with('provider')->get();
        } else {
            $accountList = $providerObj->providerAccounts;
        }
        if (!$accountList || count($accountList) == 0) {
            return Redirect::back()->withErrors('Provider Accounts not Available!')->withInput();
        }

        return view('pages.server-management.server.add', compact('providerObj', 'providerList', 'typeList', 'accountList'));

    }
    public function store(ValidateServerRequest $request)
    {
        $data = $request->all();

        if (isset($data['provider_id']) && isset($data['provider_account_id'])) {
            $providerAccountObj = ProviderAccount::find($data['provider_account_id']);

            if ($providerAccountObj->provider) {
                //provider relation exist
                if ($data['provider_id'] != $providerAccountObj->provider->id) {
                    return Redirect::back()->withErrors('Account should be linked to the same provider!')->withInput();
                }
            } else {
                //if provider relation does not exist
                return Redirect::back()->withErrors('Account cannot be null!')->withInput();
            }
        }

            $serverObj = new Server();

        
            $result = $serverObj->saveServer($data);

            if ($result['status'] == true) {
                return redirect('/admin/server-management/server')->with('message', 'Server Added!');
            } else {
                return redirect()->back()->withErrors($result['message'])->withInput();
            }
        

    }
    public function update(Request $request, $id)
    {
        $data = $request->all();

        if (isset($data['provider_account_id'])) {
            $providerAccountObj = ProviderAccount::find($data['provider_account_id']);

            if ($data['provider_id'] != $providerAccountObj->provider->id) {
                return Redirect::back()->withErrors('Account should be linked to the same provider!')->withInput();
            }
        }
        $serverObj = Server::find($id);

        if ($serverObj) {

            $serverObj->rulesFunc($id);

            if ($serverObj->validate($data)) {

                $result = $serverObj->saveServer($data);
                if ($result['status'] == true) {
                    return redirect('/admin/server-management/server')->with('message', 'Updated!');
                } else {
                    return redirect()->back()->withErrors($result['message'])->withInput();
                }
            } else {
                $errors = $serverObj->getError();
                return redirect()->back()->withErrors($errors)->withInput();
            }
        }
        return redirect()->back()->withErrors('Server not Found!')->withInput();
    }

    public function destroy($id)
    {
        $serverObj = Server::find($id);
        if ($serverObj) {
            if (!$serverObj->delete()) {
                return redirect()->back()->withErrors('Server Cannot Be Deleted!');
            }
            return redirect()->back()->with('message', 'Successfully Deleted');

        }
    }
    public function edit($id, $providerId = null)
    {
        $providerObj = null;
        if ($providerId != null) {
            $providerObj = Provider::find($providerId);
            if ($providerObj == null) {
                return redirect('/admin/server-management/server')->withErrors('Provider not found!');
            }
        }

        $serverObj = Server::find($id);

        if (!$serverObj) {
            return redirect('/admin/server-management/server')->withErrors('Server not found!');
        }

        $providerList = Provider::all();
        if (count($providerList) == 0) {
            return Redirect::back()->withErrors('Provider not found!')->withInput();
        }
        $typeList = ServerType::all();
        if (count($typeList) == 0) {
            return Redirect::back()->withErrors('Server Type not found!')->withInput();
        }

        if ($providerObj == null) {
            $accountList = ProviderAccount::with('provider')->get();
        } else {
            $accountList = $providerObj->providerAccounts;
        }
        if (!$accountList || count($accountList) == 0) {
            return Redirect::back()->withErrors('Provider Accounts not Available!')->withInput();
        }

        return view('pages.server-management.server.edit', compact('providerObj', 'serverObj', 'providerList', 'typeList', 'accountList'));
    }
    public function manage($serverId)
    {
        $serverObj = Server::with('note', 'meta', 'account.access.user')->find($serverId);

        if (!$serverObj) {
            return Redirect::back()->withErrors('Invalid Server Id');
        }
        return view('pages.server-management.manage-server', compact('serverObj'));
    }

}
