<?php

namespace App\Http\Controllers\Admin\ServerManagement;

use App\Http\Controllers\Controller;
use App\Models\Audited\Server\Provider;
use App\Models\Audited\Server\ProviderAccount;
use Illuminate\Http\Request;
use Redirect;

class ServerProviderAccountsController extends Controller
{
    public function show($pid, $id)
    {
        $serverProviderAccount = ProviderAccount::find($id);
        if (!$serverProviderAccount) {
            return redirect()->back()->withErrors('Record not found!')->withInput();
        }
        return view('pages.server-management.server-provider.edit-provider-account', compact('serverProviderAccount'));
    }

    public function store(Request $request, $id)
    {
        $data = $request->all();
        $data["provider_id"] = $id;
        $serverProviderObj = new ProviderAccount();
        $providerAcc = ProviderAccount::where('provider_id', $data['provider_id'])->where('username', $data['username'])->first();

        if ($providerAcc) {
            return redirect()->back()->withErrors('Username already exists!')->withInput();
        }

        if ($serverProviderObj->validate($data)) {
            $serverProviderObj->provider_id = !empty($data['provider_id']) ? $data['provider_id'] : null;
            $serverProviderObj->username = !empty($data['username']) ? $data['username'] : null;
            $serverProviderObj->password = !empty($data['password']) ? $data['password'] : null;
            $serverProviderObj->url = !empty($data['url']) ? $data['url'] : null;
            $serverProviderObj->notes = !empty($data['notes']) ? $data['notes'] : null;
            $serverProviderObj->is_self_owned = $data['self_owned'];
            if (!$serverProviderObj->save()) {
                return redirect()->back()->withErrors('Unable To Save !')->withInput();
            }
            return redirect('/admin/server-management/server-provider/' . $serverProviderObj->provider->id)->with('message', 'Saved Successfully!');
        }
        $errors = $serverProviderObj->getError();
        return redirect()->back()->withErrors($errors)->withInput();
    }

    public function update(Request $request, $pid, $id)
    {
        $data = $request->all();
        $serverProviderObj = ProviderAccount::find($id);
        if (!$serverProviderObj) {
            return redirect()->back()->withErrors('Entry not found!')->withInput();
        }

        $data['provider_id'] = $pid;

        $serverProviderObj->rulesFunc($id);

        if ($serverProviderObj->validate($data)) {

            $serverProviderObj->username = $data['username'];
            $serverProviderObj->provider_id = $data['provider_id'];
            $serverProviderObj->password = $data['password'];
            $serverProviderObj->url = $data['url'];
            $serverProviderObj->notes = $data['notes'];
            $serverProviderObj->is_self_owned = $data['self_owned'];
            if (!$serverProviderObj->save()) {
                return redirect()->back()->withErrors('Something went wrong!')->withInput();
            }
            return redirect('/admin/server-management/server-provider/' . $pid)->with('message', 'Updated!');
        } else {
            $errors = $serverProviderObj->getError();
            return redirect()->back()->withErrors($errors)->withInput();
        }
    }

    public function destroy($pid, $id)
    {
        $serverProviderObj = ProviderAccount::find($id);
        if ($serverProviderObj && $serverProviderObj->delete()) {
            return redirect('/admin/server-management/server-provider/' . $pid)->with('message', 'Successfully Deleted');
        }
        return redirect()->back()->withErrors('Something went wrong!')->withInput();
    }
}
