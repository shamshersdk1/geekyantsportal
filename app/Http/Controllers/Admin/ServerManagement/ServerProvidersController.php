<?php

namespace App\Http\Controllers\Admin\ServerManagement;

use App\Http\Controllers\Controller;
use App\Models\Audited\Server\Provider;
use Exception;
use Illuminate\Http\Request;
use Redirect;

class ServerProvidersController extends Controller
{
    public function index()
    {
        $serverProviders = Provider::paginate(50);
        if (!$serverProviders) {
            return view('pages.server-management.server-provider.index')->with('message', 'No record found.');
        }

        return view('pages.server-management.server-provider.index', compact('serverProviders'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $serverProviderObj = new Provider();
        // $proAccount = ProviderAccount::where('provider_id', $data['provider_id'])
        try {
            if ($serverProviderObj->validate($data)) {
                $serverProviderObj->name = !empty($data['name']) ? $data['name'] : null;
                if (!$serverProviderObj->save()) {
                    return redirect()->back()->withErrors('Unable To Save !')->withInput();
                }
                return redirect()->back()->with('message', 'Saved Successfully');
            }
            $errors = $serverProviderObj->getError();
            return redirect()->back()->withErrors($errors)->withInput();
        } catch (Exception $e) {
            return redirect()->back()->withErrors('Unable To Save !')->withInput();
        }
    }

    public function show($id)
    {
        $serverProvider = Provider::find($id);
        if (!$serverProvider) {
            return redirect()->back()->withErrors('Record not found!')->withInput();
        }
        return view('pages.server-management.server-provider.manage', compact('serverProvider'));
    }

    public function edit($id)
    {
        $serverProvider = Provider::find($id);
        if (!$serverProvider) {
            return redirect()->back()->withErrors('Record not found!')->withInput();
        }
        return view('pages.server-management.server-provider.edit', compact('serverProvider'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $serverProviderObj = Provider::find($id);
        if (!$serverProviderObj) {
            return redirect()->back()->withErrors('Entry not found!')->withInput();
        }

        $serverProviderObj->rulesFunc($id);

        if ($serverProviderObj->validate($data)) {

            $serverProviderObj->name = !empty($data['name']) ? $data['name'] : null;
            if (!$serverProviderObj->save()) {
                return redirect()->back()->withErrors('Something went wrong!')->withInput();
            }
            return redirect('/admin/server-management/server-provider')->with('message', 'Updated!');
        } else {
            $errors = $serverProviderObj->getError();
            return redirect()->back()->withErrors($errors)->withInput();
        }
    }

    public function destroy($id)
    {
        $serverProviderObj = Provider::find($id);
        if (!$serverProviderObj) {
            return redirect()->back()->withErrors('Record not found!')->withInput();
        }
        if ($serverProviderObj->delete()) {
            return redirect()->back()->with('message', 'Provider deleted');
        }
        return redirect()->back()->withErrors('Something went wrong!')->withInput();
    }
}
