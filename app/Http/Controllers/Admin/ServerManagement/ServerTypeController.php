<?php

namespace App\Http\Controllers\Admin\ServerManagement;

use App\Http\Controllers\Controller;
use App\Models\Audited\Server\ServerType;
use Illuminate\Http\Request;
use Redirect;

class ServerTypeController extends Controller
{
    public function index()
    {
        $serverTypes = ServerType::paginate(50);
        if (!$serverTypes) {
            return view('pages.server-management.server-type.index')->with('message', 'no records found.');
        }

        return view('pages.server-management.server-type.index', compact('serverTypes'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $serverTypeObj = new ServerType();
        if ($serverTypeObj->validate($data)) {
            $serverTypeObj->name = !empty($data['name']) ? $data['name'] : null;
            $serverTypeObj->code = !empty($data['code']) ? $data['code'] : null;
            if (!$serverTypeObj->save()) {
                return redirect()->back()->withErrors('Something went wrong!')->withInput();
            }
            return redirect()->back()->with('message', 'Saved Successfully');
        }
        $errors = $serverTypeObj->getError();
        return redirect()->back()->withErrors($errors)->withInput();
    }

    public function show($id)
    {
        $serverType = ServerType::find($id);
        if (!$serverType) {
            return redirect()->back()->withErrors('Something went wrong!')->withInput();
        }
        return view('pages.server-management.server-type.edit', compact('serverType'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $serverTypeObj = ServerType::find($id);
        if (!$serverTypeObj) {
            return redirect()->back()->withErrors('Entry not found!')->withInput();
        }
        $serverTypeObj->rulesFunc($id);

        if ($serverTypeObj->validate($data)) {

            $serverTypeObj->name = !empty($data['name']) ? $data['name'] : null;
            $serverTypeObj->code = !empty($data['code']) ? $data['code'] : null;
            if (!$serverTypeObj->save()) {
                return redirect()->back()->withErrors('Something went wrong!')->withInput();
            }
            return redirect('/admin/server-management/server-type')->with('message', 'Updated!');
        } else {
            $errors = $serverTypeObj->getError();
            return redirect()->back()->withErrors($errors)->withInput();
        }
    }

    public function destroy($id)
    {
        $serverTypeObj = ServerType::find($id);
        if ($serverTypeObj && $serverTypeObj->delete()) {
            return redirect('/admin/server-management/server-type')->with('message', 'Successfully Deleted');
        }
        return redirect()->back()->withErrors('Something went wrong!')->withInput();
    }
}
