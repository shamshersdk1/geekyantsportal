<?php

namespace App\Http\Controllers\Admin\ServerManagement;

use App\Http\Controllers\Controller;
use App\Models\Audited\Server\Server;
use App\Models\Audited\Server\ServerNote;
use Illuminate\Http\Request;
use Redirect;

class ServerNoteController extends Controller
{
    public function add($serverId, Request $request)
    {
        $data = $request->all();
       
        if ($data && $data['notes'] && !ctype_space($data['notes'])) {
            $data['server_id']=$serverId;
            $response = ServerNote::saveData( $data);
            if ($response['status']) {
                return redirect('/admin/server-management/server/' . $serverId . '/manage')->with('message', 'Note Added Successfully!');
            } else {
                return redirect('/admin/server-management/server')->withErrors($response['message']);
            }
        } else {
            return redirect('/admin/server-management/server')->withErrors('Note cannot be empty or only Blankspaces');
        }
    }
}
