<?php

namespace App\Http\Controllers\Admin\ServerManagement;

use App\Http\Controllers\Controller;
use App\Models\Audited\Server\Server;
use App\Models\Audited\Server\ServerMeta;
use Illuminate\Http\Request;
use Redirect;

class ServerMetaController extends Controller
{
    public function add($serverId, Request $request)
    {
        $data = $request->all();
        if ($data && $data['key'] && $data['value'] && !ctype_space($data['key']) && !ctype_space($data['value'])) {
            $data['server_id'] = $serverId;

            $serverMeta = ServerMeta::where('server_id', $serverId)->where('key', $data['key'])->first();

            if ($serverMeta) {
                return redirect()->back()->withErrors('Key is already present for this server.')->withInput();
            }

            $response = ServerMeta::saveData($data);
            if ($response['status']) {
                return redirect('/admin/server-management/server/' . $serverId . '/manage')->with('message', 'Meta Added Successfully!');
            } else {
                return Redirect::back()->withErrors($response['message']);
            }
        } else {
            return Redirect::back()->withErrors('Meta Key and Value cannot be empty or only Blankspaces');
        }
    }

    public function delete($serverId, $metaId)
    {
        $serverObj = Server::find($serverId);
        if (!$serverObj) {
            return Redirect::back()->withErrors('Invalid Server ID');
        }
        $serverMetaObj = ServerMeta::find($metaId);
        if (!$serverMetaObj) {
            return Redirect::back()->withErrors('Invalid Meta ID');
        }
        $response = ServerMeta::deleteData($metaId);
        if ($response['status']) {
            return redirect('/admin/server-management/server/' . $serverId . '/manage')->with('message', 'Meta Deleted Successfully!');
        } else {
            return Redirect::back()->withErrors($response['message']);
        }
    }
}
