<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Input;
use Validator;
use Redirect;
use App\Models\User;
use App\Models\Admin\News;
use App\Models\Admin\Leave;
use App\Models\Admin\Appraisal;
use App\Models\Admin\Dashboard;
use App\Models\Admin\Company;
use App\Models\Admin\LateComer;
use Auth;
use View;
use DB;
use Carbon\Carbon;

class RmController extends Controller
{

    
    public function dologout()
    {
        Auth::logout(); // log the user out of our application
        return Redirect::to('admin'); // redirect the user to the login screen
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
    public function dashboard()
    {
        $user = Auth::user();
        if($user->hasRole('admin') || $user->hasRole('reporting-manager') ) 
        {
            $date=date('Y-m-d');
            
            $upcomingBirthdays = User::getUpcomingBirthdays();
            $holidays=DB::table('non_working_calendar')->whereDate('date', '>=', (date('Y-01-01')))->whereDate('date', '<=', (date('Y-12-31')))->orderBy('date')->get();
            $appraisals=Dashboard::appraisals(5);
            $confirmations=Dashboard::confirmations(3);
            $active=DB::table('non_working_calendar')->whereDate('date', '>=', $date)->orderBy('date')->first();
            $news=News::orderBy('date', 'Desc')->take(5)->get();
            $pendingLeaves=Leave::where('status', 'pending')->orderBy('start_date')->paginate(5);
            $todayAllLeaves=Leave::where('status', 'approved')->where('start_date', '<=', $date)->where('end_date', '>=', $date)->orderBy('start_date')->get();
            $upcomingAllLeaves=Leave::where('status', 'approved')->where('start_date', '>', $date)->where('days', '>', 0)->orderBy('start_date')->get();
            $jsonclients=Company::getClients();
            $lateComersList = LateComer::todayLateComers();
            
            return view('pages.admin.dashboard.rm-dashboard', compact('holidays', 'active', 'news', 'appraisals', 'confirmations', 'pendingLeaves', 'jsonclients','upcomingBirthdays','todayAllLeaves','upcomingAllLeaves','lateComersList'));    
        }
        else 
        {
            return view('pages.admin.dashboard.rm-dashboard');
        }

        
    }
}
