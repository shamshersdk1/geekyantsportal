<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Admin\Credential;
use App\Models\Admin\CredentialCategory;
use App\Models\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Redirect;
use Config;
use Validator;
use Exception;
use DB;
use Auth;

class CredentialCategoryController extends Controller
{
    
    public function index()
    {
        $credentialCategories = CredentialCategory::all();
        return View('pages/admin/credential/index',['credentialCategories' => $credentialCategories]);
    }

    public function create()
    {
        return View('pages/admin/credential/add');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);
        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors(['Name is mandatory']);
        }
        $credential_category = new CredentialCategory();
        $credential_category->name = $request->name;
        if(!$credential_category->save())
            return Redirect::back()->withInput()->withErrors(['Error while saving the record']);
        return redirect('/admin/credential')->with('message', 'Successfully Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $name = 'Test';
        $var = Credential::getAppTemplate();
        $fields = json_decode($var, true);
        
        return View('pages.admin.credential.show', compact('name', 'fields') );      
        //return Redirect::back()->withInput()->withErrors(['Something went wrong']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $credential_category = CredentialCategory::find($id);
        if ( isset($credential_category) ) 
        {
            return View('pages/admin/credential/edit', ['credential_category' => $credential_category] );    
        }  
        return Redirect::back()->withErrors($asset_category->getErrors());
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);
        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors(['Name is mandatory']);
        }
        $credential_category = CredentialCategory::find($id);
        if ( empty($credential_category) )
        {
            return redirect::back()->withInput()->withErrors(['Error while finding the credential category']);
        }
        $credential_category->name = $request->name;
        if(!$credential_category->save())
            return Redirect::back()->withInput()->withErrors(['Error while saving the record']);
        return redirect('/admin/credential')->with('message', 'Successfully Updated');
    }

    
    public function destroy($id)
    {
        $credential_category = CredentialCategory::find($id);
		if($credential_category) {
			$credential_category->delete();
            return redirect('/admin/credential')->with('message', 'Deleted successfully');
		}
		else
			abort(404);
    }
}
