<?php namespace App\Http\Controllers\Admin;
use App\Models\User;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Admin\CmsProject;
use App\Models\Admin\CmsTechnology;
use DB;
use Validator;
use Auth;
use App\Models\Admin\CmsProjectUser;


use Redirect;

use Illuminate\Http\Request;

class CmsProjectController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{	
		
		
		$result=CmsProject::filter();
		return View('pages/admin/cms-project/index', ['projects' => $result['projects'],'search' =>$result['search'] ]);
        
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		// echo "here";
		// die;
		$technologies = CmsTechnology::all();
		isset($technologies)?$technologies:"";
		$users = User::all();
		$user = Auth::user();
		return View('pages/admin/cms-project/add', ['technologies' => $technologies, 'users' => $users]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */

	// public function add(){
	// 	// echo "hello";
	// 	// die;
	// 	$technologies = Technology::all();
	// 	return View('pages/admin/project/add', ['technologies' => $technologies]);
	// 	// return View('pages/admin/project/add');
	// }

	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
            'project_title' => 'required',
            'project_type' => 'required|in:"products","services"',
		])->validate();
			if($request->is_featured){
				$is_featured = "1";

			}
			else{
				$is_featured = "0";
			}
			$targetFile = public_path('uploads/images/projects');
			if(!$request->hasFile('project_image') || !$request->project_type){
				return Redirect::back()->withInput()->withErrors(['Image and Project Type Required']);
			}
			
			$image_name = $request->file('project_image')->getClientOriginalName();
			$technologies = $request->saveTech;
			$project = new CmsProject();
			$project->project_title = $request->project_title;
			$project->project_image = "/uploads/images/projects/".$image_name;
			$project->project_url = $request->project_url;
			$project->project_desc = $request->project_desc;
			$project->project_short_desc = $request->project_short_desc;
			$project->project_type = $request->project_type;
			$project->is_featured = $is_featured;
			
			if($project->save())
			{
				$request->file('project_image')->move($targetFile, $image_name);
				if($technologies){
					foreach($technologies as $technology){
						print_r($technology);
						$project->cmsTechnologies()->attach($technology);
					}
				}
				return redirect('/admin/cms-project')->with('message', 'Successfully Added');
			}
			else{
				return redirect()->back()->withErrors($project->getErrors())->withInput();
				
			}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$technologies = CmsProject::find($id)->cmsTechnologies;
		$project = CmsProject::find($id);
		if($project){
			return View('pages/admin/cms-project/show', ['project' => $project, 'technologies' => $technologies]);
		}
		else
			abort(404);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$project = CmsProject::find($id);
		$technologies = CmsTechnology::all();
		$cms_user_id=CmsProjectUser::where('cms_project_id',$id)->pluck('user_id');
	
		$user=[];
		foreach($cms_user_id as $id ){
			$cms_user=User::where('id',$id)->get();
			$cmsusers[]=$cms_user;
		}

		$users = User::where('is_active',1)->get();

		$savedTechnologies = $project->cmsTechnologies;
		if($project){
			return View('pages/admin/cms-project/edit', ['project' => $project, 'savedTechnologies' => $savedTechnologies, 'technologies' => $technologies ,'users' =>$users ,'cmsusers'=>$cmsusers]);
		}
		else
			abort(404);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id)

	{
	
		$cms_user=CmsProjectUser::where('cms_project_id',$id)->delete();

		$user_id=$request->users;
		
		$validator = Validator::make($request->all(), [
            'project_title' => 'required',
            'project_type' => 'required|in:"products","services"',
		])->validate();
		// if($request->project_title){
			$project = CmsProject::find($id);
			// echo "<pre>";
			// print_r($project->toArray());
			// die;
			if($project){
				if($request->is_featured){
					$is_featured = "1";

				}
				else{
					$is_featured = "0";
				}
				$technologies = $request->saveTech;

			

				if($request->file('project_image')){
					echo "inchange image";
					$targetFile = public_path('uploads/images/projects');
					$inputImage = $request->file('project_image');
					$image_name = $request->file('project_image')->getClientOriginalName();
					$image_to_save = "/uploads/images/projects/".$image_name;
					$editImage = true;
					if($inputImage){
					    $inputImage->move($targetFile, $image_name);
					}
				}
				else{
					$image_to_save = $project->project_image;
				}
				
				foreach($user_id as $userId){
				$cmsProjectUser=new CmsProjectUser();

				$cmsProjectUser->cms_project_id=$id;
				$cmsProjectUser->user_id=$userId;
				

				$cmsProjectUser->save();

				}
				$project->project_title = $request->project_title;
				$project->project_url = $request->project_url;
				$project->project_image = $image_to_save;
				$project->project_desc = $request->project_desc;
				// $project->project_short_desc = $request->project_short_desc;
				$project->is_featured = $is_featured;
				$project->project_type = $request->project_type;
				if($project->save()){
					if($technologies){
						$project->cmsTechnologies()->detach();
						foreach ($technologies as $technology) {
							$project->cmsTechnologies()->attach($technology);
						}
					}
					return redirect('/admin/cms-project')->with('message','Successfully Updated');
				}
				else{
					return Redirect::back()->withErrors($project->getErrors());
				}
			}
			else
				abort(404);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{	

		$project = CmsProject::find($id);
		if($project) {
			if(count($project->technologies) > 0){	
				if($project->delete() && $project->technologies()->detach()){
					return redirect('/admin/cms-project')->with('message','Successfully Deleted');
				}
			}
			if($project->delete()){
				return redirect('/admin/cms-project')->with('message','Successfully Deleted');
			}
			else{
				return Redirect::back()->withErrors($project->getErrors());
			}
		}
		else
			abort(404);
		
		
	}

}
