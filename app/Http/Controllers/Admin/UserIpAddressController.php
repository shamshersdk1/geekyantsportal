<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;

class UserIpAddressController extends Controller
{
    public function index()
    {
        $users = User::where('is_active', 1)->with('ipAddress')->get();
        return View('pages/admin/assign-ip/index', ['users' => $users]);
    }
}
