<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Admin\Company;
use App\Models\Timesheet;
use App\Models\TimesheetDetail;
use App\Models\Admin\ProjectNotes;
use App\Models\Admin\ProjectResource;
use App\Models\User;
use App\Models\Admin\Project;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\ProjectService;
use App\Services\ProjectResourceService;
use Redirect;
use Config;
use Exception;
use DB;
use Auth;


class ProjectResourceReportController extends Controller
{
    public function index(Request $request)
    {
        $start_date = date("Y-m-d", strtotime("previous monday"));
        $end_date = date('Y-m-d', strtotime($start_date . ' +13 day'));
        $display_start_date = '';
        $display_end_date = '';

        $projects = [];
        $selected_projects = [];
        $selected_projects_ids = [];

        $eligible_users =[];
        $selected_users = [];
        
        $projectList = Project::where('status','in_progress')->get();
        $resourceList = User::where('is_active',1)->where('is_billable',1)->get();
        
        
        
        if(empty($request->start_date)) {
            $start_date = date("Y-m-d", strtotime("previous monday"));
            $end_date = date('Y-m-d', strtotime($start_date . ' +13 day')); 
        } else {
            $start_date = date('Y-m-d', strtotime($request->start_date));
            $end_date = date('Y-m-d', strtotime($request->end_date));
            $display_start_date =  $request->start_date;
            $display_end_date =  $request->end_date;
        }

        if ( !empty($request->users) ){
            $eligible_users = $request->users;
            $selected_users = $request->users;
        } else {
            foreach ( $resourceList as $resource )
            {
                $eligible_users[] = $resource->id;
            }
        }

        if ( !empty($request->input_projects) ){
            $projects = $request->input_projects;
            $selected_projects = $request->input_projects;
            $selected_projects_ids = $request->input_projects;
        } else {
            foreach ( $projectList as $project )
            {
                $selected_projects[] = $project->toArray();
                $selected_projects_ids[] = $project['id'];
            }
        }

        $data = ProjectResourceService::projectResourceReport($start_date, $end_date, $eligible_users, $selected_projects_ids);

        $counts = [];
        for ($i=0;$i<count($data['project_name']);$i++ )
        {
            $counts[$i] = 0;
        }
        if ( !empty($request->users) )
        {   
            foreach( $data['projectResourceDataList'] as $index => $project_resource_data )
            {
                for ($i=0;$i<count($project_resource_data);$i++ )
                {
                    if ($project_resource_data[$i]['users'] != '' )
                    {
                        $counts[$i] = $counts[$i] + 1;
                    } else {
                        $counts[$i] = $counts[$i] + 0;
                    }
                }
            }
            for ( $i=0; $i < count($counts); $i++ )
            {
                if ($counts[$i] == 0)
                {
                    unset($data['project_name'][$i]);
                    foreach ( $data['projectResourceDataList'] as $project_resource_data )
                    {
                        unset($project_resource_data[$i]);
                    }
                }
            }

        }

        return View('pages/admin/project-resource-report/index', ['data' => $data, 'resourceList' => $resourceList, 'selected_users' => $selected_users, 'start_date' => $display_start_date, 'end_date' => $display_end_date, 'projectList' => $projectList, 'selected_projects' => $selected_projects ] );
    }    
}