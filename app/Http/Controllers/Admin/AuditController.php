<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Payroll\AppraisalService;
use App\Services\Payroll\ITSavingService;
use OwenIt\Auditing\Models\Audit;

class AuditController extends Controller
{
    public function index()
    {
        $audits = Audit::take(500)->orderBy('id','DESC')->get();
        return view('pages.admin.audit.index', compact('audits'));
    }
    public function report()
    {
        $fromDate = date('Y-m-d', strtotime("-1 days"));
        $toDate = date('Y-m-d');
        $data = [];
        $data['audits'] = Audit::orderBy('created_at')
                            ->whereBetween('created_at', [$fromDate.' 00:00:00', $toDate.' 06:00:00'])
                            ->get();

        $data['appraisalAudits'] = AppraisalService::getAudits();
        $data['itSavingAudits'] = ITSavingService::getAudits();
        return view('pages.admin.audit.pdf', compact('data','date'));
    }
}
