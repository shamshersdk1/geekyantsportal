<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\OnSiteBonus;
use App\Models\Admin\Project;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;

class OnSiteBonusController extends Controller
{
    public function index()
    {
        $OnSiteBonus = OnSiteBonus::paginate(500);
        return view('pages.admin.on-site-bonus.index', compact('OnSiteBonus'));
    }
    public function create()
    {
        $user = Auth::user();
        if ($user->hasRole('admin')) {
            $userList = User::Where('is_active', 1)->orderBy('name', 'asc')->get();
        } else {
            $userList = User::where('parent_id', $user->id)->where('is_active', 1)->orderBy('name', 'asc')->get();
        }
        $projectList = Project::All();
        return view('pages.admin.on-site-bonus.add', compact('userList', 'projectList'));

    }
    public function store(Request $request)
    {
        $user = Auth::user();
        $data = $request->all();
        if (!empty($data)) {
            $OnSiteBonus = new OnSiteBonus();
            $OnSiteBonus->employee_id = $data['user_id'];
            $OnSiteBonus->type = $data['type'];
            $OnSiteBonus->amount = $data['amount'];
            $OnSiteBonus->status = 'approved';
            $OnSiteBonus->to_date = !empty(date_to_yyyymmdd($data['to_date'])) ? date_to_yyyymmdd($data['to_date']) : null;
            $OnSiteBonus->from_date = !empty(date_to_yyyymmdd($data['from_date'])) ? date_to_yyyymmdd($data['from_date']) : null;
            $OnSiteBonus->notes = !empty($data['notes']) ? $data['notes'] : null;
            $OnSiteBonus->created_by = $user->id;
            if (!empty($data['project_id'])) {
                $OnSiteBonus->reference_type = 'App\Models\Admin\Project';
                $OnSiteBonus->reference_id = $data['project_id'];
            }
            if ($OnSiteBonus->save()) {
                return redirect('/admin/bonus/on-site')->with('message', 'Saved!');
            } else {
                return redirect()->back()->withErrors('Something went wrong!')->withInput();
            }
        } else {
            return redirect()->back()->withErrors('Empty data sent!')->withInput();
        }

    }
    public function update(Request $request, $id)
    {
        $user = Auth::user();
        $data = $request->all();
        if (!empty($data)) {

            $OnSiteBonus = OnSiteBonus::find($id);
            if ($OnSiteBonus) {
                $OnSiteBonus->employee_id = $data['user_id'];
                $OnSiteBonus->type = $data['type'];
                $OnSiteBonus->amount = $data['amount'];
                $OnSiteBonus->to_date = !empty(date_to_yyyymmdd($data['to_date'])) ? date_to_yyyymmdd($data['to_date']) : null;
                $OnSiteBonus->from_date = !empty(date_to_yyyymmdd($data['from_date'])) ? date_to_yyyymmdd($data['from_date']) : null;
                $OnSiteBonus->notes = !empty($data['notes']) ? $data['notes'] : null;
                $OnSiteBonus->created_by = $user->id;
                if (!empty($data['project_id'])) {
                    $OnSiteBonus->reference_type = 'App\Models\Admin\Project';
                    $OnSiteBonus->reference_id = $data['project_id'];
                }
                if ($OnSiteBonus->save()) {
                    return redirect('/admin/bonus/on-site')->with('message', 'Updated!');
                } else {
                    return redirect()->back()->withErrors('Something went wrong!')->withInput();
                }
            } else {
                return redirect()->back()->withErrors('Not found!')->withInput();
            }
        }

    }
    public function destroy($id)
    {
        $OnSiteBonus = OnSiteBonus::where('id', $id)->delete();
        if ($OnSiteBonus) {
            return redirect('/admin/bonus/on-site')->with('message', 'Successfully deleted!');

        }
        return redirect()->back()->withErrors('Uable to delete record');

    }
    public function edit($id)
    {
        $user = Auth::user();
        if ($user->hasRole('admin')) {
            $userList = User::Where('is_active', 1)->orderBy('name', 'asc')->get();
        } else {
            $userList = User::where('parent_id', $user->id)->where('is_active', 1)->orderBy('name', 'asc')->get();
        }
        $projectList = Project::All();
        $OnSiteBonus = OnSiteBonus::find($id);
        if ($OnSiteBonus) {
            $userId = $OnSiteBonus->employee_id;
            $projectId = !empty($OnSiteBonus->reference->id) ? $OnSiteBonus->reference->id : null;
            $type = $OnSiteBonus->type;
        }

        return view('pages.admin.on-site-bonus.edit', compact('OnSiteBonus', 'userList', 'userId', 'projectList', 'projectId', 'type'));
    }
    public function show($id)
    {
        //get
    }
}
