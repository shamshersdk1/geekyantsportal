<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\CalendarYear;
use App\Models\Admin\LeaveDeductionMonth;
use App\Models\Admin\LeaveType;
use App\Models\Leave\UserLeaveTransaction;
use App\Models\Month;
use App\Models\MonthSetting;
use App\Models\User;
use App\Services\NewLeaveService;
use DB;
use Redirect;

class LeaveDeductionMonthController extends Controller
{
    public function index()
    {
        $months = Month::getMonthList();
        return View('pages/admin/leave-deduction-month/index', compact('months'));
    }

    public function show($monthId)
    {
        $leaveDeductions = LeaveDeductionMonth::where('month_id', $monthId)->get();
        $month = Month::find($monthId);
        return View('pages/admin/leave-deduction-month/show', compact('month', 'leaveDeductions'));
    }

    public function regenerate($month_id)
    {
        $response = NewLeaveService::leaveDeduction($month_id);
        if (!empty($response['status']) && $response['status'] == false) {
            return Redirect::back()->withErrors('Something went wrong!');
        }
        return redirect('/admin/leave-deduction-month/' . $month_id);
    }

    public function monthStatus($monthId)
    {
        $response = MonthSetting::monthStatusToggle($monthId, 'leave-deduction');
        LeaveDeductionMonth::where('month_id', $monthId)->where('status', 'pending')->update(['status' => 'approved']);
        return Redirect::back()->with('message', $response);
    }

    public function ignore($leaveDeductionId)
    {
        $obj = LeaveDeductionMonth::find($leaveDeductionId);
        $obj->status = "ignored";
        if ($obj->save()) {
            return Redirect::back()->with('message', "Ignored Successfully");
        }
        return Redirect::back()->withErrors('Something went wrong!');
    }

    public function forward($leaveDeductionId)
    {
        $obj = LeaveDeductionMonth::find($leaveDeductionId);
        $obj->status = "forwarded";
        if ($obj->save()) {
            return Redirect::back()->with('message', "Forwarded Successfully");
        }
        return Redirect::back()->withErrors('Something went wrong!');
    }

    public function delete($leaveDeductionId)
    {
        $obj = LeaveDeductionMonth::find($leaveDeductionId);
        if ($obj && $obj->delete()) {
            return Redirect::back()->with('message', "Forwarded Successfully");

        }
        return Redirect::back()->withErrors(["Something went wrong!"]);

    }

    public function monthDeductions($monthId)
    {
        $month = Month::find($monthId);
        if (!$month) {
            return redirect::to('/admin/leave-deduction')->withErrors('Invalid month #' . $monthId);
        }
        $users = User::where('is_active', 1)->get();
        $leaveTypes = LeaveType::get();
        $userLeaves = NewLeaveService::getMonthlyLeaves($monthId);
        return view('pages.admin.leave-deduction-month.monthly-deductions', compact('month', 'leaveTypes', 'userLeaves', 'users'));
    }

    public function approveMonthDeductions($monthId)
    {
        $allLeaveDeductions = $_POST;
        $monthObj = Month::find($monthId);
        $calendarYearObj = CalendarYear::where('year', $monthObj->year)->first();
        $data['calendar_year_id'] = $calendarYearObj->id;
        $data['date'] = date('Y-m-d');
        $data['reference_type'] = "App\Models\Admin\LeaveDeductionMonth";

        foreach ($allLeaveDeductions as $userKey => $userLeaveDeductions) {
            foreach ($userLeaveDeductions as $leaveTypeKey => $leaveDays) {
                if (!empty($leaveDays["is_checked"]) && $leaveDays["is_checked"] && $leaveDays["value"] != 0) {
                    DB::beginTransaction();
                    try {
                        $deductionResponse = LeaveDeductionMonth::saveDeduction($userKey, $leaveTypeKey, $monthId, $leaveDays["value"], "deduction monthly basis");
                        if (!$deductionResponse) {
                            throw new Exception("LeaveDeductionMonth not updated");
                        }
                        $data['user_id'] = $userKey;
                        $data['leave_type_id'] = $leaveTypeKey;
                        $data['days'] = (-1) * $leaveDays["value"];
                        $data['reference_id'] = $deductionResponse;
                        $leaveTransactionResponse = UserLeaveTransaction::saveData($data);
                        if (!$leaveTransactionResponse) {
                            throw new Exception("UserLeaveTransaction not updated");
                        }
                        DB::commit();
                        $response = true;
                    } catch (Exception $e) {
                        DB::rollback();
                        $response = false;
                        $errors = $e;
                    }
                    if (!$response) {
                        return redirect::to('/admin/leave-deduction')->withErrors('Error Saving Data' . $e . $userKey . $leaveTypeKey . $leaveDays);
                    }
                }
            }
        }
        return Redirect::back();
    }
}
