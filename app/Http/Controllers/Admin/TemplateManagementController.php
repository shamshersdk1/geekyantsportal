<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Admin\Template;
use Redirect;
use App;

class TemplateManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$templates = Template::all();
        return View('pages/admin/template-management/index', ['templates' => $templates]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('pages/admin/template-management/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $result = Template::saveData($data);
        if($result['status'])
        {
        	return redirect('/admin/template-management')->with('message', 'Successfully Added');
        }
        else{
        	return redirect::back()->withErrors($result['errors'])->withInput();
        }

  //       $content = "<p>%%NAME%% &nbsp;IS THIS COOL %%AGE%%<\/p>";

  //       $matches = array();
		// preg_match('/%%/%%', $url, $matches);

		// print_r($matches);
		// die;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $template = Template::find($id);
        if($template) {
        	return View('pages/admin/template-management/show', ['template' => $template]);
        }
        else {
        	abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $template = Template::find($id);
        if($template) {
        	return View('pages/admin/template-management/edit', ['template' => $template]);
        }
        else {
        	abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $template = Template::find($id);
        if($template) {
        	$template->name = $request->name;
        	$template->content = $request->content;
        	if($template->save()) {
        		return redirect('/admin/template-management')->with('message', 'Successfully Updated');
        	}
        	else{
        		return redirect::back()->withErrors($template->errors())->withInput();
        	}
        }
        else {
        	abort(404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $template = Template::find($id);
        if($template) {
        	if($template->delete()) {
        		return redirect('/admin/template-management')->with('message', 'Successfully Deleted');
        	}
        	else {
        		return redirect::back()->withErrors($template->errors())->withInput();
        	}
        }
        else {
        	abort(404);
        }
    }

    public function document($id) {
    	$template = Template::find($id);
    	if($template) {
			return View('pages/admin/template-management/document', ['template' => $template]);
    	}
    	else {
    		abort(404);
    	}
    }

    public function contentApi($id) {
    	$template = Template::find($id);    	
    	if($template){
	    	$length = 0;
	    	$result = [];
	        $fullstring = $template->content;
	        if($fullstring) {
		        $len = strlen($fullstring);
		        $originalString = $fullstring;
		        while ($length <= $len) {
		            
		            $arr = $this->get_string_between($fullstring, '%%', '%%');    
		            if(is_array($arr)) {
		                $length += $arr['length'];
		                $length  += 1; 
		                array_push($result, $arr['variable']);
		                $fullstring = substr($originalString,$length);
		            } else {
		                break;
		            }
		    	}
		    	$result = array_unique($result);
               
                $result = array_combine($result, $result);
		    	return $result;
		    }
		    else {
		    	return "false";
		    }
	    }
	}

    public function get_string_between($string, $start, $end){
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        $total = strpos($string, $end, $ini);
        if(!$len)
            return 'false';
        return ['variable'=>substr($string, $ini, $len) , 'length' => $total];
    }


    public function download(Request $request) {

    	$pdf = App::make('dompdf.wrapper');
    	if($request->content)
    		$pdf->loadHTML($request->content);
    	return $pdf->stream();

    	// echo "something";
    	// die;
    }
}
