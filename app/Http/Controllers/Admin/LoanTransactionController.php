<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\LoanController;
use App\Models\Loan;
use App\Models\LoanTransaction;
use App\Models\User;
use App\Services\LoanService;
use Validator;
use Redirect;

class LoanTransactionController extends Controller
{
    public function editTransaction($loan_id, $loan_transaction_id)
    {
        $loan_transaction = LoanTransaction::find($loan_transaction_id);
        if ($loan_transaction) {
            return View('pages/admin/loan/emiedit', compact('loan_transaction'));
        } else {
            abort(404);
        }
    }
    public function updateTransaction(Request $request, $loan_id, $loan_transaction_id)
    {
        $validator = Validator::make($request->all(), [
        'paid_on' => 'required|date',
        'method' => 'required',
        'amount' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $loan_transaction=LoanTransaction::find($loan_transaction_id);
        $loan_transaction->status='paid';
        $loan_transaction->paid_on=$request->paid_on;
        $loan_transaction->method=$request->method;
        $loan_transaction->amount=$request->amount;
        $loan_transaction->comment=$request->comment;
        $loan_transaction->save();
        
        return redirect('/admin/loans/'.$loan_transaction->loan->id)->with('message', 'Payment details saved Successfully')->with('loan_transaction', $loan_transaction);
    }
    
    public function addTransaction($load_id)
    {
        $loan = Loan::find($load_id);
        return view('pages.admin.loan.emi-add', compact('loan'));
    }
    public function saveTransaction($loan_id, Request $request)
    {
        $validator = Validator::make($request->all(), [
        'pay_date' => 'required|date',
        'method' => 'required',
        'amount' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $input = $request->all();

        $response = LoanService::createLoanTransaction($loan_id, $input);
        if ( $response['status'] )
            return redirect::back()->with('message', 'Successfully Added');
        else
            return Redirect::back()->withErrors($response['message'])->withInput();
    }
}
