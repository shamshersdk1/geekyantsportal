<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\UserGoal;
use App\Models\User;
use Redirect;
use View;

class UserGoalController extends Controller
{
    public function create($user_id)
    {
        $user = User::find($user_id);
        if($user) {
            if (substr($user->name, -1)=="s") {
                $name=$user->name."'";
            } else {
                $name=$user->name."'s";
            }
            $year = date('n') > 3 ? date('Y') : date('Y') - 1;
            $current = UserGoal::where('user_id', $user_id)->where('from_date', $year."-04-01")->exists();
            $next = UserGoal::where('user_id', $user_id)->where('from_date', ($year+1)."-04-01")->exists();
            return View::make('pages.admin.user-goals.create', compact('user_id', 'name', 'current', 'next'));
        } else {
            return Redirect::back()->withErrors(['User not found']);
        }
    }
    public function store(Request $request, $user_id)
    {
        if(empty($request->year)) {
            return Redirect::back()->withErrors(['Select a Duration'])->withInput();
        }
        $user_goal = new UserGoal();
        $user_goal->user_id = $user_id;
        $user_goal->name = empty($request->name) ? null : $request->name;
        $user_goal->from_date = $request->year."-04-01";
        $user_goal->to_date = ($request->year+1)."-03-31";
        $user_goal->status = "pending";
        $user_goal->save();
        return Redirect::to("/admin/goal-assignment/".$user_goal->id)->with('message', "User Goal added");
    }
    public function userGoals($user_id)
    {
        $user = User::find($user_id);
        if($user) {
            $user_goals = UserGoal::where('user_id', $user_id)->orderBy('from_date', 'desc')->paginate(10);
            if (substr($user->name, -1)=="s") {
                $name=$user->name."'";
            } else {
                $name=$user->name."'s";
            }
            return View::make('pages.admin.user-goals.index', compact('user_id', 'user_goals', "name"));
        } else {
            return Redirect::back()->withErrors(['User not found']);
        }
    }
    public function editUserGoal($user_id,$user_goal_id)
    {
        $user = User::find($user_id);
        if (substr($user->name, -1)=="s") {
            $name=$user->name."'";
        } else {
            $name=$user->name."'s";
        }
        $userGoal = UserGoal::find($user_goal_id);

        return View::make('pages/admin/goals/goal-assignment-edit',compact('user','name','userGoal'));

    }
    public function reviewUserGoal($user_id,$user_goal_id)
    {
        $user = User::find($user_id);
        if (substr($user->name, -1)=="s") {
            $name=$user->name."'";
        } else {
            $name=$user->name."'s";
        }
        $userGoal = UserGoal::find($user_goal_id);

        return View::make('pages/admin/goals/manager-final-view',compact('user','name','userGoal'));

    }
}