<?php

namespace App\Http\Controllers\Admin\Policy;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Policy\Policy;

class PolicyController extends Controller
{
    public function showActivePolices()
	{
		$policies = Policy::where('status','active')->where('type','policy')->orderBy('effective_date','DESC')->get();
		$notifications = Policy::where('status','active')->where('type','notification')->orderBy('effective_date','DESC')->get();
		return view('pages.admin.policy.user-policies',compact('policies','notifications'));
	}
    public function index()
    {
        $policies = Policy::orderBy('effective_date','DESC')->get();
        return view('pages.admin.policy.index',compact('policies'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $policy = Policy::create(['name' => $data['name'],'reference_number' => $data['reference_number'],'effective_date' => $data['effective_date'],'url' => $data['url'],'status' => $data['status']?'active':'deactive', 'type' => $data['type']?'policy':'notification']);
        if($policy->isInvalid())
        {
            return redirect()->back()->withErrors($policy->getErrors())->withInput();
        }
        return redirect('/admin/policy')->with('message', 'save successfully!');
    }

    public function updateStatus($id)
    {
        $policy = Policy::find($id);
        if($policy->status == 'active')
            $policy->status = 'deactive';
        else
            $policy->status = 'active';
        $policy->save();
        return redirect()->back();
    }

    public function destroy($id)
    {
        $policy = Policy::find($id);
        if($policy->delete())
            return redirect()->back()->withErrors("Deleted Sucessfully");
        return redirect()->back()->withErrors("Unable to delete.");
    }

    public function show($id)
    {
        $policy = Policy::find($id);
        return view('pages.admin.policy.edit',compact('policy'));
    }

    public function update($id, Request $request)
    {
        $data = $request->all();
        $policy = Policy::find($id);
        if(!$policy)
        {
            return redirect()->back()->withErrors("Policy not found")->withInput();
        }
        $policyObj = Policy::updateOrCreate(['id' => $id], ['name' => $data['name'], 'reference_number' => $data['reference_number'], 'url' => $data['url'] , 'status' => $data['status'] ? 'active' : 'deactive', 'type' => $data['type'] ? 'policy' : 'notification', 'effective_date' => $data['effective_date']]);
        if($policyObj->isInvalid())
        {
            return redirect()->back()->withErrors($policyObj->getErrors())->withInput();
        }
        return redirect('/admin/policy')->with('message', 'update successfully!');

    }
}
