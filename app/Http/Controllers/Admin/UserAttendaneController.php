<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Attendance\UserAttendanceRegister;
use App\Models\Month;
use App\Models\User;
use App\Models\UserAttendance;
use DB;
use Illuminate\Http\Request;

class UserAttendaneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $viewData = UserAttendance::getReport(date('Y-m-d'));

        return View('pages/admin/user-attendance/index', $viewData);
    }

    public function view()
    {
        return View('pages/admin/user-attendance/month-report');
    }

    public function markAs(Request $request)
    {
        // Need to add allow only to admin members & hr members
        // Assuming that its handled by Roles and middleware.

        $user_id = $request->input('user_id');
        $date = $request->input('date');
        $markas = $request->input('markas');

        if (UserAttendance::createUserAttendanceMarkedAs($user_id, $date, $markas)) {
            return redirect()->back()->with('message', 'Status updated successfully');
        } else {
            return redirect()->back()->with('error', 'Unable to update - Please check log file!');
        }
    }

    public function monthView($monthId = null)
    {
        $monthList = Month::getMonthList();
        if (!$monthList || empty($monthList)) {
            return redirect()->back()->with('error', 'No months found!');
        }

        $monthArray = [];
        foreach ($monthList as $month) {
            if ($month->month <= date('m') || $month->year < date('Y')) {
                $monthArray[$month->id] = $month->formatMonth();
            }

        }

        if ($monthId == null) {
            $monthObj = Month::orderBy('year', 'DESC')->orderBy('month', 'DESC')->first();
        } else {
            $monthObj = Month::find($monthId);
            $month = $monthObj->month;
        }
        if (!$monthObj) {
            return redirect()->back()->with('error', 'Unable to find month!');
        }

        $startDate = $monthObj->getFirstDay();
        $lastDate = $monthObj->getLastDay();

        $attendanceObj = null;

        $noOfPresent = DB::table('user_attendance_registers')
            ->select('date', DB::raw('count(*) as count'))
            ->whereIn('status', ['present_in_office'])
            ->whereDate('date', '>=', $startDate)
            ->whereDate('date', '<=', $lastDate)
            ->groupBy('date')
            ->get();

        $noOfOnSite = DB::table('user_attendance_registers')
            ->select('date', DB::raw('count(*) as count'))
            ->whereIn('status', ['present_onsite'])
            ->whereDate('date', '>=', $startDate)
            ->whereDate('date', '<=', $lastDate)
            ->groupBy('date')
            ->get();

        $noOfLeave = DB::table('user_attendance_registers')
            ->select('date', DB::raw('count(*) as count'))
            ->whereIn('status', ['on_leave'])
            ->whereDate('date', '>=', $startDate)
            ->whereDate('date', '<=', $lastDate)
            ->groupBy('date')
            ->get();

        $noOfWFH = DB::table('user_attendance_registers')
            ->select('date', DB::raw('count(*) as count'))
            ->whereIn('status', ['present_wfh'])
            ->whereDate('date', '>=', $startDate)
            ->whereDate('date', '<=', $lastDate)
            ->groupBy('date')
            ->get();

        $attendanceCount = [];

        foreach ($noOfPresent as $present) {
            $attendanceCount[$present->date]['no_of_present'] = $present->count;
        }
        foreach ($noOfOnSite as $onsite) {
            $attendanceCount[$onsite->date]['no_of_onsite'] = $onsite->count;
        }
        foreach ($noOfLeave as $leave) {
            $attendanceCount[$leave->date]['no_of_leave'] = $leave->count;
        }
        foreach ($noOfWFH as $wfh) {
            $attendanceCount[$wfh->date]['no_of_wfh'] = $wfh->count;
        }

        for ($date = $startDate; $date <= $lastDate && $date < date('Y-m-d'); $date = date('Y-m-d', strtotime($date . "+1 days"))) {
            if ($attendanceCount[$date]) {
                $attendanceObj[$date]['no_of_present'] = $attendanceCount[$date]['no_of_present'] ? $attendanceCount[$date]['no_of_present'] : 0;
                $attendanceObj[$date]['no_of_onsite'] = $attendanceCount[$date]['no_of_onsite'] ? $attendanceCount[$date]['no_of_onsite'] : 0;
                $attendanceObj[$date]['no_of_leave'] = $attendanceCount[$date]['no_of_leave'] ? $attendanceCount[$date]['no_of_leave'] : 0;
                $attendanceObj[$date]['no_of_wfh'] = $attendanceCount[$date]['no_of_wfh'] ? $attendanceCount[$date]['no_of_wfh'] : 0;
                $attendanceObj[$date]['date'] = $date;
            }
        }

        return View('pages/admin/user-attendance/month-view', compact('monthObj', 'monthList', 'attendanceObj', 'monthArray'));
    }

    public function dateView(Request $request, $monthId)
    {
        $monthObj = Month::find($monthId);
        if (!$monthObj) {
            return redirect()->back()->with('error', 'Month not found!');
        }

        $date = $request->date;
        if (!$date) {
            return redirect()->back()->with('error', 'Date not found!');
        }

        $users = User::where('is_active', 1)->orderBy('employee_id', 'ASC')->get();
        if (!$users) {
            return redirect()->back()->with('error', 'User not found!');
        }

        $userRegisterObj = UserAttendanceRegister::where('date', $date)->with('user')->get()->sortBy('user.employee_id');
        if (!$userRegisterObj) {
            return redirect()->back()->with('error', 'User Register Data not found!');
        }

        return View('pages/admin/user-attendance/date-view', compact('userRegisterObj', 'date', 'users', 'monthObj'));
    }

    public function userView($userId, $monthId = null)
    {
        $monthList = Month::getMonthList();
        if (!$monthList || empty($monthList)) {
            return redirect()->back()->with('error', 'No months found!');
        }
        $monthArray = [];
        foreach ($monthList as $month) {
            if ($month->month <= date('m') || $month->year < date('Y')) {
                $monthArray[$month->id] = $month->formatMonth();
            }
        }
        if ($monthId == null) {
            $monthObj = Month::orderBy('year', 'DESC')->orderBy('month', 'DESC')->first();
        } else {
            $monthObj = Month::find($monthId);
            $month = $monthObj->month;
        }
        if (!$monthObj) {
            return redirect()->back()->with('error', 'Unable to find month!');
        }

        $startDate = $monthObj->getFirstDay();
        $lastDate = $monthObj->getLastDay();

        $userObj = User::find($userId);
        if (!$userObj) {
            return redirect()->back()->with('error', 'Unable to find User!');
        }

        $sum = 0;
        $inTimes = UserAttendanceRegister::where('user_id', $userId)->whereDate('date', '>=', $startDate)->whereDate('date', '<=', $lastDate)->pluck('in_time');

        foreach ($inTimes as $inTime) {
            $time = date('H:i:s', strtotime($inTime));
            $sum += strtotime($time);
        }
        $avg = $sum / count($inTimes);
        $avgInTime = date('H:i:s', $avg);

        $attendanceObj = null;

        for ($date = $startDate; $date <= $lastDate && $date < date('Y-m-d'); $date = date('Y-m-d', strtotime($date . "+1 days"))) {
            $registerObj = UserAttendanceRegister::where('date', $date)->where('user_id', $userId)->first();
            if ($registerObj) {
                $attendanceObj[$date]['date'] = $date;
                $attendanceObj[$date]['in_time'] = $registerObj->in_time;
                $attendanceObj[$date]['out_time'] = $registerObj->out_time;
                $attendanceObj[$date]['tick_count'] = $registerObj->tick_count;
                $attendanceObj[$date]['active_count'] = $registerObj->active_count;
                $attendanceObj[$date]['status'] = $registerObj->status;
            }
        }

        $totalTickSum = UserAttendanceRegister::where('user_id', $userId)->whereDate('date', '>=', $startDate)->whereDate('date', '<=', $lastDate)->sum('tick_count');
        $totalTickCount = UserAttendanceRegister::where('user_id', $userId)->whereDate('date', '>=', $startDate)->whereDate('date', '<=', $lastDate)->count();
        $avgHoursSpent = $totalTickSum / ($totalTickCount);

        return View('pages/admin/user-attendance/user-view', compact('attendanceObj', 'userObj', 'monthList', 'monthObj', 'monthArray', 'avgInTime', 'avgHoursSpent'));
    }
}
