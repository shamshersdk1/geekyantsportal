<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Appraisal;
use App\Models\Admin\AppraisalBonus;
use App\Models\Admin\PayroleGroup;
use App\Models\Admin\Role;
use App\Models\Month;
use App\Models\MonthSetting;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Input;
use Redirect;

class AppraisalController extends Controller
{
    public function index($id = null)
    {
        if ($id == null) {
            $user = \Auth::user();
            $id = $user->id;
        } else {
            $user = User::find($id);
        }
        if (empty($user)) {
            return redirect::back()->withErrors('Invalid Link Followed');
        }
        $authObj = \Auth::user();
        $hasAccess = false;
        $loggedInUser = User::with('roles')->where('id', $authObj->id)->first();
        if (!empty($loggedInUser->roles)) {
            foreach ($loggedInUser->roles as $role) {
                $roleObj = Role::find($role->id);
                if ($roleObj->code == 'users') {
                    $hasAccess = true;
                }
            }
        }

        $appraisals = Appraisal::where('user_id', $id)->orderBy('is_active', 'desc')->orderBy('effective_date', 'desc')->paginate(10)->appends(Input::except('page'));
        return view('pages.admin.appraisal.index', compact('user', 'appraisals', 'hasAccess'));
    }

    public function create($id)
    {
        $user = User::find($id);
        if (empty($user)) {
            return redirect::back()->withErrors('Invalid Link Followed');
        }
        if (count(Appraisal::where('user_id', $id)->where('name', 'Joining')->get()) > 0) {
            if (count(Appraisal::where('user_id', $id)->where('name', 'Confirmation')->get()) > 0) {
                $name = "";
                $date = "";
            } else {
                $name = "Confirmation";
                $date = $user->confirmation_date;
            }
        } else {
            $name = "Joining";
            $date = $user->joining_date;
        }
        $payrolegroups = PayroleGroup::get();
        return view('pages.admin.appraisal.add', compact('user', 'payrolegroups', 'id', 'name', 'date'));
    }

    public function store(Request $request, $id)
    {
        $user = User::find($id);
        if (empty($user)) {
            return redirect::back()->withErrors('Invalid Link Followed');
        }
        $response = Appraisal::saveAppraisal($request, $user);
        if (!$response['status']) {
            return redirect()->back()->withErrors($response['errors'])->withInput();
        }
        return redirect()->back()->with('message', 'Appraisal successfully added');
    }

    public function edit($uid, $id = null)
    {
        if ($id == null) {
            $id = $uid;
            $user = \Auth::user();
            $uid = $user->id;
        } else {
            $user = User::find($uid);
        }
        if (empty($user)) {
            return redirect::back()->withErrors('Invalid Link Followed');
        }
        $appraisal = Appraisal::find($id);
        if ($appraisal) {
            $array = Appraisal::getBonus($id, 4);
            $qtrArray = $array['Qtr'];
            $annual = $array['annual'];
            $payrolegroups = PayroleGroup::get();
            if (substr($user->name, -1) == "s") {
                $title = $user->name . '\' Appraisal';
            } else {
                $title = $user->name . '\'s Appraisal';
            }
            return view('pages.admin.appraisal.edit', compact('user', 'appraisal', 'uid', 'payrolegroups', 'title', 'qtrArray', 'annual'));
        } else {
            return redirect::back()->withErrors('Invalid Link Followed');
        }
    }

    public function update(Request $request, $uid, $id)
    {
        $user = User::find($uid);
        if (empty($user)) {
            return redirect::back()->withErrors('Invalid Link Followed');
        }
        if ($request->activate) {
            $appraisal = Appraisal::find($id);
            if ($appraisal->is_active) {
                return redirect()->back()->withErrors('Already active');
            }
            $arrayId = Appraisal::where('user_id', $uid)->where('is_active', 1)->pluck('id')->toArray();
            foreach ($arrayId as $active_id) {
                $active = Appraisal::find($active_id);
                $active->is_active = false;
                $active->save();
            }
            $appraisal->is_active = true;
            if (!$appraisal->save()) {
                return redirect()->back()->withErrors('Unable to save');
            }
            // SlackAppraisalReminder::remind($appraisal->id);
        } else {
            $response = Appraisal::updateAppraisal($request, $user, $id);
            if (!$response['status']) {
                return redirect()->back()->withErrors($response['errors'])->withInput();
            }
        }
        return redirect()->back()->with('message', 'Appraisal successfully updated');
    }

    public function destroy($uid, $id)
    {
        $appraisal = Appraisal::find($id);
        $appraisal->delete();
        return redirect::back()->with('message', 'Appraisal deleted');
    }

    public function getAppraisalBonuses()
    {
        $months = Month::orderBy('year', 'DESC')->orderBy('month', 'DESC')->get();
        return View('pages/admin/appraisal-bonus/index', compact('months'));
    }

    public function viewAppraisals($monthId)
    {
        $appraisals = AppraisalBonus::where('month_id', $monthId)->orderBy('appraisal_id', 'ASC')->orderBy('id', 'ASC')->get();

        // $appraisals = DB::select('SELECT * FROM `appraisal_bonuses` WHERE `appraisal_id` IN (SELECT `id` FROM `appraisals` where `user_id` IN (SELECT `id` FROM `users` where `is_active`=1))');
        // SELECT * FROM `appraisal_bonuses` WHERE `appraisal_id` IN (SELECT `id` FROM `appraisals` where `user_id` IN (SELECT `id` FROM `users` where `is_active`=1))
        $month = Month::find($monthId);
        return View('pages/admin/appraisal-bonus/show', compact('month', 'appraisals'));

    }

    public function regenerate($month_id)
    {
        $response = AppraisalBonus::regenerateAppraisal($month_id);
        if (!empty($response['status']) && $response['status'] == false) {
            return Redirect::back()->withErrors('Something went wrong!');
        }

        return redirect('/admin/appraisal-bonus/' . $month_id);
    }

    public function storeData(Request $request, $monthId)
    {
        $response = AppraisalBonus::store($request, $monthId);
        if ($response) {
            return Redirect::back()->with('message', 'Successfully Added');
        } else {
            return Redirect::back()->withErrors('Something went wrong!');
        }

    }
    public function addAnnualBonus($monthId, $userId)
    {
        $response = AppraisalBonus::addAnnualBonus($monthId, $userId);
        if (!empty($response['status']) && $response['status'] == false) {
            return Redirect::back()->withErrors($response['message']);

        }
        // return redirect('/admin/appraisal-bonus/' . $monthId);
        return Redirect::back()->with('message', $response['message']);

    }

    public function deleteRecord($appraisal_id)
    {
        $appraisal = appraisalBonus::find($appraisal_id);
        if ($appraisal) {
            if ($appraisal->delete()) {
                return Redirect::back()->with('message', 'Successfully Deleted');
            } else {
                return Redirect::back()->withErrors(["Something went wrong!"]);
            }
        } else {
            return Redirect::back()->withErrors(["Entry not found!"]);
        }

    }

    public function monthStatus($monthId)
    {
        $response = MonthSetting::toggleMonth($monthId);
        return Redirect::back()->with('message', $response);
    }
}
