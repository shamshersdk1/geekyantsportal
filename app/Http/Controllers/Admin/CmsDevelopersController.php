<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Admin\CmsDeveloper;
use App\Models\Admin\CmsTechnologyDeveloper;
use App\Models\Admin\CmsSlide;
use App\Models\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Redirect;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use Config;
use Exception;
use DB;
use Auth;
use Validator;

class CmsDevelopersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cmsDevelopers = CmsDeveloper::paginate(20);

        return View('pages/admin/cms-developers/index', ['cmsDevelopers' => $cmsDevelopers ] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('pages/admin/cms-developers/add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'title' => 'required',
			'status' => 'required',
            'description' => 'required',
            'slug' => 'required'
        ]);


        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors(['Please provide all the mandatory fields']);
        }

        $slugExists = CmsDeveloper::checkSlug($request->slug);
        if ( $slugExists == true ){
            return Redirect::back()->withInput()->withErrors(['Slug exists']);
        }

        $cmsObj = new CmsDeveloper();

        $cmsObj->name = $request->name;
        $cmsObj->title = $request->title;
        $cmsObj->status = $request->status;
        $cmsObj->description = $request->description;
        $cmsObj->slug = $request->slug;
        $cmsObj->keywords = $request->keywords;
        $cmsObj->meta_description = $request->meta_description;
        $cmsObj->hire_text = $request->hire_text;
        $cmsObj->youtube_link = $request->youtube_link ?: null;

        if($cmsObj->save()){
            return redirect('/admin/cms-developers')->with('message', 'Successfully Added');
        }
        else {
            return redirect::back()->withErrors($cmsObj->getErrors())->withInput();
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cmsDeveloper = CmsDeveloper::find($id);


        if($cmsDeveloper){

            return View('pages/admin/cms-developers/edit', ['cmsDeveloper' => $cmsDeveloper]);
        }
        else {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'title' => 'required',
			'status' => 'required',
            'description' => 'required',
            'slug' => 'required'
        ]);
        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors(['Please provide all the mandatory fields']);
        }

        $cmsDeveloper = CmsDeveloper::find($id);
        if ( $cmsDeveloper->slug != $request->slug ){
            $slugExists = CmsDeveloper::checkSlug($request->slug);
            if ( $slugExists == true ){
                return Redirect::back()->withInput()->withErrors(['Slug exists']);
            }
        }
        $cmsDeveloper->name = $request->name;
        $cmsDeveloper->title = $request->title;
        $cmsDeveloper->status = $request->status;
        $cmsDeveloper->description = $request->description;
        $cmsDeveloper->slug = $request->slug;
        $cmsDeveloper->keywords = $request->keywords;
        $cmsDeveloper->meta_description = !empty($request->meta_description) ? $request->meta_description : '' ;
        $cmsDeveloper->hire_text = !empty($request->hire_text) ? $request->hire_text : '' ;
        $cmsDeveloper->youtube_link = $request->youtube_link ?: null;

        if($cmsDeveloper->save()){
            return redirect('admin/cms-developers')->with('message', 'Updated successfully');
        }
        else {
            return redirect::back()->withErrors($cmsObj->getErrors())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cmsDeveloper = CmsDeveloper::find($id);
            if($cmsDeveloper->delete()){
                return redirect('/admin/cms-developers')->with('message','Successfully Deleted');
            }
            elseif(!$cmsDeveloper->delete()){
                return redirect::back()->withErrors($cmsDeveloper->getErrors());
            }
        else {
            abort(404);
        }
    }
    /**
     * Show details of the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cmsObj = CmsDeveloper::find($id);
        $cmsDevelopers =  CmsTechnologyDeveloper::with(['image' => function($query) {
                            $query->where('reference_type', '=', 'App\Models\Admin\CmsTechnologyDeveloper');
                            }],'user','cms_developer')
                            ->where('cms_technology_id',$id)->get();

        $cmsSlides =  CmsSlide::with(['image' => function($query) {
                            $query->where('reference_type', '=', 'App\Models\Admin\CmsSlide');
                            }],'cms_developer')
                            ->where('cms_technology_id',$id)->get();

        if ( isset($cmsObj) )
        {
            return View('pages/admin/cms-developers/show', ['cmsObj' => $cmsObj, 'cmsDevelopers' => $cmsDevelopers, 'cmsSlides' => $cmsSlides ] );
        }
        return Redirect::back()->withInput()->withErrors(['Something went wrong']);
    }







}
