<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Admin\Payslip;
use App\Models\Admin\PayrollRuleForCsv;
use App\Models\Admin\PayslipCsvData;
use App\Models\Admin\PayslipCsvMonth;
use App\Models\User;
use App\Models\Admin\PayslipMonth;
use App\Models\Admin\Payslip\SalaryUserData;
use App\Models\Admin\Payslip\SalaryUserDataKey;
use App\Models\Admin\Bonus;
use App\Models\Admin\ItSaving;
use App\Models\Loan;

use App\Jobs\DeletePayslipCsvData;
use Redirect;
use View;

class PayslipCsvController extends Controller
{
    public function index()
    {
        $csvMonths = PayslipCsvMonth::get();
        return view('pages.admin.payslip-csv.index', compact('csvMonths'));
    }
    public function create()
    {
        return view('pages/admin/payslip-csv/add');
    }
    public function store(Request $request)
    {
        if(empty($request->days)) {
            return Redirect::back()->withErrors(['You need to provide the no. of working days']);
        }
        $month = $request->month;
        if($month < 10) {
            $month = '0'.$month;
        }
        $id = null;
        $csvMonth = PayslipCsvMonth::where('month', $month)->where('year', $request->year)->first();
        if($csvMonth) {
            return Redirect::back()->withErrors(['Payslip already exists for the given month']);
        } else {
            $csvMonth = new PayslipCsvMonth();
            $csvMonth->month = $month;
            $csvMonth->year = $request->year;
            $csvMonth->days = $request->days;
            $csvMonth->created_by = \Auth::id();
            $csvMonth->status = "pending";
            $csvMonth->save();
            $id = $csvMonth->id;
        }
        return Redirect::to("/admin/create-payroll-csv/".$id);
    }
    public function show($id)
    {
        $csvMonth = PayslipCsvMonth::find($id);
        if($csvMonth) {
            $status = $csvMonth->status;
            return view('pages.admin.payslip-csv.show', compact('id', 'status'));
            return Redirect::back()->withErrors(['Unable to generate CSV']);
        } else {
            return Redirect::back()->withErrors(['Invalid link followed']);
        }
    }
    public function destroy($id)
    {
        $csvMonth = PayslipCsvMonth::find($id);
        if($csvMonth) {
            $job = (new DeletePayslipCsvData($csvMonth->id))->onQueue('delete-payslip-csv-data');
            dispatch($job);
            $csvMonth->delete();
            return Redirect::back()->with("message", 'Deleted Successfully');
        } else {
            return Redirect::back()->withErrors(['Invalid link follwed']);
        }
    }
    public function download($id)
    {
        $csvMonth = PayslipCsvMonth::find($id);
        if($csvMonth) {
            $month = $csvMonth->month;
            if($month < 10) {
                $month = '0'.$month;
            }
            $head = ['Sl No','Employee Code','Employee Name','Email ID','PAN No','DOB','DOJ','Designation','Gender','Bank A/c. No.','Bank IFSC Code','PF No','UAN No','ESI No','Month','Year','Total Working Day in Month','No of Days Worked'];
            $head = array_merge($head, PayrollRuleForCsv::orderBy('id')->pluck('value')->toArray());
            $head = array_merge($head, ['SL(Op Bal)','CL (Op Bal)','PL (Op Bal)','CME SL','CME CL','CME PL','SL Availed(utilised)','CL Availed','PL Availed(utilised)','Bal c/f SL','Bal c/f CL','Bal c/f PL']);
            
            // $result = Payslip::generateCsvData($month,$csvMonth->year, $csvMonth->days);
            $data = PayslipCsvData::where('payslip_csv_month_id', $id)->get();
            if(count($data) > 0) {
                $csv = \League\Csv\Writer::createFromFileObject(new \SplTempFileObject());
                $csv->insertOne($head);
                foreach($data as $value)
                {
                    $value = (json_decode(json_encode($value), true));
                    unset($value['id']);
                    unset($value['payslip_csv_month_id']);
                    unset($value['user_id']);
                    unset($value['created_at']);
                    unset($value['updated_at']);
                    $csv->insertOne($value);
                }
                $fileName = 'Report-'.$month.'-'.$csvMonth->year.'.csv';
                Storage::put('CSV/'.$fileName, $csv);
                $csvMonth->csv_path = storage_path('csv').'/'.$fileName;
                $csvMonth->save();
                $csv->output('Report-'.$month.'-'.$csvMonth->year.'.csv');
            }
        } else {
            return Redirect::back()->withErrors(['CSV data not found in database']);
        }
    }

    public function reviewMonth($monthId) {
        $users = SalaryUserData::where('month_id', $monthId)->get();
        return view('pages.admin.payslip-csv.user-list', compact('users'));
    }
    public function reviewMonthUser($monthId, $id) {

        $financialYearId = 1;
        $salaryData = SalaryUserData::with('salaryKeys.groupKey')->find($id);
        $userId = $salaryData->user_id;
        $user = User::find($userId);
        

        $bonuses = Bonus::where('user_id',$userId)->where('month_id',$monthId)->get();
        $loans = Loan::where('user_id',$userId)->where('status','in_progress')->get();
        $itSaving = ItSaving::where('user_id',$userId)->where('financial_year_id',$financialYearId)->first();

        $payslipMonth = PayslipMonth::find($monthId);
        return view('pages.admin.payslip-csv.user-data', compact('user','salaryData','payslipMonth','bonuses','loans','itSaving'));
    }
    
}
