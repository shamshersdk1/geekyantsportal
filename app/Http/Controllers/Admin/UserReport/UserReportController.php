<?php

namespace App\Http\Controllers\Admin\UserReport;

use App\Http\Controllers\Controller;
use App\Models\Admin\Project;
use App\Models\Admin\ProjectResource;
use App\Models\User;

class UserReportController extends Controller
{
    public function index()
    {
        $userList = User::orderBy('employee_id')->get(); 
        return view('pages.admin.user-report.index', compact('userList'));
    }

    public function show($id)
    {
        $userList = User::orderBy('employee_id')->get();
        $user = User::find($id);
        $date = date('Y-m-d');
        $userProjects = ProjectResource::where('user_id', $id)->where('end_date', null)->where(function ($query) use ($date, $id) {
            $query->where('user_id', $id)
                ->orWhere('end_date', '>=', $date);
        })->get();
        $tlProjects = Project::getUserProjects($user->id);
        return view('pages.admin.user-report.show', compact('userList','tlProjects','userProjects','user'));
    }
}
