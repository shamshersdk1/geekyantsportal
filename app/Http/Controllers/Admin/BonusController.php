<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Appraisal\Appraisal;
use App\Models\Admin\Bonus;
use App\Models\Admin\OnsiteAllowance;
use App\Models\Admin\OnSiteBonus;
use App\Models\Admin\Project;
use App\Models\BonusRequest;
use App\Models\Month;
use App\Models\NonworkingCalendar;
use App\Models\Transaction;
use App\Models\User;
use App\Services\BonusService;
use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Http\Request;
use Redirect;
use App\Models\Audited\Bonus\BonusConfirmed;

class BonusController extends Controller
{
    public function index()
    {
        $result = Bonus::filter();
        if ($result['status'] == false) {
            return Redirect::back()->withErrors('No results found');
        }
        $bonuses = $result['bonuses'];
        return view('pages.admin.bonus.index', compact('bonuses'));
        // $bonuses=Bonus::orderBy('date', 'desc')->orderBy('created_at', 'desc')->get();
        // return view('pages.admin.bonus.index', compact('bonuses'));
    }
    public function create()
    {

        $jsonuser = User::getUsers();

        return view('pages.admin.bonus.add', compact('jsonuser'));
    }
    public function store(Request $request)
    {
        $response = Bonus::saveBonus($request);
        if (!$response['status']) {
            return redirect()->back()->withErrors($response['errors'])->withInput();
        }
        return redirect()->back()->with('message', 'Bonus successfully added');
    }
    public function update(Request $request, $id)
    {
        $bonus = Bonus::find($id);
        if (empty($bonus)) {
            return redirect::back()->with('errors', 'invalid link followed');
        }
        if ($request->approve) {
            if ($bonus->status == 'pending') {
                $bonus->status = 'approved';
                $bonus->approved_by = \Auth::id();
                $bonus->approved_at = Carbon::now()->toDateTimeString();
                if ($bonus->save()) {
                    return redirect()->back()->with('message', 'Bonus Approved');
                }
                return redirect()->back()->with('errors', 'Unable to approve');
            } else {
                return redirect()->back()->with('errors', 'Can\'t be approved');
            }
        } elseif ($request->reject) {
            if ($bonus->status == 'pending') {
                $bonus->status = 'rejected';
                $bonus->approved_by = \Auth::id();
                $bonus->approved_at = Carbon::now()->toDateTimeString();
                if ($bonus->save()) {
                    return redirect()->back()->with('message', 'Bonus Rejected');
                }
                return redirect()->back()->with('errors', 'Unable to reject');
            } else {
                return redirect()->back()->with('errors', 'Can\'t be rejected');
            }
        } elseif ($request->paid) {
            if ($bonus->status == 'approved') {
                $bonus->paid = true;
                $bonus->paid_by = \Auth::id();
                $bonus->paid_at = Carbon::now()->toDateTimeString();
                if ($bonus->save()) {
                    return redirect()->back()->with('message', 'Bonus Payment Approved');
                }
                return redirect()->back()->with('errors', 'Unable to approve payment');
            } else {
                return redirect()->back()->with('errors', 'Bonus needs to be approved first');
            }
        } else {
            $response = Bonus::updateBonus($request, $id);
            if (!$response['status']) {
                return redirect()->back()->withErrors($response['errors'])->withInput();
            }
            return redirect()->back()->with('message', 'Bonus successfully updated');
        }
    }
    public function destroy($id)
    {
        $bonus = Bonus::find($id);
        if (empty($bonus)) {
            return redirect()->back()
                ->withErrors('Invalid link followed')
                ->withInput();
        }
        $bonus->delete();
        return redirect()->back()->with('message', 'Bonus successfully deleted');
    }
    public function edit($id)
    {
        $bonus = Bonus::find($id);
        if (empty($bonus)) {
            return redirect()->back()->withErrors('Invalid link followed');
        }
        if ($bonus->status != "pending") {
            return redirect()->back()->withErrors('Can\'t be edited');
        }
        $jsonuser = User::getUsers();
        $bmonth = date('n', strtotime($bonus->date));
        $byear = date('Y', strtotime($bonus->date));
        return view('pages.admin.bonus.edit', compact('jsonuser', 'bonus', 'bmonth', 'byear'));
    }
    public function show($id)
    {
        $bonus = Bonus::find($id);
        if (empty($bonus)) {
            return redirect()->back()->withErrors('Invalid link followed');
        }
        return view('pages.admin.bonus.show', compact('bonus'));
    }
    public function ledger(Request $request)
    {
        if (empty($request->search_year) && !empty($request->search_month)) {
            return Redirect::back()->withErrors('Month alone cannot be selected');
        }

        if (empty($request->search_year) && empty($request->search_month)) {
            $bonusList = Bonus::where('paid', 1)->orderBy('date', 'ASC')->get();
            $total = Bonus::total();
        } elseif (!empty($request->search_year) && empty($request->search_month)) {
            $start_date = $request->search_year . '-01-01';
            $end_date = $request->search_year . '-12-31';
            $bonusList = Bonus::where('date', '>=', $start_date)->where('date', '<=', $end_date)->where('paid', 1)->orderBy('date', 'ASC')->get();
            $total = Bonus::customTotal($start_date, $end_date);
        } elseif (!empty($request->search_year) && !empty($request->search_month)) {
            $start_date = date($request->search_year . '-' . $request->search_month . '-01');
            $end_date = date($request->search_year . '-' . $request->search_month . '-' . date('t', strtotime($start_date)));

            $bonusList = Bonus::where('date', '>=', $start_date)->where('date', '<=', $end_date)->where('paid', 1)->orderBy('date', 'ASC')->get();
            $total = Bonus::customTotal($start_date, $end_date);
        } else {
            return Redirect::back()->withErrors('Something went wrong');
        }
        return view('pages.admin.bonus.ledger', compact('bonusList', 'total'));
    }

    public function dueBonuses($monthId = null)
    {
        $monthList = Month::orderBy('year', 'desc')->orderBy('month', 'desc')->get();
        if (!count($monthList) > 0) {
            return view('pages.admin.bank-transfer.bank-transfer.index')->with('message', 'no month found.');
        }
        $currMonth = $monthList[0];
        if ($monthId) {
            $currMonth = Month::find($monthId);
        }
       // $users = Bonus::select('user_id')->where('month_id',$currMonth->id)->where('transaction_id', null)->distinct()->orderBy('user_id')->get();
        $requestType = "due";
        $monthObj = Month::where('month', date('m'))->where('year', date('Y'))->first();
        if (!$monthObj) {
            $monthObj = null;
        }
        $bonuses = DB::select(DB::RAW("SELECT u.name, u.employee_id , b.user_id, 
        SUM(CASE WHEN b.reference_type = 'App\\\Models\\\Admin\\\AdditionalWorkDaysBonus'  THEN b.amount ELSE 0 END) `additional_amount`,
        SUM(CASE WHEN b.reference_type = 'App\\\Models\\\Admin\\\AdditionalWorkDaysBonus' THEN 1 ELSE 0 END) `additional_count`,
        SUM(CASE WHEN b.reference_type = 'App\\\Models\\\Admin\\\OnSiteBonus'  THEN b.amount ELSE 0 END) `onsite_amount`,
        SUM(CASE WHEN b.reference_type = 'App\\\Models\\\Admin\\\OnSiteBonus'  THEN 1 ELSE 0 END) `onsite_count`,
        SUM(CASE WHEN b.reference_type = 'App\\\Models\\\User\\\UserTimesheetExtra'  THEN b.amount ELSE 0 END) `extra_amount`,
        SUM(CASE WHEN b.reference_type = 'App\\\Models\\\User\\\UserTimesheetExtra'  THEN 1 ELSE 0 END) `extra_count`,
        SUM(CASE WHEN b.reference_type = 'App\\\Models\\\Admin\\\PerformanceBonus'  THEN b.amount ELSE 0 END) `performance_amount`,
        SUM(CASE WHEN b.reference_type = 'App\\\Models\\\Admin\\\PerformanceBonus'  THEN 1 ELSE 0 END) `performance_count`,
        SUM(CASE WHEN b.reference_type = 'App\\\Models\\\Admin\\\TechTalkBonusesUser'  THEN b.amount ELSE 0 END) `techtalk_amount`,
        SUM(CASE WHEN b.reference_type = 'App\\\Models\\\Admin\\\TechTalkBonusesUser'  THEN 1 ELSE 0 END) `techtalk_count`,
        SUM(CASE WHEN b.reference_type = 'App\\\Models\\\Admin\\\ReferralBonus'  THEN b.amount ELSE 0 END) `referral_amount`,
        SUM(CASE WHEN b.reference_type = 'App\\\Models\\\Admin\\\ReferralBonus'  THEN 1 ELSE 0 END) `referral_count`
        
        FROM `bonuses` b, `users` u WHERE month_id = $currMonth->id AND u.id= b.user_id GROUP BY user_id"));
        
        return view('pages.admin.bonus.due', compact('currMonth', 'monthList','bonuses', 'requestType', 'monthObj'));
    }

    public function lockBonusApproval()
    {
        $monthObj = Month::where('month', date('m'))->where('year', date('Y'))->first();
        if ($monthObj) {
            if ($monthObj->bonus_pay_lock_date == null) {
                $monthObj->bonus_pay_lock_date = date("Y-m-d H:i:s");
                if ($monthObj->save()) {
                    return redirect('/admin/bonus-due')->with('message', 'Month Locked from Bonus Approval Successfully!');
                } else {
                    return Redirect::back()->withErrors('Error Locking Month Against Bonus Payment Approval');

                }
            } else {
                return redirect('/admin/bonus-due')->withErrors('Month Already locked');
            }
        } else {
            return redirect('/admin/bonus-due')->withErrors('Month not found');
        }

        // if ($monthObj) {
        //     $monthSettingObj = MonthSetting::where('month_id', $monthObj->id)->where('key', "bonus-due")->first();
        //     if ($monthSettingObj && $monthSettingObj->value == "locked") {
        //         return redirect('/admin/bonus-due')->withErrors('Month Already locked');
        //     } elseif ($monthSettingObj) {
        //         $monthSettingObj->value = "locked";
        //         if ($monthSettingObj->save()) {
        //             return redirect('/admin/bonus-due')->with('message', 'Month Locked from Bonus Approval Successfully!');
        //         }

        //     } else {
        //         $monthSettingObj = new MonthSetting();
        //         $monthSettingObj->month_id = $monthObj->id;
        //         $monthSettingObj->key = "bonus-due";
        //         $monthSettingObj->value = "locked";
        //         if ($monthSettingObj->save()) {
        //             return redirect('/admin/bonus-due')->with('message', 'Month Locked from Bonus Approval Successfully!');
        //         }
        //     }
        // }
        // return Redirect::back()->withErrors('Error Locking or Creating Month lock setting');
    }

    public function userDueBonuses($userId)
    {
        $userObj = User::find($userId);
        if (!$userObj) {
            return Redirect::back()->withErrors('Invalid User Id');
        }
        $requestType = "due";
        $dueBonuses = BonusService::getDueBonues($userId);
        // echo"<pre>";
        // print_r($dueBonuses);
        // die;
        $monthObj = Month::where('month', date('m'))->where('year', date('Y'))->first();
        if (!$monthObj) {
            $monthObj = null;
        }

        return view('pages.admin.bonus.due-user', compact('userObj', 'dueBonuses', 'monthObj', 'requestType'));
    }

    public function userDueBonusesApprove($userId)
    {
        $loggedInUser = Auth::User();
        $allDueBonuses = Bonus::where('user_id', $userId)->where('transaction_id', null)->get();
        if (!$allDueBonuses) {
            return redirect()->back()->withErrors('No Due Bonuses Found');
        }
        $oneDaySalary = Appraisal::getBonusAmount($userId, date('Y-m-d'));
        DB::beginTransaction();
        try {
            foreach ($allDueBonuses as $dueBonus) {
                $monthObj = Month::find($dueBonus->month_id);
                if (!$monthObj) {
                    throw new Exception("Invalid Month Id", $dueBonus->month_id);
                }
                if ($dueBonus->type == "onsite") {
                    $onsiteBRObj = BonusRequest::find($dueBonus->bonus_request_id);
                    if ($onsiteBRObj) {
                        $redeemType = json_decode($onsiteBRObj->redeem_type, true);
                        if (!empty($redeemType['onsite_allowance_id'])) {
                            $onsiteObj = OnsiteAllowance::find($redeemType['onsite_allowance_id']);
                            if ($onsiteObj) {
                                $amount = $onsiteObj->amount;
                            } else {
                                throw new Exception("missing Onsite object", $dueBonus->id);
                            }

                        } else {
                            throw new Exception("missing Onsite allowance id", $dueBonus->id);
                        }

                    } else {
                        throw new Exception("missing onsite bonus entry/object", $dueBonus->id);
                    }

                } elseif ($dueBonus->type == "additional") {
                    $additionalBRObj = BonusRequest::find($dueBonus->bonus_request_id);
                    if ($additionalBRObj) {
                        $redeemType = json_decode($additionalBRObj->redeem_type, true);
                        if (!empty($redeemType['redeem_type'])) {
                            if ($redeemType["redeem_type"] == "encash") {
                                $amount = $oneDaySalary;
                            } else {
                                continue;
                            }
                        } else {
                            throw new Exception("missing redeem type", $dueBonus->id);
                        }
                    } else {
                        throw new Exception("missing additional bonus entry/object", $dueBonus->id);
                    }
                } elseif ($dueBonus->type == "extra") {
                    $extraBRObj = BonusRequest::find($dueBonus->bonus_request_id);
                    if ($extraBRObj) {
                        $redeemType = json_decode($extraBRObj->redeem_type, true);
                        if (!empty($redeemType['redeem_type'] && $redeemType['hours'])) {
                            if ($redeemType['redeem_type'] == "encash") {
                                $amount = ($oneDaySalary / 8) * $redeemType['hours'];
                            } else {
                                continue;
                            }
                        } else {
                            throw new Exception("missing redeem type and/or Hours", $dueBonus->id);
                        }
                    } else {
                        throw new Exception("missing extra bonus entry/object", $dueBonus->id);
                    }
                } else {
                    continue;
                }

                $data['month_id'] = $monthObj->id;
                $data['financial_year_id'] = $monthObj->financial_year_id;
                $data['user_id'] = $dueBonus->user_id;
                $data['reference_id'] = $dueBonus->id;
                $data['reference_type'] = 'App\Models\Admin\Bonus';
                $data['amount'] = $amount;
                $data['type'] = 'credit';
                $transactionObj = Transaction::saveTransaction($data);
                if (!$transactionObj['status'] || $transactionObj['id'] == null) {
                    return redirect()->back()->withErrors($transactionObj['message']);
                }
                $newTransaction = Transaction::find($transactionObj['id']);
                if (!$newTransaction) {
                    return redirect()->back()->withErrors("transaction object not found");
                }
                $dueBonus->transaction_id = $transactionObj['id'];
                $dueBonus->amount = $newTransaction->amount;
                $dueBonus->draft_amount = $amount;
                $dueBonus->paid_at = date('Y-m-d H:i:s');
                $dueBonus->paid_by = $loggedInUser->id;
                if (!$dueBonus->save()) {
                    return redirect()->back()->withErrors("error updating transaction id");
                }
            }
            DB::commit();
        } catch (Exception $e) {
            dd($e->getMessage());
            DB::rollback();
            return redirect()->back()->withErrors($e);
        }
        $users = Bonus::select('user_id')->where('transaction_id', null)->distinct()->orderBy('user_id')->get();
        if (count($users) > 0) {
            return redirect('/admin/bonus-due/' . $users[0]->id . 'approve');
        }
        return redirect()->back();
    }

    public function allBonuses()
    {
        $months = Month::getMonthList();
        return view('pages.admin.bonus.all', compact('months'));
    }

    public function allBonusesMonth($monthId)
    {
        $monthObj = Month::find($monthId);
        if (!$monthObj) {
            return redirect()->back()->withErrors('Invalid Month Id');
        }
        $users = Bonus::select('user_id')->where('transaction_id', '!=', null)->where('month_id', $monthObj->id)->where('status', "approved")->distinct()->orderBy('user_id')->get();
        $requestType = "all";
        return view('pages.admin.bonus.due', compact('users', 'requestType', 'monthObj'));
    }

    public function allBonusesMonthUser($monthId, $userId)
    {
        $userObj = User::find($userId);
        if (!$userObj) {
            return Redirect::back()->withErrors('Invalid User Id');
        }
        $monthObj = Month::find($monthId);
        if (!$monthObj) {
            return Redirect::back()->withErrors('Invalid Month Id');
        }
        $duesOverview = Bonus::select('type')->where('transaction_id', '!=', null)->where('month_id', $monthId)->where('user_id', $userId)->where('status', "approved")->distinct()->orderBy('type')->get();
        $allDueBonuses = Bonus::where('month_id', $monthId)->where('user_id', $userId)->where('status', "approved")->where('transaction_id', '!=', null)->orderBy('date', 'desc')->get();
        $requestType = "all";

        //onsite start
        $amount['onsite'] = 0;
        $days['onsite'] = 0;
        $onsiteBonusObj = $allDueBonuses->where('type', 'onsite');
        foreach ($onsiteBonusObj as $onsiteBonus) {
            $onsiteBRObj = BonusRequest::find($onsiteBonus->bonus_request_id);
            $onsiteId = json_decode($onsiteBRObj->redeem_type);
            $onsiteObj = OnsiteAllowance::find($onsiteId->onsite_allowance_id);
            $approver = user::find($onsiteBonus->approved_by);
            $onsiteBonus->approver_name = $approver->name;
            $project = Project::find($onsiteObj->project_id);
            $onsiteBonus->project = $project->project_name;
            $onsiteBonus->rate = $onsiteObj->amount;
            $transactionObj = Transaction::where('id', $onsiteBonus->transaction_id)->first();
            $amount['onsite'] += $transactionObj->amount;
            // $amount['onsite'] += $onsiteBonus->amount;
            $days['onsite']++;
        }
        //onsite end

        //additional start
        $amount['additional'] = 0;
        $days['additional'] = 0;
        $rate['additional'] = Appraisal::getBonusAmount($userId, date('Y-m-d'));
        $additionalBonusObj = $allDueBonuses->where('type', 'additional');
        foreach ($additionalBonusObj as $additionalBonus) {
            if (NonworkingCalendar::isWeekend($additionalBonus->date)) {
                $additionalBonus->reason = "Weekend";
            } elseif (NonworkingCalendar::is_holiday($additionalBonus->date)) {
                $additionalBonus->reason = "Holiday";
            } else {
                $additionalBonus->reason = "On Leave";
            }
            $additionalBRObj = BonusRequest::find($additionalBonus->bonus_request_id);
            $additionalBonus->sub_type = json_decode($additionalBRObj->redeem_type)->redeem_type;
            $approver = user::find($additionalBonus->approved_by);
            $additionalBonus->approver_name = $approver->name;
            if ($additionalBonus->sub_type == "encash") {
                $transactionObj = Transaction::where('id', $additionalBonus->transaction_id)->first();
                $amount['additional'] += $transactionObj->amount;
                // $amount['additional'] += $additionalBonus->amount;
            }
            $days['additional']++;
        }
        //additional end

        //extra start
        $amount['extra'] = 0;
        $days['extra'] = 0;
        $rate['extra'] = Appraisal::getBonusAmount($userId, date('Y-m-d')) / 8;
        $extraBonusObj = $allDueBonuses->where('type', 'extra');
        foreach ($extraBonusObj as $extraBonus) {
            $extraBRObj = BonusRequest::find($extraBonus->bonus_request_id);
            $extraBonus->sub_type = json_decode($extraBRObj->redeem_type)->redeem_type;
            $extraBonus->hours = json_decode($extraBRObj->redeem_type)->hours;
            $approver = user::find($extraBonus->approved_by);
            $extraBonus->approver_name = $approver->name;
            if ($extraBonus->sub_type == "encash") {
                $transactionObj = Transaction::where('id', $extraBonus->transaction_id)->first();
                $amount['extra'] += $transactionObj->amount;
                // $amount['extra'] += $extraBonus->amount * $extraBonus->hours;
            }
            $days['extra'] += $extraBonus->hours;
        }
        //extra end

        $rate['techtalk'] = 0;
        $rate['referral'] = 0;

        $days['techtalk'] = 0;
        $days['referral'] = 0;

        $amount['techtalk'] = $rate['techtalk'] * $days['techtalk'];
        $amount['referral'] = $rate['referral'] * $days['referral'];

        return view('pages.admin.bonus.due-user', compact('userObj', 'duesOverview', 'allDueBonuses', 'rate', 'days', 'monthObj', 'amount', 'requestType'));
    }

    public static function migrateOnsiteBonus()
    {
        $bonusRequests = BonusRequest::where('status', 'approved')->where('date', '>=', '2019-04-01')->get();
        DB::beginTransaction();
        try {
            foreach ($bonusRequests as $bonusRequest) {
                if (!empty($bonusRequest->user_id) && !empty($bonusRequest->date) && !empty($bonusRequest->redeem_type)) {
                    $userObj = User::find($bonusRequest->user_id);
                    if (!$userObj) {
                        throw new Exception("Invalid user at BR id: " . $bonusRequest->id);
                    }

                    if (empty($bonusRequest->redeem_type)) {
                        throw new Exception("Invalid redeem_type/missing onsite allowance id at BR id: " . $bonusRequest->id);
                    }

                    $redeemType = json_decode($bonusRequest->redeem_type);
                    if ($bonusRequest->type == 'onsite' && !empty($redeemType['onsite_allowance_id'])) {
                        $onsiteAllowanceObj = OnsiteAllowance::find(json_decode($bonusRequest->redeem_type)->onsite_allowance_id);
                        if (!$onsiteAllowanceObj) {
                            throw new Exception("Invalid onsite allowance at BR id: " . $bonusRequest->id);
                        }
                        $data = [];
                        $data['user_id'] = $userObj->id;
                        $data['date'] = $bonusRequest->date;
                        $data['status'] = "approved";
                        $data['created_by'] = $userObj->id;
                        $data['onsite_allowance_id'] = $onsiteAllowanceObj->id;
                        $onsiteData = OnSiteBonus::saveData($data);
                        if (!$onsiteData) {
                            throw new Exception("Unable to save data at BR id: " . $bonusRequest->id);
                        }
                    } else {
                        throw new Exception("missing data at BR id: " . $bonusRequest->id);
                    }
                }
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            print_r("Exception: ");
            return $e->getMessage();
        }
        return "successfully migrated onsite data";
    }

    public function paidBonuses()
    {
        $months = Month::getMonthList();
        if (!$months) {
            return view('pages.admin.bonus.all-paid');
        }
        return view('pages.admin.bonus.all-paid', compact('months'));
    }

    public function paidBonusesMonth($monthId)
    {
        $monthObj = Month::find($monthId);
        if (!$monthObj) {
            return redirect()->back()->withErrors('Invalid Month Id');
        }
        $users = Transaction::select('user_id')->where('month_id', $monthObj->id)->where('reference_type', "App\Models\Admin\Bonus")->distinct()->orderBy('user_id')->get();
        return view('pages.admin.bonus.paid', compact('users', 'monthObj'));
    }

    public function paidBonusesMonthUser($monthId, $userId)
    {
        $userObj = User::find($userId);
        if (!$userObj) {
            return Redirect::back()->withErrors('Invalid User Id');
        }
        $monthObj = Month::find($monthId);
        if (!$monthObj) {
            return Redirect::back()->withErrors('Invalid Month Id');
        }
        $returnData = BonusService::getMonthlyBonusTransactions($userId, $monthId);
        if ($returnData) {
            $data = $returnData["data"];
            $total = $returnData["total"];
        } else {
            return Redirect::back()->withErrors('Unable to find data');
        }
        return view('pages.admin.bonus.user-paid', compact('userObj', 'monthObj', 'data', 'total'));
    }

    public function dueBonusesUser($monthId = null)
    {
        $monthList = Month::orderBy('year', 'desc')->orderBy('month', 'desc')->get();
        if (!count($monthList) > 0) {
            return view('pages.admin.bank-transfer.bank-transfer.index')->with('message', 'no month found.');
        }
        $currMonth = $monthList[0];
        if ($monthId) {
            $currMonth = Month::find($monthId);
        }
        $users = DB::select(DB::RAW("SELECT u.name as name, SUM(CASE WHEN `reference_type` = 'App\\\Models\\\Admin\\\PerformanceBonus' THEN `amount` ELSE 0 END) `performanceamount`,
                            SUM(CASE WHEN `reference_type` = 'App\\\Models\\\Admin\\\TechTalkBonusesUser' THEN `amount` ELSE 0 END) `techtalkamount`,
                            SUM(CASE WHEN `reference_type` = 'App\\\Models\\\Admin\\\ReferralBonus' THEN `amount` ELSE 0 END) `referralamount`,
                            SUM(CASE WHEN `reference_type` = 'App\\\Models\\\Admin\\\AdditionalWorkDaysBonus' THEN `amount` ELSE 0 END) `additionalamount`,
                            SUM(CASE WHEN `reference_type` = 'App\\\Models\\\User\\\UserTimesheetExtra' THEN `amount` ELSE 0 END) `extraworkingamount`, 
                            SUM(CASE WHEN `reference_type` = 'App\\\Models\\\Admin\\\OnSiteBonus' THEN `amount` ELSE 0 END) `onsiteamount`, `user_id`,
                            SUM(`amount`) as `total` FROM `bonuses` as b 
                            LEFT JOIN users u ON u.id = b.user_id
                            Where `month_id` = $currMonth->id GROUP BY `user_id`
                            
                            "));
        return view('pages.admin.bonus.due-bonus-user', compact('currMonth', 'monthList', 'users'));
    }


    public function getConfirmedBonus()
    {
        $users = BonusConfirmed::select('user_id')->distinct()->get();
        $amount = [];
        foreach($users as $user)
        {
            $bonusUsers = BonusConfirmed::where('user_id',$user->user_id)->get();
            foreach($bonusUsers as $bonusUser)
            {
                $amount[$user->user_id] += $bonusUser->bonus->amount;
            
            }
        }
        return view('pages.admin.bonus.bonus-confirmed', compact('amount','users'));
    }
}
