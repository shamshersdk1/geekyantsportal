<?php

namespace App\Http\Controllers\Admin;

use App\Events\Loan\LoanRequestApproved;
use App\Events\Loan\LoanRequestDisbursed;
use App\Events\Loan\LoanRequestReconciled;
use App\Events\Loan\LoanRequestRejected;
use App\Events\Loan\LoanRequestReviewed;
use App\Events\Loan\LoanRequestSubmitted;
use App\Http\Controllers\Controller;
use App\Models\Admin\File;
use App\Models\Admin\LoanRequest;
use App\Models\Admin\LoanRequestActivity;
use App\Models\Loan;
use App\Models\User;
use App\Models\Appraisal\AppraisalBonusType;
use Auth;
use Illuminate\Http\Request;
use Redirect;
use URL;
use App\Services\LoanService;

class LoanRequestController extends Controller
{
    public function index()
    {

        $loan_requests = LoanRequest::where('user_id', Auth::id())->orderBy('id', 'DESC')->paginate(10);
        $previous_loans = Loan::where('user_id', Auth::id())->orderBy('id', 'DESC')->paginate(10);
        return View('pages.admin.loan-request.index', compact('loan_requests', 'previous_loans'));
    }
    public function create()
    {
        $loan_types = AppraisalBonusType::where('is_loanable',1)->get();
        if(!count($loan_types)>0)
        {
            return Redirect::back()->withInput()->withErrors("Loan Types not found");
        }
        return View('pages.admin.loan-request.add',compact('loan_types'));
    }
    public function store(Request $request)
    {
        $response['status'] = false;
        
        $data=$request->all();
        if(!$data){
            return Redirect::to("admin/loan-requests")->withErrors('Empty Input');
        }
        $userObj = isset($request->name) ? User::where('name', $request->name)->first() : Auth::user();

        $response = LoanService::loanEligibleCheck($userObj->id,$data['annual_bonus_type_id'],$data['amount']);
        if($response['status'])
        {
            return Redirect::back()->withInput()->withErrors($response['error']);
        } 
        $data['user_id']=$userObj->id;
        $data['amount']=$request->amount;
        $data['appraisal_bonus_id']=$response['appraisal_bonus_id'] ? $response['appraisal_bonus_id'] : null;
        $data['description']=empty($request->description) ? null : $request->description;
        $data['emi']=$request->emi;
        $data['emi_start_date'] = $data['emi_start_date'];
        $data['application_date'] = date("Y-m-d");
      
        $loanRequest=new LoanRequest();
        if($loanRequest->validate($data)){
            $response = $loanRequest->saveLoanRequest($data);
        }
        if($response['status'])
             return Redirect::to("admin/loan-requests/success/" . $response['data'] . "")->with('message', $response['message']);
        else{
            return Redirect::back()->withInput()->withErrors($loanRequest->getError());
        }
       
    }
    public function show($id)
    {
        return Redirect::back()->withErrors(["Invalid link followed"]);
        // $loan_request = LoanRequest::with('activities')->find($id);
        // $comments = [];
        // if(!($loan_request->user_id == Auth::id() || Auth::user()->hasRole("human-resources") || Auth::user()->hasRole("account") || Auth::user()->hasRole("management"))) {
        //     if($user->hasRole('reporting-manager')) {
        //         $children = Auth::user()->children()->pluck('id')->toArray();
        //         if(!in_array($loan_request->user_id, $children)) {
        //             return Redirect::back()->withErrors(["You don't have the necessary permission to view this link"]);
        //         }
        //     } else {
        //         return Redirect::back()->withErrors(["You don't have the necessary permission to view this link"]);
        //     }
        // }
        // $activities = $loan_request->activities()->with("user")->whereNotIn('role', ["User", "Admin"])->get();
        // if($activities) {
        //     foreach($activities as $activity)
        //     {
        //         $comments[] = ['user' => $activity->role." <br/> (".$activity->user->name.")", 'comment' => $activity->comment, 'time' => datetime_in_view($activity->created_at)];
        //     }
        // }
        // $previous_loans = Loan::where('user_id', $loan_request->user->id)->orderBy('application_date', 'desc')->paginate(10);
        // $file = File::where("reference_type", "App\Models\Admin\LoanRequest")->where("reference_id", $id)->first();
        // return View('pages.admin.loan-request.show', compact('loan_request', 'comments', 'file', 'previous_loans'));

    }
    public function edit($id)
    {
        $loan_request = LoanRequest::find($id);
        $loan_types = AppraisalBonusType::where('is_loanable',1)->get();
        if(!count($loan_types)>0)
        {
            return Redirect::back()->withInput()->withErrors("Loan Types not found");
        }
        $allow_edit = false;
        $errors = [];
        if ($loan_request) {
            if ($loan_request->status == "pending" && Auth::id() == $loan_request->user_id) {
                $allow_edit = true;
            } elseif ($loan_request->status != "approved") {
                $isAllowed = LoanRequest::isAllowed($loan_request->status, $loan_request->user->parent_id);
                if ($isAllowed) {
                    $allow_edit = true;
                }
            }
            if ($allow_edit) {
                return View('pages.admin.loan-request.edit', compact('loan_request','loan_types'));
            } else {
                $errors[] = "You don't have the necessary permission to view this link";
            }
        } else {
            $errors[] = "Invalid link followed";
        }
        return Redirect::back()->withErrors($errors);
    }
    public function update(Request $request, $id)
    {
        $data=$request->all();

        if(!$data){
            return Redirect::to("admin/loan-requests")->withErrors('Empty Input');
        }
        $data['user_id']=Auth::id();
        $response = LoanService::loanEligibleCheck($data['user_id'],$data['annual_bonus_type_id'],$data['amount']);
        if($response['status'])
        {
            return Redirect::back()->withInput()->withErrors($response['error']);
        } 
       
        $data['amount']=$request->amount;
        $data['appraisal_bonus_id']=$response['appraisal_bonus_id'] ? $response['appraisal_bonus_id'] : null;
        $data['description']=empty($request->description) ? null : $request->description;
        $data['emi']=$request->emi;

        $loan_request = LoanRequest::find($id);
        $allow_edit = false;
        $errors = [];
        if ($loan_request) {
            if ($loan_request->status == "pending" && Auth::id() == $loan_request->user_id) {
                $allow_edit = true;
            } elseif ($loan_request->status != "approved") {
                $isAllowed = LoanRequest::isAllowed($loan_request->status, $loan_request->user->parent_id);
                if ($isAllowed) {
                    $allow_edit = true;
                }
            }
            if ($allow_edit) {

                if($loan_request->validate($data)){
                    $response = $loan_request->UpdateLoanRequest($id,$data);
                    if($response['status']){
                        return Redirect::to("admin/pending-loan-requests")->with('message', $response['message']);
                    }
                    return Redirect::to("admin/pending-loan-requests")->with('message', $response['message']);
                 }
                else{
                    return Redirect::back()->withInput()->withErrors($loan_request->getError());
                }
                
            }
             else {
                $errors[] = "You don't have the necessary permission to view this link";
            }
        } else {
            $errors[] = "Invalid link followed";
        }
        return Redirect::back()->withErrors($errors);
    }
    public function destroy()
    {

    }

    public function getRequests()
    {
        $user = Auth::user();
        $loan_requests = LoanRequest::where('id', null);
        $loan_requests_rm = null;
        $loan_requests_hr = null;
        $loan_requests_management = null;
        $loan_requests_account = null;
        if ($user->hasRole('reporting-manager')) {
            $children = $user->children()->pluck('id')->toArray();
            $loan_requests = $loan_requests->orWhere(function ($query) use ($children) {
                $query->whereIn('user_id', $children)->where('status', "pending");
            });
        }
        if ($user->hasRole('human-resources')) {
            $loan_requests = $loan_requests->orwhereIn('status', ["submitted", "reconcile"]);
        }
        if (Auth::user()->hasRole("management")) {
            $loan_requests = $loan_requests->orwhere('status', "review");
        }
        if ($user->hasRole('account')) {
            $loan_requests = $loan_requests->orwhere('status', "approved");
        }
        $loan_requests = $loan_requests->orderBy('created_at', "desc")->get();
        return View('pages.admin.new-loan-request.index', compact('loan_requests'));
    }
    public function showRequest($id)
    {
        $loan_request = LoanRequest::with('activities', 'user')->find($id);
        $comments = [];
        $error = 0;
        if ($loan_request) {
            $isAllowed = LoanRequest::isAllowed($loan_request->status, $loan_request->user->parent_id);
            if (!$isAllowed) {
                return Redirect::back()->withErrors(["You don't have the necessary permission to view this link"]);
            } else {
                $activities = $loan_request->activities()->with("user")->whereNotIn('role', ["User", "Admin"])->get();
                if ($activities) {
                    foreach ($activities as $activity) {
                        $comments[] = ['user' => $activity->role . " (" . $activity->user->name . ")", 'comment' => $activity->comment, 'time' => datetime_in_view($activity->created_at),'status' => $activity->status];
                    }
                }
                $previous_loans = Loan::where('user_id', $loan_request->user->id)->orderBy('application_date', 'desc')->paginate(10);
                $file = File::where("reference_type", "App\Models\Admin\LoanRequest")->where("reference_id", $id)->first();
                return View('pages.admin.new-loan-request.show', compact('loan_request', 'comments', 'file', 'previous_loans'));
            }
        } else {
            return Redirect::back()->withErrors(["Invalid link followed"]);
        }
    }
    public function action(Request $request, $id)
    {
        $loan_request = LoanRequest::find($id);
        if ($loan_request) {
            $comment = empty($request->comment) ? null : $request->comment;
            $prev = $loan_request->status;
            $error = 0;
            $role = "";
            $back = URL::previous();
            $url = str_replace("/" . $id, "", $back);
            switch ($loan_request->status) {
                case "pending":
                $loan_request->status = "submitted";
                    $role = "Reporting Manager";
                    break;
                case "submitted":$loan_request->status = "review";
                    $role = "Human Resources";
                    break;
                case "review":$loan_request->status = "reconcile";
                    $role = "Management";
                    break;
                case "reconcile":$loan_request->status = "approved";
                    $role = "Human Resources";
                    break;
                case "approved":$loan_request->status = "disbursed";
                    $role = "Finance";
                    break;
                default:$error = 1;
            }
            if ($error == 0) {
                if ($request->reject == "reject") {
                    $user = \Auth::user();
                    $loan_request->status = "rejected";
                    $loan_request->save();
                    event(new LoanRequestRejected($loan_request->id, 'rejected', $role, $comment));
                    return Redirect::to($url)->with('message', "Request rejected successfully");
                }

                if ($loan_request->status == "submitted") {
                    if ($loan_request->approved_amount == 0 || $loan_request->approved_amount == 0.00) {
                        $loan_request->approved_amount = $loan_request->amount;
                    }
                    $loan_request->save();
                    event(new LoanRequestSubmitted($loan_request->id, 'approved', $role, $comment));
                }
                if ($loan_request->status == "review") {
                    $loan_request->save();
                    event(new LoanRequestReviewed($loan_request->id,'approved', $role, $comment));
                }
                if ($loan_request->status == "reconcile") {
                    $loan_request->save();
                    event(new LoanRequestReconciled($loan_request->id,'approved', $role, $comment));
                }
                if ($loan_request->status == "approved") {
                    if (File::where("reference_type", "App\Models\Admin\LoanRequest")->where("reference_id", $id)->count() == 0) {
                        $response = LoanRequest::uploadFile($request, $id);
                    } else {
                        $response['status'] = true;
                    }
                    $loan_request->save();
                    event(new LoanRequestApproved($loan_request->id, 'approved', $role, $comment));
                }
                if ($loan_request->status == "disbursed") {
                    if (empty($request->payment_detail) || empty($request->payment_mode) || empty($request->transaction_id)
                        || empty($request->payment_date)) {
                        return Redirect::back()->withErrors(["Payment details missing"])->withInput();
                    }
                    $data_array = ['payment_detail' => $request->payment_detail, 'payment_mode' => $request->payment_mode,
                        'transaction_id' => $request->transaction_id, 'payment_date' => date_to_yyyymmdd($request->payment_date),
                        'loan_request_id' => $loan_request->id];
                    $loan_request->save();
                    event(new LoanRequestDisbursed($loan_request->id,'approved', $role, $comment, $data_array));

                }
            return Redirect::to($url)->with('message', "Request forwarded");

            }
            return Redirect::back()->withErrors(["Invalid link followed"]);
        }
    }

    public function history()
    {
        $user_ids = [Auth::id()];
        $user = Auth::user();
        if ($user->hasRole("admin") || $user->hasRole("human-resources") || $user->hasRole("account") || $user->hasRole("management")) {
            $loan_requests = LoanRequest::orderBy('created_at', "desc")->get();
        } else {
            if ($user->hasRole('reporting-manager')) {
                $children = Auth::user()->children()->pluck('id')->toArray();
                $user_ids = array_merge($user_ids, $children);
            }
            $loan_requests = LoanRequest::whereIn('user_id', $user_ids)->orderBy('created_at', "desc")->get();
        }
        return View('pages.admin.loan-request.history', compact('loan_requests'));
    }
    public function showLoanDetail($id)
    {
        $loan_request = LoanRequest::with('activities', 'loan')->find($id);
        $comments = [];
        $user = Auth::User();
        if (!($loan_request->user_id == Auth::id() || Auth::user()->hasRole("admin") || Auth::user()->hasRole("human-resources") || Auth::user()->hasRole("account") || Auth::user()->hasRole("management"))) {
            if ($user->hasRole('reporting-manager')) {
                $children = Auth::user()->children()->pluck('id')->toArray();
                if (!in_array($loan_request->user_id, $children)) {
                    return Redirect::back()->withErrors(["You don't have the necessary permission to view this link"]);
                }
            } else {
                return Redirect::back()->withErrors(["You don't have the necessary permission to view this link"]);
            }
        }
        $activities = $loan_request->activities()->with("user")->whereNotIn('role', ["User", "Admin"])->get();
        if ($activities) {
            foreach ($activities as $activity) {
                $comments[] = ['user' => $activity->role . " (" . $activity->user->name . ")", 'comment' => $activity->comment, 'time' => datetime_in_view($activity->created_at) , 'status' => $activity->status ];
            }
        }
        $previous_loans = Loan::where('user_id', $loan_request->user->id)->orderBy('application_date', 'desc')->paginate(10);
        $file = File::where("reference_type", "App\Models\Admin\LoanRequest")->where("reference_id", $id)->first();
        return View('pages.admin.loan-request.show', compact('loan_request', 'comments', 'file', 'previous_loans'));
    }

    public function streamFile($id, $file_id)
    {
        $loan_request = LoanRequest::find($id);
        if ($loan_request) {
            if (!($loan_request->user_id == Auth::id() || Auth::user()->hasRole("human-resources") || Auth::user()->hasRole("account") || Auth::user()->hasRole("management") || Auth::user()->hasRole("admin"))) {
                if (Auth::user()->hasRole('reporting-manager')) {
                    $children = Auth::user()->children()->pluck('id')->toArray();
                    if (!in_array($loan_request->user_id, $children)) {
                        return Redirect::back()->withErrors(["You don't have the necessary permission to view this link"]);
                    }
                } else {
                    return Redirect::back()->withErrors(["You don't have the necessary permission to view this link"]);
                }
            }
            $file = File::find($file_id);
            if ($file && $file->reference_id == $id) {
                $path = $file->path;
                return response()->file($path);
            } else {
                return Redirect::back()->withErrors(['Invalid link followed']);
            }
        }
    }

    public function deleteRequest($id)
    {
        $loan_request = LoanRequest::find($id);
        if ($loan_request && $loan_request->status == "pending" && $loan_request->user_id == Auth::id()) {
            $response = LoanRequestActivity::saveActivity($loan_request->id, "approved", "User", Auth::id(), "Deleted by user");
            if ($response['status']) {
                $loan_request->delete();
                return Redirect::back()->with('message', "Request deleted successfully");
            } else {
                return Redirect::back()->withErrors([$response['message']]);
            }
        }
    }

    public function getPendingRequests()
    {
        $user = Auth::user();
        if ($user->isAdmin()) {
            $children = $user->pluck('id')->toArray();
            $children[] = Auth::user()->id;

            $loan_requests = LoanRequest::whereIn('user_id', $children)->where('status', "pending")->orderBy('created_at', "desc")->get();
            return View('pages.admin.new-loan-request.index', compact('loan_requests'));
        } else if ($user->hasRole('reporting-manager')) {
            $children = $user->children()->pluck('id')->toArray();
            $children[] = Auth::user()->id;
            $loan_requests = LoanRequest::whereIn('user_id', $children)->where('status', "pending")->orderBy('created_at', "desc")->whereIn('user_id', $user->reportees->pluck('id'))->get();
            return View('pages.admin.new-loan-request.index', compact('loan_requests'));
        } else {
            return Redirect::back()->withErrors(["You don't have the necessary permission to view this link"]);
        }
    }
    public function showPendingRequest($id)
    {
        $loan_request = LoanRequest::with('activities', 'user')->where('status', 'pending')->find($id);
        $comments = [];
        $error = 0;
        if ($loan_request) {
            $isAllowed = LoanRequest::isAllowed($loan_request->status, $loan_request->user->parent_id);
            if (!$isAllowed) {
                return Redirect::back()->withErrors(["You don't have the necessary permission to view this link"]);
            } else {
                $activities = $loan_request->activities()->with("user")->whereNotIn('role', ["User", "Admin"])->get();
                if ($activities) {
                    foreach ($activities as $activity) {
                        $comments[] = ['user' => $activity->role . " (" . $activity->user->name . ")", 'comment' => $activity->comment, 'time' => datetime_in_view($activity->created_at), 'status' => $activity->status];
                    }
                }
                $previous_loans = Loan::where('user_id', $loan_request->user->id)->orderBy('application_date', 'desc')->paginate(10);
                $file = File::where("reference_type", "App\Models\Admin\LoanRequest")->where("reference_id", $id)->first();
                return View('pages.admin.new-loan-request.show', compact('loan_request', 'comments', 'file', 'previous_loans'));
            }
        } else {
            return Redirect::back()->withErrors(["Invalid link followed"]);
        }
    }
    public function getSubmittedRequests()
    {
        $user = Auth::user();
        if ($user->hasRole('human-resources') || $user->isAdmin()) {
            $loan_requests = LoanRequest::where('status', "submitted")->orderBy('created_at', "desc")->get();
            return View('pages.admin.new-loan-request.index', compact('loan_requests'));
        } else {
            return Redirect::back()->withErrors(["You don't have the necessary permission to view this link"]);
        }
    }
    public function showSubmittedRequest($id)
    {
        $loan_request = LoanRequest::with('activities', 'user')->where('status', "submitted")->find($id);
        $comments = [];
        $error = 0;
        if ($loan_request) {
            $isAllowed = LoanRequest::isAllowed($loan_request->status, $loan_request->user->parent_id);
            if (!$isAllowed) {
                return Redirect::back()->withErrors(["You don't have the necessary permission to view this link"]);
            } else {
                $activities = $loan_request->activities()->with("user")->whereNotIn('role', ["User", "Admin"])->get();
                if ($activities) {
                    foreach ($activities as $activity) {
                        $comments[] = ['user' => $activity->role . " (" . $activity->user->name . ")", 'comment' => $activity->comment, 'time' => datetime_in_view($activity->created_at), 'status' => $activity->status];
                    }
                }
                $previous_loans = Loan::where('user_id', $loan_request->user->id)->orderBy('application_date', 'desc')->paginate(10);
                $file = File::where("reference_type", "App\Models\Admin\LoanRequest")->where("reference_id", $id)->first();
                return View('pages.admin.new-loan-request.show', compact('loan_request', 'comments', 'file', 'previous_loans'));
            }
        } else {
            return Redirect::back()->withErrors(["Invalid link followed"]);
        }
    }
    public function getReviewedRequests()
    {
        $user = Auth::user();
        if ($user->hasRole('management') || $user->isAdmin()) {
            $loan_requests = LoanRequest::where('status', "review")->orderBy('created_at', "desc")->get();
            return View('pages.admin.new-loan-request.index', compact('loan_requests'));
        } else {
            return Redirect::back()->withErrors(["You don't have the necessary permission to view this link"]);
        }
    }
    public function showReviewedRequest($id)
    {
        $loan_request = LoanRequest::with('activities', 'user')->where('status', "review")->find($id);
        $comments = [];
        $error = 0;
        if ($loan_request) {
            $isAllowed = LoanRequest::isAllowed($loan_request->status, $loan_request->user->parent_id);
            if (!$isAllowed) {
                return Redirect::back()->withErrors(["You don't have the necessary permission to view this link"]);
            } else {
                $activities = $loan_request->activities()->with("user")->whereNotIn('role', ["User", "Admin"])->get();
                if ($activities) {
                    foreach ($activities as $activity) {
                        $comments[] = ['user' => $activity->role . " (" . $activity->user->name . ")", 'comment' => $activity->comment, 'time' => datetime_in_view($activity->created_at) ,'status' => $activity->status];
                    }
                }
                $previous_loans = Loan::where('user_id', $loan_request->user->id)->orderBy('application_date', 'desc')->paginate(10);
                $file = File::where("reference_type", "App\Models\Admin\LoanRequest")->where("reference_id", $id)->first();
                return View('pages.admin.new-loan-request.show', compact('loan_request', 'comments', 'file', 'previous_loans'));
            }
        } else {
            return Redirect::back()->withErrors(["Invalid link followed"]);
        }
    }
    public function getReconciledRequests()
    {
        $user = Auth::user();
        if ($user->hasRole('human-resources') || $user->isAdmin()) {
            $loan_requests = LoanRequest::where('status', "reconcile")->orderBy('created_at', "desc")->get();
            return View('pages.admin.new-loan-request.index', compact('loan_requests'));
        } else {
            return Redirect::back()->withErrors(["You don't have the necessary permission to view this link"]);
        }
    }
    public function showReconciledRequest($id)
    {
        $loan_request = LoanRequest::with('activities', 'user')->where('status', "reconcile")->find($id);
        $comments = [];
        $error = 0;
        if ($loan_request) {
            $isAllowed = LoanRequest::isAllowed($loan_request->status, $loan_request->user->parent_id);
            if (!$isAllowed) {
                return Redirect::back()->withErrors(["You don't have the necessary permission to view this link"]);
            } else {
                $activities = $loan_request->activities()->with("user")->whereNotIn('role', ["User", "Admin"])->get();
                if ($activities) {
                    foreach ($activities as $activity) {
                        $comments[] = ['user' => $activity->role . " (" . $activity->user->name . ")", 'comment' => $activity->comment, 'time' => datetime_in_view($activity->created_at), 'status' => $activity->status];
                    }
                }
                $previous_loans = Loan::where('user_id', $loan_request->user->id)->orderBy('application_date', 'desc')->paginate(10);
                $file = File::where("reference_type", "App\Models\Admin\LoanRequest")->where("reference_id", $id)->first();
                return View('pages.admin.new-loan-request.show', compact('loan_request', 'comments', 'file', 'previous_loans'));
            }
        } else {
            return Redirect::back()->withErrors(["Invalid link followed"]);
        }
    }
    public function getApprovedRequests()
    {
        $user = Auth::user();
        if ($user->hasRole('account') || $user->isAdmin()) {
            $loan_requests = LoanRequest::where('status', "approved")->orderBy('created_at', "desc")->get();
            return View('pages.admin.new-loan-request.index', compact('loan_requests'));
        } else {
            return Redirect::back()->withErrors(["You don't have the necessary permission to view this link"]);
        }
    }
    public function showApprovedRequest($id)
    {
        $loan_request = LoanRequest::with('activities', 'user')->where('status', "approved")->find($id);
        $comments = [];
        $error = 0;
        if ($loan_request) {
            $isAllowed = LoanRequest::isAllowed($loan_request->status, $loan_request->user->parent_id);
            if (!$isAllowed) {
                return Redirect::back()->withErrors(["You don't have the necessary permission to view this link"]);
            } else {
                $activities = $loan_request->activities()->with("user")->whereNotIn('role', ["User", "Admin"])->get();
                if ($activities) {
                    foreach ($activities as $activity) {
                        $comments[] = ['user' => $activity->role . " <br/> (" . $activity->user->name . ")", 'comment' => $activity->comment, 'time' => datetime_in_view($activity->created_at),'status' => $activity->status];
                    }
                }
                $previous_loans = Loan::where('user_id', $loan_request->user->id)->orderBy('application_date', 'desc')->paginate(10);
                $file = File::where("reference_type", "App\Models\Admin\LoanRequest")->where("reference_id", $id)->first();
                return View('pages.admin.new-loan-request.show', compact('loan_request', 'comments', 'file', 'previous_loans'));
            }
        } else {
            return Redirect::back()->withErrors(["Invalid link followed"]);
        }
    }
    public function applyLoan()
    {
        $users = User::where('is_active', 1)->get();
        return View('pages.admin.loan-request.apply-loan', compact('users'));
    }
    public function success($loan_id)
    {
        $loan = LoanRequest::find($loan_id);
        //$message = "Your Loan request has been ";
        //return redirect('/admin/loan-requests/success/'.$loan->id)->with('message', 'Saved!');
        return View('pages.admin.loan-request.success', compact('loan'));
    }
}
