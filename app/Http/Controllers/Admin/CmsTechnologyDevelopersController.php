<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Admin\CmsDeveloper;
use App\Models\Admin\CmsTechnologyDeveloper;
use App\Models\User;
use App\Models\Admin\File;
use App\Services\FileService;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Redirect;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use Config;
use Exception;
use DB;
use Auth;
use Validator;

class CmsTechnologyDevelopersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $cmsDevelopers = CmsDeveloper::all();
        
        // return View('pages/admin/cms-developers/index', ['cmsDevelopers' => $cmsDevelopers ] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $cmsDeveloper = CmsDeveloper::find($id);
        $users = User::where('is_active',1)->get();
        return View('pages/admin/cms-technology-developers/add', ['cmsDeveloper' => $cmsDeveloper, 'users' => $users ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        
        $flag = 0;

        $validator = Validator::make($request->all(), [
            'cms_technology_id' => 'required',
            'user_id' => 'required',
			'status' => 'required',
            'about' => 'required',
            'area_of_interest' => 'required',
            'achievement' => 'required'
        ]);
        

        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors(['Please provide all the mandatory fields']);
        }

        $cmsObj = new CmsTechnologyDeveloper();
        $cmsObj->cms_technology_id = $request->cms_technology_id;
        $cmsObj->user_id = $request->user_id;
        $cmsObj->status = $request->status;
        $cmsObj->about = $request->about;
        $cmsObj->area_of_interest = $request->area_of_interest;
        $cmsObj->achievement = $request->achievement;
        $cmsObj->save();

        if($cmsObj->save()){
            $flag = 1;
        }

        if ( $request->hasFile('file') )
        {
            $reference_type = 'App\Models\Admin\CmsTechnologyDeveloper';
            $prefix = 'cms';
            $cmsObjNew = CmsTechnologyDeveloper::where('cms_technology_id',$request->cms_technology_id)
                                ->where('user_id',$request->user_id)->first();

            $reference_id = $cmsObjNew->id;
            $single_file = $request->file;
            $type = $single_file->extension();
            $fileName = FileService::getFileName($type);
            $originalName = $single_file->getClientOriginalName();
            $upload = FileService::uploadFile($single_file, $fileName, $reference_id, $reference_type, $originalName, $prefix);
        }
        

        if( $flag == 1 ){
            return redirect('/admin/cms-developers')->with('message', 'Successfully Added developer');
        }
        else {
            return redirect::back()->withErrors($cmsObj->getErrors())->withInput();
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cmsTechnologyDeveloper =  CmsTechnologyDeveloper::with(['image' => function($query) {
                            $query->where('reference_type', '=', 'App\Models\Admin\CmsTechnologyDeveloper');
                            }],'user','cms_developer')
                            ->where('id',$id)->first();
        $cmsDevelopers = CmsDeveloper::where('status',1)->get();
        
        if( $cmsDevelopers && $cmsTechnologyDeveloper ){
            
            return View('pages/admin/cms-technology-developers/edit', ['cmsDevelopers' => $cmsDevelopers, 'cmsTechnologyDeveloper' => $cmsTechnologyDeveloper]);
        }
        else {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $flag = 0;

        $validator = Validator::make($request->all(), [
            'cms_technology_id' => 'required',
			'status' => 'required',
            'about' => 'required',
            'area_of_interest' => 'required',
            'achievement' => 'required'
        ]);
        

        if ($validator->fails()) {
            return redirect::back()->withErrors($validator->errors())->withInput($request->input());
        }
        
        $cmsObj = CmsTechnologyDeveloper::find($id);
        $cmsObj->cms_technology_id = $request->cms_technology_id;
        $cmsObj->status = $request->status;
        $cmsObj->about = $request->about;
        $cmsObj->area_of_interest = $request->area_of_interest;
        $cmsObj->achievement = $request->achievement;
        $cmsObj->save();
        
        if($cmsObj->save()){
            $flag = 1;
        }

        if ( $request->hasFile('file') )
        {
            $reference_type = 'App\Models\Admin\CmsTechnologyDeveloper';
            $prefix = 'cms';
            
            $file = File::where('reference_type',$reference_type)->where('reference_id',$id)->first();

            if( isset($file) ){
                $file->delete();
            }

            $reference_id = $id;
            $single_file = $request->file;
            $type = $single_file->extension();
            $fileName = FileService::getFileName($type);
            $originalName = $single_file->getClientOriginalName();
            $upload = FileService::uploadFile($single_file, $fileName, $reference_id, $reference_type, $originalName, $prefix);
        }

        return redirect('admin/cms-developers')->with('message', 'Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cmsDeveloper = CmsDeveloper::find($id);
            if($cmsDeveloper->delete()){
                return redirect('/admin/cms-developers')->with('message','Successfully Deleted');
            }
            elseif(!$cmsDeveloper->delete()){
                return redirect::back()->withErrors($cmsDeveloper->getErrors());
            }
        else {
            abort(404);
        }
    }
    /**
     * Show details of the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cmsObj = CmsDeveloper::find($id);
        if ( isset($cmsObj) ) 
        {
            return View('pages/admin/cms-developers/show', ['cmsObj' => $cmsObj] );    
        }  
        return Redirect::back()->withInput()->withErrors(['Something went wrong']);
    }


    

    


}
