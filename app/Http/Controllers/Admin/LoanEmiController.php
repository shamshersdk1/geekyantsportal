<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\LoanController;
use App\Models\Loan;
use App\Models\LoanEmi;
use App\Models\User;
use App\Services\SlackService\SlackMessageService\Reminder\Loan as LoanService;
use Validator;
use Redirect;

class LoanEmiController extends Controller
{
    public function edit($id)
    {
        $emi = LoanEmi::find($id);
        if ($emi) {
            return View('pages/admin/loan/emiedit', compact('emi'));
        } else {
            abort(404);
        }
    }
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
        'paid_on' => 'required|date',
        'method' => 'required',
        'amount' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
            ->withErrors($validator)
               ->withInput();
        }
        $emi=LoanEmi::find($id);
        $emi->status='paid';
        $emi->paid_on=date_to_yyyymmdd($request->paid_on);
        $emi->method=$request->method;
        $emi->amount=$request->amount;
        $emi->comment=$request->comment;
        $emi->save();
        // LoanService::emiReminder($emi->id);
        return redirect('/admin/loans/'.$emi->loan->id)->with('message', 'Payment details saved Successfully')->with('emi', $emi);
    }
    public function destroy($id)
    {
        $emi=LoanEmi::find($id);
        $emi->status='pending';
        $emi->paid_on=null;
        $emi->method=null;
        $emi->comment=null;
        $emi->save();
        return redirect::back()->with('message', 'Payment cancelled');
    }
    public function newEmi($id)
    {
        $loan = Loan::find($id);
        return view('pages.admin.loan.emi-add', compact('loan'));
    }
    public function storeNewEmi($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
        'pay_date' => 'required|date',
        'method' => 'required',
        'amount' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $loan_emi = new LoanEmi();
        $loan_emi->loan_id = $id;
        $loan_emi->amount = $request->amount;
        $loan_emi->paid_on = $request->pay_date;
        $loan_emi->due_date = $request->pay_date;
        $loan_emi->status = 'paid';
        $loan_emi->method = $request->method;
        $loan_emi->comment = $request->comment;
        $loan_emi->save();
        return redirect::back()->with('message', 'Successfully Added');
    }
}
