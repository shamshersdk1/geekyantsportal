<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Admin\Payslip;
use App\Models\Admin\PayslipMonth;
use App\Models\Admin\PayslipData;
use App\Models\User;
use App\Models\Admin\CmsProject;
use App\Models\Admin\ProfileProject;
use App\Models\Admin\PayrollRuleForCsv;
use App\Models\Admin\PayslipCsvData;
use App\Models\Admin\PayslipCsvMonth;
use Redirect;
use Response;
use View;
use PDF;
use DB;

class PayslipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->user) {
            return Redirect::back()->with('message', 'Invalid Link Followed!');
        }


        $payslips = PayslipMonth::with('user')->orderBy('year', 'DESC')->orderBy('month', 'DESC')->paginate(20);
        return view('pages/admin/payslip/index', [
                                                    'payslips' => $payslips,
                                                    'user'     => $request->user
                                                ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function uploadPayslip(Request $request)
    {
        if (!$request->user) {
            return Redirect::back()->with('message', 'Invalid Link Followed!');
        }


        return view('pages/admin/payslip/upload');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if (!$request->user) {
            return Redirect::back()->with('message', 'Invalid Link Followed!');
        }

        $savePayslipMonth = PayslipMonth::saveData($request);

        if (!$savePayslipMonth['status']) {
            return Redirect::back()->withErrors($savePayslipMonth['log'])->withInput();
        }

        return redirect('/admin/upload-payslip')->with('message', $savePayslipMonth['message']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if (!$request->user) {
            return Redirect::back()->with('message', 'Invalid Link Followed!');
        }

        
        $payslip = PayslipMonth::with('user', 'payslipData')->find($id);
        
        if($payslip && ($payslip->status == 'rejected' || $payslip->status == 'pending')) {
            $payslipData =  json_decode($payslip->json_data, true);
            return view('pages/admin/payslip/csv-data', [
                                                    'payslipData' => $payslipData,
                                                    'payslip' => $payslip,
                                                ]);
        } else {
            return view('pages/admin/payslip/show', ['payslip' => $payslip]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function pendingPayslip(Request $request)
    {
        if (!$request->user) {
            return Redirect::back()->with('message', 'Invalid Link Followed!');
        }

        
        $payslips = PayslipMonth::with('user')->where('status', 'pending')->orderBy('id', 'DESC')->paginate(50);
        return view('pages/admin/payslip/pending', [
                                                    'payslips' => $payslips,
                                                ]);
    }

    public function approvePayslip(Request $request, $id)
    {
        if (!$request->user) {
            return Redirect::back()->with('message', 'Invalid Link Followed!');
        }

        $employeeIds = !empty($_REQUEST['employeeIds'])?$_REQUEST['employeeIds'] : [];
        if (count($employeeIds) <= 0)  {
            return Redirect::back()->withErrors(['No record found to process']);
        }
        //$payslip = PayslipMonth::with('user')->where('status', 'pending')->find($id);
        $payslip = PayslipMonth::with('user')->find($id);

        if (!$payslip || $payslip->status != 'pending') {
            return Redirect::back()->withErrors(['Payslip not found']);
        }

        $userId = $request->user->id;

        $response = PayslipData::processPayslipMonthData($payslip->id, $userId, $employeeIds);

        if(isset($response['status']) && $response['status']) {
            $payslip->status = 'approved';
            $payslip->approved_by=\Auth::id();
            if (!$payslip->save()) {
                return Redirect::back()->withErrors($payslip->getErrors());
            }
        }

        return Redirect::to('/admin/payslip/'.$payslip->id);
    }

    public function rejectPayslip(Request $request, $id)
    {
        if (!$request->user) {
            return Redirect::back()->with('message', 'Invalid Link Followed!');
        }

        
        $payslip = PayslipMonth::with('user')->where('status', 'pending')->find($id);
        if (!$payslip) {
            return Redirect::back()->withErrors(['Payslip not found']);
        }

        if (count($payslip->payslipData) > 0) {
            if (!$payslip->payslipData()->delete()) {
                return Redirect::back()->withErrors(['Something went wrong, and request cannot be completed']);
            }
        }

        $payslip->status = 'rejected';
        $payslip->approved_by = \Auth::id();
        if (!$payslip->save()) {
            return Redirect::back()->withErrors($payslip->getErrors());
        }

        return Redirect::to('/admin/payslip/pending')->with('message', 'Payslip rejected and its data deleted');
    }

    public function userPayslip($month = null, $year = null)
    {
        $user = \Auth::user();
        if (is_null($year) && is_null($month)) {
            $month = date('M').' - '.date('Y');
        } else {
            $month = ucfirst($month);
            $month .= ' - '.$year;
        }
        //print_r($month);die;
        if ($user) {
            $userPayslip = User::whereHas('payslips', function ($q) use ($month) {
                               $q->where('payroll_month', $month);
            })->with(['payslips' => function ($q) use ($month) {
                $q->where('payroll_month', $month);
            }])->where('id', $user->id)->first();

            //echo "<pre>";print_r($userPayslip->toArray());die;
            return view('pages/admin/payslip/user-index')->with('userPayslip', $userPayslip);
        } else {
            return Redirect::back()->withErrors(['User not found'])->withInput();
        }
    }

    public function rejectedPayslip(Request $request)
    {
        if (!$request->user) {
            return Redirect::back()->with('message', 'Invalid Link Followed!');
        }

        
        $payslips = PayslipMonth::with('user')->where('status', 'rejected')->orderBy('id', 'DESC')->paginate(50);

        return view('pages/admin/payslip/rejected', [
                                                    'payslips' => $payslips,
                                                ]);
    }

    public function approvedPayslip(Request $request)
    {
        if (!$request->user) {
            return Redirect::back()->with('message', 'Invalid Link Followed!');
        }

        
        $payslips = PayslipMonth::with('user')->where('status', 'approved')->orderBy('id', 'DESC')->paginate(50);

        return view('pages/admin/payslip/approved', [
                                                    'payslips' => $payslips,
                                                ]);
    }



    public function approvedPayslipView(Request $request, $id)
    {
        //
    }

    public function download(Request $request, $payslipDataId)
    {
        if (!$request->user) {
            return Redirect::back()->with('message', 'Invalid Link Followed!');
        }


        $payslip = PayslipData::find($payslipDataId);
        if (!$payslip) {
            return Redirect::back()->withErrors(['Payslip not found.']);
        }

        $pdf = PDF::loadView('payslip.data', compact('payslip'));
        return $pdf->download('payslip.pdf');
    }

    public function viewPayslip(Request $request, $payslipDataId)
    {
        if (!$request->user) {
            return Redirect::back()->with('message', 'Invalid Link Followed!');
        }

        $payslip = PayslipData::find($payslipDataId);
        if (!$payslip) {
            return Redirect::back()->withErrors(['Payslip not found.']);
        }
        // return view('payslip.data', compact('payslip'));
        $template = $payslip->payslip_month->template ?  $payslip->payslip_month->template : 'jan-2018';
        
        $pdf = PDF::loadView('pages.admin.payslip.template.'.$template, compact('payslip'));
        return $pdf->stream('payslip.pdf', array('Attachment'=>0));
    }

    public function delete(Request $request, $payslipMonthId)
    {
        if (!$request->user) {
            return Redirect::back()->with('message', 'Invalid Link Followed!');
        }


        $payslip = PayslipMonth::find($payslipMonthId);
        if (!$payslip) {
            return Redirect::back()->withErrors(['Payslip not found']);
        }

        if (count($payslip->payslipData) > 0) {
            if (!$payslip->payslipData()->delete()) {
                return Redirect::back()->withErrors(['Something went wrong, and request cannot be completed']);
            }
        }

        if (!$payslip->delete()) {
            return Redirect::back()->withErrors(['Something went wrong, and request cannot be completed']);
        }

        return Redirect::back()->with('message', 'Deleted successfully');
    }

    public function downloadPayslipMonth(Request $request, $payslipMonthId)
    {
        if (!$request->user) {
            return Redirect::back()->with('message', 'Invalid Link Followed!');
        }


        $payslip = PayslipMonth::find($payslipMonthId);
        if (!$payslip) {
            return Redirect::back()->withErrors(['Payslip not found']);
        }
        if (!file_exists($payslip->file_path)) {
            return Redirect::back()->withErrors(['File not found.']);
        }
        $headers = [
                  'Content-Type' => 'application/octet-stream',
                ];

        return Response::download($payslip->file_path, 'payslip.csv', $headers);
        //test here
    }
}
