<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\User;
use App\Models\Admin\SlackAccessLog;

use Redirect;

class SlackAccessLogController extends Controller
{
    public function index()
    {
        $logs=SlackAccessLog::filter();
        return View('pages.admin.slack-access-log.index', compact('logs'));
    }

    public function show(Request $request, $id)
    {
        if (!$request->user) {
            return response("Users only", 401);
        }
        if (!$request->user->isAdmin()) {
            return response("Admins only", 401);
        }
        $log=SlackAccessLog::find($id);
        if (!$log) {
            return response()->json("Record not found", 404);
        }
        return response()->json($log);
    }
}
