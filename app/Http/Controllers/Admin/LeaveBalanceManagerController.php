<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\CalendarYear;
use App\Models\Admin\LeaveBalanceManager;
use App\Models\Admin\LeaveType;
use App\Models\Leave\Leave;
use App\Models\Leave\UserLeaveTransaction;
use App\Models\Month;
use App\Models\User;
use App\Services\Leave\UserLeaveService;
use App\Services\NewLeaveService;
use Auth;
use Excel;
use Illuminate\Http\Request;
use Redirect;

class LeaveBalanceManagerController extends Controller
{
    public function yearList()
    {
        $years = CalendarYear::orderBy('year', 'DESC')->get();

        if (!$years || empty($years)) {
            return redirect('/admin/leave-section/leave-balance-manager')->withErrors('Calendar year data not found');
        }

        return view('pages.admin.leave-balance-manager.index', ['years' => $years]);
    }

    public function userLeaveOverview($yearId, $userId)
    {
        $yearObj = CalendarYear::where('id', $yearId)->first();
        if (!$yearObj) {
            return redirect::to('/admin/leave-section/leave-balance-manager')->withErrors('year not found');
        }

        $user = User::find($userId);
        if (!($user)) {
            return redirect::to('/admin/leave-section/leave-balance-manager')->withErrors('Invalid user #' . $userId);

        }

        $userRemainingLeaveDetail = [];

        $responseRemaining = UserLeaveService::getAllUserRemainingLeaves($yearObj->id, $userId);

        if (!$responseRemaining['status']) {
            return redirect::to('/admin/leave-section/leave-balance-manager')->withErrors($responseRemaining['message']);
        }

        $userRemainingLeaveDetail = $responseRemaining[$userId];

        $leaveCreditDetails = UserLeaveTransaction::where('calendar_year_id', $yearId)->where('user_id', $userId)->orderBy('created_at', 'desc')->get();

        $leaveCredit = [];
        if ($leaveCreditDetails) {
            $leaveCredit = $leaveCreditDetails;
        }

        $leaveBalance = [];
        $leaveBalanceDetails = LeaveBalanceManager::where('user_id', $userId)->orderBy('created_at', 'desc')->get();
        if ($leaveBalanceDetails) {
            $leaveBalance = $leaveBalanceDetails;
        }

        return view('pages.admin.leave-balance-manager.user-leave-overview', compact('user', 'leaveBalance', 'leaveCredit', 'yearObj', 'userRemainingLeaveDetail'));
    }

    public function yearlyOverview($yearId)
    {
        $start_date = null;

        $yearObj = CalendarYear::where('id', $yearId)->first();
        if (!$yearObj) {
            return redirect('/admin/leave-section/leave-balance-manager')->withErrors('year not found');
        }
        $yearStartDate = $yearObj->getFirstDay();
        $yearLastDate = $yearObj->getLastDay();

        //data from datepicker//
        $leaveTypes = LeaveType::get();

        $remainingData = [];
        $responseRemaining = UserLeaveService::getAllUserRemainingLeaves($yearId, null, $start_date, null);
        if (!$responseRemaining['status']) {
            return redirect::to('/admin/leave-section/leave-balance-manager')->withErrors($responseRemaining['message']);
        }

        $remainingData = $responseRemaining;

        $users = User::where('is_active', 1)->orderBy('employee_id', 'ASC')->get();

        return view('pages.admin.leave-balance-manager.yearly-overview', compact('users', 'yearObj', 'leaveTypes', 'remainingData'));
    }

    public function balanceForm($yearId, $userId)
    {
        $yearObj = CalendarYear::where('id', $yearId)->first();
        if (!$yearObj) {
            return redirect::to('/admin/leave-section/leave-balance-manager')->withErrors('year not found');
        }

        $user = User::where('id', $userId)->first();

        $admin = Auth::user();
        if (!$admin->hasRole('admin')) {
            return redirect::to('/admin/leave-section/leave-balance-manager')->withErrors('user not authorised');
        }

        $leaveTypeList = [];
        $leaveTypeList = LeaveType::all();

        return view('pages.admin.leave-balance-manager.balance-form', compact('user', 'yearObj', 'leaveTypeList'));
    }

    public function saveForm(Request $request, $yearId, $userId)
    {
        $yearObj = CalendarYear::where('id', $yearId)->first();
        if (!$yearObj) {
            return redirect::to('/admin/leave-section/leave-balance-manager')->withErrors('year not found');
        }

        $admin = Auth::user();

        $month = date('m');
        $monthObj = Month::where('month', $month)->first();
        if (!$monthObj) {
            return redirect::to('/admin/leave-section/leave-balance-manager')->withErrors('month not found');
        }
        $monthId = $monthObj->id;

        $data = $request->all();
        if (!$data) {
            return redirect::to('/admin/leave-section/leave-balance-manager')->withErrors('data not found');
        }
        $data['user_id'] = $userId;
        $data['calendar_year_id'] = $yearId;
        $data['balanced_by'] = $admin->name;

        try {
            if ($data['days'] == 0) {
                return redirect()->back()->withErrors('Days field cannot be zero !')->withInput();
            }
            $leaveObj = new LeaveBalanceManager;
            $leaveObj::saveData($data, $monthId);

        } catch (Exception $e) {
            return redirect::to('/admin/leave-section/leave-balance-manager')->withErrors('leave not balanced');
        }
        return redirect('/admin/leave-section/leave-balance-manager/' . $yearId . '/' . $userId)->with('message', 'leave balanced succesfully');

    }

    public static function settleUserLeaves($yearId, $userId)
    {
        $userObj = User::find($userId);
        if (!$userObj) {
            return redirect('/admin/leave-section/leave-balance-manager/' . $yearId . '/' . $userId)->withErrors('user not found');
        }
        $response = NewLeaveService::settleLeavesAfterResign($userId);
        if (!$response['status']) {
            return redirect('/admin/leave-section/leave-balance-manager/' . $yearId . '/' . $userId)->withErrors($response);
        }
        return redirect('/admin/leave-section/leave-balance-manager/' . $yearId . '/' . $userId)->with('message', 'leave settled succesfully');
    }

    public static function downloadLeaveOverviewCSV($yearId)
    {
        $yearObj = CalendarYear::where('id', $yearId)->first();
        if (!$yearObj) {
            return redirect('/admin/leave-section/leave-balance-manager')->withErrors('year not found');
        }

        $yearStartDate = $yearObj->getFirstDay();
        $yearLastDate = $yearObj->getLastDay();
        //data from datepicker//
        $formData = $_GET;
        $usedLeaves = UserLeaveTransaction::where('calendar_year_id', $yearObj->id)->get();

        $leaveTypes = LeaveType::get();
        $users = User::where('is_active', 1)->orderBy('employee_id', 'ASC')->get();
        $data = [];
        foreach ($users as $user) {
            $data[$user->id]["Employee_id"] = $user->employee_id;
            $data[$user->id]["Name"] = $user->name;
            $userUsedLeaves = $usedLeaves->where('user_id', $user->id);
            foreach ($leaveTypes as $leaveType) {
                $leave = $userUsedLeaves->where('leave_type_id', $leaveType->id);
                $data[$user->id][$leaveType->title] = "0";
                foreach ($leave as $row) {
                    if (isset($row->days)) {
                        $data[$user->id][$leaveType->title] += $row->days;
                    }
                }
            }

        }
        $filename = "leave_balance_report_year_" . $yearObj->year . "_as_of_" . date('Y-m-d');
        $export_data = $data;
        return Excel::create($filename, function ($excel) use ($export_data) {
            $excel->sheet('mySheet', function ($sheet) use ($export_data) {
                $sheet->fromArray($export_data);
            });
        })->download('csv');
        return Redirect::back()->withErrors(['No data for the given month']);
    }

    public static function downloadUserLeaveTransactionCSV($yearId, $userId)
    {
        $yearObj = CalendarYear::where('id', $yearId)->first();
        if (!$yearObj) {
            return redirect('/admin/leave-section/leave-balance-manager')->withErrors('year not found');
        }
        $userObj = User::find($userId);
        if (!$userObj) {
            return redirect('/admin/leave-section/leave-balance-manager')->withErrors('user not found');
        }
        $data = [];
        $index = 0;
        $leaveTransactions = UserLeaveTransaction::where('user_id', $userObj->id)->where('calendar_year_id', $yearObj->id)->get();
        foreach ($leaveTransactions as $leaveTransaction) {
            $index++;
            $data[$index]['Leave_Type'] = $leaveTransaction->leaveType->title;
            $data[$index]['Days'] = number_format($leaveTransaction->days, 1);
            if ($leaveTransaction->reference_type == "App\Models\Leave\Leave") {
                $data[$index]['Description'] = "Leave Taken";
                $leave = Leave::find($leaveTransaction->reference_type);
                if ($leave) {
                    $data[$index]['Description'] = "Leave Taken from " . $leave->start_date . "to" . $leave->end_date . ".";
                }
            } elseif ($leaveTransaction->reference_type == "App\Models\Admin\LeaveBalanceManager") {
                $data[$index]['Description'] = "Leave Balanced";
            } elseif ($leaveTransaction->reference_type == "App\Models\Leave\LeaveCalendarYear") {
                $data[$index]['Description'] = "Yearly Credit (Pro Rata Basis)";
            } elseif ($leaveTransaction->reference_type == "App\Models\Admin\Bonus") {
                $data[$index]['Description'] = "Bonus Credit";
            }
            $data[$index]['TransactionDate'] = datetime_in_view($leaveTransaction->created_at);
        }
        $filename = "Leave_Transactions_for_" . $userObj->name . "_" . $userObj->employee_id . "_for_" . $yearObj->year . "_as_of_" . date('Y-m-d');
        $export_data = $data;
        return Excel::create($filename, function ($excel) use ($export_data) {
            $excel->sheet('mySheet', function ($sheet) use ($export_data) {
                $sheet->fromArray($export_data);
            });
        })->download('csv');
        return Redirect::back()->withErrors(['No data for the given month']);
    }
}
