<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Leave;
use App\Models\Admin\Calendar;
use App\Models\User;
use App\Services\Leave\UserLeaveService;

class LeaveOverviewController extends Controller
{
    public function index(Request $request)
    {
        $start_date = date('Y-m-d');
        $end_date = date('Y-m-d', strtotime($start_date.'+30 days'));
        if(!empty($request->start_date) && !empty($request->end_date)) {
            $start_date = date('Y-m-d');
            $end_date = date('Y-m-d', strtotime($start_date.'+30 days'));
        }
        $response = UserLeaveService::getLeaveSummary($start_date, $end_date);
        $sum=User::count();
        return view('pages.admin.admin-leave-section.leave-overview.index', ['dataArray' => $response['dataArray'],'sum' => $sum,'dates' => $response['dates'],'count' => $response['count'],'date' => $startDate]);
    }
    public function leaveSummary(Request $request)
    {
        $viewType = 'all';
        $startDate = date('Y-m-d');
        $rmId = NULL;
        $endDate = date('Y-m-d', strtotime($startDate.'+30 days'));

        if(!empty($request->date) && !empty($request->type)) {
            $date = $request->date;
            if($request->type == 'previous') {
                $startDate = date('Y-m-d', strtotime($date.'-30 days'));
                $endDate = $date;                    
            }elseif($request->type == 'next') {
                $startDate = date('Y-m-d', strtotime($date.'+30 days'));
                $endDate = date('Y-m-d', strtotime($date.'+60 days'));                
            }
            
        }
        if(!empty($request->view_type) && $request->view_type == 'team') {
            $loggedInUser = \Auth::User();
            if($loggedInUser)
                $rmId = $loggedInUser->id;
        }
        $response = UserLeaveService::getLeaveSummary($startDate, $endDate, $rmId);
        $sum = User::count();
        $users = User::orderBy('employee_id','DESC')->get();
        
        
        if(!empty($request->view_type)){
            $viewType = $request->view_type;
        }

        return view('pages.admin.admin-leave-section.leave-overview.user-summary', ['dataArray' => $response['dataArray'],'sum' => $sum,'dates' => $response['dates'],'count' => $response['count'],'date' => $startDate,'users' => $users,'viewType' => $viewType]);
    }
}
