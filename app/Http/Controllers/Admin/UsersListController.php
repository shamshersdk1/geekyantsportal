<?php
namespace App\Http\Controllers\Admin;

use App\Events\ReportingManager\RmAssigned;
use App\Events\ReportingManager\RmChanged;
use App\Events\User\UserUpdated;
use App\Http\Controllers\Controller;
use App\Models\Admin\CmsProject;
use App\Models\Admin\Designation;
use App\Models\Admin\PayroleGroup;
use App\Models\Admin\Role;
use App\Models\Admin\Technology;
use App\Models\Profile;
use App\Models\User;
use App\Models\UserDetail;
use App\Models\UserTechnologies;
use App\Services\UserService;
use Auth;
use DB;
use Exception;
use Illuminate\Http\Request;
use Redirect;
use Validator;

class UsersListController extends Controller
{
    public function index()
    {
        $result = User::filter();
        if ($result['status'] == false) {
            return Redirect::back()->withErrors($result['errors']);
        }
        if ($result['search'] != "") {
            return view('pages.userList', ['usersList' => $result['users'], 'search' => $result['search']]);
        } else {
            return view('pages.userList', ['usersList' => $result['users']]);
        }
    }

    public function create()
    {
        $id = Auth::id();
        $leaduser = User::where('id', '!=', $id)->where('is_active', 1)->get();
        $roles = Role::orderBy('id')->where('code', '!=', 'admin')->get();
        $designations = Designation::where('status', 1)->get();
        return view('pages/user/create', compact('role', 'leaduser', 'roles', 'designations'));
    }

    public function store(Request $request)
    {
        if (!$request->user) {
            return Redirect::back()->with('message', 'Invalid Link Followed!');
        }
        $saveNewUser = User::saveData($request->toArray());
        if (!$saveNewUser['status']) {
            return Redirect::back()->withErrors($saveNewUser['log'])->withInput();
        }

        return Redirect('/admin/users')->with('message', 'Successfully Added');
    }

    public function show($id)
    {
        return Redirect('/admin/users/' . $id . '/profile');
    }

    public function edit($id)
    {
        if (isset($id)) {
            $user = User::with('roles')->find($id);
            $leaduser = User::where('id', '!=', $id)->where('is_active', 1)->get();
            if ($user->parent_id) {
                $parentList = User::where('id', $user->parent_id)->first();
            } else {
                $parentList = null;
            }
            $roles = Role::orderBy('id', 'desc')->get();
            if (!$user) {
                return redirect('/admin/users')->withErrors('User not found.');
            }
            $userRolesArray = [];
            if (!empty($roles) && count($roles) > 0) {
                $userRolesArray = [];
                if (count($user->roles) > 0) {
                    foreach ($user->roles as $userRole) {
                        array_push($userRolesArray, $userRole->id);
                    }
                }
            }
            $userDetail = [];
            $detailObj = UserDetail::where('user_id', $id)->first();
            if ($detailObj) {
                $userDetail = $detailObj;
            } else {
                $userDetail = [];
            }
            $profile = Profile::where('user_id', $id)->first();
            $technologies = Technology::all();
            $userTechnologies = UserTechnologies::where('user_id', $id)->join('technologies', 'user_technologies.technology_id', '=', 'technologies.id')->select('technologies.name', 'technologies.id')->get();
            $pgroups = PayroleGroup::get();
            $designations = Designation::where('status', 1)->get();
            if ($user) {
                return view('pages.userDetails', [
                    'user' => $user,
                    'profile' => $profile,
                    'userTechnologies' => $userTechnologies,
                    'technologies' => $technologies,
                    'roles' => $roles,
                    'payrolegroups' => $pgroups,
                    'userRolesArray' => $userRolesArray,
                    'parentList' => $parentList,
                    'leaduser' => $leaduser,
                    'designations' => $designations,
                ]);
            } else {
                $users = User::paginate(3);
                return Redirect::back()->withErrors(['Invalid Id'])->with('userList', $users);
            }
        } else {
            $users = User::paginate(3);
            return Redirect::back()->withErrors(['Id Required'])->with('userList', $users);
        }
    }

    public function update(Request $request, $id)
    {
        $joinDate = (!isset($request->join_date)) ? null : date_to_yyyymmdd($request->join_date);
        $confirmationDate = (!isset($request->confirmation_date) || ($request->confirmation_date == "")) ? null : date_to_yyyymmdd($request->confirmation_date);
        $employeeId = (empty($request->employee_id)) ? null : $request->employee_id;
        $dob = (!isset($request->date_of_birth)) ? null : date_to_yyyymmdd($request->date_of_birth);
        $user = User::find($id);
        $is_billable = ($request->onoffswitch == 'on') ? 1 : 0;
        if (!$user) {
            return redirect('/admin/users')->withErrors(['User not found.']);
        }
        $validator = Validator::make($request->all(), [
            'annual_gross_salary' => 'regex:/^\d*(\.\d{0,2})?$/',
            'variable_qtr' => 'regex:/^\d*(\.\d{0,2})?$/',
            'year_end' => 'regex:/^\d*(\.\d{0,2})?$/',
            'date_of_birth' => 'required|date',
            'join_date' => 'date',
            'confirmation_date' => 'date|before:tomorrow',
            'release_date' => 'date',
        ]);

        if ($validator->fails()) {
            return redirect::back()->withErrors($validator->errors())->withInput();
        }
        if (!empty($request->annual_gross_salary) || !empty($request->variable_qtr) || !empty($request->year_end) || !empty($request->group)) {
            if (empty($request->annual_gross_salary) || empty($request->group)) {
                return redirect::back()->withErrors("In-Hand Salary and Payrole Group required")->withInput();
            }
        }
        DB::beginTransaction();
        try {
            $userId = $user->id;
            $oldRmId = !empty($user->parent_id) ? $user->parent_id : null;
            $newRmId = !empty($request->parent_id) ? $request->parent_id : null;
            $user->name = $request->name;
            $user->joining_date = $joinDate;
            $user->confirmation_date = $confirmationDate;
            $user->employee_id = $employeeId;
            $user->dob = $dob;
            if (!empty($request->email)) {
                $user->email = $request->email;
            }

            $user->release_date = empty($request->release_date) ? null : date_to_yyyymmdd($request->release_date);
            $user->parent_id = !empty($request->parent_id) ? $request->parent_id : null;
            $user->is_billable = $is_billable;
            $user->gender = !empty($request['gender']) ? $request['gender'] : null;
            $user->months = !empty($request['months']) ? $request['months'] : null;
            $user->years = !empty($request['years']) ? $request['years'] : null;
            $user->gender = !empty($request['gender']) ? $request['gender'] : null;
            $user->first_name = !empty($request['first_name']) ? $request['first_name'] : null;
            $user->middle_name = !empty($request['middle_name']) ? $request['middle_name'] : null;
            $user->last_name = !empty($request['last_name']) ? $request['last_name'] : null;
            $user->designation = !empty($request['designation']) ? $request['designation'] : null;

            if (!empty($request->role)) {
                $user->role = $request->role;
            }
            if (!$user->save()) {
                return Redirect::back()->withErrors($user->getErrors())->withInput();
            }

            event(new UserUpdated($userId));
            if ($oldRmId == null && $newRmId != null) {
                event(new RmAssigned($user->id, $newRmId));
            } elseif ($oldRmId != null && $newRmId != null && $oldRmId != $newRmId) {
                event(new RmChanged($userId, $oldRmId, $newRmId));
            } elseif ($oldRmId != null && $newRmId == null) {
                event(new RmChanged($userId, $oldRmId, $newRmId));
            }
            if (!empty($request->levels) && count($request->levels) > 0) {
                $user->roles()->detach();
                foreach ($request->levels as $level) {
                    $user->roles()->attach($level);
                }
            }
            if (!$user->save()) {
                return Redirect::back()->withErrors(['User not updated.']);
            }

            $detailObj = UserDetail::where('user_id', $id)->first();
            if ($detailObj == null) {
                $detailObj = new UserDetail;
                $detailObj->user_id = $id;
            }

            $detailObj->aadhar_no = !empty($request['aadhar_no']) ? $request['aadhar_no'] : null;
            $detailObj->pan = !empty($request['pan']) ? $request['pan'] : null;
            $detailObj->bank_ac_no = !empty($request['bank_ac_no']) ? $request['bank_ac_no'] : null;
            $detailObj->bank_ifsc_code = !empty($request['bank_ifsc_code']) ? $request['bank_ifsc_code'] : null;
            $detailObj->pf_no = !empty($request['pf_no']) ? $request['pf_no'] : null;
            $detailObj->uan_no = !empty($request['uan_no']) ? $request['uan_no'] : null;
            $detailObj->esi_no = !empty($request['esi_no']) ? $request['esi_no'] : null;

            if ($detailObj->save()) {
                DB::commit();
                return Redirect::back()->with('message', 'Successfully updated');
            } else {
                throw new Exception('User Detail not saved');
            }

        } catch (Exception $e) {
            DB::rollback();
            return Redirect::back()->withErrors($e->getMessage());
        }
        return Redirect::back()->with('message', 'Successfully updated');
    }

    public function toggle($id)
    {
        if (isset($id)) {
            $user = User::find($id);

            if ($user->is_active == 1) {
                $response = UserService::deactivatePreCheck($id);
                if ($response['status'] == false) {
                    return Redirect::back()->withErrors($response['error']);
                }

            }

            $toggle = User::where('id', $id)->update(['is_active' => !$user->is_active]);
            $profile = Profile::where('user_id', $id)->update(['is_active' => !$user->is_active]);
            return Redirect::back()->with('message', 'Access permissions successfully changed.');

        } else {
            $users = User::paginate(3);
            return Redirect::back()->withErrors(['Access permissions Failed'])->with('userList', $users);
        }
    }

    public function destroy($id)
    {
        if (isset($id)) {
            $user = User::find($id)->delete();
            //return $user;
            return redirect('/admin/users')->with('message', 'successfully deleted.');
        } else {
            $users = User::paginate(3);
            return Redirect::back()->withErrors(['Delete Failed'])->with('userList', $users);
        }
    }

    public function deleteTechnology($techid, $userid)
    {

        if (isset($techid) && isset($userid)) {
            $technology = UserTechnologies::where('technology_id', $techid)->where('user_id', $userid)->get();
            if ($technology) {
                // echo $techid.",".$technologyid;
                $technology = UserTechnologies::where('technology_id', $techid)->where('user_id', $userid)->delete();
                if ($technology) {
                    return redirect::back()->with('message', 'Technology Deleted');
                }
                return redirect::back()->withErrors('unable to Deleted');
            } else {
                abort(404);
            }
        }
    }
    public function addTechnology(Request $request, $id)
    {

        if (count($request->technologies) > 0) {
            $user = User::find($id);
            if ($user) {
                if (count($request->technologies) > 0) {
                    if (count($user->technologies) > 0) {
                        $user->technologies()->detach();
                    }
                    foreach ($request->technologies as $technology) {
                        $user->technologies()->attach($technology);
                    }
                    return redirect::back()->with('message', 'Successfully Technology Added');
                }
            } else {
                abort(404);
            }
        } else {
            return redirect::back()->withErrors("No Technology selected");
        }
    }

    public function addUserProfile($id)
    {

        $user = User::find($id);
        $projects = CmsProject::all();

        if ($user) {
            return view('pages/admin/profile/create')->with('user', $user)->with('projects', $projects);
        } else {
            return view('pages/admin/profile/create')->with('projects', $projects);
        }
    }

    public function showUser($id)
    {

        $user = User::with('profile')->find($id);
        if ($user) {
            return view('pages/user/edit')->with('user', $user);
        }

        return redirect::back()->withErrors(["User does not exists"]);
    }

    public function updateUser(Request $request, $id)
    {

        $user = User::with('profile')->find($id);

        if ($user) {
            if (isset($user->profile)) {
                $profileId = $user->profile->id;

                $profile = Profile::find($profileId);

                if ($profile) {
                    $profile->github = isset($request->github) ? $request->github : null;
                    $profile->stack_overflow = isset($request->stack_overflow) ? $request->stack_overflow : null;
                    $profile->phone = isset($request->phone) ? $request->phone : null;

                    if ($request->image) {
                        $path = public_path('uploads/images/team');
                        // $image = $request->file('image')->getClientOriginalName();
                        $extensionType = $request->file('image')->getClientOriginalExtension();
                        $new_path = time() . '.' . $extensionType;

                        $request->file('image')->move($path, $new_path);

                        $profile->image = "/uploads/images/team/" . $new_path;
                    }
                    if ($profile->save()) {
                        return redirect::back()->withSuccess(["Successfully updated"]);
                    } else {
                        return redirect::back()->withErrors(["Could not save in profile table"]);
                    }
                } else {
                    return redirect::back()->withErrors(["User Profile does not exists"]);
                }
            } else {
                return redirect::back()->withErrors(["User Profile does not exists"]);
            }
        } else {
            return redirect::back()->withErrors(["User does not exists"]);
        }
    }

    public function search(Request $request)
    {
        if (!$request->user) {
            return Redirect::back()->with('message', 'Invalid Link Followed!');
        }

        if (!$request->user->isAdmin()) {
            return redirect('/');
        }

        if (empty($request->search)) {
            return redirect('/admin/users');
        }

        $search = $request->search;
        $users = User::where('email', 'LIKE', '%' . $search . '%')->orwhere('name', 'LIKE', '%' . $search . '%')->orwhere('id', $search)->paginate(10);

        return view('pages.userList', [
            'usersList' => $users,
            'search' => $search,
        ]);
    }
    public function toggleBillable($id)
    {
        if (isset($id)) {
            $user = User::find($id);
            $toggle = User::where('id', $id)->update(['is_billable' => !$user->is_billable]);
            return redirect('/admin/users')->with('message', 'Billing status successfully changed.');
        } else {
            $users = User::paginate(3);
            return Redirect::back()->withErrors(['Access permissions Failed'])->with('userList', $users);
        }
    }
    public function getReportingManagers()
    {
        $managerIds = User::where('is_active', 1)->where('parent_id', '!=', 0)->whereNotNull('parent_id')->groupBy('parent_id')->pluck('parent_id')->toArray();
        $managers = User::with('reportees')->whereIn('id', $managerIds)->where('is_active', 1)->orderBy('employee_id', 'ASC')->get();

        return view('pages/admin/reporting-managers/index', ['managers' => $managers]);
    }

}
