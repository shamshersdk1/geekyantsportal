<?php

namespace App\Http\Controllers\Admin;

use App\Events\Project\AccountManagerChanged;
use App\Events\Project\BdManagerChanged;
use App\Events\Project\DeveloperAssigned;
use App\Events\Project\DeveloperRemoved;
use App\Events\Project\ProjectManagerChanged;
use App\Events\Timesheet\SlackChannelLinked;
use App\Http\Controllers\Controller;
use App\Models\Admin\Client;
use App\Models\Admin\Cron;
use App\Models\Admin\Project;
use App\Models\Admin\ProjectCategory;
use App\Models\Admin\ProjectFiles;
use App\Models\Admin\ProjectNotes;
use App\Models\Admin\ProjectResource;
use App\Models\Admin\ProjectSlackChannel;
use App\Models\Admin\ProjectSprints;
use App\Models\Admin\ResourcePrice;
use App\Models\Admin\SprintResource;
use App\Models\Admin\Technology;
use App\Models\Admin\Timelog;
use App\Models\TimesheetDetail;
use App\Models\User;
use App\Models\UserRole;
use App\Services\ProjectService;
use App\Services\ResourceService;
use App\Services\SlackService\SlackApiServices;
use App\Services\TimesheetService;
use App\Services\UserService;
use Auth;
use DB;
use Exception;
use Illuminate\Http\Request;
use Input;
use Redirect;
use Storage;
use Config;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = Project::filter();
        $result_final = ProjectService::currentAllocatedResources($result);

        return View('pages/admin/project/index', ['projects' => $result_final['projects'], 'search' => $result_final['search'], 'active_users' => $result_final['active_users']]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createNew()
    {
        $publicChannels = SlackApiServices::getPublicChannels();
        $privateChannels = SlackApiServices::getPrivateChannels();

        $users = User::where('is_active', 1)->get();
        isset($users) ? $users : "";
        $companies = Client::all();
        $categories = ProjectCategory::all();
        $selected_channels = [];
        return View('pages/admin/project/add', ['companies' => $companies, 'users' => $users, 'publicChannels' => $publicChannels, 'privateChannels' => $privateChannels, 'categories' => $categories, 'selected_channels' => $selected_channels]);
    }
    public function create()
    {
        $clientList = Client::all();
        $projectCategories = ProjectCategory::all();
        $slackPublicChannels = SlackApiServices::getPublicChannels();
        $allPrivateChannels = SlackApiServices::getPrivateChannels();
        $slackPrivateChannels = [];

        $salesExecutiveList = UserService::getSalesExecutiveList();
        $projectCoordinatorList = UserService::getProjectCoordinatorList();
        $projectManagerList = UserService::getProjectManagerList();
        $codeLeadList = UserService::getAllActiveUserList();
        $deliveryLeadList = UserService::getAllActiveUserList();
        $allActiveUserList = UserService::getAllActiveUserList();
        $antManagerId = Config::get('slack.ant_manager_id');
        foreach ($allPrivateChannels as $slackPrivateChannel) {
            $lastThreeChar = substr($slackPrivateChannel->name, -3);
            if ($lastThreeChar == "-bd") {
                $slackPrivateChannels[] = $slackPrivateChannel;
            }
        }
        return View('pages/admin/project/add-new', ['clientList' => $clientList,
            'projectCategories' => $projectCategories,
            'slackPublicChannels' => $slackPublicChannels,
            'slackPrivateChannels' => $slackPrivateChannels,
            'salesExecutiveList' => $salesExecutiveList,
            'projectCoordinatorList' => $projectCoordinatorList,
            'projectManagerList' => $projectManagerList,
            'codeLeadList' => $codeLeadList,
            'deliveryLeadList' => $deliveryLeadList,
            'allActiveUserList' => $allActiveUserList,
            'antManagerId' => $antManagerId,
        ]);
    }

    public function edit($id)
    {
        $selected_public_channels = [];
        $selected_private_channels = [];
        $project = Project::with('projectLeads', 'client','projectSlackChannels')->find($id);
        
        $clientList = Client::all();
        $projectCategories = ProjectCategory::all();
        $slackPublicChannels = SlackApiServices::getPublicChannels();
        $allPrivateChannels = SlackApiServices::getPrivateChannels();
        $slackPrivateChannels = [];
        foreach ($allPrivateChannels as $slackPrivateChannel) {
            $lastThreeChar = substr($slackPrivateChannel->name, -3);
            if ($lastThreeChar == "-bd") {
                $slackPrivateChannels[] = $slackPrivateChannel;
            }
        }
        $salesExecutiveList = UserService::getSalesExecutiveList();
        $projectCoordinatorList = UserService::getProjectCoordinatorList();
        $projectManagerList = UserService::getProjectManagerList();
        $codeLeadList = UserService::getAllActiveUserList();
        $deliveryLeadList = UserService::getAllActiveUserList();
        $allActiveUserList = UserService::getAllActiveUserList();
        $antManagerId = Config::get('slack.ant_manager_id');
        $publicSlackChannels = ProjectSlackChannel::where('project_id', $id)->where('type', 'public')->get();
        if (count($publicSlackChannels) > 0) {
            foreach ($publicSlackChannels as $publicSlackChannel) {
                // dd($publicSlackChannel);
                $temp = [];
                $temp['id'] = $publicSlackChannel->slack_id;
                $res = SlackApiServices::getPublicChannelName($publicSlackChannel->slack_id);
                $temp['name'] = $res['data'];
                $selected_public_channels[] = $temp;
            }
        }
        $privateSlackChannels = ProjectSlackChannel::where('project_id', $id)->where('type', 'private')->get();
        if (count($privateSlackChannels) > 0) {
            foreach ($privateSlackChannels as $privateSackChannel) {
                $temp = [];
                $temp['id'] = $privateSackChannel->slack_id;
                $res = SlackApiServices::getPrivateChannelName($privateSackChannel->slack_id);
                $temp['name'] = $res['data'];
                $selected_private_channels[] = $temp;
            }
        }

        return View('pages/admin/project/edit-new', ['clientList' => $clientList,
            'projectCategories' => $projectCategories,
            'slackPublicChannels' => $slackPublicChannels,
            'slackPrivateChannels' => $slackPrivateChannels,
            'salesExecutiveList' => $salesExecutiveList,
            'projectCoordinatorList' => $projectCoordinatorList,
            'projectManagerList' => $projectManagerList,
            'codeLeadList' => $codeLeadList,
            'deliveryLeadList' => $deliveryLeadList,
            'allActiveUserList' => $allActiveUserList,
            'project' => $project,
            'selected_public_channels' => $selected_public_channels,
            'selected_private_channels' => $selected_private_channels,
            'antManagerId' => $antManagerId,
        ]);
    }

    public function createConvert($id)
    {
        $users = User::where('is_active', 1)->get();
        $publicChannels = SlackApiServices::getPublicChannels();
        $privateChannels = SlackApiServices::getPrivateChannels();
        if ($id) {
            $company = Client::find($id);
        }
        isset($company) ? $company : "";
        $companies = Client::all();
        return View('pages/admin/project/add', ['company' => $company, 'users' => $users, 'publicChannels' => $publicChannels, 'privateChannels' => $privateChannels]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $data = $request->all();
        if (empty($data)) {
            return $this->failResponse('Bad request', 400, ['Empty data sent']);
        } else {
            $res = ProjectService::saveData($data);

            if ($res['status']) {
                return redirect('/admin/project')->with('message', $res['message']);
            } else {
                return redirect::back()->withErrors($res['message'])->withInput();
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editNew($id)
    {
        $selected_channels = [];
        $project = Project::find($id);

        $selected_public_channels = ProjectSlackChannel::where('project_id', $id)->where('type', 'public')->get()->toArray();
        $selected_private_channels = ProjectSlackChannel::where('project_id', $id)->where('type', 'private')->get()->toArray();

        if ($project) {
            $publicChannels = SlackApiServices::getPublicChannels();
            $privateChannels = SlackApiServices::getPrivateChannels();
            $users = User::where('is_active', 1)->get();
            isset($users) ? $users : "";
            $companies = Client::all();
            $categories = ProjectCategory::all();

            return View('pages/admin/project/edit', ['companies' => $companies, 'users' => $users, 'project' => $project, 'publicChannels' => $publicChannels, 'privateChannels' => $privateChannels, 'categories' => $categories, 'selected_public_channels' => $selected_public_channels, 'selected_private_channels' => $selected_private_channels]);
        } else {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $project = Project::find($id);
        if ($project) {
            if ($request->project_manager_id) {
                $find = User::where('id', $request->project_manager_id)->first();
                $managerName = $find->name;
            } else {
                $managerName = " ";
            }
            if (!empty($request->account_manager_id) && $request->account_manager_id != $project->account_manager_id) {
                event(new AccountManagerChanged($project->account_manager_id, $request->account_manager_id, $project->id));
            }
            if (!empty($request->bd_manager_id) && $request->bd_manager_id != $project->bd_manager_id) {
                event(new BdManagerChanged($project->bd_manager_id, $request->bd_manager_id, $project->id));
            }
            if (!empty($request->project_manager_id) && $request->project_manager_id != $project->project_manager_id) {
                event(new ProjectManagerChanged($project->project_manager_id, $request->project_manager_id, $project->id));
            }

            $project->project_name = $request->project_name;
            $project->account_manager_id = !empty($request->account_manager_id) ? $request->account_manager_id : null;
            $project->project_manager = $managerName;
            $project->bd_manager_id = !empty($request->bd_manager_id) ? $request->bd_manager_id : null;
            $project->channel_for_developer = $request->channel_for_developer;
            $project->channel_for_manager = $request->channel_for_manager;
            $project->company_id = $request->company_id;
            $project->status = $request->status;
            $project->notes = $request->notes;
            $project->project_category_id = $request->project_category;
            $project->project_manager_id = !empty($request->project_manager_id) ? $request->project_manager_id : null;
            if ($request->start_date) {
                $project->start_date = date_to_yyyymmdd($request->start_date);
            } else {
                $project->start_date = date("Y-m-d", strtotime($request->start_date));
            }
            if ($request->end_date) {
                if (strtotime($request->start_date) > strtotime($request->end_date)) {
                    return Redirect::back()->withErrors(['End Date can\'t be greater than start date'])->withInput();
                }
                $project->end_date = date_to_yyyymmdd($request->end_date);
            } else {
                $project->end_date = null;
            }
            if ($project->save()) {
                $message = 'Successfully Updated';
                if ($project->project_manager_id != null) {
                    $manager = $project->projectManager;
                    if (!$manager->hasRole('team-lead')) {
                        if (!UserRole::createUserRole($manager->id, 'team-lead')) {
                            $message = $message . " but unable to assign role to the team lead";
                        }
                    }
                }
                $flag = 0;
                if (isset($request->channels)) {
                    $temp = ProjectSlackChannel::where('project_id', $id)->where('type', 'public')->delete();
                    foreach ($request->channels as $channel) {
                        $exists = ProjectSlackChannel::where('slack_id', $channel)->count();
                        if (empty($exists) || $exists == 0) {
                            $slackChannel = new ProjectSlackChannel();
                            $slackChannel->project_id = $project->id;
                            $slackChannel->slack_id = $channel;
                            $slackChannel->save();
                            $tmpTableUpdateNeeded = TimesheetService::isChannelPresent($channel);
                            if ($tmpTableUpdateNeeded) {
                                event(new SlackChannelLinked($channel));
                            }
                        } else {
                            $flag = 1;
                        }
                    }
                }
                if (isset($request->private_channels)) {
                    $temp = ProjectSlackChannel::where('project_id', $id)->where('type', 'private')->delete();
                    foreach ($request->private_channels as $private_channel) {
                        $exists = ProjectSlackChannel::where('slack_id', $private_channel)->count();
                        if (empty($exists) || $exists == 0) {
                            $slackChannel = new ProjectSlackChannel();
                            $slackChannel->project_id = $project->id;
                            $slackChannel->slack_id = $private_channel;
                            $slackChannel->type = 'private';
                            $slackChannel->save();
                        } else {
                            $flag = 1;
                        }
                    }
                }
                if ($flag == 1) {
                    $message = $message . " but some channels are associated with other projects hence, were not added";
                }
                return redirect::back()->with('message', $message);
            } else {
                return redirect::back()->withErrors($project->getErrors());
            }
        } else {
            abort(404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project = Project::find($id);
        $allocated = ProjectResource::where('project_id', '=', $id)->get()->count();

        if ($project) {
            // if(count($project->projectSprints) > 0){
            //     $project->projectSprints->delete();
            // }
            if (count($project->projectNotes) > 0) {
                $notes = $project->projectNotes;
                foreach ($notes as $note) {
                    $note->delete();
                }
            }
            if (count($project->projectSprints) > 0) {
                $sprints = $project->projectSprints;
                foreach ($sprints as $sprint) {
                    $sprint->delete();
                }
            }
            if (count($project->projectFiles) > 0) {
                $files = $project->projectFiles;
                foreach ($files as $file) {
                    $file->delete();
                }
            }
            if (count($project->Timelog) > 0) {
                $timelogs = $project->Timelog;
                foreach ($timelogs as $timelog) {
                    $timelog->delete();
                }
            }
            if (count($project->technologies) > 0) {
                $project->technologies()->detach();
            }
            if ($allocated > 0) {
                return redirect::back()->with('message', 'Can not delete the project. Reference to project resources found. Kindly proceed with closing the project.');
            }
            if ($project->delete()) {
                return redirect('/admin/project')->with('message', 'Successfully Deleted');
            } elseif (!$project->delete()) {
                return redirect::back()->withErrors($project->getErrors());
            }
        } else {
            abort(404);
        }
    }
    public function viewDashboard($id)
    {

        $user = Auth::user();
        $giveAccess = false;
        if ($user->hasRole('admin') || $user->hasRole('account')) {
            $giveAccess = true;
        }
        if ($user->isAdmin()) {
            $projectNotes = ProjectNotes::where('project_id', $id)->get();
            $projectFiles = ProjectFiles::where('project_id', $id)->get();
        } else {
            $projectNotes = ProjectNotes::where('project_id', $id)->where('private_to_admin', 0)->get();
            $projectFiles = ProjectFiles::where('project_id', $id)->where('private_to_admin', 0)->get();
        }
        $projectinfo = Project::with('timesheets')->find($id);
        $companies = $projectinfo->companies;
        $contacts = $companies->contacts->where('primary', 1);
        $technologies = Technology::all();
        $projectTechnologies = $projectinfo->technologies;
        $projectResources = $projectinfo->resources;
        $users = User::where('is_active', 1)->get();
        $projectData = ProjectResource::with('user_data')->with('resource_type')->where('project_id', $id)->orderByRaw('ISNULL(end_date) DESC,  end_date DESC ')->orderBy('start_date', 'DESC')->get();
        $start_date = Input::get('start_date');
        $end_date = Input::get('end_date');
        $timesheets = $projectinfo->timesheets();
        $dates = ['start_date' => " - ", 'end_date' => " - "];
        if (!empty($start_date)) {
            $timesheets = $timesheets->where('date', '>=', $start_date);
            $dates['start_date'] = date("j F, Y", strtotime($start_date));
        }
        if (!empty($end_date)) {
            $timesheets = $timesheets->where('date', '<=', $end_date);
            $dates['end_date'] = date("j F, Y", strtotime($end_date));
        }
        $timesheets = $timesheets->pluck('id')->toArray();
        $resource_price = ResourcePrice::where('project_id', $id)->get();
        $details = TimesheetDetail::with('timesheet')->whereIn('timesheet_id', $timesheets)->orderBy('created_at', 'desc')->paginate(10);
        $billingScheduleReminder = Cron::where('reference_id', $id)->where('reference_type', 'App\Models\Admin\BillingScheduleReminder')->first();

        return View('pages/admin/project/show', ['projectinfo' => $projectinfo, 'projectNotes' => $projectNotes,
            'projectFiles' => $projectFiles, 'companies' => $companies, 'contacts' => $contacts,
            'technologies' => $technologies, 'projectTechnologies' => $projectTechnologies,
            'user' => $user, 'resources' => $users, 'projectResources' => $projectResources,
            'projectData' => $projectData, 'giveAccess' => $giveAccess, 'timesheets' => $details,
            'dates' => $dates, 'resource_price' => $resource_price, 'billingScheduleReminder' => $billingScheduleReminder]);

    }
    public function viewDashboardNew($id)
    {

        $user = Auth::user();
        $giveAccess = false;
        if ($user->hasRole('admin') || $user->hasRole('account')) {
            $giveAccess = true;
        }
        if ($user->isAdmin()) {
            $projectNotes = ProjectNotes::where('project_id', $id)->get();
            $projectFiles = ProjectFiles::where('project_id', $id)->get();
        } else {
            $projectNotes = ProjectNotes::where('project_id', $id)->where('private_to_admin', 0)->get();
            $projectFiles = ProjectFiles::where('project_id', $id)->where('private_to_admin', 0)->get();
        }
        $projectinfo = Project::with('timesheets')->find($id);
        $companies = $projectinfo->companies;
        $contacts = $companies->contacts->where('primary', 1);
        $technologies = Technology::all();
        $projectTechnologies = $projectinfo->technologies;
        $projectResources = $projectinfo->resources;
        $users = User::where('is_active', 1)->get();
        $projectData = ProjectResource::with('user_data')->with('resource_type')->where('project_id', $id)->orderByRaw('ISNULL(end_date) DESC,  end_date DESC ')->orderBy('start_date', 'DESC')->get();
        $start_date = Input::get('start_date');
        $end_date = Input::get('end_date');
        $timesheets = $projectinfo->timesheets();
        $dates = ['start_date' => " - ", 'end_date' => " - "];
        if (!empty($start_date)) {
            $timesheets = $timesheets->where('date', '>=', $start_date);
            $dates['start_date'] = date("j F, Y", strtotime($start_date));
        }
        if (!empty($end_date)) {
            $timesheets = $timesheets->where('date', '<=', $end_date);
            $dates['end_date'] = date("j F, Y", strtotime($end_date));
        }
        $timesheets = $timesheets->pluck('id')->toArray();
        $resource_price = ResourcePrice::where('project_id', $id)->get();
        $details = TimesheetDetail::with('timesheet')->whereIn('timesheet_id', $timesheets)->orderBy('created_at', 'desc')->paginate(10);
        return View('pages/admin/project/show-new', ['projectinfo' => $projectinfo, 'projectNotes' => $projectNotes,
            'projectFiles' => $projectFiles, 'companies' => $companies, 'contacts' => $contacts,
            'technologies' => $technologies, 'projectTechnologies' => $projectTechnologies,
            'user' => $user, 'resources' => $users, 'projectResources' => $projectResources,
            'projectData' => $projectData, 'giveAccess' => $giveAccess, 'timesheets' => $details,
            'dates' => $dates, 'resource_price' => $resource_price]);

    }

    public function addComment(Request $request, $id)
    {

        $data = $request->all();
        if (isset($request->private_to_admin)) {
            $data['private_to_admin'] = 1;
            // $projectNotes->private_to_admin = 1;
        } else {
            $data['private_to_admin'] = 0;
        }
        $data['user_name'] = Auth::user()->name;
        $data['project_id'] = $id;

        $result = ProjectNotes::saveData($data);
        if ($result['status']) {
            return redirect('/admin/project/' . $id . '/dashboard')->with('message', 'Successfully Comment Added');
        } else {
            return redirect::back()->withErrors($result['errors'])->withInput();
        }
    }

    public function showComment($id)
    {
        $projectComment = ProjectNotes::find($id);
        return View('/admin/project/' . $id . '/dashboard', ['projectComment' => $projectComment]);
    }

    public function closeSprint($id)
    {
        if (isset($id)) {
            $projectSprints = ProjectSprints::find($id);
            $projectId = $projectSprints->project_id;
            $projectSprints->status = 'closed';
            if ($projectSprints->save()) {
                return redirect::back()->with('message', 'Successfully closed');
            }

            return redirect::back()->withErrors('unable to close');
        }

        return redirect::back()->withErrors('sprint Not found');
    }

    public function showSprints($id)
    {

        $projectSprints = ProjectSprints::find($id);
        if (isset($projectSprints)) {
            return View('/admin/project/' . $id . '/dashboard', ['projectSprints' => $projectSprints]);
        }

        return View('/admin/project/' . $id . '/dashboard', ['projectSprints' => null]);
    }

    public function addSprint($id)
    {

        $count = ProjectSprints::where('project_id', $id)->count();
        $count++;
        $users = User::all();
        // echo"<pre>";
        // print_r($users);
        // die;

        $projectinfo = Project::find($id);
        $technologies = $projectinfo->technologies;
        return View('pages/admin/project/add-sprint', ['lastSprintID' => $count, 'projectId' => $id, 'projectinfo' => $projectinfo, 'users' => $users, 'technologies' => $technologies]);
    }

    public function editSprint($id)
    {

        $users = User::all();
        $project_id = ProjectSprints::find($id)->project_id;
        $projectinfo = Project::find($project_id);
        $technologies = $projectinfo->technologies;
        return View('pages/admin/project/edit-sprint', ['lastSprintID' => $id, 'projectId' => $id, 'projectinfo' => $projectinfo, 'users' => $users, 'technologies' => $technologies]);
    }

    public function destroySprint($id)
    {
        if (isset($id)) {
            $sprint = ProjectSprints::find($id);
            if ($sprint) {
                if ($sprint->status == "open") {
                    if ($sprint->delete()) {
                        return redirect::back()->with('message', 'Successfully deleted');
                    } else {
                        return redirect::back()->withErrors($sprint->getErrors());
                    }

                } else {
                    return redirect('/admin/project')->withErrors("sprint is already closed");
                }

            } else {
                return redirect::back()->withErrors($sprint->getErrors());
            }

        } else {
            return redirect('/admin/project')->withErrors("sprint Not found");
        }

    }

    public function storeSprint(Request $request, $id)
    {

        if ($request->action == 'cancelandgotodashboard') {
            return redirect('/admin/project/' . $id . '/dashboard');
        }

        if (($request->start_date == null) || ($request->end_date == null)) {
            return back()->withErrors("Start date and End date is required")->withInput();
        }

        $checked_user = $request->user;
        $hrs = $request->hrs;

        $projectSprints = new ProjectSprints();
        $projectSprints->title = $request->sprint_title;
        $projectSprints->project_id = $request->project_id;
        $projectSprints->description = $request->sprint_desc;
        $projectSprints->status = 'open';
        $projectSprints->start_date = date("Y-m-d", strtotime($request->start_date));
        $projectSprints->end_date = date("Y-m-d", strtotime($request->end_date));
        if ($projectSprints->save()) {

            if (isset($checked_user)) {
                foreach ($checked_user as $key => $userId) {

                    $resourceObj = new SprintResource();
                    $resourceObj->sprint_id = $projectSprints->id;
                    $resourceObj->employee_id = $userId;
                    $resourceObj->hours = $hrs[$userId];
                    if ($resourceObj->save()) {
                        // return redirect::back()->with('message','Successfully Added ');
                    } else {
                        return back()->withErrors($resourceObj->getErrors())->withInput();

                    }

                }

                if ($request->action == 'save') {
                    return redirect::back()->with('message', 'Successfully Added ');
                } else {
                    return redirect('/admin/project/' . $id . '/dashboard')->with('message', 'Successfully Added');
                }

            } else {
                if ($request->action == 'save') {
                    return redirect::back()->with('message', 'Successfully Added and No user has been assigned');
                } else {
                    return redirect('/admin/project/' . $id . '/dashboard')->with('message', 'Successfully Added and No user has been assigned');
                }

            }
            // return redirect('/admin/project')->with('message','Successfully Deleted');
        } else {
            return back()->withErrors($projectSprints->getErrors())->withInput()->withInput();
            // return redirect::back()->withErrors($project->getErrors());
        }

        // return View('pages/admin/project/add-sprint');
    }

    public function sprintView()
    {
        $sprintId = 43;
        $projectSprints = ProjectSprints::where('id', $id)->get();

        $projectinfo = Project::find($projectSprints->project_id);
        $companies = $projectinfo->companies;
        $contacts = $companies->contacts->where('primary', 1);
        $technologies = Technology::all();
        $projectTechnologies = $projectinfo->technologies;

        $projectSprints = ProjectSprints::where('id', $id)->get();

        $timelogs = Timelog::with('user')->where('project_id', $projectSprints->project_id)->where('current_date', '<', $projectSprints->end_date)->where('current_date', '>', $projectSprints->start_date)->orderBy('id', 'desc')->take(10)->get();

        // $timelogs = $projectinfo->Timelog->sortByDesc('id')->take(3);
        // echo "<pre>";
        // print_r($timelogs->toArray());
        // die;

        return View('pages/admin/project/show', ['projectinfo' => $projectinfo, 'projectNotes' => $projectNotes, 'projectSprints' => $projectSprints, 'projectFiles' => $projectFiles, 'companies' => $companies, 'contacts' => $contacts, 'technologies' => $technologies, 'projectTechnologies' => $projectTechnologies, 'timelogs' => $timelogs]);

    }

    public function updateSprint(Request $request, $id)
    {

        if ($request->action == 'cancelandgotodashboard') {
            return redirect('/admin/project/' . $request->project_id . '/dashboard');
        }

        $checked_user = $request->user;
        $hrs = $request->hrs;

        $projectSprints = ProjectSprints::find($id);

        $projectSprints->title = $request->sprint_title;
        $projectSprints->project_id = $request->project_id;
        $projectSprints->description = $request->sprint_desc;
        $projectSprints->start_date = date("Y-m-d", strtotime($request->start_date));
        $projectSprints->end_date = date("Y-m-d", strtotime($request->end_date));
        if ($projectSprints->update()) {
            // echo "hello";
            // die;
            $deleteAllocatedUsers = SprintResource::where('sprint_id', $id)->delete();

            if (isset($checked_user)) {
                foreach ($checked_user as $key => $userId) {

                    $resourceObj = new SprintResource();

                    $resourceObj->sprint_id = $id;
                    $resourceObj->employee_id = $userId;
                    $resourceObj->hours = $hrs[$userId];

                    if ($resourceObj->save()) {

                        //return redirect::back()->with('message','Successfully Added ');
                    } else {
                        return back()->withErrors($resourceObj->getErrors())->withInput();

                    }

                }

                if ($request->action == 'save') {
                    return redirect::back()->with('message', 'Successfully Added ');
                } else {
                    return redirect('/admin/project/' . $request->project_id . '/dashboard')->with('message', 'Successfully Updated');
                }

            } else {
                if ($request->action == 'save') {
                    return redirect::back()->with('message', 'Successfully Updated and No user has been assigned');
                } else {
                    return redirect('/admin/project/' . $request->project_id . '/dashboard')->with('message', 'Successfully Updated and No user has been assigned');
                }

            }
            // return redirect('/admin/project')->with('message','Successfully Deleted');
        } else {
            return back()->withErrors($projectSprints->getErrors())->withInput()->withInput();
            // return redirect::back()->withErrors($project->getErrors());
        }

        // return View('pages/admin/project/add-sprint');
    }

    public function uploadFile(Request $request, $id)
    {
        if ($request->file('project_file')) {
            $project = Project::find($id);
            if ($project) {
                $projectFile = new ProjectFiles();
                if ($request->hasFile('project_file')) {
                    $targetFile = storage_path('app/uploads/project-files');
                    $fileName = $request->file('project_file')->getClientOriginalName();
                    $projectFile->project_id = $project->id;
                    $projectFile->path = $fileName;
                    if (isset($request->private_to_admin)) {
                        $projectFile->private_to_admin = 1;
                    } else {
                        $projectFile->private_to_admin = 0;
                    }
                    if ($projectFile->save()) {
                        $request->file('project_file')->move($targetFile, $fileName);
                        return redirect::back()->with('message', 'Successfully File Uploaded');
                    } else {
                        // echo "else";
                        // die;
                        return redirect::back()->withErrors($projectFile->getErrors());
                    }

                }
            } else {
                abort(404);
            }
        } else {
            return redirect::back()->withErrors("No File choosen to Upload");
        }
        // print_r($request->all());
        // echo $id;
        // die;
    }

    public function addTechnology(Request $request, $id)
    {
        // print_r($request->all());
        // die;
        if (count($request->technologies) > 0) {
            $project = Project::find($id);
            if ($project) {
                if (count($request->technologies) > 0) {
                    if (count($project->technologies) > 0) {
                        $project->technologies()->detach();
                    }
                    foreach ($request->technologies as $technology) {
                        $project->technologies()->attach($technology);
                    }
                    return redirect::back()->with('message', 'Successfully Technology Added');
                }
            } else {
                abort(404);
            }
        } else {
            return redirect::back()->withErrors("No Technology selected");
        }

    }

    public function deleteTechnology($techid, $projectid)
    {

        $project = Project::find($projectid);
        if ($project) {
            // echo $techid.",".$projectid;
            $project->technologies()->detach($techid);
            return redirect::back()->with('message', 'Technology Deleted');
        } else {
            abort(404);
        }
    }

    // add date column here.

    public function addUser(Request $request, $id)
    {

        $input = $request->all();

        if (!empty($input)) {
            $project = Project::find($id);
            if ($request->end_date) {
                if ($request->end_date < $request->start_date) {
                    return redirect::back()->withErrors("Select  Valid date");
                }
            }
            if ($project) {
                $projectResources = new ProjectResource();
                $projectResources->project_id = $id;
                $projectResources->user_id = !empty($request->user_id) ? $request->user_id : null;
                $projectResources->resource_price_id = !empty($request->resource_price_id) ? $request->resource_price_id : null;
                $projectResources->start_date = $request->start_date;
                $projectResources->end_date = !empty($request->end_date) ? $request->end_date : null;
                if ($projectResources->save()) {
                    event(new DeveloperAssigned($projectResources->user_id, $projectResources->project_id, $projectResources->start_date,
                        $projectResources->end_date));
                    return redirect::back()->with('message', 'Successfully Resource Added');
                }
            } else {
                abort(404);
            }
        } else {
            return redirect::back()->withErrors("No Resource selected");
        }

    }

    public function deleteUser($userid, $projectid)
    {

        $project = Project::find($projectid);

        if ($project) {
            $project->resources()->detach($userid);
            return redirect::back()->with('message', 'Resource Deleted');
        } else {
            abort(404);
        }
    }
    //

    public function destorySprints($id)
    {

        $projectSprints = ProjectSprints::find($id);
        if ($projectSprints) {
            $projectSprints = ProjectSprints::find($id)->delete();
            $sprintResource = SprintResource::where('sprint_id', $id)->delete();
            if ($sprintResource) {
                return redirect::back()->with('message', 'Sprint Deleted');
            }

            return redirect::back()->with("Sprint deleted & No Resource found");
        } else {
            return redirect::back()->withErrors("project Sprint Not found");
        }

    }

    public function viewSprints($id)
    {

        $user = Auth::user();
        if ($user) {
            // $id = 44;
            $projectSprints = ProjectSprints::find($id);
            $projectResource = SprintResource::with('user')->where('sprint_id', $id)->get();

            $projectinfo = Project::find($projectSprints->project_id);
            $companies = $projectinfo->companies;
            $contacts = $companies->contacts->where('primary', 1);
            $technologies = Technology::all();
            $projectTechnologies = $projectinfo->technologies;

            $timelogs = Timelog::with('user')->where('project_id', $projectSprints->project_id)->where('current_date', '<=', $projectSprints->end_date)->where('current_date', '>=', $projectSprints->start_date)->orderBy('id', 'desc')->get();

            // echo"<pre>";
            // print_r($timelogs->toArray());
            // die;
            //  // $timelogs = $projectinfo->Timelog->sortByDesc('id')->take(3);
            $report = [];

            foreach ($projectResource as $key => $users) {
                $temp = [];
                $temp['name'] = $users->user->name;
                $temp['allocated'] = $users->hours;

                $consumeHours = DB::table('timelogs')->selectRaw('sum(minutes) as sum')->where('project_id', $projectSprints->project_id)->where('current_date', '<', $projectSprints->end_date)->where('current_date', '>', $projectSprints->start_date)->where('user_id', $users->employee_id)->first();

                $temp['consume'] = $consumeHours->sum / 60;
                $report[] = $temp;

            }
            // echo "<pre>";
            // print_r($projectSprints->toArray());
            // die;

            return View('pages/admin/project/view-sprint', ['report' => $report, 'timelogs' => $timelogs, 'projectSprints' => $projectSprints, 'projectinfo' => $projectinfo, 'companies' => $companies, 'contacts' => $contacts, 'technologies' => $technologies, 'projectTechnologies' => $projectTechnologies, 'user' => $user]);
        } else {
            abort(404);
        }
    }

    public function show(Request $request, $id)
    {
        if (!$request->user) {
            return response("Users only", 401);
        }
        if (!$request->user->isAdmin()) {
            return response("Admins only", 401);
        }
        $result = ProjectResource::find($id);
        if (!$result) {
            return response()->json("Resource not found", 400);
        }
        return response()->json($result);
    }

    public function updateList(Request $request, $id)
    {
        if (!$request->user->isAdmin()) {
            return response("Admins only", 401);
        }
        $result = ProjectResource::find($id);
        if (!$result || !$request->start_date) {
            return redirect::back()->withErrors("Select Data First");
        }
        if ($request->end_date) {
            if ($request->end_date < $request->start_date) {
                return redirect::back()->withErrors("Select  Valid date");
            }
        }
        $result->user_id = $request->user_id;
        $result->resource_price_id = !empty($request->resource_price_id) ? $request->resource_price_id : null;
        $result->start_date = $request->start_date;
        $result->end_date = !empty($request->end_date) ? $request->end_date : null;
        if ($result->update()) {
            return redirect('/admin/project/' . $request->project_id . '/dashboard')->with('message', 'Successfully Updated');
        } else {
            return redirect::back()->withErrors("project Resource Not Updated");
        }
    }

    public function destroyResouce($id)
    {
        $projectResources = ProjectResource::find($id);

        if ($projectResources) {
            event(new DeveloperRemoved($projectResources->user_id, $projectResources->project_id));
            $projectResources->delete();
            return redirect::back()->with('message', 'Resource Deleted');
        } else {
            abort(404);
        }

    }
    public function releaseResource($id)
    {
        $projectResources = ProjectResource::find($id);
        $end_date = date('Y-m-d', strtotime("-1 days"));
        if ($projectResources) {
            try {
                $projectResources->end_date = $end_date;
                $projectResources->save();
                event(new DeveloperRemoved($projectResources->user_id, $projectResources->project_id));
                return redirect::back()->with('message', 'Resource released');
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        } else {
            abort(404);
        }

    }
    public function projectSlackReport(Request $request)
    {
        $publicChannels = SlackApiServices::getPublicChannels();
        $project_details = ProjectService::getProjectSlackReport($publicChannels);
        return View('pages/admin/project-slack/project-slack-report', ['project_details' => $project_details]);
    }

    public function generateReportCsv($id)
    {
        $project = Project::has('resources')->with('resources')->find($id);
        if ($project) {
            $start_date = (empty(Input::get('start_date')) || Input::get('start_date') == " - ") ? null : date("Y-m-d", strtotime(str_replace(['%20', ','], " ", Input::get('start_date'))));
            $end_date = (empty(Input::get('end_date')) || Input::get('end_date') == " - ") ? null : date("Y-m-d", strtotime(str_replace(['%20', ','], " ", Input::get('end_date'))));
            if (empty($start_date) || empty($end_date) || $start_date == " - " || $end_date == " - ") {
                return Redirect::back()->withErrors(['Dates not selected or are invalid']);
            }
            $user_ids = $project->resources()->pluck('users.id')->toArray();
            $users = $project->resources()->select('users.name as name', 'users.id as id')->get();
            $project_ids = [$project->id];
            $data = ResourceService::csvReportParser($start_date, $end_date, $user_ids, $project_ids);
            if (isset($data['tasks']) && count($data['tasks']) > 0) {
                $csv = \League\Csv\Writer::createFromFileObject(new \SplTempFileObject());
                $head = [""];
                foreach ($users as $user) {
                    $head[] = $user->name . " (Hrs)";
                    $head[] = "Task";
                }
                $csv->insertOne($head);
                $head = ["Total"];
                foreach ($users as $user) {
                    $head[] = $data['total_durations'][$user->id];
                    $head[] = "";
                }
                $csv->insertOne($head);

                foreach ($data['dates'] as $date) {
                    $array = [];
                    $array[] = date("l, M j, Y", strtotime($date['date']));
                    foreach ($data['durations'][$date['date']] as $key => $value) {
                        $array[] = $value;
                        $array[] = $data['tasks'][$date['date']][$key];
                    }
                    $csv->insertOne($array);
                }
                $fileName = 'Report-' . $start_date . '-' . $end_date . '.csv';
                Storage::put('Timesheet-Reports/' . $fileName, $csv);
                $path = storage_path('app/Timesheet-Reports/' . $fileName);
                return response()->download($path);
            }
            return Redirect::back()->withErrors(['No Timesheets filled in the date range']);
        }
        return Redirect::back()->withErrors(['Invalid link followed']);
    }

    public function generateLogCsv($id)
    {
        $project = Project::has('timesheets')->find($id);
        if ($project) {
            $start_date = (empty(Input::get('start_date')) || Input::get('start_date') == " - ") ? null : date("Y-m-d", strtotime(str_replace(['%20', ','], " ", Input::get('start_date'))));
            $end_date = (empty(Input::get('end_date')) || Input::get('end_date') == " - ") ? null : date("Y-m-d", strtotime(str_replace(['%20', ','], " ", Input::get('end_date'))));
            if (empty($start_date) || empty($end_date) || $start_date == " - " || $end_date == " - ") {
                return Redirect::back()->withErrors(['Dates not selected or are invalid']);
            }
            $data = ResourceService::csvLogParser($id, $start_date, $end_date);
            if (isset($data['rows']) && count($data['rows']) > 0) {
                $csv = \League\Csv\Writer::createFromFileObject(new \SplTempFileObject());
                $csv->insertOne($data['head']);
                foreach ($data['rows'] as $row) {
                    $csv->insertOne($row);
                }
                $fileName = 'Log-' . $start_date . '-' . $end_date . '.csv';
                Storage::put('Timesheet-Logs/' . $fileName, $csv);
                $path = storage_path('app/Timesheet-Logs/' . $fileName);
                return response()->download($path);
            }
            return Redirect::back()->withErrors(['No Timesheets filled in the date range']);
        }
        return Redirect::back()->withErrors(['Invalid link followed']);
    }
    public function addProjectLinks($id)
    {
        $project = Project::where('id', $id)->first();
        return View('pages/admin/project/add-project-file', ['id' => $id, 'project' => $project]);
    }
    public function storeProjectLinks(Request $request, $id)
    {
        $user = Auth::user();
        $projectFileObj = new ProjectFiles();
        $projectFileObj->project_id = $id;
        $projectFileObj->name = $request['name'];
        $projectFileObj->path = $request['path'];
        $projectFileObj->description = $request['description'];
        $projectFileObj->added_by = $user->id;
        $projectFileObj->private_to_admin = 0;
        if ($projectFileObj->save()) {
            return redirect('/admin/project/' . $id . '/dashboard')->with('message', 'Resource price added!');
        } else {
            return Redirect::back()->withErrors(['Something went wrong']);
        }
    }
    public function showSlackChannel() {
        $slackPublicChannels = SlackApiServices::getPublicChannels();
        $allPrivateChannels = SlackApiServices::getPrivateChannels();
        return View('pages.admin.project-slack.channel-list', compact('slackPublicChannels','allPrivateChannels'));
    }
}
