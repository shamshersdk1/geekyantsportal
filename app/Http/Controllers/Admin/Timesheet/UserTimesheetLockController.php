<?php

namespace App\Http\Controllers\Admin\Timesheet;

use App\Http\Controllers\Controller;
use App\Models\Timesheet\UserTimesheetLock;
use Auth;
use Illuminate\Http\Request;

class UserTimesheetLockController extends Controller
{

    public function index($query = null)
    {
        $lockObj = UserTimesheetLock::paginate(100);
        return view('pages.admin.new-timesheet.lock', compact('lockObj'));
    }

    public function edit($id)
    {
        $lockObj = UserTimesheetLock::find($id);
        return view('pages.admin.new-timesheet.lock-edit', compact('lockObj'));
    }

    public function update(Request $request, $id)
    {
        $user = Auth::user();

        if (!($user->hasRole('admin')) || !($user->role == 'admin')) {
            return redirect()->back()->withErrors('Unauthorized access!')->withInput();
        }

        $data = $request->all();

        if (!empty($data)) {

            $lockObj = UserTimesheetLock::find($id);
            if ($lockObj) {
                $lockObj->date = !empty(date_to_yyyymmdd($data['to_date'])) ? date_to_yyyymmdd($data['to_date']) : null;
                $lockObj->billing_date = !empty(date_to_yyyymmdd($data['from_date'])) ? date_to_yyyymmdd($data['from_date']) : null;
                $lockObj->notes = !empty($data['notes']) ? $data['notes'] : null;
                $lockObj->created_by = $user->id;
                if (!empty($data['project_id'])) {
                    $lockObj->reference_type = 'App\Models\Admin\Project';
                    $lockObj->reference_id = $data['project_id'];
                }
                if ($lockObj->save()) {
                    return redirect('/admin/bonus/on-site')->with('message', 'Updated!');
                } else {
                    return redirect()->back()->withErrors('Something went wrong!')->withInput();
                }
            } else {
                return redirect()->back()->withErrors('Not found!')->withInput();
            }
        }
    }
}
