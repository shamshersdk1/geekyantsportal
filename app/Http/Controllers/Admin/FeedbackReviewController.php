<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Admin\FeedbackUser;
use App\Models\Admin\FeedbackMonth;
use App\Models\User;
use App\Models\Admin\Role;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Services\FeedbackService;

use Redirect;
use Config;
use Exception;
use DB;
use Auth;
use Validator;

class FeedbackReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getList()
    {
        $user = Auth::user();
        return View('pages/admin/feedback-review-rm/index', ['user' => $user]);
    }

    public function getFeedbackListForUser($user_id, $month_id)
    {
        $reviewer = Auth::user();
        $reviews = [];
        // Get all roles
        $roleList = FeedbackService::getAllReviewerType($reviewer->id, $month_id);
        foreach ( $roleList as $role )
        {
            $tempArray = FeedbackUser::where('user_id',$user_id)->where('feedback_month_id',$month_id)->where('reviewer_type',$role->id)
                                     ->where('reviewer_id',$reviewer->id)->groupBy('project_id')->get();
            $data = [];
            foreach ($tempArray as $temp) {
                $feedbakUser = [];
                $feedbakUser['project'] = $temp->project->toArray();
                $feedbakUser['project']['project_reviews'] = FeedbackService::getProjectReview($user_id, $reviewer->id, $role->id, $temp->project->id, $month_id);
                $data[]=  $feedbakUser['project'];
            }
            $reviews[$role->code] = $data;
        }
        $forUser = User::find($user_id);
        $month = FeedbackMonth::find($month_id);
        $monthName = "";
        if($month) {
            $monthName = date('M, Y', mktime(0, 0, 0, $month->month, 10, $month->year)); 
        }
        $reviews['month'] = $month;
        $reviews['isLocked'] = $month->locked;
        $reviews['reviewer_type'] = $roleList;
        $reviews['for_user'] = $forUser;
        return View('pages/admin/feedback/review/detail', compact('user','reviews','forUser','monthName','month'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        return View('pages/admin/question/add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'question' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect::back()->withErrors($validator->errors())->withInput($request->input());
        }
        $questionObj = new FeedbackQuestionTemplate();
        $questionObj->question = $request->question;
        $questionObj->status = !empty($request->status) ? $request->status : false;
    
        if ( $questionObj->save() )
        {
            return redirect('/admin/question/'.$questionObj->id)->with('message', 'Successfully added question');
        }
        else
        {
            return Redirect::back()->withInput()->withErrors($questionObj->getErrors());
        }

    }
    /**
     * Show details of the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $questionObj = FeedbackQuestionTemplate::find($id);
        if ( $questionObj ) {
            return View('pages/admin/question/show', ['questionObj' => $questionObj] );
        } else {
            return Redirect::back()->withInput()->withErrors('No record found');
        }
        
        
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $questionObj = FeedbackQuestionTemplate::find($id);
        $designations = Designation::all();
        $roles = Role::all();
        if($questionObj){

            return View('pages/admin/question/edit', ['questionObj' => $questionObj, 'designations' => $designations,'roles' => $roles]);
        }
        else {
            return Redirect::back()->withInput()->withErrors('No record found');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateRating(Request $request)
    {
        $input = $request->all();
        foreach( $input['status'] as $index => $row )
        {
            dump($input['rating'][$index]);
            $feedbackUserObj = FeedbackUser::find($index);
            if( $feedbackUserObj )
            {
                $feedbackUserObj->rating = !empty($input['rating'][$index]) ? $input['rating'][$index] : '';
                $feedbackUserObj->status = !empty($input['status'][$index]) ? $input['status'][$index] : $feedbackUserObj->status;
                $feedbackUserObj->save();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $questionObj = FeedbackQuestionTemplate::find($id);
        if ( $questionObj->delete() )
        {
            return redirect('/admin/question')->with('message','Successfully Deleted');
        }
        else
        {
            return redirect::back()->withErrors('Error while deleting the record');
        }
    }

    public function months()
    {
        $monthList = FeedbackMonth::orderBy('year', 'DESC')->orderBy('month', 'DESC')->paginate(12);
        return View('pages.admin.feedback.reports.month', compact('monthList'));
    }

    public function report($id) {
        $monthObj = FeedbackMonth::find($id);
        $feedbakUsers = FeedbackUser::with('user')->where('feedback_month_id', $id)->groupBy('user_id')->orderBy('user_id','ASC')->get();
        $users = [];
        foreach ($feedbakUsers as $feedbakUser) {
            $data = [];
            $data['id'] = $feedbakUser->id;
            $data['user'] = User::find($feedbakUser->user_id)->toArray();
            $data['reviews'] = FeedbackService::getReviewDetail($feedbakUser->user_id, $monthObj->id);
            $data['overall_completion'] = FeedbackService::getOverallCompletionStatus($feedbakUser->user_id, $monthObj->id);
            $users[] = $data;
        }
        return View('pages.admin.feedback.reports.index', compact('monthObj','feedbakUsers','users'));
    }

    public function showSummary($month_id){
        $authUser = Auth::user();
        if ( !$authUser->hasRole('admin') )
        {
            return Redirect::back()->withInput()->withErrors('Invalid access');
        }   
        // Fetch all records for a user for month
        $users = User::where('is_active', 1)->get();
        $usersFeedbacks = FeedbackUser::getUsersSummary($month_id);
        $month_range = FeedbackMonth::getMonthStartAndEnd($month_id);
        $usersData = [];
        foreach($users as $user){
            $usersData[$user->id]['can_show'] = false;
            $usersData[$user->id]['feedback_rating'] = \App\Services\FeedbackService::getUserRating($month_id,$user->id);
            $usersData[$user->id]['current_salary'] = \App\Models\Admin\Appraisal::getCurrentSalary($user->id);
            $usersData[$user->id]['feedback_bonus'] = 0;

            $usersData[$user->id]['onsite_bonus'] = 0;
            $usersData[$user->id]['additional_working_day_bonus'] = 0;
            $usersData[$user->id]['techtalk_bonus'] = 0;

            if($usersData[$user->id]['feedback_rating'] > 0){
                $feedback_bonus_total_monthly = round($usersData[$user->id]['current_salary']->qtr_bonus/12);
                $feedback_bonus_on_feedback = ($feedback_bonus_total_monthly/100) * ($usersData[$user->id]['feedback_rating'] * 10);
                $usersData[$user->id]['feedback_bonus'] = $feedback_bonus_on_feedback;
            }

            $usersData[$user->id]['can_show'] = ($usersData[$user->id]['current_salary']->qtr_bonus > 0);

        }

        return View('pages/admin/feedback/review/show-summary', ["users"=>$users, "usersFeedbacks"=>$usersFeedbacks, "usersData" => $usersData, "month_id"=>$month_id]);
    }

    public function showAdmin($month_id, $user_id)
    {
        $authUser = Auth::user();
        if ( !$authUser->hasRole('admin') )
        {
            return Redirect::back()->withInput()->withErrors('Invalid access');
        }
        $reviews = [];
        // Fetch all records for a user for month
        $tempArray = FeedbackUser::where('user_id',$user_id)->where('feedback_month_id',$month_id)
                    ->groupBy('project_id')->get();

        foreach ($tempArray as $temp) 
        {
            $feedbakUser = [];
            $feedbakUser['project'] = $temp->project->toArray();
            $reviewerIdList = FeedbackUser::where('user_id',$user_id)->where('feedback_month_id',$month_id)->where('project_id',$temp->project_id)
                                            ->select()->distinct('reviewer_id')->pluck('reviewer_id')->toArray();
            foreach( $reviewerIdList as $reviewerId )
            {
                $feedbakUser['project']['reviewer'][$reviewerId] = User::find($reviewerId)->toArray();
                $feedbakUser['project']['reviewer'][$reviewerId]['project_reviews'] = FeedbackService::getReviewedProjectAdmin($user_id, $reviewerId, $temp->project->id, $month_id);
            }
            $reviews[] = $feedbakUser;
        }
        $forUser = User::find($user_id);
        $month = FeedbackMonth::find($month_id);
        $monthName = "";
        if($month) {
            $monthName = date('M, Y', mktime(0, 0, 0, $month->month, 10, $month->year)); 
        }
        
        return View('pages/admin/feedback/review/show-admin', compact('authUser','reviews','forUser','monthName','month'));
    }

    public function showUserMonths()
    {
        $monthList = FeedbackMonth::paginate(12);
        return View('pages.admin.feedback.review.month-list', compact('monthList'));
    }

    public function showUser($month_id)
    {
        $forUser = Auth::user();
        $user_id = $forUser->id;
        $reviews = [];
        // Fetch all records for a user for month
        $tempArray = FeedbackUser::where('user_id',$user_id)->where('feedback_month_id',$month_id)
                    ->groupBy('reviewer_id')->get();
        foreach ($tempArray as $temp) 
        {
            $feedbakUser = [];
            $feedbakUser['reviewer'] = $temp->reviewer->toArray();
            $feedbakUser['reviewer']['project_reviews'] = FeedbackService::getReviewedProjectAdmin($user_id, $temp->reviewer->id, $month_id);
            $reviews[] = $feedbakUser;
        }   
        $month = FeedbackMonth::find($month_id);
        $monthName = "";
        if($month) {
            $monthName = date('M, Y', mktime(0, 0, 0, $month->month, 10, $month->year)); 
        }
        // dd($reviews);
        return View('pages/admin/feedback/review/show-user', compact('authUser','reviews','forUser','monthName','month'));
    }
    
}
