<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Floor;
use App\Models\FloorsSeat;
use Validator;
use Redirect;
use Route;
use Carbon\Carbon;

class FloorsSeatController extends Controller
{
    
    public function create()
    {
        $floors = Floor::all();
        return view('pages/admin/floors-seat/add',[ 'floors' => $floors ]);
    }

    public function show($id)
    {
        $floor = Floor::find($id);
        return view('pages/admin/floors-seat/add',[ 'floor' => $floor ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'number_of_seats' => 'required'
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors(['Please provide all the mandatory fields']);
        }
        FloorsSeat::createSeats($request->number_of_seats, $request->floor_id);
        $floor = Floor::where('id',$request->floor_id)->first();
        $floor->number_of_seats = $floor->number_of_seats + $request->number_of_seats;
        $floor->save();
        return Redirect::back()->withInput()->withMessage('Successfully Added');
    }

    
}
