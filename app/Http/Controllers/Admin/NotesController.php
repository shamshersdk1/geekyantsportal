<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\Admin\Lead;
use App\Http\Requests;

class NotesController extends Controller
{
    //
    public $timestamps = false;
	public function index()
	{	
		
		$userRole = UserRole::all();

		return View('pages/admin/userRole/index', ['userRole' => $userRole]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */

	// public function add(){
	// 	// echo "hello";
	// 	// die;
	// 	$technologies = Technology::all();
	// 	return View('pages/admin/project/add', ['technologies' => $technologies]);
	// 	// return View('pages/admin/project/add');
	// }

	public function store(Request $request)
	{		
			
			$userRole = new UserRole();
			
			// if($request->role==null)
			// 	return redirect()->back()->withErrors(['message', 'The Role be define']);
			$userRole->role = $request->role;
			$userRole->description = $request->description;
			$userRole->created_at = $userRole->freshTimestamp();
			if($userRole->save())
			{
				return redirect('/admin/user-role')->with('message', 'Successfully Added');

			}
			else{
				return redirect()->back()->withErrors($userRole->getErrors());
				
			}
		
		
		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	// public function show($id)
	// {

	// 	$technologies = UserRole::find($id)->technologies;
	// 	$project = Project::find($id);
	// 	return View('pages/admin/project/show', ['project' => $project, 'technologies' => $technologies]);
	// }

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{

		$userRole = UserRole::find($id);
		if($userRole==null)
				return redirect()->back()->withErrors(['message', 'User not found']);

		return View('pages/admin/userRole/add', ['userRole' => $userRole]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		// if(!isset($id))
		// 	return redirect::back()->withErrors(['User Id not found']);
		// if($request->role==null)
		// 		return redirect()->back()->withErrors(['message', 'The Role be define']);

	 	$userRoleObj = UserRole::find($id);
	 	$userRoleObj->role = $request->role;
	 	$userRoleObj->description = $request->description;

	 	if($userRoleObj->save())
			{
				return redirect('/admin/user-role')->with('message', 'Successfully Added');

			}
			else{
				return redirect()->back()->withErrors($userRoleObj->getErrors());
				
			}

	 // 	if($userRoleObj->update(['role' => $request->role, 'description' => $request->description]))
		// {
		// 	return redirect('/admin/user-role')->with('message','Successfully Updated');
		// }
		// else{
		// 	return redirect::back()->withErrors($userRoleObj->getErrors());
		// }

		
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$userRole = UserRole::find($id);
		// echo json_encode($array);
		if($userRole){
			if($userRole->delete()){
				$array = ['status' => true];
			}
			else{
				$array = ['status' =>false];
			}
			return redirect('/admin/user-role')->with('message','Successfully Deleted');
		}
	}

}
