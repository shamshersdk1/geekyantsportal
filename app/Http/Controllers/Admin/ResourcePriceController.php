<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Project;
use App\Models\Admin\ResourcePrice;
use Illuminate\Http\Request;
use Redirect;

class ResourcePriceController extends Controller
{
    public function index($id)
    {
        $resource_price = ResourcePrice::where('project_id', $id)->orderBy('type')->get();
        $project = Project::where('id', $id)->first();
        return View('pages/admin/project/resource-price', compact('resource_price', 'project', 'id'));
    }
    public function add($id)
    {
        $project = Project::where('id', $id)->first();
        return View('pages/admin/project/add-resource-price', ['id' => $id, 'project' => $project]);
    }
    public function create(Request $request, $id)
    {
        $resourcePriceObj = new ResourcePrice();
        $resourcePriceObj->project_id = $id;
        $resourcePriceObj->type = $request['type'];
        $resourcePriceObj->price = $request['price'];
        if ($resourcePriceObj->save()) {
            return redirect('/admin/project/' . $id . '/dashboard')->with('message', 'Resource price added!');

        } else {
            return Redirect::back()->withErrors(['Something went wrong!']);
        }
    }
    public function remove(Request $request, $id)
    {
        $resourcePriceObj = ResourcePrice::where('id', $request['resource_id'])->delete();
        if ($resourcePriceObj) {
            return redirect('/admin/project/' . $id . '/dashboard')->with('message', 'Deleted Successfully!');

        } else {
            return redirect('/admin/project/' . $id . '/dashboard')->withErrors(['Uable to delete record!']);
        }
    }
    public function edit(Request $request, $id)
    {
        $resource_price = ResourcePrice::where('id', $request['resource_id'])->first();
        return View('pages/admin/project/edit-resource-price', ['resource_price' => $resource_price, 'id' => $id]);
    }
    public function update(Request $request, $id)
    {
        $resourcePriceObj = ResourcePrice::where('id', $request['resource_id'])->first();
        $resourcePriceObj->type = $request['type'];
        $resourcePriceObj->price = $request['price'];
        if ($resourcePriceObj->save()) {
            return redirect('/admin/project/' . $id . '/dashboard')->with('message', 'Resource price added!');

        } else {
            return Redirect::back()->withErrors(['Something went wrong!']);

        }
    }
}
