<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Admin\Timelog;
use App\Models\Admin\Project;
use App\Models\Admin\ProjectSprints;
use App\Models\Admin\SprintResource;
use App\Models\Admin\ProjectSlackChannel;
use App\Models\Admin\ProjectResource;
use App\Models\Admin\SlackUser;
use App\Models\TimesheetDetail;
use App\Services\SlackService\SlackListnerService\SlackKeyword\Timesheet;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use Validator;
use Redirect;
use App\Models\User;
use Auth;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use Config;
use Exception;
use Mail;
use App\Mail\PasswordResetMail;

class TimelogController extends Controller
{
    public function dashboard()
    {

       	
    	$userId = Auth::user()->id;

    	$timelogHistoryObj = Timelog::with('project')->where('user_id',$userId)->where('is_recorded',false)->orderBy('updated_at','desc')->get();
        
    	return View('pages/admin/timelog/dashboard',['timelogHistory' => $timelogHistoryObj]);
    }
    public function history(Request $request)
    {	


    	$userId =  Auth::user()->id;
        $projects = Project::all();
    	$timelogHistoryObj = Timelog::with('project')->where('user_id',$userId)->where('is_recorded',true)->orderBy('updated_at','desc')->paginate(10);
    	// return $timelogHistoryObj;

    	
    	return View('pages/admin/timelog/history',['timelogHistory' => $timelogHistoryObj, 'projects' => $projects]);
    }

    public function store(Request $request)
    {	
    	print_r($request->all());
    	// die;
    }

    public function filter(Request $request)
    {   
        // print_r($request->project);
        // die;
        $projects = Project::all();
        $userId = Auth::user()->id;
        if($request->month && $request->project){
            $timelogHistoryObj = Timelog::with('project')->where('current_date','like','____-'.$request->month.'-__')->where('is_recorded',true)->where('project_id', $request->project)->paginate();
        }
        elseif($request->month){
            $timelogHistoryObj = Timelog::with('project')->where('current_date','like','____-'.$request->month.'-__')->where('is_recorded',true)->paginate();
            //->where('user_id',$userId)->orderBy('updated_at','desc')->paginate(2);
        }
        elseif($request->project){
             $timelogHistoryObj = Timelog::with('project')->where('project_id', $request->project)->where('is_recorded',true)->paginate();
        }
        else
        {
            $timelogHistoryObj = Timelog::with('project')->where('user_id',$userId)->where('is_recorded',true)->orderBy('updated_at','desc')->paginate(10);
        }

        // return $timelogHistoryObj;
        // echo "<pre>";
        // print_r($timelogHistoryObj->toArray());
        // die;
        return View('pages/admin/timelog/history',['timelogHistory' => $timelogHistoryObj, 'projects' => $projects]);

    }


     public function dashboardAction(Request $request)
    {	
    	//print_r($request->all());
    	//print_r($request->message);
    	if(!isset($request->index))
    		return back()->with('message', ' No entity Selected');	

    	
    	if($request->action == 'delete'){
	    		foreach ($request->index as $key => $id) {
	    		$delete = Timelog::destroy($id);
	    		
	    	}
	    	if($delete)
	    		 return back()->with('message', ' Successfully deleted');
    		
    	}	
    	if($request->action == 'record'){
    		foreach ($request->index as $key => $id) {
	    		$isRecorded = Timelog::where('id',$id)->update(['is_recorded' => 1]);
	    		
	    	}
	       if($isRecorded)
	    		 return back()->with('message', ' Successfully recorded');	
    	}

    	if($request->action == 'save'){

    		foreach ($request->index as $key => $value) {
	    		// $isRecorded = Timelog::where('id',$value)->update(['message' => $request->message[$value],'minutes' => $request->minute[$value]]);
	    		
	    	}
	       if($isRecorded)
	    		 return back()->with('message', ' Successfully Saved');	
    	}
    		
    }
    public function saveTimelog(Request $request)
    {   
        return Timesheet::main($request);
        /*
        $channelId = $request->input('channel_id');
        $channelName = $request->input('channel_name');
        $message = $request->input('text');
        $userName = $request->input('user_name');
        $slackUserId = $request->input('user_id');
        $current = date('Y-m-d');
        $data = [];
        // $minutes = 0;
        
        $slackUser = SlackUser::where('slack_id',$slackUserId)->first();
        if(!empty($slackUser) && !empty($slackUser->user)) {
            $user = $slackUser->user;
        }
        $userId;
        if(!$user) {
            try{
                $client = new Client(['base_uri' => config('app.slack_url')]);
                $response = $client->request("POST", "users.info", [
                        'form_params' => [
                            'token' => config('app.slack_token'),
                            'user' => $slackUserId
                        ]
                ]);
            } catch(Exception $e) {
                return (['response_type' => 'in_channel', 'text' => "Slack user info api not worked." ]);  
            }
            $userCreateResponse =  $response->getBody();
            $response = json_decode($userCreateResponse);
            $slackUsers = $response->user;

            if($slackUsers) {
                // $data['name'] = $slackUsers->name;
                $data['email'] = $slackUsers->profile->email;
                // $data['password'] = rand(10,1000);
            }

            if($data['email']) {
                $slackEmail = User::where('email',$data['email'])->first();
                if(!$slackEmail) {
                    return (['response_type' => 'ephemeral', 'text' => "Couldn't find your credentials in GeekyAnts DB", 'attachments' => ['text' => "Contact your Reporting Manager"], 'icon_emoji' => ":fearful:" ]);  
                //    if($data['name'] && $data['email'] && $data['password'] && $slackUsers) {
                //         $user = User::saveData($data);
                //         //send mail
                //         $userId = $user['id'];
                //         \Log::info('sending mail');
                //         Mail::to($user['user'])->send(new PasswordResetMail($data));
                //         \Log::info('sent mail');
                //     } else {
                //         return ['response_type' => 'in_channel', 'text' => "No sufficient data of user to save in our db." ];  
                //     } 
                } else {
                    $userId = $slackEmail->id;
                    $user = $slackEmail;
                }
            }
        } else {
            $userId = $user->id;
        }

        $projectSlack = ProjectSlackChannel::with('projects')->where('slack_id',$channelId)->first();

        if(!$projectSlack || empty($projectSlack->project)){
            return (['response_type' => 'in_channel', 'text' => "This channel is not linked to any project"]); 
        }
        $is_allowed = ProjectResource::where('user_id',$user_id)->where('project_id',$project_id)->where(function ($query) {
            $query->where('end_date', '>=', $current)->orWhere('end_date', null);
        })->first();
        if(!$is_allowed) {
            return (['response_type' => 'ephemeral', 'text' => "You are not a part of this project" ]);  
        }
        $timePos = strrpos($message,'[');
        $timeLastPos = strrpos($message,']');
        
        $taskpos = strpos($message,'[');
        $tasklastPos = strpos($message,']');

        if($timePos && $timeLastPos){
            $length = $timeLastPos - $timePos - 1;
            try
            {
                $num = (float)substr($message,$timePos+1,$length);
                if(!preg_match('/^\d*(\.\d{0,2})?$/', $num)) {
                    return (['response_type' => 'ephemeral', 'text' => 'The duration you have entered is invalid']);
                }
                $time = $num;
            } 
            catch(Exception $e)
            {
                return (['response_type' => 'ephemeral', 'text' => $e->getMessage()]);  
            }
        } else {
            return (['response_type' => 'ephemeral', 'text' => 'Please use correct format to log your work.']);
        }
        if($taskPos && $taskLastPos){
            $length = $taskLastPos - $taskPos - 1;
            $desc = substr($message,$taskPos+1,$length);
            if (preg_match('/[\'^"£$%&*()}{@#~?><>,|=_+¬-]/', $desc)) {
                return (['response_type' => 'ephemeral', 'text' => 'Special characters not allowed !']);
            }
        } else {
            return (['response_type' => 'ephemeral', 'text' => 'Please use correct format to log your work.']);
        }
        
        $timesheet = Timesheet::where('date', $current)->where('user_id', $userId)->where('project_id', $projectSlack->project->id)->get();
        $timesheetId;
        if($timesheet) {
            if (isset($timesheet->approver_id) ) {
                return (['response_type' => 'ephemeral', 'text' => "your timesheet for today has already been approved"]);
            }
            $timesheetId = $timesheet->id;
        } else {
            try{
                $timesheetObj = new Timesheet();
                $timesheetObj->user_id = $userId;
                $timesheetObj->project_id = $projectSlack->project->id;
                $timesheetObj->date = $current;
                $timesheetObj->hours = 0;
                $timesheetObj->save();
                $timesheetId = $timesheetObj->id;
            } catch (Exception $e) {
                return (['response_type' => 'ephemeral', 'text' => "Unable to save your timesheet, try to save it through\n".config('app.geekyants_portal_url')."/user/timesheet", 'attachments' => ['text' => $e->getMessage()]]);
            } 
        }
        
        if($project && $user){
            try {
                $timesheetDetail = new TimesheetDetail();
                $timesheetDetail->timesheet_id = $timesheetId;
                $timesheetDetail->duration = $time;
                $timesheetDetail->reason = $desc;
                $timesheetDetail->save();
                $timesheetDetail->timesheet->approved_hours = $timesheetDetail->timesheet->sumOfHours();
                $timesheetDetail->timesheet->save();
                return (['response_type' => 'ephemeral', 'text' => 'Timesheet has been set']);
            } catch (Exception $e) {
                return (['response_type' => 'ephemeral', 'text' => "Unable to save your timesheet, try to save it through\n".config('app.geekyants_portal_url')."/user/timesheet", 'attachments' => ['text' => $e->getMessage()]]);
            }
        } else {
            return (['response_type' => 'in_channel', 'text' => "Project not found in GeekyAnts DB."]); 
        }
        
        
        
        // $message = substr($message,0,$timePos);
        // $sprintId = ProjectSprints::where('start_date','<', $current)->where('end_date','>' ,$current)->where('project_id',$project->id)->select('id')->first();
        
        // $preAllocated = false;
        
        // if($sprintId)
        //     $preAllocated = SprintResource::where('sprint_id',$sprintId->id)->where('employee_id',$userId)->first();
        
        
        // if($preAllocated)
        //     $preAllocated = 1;
        // else
        //     $preAllocated = 0; 
                
        // if($project && $user){
            //     $timelogObj = new Timelog();
            //     $timelogObj->user_id = $userId;
            //     $timelogObj->project_id = $project->id;
            //     $timelogObj->times = $times;
            //     $timelogObj->current_date = $current;
        //     $timelogObj->message = $message;
        //     $timelogObj->is_recorded = 0;
        //     $timelogObj->is_pre_Allocated = $preAllocated;


        //     if($timelogObj->save())
        //         return ['text' => 'timelog has been set', 'attachments' => ['text' => "timelog has been set"]];
        //     else
        //         return ['text' => "Something went wrong"];  
        // } else {
        //     return ['text' => "Project not found."]; 
        // }
        */
    }

}
 