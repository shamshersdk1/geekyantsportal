<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Validator;
use Redirect;
use Route;
use Carbon\Carbon;

class ChecklistController extends Controller
{
    public function index()
    {
        return view('pages/admin/checklist/index');
    }
    
}
