<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Event;
use App\Models\UserEvent;
use App\Models\UserEventAttachment;
use File;
use Validator;
use Redirect;

class UserEventController extends Controller
{
    public function index()
    {
        return redirect::back();
    }
    public function create(Request $request)
    {
        $jsonuser=User::getUsers();
        $jsonadmin=User::getUsers('admin');
        $events=Event::get();
        if (!empty($request->event)) {
            $eventno=$request->event;
        } else {
            $eventno=0;
        }
        return view('pages.admin.user-events.add', compact('jsonuser', 'jsonadmin', 'admins', 'events', 'eventno'));
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
        'name' => 'required',
        'event' => 'required',
        'date' => 'required|date',
        'approved_by' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
            ->withErrors($validator)
               ->withInput();
        }
        $userEventobj=new UserEvent;
        $user=User::where('name', $request->name)->first();
        if (empty($user)) {
            return redirect()->back()
            ->withErrors('Enter a valid User')
               ->withInput();
        }
        $admin=User::where('name', $request->approved_by)->where('role', 'admin')->first();
        if (empty($admin)) {
            return redirect()->back()
            ->withErrors('Enter a valid Admin')
               ->withInput();
        }
        
        $date=strtotime($request->date);
        $date=date('y-m-d', $date);
        $check=UserEvent::where('user_id', $user->id)->where('event_id', $request->event)->where('added_on', $date)->get();
        if (count($check)>0) {
            return redirect()->back()
            ->withErrors('Entry already exists!')
            ->withInput();
        }
        $userEventobj->user_id=$user->id;
        $userEventobj->event_id=$request->event;
        $userEventobj->added_on=$date;
        $userEventobj->approved_by=$admin->id;
        $userEventobj->uploaded_by=\Auth::id();
        $userEventobj->note=$request->note;
        $targetFile=[];
        $filename=[];
        $attname=[];
        $count=0;
        for ($i=1; $i<=$request->attcount; $i++) {
            $name="attachment".$i;
            if ($file = $request->file($name)) {
                $attname[$count]=$name;
                $targetFile[$count]= storage_path('Events/'.str_replace(' ', '_', $userEventobj->event->name).'/'.$user->id.'');
                $fileName[$count]= $file->getClientOriginalName();
                if (file_exists($targetFile[$count].'/'.$fileName[$count])) {
                    return redirect()->back()
                    ->withErrors('Unable to save, file'.$fileName[$count] .'already exists!')
                    ->withInput();
                }
                $count++;
            }
        }
        try {
            $userEventobj->save();
            for ($i=0; $i<$count; $i++) {
                $file = $request->file($attname[$i]);
                $usereventattachmentobj=new UserEventAttachment;
                $usereventattachmentobj->user_event_id=$userEventobj->id;
                $usereventattachmentobj->attachment=$fileName[$i];
                $file->move($targetFile[$i], $fileName[$i]);
                $usereventattachmentobj->save();
            }
            return redirect::back()->with('message', 'User event added successfully!');
        } catch (Exception $e) {
            return redirect()->back()
            ->withErrors('Unable to save')
            ->withInput();
        }
    }
    public function show($id)
    {
        $userevent=UserEvent::find($id);
        if ($userevent) {
            return View('pages/admin/user-events/show', compact('userevent') );
        } else {
            abort(404);
        }
    }
    public function edit($id)
    {
        $userevent = UserEvent::find($id);
        if ($userevent) {
            $jsonuser=User::getUsers();
            $jsonadmin=User::getUsers('admin');
            $events=Event::get();
            return View('pages/admin/user-events/edit', compact('userevent', 'events', 'jsonuser', 'jsonadmin' ));
        } else {
            abort(404);
        }
    }
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
        'name' => 'required',
        'event' => 'required',
        'date' => 'required|date',
        'approved_by' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
            ->withErrors($validator)
               ->withInput();
        }
        $userevent=UserEvent::find($id);
        $user=User::where('name', $request->name)->first();
        if (empty($user)) {
            return redirect()->back()
            ->withErrors('Enter a valid User')
               ->withInput();
        }
        $admin=User::where('name', $request->approved_by)->where('role', 'admin')->first();
        if (empty($admin)) {
            return redirect()->back()
            ->withErrors('Enter a valid Admin')
               ->withInput();
        }
        
        if ($userevent) {
            $attachments=$userevent->attachments;
            $attid=explode(" ", $request->attachments_deleted);
            foreach ($attid as $a) {
                $uea=UserEventAttachment::find($a);
                if ($uea) {
                    $uea->delete();
                }
            }
            $date=strtotime($request->date);
            $date=date('y-m-d', $date);
            $check=UserEvent::where('user_id', $user->id)->where('event_id', $request->event)->where('added_on', $date)->where('id', '!=', $id )->get();
            if ((count($check)>0)) {
                return redirect()->back()
                ->withErrors('Entry already exists!')
                ->withInput();
            }
            $userevent->user_id=$user->id;
            $userevent->event_id=$request->event;
            $userevent->added_on=$date;
            $userevent->approved_by=$admin->id;
            $userevent->uploaded_by=\Auth::id();
            $userevent->note=$request->note;
            $targetFile=[];
            $filename=[];
            $attname=[];
            $count=0;
            for ($i=1; $i<=$request->attcount; $i++) {
                $name="attachment".$i;
                if ($file = $request->file($name)) {
                    $attname[$count]=$name;
                    $targetFile[$count]= storage_path('Events/'.str_replace(' ', '_', $userevent->event->name).'/'.$user->id.'');
                    $fileName[$count]= $file->getClientOriginalName();
                    if (file_exists($targetFile[$count].'/'.$fileName[$count])) {
                        return redirect()->back()
                        ->withErrors('Unable to save, file: '.$fileName[$count] .'already exists!')
                        ->withInput();
                    }
                    $count++;
                }
            }
            if ($userevent->save()) {
                for ($i=0; $i<$count; $i++) {
                    $file = $request->file($attname[$i]);
                    $usereventattachmentobj=new UserEventAttachment;
                    $usereventattachmentobj->user_event_id=$userevent->id;
                    $usereventattachmentobj->attachment=$fileName[$i];
                    $file->move($targetFile[$i], $fileName[$i]);
                    if ($usereventattachmentobj->save()) {
                        return redirect('/admin/user-events/'.$userevent->id)->with('message', 'Successfully Updated');
                    } else {
                        return redirect::back()->withErrors($usereventattachmentobj->getErrors())->withInput();
                    }
                }
                return redirect('/admin/user-events/'.$userevent->id)->with('message', 'Successfully Updated');
            } else {
                return redirect::back()->withErrors($userevent->getErrors())->withInput();
            }
        } else {
            abort(404);
        }
    }
    public function destroy($id)
    {
        $userevent=UserEvent::find($id);
        if ($userevent) {
            $userevent->delete();
            return redirect::back()->with('message', 'Successfully Deleted');
        } else {
            abort(404);
        }
    }
}
