<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Admin\ExpenseHead;
use App\Models\User;

use App\Http\Controllers\Controller;

use Redirect;
use Auth;
use Validator;

class ExpenseHeadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $expenseHeads = ExpenseHead::paginate(100);
        return View('pages/admin/expense-head/index', ['expenseHeads' => $expenseHeads] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::whereHas('roles', function($query){
            $query->whereIn('code', ['admin', 'office-admin', 'system-admin', 'reporting-manager', 'humnar-resources', 'account']);
            })->where('is_active',1)->get();
        return View('pages/admin/expense-head/add',['users' => $users]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title'  => 'required'
        ]);
        if ($validator->fails()) {
            return redirect::back()->withErrors($validator->errors())->withInput($request->input());
        }
        $expenseHeadObj = new ExpenseHead();
        $expenseHeadObj->title = $request->title;
        $expenseHeadObj->low_cost_approver = $request->low_cost_approver;
        $expenseHeadObj->high_cost_approver = $request->high_cost_approver;
    
        if ( $expenseHeadObj->save() )
        {
            return redirect('/admin/expense-head')->with('message', 'Successfully added expense head');
        }
        else
        {
            return Redirect::back()->withInput()->withErrors($expenseHeadObj->getErrors());
        }

    }
    /**
     * Show details of the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $expenseHeadObj = ExpenseHead::find($id);
        if ( $expenseHeadObj ) {
            return View('pages/admin/expense-head/show', ['expenseHeadObj' => $expenseHeadObj] );
        } else {
            return Redirect::back()->withInput()->withErrors('No record found');
        }
        
        
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $expenseHeadObj = ExpenseHead::find($id);
        $users = User::whereHas('roles', function($query){
            $query->whereIn('code', ['admin', 'office-admin', 'system-admin', 'reporting-manager', 'humnar-resources', 'account']);
            })->where('is_active',1)->get();
        if($expenseHeadObj){

            return View('pages/admin/expense-head/edit', ['expenseHeadObj' => $expenseHeadObj, 'users' => $users]);
        }
        else {
            return Redirect::back()->withInput()->withErrors('No record found');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title'  => 'required'
        ]);
        if ($validator->fails()) {
            return redirect::back()->withErrors($validator->errors())->withInput($request->input());
        }
        $expenseHeadObj = ExpenseHead::find($id);
        if ( $expenseHeadObj ) {
            $expenseHeadObj->title = $request->title;
            $expenseHeadObj->low_cost_approver = $request->low_cost_approver;
            $expenseHeadObj->high_cost_approver = $request->high_cost_approver;
            if ( $expenseHeadObj->save() )
            {
                return redirect('/admin/expense-head')->with('message', 'Successfully updated');
            }
            else
            {
                return Redirect::back()->withInput()->withErrors($expenseHeadObj->getErrors());
            }
        } else {
            return Redirect::back()->withInput()->withErrors('No record found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $expenseHeadObj = ExpenseHead::find($id);
        if ( $expenseHeadObj->delete() )
        {
            return redirect('/admin/expense-head')->with('message','Successfully Deleted');
        }
        else
        {
            return redirect::back()->withErrors('Error while deleting the record');
        }
    }
    
}
