<?php

namespace App\Http\Controllers\Admin\Appraisal;

use App\Http\Controllers\Controller;
use App\Http\Requests\ValidateAppraisalRequest;
use App\Models\Appraisal\Appraisal;
use App\Models\Appraisal\AppraisalBonus;
use App\Models\Appraisal\AppraisalBonusType;
use App\Models\Appraisal\AppraisalComponent;
use App\Models\Appraisal\AppraisalComponentType;
use App\Models\Appraisal\AppraisalType;
use App\Models\User;

class AppraisalController extends Controller
{
    public function index()
    {
        $appraisals = Appraisal::orderBy('created_at','DESC')->get();

        return view('pages.appraisal.appraisal.index', compact('appraisals'));
    }

    public function show($id)
    {
        $appraisal = Appraisal::find($id);
        $bonusTypes = AppraisalBonusType::all();
        if (!count($bonusTypes) > 0) {
            return redirect()->back()->withErrors('No Bonus type Found.')->withInput();
        }
        if (!count($appraisal) > 0) {
            return redirect()->back()->withErrors('No Appraisal type Found.')->withInput();
        }
        return view('pages.appraisal.appraisal.show', compact('appraisal', 'bonusTypes'));
    }
    public function addAppraisal()
    {
        $appraisalTypes = AppraisalType::all();
        if (!count($appraisalTypes) > 0) {
            return redirect()->back()->withErrors('No Appraisal type Found .Please Add Appraisal type First')->withInput();
        }
        $appraisalComponentTypes = AppraisalComponentType::all();

        $users = User::where('is_active', 1)->orderBy('name', 'asc')->get();

        $appraisalBonusType = AppraisalBonusType::all();

        return view('pages.appraisal.appraisal.add', compact('appraisalComponentTypes', 'users', 'appraisalTypes', 'appraisalBonusType'));

    }
    public function storeAppraisal(ValidateAppraisalRequest $request)
    {

        $data = $request->all();
        if (!$data) {
            return redirect()->back()->withErrors('Input Not Found')->withInput();

        }
        
        // $lastAppraisal = Appraisal::where('user_id', $data['user_id'])->orderBy('id', 'desc')->first();
        // if ($lastAppraisal && $lastAppraisal->effective_date > $data['effective_date']) {
        //     return redirect()->back()->withErrors('Current effective date should be greater than last effective date')->withInput();
        // }
        $appraisalData = [];
        $appraisalData['user_id'] = isset($data['user_id']) ? $data['user_id'] : '';
        $appraisalData['type_id'] = isset($data['type_id']) ? $data['type_id'] : '';
        $appraisalData['effective_date'] = isset($data['effective_date']) ? $data['effective_date'] : '';

        $appraisal = new Appraisal();

        $checkAppraisal = Appraisal::where('user_id', $appraisalData['user_id'])->where('type_id', $appraisalData['type_id'])->where('effective_date', $appraisalData['effective_date'])->first();
        if ($checkAppraisal) {
            return redirect()->back()->withErrors('Appraisal already exists!')->withInput();
        }

        if ($appraisal->validate($appraisalData)) {
            $appraisal->user_id = $appraisalData['user_id'];
            $appraisal->type_id = $appraisalData['type_id'];
            $appraisal->effective_date = !empty($appraisalData['effective_date']) ? $appraisalData['effective_date'] : null;

            if (!$appraisal->save()) {
                return redirect()->back()->withErrors('Inputs Not Found!')->withInput();
            }

            if (isset($data['component'])) {
                $appraisalComponents = $data['component'];
                if (count($appraisalComponents) > 0) {
                    foreach ($appraisalComponents as $key => $component) {
                        if ($component) {
                            $appraisalComponent = new AppraisalComponent();
                            $appraisalComponent->appraisal_id = $appraisal->id;
                            $appraisalComponent->component_id = $key;
                            $appraisalComponent->value = $component;

                            if (!$appraisalComponent->save()) {
                                return redirect()->back()->withErrors('Something went wrong!')->withInput();

                            }
                        }
                    }
                }
            }
            if (isset($data['bonus'])) {
                $bonuses = $data['bonus'];
                foreach ($bonuses as $key => $bonus) {
                        $appraisalBonus = AppraisalBonus::create(['appraisal_id' => $appraisal->id,'appraisal_bonus_type_id' => $key, 'value' => $bonus['value'] ? $bonus['value'] : 0, 'value_date' => !empty($bonus['value_date']) ? $bonus['value_date'] : null]);
                        if (!$appraisalBonus->isValid()) {
                            return redirect()->back()->withErrors($appraisalBonus->getErrors())->withInput();
                        }
                }
            }
            return redirect('admin/appraisal')->withMessage('Saved Successfully');

        } else {
            $errors = $appraisal->getError();
            return redirect()->back()->withErrors($errors)->withInput();

        }

    }

    public function edit($id)
    {
        $appraisal = Appraisal::with('user', 'type')->find($id);
        if (!$appraisal) {
            return redirect('admin/appraisal')->withErrors('Appraisal Not found')->withInput();
        }
        $users = User::where('is_active', 1)->orderBy('name', 'asc')->get();
        $appraisalTypes = AppraisalType::all();

        $appraisalComponentTypes = AppraisalComponentType::where('is_computed',0)->get();
        $appraisalComponent = $appraisal->appraisalComponent;

        $appraisalComponentObj = [];
        foreach ($appraisalComponentTypes as $index => $type) {
            if($type->is_computed == 0){
                $appraisalComponentObj[$index]['name'] = $type->name;
                $appraisalComponentObj[$index]['id'] = $type->id;
                $appraisalComponentObj[$index]['value'] = 0;

                $appraisalBonusType = AppraisalBonusType::all();

                foreach ($appraisalComponent as $component) {
                    if ($component->component_id === $type->id) {
                        $appraisalComponentObj[$index]['value'] = $component->value;
                    }

                }
            }
        }

        $appraisalBonus = $appraisal->appraisalBonus;

        $appraisalBonusObj = [];
        foreach ($appraisalBonusType as $index => $type) {
            $appraisalBonusObj[$index]['name'] = $type->description;
            $appraisalBonusObj[$index]['id'] = $type->id;
            $appraisalBonusObj[$index]['value'] = 0;
            $appraisalBonusObj[$index]['value_date'] = 0;

            foreach ($appraisalBonus as $bonus) {
                if ($bonus->appraisal_bonus_type_id === $type->id) {
                    $appraisalBonusObj[$index]['value'] = $bonus->value;
                    $appraisalBonusObj[$index]['value_date'] = $bonus->value_date;

                }

            }

        }

        return view('pages.appraisal.appraisal.edit', compact('appraisal', 'users', 'appraisalTypes', 'appraisalComponentTypes', 'appraisalComponent', 'appraisalComponentObj', 'appraisalBonusType', 'appraisalBonusObj'));
    }

    public function update(ValidateAppraisalRequest $request, $id)
    {
        $data = $request->all();
        $appraisalObj = Appraisal::find($id);

        if (!$appraisalObj) {
            return redirect('admin/appraisal')->back()->withErrors('Appraisal not found!')->withInput();
        }
        $appraisalObj->user_id = $data['user_id'];
        $appraisalObj->effective_date = $data['effective_date'];
        $appraisalObj->type_id = $data['type_id'];
        if (!$appraisalObj->update()) {
            return redirect()->back()->withErrors('Something went wrong!')->withInput();
        }
        if (isset($data['component'])) {
            $appraisalComponent = $data['component'];

            foreach ($appraisalComponent as $key => $component) {
                if (isset($component)) {
                    $appraisalComponent = AppraisalComponent::where('appraisal_id', $appraisalObj->id)->where('component_id', $key)->first();
                    if ($appraisalComponent) {
                        $appraisalComponent->appraisal_id = $appraisalObj->id;
                        $appraisalComponent->component_id = $key;
                        $appraisalComponent->value = $component;
                        if (!$appraisalComponent->save()) {
                            return redirect()->back()->withErrors('Something went wrong!')->withInput();

                        }
                    } else {
                        $appraisalComponent = new AppraisalComponent();
                        $appraisalComponent->appraisal_id = $appraisalObj->id;
                        $appraisalComponent->component_id = $key;
                        $appraisalComponent->value = $component;

                        if (!$appraisalComponent->save()) {
                            return redirect()->back()->withErrors('Something went wrong!')->withInput();

                        }
                    }
                }
            }
        }

        $errors = "";
        $bonusStatus = false;
        if (isset($data['bonus'])) {
            $bonuses = $data['bonus'];
            foreach ($bonuses as $key => $bonus) {
                $obj = AppraisalBonus::where('appraisal_id',$appraisalObj->id)->where('appraisal_bonus_type_id', $key)->first();
                if($obj)
                {
                    $obj->appraisal_id = $appraisalObj->id;
                    $obj->appraisal_bonus_type_id = $key;
                    $obj->value = $bonus['value'] ? $bonus['value'] : 0;
                    $obj->value_date = !empty($bonus['value_date']) ? $bonus['value_date'] : null;
                    if(!$obj->save())
                    {
                        $errors = $errors.$obj->getErrors();
                        $bonusStatus = true;
                    }
                }
            }

        }
        if($bonusStatus)
        {
            return redirect()->back()->withErrors($errors)->withInput();
        }
        return redirect()->back()->with('message', 'Updated!');
    }

    public function store(ValidateAppraisalRequest $request)
    {
        $data = $request->all();
        $appraisalObj = new Appraisal();
        if ($appraisalObj->validate($data)) {
            $appraisalObj->user_id = !empty($data['user_id']) ? $data['user_id'] : null;
            $appraisalObj->effective_date = !empty($data['effective_date']) ? $data['effective_date'] : null;
            $appraisalObj->type_id = !empty($data['type_id']) ? $data['type_id'] : null;
            if (!$appraisalObj->save()) {
                return redirect()->back()->withErrors('Something went wrong!')->withInput();
            }
            return redirect()->back()->with('message', 'Saved Successfully');
        } else {
            $errors = $appraisalObj->getError();
            return redirect()->back()->withErrors($errors)->withInput();
        }
    }

    public function destroy($id)
    {
        $appraisalObj = Appraisal::find($id);
        if ($appraisalObj) {
            if ($appraisalObj->delete()) {
                return redirect('/admin/appraisal')->with('message', 'Successfully Deleted');
            }
        }
        return redirect()->back()->withErrors('Appraisal Not Found!')->withInput();
    }
}
