<?php

namespace App\Http\Controllers\Admin\Appraisal;

use App\Http\Controllers\Controller;
use App\Models\Appraisal\AppraisalComponentType;
use Illuminate\Http\Request;

class AppraisalComponentTypeController extends Controller
{
    public function index()
    {
        $appraisalComponentTypes = AppraisalComponentType::paginate(50);
        if (!$appraisalComponentTypes) {
            return view('pages.appraisal.appraisal-component-type.index')->with('message', 'no records found.');
        }

        return view('pages.appraisal.appraisal-component-type.index', compact('appraisalComponentTypes'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $appraisalComponentTypeObj = AppraisalComponentType::create(['name' => $data['name'], 'code' => $data['code'], 'is_computed' => array_key_exists("is_computed", $data) ? $data['is_computed'] ? $data['is_computed'] : false : null, 'is_company_expense' => array_key_exists("is_company_expense", $data) ? $data['is_company_expense'] ? $data['is_company_expense'] : false : null, 'is_conditional_prorata' => array_key_exists("is_conditional_prorata", $data) ? $data['is_conditional_prorata'] ? $data['is_conditional_prorata'] : false : null, 'prorata_function' => $data['prorata_function']]);
        if ($appraisalComponentTypeObj->isValid()) {
            if ($appraisalComponentTypeObj->save()) {
                return redirect()->back()->with('message', 'Saved Successfully');
            }
            return redirect()->back()->withErrors('Unable To Save')->withInput();
        }
        return redirect()->back()->withErrors($appraisalComponentTypeObj->getErrors())->withInput();
    }

    public function show($id)
    {
        $appraisalComponentType = AppraisalComponentType::find($id);
        if (!$appraisalComponentType) {
            return redirect('/admin/appraisal/appraisal-component-type')->withErrors('Appraisal component type not found!')->withInput();
        }
        return view('pages.appraisal.appraisal-component-type.edit', compact('appraisalComponentType'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $appraisalComponentTypeObj = AppraisalComponentType::updateOrCreate(['id' => $id], ['name' => $data['name'], 'code' => $data['code'], 'is_computed' => $data['is_computed'] ? $data['is_computed'] : false, 'is_company_expense' => $data['is_company_expense'] ? $data['is_company_expense'] : false, 'is_conditional_prorata' => $data['is_conditional_prorata'] ? $data['is_conditional_prorata'] : false, 'prorata_function' => $data['prorata_function']]);
        if ($appraisalComponentTypeObj->isValid()) {
            if ($appraisalComponentTypeObj->update()) {
                return redirect('/admin/appraisal/appraisal-component-type')->with('message', 'Updated!');
            }
            return redirect()->back()->withErrors('Unable To Update')->withInput();
        }
        return redirect()->back()->withErrors($appraisalComponentTypeObj->getErrors())->withInput();
    }

    public function destroy($id)
    {
        $appraisalComponentTypeObj = AppraisalComponentType::find($id);
        if ($appraisalComponentTypeObj && $appraisalComponentTypeObj->delete()) {
            return redirect('/admin/appraisal/appraisal-component-type')->with('message', 'Successfully Deleted');
        }
        return redirect()->back()->withErrors('Something went wrong!')->withInput();
    }
}
