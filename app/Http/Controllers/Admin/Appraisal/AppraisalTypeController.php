<?php

namespace App\Http\Controllers\Admin\Appraisal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Appraisal\AppraisalType;

class AppraisalTypeController extends Controller
{
    public function index()
    {
        $appraisalTypes = AppraisalType::paginate(50);
        if (!$appraisalTypes) {
            return view('pages.appraisal.appraisal-type.index')->with('message', 'no records found.');
        }
        return view('pages.appraisal.appraisal-type.index', compact('appraisalTypes'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $appraisalTypeObj = new AppraisalType();
        $appraisalTypeObj->name = $data['name'];
        $appraisalTypeObj->code = $data['code'];
        if($appraisalTypeObj->isValid())
        {
            if ($appraisalTypeObj->save()) {
                return redirect('/admin/appraisal/appraisal-type')->with('message', 'save successfully!');
            }
            return redirect()->back()->withErrors('Unable To Save')->withInput();
        }
        return redirect('/admin/appraisal/appraisal-type')->withErrors($appraisalTypeObj->getErrors() )->withInput();
       
    }

    public function show($id)
    {
        $appraisalType = AppraisalType::find($id);
        if (!$appraisalType) {
            return redirect('/admin/appraisal/appraisal-type')->withErrors('Appraisal type not found!')->withInput();
        }
        return view('pages.appraisal.appraisal-type.edit', compact('appraisalType'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $appraisalTypeObj = AppraisalType::find($id);
        if (!$appraisalTypeObj) {
            return redirect()->back()->withErrors('Appraisal type not found!')->withInput();
        }
        $appraisalTypeObj->name = $data['name'];
        $appraisalTypeObj->code = $data['code'];
        if($appraisalTypeObj->isValid())
        {
            if ($appraisalTypeObj->update()) {
                return redirect('/admin/appraisal/appraisal-type')->with('message', 'Updated!');
            }
            return redirect()->back()->withErrors('Unable To Update')->withInput();
        }
        return redirect('/admin/appraisal/appraisal-type')->withErrors($appraisalTypeObj->getErrors() )->withInput();
    }

    public function destroy($id)
    {
        $appraisalTypeObj = AppraisalType::find($id);
        if ($appraisalTypeObj && $appraisalTypeObj->delete()) {
            return redirect('/admin/appraisal/appraisal-type')->with('message', 'Successfully Deleted');
        }
        return redirect()->back()->withErrors('Something went wrong!')->withInput();
    }
}
