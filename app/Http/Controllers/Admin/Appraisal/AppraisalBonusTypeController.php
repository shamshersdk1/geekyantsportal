<?php

namespace App\Http\Controllers\Admin\Appraisal;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Redirect;
use App\Models\Appraisal\AppraisalBonusType;

class AppraisalBonusTypeController extends Controller
{
    public function index()
    {
        $appraisalBonusType=AppraisalBonusType::all();
        return view('pages.appraisal.appraisal-bonus-type.index', compact('appraisalBonusType'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        if(!$data){
            return redirect()->back()->withErrors('Missing Input')->withInput();
        }
        $appraisalBonusType = new AppraisalBonusType();
        
        $appraisalBonusType->code=$data['code'];
        $appraisalBonusType->description=$data['description'];   
        $appraisalBonusType->is_loanable = $data['is_loanable'];
        if($appraisalBonusType->isValid())
        {
            if($appraisalBonusType->save()){
                return redirect()->back()->withMessage('Successfully Saved!');
            }
            return redirect()->back()->withErrors('Unable To Save')->withInput();
        }
        return redirect()->back()->withErrors($appraisalBonusType->getErrors())->withInput();        
    }

    public function edit($id){
        $appraisalBonusType = AppraisalBonusType::find($id);
        if(! $appraisalBonusType){
            return redirect('/admin/appraisal/appraisal-bonus-type')->withErrors('Appraisal bonus type not found!')->withInput();
        }
        return view('pages.appraisal.appraisal-bonus-type.edit', compact('appraisalBonusType'));
    }

    public function update(Request $request, $id)
    {
        $appraisalBonusObj = AppraisalBonusType::find($id);
        $data = $request->all();
        if (!$appraisalBonusObj) {
            return redirect()->back()->withErrors('Bonus Type not found!')->withInput();
        }
        $appraisalBonusObj->code = $data['code'];
        $appraisalBonusObj->description = $data['description'];
        $appraisalBonusObj->is_loanable = $data['is_loanable'];
        if($appraisalBonusObj->isValid())
        {
            if ($appraisalBonusObj->save()) {
                return redirect('/admin/appraisal/appraisal-bonus-type')->with('message', 'Update Successful!');
            }
            return redirect()->back()->withErrors('Unable To Update')->withInput();
        }
        return redirect()->back()->withErrors($appraisalBonusObj->getErrors())->withInput();
    } 

    public function destroy( $id)
    {
        $appraisalBonusType = AppraisalBonusType::find($id);
        if ($appraisalBonusType) {
            if(!$appraisalBonusType->delete()){
                return redirect()->back()->withErrors('Appraisal Bonus type cannot be deleted!')->withInput();
            }
            return redirect()->back()->with('message', 'Successfully Deleted');
        }
        return redirect()->back()->withErrors('Appraisal Bonus type not found')->withInput();
    }
}
