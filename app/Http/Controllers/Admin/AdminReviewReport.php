<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Calendar;
use App\Models\Admin\Leave;
use App\Models\Admin\Project;
use App\Models\BonusRequest;
use App\Models\Month;
use App\Models\User;
use App\Models\UserTimesheet;
use App\Models\User\UserTimesheetExtra;
use App\Services\TimesheetReportService;
use Excel;
use Illuminate\Http\Request;
use Redirect;
use Auth;

class AdminReviewReport extends Controller
{
 public function index()
 {
  //Request Input Data
  $employee_id = request('employee_id', $default = null);
  $month       = request('month', $default = date('m', strtotime(date('Y-m') . " -1 month")));

  $output                = TimesheetReportService::getEmployeeWise($employee_id, $month);
  $output['user_report'] = false;
  //Render File

  return view('pages.admin.admin-review-report.index', $output);
 }
 public function MisReportMonth()
 {

  $months = Month::orderBy('year', 'DESC')->orderBy('month', 'DESC')->get();
  return View('pages.admin.admin-review-report.mis-report-month', compact('months'));
 }

 public function UserReportMIS($monthId, $userId)
 {

  $authUser = \Auth::User();
  $user     = User::find($userId);
  if ($authUser->role != 'admin' && $user && $user->parent_id != $authUser->id) {
   return redirect::back()->withErrors('Unauthorized Access');
  }
  $monthObj = Month::find($monthId);

  //Request Input Data
  //$employee_id = \Auth::user()->employee_id;

  $month  = request('month', $default = date('m', strtotime(date('Y-m') . " -1 month")));
  $output = TimesheetReportService::getEmployeeWise($user->employee_id, $month);

  $output['user_report'] = true;
  //Render File
  return view('pages.admin.admin-review-report.index', compact('output', 'monthObj', 'user'));

 }
 public function MISReports($monthId)
 {

  $monthObj = Month::find($monthId);

  if (!$monthObj) {
   return 0;
  }

  $month_start_date = $monthObj->getFirstDay();
  $month_end_date   = $monthObj->getLastDay();

  $users = TimesheetReportService::getUserTimesheet($month_start_date, $month_end_date);

  return View('pages.admin.admin-review-report.mis-reports', compact('monthObj', 'users'));

 }
 public function MisReport($monthId)
 {
  ///new  month
  $monthObj = Month::find($monthId);
  $user     = \Auth::User();
  if (!$monthObj) {
   return redirect::back()->withErrors('Invalid Month ID');
  }

  //$month = request('month', $default = date('m', strtotime(date('Y-m') . " -1 month")));

  if (!$user) {
   return redirect::back()->withErrors('Unauthorized Access');
  }

  // if ($user->role == 'admin') {
  //  $output['users'] = User::where('is_active', 1)->orderBy('employee_id', 'ASC')->with('reportees')->get();
  // } else {
  //  $output['users'] = User::where('is_active', 1)->where('parent_id', $user->id)->orderBy('employee_id', 'ASC')->with('reportees')->get();
  // }
  // $ids =[];
  // foreach ($output['users'] as $user) {
  //   $ids[] = $user->id;
  // } 
  $month_start_date = $monthObj->getFirstDay();
  $month_end_date   = $monthObj->getLastDay();

  $timesheetDetails;
  $output['users'] = User::where('is_active', 1)->orderBy('employee_id', 'ASC')->with('reportees')->get();
  if ($user->role == 'admin') {
   $timesheetDetails = TimesheetReportService::getEmployeeTimesheeetDetail($month_start_date, $month_end_date);
 }  
 else {
   $user=User::where('parent_id',$user->id)->get();

   $userIds=[];
   if(count($user)>0){
    foreach($user as $user){
        $userIds[]=$user->id;
    }
   }
 $timesheetDetails = TimesheetReportService::getEmployeeTimesheeetDetail($month_start_date, $month_end_date,$userIds);
}

  
  // $output = array();
  // $output['users'] = User::where('is_active', 1)->orderBy('employee_id', 'ASC')->with('reportees')->get();
  // $users = $output['users'];
  // $output['user_timesheet_map'] = array();
  // $output['month'] = $monthObj;
  // $output['month_string'] = "--"; //\DateTime::createFromFormat('!m', $month)->format('F');
  // foreach ($output['users'] as $user) {
  //     $childIds = [];
  //     foreach ($user->reportees as $reportee) {
  //         $childIds[] = $reportee->id;
  //     }
  //     if (count($childIds) > 0) {
  //         $childIds = implode($childIds, ',');
  //         $timesheetDetail = TimesheetReportService::getEmployeeTimesheeetDetail($month_start_date, $month_end_date, $childIds);

  //         foreach ($timesheetDetail as $timesheetData) {
  //             $totalHour = 0;
  //             $approvedHour = 0;
  //             $timesheetData = (array) $timesheetData;
  //             if (!empty($timesheetData['total_hour'])) {
  //                 $totalHour = $timesheetData['total_hour'];
  //                 $approvedHour = $timesheetData['total_hour'] - $timesheetData['total_extra_hour'];
  //             }
  //             $output['user_timesheet_map'][$reportee->id]['employee_total_time'] = $totalHour;
  //             $output['user_timesheet_map'][$reportee->id]['approved_total_time'] = $approvedHour;
  //         }

  //     }
  //     //$data = TimesheetReportService::getUserTimesheet($month_start_date, $month_end_date, $user->id);
  //     //$data['users'] = TimesheetReportService::getEmployeeTimesheeetDetail($month_start_date, $month_end_date);
  //     // $output['user_timesheet_map'][$user->id]['employee_total_time'] = $data['employee_total_time'];
  //     // $output['user_timesheet_map'][$user->id]['approved_total_time'] = $data['approved_total_time'];

  // }

  //$data['users'] = TimesheetReportService::getEmployeeTimesheeetDetail($month_start_date, $month_end_date);
//  echo"<pre>";
//  print_r($timesheetDetails);
//  die;
  return View('pages.admin.admin-review-report.mis-reports', compact('output', 'monthObj', 'users', 'data', 'timesheetDetails'));
 }

 public function UserReport(Request $request)
 {
  $defaultMonth = Month::orderBy('year', 'DESC')->orderBy('month', 'DESC')->first();
  $users        = \Auth::user();

  $role = $users->role;
 
  if ($users->hasRole('admin') || $users->hasRole('reporting-manager')) {
    $id   = $request->get('user');
   $user = User::where('id', $id)->first();

   if (!$user) {
    $employee_id = $users->employee_id;
    $user_id     = $users->id;
   } else {
    $employee_id = $user->employee_id;
    $user_id     = $user->id;

   }

  } else {
   $employee_id = $users->employee_id;
   $user_id     = $users->id;

  }
 
  //$month = request('month', $default = date('m', strtotime(date('Y-m') . " -1 month")));
  $monthId = request('monthId');
  if (!$monthId) {
   $monthId = $defaultMonth->id;
  }
  $monthObj = Month::find($monthId);

  if ($monthObj) {
   $start_date = $monthObj->getFirstDay();
   $end_date   = $monthObj->getLastDay();
  }

  // $monthList = [];
  $monthList = Month::OrderBy('year', 'desc')->OrderBy('month', 'desc')->with('financialYear')->get();

  $output    = TimesheetReportService::getEmployeeWise($employee_id, $monthId);
  $project   = TimesheetReportService::getProjectWiseTimesheetDetail($start_date, $end_date, $user_id);
  $timesheet = TimesheetReportService::getEmployeeTimesheeetDetail($start_date, $end_date, $user_id);

  $user                              = User::where('is_active', 1)->get();
  $output['user_report']             = true;
  $userExtra                         = UserTimesheetExtra::where('user_id', $user_id)->whereBetween('date', [$start_date, $end_date])->get();
  $data['bonuses']['bonus_requests'] = BonusRequest::with('approvedBy', 'approvedBonus.approver', 'approvedBonus.referral')->where('user_id', $user_id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'ASC')->get();

 if($users->hasRole('reporting-manager')){
   $parentUser=$users->reportees;
 }


  return view('pages.admin.admin-review-report.user-report', compact('output', 'monthList', 'user', 'role', 'project', 'timesheet', 'userExtra', 'data','parentUser'));
 }

 public function ProjectReport()
 {
  $projectList = Project::All();
  $monthList   = Month::OrderBy('year', 'desc')->OrderBy('month', 'desc')->with('financialYear')->get();

  $project_id = request('project_id', $default = null);
  $month      = request('month', $default = $monthList[0]->id);

  $output                = TimesheetReportService::getProjectWise($project_id, $month);
  $output['user_report'] = false;

  // dd($output['user_timesheet']);
  $monthObj   = Month::find($month);
  $start_date = $monthObj->getFirstDay();
  $end_date   = $monthObj->getLastDay();

  $result = TimesheetReportService::getUserProjectWiseDetails($start_date, $end_date, $project_id);
  // $data['bonuses']['bonus_requests'] = BonusRequest::with('user', 'approvedBonus.approver', 'approvedBonus.referral')->where('user_id', $userId)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'ASC')->get();
  $userExtra = UserTimesheetExtra::with('user')->where('project_id', $project_id)->whereBetween('date', [$start_date, $end_date])->get();
 
  return view('pages.admin.admin-review-report.projectwise', compact('output', 'projectList', 'monthList', 'result', 'userExtra'));
 }

 public function MisProjectReport()
 {
  return view('pages.admin.admin-review-report.mis-report-project');
 }
 public function DownloadProjectReport()
 {
  $project_id = request('project_id', $default = null);
  $month      = request('month', $default = null);

  $timesheetReport = TimesheetReportService::getProjectWise($project_id, $month);
  //if ($timesheetReport['total_approved_hrs'] > 0) {
  if ($timesheetReport['total_employees_hrs'] > 0) {

   $UserTimesheet = UserTimesheet::where([['project_id', $project_id],
    ['status', '!=', 'rejected'],
    ['date', '>=', $timesheetReport['month_start_date']],
    ['date', '<=', $timesheetReport['month_end_date']]])
    ->groupBy('user_id')->pluck('user_id')->toArray();

   $users = User::whereIn('id', $UserTimesheet)->get();

   //$head = [$timesheetReport['total_approved_hrs']];
   $head  = [$timesheetReport['total_employees_hrs']];
   $head1 = ['Total'];

   foreach ($users as $resource) {
    $head  = array_merge($head, ['Hrs', $resource->name]);
    $usr   = TimesheetReportService::getEmployeeWiseTimesheetSummary($resource->id, $month);
    $head1 = array_merge($head1, [$usr['employee_hrs'], '']);
   }
   $export_data = [$head, $head1];
   while ($timesheetReport['month_start_date'] <= $timesheetReport['month_end_date']) {
    $isHoliday = false;
    $isWeekend = false;
    $isLeave   = false;

    $value = [date("D d M,Y", strtotime($timesheetReport['month_start_date']))];
    if (Calendar::whereDate('date', $timesheetReport['month_start_date'])->count() > 0) {
     $isHoliday = true;

    } elseif (date('N', strtotime($timesheetReport['month_start_date'])) >= 6) {
     $isWeekend = true;

    }
    foreach ($users as $resource) {
     $UserTimesheet = UserTimesheet::where([['user_id', $resource->id], ['project_id', $project_id], ['date', $timesheetReport['month_start_date']], ['status', '!=', 'rejected']])
      ->with('review')->get();
     if ($UserTimesheet->isNotEmpty()) {
      $task     = [];
      $duration = null;
      foreach ($UserTimesheet as $timesheet) {
        array_push($task, $timesheet->task);
        $duration += $timesheet->duration;
       
      }
      $value = array_merge($value, [$duration, implode(', ', $task)]);

     } else {
      if ($isWeekend) {
       $value = array_merge($value, ["Weekend", ""]);
      } elseif ($isHoliday) {
       $value = array_merge($value, ["HOLIDAY", ""]);
      } elseif (Leave::isOnLeaveToday($resource->id, $timesheetReport['month_start_date'])) {
       $value = array_merge($value, ["Leave", ""]);
      } else {
       $value = array_merge($value, ["0", ""]);
      }
     }
    }
    array_push($export_data, $value);
    $timesheetReport['month_start_date']++;
   }
   $filename = $timesheetReport['project']['project_name'] . "_" . $timesheetReport['month'] . "_" . $timesheetReport['year'];
   return Excel::create($filename, function ($excel) use ($export_data) {
    $excel->sheet('mySheet', function ($sheet) use ($export_data) {
     $sheet->fromArray($export_data);
    });
   })->download('csv');
  }return Redirect::back()->withErrors(['No data for the given month']);
 }
 public function userReportMISReview($monthId, $userId)
 {
  $authUser = \Auth::User();
  $user     = User::find($userId);
  if ($authUser->role != 'admin' && $user && $user->parent_id != $authUser->id) {
   return redirect::back()->withErrors('Unauthorized Access');
  }
  //check for admin and RM access
  $user     = User::find($userId);
  $monthObj = Month::find($monthId);

  $start_date = $monthObj->getFirstDay();
  $end_date   = $monthObj->getLastDay();
  //$data = TimesheetReportService::getUserTimesheetDetail($userId, $monthId);

  $data['can_approve'] = true;
  if (!empty($data['bonuses']['bonus_requests'])) {
   foreach ($data['bonuses']['bonus_requests'] as $bonus) {
    $bonus->redeem_type = json_decode($bonus->redeem_type, true);
    if ($bonus->status == 'pending') {
     $data['can_approve'] = false;
     break;
    }

   }
  }

  $data['timesheet_overview'] = TimesheetReportService::getEmployeeTimesheeetDetail($start_date, $end_date, $userId);
  $data['timesheet_details']  = TimesheetReportService::getDayWiseUserTimesheeetDetail($userId, $start_date, $end_date);

  return View('pages.admin.admin-review-report.mis-report-month-review', compact('monthObj', 'data', 'user', 'pendingBonuseRequest', 'userMonthlyTimesheet'));

 }
}
