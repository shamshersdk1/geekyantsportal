<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Seat;
use Validator;
use Redirect;
use Route;
use Carbon\Carbon;

class SeatController extends Controller
{
    
    public function index()
    {
        $floors = Seat::floorList();
        return view('pages/admin/seat/index',  ['floors' => $floors ]);
    }

    public function show($id)
    {
        $allocation_details = Seat::floorDetails($id);
        $seats = json_encode(Seat::with('user','user_profile')->where('floor',$id)->orderBy('name')->get());
        $users = json_encode(User::where('is_active',1)->get());
        
        return view('pages/admin/seat/show',  ['seats' => $seats, 'users' => $users, 'floor' => $id, 'allocation_details' => $allocation_details]);
    }
    
}
