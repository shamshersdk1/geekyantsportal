<?php namespace App\Http\Controllers\Admin\ReportingManager;

use App\Http\Controllers\Controller;
use App\Models\Timesheet\UserTimesheetLock;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Redirect;
use App\Models\Month;

class MyTeamController extends Controller
{
    public function index()
    {
        $rm = Auth::user();
        if (!($rm->hasRole('reporting-manager') || $rm->hasRole('account') || $rm->hasRole('admin'))) {
            return redirect('/admin/my-team')->withErrors('Invaled Access');
        }

        if ($rm->hasRole('admin') && $rm->role == 'admin') {
            $users = User::where('is_active', 1)->paginate(200);
        } else if ($rm->hasRole('reporting-manager')) {
            $users = User::where('parent_id', $rm->id)->where('is_active', 1)->paginate(200);
        } else {
            $users = [];
        }
        if (!$users) {
            return redirect('/admin/my-team')->withErrors('Users not found');
        }
        $monthId=Month::getMonth(date('Y-m-d H:i:s'));
       

        return view('pages/admin/reporting-manager/my-team/index', compact('users','monthId'));
    }

    public function editUserDetail($userId)
    {
        $user = User::find($userId);

        if (!$user) {
            return redirect('/admin/my-team')->withErrors('Users not found');
        }

        $userLockObj = UserTimesheetLock::where('user_id', $userId)->first();

        return view('pages/admin/reporting-manager/my-team/edit', compact('user', 'userLockObj'));
    }

    public function saveUserLock(Request $request, $userId)
    {
        $data = $request->all();

        $lockObj = UserTimesheetLock::where('user_id', $userId)->first();

        if ($data['billing_date'] && $data['lock_date'] < $data['billing_date']) {
            return redirect('/admin/my-team/' . $userId . '/edit')->withErrors('Lock Date cannot be less than Billing Lock Date');
        }

        if (!$lockObj) {
            return redirect('/admin/my-team/' . $userId . '/edit')->withErrors('User\'s timesheet lock entry not found');
        }
        $dt = date_to_yyyymmdd($data['lock_date']);
        $lockObj->date = $dt;
        $lockObj->billing_date = $data['billing_date'];

        if ($lockObj->save()) {
            return redirect::back()->with('message', 'Successfully updated');
        }

        return redirect('/admin/my-team/' . $userId . '/edit')->withErrors('User\'s timesheet lock not updated');
    }

}
