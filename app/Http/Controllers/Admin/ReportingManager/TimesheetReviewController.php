<?php namespace App\Http\Controllers\Admin\ReportingManager;

use App\Http\Controllers\Controller;
use App\Models\BonusRequest;
use App\Models\User;
use App\Models\User\UserTimesheetWeek;
use App\Models\Week;
use App\Services\TimesheetReportService;
use Auth;

class TimesheetReviewController extends Controller
{
 public function showWeeks()
 {
  $user = Auth::User();
  $role = $user->role;

  $weekObj = Week::orderBy('start_date', 'DESC')->paginate(60);

  // $weekObj = [];
  // foreach ($weeks as $week) {

  //     $weekObj[] = Week::with('pending')->find($week->id);

  // }

  return view('pages/admin/reporting-manager/timesheet/index')->with('weeks', $weekObj);

 }
 public static function lockWeek($weekId)
 {
  $user = Auth::user();
  if (!$user->hasRole('admin')) {
   return redirect('/admin/timesheet-review')->withErrors('Invalid User Acess');

  }
  $reviewed_at = date("Y-m-d h:i:s");

  if (!$weekId) {
   return redirect('/admin/timesheet-review')->withErrors('Invalid Week #' . $weekId);
  }
  $weekObj = Week::find($weekId);

  if (!$weekObj) {
   return redirect('/admin/timesheet-review')->withErrors('Week Not Found');

  }
  $weekObj->is_locked = 1;
  $weekObj->locked_at = $reviewed_at;

  if (!$weekObj->save()) {
   return redirect('/admin/timesheet-review')->withErrors('Week Cannot Be locked');
  }
  return redirect('/admin/timesheet-review');

 }
 public function showWeeklyUser($weekId)
 {
  $weekObj = Week::find($weekId);
  if (!$weekObj) {
   return redirect('/admin/timesheet-review')->withErrors('Invalid Week #' . $weekId);
  }

  $user  = Auth::User();
  $users = UserTimesheetWeek::getRMUsers($weekId, $user->id);
  if (!($user->hasRole('') || $user->hasRole('reporting-manager'))) {
   return redirect('/admin/dashboard')->withErrors('Invalid access!');
  }

  $users = BonusRequest::where('month_id', $monthId)->orderBy('id', 'DESC')->get();
  return view('pages/admin/bonus-request/all', compact('monthId', 'monthName'));

 }
 public function showUsers($weekId)
 {
  // $date = '2019-03-25';
  // Week::addWeek($date);
  $weekObj = Week::find($weekId);
  if (!$weekObj) {
   return redirect('/admin/timesheet-review')->withErrors('Invalid week ID #' . $weekId);
  }
  if (Auth::User()->role == 'admin') {
   $users = UserTimesheetWeek::where('week_id', $weekObj->id)->get();
  } else {
   $users = UserTimesheetWeek::where('week_id', $weekObj->id)->where('parent_id', Auth::User()->id)->get();
  }
  return view('pages/admin/reporting-manager/timesheet/user-list', compact('users', 'weekObj'));

  /*
  $week = Week::where('id', $weekId);
  dd($week);
  if ($week->end_date < Carbon::now()->format('Y-m-d')) {
  $users = Auth::User()->reportees;
  } else {
  $users = Auth::User()->reportees;
  }
  }

  $success = 1;
  $data = TimesheetReportService::getUserWeeklyTimesheet($weekId, $userId);
  $weekObj = Week::find($weekId);
  $user = User::find($userId);
  $userWeeklyTimesheet = UserTimesheetWeek::where('user_id', $userId)->where('week_id', $weekId)->first();
  return view('pages/admin/reporting-manager/timesheet/user-report', compact('weekObj', 'data', 'userWeeklyTimesheet', 'user', 'success'));
   */
  // $data = TimesheetReportService::getUserWeeklyTimesheet($weekId, $userId);
  // echo "<pre>";
  // print_r($data);
  // die;
  // $monthObj = Month::find($monthId);
  // $monthName = date("M", mktime(0, 0, 0, $monthObj->month, 10)) . ' ' . $monthObj->year;
  // $bonuses = BonusRequest::where('month_id', $monthId)->where('status', 'pending')->orderBy('id', 'DESC')->get();
  // return view('pages/admin/bonus-request/index', compact('monthId', 'monthName'));

 }

 public function showWeekyUserReport($weekId, $userId)
 {

  $success  = 1;
  $authUser = Auth::User();

  $weekObj    = Week::find($weekId);
  $start_date = $weekObj->start_date;
  $end_date   = $weekObj->end_date;

  $data['timesheet_overview'] = TimesheetReportService::getEmployeeTimesheeetDetail($start_date, $end_date, $userId);
  //$data['timesheet_details'] = TimesheetReportService::getDayWiseUserTimesheeetDetail($userId, $start_date, $end_date);

  $user = User::find($userId);
  if ($authUser->role != 'admin' && $user->parent_id != $authUser->id) {
   return redirect('/admin/timesheet-review')->withErrors('Unauthorized Access');
  }
  $userWeeklyTimesheet = UserTimesheetWeek::where('user_id', $userId)->where('week_id', $weekId)->first();
  return view('pages/admin/reporting-manager/timesheet/user-report', compact('weekObj', 'data', 'userWeeklyTimesheet', 'user', 'success'));
 }
 public function show($id)
 {

  $months = Month::orderBy('id', 'DESC')->get();
  return view('pages/admin/bonus-request/months', compact('months'));
 }

 public function reviewWeekyUserReport($weekId, $userId)
 {

  $user     = User::find($userId);
  $authUser = Auth::User();
  $success  = 1;
  $weekObj  = Week::find($weekId);
  if ($authUser->role != 'admin' && $user->parent_id != $authUser->id) {
   return redirect('/admin/timesheet-review')->withErrors('Unauthorized Access');
  }
  $start_date = $weekObj->start_date;
  $end_date   = $weekObj->end_date;

  $userWeeklyTimesheet        = UserTimesheetWeek::where('user_id', $userId)->where('week_id', $weekId)->first();
  $data['timesheet_overview'] = TimesheetReportService::getEmployeeTimesheeetDetail($start_date, $end_date, $userId);
  $data['timesheet_details']  = TimesheetReportService::getDayWiseUserTimesheeetDetail($userId, $start_date, $end_date);

  $data['pending_bonuse_request']    = BonusRequest::where('user_id', $userId)->where('status', 'pending')->whereBetween('date', [$weekObj->start_date, $weekObj->end_date])->orderBy('date', 'ASC')->count();
  $data['bonuses']['bonus_requests'] = BonusRequest::with('user', 'approvedBonus.approver', 'approvedBonus.referral')->where('user_id', $userId)->whereBetween('date', [$weekObj->start_date, $weekObj->end_date])->orderBy('date', 'ASC')->get();

//   echo "<pre>";
  //   print_r(json_decode($data['bonuses']['bonus_requests'][0]->redeem_type)->hours);
  //   die;
  return view('pages/admin/reporting-manager/timesheet/review', compact('weekObj', 'data', 'userWeeklyTimesheet', 'user', 'success'));

 }
 public function updateUser($weekId)
 {
  $user     = Auth::user();
  $parentId = $user->id;

  TimesheetReportService::autoApproveCheck($weekId, $parentId);

  return redirect('/admin/timesheet-review/' . $weekId);
 }
    public function showRequests(){
        // $parentUser = Auth::user();
        // if ($parentUser && $parentUser->hasRole('reporting-manager')) {
        //     $userId      = $parentUser->id;
        //     $currentDate = date("Y-m-d");
        //     $endDate     = $currentDate;
        //     $startDate   = date('Y-m-d', strtotime("-30 days", strtotime($currentDate)));

        //     $users   = User::where('parent_id', $userId)->get();
        //     $userIds = [];
        //     if (count($users) > 0) {
        //         foreach ($users as $user) {
        //             $userIds[] = $user->id;
        //         }
        //         $data = TimesheetReportService::getpendingRequests($userIds, $startDate, $endDate);
        //     }
        // }
        // echo "<pre>";
        // print_r($data);
        // die;
        return view('pages/reporting-manager/timesheet-approval/index', compact('data'));
    }
}
