<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Profile;
use App\Models\User;
use App\Models\Admin\CmsProject;
use App\Models\Admin\ProfileProject;
use Redirect;

class ProfileController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $profile = Profile::where('user_id', $id)->first();
        if ( $profile ){

            $exists = false;
            if (!empty($request->slug)) {
                $profile = Profile::where('user_id', $id)->first();
                if ($profile) {
                    if ($profile->profile_url != $request->slug) {
                        $exists = Profile::existsSlug($request->slug);
                    }
                }
            } else {
                $profile = Profile::where('user_id', $id)->first();
                if ($profile) {
                    if ($profile->profile_url != str_replace(" ", "-", strtolower($profile->user->name))) {
                        $exists = Profile::existsSlug(str_replace(" ", "-", strtolower($profile->user->name)));
                    }
                }
            }
            if ($exists) {
                return Redirect::back()->withErrors(['slug already in use'])->withInput();
            }
            $result = Profile::saveData($request, $profile->id);
            
            if ($result) {
                return Redirect::back()->with('message', 'Successfully Updated');
            } else {
                return redirect::back();
            }

        } else {
            return Redirect::back()->withErrors(['User profile doesn\'t exist'])->withInput();
        }
    }
}
