<?php
namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Input;
use Validator;
use Redirect;
use App\Models\User;
use App\Models\Admin\LateComer;

use Auth;
use App\Models\Admin\PayroleGroup;

class LateComerController extends Controller
{
    public function index(Request $request)
    {   
        return view('pages/admin/late-comers/index');
    }

    public function create()
    {
        dd('Late comers create page will go here');
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        // 
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

}
