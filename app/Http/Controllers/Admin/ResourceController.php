<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Admin\Company;
use App\Models\Admin\Project;
use App\Models\Admin\ProjectNotes;
use App\Models\Admin\ProjectFiles;
use App\Models\Admin\ProjectSprints;
use App\Models\Admin\SprintResource;
use App\Models\Admin\Technology;
use App\Models\Admin\Timelog;
use App\Models\Admin\CmsTechnology;
use App\Models\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Redirect;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use Config;
use Exception;
use DB;
use Auth;
use Input;

class ResourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return View('pages/admin/resources/index');

        // $projects = Project::all();
        // return View('pages/admin/project/index', ['projects' => $projects]);
    }
    public function show() {
        $check_date = Input::get(['check_date']);
        $data = DB::table('users')
            ->join('sprint_resources', 'users.id', '=', 'sprint_resources.employee_id')
            ->join('project_sprints', 'sprint_resources.id', '=', 'project_sprints.id')
            ->join('projects', 'project_sprints.project_id', '=', 'projects.id')
            ->whereDate('project_sprints.start_date','<=',$check_date)
            ->whereDate('project_sprints.end_date','>=',$check_date)
            ->where('project_sprints.status','=','open')
            
            ->select(DB::raw('users.id,users.name, project_sprints.title, projects.project_name, project_sprints.start_date,
                    project_sprints.end_date,(sprint_resources.hours * 100 / 40) as allocation'))
            ->groupBy('users.id')                    
            ->get();
        return View('pages/admin/resources/show', ['resources' => $data]);

    }

    public function myResource() {
        $id=Auth::id();
        $users=User::where('parent_id',$id)->orderBy('id', 'DESC')->paginate(20);
        $jsonuser=User::getUsers();
        return View('pages/admin/resources/my-resource',['records' => $users,'jsonuser'=>$jsonuser]);        

    }

    
}
