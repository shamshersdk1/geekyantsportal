<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Bonus;
use App\Models\Admin\PerformanceBonus;
use App\Models\Month;
use App\Models\User;
use Auth;
use DB;
use Illuminate\Http\Request;

class PerformanceBonusController extends Controller
{
    public function index()
    {
        $performanceBonus = PerformanceBonus::orderBy('month_id','desc')->get();
        return view('pages.admin.performance-bonus.index', compact('performanceBonus'));
    }
    public function create()
    {
        $user = Auth::user();
        if ($user->hasRole('admin')) {
            $userList = User::Where('is_active', 1)->orderBy('name', 'asc')->get();
        } else {
            $userList = User::where('parent_id', $user->id)->where('is_active', 1)->orderBy('name', 'asc')->get();
        }
        $monthList = Month::orderBy('year', 'Desc')->orderBy('month', 'Desc')->get();
        return view('pages.admin.performance-bonus.add', compact('userList', 'monthList'));

    }
    public function store(Request $request)
    {
        
        $user = Auth::user();
        $data = $request->all();
       
        $monthObj = Month::find($data['month_id']);

        if (!$monthObj) {
            return redirect()->back()->withErrors('Month not found!')->withInput();

        }

        try {
            if (!empty($data)) {
                $data['month_id'] = $monthObj->id;
                $data['created_by'] = $user->id;

                $performanceObj = PerformanceBonus::saveData($data);
                
                if ($performanceObj) {

                    if ($performanceObj->status == 'approved') {
                        $bonusObj = new Bonus;
                        $bonusObj->user_id = $data['user_id'];
                        $bonusObj->month_id = $monthObj->id;
                        $bonusObj->status = 'approved';
                        $bonusObj->amount = $data['amount'];
                        $bonusObj->draft_amount = $data['amount'];
                        $bonusObj->date = $monthObj->getFirstDay();
                        $bonusObj->notes = !empty($data['comment']) ? $data['comment'] : null;
                        $bonusObj->created_by = $user->id;
                        $bonusObj->approved_by = $user->id;
                        $bonusObj->type = "extra";
                        $bonusObj->reference_id = $performanceObj->id;
                        $bonusObj->reference_type = 'App\Models\Admin\PerformanceBonus';

                        if ($bonusObj->save()) {
                            DB::commit();
                            return redirect('/admin/bonus/performance')->with('message', 'Updated!');
                        }
                    } else {
                        DB::commit();
                        return redirect('/admin/bonus/performance')->with('message', 'Updated!');
                    }
                } else {
                    return redirect()->back()->withErrors('Something went wrong!')->withInput();
                }
            } else {
                return redirect()->back()->withErrors('Something went wrong!')->withInput();
            }

        } catch (Exception $e) {
            DB::rollback();
        }
        return redirect()->back()->withErrors('Not Found')->withInput();
    }
    public function update(Request $request, $id)
    {
        $user = Auth::user();
        $data = $request->all();
      //  $month = date('m', strtotime($data['date']));
        $monthObj = Month::find($data['month_id']);

        if (!$monthObj) {
            return redirect()->back()->withErrors('Month not found!')->withInput();

        }

        try {
            if (!empty($data)) {
                $performanceObj = PerformanceBonus::find($id);

                if ($performanceObj) {

                    $status = $performanceObj->status;

                    $performanceObj->user_id = $data['user_id'];
                    $performanceObj->month_id = $monthObj->id;
                    $performanceObj->status = $data['status'];
                    $performanceObj->amount = $data['amount'];
                    $performanceObj->comment = !empty($data['comment']) ? $data['comment'] : null;
                    $performanceObj->created_by = $user->id;
                    $performanceObj->created_at = date('Y-m-d');

                    if ($performanceObj->save()) {
                        $bonusObj = new Bonus;
                        $bonusObj->user_id = $data['user_id'];
                        $bonusObj->month_id = $monthObj->id;
                        $bonusObj->status = $data['status'];
                        $bonusObj->amount = $data['amount'];
                        $bonusObj->draft_amount = $data['amount'];
                        $bonusObj->date = $monthObj->getFirstDay();
                        $bonusObj->notes = !empty($data['comment']) ? $data['comment'] : null;
                        $bonusObj->created_by = $user->id;
                        $bonusObj->approved_by = $user->id;
                        $bonusObj->type = "extra";
                        $bonusObj->reference_id = $performanceObj->id;
                        $bonusObj->reference_type = 'App\Models\Admin\PerformanceBonus';

                        if ($bonusObj->save()) {
                            DB::commit();
                            return redirect('/admin/bonus/performance')->with('message', 'Updated!');
                        }
                    } else {
                        return redirect()->back()->withErrors('Something went wrong!')->withInput();
                    }
                } else {
                    return redirect()->back()->withErrors('Not found!')->withInput();
                }

            }
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->withErrors('Not found!')->withInput();
        }
        return redirect('/admin/bonus/performance')->with('message', 'Updated!');

    }

    public function destroy($id)
    {
        $performanceObj = PerformanceBonus::where('id', $id)->delete();
        if ($performanceObj) {
            return redirect('/admin/bonus/performance')->with('message', 'Successfully deleted!');

        }
        return redirect()->back()->withErrors('Uable to delete record');

    }
    public function edit($id)
    {
        $user = Auth::user();
        if ($user->hasRole('admin')) {
            $userList = User::Where('is_active', 1)->orderBy('name', 'asc')->get();
        } else {
            $userList = User::where('parent_id', $user->id)->where('is_active', 1)->orderBy('name', 'asc')->get();
        }

        $performanceObj = PerformanceBonus::find($id);
        if ($performanceObj) {
            $userId = $performanceObj->user_id;
        }
        $monthList = Month::orderBy('year', 'Desc')->orderBy('month', 'Desc')->get();

        return view('pages.admin.performance-bonus.edit', compact('monthList','performanceObj', 'userList', 'userId'));
    }
    public function show($id)
    {
        $performanceObj = PerformanceBonus::find($id);
        if ($performanceObj) {
            $userId = $performanceObj->user_id;
        }

        return view('pages.admin.performance-bonus.show', compact('performanceObj', 'id'));
    }
    public function approve($id)
    {
        $user = Auth::user();
        $performanceObj = PerformanceBonus::find($id);
        $performanceObj->status = 'approved';
        if ($performanceObj->save()) {
            $bonusObj = new Bonus;
            $bonusObj->user_id = $performanceObj->user_id;
            $bonusObj->month_id = $performanceObj->month_id;
            $bonusObj->status = 'approved';
            $bonusObj->amount = $performanceObj->amount;
            $bonusObj->draft_amount = $performanceObj->amount;
            $bonusObj->date = $monthObj->getFirstDay();
            $bonusObj->notes = !empty($performanceObj->comment) ? $performanceObj->comment : null;
            $bonusObj->created_by = $user->id;
            $bonusObj->approved_by = $user->id;
            $bonusObj->type = "extra";
            $bonusObj->reference_id = $performanceObj->id;
            $bonusObj->reference_type = 'App\Models\Admin\PerformanceBonus';
            if ($bonusObj->save()) {
                return redirect('/admin/bonus/performance')->with('message', 'Approved!');
            }
        }
        return redirect('/admin/bonus/performance')->withErrors($performanceObj->getErrors().$bonusObj->getErrors());
    }
}
