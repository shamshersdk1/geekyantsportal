<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\AdditionalWorkDaysBonus;
use App\Models\Admin\Project;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;

class AdditionalWorkDaysBonusController extends Controller
{
    public function index()
    {
        $AdditionalWorkDaysBonus = AdditionalWorkDaysBonus::paginate(500);
        return view('pages.admin.addtional-work-days-bonus.index', compact('AdditionalWorkDaysBonus'));
    }
    public function create()
    {
        $user = Auth::user();
        if ($user->hasRole('admin')) {
            $userList = User::Where('is_active', 1)->orderBy('name', 'asc')->get();
        } else {
            $userList = User::where('parent_id', $user->id)->where('is_active', 1)->orderBy('name', 'asc')->get();
        }
        $projectList = Project::All();
        return view('pages.admin.addtional-work-days-bonus.add', compact('userList', 'projectList'));

    }
    public function store(Request $request)
    {
        $user = Auth::user();
        $data = $request->all();
        if (!empty($data)) {
            $AdditionalWorkDaysBonus = new AdditionalWorkDaysBonus();
            $AdditionalWorkDaysBonus->employee_id = $data['user_id'];
            $AdditionalWorkDaysBonus->type = $data['type'];
            $AdditionalWorkDaysBonus->amount = $data['amount'];
            $AdditionalWorkDaysBonus->status = 'approved';
            $AdditionalWorkDaysBonus->to_date = !empty(date_to_yyyymmdd($data['to_date'])) ? date_to_yyyymmdd($data['to_date']) : null;
            $AdditionalWorkDaysBonus->from_date = !empty(date_to_yyyymmdd($data['from_date'])) ? date_to_yyyymmdd($data['from_date']) : null;
            $AdditionalWorkDaysBonus->notes = !empty($data['notes']) ? $data['notes'] : null;
            $AdditionalWorkDaysBonus->created_by = $user->id;
            if (!empty($data['project_id'])) {
                $AdditionalWorkDaysBonus->reference_type = 'App\Models\Admin\Project';
                $AdditionalWorkDaysBonus->reference_id = $data['project_id'];
            }
            if ($AdditionalWorkDaysBonus->save()) {
                return redirect('/admin/bonus/additional-work-days')->with('message', 'Saved!');
            } else {
                return redirect()->back()->withErrors('Something went wrong!')->withInput();
            }
        } else {
            return redirect()->back()->withErrors('Empty data sent!')->withInput();
        }
    }
    public function update(Request $request, $id)
    {
        $user = Auth::user();
        $data = $request->all();
        if (!empty($data)) {

            $AdditionalWorkDaysBonus = AdditionalWorkDaysBonus::find($id);
            if ($AdditionalWorkDaysBonus) {
                $AdditionalWorkDaysBonus->employee_id = $data['user_id'];
                $AdditionalWorkDaysBonus->type = $data['type'];
                $AdditionalWorkDaysBonus->amount = $data['amount'];
                $AdditionalWorkDaysBonus->to_date = !empty(date_to_yyyymmdd($data['to_date'])) ? date_to_yyyymmdd($data['to_date']) : null;
                $AdditionalWorkDaysBonus->from_date = !empty(date_to_yyyymmdd($data['from_date'])) ? date_to_yyyymmdd($data['from_date']) : null;
                $AdditionalWorkDaysBonus->notes = !empty($data['notes']) ? $data['notes'] : null;
                $AdditionalWorkDaysBonus->created_by = $user->id;
                if (!empty($data['project_id'])) {
                    $AdditionalWorkDaysBonus->reference_type = 'App\Models\Admin\Project';
                    $AdditionalWorkDaysBonus->reference_id = $data['project_id'];
                }
                if ($AdditionalWorkDaysBonus->save()) {
                    return redirect('/admin/bonus/additional-work-days')->with('message', 'Updated!');
                } else {
                    return redirect()->back()->withErrors('Something went wrong!')->withInput();
                }
            } else {
                return redirect()->back()->withErrors('Not found!')->withInput();
            }
        }

    }
    public function destroy($id)
    {
        $AdditionalWorkDaysBonus = AdditionalWorkDaysBonus::where('id', $id)->delete();
        if ($AdditionalWorkDaysBonus) {
            return redirect('/admin/bonus/additional-work-days')->with('message', 'Successfully deleted!');

        }
        return redirect()->back()->withErrors('Uable to delete record');

    }
    public function edit($id)
    {
        $user = Auth::user();
        if ($user->hasRole('admin')) {
            $userList = User::Where('is_active', 1)->orderBy('name', 'asc')->get();
        } else {
            $userList = User::where('parent_id', $user->id)->where('is_active', 1)->orderBy('name', 'asc')->get();
        }
        $projectList = Project::All();
        $AdditionalWorkDaysBonus = AdditionalWorkDaysBonus::find($id);
        if ($AdditionalWorkDaysBonus) {
            $userId = $AdditionalWorkDaysBonus->employee_id;
            $projectId = !empty($AdditionalWorkDaysBonus->reference->id) ? $AdditionalWorkDaysBonus->reference->id : null;
            $type = $AdditionalWorkDaysBonus->type;
        }
        return view('pages.admin.addtional-work-days-bonus.edit', compact('AdditionalWorkDaysBonus', 'userList', 'userId', 'projectList', 'projectId', 'type'));
    }
    public function show($id)
    {
        //get
    }
}
