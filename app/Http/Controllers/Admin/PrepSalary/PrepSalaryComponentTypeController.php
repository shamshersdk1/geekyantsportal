<?php

namespace App\Http\Controllers\Admin\PrepSalary;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Audited\Salary\PrepSalaryComponentType;
use App\Models\Audited\Salary\PrepSalaryComponentTypeDependency;

class PrepSalaryComponentTypeController extends Controller
{
    public function index()
    {
        $prepSalaryComponentTypes = PrepSalaryComponentType::paginate(50);
        $selected_classes = [];
        if (!$prepSalaryComponentTypes) {
            return view('pages.admin.prep-salary-component-type.index')->with('message', 'no records found.');
        }
        return view('pages.admin.prep-salary.prep-salary-component-type.index', compact('prepSalaryComponentTypes','selected_classes'));
    }

    public function show($id)
    {
        $selected_component_types = PrepSalaryComponentTypeDependency::where('component_id', $id)->get();
        $prepSalaryComponentType = PrepSalaryComponentType::find($id);
        if (!$prepSalaryComponentType) {
            return redirect('/admin/prep-salary-component-type')->withErrors('Prepare salary component type not found!')->withInput();
        }
        $prepSalaryComponentTypes = PrepSalaryComponentType::where('id','<>',$id)->get();
        $prepSalaryComponentType->service_class = substr($prepSalaryComponentType->service_class,27);
        return view('pages.admin.prep-salary.prep-salary-component-type.edit', compact('prepSalaryComponentType','prepSalaryComponentTypes','selected_component_types'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        if(!$data){
            return redirect()->back()->withErrors('Missing Input')->withInput();
        }
        if(!$data['service_class']){
            return redirect()->back()->withErrors('Service Class Required')->withInput();
        }
        $PrepSalaryComponentTypeObj = new PrepSalaryComponentType();
        $PrepSalaryComponentTypeObj->name=$data['name'];
        $PrepSalaryComponentTypeObj->code=$data['code'];     
        $PrepSalaryComponentTypeObj->service_class="App\Services\SalaryService\\".$data['service_class'];   
        if($PrepSalaryComponentTypeObj->isValid())
        {
            if($PrepSalaryComponentTypeObj->save()){
                if(!empty($data['dependencies'])){
                    foreach($data['dependencies'] as $key => $value)
                    {
                        
                        $dependencyObj = new PrepSalaryComponentTypeDependency();
                        $dependencyObj->component_id = $PrepSalaryComponentTypeObj->id;
                        $dependencyObj->dependent_id = $value;
                        if($dependencyObj->isValid())
                        {
                            if(!$dependencyObj->save()){
                                return redirect()->back()->withErrors('Unable To Save')->withInput();
                            }
                        }
                    }
                }
                return redirect()->back()->withMessage('Successfully Saved!');
            }
            return redirect()->back()->withErrors('Unable To Save')->withInput();
        }
        return redirect()->back()->withErrors($PrepSalaryComponentTypeObj->getErrors())->withInput();        
    }

    public function update(Request $request, $id)
    {

        $data = $request->all();
        if(!$data['service_class']){
            return redirect()->back()->withErrors('Service Class Required')->withInput();
        }
        $PrepSalaryComponentTypeObj = PrepSalaryComponentType::find($id);
        if (!$PrepSalaryComponentTypeObj) {
            return redirect()->back()->withErrors('Entry not found!')->withInput();
        }
        $PrepSalaryComponentTypeObj->name=$data['name'];
        $PrepSalaryComponentTypeObj->code=$data['code'];     
        $PrepSalaryComponentTypeObj->service_class="App\Services\SalaryService\\".$data['service_class'];  
        if($PrepSalaryComponentTypeObj->isValid())
        {
            if ($PrepSalaryComponentTypeObj->update()) {
                PrepSalaryComponentTypeDependency::where('component_id',$id)->delete();
                if(!empty($data['dependencies'])){
                foreach($data['dependencies'] as $key => $value)
                {
                    
                    $dependencyObj = new PrepSalaryComponentTypeDependency();
                    $dependencyObj->component_id = $PrepSalaryComponentTypeObj->id;
                    $dependencyObj->dependent_id = $value;
                    if($dependencyObj->isValid())
                    {
                        if(!$dependencyObj->save()){
                            return redirect()->back()->withErrors('Unable To Save')->withInput();
                        }
                    }
                }
            }
                return redirect('/admin/prep-salary-component-type')->with('message', 'Updated!');   
            }
            return redirect()->back()->withErrors('Unable To Update')->withInput();
        }
        return redirect()->back()->withErrors($PrepSalaryComponentTypeObj->getErrors() )->withInput();
    }

    public function destroy($id)
    {
        $prepSalaryComponentTypeObj = PrepSalaryComponentType::find($id);
        if ($prepSalaryComponentTypeObj && $prepSalaryComponentTypeObj->delete()) {
            return redirect('/admin/prep-salary-component-type')->with('message', 'Successfully Deleted');
        }
        return redirect()->back()->withErrors('Something went wrong!')->withInput();
    }
}
