<?php

namespace App\Http\Controllers\Admin\PrepSalary;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Audited\Salary\PrepSalaryUser;
use App\Models\Audited\Salary\PrepSalaryComponent;
use App\Models\User;

class PrepSalaryGenerateController extends Controller
{
    public function generate($id)
    {
        $prepSalaryComponent = PrepSalaryComponent::find($id);
    //     if(!$prepSalaryComponent)
    //     {
    //         return redirect()->back()->withErrors('Prepare Salary Component not found')->withInput();
    //     }

    //     $users = User::where('is_active','1')->get();
    //     if(!$users)
    //     {
    //         return redirect()->back()->withMessage('User not found"');
    //     }
    //     foreach($users as $user)
    //     {
    //         $prepSalaryUserObj = new PrepSalaryUser();
    //         $prepSalaryUserObj->prep_salary_id = $prepSalaryComponent->salary->id;
    //         $prepSalaryUserObj->user_id = $user->id;
    //         $prepSalaryUserObj->prep_salary_component_id = $prepSalaryComponent->type->id;
            $strModelName = $prepSalaryComponent->type->service_class;
            if(!class_exists($strModelName)) {
                return redirect()->back()->withErrors('Class Not Exists')->withInput();
            }
            $strModelObj = new $strModelName();
            $strModelObj->setComponent($prepSalaryComponent->prep_salary_id);
            $strModelObj->generate();
            // $prepSalaryUserObj->value = json_encode($strModelObj->getValue());
            // if(!$prepSalaryUserObj->save())
            // {
            //     return redirect()->back()->withErrors('Something went wrong')->withInput();
            // }
            // return redirect()->back()->withMessage('Successfully Generated!');
        
    }
}
