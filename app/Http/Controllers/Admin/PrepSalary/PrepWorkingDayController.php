<?php

namespace App\Http\Controllers\Admin\PrepSalary;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Audited\Salary\PrepSalaryComponent;
use App\Models\Audited\Salary\PrepWorkingDay;

class PrepWorkingDayController extends Controller
{
    public function show($prepSalaryId,$prepComponentId)
    {
        $prepSalaryComponent = PrepSalaryComponent::find($prepComponentId);
        if(!count($prepSalaryComponent)>0){
            return redirect()->back()->withErrors('Prep Salary component not found!')->withInput();
        }
        $workingDayObjs = PrepWorkingDay::where('prep_salary_id',$prepSalaryId)->get();
        if(!count($workingDayObjs)>0){
            return redirect()->back()->withErrors('Something went wrong!')->withInput();
        }
        return view('pages.admin.prep-salary.prep-working-days.index',compact('workingDayObjs','prepSalaryComponent'));
    }
}