<?php

namespace App\Http\Controllers\Admin\PrepSalary;

use App\Events\PrepGenerateAll;
use App\Http\Controllers\Controller;
use App\Jobs\PrepareSalary\AdhocPaymentJob;
use App\Jobs\PrepareSalary\AppraisalBonusJob;
use App\Jobs\PrepareSalary\AppraisalComponentJob;
use App\Jobs\PrepareSalary\BonusesJob;
use App\Jobs\PrepareSalary\FoodDeductionJob;
use App\Jobs\PrepareSalary\InsuranceDeductionJob;
use App\Jobs\PrepareSalary\LoanDeductionJob;
use App\Jobs\PrepareSalary\TdsJob;
use App\Jobs\PrepareSalary\VpfJob;
use App\Models\Appraisal\AppraisalBonusType;
use App\Models\Appraisal\AppraisalComponentType;
use App\Models\Audited\Bonus\PrepBonus;
use App\Models\Audited\Salary\PrepAdhocPayment;
use App\Models\Audited\Salary\PrepAppraisalBonus;
use App\Models\Audited\Salary\PrepCurrentGross;
use App\Models\Audited\Salary\PrepFoodDeduction;
use App\Models\Audited\Salary\PrepInsurance;
use App\Models\Audited\Salary\PrepLoanEmi;
use App\Models\Audited\Salary\PrepSalary;
use App\Models\Audited\Salary\PrepSalaryComponent;
use App\Models\Audited\Salary\PrepSalaryComponentType;
use App\Models\Audited\Salary\PrepSalaryExecution;
use App\Models\Audited\Salary\PrepTds;
use App\Models\Audited\Salary\PrepUser;
use App\Models\Audited\Salary\PrepVpf;
use App\Models\Audited\WorkingDay\UserWorkingDay;
use App\Models\Month;
use App\Models\Transaction;
use App\Models\User;
use App\Services\SalaryService;
use App\Services\SalaryService\DependencyCheckComponent;
use DB;
use Excel;
use Illuminate\Http\Request;
use Redirect;
use GuzzleHttp\Client;

class PrepSalaryController extends Controller
{
    public function index()
    {
        $prepSalaries = PrepSalary::orderBy('created_at', 'DESC')->get();
        $months = Month::orderBy('year', 'desc')->orderBy('month', 'desc')->get();
        if (!count($months) > 0) {
            return view('pages.admin.prep-salary.prep-salary.index')->with('message', 'no month found.');
        }
        $monthList = [];
        foreach ($months as $month) {
            $monthList[$month->id] = $month->formatMonth();
        }

        if (!$prepSalaries) {
            return view('pages.admin.prep-salary.prep-salary.index')->with('message', 'no records found.');
        }
        return view('pages.admin.prep-salary.prep-salary.index', compact('prepSalaries', 'monthList'));
    }

    private function _userPlayslipData($monthId, $user_id)
    {

        $days_worked = 21;

        // $userWorkingObj = PrepWorkingDay::where('user_id', $user_id)->where('prep_salary_id', $id)->first();
        $month = Month::find($monthId);
        $userWorkingObj = UserWorkingDay::where('user_id', $user_id)->where('month_id', $monthId)->first();
        if ($userWorkingObj) {
            // $days_worked = $userWorkingObj->user_worked_days;
            $workingDays = $userWorkingObj->working_days;
            $days_worked = $userWorkingObj->working_days + $userWorkingObj->lop;
        }

        $user_transactions = Transaction::where([
            ['month_id', '=', $monthId],
            ['user_id', '=', $user_id],
            ['amount', '!=', 0],
            ['reference_type', '!=', 'App\Models\Audited\BankTransfer\BankTransferUser'],
            ['reference_type', '!=', 'App\Models\Audited\BankTransfer\BankTransferHoldUser'],
            ['reference_type', '!=', 'App\Models\Loan'],
        ])->get();
        $net_transfer = 0;
        $total_earning = 0;
        $total_deduction = 0;
        $deduction = [];
        $earning = [];
        $onsite_bonus = 0;
        $bonus = 0;
        $additional_bonus = 0;
        $extra_bonus = 0;
        $performance_bonus = 0;
        $tech_talk_bonus = 0;
        foreach ($user_transactions as $t) {

            if ($t->is_company_expense) {
                continue;
            }

            if ($t->amount > 0) {
                $earning[] = $t;
                $total_earning += $t->amount;
            } else {
                $deduction[] = $t;
                $total_deduction += $t->amount;
            }
            if ($t->reference->reference_type == 'App\Models\Admin\OnSiteBonus') {
                $onsite_bonus += $t->amount;
            } elseif ($t->reference->reference_type == 'App\Models\Admin\AdditionalWorkDaysBonus') {
                $additional_bonus += $t->amount;
            } elseif ($t->reference->reference_type == 'App\Models\User\UserTimesheetExtra') {
                $extra_bonus += $t->amount;
            } elseif ($t->reference->reference_type == 'App\Models\Admin\PerformanceBonus') {
                $performance_bonus += $t->amount;
            } elseif ($t->reference->reference_type == 'App\Models\Admin\TechTalkBonusesUser') {
                $tech_talk_bonus += $t->amount;
            } elseif ($t->reference == 'App\Models\Admin\Bonus') {
                $bonus += $t->amount;
            }

            $net_transfer += $t->amount;
        }
        $user = User::find($user_id);
        return compact('deduction', 'earning', 'user', 'total_earning', 'total_deduction', 'net_transfer', 'days_worked', 'onsite_bonus', 'additional_bonus', 'extra_bonus', 'performance_bonus', 'tech_talk_bonus', 'bonus', 'month', 'workingDays');
    }

    public function downloadUserPayslip($monthId, $user_id)
    {
        $data = $this->_userPlayslipData($monthId, $user_id);
        $pdf = \PDF::loadView('pages.admin.prep-salary.prep-salary.pdf.payslip', $data);
        return $pdf->download('payslip-' . $user_id . '.pdf', array('Attachment' => 0));
    }

    public function showUserPayslip($monthId, $user_id)
    {
        $data = $this->_userPlayslipData($monthId, $user_id);

        return view('pages.admin.prep-salary.prep-salary.user-payslip', $data);
    }

    public function showPayslips($monthId = null)
    {
        if ($monthId == null) {
            $month = date('m');
            $year = date('Y');
            $monthObj = Month::where('month', $month)->where('year', $year)->first();

            if ($monthObj == null) {
                return redirect()->back()->withErrors('Month not found!')->withInput();
            }

            $monthId = $monthObj->id;
        } else {
            $monthObj = Month::find($monthId);
        }

        $transactions = Transaction::where('month_id', $monthId)->groupBy('user_id')->get();
        $monthList = Month::orderBy('year', 'DESC')->orderBy('month', 'DESC')->get();

        return view('pages.admin.prep-salary.prep-salary.payslips', compact('transactions', 'monthId', 'monthList', 'monthObj'));
    }

    public function show($id)
    {
        $prepSalarie = PrepSalary::find($id);
        PrepSalaryExecution::componentStatus($id);
        if (!$prepSalarie) {
            return redirect('/admin/prep-salary')->withErrors("no record found.")->withInput();
        }
        return view('pages.admin.prep-salary.prep-salary.show', compact('prepSalarie'));
    }
    public function discardPrepSalary($id)
    {
        $prepSalObj = PrepSalary::find($id);
        if (!$prepSalObj) {
            return redirect('/admin/prep-salary')->withErrors("Prep Salary not found.");
        }
        $prepSalObj->status = 'discarded';
        if (!$prepSalObj->save()) {
            return redirect('/admin/prep-salary')->withErrors("Prep Salary status not saved.");
        }
        return redirect('/admin/prep-salary')->with("message", "Prep Salary discarded.");
    }

    public function storeSalary(Request $request)
    {
        $data = $request->all();
        $month = Month::find($data['id']);
        if (!$month) {
            return redirect('/admin/prep-salary')->withErrors("no month found.")->withInput();
        }
        // $monthExists = PrepSalary::where('month_id', $month->id)->first();
        // if (count($monthExists) > 0) {
        //     return redirect('/admin/prep-salary')->withErrors("Month " . $month->formatMonth() . " already exists")->withInput();
        // }
        $prepSalaryObj = new PrepSalary();
        $prepSalaryObj->month_id = $data['id'];
        $prepSalaryObj->status = 'open';
        if ($prepSalaryObj->isValid()) {
            if ($prepSalaryObj->save()) {
            } else {
                return redirect()->back()->withErrors('Unable To Save')->withInput();
            }
        } else {
            return redirect('/admin/prep-salary')->withErrors($prepSalaryObj->getErrors())->withInput();
        }
        $prepSalaryComponents = PrepSalaryComponentType::all();
        foreach ($prepSalaryComponents as $prepSalaryComponent) {
            $prepSalaryComponentObj = new PrepSalaryComponent();
            $prepSalaryComponentObj->prep_salary_id = $prepSalaryObj->id;
            $prepSalaryComponentObj->prep_salary_component_type_id = $prepSalaryComponent->id;
            $prepSalaryComponentObj->is_generated = 0;
            if ($prepSalaryObj->isValid()) {
                if ($prepSalaryComponentObj->save()) {
                } else {
                    return redirect('/admin/prep-salary')->withErrors("Unable to Save")->withInput();
                }
            } else {
                return redirect('/admin/prep-salary')->withErrors($prepSalaryComponentObj->getErrors())->withInput();
            }
        }
        return redirect('/admin/prep-salary/' . $prepSalaryObj->id)->with('message', 'Saved! Of Month ' . $month->formatMonth());
    }

    public function destroy($id)
    {
        $prepSalaryObj = PrepSalary::find($id);
        if ($prepSalaryObj && $prepSalaryObj->delete()) {
            return redirect('/admin/prep-salary')->with('message', 'Successfully Deleted');
        }
        return redirect()->back()->withErrors('Something went wrong!')->withInput();
    }

    public function lockMonth($id)
    {
        $prepSalaryObj = PrepSalary::find($id);

        foreach ($prepSalaryObj->components as $component) {
            $componentObj = PrepSalaryComponent::find($component->id);
            $componentObj->status = 'closed';
            if (!$componentObj->update()) {
                return redirect()->back()->withErrors('Something went wrong!')->withInput();
            }
        }
        $prepSalaryObj->status = 'closed';
        if (!$prepSalaryObj->update()) {
            return redirect()->back()->withErrors('Sometbhasbdhhing went wrong!')->withInput();
        }
        return redirect('/admin/prep-salary')->with('message', 'Successfully Locked');
    }

    private function checkDependency($prepSalaryComponentId)
    {
        $response['status'] = false;
        $response['errors'] = [];
        $prepSalaryComponent = PrepSalaryComponent::find($prepSalaryComponentId);
        if (!$prepSalaryComponent) {
            $response['status'] = false;
            $response['errors'] = 'Prep Salary Component Not Found';
        }
        if ($prepSalaryComponent->type && $prepSalaryComponent->type->dependsOn) {
            foreach ($prepSalaryComponent->type->dependsOn as $dependent) {
                foreach ($dependent->dependentOn->prepSalaryComponent as $component) {
                    if ($prepSalaryComponent->prep_salary_id == $component->prep_salary_id && $component->is_generated == 0) {
                        $response['status'] = true;
                        array_push($response['errors'], "Generate Component : " . $dependent->dependentOn->name);
                    }
                }
            }
        }
        return $response;
    }

    private function dependentOn($prepSalaryComponentId)
    {
        $response['status'] = false;
        $response['errors'] = '';
        $prepSalaryComponent = PrepSalaryComponent::find($prepSalaryComponentId);
        if ($prepSalaryComponent->type->dependentOn) {
            foreach ($prepSalaryComponent->type->dependentOn as $dependent) {
                if ($dependent->dependentReverseOn) {
                    foreach ($dependent->dependentReverseOn->prepSalaryComponent as $component) {
                        if ($prepSalaryComponent->prep_salary_id == $component->prep_salary_id && $component->is_generated == 1) {
                            $component->is_generated = 0;
                            if ($component->isValid()) {
                                if (!$component->save()) {
                                    $response['errors'] = $response['errors'] . "Unable to Save";
                                }
                            } else {
                                $response['errors'] = $response['errors'] . $component->getErrors();
                            }
                        }
                    }
                }
            }
        }
        return $response;
    }

    public function generate($id)
    {
        $userObject = \Auth::user();
        if (!$userObject) {
            return redirect()->back()->withErrors('failed to get user')->withInput();
        }
        $prepSalaryComponent = PrepSalaryComponent::find($id);
        if (!$prepSalaryComponent) {
            return redirect()->back()->withErrors('Prepare Salary Component not found')->withInput();
        }
        // $response = self::checkDependency($id);
        // if($response['status'])
        // {
        //     return redirect()->back()->withErrors($response['errors'])->withInput();
        // }

        $dependencyCheck = new DependencyCheckComponent($prepSalaryComponent->type->id, $prepSalaryComponent->salary->id);

        $response = $dependencyCheck->checkDependency();

        if (!$response) {
            return redirect()->back()->withErrors('Generate Dependent Component First');
        }

        $response = self::dependentOn($id);

        $strModelName = $prepSalaryComponent->type->service_class;
        if (!class_exists($strModelName)) {
            return redirect()->back()->withErrors('Class Not Exists')->withInput();
        }
        $strModelObj = new $strModelName();
        $strModelObj->setComponent($prepSalaryComponent->id);
        $strModelObj->setUserId($userObject->id);
        $strModelObj->setMonthYear($prepSalaryComponent->salary->month->month, $prepSalaryComponent->salary->month->year);
        $response = $strModelObj->generate();
        $prepSalaryComponent->is_generated = 1;

        if (!$response['status']) {
            return redirect()->back()->withErrors($response['errors'])->withInput();
        }

        if (!$prepSalaryComponent->update()) {
            return redirect()->back()->withErrors('Something went wrong!')->withInput();
        }
        return redirect()->back()->withMessage('Successfully generated of ' . $prepSalaryComponent->type->name . ' for ' . $prepSalaryComponent->salary->month->formatMonth());
    }

    public function getView($id)
    {
        $prepSalaryComponent = PrepSalaryComponent::find($id);
        if (!$prepSalaryComponent) {
            return redirect()->back()->withErrors('Prep Salary Id Not Found')->withInput();
        }
        //  $prepUsers = $prepSalaryComponent->salary->prepUsers;
        $prepUsers = PrepUser::where('prep_salary_id', $prepSalaryComponent->salary->id)->with('user')->select('prep_users.*', \DB::raw('(SELECT employee_id FROM users WHERE prep_users.user_id = users.id ) as employee_id'))->orderBy('employee_id')->get();
        $response = [];
        if (count($prepUsers) > 0) {
            foreach (PrepSalaryComponent::where('prep_salary_id', $prepSalaryComponent->prep_salary_id)->get() as $prepComponenet) {
                foreach (PrepUser::where('prep_salary_id', $prepSalaryComponent->prep_salary_id)->cursor() as $prepUser) {
                    $strModelName = $prepComponenet->type->service_class;
                    $strModelObj = new $strModelName;
                    $strModelObj->setComponent($id);
                    $strModelObj->setUserId($prepUser->user_id);
                    $response[$prepUser->user_id][$prepComponenet->type->code] = $strModelObj->getHtml(); //Remove prep_salary_id
                }
            }
        }
        return view('pages.admin.prep-salary.partials.index', compact('prepUsers', 'response'));
    }

    public function downloadCSV($id)
    {
        $prepSalaryComponent = PrepSalaryComponent::find($id);
        if (!$prepSalaryComponent) {
            return redirect()->back()->withErrors('Prep Salary Id Not Found')->withInput();
        }
        //  $prepUsers = $prepSalaryComponent->salary->prepUsers;
        $prepUsers = PrepUser::where('prep_salary_id', $prepSalaryComponent->salary->id)->with('user')->select('prep_users.*', \DB::raw('(SELECT employee_id FROM users WHERE prep_users.user_id = users.id ) as employee_id'))->orderBy('employee_id')->get();
        $response = [];

        if (count($prepUsers) > 0) {
            foreach (PrepSalaryComponent::where('prep_salary_id', $prepSalaryComponent->prep_salary_id)->get() as $prepComponenet) {
                foreach (PrepUser::where('prep_salary_id', $prepSalaryComponent->prep_salary_id)->cursor() as $prepUser) {
                    $strModelName = $prepComponenet->type->service_class;
                    $strModelObj = new $strModelName;
                    $strModelObj->setComponent($id);
                    $strModelObj->setUserId($prepUser->user_id);
                    $response[$prepUser->user_id][$prepComponenet->type->code] = $strModelObj->getHtml(); //Remove prep_salary_id
                }
            }
        }

        function array_merge_add($x, $y)
        {
            $myArray = $x;
            foreach ($y as $k => $v) {
                if (isset($myArray[$k])) {
                    if (is_numeric($myArray[$k])) {
                        $myArray[$k] = $myArray[$k] + $v;
                    } else {
                        $myArray[$k] = $myArray[$k] . " + " . $v;
                    }

                } else {
                    $myArray[$k] = $v;
                }
            }
            return $myArray;
        }

        function getFlatKeyArray($data, $prefix = '')
        {
            $output = [];
            if (isset($data['key']) && isset($data['value'])) {
                $prefix = substr($prefix, 0, -1);
                $last_pos = strrpos($prefix, "_");
                $prefix = substr($prefix, 0, $last_pos) . "_";
                $output[$prefix . $data['key']] = $data['value'];
            } else {
                foreach ($data as $key => $value) {
                    if (is_array($value)) {
                        if (is_numeric($key) && !(isset($value['key']) && isset($value['value']))) {
                            $output = array_merge_add($output, getFlatKeyArray($value, $prefix));
                        } else {
                            $output = array_merge_add($output, getFlatKeyArray($value, $prefix . $key . '_'));
                        }

                    } else {
                        $output[$prefix . $key] = $value;
                    }
                }
            }
            return $output;
        }

        $users_map = [];
        foreach ($prepUsers as $user) {
            $users_map[$user->user_id] = $user->user;
        }

        $final_response = [];
        $keys = [];
        foreach ($response as $user_id => $data) {
            $user_info = [];
            $user_info['id'] = $users_map[$user_id]->id;
            $user_info['name'] = $users_map[$user_id]->name;
            $user_info['email'] = $users_map[$user_id]->email;
            $user_info['employee_id'] = $users_map[$user_id]->employee_id;

            $final_response[$user_id] = array_merge($user_info, getFlatKeyArray($data));

            foreach ($final_response[$user_id] as $field_key => $field_value) {
                $keys[$field_key] = $field_key;
            }
        }

        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=prep-salary-data-' . time() . '.csv');

        echo implode(",", $keys) . "\n";
        foreach ($final_response as $user_id => $data) {
            $line = '';
            foreach ($keys as $key => $value) {
                if (isset($data[$key])) {
                    $line .= $data[$key] . ',';
                } else {
                    $line .= ',';
                }

            }
            echo $line . "\n";
        }

        exit();
    }

    public function generatePayslip($id, $userId = null)
    {
        if ($userId == null) {
            $userObj = User::where('is_active', 1)->orderBy('employee_id', 'ASC')->first();
            $userId = $userObj->id;
        } else {
            $userObj = User::find($userId);
            if (!$userObj) {
                return redirect('/admin/prep-salary')->withErrors("User not found.")->withInput();
            }
        }

        $userList = User::where('is_active', 1)->orderBy('employee_id', 'ASC')->get();

        $prepSalaryObj = PrepSalary::find($id);
        $currentGrossObj = PrepCurrentGross::where('prep_salary_id', $prepSalaryObj->id)->where('user_id', $userId)->first();

        if (!$currentGrossObj || !$prepSalaryObj) {
            return redirect('/admin/prep-salary')->withErrors("No record found.")->withInput();
        }

        $appraisalBonusObj = PrepAppraisalBonus::where('prep_salary_id', $prepSalaryObj->id)->where('user_id', $userId)->get();

        $appraisalComponentTypes = AppraisalComponentType::all();
        $appraisalBonusTypes = AppraisalBonusType::all();

        $vpfObj = PrepVpf::where('prep_salary_id', $prepSalaryObj->id)->where('user_id', $userId)->get();

        $insuranceObj = PrepInsurance::where('prep_salary_id', $prepSalaryObj->id)->where('user_id', $userId)->get();

        $loanObj = PrepLoanEmi::where('prep_salary_id', $prepSalaryObj->id)->where('user_id', $userId)->get();
        //loan

        //--bonus itsaving LOAN
        $tdsObj = PrepTds::where('prep_salary_id', $prepSalaryObj->id)->where('user_id', $userId)->first();
        $salaryResponse = self::generateSalaryData($id, $userId);

        $storeSalaryObj = Salary::where('user_id', $userId)->where('month_id', $prepSalaryObj->month->id)->first();
        $foodObj = PrepFoodDeduction::where('prep_salary_id', $prepSalaryObj->id)->where('user_id', $userId)->first();
        $bonusObjs = PrepBonus::where('prep_salary_id', $prepSalaryObj->id)->where('user_id', $userId)->get();
        return view('pages.admin.prep-salary.prep-salary.payslip', compact('bonusObjs', 'foodObj', 'storeSalaryObj', 'tdsObj', 'salaryResponse', 'userObj', 'prepSalaryObj', 'userList', 'currentGrossObj', 'appraisalComponentTypes', 'appraisalBonusObj', 'appraisalBonusTypes', 'vpfObj', 'insuranceObj', 'loanObj'));
    }

    public function generateSalaryData($prepSalaryId, $userId)
    {
        $prepSalaryObj = PrepSalary::find($prepSalaryId);
        $currentGrossObj = PrepCurrentGross::where('prep_salary_id', $prepSalaryObj->id)->where('user_id', $userId)->first();
        $response['monthlyGrossSalary'] = 0;
        $response['totalDeductions'] = 0;
        $response['netPayable'] = 0;
        foreach ($currentGrossObj->items as $item) {
            $item->value > 0 ? $response['monthlyGrossSalary'] += $item->value : $response['totalDeductions'] -= $item->value;
            $response['netPayable'] += $item->value;
        }
        $loanObjs = PrepLoanEmi::where('prep_salary_id', $prepSalaryObj->id)->where('user_id', $userId)->get();
        foreach ($loanObjs as $loanObj) {
            $response['totalDeductions'] -= $loanObj->emi_amount;
            $response['netPayable'] += $loanObj->emi_amount;
        }
        $insuranceObjs = PrepInsurance::where('prep_salary_id', $prepSalaryObj->id)->where('user_id', $userId)->get();
        foreach ($insuranceObjs as $insuranceObj) {
            $response['totalDeductions'] -= $insuranceObj->value;
            $response['netPayable'] += $insuranceObj->value;
        }
        $appraisalBonusObjs = PrepAppraisalBonus::where('prep_salary_id', $prepSalaryObj->id)->where('user_id', $userId)->get();
        foreach ($appraisalBonusObjs as $appraisalBonusObj) {
            $response['totalDeductions'] -= $appraisalBonusObj->value;
            $response['netPayable'] += $appraisalBonusObj->value;
        }
        $tdsObj = PrepTds::where('prep_salary_id', $prepSalaryObj->id)->where('user_id', $userId)->first();
        if ($tdsObj) {
            $response['totalDeductions'] -= $tdsObj->componentByKey('tds-for-the-month')->value;
            $response['netPayable'] -= $tdsObj->componentByKey('tds-for-the-month')->value;
        }
        $bonusObjs = PrepBonus::where('prep_salary_id', $prepSalaryObj->id)->where('user_id', $userId)->get();
        if (count($bonusObjs) > 0) {
            foreach ($bonusObjs as $bonusObj) {
                $response['netPayable'] += $bonusObj->bonus->amount;
            }
        }
        $foodObjs = PrepFoodDeduction::where('prep_salary_id', $prepSalaryObj->id)->where('user_id', $userId)->first();
        $response['totalDeductions'] -= $foodObjs->value;
        $response['netPayable'] += $foodObjs->value;
        return $response;
    }

    public function salaryStore($prepSalaryId, $userId)
    {
        $prepSalaryObj = PrepSalary::find($prepSalaryId);
        $salaryResponse = self::generateSalaryData($prepSalaryId, $userId);
        if (!count(Salary::where('user_id', $userId)->where('month_id', $prepSalaryObj->month->id)->first()) > 0) {
            $salaryObj = Salary::create(['user_id' => $userId, 'month_id' => $prepSalaryObj->month->id, 'monthly_gross_salary' => $salaryResponse['monthlyGrossSalary'],
                'total_deductions' => -$salaryResponse['totalDeductions'], 'net_payable' => $salaryResponse['netPayable']]);
            if (!$salaryObj->isValid()) {
                return redirect()->back()->withErrors($salaryObj->getErrors());
            }
            dispatch(new AppraisalComponentJob($userId, $prepSalaryId));
            dispatch(new AppraisalBonusJob($userId, $prepSalaryId));
            dispatch(new FoodDeductionJob($userId, $prepSalaryId));
            dispatch(new InsuranceDeductionJob($userId, $prepSalaryId));
            dispatch(new LoanDeductionJob($userId, $prepSalaryId));
            dispatch(new TdsJob($userId, $prepSalaryId));
            dispatch(new VpfJob($userId, $prepSalaryId));
            dispatch(new BonusesJob($userId, $prepSalaryId));
            dispatch(new AdhocPaymentJob($userId, $prepSalaryId));
        }
        return redirect()->back()->withMessage('Saved Successfully');
    }

    public function finalize($prepSalaryId)
    {
        $prepSalaryObjs = PrepUser::where('prep_salary_id', $prepSalaryId)->get();
        $prepSalary = PrepSalary::find($prepSalaryId);
        if (!$prepSalary) {
            return redirect()->back()->withErrors("Prep Salary Not Found.");
        }
        $prepSalary->status = 'finalizing';
        $prepSalary->save();
        foreach ($prepSalaryObjs as $prepSalaryObj) {
            PrepFoodDeduction::where('prep_salary_id', $prepSalaryId)->where('user_id', $prepSalaryObj->user_id)->update(['status' => 'processing']);
            dispatch(new FoodDeductionJob($prepSalaryObj->user_id, $prepSalaryId));

            PrepCurrentGross::where('prep_salary_id', $prepSalaryId)->where('user_id', $prepSalaryObj->user_id)->first()->items()->update(['status' => 'processing']);
            dispatch(new AppraisalComponentJob($prepSalaryObj->user_id, $prepSalaryId));

            PrepAppraisalBonus::where('prep_salary_id', $prepSalaryId)->where('user_id', $prepSalaryObj->user_id)->update(['status' => 'processing']);
            dispatch(new AppraisalBonusJob($prepSalaryObj->user_id, $prepSalaryId));

            PrepInsurance::where('prep_salary_id', $prepSalaryId)->where('user_id', $prepSalaryObj->user_id)->update(['status' => 'processing']);
            dispatch(new InsuranceDeductionJob($prepSalaryObj->user_id, $prepSalaryId));

            PrepLoanEmi::where('prep_salary_id', $prepSalaryId)->where('user_id', $prepSalaryObj->user_id)->update(['status' => 'processing']);
            dispatch(new LoanDeductionJob($prepSalaryObj->user_id, $prepSalaryId));

            $obj = PrepTds::where('prep_salary_id', $prepSalaryId)->where('user_id', $prepSalaryObj->user_id)->first()->componentByKey('tds-for-the-month');
            $obj->status = "processing";
            $obj->save();
            dispatch(new TdsJob($prepSalaryObj->user_id, $prepSalaryId));

            PrepVpf::where('prep_salary_id', $prepSalaryId)->where('user_id', $prepSalaryObj->user_id)->update(['status' => 'processing']);
            dispatch(new VpfJob($prepSalaryObj->user_id, $prepSalaryId));

            PrepBonus::where('prep_salary_id', $prepSalaryId)->where('user_id', $prepSalaryObj->user_id)->update(['status' => 'processing']);
            dispatch(new BonusesJob($prepSalaryObj->user_id, $prepSalaryId));

            PrepAdhocPayment::where('prep_salary_id', $prepSalaryId)->where('user_id', $prepSalaryObj->user_id)->update(['status' => 'processing']);
            dispatch(new AdhocPaymentJob($prepSalaryObj->user_id, $prepSalaryId));
        }
        return redirect()->back()->withMessage('Finalizing');
    }

    public function viewTds($id)
    {
        $prepSalary = PrepSalary::find($id);
        if (!$prepSalary) {
            return redirect()->back()->withErrors('Prep Salary Id Not Found')->withInput();
        }
        $prepTdsObjs = PrepTds::where('prep_salary_id', $prepSalary->id)->get();
        return view('pages.admin.prep-salary.prep-salary.tds', compact('prepTdsObjs', 'prepSalary'));
    }

    public function prepSalarySheet($id)
    {
        $prepSalObj = PrepSalary::find($id);
        if (!$prepSalObj) {
            return redirect()->back()->withErrors('Prep Salary Id Not Found')->withInput();
        }
        $data = SalaryService::getSalarySheetData($id);
        $users = User::orderBy('employee_id', 'ASC')->get();
        return view('pages.admin.prep-salary.prep-salary.salary-sheet', compact('data', 'users', 'prepSalObj'));
    }

    public function getUsers($salaryId)
    {
        // $users = User::orderBy('is_active','desc')->orderBy('employee_id')->get();
        // $prepSalaryObj = PrepSalary::find($salaryId);
        $prepSalaryObj = PrepSalary::find($salaryId);
        // if(count($prepSalaryObj->prepUsers?$prepSalaryObj->prepUsers:true) > 0 ){
        //     $prepIds = $prepSalaryObj->prepUsers->pluck('id');
        //     $all_users = User::whereNotIn('id',$prepIds)->get();
        //     $checl = $prepSalaryObj->prepUsers;
        //     $users = $checl->merge($all_users);
        // }
        // else
        $compType = PrepSalaryComponentType::where('code', 'user')->first();
        if ($compType) {
            $prepSalCompObj = PrepSalaryComponent::where('prep_salary_id', $salaryId)->where('prep_salary_component_type_id', $compType->id)->first();
        }

        $prepUsers = PrepUser::where('prep_salary_id', $salaryId)->get();
        $users = User::orderBy('is_active', 'desc')->orderBy('employee_id')->get();
        return view('pages.admin.prep-salary.prep-salary.users-view', compact('users', 'prepSalaryObj', 'prepSalCompObj', 'prepUsers'));
    }

    public function setUsers(Request $request, $salaryId)
    {
        //check
        $selectedUsers = $request->all();
        $prepSalaryObj = PrepSalary::find($salaryId);

        if (empty($selectedUsers['options']) || !isset($selectedUsers['options'])) {
            return Redirect::back()->withErrors('Please select at least one user.');
        }

        $users = User::all();
        PrepUser::where('prep_salary_id', $salaryId)->delete();

        foreach ($selectedUsers['options'] as $selectedUser) {
            $user = PrepUser::create(['prep_salary_id' => $salaryId, 'user_id' => $selectedUser]);
        }
        return redirect('/admin/prep-users/' . $salaryId);
    }

    public function statusMonth($prepSalaryId, $monthId)
    {
        if (!\Auth::User()->isAdmin()) {
            return Redirect::back()->withErrors('Unauthorized Access');
        }
        $userComponentId = PrepSalaryComponentType::where('code', 'user')->first()->id;
        $prepSalaryComponentObj = PrepSalaryComponent::where('prep_salary_id', $prepSalaryId)->where('prep_salary_component_type_id', $userComponentId)->first();
        if (!$prepSalaryComponentObj) {
            return Redirect::back()->withErrors('Prep Salary Component Not Found!');
        }

        $prepSalaryComponentObj->is_generated = 1;
        if ($prepSalaryComponentObj->status == null || $prepSalaryComponentObj->status == 'open') {
            $prepUsers = PrepUser::where('prep_salary_id', $prepSalaryId)->get();
            if (!$prepUsers || empty($prepUsers) || !isset($prepUsers)) {
                return Redirect::back()->withErrors('Please select at least one user!');
            }
            $prepSalaryComponentObj->status = 'closed';
        } else if ($prepSalaryComponentObj->status == 'closed') {
            $prepSalaryComponentObj->status = 'open';
        }

        if (!$prepSalaryComponentObj->save()) {
            return Redirect::back()->withErrors('Something went wrong!');
        }

        $count = PrepUser::where('prep_salary_id', $prepSalaryId)->count();
        if ($count < 1) {
            return Redirect::back()->withErrors('Please select atleast one user.');
        }

        return Redirect::back()->withMessage($response['message']);
    }

    public function generateAll($prepSalaryId)
    {
        $prepSalary = PrepSalary::find($prepSalaryId);
        if (!$prepSalary) {
            return Redirect::back()->withErrors("Prep Salary Not Found");
        }

        if ($prepSalary->status != "open" && $prepSalary->status != "processed") {
            return Redirect::back()->withErrors("PrepSalary already processed/processing");
        }
        $userComponent = PrepSalaryComponentType::where('code', 'user')->first();
        if (!$userComponent) {
            return Redirect::back()->withErrors("User Component Missing!");
        }
        PrepSalaryComponent::where('prep_salary_id', $prepSalaryId)->where('prep_salary_component_type_id', '!=', $userComponent->id)->update(['is_generated' => 0]);
        if ($prepSalary->status == "open" || $prepSalary->status == "processed") {
            $response = SalaryService::checkPreCondition($prepSalaryId);
            if ($response['status']) {
                return Redirect::back()->withErrors($response['errors']);
            }
            PrepSalaryExecution::where('prep_salary_id', $prepSalaryId)->delete();
            $prepSalary->status = "in_progress";
            if ($prepSalary->save()) {
                foreach($prepSalary->components as $component)
                {
                    if($component->is_generated == 1)
                        continue;

                    $strModelName = $component->type->service_class;
                    $strModelObj = new $strModelName;
                    $strModelObj->setComponent($component->id);
                    $strModelObj->queue();                    
                }
            }

        }

        return Redirect::back();
    }

    public function downloadCSVTds($id)
    {
        $prepSalary = PrepSalary::find($id);
        if (!$prepSalary) {
            return redirect()->back()->withErrors('Prep Salary Id Not Found')->withInput();
        }
        $prepTdsObjs = PrepTds::where('prep_salary_id', $prepSalary->id)->get();
        $data = [];
        foreach ($prepTdsObjs as $index => $prepTdsObj) {
            $data[$prepTdsObj->id]["Sl_No"] = $index + 1;
            $data[$prepTdsObj->id]["Employee Id"] = $prepTdsObj->user->employee_id;
            $data[$prepTdsObj->id]["Employee Name"] = $prepTdsObj->user->name;
            foreach ($prepTdsObj->components as $component) {
                $component->key = str_replace("-", " ", $component->key);
                $component->key = ucwords(strtolower($component->key));
                $data[$prepTdsObj->id][$component->key] = $component->value;
            }
        }
        $filename = "Prep_Tds";
        $export_data = $data;
        return Excel::create($filename, function ($excel) use ($export_data) {
            $excel->sheet('mySheet', function ($sheet) use ($export_data) {
                $sheet->fromArray($export_data);
            });
        })->download('csv');
    }

    public function prepSalarySheetDownload($id)
    {
        $prepSalObj = PrepSalary::find($id);
        if (!$prepSalObj) {
            return redirect()->back()->withErrors('Prep Salary Id Not Found')->withInput();
        }
        $data = SalaryService::getSalarySheetData($id);
        $dataExp = [];
        $i = 0;
        foreach ($data as $index => $obj) {
            $i++;
            foreach ($obj as $index2 => $obj2) {
                $index2 = str_replace("_", " ", $index2);
                $dataExp[$index]['Sl_No'] = $i;
                $dataExp[$index][$index2] = $obj2;
            }
        }
        $filename = "Prep_Salary_Sheet";
        $export_data = $dataExp;
        return Excel::create($filename, function ($excel) use ($export_data) {
            $excel->sheet('mySheet', function ($sheet) use ($export_data) {
                $sheet->fromArray($export_data);
            });
        })->download('csv');
    }

    public static function salarySheetCompare($monthId = null)
    {
        if ($monthId == null) {
            $month = date('m');
            $monthObj = Month::where('month', $month)->orderBy('year', 'DESC')->first();
        } else {
            $monthObj = Month::find($monthId);
        }

        if (!$monthObj) {
            return redirect()->back()->withErrors('Month Not Found')->withInput();
        }
        $prepSalObj = PrepSalary::where('month_id', $monthObj->id)->whereNotIn('status', ['open', 'discarded'])->orderBy('created_at', 'DESC')->first();
        if (!$prepSalObj) {
            return redirect()->back()->withErrors('Prep Salary Id Not Found')->withInput();
        }

        $response = SalaryService::getComparedSalarySheet($prepSalObj->id);

        if ($response['status'] == false) {
            return redirect()->back()->withErrors($response['errors'])->withInput();
        }

        $data = $response['data']['combined'];
        $prevMonthObj = $response['data']['prev_month'];
        $users = User::orderBy('employee_id', 'ASC')->get();
        $apprCompType = AppraisalComponentType::all();
        $monthList = Month::orderBy('year', 'DESC')->orderBy('month', 'DESC')->get();

        return view('pages.admin.prep-salary.prep-salary.salary-sheet-compare', compact('data', 'prepSalObj', 'users', 'apprCompType', 'prevMonthObj', 'monthList'));
    }

    public function salarySheetCompareDownload($prepSalId)
    {
        $prepSalObj = PrepSalary::find($prepSalId);
        if (!$prepSalObj) {
            return redirect()->back()->withErrors('Prep Salary Id Not Found')->withInput();
        }

        $response = SalaryService::getComparedSalarySheet($prepSalObj->id);
        if ($response['status'] == false) {
            return redirect()->back()->withErrors($response['errors'])->withInput();
        }

        $data = $response['data']['combined'];

        $dataExp = [];
        $i = 0;
        foreach ($data as $index => $obj) {
            $i++;
            foreach ($obj as $index2 => $obj2) {
                $index2 = str_replace("_", " ", $index2);
                $dataExp[$index]['Sl_No'] = $i;
                $dataExp[$index][$index2] = $obj2;
            }
        }
        $filename = "Prep_Salary_Sheet";
        $export_data = $dataExp;
        return Excel::create($filename, function ($excel) use ($export_data) {
            $excel->sheet('mySheet', function ($sheet) use ($export_data) {
                $sheet->fromArray($export_data);
            });
        })->download('csv');
    }
}
