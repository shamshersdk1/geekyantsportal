<?php
namespace App\Http\Controllers\Admin\Tds;

use App\Http\Controllers\Controller;
use App\Models\Admin\FinancialYear;
use App\Models\Appraisal\AppraisalBonusType;
use App\Models\Appraisal\AppraisalComponentType;
use App\Models\Audited\AdhocPayments\AdhocPaymentComponent;
use App\Models\Audited\Salary\PrepTds;
use App\Models\Audited\WorkingDay\UserWorkingDay;
use App\Models\Month;
use App\Models\User;
use App\Services\Payroll\TDSService;
use Redirect;
use Session;

class TdsController extends Controller
{
    public function index($userId, $financialYearId = null)
    {
        $userObj = User::find($userId);
        if (!$userObj) {
            return redirect()->back()->with('error', 'Unable to find User!');
        }
        if ($financialYearId == null) {
            $year = date('Y');
            $financialYearObj = FinancialYear::where('year', $year)->first();
            $financialYearId = $financialYearObj->id;
        } else {
            $financialYearObj = FinancialYear::find($financialYearId);
            $year = $financialYearObj->year;
        }
        $monthObj = Month::where('status', 'in_progress')->where('financial_year_id', $financialYearId)->orderBy('year', 'ASC')->orderBy('month', 'ASC')->first();
        if (!$monthObj) {
            $currMonth = date('m');
            $monthObj = Month::where('month', $currMonth)->where('financial_year_id', $financialYearId)->first();
        } else {
            $currMonth = $monthObj->month;
        }

        $tdsResponse = TDSService::getUserTDSSummary($userId, $financialYearId);

        if ($tdsResponse['status'] == false) {
            Session::flash('alert-class', $tdsResponse['errors']);
            $tdsData = $tdsResponse['data'];
            $tdsSummary = [];
        } else {
            Session::flash('alert-class', '');
            $tdsData = $tdsResponse['data'];
            $tdsSummary = TDSService::getUserAnnualSummary($tdsData, $financialYearId, $userId);
        }

        $appraisalCompType = AppraisalComponentType::all();
        $appraisalBonusType = AppraisalBonusType::all();
        $adhocPaymentComponents = AdhocPaymentComponent::all();
        $financialYearList = FinancialYear::all();
        $userList = User::orderBy('employee_id', 'ASC')->get();
        $monthsArr = FinancialYear::getAllFinancialMonth($financialYearId);

        $tdsId = AppraisalComponentType::where('code', 'tds')->first()->id;

        $lastMonth = Month::where('status', 'processed')->where('financial_year_id', $financialYearObj->id)->orderBy('year', 'DESC')->orderBy('month', 'DESC')->first();
        if ($lastMonth) {
            $prepMonth = $lastMonth->month;
        } else {
            $prepMonth = date('m');
        }
        return view('pages.tds.index', compact('lastMonth', 'prepMonth', 'monthObj', 'adhocPaymentComponents', 'userObj', 'appraisalCompType', 'financialYearList', 'financialYearObj', 'userList', 'tdsData', 'monthsArr', 'appraisalBonusType', 'tdsSummary', 'currMonth', 'tdsId'));
    }

    public function showUserTDS($monthId, $user_id)
    {
        $data = $this->_userTDSData($monthId, $user_id);

        return view('pages.tds.tds-payslip', $data);
    }

    private function _userTDSData($monthId, $user_id)
    {
        $monthObj = Month::find($monthId);
        if ($monthObj == null) {
            return null;
        }

        $prepSalariesArr = TDSService::getUserPrepSalaries($user_id, $monthObj->financial_year_id);

        $userWorkingObj = UserWorkingDay::where('user_id', $user_id)->where('month_id', $monthId)->first();
        if ($userWorkingObj) {
            $workingDays = $userWorkingObj->working_days;
            $days_worked = $userWorkingObj->working_days + $userWorkingObj->lop;
        }

        $tdsSummary = [];
        if (!empty($prepSalariesArr) && isset($prepSalariesArr[$monthId])) {
            $prepSalId = $prepSalariesArr[$monthId];
            $tdsObj = PrepTds::where('prep_salary_id', $prepSalId)->where('user_id', $user_id)->first();
            if ($tdsObj->components != null && count($tdsObj->components) > 0) {
                foreach ($tdsObj->components as $component) {
                    $tdsSummary[$component->key] = $component->value;
                }
            }
        }

        $user = User::find($user_id);

        return compact('tdsSummary', 'user', 'monthObj');
    }

    public function downloadUserTDS($monthId, $user_id)
    {
        $data = $this->_userTDSData($monthId, $user_id);
        $pdf = \PDF::loadView('pages.tds.pdf.tds-breakdown', $data);
        return $pdf->download('tds-breakdown-' . $user_id . '.pdf', array('Attachment' => 0));
    }
}
