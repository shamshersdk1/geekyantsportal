<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Admin\Department;
use App\Models\User;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Redirect;
use Config;
use Exception;
use DB;
use Auth;
use Validator;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = Department::paginate(100);
        return View('pages/admin/department/index', ['departments' => $departments] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::where('is_active',1)->get();
        return View('pages/admin/department/add',['users' => $users]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'head'  => 'required',
			'first_contact'  => 'required',
			'type'  => 'required'
        ]);
        if ($validator->fails()) {
            return redirect::back()->withErrors($validator->errors())->withInput($request->input());
        }
        $departmentObj = new Department();
        $departmentObj->type = $request->type;
        $departmentObj->head = $request->head;
        $departmentObj->description = $request->description;
        $departmentObj->first_contact = $request->first_contact;
    
        if ( $departmentObj->save() )
        {
            return redirect('/admin/department')->with('message', 'Successfully added department');
        }
        else
        {
            return Redirect::back()->withInput()->withErrors($departmentObj->getErrors());
        }

    }
    /**
     * Show details of the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $departmentObj = Department::find($id);
        if ( $departmentObj ) {
            return View('pages/admin/department/show', ['departmentObj' => $departmentObj] );
        } else {
            return Redirect::back()->withInput()->withErrors('No record found');
        }
        
        
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $departmentObj = Department::find($id);
        $users = User::where('is_active',1)->get();
        if($departmentObj){

            return View('pages/admin/department/edit', ['departmentObj' => $departmentObj, 'users' => $users]);
        }
        else {
            return Redirect::back()->withInput()->withErrors('No record found');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'head'  => 'required',
			'first_contact'  => 'required',
			'type'  => 'required'
        ]);
        if ($validator->fails()) {
            return redirect::back()->withErrors($validator->errors())->withInput($request->input());
        }
        $departmentObj = Department::find($id);
        if ( $departmentObj ) {
            $departmentObj->type = $request->type;
            $departmentObj->head = $request->head;
            $departmentObj->first_contact = $request->first_contact;
            $departmentObj->description = $request->description;
            if ( $departmentObj->save() )
            {
                return redirect('/admin/department')->with('message', 'Successfully updated designation');
            }
            else
            {
                return Redirect::back()->withInput()->withErrors($departmentObj->getErrors());
            }
        } else {
            return Redirect::back()->withInput()->withErrors('No record found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $departmentObj = Department::find($id);
        if ( $departmentObj->delete() )
        {
            return redirect('/admin/department')->with('message','Successfully Deleted');
        }
        else
        {
            return redirect::back()->withErrors('Error while deleting the record');
        }
    }
    
}
