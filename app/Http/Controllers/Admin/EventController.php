<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Event;
use App\Models\UserEvent;
use Redirect;
use Exception;

class EventController extends Controller
{
    public function index()
    {
        $events=Event::orderBy('id','desc')->get();
        return view('pages.admin.events.index', compact('events'));
    }
    public function create()
    {
        $available_users = User::where('is_active', 1)->get();
        return view('pages.admin.events.add', compact('available_users'));
    }
    public function store(Request $request)
    {   
        $data = $request->toArray();
        $response = Event::saveData($data);
        if(!empty($response['status']) && $response['status'] == false)
            return Redirect::back()->withErrors($response['message']);  
        $responseUser = UserEvent::saveData($data['new_developers'],$response['data']);
        if(!empty($responseUser['status']) && $responseUser['status'] == false)
            return Redirect::back()->withErrors($response['message'].$responseUser['message']);  
        return redirect('/admin/events')->with('message', $response['message']." ".$responseUser['message']);
    }
    public function show($id)
    {
        $event=Event::find($id);
        $userevents=UserEvent::where('event_id', $id)->get();
        return view('pages.admin.events.show', compact('event', 'userevents'));
    }
    public function destroy($id)
    {
        $event=Event::find($id);
        if($event){
            $event->delete();
            return redirect()->back()->withErrors('Event deleted!');
        }
        return redirect()->back()->withErrors('Not Found!');
    }
    public function edit($id)
    {
        $event = Event::find($id);
        $available_users = UserEvent::get();
        //$available_users = User::where('is_active', 1)->get();
        $all_users = User::where('is_active', 1)->get();
        //print_r($available_users);
        //die;
        if ( isset($event) ) 
            return View('pages/admin/events/edit',compact('event','available_users','all_users') );    
        return Redirect::back()->withErrors($event->getErrors());
    }
    public function update(Request $request, $id)
    {
        
        $data = $request->toArray();
        $response = Event::updateData($data, $id);
        if(!empty($response['status']) && $response['status'] == false)
            return Redirect::back()->withErrors($response['message']); 
        if(!empty($data['new_developers'])){
            $responseUser = UserEvent::updateData($data['new_developers'],$response['data']);
            if(!empty($responseUser['status']) && $responseUser['status'] == false)
                return Redirect::back()->withErrors($response['message'].$responseUser['message']);  
            return redirect('/admin/events')->with('message', $response['message']." ".$responseUser['message']);
        }
        return Redirect::back()->withErrors($response['message']); 
    }
}
