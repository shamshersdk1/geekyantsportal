<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\CalendarYear;
use App\Models\Leave\UserLeaveTransaction;
use App\Models\Month;
use App\Models\User;
use App\Services\NewLeaveService;
use Redirect;

class LeaveMisController extends Controller
{
    public function index()
    {
        $months = Month::getMonthList();
        return view('pages.admin.leave-mis.index', ['months' => $months]);
    }
    public function monthOverview($monthId)
    {
        $month = Month::find($monthId);
        if (!$month) {
            return redirect::to('/admin/leave-section/mis-leave-report')->withErrors('Invalid month #' . $monthId);
        }
        $userLeaveDetail = NewLeaveService::getMonthlyLeaveReport($monthId);
        return view('pages.admin.leave-mis.month-overview', compact('month', 'userLeaveDetail'));
    }
    public function yearOverview($monthId, $userId)
    {
        $monthValid = Month::find($monthId);
        if (!$monthValid) {
            return redirect::to('/admin/leave-section/mis-leave-report')->withErrors('Invalid month #' . $monthId);
        }
        $calendarYear = CalendarYear::where('year', $monthValid->year)->first();
        if (!$calendarYear) {
            return redirect::to('/admin/leave-section/mis-leave-report')->withErrors('Invalid Calender #' . $calendarYear->id);
        }

        $user = User::find($userId);
        if (!($user)) {
            return redirect::to('/admin/leave-section/mis-leave-report')->withErrors('Invalid user #' . $userId);

        }
        $months = Month::getMonthList($monthValid->year);
        $userLeaveDetail = [];
        foreach ($months as $month) {
            $userLeaveDetail[] = NewLeaveService::getYearlyLeaveReport($month->id, $userId);
        };
        $allLeaveTotal = 0;
        foreach ($userLeaveDetail as $leave) {
            $allLeaveTotal += $leave["allLeaveTotal"];
        }
        $totalLeaveDetail = NewLeaveService::getYearlyLeaves($monthValid->year, $userId);
        $upcomingLeaves = NewLeaveService::getUpcomingLeaves($userId);
        $leaveCreditDetails = UserLeaveTransaction::where('calendar_year_id', $calendarYear->id)->where('user_id', $userId)->with('reference')->with('leaveType')->orderBy('id', 'asc')->get();
        // echo "<pre>";
        // foreach ($leaveCreditDetails as $abc) {
        //     print_r($abc);
        //     print_r($abc->reference->calendarYear);
        //     print_r("---------------------------------------------------------");
        //     print_r("<br/>");
        // }

        // die;
        $leaveCredit = 0;
        if ($leaveCreditDetails) {
            $leaveCredit = $leaveCreditDetails;
        }
        return view('pages.admin.leave-mis.year-overview', compact('user', 'months', 'monthValid', 'userLeaveDetail', 'totalLeaveDetail', 'upcomingLeaves', 'allLeaveTotal', 'leaveCredit'));
    }

}
