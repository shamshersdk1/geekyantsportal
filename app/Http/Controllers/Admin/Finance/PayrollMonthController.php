<?php

namespace App\Http\Controllers\Admin\Finance;

use App\Http\Controllers\Controller;
use App\Models\Admin\Appraisal;
use App\Models\Admin\Bonus;
use App\Models\Admin\FeedbackUser;
use App\Models\Admin\Finance\SalaryUserData;
use App\Models\Admin\Insurance\Insurance;
use App\Models\Admin\ItSaving;
use App\Models\Admin\Leave;
use App\Models\Admin\PayroleGroup;
use App\Models\Loan;
use App\Models\Month;
use App\Models\Transaction;
use App\Models\User;
use App\Services\CalendarService;
use App\Services\NewLeaveService;
use App\Services\Payroll\PayrollService;
use App\Services\Payroll\TDSService;
use Redirect;

class PayrollMonthController extends Controller
{
    public function index()
    {
        $months = Month::orderBy('year', 'DESC')->orderBy('month', 'DESC')->get();
        return view('pages.admin.finance.payroll-month.index', compact('months'));
    }
    public function TDSStore($id)
    {
        $status = !empty($_REQUEST['status']) ? $_REQUEST['status'] : null;
        if (!$status) {
            return Redirect::back()->withInput()->withErrors(['Unable to update the status. Invalid parameters']);
        }
        $obj = SalaryUserData::find($id);

        if (!$obj) {
            return Redirect::back()->withInput()->withErrors(['Unable to update the status. Invalid parameters']);
        }
        if ($obj->status != 'pending') {
            return Redirect::back()->withInput()->withErrors(['Cannot update the status']);
        }
        if ($status == 'hold') {
            $obj->status = 'on hold';
            $obj->save();
        } else if ($status == 'approve') {
            $obj->status = 'approved';
            $obj->save();
        }

        TDSService::storeTDS($obj);

        return Redirect::back();
    }
    public function showUsers($monthId)
    {
        $month = Month::find($monthId);
        if (!$month->checkAllStatus()) {
            return Redirect::back()->withInput()->withErrors(['Something is incompleted!']);
        }
        $groups = PayroleGroup::get();
        $users = SalaryUserData::where('month_id', $monthId)->orderBy('employee_id', 'ASC')->get();
        return view('pages.admin.finance.payroll-month.user-list', compact('groups', 'month', 'users'));
    }
    public function showSalaryData($monthId, $id)
    {
        $monthObj = Month::find($monthId);
        if (!$monthObj) {
            return Redirect::back()->withInput()->withErrors(['Invalid Month ID']);
        }
        if (!$monthObj->checkAllStatus()) {
            return Redirect::back()->withInput()->withErrors(['Something is incompleted!']);
        }
        $tdsData = [];
        $financialYearId = $monthObj->financial_year_id;

        $salaryData = SalaryUserData::with('salaryKeys.groupKey.calc_upon')->find($id);
        //$salaryData->getTotalEarning();
        //$salaryData = SalaryUserData::with('salaryKeys.groupKey.calc_upon')->find($id);
        if ($salaryData) {
            // $tdsData = PayrollService::getTDSData($id);
            $tdsData = PayrollService::getTDSDataNew($id);

            // echo "<pre>";
            // print_r($tdsData);
            // die;
            $userId = $salaryData->user_id;
            // $appraisalId = $salaryData->appraisal_id;
            $user = User::find($userId);
            // $tdsData = PayrollService::getTDSData($id);
        }

        //$bonuses = Bonus::where('user_id', $userId)->where('month_id', $monthId)->get();
        $bonuses = Bonus::getPreviousMonthBonues($userId, $monthId);

        $loans = Loan::where('user_id', $userId)->where('status', 'in_progress')->get();
        $itSaving = ItSaving::where('user_id', $userId)->where('financial_year_id', $financialYearId)->first();
        //$insurances = InsuranceDeduction::where('user_id', $userId)->where('month_id', $monthId)->get();

        $insurances = Insurance::where('insurable_id', $userId)->get();
        $startDate = date('Y-m-d', strtotime($monthObj->getFirstDay()));
        $endDate = date('Y-m-d', strtotime($monthObj->getLastDay()));
        $userLeave['sick_leave'] = NewLeaveService::getTotalLeaves($startDate, $endDate, $userId, 'sick');
        $userLeave['paid_leave'] = NewLeaveService::getTotalLeaves($startDate, $endDate, $userId, 'paid');
        $userLeave['days_worked'] = NewLeaveService::calculateWorkingDays($startDate, $endDate, $userId);
        $userLeave['working_days'] = CalendarService::getWorkingDays($monthObj->month, $monthObj->year);

        $userLeaves = Leave::where('user_id', $userId)->where('start_date', '>=', $monthObj->getFirstDay())->where('end_date', '<=', $monthObj->getLastDay())->whereIN('status', ['approved', 'pending'])->get();

        $feedbackRating = FeedbackUser::where('user_id', $userId)->where('feedback_month_id', $monthId)->sum('rating');

        $appraisal = Appraisal::getAppraisalByDate($userId, $startDate);
        $oneDaySalary = Appraisal::getOneDaySalary($userId, $startDate);

        return view('pages.admin.finance.payroll-month.user-data', compact('user', 'salaryData', 'bonuses', 'loans', 'itSaving', 'tdsData', 'insurances', 'userLeave', 'feedbackRating', 'appraisal', 'monthObj', $oneDaySalary));
    }
    public function showTransaction($monthId, $id)
    {
        $monthObj = Month::find($monthId);
        if (!$monthObj) {
            return Redirect::back()->withInput()->withErrors(['Invalid Month ID']);
        }
        if (!$monthObj->checkAllStatus()) {
            return Redirect::back()->withInput()->withErrors(['Something is incompleted!']);
        }
        $tdsData = [];
        $financialYearId = $monthObj->financial_year_id;

        $salaryData = SalaryUserData::with('salaryKeys.groupKey.calc_upon')->find($id);
        if ($salaryData) {
            // $tdsData = PayrollService::getTDSData($id);
            $userId = $salaryData->user_id;
            // $appraisalId = $salaryData->appraisal_id;
            $user = User::find($userId);
            // $tdsData = PayrollService::getTDSData($id);
        }
        $transactions = Transaction::with('reference')->where('user_id', $user->id)->where('month_id', $monthId)->get();
        // echo "<pre>";
        // print_r($transactions[0]->reference->toArray());
        // print_r($transactions->toArray());
        // die;
        $bonuses = Bonus::where('user_id', $userId)->where('month_id', $monthId)->get();

        $loans = Loan::where('user_id', $userId)->where('status', 'in_progress')->get();
        $itSaving = ItSaving::where('user_id', $userId)->where('financial_year_id', $financialYearId)->first();

        return view('pages.admin.finance.payroll-month.user-transaction-data', compact('user', 'monthObj', 'transactions'));
    }
    // public function showGroupRule($monthId, $groupId)
    // {
    //     $month = Month::find($monthId);
    //     $groupObj = PayroleGroup::find($groupId);
    //     $groupRules = PayroleGroupRuleMonth::where('payrole_group_id', $groupId)->where('month_id', $monthId)->get();

    //     if (count($groupRules) == 0) {
    //         $groupRules = PayroleGroupRuleMonth::addGroupRule($monthId, $groupId);
    //     }

    //     if (!$month || !$groupObj) {
    //         return Redirect::back()->withInput()->withErrors(['Invalid Month or Group ID']);
    //     }

    //     return view('pages.admin.finance.payroll-rules-for-csv-month.group-rule-month', compact('groupObj', 'month', 'groupRules'));
    // }
    // public function editGroupRule($monthId, $groupId, $ruleId)
    // {

    // }
}
