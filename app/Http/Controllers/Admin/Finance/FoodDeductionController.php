<?php

namespace App\Http\Controllers\Admin\Finance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Month;
use App\Models\MonthSetting;
use Redirect;
use App\Models\Audited\FoodDeduction\FoodDeduction;
use App\Models\Audited\WorkingDay\UserWorkingDay;

use App\Models\User;

class FoodDeductionController extends Controller
{
    public function index()
    {
        $months = Month::orderBy('year', 'DESC')->orderBy('month', 'DESC')->get();
        if (!count($months)>0) {
            return Redirect::back()->withInput()->withErrors(['no month found']);
        }
        return view('pages.admin.finance.food-deduction.index', compact('months'));
    }

    public function show($id)
    {
        $month = Month::find($id);
        if (!count($month)>0) {
            return Redirect::back()->withInput()->withErrors(['month not found']);
        }

        $users=User::where('is_active',1)->orderBy("employee_id")->get();
        if (!count($users)>0) {
            return Redirect::back()->withInput()->withErrors(['No User found']);
        }
        $foodDeductionObjs = FoodDeduction::where('month_id',$id)->get();
        return view('pages.admin.finance.food-deduction.show', compact('month','users','foodDeductionObjs'));
    }

   

    public function lockMonth($monthId){
        if (!\Auth::User()->isAdmin()) {
            return Redirect::back()->withErrors('Unauthorized Access');
        }
        $response = MonthSetting::monthStatusToggle($monthId,'monthly-food-deduction');
        if(!empty($response['status']) && $response['status'] == false)
            return Redirect::back()->withErrors('Something went wrong!');          
        return Redirect::back()->withMessage($response['message']);
    }

    public function update(Request $request,$monthId){
        $status = false;
        $data = $request->new_amount;
        $errors = "";
        if(!empty($data)){
            foreach($data as $index=>$loan){  
            $deductionObj = FoodDeduction::find($index);
            $deductionObj->amount = $data[$index];
            if(!$deductionObj->save()){
                $status = true;
                $errors = $deductionObj->getErrors();
            }
         }
        }
        if($status){
            return Redirect::back()->withErrors($errors);    
        }   
        return redirect('/admin/food-deduction/'.$monthId);
    }

    public function destroy($id)
    {
        $foodDeductionObj = FoodDeduction::find($id);
        if($foodDeductionObj && $foodDeductionObj->delete())
        {
            return Redirect::back()->withErrors("Record of ".$foodDeductionObj->user->name." has been deleted.");
        }
        return Redirect::back()->withErrors(["Entry not found!"]);
    }

    public function regenerate($monthId)
    {
        $month = Month::find($monthId);
        if(!$month) {
            $response['message'] = "Invalid month id ".$monthId;
            return $response;
        }
        if($month->monthlyVariableBonusSetting == 'locked') {
            $response['message'] = "Cannot regenrate. Month is locked";
            return $response;
        }
        if($month->workingDaySetting?$month->workingDaySetting->value != "locked":true)
        {
            return redirect()->back()->withErrors('Working Day Month not locked');
        }
        $users=User::where('is_active',1)->orderBy("employee_id")->get();
        if (!count($users)>0) {
            return Redirect::back()->withInput()->withErrors(['No User found']);
        }
        FoodDeduction::where('month_id',$monthId)->delete();
        foreach($users as $user){
            $user_working_day = UserWorkingDay::where('month_id',$monthId)->where('user_id',$user->id)->first();
            $amount = -1500;
            if($user_working_day)
                $amount = round((-1500 / $month->working_days ) * $user_working_day->working_days,2);
            $foodDeductionObj = FoodDeduction::create(['user_id' => $user->id,'month_id' => $monthId,'amount' => $amount , 'actual_amount'=> $amount]);
            if(!$foodDeductionObj->isValid())
            {
                return Redirect::back()->withInput()->withErrors($foodDeductionObj->getErrors());
            }
        }
        return redirect('/admin/food-deduction/'.$monthId)->withMessage("Regenerated");
    }
}
