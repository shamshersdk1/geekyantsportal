<?php

namespace App\Http\Controllers\Admin\Finance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Month;
use App\Models\MonthSetting;
use Redirect;
use App\Models\Appraisal\AppraisalBonus;
use App\Models\Appraisal\Appraisal;
use App\Models\Appraisal\AppraisalBonusType;
use App\Models\Audited\MonthlyVariableBonus\MonthlyVariableBonus;
use App\Models\Audited\FixedBonus\FixedBonus;
use App\Models\Audited\FixedBonus\FixedBonusComponent;
use Illuminate\Database\Eloquent\Builder;

class FixedBonusController extends Controller
{
    public function index()
    {
        $months = Month::orderBy('year', 'DESC')->orderBy('month', 'DESC')->get();
        if (!count($months)>0) {
            return Redirect::back()->withInput()->withErrors(['no month found']);
        }
        return view('pages.admin.finance.fixed-bonus.index', compact('months'));
    }

    public function show($id)
    {
        $month = Month::find($id);
        if (!count($month)>0) {
            return Redirect::back()->withInput()->withErrors(['month not found']);
        }
        $appraisalBonusTypes = AppraisalBonusType::all();
        if(!count($appraisalBonusTypes)>0)
        {
             return Redirect::back()->withInput()->withErrors(['Appraisal Bonus Types not found']);
        }
        $monthlyDeductions = FixedBonus::where('month_id',$id)->get();
        return view('pages.admin.finance.fixed-bonus.show', compact('appraisalBonusTypes','month', 'monthlyDeductions','monthId'));
    }

    public function destroy($id)
    {
       $fixed_bonus_obj = FixedBonus::find($id);
       if($fixed_bonus_obj && $fixed_bonus_obj->delete())
       {
            return redirect('/admin/fixed-bonus/')->withErrors("Record of ".$fixed_bonus_obj->user->name." has been deleted.");
       }
        return redirect('/admin/fixed-bonus/')->withErrors(["Entry not found!"]);
    }

    public function lockMonth($monthId){
        if (!\Auth::User()->isAdmin()) {
            return Redirect::back()->withErrors('Unauthorized Access');
        }
        $response = MonthSetting::monthStatusToggle($monthId,'fixed-bonus');
        if(!empty($response['status']) && $response['status'] == false)
            return Redirect::back()->withErrors('Something went wrong!');          
        return Redirect::back()->withMessage($response['message']);
    }

    public function update(Request $request,$monthId){
       
        $status = false;
        $data = $request->new_amount;
        $response=[];
        if(!empty($data)){
            foreach($data as $index=>$data2){  
                foreach($data2 as $index2=>$amount){  
                    $deductionObj = FixedBonusComponent::find($index);
                    if($deductionObj){
                        if(!$amount || $amount==0){
                            $deductionObj->delete();
                        }
                        else{
                        $deductionObj->value = $amount;
                            if(!$deductionObj->update()){
                                $status = true;
                                $errors[] = $deductionObj->getErrors();
                            }
                        }
                    }
                }
            }
        }
        if($status)
            return Redirect::back()->withErrors($errors);       
        return redirect('/admin/fixed-bonus/'.$monthId);
    }

    public function regenerate($monthId)
    {
        $status = false;
        $errors = '';
        $month = Month::find($monthId);
        if(!$month) {
            $response['message'] = "Invalid month id ".$monthId;
            return $response;
        }
        if($month->FixedBonusSetting == 'locked') {
            $response['message'] = "Cannot regenrate. Month is locked";
            return $response;
        }
        FixedBonus::where('month_id', $monthId)->get()->each(function ($monthylDeductionObj) {
            $monthylDeductionObj->delete();
        });
        $appraisals = Appraisal::whereHas('appraisalBonus', function (Builder $query) use ($month) {
            $query->whereDate('value_date', '>=',$month->getFirstDay())->whereDate('value_date', '<=',$month->getLastDay());
        })->get();
        if(!count($appraisals) > 0)
        {
            return Redirect::back()->withErrors("No appraisal found");
        }
        foreach($appraisals as $appraisal)
        {
            $fixedBonusObj = FixedBonus::create(['user_id' => $appraisal->user_id,'month_id' => $monthId,'appraisal_id' => $appraisal->id]);
            if(!$fixedBonusObj->isValid())
            {
                return Redirect::back()->withErrors($fixedBonusObj->getErrors());
            }
            if($appraisal->appraisalBonus)
            {
                foreach($appraisal->appraisalBonus as $bonus)
                {
                    
                    if(Month::getMonth($bonus->value_date) == $monthId)
                    {
                        $monthylDeductionComponent = FixedBonusComponent::create(['fixed_bonus_id' => $fixedBonusObj->id,'key' => $bonus->appraisalBonusType->code,'value'=> $bonus->value,'appraisal_bonus_id' => $bonus->id]);
                        if(!$monthylDeductionComponent->isValid())
                        {
                            $status = true;
                            $errors = $errors.$monthylDeductionComponent->getErrors();
                        }
                    }
                }
            }
        }
        if($status)
        {
            return Redirect::back()->withErrors($errors);
        }
        $monthlyDeductions = FixedBonus::where('month_id',$monthId)->get();
        $appraisalBonusTypes = AppraisalBonusType::all();
        if(!count($appraisalBonusTypes)>0)
        {
             return Redirect::back()->withInput()->withErrors(['Appraisal Bonus Types not found']);
        }
        return view('pages.admin.finance.fixed-bonus.show', compact('monthlyDeductions','appraisalBonusTypes','month','monthId'));
    }
}
