<?php

namespace App\Http\Controllers\Admin\Finance;

use App\Http\Controllers\Controller;
use App\Models\Admin\Finance\PayroleGroupRuleMonth;
use App\Models\Admin\PayroleGroup;
use App\Models\Admin\PayrollRuleForCsv;
use App\Models\Month;
use Redirect;

class PayrollRuleForCsvMonthController extends Controller
{
    public function index()
    {
        $months = Month::orderBy('year', 'DESC')->orderBy('month', 'DESC')->get();
        $rules = PayrollRuleForCsv::get();
        return view('pages.admin.finance.payroll-rules-for-csv-month.index', compact('months', 'rules'));
    }
    public function showGroup($monthId)
    {
        $month = Month::find($monthId);
        $groups = PayroleGroup::get();

        return view('pages.admin.finance.payroll-rules-for-csv-month.group', compact('groups', 'month'));
    }
    public function showGroupRule($monthId, $groupId)
    {
        $month = Month::find($monthId);
        $groupObj = PayroleGroup::find($groupId);
        $groupRules = PayroleGroupRuleMonth::with('csvValue')->where('payrole_group_id', $groupId)->where('month_id', $monthId)->get();
        // echo "<pre>";
        // print_r($groupRules->toArray());
        // die;
        if (count($groupRules) == 0) {
            $groupRules = PayroleGroupRuleMonth::addGroupRule($monthId, $groupId);
        }

        if (!$month || !$groupObj) {
            return Redirect::back()->withInput()->withErrors(['Invalid Month or Group ID']);
        }

        return view('pages.admin.finance.payroll-rules-for-csv-month.group-rule-month', compact('groupObj', 'month', 'groupRules'));
    }
    public function editGroupRule($monthId, $groupId, $ruleId)
    {

    }
}
