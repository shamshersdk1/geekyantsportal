<?php

namespace App\Http\Controllers\Admin\Finance;

use App\Http\Controllers\Controller;
use App\Models\LoanInterest;
use App\Models\Month;
use App\Models\MonthSetting;
use Illuminate\Http\Request;
use Redirect;

class LoanInterestController extends Controller
{
    public function index()
    {
        $months = Month::orderBy('year', 'DESC')->orderBy('month', 'DESC')->get();
        return view('pages.admin.finance.loan-interest-income.index', compact('months'));
    }

    public function show($monthId)
    {
        $monthObj = Month::find($monthId);
        if (!$monthObj) {
            return Redirect::back()->withErrors('Month not found!');
        }

        $loanInterests = LoanInterest::where('month_id', $monthId)->get();

        return view('pages.admin.finance.loan-interest-income.show', compact('monthObj', 'loanInterests'));
    }

    public function regenerate($month_id)
    {
        $response = LoanInterest::regenerateInterest($month_id);
        if (!empty($response['status']) && $response['status'] == false) {
            return Redirect::back()->withErrors('Something went wrong!');
        }

        return redirect('/admin/loan-interest-income/' . $month_id)->with('message', $response['message']);
    }

    public function monthStatusDeduction($monthId)
    {
        if (!\Auth::User()->isAdmin()) {
            return Redirect::back()->withErrors('Unauthorized Access');
        }
        $response = MonthSetting::monthStatusToggle($monthId, 'loan-interest-income');
        if (!empty($response['status']) && $response['status'] == false) {
            return Redirect::back()->withErrors('Something went wrong!');
        }

        return Redirect::back()->withMessage($response['message']);
    }

    public function update(Request $request, $monthId)
    {
        $amount = $request->interest;

        if (!empty($amount)) {
            $response = LoanInterest::updateData($amount, $monthId);
            if (!empty($response['status']) && $response['status'] == false) {
                return Redirect::back()->withErrors('Something went wrong!');
            }

            return redirect('/admin/loan-interest-income/' . $monthId);
        }
    }
}
