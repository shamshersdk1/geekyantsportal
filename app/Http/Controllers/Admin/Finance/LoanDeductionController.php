<?php

namespace App\Http\Controllers\Admin\Finance;

use App\Http\Controllers\Controller;
use App\Models\Admin\Bonus;
use App\Models\Admin\Finance\PayroleGroupRuleMonth;
use App\Models\Admin\Finance\SalaryUserData;
use Illuminate\Http\Request;
use App\Models\Admin\ItSaving;
use App\Models\Admin\PayroleGroup;
use App\Models\LoanEmi;
use App\Models\Month;
use App\Models\User;
use App\Models\Transaction;
use App\Services\Payroll\PayrollService;
use Illuminate\Database\Eloquent\SoftDeletes;
use Redirect;
use App\Models\MonthSetting;

class LoanDeductionController extends Controller
{
    public function index()
    {
        $months = Month::orderBy('year', 'DESC')->orderBy('month', 'DESC')->get();
        return view('pages.admin.finance.loan-deduction.index', compact('months'));
    }
    public function show($monthId)
    {
        $month = Month::find($monthId);
        //$loanDeductions = LoanEmi::getEmis($monthId);
        $loanDeductions = LoanEmi::where('month_id', $monthId)->get();
        return view('pages.admin.finance.loan-deduction.emi', compact('month', 'loanDeductions','monthId'));
    }
    public function showSalaryData($monthId, $id)
    {
        $monthObj = Month::find($monthId);
        if (!$monthObj) {
            return Redirect::back()->withInput()->withErrors(['Invalid Month ID']);
        }
        $tdsData = [];
        $financialYearId = $monthObj->financial_year_id;
        

        $salaryData = SalaryUserData::with('salaryKeys.groupKey.calc_upon')->find($id);
        $salaryData = SalaryUserData::with('salaryKeys.groupKey.calc_upon')->find($id);
        if($salaryData) {
            $tdsData = PayrollService::getTDSData($id);
            $userId = $salaryData->user_id;
            // $appraisalId = $salaryData->appraisal_id;
            $user = User::find($userId); 
            // $tdsData = PayrollService::getTDSData($id);
        }
        
        $bonuses = Bonus::where('user_id', $userId)->where('month_id', $monthId)->get();
        $loans = Loan::where('user_id', $userId)->where('status', 'in_progress')->get();
        $itSaving = ItSaving::where('user_id', $userId)->where('financial_year_id', $financialYearId)->first();

        return view('pages.admin.finance.payroll-month.user-data', compact('user', 'salaryData', 'bonuses', 'loans', 'itSaving','tdsData'));
    }
    public function showTransaction($monthId, $id)
    {
        $monthObj = Month::find($monthId);
        if (!$monthObj) {
            return Redirect::back()->withInput()->withErrors(['Invalid Month ID']);
        }
        $tdsData = [];
        $financialYearId = $monthObj->financial_year_id;
        

        $salaryData = SalaryUserData::with('salaryKeys.groupKey.calc_upon')->find($id);
        $salaryData = SalaryUserData::with('salaryKeys.groupKey.calc_upon')->find($id);
        if($salaryData) {
            $tdsData = PayrollService::getTDSData($id);
            $userId = $salaryData->user_id;
            // $appraisalId = $salaryData->appraisal_id;
            $user = User::find($userId); 
            // $tdsData = PayrollService::getTDSData($id);
        }
        $transactions = Transaction::with('reference')->where('user_id',$user->id)->where('month_id', $monthId)->get();
        // echo "<pre>";
        // print_r($transactions[0]->reference->toArray());
        // print_r($transactions->toArray());
        // die;
        $bonuses = Bonus::where('user_id', $userId)->where('month_id', $monthId)->get();

        $loans = Loan::where('user_id', $userId)->where('status', 'in_progress')->get();
        $itSaving = ItSaving::where('user_id', $userId)->where('financial_year_id', $financialYearId)->first();

        return view('pages.admin.finance.payroll-month.user-transaction-data', compact('user', 'monthObj', 'bonuses', 'loans', 'itSaving','tdsData','transactions'));
    }
    // public function showGroupRule($monthId, $groupId)
    // {
    //     $month = Month::find($monthId);
    //     $groupObj = PayroleGroup::find($groupId);
    //     $groupRules = PayroleGroupRuleMonth::where('payrole_group_id', $groupId)->where('month_id', $monthId)->get();

    //     if (count($groupRules) == 0) {
    //         $groupRules = PayroleGroupRuleMonth::addGroupRule($monthId, $groupId);
    //     }

    //     if (!$month || !$groupObj) {
    //         return Redirect::back()->withInput()->withErrors(['Invalid Month or Group ID']);
    //     }

    //     return view('pages.admin.finance.payroll-rules-for-csv-month.group-rule-month', compact('groupObj', 'month', 'groupRules'));
    // }
    // public function editGroupRule($monthId, $groupId, $ruleId)
    // {

    // }

    public function monthStatusDeduction($monthId){
        if (!\Auth::User()->isAdmin()) {
            return Redirect::back()->withErrors('Unauthorized Access');
        }
        $response = MonthSetting::monthStatusToggle($monthId,'loan-deduction');
        if(!empty($response['status']) && $response['status'] == false)
            return Redirect::back()->withErrors('Something went wrong!');          
        return Redirect::back()->withMessage($response['message']);
    }

    public function regenerate($month_id){

        $response = LoanEmi::regenerateEmi($month_id);
        if(!empty($response['status']) && $response['status'] == false)
            return Redirect::back()->withErrors('Something went wrong!');   
        return redirect('/admin/loan-deduction/'.$month_id)->with('message',$response['message']);
    }

    public function updateData(Request $request,$monthId){
        $data = $request->new_amount;
        if(!empty($data)){
            $response = LoanEmi::updateData($data,$monthId);
            if(!empty($response['status']) && $response['status'] == false)
                return Redirect::back()->withErrors('Something went wrong!');       
            return redirect('/admin/loan-deduction/'.$monthId);
        }
    }

    public function deleteRecord($loan_deduct_id){
        $loanObj = LoanEmi::find($loan_deduct_id);
        if($loanObj && $loanObj->delete())
        {
            return Redirect::back()->withErrors("Record of ".$loanObj->user->name." has been deleted.");
        }
        return Redirect::back()->withErrors(["Entry not found!"]);
    }
}
