<?php

namespace App\Http\Controllers\Admin\Finance;

use App\Http\Controllers\Controller;
use App\Models\Month;
use App\Models\Transaction;
use App\Models\User;
use App\Services\SalaryService\TransactionService;
use DB;
use Illuminate\Http\Request;
use Redirect;
use App\Models\Appraisal\AppraisalComponentType;
use App\Models\Appraisal\AppraisalBonusType;

class TransactionController extends Controller
{
    public function index()
    {
        $months = Month::orderBy('year', 'desc')->orderBy('month', 'desc')->get();
        return view('pages.admin.finance.transaction.index', compact('months'));
    }
    public function showUserList($monthId)
    {
        $month = Month::find($monthId);
        $transactions = Transaction::where('month_id', $monthId)->get();
        return view('pages.admin.finance.transaction.show', compact('transactions', 'month'));
    }
    public function showUserTransaction($monthId, $userId)
    {
        $month = Month::find($monthId);
        $user = User::find($userId);

        if (!$month || !$user) {
            return Redirect::back()->withInput()->withErrors(['Invalid Month or User ID']);
        }

        $transactions = Transaction::with('reference')->where('month_id', $monthId)->where('user_id', $userId)->get();

        return view('pages.admin.finance.transaction.user-transaction', compact('user', 'month', 'transactions'));
    }
    public function summary($id = null)
    {
        if ($id != null) {
            $monthObj = Month::find($id);
        } else {
            $monthObj = Month::where('status', 'in_progress')->orderBy('year', 'DESC')->orderBy('month', 'DESC')->first();
        }
        if (!$monthObj) {
            return Redirect::back()->withErrors(['Month not found']);
        }

        $users = User::orderBy('employee_id', 'ASC')->get();
        if (!$users || empty($users)) {
            return Redirect::back()->withErrors(['Users not found']);
        }

        $transUsers = DB::select(DB::raw("SELECT DISTINCT user_id
            from transactions WHERE is_company_expense = 0 order by user_id"));

        $userArray = [];
        foreach ($transUsers as $user) {
            $userArray[$user->user_id] = $user->user_id;
        }

        $data = TransactionService::transactionSummary($monthObj->id);

        $monthArray = Month::orderBy('year', 'Desc')->orderBy('month', 'Desc')->get();

        return view('pages.admin.finance.transaction.summary', compact('users', 'data', 'monthArray', 'monthObj', 'userArray'));
    }

    public function create()
    {
        $users = User::where('is_active', 1)->orderBy('employee_id', 'ASC')->get();
        $monthList = Month::orderBy('year', 'Desc')->orderBy('month', 'Desc')->get();

        $appraisalComponentsTypes = AppraisalComponentType::where('is_company_expense',0)->get();
        $appraisalBonuses = AppraisalBonusType::all();

        return view('pages.admin.finance.transaction.add', compact('appraisalComponentsTypes','appraisalBonuses','users', 'data', 'monthList', 'monthObj'));
    }

    public function store(Request $request)
    {
        $monthObj = Month::find($request->month_id);

        $data = [];
        $data['month_id'] = $request->month_id;
        $data['financial_year_id'] = $monthObj->financial_year_id;
        $data['user_id'] = $request->user_id;
        $data['reference_id'] = $request->reference_id;
        $data['reference_type'] = $request->reference_type;
        $data['type'] = ($request->amount < 0) ? "debit" : "credit";
        $data['amount'] = $request->amount;


        $response = Transaction::saveTransaction($data);

        if ($response['status'] == true) {
            return redirect('/admin/transaction-summary')->with('message', $response['message']);
        }
        return Redirect::back()->withInput()->withErrors($response['message']);
    }
}
