<?php

namespace App\Http\Controllers\Admin\Finance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Month;
use App\Models\MonthSetting;
use Redirect;
use App\Models\Appraisal\AppraisalBonus;
use App\Models\Appraisal\Appraisal;
use App\Models\Appraisal\AppraisalBonusType;
use App\Models\Audited\VariablePay\VariablePay;
use App\Models\Audited\VariablePay\VariablePayComponent;

class VariablePayController extends Controller
{
    public function index()
    {
        $months = Month::orderBy('year', 'DESC')->orderBy('month', 'DESC')->get();
        if (!count($months)>0) {
            return Redirect::back()->withInput()->withErrors(['no month found']);
        }
        return view('pages.admin.finance.variable-pay.index', compact('months'));
    }

    public function show($id)
    {
        $month = Month::find($id);
        $lastMonth = Month::where('month',$month->month - 1)->first();
        if (!count($month)>0) {
            return Redirect::back()->withInput()->withErrors(['month not found']);
        }
        $appraisalBonusTypes = AppraisalBonusType::all();
        if(!count($appraisalBonusTypes)>0)
        {
             return Redirect::back()->withInput()->withErrors(['Appraisal Bonus Types not found']);
        }
        $lastMonthPays = VariablePay::where('month_id',$id)->get();
        return view('pages.admin.finance.variable-pay.show', compact('appraisalBonusTypes','month', 'lastMonthPays','lastMonth'));
    }

    public function destroy($monthId,$variablePayId)
    {
        $variable_bonus_obj = VariablePay::find($variablePayId);
        if($variable_bonus_obj && $variable_bonus_obj->delete())
        {
            return redirect('/admin/variable-pay/'.$monthId)->withErrors("Record of ".$variable_bonus_obj->user->name." has been deleted.");
        }
         return redirect('/admin/variable-pay/'.$monthId)->withErrors(["Entry not found!"]);
    }

    public function lockMonth($monthId){
        if (!\Auth::User()->isAdmin()) {
            return Redirect::back()->withErrors('Unauthorized Access');
        }
        $response = MonthSetting::monthStatusToggle($monthId,'variable-pay');
        if(!empty($response['status']) && $response['status'] == false)
            return Redirect::back()->withErrors('Something went wrong!');          
        return Redirect::back()->withMessage($response['message']);
    }

    public function update(Request $request,$monthId){
       
        $status = false;
        $data = $request->new_amount;
        $response=[];
        if(!empty($data)){
            foreach($data as $index=>$data2){  
                foreach($data2 as $index2=>$amount){  
                    $deductionObj = VariablePayComponent::find($index);
                    if($deductionObj){
                        if(!$amount || $amount==0){
                            $deductionObj->delete();
                        }
                        else{
                        $deductionObj->value = $amount;
                            if(!$deductionObj->update()){
                                $status = true;
                                $errors[] = $deductionObj->getErrors();
                            }
                        }
                    }
                }
            }
        }
        if($status)
            return Redirect::back()->withErrors($errors);       
        return redirect('/admin/variable-pay/'.$monthId);
    }

    public function regenerate($monthId)
    {
       
        $status = false;
        $errors = '';
        $month = Month::find($monthId);
        $lastMonth = Month::getLastMonth($monthId);
        if(!$lastMonth) {
            $response['message'] = "Invalid month id ".$monthId;
            return Redirect::back()->withErrors($response['message']);   
        }
        if($month->variablePaySetting == 'locked') {
            $response['message'] = "Cannot regenrate. Month is locked";
            return $response;
        }
         
        VariablePay::where('month_id', $monthId)->get()->each(function ($lastMonthPay) {
            $lastMonthPay->delete();
        });
        $appraisals = Appraisal::getAllAppraisal($lastMonth->id);
        if(!count($appraisals) > 0)
        {
            return Redirect::back()->withErrors("No appraisal found");
        }
        
        foreach($appraisals as $appraisal)
        {
            $lastmonthPay = VariablePay::create(['user_id' => $appraisal->user_id,'month_id' => $monthId, 'appraisal_id' => $appraisal->id]);
            if(!$lastmonthPay->isValid())
            {
                return Redirect::back()->withErrors($lastmonthPay->getErrors());
            }
            
            if($appraisal->appraisalBonus)
            {
                foreach($appraisal->appraisalBonus as $bonus)
                {
                    
                    if($bonus->value_date == null && ($bonus->value != 0))
                    {
                        
                        $lastMonthPayComponent = VariablePayComponent::create(['variable_pay_id' => $lastmonthPay->id,'key' => $bonus->appraisalBonusType->code,'value'=> round($bonus->value/12,2), 'appraisal_bonus_id' => $bonus->id]);
                        if(!$lastMonthPayComponent->isValid())
                        {
                            $status = true;
                            $errors = $lastMonthPayComponent->getErrors();
                        }
                    }
                }
            }
        }
        if($status)
        {
            return Redirect::back()->withErrors($errors);
        }
        $lastMonthPays = VariablePay::where('month_id',$monthId)->get();
        $appraisalBonusTypes = AppraisalBonusType::all();
        if(!count($appraisalBonusTypes)>0)
        {
             return Redirect::back()->withInput()->withErrors(['Appraisal Bonus Types not found']);
        }

        return view('pages.admin.finance.variable-pay.show', compact('lastMonthPays','appraisalBonusTypes','month','monthId'));
    }
}
