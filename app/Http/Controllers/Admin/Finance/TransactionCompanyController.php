<?php

namespace App\Http\Controllers\Admin\Finance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Transaction;
use App\Models\Month;
use App\Models\User;
use Redirect;

class TransactionCompanyController extends Controller
{
    public function summary($id = null)
    {
        $users = User::orderBy('employee_id', 'ASC')->get();
        foreach($users as $user)
        {
            $amount = Transaction::where('is_company_expense','1')->where('user_id',$user->id)->sum('amount');
            if($id > 0)
            {
                $monthObj = Month::find($id);
                $amount = Transaction::where('is_company_expense','1')->where('user_id',$user->id)->where('month_id',$id)->sum('amount');
            }
            if($amount)
                $transactions[$user->id] = $amount;
        }
        if (!$users || empty($users)) {
            return Redirect::back()->withErrors(['Users not found']);
        }
        $monthArray = Month::orderBy('year', 'Desc')->orderBy('month', 'Desc')->get();
        return view('pages.admin.finance.transaction-summary-company.summary', compact('users','transactions', 'monthArray', 'monthObj', 'userArray'));
    }

    public function create()
    {
        $users = User::where('is_active', 1)->orderBy('employee_id', 'ASC')->get();
        $monthList = Month::orderBy('year', 'Desc')->orderBy('month', 'Desc')->get();
        return view('pages.admin.finance.transaction-summary-company.add', compact('users', 'data', 'monthList', 'monthObj'));
    }

     public function store(Request $request)
    {
        $data = $request->all();
        $monthObj = Month::find($data['month_id']);
        $data['date'] = date('Y-m-d');
        $data['financial_year_id'] = $monthObj->financial_year_id;
        $data['type'] = $data['amount'] < 0 ? "debit" : "credit";
        $data['is_company_expense'] = 1; 
        if ($data['type'] == 'debit') {
            $data['amount'] = $data['amount'] < 0 ? $data['amount'] : (-1) * $data['amount'];
        } else {
            $data['amount'] = $data['amount'] > 0 ? $data['amount'] : (-1) * $data['amount'];
        }
        $response = Transaction::saveTransaction($data);
        if ($response['status'] == true) {
            return redirect('/admin/transaction-summary-company')->with('message', $response['message']);
        }
        return Redirect::back()->withInput()->withErrors($response['message']);
    }
}
