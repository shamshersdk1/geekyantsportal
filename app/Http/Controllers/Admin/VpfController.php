<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\User;
use App\Models\Admin\VPF;
use App\Models\Admin\VpfDeduction;
use Redirect;
use App\Models\Month;
use App\Models\MonthSetting;

class VpfController extends Controller
{
    /**
     * To show the list of Months
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vpfList = VPF::where('parent_id', null)->with('user','creator')->orderBy('id','DESC')->get();
        return View('pages/admin/vpf/index', compact('vpfList'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        if ($user->hasRole('admin') || $user->hasRole('account-manager') || $user->hasRole('human-resources'))
            $userList = User::Where('is_active', 1)->orderBy('name', 'asc')->get();
        else
            $userList = User::where('parent_id', $user->id)->where('is_active', 1)->orderBy('name', 'asc')->get();
        return view('pages/admin/vpf/add', compact('userList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$this->validate($request,['amount' =>'required|Numeric',]);
        $response = VPF::saveData($request);
        if($response)
            return redirect('/admin/vpf')->with('message', 'Saved!');
        else
            return redirect('/admin/vpf')->withErrors('Something went wrong!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vpf = VPF::find($id);
        
        $vpfLogs = VPF::where('user_id',$vpf->user_id)->whereNotNull('parent_id')->get();

        return View('pages/admin/vpf/show',compact('vpf','vpfLogs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vpf = VPF::find($id);
        if ( isset($vpf) ) 
            return View('pages/admin/vpf/edit', ['vpf' => $vpf] );    
        return Redirect::back()->withErrors($vpf->getErrors());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       // $this->validate($request,['amount' =>'required|Numeric',]);
        $response = VPF::updateRequest($request,$id);
        if($response == true)
            return redirect('/admin/vpf')->with('message', 'Saved!');
        else
            return Redirect::back()->withErrors($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vpf = VPF::find($id);
        if($vpf)
            if($vpf->delete())
                return redirect('/admin/vpf')->with('message','Successfully Deleted');
            else
                return Redirect::back()->withErrors(["Something went wrong!"]);
        else
            return Redirect::back()->withErrors(["Something went wrong!"]);
    }

    public function approve($id)
    {
        $user = Auth::user();
        if($user->hasRole('account-manager') || $user->hasRole('admin')){
            $vpf =  VPF::find($id);
            $vpf->status = 'approved';
            $vpf->updated_by = $user->id;
            if($vpf->save())
                return Redirect::back()->with('message', 'Successfully Approved');
            else
                 return Redirect::back()->withErrors(["Something went wrong!"]);
        }
        return Redirect::back()->withErrors(["You don't have the necessary permission to view this link"]);
    }

    public function reject($id)
    {
        $user = Auth::user();
        if($user->hasRole('account-manager') || $user->hasRole('admin')){
            $vpf =  VPF::find($id);
            $vpf->status = 'rejected';
            $vpf->updated_by = $user->id;
            if($vpf->save())
                return Redirect::back()->with('message', 'Successfully Rejected');
            else
                 return Redirect::back()->withErrors(["Something went wrong!"]);
        }
         return Redirect::back()->withErrors(["You don't have the necessary permission to view this link"]);
    }

    public function deleteRecord($vpf_id){
        $vpf = VpfDeduction::find($vpf_id);
        if($vpf && $vpf->delete())
                return Redirect::back()->withErrors("Record of ".$vpf->user->name." has been deleted.");
        return Redirect::back()->withErrors("Something went wrong!");
    }

    public function getVpfDeductions(){
        $months = Month::orderBy('year','DESC')->orderBy('month','DESC')->get(); 
        return View('pages/admin/vpf-deduction/index', compact('months'));
    }

    public function viewDeductions($monthId){

        $vpfDeductions = VpfDeduction::where('month_id',$monthId)->get();
        //$month = MonthSetting::where('month_id',$monthId)->where('key','vpf-deduction')->first();
        $month = Month::find($monthId);

        //return View('pages/admin/vpf-deduction/show',['vpfList' => $vpf,'monthId' => $monthId, 'month'=> $month]);
        return View('pages/admin/vpf-deduction/show',compact('month','vpfDeductions'));
    }

    public function storeData(Request $request,$monthId){
        //$this->validate($request,['new_amount' =>'required|Numeric',]);
        $response = VpfDeduction::store($request,$monthId);
        if($response)
            return Redirect::back()->with('message', 'Successfully Added');
        else
            return Redirect::back()->withErrors('Something went wrong!');
    }

    public function regenerate($month_id){
        $response = VpfDeduction::regenerateVpf($month_id);
        if(!empty($response['status']) && $response['status'] == false)
            return Redirect::back()->withErrors('Something went wrong!');       
        
        return redirect('/admin/vpf-deduction/'.$month_id);
    }
    public function monthStatus($monthId){
        if (!\Auth::User()->isAdmin()) {
            return Redirect::back()->withErrors('Unauthorized Access');
        }
        $response = MonthSetting::monthStatusToggle($monthId,'vpf-deduction');
        if(!empty($response['status']) && $response['status'] == false)
            return Redirect::back()->withErrors('Something went wrong!');          
        return Redirect::back()->withMessage($response['message']);
    }
}
