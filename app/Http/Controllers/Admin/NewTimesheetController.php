<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Auth;

class NewTimesheetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    return View('pages/admin/new-timesheet/index');
    }
//     /**
    //      * Show the form for creating a new resource.
    //      *
    //      * @return \Illuminate\Http\Response
    //      */
    public function timesheetHistory()
    {
        return View('pages/admin/new-timesheet/history');
    }
    public function leadView()
    {
        $user = Auth::user();
        return redirect('/admin/timesheet-review');
    }

    public function showAll()
    {
    }
    public function remindMe()
    {
        $user = Auth::user();
        //$user = User::where('id', '110');
        if ($user->timesheet_reminder == 1) {
            $user->timesheet_reminder = 0;
        } else {
            $user->timesheet_reminder = 1;
        }
        return response()->json([
            'data' => [
                'success' => $user->save(),
            ],
        ]);
    }
}
