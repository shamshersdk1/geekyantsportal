<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\ReferralBonus;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Validator;
use App\Models\Admin\Bonus;
use App\Models\Month;


class ReferralBonusController extends Controller
{
    public function index()
    {
        $referralBonuses = ReferralBonus::orderBy('id','DESC')->get();
        return view('pages.admin.referral-bonus.index', compact('referralBonuses'));

    }

    public function create()
    {
        $userList = User::Where('is_active', 1)->orderBy('name', 'asc')->get();
        return view('pages.admin.referral-bonus.add', compact('userList'));
    }

    public function store(Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'referral_id' => 'required',
            'due_date' => 'required',
            'amount' => 'required'
            ]);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        if ( $request->user_id == $request->referral_id )
        {
            return redirect()->back()->withInput()->withErrors('Referred Employee and Referred By can not be the same person');
        }
        $referralBonusObj = new ReferralBonus();
        $referralBonusObj->user_id = $request->user_id;
        $referralBonusObj->referral_id = $request->referral_id;
        $referralBonusObj->due_date = $request->due_date;
        $referralBonusObj->amount = $request->amount;
        $referralBonusObj->status = 'pending';
        $referralBonusObj->notes = !empty($request->notes) ? $request->notes : '';
        if ($referralBonusObj->save()) {
            return redirect('/admin/bonus/referral')->with('message', 'Sucessfully Saved');
        } else {
            return redirect()->back()->withErrors($referralBonusObj->getErrors())->withInput();
        }
    }

    public function edit($id)
    {
        $userList = User::Where('is_active', 1)->orderBy('name', 'asc')->get();
        $referralBonus = ReferralBonus::find($id);
        return view('pages.admin.referral-bonus.edit', compact('referralBonus', 'userList'));
    }

    public function update(Request $request, $id)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'referral_id' => 'required',
            'due_date' => 'required',
            'amount' => 'required'
            ]);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        if ( $request->user_id == $request->referral_id )
        {
            return redirect()->back()->withInput()->withErrors('Referred Employee and Referred By can not be the same person');
        }
        $referralBonusObj = ReferralBonus::find($id);
        $referralBonusObj->user_id = $request->user_id;
        $referralBonusObj->referral_id = $request->referral_id;
        $referralBonusObj->due_date = $request->due_date;
        $referralBonusObj->amount = $request->amount;
        $referralBonusObj->notes = !empty($request->notes) ? $request->notes : '';
        if ($referralBonusObj->save()) {
            return redirect('/admin/bonus/referral')->with('message', 'Updated Successfully!');
        } else {
            return redirect()->back()->withErrors($referralBonusObj->getErrors())->withInput();
        }
        

    }
    public function destroy($id)
    {
        $referralBonusObj = ReferralBonus::find($id)->delete();
        if ($referralBonusObj) {
            return redirect('/admin/bonus/referral')->with('message', 'Successfully deleted!');

        }
        return redirect()->back()->withErrors('Uable to delete record');
    }

    public function approve($id)
    {
        $user = Auth::user();
        $referralObj = ReferralBonus::find($id);
        $referralObj->status = 'approved';
        $referralObj->approver_id = $user->id;
        if ($referralObj->save()) {
            $bonusObj = new Bonus;
            $bonusObj->user_id = $referralObj->user_id;
            $bonusObj->month_id = Month::getMonth($referralObj->due_date);
            $bonusObj->status = 'approved';
            $bonusObj->amount = $referralObj->amount;
            $bonusObj->draft_amount = $referralObj->amount;
            $bonusObj->date = $referralObj->due_date;
            $bonusObj->notes = !empty($referralObj->comment) ? $referralObj->comment : null;
            $bonusObj->created_by = $user->id;
            $bonusObj->approved_by = $user->id;
            $bonusObj->type = "referral";
            $bonusObj->reference_id = $referralObj->id;
            $bonusObj->reference_type = 'App\Models\Admin\RefferralBonus';
            if ($bonusObj->save()) {
                return redirect('/admin/bonus/referral')->with('message', 'Approved!');
            }else{
                return redirect('/admin/bonus/referral')->withErrors($bonusObj->getErrors());
            }
        }
        return redirect('/admin/bonus/referral')->withErrors($referralObj->getErrors());
    }

    public function reject($id)
    {
        $user = Auth::user();
        $referralObj = ReferralBonus::find($id);
        $referralObj->status = 'rejected';
        $referralObj->approver_id = $user->id;
        if ($referralObj->save()) {
            return redirect('/admin/bonus/referral')->with('message', 'Rejected!');
        }
        return redirect('/admin/bonus/referral')->withErrors($referralObj->getErrors());
    }
    
}
