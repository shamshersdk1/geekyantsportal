<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Admin\FeedbackUser;
use App\Models\Admin\FeedbackMonth;
use App\Models\User;
use App\Models\Admin\Role;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Services\FeedbackService;

use Redirect;
use Config;
use Exception;
use DB;
use Auth;
use Validator;

class FeedbackUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getRMList()
    {
        $user = Auth::user();
        $reporteeList = $user->children;
        $feedbackMonths = FeedbackMonth::all();
        return View('pages/admin/feedback-review-rm/index', ['user' => $user]);
        // dd($reporteeList->toArray());
    }

    public function getFeedbackListForUser($user_id, $month_id)
    {
        $user = Auth::user();
        $rmRole = Role::where('code','reporting-manager')->first();
        $reviews = FeedbackUser::where('user_id',$user_id)->where('feedback_month_id',$month_id)->where('reviewer_type',$rmRole->id)
                            ->where('reviewer_id',$user->id)->get();
        
        //return View('pages/admin/feedback-review-rm/detail', ['reviews' => $reviews]);
        return View('pages/admin/feedback/review/index', ['reviews' => $reviews]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        return View('pages/admin/question/add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'question' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect::back()->withErrors($validator->errors())->withInput($request->input());
        }
        $questionObj = new FeedbackQuestionTemplate();
        $questionObj->question = $request->question;
        $questionObj->status = !empty($request->status) ? $request->status : false;
    
        if ( $questionObj->save() )
        {
            return redirect('/admin/question/'.$questionObj->id)->with('message', 'Successfully added question');
        }
        else
        {
            return Redirect::back()->withInput()->withErrors($questionObj->getErrors());
        }

    }
    /**
     * Show details of the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $questionObj = FeedbackQuestionTemplate::find($id);
        if ( $questionObj ) {
            return View('pages/admin/question/show', ['questionObj' => $questionObj] );
        } else {
            return Redirect::back()->withInput()->withErrors('No record found');
        }
        
        
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $questionObj = FeedbackQuestionTemplate::find($id);
        $designations = Designation::all();
        $roles = Role::all();
        if($questionObj){

            return View('pages/admin/question/edit', ['questionObj' => $questionObj, 'designations' => $designations,'roles' => $roles]);
        }
        else {
            return Redirect::back()->withInput()->withErrors('No record found');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateRating(Request $request)
    {
        $input = $request->all();
        foreach( $input['status'] as $index => $row )
        {
            dump($input['rating'][$index]);
            $feedbackUserObj = FeedbackUser::find($index);
            if( $feedbackUserObj )
            {
                $feedbackUserObj->rating = !empty($input['rating'][$index]) ? $input['rating'][$index] : '';
                $feedbackUserObj->status = !empty($input['status'][$index]) ? $input['status'][$index] : $feedbackUserObj->status;
                $feedbackUserObj->save();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $questionObj = FeedbackQuestionTemplate::find($id);
        if ( $questionObj->delete() )
        {
            return redirect('/admin/question')->with('message','Successfully Deleted');
        }
        else
        {
            return redirect::back()->withErrors('Error while deleting the record');
        }
    }
    
}
