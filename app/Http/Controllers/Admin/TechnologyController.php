<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Admin\Technology;
use App\Models\Admin\TechnologyCategory;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Redirect;

class TechnologyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $technologies = Technology::all();
        return View('pages/admin/technology/index', ['technologies' => $technologies]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = TechnologyCategory::get()->map(function ($item){
            return ['id'=>$item->id, 'name'=>$item->name];
        });
        return View('pages/admin/technology/add', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $technology = new Technology();
        $technology->name = $request->name;
        $technology->technology_category_id = $request->category;
        if($technology->save()){
            return redirect('/admin/technology')->with('message', 'Successfully Added');
        }
        else{
            return redirect::back()->withErrors($technology->getErrors())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $technology = Technology::find($id);
        if($technology){
            return View('pages/admin/technology/show', ['technology' => $technology]);
        }
        else{
            abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $technology = Technology::find($id);
        $categories = TechnologyCategory::get()->map(function ($item){
            return ['id'=>$item->id, 'name'=>$item->name];
        });
        if($technology) {
            return View('pages/admin/technology/edit', compact('technology', 'categories'));
        }
        else {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $technology = Technology::find($id);
        if($technology) {
            $technology->name = $request->name;
            $technology->technology_category_id = $request->category;
            if($technology->save()){
                return redirect('/admin/technology')->with('message', 'Successfully Updated');
            }
            else{
                return redirect::back()->withErrors($technology->getErrors())->withInput();
                }

        }
        else {
            abort(404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $technology = Technology::find($id);
        if($technology) {
            if($technology->delete()){
                return redirect('/admin/technology')->with('message', 'Successfully Deleted');
            }
            else{
                return redirect::back()->withErrors($technology->getErrors())->withInput();
                }
        }
        else {
            abort(404);
        }
    }
}
