<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\UserProfileVisibility;
use App\Models\User;

class ProfileBuilderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($userId)
    {
        $user = User::find($userId);
        if (!$user) {
            return redirect('/admin/profile-builder');
        }

        $userVisibility = UserProfileVisibility::where('user_id', $userId)->first();
        // if ($userVisibility) {
        //     return View('pages/admin/profile-builder/index', compact('userVisibility'));
        // }
        return View('pages/admin/profile-builder/index', compact('userVisibility', 'user'));

    }

}
