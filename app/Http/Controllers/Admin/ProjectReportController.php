<?php

namespace App\Http\Controllers\Admin;



use App\Models\Admin\Company;
use App\Models\Admin\Project;
use App\Models\Admin\ProjectNotes;
use App\Models\Admin\ProjectFiles;
use App\Models\Admin\Timelog;
use App\Models\Admin\ProjectSprints;
use App\Models\Admin\SprintResource;
use App\Models\Admin\Technology;
use App\Models\Admin\CmsTechnology;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Redirect;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use Config;
use Exception;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\ProjectReportService;

class ProjectReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::all();
        return View('pages/admin/project-report/index',['projects' => $projects ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        echo "create";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        echo "store";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        echo "asdfasdf";
        echo $id;
        //echo $request->projectId;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        echo "edit";
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        echo "update";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function showProjectDetails($id)
    {

        $projectId = $id;
        $projectinfo = Project::find($id);
        $companies = $projectinfo->companies;
        $contacts = $companies->contacts->where('primary', 1);
        $technologies = Technology::all();
        $projectTechnologies = $projectinfo->technologies;
        
        $x = ProjectSprints::where('project_id',$id)->select('id')->get();       
               // print_r($projectId->toArray());
            $data = [];
           foreach ($x as  $sprintId) {
               # code...

          
               $projectSprints = DB::table('timelogs')
                   ->join('users', 'timelogs.user_id', '=', 'users.id')
                   ->leftjoin('projects', 'timelogs.project_id', '=', 'projects.id')
                   ->leftJoin('project_sprints', function($join)
                               {
                                   $join->on('project_sprints.start_date','<=','timelogs.current_date');
                                   $join->on('project_sprints.end_date','>=','timelogs.current_date');
                               })
                   ->where('timelogs.project_id',$projectId)
                   ->where('project_sprints.id', $sprintId->id)
                   ->select('users.*', 'timelogs.*','projects.project_name', 'project_sprints.id as sprint_id')
                   ->get();
               $data[$sprintId->id] = $projectSprints;
            }


            $unknownTimelogs = DB::table('timelogs')
                       ->join('users', 'timelogs.user_id', '=', 'users.id')
                       ->leftjoin('projects', 'timelogs.project_id', '=', 'projects.id')
                       ->leftJoin('project_sprints', function($join)
                                   {
                                        $join->on('project_sprints.start_date','<=','timelogs.current_date');
                                        $join->on('project_sprints.end_date','>=','timelogs.current_date');
                                   })
                       ->where('timelogs.project_id',$id)
                       ->whereNull('project_sprints.end_date')
                       ->select('users.*','timelogs.id as timelog_id', 'timelogs.*', 'projects.project_name as Project Name','project_sprints.*')

                       ->get();


            // echo "<pre>";
            // print_r($data);
            // die;

               // $data = isset($data)?$data:"";
           
        return View('pages/admin/project-report/index',['data' => $data, 'projectinfo' => $projectinfo, 'companies' => $companies, 'contacts' => $contacts, 'technologies' => $technologies, 'projectTechnologies' => $projectTechnologies, 'unknownTimelogs' => $unknownTimelogs ]);

        // print_r($request);
        // rprint_r($request->all());
    }

    public function download($id)
    {


        $download = ProjectReportService::exportCsv($id);
        
    }
}
