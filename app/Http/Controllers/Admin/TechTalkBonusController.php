<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\TechTalkBonus;
use App\Models\Admin\TechTalkBonusesUser;
use App\Models\User;
use App\Services\BonusService;
use Auth;
use Illuminate\Http\Request;
use App\Models\Admin\Bonus;
use App\Models\Month;

// use Illuminate\Database\Eloquent\SoftDeletes;

class TechTalkBonusController extends Controller
{
 public function index()
 {
  $techTalkBonus = TechTalkBonus::orderBy('date', 'DESC')->get();
  return view('pages.admin.tech-talk-bonus.index', compact('techTalkBonus'));

 }
 public function create()
 {
  $user = Auth::user();
  if ($user->hasRole('admin')) {
   $userList = User::Where('is_active', 1)->orderBy('employee_id', 'asc')->get();
  } else {
   $userList = User::where('parent_id', $user->id)->where('is_active', 1)->orderBy('employee_id', 'asc')->get();
  }
  return view('pages.admin.tech-talk-bonus.add', compact('userList'));
 }
 public function store(Request $request)
 {
  $user = Auth::user();
  $data = $request->all();

  if (!empty($data)) {
   $req['user_id']        = count($data['user_id']) > 0 ? $data['user_id'] : [];
   $req['topic']          = !empty($data['topic']) ? $data['topic'] : '';
   $req['tech_talk_date'] = !empty(date_to_yyyymmdd($data['date'])) ? date_to_yyyymmdd($data['date']) : null;
   $req['status']         = 'pending';
   $req['notes']          = !empty($data['notes']) ? $data['notes'] : '';
   $req['created_by']     = $user->id;

   $response = TechTalkBonusesUser::saveTechTalkData($req);

   if (!$response) {
    return redirect()->back()->withErrors('Something went wrong!')->withInput();
   } else {
    return redirect('/admin/bonus/tech-talk')->with('message', 'Saved!');
   }
  } else {
   return redirect()->back()->withErrors('Empty data sent!')->withInput();

  }
 }

 public function update(Request $request, $id)
 {

  $user = Auth::user();
  $data = $request->all();

  if (!empty($data)) {

   $techTalkBonus = TechTalkBonus::find($id);
//    $techTalkBonus->employee_id = $data['user_id'];
   $techTalkBonus->topic = $data['topic'];
//    $techTalkBonus->amount      = $data['amount'];
   $techTalkBonus->date       = !empty(date_to_yyyymmdd($data['date'])) ? date_to_yyyymmdd($data['date']) : null;
   $techTalkBonus->notes      = !empty($data['note']) ? $data['note'] : null;
   $techTalkBonus->created_by = $user->id;

   $techTalkBonusUser = TechTalkBonusesUser::where('tech_talk_id', $id);
   if ($techTalkBonusUser) {
    $techTalkBonusUser->delete();

   }

   $user_id = $data['user_id'];
   foreach ($user_id as $tech_user_id) {

    $techUser               = new TechTalkBonusesUser();
    $techUser->tech_talk_id = $id;
    $techUser->user_id      = $tech_user_id;

    if (!$techUser->save()) {
     return redirect('/admin/bonus/tech-talk')->with('message', 'Update Failed!');

    }
   }
   if ($techTalkBonus->save()) {
    return redirect('/admin/bonus/tech-talk')->with('message', 'Updated!');
   } else {
    return redirect()->back()->withErrors('Something went wrong!')->withInput();
   }
  } else {
   return redirect()->back()->withErrors('Empty data sent!')->withInput();
  }

 }
 public function destroy($id)
 {
  $techTalkBonus = TechTalkBonus::where('id', $id)->delete();
  if ($techTalkBonus) {
   return redirect('/admin/bonus/tech-talk')->with('message', 'Successfully deleted!');

  }
  return redirect()->back()->withErrors('Uable to delete record');
 }
 public function edit($id)
 {
  $user = Auth::user();
  if ($user->hasRole('admin')) {
   $userList = User::Where('is_active', 1)->orderBy('name', 'asc')->get();
  } else {
   $userList = User::where('parent_id', $user->id)->where('is_active', 1)->orderBy('name', 'asc')->get();
  }
  $techTalkBonus = TechTalkBonus::find($id);
  $techUserObj   = TechTalkBonusesUser::where('tech_talk_id', $id)->get();

  return view('pages.admin.tech-talk-bonus.edit', compact('techTalkBonus', 'userList', 'techUserObj'));
 }
 public function show($id)
 {
  //
 }
 public function review($id)
 {
  $techTalkBonus = TechTalkBonus::with('techtalkuser.user', 'user')->find($id);

  return view('pages.admin.tech-talk-bonus.review', compact('techTalkBonus'));

 }
 public static function saveUserReview(Request $request, $id)
 {
  $data = $request->all();
  $user = Auth::user();

  $techBonus = TechTalkBonus::find($id);
  foreach ($data as $index => $techData) {
   $techObj = TechTalkBonusesUser::where('user_id', $index)->first();
   
   if ($techObj) {
    $techObj->rating   = $techData['rating'];
    $techObj->amount   = $techData['amount'];
    $techObj->comments = $techData['comment'];
   if (!$techObj->save()) {
    return redirect()->back()->with('message', 'Review Update Failed');
   }
    }
  }
  if ($techBonus) {
    $techBonus->amount = $techBonus->techtalkuser->sum('amount');
    $techBonus->approved_by = $user->id;
   $techBonus->status = 'inprogress';
   $techBonus->save();
  }

  return redirect()->back()->with('message', 'Review Update Successful');

 }
 public static function updateStatus($id)
 {

  $user = Auth::user();
  $techTalk = TechTalkBonus::find($id);

  $techTalk->status = 'approved';

  $techTalkUser = TechTalkBonusesUser::where('tech_talk_id', $id)->get();

  if ($techTalkUser && count($techTalkUser) > 0) {

   foreach ($techTalkUser as $user) {

    $data['user_id'] = $user->user_id;
    $data['date']    = $techTalk->date;
    $data['type']    = 'techtalk';
    $data['status']  = 'approved';
    $bonusObj = Bonus::create(['month_id' => Month::getMonth($techTalk->date), 'user_id' => $user->user_id,'date' => $techTalk->date,'status' => 'approved','approved_by' => $techTalk->approved_by, 'type' => 'techtalk' , 'reference_id' => $user->id, 'reference_type' => 'App\Models\Admin\TechTalkBonusesUser','created_by' => $user->id,'amount' => $user->amount,'draft_amount'=> $user->amount ]);
    if(!$bonusObj->isValid())
    {
        $errors=$bonusObj->getErrors();
    }
   }

  }

  if (!$techTalk->save()) {
   return redirect()->back()->with('message', 'Review Not Completed');

  } else {
   return redirect('/admin/bonus/tech-talk');
  }
 }
}
