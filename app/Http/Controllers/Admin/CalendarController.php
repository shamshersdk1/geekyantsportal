<?php

namespace App\Http\Controllers\Admin;


use App\Models\Admin\Calendar;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Redirect;

class CalendarController extends Controller
{
    public function show() {
        // $nonWorkingDays = Calendar::all();
    	return View ('pages/admin/project/calendar');
    }
    public function events() {
        $nonWorkingDays = Calendar::all();
        return $nonWorkingDays;
    }

    public function deleteEvent(Request $request) {

        if($request->has('date') && $request->has('title')) {
            $calendar = Calendar::where('date',$request->get('date'))->where('reason',$request->get('title'))->first();
            if($calendar){
                if($calendar->delete()){
                    return 'true';
                }
                else{
                    return 'false';
                }
            }
            else {
                return 'false';
            }
        }
        return 'false';
    }

    public function save(Request $request) {
        // print_r($request->all());
        // die;
        $data = $request->all();
        if($request->type == "Weekend")
            $data['color'] = "red";
        if($request->type == "Holiday")
            $data['color'] = "green";
        if($request->type == "Others")
            $data['color'] = "orange";
    	$result = Calendar::saveData($data);
        if($result['status'])
        {
            return redirect('/admin/calendar')->with('message', 'Event Added');
        }
        else{
            return redirect::back()->withErrors($result['errors'])->withInput();
        }
    }

    public function remove($id) {
    	$calendar = Calendar::find($id);
    	if($calendar){
    		if($calendar->delete()){
    			return redirect('/admin/calendar')->with('message', 'Successfully Added');
    		}
    		else{
    			return redirect::back()->withErrors($calendar->getErrors())->withInput();
    		}
    	}
    	else {
    		abort(404);
    	}
    }
    public function update(Request $request, $id) {
    	$calendar = Calendar::find($id);
    	if($calendar) {
    		$calendar->date = $request->date;
    		$calendar->type = $request->type;
    		$calendar->reason = $request->reason;
    		if($calendar->save()){
    			return redirect('/admin/calendar')->with('message', 'Successfully Added');
    		}
    		else{
    			return redirect('/admin/calendar')->with('message', 'Successfully Added');
    		}
    	}
    	else {
    		abort(404);
    	}

    }
}
