<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Admin\Company;
use App\Models\Admin\Project;
use App\Models\Admin\ProjectQuotation;
use App\Models\Admin\ProjectQuotationMilestone;
use App\Models\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Redirect;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use Config;
use Exception;
use DB;
use Auth;

class ProjectQuotationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projectQuotations = ProjectQuotation::with('client')->orderBy('updated_at','desc')->get();
       
        return View('pages/admin/project-quotation/index', ['projectQuotations' => $projectQuotations]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try{
            $client = new Client(['base_uri' => config('app.slack_url')]);
            $response = $client->request("POST", "channels.list", [
                'form_params' => [
                    'token' => config('app.slack_token')
                ]
                ]);
            $privateResponse = $client->request("POST", "groups.list", [
                'form_params' => [
                    'token' => config('app.slack_token')
                ]
                ]);
        } catch(Exception $e) {
            echo $e->getMessage();
        }
        $channelCreateResponse =  $response->getBody();
        $publicChannels = json_decode($channelCreateResponse);
        $channelPrivateResponse =  $privateResponse->getBody();
        $privateChannels = json_decode($channelPrivateResponse);
        $users = User::all();
        isset($users)?$users:"";
        $companies = Company::all();
        return View('pages/admin/project-quotation/add', ['companies' => $companies, 'users' => $users, 'publicChannels' => $publicChannels->channels, 'privateChannels' => $privateChannels->groups]);
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $projectQuotation = new ProjectQuotation();
        $projectQuotation->client_id = $request->company_id;
        $projectQuotation->name = $request->project_quotation;
        $projectQuotation->description = $request->description;
        if($projectQuotation->save()){
            return redirect('/admin/project-quotation')->with('message', 'Successfully Added');
        }
        else {
            return redirect::back()->withErrors($projectQuotation->getErrors())->withInput();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(isset($id)){
            $quotationObj = ProjectQuotation::with('projectQuotationMilestones','client')->find($id);
            $client = $quotationObj->client;
            $projectQuotationMilestones = ProjectQuotationMilestone::with('sprints')->where('project_quotation_id',$id)->orderBy('updated_at','desc')->get();
            if($quotationObj)
            {
                $projects = Project::all();
                return View('/pages/admin/project-quotation/view',['quotationinfo' => $quotationObj,'companies' => $client,'milestones' => $projectQuotationMilestones, 'projects' => $projects]);
            }
        }
        
    }

    public function addMilestone($quotationId)
    {
        $quotationObj = ProjectQuotation::with('projectQuotationMilestones','client')->find($quotationId);
        $client = $quotationObj->client;
        $projectQuotationMilestones = ProjectQuotationMilestone::where('project_quotation_id',$quotationId)->orderBy('updated_at','desc')->get();
        if($quotationObj)
        {
            return View('/pages/admin/project-quotation/add-milestone',['quotationId' => $quotationId, 'quotationinfo' => $quotationObj,'companies' => $client,'milestones' => $projectQuotationMilestones,]);
        }
        

    }

    public function updateMilestone(Request $request,$milestoneId)
    {
        // print_r($request->all());
        // echo $milestoneId;
        // die;
        if(isset($milestoneId)){
            $milestone = ProjectQuotationMilestone::find($milestoneId);
            if($milestone){
                $milestone->title = $request->title;
                $milestone->details = $request->details;
                $milestone->delivery_date = $request->date;
                $milestone->cost = $request->cost;
                if($milestone->save())
                    return redirect('admin/project-quotation/'.$milestone->project_quotation_id)->with('message','Milestone Updated');
                else
                    return redirect('admin/project-quotation/'.$milestone->project_quotation_id)->withErrors('unable to update');
            }
            else
                return redirect('/admin/project-quotation/')->withErrors('MilestoneId not found');
        }
        else
            return redirect('/admin/project-quotation/')->withErrors('MilestoneId not found');

    }
    public function editMilestone($quotationId, $milestoneId)
    {
        $quotationObj = ProjectQuotation::with('projectQuotationMilestones','client')->find($quotationId);
        $client = $quotationObj->client;
        $projectQuotationMilestones = ProjectQuotationMilestone::where('project_quotation_id',$quotationId)->orderBy('updated_at','desc')->get();
        if($quotationObj)
        {
            if(isset($milestoneId)){
                $projectQuotationMilestones = ProjectQuotationMilestone::find($milestoneId);
                if($projectQuotationMilestones)
                    return View('/pages/admin/project-quotation/edit-milestone',['projectQuotationMilestones' => $projectQuotationMilestones, 'quotationinfo' => $quotationObj,'companies' => $client,'milestones' => $projectQuotationMilestones,]);
                else
                    return redirect('/admin/project-quotation/')->withErrors('MilestoneId not found');
            }
            else
                 return redirect('/admin/project-quotation/')->withErrors('MilestoneId not found');
        }
        else {
            abort(404);
        }
        

    }
    public function deleteMilestone($milestoneId)
    {
        if(isset($milestoneId)){
            $milestone = ProjectQuotationMilestone::with('sprints')->find($milestoneId);
            $projectQuotationId = $milestone->project_quotation_id;
            if($milestone){
                if($milestone->sprints) {
                    $milestone->sprints()->detach();
                }
                if($milestone->delete())
                    return redirect('/admin/project-quotation/'.$projectQuotationId)->with('message', 'Successfully deleted');
                else
                    return redirect('/admin/project-quotation/'.$projectQuotationId)->withErrors('unable to delete');
            }
            else
                return redirect('/admin/project-quotation/'.$projectQuotationId)->withErrors('Milestone not found');
        }        
        return redirect('/admin/project-quotation/')->withErrors('Milestone Id not found');

    }

    public function storeMilestone(Request $request)
    {
        // return $request->all();
        if(isset($request->quotationId)) {    

            
            
            $addMilestone = new ProjectQuotationMilestone();
            $addMilestone->project_quotation_id = $request->quotationId;
            $addMilestone->title = $request->title;
            $addMilestone->details = $request->details;
            $addMilestone->delivery_date = $request->date;
            $addMilestone->cost = $request->cost;

            if($addMilestone->save()){

                    return redirect('admin/project-quotation/'.$request->quotationId)->with('message','Milestone has been added');
            }
            else{
                return redirect::back()->withErrors($addMilestone->getErrors())->withInput();
            }
        } else{
            return redirect::back()->withErrors(['not found'])->withInput();
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project = Project::find($id);

        if($project){
            try{
                $client = new Client(['base_uri' => config('app.slack_url')]);
                $response = $client->request("POST", "channels.list", [
                    'form_params' => [
                        'token' => config('app.slack_token')
                    ]
                    ]);
                $privateResponse = $client->request("POST", "groups.list", [
                    'form_params' => [
                        'token' => config('app.slack_token')
                    ]
                    ]);
            } catch(Exception $e) {
                echo $e->getMessage();
            }
            $channelCreateResponse =  $response->getBody();
            $publicChannels = json_decode($channelCreateResponse);
            $channelPrivateResponse =  $privateResponse->getBody();
            $privateChannels = json_decode($channelPrivateResponse);
            $users = User::all();
            isset($users)?$users:"";
            $companies = Company::all();
            return View('pages/admin/project/edit', ['companies' => $companies, 'users' => $users, 'project' => $project, 'publicChannels' => $publicChannels->channels, 'privateChannels' => $privateChannels->groups]);
        }
        else {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $project = Project::find($id);
        if($project) {
            $project->project_name = $request->project_name;
            $project->account_manager = $request->account_manager;
            $project->project_manager = $request->project_manager;
            $project->bd_manager = $request->bd_manager;
            $project->channel_for_developer = $request->channel_for_developer;
            $project->channel_for_manager = $request->channel_for_manager;
            $project->start_date = date("Y-m-d", strtotime($request->start_date));
            $project->end_date = date("Y-m-d", strtotime($request->end_date));
            $project->company_id = $request->company_id;
            $project->status = $request->status;
            $project->notes = $request->notes;
            if($project->save()){ 
                return redirect('/admin/project')->with('message','Successfully Updated');
            }
            else{
                return redirect::back()->withErrors($project->getErrors());
            }
        }
        else {
            abort(404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project = Project::find($id);
        if($project) {
            // if(count($project->projectSprints) > 0){
            //     $project->projectSprints->delete();
            // }
            if(count($project->projectNotes) > 0){
                $notes = $project->projectNotes;
                foreach($notes as $note){
                    $note->delete();
                }
            }
            if(count($project->projectSprints) > 0){
                $sprints = $project->projectSprints;
                foreach($sprints as $sprint){
                    $sprint->delete();
                }
            }
            if(count($project->projectFiles) > 0){
                $files = $project->projectFiles;
                foreach($files as $file){
                    $file->delete();
                }
            }
            if(count($project->Timelog) > 0){
                $timelogs = $project->Timelog;
                foreach($timelogs as $timelog){
                    $timelog->delete();
                }
            }
            if(count($project->technologies) > 0){
                $project->technologies()->detach();
            }
            if($project->delete()){
                return redirect('/admin/project')->with('message','Successfully Deleted');
            }
            elseif(!$project->delete()){
                return redirect::back()->withErrors($project->getErrors());
            }
        }
        else {
            abort(404);
        }
    }

    public function getSprints($projectId) {
        $project = Project::find($projectId);
        $projectSprints = $project->projectSprints;
        return $projectSprints;
    }

    public function mapSprints(Request $request) {
        // return array_unique($request->sprints);
        // return $request->all();

        $milestone = ProjectQuotationMilestone::with('sprints')->find($request->milestone_id);
        if($milestone) {
            if($request->sprints) {
                
                $request->sprints = array_unique($request->sprints);
                if($milestone->sprints) {
                    $milestone->sprints()->detach();
                }

                foreach($request->sprints as $sprint) {
                    $milestone->sprints()->attach($sprint, ['project_id' => $request->project_id]);
                }
                return redirect::back()->with('message','Sprints Maped Successfully'); 
            }
            else {
                return redirect::back()->withErrors(['No Sprint Selected']);
            }
        }
        else {
            return redirect::back()->withErrors(['Milestone not found']);
        }
    }

    public function showMilestone($quotationId, $milestoneId) {
        $milestone = ProjectQuotationMilestone::with('sprints')->find($milestoneId);
        if($milestone) {
            $quotationObj = ProjectQuotation::with('projectQuotationMilestones','client')->find($quotationId);
            $client = $quotationObj->client;
            $projectQuotationMilestones = ProjectQuotationMilestone::where('project_quotation_id',$quotationId)->orderBy('updated_at','desc')->get();
            return View('pages/admin/project-quotation/show-milestone', ['milestone' => $milestone, 'quotationinfo' => $quotationObj,'companies' => $client, 'milestones' => $projectQuotationMilestones]);
        }   
        else {
            abort(404);
        }
    }

    public function getMilestoneSprints($id) {
        $milestone = ProjectQuotationMilestone::find($id);
        if($milestone) {
            $sprints = $milestone->sprints;
            return $sprints;
        }
        else {
            abort(404);
        }
    }
}
