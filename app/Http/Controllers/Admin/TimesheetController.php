<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Admin\Company;
use App\Models\Timesheet;
use App\Models\TimesheetDetail;
use App\Models\Admin\ProjectNotes;
use App\Models\Admin\ProjectResource;
use App\Models\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\ProjectService;
use App\Services\ResourceService;
use App\Jobs\Slack\SlackTimesheetNotification;
use Redirect;
use Config;
use Exception;
use DB;
use Auth;


class TimesheetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $current_date = date('Y-m-d');
        $userList = User::where('parent_id',$user->id)
                          ->get()->toArray();
        $userDataList = [];
        $projects = '';
        $hours_details = [];
        
        foreach ($userList as $u) {
            $projects = '';
            
            $currentProjectsEndDated = ProjectResource::with('project')
                                ->where('user_id',$u['id'])
                                ->where('end_date','>=',$current_date);

            $currentProjectsWithNull = ProjectResource::with('project')
                                ->where('user_id',$u['id'])
                                ->whereNull('end_date');
            
            $currentProjects = $currentProjectsEndDated->union($currentProjectsWithNull)->get();                                                               
            
            if ( count($currentProjects) > 0 ) {
                
                foreach ( $currentProjects as $currentProject ){
                    $projectsArray[] = $currentProject->project->project_name;
                }
               $projects = implode($projectsArray,' | ');
               $projectsArray = [];
               
            }
            $hours_details = ProjectService::currentLastWeekHours($u['id'], $current_date);
            

            $userData['user_id'] = $u['id'];
            $userData['user_name'] = $u['name'];
            $userData['projects'] = $projects;
            $userData['hours_details'] = $hours_details;
            array_push($userDataList,$userData);
            
        }
        return View('pages/admin/timesheets/index', ['userDataList' => $userDataList, 'user' => $user]);
    }
//     /**
//      * Show the form for creating a new resource.
//      *
//      * @return \Illuminate\Http\Response
//      */
    public function show($id, Request $request)
    {
        
        $user = Auth::user();
        
        $parent_id = User::where('id',$id)->first();
        $user_name = $parent_id['name'];
        
        $start_date = '2017-12-01';
        $end_date = '2017-12-24';

        if(empty($request->start_date)) {
            $start_date = date('Y-m-d');
            $day = date('N')-1;
            $start_date = date('Y-m-d', strtotime($start_date.'-'.$day.' days'));
            $end_date = date('Y-m-d', strtotime($start_date.'+6 days'));
        } else {
            $start_date = date('Y-m-d', strtotime($request->start_date));
            $end_date = date('Y-m-d', strtotime($request->end_date));
            
        }

        $data = ProjectService::projectsArray($id, $start_date, $end_date);
        $dates = $data['dates'];
        $projects = $data['projects'];
        $projects = json_encode($projects);
        $data[] = !empty($data[0])?$data[0]:[];
        $max= date_diff(date_create($start_date), date_create($end_date))->format('%a');
        $date = $start_date;
        $verticalTotals = [];
        $verticalApproveTotals = [];
        for($i=0;$i<=$max;$i++)
        {
            $t = 0;    
            $approved_totals = 0;
            $timesheets = Timesheet::where('user_id', $id)
                          ->where('date',$date)->get();
            foreach( $timesheets as $timesheet ) {
                if ( $timesheet->approver_id != null ){
                    $approved_totals = $approved_totals + $timesheet->approved_hours;
                }
                $timesheet_details = TimesheetDetail::where('timesheet_id',$timesheet->id)->get();
                foreach($timesheet_details as $timesheet_detail){
                    $t = $t + $timesheet_detail->duration;
                }
            }       
            $subTotal = [];
            $subTotal['total'] = $t;
            $subTotal['approved_total'] = $approved_totals;           
            $verticalTotals[] = $subTotal;
            // array_push($verticalTotals, $t);
            // array_push($verticalApproveTotals, $approved_totals);
            $date = date('Y-m-d', strtotime($date . ' +1 day'));
        }
        
        return View('pages/admin/timesheets/show', compact('user','dates','projects','verticalTotals','id','user_name'));
        
    }
    public function approval($id)
    {
        $user = Auth::user();
        $selected_date = date('Y-m-d',strtotime("-1 days"));
        // echo "<pre>";
        // print_r($date);
        // die;            
        
        $timesheets = Timesheet::with('user','project','approver') 
                        ->where('user_id',$id)
                        ->where('date','=',$selected_date)
                        ->get();
        // echo "<pre>";
        // print_r($timesheets->toArray());
        // die;                        
        return View('pages/admin/timesheets/show', ['user' => $user,'timesheets' => $timesheets, 'selected_date' => $selected_date ]);
    }

    public function showAll()
    {
        $user = Auth::user();
        $current_date = date('Y-m-d');
        $userList = User::where('is_active',1)
                          ->where('is_billable',1)
                          ->orderBy('employee_id')
                          ->get()->toArray();
        $userDataList = [];
        $projects = '';
        $hours_details = [];
        
        foreach ($userList as $u) {
            $projects = '';
            
            $currentProjectsEndDated = ProjectResource::with('project')
                                ->where('user_id',$u['id'])
                                ->where('end_date','>=',$current_date);

            $currentProjectsWithNull = ProjectResource::with('project')
                                ->where('user_id',$u['id'])
                                ->whereNull('end_date');
            
            $currentProjects = $currentProjectsEndDated->union($currentProjectsWithNull)->get();                                                               
            
            if ( count($currentProjects) > 0 ) {
                
                foreach ( $currentProjects as $currentProject ){
                    $projectsArray[] = $currentProject->project->project_name;
                }
               $projects = implode($projectsArray,' | ');
               $projectsArray = [];
               
            }
            $hours_details = ProjectService::currentLastWeekHours($u['id'], $current_date);
            

            $userData['user_id'] = $u['id'];
            $userData['user_name'] = $u['name'];
            $userData['projects'] = $projects;
            $userData['hours_details'] = $hours_details;
            array_push($userDataList,$userData);
            
        }
        return View('pages/admin/timesheets/index', ['userDataList' => $userDataList, 'user' => $user]);
    }

    public function sendSlackNotification($id)
    {
        $user = User::find($id);
        if($user) {
            $day = date('N');
            $start_date = date('Y-m-d', strtotime('-'.($day-1).' days', time()));
            if($day > 5) {
                $end_date = date('Y-m-d', strtotime('-'.($day-5).' days', time()));
            } else {
                $end_date = date('Y-m-d', strtotime('-1 days', time()));
            }
            $data = ['id' => $id, 'start_date' => $start_date, 'end_date' => $end_date];
            $job = (new SlackTimesheetNotification($data))->onQueue('timesheet-slack-notification');
            dispatch($job);
            return Redirect::back()->with('message', 'Notification sent');
        } else {
            return Redirect::back()->withErrors(['Invalid link followed']);
        }
    }
}