<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\SshKey;
use App\Models\User;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Redirect;
use Config;
use Exception;
use DB;
use Auth;
use Validator;

class SshKeyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sshKeys = SshKey::orderBy('user_id','DESC')->get();
        return View('pages/admin/ssh-key/index', ['sshKeys' => $sshKeys ] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::where('is_active',1)->get();
        return View('pages/admin/ssh-key/add', ['users' => $users]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'ssh_key' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect::back()->withErrors($validator->errors())->withInput($request->input());
        }
        $input = $request->all();        

        $response = SshKey::saveData($input);
    
        if ( $response['status'] )
        {
            return redirect('/admin/ssh-keys/'.$response['message'])->with('message', 'Successfully added ssh key');
        }
        else
        {
            return Redirect::back()->withInput()->withErrors($response['message']);
        }

    }
    /**
     * Show details of the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sshKeyObj = SshKey::find($id);
        if( !empty($sshKeyObj) ) {
            return View('pages/admin/ssh-key/show', ['sshKeyObj' => $sshKeyObj] );
        }
        else {
            return Redirect::back()->withInput()->withErrors('SSH Key details not found');
        }
        
        
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sshKeyObj = SshKey::find($id);

        if($sshKeyObj){
            return View('pages/admin/ssh-key/edit', ['sshKeyObj' => $sshKeyObj]);
        }
        else {
            return Redirect::back()->withInput()->withErrors('SSH Key details not found');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'ssh_key' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect::back()->withErrors($validator->errors())->withInput($request->input());
        }
        $input = $request->all();
        
        $response = SshKey::updateData($id, $input);
        if ( $response['status'] )
        {
            return redirect('/admin/ssh-keys/'.$response['message'])->with('message', 'Successfully updated ssh key');
        }
        else
        {
            return Redirect::back()->withInput()->withErrors($response['message']);
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $obj = SshKey::find($id);
        if(!$obj) {
            return Redirect::back()->withInput()->withErrors('Invalid Ssh Key #'.$id);
        }
        if($obj->delete())
            return redirect('/admin/ssh-keys')->with('message', 'Record deleted successfully');
    }

    public function showMyRequest()
    {
        $user = Auth::user();
        $sshKeyObj = SshKey::where('user_id',$user->id)->first();
        if ( empty($sshKeyObj) )
        {
            return redirect('/admin/');
        }
        return View('pages/admin/ssh-key/my-show', ['sshKeyObj' => $sshKeyObj]);
    }

    public function getMyRequest()
    {
        $user = Auth::user();
        $sshKeyObj = SshKey::where('user_id',$user->id)->first();
        if ( empty($sshKeyObj) )
        {
            return redirect('/admin/ssh-keys');
        }
        return View('pages/admin/ssh-key/my-edit', ['sshKeyObj' => $sshKeyObj]);
    }
    public function updateMyRequest($id,Request $request)
    {
        $validator = Validator::make($request->all(), [
            'ssh_key' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect::back()->withErrors($validator->errors())->withInput($request->input());
        }
        $input = $request->all();
        $input['is_verified'] = 'true';
        $response = SshKey::updateData($id, $input);
        if ( $response['status'] )
        {
            return redirect('/admin/ssh-keys/my-ssh-key/view')->with('message', 'Successfully updated ssh key');
        }
        else
        {
            return Redirect::back()->withInput()->withErrors($response['message']);
        }
    }
    public function createMyRequest()
    {
        $user = Auth::user();
        $sshKeyObj = SshKey::where('user_id',$user->id)->first();
        if ( empty($sshKeyObj) )
        {
            return View('pages/admin/ssh-key/my-add');
        }
        else {
            return View('pages/admin/ssh-key/my-edit', ['sshKeyObj' => $sshKeyObj]);
        }
        
    }
    
    public function addMyRequest(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'ssh_key' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect::back()->withErrors($validator->errors())->withInput($request->input());
        }
        $input = $request->all();
        
        $response = SshKey::saveMyData($input);
        if ( $response['status'] )
        {
            return redirect('/admin/ssh-keys/my-ssh-key/view')->with('message', 'Successfully added ssh key');
        }
        else
        {
            return Redirect::back()->withInput()->withErrors($response['message']);
        }
    }
}

