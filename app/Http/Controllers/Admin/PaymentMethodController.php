<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Admin\PaymentMethod;
use App\Models\User;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Redirect;
use Config;
use Exception;
use DB;
use Auth;
use Validator;

class PaymentMethodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paymentMethods = PaymentMethod::paginate(1000);
        return View('pages/admin/payment-method/index', ['paymentMethods' => $paymentMethods] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::where('is_active',1)->get();
        return View('pages/admin/payment-method/add',['users' => $users]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'  => 'required'
        ]);
        if ($validator->fails()) {
            return redirect::back()->withErrors($validator->errors())->withInput($request->input());
        }
        $paymentMethodObj = new PaymentMethod();
        $paymentMethodObj->name = $request->name;
        $paymentMethodObj->account_number = !empty($request->account_number) ? $request->account_number : '' ;
        $paymentMethodObj->description = !empty($request->description) ? $request->description : '' ;
    
        if ( $paymentMethodObj->save() )
        {
            return redirect('/admin/payment-method')->with('message', 'Successfully added');
        }
        else
        {
            return Redirect::back()->withInput()->withErrors($paymentMethodObj->getErrors());
        }

    }
    /**
     * Show details of the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $paymentMethodObj = PaymentMethod::find($id);
        if ( $paymentMethodObj ) {
            return View('pages/admin/payment-method/show', ['paymentMethodObj' => $paymentMethodObj] );
        } else {
            return Redirect::back()->withInput()->withErrors('No record found');
        }
        
        
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $paymentMethodObj = PaymentMethod::find($id);
        if($paymentMethodObj){

            return View('pages/admin/payment-method/edit', ['paymentMethodObj' => $paymentMethodObj]);
        }
        else {
            return Redirect::back()->withInput()->withErrors('No record found');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name'  => 'required'
        ]);
        if ($validator->fails()) {
            return redirect::back()->withErrors($validator->errors())->withInput($request->input());
        }
        $paymentMethodObj = PaymentMethod::find($id);
        if ( $paymentMethodObj ) {
            $paymentMethodObj->name = $request->name;
            $paymentMethodObj->account_number = !empty($request->account_number) ? $request->account_number : '' ;
            $paymentMethodObj->description = !empty($request->description) ? $request->description : '' ;
            if ( $paymentMethodObj->save() )
            {
                return redirect('/admin/payment-method/'.$paymentMethodObj->id)->with('message', 'Successfully updated');
            }
            else
            {
                return Redirect::back()->withInput()->withErrors($paymentMethodObj->getErrors());
            }
        } else {
            return Redirect::back()->withInput()->withErrors('No record found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $paymentMethodObj = PaymentMethod::find($id);
        if ( $paymentMethodObj->delete() )
        {
            return redirect('/admin/payment-method')->with('message','Successfully Deleted');
        }
        else
        {
            return redirect::back()->withErrors('Error while deleting the record');
        }
    }
    
}
