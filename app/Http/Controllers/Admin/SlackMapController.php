<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\User;
use App\Models\Admin\SlackUser;

use Redirect;

class SlackMapController extends Controller
{
    public function index()
    {
        $users = User::where('is_active', 1)->with('slackUsers')->get();
        $assigned = [];
        foreach($users as $user) {
            if($user->slackUser) {
                $assigned[$user->id] = $user->slackUser;
                $assigned[$user->id]->email = $user->slackUser->email;
                $user->slackUser =  $user->slackUser;
            }
        }
        $usersjson = json_encode($users);
        $slackUsers = json_encode(SlackUser::where('deleted', null)->orWhere('deleted', '0')->get());
        $assigned = json_encode($assigned);
        return view('pages.admin.slack-user-map.index', compact('users', 'usersjson', 'slackUsers', 'assigned'));
    }
    public function update(Request $request, $id)
    {
        return;
        // $user = User::find($id);
        // if(!$user) {
        //     return response()->json('User not found', 404);
        // }
        // if(empty($request->slack_id)) {
        //     $user->slackUsers()->detach();
        //     return response()->json('Changes saved', 200);
        // }
        // $slack_user = SlackUser::with('users')->where('id', $request->slack_id)->first();
        // if(empty($slack_user)) {
        //     return response()->json('Slack id not found', 404);
        // }
        // if(!empty($slack_user->user) && $slack_user->user->id != $id) {
        //     return response()->json('Selected id already linked with '.$slack_user->user->name, 400);
        // } else {
        //     $user->slackUsers()->detach();
        //     $user->slackUsers()->attach($request->slack_id);
        //     return response()->json('Changes saved', 200);
        // }
    }
}
