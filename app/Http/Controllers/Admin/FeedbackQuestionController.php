<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Admin\FeedbackQuestionTemplate;
use App\Models\Admin\Designation;
use App\Models\User;
use App\Models\Admin\Role;
use App\Models\Admin\Question;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Services\FeedbackService;

use Redirect;
use Config;
use Exception;
use DB;
use Auth;
use Validator;

class FeedbackQuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $designations = Designation::all();
        // $roles = Role::whereIn('code',['team-lead','reporting-manager'])->get();
        // $selected_designtions = [];
        // $selected_roles = [];
        // if ( !empty($request->designation) ){
        //     $selected_designtions = $request->designation;
        // }
        // if ( !empty($request->role) ){
        //     $selected_roles = $request->role;
        // }
        // $questions = FeedbackService::getQuestions($selected_designtions, $selected_roles);
        $questions = Question::paginate(500);
        
        return View('pages/admin/feedback/question/index', ['questions' => $questions, 
                                                            // 'selected_designtions' => $selected_designtions, 
                                                            // 'designations' => $designations, 
                                                            // 'roles' => $roles, 
                                                            // 'selected_roles' => $selected_roles
                                                             ] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('pages/admin/feedback/question/add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'question' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect::back()->withErrors($validator->errors())->withInput($request->input());
        }
        $questionObj = new Question();
        $questionObj->question = $request->question;
        $questionObj->status = !empty($request->status) ? $request->status : false;
    
        if ( $questionObj->save() )
        {
            return redirect('/admin/feedback/question/'.$questionObj->id.'/edit')->with('message', 'Successfully added question');
        }
        else
        {
            return Redirect::back()->withInput()->withErrors($questionObj->getErrors());
        }

    }
    /**
     * Show details of the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $questionObj = Question::find($id);
        if ( $questionObj ) {
            return View('pages/admin/feedback/question/show', ['questionObj' => $questionObj] );
        } else {
            return Redirect::back()->withInput()->withErrors('No record found');
        }
        
        
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $questionObj = Question::find($id);
        $designations = Designation::all();
        $questionDesignationArray=[];
        $questionRoleArray =[];
        if ( !empty($questionObj->mappedQuestion) ) 
        {
            $questionDesignationArray = [];
            $questionRoleArray =[];
            foreach ($questionObj->mappedQuestion as $mappedQuestion) {
                if ( $mappedQuestion->reference_type == 'App\Models\Admin\Designation' )
                    array_push($questionDesignationArray, $mappedQuestion->reference_id);
                else
                    array_push($questionRoleArray, $mappedQuestion->reference_id);
            }
        }
        $roles = Role::whereIn('code',['team-lead', 'account-manager', 'sales-manager', 'code-lead', 'delivery-lead', 'project-coordinator'])->get();
        $applicableToRole = Role::whereIn('code',['team-lead', 'account-manager', 'sales-manager', 'code-lead', 'delivery-lead', 'project-coordinator'])->get();
        
        if($questionObj) {
            return View('pages/admin/feedback/question/edit', compact('questionObj', 'designations','roles','questionDesignationArray','questionRoleArray','applicableToRole'));
        }
        else {
            return Redirect::back()->withInput()->withErrors('No record found');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'question' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect::back()->withErrors($validator->errors())->withInput($request->input());
        }
        $input = $request->all();
        $questionObj = FeedbackService::updateQuestion($id, $input);    
        if ( $questionObj['status'] ) {
            return redirect('/admin/feedback/question')->with('message', 'Successfully updated designation');
        } else {
            return Redirect::back()->withInput()->withErrors($questionObj->getErrors());
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $questionObj = Question::find($id);
        if ( $questionObj->delete() )
        {
            return redirect('/admin/feedback/question')->with('message','Successfully Deleted');
        }
        else
        {
            return redirect::back()->withErrors('Error while deleting the record');
        }
    }
    
}
