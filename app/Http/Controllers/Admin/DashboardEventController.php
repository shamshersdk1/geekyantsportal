<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\DashboardEvent;
use App\Models\User;

use Redirect;
use Log;

class DashboardEventController extends Controller
{
    public function index()
    {
        $events=DashboardEvent::get();
        $jsonuser=User::getUsers();
        return view('pages.admin.dashboard-events.index', compact('events', 'jsonuser'));
    }
    public function store(Request $request)
    {
        $response=DashboardEvent::saveData($request, 0);
        if ($response['status']) {
            return redirect::back()->with('message', $response['message']);
        } else {
            return redirect::back()->withErrors($response['error']);
        }
    }
    public function show(Request $request, $id)
    {
        if (!$request->user) {
            return response("Users only", 401);
        }

        if (! $request->user->isAdmin()) {
            return response("Admins only", 401);
        }
        $event=DashboardEvent::find($id);
        if (!$event) {
            return response()->json("Event not found", 400);
        }
        $event->name=$event->user->name;
        $event->time=date('H:i', strtotime($event->time));
        return response()->json($event);
    }
    public function update(Request $request, $id)
    {
        $response=DashboardEvent::saveData($request, $id);
        if ($response['status']) {
            return redirect::back()->with('message', $response['message']);
        } else {
            return redirect::back()->withErrors($response['error']);
        }
    }
    public function destroy($id)
    {
        $event=DashboardEvent::find($id);
        if (empty($event)) {
            return redirect::back()->withErrors('invalid link followed');
        }
        $event->delete();
        return redirect::back()->with('message', 'Successfully deleted');
    }
}
