<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Admin\CmsTechnology;
use App\Models\Admin\Timelog;
use App\Models\Admin\SlackUser;

use App\Models\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\Mail\PasswordResetMail;
use App\Services\LeaveService;
use App\Services\TimelogService;
use App\Services\ProjectService;


class EmployeeReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $report = [];
        $users = User::all();
        // echo "<pre>";
        // $start_date = '2016-09-01';

        // $end_date = '2016-09-30';
        // $start_date = \Carbon\Carbon::parse($start_date);
        // $agoDate = $start_date->subDays($start_date->dayOfWeek)->subWeek();
        // print_r($agoDate);
        // die;

        // $date = '2016-09-02';
        $start_date = \Carbon\Carbon::now();
        $start_date =  $start_date->subDays($start_date->dayOfWeek - 1)->subWeek()->format('Y-m-d'); // monday

        // print_r($start_date);
        // die;

        $end_date = \Carbon\Carbon::parse($start_date)->addDays(42);
        $end_date = $end_date->endOfWeek($end_date)->format('Y-m-d'); //sunday
        // print_r($end_date);
        // die;

        foreach ($users as $user) {
            $week = [];
            $date = $start_date;

            while($start_date <= $date && $end_date >= $date){

                $data = DB::table('project_sprints')
                    ->leftJoin('sprint_resources', 'project_sprints.id', '=', 'sprint_resources.sprint_id')

                    ->leftJoin('projects', 'project_sprints.project_id', '=', 'projects.id')
                    ->Join('users', 'sprint_resources.employee_id', '=', 'users.id')
                    ->select('sprint_resources.*','project_sprints.id','projects.project_name','users.name')
                    ->where('sprint_resources.employee_id',$user->id)
                    ->where('project_sprints.start_date',$date)
                    ->get();

               $date = strtotime($date);
               $nextdate = date("Y-m-d", strtotime("+7 day", $date));
               $date = date("Y-m-d", $date);
               $consumeminutes = Timelog::where('user_id',$user->id)->where('current_date','>=',$date)->where('current_date','<=',$nextdate)->select('minutes','id')->get();

                $temp['allocated'] = $data->toArray();
                $temp['consumed'] = $consumeminutes->toArray();

                $week[$date] = $temp;

                $date = strtotime($date);
                $date = date("Y-m-d", strtotime("+7 day", $date));

            }

            $report[$user->name] = $week;

        }

        return View('pages/admin/employee-report/index',['report' => $report, 'start_date' => $start_date]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        echo "show function";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function NewEmployeeReport()
    {
        $report = [];
        $users = User::all();

        $start_date = \Carbon\Carbon::now();
        $start_date =  $start_date->subDays($start_date->dayOfWeek - 1)->subWeek()->format('Y-m-d'); // monday
        $end_date = \Carbon\Carbon::parse($start_date)->addDays(42);
        $end_date = $end_date->endOfWeek($end_date)->format('Y-m-d'); //sunday

        foreach ($users as $user) {
            $week = [];
            $date = $start_date;
            //log will allow to get the date of consecutive week
            while($start_date <= $date && $end_date >= $date){

                $allcoatedHours = ProjectService::allocatedHours($user->id, $date);
                 $consumeHours = TimelogService::consumeHours($user->id, $date);
                 $globalLeave = LeaveService::globalLeaveCount($date);
                 $personalLeave = LeaveService::personalLeaveCount($user->id , $date);
                $temp['allocated'] = $allcoatedHours;
                $temp['consumed'] = $consumeHours;
                $temp['leave'] = $personalLeave;
                $temp['holiday'] = $globalLeave;
                $week[$date] = $temp;

                $date = strtotime($date);
                $date = date("Y-m-d", strtotime("+7 day", $date));

            }
            $report[$user->name] = $week;


        }

        return View('pages/admin/employee-report/newEmployeeReport',['report' => $report, 'start_date' => $start_date]);
    }
}
