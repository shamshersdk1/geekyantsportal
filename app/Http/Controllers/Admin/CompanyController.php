<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Admin\Client;
use App\Models\Admin\Project;
use App\Models\Admin\ProjectCategory;
use App\Models\Admin\Contact;
use App\Models\User;
use DB;
use Redirect;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Promise;
use Illuminate\Http\Request;
use App\Services\CompanyService;

class CompanyController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$companies = Client::paginate(100);
		return View('pages/admin/company/index', ['companies' => $companies]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View('pages/admin/company/add');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$data = $request->all();
		$company = new Client();
		
		if (empty($data)) {
            return $this->failResponse('Bad request', 400, ['Empty data sent']);
        }
        else{
            $res = CompanyService::saveData($data);
			if($res['status'])
			{
				if($request->redirectedFromLead)
						return redirect('/admin/project/create/'.$company->id)->with('message', 'Successfully Added');
					else
						return redirect('/admin/company')->with('message', 'Successfully Added');
			}
			else
			{
				return redirect()->back()->withErrors($res['message'])->withInput();
			}
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$company = Client::with('contacts')->find($id);
		$projects = Project::where('company_id',$company->id)->paginate(10);
		if($company){
			return View('pages/admin/company/show', ['company' => $company,'projects' => $projects] );
		}
		else{
			abort(404);
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$company = Client::with('contacts')->find($id);
		if($company){
			return View('pages/admin/company/edit', ['company' => $company]);
		}
		else {
			abort(404);
		}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		// return $request->all();
		$company = Client::find($id);
		if($company){
			$company->name = $request->name;
			$company->email = $request->email;
			$company->work_phone = $request->work_phone;
			$company->mobile = $request->mobile;
			$company->website = $request->website;
			$company->billing_add_street = $request->billing_add_street;
			$company->billing_add_city = $request->billing_add_city;
			$company->billing_add_state = $request->billing_add_state;
			$company->billing_add_zip_code = $request->billing_add_zip_code;
			$company->billing_add_country = $request->billing_add_country;
			$company->billing_add_fax = $request->billing_add_fax;
			$company->shipping_add_street = $request->shipping_add_street;
			$company->shipping_add_city = $request->shipping_add_city;
			$company->shipping_add_state = $request->shipping_add_state;
			$company->shipping_add_zip_code = $request->shipping_add_zip_code;
			$company->shipping_add_country = $request->shipping_add_country;
			$company->shipping_add_fax = $request->shipping_add_fax;

			if($company->save()) {
				if(count($company->contacts) > 0){

					$contact = $company->contacts->where('primary', 1)[0];
					if($contact){	
						$contact->first_name = $request->first_name;
						$contact->last_name = $request->last_name;					
						if($company->save() && $contact->save()){
							return redirect('/admin/company')->with('message', 'Successfully Updated');
						}
						else {
							return redirect()->back()->withErrors($company->getErrors());
						}
					}
				}
				elseif(count($company->contacts) <= 0 && $request->first_name){
					
					$contact = new Contact();
					$contact->first_name = $request->first_name;
					$contact->last_name = $request->last_name;
					$contact->primary = 1;
					$contact->company_id = $company->id;
					if($contact->save()){
						return redirect('/admin/company')->with('message', 'Successfully Updated');
					}
					else {
						return redirect()->back()->withErrors($company->getErrors());
					}
				}
				else {
					return redirect('/admin/company')->with('message', 'Successfully Updated');
				}
			}
			else {
				return redirect()->back()->withErrors($company->getErrors());
			}
		}
		else{
			abort(404);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$company = Company::find($id);

		if($company){
			// $contacts = $company->contacts;
			if(count($company->contacts) > 0){
				$contacts = Contact::where('company_id', $company->id);
				if($company->delete() && $contacts->delete()) {
					return redirect('/admin/company')->with('message', 'Successfully Deleted');
				}
				else {
					return redirect()->back()->withErrors([$company->getErrors()]);
				}
			}
			else {
				if($company->delete()){
					return redirect('/admin/company')->with('message', 'Successfully Deleted');
				}
				else {
					return redirect()->back()->withErrors($company->getErrors());
				}
			}
		}
		else{
			abort(404);
		}
		
	}

	public function createNewProject($id)
	{
		// $company = Company::find($id);
		// dd($company->toArray());

		try{
            $client = new GuzzleClient(['base_uri' => config('app.slack_url')]);
            $response = $client->request("POST", "channels.list", [
                'form_params' => [
                    'token' => config('app.slack_token')
                ]
                ]);
            $privateResponse = $client->request("POST", "groups.list", [
                'form_params' => [
                    'token' => config('app.slack_token')
                ]
                ]);
        } catch(Exception $e) {
            echo $e->getMessage();
        }
        $channelCreateResponse =  $response->getBody();
        $publicChannels = json_decode($channelCreateResponse);
        $channelPrivateResponse =  $privateResponse->getBody();
        $privateChannels = json_decode($channelPrivateResponse);

        $users = User::where('is_active',1)->get();
        isset($users)?$users:"";
        $company = Client::find($id);
        $categories = ProjectCategory::all();
        $selected_channels = [];
        return View('pages/admin/project/add', ['company' => $company, 'users' => $users, 'publicChannels' => $publicChannels->channels, 'privateChannels' => $privateChannels->groups, 'categories' => $categories, 'selected_channels' => $selected_channels]);
	}

}
