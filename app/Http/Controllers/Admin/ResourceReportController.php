<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Admin\Company;
use App\Models\Timesheet;
use App\Models\TimesheetDetail;
use App\Models\Admin\ProjectNotes;
use App\Models\Admin\ProjectResource;
use App\Models\User;
use App\Models\Admin\Project;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\ProjectService;
use App\Services\ResourceService;
use Redirect;
use Config;
use Exception;
use DB;
use Auth;


class ResourceReportController extends Controller
{
    public function index(Request $request)
    {
        $eligible_users =[];
        $start_date = date("Y-m-d", strtotime("previous monday"));
        $end_date = date('Y-m-d', strtotime($start_date . ' +13 day'));
        $selected_users = [];
        $display_start_date = '';
        $display_end_date = '';
        $filter = '';
        $projects = [];
        $selected_projects = [];
        $selected_projects_ids = [];
        $input_projects_flag = false;

        $resourceList = User::where('is_active',1)->where('is_billable',1)->get();
        $projectList = Project::whereIn('status',['in_progress','free'])->get();
        
        
        if(empty($request->start_date)) {
            $start_date = date("Y-m-d", strtotime("previous monday"));
            $end_date = date('Y-m-d', strtotime($start_date . ' +13 day')); 
        } else {
            $start_date = date('Y-m-d', strtotime($request->start_date));
            $end_date = date('Y-m-d', strtotime($request->end_date));
            $display_start_date =  $request->start_date;
            $display_end_date =  $request->end_date;
        }

        if ( !empty($request->users) ){
            $eligible_users = $request->users;
            $selected_users = $request->users;
        } else {
            foreach ( $resourceList as $resource )
            {
                $eligible_users[] = $resource->id;
            }
        }

        if ( !empty($request->input_projects) ){
            $projects = $request->input_projects;
            $selected_projects = $request->input_projects;
            $selected_projects_ids = $request->input_projects;
            $input_projects_flag = true;
        } else {
            foreach ( $projectList as $project )
            {
                $selected_projects[] = $project->toArray();
                $selected_projects_ids[] = $project['id'];
            }
        }


        if ( !empty($request->filter) )
        {
            $filter = $request->filter;
        }
        
        $data = ResourceService::resourceAllocationReport($start_date, $end_date, $eligible_users, $filter, $selected_projects_ids, $input_projects_flag);
        
        return View('pages/admin/resource-report/index', ['data' => $data, 'resourceList' => $resourceList, 'selected_users' => $selected_users, 'start_date' => $display_start_date, 'end_date' => $display_end_date, 'projectList' => $projectList, 'selected_projects' => $selected_projects ] );
    }

    public function resourceAllocationReportWithHours(Request $request)
    {
        $display_start_date = '';
        $display_end_date = '';

        $eligible_users =[];
        $selected_users = [];

        $projects = [];
        $selected_projects = [];
        $selected_projects_ids = [];

        $projectList = Project::where('status','in_progress')->get();

        $resourceList = User::where('is_active',1)->where('is_billable',1)->get();
        
        $start_date = date("Y-m-d", strtotime("previous monday"));
        $end_date = date('Y-m-d', strtotime($start_date . ' +6 day'));

        if(empty($request->start_date)) {
            $start_date = date("Y-m-d", strtotime("previous monday"));
            $end_date = date('Y-m-d', strtotime($start_date . ' +6 day')); 
        } else {
            $start_date = date('Y-m-d', strtotime($request->start_date));
            $end_date = date('Y-m-d', strtotime($request->end_date));
            $display_start_date =  $request->start_date;
            $display_end_date =  $request->end_date;
        }

        if ( !empty($request->users) ){
            $eligible_users = $request->users;
            $selected_users = $request->users;
        } else {
            foreach ( $resourceList as $resource )
            {
                $eligible_users[] = $resource->id;
            }
        }

        if ( !empty($request->input_projects) ){
            $projects = $request->input_projects;
            $selected_projects = $request->input_projects;
            $selected_projects_ids = $request->input_projects;
        } else {
            foreach ( $projectList as $project )
            {
                $selected_projects[] = $project->toArray();
                $selected_projects_ids[] = $project['id'];
            }
        }
        
        
        $data = ResourceService::resourceAllocationReportWithHours($start_date, $end_date, $eligible_users, $selected_projects_ids);

        $counts = [];
        for ($i=0;$i<count($data['resource_name']);$i++ )
        {
            $counts[$i] = 0;
        }
        if ( !empty($request->input_projects) )
        {   
            foreach( $data['resourceDataList'] as $index => $resource_data )
            {
                for ($i=0;$i<count($resource_data);$i++ )
                {
                    if ($resource_data[$i]['projects'] != '' )
                    {
                        $counts[$i] = $counts[$i] + 1;
                    } else {
                        $counts[$i] = $counts[$i] + 0;
                    }
                }
            }
            for ( $i=0; $i < count($counts); $i++ )
            {
                if ($counts[$i] == 0)
                {
                    unset($data['resource_name'][$i]);
                    foreach ( $data['resourceDataList'] as $resource_data )
                    {
                        unset($resource_data[$i]);
                    }
                }
            }

        }
        
        return View('pages/admin/resource-hours-report/index', ['data' => $data, 'start_date' => $display_start_date, 'end_date' => $display_end_date, 'resourceList' => $resourceList, 'selected_users' => $selected_users, 'projectList' => $projectList, 'selected_projects' => $selected_projects ]);
    }
}