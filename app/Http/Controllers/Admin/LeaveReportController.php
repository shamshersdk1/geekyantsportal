<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\CalendarYear;
use App\Models\Admin\LeaveType;
use App\Models\Leave\Leave;
use App\Models\User;
use App\Services\Leave\UserLeaveService;
use Excel;
use Redirect;

class LeaveReportController extends Controller
{
    public function yearList()
    {
        $years = CalendarYear::orderBy('year', 'DESC')->get();

        if (!$years || empty($years)) {
            return redirect('/admin/leave-section/leave-report')->withErrors('Calendar year data not found');
        }

        return view('pages.admin.leave-report.index', ['years' => $years]);
    }

    public function yearlyOverview($yearId)
    {
        $start_date = null;
        $till_date = null;
        $yearObj = CalendarYear::where('id', $yearId)->first();
        if (!$yearObj) {
            return redirect('/admin/leave-section/leave-report')->withErrors('year not found');
        }
        $yearStartDate = $yearObj->getFirstDay();
        $yearLastDate = $yearObj->getLastDay();

        //data from datepicker//
        $formData = $_GET;
        if ((isset($formData["till_date"]) ? $formData["till_date"] != "" : false)) {
            if (($formData["till_date"] < $yearStartDate) || ($formData["till_date"] > $yearLastDate)) {
                return redirect::to('/admin/leave-section/leave-report/' . $yearObj->id)->withErrors("Invalid Date!, Select Date within this year");
            }
            $start_date = $yearStartDate;
            $till_date = $formData["till_date"];
        }

        $leaveTypes = LeaveType::get();

        $usedLeaveData = UserLeaveService::getUsersLeaveReport($yearId, $till_date);
        if (!$usedLeaveData['status']) {
            return redirect::to('/admin/leave-section/leave-report')->withErrors($usedLeaveData['message']);
        }

        $users = User::where('is_active', 1)->orderBy('employee_id', 'ASC')->get();

        foreach($users as $user)
        {
            $leaveBalance[$user->id] = UserLeaveService::getUserAllowedLeaveBalance($user->id,$yearObj->year);
        }
        return view('pages.admin.leave-report.yearly-overview', compact('leaveBalance','usedLeaveData', 'users', 'yearObj', 'leaveTypes', 'till_date'));
    }

    public static function downloadLeaveOverviewCSV($yearId)
    {
        $yearObj = CalendarYear::where('id', $yearId)->first();
        if (!$yearObj) {
            return redirect('/admin/leave-section/leave-report')->withErrors('year not found');
        }

        $yearStartDate = $yearObj->getFirstDay();
        $yearLastDate = $yearObj->getLastDay();
        //data from datepicker//
        $formData = $_GET;
        if ((isset($formData["till_date"]) ? $formData["till_date"] != "" : false)) {
            if (($formData["till_date"] < $yearStartDate) || ($formData["till_date"] > $yearLastDate)) {
                return redirect::to('/admin/leave-section/leave-report/' . $yearObj->id)->withErrors("Invalid Dates!, Select Dates within this year");
            }
            $usedLeaves = Leave::where('calendar_year_id', $yearObj->id)->whereDate('end_date', '<=', $formData["till_date"])->where('status', 'approved')->get();
            $tillDataLeaves = Leave::whereDate('start_date', '<=', $formData["till_date"])->whereDate('end_date', '>', $formData["till_date"])->where('status', 'approved')->get();

            $usedLeavesList = $usedLeaves->merge($tillDataLeaves);
        } else {
            $usedLeavesList = Leave::where('calendar_year_id', $yearObj->id)->where('status', 'approved')->get();
        }
        // dd($usedLeavesList);
        $leaveTypes = LeaveType::get();
        $users = User::where('is_active', 1)->orderBy('employee_id', 'ASC')->get();
        $data = [];
        foreach ($users as $user) {
            $data[$user->id]["Employee_id"] = $user->employee_id;
            $data[$user->id]["Name"] = $user->name;
            $userUsedLeaves = $usedLeavesList->where('user_id', $user->id);
            foreach ($leaveTypes as $leaveType) {
                $leave = $userUsedLeaves->where('leave_type_id', $leaveType->id);
                $data[$user->id][$leaveType->title] = "0";
                foreach ($leave as $row) {
                    if (isset($row->days)) {
                        if ($formData["till_date"] != null && $row->end_date > $formData["till_date"]) {
                            $interval = UserLeaveService::calculateWorkingDays($row->start_date, $formData["till_date"], $user->id);
                            $data[$user->id][$leaveType->title] += $interval;
                        } else {
                            $data[$user->id][$leaveType->title] += $row->days;
                        }
                    }
                }
            }

        }
        $filename = "leave_report_year_" . $yearObj->year . "_as_of_" . date('Y-m-d');
        $export_data = $data;
        return Excel::create($filename, function ($excel) use ($export_data) {
            $excel->sheet('mySheet', function ($sheet) use ($export_data) {
                $sheet->fromArray($export_data);
            });
        })->download('csv');
        return Redirect::back()->withErrors(['No data for the given month']);
    }
}
