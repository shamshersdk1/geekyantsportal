<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Credential;
use App\Models\Admin\CredentialCategory;
use App\Models\Admin\CredentialSharing;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Redirect;
use Validator;

class CredentialController extends Controller
{

    public function index()
    {
        $user = Auth::user();
        if($user->hasRole('admin') || $user->hasRole('super-admin') || $user->hasRole('system-admin')){
            $credentials = Credential::getFormattedRecords();
            return View('pages/admin/credential/index', ['credentials' => $credentials]);
        } else {
            $sharedCredentials = Credential::getSharedRecords($user->id);
            return View('pages/admin/credential/index', ['sharedCredentials' => $sharedCredentials]);
        }

    }

    public function create()
    {
        $credential_category = CredentialCategory::find(1);
        $fields = json_decode($credential_category->json_data, true);
        return View('pages.admin.credential.add', compact('fields', 'credential_category'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors(['Name is mandatory']);
        }
        $credential_category = new CredentialCategory();
        $credential_category->name = $request->name;
        if (!$credential_category->save()) {
            return Redirect::back()->withInput()->withErrors(['Error while saving the record']);
        }

        return redirect('/admin/credential')->with('message', 'Successfully Added');
    }

    public function show($id)
    {
        $credentials = Credential::getFormattedRecords();
        $jsonUsers = User::getUsers();
        $selected_credential = Credential::find($id);
        return View('pages/admin/credential/index', ['credentials' => $credentials, 'jsonUsers' => $jsonUsers, 'selected_credential' => $selected_credential]);
    }

    public function edit($id)
    {
        $credential = Credential::find($id);
        $fields = json_decode($credential->json_data, true);
        if (isset($credential)) {
            return View('pages/admin/credential/edit', ['credential' => $credential, 'fields' => $fields]);
        }
        return Redirect::back()->withErrors($credential->getErrors());
    }

    public function update(Request $request, $id)
    {
        $credential = Credential::find($id);
        $data_array = json_decode($credential->json_data, true);
        $to_be_saved = [];
        foreach ($data_array as $data_row) {
            $to_be_saved_row = [];
            $to_be_saved_row['key'] = $data_row['key'];
            $to_be_saved_row['title'] = $data_row['title'];

            if ($data_row['key'] == 'login_url') {
                $to_be_saved_row['value'] = $request->login_url;
            } elseif ($data_row['key'] == 'user_name') {
                $to_be_saved_row['value'] = $request->user_name;
            } elseif ($data_row['key'] == 'password') {
                $to_be_saved_row['value'] = $request->password;
            }

            $to_be_saved[] = $to_be_saved_row;
        }
        $credential->name = $request->name;
        $credential->json_data = json_encode($to_be_saved);
        $credential->notes = $request->notes;
        $credential->save();
        return redirect('/admin/shared-credential')->with('message', 'Successfully Updated');
    }

    public function destroy($id)
    {
        $credential_category = CredentialCategory::find($id);
        if ($credential_category) {
            $credential_category->delete();
            return redirect('/admin/credential')->with('message', 'Deleted successfully');
        } else {
            abort(404);
        }

    }

    public function updateCredential(Request $request)
    {
        $credential_category = CredentialCategory::find($request->credential_category_id);
        $user = Auth::user();
        if(!$user)
            return false;
        $data_array = json_decode($credential_category->json_data, true);
        $to_be_saved = [];
        foreach ($data_array as $data_row) {
            $to_be_saved_row = [];
            $to_be_saved_row['key'] = $data_row['key'];
            $to_be_saved_row['title'] = $data_row['title'];

            if ($data_row['key'] == 'login_url') {
                $to_be_saved_row['value'] = $request->login_url;
            } elseif ($data_row['key'] == 'user_name') {
                $to_be_saved_row['value'] = $request->user_name;
            } elseif ($data_row['key'] == 'password') {
                $to_be_saved_row['value'] = $request->password;
            }

            $to_be_saved[] = $to_be_saved_row;
        }
        $credential = new Credential();
        $credential->credential_category_id = $request->credential_category_id;
        $credential->name = $request->name;
        $credential->json_data = json_encode($to_be_saved);
        $credential->notes = !empty($request->notes) ? $request->notes : '';
        $credential->created_by = $user->id;
        $credential->save();
        return redirect('/admin/shared-credential')->with('message', 'Successfully Updated');
    }

    public function shareCredential(Request $request)
    {

        if (!empty($request->selected_users) && !empty($request->credential_id)) {
            foreach ($request->selected_users as $selected_user) {
                $cred_share = new CredentialSharing();
                $cred_share->user_id = $selected_user;
                $cred_share->credential_id = $request->credential_id;
                if (!$cred_share->save()) {
                    \Log::info($errors);
                }
            }
            return redirect('/admin/shared-credential')->with('message', 'Successfully Shared');
        }
    }
}
