<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Admin\TechEvent;
use App\Models\User;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Redirect;
use Config;
use Exception;
use DB;
use Auth;
use Validator;

class TechEventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $techEvents = TechEvent::paginate(20);
        return View('pages/admin/tech-events/index', ['techEvents' => $techEvents ] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();
        $user = Auth::user();
            return View('pages/admin/tech-events/add',['users' => $users]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'url' => 'required',
            'location' => 'required',
            'start_date' => 'required',
            'end_date' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect::back()->withErrors($validator->errors())->withInput($request->input());
        }
        if ( !$request->hasFile('display_file') )
        {
            return Redirect::back()->withInput()->withErrors(['Please select a display image file']);
        }
        
        $response = TechEvent::saveData($request->all());
    
        if ( $response['status'] )
        {
            return redirect('/admin/tech-events/'.$response['message'])->with('message', 'Successfully added tech event');
        }
        else
        {
            return Redirect::back()->withInput()->withErrors($response['message']);
        }

    }
    /**
     * Show details of the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $techEventObj = TechEvent::with('image','logoImage')->find($id);
        return View('pages/admin/tech-events/show', ['techEventObj' => $techEventObj] );
        
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $techEvent = TechEvent::with('image','logoImage')->find($id);

        if($techEvent){

            return View('pages/admin/tech-events/edit', ['techEvent' => $techEvent]);
        }
        else {
            return Redirect::back()->withInput()->withErrors('Tech event not found');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'url' => 'required',
            'location' => 'required',
            'start_date' => 'required',
            'end_date' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect::back()->withErrors($validator->errors())->withInput($request->input());
        }

        $input = $request->all();
        $response = TechEvent::updateData($id, $input);
        if ( $response['status'] )
        {
            return redirect('/admin/tech-events/'.$response['message'])->with('message', 'Successfully updated tech event');
        }
        else
        {
            return Redirect::back()->withInput()->withErrors($response['message']);
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response = TechEvent::deleteRecord($id);
        if ( $response['status'] )
        {
            return redirect('/admin/tech-events')->with('message','Successfully Deleted');
        }
        else
        {
            return redirect::back()->withErrors($response['message']);
        }
    }
    
}
