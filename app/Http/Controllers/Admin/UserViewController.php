<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Appraisal;
use App\Models\Admin\Bonus;
use App\Models\Admin\CmsProject;
use App\Models\Admin\Company;
use App\Models\Admin\DashboardEvent;
use App\Models\Admin\Leave;
use App\Models\Admin\News;
use App\Models\Admin\ProfileProject;
use App\Models\Loan;
use App\Models\LoanEmi;
use App\Models\Transaction;
use App\Models\User;
use App\Models\UserView;
use App\Services\CtcService;
use App\Models\Admin\LoanRequest;
use App\Services\NewLeaveService;
use DB;
use Excel;
use Illuminate\Http\Request;
use Redirect;

class UserViewController extends Controller
{
    public function showDashboard($id = null)
    {
        if ($id == null) {
            $user = \Auth::user();
            $id = $user->id;
        } else {
            $user = User::find($id);
        }
        if (!$user) {
            return redirect('/admin/users')->withErrors('User not found.');
        }
        $date = date('Y-m-d');
        $holidays = DB::table('non_working_calendar')->whereDate('date', '>=', date('Y', strtotime($date)) . '-04-01')->whereDate('date', '<=', (date('Y', strtotime($date)) + 1) . '-03-31')->orderBy('date')->get();
        $active = DB::table('non_working_calendar')->whereDate('date', '>=', $date)->orderBy('date')->first();
        $result = UserView::leaves($user);
        $consumedSickLeaves = $result['consumedSickLeaves'];
        $totalSickLeaves = $result['totalSickLeaves'];
        $consumedPaidLeaves = $result['consumedPaidLeaves'];
        $totalPaidLeaves = $result['totalPaidLeaves'];
        $result = UserView::payslip($id, 5);
        $nextLeave = Leave::where('user_id', $id)->where('status', 'approved')->where('start_date', '>', $date)->orderBy('start_date')->first();
        $bdays = UserView::bdate(5);
        $news = News::orderBy('date', 'Desc')->take(5)->get();
        $events = DashboardEvent::where('user_id', $id)->where('date', '>', $date)->orderBy('date')->orderBy('time')->take(5)->get();
        return view('pages.admin.userView.user-dashboard', compact('user', 'result', 'holidays', 'active', 'consumedSickLeaves', 'totalSickLeaves', 'consumedPaidLeaves', 'totalPaidLeaves', 'nextLeave', 'bdays', 'news', 'events'));
    }
    public function showProfile($id = null)
    {
        if ($id == null) {
            $user = \Auth::user();
        } else {
            $user = User::find($id);
        }
        if (!$user) {
            return redirect('/admin/users')->withErrors('User not found.');
        }
        if (!empty($user->profile->keywords)) {
            $keywords = explode(',', $user->profile->keywords);
        } else {
            $keywords = "";
        }
        if (!empty($user->profile->expertise_json)) {
            $skills = json_decode($user->profile->expertise_json, true);
        } else {
            $skills = "";
        }
        if (!empty($user->profile->knowledge)) {
            $knowledge = explode(',', $user->profile->knowledge);
        } else {
            $knowledge = "";
        }
        $colours = ["red", "orange", "yellow", "green", "blue", "purple", "black"];
        return view('pages.admin.userView.profile', compact('user', 'keywords', 'skills', 'colours', 'knowledge'));
    }
    public function showSalary(Request $request, $id = null)
    {
        if ($id == null) {
            $user = \Auth::user();
            $id = \Auth::id();
        } else {
            $user = User::find($id);
        }
        if (!$user) {
            return redirect('/admin/users')->withErrors('User not found.');
        }
        $resultObj = new CtcService;
        $result = $resultObj->downloadUserPayslip($id);

        if (!$result['status'] || empty($result['data'])) {
            return Redirect::back()->withErrors($result['log']);
        }
        $data = $result['data'];
        return view('pages.admin.userView.salary', compact('user', 'data'));
    }
    public function showPayslips($id = null)
    {
        if ($id == null) {
            $user = \Auth::user();
            $id = $user->id;
        } else {
            $user = User::find($id);
        }
        if (!$user) {
            return redirect('/admin/users')->withErrors('User not found.');
        }
        $result = UserView::payslip($id, -1);
        return view('pages.admin.userView.payslips', compact('user', 'result'));
    }
    public function showAppraisals($id = null)
    {
        if ($id == null) {
            $user = \Auth::user();
        } else {
            $user = User::find($id);
        }
        if (empty($user)) {
            return redirect::back()->withErrors('Invalid Link Followed');
        }
        $appraisals = Appraisal::where('user_id', $id)->orderBy('is_active', 'desc')->orderBy('effective_date', 'desc')->get();
        if (substr($user->name, -1) == "s") {
            $title = $user->name . '\' Appraisals';
        } else {
            $title = $user->name . '\'s Appraisals';
        }
        return view('pages.admin.userView.appraisals', compact('user', 'appraisals', 'title'));
    }
    public function showLeaves($id = null)
    {
        if ($id == null) {
            $user = \Auth::user();
            $id = \Auth::id();
            $jsonclients = "";
        } else {
            $user = User::find($id);
            $jsonclients = Company::getClients();
        }
        if (empty($user)) {
            return redirect::back()->withErrors('Invalid Link Followed');
        }
        $result = UserView::leaves($user);
        $consumedSickLeaves = NewLeaveService::getUserLeaveTaken($user->id, 'sick');
        $totalSickLeaves = NewLeaveService::getAllowedLeave($user->id, 'sick');
        $consumedPaidLeaves = NewLeaveService::getUserLeaveTaken($user->id, 'paid');
        $totalPaidLeaves = NewLeaveService::getAllowedLeave($user->id, 'paid');
        $carryForwardLeaves = NewLeaveService::getCarryForwardLeaves($user->id);
        $avaliableSickLeaves = $totalSickLeaves - $consumedSickLeaves;
        $avaliablePaidLeaves = $totalPaidLeaves - $consumedPaidLeaves;
        $leaves = Leave::where('user_id', $id)->orderBy('start_date', 'desc')->orderBy('status')->paginate(9);
        return view('pages.admin.userView.leaves', compact('user', 'avaliableSickLeaves', 'consumedSickLeaves', 'totalSickLeaves', 'avaliablePaidLeaves', 'consumedPaidLeaves', 'totalPaidLeaves', 'leaves', 'jsonclients', 'carryForwardLeaves'));
    }
    public function showLoans($id = null)
    {
        if ($id == null) {
            $user = \Auth::user();
            $id = \Auth::id();
        } else {
            $user = User::find($id);
        }
        if (empty($user)) {
            return redirect::back()->withErrors('Invalid Link Followed');
        }
        $loans = Loan::where('user_id', $id)->get();
        return view('pages.admin.userView.loans', compact('user', 'loans'));
    }
    public function showEMI($id)
    {
        $user = \Auth::user();
        $loan = Loan::find($id);
        if ($loan->user->id != \Auth::id() && !(\Auth::user()->isAdmin())) {
            return redirect::back()->withErrors('You can\'t view this link');
        }
        $emis = LoanEmi::where('loan_id', $id)->paginate(12);
        if (empty($emis)) {
            return redirect::back()->withErrors('Invalid Link Followed');
        }
        return view('pages.admin.userView.loanemi', compact('user', 'emis'));
    }
    public function showEMIadmin($id, $lid)
    {
        $loan = Loan::find($lid);
        $user = $loan->user;
        if (!(\Auth::user()->isAdmin())) {
            return redirect::back()->withErrors('You can\'t view this link');
        }
        $emis = LoanEmi::where('loan_id', $lid)->paginate(12);
        if (empty($emis)) {
            return redirect::back()->withErrors('Invalid Link Followed');
        }
        return view('pages.admin.userView.loanemi', compact('user', 'emis'));
    }
    public function showBonuses($id = null)
    {
        if ($id == null) {
            $user = \Auth::user();
            $id = \Auth::id();
            $bonuses = Bonus::where('user_id', $id)->where('status', 'approved')->get();
        } else {
            $user = User::find($id);
            $bonuses = Bonus::where('user_id', $id)->get();
        }
        if (empty($user)) {
            return redirect::back()->withErrors('Invalid Link Followed');
        }
        return view('pages.admin.userView.bonus', compact('user', 'bonuses'));
    }
    public function editProfile($id)
    {
        $user = User::find($id);
        if (empty($user)) {
            return redirect::back()->withErrors('Invalid Link Followed');
        }
        if (!empty($user->profile->expertise_json)) {
            $expertise = json_decode($user->profile->expertise_json, true);
        } else {
            $expertise = "";
        }
        $projects = CmsProject::all();
        $profileProjects = ProfileProject::where('profile_id', $user->profile->id)->select('project_id')->get();

        $profileProjects = $profileProjects->toArray();

        $selectedProjects = [];
        foreach ($profileProjects as $key => $value) {
            array_push($selectedProjects, $value['project_id']);
        }

        return view('pages.admin.userView.profile-edit', compact('user', 'expertise', 'projects', 'selectedProjects'));
    }
    public function showBdays($id = null)
    {
        if ($id == null) {
            $user = \Auth::user();
            $id = \Auth::id();
        } else {
            $user = User::find($id);
        }
        if (empty($user)) {
            return redirect::back()->withErrors('Invalid Link Followed');
        }
        $bdays = UserView::bdate(-1);
        return view('pages.admin.userView.bdays', compact('user', 'bdays'));
    }
    public function showMyEvents($id = null)
    {
        if ($id == null) {
            $user = \Auth::user();
            $id = \Auth::id();
        } else {
            $user = User::find($id);
        }
        if (empty($user)) {
            return redirect::back()->withErrors('Invalid Link Followed');
        }
        $date = date('Y-m-d');
        $events = DashboardEvent::where('user_id', $id)->orderBy('date', 'desc')->orderBy('time')->paginate(10);
        return view('pages.admin.userView.my-events', compact('user', 'events'));
    }
    public function showFinancialTransactions($id)
    {
        $user = User::find($id);
        if (empty($user)) {
            return redirect::back()->withErrors('Invalid Link Followed');
        }
        $transactions = Transaction::with('reference')->where('user_id', $user->id)->orderBy('id', 'desc')->get();
        return view('pages.admin.userView.financial-transactions', compact('user', 'transactions'));

    }

    public function downloadFinancialTransactions($id)
    {
        $user = User::find($id);
        if (empty($user)) {
            return redirect::back()->withErrors('Invalid Link Followed');
        }
        $transactions = Transaction::where('user_id', $user->id)->orderBy('date', 'desc')->orderBy('id', 'desc')->get();
        $data = [];
        foreach ($transactions as $index => $transaction) {
            $data[$index]["Transaction_Id"] = $transaction->id;
            $data[$index]["Date"] = date_in_view($transaction->date);
            $data[$index]["Type"] = ucfirst($transaction->type);
            $data[$index]["Amount"] = $transaction->amount;
            if ($transaction->reference_type == "App\Models\Admin\Bonus" && $transaction->reference && $transaction->reference->type) {
                $data[$index]["Transaction_For"] = ucfirst($transaction->reference->type) . " bonus on " . $transaction->reference->date;
            } elseif ($transaction->reference_type == "App\Models\Admin\Finance\ItSavingUserData" && $transaction->reference) {
                $data[$index]["Transaction_For"] = "TDS";
            } elseif ($transaction->reference_type == "App\Models\Admin\Finance\SalaryUserDataTDS" && $transaction->reference && $transaction->reference->tds_for_the_month) {
                $data[$index]["Transaction_For"] = "TDS, where TDS for the month was " . $transaction->reference->tds_for_the_month;
            } elseif ($transaction->reference_type == "App\Models\Admin\Finance\PayroleGroupRuleMonth" && $transaction->reference && $transaction->reference->name && $transaction->reference->type) {
                $data[$index]["Transaction_For"] = "Monthly " . $transaction->reference->type . " for " . $transaction->reference->name;
            } elseif ($transaction->reference_type == "App\Models\Admin\Insurance\InsuranceDeduction" && $transaction->reference && $transaction->reference->insurance_policy_id) {
                $data[$index]["Transaction_For"] = "Insurance deduction for Policy " . $transaction->reference->insurance_policy_id;
            } elseif ($transaction->reference_type == "App\Models\Admin\VpfDeduction" && $transaction->reference && $transaction->reference->vpf_id) {
                $data[$index]["Transaction_For"] = "VPF deduction for VPF ID " . $transaction->reference->vpf_id;
            } elseif ($transaction->reference_type == "App\Models\Admin\AppraisalBonus" && $transaction->reference && $transaction->reference->type) {
                $data[$index]["Transaction_For"] = $transaction->reference->type . "Appraisal Bonus";
            } else {
                $data[$index]["Transaction_For"] = $transaction->reference_type . " | " . $transaction->reference_id;
            }

            $data[$index]["Transaction_Id"] = $transaction->reference_id;
            $data[$index]["Created_At"] = datetime_in_view($transaction->created_at);
        }
        if (count($transactions) <= 0) {
            $data['warning'] = "No Transactions found for this user";
        }
        $filename = "financial_transactions_of_" . $user->name . "_" . $user->employee_id . "_as_of_" . date('Y-m-d');
        $export_data = $data;
        return Excel::create($filename, function ($excel) use ($export_data) {
            $excel->sheet('mySheet', function ($sheet) use ($export_data) {
                $sheet->fromArray($export_data);
            });
        })->download('csv');
        return view('pages.admin.userView.financial-transactions', compact('user', 'transactions'));
    }
}
