<?php

namespace App\Http\Controllers\Admin\BankTransfer;

use App\Http\Controllers\Controller;
use App\Jobs\BankTransfer\ReverseBankTransferJob;
use App\Models\Admin\File;
use App\Models\Audited\BankTransfer\BankTransfer;
use App\Models\Audited\BankTransfer\BankTransferHoldUser;
use App\Models\Audited\BankTransfer\BankTransferUser;
use App\Models\Month;
use App\Models\Transaction;
use App\Services\BankTransferService;
use Excel;
use Illuminate\Http\Request;
use Redirect;

class BankTransferController extends Controller
{

    public function index($monthId = null)
    {
        $monthList = Month::orderBy('year', 'desc')->orderBy('month', 'desc')->get();
        if (!count($monthList) > 0) {
            return view('pages.admin.bank-transfer.bank-transfer.index')->with('message', 'no month found.');
        }
        $currMonth = $monthList[0];
        if ($monthId) {
            $currMonth = Month::find($monthId);
        }
        $bankTransfers = BankTransfer::where('month_id', $currMonth->id)->get();
        return view('pages.admin.bank-transfer.bank-transfer.index', compact('currMonth', 'monthList', 'bankTransfers'));
    }

    public function saveTransfer(Request $request, $bankTransferUserId)
    {
        $transferUserObj = BankTransferUser::find($bankTransferUserId);
        if (!$transferUserObj) {
            return Redirect::back()->withErrors('Bank Transfer User not found!');
        }

        if (!$request->actual_amount || $request->actual_amount < 0) {
            return Redirect::back()->withErrors('Enter correct amount!');
        }

        $transferUserObj->transaction_amount = $request->actual_amount ? $request->actual_amount : 0;
        $transferUserObj->mode_of_transfer = $request->mode_of_transfer ? $request->mode_of_transfer : 0;
        $transferUserObj->comment = $request->comment ? $request->comment : 0;
        $transferUserObj->bank_transaction_id = $request->reference_number ? $request->reference_number : 0;
        $transferUserObj->status = "pending";

        if (!$transferUserObj->save()) {
            return Redirect::back()->withErrors('Data not saved!');
        }

        return Redirect::back()->with('message', 'Bank Transfer data saved.');
    }

    public function completeTransfer($bankTransferUserId)
    {
        $transferUserObj = BankTransferUser::find($bankTransferUserId);
        if (!$transferUserObj) {
            return Redirect::back()->withErrors('Bank Transfer User not found!');
        }

        if (!$transferUserObj->mode_of_transfer || $transferUserObj->mode_of_transfer == '') {
            return Redirect::back()->withErrors('Save Mode Of Transfer!');
        }
        if (!$transferUserObj->bank_transaction_id || $transferUserObj->bank_transaction_id == '') {
            return Redirect::back()->withErrors('Save Reference Number!');
        }

        $transferUserObj->status = "completed";

        if (!$transferUserObj->save()) {
            return Redirect::back()->withErrors('Data not saved!');
        }

        return Redirect::back()->with('message', 'Bank Transfer rejected!');
    }

    public function rejectTransfer($bankTransferUserId)
    {
        $transferUserObj = BankTransferUser::find($bankTransferUserId);
        if (!$transferUserObj) {
            return Redirect::back()->withErrors('Bank Transfer User not found!');
        }

        if (!$transferUserObj->comment || $transferUserObj->comment == '') {
            return Redirect::back()->withErrors('Save comment!');
        }

        $transferUserObj->status = "rejected";

        if (!$transferUserObj->save()) {
            return Redirect::back()->withErrors('Data not saved!');
        }
        dispatch(new ReverseBankTransferJob($bankTransferUserId));

        return Redirect::back()->with('message', 'Bank Transfer rejected!');
    }

    public static function downloadCSV($id)
    {
        $data = [];
        $i = 0;
        $bankTransferObj = BankTransfer::find($id);
        $bankTransferObjs = BankTransferUser::select('bank_transfer_users.*', \DB::raw('(SELECT employee_id FROM users WHERE bank_transfer_users.user_id = users.id ) as employee_id'))
            ->orderBy('employee_id')->where('bank_transfer_id', $id)->get();
        if ($bankTransferObjs) {
            foreach ($bankTransferObjs as $index => $bankTransfer) {
                if ($bankTransfer->amount != 0 && $bankTransfer->user->is_active == 1) {
                    $i++;
                    $data[$bankTransfer->id]["Sl_No"] = $i;
                    $data[$bankTransfer->id]["Employee_id"] = $bankTransfer->user->employee_id;
                    $data[$bankTransfer->id]["Transaction_Type"] = "NFT";
                    $data[$bankTransfer->id]["Debit_Account_No"] = "029751000005";
                    $data[$bankTransfer->id]["IFSC"] = $bankTransfer->user->userDetails->bank_ifsc_code;
                    $data[$bankTransfer->id]["Beneficiary_Account_No"] = $bankTransfer->user->userDetails->bank_ac_no;
                    $data[$bankTransfer->id]["Beneficiary_Name"] = $bankTransfer->user->name;
                    $data[$bankTransfer->id]["Amount"] = $bankTransfer->amount;
                    $data[$bankTransfer->id]["Remarks_for_Client"] = "Salary for " . $bankTransferObj->month->formatMonth();
                    $data[$bankTransfer->id]["Remarks_for_Beneficiary"] = "Salary for " . $bankTransferObj->month->formatMonth();
                }
            }
        }
        $filename = "Bank_Transfer_of_" . $bankTransferObj->month->formatMonth();
        $export_data = $data;
        return Excel::create($filename, function ($excel) use ($export_data) {
            $excel->sheet('mySheet', function ($sheet) use ($export_data) {
                $sheet->fromArray($export_data);
            });
        })->download('csv');
    }

    public function complete($bankTransferId)
    {
        $bankTransfers = BankTransfer::find($bankTransferId);
        if (!$bankTransferId) {
            return Redirect::back()->withErrors('Bank Transfer Id not found!');
        }
        $overallStatus = '';

        if (count($bankTransfers->transferUsers->where('status', 'pending')) > 0) {
            $overallStatus = 'pending';
            $bankTransfers->status = 'open';
            $bankTransfers->save();
        } else {
            $bankTransfers->status = 'completed';
            $bankTransfers->save();
        }
        return view('pages.admin.bank-transfer.bank-transfer.complete', compact('bankTransfers', 'overallStatus'));
    }

    public function update(Request $request, $id)
    {
        $status = false;
        $actualAmounts = $request->actual_amount;
        $modeOfTransfers = $request->mode_of_transfer;
        $referenceNumbers = $request->reference_number;
        $bankTransfers = BankTransfer::where('month_id', $id)->first();

        foreach ($bankTransfers->transferUsers as $bankTransfer) {
            $bankTransferObj = BankTransferUser::find($bankTransfer->id);
            $bankTransferObj->transaction_amount = $actualAmounts[$bankTransferObj->id];
            $bankTransferObj->mode_of_transfer = $modeOfTransfers[$bankTransferObj->id];
            $bankTransferObj->bank_transaction_id = $referenceNumbers[$bankTransfer->id];
            if (!$bankTransferObj->update()) {
                $status = true;
                $errors[] = $bankTransferObj->getErrors();
            }
        }
        if ($status) {
            return Redirect::back()->withErrors($errors);
        }

        return Redirect::back();
    }

    public function getHolds($bankTransferId)
    {
        $bankTransferHold = BankTransfer::find($bankTransferId);
        return view('pages.admin.bank-transfer.bank-transfer-hold.index', compact('bankTransferHold'));
    }

    public function release($bankTransferId, $bankTransferHoldId)
    {
        $currentDate = date("Y-m-d");
        $month = Month::getMonthByDate($currentDate);
        $bankTransferObj = BankTransferHoldUser::find($bankTransferHoldId);
        $check = BankTransferHoldUser::where('id', $bankTransferHoldId)->update(['is_processed' => 1]);
        if (!$check) {
            Redirect::back()->withError("Something went wrong");
        }

        ($bankTransferObj->amount > 0) ? $type = "credit" : $type = "debit";
        $transactionObj = Transaction::create(['month_id' => $month->id, 'financial_year_id' => $month->financial_year_id, 'user_id' => $bankTransferObj->user_id, 'reference_id' => $bankTransferObj->id, 'reference_type' => 'App\Models\Audited\BankTransfer\BankTransferHoldUser', 'amount' => $bankTransferObj->amount, 'type' => $type]);
        if (!$transactionObj->isValid()) {
            Redirect::back()->withError($transactionObj->getErrors());
        }

        return Redirect::back()->withMessage("Successfully Released");
    }

    public function saveAllTransfer(Request $request)
    {
        $data = $request->all();
        $errors = "";
        $status = false;
        foreach ($data as $index => $obj) {
            if ($index != "_token" && $index != "_method") {
                $transferUserObj = BankTransferUser::find($index);
                if (!$transferUserObj) {
                    $status = true;
                    $errors = $errors . "Unable To update!";
                    continue;
                }

                if ($obj['transaction_amount'] < 0) {
                    $status = true;
                    $errors = $errors . "Unable to update the status for #" . $transferUserObj->user->employee_id . " - " . $transferUserObj->user->name . ". Actual Amount is in negative.";
                    continue;
                }
                $transferUserObj->transaction_amount = $obj['transaction_amount'] ? $obj['transaction_amount'] : null;
                $transferUserObj->mode_of_transfer = $obj['mode_of_transfer'] ? $obj['mode_of_transfer'] : null;
                $transferUserObj->comment = $obj['comment'] ? $obj['comment'] : null;
                $transferUserObj->bank_transaction_id = $obj['bank_transaction_id'] ? $obj['bank_transaction_id'] : null;
                $transferUserObj->status = "pending";
                if (!$transferUserObj->save()) {
                    $status = true;
                    $errors = $errors . "Unable to update the status for #" . $transferUserObj->user->employee_id . " - " . $transferUserObj->user->name . ". Please save the record before any action.";
                }
            }
        }
        if ($status) {
            return Redirect::back()->withErrors($errors);
        }
        return Redirect::back()->with('message', 'Bank Transfer data saved.');
    }

    public function completeAllTransfer($bankTransferId)
    {
        $status = false;
        $errors = "Plase update the Mode of Payment and Reference number for the follwoing record to complete:-<br>";
        $transferUserObjs = BankTransfer::find($bankTransferId);
        if (!count($transferUserObjs) > 0) {
            return Redirect::back()->withErrors('Bank Transfer User not found!');
        }
        foreach ($transferUserObjs->transferUsers->where('status', '!=', 'completed') as $transferUserObj) {
            if ($transferUserObj->mode_of_transfer == null || $transferUserObj->bank_transaction_id == null || $transferUserObj->mode_of_transfer == '' || $transferUserObj->bank_transaction_id == '') {
                $status = true;
                $errors = $errors . "#" . $transferUserObj->user->employee_id . ". " . $transferUserObj->user->name . "<br>";
                continue;
            }
            $transferUserObj->status = "completed";
            $transferUserObj->save();
        }

        if ($status) {
            return Redirect::back()->withErrors($errors);
        }
        $transferUserObjs->status = "completed";
        $transferUserObjs->save();
        return Redirect::back()->with('message', 'Bank Transfer completed!');
    }

    public function rejectAllTransfer($bankTransferId)
    {
        $status = false;
        $errors = "Plase update the comment number for the follwoing record to Reject: <br>";
        $transferUserObjs = BankTransfer::find($bankTransferId);
        if (!count($transferUserObjs) > 0) {
            return Redirect::back()->withErrors('Bank Transfer User not found!');
        }
        foreach ($transferUserObjs->transferUsers->where('status', '!=', 'completed') as $transferUserObj) {
            if ($transferUserObj->comment == null || $transferUserObj->comment == '') {
                $status = true;
                $errors = $errors . "#" . $transferUserObj->user->employee_id . ". " . $transferUserObj->user->name . "<br>";
                continue;
            }
            $transferUserObj->status = "rejected";
            $transferUserObj->save();
        }
        if ($status) {
            return Redirect::back()->withErrors($errors);
        }
        $transferUserObjs->satus = "completed";
        $transferUserObjs->save();
        return Redirect::back()->with('message', 'Bank Transfer rejected!');
    }
    public function showSalarySheet($bankTransferId)
    {
        $data = [];
        $bankTransferObj = BankTransfer::find($bankTransferId);

        if (!$bankTransferObj) {
            return Redirect::back()->withErrors('Invalid Bank Transfer #' . $bankTransferId);
        }

        $data = Transaction::getSalarySheet($bankTransferObj->month_id);
        return view('pages.admin.bank-transfer.salary-sheet.index', compact('data'));
    }
    public function downloadSalarySheet($bankTransferId)
    {
        $bankTransferObj = BankTransfer::find($bankTransferId);

        if (!$bankTransferObj) {
            return Redirect::back()->withErrors('Invalid Bank Transfer #' . $bankTransferId);
        }

        $data = Transaction::getSalarySheet($bankTransferObj->month_id);
        $filename = "Salary_Sheet_of_" . $bankTransferObj->month->formatMonth();
        $export_data = $data;
        return Excel::create($filename, function ($excel) use ($export_data) {
            $excel->sheet('mySheet', function ($sheet) use ($export_data) {
                $sheet->fromArray($export_data);
            });
        })->download('csv');
    }

    public function upload(Request $request, $bankTransferId)
    {
        $bankTransferObj = BankTransfer::find($bankTransferId);
        if (!$bankTransferObj) {
            return Redirect::back()->withErrors('Invalid Bank Transfer #' . $bankTransferId);
        }
        if ($request->file) {
            $response = BankTransferService::uploadFile($request->file, $bankTransferId);
            if (!$response || !$response['status']) {
                return Redirect::back()->withErrors('File Upload Failed!');
            }
        } else {
            return Redirect::back()->withErrors('File not found!');
        }

        if ($response['data']) {
            $result = BankTransferService::transferData($request->file, $bankTransferId);
            if ($result['status'] == false) {
                return Redirect::back()->withErrors($result['message']);
            }
        } else {
            return Redirect::back()->withErrors('File not found!');
        }

        return redirect('/admin/bank-transfer/' . $bankTransferId . '/view-complete')->with('message', 'File Uploaded successfully!');
    }
}
