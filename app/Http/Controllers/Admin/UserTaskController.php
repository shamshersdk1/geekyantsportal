<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Project;
use App\Models\Admin\ProjectResource;
use App\Models\Admin\Calendar;
use App\Models\Admin\UserTask;
use App\Models\User;

use Auth;
use Input;
use Redirect;

class UserTaskController extends Controller
{
    public function index()
    {
        $response = UserTask::getData();
        if($response['status']) {
            $data = $response['data'];
            $dates = $response['dates'];
            $date_code = $response['date_code'];
            $users = User::where('is_active', 1)->get();
            $available_users = User::whereNotIn('id', $response['exclude'])->where('is_active', 1)->get();
            return view('pages.admin.user-task.index', compact('data', 'dates', 'date_code', 'users', 'available_users'));
        } else {
            return Redirect::back()->withError([$response['message']]);
        }
    }
}
