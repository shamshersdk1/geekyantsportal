<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\Admin\PayslipMonth;

use App\Models\Admin\PayslipData;

use PDF;

use Redirect;

class EmployeePayslipController extends Controller
{

    public function viewPayslip(Request $request, $id)
    {
        $payslip = PayslipData::find($id);
        if (!$payslip) {
            return Redirect::back()->withErrors(['Payslip not found.']);
        }
        if (\Auth::id()!=$payslip->user->id&&!(\Auth::user()->isAdmin())) {
            return Redirect::to('/user/payslips')->with('message', 'You are not authorized to view this page');
        }
        $template = $payslip->payslip_month->template ?  $payslip->payslip_month->template : 'jan-2018';
        
        $pdf = PDF::loadView('pages.admin.payslip.template.'.$template, compact('payslip'));
        return $pdf->stream('payslip.pdf', array('Attachment'=>0));
    }
}
