<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\LeaveService;
use App\Models\Admin\Leave;
use App\Models\User;
use App\Models\SystemSetting;
use Log;
use Auth;
use Redirect;
use Validator;

class EmployeeLeaveController extends Controller
{
    public function history()
    {
        $user = Auth::user();
        $leaves = Leave::where('user_id', \Auth::id())->orderBy('id', 'desc')->paginate(10);
        return View("/pages/admin/employee-leave/history", ['user' => $user, 'leaves' => $leaves]);
    }

    public function template()
    {
        $leaveWithUser = Leave::with('user')->where('id', 20)->first();
        return view('emails.employee-leave-handled', ['leave' => $leaveWithUser, 'user' => 'Pritesh']);
    }

    public function apply()
    {
        $user = Auth::user();
        $totalSickLeaves = SystemSetting::where('key', 'max_sick_leave')->first();
        $totalPaidLeaves = SystemSetting::where('key', 'max_paid_leave')->first();

        if ($totalSickLeaves) {
            $totalSickLeaves = $totalSickLeaves->value;
        }
        if ($totalPaidLeaves) {
            $totalPaidLeaves = $totalPaidLeaves->value;
        }

        if ($user) {
            if (isset($user->joining_date)) {
                $leaves = $user->leaves->where('type', 'sick')->where('status', 'approved');
                $joiningDate = $user->joining_date;
                $totalSickLeaves = LeaveService::maxSickLeave($joiningDate);
                $consumedSickLeaves = 0;
                foreach ($leaves as $leave) {
                    $consumedSickLeaves = $consumedSickLeaves + $leave->days;
                }
                $avaliableSickLeaves = $totalSickLeaves - $consumedSickLeaves;
                if ($avaliableSickLeaves <= 0) {
                    $avaliableSickLeaves = 0;
                }
            } else {
                $avaliableSickLeaves = 0;
                $consumedSickLeaves = 0;
            }
            if (isset($user->confirmation_date)) {
                $leaves = $user->leaves->whereIn('type', ['paid','half'])->where('status', 'approved');
                $joiningDate = $user->joining_date;
                $totalPaidLeaves = LeaveService::maxPersonalLeave($joiningDate);
                $consumedPaidLeaves = 0;
                foreach ($leaves as $leave) {
                    $consumedPaidLeaves = $consumedPaidLeaves + $leave->days;
                }
                $avaliablePaidLeaves = $totalPaidLeaves - $consumedPaidLeaves;
            } else {
                $avaliablePaidLeaves = 0;
                $consumedPaidLeaves = 0;
            }

            return View('/pages/admin/employee-leave/apply-leave', ['user' => $user, 'avaliableSickLeaves' => $avaliableSickLeaves, 'consumedSickLeaves' => $consumedSickLeaves, 'totalSickLeaves' => $totalSickLeaves, 'avaliablePaidLeaves' => $avaliablePaidLeaves, 'consumedPaidLeaves' => $consumedPaidLeaves, 'totalPaidLeaves' => $totalPaidLeaves]);
        } else {
            abort(404);
        }
    }

    public function requestLeave(Request $request)
    {
        echo "TEST";
        die;
        $user = Auth::user();
        if (!$user) {
            return redirect::back()->withErrors('User Not Found')->withInput($request->input());
        }
        $validator = Validator::make($request->all(), [
            'start_date' => 'required',
            'end_date' => 'required',
            'type' => 'required|in:paid,unpaid',
            ]);
            
        if ($validator->fails()) {
            return redirect::back()->withErrors($validator->errors())->withInput($request->input());
        }
            
        $input = $request->all();
        echo "<pre>";
        print_r($input);
        die;
        $input['start_date'] = date_to_yyyymmdd($request->start_date);
        $input['end_date'] = date_to_yyyymmdd($request->end_date);
        $input['user_id'] = $user->id;
        $skip=0;
        $response  = Leave::validateLeave($user, $input, $skip);
        if (!$response['status']) {
            return redirect::back()->withErrors($response['errors'])->withInput($request->input());
        }


        /*if($request->type == "sick"){

            if(!isset($user->profile->join_date)) {
                return redirect::back()->withErrors("Joining Date is not avaliable")->withInput($request->input());
            }

            $response = Leave::validateAndSaveSickLeave($user, $input);
            if(!$response['status']) {
                return redirect::back()->withErrors($response['message'])->withInput($request->input());
            }
            return redirect('/admin/employee-leave/history')->with('message', $response['message']);
    	}
    	elseif ($request->type == "paid") {

            if(!isset($user->profile->confirmation_date)) {
                return redirect::back()->withErrors("Confirmation Date is not avaliable")->withInput();
            }

            $response = Leave::validateAndSavePaidLeave($user, $input);
            if(!$response['status']) {
                return redirect::back()->withErrors($response['message'])->withInput();
            }
            return redirect('/admin/employee-leave/history')->with('message', $response['message']);

    	}
    	elseif ($request->type == "unpaid") {*/

            $result = Leave::saveData($input);
        if ($result['status']) {
            return redirect::back()->with('message', 'Request Submitted');
        } else {
            return redirect::back()->withErrors($result['errors'])->withInput();
        }
    /*} else {
        return redirect::back()->withErrors("Please select a Leave type")->withInput();
    }*/
    }


    /**
     * Cancel an upcoming 'pending' or 'approved' leave.
     *
     * @return Response
    */
    public function cancelLeave(Request $request, $id)
    {
        $user = Auth::user();
        $leave = Leave::find($id);
        if ($leave) {
            $today = date("Y-m-d");
            $leaveStartDate = date('Y-m-d', strtotime($leave->start_date));

            if ($today >= $leaveStartDate) {
                return redirect::back()->withErrors(['Leave cannot be cancelled as Leave has already started or passed.'])->withInput();
            } else {
                if ($request->cancel) {
                    if ($user->isAdmin()) {
                        $leave->approver_id=Auth::id();
                    }
                    $leave->status = "cancelled";
                    $leave->action_date=date('Y-m-d');
                    if ($leave->save()) {
                        Leave::notifyLeaveAction($leave->id);
                        // return redirect('/admin/employee-leave/history')->with('message', 'Leave Cancelled');
                        return redirect::back()->with('message', 'Leave Cancelled');
                    } else {
                        return redirect::back()->withErrors(['Something went wrong.'])->withInput();
                    }
                }
            }
        } else {
            abort(404);
        }
    }
    public function getLeave($id)
    {
        $leave = Leave::find($id);
        if ($leave && $leave->user_id==Auth::id()) {
            return response()->json($leave);
        } else {
            return response()->json("You can't view this link", 400);
        }
    }
}
