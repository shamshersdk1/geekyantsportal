<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\FinancialYear;
use App\Models\Admin\ItSaving;
use App\Models\Month;
use App\Models\MonthSetting;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use App\Models\Admin\ItSavingOther;
use App\Models\Appraisal\AppraisalComponentType;
use App\Models\Transaction;
use App\Services\Payroll\AppraisalService;
use App\Models\Admin\VpfDeduction;
use App\Models\Admin\ItSavingMonth;
use App\Models\Admin\ItSavingMonthOther;
use App\Jobs\PrepareSalary\Regenerate\ItSavingMonthJob;
use App\Services\Payroll\ITSavingService;

class ItSavingController extends Controller
{
    public function index()
    {
        $authUser = \Auth::user();
        $financialYear = FinancialYear::orderBy('year', 'DESC')->get();
        // if ($authUser->role != "admin" || !$authUser->isAdmin()) {
        //     $obj = ItSaving::where('user_id', $authUser->id)->distinct()->pluck('financial_year_id')->toArray();
        //     $financialYear = FinancialYear::whereIn('id', $obj)->orderBy('id', 'DESC')->get();
        // } else {
        //     $financialYear = FinancialYear::orderBy('id', 'DESC')->get();
        // }

        return view('pages/it-saving/index', compact('financialYear'));
    }
    public function show($financial_year_id, $user_id)
    {
        $day_of_month = date('d');

        $month = date('m');
        
        $monthObj = Month::orderBy('year','DESC')->orderBy('month','DESC')->where('month', $month)->where('financial_year_id',$financial_year_id)->first();
        
        $monthId = $monthObj->id;

        $authUser = \Auth::user();
        if ($authUser->id != $user_id && $authUser->role != "admin") {
            return redirect('/admin/it-saving');
        }
        $financialYear = FinancialYear::find($financial_year_id);

        if ($day_of_month >= 21) {
            if (!($authUser->hasRole('account') || $authUser->role == 'admin')) {
                $financialYear->is_locked = 1;
            } else {
                $monthLock = MonthSetting::where('month_id', $monthId)->where('key', 'it-saving')->first();
                if ($monthLock && $monthLock->value == 'locked') {
                    $financialYear->is_locked = 1;
                }
            }
        }

        if (!$financialYear) {
            return redirect()->back()->withErrors("No Record");
        }
        if ($financialYear->is_locked == 1) {
            $isLocked = true;
        } else {
            $isLocked = false;
        }
        $template = '2019-2020';
        if ($financialYear) {
            $template = trim($financialYear->it_saving_template);
        }
        //{"0": {"Ttile":"2321312"}, "1": {"Ttile":"2321312"}}

        $user = User::find($user_id);
        $ItSavingObj = ItSaving::where('financial_year_id', $financialYear->id)->where('user_id', $user_id)->first();
        if ($ItSavingObj) {
            // if ($ItSavingObj->previous_form_16_12b) {
            //     $ItSavingObj->previous_form_16_12b = true;
            // } else {
            //     $ItSavingObj->previous_form_16_12b = false;
            // }

            //$ItSavingObj->previous_form_16_12b = true;
            $data = ItSavingOther::where('it_savings_id',$ItSavingObj->id)->where('type','other_multiple_investments')->pluck('value','title')->toArray();
            if(count($data)>0)
            {
                $array['name']=[];
                $array['amount']=[];
                foreach($data as $key => $value){
                    $array['name'][]=$key;
                $array['amount'][]=$value;
                }
                $ItSavingObj->other_multiple_investments = $array;
            }

            $data = ItSavingOther::where('it_savings_id',$ItSavingObj->id)->where('type','other_multiple_deductions')->pluck('value','title')->toArray();
            if(count($data)>0)
            {
                $array['name']=[];
                $array['amount']=[];
                foreach($data as $key => $value){
                    $array['name'][]=$key;
                $array['amount'][]=$value;
                }
                $ItSavingObj->other_multiple_deductions = $array;
            }
        }

        $monthObj = Month::where('status', 'in_progress')->where('financial_year_id', $financial_year_id)->orderBy('year','ASC')->orderBy('month','ASC')->first();
        $pfEmployee = AppraisalComponentType::getIdByKey('pf-employee');
        $pfEmployeeAmount = 0;
        $vpfAmount = 0;
        $pfEmployeeAmount = Transaction::getPaidTillDate($user_id, $financial_year_id, 'App\Models\Appraisal\AppraisalComponentType', $pfEmployee);
        $vpfAmount = Transaction::getPaidTillDate($user_id, $financial_year_id, 'App\Models\Admin\VpfDeduction');
       if($monthObj->status == "in_progress"){
            $appraisalList = AppraisalService::getCurrentMonthAppraisal($user_id, $monthObj->id);
            if (!$appraisalList == null || isset($appraisalList[0])) {
                $currData = AppraisalService::generateCurrentData($appraisalList, $monthObj->id, $user_id);
                $pfEmployeeAmount += $currData[$pfEmployee];
            }
            $vpfAmount += VpfDeduction::getCurrMonthVpf($monthObj->id, $user_id);
        }
        

        return view('pages/it-saving/template/' . $template, compact('vpfAmount','pfEmployeeAmount','financialYear', 'ItSavingObj', 'user', 'isLocked'));
    }
    public function store(Request $request)
    {
        $data = $request->all();
        if (!empty($data)) {
            if(isset($data['status']))
                $data['status'] = 'pending';
            else
                $data['status'] = 'completed';
            $obj = new ItSaving;
            $response = ItSaving::saveData($data, $obj);
            if (isset($response['status']) && $response['status']) {
                return redirect('/admin/it-saving')->with('message', "Saved Successfully!");
            } else {
                return redirect()->back()->withErrors($response['message'])->withInput();
            }
        } else {
            return redirect()->back()->withErrors('Empty data sent!')->withInput();
        }
    }
    public function update($id, Request $request)
    {
        $data = $request->all();
        if (!empty($data)) {
            if(isset($data['status']))
                $data['status'] = 'pending';
            else
                $data['status'] = 'completed';
            $obj = ItSaving::find($id);
            $response = ItSaving::saveData($data, $obj);
            if (isset($response['status']) && $response['status']) {
                return redirect()->back()->with('message', "Updated Successfully!");
            } else {
                return redirect()->back()->withErrors($response['message'])->withInput();
            }
        } else {
            return redirect()->back()->withErrors('Empty data sent!')->withInput();
        }

    }
    public function userList($financial_year_id)
    {
        $financialYear = FinancialYear::find($financial_year_id);
        $itSavings = ItSaving::where('financial_year_id', $financial_year_id)->get();
        return view('pages/it-saving/admin-view', compact('financialYear', 'itSavings'));
    }

    public function monthUserList($month=null)
    {
        $months = Month::orderBy('year','DESC')->orderBy('month','DESC')->get();
        if(!count($months)  > 0)
        {
            return redirect()->back()->withErrors('No Month Found!')->withInput();
        }
        if(!$month)
        {
             $month = $months[0]->id;
        }
        $monthObj = Month::find($month);
        $financial_year_id = $monthObj->financial_year_id;
        $ItSavingObjMonths = ItSavingMonth::where("month_id",$month)->orderBy('updated_at','DESC')->distinct('user_id')->get();
        $ItSavingMonthObj = new ItSavingMonth;
        $columns=$ItSavingMonthObj->getTableColumns();
        return view('pages/it-saving/month-view', compact('ItSavingObjMonths','columns','monthObj','months', 'ItSavingObj'));
    }

    public function regenerate($monthId)
    {
        $monthObj = Month::find($monthId);
        $savings = ItSaving::where('financial_year_id', $monthObj->financial_year_id)->get();
        if($monthObj->vpfSetting?$monthObj->vpfSetting->value != "locked":true)
        {
            return redirect()->back()->withErrors('Vpf deduction Month not locked');
        }
        ItSavingMonth::where('financial_year_id', $monthObj->financial_year_id)->where('month_id', $monthObj->id)->delete();
        foreach ($savings as $saving) {
            dispatch(new ItSavingMonthJob($monthId,$saving->id));
        }
        return redirect('/admin/it-saving/month/'.$monthId);
    }

    public function lockMonth($monthId){
        if (!\Auth::User()->isAdmin()) {
            return redirect()->back()->withErrors('Unauthorized Access');
        }
        $response = MonthSetting::monthStatusToggle($monthId,'it-saving-month');
        if(!empty($response['status']) && $response['status'] == false)
            return redirect()->back()->withErrors('Something went wrong!');          
        return redirect()->back()->withMessage($response['message']);
    }

    public function create($financialYearId,$userId = null)
    {
        $userList = User::where('is_active',1)->get();
        if($userId)
        {
            $user = User::find($userId);
        }
        $day_of_month = date('d');
        $month = date('m');
        $financialYear = FinancialYear::find($financialYearId);
        if (!$financialYear) {
            return redirect()->back()->withErrors("No Record");
        }
        $template = '2019-2020';
        if ($financialYear) {
            $template = trim($financialYear->it_saving_template);
        }
        $copyButton = true;
        $isCreate = true;
        return view('pages/it-saving/template/' . $template, compact('isCreate','userList','viewStatus','copyButton','vpfAmount','pfEmployeeAmount','financialYear', 'ItSavingObj', 'user', 'isLocked'));
    }

    public function copyFromPrevious($financialYearId,$id)
    {
        $userList = User::where('is_active',1)->get();
        $month = date('m');
        $financialYear = FinancialYear::find($financialYearId);
        $ItSavingObj = ItSaving::where('user_id', $id)->orderBy('financial_year_id')->first();
        if(!$ItSavingObj){
            return redirect()->back()->withErrors("No Previous Record");
        }
        $template = trim($financialYear->it_saving_template);
        $user = User::find($ItSavingObj->user_id);
        if ($ItSavingObj) {
            $otherData = ITSavingService::getItSavingOtherData($ItSavingObj->id);
            $ItSavingObj->other_multiple_investments = $otherData['other_multiple_investments'];
            $ItSavingObj->other_multiple_deductions = $otherData['other_multiple_deductions'];
        }

        $data = ITSavingService::getItSavingTransactionData($ItSavingObj->id);
        $vpfAmount = $data['vpfAmount'];
        $pfEmployeeAmount = $data['pfEmployeeAmount'];
        $isCreate = true;
        return view('pages/it-saving/template/' . $template, compact('isCreate','userList','vpfAmount','pfEmployeeAmount','financialYear', 'ItSavingObj', 'user', 'isLocked'));
    }
}
