<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\BillingScheduleReminder;
use App\Models\Admin\ProjectResource;
use Illuminate\Http\Request;
use Redirect;

class BillingScheduleReminderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $billingScheduleObj = BillingScheduleReminder::find($id);
        if(!$billingScheduleObj)
            return Redirect::back()->withInput()->withErrors(['Invalid invoice id'.$id]);
        $projectId = $billingScheduleObj->project_id;
        $invoiceDate = $billingScheduleObj->invoice_date;

        $resources = ProjectResource::with('resource','resource_type')
                                        ->where('project_id',$projectId)
                                        ->whereDate('start_date','<=', $invoiceDate)
                                        ->whereDate('end_date','>=', $invoiceDate)
                                        ->orWhere(function ($query) use($projectId, $invoiceDate) {
                                            $query->where('project_id',$projectId)
                                            ->whereDate('start_date','<=', $invoiceDate)
                                            ->whereNull('end_date');
                                        })
                                        ->get();
        return view('pages/admin/billing-schedule/show',  compact('billingScheduleObj','resources'));
    }

    /**
     * Show the form for editing the specified resource.
     *, $start_date
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $billingScheduleReminder = BillingScheduleReminder::where('id', $request['billing_id'])->delete();
        if ($billingScheduleReminder) {
            return redirect('/admin/project/' . $id . '/dashboard')->with('message', 'Deleted Successfully!');

        } else {
            return redirect('/admin/project/' . $id . '/dashboard')->withErrors(['Uable to delete record!']);
        }
    }
}
