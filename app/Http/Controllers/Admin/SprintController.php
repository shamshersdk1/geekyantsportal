<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Input;
use Validator;
use Redirect;
use App\Models\User;
use App\Models\Admin\Timelog;
use App\Models\Admin\Project;
use App\Models\Admin\ProjectSprints;
use App\Models\Admin\SprintResource;
use Auth;


class SprintController extends Controller
{
    public function index() {
        
    	$userId = Auth::user()->id;
    	$consumeHours = [];
        $allocatedHours = [];
        $additionalHours = [];


    	$projectObj = Project::with('projectSprints.sprintResource.user','timelog')->get();
        $allProjects = Project::all();
       //  echo "<pre>";
       // print_r($projectObj->toArray());
       // die;
        foreach ($projectObj as $project) {
           $sprints = $project->projectSprints;
           foreach ($sprints as $sprint) {
              $resource = $sprint->sprintResource;
               foreach ($resource as $hours) {
                    if(!isset($allocatedHours[$sprint->id]))
                            $allocatedHours[$sprint->id]= 0;
                    $allocatedHours[$sprint->id] = ($allocatedHours[$sprint->id] + $hours->hours);  
               }
            }
        }
       
        foreach ($projectObj as $key => $projects) {

            foreach ($projects->timelog as $key => $timelog) {
                $date = $timelog->current_date;
                if($timelog->is_pre_Allocated){ 
                    foreach ($projects->projectSprints as $key => $sprint) {
                        if(!isset($consumeHours[$sprint->id]))
                            $consumeHours[$sprint->id]= 0;
                            if($sprint->start_date <= $date && $sprint->end_date >= $date){
                                $minutesToHours =($timelog->minutes)/60;
                                $consumeHours[$sprint->id] =$consumeHours[$sprint->id] + $minutesToHours ;
                            }

                        // $consumeHours[$sprint->id] = $consumeHours[$sprint->id] / 60;                            
                    }
                }      
            }
        }

        foreach ($projectObj as $key => $projects) {

            foreach ($projects->timelog as $key => $timelog) {
                $date = $timelog->current_date;
                if(!$timelog->is_pre_Allocated){ 
                    foreach ($projects->projectSprints as $key => $sprint) {
                        if(!isset($additionalHours[$sprint->id]))
                            $additionalHours[$sprint->id]= 0;
                            if($sprint->start_date <= $date && $sprint->end_date >= $date){
                                $minutesToHours =($timelog->minutes)/60;
                                $additionalHours[$sprint->id] =  $additionalHours[$sprint->id] + $minutesToHours ;
                            }
                        
                        //$additionalHours[$sprint->id] = $additionalHours[$sprint->id] / 60;    
                    }

                }       
            
            }
        }

        
       
     //    if(isset($projectObj)){
     //    	foreach ($projectObj as $key => $projectDetails) {
     //            if(isset($projectDetails->timelog)){
     //        		foreach ($projectDetails->timelog as $key => $timelog) {

     //        			if(!isset($consumeHours[$timelog->project_id]))
     //        				$consumeHours[$timelog->project_id]= 0;
            			
     //        			$consumeHours[$timelog->project_id] = $consumeHours[$timelog->project_id] + $timelog->minutes;
                        
     //        		}

     //            }    
     //    	}
    	// }


      
        // echo "<pre>";
        // print_r($allProjects->toArray());
        // die;

    	return View ('pages/admin/sprint/dashboard',['projects' => $allProjects,'projectObj' => $projectObj,'consumeHours'=> $consumeHours,'allocatedHours'=> $allocatedHours,'additionalHours' => $additionalHours]);
    }
    
    public function show()
    {
        
        $id = $_GET['projectId'];
        if($id == null)
            return redirect('/admin/project/sprint-dashboard');
        
        $userId = Auth::user()->id;
        $consumeHours = [];
        $allocatedHours = [];
        $additionalHours = [];


        $projectObj = Project::with('projectSprints.sprintResource.user','timelog')->where('id',$id)->get();
        $allProjects = Project::all();
       //  echo "<pre>";
       // print_r($projectObj->toArray());
       // die;
        foreach ($projectObj as $project) {
           $sprints = $project->projectSprints;
           foreach ($sprints as $sprint) {
              $resource = $sprint->sprintResource;
               foreach ($resource as $hours) {
                    if(!isset($allocatedHours[$sprint->id]))
                            $allocatedHours[$sprint->id]= 0;
                    $allocatedHours[$sprint->id] = ($allocatedHours[$sprint->id] + $hours->hours);  
               }
            }
        }
       
        foreach ($projectObj as $key => $projects) {

            foreach ($projects->timelog as $key => $timelog) {
                $date = $timelog->current_date;
                if($timelog->is_pre_Allocated){ 
                    foreach ($projects->projectSprints as $key => $sprint) {
                        if(!isset($consumeHours[$sprint->id]))
                            $consumeHours[$sprint->id]= 0;
                            if($sprint->start_date <= $date && $sprint->end_date >= $date){
                                $minutesToHours =($timelog->minutes)/60;
                                $consumeHours[$sprint->id] =$consumeHours[$sprint->id] + $minutesToHours ;
                            }

                        // $consumeHours[$sprint->id] = $consumeHours[$sprint->id] / 60;                            
                    }
                }      
            }
        }

        foreach ($projectObj as $key => $projects) {

            foreach ($projects->timelog as $key => $timelog) {
                $date = $timelog->current_date;
                if(!$timelog->is_pre_Allocated){ 
                    foreach ($projects->projectSprints as $key => $sprint) {
                        if(!isset($additionalHours[$sprint->id]))
                            $additionalHours[$sprint->id]= 0;
                            if($sprint->start_date <= $date && $sprint->end_date >= $date){
                                $minutesToHours =($timelog->minutes)/60;
                                $additionalHours[$sprint->id] =  $additionalHours[$sprint->id] + $minutesToHours ;
                            }
                        
                        //$additionalHours[$sprint->id] = $additionalHours[$sprint->id] / 60;    
                    }

                }       
            
            }
        }

        
     
        return View ('pages/admin/sprint/dashboard',['projects' => $allProjects,'projectObj' => $projectObj,'consumeHours'=> $consumeHours,'allocatedHours'=> $allocatedHours,'additionalHours' => $additionalHours]);
    }
    // public function showSpintResource()
    // {
    // //   echo "showresource<pre>";
    // //   $projectObj = Project::with('projectSprints.sprintResource.user')->get();
    // //   $temp = [];
    // //   $allocatedUser = [];
    // //   //print_r($projectObj->toArray());
    // //   foreach ($projectObj as $projectDetails) {
    // //     foreach ($projectDetails->projectSprints as $sprints) {
    // //       // print_r($projectDetails->toArray());
    // //         if(count($sprints->sprintResource)>0){
    // //             foreach ($sprints->sprintResource as $key => $user) {

                    

    // //                 // $user[$user->employee_id] = $user->user->name;
    // //                 // $project[$user->employee_id] = $projectDetails->project_name;
    // //                 // echo $project[$user->employee_id];
    // //                 // $userDetails = User::find($user->id);
    // //                 $temp['userName'] = $user->user->name;
    // //                 $temp['userEmail'] = $user->user->email;
    // //                 $temp['projectName'] = $projectDetails->project_name;
    // //                 $temp['start_date'] = $sprints->start_date;
    // //                 $temp['end_date'] = $sprints->end_date;
    // //                 //print_r($temp);
    // //                 $allocatedUser[] = $temp;
    // //             }
    // //         }
    // //     }
    // //     # code...
    // // }

    
    // //     print_r($allocatedUser);
    // //     $userDetails = User::all();
    // //     print_r($userDetails->toArray());

    // //     foreach ($userDetails as $user) {
            
    // //     }
      

    // }

    // public function destroy($id)
    // {
    //     echo "delete funtin ";
    //     echo $id;
    //     $sprint = ProjectSprints::find($id);
    //     if($sprint->delete())
    //         echo "deled";
    //     else
    //         echo $sprint->error();

    // }
}
