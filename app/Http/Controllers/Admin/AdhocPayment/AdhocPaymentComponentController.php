<?php

namespace App\Http\Controllers\Admin\AdhocPayment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Audited\AdhocPayments\AdhocPaymentComponent;

class AdhocPaymentComponentController extends Controller
{
    public function index()
    {
        $adhocPaymentComponents = AdhocPaymentComponent::all();
        if (!$adhocPaymentComponents) {
            return view('pages.admin.adhoc-payments.adhoc-payment-component.index')->with('message', 'no records found.');
        }
        return view('pages.admin.adhoc-payments.adhoc-payment-component.index', compact('adhocPaymentComponents'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $adhocPaymentComponentObj = AdhocPaymentComponent::create(['key' => $data['key'],'description' => $data['description'],'type' =>  $data['type']]);
        if(!$adhocPaymentComponentObj->isValid())
        {
            return redirect()->back()->withErrors($adhocPaymentComponentObj->getErrors() )->withInput();
        }
        return redirect()->back()->with('message', 'Saved Successfully');
    }

    public function show($id)
    {
        $adhocPaymentComponentObj = AdhocPaymentComponent::find($id);
        if (!$adhocPaymentComponentObj) {
            return redirect('/admin/adhoc-payment-component')->withErrors('Appraisal component type not found!')->withInput();
        }
        return view('pages.admin.adhoc-payments.adhoc-payment-component.show', compact('adhocPaymentComponentObj'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $adhocPaymentComponentObj = AdhocPaymentComponent::updateOrCreate(['id' => $id],['key' => $data['key'],'description' => $data['description'],'type' => $data['type']]);
        if(!$adhocPaymentComponentObj->isValid())
        {
            return redirect()->back()->withErrors($adhocPaymentComponentObj->getErrors() )->withInput();
        }
        return redirect('/admin/adhoc-payment-component')->with('message', 'Updated!');   
    }

    public function destroy($id)
    {
        $adhocPaymentComponentObj = AdhocPaymentComponent::find($id);
        if ($adhocPaymentComponentObj && $adhocPaymentComponentObj->delete()) {
            return redirect('/admin/adhoc-payment-component')->with('message', 'Successfully Deleted');
        }
        return redirect()->back()->withErrors("Record is been used.")->withInput();
    }
}
