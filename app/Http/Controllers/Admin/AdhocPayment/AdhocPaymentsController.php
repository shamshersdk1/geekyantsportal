<?php

namespace App\Http\Controllers\Admin\AdhocPayment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Audited\AdhocPayments\AdhocPaymentComponent;
use App\Models\Audited\AdhocPayments\AdhocPayment;
use App\Models\User;
use App\Models\Month;
use Redirect;
use App\Models\MonthSetting;

class AdhocPaymentsController extends Controller
{

    public function index()
    {
        $months = Month::orderBy('year', 'DESC')->orderBy('month', 'DESC')->get();
        if (!count($months)>0) {
            return Redirect::back()->withInput()->withErrors(['no month found']);
        }
        return view('pages.admin.adhoc-payments.adhoc-payments.index', compact('months'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $adhocComponent = AdhocPaymentComponent::find($data['adhoc_payment_component_id']);
        $adhocPaymentObj = AdhocPayment::create(['adhoc_payment_component_id' => $data['adhoc_payment_component_id'],'month_id' => $data['month_id'],'user_id' =>  $data['user_id'],'amount' =>  $adhocComponent->type=='credit' ? abs($data['amount']) : -abs($data['amount']) ,'comment' =>  $data['comment']]);
        if(!$adhocPaymentObj->isValid())
        {
            return redirect()->back()->withErrors($adhocPaymentObj->getErrors() )->withInput();
        }
        return redirect()->back()->with('message', 'Saved Successfully');
    }

    public function show($id)
    {
        $adhocPayments = AdhocPayment::where('month_id',$id)->get();
        $adhocPaymentsComponents = AdhocPaymentComponent::all();
        $users = User::where('is_active',1)->get();
        $month = Month::find($id);
        if (!$adhocPaymentsComponents) {
            return view('pages.admin.adhoc-payments.adhoc-payments.index')->with('message', 'no records found.');
        }
        return view('pages.admin.adhoc-payments.adhoc-payments.show', compact('month','adhocPaymentsComponents','users','adhocPayments'));
    }

    public function edit($id)
    {
        $adhocPaymentObj = AdhocPayment::find($id);
        if (!$adhocPaymentObj) {
            return redirect()->back()->withErrors("Not Found")->withInput();
        }
        $adhocComponents = AdhocPaymentComponent::all();
        if (!count($adhocComponents) > 0) {
            return redirect()->back()->withErrors("Adhoc Component Not Found")->withInput();
        }
        $users = User::where('is_active',1)->get();
        return view('pages.admin.adhoc-payments.adhoc-payments.edit', compact('users','adhocComponents','adhocPaymentObj'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $adhocComponent = AdhocPaymentComponent::find($data['adhoc_payment_component_id']);
        $adhocPaymentObj = AdhocPayment::updateOrCreate(['id' => $id],['adhoc_payment_component_id' => $data['adhoc_payment_component_id'],'user_id' =>  $data['user_id'],'amount' =>  $adhocComponent->type=='credit' ? abs($data['amount']) : -abs($data['amount']) ,'comment' =>  $data['comment']]);
        if(!$adhocPaymentObj->isValid())
        {
            return redirect()->back()->withErrors($adhocPaymentObj->getErrors() )->withInput();
        }
        return redirect('/admin/adhoc-payments/'.$adhocPaymentObj->month_id)->with('message', 'Updated!'); 
    }

    public function destroy($id)
    {
        $adhocPaymentObj = AdhocPayment::find($id);
        if($adhocPaymentObj && $adhocPaymentObj->delete())
        {
            return Redirect::back()->withMessage("Record of ".$adhocPaymentObj->user->name." has been deleted.");
        }
        return Redirect::back()->withErrors(["Entry not found!"]);
    }

    public function lockMonth($monthId){
        if (!\Auth::User()->isAdmin()) {
            return Redirect::back()->withErrors('Unauthorized Access');
        }
        $response = MonthSetting::monthStatusToggle($monthId,'adhoc-payment');
        if(!empty($response['status']) && $response['status'] == false)
            return Redirect::back()->withErrors('Something went wrong!');          
        return Redirect::back()->withMessage($response['message']);
    }
}
