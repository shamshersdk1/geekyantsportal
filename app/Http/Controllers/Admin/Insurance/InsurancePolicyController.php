<?php

namespace App\Http\Controllers\Admin\Insurance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Admin\Insurance\InsurancePolicy;
use App\Models\Admin\Insurance\Insurance;

use Auth;
use Log;
use Redirect;
use DB;

class InsurancePolicyController extends Controller {


	/**
     * Display a listing of the primary insurance policy (show record with parent_id NULL)
     * Sub group can be added within the primary insurnace
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        return view('pages/admin/insurance/policy/index');
    }
    public function show($id) {
        $policy =  InsurancePolicy::with('insurances.insurable')->find($id);

        if(!$policy || $policy->parent_id != NULL) {
            return redirect('/admin/insurance-policy')->withErrors(['Invalid Id #'.$id]);
            //return Redirect::back()->withInput()->withErrors(['Invalid Id #'.$id]);
        }
        return view('pages/admin/insurance/policy/show', compact('policy'));
    }

    public function userReport(){
        $sql = "SELECT * FROM `users` WHERE `is_active` = 1 and `id` NOT IN ( SELECT `insurable_id` FROM `insurances` as i, `insurance_policies` as ip WHERE `insurable_type` = 'App\\\Models\\\User' and ip.id = i.insurance_policy_id and ip.status = 'approved' and ip.status != 'pending')";
        $sql2 = "SELECT * FROM `users` WHERE `is_active` = 0 and id IN ( SELECT `insurable_id` FROM `insurances` as i, `insurance_policies` as ip WHERE `insurable_type` = 'App\\\Models\\\User' and ip.id = i.insurance_policy_id and ip.status = 'approved')";
        $ActiveUsers = DB::select($sql);
        $unActiveUsers = DB::select($sql2);
        return view('pages/admin/insurance/user-report', compact('ActiveUsers','unActiveUsers'));
    }
}