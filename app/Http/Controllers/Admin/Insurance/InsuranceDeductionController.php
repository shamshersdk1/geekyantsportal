<?php

namespace App\Http\Controllers\Admin\Insurance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Month;
use App\Models\MonthSetting;
use App\Models\Admin\Insurance\InsuranceDeduction;
use Auth;
use Log;
use Redirect;

class InsuranceDeductionController extends Controller {
	/**
     * Display a listing of the employee insurance deduction monthly.
     *
     * @return \Illuminate\Http\Response
     */
    public function monthList(){   
        // Month List
        $months = Month::getMonthList();
        return view('pages/admin/insurance/deduction/months',compact('months'));
    }
    public function monthlyDeduction($monthId) {
        $insuranceDeductions = InsuranceDeduction::where('month_id',$monthId)->orderBy('user_id','desc')->get();
        $month = Month::find($monthId);
        return view('pages/admin/insurance/deduction/show', compact('month','insuranceDeductions'));
    }

    public function monthStatusDeduction($monthId){
        if (!\Auth::User()->isAdmin()) {
            return Redirect::back()->withErrors('Unauthorized Access');
        }
        $response = MonthSetting::monthStatusToggle($monthId,'insurance-deduction');
        if(!empty($response['status']) && $response['status'] == false)
            return Redirect::back()->withErrors('Something went wrong!');          
        return Redirect::back()->withMessage($response['message']);
    }

    public function regenerate($month_id){
        $response = InsuranceDeduction::regenerateInsurance($month_id);
        if(!empty($response['status']) && $response['status'] == false)
            return redirect('/admin/insurance-deduction/'.$month_id)->withErrors('Something went wrong!');          
        return redirect('/admin/insurance-deduction/'.$month_id)->withMessage($response['message']);
    }

    public function deleteRecord($id){
        $insuranceObj = InsuranceDeduction::find($id);
        if($insuranceObj && $insuranceObj->delete())
        {
            return Redirect::back()->withErrors("Record of ".$insuranceObj->user->name." has been deleted.");
        }
        return Redirect::back()->withErrors(["Entry not found!"]);
    }
}