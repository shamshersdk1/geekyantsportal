<?php

namespace App\Http\Controllers\Admin\Insurance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Admin\Insurance\InsurancePolicy;
use App\Models\Admin\Insurance\Insurance;

use Auth;
use Log;
use Redirect;

class InsuranceController extends Controller {


	/**
     * Display a listing of the employee insurance.
     *
     * @return \Illuminate\Http\Response
     */
        public function index()
        {   
            return view('pages/admin/insurance/policy/index');
        }
        public function show($id) {
            $policy =  InsurancePolicy::with('insurances.insurable')->find($id);

            if(!$policy || $policy->parent_id != NULL) {
                return redirect('/admin/insurance-policy')->withErrors(['Invalid Id #'.$id]);
                //return Redirect::back()->withInput()->withErrors(['Invalid Id #'.$id]);
            }
            return view('pages/admin/insurance/policy/show', compact('policy'));
        }
        public function userInsurance() {
            $insurances = Insurance::orderBy('id','DESC')->get();
            
            return view('pages/admin/insurance/user-insurance/index', compact('insurances'));
        }
        public function list() {   
            return view('pages/admin/insurance/insurance',
                [
                    'insuranceData' => [
                        [
                            'name' => 'Basic',
                            'type' => '',
                            'number' => 'GA/2018-19/PP/001',
                            'effective_date' => '20/08/2018',
                            'monthly_deduction' => '350',
                            'coverage_amount' => '100000',
                            'assigned_user' => '10',
                            'url' => 'https://drive.google.com/file/d/1oQWIZLokMObx_vzV8zFCOV5AxMbwj5fH/view',
                            'active' => true
                        ],
                        [
                            'name' => 'Advance',
                            'type' => '',
                            'number' => 'GA/2018-19/PP/001',
                            'effective_date' => '20/08/2018',
                            'monthly_deduction' => '350',
                            'coverage_amount' => '100000',
                            'assigned_user' => '10',
                            'url' => 'https://drive.google.com/file/d/1oQWIZLokMObx_vzV8zFCOV5AxMbwj5fH/view',
                            'active' => true
                        ]
                    ]
                ]
            );
        }


        public function add() {   
            return view('pages/admin/insurance/add-insurance');
        }

 
}