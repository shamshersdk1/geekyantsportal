<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Admin\Incident;
use App\Models\Admin\IncidentUserAccess;
use App\Models\Admin\Department;
use App\Models\User;

use App\Services\IncidentService;

use Redirect;
use Auth;
use Validator;

class IncidentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
    
        if ( $user->role == 'admin') 
        {
            $incidents = Incident::paginate(1000);
        } 
        else 
        {
            $incidentDepartment = [];
            $array1 = $user->headOfDepartments->pluck('id')->toArray();
            $array2 = $user->firstContactOfDepartments->pluck('id')->toArray();
            $incident_ids = IncidentUserAccess::where('user_id',$user->id)->pluck('id')->toArray();
            if ( !empty($array1) && !empty($array2) )
            {
                $incidentDepartment =  array_merge( $array1, $array2) ;
            } elseif ( !empty($array1) && empty($array2) ) {
                $incidentDepartment = $array1;
            } elseif ( empty($array1) && !empty($array2) ) {
                $incidentDepartment = $array2;
            }
            $incidents = Incident::whereIn('department_id', $incidentDepartment)->orWhere('assigned_to',$user->id)
                                    ->orWhere('raised_by',$user->id)->orWhereIn('id',$incident_ids)->paginate(1000);
        }
        
        return View('pages/admin/incident/index', ['incidents' => $incidents] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = Department::all();
        return View('pages/admin/incident/add',['departments' => $departments]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'subject'  => 'required',
			'department_id'  => 'required'
        ]);
        if ($validator->fails()) {
            return redirect::back()->withErrors($validator->errors())->withInput($request->input());
        }
        $incidentObj = new Incident();
        $incidentObj->subject = $request->subject;
        $incidentObj->department_id = $request->department_id;
        $incidentObj->raised_by = Auth::user()->id;
        $incidentObj->status = 'open';
    
        if ( $incidentObj->save() )
        {
            IncidentService::defaultAssignUser($incidentObj->id);
            return redirect('/admin/incident/'.$incidentObj->id)->with('message', 'Successfully added incident');
        }
        else
        {
            return Redirect::back()->withInput()->withErrors($incidentObj->getErrors());
        }

    }
    /**
     * Show details of the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();
        $incident = Incident::find($id);
        $read_only = true;
        $access = IncidentService::checkUserAccess($incident->id, $user->id);
        if( $access['show_incident_flag'] )
        {
            $read_only = $access['read_only_flag'];
            return View('pages/admin/incident/show',compact('incident','read_only','user') );
        } else {
            return redirect('/admin/incident')->withErrors('Invaild Access');
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $incidentObj = Incident::find($id);
        if ( $incidentObj->delete() )
        {
            return redirect('/admin/incident')->with('message','Successfully Deleted');
        }
        else
        {
            return redirect::back()->withErrors('Error while deleting the record');
        }
    }
    
}
