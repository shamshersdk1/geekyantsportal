<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\TechTalkVideo;
use Redirect;


class TechTalkVideoController extends Controller
{
    
    public function index()
    {
        $techTalkVideos = TechTalkVideo::paginate(20);
        return View('pages/admin/tech-talk-videos/index', ['techTalkVideos' => $techTalkVideos ] );
    }

    
    public function create()
    {
        return View('pages/admin/tech-talk-videos/add');
    }

    public function store(Request $request)
    {

        if( empty($request->url) || empty($request->title) )
        {
            return Redirect::back()->withInput()->withErrors(['Missing inputs']);
        }

        $techTalkVideoObj = new TechTalkVideo();
        $techTalkVideoObj->url = $request->url;
        $techTalkVideoObj->title = $request->title;
        $techTalkVideoObj->status = $request->status;
        $techTalkVideoObj->description = $request->description;
        if($techTalkVideoObj->save()){
            return redirect('/admin/tech-talk-videos')->with('message', 'Successfully Added');
        }
        else {
            return redirect::back()->withErrors($techTalkVideoObj->getErrors())->withInput();
        }

    }

    public function show($id)
    {
        $techTalkVideoObj = TechTalkVideo::find($id);
        
        if ( $techTalkVideoObj )
        {
            return View('pages/admin/tech-talk-videos/show', ['techTalkVideoObj' => $techTalkVideoObj] );
        }
        return Redirect::back()->withInput()->withErrors(['No record found']);
    }
    
    public function edit($id)
    {
        $techTalkVideoObj = TechTalkVideo::find($id);
        if($techTalkVideoObj){

            return View('pages/admin/tech-talk-videos/edit', ['techTalkVideoObj' => $techTalkVideoObj]);
        }
        else {
            return Redirect::back()->withInput()->withErrors(['No record found']);
        }
    }

    
    public function update(Request $request, $id)
    {

        if( empty($request->url) || empty($request->title) )
        {
            return Redirect::back()->withInput()->withErrors(['Missing inputs']);
        }

        $techTalkVideoObj = TechTalkVideo::find($id);
        $techTalkVideoObj->url = $request->url;
        $techTalkVideoObj->title = $request->title;
        $techTalkVideoObj->status = $request->status;
        $techTalkVideoObj->description = $request->description ?: null;
        if($techTalkVideoObj->save()){
            return redirect('/admin/tech-talk-videos')->with('message', 'Successfully Added');
        }
        else {
            return redirect::back()->withErrors($techTalkVideoObj->getErrors())->withInput();
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $techTalkVideoObj = TechTalkVideo::find($id);
            if($techTalkVideoObj->delete()){
                return redirect('/admin/tech-talk-videos')->with('message','Successfully Deleted');
            }
            elseif(!$techTalkVideoObj->delete()){
                return redirect::back()->withErrors($techTalkVideoObj->getErrors());
            }
        else {
            abort(404);
        }
    }

}
