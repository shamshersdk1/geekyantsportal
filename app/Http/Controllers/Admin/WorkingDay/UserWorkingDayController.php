<?php

namespace App\Http\Controllers\Admin\WorkingDay;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Month;
use App\Models\User;
use App\Models\Audited\WorkingDay\UserWorkingDay;
use Redirect;
use App\Services\CalendarService;
use App\Models\MonthSetting;
use App\Services\NewLeaveService;
use App\Services\Leave\UserLeaveService;


class UserWorkingDayController extends Controller
{
    public function index()
    {
        $months = Month::orderBy('year', 'DESC')->orderBy('month', 'DESC')->get();
        if (!count($months)>0) {
            return Redirect::back()->withInput()->withErrors(['no month found']);
        }
        return view('pages.admin.user-working-day.index', compact('months'));
    }

    public function show($id)
    {
        $month = Month::find($id);
        if (!count($month)>0) {
            return Redirect::back()->withInput()->withErrors(['month not found']);
        }

        $users=User::where('is_active',1)->orderBy("employee_id")->get();
        if (!count($users)>0) {
            return Redirect::back()->withInput()->withErrors(['No User found']);
        }
        $workingDayObjs = UserWorkingDay::where('month_id',$id)->get();
        return view('pages.admin.user-working-day.show', compact('month','users','workingDayObjs'));
    }

    public function update(Request $request,$monthId){
        $status = false;
        $workingDays = $request->working_days;
        $lops = $request->lops;
        $errors = "";
        if(!empty($workingDays)){
            foreach($workingDays as $index=>$workingDay){  
            $workingDayObj = UserWorkingDay::find($index);
            $workingDayObj->working_days = $workingDays[$index];
            $workingDayObj->lop = $lops[$index];
            if(!$workingDayObj->save()){
                $status = true;
                $errors = $workingDayObj->getErrors();
            }
         }
        }
        if($status){
            return Redirect::back()->withErrors($errors);    
        }   
        return redirect('/admin/user-working-day/'.$monthId);
    }

    public function lockMonth($monthId){
        if (!\Auth::User()->isAdmin()) {
            return Redirect::back()->withErrors('Unauthorized Access');
        }
        $response = MonthSetting::monthStatusToggle($monthId,'working-day');
        if(!empty($response['status']) && $response['status'] == false)
            return Redirect::back()->withErrors('Something went wrong!');          
        return Redirect::back()->withMessage($response['message']);
    }

    public function regenerate($monthId)
    {
        $month = Month::find($monthId);
        if(!$month) {
            $response['message'] = "Invalid month id ".$monthId;
            return $response;
        }
        if($month->monthlyVariableBonusSetting == 'locked') {
            $response['message'] = "Cannot regenrate. Month is locked";
            return $response;
        }
        $users=User::where('is_active',1)->orderBy("employee_id")->get();
        if (!count($users)>0) {
            return Redirect::back()->withInput()->withErrors(['No User found']);
        }
        UserWorkingDay::where('month_id',$monthId)->delete();

        $startDate = $month->getFirstDay();
        $endDate = $month->getLastDay();

        foreach($users as $user){
            $userWorkingObj = UserWorkingDay::create(['user_id' => $user->id,'month_id' => $monthId,'working_days' => NewLeaveService::getUserWorkingDays($startDate,$endDate,$user->id) , 'lop'=> 0]);
            if(!$userWorkingObj->isValid())
            {
                return Redirect::back()->withInput()->withErrors($userWorkingObj->getErrors());
            }
            $userWorkingObj->save();
        }
        return redirect('/admin/user-working-day/'.$monthId)->withMessage("Regenerated");
    }
}
