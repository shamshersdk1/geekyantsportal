<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Admin\UserContact;
use App\Models\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Config;
use Exception;
use DB;
use Auth;
use Validator;
use Redirect;

class UserContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $userContactList = UserContact::with('user','profile')->orderBy('user_id','ASC')->get();
        $user = Auth::user();

        if( $user->hasRole('admin') || $user->hasRole('human-resources') )
        {
            return View('pages/admin/user-contacts/index',['userContactList' => $userContactList]);
        }
        else
        {
            return View('pages/admin/user-contacts/index-view',['userContactList' => $userContactList]);
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();
        $user = Auth::user();
        if( $user->hasRole('admin') || $user->hasRole('human-resources') )
            return View('pages/admin/user-contacts/add',['users' => $users]);
        else
            return redirect('/admin/user-contacts');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
			'mobile' => 'required',
			'emergency_contact_number' => 'required',
			'personal_email' => 'required',
			'office_email' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect::back()->withInput()->withErrors(['Please fill all the mandatory fields']);
        }
        $alreadyExists = UserContact::isAlreadyExist($request->user_id);
        if ( $alreadyExists ) {
            return redirect::back()->withInput()->withErrors(['User Contact already exists']);
        }

        $save = UserContact::saveData($request->all());
        if(!$save)
        {
            return redirect::back()->withInput()->withErrors(['Something went wrong']);
        }
        return redirect('admin/user-contacts')->with('message', 'User contact added successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        if( $user->hasRole('admin') || $user->hasRole('human-resources') )
        {
            $userContact = UserContact::with('user')->find($id);
            if( !empty($userContact) )
            {
                return View('pages/admin/user-contacts/edit', ['userContact' => $userContact] );
            }  
                return Redirect::back()->withErrors(['User contact not found']);
        }
        else
        {
            return redirect('/admin/user-contacts');
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
			'mobile' => 'required',
			'emergency_contact_number' => 'required',
			'personal_email' => 'required',
			'office_email' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect::back()->withInput()->withErrors(['Please fill all the mandatory fields']);
        }

        $update = UserContact::updateData($request->all());
        if(!$update)
        {
            return redirect::back()->withInput()->withErrors(['Something went wrong']);
        }
        return redirect('admin/user-contacts')->with('message', 'User contact updated successfully');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $userContact = UserContact::find($id);
		// echo json_encode($array);
		if($userContact){
			if($userContact->delete()){
                return redirect('/admin/user-contacts')->with('message','Successfully Deleted');
			}
			else{
				return redirect::back()->withInput()->withErrors(['Something went wrong while deleting']);
			}
		}
        else{
            return redirect::back()->withInput()->withErrors(['User contact details not found']);
        }
    }
}
