<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CalendarEvent;
use Validator;
use Redirect;
use Route;


class CalendarEventController extends Controller
{
    public function index()
    {
        $data = CalendarEvent::orderBy('id','DESC')->paginate(20);
        return view('pages/admin/calendar-events/index',compact('data'));
    }
    
}
