<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\User;
use App\Models\Admin\Vendor;
use App\Models\Admin\VendorContact;
use App\Models\Admin\VendorServices;
use App\Models\Admin\VendorServicesName;

use Redirect;
use Auth;
use Validator;

class VendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return View('pages/admin/vendor/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('pages/admin/vendor/add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'subject'  => 'required',
			'department_id'  => 'required'
        ]);
        if ($validator->fails()) {
            return redirect::back()->withErrors($validator->errors())->withInput($request->input());
        }
        $incidentObj = new Incident();
        $incidentObj->subject = $request->subject;
        $incidentObj->department_id = $request->department_id;
        $incidentObj->raised_by = Auth::user()->id;
        $incidentObj->status = 'open';
    
        if ( $incidentObj->save() )
        {
            IncidentService::defaultAssignUser($incidentObj->id);
            return redirect('/admin/incident/'.$incidentObj->id)->with('message', 'Successfully added incident');
        }
        else
        {
            return Redirect::back()->withInput()->withErrors($incidentObj->getErrors());
        }

    }
    /**
     * Show details of the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // 
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $incidentObj = Incident::find($id);
        if ( $incidentObj->delete() )
        {
            return redirect('/admin/incident')->with('message','Successfully Deleted');
        }
        else
        {
            return redirect::back()->withErrors('Error while deleting the record');
        }
    }
    
}
