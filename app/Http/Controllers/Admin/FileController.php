<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Admin\File;
use App\Models\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Redirect;
use Config;
use Exception;
use DB;
use Auth;

class FileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $file = File::find($id);
		if($file) {
			$file->delete();
            return redirect::back()->with('message', 'File deleted successfully');
		}
		else{
            return redirect::back()->withInput()->withErrors(['File could not be deleted']);
            abort(404);
        }
			
    }

    public function streamFile($file_id)
    {
        if(!(Auth::user()->hasRole("human-resources") || Auth::user()->hasRole("account") || Auth::user()->hasRole("management") || Auth::user()->hasRole("admin"))) {
            return Redirect::back()->withErrors(["You don't have the necessary permission to view this link"]);
        }
            
        $file = File::find($file_id);

        if (!empty($file)) {
            return response($file->fileContent->content, 200)->withHeaders(array(
                'Content-Description' => 'File Transfer',
                'Content-Type' => 'application/octet-stream',
                'Content-Disposition' => 'attachment; filename="' . $file->name . '"',
            ));
        }
    }
    
}
