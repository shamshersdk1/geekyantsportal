<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Admin\PayrollRuleForCsv;

use Validator;
use Redirect;

class PayrollRuleForCsvController extends Controller
{
    public function index()
    {
        $rules = PayrollRuleForCsv::get();
        return view('pages.admin.payroll.csv-rules.index', compact('rules'));
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
        'key' => 'required',
        'value' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $key = $request->key;
        $key = str_replace('_', '', $key);
        $key = preg_replace('/[0-9]+/', '', $key);
        if(!ctype_lower($key)) {
            return redirect()->back()->withErrors(['Key should be all lowercase saperated by "_" (underscore)']);
        }
        $existing = PayrollRuleForCsv::where('key', $request->key)->orWhere('value', $request->value)->count();
        if($existing > 0) {
            return redirect()->back()->withErrors(['Key/Value already exists']);
        }
        $rule = new PayrollRuleForCsv;
        $rule->key = $request->key;
        $rule->value = $request->value;
        $rule->save();
        return Redirect::back()->with('message', 'Rule Saved');
    }
    public function destroy($id)
    {
        $rule = PayrollRuleForCsv::find($id);
        if($rule) {
            $rule->delete();
            return Redirect::back()->with('message', 'Rule Deleted');
        } else {
            return redirect()->back()->withErrors(['Rule not found']);
        }
    }
    public function show($id)
    {
        $rule = PayrollRuleForCsv::find($id);
        if($rule) {
            return response()->json($rule);
        } else {
            return response()->json('Rule not found', 400);
        }
    }
    public function update($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
        'key' => 'required',
        'value' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $key = $request->key;
        $key = str_replace('_', '', $key);
        $key = preg_replace('/[0-9]+/', '', $key);
        if(!ctype_lower($key)) {
            return redirect()->back()->withErrors(['Key should be all lowercase saperated by "_" (underscore)']);
        }
        $existing = PayrollRuleForCsv::where('key', $request->key)->orWhere('value', $request->value)->get();
        if(!empty($existing)) {
            if(count($existing) > 1) {
                return redirect()->back()->withErrors(['Key/Value already exists']);
            } else {
                $currRule = $existing->first();
                if(!empty($currRule) && ($currRule->id != $id)) {
                return redirect()->back()->withErrors(['Key/Value already exists']);
                }
            }
        }
        $rule = PayrollRuleForCsv::find($id);
        if($rule) {
            $rule->key = $request->key;
            $rule->value = $request->value;
            $rule->save();
            return Redirect::back()->with('message', 'Rule Saved');
        } else {
            return redirect()->back()->withErrors(['Rule not found']);
        }
    }
}
