<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Admin\CompOff;

use Redirect;

class CompOffController extends Controller
{
    public function index()
    {
        $pendingLeaves=[];
        $jsonuser=User::getUsers();
        return view(
            'pages.admin.admin-leave-section.comp-off.index',
            compact('pendingLeaves', 'jsonuser')
        );
    }
    public function store(Request $request)
    {
        $result=CompOff::validateCompOff($request);
        if($result['status']) {
            $result = CompOff::saveCompOff($request, $result['id']);
            if($result['status']) {
                return Redirect::back()->with('message', 'CompOff saved successfully');
            } else {
                return Redirect::back()->withErrors($result['errors']);
            }
        } else {
            return Redirect::back()->withErrors($result['errors']);
        }
    }
    public function getCompOff($id)
    {
        $compOff = CompOff::find($id);
        if($compOff) {
            return response()->json($compOff);
        } else {
            return response()->json('CompOff not found', 400);
        }
    }
    public function update(Request $request, $id)
    {
        $result=CompOff::validateCompOff($request);
        if($result['status']) {
            $result = CompOff::updateCompOff($request, $id, $result['id']);
            if($result['status']) {
                return response()->json('CompOff updated successfully', 200);
            } else {
                return response()->json($result['errors'], 400);
            }
        } else {
            return response()->json($result['errors'], 400);
        }
    }
    public function delete($id)
    {
        $compOff = CompOff::find($id);
        if($compOff) {
            $compOff->delete();
            return Redirect::back()->with('CompOff deleted');
        } else {
            return Redirect::back()->withErrors('CompOff not found');
        }
    }
}
