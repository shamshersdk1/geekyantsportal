<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Admin\ReferenceNumber;
use App\Models\User;
use App\Models\DocumentType;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Redirect;
use Auth;

class ReferenceNumberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $referenceNumbers = ReferenceNumber::orderBy('id','DESC')->get();
        return View('pages/admin/reference-numbers/index', ['referenceNumbers' => $referenceNumbers ] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $documentTypes = DocumentType::all();
        
        return View('pages/admin/reference-numbers/add',['documentTypes' => $documentTypes]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        if ( empty($request->document_type_id) || empty($request->signed_by) || empty($request->for) ) {
            return redirect::back()->withInput($request->input())->withErrors('One or more fields missing');
        }
        $input = $request->all();
        
        $response = ReferenceNumber::saveData($input);


        if ( $response['status'] )
        {
            return redirect('/admin/reference-number/'.$response['id']);
        }
        else
        {
            return Redirect::back()->withInput()->withErrors($response['message']);
        }

    }
    /**
     * Show details of the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $referenceObj = ReferenceNumber::find($id);
        if ( !empty($referenceObj) ){
            return View('pages/admin/reference-numbers/show', ['referenceObj' => $referenceObj] );
        } else {
            return redirect('/admin/reference-number')->withErrors('No record found');;
        }
        
        
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return redirect('/admin/reference-number');
        // $referenceObj = ReferenceNumber::find($id);

        // if($referenceObj){

        //     return View('pages/admin/reference-numbers/edit', ['referenceObj' => $referenceObj]);
        // }
        // else {
        //     return Redirect::back()->withInput()->withErrors('Reference Number not found');
        // }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        return redirect('/admin/reference-number');

        // if ( empty($request->document_type) || empty($request->signed_by) || empty($request->for) || empty($request->info) ) {
        //     return redirect::back()->withInput($request->input())->withErrors('One or more fields missing');
        // }
        // $input = $request->all();
        
        // $response = ReferenceNumber::updateData($id, $input);
        
        // if ( $response['status'] )
        // {
        //     return redirect('/admin/reference-number/'.$response['message'])->with('message', 'Successfully updated reference');
        // }
        // else
        // {
        //     return Redirect::back()->withInput()->withErrors($response['message']);
        // }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return redirect('/admin/reference-number');
    }
    
}
