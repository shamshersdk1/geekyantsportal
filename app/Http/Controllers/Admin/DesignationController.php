<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Admin\Designation;
use App\Models\User;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Redirect;
use Config;
use Exception;
use DB;
use Auth;
use Validator;

class DesignationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $designations = Designation::paginate(100);
        $designation_user_count_map = DB::table('users')
                                        ->where('is_active',1)
                                        ->select('designation', DB::raw('count(*) as count'))
                                        ->groupBy('designation')
                                        ->get()
                                            ->pluck('count', 'designation')
                                            ->all();

        return View('pages/admin/designation/index', ['designations' => $designations, 'designation_user_count_map'=> $designation_user_count_map] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('pages/admin/designation/add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'designation' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect::back()->withErrors($validator->errors())->withInput($request->input());
        }
        $designationObj = new Designation();
        $designationObj->designation = $request->designation;
        $designationObj->status = !empty($request->status) ? $request->status : false;
    
        if ( $designationObj->save() )
        {
            return redirect('/admin/designations/'.$designationObj->id)->with('message', 'Successfully added designation');
        }
        else
        {
            return Redirect::back()->withInput()->withErrors($designationObj->getErrors());
        }

    }
    /**
     * Show details of the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $designationObj = Designation::find($id);
        if ( $designationObj ) {
            return View('pages/admin/designation/show', ['designationObj' => $designationObj] );
        } else {
            return Redirect::back()->withInput()->withErrors('No record found');
        }
        
        
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $designationObj = Designation::find($id);
        if($designationObj){

            return View('pages/admin/designation/edit', ['designationObj' => $designationObj]);
        }
        else {
            return Redirect::back()->withInput()->withErrors('No record found');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'designation' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect::back()->withErrors($validator->errors())->withInput($request->input());
        }
        $designationObj = Designation::find($id);
        if ( $designationObj ) {
            $designationObj->designation = $request->designation;
            $designationObj->status = $request->status;
            if ( $designationObj->save() )
            {
                return redirect('/admin/designations/'.$designationObj->id)->with('message', 'Successfully updated designation');
            }
            else
            {
                return Redirect::back()->withInput()->withErrors($designationObj->getErrors());
            }
        } else {
            return Redirect::back()->withInput()->withErrors('No record found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $designationObj = Designation::find($id);
        if ( $designationObj->delete() )
        {
            return redirect('/admin/designations')->with('message','Successfully Deleted');
        }
        else
        {
            return redirect::back()->withErrors('Error while deleting the record');
        }
    }
    
}
