<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Profile;
use App\Models\User;
use App\Models\Admin\CmsProject;
use App\Models\Admin\Role;
use App\Models\Admin\ProfileProject;
use Redirect;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*$users = User::has('user_profile')->where('is_active',1)->select('id')->get();

        $profiles = Profile::whereIn('user_id',$users)->paginate(100);*/
        $roles = Role::paginate(10);
        return view('pages/admin/role/index', ['roles' => $roles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        return view('pages/admin/role/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!$request->user) {
            return Redirect::back()->with('message', 'Invalid Link Followed!');
        }

        if (! $request->user->isAdmin()) {
            return redirect('/');
        }

        if (empty($request->name)) {
            return Redirect::back()->withErrors(['Name is required']);
        }
        $roleObj = Role::where('name', $request->name)->first();
        if ($roleObj) {
            return Redirect::back()->withErrors(['Role with this name already exists']);
        }
        $role = new Role();
        $role->name = $request->name;
        $role->code = $request->code;
        $role->description = $request->desc;
        if (!$role->save()) {
            return Redirect::back()->withErrors($role->getErrors());
        }
        return redirect('/admin/role')->with('message', 'Role added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function show($id)
    // {
    //     //
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if (!$request->user) {
            return response("Users only", 401);
        }

        if (! $request->user->isAdmin()) {
            return response("Admins only", 401);
        }

        $role = Role::find($id);
        if (!$role) {
            return response()->json("Role not found", 400);
        }
        return response()->json($role);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!$request->user) {
            return response("Users only", 401);
        }

        if (! $request->user->isAdmin()) {
            return response("Admins only", 401);
        }

        if (empty($request->name)) {
                return response()->json("Name is required", 400);
        }

        $role = Role::find($id);
        if (!$role) {
            return response()->json("Role not found", 400);
        }
        $role->name = $request->name;
        $role->code = $request->code;
        $role->description = $request->desc;
        if (!$role->save()) {
            return response()->json($request, 400);
        }
        return response(200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if (!$request->user) {
            return Redirect::back()->with('message', 'Invalid Link Followed!');
        }

        if (! $request->user->isAdmin()) {
            return redirect('/');
        }

        $role = Role::find($id);
        if (!$role) {
            return Redirect::back()->withErrors(['Role not found.']);
        }

        if (!$role->delete()) {
            return Redirect::back()->withErrors($role->getErrors());
        }
        return redirect('/admin/role')->with('message', 'Role deleted successfully');
    }

    // public function search(Request $request)
    // {

    //     if ( !$request->user )
    //         return Redirect::back()->with('message', 'Invalid Link Followed!');

    //     if (!$request->user->isAdmin() ) {
    //         return redirect('/');
    //     }
    //     if ( empty( $request->search) ) {
    //         return redirect('/admin/profile');
    //     }

    //     $search = $request->search;
    //     $profiles = Profile::with('user')->whereHas('user', function ($query) use ($search){
    //         $query->where('name', 'LIKE', '%'.$search.'%')->orWhere('email', 'LIKE', '%'.$search.'%');
    //     })->paginate(10);

    //     return view('pages/admin/profile/index', [
    //                                                 'profiles' => $profiles,
    //                                                 'search' => $search
    //                                                 ]);
    // }
    public function switchToSuperAdmin()
    {
        $user = \Auth::user();
        if($user->role == "admin") {
            $user->role = "user";
            $user->save();
            $message = "You are now a normal user";
        } else {
            if($user->isAdmin()) {
                $user->role = "admin";
                $user->save();
                $message = "You are now a super admin";
            } else {
                $message = "Something went wrong";
            }
        }
        return Redirect::back()->withInput()->with('superAdminMessage', $message);
    }
}
