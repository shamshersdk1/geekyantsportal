<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\HireEmail;
use App\Jobs\SpamFilter;
use Validator;
use Redirect;
use Route;
use Carbon\Carbon;

use Input;

class HireEmailController extends Controller
{
    
    public function index()
    {
        $filter = Input::get('status');
        if(!empty($filter) && $filter != "" && !($filter =="spam" || $filter =="reviwed")) {            
            $hireEmailList = HireEmail::where('status', $filter)->orderBy('id', 'desc')->paginate(50);
        } elseif(($filter =="spam" || $filter =="reviwed")) {
            $hireEmailList = HireEmail::where('review_status', $filter)->orderBy('id', 'desc')->paginate(50);
        } else {
            $hireEmailList = HireEmail::orderBy('id', 'desc')->paginate(50);
        }
        return view('pages/admin/hire-email/index',  ['hireEmailList' => $hireEmailList ]);
    }

    public function create()
    {
        
    }

    public function store(Request $request)
    {
        
    }
    
    public function edit($id)
    {
        
    }
    public function show($id)
    {
        $hireEmail = HireEmail::find($id);
        $technologies = json_decode($hireEmail->technologies,true);
        $documents = json_decode($hireEmail->documents,true);
        if ( !empty($hireEmail) )
            return view('pages/admin/hire-email/show',  ['hireEmail' => $hireEmail,'technologies' => $technologies, 'documents' => $documents ]);
        else
            return Redirect::back()->withInput()->withErrors(['Hire Email doesnt exist']);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $email = HireEmail::find($id);
        if($email && $email->status!="sent") {
            $job = (new SpamFilter($id))->onQueue('spam-filter');
            dispatch($job);
            return Redirect::back()->withInput()->with('message', 'Request submitted');
        } else {
            return Redirect::back()->withInput()->withErrors(['Invalid link followed']);
        }
    }
    public function destroy($id)
    {
        
    }

    public function review()
    {
        $count = HireEmail::where('review_status', "unreviewed")->count();
        $hireEmail = HireEmail::orderBy('created_at', 'asc')->where('review_status', "unreviewed")->first();
        return view('pages/admin/hire-email/review',  compact('hireEmail', 'count'));
    }

    public function reviewAction($id)
    {
        $action = empty(Input::get('action')) ? null : Input::get('action');
        if(!is_null($action)) {
            $response = HireEmail::reviewAction($action, $id);
            if($response['status']) {
                return Redirect::back()->with('message', $response['message']);
            } else {
                return Redirect::back()->withErrors([$response['message']]);
            }
        } else {
            return Redirect::back()->withErrors("Invalid link followed");
        }
    }    
    
}
