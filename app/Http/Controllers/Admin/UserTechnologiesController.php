<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Models\UserTechnologies;
use App\Models\Admin\Technology;
use App\Models\User;
use App\Models\Admin\Leave;
use App\Models\Loan;

use Redirect;
use DB;
use Input;

class UserTechnologiesController extends Controller
{
    public function index()
    {    
        $jsonuser=User::getUsers();
        $result=UserTechnologies::filter();
        if ($result['status']==false) {
            return redirect::back()->withErrors($result['errors'])->withInput();
        }
        $users = $result['userList']->get();
        $techArray = [];
        $approvedSkills = [];
        $idArray = [];
        foreach($users as $key => $user)
        {
            $total = $user->userSkills()->whereIn('status', ['pending', 'approved'])->count();
            if($total > 0) {
                $str = "";
                $pendingTechId = $user->userSkills()->where('status', 'pending')->pluck('technology_id')->toArray();
                if(count($pendingTechId) > 0) {
                    $pendingTech = implode(", " ,$user->technologies()->whereIn('technologies.id', $pendingTechId)->pluck('name')->toArray());
                    $techArray[$user->id] = $pendingTech;
                } else {
                    $techArray[$user->id] = '--';
                }
                $approvedSkills[$user->id] = $total - count($pendingTechId);
                $idArray[] = $user->id;
            }
        }
        $users = $result['userList']->whereIn('id', $idArray)->paginate(20, ['*'], 'userList');
        return View('pages/admin/skill/index',compact('users', 'jsonuser', 'techArray', 'approvedSkills'));
    }

    public function create()
    {
        $jsonuser=User::getUsers();
        $technologies = Technology::get();
        return View('pages/admin/skill/add', compact('jsonuser', 'technologies'));
    }

    public function store(Request $request)
    {
        $user = User::where('name', $request->name)->first();
        if(!$user) {
            return redirect::back()->withErrors('Select a valid user')->withInput();
        }
        // $tech = Technology::where('name', $request->tech)->first();
        if(empty($request->tech)) {
            return redirect::back()->withErrors('Select a technology')->withInput();
        }
        $alreadyAssignedTechnology = UserTechnologies::where('user_id', $user->id)->whereIn('technology_id', $request->tech)->count();
        if ($alreadyAssignedTechnology > 0){      
            return redirect::back()->withErrors("Can't re-assign same Technology");
        }
        foreach($request->tech as $tech)
        {
            $technology = new UserTechnologies();
            $technology->user_id = $user->id;
            $technology->technology_id = $tech; 
            $technology->status = "approved"; //should be set to pending
            $technology->save();
        }
        return redirect('/admin/skill')->with('message', 'Successfully Added');
    }

    public function edit($id)
    {
        // $jsonuser=User::all();
        // $jsonlist=Technology::all();
        // $technology = UserTechnologies::find($id);
        // $selecteduser = User::where('id', $technology->user_id)->first();
        // $selectedtechnology = Technology::where('id', $technology->technology_id)->first();
        // if($technology) {
        //     return View('pages/admin/skill/edit', ['technology' => $technology, 'selecteduser'=>$selecteduser,'selectedtechnology'=>$selectedtechnology, 'jsonuser'=>$jsonuser, 'jsonlist'=>$jsonlist]);
        // }
        // else {
        //     abort(404);
        // }
        $user = User::find($id);
        if(!$user) {
            return redirect::back()->withErrors('User not found');
        }
        $pendingSkills = UserTechnologies::where('user_id', $id)->where('status', 'pending')->pluck('technology_id')->toArray();
        $technologies = Technology::whereNotIn('id', $pendingSkills)->get();
        $pendingTechnologies = Technology::whereIn('id', $pendingSkills)->get();
        $selectedTechnologies = UserTechnologies::where('user_id', $id)->where('status', 'approved')->pluck('technology_id')->toArray();
        return View('pages/admin/skill/edit', compact('technologies','pendingTechnologies', 'selectedTechnologies', 'user'));
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);
        if(!$user) {
            return redirect::back()->withErrors('User not found')->withInput();
        }
        if(empty($request->tech)) {
            return redirect::back()->withErrors('Select a technology')->withInput();
        }
        $selectedTechnologies = UserTechnologies::where('user_id', $id)->where('status', 'approved')->pluck('technology_id')->toArray();
        $sub = array_diff($selectedTechnologies, $request->tech);
        $add = array_diff($request->tech, $selectedTechnologies);
        if(count($sub) > 0) {
            $user->technologies()->detach($sub);
        }
        if(count($add) > 0) {
            foreach($add as $new)
            {
                $user->technologies()->attach([$new => ['status' => 'approved', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]]);
            }
        }
        if(count($request->pendingTech) > 0) {
            $pendingSkills = UserTechnologies::where('user_id', $id)->whereIn('technology_id', $request->pendingTech)->where('status', 'pending')->get();
            foreach($pendingSkills as $skill)
            {
                $skill->status = 'approved';
                $skill->save();
            }
        }
        return redirect('/admin/skill')->with('message', 'Successfully Updated');
        // $array = \Request::all();
        // $technology = UserTechnologies::find($id);
        // if($technology) {
        //     $alreadyAssignedTechnology = UserTechnologies::where('user_id', $array['user_id'])->where('technology_id', $array['technology_id'])->first();          
        //     if ($alreadyAssignedTechnology){      
        //         return redirect::back()->withErrors("Can't re-assign same Technology");
        //     }
        //     $technology->user_id = $array['user_id'];
        //     $technology->technology_id = $array['technology_id']; 
        //     if($technology->save()){
        //         return redirect('/admin/skill')->with('message', 'Successfully Updated');
        //     }
        //     else{
        //         return redirect::back()->withErrors($technology->getErrors())->withInput();
        //     }

        // }
        // else {
        //     abort(404);
        // }
    }

    public function accept($id)
    {
        $technology = UserTechnologies::find($id);
        if($technology) {
            $technology->status = "approved";
            $technology->save();
            return redirect('/admin/skill')->with('message', 'Successfully Updated');     
        }
        else {
            abort(404);
        }
    }

    public function reject($id)
    {
        $technology = UserTechnologies::find($id);
        if($technology) {
            $technology->status = "rejected";
            $technology->save();
            return redirect('/admin/skill')->with('message', 'Successfully Updated');
        }
        else {
            abort(404);
        }
    }

}

