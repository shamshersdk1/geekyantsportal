<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\IpAddressRange;
use App\Models\Admin\UserIpAddress;
use Illuminate\Http\Request;


class IpAddressRangeController extends Controller
{
    public function index()
    {
        $IpAddressRange = IpAddressRange::paginate(500);
        return view('pages.admin.ip-address.index', compact('IpAddressRange'));
    }
    public function create()
    {
        return view('pages.admin.ip-address.add');
    }
    public function store(Request $request)
    {
        $data = $request->all();
        if (!empty($data)) {

            $response = IpAddressRange::saveIpRange($data);
            if (!$response['status']) {
                return redirect()->back()->withErrors($response['message'])->withInput();
            }
            return redirect('/admin/ip-address-range')->with('message', $response['message']);

        } else {
            return redirect()->back()->withErrors('Empty data sent!')->withInput();
        }
    }
    public function update(Request $request, $id)
    {
        $data = $request->all();
        if (!empty($data)) {
            $response = IpAddressRange::updateIpRange($data, $id);
            if (!$response['status']) {
                return redirect()->back()->withErrors($response['message'])->withInput();
            }
            return redirect('/admin/ip-address-range')->with('message', $response['message']);

        } else {
            return redirect()->back()->withErrors('Empty data sent!')->withInput();
        }
    }
    public function destroy($id)
    {
        $IpAddressRange = IpAddressRange::find($id);
        if ($IpAddressRange) {
            //delete all ip's within this range
            $start_ip = ip2long($IpAddressRange['start_ip']);
            $end_ip = ip2long($IpAddressRange['end_ip']);

            while ($start_ip <= $end_ip) {
                $ip = long2ip($start_ip);
                $isUsed = UserIpAddress::where('ip_address', $ip)->delete();
                $start_ip++;
            }
            if (!$IpAddressRange->delete()) {
                return redirect()->back()->withErrors('Uable to delete records');
            }
        } else {
            return redirect()->back()->withErrors('Uable to delete record');
        }
        return redirect('/admin/ip-address-range')->with('message', 'Successfully deleted!');
    }

    public function edit($id)
    {
        $usedIps = collect();
        $IpAddressRange = IpAddressRange::find($id);
        $IpAddressRange1 = IpAddressRange::all();

        if ($IpAddressRange) {
            //checking if any ip within this range is assigned to any user
            $start_ip = ip2long($IpAddressRange['start_ip']);
            $end_ip = ip2long($IpAddressRange['end_ip']);

            while ($start_ip <= $end_ip) {
                $ip = long2ip($start_ip);
                $isUsed = UserIpAddress::where('ip_address', $ip)->with('user', 'assignee')->first();
                if (count($isUsed) > 0) {
                    $usedIps->push($isUsed);
                }
                $start_ip++;
            }
            if ($usedIps->count() > 0) {
                return view('pages.admin.ip-address.edit', compact('IpAddressRange', 'usedIps'))->withErrors('Ips within this range is already assigned to users. Hence, you can not edit the range. You may delete the range which will release all the assigned ips in this range.');
            } else {
                return view('pages.admin.ip-address.edit', compact('IpAddressRange', 'usedIps'));
            }
        }

    }
    public function show($id)
    {
        //get
    }
}
