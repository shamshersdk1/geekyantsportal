<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Watson\Validating\ValidatingTrait;
use App\Http\Controllers\Controller;
use App\Models\Admin\Log;
use Redirect;
use Exception;
use Validator;
use App\Services\LogService;
use App\Models\User;

class LogController extends Controller
{
    public function index()
    {
        $jsonuser=User::getUsers();
        $result=Log::filter();
        if ($result['status']==false) {
            return redirect::back()->withErrors($result['errors'])->withInput();
        }
        return View('pages/admin/log/index', ['records' => $result['logList'],'jsonuser'=>$jsonuser]);
    }

    public function show(Request $request, $id)
    {
        if (!$request->user) {
            return response("Users only", 401);
        }
        if (!$request->user->isAdmin()) {
            return response("Admins only", 401);
        }
        $records=Log::find($id);
        if (!$records) {
            return response()->json("Record not found", 400);
        }
        $date = $records->created_at->diffForHumans();
        $records["date"]=$date;   
        return response()->json($records);
    }

    
}
