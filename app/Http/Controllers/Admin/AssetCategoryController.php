<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Admin\AssetCategory;
use App\Models\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Redirect;
use Config;
use Exception;
use DB;
use Auth;

class AssetCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $assetCategoryList =[];
        $assetCategoryList = AssetCategory::all();
        return View('pages/admin/asset-management/category/index', ['assetCategoryList' => $assetCategoryList ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('pages/admin/asset-management/category/add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        if(!$request->name)
        {
            return redirect::back()->withInput()->withErrors(['Please provide asset category name']);
        }
        $asset_category = new AssetCategory();
        $asset_category->name = $request->name;
        $asset_category->save();
        return redirect('admin/asset-management/asset-category')->with('message', 'Asset Category added successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $asset_category = AssetCategory::find($id);
        if ( isset($asset_category) ) 
        {
            return View('pages/admin/asset-management/category/show', ['asset_category' => $asset_category ] );    
        }  
        return Redirect::back()->withInput()->withErrors(['Something went wrong']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $asset_category = AssetCategory::find($id);
        if ( isset($asset_category) ) 
        {
            return View('pages/admin/asset-management/category/edit', ['asset_category' => $asset_category] );    
        }  
        return Redirect::back()->withErrors($asset_category->getErrors());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $asset_category = AssetCategory::find($id);
        if ( isset($asset_category) ) 
        {
            if ( $request->name )
            {
                
                $asset_category->name = $request->name;
                if ( $asset_category->save() )
                {
                    return Redirect::back()->with('message', 'Successfully updated');   
                } else {
                    return Redirect::back()->withErrors($asset_category->getErrors());
                }  
            }
        }
        return redirect::back()->withInput()->withErrors(['Something went wrong']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $assetCategory = AssetCategory::find($id);
		if($assetCategory) {
			if(count($assetCategory->assets) > 0){	
				if($assetCategory->delete() && $assetCategory->assets()->detach()){
					return redirect('/admin/asset-management/asset-category')->with('message','Successfully Deleted');
				}
			}
			if($assetCategory->delete()){
				return redirect('/admin/asset-management/asset-category')->with('message','Successfully Deleted');
			}
			else{
				return Redirect::back()->withErrors($assetCategory->getErrors());
			}
		}
		else
			abort(404);
    }
}
