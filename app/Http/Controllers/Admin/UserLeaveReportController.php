<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Leave;
use App\Models\Admin\Calendar;
use App\Models\User;
use App\Services\AttendanceReportService;

use Excel;

class UserLeaveReportController extends Controller
{
    public function index(Request $request)
    {
        if(empty($request->start_date)||empty($request->end_date)) {
            $start_date = date("Y-m-d", strtotime("previous monday"));
            $start_date = date("Y-m-d", strtotime($start_date . ' -21 day'));
            $end_date =  date('Y-m-d', strtotime(" -1 day " ));
        } else {
            $start_date = $request->start_date;
            $end_date = $request->end_date;
        }
        $data = AttendanceReportService::getDetails($start_date, $end_date);
        $dates = $data['dates'];
        $code = $data['code'];
        $result_data = $data['result_data'];
        
        return view('pages.admin.admin-leave-section.user-leave.index', compact('dates','code','result_data'));
    }
    public function downloadExcel(Request $request)
    {
        if(empty($request->start_date)||empty($request->end_date)) {
            $start_date = date("Y-m-d", strtotime("previous monday"));
            $start_date = date("Y-m-d", strtotime($start_date . ' -21 day'));
            $end_date =  date('Y-m-d', strtotime(" -1 day " ));
        } else {
            $start_date = $request->start_date;
            $end_date = $request->end_date;
        }
        $filename = 'Attendance_'.date( "jS", strtotime( $start_date )).'_'.date( "M", strtotime( $start_date )).'_' .date( "jS", strtotime( $end_date )).'_'.date( "M", strtotime( $end_date ));
        $data = AttendanceReportService::getDetails($start_date, $end_date);
        $dates = $data['dates'];
        $result_data = $data['result_data'];
        $count = count($dates);
        $headers = [];
        $headers[0] = 'Employee ID';
        $headers[1] = 'Employee Name';
        for ( $i=2; $i < $count; $i++ )
        {
            $headers[$i] = $dates[$i];
        }
        $headers[$count] = 'SL Used';
        $headers[$count+1] = 'PL Used';
        $headers[$count+2] = 'Other Used';
        
        $finalHeaderArray[] = $headers;
        
        $export_data = array_merge($finalHeaderArray, $result_data);    
        
        return Excel::create($filename, function($excel) use ($export_data) {
            $excel->sheet('mySheet', function($sheet) use ($export_data)
            {
                $sheet->fromArray($export_data);
            });
        })->download('csv');

    }
}
