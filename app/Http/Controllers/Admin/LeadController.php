<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Lead;
use Illuminate\Http\Request;
use App\Http\Requests;
class LeadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $leads = Lead::where('status','new_lead')->orderBy('updated_at','desc')->get();
         
        $state = "new_lead";
        // return View('pages/lead/index', ['lead' => $lead]);

        return View('pages/admin/lead/index', ['leads' => $leads, 'state' => $state]);
    }

    public function showstatus($status)
    {   
        if($status == "all") {
            // echo "here";
            // die;
            $leads = Lead::orderBy('updated_at','desc')->get();
            $state = "all";
        }
        else {
            $leads = Lead::where('status',$status)->orderBy('updated_at','desc')->get();
            $state = $status;
        }


        // return View('pages/lead/index', ['lead' => $lead]);

        return View('pages/admin/lead/index', ['leads' => $leads, 'state' => $status]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('pages/admin/lead/new-lead'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $lead = new Lead();
        $lead->name = $request->name;
        $lead->email = $request->email;
        $lead->subject = $request->subject;
        $lead->source = $request->source;
        $lead->is_read = false;
        $lead->message = $request->message;
        $lead->status = "new_lead";
        if($lead->save()){
            $leads = Lead::where('status','new_lead')->orderBy('updated_at','desc')->get();
       
            return View('pages/admin/lead/index', ['leads' => $leads]);
            
        }
        else {
            return back()->withErrors($lead->getErrors())->withInput();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

        $lead = Lead::find($id);
        


        return View('pages/admin/lead/show', ['lead' => $lead]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $lead = lead::find($id);
        if($lead==null)
                return redirect()->back()->withErrors(['message', 'User not found']);

        return View('pages/admin/lead/add', ['lead' => $lead]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $leadObj = lead::find($id);
        $leadObj->status = $request->status;
        //mention other feild also if required
        if($leadObj->save()){
            return back()->with('message', 'Updated Successfully');
        }
        else {
            return back()->withErrors($leadObj->getErrors());
        }



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $lead = lead::find($id);
        // echo json_encode($array);
        if($lead){
            if($lead->delete()){
                return redirect('/admin/lead')->with('message','Successfully Deleted');
            }
            else{
                return Redirect::back()->withErrors(['Not able to delete']);
            }
            
        }
        return Redirect::back()->withErrors(['User not found']);
    }


    public function displayConversation($id)
    {

        $lead = lead::find($id);
        $lead->is_read = true;
        $lead->save();
       

        if($lead==null)
                return redirect()->back()->withErrors(['message', 'User not found']);
        
        // $conversation = lead::where('email',$user);
        // print_r( $conversation->toArray());
        // die;
        return View('pages/admin/lead/viewconversation', ['leads' => $lead]);
    }


    public function convertLead($id)
    {
        // print_r($id);
        // die;  
        $lead = Lead::find($id);
        if($lead){
            return View('pages/admin/company/add', ['lead' => $lead]);

        }
        else
        {
            abort(404);
        }
        // echo "<pre>";
        // print_r($lead->toArray());
        // die ;

    }
}
