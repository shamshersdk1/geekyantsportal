<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\User;
use App\Models\NonworkingCalendar;

use App\Services\PayslipDataService;
use App\Services\TimesheetReportService;
use App\Services\VariablePayService;

class SalaryReviewController extends Controller
{

     public function index($employee_id){
        $success = false;
        $user = User::where('employee_id',$employee_id)->first();
        if($user){
            $success = true;

        }
        //Request Input Data
        $month = date('m');
        $month_string = date('M Y');
        $year = date('Y');
        $output = PayslipDataService::getSalaryByUserIdAndMonth($user->id, $month, $year);
        return view('pages.admin.salary.review', ['success'=> $success, 'employee_id' => $employee_id,'user'=>$user, 'month_string'=>$month_string, 'output'=>$output]);
     }

     public function view(){
        return view('pages.admin.salary.view',
            [   
                'feedback_status' => 'approved',
                'feebback_score' => '8.5',
                'total_vb' => '4000',

                'leave_status' => 'approved',
                'total_working_day' => '22',
                'pl' => '2',
                'sl' => '1',
                'encash_pl' => '1',
                'additional_pl' => '1',
                'lop' => '1',

                'bonus_status' => 'pending',
                'onsite_bonus_amount' => '2000',
                'additional_bonus_amount' => '2000',
                'referral_bonus_amount' => '2000',
                'techtalk_bonus_amount' => '2000',
                'other_bonus_amount' => '',

                'ctc' => '450000',

                'loan_status' => 'approved',
                'loanData' => [
                    [
                        'loan_amount' => '40000',
                        'loan_balance' => '30000',
                        'monthly_deduction' => '10000'
                    ]
                ],

                'prev_earning' => '200000',
                'it_saving' => '1000',

                'tax_status' => 'pending',
                'prof_tax' => '200',
                'it_tax' => '200',

                'insurance_status' => 'pending',
                'insurance_type' => 'health',
                'insurance_coverage' => '40000',
                'insurance_installment' => '350'
            ]
        );
     }

     public function indexMis(){
        $lastMonth = date("m", strtotime("first day of previous month"));
        $lastMonthYear = date("Y", strtotime("first day of previous month"));
        $thisMonth = date("m", strtotime("first day of this month"));
        $thisMonthYear = date("Y", strtotime("first day of this month"));
        $success = false;
        $users = User::where('is_active',1)->get();
        $payslipData = [];
        $variablePayTimesheetLastMonth = [];
        $lastMonthCalendar = NonworkingCalendar::getCalendarDatewise($lastMonth, $lastMonthYear);
        $thisMonthCalendar = NonworkingCalendar::getCalendarDatewise($thisMonth, $thisMonthYear);
        $userCalendar = [];
        $varaiblePay = [];

        if($users){
            $success = true;
            foreach($users as $user){
                $payslipData[$user->id] = PayslipDataService::getSalaryByUserIdAndMonth($user->id, $thisMonth, $thisMonthYear);
                if($payslipData[$user->id] === false){
                    echo "Missing PayslipData";
                    print_r($user);
                    die;
                }
                $lastMonthEmployeeCalendar = NonworkingCalendar::getUserCalendarDatewise($user->id, $lastMonth, $lastMonthYear);
                $varaiblePay[$user->id] = VariablePayService::getEmployeeVariablePay($user->id, $lastMonth, $lastMonthYear);
                $userCalendar[$user->id] = $lastMonthEmployeeCalendar['summary']['counts'];
                $timesheetSummary[$user->id] = TimesheetReportService::getEmployeeWiseTimesheetSummary($user->id, $lastMonth, $lastMonthYear);
            }
        }
        
        
        return view('pages.admin.salary.review-mis', [
            'success'=> $success, 
            'users'=>$users, 
            'payslipData'=>$payslipData, 
            'lastMonthCalendar'=>$lastMonthCalendar,
            'thisMonthCalendar'=>$thisMonthCalendar,
            'userCalendar'=>$userCalendar,
            'varaiblePay'=>$varaiblePay,
            'timesheetSummary'=>$timesheetSummary
            ]);  
     }

}
