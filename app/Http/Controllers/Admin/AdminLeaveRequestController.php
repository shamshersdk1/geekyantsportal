<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Client;
use App\Models\Admin\Leave;
use App\Models\Admin\Project;
use App\Models\Admin\ProjectResource;
use App\Models\Leave\UserLeaveTransaction;
use App\Models\User;
use App\Services\LeaveService;
use Auth;
use DB;
use Illuminate\Http\Request;
use Redirect;
use Session;
use Validator;

class AdminLeaveRequestController extends Controller
{
    public function index()
    {
        $users = null;
        $result = [];
        $userArray = [];

        $arrayId = Leave::arrayId();

        $getData = $_GET ? $_GET : null;
        $searchuser = isset($getData['searchuser']) ? $getData['searchuser'] : '';
        
        $users = User::where('is_active', 1)->where('name', 'LIKE', '%' . $searchuser . '%')->get();

        foreach ($users as $user) {
            $userArray[$user->id] = $user->id;
        }
        $result['pendingLeaves'] = Leave::with('user', 'leaveType')->whereIn('user_id', $userArray)->where('status', 'pending')->orderBy('start_date', 'DESC')->paginate(50);
        $result['upcomingLeaves'] = Leave::with('user', 'leaveType')->whereIn('user_id', $userArray)->whereIn('id', $arrayId)->orderBy('start_date', 'DESC')->paginate(50);
        $result['leaveList'] = Leave::with('user', 'leaveType')->whereIn('user_id', $userArray)->orderBy('start_date', 'DESC')->paginate(100);
        $result['title'] = 'Leave Dashboard';
        $result['status'] = true;

        $startDate = isset($getData['start_date']) ? $getData['start_date'] : null;
        $endDate = isset($getData['end_date']) ? $getData['end_date'] : null;
        if($startDate && $endDate)
        {
            $result['pendingLeaves'] = Leave::with('user', 'leaveType')->whereIn('user_id', $userArray)->where('status', 'pending')->orderBy('start_date', 'DESC')->whereBetween('start_date', [$startDate, $endDate])->orWhereBetween('end_date', [$startDate, $endDate])->paginate(50);
            $result['upcomingLeaves'] = Leave::with('user', 'leaveType')->whereIn('user_id', $userArray)->whereIn('id', $arrayId)->orderBy('start_date', 'DESC')->whereBetween('start_date', [$startDate, $endDate])->orWhereBetween('end_date', [$startDate, $endDate])->paginate(50);
            $result['leaveList'] = Leave::with('user', 'leaveType')->whereIn('user_id', $userArray)->orderBy('start_date', 'DESC')->whereBetween('start_date', [$startDate, $endDate])->orWhereBetween('end_date', [$startDate, $endDate])->paginate(100);
        }
        $jsonuser = User::getUsers();
        $jsonclients = Client::getClients();
        // $result = Leave::filter();
        if ($result['status'] == false) {
            return redirect::back()->withErrors($result['errors'])->withInput();
        }
        return View("pages/admin/leave-request-new", ['pendingLeaves' => $result['pendingLeaves'], 'upcomingLeaves' => $result['upcomingLeaves'], 'leaveList' => $result['leaveList'], 'jsonuser' => $jsonuser, 'jsonclients' => $jsonclients, 'title' => $result['title']]);
    }

    public function leaveStatus(Request $request, $id)
    {
        $leave = Leave::find($id);
        if ($leave) {
            $leave->approver_id = Auth::id();
            $leave->action_date = date('Y-m-d');
            if ($request->approve) {
                $leave->status = "approved";
                if ($leave->save()) {
                    if (!empty($request->client)) {
                        $email = Client::where('name', $request->client)->first()->email;
                        Leave::notifyClient($email, $leave);
                    }
                    Leave::notifyLeaveAction($leave->id);
                    return redirect::back()->with('message', 'Request Approved');
                } else {
                    return redirect::back()->withErrors(['Something went wrong.'])->withInput();
                }
            } elseif ($request->cancel) {
                DB::beginTransaction();
                try {
                    $leave->status = "cancelled";
                    if ($leave->save()) {
                        Leave::notifyLeaveAction($leave->id);

                        $data['days'] = (-1) * $leave->days;
                        $data['calculation_date'] = $leave->end_date;
                        $data['calendar_year_id'] = null;
                        $data['leave_type_id'] = $leave->leave_type_id;
                        $data['user_id'] = $leave->user_id;
                        $data['reference_type'] = 'App\Models\Leave\Leave';
                        $data['reference_id'] = $leave->id;
                        $transObj = UserLeaveTransaction::saveData($data);
                        if ($transObj) {
                            DB::commit();
                            return redirect::back()->with('message', 'Request cancelled');
                        }
                    } else {
                        return redirect::back()->withErrors(['Something went wrong.'])->withInput();
                    }
                } catch (Exception $e) {
                    DB::rollback();
                }
                return redirect::back()->withErrors(['Something went wrong.'])->withInput();
            }
        } else {
            abort(404);
        }
    }

    /**
     * Leave corresponding to leave ID.
     *
     * @return Response
     */
    public function getLeave(Request $request, $id)
    {
        $leave = Leave::with('user', 'projectManagerApprovals')->where('id', $id)->first();
        if ($leave) {
            $managerInfo = [];
            $projectIds = ProjectResource::getActiveProjectIdArray($leave->user->id, $leave->start_date, $leave->end_date);
            if (!empty($leave->projectManagerApprovals) && !empty($projectIds)) {
                foreach ($leave->projectManagerApprovals as $item) {
                    if (!empty($item->manager)) {
                        $projects = Project::where('project_manager_id', $item->user_id)->whereIn('id', $projectIds)->pluck('project_name')->toArray();
                        if (empty($projects)) {
                            $projects = ["No active projects"];
                        }
                        $managerInfo[] = array('name' => $item->manager->name, 'status' => $item->status, 'projects' => $projects, 'date' => date_in_view($item->updated_at));
                    }
                }
            }
            $leave->managerInfo = $managerInfo;
            return response()->json($leave);
        } else {
            return response()->json('Leave not found', 400);
        }
    }

    /**
     * Change type of leave(sick, paid, unpaid).
     *
     * @return Response
     */
    public function updateLeave(Request $request, $id)
    {
        $user = Auth::user();
        $data = $request->all();
        $skip = 1;
        $role = 'user';
        $validator = Validator::make(
            $request->all(), [
                'start_date' => 'required',
                'end_date' => 'required',
                'half' => 'in:first,second',
                'type' => 'required|in:sick,paid,unpaid,half,compoff',
            ]
        );

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        if (empty($data['type'])) {
            return response()->json("Type not specified", 400);
        }
        $leave = Leave::find($id);
        if ($leave) {
            if ($role != 'admin') {
                $response = Leave::validateLeave($leave->user, $request, $skip, $role);
                if (!$response['status']) {
                    Session::flash('message', $response['errors']);
                    Session::flash('alert-class', 'alert-danger');
                    return response()->json($response['errors'], 400);
                }
            }
            $leave->approver_id = Auth::id();
            if ($data['rejectreason'] != '') {
                if ($leave->status == 'rejected') {
                    $leave->cancellation_reason = $data['rejectreason'];
                } else {
                    Session::flash('message', 'Press reject button to reject the leave');
                    Session::flash('alert-class', 'alert-danger');
                    return response()->json($response['errors'], 400);
                }
            }
            $leave->type = $data['type'];
            $leave->start_date = $data['start_date'];
            if ($data['type'] == 'half') {
                $leave->end_date = $data['start_date'];
                $leave->days = 0.5;
                $leave->half = $data['half'];
            } else {
                $leave->end_date = $data['end_date'];
                $leave->days = LeaveService::calculateWorkingDays($request['start_date'], $request['end_date'], $leave->user->id); //user id needs to be passed
            }
            if ($leave->save()) {
                return response()->json($leave, 200);
            } else {
                return response()->json("Unable to save", 400);
            }
        } else {
            abort(404);
        }
    }
    public function rejectLeave(Request $request, $id)
    {
        $user = Auth::user();
        $data = $request->all();
        $skip = 1;
        $role = 'user';
        $leave = Leave::find($id);
        if ($leave) {
            if ($leave->type == 'rejected') {
                Session::flash('message', 'Leave already rejected');
                Session::flash('alert-class', 'alert-danger');
                return response()->json($response['errors'], 400);
            }
            $leave->cancellation_reason = $data['rejectreason'];
            $leave->status = "rejected";
            $leave->action_date = date('Y-m-d');
            $response = Leave::validateLeave($leave->user, $request, $skip, $role);
            $leave->approver_id = Auth::id();
            if ($leave->save()) {
                Leave::notifyLeaveAction($leave->id);
                return response()->json($leave, 200);
            } else {
                return response()->json("Unable to save", 400);
            }
        } else {
            abort(404);
        }
    }

    /**
     * Create new leave.
     *
     * @return Response
     */
    public function createNewLeave(Request $request)
    {
        $userObject = $request->user;
        $role = 'user';
        $user = User::where('name', $request->employee)->first();

        if (!$user) {
            return redirect::back()->withErrors('User Not Found')->withInput($request->input());
        }
        $validator = Validator::make($request->all(), [
            'type' => 'required|in:sick,paid,unpaid,half,compoff',
            'start_date' => 'required',
            'end_date' => 'required',
            'half' => 'in:first,second',
        ]);

        if ($validator->fails()) {
            return redirect::back()->withErrors($validator->errors())->withInput($request->input());
        }
        if (!LeaveService::isLeaveValid($request->start_date, $request->end_date)) {
            return redirect::back()->withErrors("Start of Leave should be on or before End Date ")->withInput($request->input());
        }

        // if (!LeaveService::isLeaveAppliedForCurrentWorkingYear($request->start_date, $request->end_date, $role)) {
        //     return redirect::back()->withErrors("Leave can be applied only for current working year.")->withInput($request->input());
        // }

        // $leaveLength=Carbon::parse($request->end_date)->diffInDays(Carbon::parse($request->start_date));
        // if($role != 'admin') {
        //     if ($leaveLength>13&&$request->type!='unpaid') {
        //         return redirect::back()->withErrors("Duration of leave should not be more than 14 calender days ")->withInput($request->input());
        //     }
        // }
        $data = $request->all();
        $data['user_id'] = $user->id;

        if (!isset($data['type']) || empty($data['type'])) {
            return redirect::back()->withErrors(['Leave Type is required'])->withInput();
        }

        $result = Leave::saveData($data, $role);

        if ($result['status']) {
            $leave = Leave::find($result['id']);
            $leave->status = $leave->status;
            $leave->approver_id = \Auth::id();

            if ($leave->save()) {
                return redirect('/admin/leave-section/confirmation')->with('message', 'Request Sent');
            } else {
                return redirect::back()->withErrors(['Something went wrong.'])->withInput();
            }
        } else {
            return redirect::back()->withErrors($result['errors'])->withInput($request->input());
        }
    }
}
