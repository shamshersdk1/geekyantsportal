<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\BadLeadsFilter;
use Validator;
use Redirect;
use Route;
use Carbon\Carbon;

class BadLeadsFilterController extends Controller
{
    
    public function index()
    {
        $badLeadsFilterList = BadLeadsFilter::all();
        // dd($floors->toArray());
        return view('pages/admin/bad-leads-filter/index',  ['badLeadsFilterList' => $badLeadsFilterList ]);
    }

    public function create()
    {
        return view('pages/admin/bad-leads-filter/add');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'field_name' => 'required'
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors(['Please provide all the mandatory fields']);
        }

        $badLeadsFilter = new BadLeadsFilter();

        $badLeadsFilter->field_name = $request->field_name;
        $badLeadsFilter->filter_value = $request->filter_value;

        if($badLeadsFilter->save()){
            return redirect('/admin/bad-leads-filter')->with('message', 'Successfully Added');
        }
        else {
            return redirect::back()->withErrors($badLeadsFilter->getErrors())->withInput();
        }

    }
    
    public function edit($id)
    {
        $badLeadsFilter = BadLeadsFilter::find($id);
        
        if ( isset($badLeadsFilter) ) 
        {
            return View('pages/admin/bad-leads-filter/edit', ['badLeadsFilter' => $badLeadsFilter] );    
        }  
        return Redirect::back()->withInput()->withErrors(['Something went wrong']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'field_name' => 'required'
        ]);
        $badLeadsFilter = badLeadsFilter::find($id);
        
        if ( isset($badLeadsFilter) ) 
        {
            $badLeadsFilter->field_name = $request->field_name;
            $badLeadsFilter->filter_value = !empty($request->filter_value) ? $request->filter_value : '';
            
            if ( $badLeadsFilter->save() )
            {
                return Redirect::to('admin/bad-leads-filter')->with('message', 'Successfully updated');   
            } else {
                return Redirect::back()->withErrors($badLeadsFilter->getErrors());
            }  
            
        }
        return redirect::back()->withInput()->withErrors(['Something went wrong']);
    }
    public function destroy($id)
    {
        $badLeadsFilter = badLeadsFilter::find($id);
        if($badLeadsFilter) {
            if($badLeadsFilter->delete()){
                return redirect('admin/bad-leads-filter')->with('message','Successfully Deleted');
            }
            else{
                return Redirect::back()->withErrors($badLeadsFilter->getErrors());
            }
        }
        else
            abort(404);
    }
    
}
