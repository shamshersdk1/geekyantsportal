<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

use App\Models\SystemSetting;
use App\Models\Admin\Leave;
use App\Models\User;
use App\Models\Admin\Calendar;

use App\Services\LeaveService;

use PDF;
use Validator;
use Redirect;
use Input;
use DateTime;

define("CALENDAR", CAL_GREGORIAN);


class AdminLeaveReportController extends Controller
{
    public function index()
    {
        return view('pages.admin.admin-leave-section.leave-report.index');
    }
}
