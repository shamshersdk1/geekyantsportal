<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Seat;
use App\Models\Floor;
use App\Models\FloorsSeat;
use App\Services\FloorService;
use Validator;
use Redirect;
use Route;
use Carbon\Carbon;

class FloorController extends Controller
{
    
    public function index()
    {
        $floors = Floor::orderBy('type')->get();
        // dd($floors->toArray());
        return view('pages/admin/floor/index',  ['floors' => $floors ]);
    }

    public function create()
    {
        return view('pages/admin/floor/add');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors(['Please provide all the mandatory fields']);
        }


        $floorObj = new Floor();

        $floorObj->name = $request->name;
        $floorObj->number_of_seats = $request->number_of_seats;
        $floorObj->floor_number = $request->floor_number;
        $floorObj->type = $request->type;

        if($floorObj->save()){
            FloorsSeat::createSeats($request->number_of_seats, $floorObj->id);
            return redirect('/admin/seat-management')->with('message', 'Successfully Added');
        }
        else {
            return redirect::back()->withErrors($floorObj->getErrors())->withInput();
        }

    }
    
    public function show($floor_id)
    {
        $floor = Floor::where('id',$floor_id)->first();
        if ( isset($floor) ){
            $floor_number = $floor->floor_number;
            $floor_seats = FloorsSeat::where('floor_id',$floor_id)->orderBy('id','ASC')->get();
            $floor_details_unformatted = FloorService::floorDetails($floor_seats);
            $floor_details = json_encode(FloorService::formatFloorDetails($floor_details_unformatted));
            $users = json_encode(User::where('is_active',1)->get());
            $occupied_seats = FloorsSeat::getCount($floor_id);
            $empty_seats = $floor->number_of_seats - $occupied_seats;
            return view('pages/admin/floor/show',  ['floor_detail' => $floor,'floor_details' => $floor_details, 'floor' => $floor_number, 'users' => $users, 'occupied_seats' => $occupied_seats, 'empty_seats' => $empty_seats]);
        }
        return Redirect::back()->withInput()->withErrors(['Invalid Floor Id']);
    }
    public function showRM()
    {
        $floors = Floor::orderBy('type', 'ASC')->get();
        foreach ($floors as $floor) {
            $floorSeatings[$floor->id] = $floor;
            $floorSeatings[$floor->id]['seats'] = FloorService::getSeatArragement($floor->id);
        }
        return view('pages/admin/floor/show-rm',  compact('floorSeatings','floors'));
    }

    public function edit($floor_id)
    {
        $floor = Floor::find($floor_id);
        return view('pages/admin/floor/edit',compact('floor'));
    }

    public function update(Request $request, $id)
    {
        
         $floorObj = Floor::find($id);
         $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors(['Please provide all the mandatory fields']);
        }
        $floorObj->name = $request->name;
        $floorObj->number_of_seats = $request->number_of_seats;
        $floorObj->floor_number = $request->floor_number;
        $floorObj->type = $request->type;

        if($floorObj->save()){
            return redirect('/admin/seat-management')->with('message', 'Successfully Added');
        }
        else {
            return redirect::back()->withErrors($floorObj->getErrors())->withInput();
        }        
    }
    
}
