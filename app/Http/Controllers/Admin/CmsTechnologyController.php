<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Admin\CmsTechnology;
use DB;
use Illuminate\Http\Request;
use Redirect;
class CmsTechnologyController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$technologies = CmsTechnology::paginate(100);
		// echo "<pre>";
		// print_r($technologies->toArray());
		// die;
		// isset($technologies)?$technologies:"";
		// print_r($technologies);
		
		return View('pages/admin/cms-technology/index',['technologies' => $technologies]);

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View('pages/admin/cms-technology/add');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		// echo "sfuh";
		// die;
		// echo "dfkj";
		// die;
		// echo "<pre>";
		// $inputImage = $request->file('logo')->getClientOriginalName();
		// print_r($inputImage);
		// die;

		// if($request->name && $request->short_description && $request->file('logo')){
			//print_r($request->all());
			//die;

			if(!$request->hasFile('logo')){

				return Redirect::back()->withInput()->withErrors(['Logo is required']);
			}
			
			$targetFile = storage_path('app/uploads/technology');
			//return $request->has('logo');

			

			$image_name = $request->file('logo')->getClientOriginalName();

			if($request->is_featured){
				$is_featured = "1";
			}
			else{
				$is_featured = "0";
			}

			$data = $request->all();
			$data['image_name'] = $image_name;
			$data['is_featured'] = $is_featured;

			$result = CmsTechnology::saveData($data);
			if($result['status'])
			{
				$request->file('logo')->move($targetFile, $image_name);
				return redirect('/admin/cms-technology')->with('message', 'Successfully Added');
			}
			else{
				return redirect::back()->withErrors($result['errors'])->withInput();
			}

			/*$technology = new CmsTechnology();
			$technology->name = $request->name;
			$technology->logo = $image_name;
			$technology->slug = strtolower($request->name);
			$technology->short_description = $request->short_description;
			$technology->detailed_description = $request->detailed_description;
			$technology->is_featured = $is_featured;
			if($technology->save())
			{
				$request->file('logo')->move($targetFile, $image_name);
				return redirect('/admin/cms-technology')->with('message', 'Successfully Added');
			}
			else{
				return redirect::back()->withErrors($technology->getErrors())->withInput();
			}*/

		// }
		// else
		// 	return redirect()->back()->withErrors($technology->getErrors());
	}	

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$technology = CmsTechnology::find($id);
		// print_r($technology);
		if($technology)
			return View('pages/admin/cms-technology/show',['technology' => $technology]);
		else
			return Redirect::back()->withErrors(['Invalid request']);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$technology = CmsTechnology::find($id);
		if($technology)
			return View('pages/admin/cms-technology/edit', ['technology' => $technology]);
		else
			abort(404);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$technology = CmsTechnology::find($id);
		if($technology){
			if($request->is_featured){
				$is_featured = "1";

			}
			else{
				$is_featured = "0";
			}
			if($request->file('logo')){
				$targetFile = storage_path('app/uploads');
				$inputImage = $request->file('logo');
				$image_name = $request->file('logo')->getClientOriginalName();
				if($inputImage){
				    $request->file('logo')->move($targetFile, $image_name);
				}
			}
			else{
				$image_name = $technology->logo;
			}
			// $technologyObj = Technology::where('id', $id)->update(['name' => $request->name, 'slug' => strtolower($request->slug), 'short_description' => $request->short_description, 'detailed_description' => $request->detailed_description, 'logo'=> $image_name, 'is_featured' => $is_featured]);
			$technology->name = $request->name;
			$technology->slug = strtolower($request->slug);
			$technology->short_description = $request->short_description;
			$technology->detailed_description = $request->detailed_description;
			$technology->logo = $image_name;
			$technology->is_featured = $is_featured;
			if($technology->save()){
				return redirect('/admin/cms-technology')->with('message','Successfully Updated');
			}
			else{
				return redirect::back()->withErrors($technology->getErrors());
			}
		}
		else{
			abort(404);
		}

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$technology = CmsTechnology::find($id);
		if($technology){
			if($technology->delete()){
				return redirect('/admin/cms-technology')->with('message','Successfully Deleted');
			}
			else{
				return redirect::back()->withErrors($technology->getErrors());
			}

		}
		else{
			abort(404);
		}

	}


}
