<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\PurchaseOrder;
use App\Models\User;

use Auth;

class PurchaseOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $purchaseOrders = PurchaseOrder::paginate(100);
        return View('pages/admin/purchase-order/index', ['purchase_orders' => $purchaseOrders]);
    }
    public function show($id)
    {
        $user = Auth::user();
        $purchaseOrder = PurchaseOrder::find($id);
        return View('pages/admin/purchase-order/show', ['purchase_orders' => $purchaseOrder]);
    }

}
