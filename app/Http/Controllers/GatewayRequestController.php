<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Gateway;
use App\Jobs\GatewayRequest;

class GatewayRequestController extends Controller
{
    public function store(Request $request){

        $token = $request->input('token');

        if($token == ''){
            echo 'ERROR: Missing Token';
            return;
        }

        $gateway = Gateway::where('token',$token)->first();
        if(!$gateway){
            echo 'ERROR: Invalid Gateway / Invalid Token';
            return;
        }

        $all = $request->all();

        $data = json_encode($all);

        $dataArray = json_decode($data, true);
        $dataArray['gateway_id'] = $gateway->id;
        
        $data = json_encode($dataArray);
        
        $gatewayRequest = new GatewayRequest($data);
        $gatewayRequest->onQueue('gateway');

        if(dispatch($gatewayRequest)){
            echo 'OK';
        }else{
            echo 'ERROR';
        }
    }
}
