<?php

namespace App\Http\Controllers\UserView;

use App\Http\Controllers\Controller;
use App\Models\Admin\Appraisal;
use App\Models\User;
use Input;
use Redirect;

class AppraisalController extends Controller
{
 public function index($id = null)
 {
  $user = \Auth::user();
  if (empty($user)) {
   return redirect::back()->withErrors('Invalid Link Followed');
  }
  $appraisals = Appraisal::where('user_id', $user->id)->orderBy('is_active', 'desc')->orderBy('effective_date', 'desc')->paginate(10)->appends(Input::except('page'));
  return view('pages.userView.payslips.appraisals', compact('user', 'appraisals'));
 }
}
