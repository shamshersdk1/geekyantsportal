<?php

namespace App\Http\Controllers\UserView;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Admin\Appraisal;
use App\Models\User;
use App\Models\Admin\PayroleGroup;
use App\Models\Notify;
use Validator;
use Redirect;
use Input;

class WifiPasswordController extends Controller
{
    public function index()
    {
        $user = \Auth::User();
        if(!$user)
            return redirect::back()->withErrors('Invalid Link Followed');
        
        $parts = explode("@", $user->email);

        $username = !empty($parts[0]) ? $parts[0] : $user->first_name;

        return view('pages.admin.userView.wifi-password',compact('username'));
    }

    public function updatePassword(Request $request)
    {
        $user = \Auth::User();
        $password = !empty($request->password) ? $request->password : null;
        $rePassword = !empty($request->re_password) ? $request->re_password : null;
        if(empty($password) || empty($rePassword))
            return redirect::back()->withErrors('Invalid inputs');
        
        if($password !== $rePassword)
            return redirect::back()->withErrors('Password do not matched');
        
        $action = 'user_create';

        $payload = [
            'action'=> $action,
            'email'=> $user->email,
            'first_name'=> $user->first_name,
            'last_name'=> $user->last_name,
            'password'=>$password
        ];


        $response = Notify::saveData($action, $payload);
        if(!$response)
            return redirect::back()->withErrors('Unable to update the password');

        return redirect('user/wifi-password')->with('message', 'Password Updated Successfully!');
    }

   
}
