<?php

namespace App\Http\Controllers\UserView;

use App\Http\Controllers\Controller;
use App\Models\Admin\Leave;
use App\Models\Admin\Project;
use App\Models\User;
use App\Services\BonusService;
use App\Services\Calendar;
use App\Services\ProjectService;
use Auth;

class UserOvertimeController extends Controller
{

    public function index()
    {
        $user = Auth::user();

        $month = date('m');
        $year = date('Y');

        $obj = new Calendar();
        $data = $obj->getCalendar($month, $year);

        foreach ($data['days'] as $index => $day) {
            if ($day) {
                $response = Leave::isOnLeaveToday($user->id, $day['date']);
                if ($response) {
                    $data['days'][$index]['on_leave'] = 'true';
                } else {
                    $data['days'][$index]['on_leave'] = 'false';
                };
                $bonus = BonusService::checkUserBonusDay($user->id, $day['date']);

                if ($bonus) {
                    $data['days'][$index]['bonus'] = $bonus;
                } else {
                    $data['days'][$index]['bonus'] = [];
                };
            }
        }

        // echo "<pre>";
        // dd($data);
        // die;
        // $str = "sdsfas'\%&*()";
        // $addStr = addslashes($str);
        // echo $addStr;
        // echo json_encode($str, JSON_UNESCAPED_SLASHES);
        // die;

        $projectsCollection = Project::all();
        $projects = $projectsCollection->toArray();
        $week = ProjectService::getWeekArray();
        $months = $this->getMonthsArray();

        return view('pages.userView.overtime-allowance', compact('user', 'months', 'week', 'projects', 'data'));
    }

    private function getMonthsArray()
    {
        $months = [date('M Y')];

        for ($i = 1; $i < 5; $i++) {
            array_push($months, date('M Y', strtotime("-$i month")));
        }
        return $months;

    }

    // private function getCalendarArray($month, $year){
    //     if($month < 1 || $month > 12){
    //         throw new Exception("Invalid Month");
    //     }

    //     $start_date = $year.'-'.$month.'-01';
    //     $no_of_days = date("t", strtotime($start_date));

    //     $calendar = [];
    //     $currentArray = $this->getWeekArray();
    //     for($day = 1; $day <= $no_of_days; $day++){
    //         $name_of_the_day = date('D', strtotime($year.'-'.$month.'-'.$day));
    //         $number_of_the_day = date('N', strtotime($year.'-'.$month.'-'.$day));
    //         if(strlen($day) === 1){
    //             $day= "0".$day;
    //         }
    //         $currentArray[$name_of_the_day] = $year.'-'.$month.'-'.$day;
    //         if($number_of_the_day%7 == 0 && $no_of_days != $day){
    //             $calendar[] = $currentArray;
    //             $currentArray = $this->getWeekArray();
    //         }
    //     }
    //     $calendar[] = $currentArray;

    //     return $calendar;
    // }

    // private function getWeekArray(){
    //     return ['Mon' =>0, 'Tue' =>0, 'Wed' =>0, 'Thu' =>0, 'Fri' =>0, 'Sat' =>0, 'Sun' =>0];
    // }
}
