<?php

namespace App\Http\Controllers\UserView;

use App\Http\Controllers\Controller;
use App\Models\Admin\Appraisal;
use App\Models\Admin\AssetAssignmentDetail;
use App\Models\Admin\Bonus;
use App\Models\Admin\CalendarYear;
use App\Models\Admin\CmsProject;
use App\Models\Admin\DashboardEvent;
use App\Models\Admin\FinancialYear;
use App\Models\Admin\Leave;
use App\Models\Admin\LoanRequest;
use App\Models\Admin\News;
use App\Models\Admin\ProfileProject;
use App\Models\Admin\Project;
use App\Models\Admin\ProjectResource;
use App\Models\Admin\Technology;
use App\Models\Appraisal\AppraisalBonusType;
use App\Models\Appraisal\AppraisalComponentType;
use App\Models\Audited\AdhocPayments\AdhocPaymentComponent;
use App\Models\Loan;
use App\Models\LoanEmi;
use App\Models\Month;
use App\Models\Timesheet;
use App\Models\TimesheetDetail;
use App\Models\Transaction;
use App\Models\User;
use App\Models\UserTechnologies;
use App\Models\UserView;
use App\Services\CtcService;
use App\Services\FeedbackService;
use App\Services\Leave\UserLeaveService;
use App\Services\NewLeaveService;
use App\Services\Payroll\TDSService;
use App\Services\ProjectService;
use Auth;
use DB;
use Exception;
use Illuminate\Http\Request;
use Input;
use Redirect;
use Session;
use App\Services\SalaryService;

class UserViewController extends Controller
{
    public function showDashboard($id = null)
    {
        if ($id == null) {
            $user = \Auth::user();
            $id = $user->id;
        } else {
            $user = User::find($id);
        }
        if (!$user) {
            return redirect('/dashboard')->withErrors('User not found.');
        }

        $userId = $user->id;
        $date = date('Y-m-d');
        $year = date('Y');
        $calendarYear = CalendarYear::where('year', $year)->first();
        $holidays = DB::table('non_working_calendar')->whereDate('date', '>=', (date('Y-01-01')))->whereDate('date', '<=', (date('Y-12-31')))->orderBy('date')->get();
        $active = DB::table('non_working_calendar')->whereDate('date', '>=', $date)->orderBy('date')->first();
        // $available = UserLeaveService::getUserRemainingLeaves($user->id);
        $consumed = UserLeaveService::getUserConsumedLeaveBalance($user->id);
        $allowed = UserLeaveService::getUserAllowedLeaveBalance($user->id);

        // $avaliableSickLeaves = $available['sick'];
        $consumedSickLeaves = (-1) * $consumed['sick'];
        $totalSickLeaves = $allowed['sick'];
        // $avaliablePaidLeaves = $available['paid'];
        $consumedPaidLeaves = (-1) * $consumed['paid'];
        $totalPaidLeaves = $allowed['paid'];

        $carryForwardLeaves = NewLeaveService::getCarryForwardLeaves($user->id);

        $year = date('Y');
        $yearObj = CalendarYear::where('year', $year)->first();

        if (!$yearObj) {
            return redirect::back()->withErrors('Year not found');
        }
        $userRemainingLeaveDetail = UserLeaveService::getUserAvailableLeaveBalance($yearObj->year, $user->id);

        $result = UserView::payslip($id, 5);
        $nextLeave = Leave::where('user_id', $id)->where('status', 'approved')->where('start_date', '>', $date)->orderBy('start_date')->first();
        $bdays = UserView::bdate(5);
        $news = News::orderBy('date', 'Desc')->take(5)->get();
        $events = DashboardEvent::where('user_id', $id)->where('date', '>', $date)->orderBy('date')->orderBy('time')->take(5)->get();
        $assets = AssetAssignmentDetail::assignedAssets($id);

        $userProjects = ProjectResource::where('user_id', $userId)->where('end_date', null)->where(function ($query) use ($date, $userId) {
            $query->where('user_id', $userId)
                ->orWhere('end_date', '>=', $date);
        })->get();
        $tlProjects = Project::getUserProjects($user->id);

        return view('pages.userView.user-dashboard', compact('user', 'result', 'holidays', 'active', 'consumedSickLeaves', 'totalSickLeaves', 'consumedPaidLeaves', 'totalPaidLeaves', 'nextLeave', 'bdays', 'news', 'events', 'assets', 'carryForwardLeaves', 'userProjects', 'tlProjects', 'paid_leave', 'sick_leave', 'consumedSickLeaves', 'consumedPaidLeaves', 'totalPaidLeaves', 'totalSickLeaves', 'userRemainingLeaveDetail'));
    }
    public function showSalaryDetails($id = null)
    {
        if ($id == null) {
            $user = \Auth::user();
            $id = $user->id;
        } else {
            $user = User::find($id);
        }
        if (!$user) {
            return redirect('/dashboard')->withErrors('User not found.');
        }

        $curYear = date('Y');
        $financialYearObj = FinancialYear::where('year', $curYear)->first();
        if (!$financialYearObj) {
            return redirect('/dashboard')->withErrors('Financial Year not found.');
        }

        $tdsData = [];
        $tdsSummary = [];

        $tdsResponse = TDSService::getUserTDSSummary($user->id, $financialYearObj->id);
        $tdsData = $tdsResponse['data'];

        if ($tdsResponse['status'] == false) {
            Session::flash('alert-class', $tdsResponse['errors']);
            $tdsSummary = [];
        } else {
            Session::flash('alert-class', '');
            $tdsSummary = TDSService::getUserAnnualSummary($tdsData, $financialYearObj->id, $user->id);
        }

        $appraisalCompType = AppraisalComponentType::whereNotIn('code', ['esi-employeer', 'pf-other', 'pf-employeer'])->get();
        $appraisalBonusType = AppraisalBonusType::all();
        $adhocPaymentComponents = AdhocPaymentComponent::all();
        $monthsArr = FinancialYear::getAllFinancialMonth($financialYearObj->id);

        $tdsId = AppraisalComponentType::where('code', 'tds')->first()->id;

        $monthObj = Month::where('status', 'processed')->where('financial_year_id', $financialYearObj->id)->orderBy('year', 'DESC')->orderBy('month', 'DESC')->first();
        if ($monthObj) {
            $currMonth = $monthObj->month;
        } else {
            $currMonth = date('m');
        }

        $payslips = Transaction::where('user_id',$user->id)->GroupBy('month_id')->OrderBy('month_id','DESC')->limit(15)->get();
        return view('pages.userView.salary-details', compact('payslips','monthsArr', 'user', 'appraisalCompType', 'appraisalBonusType', 'adhocPaymentComponents', 'currMonth', 'tdsData', 'tdsSummary', 'tdsId', 'monthObj', 'financialYearObj'));
    }
    public function showProfile($id = null)
    {
        if ($id == null) {
            $user = \Auth::user();
        } else {
            $user = User::find($id);
        }
        if (!$user) {
            return redirect('/dashboard')->withErrors('User not found.');
        }
        if (!empty($user->profile->keywords)) {
            $keywords = explode(',', $user->profile->keywords);
        } else {
            $keywords = "";
        }
        if (!empty($user->profile->expertise_json)) {
            $skills = json_decode($user->profile->expertise_json, true);
        } else {
            $skills = "";
        }
        if (!empty($user->profile->knowledge)) {
            $knowledge = explode(',', $user->profile->knowledge);
        } else {
            $knowledge = "";
        }
        $colours = ["red", "orange", "yellow", "green", "blue", "purple", "black"];
        return view('pages.userView.profile', compact('user', 'keywords', 'skills', 'colours', 'knowledge'));
    }
    public function showSalary(Request $request, $id = null)
    {
        if ($id == null) {
            $user = \Auth::user();
            $id = \Auth::id();
        } else {
            $user = User::find($id);
        }
        if (!$user) {
            return redirect('/dashboard')->withErrors('User not found.');
        }
        $resultObj = new CtcService;
        $result = $resultObj->downloadUserPayslip($id);

        if (!$result['status'] || empty($result['data'])) {
            return Redirect::back()->withErrors($result['log']);
        }
        $data = $result['data'];
        return view('pages.userView.salary', compact('user', 'data'));
    }
    public function showPayslips($id = null)
    {
        if ($id == null) {
            $user = \Auth::user();
            $id = $user->id;
        } else {
            $user = User::find($id);
        }
        if (!$user) {
            return redirect('/dashboard')->withErrors('User not found.');
        }

        $appraisals = Appraisal::where('user_id', $id)->orderBy('is_active', 'desc')->orderBy('effective_date', 'desc')->paginate(10)->appends(Input::except('page'));

        $result = UserView::payslip($id, -1);
        return view('pages.userView.payslips.index', compact('user', 'result', 'appraisals'));
    }
    public function showAppraisals($id = null)
    {

        // if ($id == null) {
        //     $user = \Auth::user();
        // } else {
        //     $user = User::find($id);
        // }
        if (empty($user)) {
            return redirect::back()->withErrors('Invalid Link Followed');
        }
        $appraisals = Appraisal::where('user_id', $id)->orderBy('is_active', 'desc')->orderBy('effective_date', 'desc')->get();
        if (substr($user->name, -1) == "s") {
            $title = $user->name . '\' Appraisals';
        } else {
            $title = $user->name . '\'s Appraisals';
        }
        return view('pages.userView.appraisals', compact('user', 'appraisals', 'title'));
    }

    public function showLoans($id = null)
    {
        if ($id == null) {
            $user = \Auth::user();
            $id = \Auth::id();
        } else {
            $user = User::find($id);
        }
        if (empty($user)) {
            return redirect::back()->withErrors('Invalid Link Followed');
        }
        $loans = Loan::where('user_id', $id)->get();
        $pendingLoans = LoanRequest::where('user_id', $id)->get();
        return view('pages.userView.loans', compact('user', 'loans', 'pendingLoans'));
    }
    public function showEMI($id)
    {
        $user = \Auth::user();
        $loan = Loan::find($id);
        if ($loan->user->id != \Auth::id() && !(\Auth::user()->isAdmin())) {
            return redirect::back()->withErrors('You can\'t view this link');
        }
        $emis = LoanEmi::where('loan_id', $id)->paginate(12);
        if (empty($emis)) {
            return redirect::back()->withErrors('Invalid Link Followed');
        }
        return view('pages.userView.loanemi', compact('user', 'emis'));
    }
    public function showEMIadmin($id, $lid)
    {
        $loan = Loan::find($lid);
        $user = $loan->user;
        if (!(\Auth::user()->isAdmin())) {
            return redirect::back()->withErrors('You can\'t view this link');
        }
        $emis = LoanEmi::where('loan_id', $lid)->paginate(12);
        if (empty($emis)) {
            return redirect::back()->withErrors('Invalid Link Followed');
        }
        return view('pages.userView.loanemi', compact('user', 'emis'));
    }
    public function showBonuses($id = null)
    {
        if ($id == null) {
            $user = \Auth::user();
            $id = \Auth::id();
            $bonuses = Bonus::where('user_id', $id)->where('status', 'approved')->get();
        } else {
            $user = User::find($id);
            $bonuses = Bonus::where('user_id', $id)->get();
        }
        if (empty($user)) {
            return redirect::back()->withErrors('Invalid Link Followed');
        }
        return view('pages.userView.bonus', compact('user', 'bonuses'));
    }
    public function editProfile($id = null)
    {
        if ($id == null) {
            $user = \Auth::user();
        } else {
            $user = User::find($id);
        }
        // $user = User::find($id);
        if (empty($user)) {
            return redirect::back()->withErrors('Invalid Link Followed');
        }
        if (!empty($user->profile->expertise_json)) {
            $expertise = json_decode($user->profile->expertise_json, true);
        } else {
            $expertise = "";
        }
        $projects = CmsProject::all();
        $profileProjects = ProfileProject::where('profile_id', $id)->select('project_id')->get();

        $profileProjects = $profileProjects->toArray();

        $selectedProjects = [];
        foreach ($profileProjects as $key => $value) {
            array_push($selectedProjects, $value['project_id']);
        }
        return view('pages.userView.profile-edit', compact('user', 'expertise', 'projects', 'selectedProjects'));
    }
    public function showBdays($id = null)
    {
        if ($id == null) {
            $user = \Auth::user();
            $id = \Auth::id();
        } else {
            $user = User::find($id);
        }
        if (empty($user)) {
            return redirect::back()->withErrors('Invalid Link Followed');
        }
        $bdays = UserView::bdate(-1);
        return view('pages.userView.bdays', compact('user', 'bdays'));
    }
    public function showMyEvents($id = null)
    {
        if ($id == null) {
            $user = \Auth::user();
            $id = \Auth::id();
        } else {
            $user = User::find($id);
        }
        if (empty($user)) {
            return redirect::back()->withErrors('Invalid Link Followed');
        }
        $date = date('Y-m-d');
        $events = DashboardEvent::where('user_id', $id)->orderBy('date', 'desc')->orderBy('time')->paginate(10);
        return view('pages.userView.my-events', compact('user', 'events'));
    }
    public function redirectToDashboard()
    {
        return redirect()->to('/user/dashboard');
    }

    public function showSkills($id = null)
    {
        if ($id == null) {
            $user = \Auth::user();
            $id = \Auth::id();
        } else {
            $user = User::find($id);
        }
        if (empty($user)) {
            return redirect::back()->withErrors('Invalid Link Followed');
        }
        $records = UserTechnologies::where('user_id', $id)->with('technology')->orderBy('id', 'DESC')->paginate(9);
        $availableSkills = UserTechnologies::where('user_id', $id)->whereIn('status', ['approved', 'pending'])->pluck('technology_id')->toArray();
        $techlist = Technology::whereNotIn('id', $availableSkills)->get();
        return view('pages.userView.skill', compact('user', 'records', 'techlist'));
    }

    public function saveSkills(Request $request)
    {
        $array = \Request::all();
        $id = \Auth::id();

        $alreadyAssignedTechnology = UserTechnologies::where('user_id', $id)->where('technology_id', $array['technology_id'])->first();
        if ($alreadyAssignedTechnology) {
            return redirect::back()->withErrors("Can't re-assign same Technology");
        }
        $technology = new UserTechnologies();
        $technology->user_id = $id;
        $technology->technology_id = $array['technology_id'];
        $technology->status = "pending";
        if ($technology->save()) {
            return redirect('/user/skill')->with('message', 'Successfully Added');
        } else {
            return redirect::back()->withErrors($technology->getErrors())->withInput();
        }
    }
    public function showTimesheet(Request $request)
    {
        $user = \Auth::user();
        $start_date = '2017-12-01';
        $end_date = '2017-12-24';
        if (empty($request->start_date)) {
            $start_date = date('Y-m-d');
            $day = date('N') - 1;
            $start_date = date('Y-m-d', strtotime($start_date . '-' . $day . ' days'));
            $end_date = date('Y-m-d', strtotime($start_date . '+6 days'));
        } else {
            $start_date = date('Y-m-d', strtotime($request->start_date));
            $end_date = date('Y-m-d', strtotime($request->end_date));

        }

        $data = ProjectService::projectsArray($user->id, $start_date, $end_date);
        $dates = $data['dates'];
        $projects = $data['projects'];
        $projects = json_encode($projects);
        $data[] = !empty($data[0]) ? $data[0] : [];

        $max = date_diff(date_create($start_date), date_create($end_date))->format('%a');
        $date = $start_date;
        $verticalTotals = [];
        for ($i = 0; $i <= $max; $i++) {
            $t = 0;
            $timesheets = Timesheet::where('user_id', $user->id)
                ->where('date', $date)->get();
            foreach ($timesheets as $timesheet) {
                $timesheet_details = TimesheetDetail::where('timesheet_id', $timesheet->id)->get();
                foreach ($timesheet_details as $timesheet_detail) {
                    $t = $t + $timesheet_detail->duration;
                }
            }
            array_push($verticalTotals, $t);
            $date = date('Y-m-d', strtotime($date . ' +1 day'));
        }
        return View('pages/userView/timesheet/index', compact('user', 'dates', 'projects', 'verticalTotals', 'start_date', 'end_date'));
    }
    public function showDetailTimesheet(Request $request)
    {
        $user = \Auth::user();
        $user_id = $user->id;
        $selected_date = $request->selected_date;
        $timesheets = Timesheet::with('user', 'project', 'approver')
            ->where('user_id', $user->id)
            ->where('date', '=', $selected_date)
            ->get();
        $len = count($timesheets);
        if ($len < 1) {

            $projects = ProjectResource::with('user_data')
                ->where('user_id', $user_id)
                ->where('start_date', '<=', $selected_date)
                ->whereNull('end_date')
                ->orWhere('end_date', '>=', $selected_date)
                ->get();
            if (isset($projects)) {
                $this->createTimesheet($projects, $selected_date);

            }
        }
        $timesheets = Timesheet::with('user', 'project', 'approver')
            ->where('user_id', $user->id)
            ->where('date', '=', $selected_date)
            ->get();
        return View('pages/userView/timesheet/show', ['timesheets' => $timesheets, 'selected_date' => $selected_date, 'user' => $user]);
    }
    public function saveTimesheet(Request $request)
    {
        // echo "<pre>";
        // print_r($request->all());
        // die;
        if ($request->user_type == 'user') {
            $temp = $request->hours;
            foreach ($temp as $key => $t) {
                try {
                    $timesheetObj = Timesheet::find($key);
                    $timesheetObj->hours = $t;
                    $timesheetObj->save();
                } catch (Exception $e) {
                    echo $e->getMessage();
                }
            }
        }
        if ($request->user_type == 'approver') {
            $user = \Auth::user();
            $temp = $request->hours;
            foreach ($temp as $key => $t) {
                try {
                    $timesheetObj = Timesheet::find($key);
                    $timesheetObj->approved_hours = $t;
                    $timesheetObj->approver_id = $user->id;
                    $timesheetObj->approval_date = date('Y-m-d');
                    $timesheetObj->save();
                } catch (Exception $e) {
                    echo $e->getMessage();
                }
            }
        }

        return redirect::back()->with('message', 'Successfully updated');
    }

    public function createTimesheet($projects, $selected_date)
    {
        // echo "<pre>";
        // print_r($projects->toArray());
        // die;
        foreach ($projects as $project) {
            try {
                $timesheetObj = new Timesheet();
                $timesheetObj->user_id = $project->user_id;
                $timesheetObj->project_id = $project->project_id;
                $timesheetObj->date = $selected_date;
                $timesheetObj->hours = 0;
                $timesheetObj->save();
            } catch (Exception $e) {
                echo $e->getMessage();
                return false;
            }
        }
        return true;

    }
    public function showAssets()
    {
        $assets = [];
        $user = \Auth::user();
        $id = $user->id;

        if (!$user) {
            return redirect('/dashboard')->withErrors('User not found.');
        }

        $assets = AssetAssignmentDetail::assetsHistory($id);
        $prevoiusAssets = AssetAssignmentDetail::getPrevoiusAsset($id);
        $currentAssets = AssetAssignmentDetail::getCurrentAsset($id);
        return view('pages.userView.assets', compact('user', 'currentAssets', 'prevoiusAssets'));
    }
    public function showFeedback()
    {
        $user = \Auth::user();
        $id = $user->id;

        if (!$user) {
            return redirect('/dashboard')->withErrors('User not found.');
        }
        $feedbackMonthDetails = FeedbackService::getUserMonthList($user->id);
        return view('pages.userView.feedback', compact('user', 'feedbackMonthDetails'));
    }
    public function projectDetailTimesheet($id, Request $request)
    {
        $user = \Auth::user();
        $start_date = '2017-12-01';
        $end_date = '2017-12-24';
        if (empty($request->start_date)) {
            $start_date = date('Y-m-d');
            $day = date('N') - 1;
            $start_date = date('Y-m-d', strtotime($start_date . '-' . $day . ' days'));
            $end_date = date('Y-m-d', strtotime($start_date . '+6 days'));
        } else {
            $start_date = date('Y-m-d', strtotime($request->start_date));
            $end_date = date('Y-m-d', strtotime($request->end_date));

        }
        $project = Project::find($id);
        $data = ProjectService::projectsArrayWithProject($user->id, $start_date, $end_date, $id);
        $dates = $data['dates'];
        $projects = $data['projects'];
        if (count($projects) < 1) {
            return redirect::back()->withErrors(['Invalid project id.'])->withInput();
        }
        $projects = json_encode($projects);
        $data[] = !empty($data[0]) ? $data[0] : [];

        return View('pages/userView/timesheet/details', compact('user', 'dates', 'projects', 'project', 'start_date', 'end_date'));
    }

    public function showUserPayslip($monthId)
    {
        $user = \Auth::user();
        if (!$user) {
            return redirect()->back()->withErrors('User not found.');
        }
        $data = SalaryService::_userPlayslipData($monthId, $user->id);
        return view('pages.admin.prep-salary.prep-salary.user-payslip', $data);
    }

    public function downloadUserPayslip($monthId)
    {
        $user = \Auth::user();
        if (!$user) {
            return redirect()->back()->withErrors('User not found.');
        }
        $data = SalaryService::_userPlayslipData($monthId, $user->id);
        $pdf = \PDF::loadView('pages.admin.prep-salary.prep-salary.pdf.payslip', $data);
        return $pdf->download('payslip-' . $user->id . '.pdf', array('Attachment' => 0));
    }

    public function showUserTDS($monthId)
    {
        if($monthId < 19)
        {
            return redirect()->back()->withErrors('Data not available.');
        }
        $user = \Auth::user();
        if (!$user) {
            return redirect()->back()->withErrors('User not found.');
        }
        $data = SalaryService::_userTDSData($monthId, $user->id);
        return view('pages.tds.tds-payslip', $data);
    }

    public function downloadUserTDS($monthId)
    {
        if($monthId < 19)
        {
            return redirect()->back()->withErrors('Data not available.');
        }
        $user = \Auth::user();
        if (!$user) {
            return redirect()->back()->withErrors('User not found.');
        }
        $data = SalaryService::_userTDSData($monthId, $user->id);
        $pdf = \PDF::loadView('pages.tds.pdf.tds-breakdown', $data);
        return $pdf->download('tds-breakdown-' . $user->id . '.pdf', array('Attachment' => 0));
    }
}
