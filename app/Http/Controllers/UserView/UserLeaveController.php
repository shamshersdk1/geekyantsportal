<?php

namespace App\Http\Controllers\UserView;

use App\Events\Leave\LeaveApproved;
use App\Events\Leave\LeaveCancelled;
use App\Http\Controllers\Controller;
use App\Models\Admin\CalendarYear;
use App\Models\Admin\Leave;
use App\Models\Leave\UserLeaveTransaction;
use App\Models\User;
use App\Services\LeaveService;
use App\Services\Leave\UserLeaveService;
use App\Services\NewLeaveService;
use Auth;
use Illuminate\Http\Request;
use Redirect;
use Validator;

class UserLeaveController extends Controller
{
    //old name : showLeaves
    public function index($id = null)
    {
        if ($id == null) {
            $user = \Auth::user();
            $id = \Auth::id();
            $jsonclients = json_encode("");
        } else {
            $user = User::find($id);
            $jsonclients = Company::getClients();
        }
        if (empty($user)) {
            return redirect::back()->withErrors('Invalid Link Followed');
        }

        $data = LeaveService::getUserReportee($user->id);
        $result = LeaveService::getUserLeaveReport($user->id);

        $consumedSickLeaves = $result['consumedSickLeaves'];
        $totalSickLeaves = $result['totalSickLeaves'];
        $consumedPaidLeaves = $result['consumedPaidLeaves'];
        $totalPaidLeaves = $result['totalPaidLeaves'];

        $avaliableSickLeaves = $totalSickLeaves - $consumedSickLeaves;
        $avaliablePaidLeaves = $totalPaidLeaves - $consumedPaidLeaves;
        $leaves = Leave::with('approver')->where('user_id', $id)->orderBy('start_date', 'desc')->get();
        return view('pages.userView.leaves', compact('user', 'avaliableSickLeaves', 'consumedSickLeaves', 'totalSickLeaves', 'avaliablePaidLeaves', 'consumedPaidLeaves', 'totalPaidLeaves', 'leaves', 'jsonclients', 'data'));
    }

    public function indexNew($id = null)
    {
        if ($id == null) {
            $user = \Auth::user();
            $id = \Auth::id();
            $jsonclients = json_encode("");
        } else {
            $user = User::find($id);
            $jsonclients = Company::getClients();
        }
        if (empty($user)) {
            return redirect::back()->withErrors('Invalid Link Followed');
        }

        $data = NewLeaveService::getUserReportee($user->id);
        $result = NewLeaveService::getUserLeaveReport($user->id);

        $consumed = UserLeaveService::getUserConsumedLeaveBalance($user->id);
        // $available = UserLeaveService::getUserRemainingLeaves($user->id);
        $allowed = UserLeaveService::getUserAllowedLeaveBalance($user->id);

        // $avaliableSickLeaves = $available['sick']; //UserLeaveService::getUserRemainingLeave($user->id, 'sick');
        $totalSickLeaves = $allowed['sick']; //UserLeaveService::getAllowedLeave($user->id, 'sick');
        // $avaliablePaidLeaves = $available['paid']; //UserLeaveService::getUserRemainingLeave($user->id, 'paid');
        $totalPaidLeaves = $allowed['paid']; //UserLeaveService::getAllowedLeave($user->id, 'paid');

        $consumedSickLeaves = (-1) * $consumed['sick'];
        $consumedPaidLeaves = (-1) * $consumed['paid'];

        $carryForwardLeaves = NewLeaveService::getCarryForwardLeaves($user->id);

        // $consumedSickLeaves = $totalSickLeaves - $avaliableSickLeaves;
        // $consumedPaidLeaves = $totalPaidLeaves - $avaliablePaidLeaves;
        $optionalLeaves = UserLeaveService::getOptionaLeaves();
        $userOptionalLeave = UserLeaveService::checkUserOptionaLeave($user->id);

        $currentDate = date('Y-m-d');

        $year = date('Y');
        $yearObj = CalendarYear::where('year', $year)->first();

        if (!$yearObj) {
            return redirect::back()->withErrors('Year not found');
        }
        $userRemainingLeaveDetail = UserLeaveService::getUserAvailableLeaveBalance($yearObj->year, $user->id);

        $leaveCreditDetails = UserLeaveTransaction::where('calendar_year_id', $yearObj->id)->where('user_id', $user->id)->orderBy('created_at', 'desc')->get();

        $leaveCredit = [];
        if ($leaveCreditDetails) {
            $leaveCredit = $leaveCreditDetails;
        }

        $leaves = Leave::with('approver')->where('user_id', $id)->orderBy('start_date', 'desc')->get();
        return view('pages.userView.leaves-new', compact('user', 'consumedSickLeaves', 'totalSickLeaves', 'consumedPaidLeaves', 'totalPaidLeaves', 'leaves', 'jsonclients', 'data', 'carryForwardLeaves', 'optionalLeaves', 'userOptionalLeave', 'consumed', 'allowed', 'currentDate', 'userRemainingLeaveDetail', 'leaveCredit'));
    }

    // api
    public function saveOverlapRequest(Request $request)
    {
        $user = Auth::user();
        if ($user->id != $request->user_id) {
            return response()->json('You dont have the required permissions', 403);
        }
        $overlapRequest = new LeaveOverlapRequest();
        $overlapRequest->user_id = $user->id;
        $overlapRequest->project_id = $request->project_id;
        $overlapRequest->leave_id = $request->leave_id;
        $overlapRequest->reason = $request->reason;
        if ($overlapRequest->save()) {
            return response()->json('Successfully saved', 200);
        } else {
            return response()->json('Unable to save', 400);
        }
    }
    public function leaveStatus(Request $request, $id)
    {
        $leave = Leave::find($id);
        if ($leave) {
            $leave->approver_id = Auth::id();
            $leave->action_date = date('Y-m-d');
            if ($request->approve) {
                $leave->status = "approved";
                if ($leave->save()) {
                    if (!empty($request->client)) {
                        $email = Company::where('name', $request->client)->first()->email;
                        // Leave::notifyClient($email, $leave);
                    }
                    // Leave::notifyLeaveAction($leave->id);
                    event(new LeaveApproved($leave->id));
                    return redirect::back()->with('message', 'Request Approved');
                } else {
                    return redirect::back()->withErrors(['Something went wrong.'])->withInput();
                }
            } elseif ($request->cancel) {
                $leave->status = "cancelled";
                if ($leave->save()) {
                    // Leave::notifyLeaveAction($leave->id);
                    event(new LeaveCancelled($leave->id));
                    return redirect::back()->with('message', 'Request cancelled');
                } else {
                    return redirect::back()->withErrors(['Something went wrong.'])->withInput();
                }
            }
        } else {
            return redirect::back()->withErrors(['Leave not found.'])->withInput();
        }
    }

    // api
    public function checkLeaveProjects(Request $request)
    {
        $user = User::find($request->id);
        if ($user) {
            $result = UserView::leaveProjects($request);
            $data = $result['data'];
            if ($result['count'] > 0) {
                $data['dates'] = $result['dates'];
                $data['code'] = $result['code'];
            }
            return response()->json($data, 200);
        } else {
            return response()->json('User not found', 404);
        }
    }

    // api
    public function saveOverlapLeave(Request $request)
    {
        $user = Auth::user();
        if ($user->id != $request->user_id) {
            return response()->json('You dont have the required permissions', 403);
        }
        $validator = Validator::make($request->all(), [
            'start_date' => 'required',
            'end_date' => 'required',
            'type' => 'required|in:sick,paid,unpaid',
        ]);

        if ($validator->fails()) {
            $message = $validator->errors();
            return response()->json($message, 400);
        }
        $input = $request->all();
        $input['user_id'] = $user->id;
        $skip = 0;
        $response = Leave::validateLeave($user, $input, $skip);
        if (!$response['status']) {
            $message = $response['errors'];
            return response()->json($message, 400);
        }
        $result = Leave::saveData($input);
        if ($result['status']) {
            return response()->json(['user_id' => $user->id, 'leave_id' => $result['id']], 200);
        } else {
            $message = $result['errors'];
            return response()->json($message, 400);
        }
    }
}
