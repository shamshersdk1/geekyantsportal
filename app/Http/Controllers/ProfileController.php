<?php 
namespace App\Http\Controllers;

use App\Models\Profile;
use App\Models\Admin\Project;
use App\Models\Admin\CmsProject;
use MandrillMail;
//use Weblee\Mandrill\Mail;
// use Mail;
use App\Http\Requests;
use Redirect;
use Illuminate\Http\Request;
use App\Mail\ContactUsMail;
use Illuminate\Support\Facades\Mail;
use App\Models\User;

class ProfileController extends Controller {
	
	public function index() {

		$profiles = Profile::with('user')->where('is_active',1)->get();

		// $profiles = Profile::with('user')->whereHas('user', function ($query) {
		//     $query->where('is_active', '=', 1);
		// })->get();

		return \View::make('pages.team.index', compact('profiles'));
	}

	public function emailSetting() {
		$password = 'abhi';
		$username = 'as@gm.com';

		$key = 'password to (en/de)crypt';
		$string = $password;

		$iv = mcrypt_create_iv(
		    mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC),
		    MCRYPT_DEV_URANDOM
		);

		$encrypted = base64_encode(
		    $iv .
		    mcrypt_encrypt(
		        MCRYPT_RIJNDAEL_128,
		        hash('sha256', $key, true),
		        $string,
		        MCRYPT_MODE_CBC,
		        $iv
		    )
		);

		echo $encrypted."encrypted";

		$data = base64_decode($encrypted);
		$iv = substr($data, 0, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC));

		$decrypted = rtrim(
		    mcrypt_decrypt(
		        MCRYPT_RIJNDAEL_128,
		        hash('sha256', $key, true),
		        substr($data, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC)),
		        MCRYPT_MODE_CBC,
		        $iv
		    ),
		    "\0"
		);

		return $decrypted;
	}

	public function saveEmailCredential(Request $request) {
		$password = 'abhi';
		$username = 'as@gm.com';

		$key = 'password to (en/de)crypt';
		$string = $password;

		$iv = mcrypt_create_iv(
		    mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC),
		    MCRYPT_DEV_URANDOM
		);

		$encrypted = base64_encode(
		    $iv .
		    mcrypt_encrypt(
		        MCRYPT_RIJNDAEL_128,
		        hash('sha256', $key, true),
		        $string,
		        MCRYPT_MODE_CBC,
		        $iv
		    )
		);

		$setting = Setting::save();

		
	}

	public function view($id) {

		$profileData = [];
		$projects = CmsProject::all();

		$profile = Profile::with('user', 'project')->whereHas('user', function ($query) {
				    $query->where('user_profile.is_active', '=', 1);
				})->where('profile_url', $id)->first();
		if(!$profile){
			return redirect('/team');
		}
		if($profile)
			$profileData = $profile->toArray();
		
		if(!empty($profileData['keywords']))
			$profileData['keywords'] = explode(',' ,$profileData['keywords']);
		if(!empty($profileData['knowledge']))
			$profileData['knowledge'] = explode(',', $profileData['knowledge']);
		if(!empty($profileData['expertise_json']))
			$profileData['expertise'] = json_decode($profileData['expertise_json'],true);

		if($projects)
			$projects = $projects->toArray();
		//$profileData['achievements'] = "###";//json_decode($profileData['expertise_json']);
		// /return $profile;
		return \View::make('pages.team.view', compact('profileData','projects'));
	}

	public function sendMail(Request $request) {
		
        try {

            if( empty($request->get('email')) || empty($request->get('message')) )
            	return Redirect::to('/')->withErrors(['Invalid data']);
            
           	$phone = $request->has('phone') ? $request->get('phone'): null;

           	$name = $request->has('name') ? $request->get('name'): null;

           	$data['phone'] = $phone;
           	$data['name'] = $name;
           	$data['email'] = $request->get('email');
           	$data['content'] = $request->get('message');
           	$data['route'] = $request->has('route') ? $request->get('route') : NULL;

            Mail::send('emails.message', [
					                        'content' => $data['content'],
					                        'phone' => $data['phone'],
					                        'email' => $data['email'],
					                        'name' => $data['name'],
					                    ], function($message) use ($data) {

                $message->to('leads@sahusoft.com', 'Leads')
                		->subject(!empty($data['route']) ? $data['route'] : 'Contact Us - GeekyAnts');
                $message->replyTo($data['email']);
                $message->from('support@geekyants.com');
            });

            // Mail::to('leads@sahusoft.com')->send(new ContactUsMail($data));
            // Mail::to('faiz@sahusoft.com')
            // 		->replyTo('faizz.af@gmail.com', 'Faiz')
            // 		->send(new ContactUsMail($data));

            if($request->has('number'))
            	return redirect()->back()->with('message', 'Thank you for your mail.');

            return redirect()->back()->with('message', 'Thank you for your mail.');
        }
        catch (\Exception $e) {

            return redirect('/')->withErrors('Something went wrong.');
        }
    }

}
