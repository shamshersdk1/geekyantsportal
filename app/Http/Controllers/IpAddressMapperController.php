<?php

namespace App\Http\Controllers;

use App\Models\Admin\IpAddressMapper;
use App\Models\Admin\IpAddressRange;
use Excel;

class IpAddressMapperController extends Controller
{
    public function getWifiMacCSV()
    {

        $IpAddressMapperObjs = IpAddressMapper::all();
        $data = [];
        foreach ($IpAddressMapperObjs as $IpAddressMapperObj) {
            if(!$IpAddressMapperObj->userIp->user)
                continue;

            $employee_id = $IpAddressMapperObj->userIp->user->employee_id;
            $macAddress = null;
            if ($IpAddressMapperObj->reference_type == 'App\Models\Admin\AssetMeta' && $IpAddressMapperObj->reference) {
                $macAddress = $IpAddressMapperObj->reference->value;
            } elseif ($IpAddressMapperObj->reference_type == 'App\Models\Admin\MyNetworkDevice') {
                $macAddress = $IpAddressMapperObj->reference->mac_address;
            }

            $data[] = array(
                    'desc'=>$employee_id . '#'.$IpAddressMapperObj->id,
                    'mac'=>strtoupper(str_replace(":","-",$macAddress))
                );
        }
        ob_end_clean();
        ob_start();
        Excel::create('WifiMacXLS', function ($excel) use ($data) {

            $excel->sheet('sheet1', function ($sheet) use ($data) {

                $sheet->loadView('excel.get-wifi', ['data' => $data]);
            });
        })->export('xls');

    }

    public function getUserAssetIp()
    {
        $getwayIp = "10.0.1.1";
        $IpAddressMapperObjs = IpAddressMapper::all();
        foreach ($IpAddressMapperObjs as $IpAddressMapperObj) {
            $range_id = IpAddressRange::findIprange($IpAddressMapperObj->userIp->ip_address);
            $username = $IpAddressMapperObj->userIp->user?$IpAddressMapperObj->userIp->user->name:'GeekyAnts';
            $employee_id = $IpAddressMapperObj->userIp->user?$IpAddressMapperObj->userIp->user->employee_id:'NA';
            $macAddress = null;
            $deviceName = '';
            $assetCategory = '';
            $assetName = '';

            if ($IpAddressMapperObj->reference_type == 'App\Models\Admin\AssetMeta' && $IpAddressMapperObj->reference) {
                $macAddress = $IpAddressMapperObj->reference->value;
                $assetName = $IpAddressMapperObj->reference->asset->name;
                $assetCategory = $IpAddressMapperObj->reference->asset->assetCategory->name;

            } elseif ($IpAddressMapperObj->reference_type == 'App\Models\Admin\MyNetworkDevice') {
                $macAddress = $IpAddressMapperObj->reference->mac_address;
                $assetName = $IpAddressMapperObj->reference->name;
                $assetCategory = 'NetworkDevice';
            }
            $deviceName = $username . ' - ' . $assetCategory . ' - ' . $assetName;

            echo $employee_id . ','
            . $deviceName . ','
            . $macAddress . ','
            . $IpAddressMapperObj->userIp->ip_address . ','
                . $getwayIp . ','
                . $range_id . "\n";
        }
    }
    public function getLastUpdatedAt()
    {
        $obj = IpAddressMapper::orderBy('updated_at', 'desc')->first();

        if ($obj) {
            echo $obj->updated_at;
        }

    }
    public function store($data)
    {
        $response = [
            'result' => "",
            'status' => true,
            'message' => "Assigned",
        ];

        $obj = new IpAddressMapper;
        $obj->reference_type = $data['reference_type'];
        $obj->reference_id = $data['reference_id'];
        $obj->user_ip_address_id = $data['user_ip_address_id'];
        if (!$obj->save()) {
            $response['status'] = false;
            $response['message'] = $obj->getErrors();
            return $response;
        }
        return $response;

    }
    public function destroy($id)
    {
        $IpAddressMapper = IpAddressMapper::where('id', $id)->delete();
        if (!$IpAddressMapper) {
            return redirect()->back()->withErrors('Uable to delete records');

        }
        return redirect()->back()->with('message', 'Successfully deleted!');
    }
}
