<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Input;
use Validator;
use Redirect;
use App\Models\User;
use Auth;
use View;

class LoginController extends Controller {

	public function login()
	{

		//\Log::info('inpu'.print_r(Input::all(),true));
		$rules = array(
		    'email'    => 'required|email', // make sure the email is an actual email
		    'password' => 'required' // password can only be alphanumeric and has to be greater than 3 characters
		);
		// run the validation rules on the inputs from the form
		//$validator = Validator::make(Input::all(), $rules);

		$validator =  Validator::make(Input::all(), $rules);

		// if the validator fails, redirect back to the form
		if ($validator->fails()) {

		    return Redirect::to('admin')
		        ->withErrors($validator) // send back all errors to the login form
		        ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
		} else {

		    //Create our user data for the authentication
		    $userdata = array(
		        'email'     => Input::get('email'),
		        'password'  => Input::get('password')
		    );

		    $user = User::where('email',Input::get('email'))->first();
		    if( $user->is_active != 1 ) {
		    	return Redirect::back()->withErrors(['You account is not active, ask your HR to activate it']);
		    }
		    // attempt to do the login
		    if (Auth::attempt($userdata)) {

                /*if(User::isAdmin($user->id)) {
                    return Redirect::to('/admin');
                }
                if(User::isAuthor($user->id)) {
                    return Redirect::to('/dashboard');
                }*/

                return redirect('/admin/dashboard');

		    } else {
		    	\Log::info('error');
		        // validation not successful, send back to form
		        return Redirect::back()->withErrors(['Invalid Username/Password']);

		    }
		}
	}

	public function dologout() {
		Auth::logout(); // log the user out of our application
    	return Redirect::to('admin'); // redirect the user to the login screen
	}

	public function showLogin( Request $request) {
		if(!empty($request->user))
		{ 
			if ( $request->user->isAdmin() )
			{	
				return Redirect::to('/admin/dashboard');
			} else {
				return Redirect::to('/user/dashboard');
			}
		}
		return View::make('pages/admin/admin-login');
	}


	public function signup(Request $request) {

		$data = $request->all();

        try {

            $rules = array(
			    'email'    		  => 'required|email', // make sure the email is an actual email
			    'password' 		  => 'required', // password can only be alphanumeric and has to be greater than 3 characters
			    'confirmPassword' => 'required',
			    'name' 			  => 'required'
			);

			$validator =  Validator::make($request->all(), $rules);

			if ($validator->fails()) {

			    return Redirect::back()
			        ->withErrors($validator)->withInput(); // send back all errors to the login form
			}

			// If not a Sahusoft email
			$isSahusoftEmail = substr($data['email'], -13);
			if($isSahusoftEmail != '@sahusoft.com')
				return Redirect::back()->withErrors(['Not the user of SahuSoft']);

			// If duplicate email exists
            $email = $request->email;
            $exist = User::where('email', $email)->first();
            if($exist)
            	return Redirect::back()->withErrors(['Account with email "'.$email.'" already exist'])->withInput();

			// If password does not match
            if($request->password != $request->confirmPassword) {
            	return Redirect::back()->withErrors(['Password mismatch'])->withInput();
            }

            $data = $request->all();

            $response  = User::saveData($data);

            if(!$response['success'])
            	return Redirect::back()->withErrors($response['errorMessage'])->withInput();


            return redirect('admin')->with('message', 'Successfully registered.');
        }
        catch (\Exception $e) {
            ini_set('memory_limit', '-1');
        	return response()->json(['error' => 'Unable to create user\'s account.'], 401);
        }
    }
}
