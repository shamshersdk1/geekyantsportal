<?php

namespace App\Http\Controllers\Mobile;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\LeaveService;
use App\Models\Admin\Leave;
use App\Models\User;
use App\Models\UserView;
use App\Models\SystemSetting;

use Auth;
use Redirect;
use Validator;
use View;
use Input;
use DateTime;


class MobileDashboardController extends Controller
{
    public function home( Request $request) {  
        $user = Auth::user();
        if (empty($user)) {
            return redirect::back()->withErrors('Invalid Link Followed');
        }
            $result=UserView::leaves($user);
            $consumedSickLeaves = $result['consumedSickLeaves'];
            $totalSickLeaves = $result['totalSickLeaves'];
            $consumedPaidLeaves = $result['consumedPaidLeaves'];
            $totalPaidLeaves = $result['totalPaidLeaves'];
            $avaliableSickLeaves = $totalSickLeaves - $consumedSickLeaves;
            $avaliablePaidLeaves = $totalPaidLeaves - $consumedPaidLeaves;
            $userUnpaidLeavesCount =  $result['consumedUnpaidLeaves'];
            $leaves=Leave::findUserLeaves($user->id);
            return View::make('pages/mobile/dashboard', compact('user', 'avaliableSickLeaves', 'consumedSickLeaves', 'totalSickLeaves', 'avaliablePaidLeaves', 'consumedPaidLeaves', 'totalPaidLeaves', 'leaves','userUnpaidLeavesCount'));

    }     
    public function leave( Request $request) {   
        return View::make("pages/mobile/leave");
    }

    public function findDays()
    {   
        \Log::info('date verify');  
        $start = Input::get('start');
        $start_date=date( "Y-m-d", strtotime($start) );
        $end = Input::get('end');
        $end_date=date( "Y-m-d", strtotime($end) );
        $days = LeaveService::calculateWorkingDays($start_date , $end_date);
        return $days;
    }    

    
    public function requestLeave(Request $request) 
    {
         $user = Auth::user();

        if (!$user) {
            return redirect::back()->withErrors('User Not Found')->withInput($request->input());
        }
        $validator = Validator::make($request->all(), [
            'start_date' => 'required',
            'end_date' => 'required',
            'reason' => 'required',
            'type' => 'required|in:sick,paid,unpaid',
        ]);

        if ($validator->fails()) {
            return redirect::back()->withErrors($validator->errors())->withInput($request->input());
        }
        
        $input = $request->all();

        $input['user_id'] = $user->id;
        $skip=0;
        $response  = Leave::validateLeave($user, $input, $skip);
        if (!$response['status']) {
            \Log::info("fails");
            return redirect::back()->withErrors($response['errors'])->withInput($request->input());
        }


        /*if($request->type == "sick"){

            if(!isset($user->profile->join_date)) {
                return redirect::back()->withErrors("Joining Date is not avaliable")->withInput($request->input());
            }

            $response = Leave::validateAndSaveSickLeave($user, $input);
            if(!$response['status']) {
                return redirect::back()->withErrors($response['message'])->withInput($request->input());
            }
            return redirect('/admin/employee-leave/history')->with('message', $response['message']);
        }
        elseif ($request->type == "paid") {

            if(!isset($user->profile->confirmation_date)) {
                return redirect::back()->withErrors("Confirmation Date is not avaliable")->withInput();
            }

            $response = Leave::validateAndSavePaidLeave($user, $input);
            if(!$response['status']) {
                return redirect::back()->withErrors($response['message'])->withInput();
            }
            return redirect('/admin/employee-leave/history')->with('message', $response['message']);

        }
        elseif ($request->type == "unpaid") {*/

            $result = Leave::saveData($input);
        if ($result['status']) {
            return View::make('pages/mobile/confirmation');
        } else {
            return redirect::back()->withErrors($result['errors'])->withInput();
        }
    /*} else {
        return redirect::back()->withErrors("Please select a Leave type")->withInput();
    }*/
    
    }
}
