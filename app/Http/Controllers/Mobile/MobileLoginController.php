<?php 
namespace App\Http\Controllers\Mobile;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Input;
use Validator;
use Redirect;
use App\Models\User;
use Auth;
use View;

class MobileLoginController extends Controller {

	public function login()
	{
		//\Log::info('inpu'.print_r(Input::all(),true));
		$rules = array(
		    'email'    => 'required|email', // make sure the email is an actual email
		    'password' => 'required' // password can only be alphanumeric and has to be greater than 3 characters
		);
		// run the validation rules on the inputs from the form
		//$validator = Validator::make(Input::all(), $rules);

		$validator =  Validator::make(Input::all(), $rules);

		// if the validator fails, redirect back to the form
		if ($validator->fails()) {


		    return Redirect::to('/mobile')
		        ->withErrors($validator) // send back all errors to the login form
		        ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
		} else {

		    //Create our user data for the authentication
		    $userdata = array(
		        'email'     => Input::get('email'),
		        'password'  => Input::get('password')
		    );

		    $user = User::where('email',Input::get('email'))->first();

		    if(!$user){
		    	return Redirect::to('/mobile')->withErrors(['Invalid Username/Password']);
		    }

			    if( $user->is_active != 1 ) {
			    	return Redirect::to('/mobile')->withErrors(['You account is not active, ask your HR to activate it']);
			    }
			    // attempt to do the login
			    if (Auth::attempt($userdata)) {

	                /*if(User::isAdmin($user->id)) {
	                    return Redirect::to('/admin');
	                }
	                if(User::isAuthor($user->id)) {
	                    return Redirect::to('/dashboard');
	                }*/


	                return redirect('/mobile/dashboard'); 

			    } else {
			    	\Log::info('error');
			        // validation not successful, send back to form
			        return Redirect::to('/mobile')->withErrors(['Invalid Username/Password']);

			    }
		}
	}

	public function dologout() {  //make here redirect  for mobile login page.
		Auth::logout(); // log the user out of our application
		return Redirect::to('/mobile'); // redirect the user to the login screen
	}


	public function showLogin( Request $request) {
		
		if(!empty($request->user))
			return Redirect::to('/mobile/dashboard'); // redirect the user to the login screen
		return View::make('pages/mobile/login');
	}


}
