<?php
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\User;
use App\Models\Social;

use Config;
use Socialite;
use Input;
use Redirect;

use Log;
use App\Helpers\Sys;

class SocialController extends Controller
{
    public function getSocialRedirect( $provider )
    {

        $providerKey = Config::get('services.' . $provider);

        if (empty($providerKey)) {
            return Redirect::back()->with('message', 'No such provider');
        }

        return Socialite::driver( $provider )->with(['hd' => 'geekyants.com'])->redirect();

    }

    public function getSocialHandle( $provider )
    {
        $isMobile = Sys::checkIsMobile();
        if(!$isMobile){
                if (Input::get('error') != '') {
                    return redirect()->to('/admin')->withErrors(['Authentication denied by user.']);
                }

                $user = Socialite::driver( $provider )->user();


                list($username, $domain) = explode('@', $user->email);
                if(trim(strtolower($domain)) != 'geekyants.com'){
                    return redirect()->to('/admin')->withErrors(['Authentication denied by user - Invalid Domain']);
                }

                $message = 'Your account is not active, please ask management to activate it.';
                $socialUser = null;

                //Check is this email present
                $userExists = User::where('email', '=', $user->email)->first();
                
                $email = $user->email;

                if (!$user->email) {
                    $email = 'missing' . str_random(10);
                }

                if (!empty($userExists)) {

                    $socialUser = $userExists;

                } else {
                    
                    $sameSocialId = Social::where('social_id', '=', $user->id)
                        ->where('provider', '=', $provider )
                        ->first();

                    if (empty($sameSocialId)) {

                        //There is no combination of this social id and provider, so create new one
                        $data = [
                                    'email' => $email,
                                    'name' => $user->name,
                                    'password' => bcrypt(str_random(16)),
                                    'role' => 'user',
                                ];  
                        $newUser = User::saveData($data);
                        if ( !$newUser['status'] ) {
                            return redirect()->to('/admin')->withErrors($newUser['log']);
                        }
                        $newUserObj = User::find($newUser['id']);

                        $socialData = new Social;
                        $socialData->social_id = $newUser['id'];
                        $socialData->provider= $provider;
                        $socialData->token= $user->token;
                        $socialData->save();
                        $socialUser = $newUserObj;

                        $message = 'Your account is registered successfully, please ask management to activate your account.';
                    } else {
                        //Load this existing social user
                        $socialUser = $sameSocialId->user;

                    }
                }

                if( $socialUser->is_active != 1 ) {

                    return redirect()->to('/admin')->with('message', $message);
                }

                auth()->login($socialUser, true);
                if ( $socialUser['role'] == 'admin' )
                {
                    return redirect()->to('/admin/dashboard');
                } else {
                    return redirect()->to('/user/dashboard');
                }
                
        }if($isMobile){
                if (Input::get('error') != '') {
                    return redirect()->to('/mobile')->withErrors(['Authentication denied by user.']);
                }
                $user = Socialite::driver( $provider )->user();

                list($username, $domain) = explode('@', $user->email);
                if(trim(strtolower($domain)) != 'geekyants.com'){
                    return redirect()->to('/mobile')->withErrors(['Authentication denied by user - Invalid Domain']);
                }

                $message = 'Your account is not active, please ask management to activate it.';
                $socialUser = null;

                //Check is this email present
                $userExists = User::where('email', '=', $user->email)->first();

                $email = $user->email;

                if (!$user->email) {
                    $email = 'missing' . str_random(10);
                }

                if (!empty($userExists)) {

                    $socialUser = $userExists;

                } else {

                    $sameSocialId = Social::where('social_id', '=', $user->id)
                        ->where('provider', '=', $provider )
                        ->first();

                    if (empty($sameSocialId)) {

                        //There is no combination of this social id and provider, so create new one
                        $data = [
                                    'email' => $email,
                                    'name' => $user->name,
                                    'password' => bcrypt(str_random(16)),
                                    'role' => 'user',
                                ];  
                        $newUser = User::saveData($data);
                        if ( !$newUser['status'] ) {
                            return redirect()->to('/mobile')->withErrors($newUser['log']);
                        }
                        $newUserObj = User::find($newUser['id']);

                        $socialData = new Social;
                        $socialData->social_id = $newUser['id'];
                        $socialData->provider= $provider;
                        $socialData->token= $user->token;
                        $socialData->save();
                        $socialUser = $newUserObj;

                        $message = 'Your account is registered successfully, please ask management to activate your account.';
                    } else {
                        //Load this existing social user
                        $socialUser = $sameSocialId->user;

                    }
                }

                if( $socialUser->is_active != 1 ) {

                    return redirect()->to('/mobile')->with('message', $message);
                }
                auth()->login($socialUser, true);
                return redirect()->to('/mobile/dashboard');
        }        
    }
}

