<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Admin\UserEducation;

class UserEducationController extends ResourceController
{
    protected $model = "\App\Models\Admin\UserEducation";

    public function update($id)
    {
        $input = $this->getRequestBody();
        $query = UserEducation::where('id', $id)->first();
        $res = UserEducation::updateStatus($input['status'], $id);
        if ($res['status']) {
            return $this->failResponse($res['message']);
        }
        return $this->successResponse($res);
    }

    public function getUserEducation($userId)
    {
        $query = UserEducation::with('degree')->where('user_id', $userId)->where('status', "approved")->orderBy('degree_id', 'DESC')->get();
        if (empty($query->toArray())) {
            return $this->failResponse("No record found");
        }
        return $this->successResponse($query);

    }

}
