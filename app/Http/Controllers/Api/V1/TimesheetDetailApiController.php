<?php 

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\V1\BaseApiController;
use App\Models\Timesheet;
use App\Models\TimesheetDetail;
use Validator;
use Auth;

class TimesheetDetailApiController extends BaseApiController {


	public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($request->all(), [
            'timesheet_id' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->failResponse($validator->errors());
        }
		
        $timesheet_id = $input['timesheet_id'];
		$timesheet_details = $input['timesheet_details'];
        $deletedRows = TimesheetDetail::where('timesheet_id',$timesheet_id)
						        ->delete();
        
        if ( count($timesheet_details) > 0 ){
            foreach ($timesheet_details as $timesheet_detail){
                $timesheetDetailObj = new TimesheetDetail();
                $timesheetDetailObj->timesheet_id = $timesheet_id;
                $timesheetDetailObj->start_time = $timesheet_detail['start_time'];
                $timesheetDetailObj->end_time = $timesheet_detail['end_time'] ;
                $timesheetDetailObj->duration = $timesheet_detail['end_time']-$timesheet_detail['start_time'] ;
                $timesheetDetailObj->reason = $timesheet_detail['reason'];
                $timesheetDetailObj->save();
            }
        }
        return 1;
	}
    public function delete($id)
    {
		$user = Auth::user();
		$user_id = $user->id;

        $timesheet_details = TimesheetDetail::where('id',$id)->first();
        $timesheet = Timesheet::where('id',$timesheet_details->timesheet_id)->first();

        if ( $user_id == $timesheet->user_id ) {
            $timesheet_details->delete();
            return 'Deleted';
        }
        return 'Not deleted';
	}
}
