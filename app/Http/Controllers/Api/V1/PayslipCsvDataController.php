<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Api\V1\BaseApiController;

use App\Models\Admin\PayrollRuleForCsv;
use App\Models\Admin\PayslipCsvData;
use App\Models\Admin\PayslipCsvMonth;
use App\Models\Admin\Payslip;
use App\Models\LoanEmi;

use Auth;
use Redirect;
use View;
use DB;
use Storage;
use Response;

class PayslipCsvDataController extends BaseApiController
{
    public function index(Request $request)
    {
        $id = $request->id;
        $csvMonth = PayslipCsvMonth::find($id);
        if($csvMonth) {
            $month = $csvMonth->month;
            if($month < 10) {
                $month = '0'.$month;
            }
            $result = Payslip::generateCsvData($month,$csvMonth->year, $csvMonth->days, $id);
            $data = isset($result['data']) ? $result['data'] : [];
            $editable = isset($result['editable']) ? $result['editable'] : array_fill(0, count($data[0], false));
            $editable = $editable;
            if(count($data) > 0) {
                $heading = $data[0];
                unset($data[0]);
                $data = $data;
                $result = [];
                
                $response['headers'] = $heading;
                $response['list'] = $data;
                $response['editable'] = $editable;

                $result['data'] = $response;

                return $this->successResponse($result);
            } else {
                return $this->failResponse('Unable to generate CSV');
            }
        } else {
            return $this->failResponse('Invalid link followed');
        }
    }

    public function show()
    {

    }
    
    public function update(Request $request, $id)
    {
        $employee_code = empty($request->employee_code) ? null : $request->employee_code;
        $index = empty($request->index) ? null:$request->index;
        $value = empty($request->value) ? null : $request->value;
        if(is_null($employee_code)||is_null($index)) {
            return $this->failResponse('Incomplete request');
        }
        $csvMonth = PayslipCsvMonth::find($id);
        if(!$csvMonth) {
            return $this->failResponse('Invalid link followed');
        } else {
            $key = PayrollRuleForCsv::where('value', $index)->first();
            if($key) {
                $index = $key->key;
            } else {
                $index = preg_replace('/[\(\)\/]/', null, $index);
                $index = preg_replace('/[^a-zA-Z0-9_]/', '_', $index);
                $index = strtolower($index);
            }
            $currentRecord = PayslipCsvData::where('payslip_csv_month_id', $csvMonth->id)->where('employee_code', $employee_code)->first();
            if(!$currentRecord) {
                return $this->failResponse('Unable to find the record to update');
            }
            $currentValue = $currentRecord->$index;
            if($currentValue == $value) {
                return $this->successResponse('Updated Successfully');
            }
            $currentRecord->$index = $value;
            if($currentRecord->save()) {
                return $this->successResponse('Updated Successfully');
            } else {
                return $this->failResponse('Unable to update');
            }
        }
    }
    public function createCsv($id)
    {
        $csvMonth = PayslipCsvMonth::find($id);
        if($csvMonth) {
            $month = $csvMonth->month;
            if($month < 10) {
                $month = '0'.$month;
            }
            $head = ['Sl No','Employee Code','Employee Name','Email ID','PAN No','DOB','DOJ','Designation','Gender','Bank A/c. No.','Bank IFSC Code','PF No','UAN No','ESI No','Month','Year','Total Working Day in Month','No of Days Worked'];
            $head = array_merge($head, PayrollRuleForCsv::orderBy('id')->pluck('value')->toArray());
            $head = array_merge($head, ['SL(Op Bal)','CL (Op Bal)','PL (Op Bal)','CME SL','CME CL','CME PL','SL Availed(utilised)','CL Availed','PL Availed(utilised)','Bal c/f SL','Bal c/f CL','Bal c/f PL']);
            
            $data = PayslipCsvData::where('payslip_csv_month_id', $id)->get();
            if(count($data) > 0) {
                $csv = \League\Csv\Writer::createFromFileObject(new \SplTempFileObject());
                $csv->insertOne($head);
                foreach($data as $value)
                {
                    $value = (json_decode(json_encode($value), true));
                    unset($value['id']);
                    unset($value['payslip_csv_month_id']);
                    unset($value['user_id']);
                    unset($value['created_at']);
                    unset($value['updated_at']);
                    $csv->insertOne($value);
                }
                $fileName = 'Payroll-'.$month.'-'.$csvMonth->year.'.csv';
                Storage::put('CSV/'.$fileName, $csv);
                $csvMonth->csv_path = storage_path('app/CSV').'/'.$fileName;
                $csvMonth->status = "approved";
                $csvMonth->save();
                $result = ['data' => $csvMonth->csv_path];
                return $this->successResponse($result);
            }
        }
        return $this->failResponse("Invalid link followed");
    }
    public function download(Request $request)
    {
        return response()->download($request->file);
    }
}
