<?php

namespace App\Http\Controllers\Api\V1;

class CmsProjectUserController extends ResourceController
{
    protected $model = "\App\Models\Admin\CmsProjectUser";

    public function store()
    {
        $input = $this->getRequestBody();
        $res = $this->model::saveData($input['data']);
        if ($res['status']) {
            return $this->failResponse($res['message']);
        }
        $obj = $this->model::where('id', $res['id'])->with('userProject')->get();
        return $this->successResponse($obj);
    }

    public function show($id)
    {
        $objs = $this->model::where('user_id', $id)->with('userProject')->get();
        if (empty($objs->toArray())) {
            return $this->failResponse("not found");
        }
        return $this->successResponse($objs);
    }

    public function update($id)
    {
        $input = $this->getRequestBody();
        $projectUserObj = $this->model::find($input['id']);
        $projectUserObj->role = $input['description'];
        if (!$projectUserObj->save()) {
            return $this->failResponse("Something went wrong");
        }
        $projectUserObj = $this->model::where('id', $id)->with('userProject')->get();
        return $this->successResponse($projectUserObj);
    }

    public function destroy($id)
    {
        $cmsObj = $this->model::find($id);
        if (!$cmsObj->delete()) {
            return $this->failResponse("unable to delete");
        }
        return $this->successResponse('Successfully Deleted');

    }
}
