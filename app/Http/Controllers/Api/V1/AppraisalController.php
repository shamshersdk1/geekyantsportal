<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Api\V1\BaseApiController;

use App\Models\Admin\PayslipCsvData;
use App\Models\Admin\PayslipCsvMonth;
use App\Models\LoanEmi;

use Auth;
use Redirect;
use View;
use DB;

class AppraisalController extends BaseApiController
{
    public function index(Request $request)
    {
        $id = $request->id;
        $csvMonth = PayslipCsvMonth::find($id);
        if($csvMonth) {
            $month = $csvMonth->month;
            if($month < 10) {
                $month = '0'.$month;
            }
            $appraisals = PayslipCsvData::appraisals($id);
            $result = [];
            $result['data'] = $appraisals;
            return $this->successResponse($result);
        } else {
            return $this->failResponse('Invalid link followed');
        }
    }
    public function show()
    {
    }
}
