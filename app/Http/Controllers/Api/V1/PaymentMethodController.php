<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Validator;

class PaymentMethodController extends ResourceController
{
    protected $model = "\App\Models\Admin\PaymentMethod";

    public function index($query = null)
    {
        if (!$query) {
            $query = $this->obj->buildQuery();
        }
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        $pagination = \Input::has('pagination') ? \Input::get('pagination') : 1;
        $query = $query->orderBy('id','DESC');
        if ($pagination == 0) {
            $sendData = $query->get()->toArray();
            return $this->successResponse($sendData);

        } else {
            $perPage = \Input::has('per_page') ? \Input::get('per_page') : $this->per_page;
            $total = $query->count();
            $paginator = $query->paginate($perPage);
            $result = $paginator->toArray();
            $perPage = $result['per_page'];
            $currentPage = $result['current_page'];
            $metaData = $this->formatMetaData($total, $perPage, $currentPage);
            $sendData = $result['data'];

            return $this->successResponse($sendData, $metaData);
        }
    }
}
