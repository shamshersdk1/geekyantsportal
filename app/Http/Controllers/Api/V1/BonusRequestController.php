<?php

namespace App\Http\Controllers\Api\V1;

use App\Events\BonusRequest\BonusRequestRejected;
use App\Http\Controllers\Api\V1\ResourceController;
use App\Models\Admin\AdditionalWorkDaysBonus;
use App\Models\Admin\Bonus;
use App\Models\Admin\OnsiteAllowance;
use App\Models\Admin\Project;
use App\Models\Admin\ProjectResource;
use App\Models\BonusRequest;
use App\Models\Month;
use App\Models\MonthSetting;
use App\Models\User;
use App\Services\BonusService;
use Auth;
use Illuminate\Http\Request;
use Input;

class BonusRequestController extends ResourceController
{

    protected $model = "App\Models\BonusRequest";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($query = null)
    {
        $monthId = Input::has('monthId') ? \Input::get('monthId') : null;
        $filter = Input::has('filter') ? \Input::get('filter') : null;
        $type = Input::has('type') ? \Input::get('type') : null;
        $user_id = Input::has('user_id') ? \Input::get('user_id') : null;
        if (!$query) {
            $query = $this->obj->buildQuery();
        }
        $userObj = \Auth::User();

        if (!$userObj || !($userObj->hasRole('reporting-manager') || $userObj->hasRole("admin"))) {
            return $this->failResponse("Invalid Access");
        }
        $userIds = [];
        if ($userObj->role != 'admin') {
            foreach ($userObj->children as $child) {
                $userIds[] = $child->id;
            }
            $query = $query->whereIn('user_id', $userIds);
        }
        if ($filter && $filter != 'all') {
            $query = $query->where('status', $filter);
        }
        if ($type && $type != 'na') {
            $query = $query->where('type', $type);
        }
        // if( $filter && $filter == 'other' ) {
        //     $query = $query->whereNotIn('status', ['additional','onsite']);
        // }

        if ($user_id && $user_id != 0) {
            $query = $query->where('user_id', $user_id);
        }
        $query = $query->where('month_id', $monthId);
        $query = $query->with('user', 'bonusRequestProjects.project', 'approvedBonus', 'approvedBonus.approver', 'approvedBonus.referral', 'approvedBy');

        $query = $query->orderBy('date');
        return parent::index($query);
        return $this->successResponse($sendData);

    }
    public function store()
    {
        $userObj = \Auth::User();

        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if (!$userObj || $userObj->role != "admin") {
            return $this->failResponse("Unauthorized Access");
        }
        $input = $this->getRequestBody();

        $bonusReqObj = new BonusRequest;
        $bonusReqObj->user_id = !empty($input['user_id']) ? $input['user_id'] : null;
        $bonusReqObj->month_id = !empty($input['month_id']) ? $input['month_id'] : null;
        $bonusReqObj->date = !empty($input['date']) ? date_to_yyyymmdd($input['date']) : null;
        $bonusReqObj->type = !empty($input['type']) ? $input['type'] : null;
        $bonusReqObj->sub_type = !empty($input['sub_type']) ? $input['sub_type'] : null;
        $bonusReqObj->title = !empty($input['title']) ? $input['title'] : null;
        $bonusReqObj->referral_for = !empty($input['referral_for']) ? $input['referral_for'] : null;
        $bonusReqObj->status = "approved";
        if (!$bonusReqObj->save()) {
            return $this->failResponse('Unable to request bonus');
        }
        $bonusAprrovedReq = Bonus::saveApprovedBonus($bonusReqObj);
        if (!$bonusAprrovedReq['status']) {
            return $this->failResponse("Unable to approve bonus");
        }
        $bonusReqObj->addActivity('Bonus Request By Admin');
        $bonusReqObj->addActivity('Approved Request');
        return $this->successResponse($input);
    }
    public function destroy($id)
    {
        $userObj = Auth::User();

        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if (!$userObj->hasRole('admin') && !$userObj->hasRole('account') && !$userObj->hasRole('reporting-manager')) {
            return $this->failResponse('Unauthorized Access');

        }
        if (AdditionalWorkDaysBonus::find($id)->delete()) {
            return $this->successResponse('Deleted successfully');
        } else {
            return $this->failResponse("Couldn't delete the record");
        }
    }
    public function update($id)
    {
        $userObj = \Auth::User();

        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if (!$userObj || $userObj->role != "admin") {
            return $this->failResponse("Unauthorized Access");
        }
        $data = $this->getRequestBody();
        $input = $data['params'];

        \Log::info('UPDATEd BONUS' . json_encode($data));

        $status = !empty($input['status']) ? $input['status'] : null;
        $bonusReqObj = BonusRequest::where('id', $id)->update(['status' => $status]);
        if (!$bonusReqObj) {
            return $this->failResponse("Unable to approve bonus");
        }
        $bonusAprrovedReq = Bonus::saveApprovedBonus($input);
        if (!$bonusAprrovedReq['status']) {
            return $this->failResponse("Unable to approve bonus");
        }
        $bonusReqObj->addActivity('Approved Bonus Request with Modifications');
        $sendData = BonusRequest::where('id', $id)->with('user', 'approvedBonus.approver')->first();
        return $this->successResponse($sendData);

    }
    public function show($id)
    {
        $userObj = Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        $bonusReqObj = BonusRequest::where('id', $id)->with('user', 'referral', 'approvedBonus.approver', 'approvedBonus.referral', 'approvedBonus.user')->first();
        if ($bonusReqObj) {
            return $this->successResponse($bonusReqObj);
        } else {
            return $this->failResponse("No record");
        }
    }
    public function markAsRejected(Request $request)
    {
        $userObj = \Auth::User();

        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if (!$userObj || !$userObj->hasRole('admin')) {
            return $this->failResponse("Unauthorized Access");
        }
        $input = $request->all();

        $bonusReqObj = BonusRequest::find($input['id']);
        $bonusReqObj->status = "rejected";
        $bonusReqObj->notes = !empty($input['notes']) ? $input['notes'] : null;
        $bonusReqObj->approver_id = $userObj->id;
        if (!$bonusReqObj->save()) {
            return $this->failResponse("Unable to reject bonus");
        }
        event(new BonusRequestRejected($bonusReqObj->id));
        $bonusReqObj->addActivity('Bonus Request Rejected');
        // $bonusRejectReq = Bonus::saveRejectBonus($input);
        $sendData = BonusRequest::where('id', $input['id'])->first();
        return $this->successResponse($sendData);
    }
    public function markAsApproved(Request $request)
    {
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if (!$userObj || !($userObj->hasRole('admin') || $userObj->hasRole('reporting-manager'))) {
            return $this->failResponse("Unauthorized Access");
        }
        $input = $request->all();
        $bonusReqObj = BonusRequest::find($input['id']);

        if (!$bonusReqObj) {
            return $this->failResponse("Invalid bonus request id");
        }

        if ($userObj->role != 'admin' && $userObj->hasRole('reporting-manager') && $bonusReqObj->user->parent_id != $userObj->id) {
            return $this->failResponse("Only respective reporting manager can approve this request");
        }
        $bonusReqObj->notes = !empty($input['notes']) ? $input['notes'] : null;
        $bonusReqObj->approver_id = $userObj->id;

        if ($input['type'] == 'onsite') {
            if (empty($input['redeem_info']['onsite_allowance_id'])) {
                return $this->failResponse("Missing onsite Allowance Info");
            } else {
                $onsiteAllowanceId = $input['redeem_info']['onsite_allowance_id'];
                $onSiteAllowanceObj = OnsiteAllowance::find($onsiteAllowanceId);

                if (!$onSiteAllowanceObj) {
                    return $this->failResponse("Missing onsite Allowance Info");
                }

                $redeem_data = [];
                $redeem_data['onsite_allowance_id'] = $onsiteAllowanceId ?: null;
                $bonusReqObj->redeem_type = json_encode($redeem_data);
                $bonusReqObj->sub_type = $onSiteAllowanceObj->type;
            }
        }

        if (!$bonusReqObj->save()) {
            return $this->failResponse("Missing onsite Allowance Info");
        }
        $response = BonusService::approveBonusRequest($bonusReqObj->id);

        $bonusReqObj->addActivity('Bonus Request Approved');
        $sendData = BonusRequest::where('id', $input['id'])->with('user', 'approvedBonus.approver')->first();
        return $this->successResponse($sendData);

    }
    public function lockMonth($id)
    {
        $userObj = \Auth::User();

        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if (!$userObj || $userObj->role != "admin") {
            return $this->failResponse("Unauthorized Access");
        }
        $month = Month::find($id);
        if ($month) {
            $monthSetting = new MonthSetting;
            $monthSetting->month_id = $id;
            $monthSetting->key = "bonus-request";
            $monthSetting->value = "locked";
            if (!$monthSetting->save()) {
                return $this->failResponse("Unable to Lock Month");
            }
            return $this->successResponse("Month Successfully Locked");

        }
    }

    public function getCounts(Request $request)
    {
        $userObj = \Auth::User();
        if (!$userObj || $userObj->role != "admin") {
            return $this->failResponse("Invalid Access");
        }
        $counts = BonusRequest::getBonusStatusCounts($request['month_id']);
        return $this->successResponse($counts);
    }

    public function bonusApprovePayment()
    {
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if (!$userObj || $userObj->role != "admin") {
            return $this->failResponse("Unauthorized Access");
        }
        $input = $this->getRequestBody();
        $response = BonusService::addNewBonus($input);
        if ($response['status']) {
            return $this->successResponse($response['message']);
        } else {
            return $this->failResponse($response['message']);
        }

    }

    public function getUserDetails()
    {
        $input = $this->getRequestBody();
        $data = [];
        $index = 0;
        if ($input && isset($input["type"]) && isset($input["user_id"]) && isset($input["date"])) {
            if ($input["type"] == "additional" || $input["type"] == "extra") {
                $data = ProjectResource::getActiveProjectIdArray($input["user_id"], $start_date = $input["date"], $end_date = $input["date"]);
                foreach ($data as $index => $dat) {
                    $obj = Project::find($dat);
                    $data[$index] = $obj;
                }
            } elseif ($input["type"] == "onsite") {
                $data = OnsiteAllowance::with('project')->where('user_id', $input["user_id"])->whereDate('start_date', '<=', $input["date"])->whereDate('end_date', '>=', $input["date"])->where('status', 'approved')->get();
            } else {
                $data = [];
            }
            return $this->successResponse($data);
        }
    }
}
