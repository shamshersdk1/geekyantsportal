<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\V1\ResourceController;
use App\Models\Admin\Incident;
use App\Models\Admin\IncidentUserAccess;
use App\Models\User;
use Auth;
use App\Services\IncidentService;

class IncidentController extends ResourceController
{

    protected $model = "App\Models\Admin\Incident";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($query = null)
    {

        if (!$query) {
            $query = $this->obj->buildQuery();
        }
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if ( $userObj->role == 'admin') 
        {
            $query = $query->with('assignee', 'department', 'raisedBy','incidentUsers');
            $query = $query->orderBy('id', 'DESC');
        } 
        else 
        {
            $incidentDepartment = [];
            $array1 = $userObj->headOfDepartments->pluck('id')->toArray();
            $array2 = $userObj->firstContactOfDepartments->pluck('id')->toArray();
            $incident_ids = IncidentUserAccess::where('user_id',$userObj->id)->pluck('id')->toArray();
            if ( !empty($array1) && !empty($array2) )
            {
                $incidentDepartment =  array_merge( $array1, $array2) ;
            } elseif ( !empty($array1) && empty($array2) ) {
                $incidentDepartment = $array1;
            } elseif ( empty($array1) && !empty($array2) ) {
                $incidentDepartment = $array2;
            }
            $query = $query->with('assignee', 'department', 'raisedBy','incidentUsers')->whereIn('department_id', $incidentDepartment)
                           ->orWhere('assigned_to',$userObj->id)->orWhere('raised_by',$userObj->id)->orWhereIn('id',$incident_ids)
                           ->orderBy('id', 'DESC');
        }
        return parent::index($query);
        
    }
    public function store()
    {
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }

        if (!$userObj->hasRole('user')) {
            return $this->failResponse('Unauthorized Access');
        }

        $data = $this->getRequestBody();
        if(empty($data))
            return $this->failResponse('Empty data');

        $response = $this->model::saveData($data);
        if(!$response)
            return $this->failResponse('Unable to save the data');

        return $this->successResponse("Successfully saved");
    }
    public function show($id)
    {
        
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }

        $obj = Incident::with('assignee', 'department', 'raisedBy','incidentUsers')->find($id);
        if (!$obj) {
            return $this->failResponse("Invalid #" . $id);
        }
        // if (!$userObj->hasRole('admin') && !$userObj->hasRole('office-admin')) {
        //     return $this->failResponse('Unauthorized Access');
        // }
        
        return $this->successResponse($obj);
    }
    public function update($id)
    {
        if (empty($id)) {
            return $this->failResponse('Bad request', 400);
        }

        $obj = $this->obj->find($id);

        if (empty($obj)) {
            return $this->failResponse('Invalid id "' . $id . '". Unable to update the record', 404);
        }

        $data = $this->getRequestBody();
        
        if (empty($data)) {
            return $this->failResponse('Bad request', 400, ['Empty data sent']);
        }
        if( !empty($data['status']) &&  ($data['status'] == 'closed' || $data['status'] == 'verified_closed' || $data['status'] == 'reopen' ) )
        {
            $response = $this->model::updateStatus($id, $data['status']); 
        } else {
            $response = $this->model::updateData($id, $data);
        }
        if( $response )
        {
            $obj = Incident::with('assignee', 'department', 'raisedBy','incidentUsers')->find($id);
            return $this->successResponse($obj->toArray());
        } else {
            return $this->failResponse('Unable to save the data');
        }
    }

    public function transferIncident()
    {
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        $input = $this->getRequestBody();
        \Log::info( json_encode($input) );
        if( empty($input['incident_id']) )
        {
            return $this->failResponse("Invalid Access");
        }
        if ( !empty($input['department_id']) )
        {
            $res = IncidentService::transferIncidentDepartment($input['incident_id'],$input['department_id']);
        }
        elseif ( !empty($input['user_id']) )
        {
            $res = IncidentService::transferIncidentUser($input['incident_id'],$input['user_id']);
        }
        if( $res['status'] )
        {
            $incidentObj = Incident::with('assignee', 'department', 'raisedBy','incidentUsers')->find($input['incident_id']);
            return $this->successResponse($incidentObj);
        }
        else {
            return $this->failResponse($res['message']);
        }

    }
}
