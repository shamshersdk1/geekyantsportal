<?php

namespace App\Http\Controllers\Api\V1\Insurance;
use App\Http\Controllers\Api\V1\ResourceController;
use App\Models\User;
use App\Models\Admin\Insurance\InsurancePolicy;
use App\Events\ActivityLog;
use App\Services\Files\CSVFileService;
use App\Models\Admin\Insurance\Insurance;
use Auth;
use Input;
use Excel;
use Response;
class InsurancePolicyController extends ResourceController
{


    protected $model = "App\Models\Admin\Insurance\InsurancePolicy";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($query = null)
    {

        if (!$query) {
            $query = $this->obj->buildQuery();
        }
        $userObj = \Auth::User();
        
        if (!$userObj || !($userObj->hasRole('admin') || $userObj->hasRole('account'))) {
            return $this->failResponse("Invalid Access");
        }
        
        $status = \Input::has('status') ? \Input::get('status') : null;
        $subGroup = \Input::has('sub_group') ? \Input::get('sub_group') : 0;
        //$parentId = \Input::has('parent_id') ? \Input::get('parent_id') : NULL;
        
        $query = $query->whereNull('parent_id');
        if($status)
            $query = $query->where('status', $status);

        if($subGroup)
            $query = $query->where('sub_group', $subGroup);
                
        $query = $query->with('user');
        $query = $query->orderBy('id', 'DESC');

        $sendData = $query->get()->toArray();
        return parent::index($query);

    }
    public function show($id) {
        $userObj = Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if (!$userObj || !($userObj->hasRole('admin') || $userObj->hasRole('account'))) {
            return $this->failResponse("Invalid Access");
        }
        $obj = InsurancePolicy::with('subGroups.insurances.insurable')->find($id);
        if (!$obj) {
            return $this->failResponse("Invalid id ".$id);
        }
        return $this->successResponse($obj);
    }
    public function store()
    {
        $authUser = Auth::user();
        if (!$authUser) {
            return $this->failResponse('Invalid Access');
        }
        if (!($authUser->hasRole('admin')) && !($authUser->hasRole('account')) ) {
            return $this->failResponse('Unauthorized Access');
        }
        $input = $this->getRequestBody();
        $response = InsurancePolicy::saveData($input);
        // $AdditionalWorkDaysBonus = new AdditionalWorkDaysBonus();
        // $AdditionalWorkDaysBonus->employee_id = $input['user_id'];
        // $AdditionalWorkDaysBonus->type = $input['type'];
        // $AdditionalWorkDaysBonus->reference_id = $input['project_id'];
        // $AdditionalWorkDaysBonus->reference_type = 'App\Models\Admin\Project';
        // $AdditionalWorkDaysBonus->amount = $input['amount'];
        // $AdditionalWorkDaysBonus->status = 'approved';
        // $AdditionalWorkDaysBonus->to_date = !empty(date_to_yyyymmdd($input['to_date'])) ? date_to_yyyymmdd($input['to_date']) : null;
        // $AdditionalWorkDaysBonus->from_date = !empty(date_to_yyyymmdd($input['from_date'])) ? date_to_yyyymmdd($input['from_date']) : null;
        // $AdditionalWorkDaysBonus->notes = !empty($input['notes']) ? $input['notes'] : null;
        // $AdditionalWorkDaysBonus->created_by = $authUser->id;
        // if (!$AdditionalWorkDaysBonus->save()) {

        //     return $this->failResponse('Unable to save contact');
        // }
        // $AdditionalWorkDaysBonusObj = AdditionalWorkDaysBonus::where('id', $AdditionalWorkDaysBonus->id)->with('user', 'reviewer')->first();
        return $this->successResponse('Saved successfully');
    }
    public function markAsRequested($id) {
        $inputs = \Input::all();

        $userObj = \Auth::User();

        if (!$userObj || !($userObj->hasRole('admin') || $userObj->hasRole('account'))) {
            return $this->failResponse("Invalid Access");
        }
        $obj = InsurancePremium::with('user','approvedBy','requestedBy')->find($id);
        
        if(!$obj)
            return $this->failResponse("Invalid ID ".$id);
        
        if(!$obj->status = 'requested')
            return $this->failResponse("Unable to update the record, already requested");

        $obj->requested_by = $userObj->id;
        $obj->amount = !empty($inputs['amount'])? (int)$inputs['amount'] : 0;
        $obj->status = 'requested';
        $obj->date = date('Y-m-d');
        
        if(!$obj->save())
            return $this->failResponse("Unable to save the record");
        
        $obj->addActivity();

        return $this->successResponse($obj);


    }
    public function updateRecord($id) {
        $inputs = \Input::all();
        $userObj = \Auth::User();

        if (!$userObj || !($userObj->hasRole('admin') || $userObj->hasRole('account'))) {
            return $this->failResponse("Invalid Access");
        }
        $obj = InsurancePremium::find($id);
        
        if(!$obj)
            return $this->failResponse("Invalid ID ".$id);
        $newAmount = !empty($inputs['amount'])? (int)$inputs['amount'] : 0;
        $newObj = $obj->replicate();

        $newObj->amount = !empty($inputs['amount'])? (int)$inputs['amount'] : 0;
        $newObj->parent_id = $obj->id;
        if(!$newObj->save())
            return $this->failResponse("Unable to save the record");
        
        $obj->status = 'closed';
        $obj->save();

        //$obj->addActivity();
        $newRecord = InsurancePremium::with('user','lastUpdatedRec')->find($newObj->id);
        //$newRecord['last_updated'] = InsurancePremium::where('user_id',$newObj->user_id)->orderBy('created_at', 'desc')->skip(1)->take(1)->first();

        return $this->successResponse($newRecord);


    }
    public function markAsApproved($id) {
        $userObj = \Auth::User();

        if (!$userObj || !($userObj->hasRole('admin') || $userObj->hasRole('account'))) {
            return $this->failResponse("Invalid Access");
        }
        $obj = InsurancePremium::with('user','approvedBy','requestedBy')->find($id);
        
        if(!$obj)
            return $this->failResponse("Invalid ID ".$id);
        
        if(!$obj->status = 'approved')
            return $this->failResponse("Unable to update the record, already approved");

        $obj->approved_by = $userObj->id;
        $obj->status = 'approved';

        if(!$obj->save())
            return $this->failResponse("Unable to save the record");
        $obj->addActivity();
        return $this->successResponse($obj);
    }
    public function markAsClosed($id) {
        $userObj = \Auth::User();

        if (!$userObj || !($userObj->hasRole('admin') || $userObj->hasRole('account'))) {
            return $this->failResponse("Invalid Access");
        }
        $obj = InsurancePremium::with('user','approvedBy','requestedBy')->find($id);
        
        if(!$obj)
            return $this->failResponse("Invalid ID ".$id);
        
        if(!$obj->status = 'closed')
            return $this->failResponse("Unable to update the record, already approved");

        $obj->approved_by = $userObj->id;
        $obj->status = 'closed';

        if(!$obj->save())
            return $this->failResponse("Unable to save the record");
        
        $obj->addActivity();

        return $this->successResponse($obj);


    }
     public function destroy($id)
    {
        $userObj = Auth::User();

        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if (!$userObj->hasRole('admin') && !$userObj->hasRole('account') && !$userObj->hasRole('reporting-manager')) {
            return $this->failResponse('Unauthorized Access');

        }
        $obj = InsurancePolicy::find($id);
        if (!$obj) {
            return $this->failResponse('Invalid id '.$id);
        }
    
        if($obj->delete()) {
            Insurance::where('insurance_policy_id', $obj->id)->delete();
        }

        return $this->successResponse('Successfully deleted');
        
    }
    public function downloadCSV($id) {
        $insurnaceObj = InsurancePolicy::with('insurances')->find($id);
        $data=[];
        if ($insurnaceObj) {
            $head = [''];
            $head = ['Insurance Name', 'Policy Number', 'Coverage', 'Name', 'Employee ID', 'Amount'];

            $data[] = $head;
            if (count($insurnaceObj->insurances) > 0) {
                foreach ($insurnaceObj->insurances as $value) {
                    $items = [$insurnaceObj->name, $insurnaceObj->coverage_amount, $insurnaceObj->policy_number, $value->insurable->name, $value->insurable->employee_id, $insurnaceObj->premium_amount];
                    $data[] = $items;
                }
            }
        }
        $filepath = CSVFileService::createAndStoreExcel($data);

        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=file.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );
        $path = 'excel/exports';
        //$filename = "test9";
        $type = 'csv';
        $dd = Excel::create($filename, function ($excel) use ($data) {
            $excel->sheet('mySheet', function ($sheet) use ($data) {
                $sheet->fromArray($data);
            });
        });
        $file = storage_path().'/'.$filepath;
          
        //return Response::download($file,null ,$headers);


        return Response::stream($dd, 200, $headers);

        return response()->download(storage_path().'/'.$filepath);
        //return $this->successResponse($filepath);
    }
    // public function store()
    // {
    //     $authUser = Auth::user();
    //     if (!$authUser) {
    //         return $this->failResponse('Invalid Access');
    //     }
    //     if (!($authUser->hasRole('admin')) && !($authUser->hasRole('account')) && !($authUser->hasRole('reporting-manager'))) {
    //         return $this->failResponse('Unauthorized Access');
    //     }
    //     $input = $this->getRequestBody();
    //     $AdditionalWorkDaysBonus = new AdditionalWorkDaysBonus();
    //     $AdditionalWorkDaysBonus->employee_id = $input['user_id'];
    //     $AdditionalWorkDaysBonus->type = $input['type'];
    //     $AdditionalWorkDaysBonus->reference_id = $input['project_id'];
    //     $AdditionalWorkDaysBonus->reference_type = 'App\Models\Admin\Project';
    //     $AdditionalWorkDaysBonus->amount = $input['amount'];
    //     $AdditionalWorkDaysBonus->status = 'approved';
    //     $AdditionalWorkDaysBonus->to_date = !empty(date_to_yyyymmdd($input['to_date'])) ? date_to_yyyymmdd($input['to_date']) : null;
    //     $AdditionalWorkDaysBonus->from_date = !empty(date_to_yyyymmdd($input['from_date'])) ? date_to_yyyymmdd($input['from_date']) : null;
    //     $AdditionalWorkDaysBonus->notes = !empty($input['notes']) ? $input['notes'] : null;
    //     $AdditionalWorkDaysBonus->created_by = $authUser->id;
    //     if (!$AdditionalWorkDaysBonus->save()) {

    //         return $this->failResponse('Unable to save contact');
    //     }
    //     $AdditionalWorkDaysBonusObj = AdditionalWorkDaysBonus::where('id', $AdditionalWorkDaysBonus->id)->with('user', 'reviewer')->first();
    //     return $this->successResponse($AdditionalWorkDaysBonusObj);
    // }
    // public function destroy($id)
    // {
    //     $userObj = Auth::User();

    //     if (!$userObj) {
    //         return $this->failResponse("Invalid Access");
    //     }
    //     if (!$userObj->hasRole('admin') && !$userObj->hasRole('account') && !$userObj->hasRole('reporting-manager')) {
    //         return $this->failResponse('Unauthorized Access');

    //     }
    //     if (AdditionalWorkDaysBonus::find($id)->delete()) {
    //         return $this->successResponse('Deleted successfully');
    //     } else {
    //         return $this->failResponse("Couldn't delete the record");
    //     }
    // }
    // public function updateBonus($id)
    // {
    //     $userObj = Auth::User();

    //     if (!$userObj) {
    //         return $this->failResponse("Invalid Access");
    //     }
    //     if (!$userObj->hasRole('admin') && !$userObj->hasRole('account') && !$userObj->hasRole('reporting-manager')) {
    //         return $this->failResponse('Unauthorized Access');

    //     }
    //     $input = $this->getRequestBody();
    //     $AdditionalWorkDaysBonus = AdditionalWorkDaysBonus::find($input['bonus_id']);
    //     $AdditionalWorkDaysBonus->employee_id = $input['user_id'];
    //     $AdditionalWorkDaysBonus->type = $input['type'];
    //     $AdditionalWorkDaysBonus->reference_id = $input['project_id'];
    //     $AdditionalWorkDaysBonus->amount = $input['amount'];
    //     $AdditionalWorkDaysBonus->to_date = !empty(date_to_yyyymmdd($input['to_date'])) ? date_to_yyyymmdd($input['to_date']) : null;
    //     $AdditionalWorkDaysBonus->from_date = !empty(date_to_yyyymmdd($input['from_date'])) ? date_to_yyyymmdd($input['from_date']) : null;
    //     $AdditionalWorkDaysBonus->notes = !empty($input['notes']) ? $input['notes'] : null;
    //     $AdditionalWorkDaysBonus->created_by = $userObj->id;

    //     if (!$AdditionalWorkDaysBonus->save()) {
    //         return $this->failResponse('Unable to save contact');
    //     }
    //     $AdditionalWorkDaysBonusObj = AdditionalWorkDaysBonus::where('id', $AdditionalWorkDaysBonus->id)->with('user' , 'reviewer')->first();
    //     return $this->successResponse($AdditionalWorkDaysBonusObj);
    // }
    // public function show($id)
    // {
    //     $userObj = Auth::User();
    //     if (!$userObj) {
    //         return $this->failResponse("Invalid Access");
    //     }
    //     $AdditionalWorkDaysBonusObj = AdditionalWorkDaysBonus::where('id', $id)->with('user', 'reviewer')->first();
    //     if ($AdditionalWorkDaysBonusObj) {
    //         return $this->successResponse($AdditionalWorkDaysBonusObj);
    //     } else {
    //         return $this->failResponse("No record");
    //     }
    // }
}
