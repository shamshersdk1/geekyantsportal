<?php

namespace App\Http\Controllers\Api\V1\Insurance;
use App\Http\Controllers\Api\V1\ResourceController;
use App\Models\User;
use App\Models\Admin\Insurance\Insurance;
use App\Events\ActivityLog;
use Auth;
use Input;
class InsuranceController extends ResourceController
{
    protected $model = "App\Models\Admin\Insurance\Insurance";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($query = null)
    {

        if (!$query) {
            $query = $this->obj->buildQuery();
        }
        $userObj = \Auth::User();
        
        if (!$userObj || !($userObj->hasRole('admin') || $userObj->hasRole('account'))) {
            return $this->failResponse("Invalid Access");
        }
        
        //$status = \Input::has('status') ? \Input::get('status') : null;
        
        // if($status)
        //     $query = $query->where('status', $status);
                
        $query = $query->with('insurable','policy');
        $query = $query->orderBy('id', 'DESC');

        $sendData = $query->get()->toArray();
        return parent::index($query);

    }
    public function show($id) {
        $userObj = Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if (!$userObj || !($userObj->hasRole('admin') || $userObj->hasRole('account'))) {
            return $this->failResponse("Invalid Access");
        }
        $obj = InsurancePolicy::with('subGroups')->find($id);
        
        if (!$obj) {
            return $this->failResponse("Invalid id ".$id);
        }
        return $this->successResponse($obj);
    }
    public function store()
    {
        $authUser = Auth::user();
        if (!$authUser) {
            return $this->failResponse('Invalid Access');
        }
        if (!($authUser->hasRole('admin')) && !($authUser->hasRole('account')) ) {
            return $this->failResponse('Unauthorized Access');
        }
        $input = $this->getRequestBody();
        $response = Insurance::saveData($input);
        
        if(isset($response['status']) && $response['status'] == false)
            return $this->failResponse($response['message']);

        $id = !empty($response['id']) ?  $response['id'] : null;
        
        $obj = Insurance::with('insurable','addedBy')->find($id);
        
        if(!$obj)
            return $this->failResponse('#Unable to save the record');

        return $this->successResponse($obj);

    }
    public function update($id)
    {
        $authUser = Auth::user();
        if (empty($id)) {
            return $this->failResponse('Bad request', 400);
        }

        if (!($authUser->hasRole('admin')) && !($authUser->hasRole('account')) ) {
            return $this->failResponse('Unauthorized Access');
        }
        $input = $this->getRequestBody();

        $obj = Insurance::find($id);
        
        if(!$obj)
            return $this->failResponse('Invalid id '.$id);
        
        if(!empty($input['status']))
            $obj->status = $input['status'];
        if(!empty($input['status']) && $input['status'] == 'closed' )
            $obj->inactive_date = date('Y-m-d');
        
        if(!$obj->save())
            return $this->failResponse('Unable to update the record');

        return $this->successResponse($obj);
        
    }
    public function markAsRequested($id) {
        $inputs = \Input::all();

        $userObj = \Auth::User();

        if (!$userObj || !($userObj->hasRole('admin') || $userObj->hasRole('account'))) {
            return $this->failResponse("Invalid Access");
        }
        $obj = InsurancePremium::with('user','approvedBy','requestedBy')->find($id);
        
        if(!$obj)
            return $this->failResponse("Invalid ID ".$id);
        
        if(!$obj->status = 'requested')
            return $this->failResponse("Unable to update the record, already requested");

        $obj->requested_by = $userObj->id;
        $obj->amount = !empty($inputs['amount'])? (int)$inputs['amount'] : 0;
        $obj->status = 'requested';
        $obj->date = date('Y-m-d');
        
        if(!$obj->save())
            return $this->failResponse("Unable to save the record");
        
        $obj->addActivity();

        return $this->successResponse($obj);


    }
    public function updateRecord($id) {
        $inputs = \Input::all();
        $userObj = \Auth::User();

        if (!$userObj || !($userObj->hasRole('admin') || $userObj->hasRole('account'))) {
            return $this->failResponse("Invalid Access");
        }
        $obj = InsurancePremium::find($id);
        
        if(!$obj)
            return $this->failResponse("Invalid ID ".$id);
        $newAmount = !empty($inputs['amount'])? (int)$inputs['amount'] : 0;
        $newObj = $obj->replicate();

        $newObj->amount = !empty($inputs['amount'])? (int)$inputs['amount'] : 0;
        $newObj->parent_id = $obj->id;
        if(!$newObj->save())
            return $this->failResponse("Unable to save the record");
        
        $obj->status = 'closed';
        $obj->save();

        //$obj->addActivity();
        $newRecord = InsurancePremium::with('user','lastUpdatedRec')->find($newObj->id);
        //$newRecord['last_updated'] = InsurancePremium::where('user_id',$newObj->user_id)->orderBy('created_at', 'desc')->skip(1)->take(1)->first();

        return $this->successResponse($newRecord);


    }
    public function markAsApproved($id) {
        $userObj = \Auth::User();

        if (!$userObj || !($userObj->hasRole('admin') || $userObj->hasRole('account'))) {
            return $this->failResponse("Invalid Access");
        }
        $obj = InsurancePremium::with('user','approvedBy','requestedBy')->find($id);
        
        if(!$obj)
            return $this->failResponse("Invalid ID ".$id);
        
        if(!$obj->status = 'approved')
            return $this->failResponse("Unable to update the record, already approved");

        $obj->approved_by = $userObj->id;
        $obj->status = 'approved';

        if(!$obj->save())
            return $this->failResponse("Unable to save the record");
        $obj->addActivity();
        return $this->successResponse($obj);
    }
    public function markAsClosed($id) {
        $userObj = \Auth::User();

        if (!$userObj || !($userObj->hasRole('admin') || $userObj->hasRole('account'))) {
            return $this->failResponse("Invalid Access");
        }
        $obj = InsurancePremium::with('user','approvedBy','requestedBy')->find($id);
        
        if(!$obj)
            return $this->failResponse("Invalid ID ".$id);
        
        if(!$obj->status = 'closed')
            return $this->failResponse("Unable to update the record, already approved");

        $obj->approved_by = $userObj->id;
        $obj->status = 'closed';

        if(!$obj->save())
            return $this->failResponse("Unable to save the record");
        
        $obj->addActivity();

        return $this->successResponse($obj);


    }
    // public function store()
    // {
    //     $authUser = Auth::user();
    //     if (!$authUser) {
    //         return $this->failResponse('Invalid Access');
    //     }
    //     if (!($authUser->hasRole('admin')) && !($authUser->hasRole('account')) && !($authUser->hasRole('reporting-manager'))) {
    //         return $this->failResponse('Unauthorized Access');
    //     }
    //     $input = $this->getRequestBody();
    //     $AdditionalWorkDaysBonus = new AdditionalWorkDaysBonus();
    //     $AdditionalWorkDaysBonus->employee_id = $input['user_id'];
    //     $AdditionalWorkDaysBonus->type = $input['type'];
    //     $AdditionalWorkDaysBonus->reference_id = $input['project_id'];
    //     $AdditionalWorkDaysBonus->reference_type = 'App\Models\Admin\Project';
    //     $AdditionalWorkDaysBonus->amount = $input['amount'];
    //     $AdditionalWorkDaysBonus->status = 'approved';
    //     $AdditionalWorkDaysBonus->to_date = !empty(date_to_yyyymmdd($input['to_date'])) ? date_to_yyyymmdd($input['to_date']) : null;
    //     $AdditionalWorkDaysBonus->from_date = !empty(date_to_yyyymmdd($input['from_date'])) ? date_to_yyyymmdd($input['from_date']) : null;
    //     $AdditionalWorkDaysBonus->notes = !empty($input['notes']) ? $input['notes'] : null;
    //     $AdditionalWorkDaysBonus->created_by = $authUser->id;
    //     if (!$AdditionalWorkDaysBonus->save()) {

    //         return $this->failResponse('Unable to save contact');
    //     }
    //     $AdditionalWorkDaysBonusObj = AdditionalWorkDaysBonus::where('id', $AdditionalWorkDaysBonus->id)->with('user', 'reviewer')->first();
    //     return $this->successResponse($AdditionalWorkDaysBonusObj);
    // }
    // public function destroy($id)
    // {
    //     $userObj = Auth::User();

    //     if (!$userObj) {
    //         return $this->failResponse("Invalid Access");
    //     }
    //     if (!$userObj->hasRole('admin') && !$userObj->hasRole('account') && !$userObj->hasRole('reporting-manager')) {
    //         return $this->failResponse('Unauthorized Access');

    //     }
    //     if (AdditionalWorkDaysBonus::find($id)->delete()) {
    //         return $this->successResponse('Deleted successfully');
    //     } else {
    //         return $this->failResponse("Couldn't delete the record");
    //     }
    // }
    // public function updateBonus($id)
    // {
    //     $userObj = Auth::User();

    //     if (!$userObj) {
    //         return $this->failResponse("Invalid Access");
    //     }
    //     if (!$userObj->hasRole('admin') && !$userObj->hasRole('account') && !$userObj->hasRole('reporting-manager')) {
    //         return $this->failResponse('Unauthorized Access');

    //     }
    //     $input = $this->getRequestBody();
    //     $AdditionalWorkDaysBonus = AdditionalWorkDaysBonus::find($input['bonus_id']);
    //     $AdditionalWorkDaysBonus->employee_id = $input['user_id'];
    //     $AdditionalWorkDaysBonus->type = $input['type'];
    //     $AdditionalWorkDaysBonus->reference_id = $input['project_id'];
    //     $AdditionalWorkDaysBonus->amount = $input['amount'];
    //     $AdditionalWorkDaysBonus->to_date = !empty(date_to_yyyymmdd($input['to_date'])) ? date_to_yyyymmdd($input['to_date']) : null;
    //     $AdditionalWorkDaysBonus->from_date = !empty(date_to_yyyymmdd($input['from_date'])) ? date_to_yyyymmdd($input['from_date']) : null;
    //     $AdditionalWorkDaysBonus->notes = !empty($input['notes']) ? $input['notes'] : null;
    //     $AdditionalWorkDaysBonus->created_by = $userObj->id;

    //     if (!$AdditionalWorkDaysBonus->save()) {
    //         return $this->failResponse('Unable to save contact');
    //     }
    //     $AdditionalWorkDaysBonusObj = AdditionalWorkDaysBonus::where('id', $AdditionalWorkDaysBonus->id)->with('user' , 'reviewer')->first();
    //     return $this->successResponse($AdditionalWorkDaysBonusObj);
    // }
    // public function show($id)
    // {
    //     $userObj = Auth::User();
    //     if (!$userObj) {
    //         return $this->failResponse("Invalid Access");
    //     }
    //     $AdditionalWorkDaysBonusObj = AdditionalWorkDaysBonus::where('id', $id)->with('user', 'reviewer')->first();
    //     if ($AdditionalWorkDaysBonusObj) {
    //         return $this->successResponse($AdditionalWorkDaysBonusObj);
    //     } else {
    //         return $this->failResponse("No record");
    //     }
    // }
}
