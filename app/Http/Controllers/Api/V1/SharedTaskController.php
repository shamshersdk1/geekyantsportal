<?php 

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\V1\BaseApiController;

use App\Models\Admin\UserTask;
use App\Models\Admin\SharedTask;

class SharedTaskController extends BaseApiController
{
    public function store(Request $request)
    {
        $user_id = $request->user_id;
        $owner_array = empty($request->owner_array) ? [] : $request->owner_array;
        if(\Auth::check()) {
            $response = SharedTask::assign($user_id, $owner_array, \Auth::id());
            if($response['status']) {
                return $this->successResponse("Saved Successfully");
            } else {
                return $this->failResponse($response['message']);
            }
        } else {
            return $this->failResponse("You need to login first");
        }
    }
}
