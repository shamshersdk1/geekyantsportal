<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\V1\ResourceController;
use App\Models\User;
use App\Models\Admin\Project;
use App\Models\UserRole;
use App\Services\ProjectService;
use App\Services\ResourceService;
use Illuminate\Http\Request;
use Auth;
use DB;
use Exception;
use Input;
use Redirect;
use Storage;
use Config;

class TechnologyController extends ResourceController{

    protected $model = "App\Models\Admin\Technology";

    public function index($query = null)
    {   
        if (!$query) {
            $query = $this->obj->buildQuery();
        }
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        $sendData = $query->get()->toArray();
        return $this->successResponse($sendData);
    }

}
        

