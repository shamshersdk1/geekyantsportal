<?php
namespace App\Http\Controllers\Api\V1;

use App\Models\Admin\AssetMeta;
use App\Models\Admin\IpAddressMapper;

class AssetAssignmentDetailController extends ResourceController
{
    protected $model = "\App\Models\Admin\AssetAssignmentDetail";

    public function index($query = null)
    {
        if (!$query) {
            $query = $this->obj->buildQuery();
        }
        $assetId = \Input::has('asset_id') ? \Input::get('asset_id') : 0;
        $query = $query->where('asset_id', $assetId);
        $query = $query->orderBy('from_date', 'DESC');
        $query = $query->with('user', 'assignee', 'releasedBy', 'asset.metas.ipMapper.userIp');

        return parent::index($query);
    }
    public function store()
    {
        $authUser = \Auth::user();
        if (!$authUser) {
            return $this->failResponse('Invalid Access');
        }
        if (!($authUser->hasRole('admin')) && !($authUser->hasRole('office-admin'))) {
            return $this->failResponse('Unauthorized Access');
        }
        $input = $this->getRequestBody();
        $isAssigned = $this->model::checkAvailability($input['asset_id'], $input['from_date']);
        if ($isAssigned) {
            return $this->failResponse('Asset already assigned on the given date');
        } else {
            $assetObj = new $this->model;
            $response = $this->model::saveData($input, $assetObj);
            return $response;
        }
    }

    public function update($id)
    {
        $authUser = \Auth::user();
        if (!$authUser) {
            return $this->failResponse('Invalid Access');
        }
        $input = $this->getRequestBody();
        $assetObj = $this->model::with('user', 'assignee', 'releasedBy')->find($id);

        if (!$assetObj) {
            return $this->failResponse('Record not found');
        }
        if ($assetObj->from_date > date_to_yyyymmdd($input['to_date'])) {
            return $this->failResponse('"To Date" can not be less than "From Date"');
        }
        $assetObj->to_date = !empty($input['to_date']) ? $input['to_date'] : null;
        $assetObj->released_by = $authUser->id;
        if (!$assetObj->save()) {
            return $this->failResponse('Unable to update record');
        }
        $metaObjs = AssetMeta::where('asset_id', $assetObj->asset_id)->get();
        foreach ($metaObjs as $metaObj) {
            $IpAddressMapperObj = IpAddressMapper::where('reference_type', 'App\Models\Admin\AssetMeta')->where('reference_id', $metaObj->id)->delete();
        }
        $assetObj2 = $this->model::with('user', 'assignee', 'releasedBy', 'asset.metas.ipMapper.userIp')->find($id);

        return $this->successResponse($assetObj2);
    }
    public function show($id)
    {
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        $obj = $this->model::where('id', $id)->with('asset.metas')->first();
        if (!$obj) {
            return $this->failResponse("Not found");
        } else {
            return $this->successResponse($obj);
        }
    }
}
