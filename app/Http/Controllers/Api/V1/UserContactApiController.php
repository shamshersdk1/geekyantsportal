<?php 

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\V1\ResourceController;
use App\Models\Admin\UserContact;
use Validator;
use Config;
use Exception;
use DB;
use Auth;

class UserContactApiController extends ResourceController {

    protected $model = 'App\Models\Admin\UserContact';

    public function updateStatus(Request $request)
    {
        $flag = false;
        $user = Auth::user();
        if( $user->hasRole('admin') || $user->hasRole('human-resources') )
        {
            $flag = true;
        }
        if ( !$flag )
        {
            return $this->failResponse(['success' => false,'message' => 'Insufficient privileges']);
        }
        if ( empty($user) )
        {
            return $this->failResponse(['success' => false,'message' => 'Invalid request']);
        }
        $validator = Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->failResponse($validator->errors());
        }
        $userContact = UserContact::find($request->id);
        if (!empty($userContact) )
        {
            $toggle = UserContact::where('id', $userContact->id)->update(['status' => !$userContact->status]);
            return $this->successResponse(['success' => true,'message' => 'Successfully Updated']);
        }
        else{
            return $this->failResponse(['success' => false,'message' => 'User Contact Details not found']);
        }
	}

    public function deleteUserContact(Request $request)
    {
        $user=\Auth::user();
        if ( empty($user) )
        {
            return $this->failResponse(['success' => false,'message' => 'Invalid request']);
        }
        $validator = Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->failResponse($validator->errors());
        }
        $userContact = UserContact::find($request->id);
        if (!empty($userContact) )
        {
            if ($userContact->delete())
            {
                return $this->successResponse(['success' => true,'message' => 'Successfully deleted']);
            }
            else
            {
                return $this->failResponse(['success' => false,'message' => 'Could not delete the user contact']);
            }
        }
        else{
            return $this->failResponse(['success' => false,'message' => 'User Contact Details not found']);
        }
    }
}
