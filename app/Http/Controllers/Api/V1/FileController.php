<?php 

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\V1\ResourceController;
use App\Services\FileService;
use App\Models\Admin\File;
use Validator;
use Auth;

class FileController extends ResourceController {
    protected $model = "\App\Models\Admin\File";

    public function store()
    {

        if(empty($_FILES))
            return $this->failResponse("Invalid inputs");
        $file = $_FILES;
        
        $respnse = FileService::uploadFileInStorage($file);
        if(isset($respnse['status']) && $respnse['status'] == false ) {
            $message = "Unable to upload";
            
            if(empty($respnse['message']))
                $message = $respnse['message'];
            return $this->failResponse($message);
        }
        if(isset($respnse['status']) && $respnse['status'] == true) {
            return $this->successResponse($respnse['data']);
        }
        \Log::info(json_encode($file));

        // //{"file":{"name":"Slack-Private.txt","type":"text\/plain","tmp_name":"\/private\/var\/tmp\/phpEnh8sc","error":0,"size":138920}}

       

        ///Users/amaan/Sites/projects/GeekyAnts-Portal/storage/uploads/
        // FileService::uploadFile($file, '/Users/varun/Sites/projects/GeekyAnts-portal/storage/temp/test.txt');
        // if ($file->hasFile('file')) {
        //     $reference_type = 'App\Models\Admin\Asset';
        //     $prefix = 'asset';
        //     // print_r($reference_type);
        //     foreach ($request->file as $single_file) {
        //         $fileSize = $single_file->getClientSize();
        //         if ($fileSize > 20000000) {
        //             return Redirect::back()->withInput()->withErrors(['Filesize exceeds 20MB']);
        //         }
        //         $type = $single_file->extension();
        //         $fileName = FileService::getFileName($type);
        //         $originalName = $single_file->getClientOriginalName();
        //         $upload = FileService::uploadFile($single_file, $fileName, $reference_id, $reference_type, $originalName, $prefix);
        //     }
        // }


            if($file->status=='true' ){
                return $this->successResponse("Image upload done and stored");

            }
           
     
        // $response['status'] = true;
        // $response['file'] = $fileObj;
        // // $response['message'] = "Successfull";
        // $obj = new File;
        // $obj->name = $name;
        // $obj->path = $path;
        // if($obj->save()){
        //     return $this->successResponse($obj);
        // }else{
        //     return $this->failResponse("Validation failed".json_encode($obj->getErrors()));
        // }
        // if (\Input::has('reference_id') && \Input::has('reference_type')) {
        //     $reference_type = \Input::get('reference_id');
        //     $reference_type = \Input::get('reference_type');
        // }
        // if ( $user && $feedbackMonthId != 0 )
        // {
        //     $res = FeedbackService::getUserList($user->id, $feedbackMonthId);
            
        //     return $this->successResponse($res, '');
        // }
        // else {
        //     return $this->failResponse("Input parameters missing");
        // }    
    }


}
