<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Admin\ReferenceNumber;
use Illuminate\Http\Request;
use Validator;

class ReferenceNumberController extends ResourceController
{
    protected $model = "\App\Models\Admin\ReferenceNumber";

    public function cancel(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->failResponse($validator->errors());
        }
        $id = $input['id'];
        $referenceObj = ReferenceNumber::cancelReference($id);
        
        if( $referenceObj['status'] ) {
            return $this->successResponse($referenceObj);
        } else {
            return $this->failResponse($referenceObj['message']);
        }
    }
    
}
