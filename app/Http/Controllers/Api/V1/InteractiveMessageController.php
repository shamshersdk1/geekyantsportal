<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\V1\BaseApiController;

use App\Models\Admin\ProjectSlackChannel;
use App\Models\Admin\SlackUser;
use App\Services\SlackService\SlackConstants;

use View;

class InteractiveMessageController extends BaseApiController
{
    public function setProject(Request $request) {
		try {
			$payload = json_decode($request->payload, true);
			$project_id = $payload['actions'][0]['selected_options'][0]['value'];
			$channel_id = $payload['channel']['id'];
			$user_slack_id = $payload['user']['id'];
			$callback_id = $payload['callback_id'];
			if(!$user || !$user->isAdmin()) {
				$error = SlackConstants::ACCESS_DENIED;
				$text = View::make('slack.error.error', compact('error'))->render();
			} else {
				if($callback_id == "project_connect") {
					$slack_user = SlackUser::with('users')->where('slack_id', $user_slack_id)->first();
					$user = $slack_user->user;
					$response = ProjectSlackChannel::connect($project_id, $channel_id);
					$message = $response['message'];
					$text = View::make('slack.connect.success', compact('message'))->render();
					return ["response_type" => "ephemeral", "text" => $text];
				} elseif($callback_id == "disconnect_confirmation") {
					$projectSlack = ProjectSlackChannel::with('project')->where('slack_id', $channel_id)->first();
					$project = $projectSlack->project;
					$message = SlackConstants::projectRemoved($project->project_name);
					$projectSlack->delete();
					$text = View::make('slack.disconnect.success', compact('message'))->render();
					return ["response_type" => "ephemeral", "text" => $text];
				}
			}
		} catch(Exception $e) {
			$error = SlackConstants::SOMETHING_WENT_WRONG;
			$text = View::make('slack.error.error', compact('error'))->render();
			return ["response_type" => "ephemeral", "text" => $text];
		}
	}
}
