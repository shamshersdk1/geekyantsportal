<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\V1\BaseApiController;
use App\Models\Admin\Cron;
use Config;
use Illuminate\Http\Request;

class CronApiController extends BaseApiController
{
    public function ShowBillSchedule($id)
    {
        $cron = Cron::where('reference_id', $id)->where('reference_type', 'App\Models\Admin\BillingScheduleReminder')->first();
        if ($cron) {
            return $this->successResponse($cron);
        } else {
            return $this->failResponse('Invalid link followed');
        }
    }
    public function store(Request $request, $id)
    {
        $data = $this->getRequestBody();
        if (empty($data['token'])) {
            return $this->failResponse('Token not specified');
        }
        if ($data['token'] !== Config::get('app.geekyants_portal_token')) {
            return $this->failResponse('Authentication Failed!');
        }
        $status = true;
        $message = '';

        $billSchedule = new Cron();
        $billSchedule->title = $data['title'];
        $billSchedule->event_type = $data['event_type'];
        $billSchedule->event_term = !empty($ata['event_term']) ? $ata['event_term'] : null;
        $billSchedule->reference_type = $data['reference_type'];
        $billSchedule->reference_id = $id;
        $billSchedule->start_date = $data['start_date'];
        $billSchedule->end_date = !empty($data['end_date']) ? $data['end_date'] : null;
        $billSchedule->remind_at = $data['remind_at'];
        $billSchedule->sub_type = !empty($data['sub_type']) ? $data['sub_type'] : null;
        $billSchedule->end_date = !empty($data['end_date']) ? $data['end_date'] : null;
        $billSchedule->mon = !empty($data['mon']) ? $data['mon'] : 0;
        $billSchedule->tue = !empty($data['tue']) ? $data['tue'] : 0;
        $billSchedule->wed = !empty($data['wed']) ? $data['wed'] : 0;
        $billSchedule->thu = !empty($data['thu']) ? $data['thu'] : 0;
        $billSchedule->fri = !empty($data['fri']) ? $data['fri'] : 0;
        $billSchedule->first_working_day = !empty($data['first_working_day']) ? $data['first_working_day'] : 0;
        $billSchedule->last_working_day = !empty($data['last_working_day']) ? $data['last_working_day'] : 0;

        if ($billSchedule->save()) {
            $message = "Successfully Saved!";
        } else {
            $status = false;
            $message = $billSchedule->getErrors();
        }
        $response['status'] = $status;
        $response['message'] = $message;
        $response['data'] = $billSchedule;

        return $response;
    }
    public function show($id, $bill_schedule_id)
    {
        $cron = Cron::where('id', $bill_schedule_id)->get();
        if ($cron) {
            $result = [];
            $result['data'] = $cron;
            return $this->successResponse($result);
        } else {
            return $this->failResponse('Invalid link followed');
        }

    }
    public function update(Request $request, $id, $bill_schedule_id)
    {
        $data = $this->getRequestBody();
        if (empty($data['token'])) {
            return $this->failResponse('Token not specified');
        }
        if ($data['token'] !== Config::get('app.geekyants_portal_token')) {
            return $this->failResponse('Authentication Failed!');
        }
        $status = true;
        $message = '';

        $billSchedule = Cron::find($bill_schedule_id);
        $billSchedule->title = $data['title'];
        $billSchedule->event_type = $data['event_type'];
        $billSchedule->event_term = !empty($ata['event_term']) ? $ata['event_term'] : null;
        $billSchedule->reference_type = $data['reference_type'];
        $billSchedule->reference_id = $id;
        $billSchedule->start_date = $data['start_date'];
        $billSchedule->end_date = !empty($data['end_date']) ? $data['end_date'] : null;
        $billSchedule->remind_at = $data['remind_at'];
        $billSchedule->sub_type = !empty($data['sub_type']) ? $data['sub_type'] : null;
        $billSchedule->end_date = !empty($data['end_date']) ? $data['end_date'] : null;
        $billSchedule->mon = !empty($data['mon']) ? $data['mon'] : 0;
        $billSchedule->tue = !empty($data['tue']) ? $data['tue'] : 0;
        $billSchedule->wed = !empty($data['wed']) ? $data['wed'] : 0;
        $billSchedule->thu = !empty($data['thu']) ? $data['thu'] : 0;
        $billSchedule->fri = !empty($data['fri']) ? $data['fri'] : 0;
        $billSchedule->first_working_day = !empty($data['first_working_day']) ? $data['first_working_day'] : 0;
        $billSchedule->last_working_day = !empty($data['last_working_day']) ? $data['last_working_day'] : 0;

        if ($billSchedule->save()) {
            $message = "Successfully updated!";
        } else {
            $status = false;
            $message = $billSchedule->getErrors();
        }
        $response['status'] = $status;
        $response['message'] = $message;
        $response['data'] = $billSchedule;

        return $response;

    }
}
