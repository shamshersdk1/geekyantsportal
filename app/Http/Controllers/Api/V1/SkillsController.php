<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Models\Admin\ResourcePrice;
use Validator;
use App\Models\Admin\Skill;
use App\Models\Admin\UserSkills;

use Auth;

class SkillsController extends ResourceController
{
    protected $model = "\App\Models\Admin\Skill";


    public function show($id){

        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
       
        $skill_id=UserSkills::where('user_id',$id)->pluck('skill_id');
            // print_r($skill_id[0]);
        foreach($skill_id as $id){
            $skills=Skill::where('id',$id)->pluck('skill_name')->first();
            
            $skill_name[]=$skills;
        }  


        if(!$skill_name){
            return $this->failResponse("No Skill Found");
        }
            return $this->successResponse($skill_name);
      
    }
    // public function index($query = null)
    // {
    //     if (!$query) {
    //         $query = $this->obj->buildQuery();
    //     }
    //     $userObj = \Auth::User();
    //     if (!$userObj) {
    //         return $this->failResponse("Invalid Access");
    //     }
       
    //     $skills=UserSkill::get();
       
    //             return $this->successResponse($skills);
      
    // }

    public function store(){
        $input=$this->getRequestBody();
       
        if(!$input){
            return $this->failResponse("Input Item Empty");

        }

        $string=$input['items'];
        $skill=strtolower($string);
       
        $user_id=$input['id'];

        $unique_skill=Skill::where('skill_name',$skill)->first();

        if(!$unique_skill){
            $response = $this->model::saveData($skill);  //IF Skill is unique save skill in skill table as well as User Skill Table
            $skill_id=Skill::where('skill_name',$skill)->pluck('id');
            $result=UserSkills::saveData($skill_id,$user_id);

        }
       else{
        $skill_id=Skill::where('skill_name',$skill)->pluck('id');

        $result=UserSkills::saveData($skill_id,$user_id);

       }



        
    }
}