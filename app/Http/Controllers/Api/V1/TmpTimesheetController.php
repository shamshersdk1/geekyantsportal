<?php 

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
// use App\Http\Controllers\Api\V1\BaseApiController;
use App\Http\Controllers\Api\V1\ResourceController;
use App\Services\TimesheetService;
use App\Models\TmpTimesheet;
use App\Services\SlackService\SlackApiServices;
use Validator;
use Auth;

class TmpTimesheetController extends ResourceController {
    protected $model = "\App\Models\TmpTimesheet";

    public function index($query = null)
    {
        $teamLeadId = null;
        $userObj = Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if (!$query) {
            $query = $this->obj->buildQuery();
        }

        if (\Input::has('order_by_field') && \Input::has('order_by_type')) {
            $query = $query->orderBy(\Input::get('order_by_field'), \Input::get('order_by_type'));
        }

        $relationsAsStr = \Input::has('relations') ? \Input::get('relations') : '';
        $relationsAsArr = array_map('trim', array_filter(explode(',', $relationsAsStr)));
        $query->with($relationsAsArr);
        if ( !$teamLeadId )
        {
            $query = $query->where('user_id',$userObj->id);
        }
        $query = $query->orderBy('date','DESC');

        $pagination = \Input::has('pagination') ? \Input::get('pagination') : 1;

        if ($pagination == 0) {

            $sendData = $query->get()->toArray();

            return $this->successResponse($sendData);

        } else {

            $perPage = \Input::has('per_page') ? \Input::get('per_page') : $this->per_page;

            $total = $query->count();

            $paginator = $query->paginate($perPage);

            $result = $paginator->toArray();

            $perPage = $result['per_page'];
            $currentPage = $result['current_page'];
            $modifiedResult = [];
            foreach( $result['data'] as $row )
            {
                
                $channel = SlackApiServices::getChannelName($row['channel_id']);
                $record = [];
                $record['id'] = $row['id'];
                $record['user_id'] = $row['user_id'];
                $record['channel_name'] = $channel['data'];
                $record['duration'] = $row['duration'];
                $record['task'] = $row['task'];
                $record['date'] = $row['date'];
                $modifiedResult[] = $record;
            }

            $metaData = $this->formatMetaData($total, $perPage, $currentPage);
            $sendData = $modifiedResult;

            return $this->successResponse($sendData, $metaData);

        }
    }

	public function update($id)
    {
        $input = $this->getRequestBody();
        $input = $input['data'];
        
        $res = TimesheetService::updateTmpTimesheet($input['id'], $input['duration'], $input['task']);
        if( $res['status'] ) {
            return $this->successResponse($res);
        }
        else{
            return $this->failResponse($res['message']);
        }

    }

    public function destroy($id)
    {
        $user = Auth::user();
        $tmpTimesheetObj = TmpTimesheet::find($id);
        if ( !$tmpTimesheetObj )
        {   
            return $this->failResponse('No record found');
        }
        else
        {
            if( $tmpTimesheetObj->user_id != $user->id)
            {
                return $this->failResponse('Unauthorized');
            }
            else
            {
                TimesheetService::deleteTmpTimesheet($id);
                return $this->successResponse('Record Deleted');
            }
        }
    }
    

}
