<?php 

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\V1\ResourceController;
use App\Models\Admin\CmsDeveloper;
use App\Models\Admin\CmsTechnologyDeveloper;
use App\Models\Admin\CmsSlide;
use Validator;
use Config;
use Exception;
use DB;
use Auth;

class CmsDeveloperApiController extends ResourceController {
    protected $model = 'App\Models\Admin\CmsDeveloper';

    public function updateStatus(Request $request)
    {

        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->failResponse($validator->errors());
        }
        $id = $input['id'];
        $cmsObj = CmsDeveloper::find($id);
        $toggle = CmsDeveloper::where('id', $id)->update(['status' => !$cmsObj->status]);
        return $this->successResponse(['success' => true,'message' => 'Successfully Updated']);
	}
    public function updateDeveloperStatus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->failResponse($validator->errors());
        }
        $id = $request->id;
        $cmsObj = CmsTechnologyDeveloper::find($id);
        $toggle = CmsTechnologyDeveloper::where('id', $id)->update(['status' => !$cmsObj->status]);
        return $this->successResponse(['success' => true,'message' => 'Successfully Updated Developer Status']);
    }
    public function updateSlideStatus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->failResponse($validator->errors());
        }
        $id = $request->id;
        $cmsObj = CmsSlide::find($id);
        $toggle = CmsSlide::where('id', $id)->update(['status' => !$cmsObj->status]);
        return $this->successResponse(['success' => true,'message' => 'Successfully Updated Slide Status']);
    }
}
