<?php 

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\V1\BaseApiController;

use App\Models\Admin\UserTask;
use App\Models\Admin\SharedTask;

class UserTaskController extends BaseApiController
{
    public function store(Request $request)
    {
        $text = empty($request->text) ? null : $request->text;
        $date = empty($request->date) ? null : date("Y-m-d" ,strtotime($request->date));
        $is_note = empty($request->is_note) || $request->is_note == 'false' ? false : $request->is_note;
        $developer_id = $request->user_id;
        
        $response = UserTask::saveData($date, $developer_id, $text, $is_note);
        if($response['status']) {
            return $this->successResponse($response['message']);
        } else {
            return $this->failResponse($response['message']);
        }
    }
}
