<?php

namespace App\Http\Controllers\Api\V1;

class CmsProjectController extends ResourceController
{
    protected $model = "\App\Models\Admin\CmsProject";

    public function index($query = null)
    {
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if (!$userObj->hasRole('admin') && !$userObj->hasRole('office-admin')) {
            return $this->failResponse('Unauthorized Access');
        }

        $projects = $this->model::all();
        return $this->successResponse($projects);
    }

}
