<?php

namespace App\Http\Controllers\Api\V1\Slack;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\SlashCommandService;
use App\Services\LeaveService;


use App\Models\Admin\Leave;
use App\Models\User;
use App\Models\SystemSetting;
use App\Models\Admin\SlackAccessLog;
use App\Services\SlackService\SlackConstants;

use Auth;
use Redirect;
use Exception;
use Throwable;
use View;
use Config;

class SlackInteractiveComponentsController extends Controller
{
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //
    }

	public function store(Request $request)
	{
        try {
			$payload = json_decode($request->payload, true);
            if(!empty($payload['type']) && $payload['type'] == 'block_actions') {
                $val = !empty($payload['actions'][0]['action_id'])?$payload['actions'][0]['action_id']:NULL;
            } else {
			     $val = $payload['callback_id'];
            }
            
            $val = str_replace("_", "", ucwords($val, "_"));

            $class = SlackConstants::INTERACTIVE_COMPONENT_NAMESPACE."\\".$val;
            $result = $class::main($request);
            if($result) {
				$from = "Slack user id: ".$result['user_id'];
				$to = $result['reference_id'];
				SlackAccessLog::saveData($from, $to, 'a', json_encode($payload));
				$response =  $result['response'];
			}
		}
		catch (Exception $e)
		{
            $from = "Slack bot";
			$to = empty($payload['user']['id']) ? null : "Slack user id: ".$payload['user']['id'];
			SlackAccessLog::saveData($from, $to, json_encode($payload['original_message']), json_encode($payload));
			$error = SlackConstants::SOMETHING_WENT_WRONG;
			$message = View::make('slack.error.error', compact('error'))->render();
			$response = ["response_type" => "ephemeral", "replace_original" => false, "text" => $message];
        }
        return $response;
	}
	public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
