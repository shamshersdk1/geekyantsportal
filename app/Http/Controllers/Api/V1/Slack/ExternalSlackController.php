<?php

namespace App\Http\Controllers\Api\V1\Slack;

use App\Http\Controllers\Api\V1\BaseApiController;
use App\Jobs\Slack\SlackReminder\External\ExternalApiCallChannel;
use App\Jobs\Slack\SlackReminder\External\ExternalApiCallFollowup;
use App\Jobs\Slack\SlackReminder\External\ExternalApiCallUser;
use App\Services\External\MessagingService;
use Config;
use Illuminate\Http\Request;

class ExternalSlackController extends BaseApiController
{
    public function sendMessageUser(Request $request)
    {

        $slack_token = Config::get('slack.slack_api_token');

        if ($slack_token == $request->token) {
            $job = (new ExternalApiCallUser($request->user_id, $request->message_text))->onQueue('slack-external-message');
            dispatch($job);
            return $this->successResponse('Message Sent');
        }
        return $this->successResponse('Unauthorized');
    }

    public function sendMessageChannel(Request $request)
    {

        $slack_token = Config::get('slack.slack_api_token');

        if ($slack_token == $request->token) {
            if (!empty($request->channel_id) && !empty($request->lead_id) && !empty($request->name) && !empty($request->email) && !empty($request->company)
                && !empty($request->created_at)) {
                $job = (new ExternalApiCallChannel($request->channel_id, $request->lead_id, $request->name, $request->email, $request->company, $request->created_at,
                    $request->skype, $request->referred_by, $request->requirement))->onQueue('slack-external-message');
                dispatch($job);
                return $this->successResponse('Message Sent');
            }
            return $this->successResponse('Missing arguments');
        }
        return $this->successResponse('Unauthorized');
    }
    public function sendFollowUpMessage(Request $request)
    {

        $slack_token = Config::get('slack.slack_api_token');

        if ($slack_token == $request->token) {
            if (!empty($request->channel_id) && !empty($request->lead_id) && !empty($request->name) && !empty($request->email) && !empty($request->company)
                && !empty($request->created_at)) {
                $job = (new ExternalApiCallFollowup($request->channel_id, $request->lead_id, $request->name, $request->email, $request->company, $request->created_at,
                    $request->skype))->onQueue('slack-external-message');
                dispatch($job);
                return $this->successResponse('Message Sent');
            }
            return $this->successResponse('Missing arguments');
        }
        return $this->successResponse('Unauthorized');
    }
    public function sendAssignedToMessage(Request $request)
    {
        $slack_token = \Config::get('slack.slack_api_token');

        if ($slack_token == $request->token) {
            if (!empty($request->channel_id) && !empty($request->lead_id) && !empty($request->name) && !empty($request->email) && !empty($request->company)
                && !empty($request->created_at) && !empty($request->assigned_to)) {

                $data = $request->all();
                $template = \View::make('messaging-app.slack.external.assigned-to-msg', ['lead' => $request->name, 'assinedTo' => $request->assigned_to])->render();
                MessagingService::sendNotificationOnChannel($template, $data);
                return $this->successResponse('Message Sent');

            }
            return $this->successResponse('Missing arguments');

        }
        return $this->successResponse('Unauthorized');

    }

}
