<?php

namespace App\Http\Controllers\Api\V1\Slack;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\V1\BaseApiController;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\SlackService\SlackMessageService\SlackMessageService;
use App\Jobs\Slack\SlackSendMessage;
use App\Models\User;

use Auth;
use Redirect;
use View;

class SlackMessageApiController extends BaseApiController
{
    public function sendMessageToUser(Request $request) {
        if(empty($request->user_id) || empty($request->message)) {
            return $this->failResponse('User Id and message are required');
        }
        $user = User::find($request->user_id);
        if(!$user) {
            return $this->failResponse('User not found');
        }
        $as_user = empty($request->as_user) ? null : $request->as_user;
        $botname = empty($request->botname) ? null : $request->botname;
        $attachments = empty($request->attachments) ? null : $request->attachments;
        $args = ['user_id' => $request->user_id,
                 'message' => $request->message,
                 'attachments' => $attachments,
                 'send_to' => "user"
                ];
        $job = (new SlackSendMessage($args))->onQueue('slack-send-message');
        dispatch($job);
        return $this->successResponse('Message sent');
    	// $result = SlackMessageService::sendMessageToUser($request->user_id, $request->message, $attachments);
        // if($result['status']) {
        // } else {
        //     return $this->failResponse($result['message']);
        // }
    }
    public function sendMessageToChannel(Request $request) {
        if(empty($request->channel_id) || empty($request->message)) {
            return $this->failResponse('Channel Id and message are required');
        }
        $as_user = empty($request->as_user) ? null : $request->as_user;
        $botname = empty($request->botname) ? null : $request->botname;
        $link_names = empty($request->link_names) ? null : $request->link_names;
        $args = ['channel_id' => $request->channel_id,
                 'message' => $request->message,
                 'link_names' => $link_names,
                 'send_to' => "channel"         
                ];
        $job = (new SlackSendMessage($args))->onQueue('slack-send-message');
        dispatch($job);
        return $this->successResponse('Message sent');
    	// $result = SlackMessageService::sendMessageToChannel($request->channel_id, $request->message, $asUser, $botName, $linkNames);
        // if($result['status']) {
        // } else {
        //     return $this->failResponse($result['message']);
        // }
    }
}
