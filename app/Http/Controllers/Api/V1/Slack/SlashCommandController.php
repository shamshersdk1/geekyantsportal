<?php

namespace App\Http\Controllers\Api\V1\Slack;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\SlashCommandService;
use App\Services\LeaveService;


use App\Models\Admin\Leave;
use App\Models\User;
use App\Models\SystemSetting;
use App\Models\Admin\SlackAccessLog;
use App\Services\SlackService\SlackConstants;

use Auth;
use Redirect;
use Exception;
use Throwable;
use View;
use Config;

class SlashCommandController extends Controller
{
    public function saveNotes(Request $request) {
    	$result = SlashCommandService::saveNotes($request);
    	if($result) {
    		return $result;
    	}
	}
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //
    }

	public function store(Request $request)
	{
		$message = $request->input('text');
		$arr = explode(' ',trim($message));
		if(isset($arr[0])) {
			$val = strtolower($arr[0]);
			if($val == 'help' || trim($val) == "" || empty($val)) {
				$attachments = [];
				$commands = Config::get('slack.slashcommands');
				foreach($commands as $command)
				{
					// $class = "\App\Services\SlackService\SlackListnerService\SlashCommand\\".$command;
					$helpArray = $command::help();
					if($helpArray && is_array($helpArray)) {
						foreach($helpArray as $help)
						{
							$attachments[] = $help;
						}
					}
				}
				return ["response_type" => "epmermal", "text" => "Use the following formats:",
											"attachments" => $attachments];
			}
			try
			{
				$className = ucfirst(str_replace('-','',$val));
				
				$class = SlackConstants::SLASH_COMMAND_NAMESPACE."\Slack".$className;
				$result = $class::main($request);
				if($result) {
					$from = "Slack user id: ".$request->input('user_id');
					$to = "Slack channel id: ".$request->input('channel_id');
					SlackAccessLog::saveData($from, $to, json_encode($request->all()), json_encode($result));
					return $result;
				}
			}
			catch (Exception $e)
			{
				$from = empty($request->input('user_id')) ? null : "Slack user id: ".$request->input('user_id');
				$to = empty($request->input('channel_id')) ? null : "Slack channel id: ".$request->input('channel_id');
				SlackAccessLog::saveData($from, $to, json_encode($request->all()), $e->getMessage());
				$error = SlackConstants::INCORRECT_FORMAT;
				$message = View::make('slack.error.error', compact('error'))->render();
				return ["response_type" => "ephemeral", "text" => $message];
			}
			catch(Throwable $e)
			{
				
				$from = empty($request->input('user_id')) ? null : "Slack user id: ".$request->input('user_id');
				$to = empty($request->input('channel_id')) ? null : "Slack channel id: ".$request->input('channel_id');
				SlackAccessLog::saveData($from, $to, json_encode($request->all()), $e->getMessage());
				$error = SlackConstants::INCORRECT_FORMAT;
				$message = View::make('slack.error.error', compact('error'))->render();
				return ["response_type" => "ephemeral", "text" => $message];
			}
		} else {
			$error = SlackConstants::NO_COMMAND;
			$message = View::make('slack.error.error', compact('error'))->render();
			return ["response_type" => "ephemeral", "text" => $message];
		}
	}
	public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
