<?php

namespace App\Http\Controllers\Api\V1\Leave;

use App\Http\Controllers\Api\V1\ResourceController;

class LeaveTypeController extends ResourceController
{
    protected $model = "\App\Models\Admin\LeaveType";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

}
