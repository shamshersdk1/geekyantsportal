<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Admin\Education;

class EducationController extends ResourceController
{
    protected $model = "\App\Models\Admin\Education";

    public function show()
    {

        $query = Education::get();
        if (empty($query->toArray())) {
            return $this->failResponse("No record found");
        }
        return $this->successResponse($query);
    }

}
