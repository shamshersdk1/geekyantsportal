<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Admin\Vendor;
use App\Models\Admin\VendorServicesName;
use App\Services\VendorServices;

class VendorController extends ResourceController
{
    protected $model = "\App\Models\Admin\Vendor";

    public function index($query = null)
    {
        if (!$query) {
            $query = $this->obj->buildQuery();
        }
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if ($userObj && !($userObj->hasRole('admin') || $userObj->hasRole('office-admin'))) {
            return $this->failResponse("Invalid Access");
        }
        if (\Input::has('q')) {
            $query = $query->where('name', 'like', '%' . \Input::get('q') . '%')->orWhere('type', 'like', '%' . \Input::get('q') . '%')
                ->orWhere('address', 'like', '%' . \Input::get('q') . '%')->orWhere('pan', 'like', '%' . \Input::get('q') . '%')
                ->orWhere('gst', 'like', '%' . \Input::get('q') . '%');
        }
        $query = $query->with('vendorContacts', 'vendorServices.vendorServicesName');
        $pagination = \Input::has('pagination') ? \Input::get('pagination') : 1;
        $query = $query->orderBy('id', 'DESC');
        if ($pagination == 0) {

            $sendData = $query->get()->toArray();
            return $this->successResponse($sendData);

        } else {

            $perPage = \Input::has('per_page') ? \Input::get('per_page') : $this->per_page;

            $total = $query->count();

            $paginator = $query->paginate($perPage);

            $result = $paginator->toArray();

            $perPage = $result['per_page'];
            $currentPage = $result['current_page'];

            $metaData = $this->formatMetaData($total, $perPage, $currentPage);
            $sendData = $result['data'];

            return $this->successResponse($sendData, $metaData);

        }
    }

    public function store()
    {
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if ($userObj && !($userObj->hasRole('admin') || $userObj->hasRole('office-admin'))) {
            return $this->failResponse("Invalid Access");
        }
        $input = $this->getRequestBody();
        $res = VendorServices::saveData($input['data']);
        if ($res['status']) {
            return $this->successResponse('Successfully saved');
        } else {
            return $this->failResponse("Unable to save");
        }

    }

    public function show($id)
    {
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if ($userObj && !($userObj->hasRole('admin') || $userObj->hasRole('office-admin'))) {
            return $this->failResponse("Invalid Access");
        }
        $obj = Vendor::with('vendorContacts', 'vendorServices.vendorServicesName')->find($id);
        if (!$obj) {
            return $this->failResponse("Invalid #" . $id);
        }
        return $this->successResponse($obj);
    }

    public function update($id)
    {
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if ($userObj && !($userObj->hasRole('admin') || $userObj->hasRole('office-admin'))) {
            return $this->failResponse("Invalid Access");
        }
        $input = $this->getRequestBody();
        \Log::info(json_encode($input['data']));
        $res = VendorServices::updateData($id, $input['data']);
        if ($res['status']) {
            return $this->successResponse('Successfully saved');
        } else {
            return $this->failResponse("Unable to save");
        }
    }

    public function destroy($id)
    {
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if ($userObj && !($userObj->hasRole('admin') || $userObj->hasRole('office-admin'))) {
            return $this->failResponse("Invalid Access");
        }
        if (Vendor::find($id)->delete()) {
            return $this->successResponse('Deleted successfully');
        } else {
            return $this->failResponse("Couldn't delete the record");
        }
    }

    public function getServices()
    {
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if ($userObj && !($userObj->hasRole('admin') || $userObj->hasRole('office-admin'))) {
            return $this->failResponse("Invalid Access");
        }
        $services = VendorServicesName::all();
        return $this->successResponse($services);
    }
}
