<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\V1\BaseApiController;
use Illuminate\Http\Request;
use App\Models\Notify;
use App\Models\Location;
use Input;

class notifyController extends BaseApiController
{    
    private function checkAuthKey(){

        $authkey = Input::get('authkey');
        $locationObj = Location::where('secret', $authkey)->first();
        // if($authkey == config('app.shared_key_auth_notify')){
        //     return true;
        // }
        if($locationObj)
            return $locationObj->id;

        return false;
    }

    public function index(){
        $locationId = $this->checkAuthKey();
        if(!$locationId){
            return $this->failResponse('Invalid Auth Key = '.Input::get('authkey'));
        }

        $item = Notify::where('location_id',$locationId)->first();
        if($item && $item->id > 0){
            return $this->successResponse($item->toArray());
        }
        
        return $this->failResponse('Nothing in queue');
    }

    public function show($id){
        $locationId = $this->checkAuthKey();
        if(!$locationId){
            return $this->failResponse('Invalid Auth Key');
        }

        $item = Notify::where('id',$id)->where('location_id',$locationId)->first();

        if($item && $item->id == $id){
           return $this->successResponse($item->toArray());
        }
        return $this->failResponse('Invalid ID');   
    }


    public function destroy($id){
        $locationId = $this->checkAuthKey();
        if(!$locationId){
            return $this->failResponse('Invalid Auth Key');
        }
        $item = Notify::where('id',$id)->where('location_id',$locationId)->first();
        //$item = Notify::find($id);
        if($item && $item->id == $id){
            if($item->delete()){
                return $this->successResponse("Deleted");
            }else{
                return $this->failResponse('Unable to delete'); 
            }
        }
        return $this->failResponse('Invalid ID');   
    }
}


