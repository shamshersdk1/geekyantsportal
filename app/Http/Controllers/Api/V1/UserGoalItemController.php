<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
// use App\Http\Controllers\Api\BaseApiController;
use App\Http\Controllers\Api\V1\ResourceController;

use App\Models\Admin\Goal;
use App\Models\Admin\Tag;
use App\Models\Admin\UserGoalItem;
use App\Models\Admin\UserGoal;


use Auth;
use Redirect;
use View;
use DB;
use stdClass;


class UserGoalItemController extends ResourceController
{
    protected $model = "\App\Models\Admin\UserGoalItem";

    public function store()
    {
        $input = $this->getRequestBody();
        $response = ['status' => false, 'message' => ""];
        if(!empty($input['user_goal_id']) && !empty($input['goal_id']) && (!empty($input['is_private']) || $input['is_private'] == false) && (!empty($input['is_idp']) || $input['is_idp'] == false)) {
            $response = UserGoalItem::saveItem($input['user_goal_id'], $input['goal_id'], $input['is_private'], $input['is_idp']);
        } else {
            $response['message'] = "Incomplete request";
        }
        if($response['status']) {
            return $this->successResponse($response);
        } else {
            return $this->failResponse($response['message']);
        }
    }

    public function destroy($id)
    {
        $item = UserGoalItem::find($id);
        if($item) {
            $item->delete();
            return $this->successResponse("Goal deleted");
        } else {
            return $this->failResponse("Invalid link followed");
        }
    }

    public function update1(Request $request, $id)
    {
        if($request->user_goal_items) {
            foreach($request->user_goal_items as $item)
            {
                $user_goal_item = UserGoalItem::find($item['id']);
                if($user_goal_item->is_private) {
                    if(!$user_goal_item->is_idp) {
                        $response = Goal::updateGoal($item, $user_goal_item->goal_id);
                        $user_goal_item->is_private = $item['is_private'];
                        $user_goal_item->save();
                    } else {
                        $response = Goal::updateIdp($item, $user_goal_item->goal_id);
                    }
                } else {
                    if($item['is_private'] == true) {
                        $response = Goal::saveGoal($item);
                        if($response['status']) {
                            $user_goal_item->goal_id = $response['id'];
                            $user_goal_item->save();
                        }
                    }
                }
            }
            return $this->successResponse("Goals updated");
        }
        return $this->failResponse("Nothing to update");
    }

    public function update($id)
    {
        $input = $this->getRequestBody();
        if( !empty($input['overall_feedback']) )
        {
            $updateStatus = UserGoal::updateOverallFeedback($id,$input['overall_feedback']);
            if( !$updateStatus['status'] )
            {
                return $this->failResponse("Unable to update overall feedback");
            }
            else
            {
                return $this->successResponse("Feedback Updated");
            }
        }
        if( !empty($input['status']) )
        {
            $updateStatus = UserGoal::updateStatus($id,$input['status']);
            if( !$updateStatus['status'] )
            {
                return $this->failResponse("Unable to update status");
            }
        }
        if ( !empty($input['flag']) )
        {
            $deleteStatus = UserGoalItem::removeTemporaryRecords($id);
            if( !$updateStatus['status'] )
            {
                return $this->failResponse("Unable to delete temporary goal items");
            }

        }
        foreach($input['goals'] as $goal)
        {
            if($goal['id'] == null) {
                UserGoalItem::saveItem($id, $goal);
            } else {
                UserGoalItem::updateItem($goal);
            }
        }
        $returnObj['data'] = UserGoalItem::where('user_goal_id',$id)->orderBy('created_at','ASC')->get();
        return $this->successResponse($returnObj);
    }

    public function updateReview($id)
    {
        $input = $this->getRequestBody();
        $review_comment = !empty($input['review']) ? $input['review'] : '';
        $result = UserGoalItem::updateReviewComment($id, $review_comment);
        if( $result['status'] )
        {
            return $this->successResponse("Goal Item updated");
        }
        else
        {
            return $this->failResponse("Unable to update goal item");
        }   
    }

    public function updatePoints($id)
    {
        $input = $this->getRequestBody();
        $points = !empty($input['points']) ? $input['points'] : '';
        $result = UserGoalItem::updatePoints($id, $points);
        if( $result['status'] )
        {
            return $this->successResponse("Goal Item updated");
        }
        else
        {
            return $this->failResponse("Unable to update goal item");
        }   
    }
}