<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\V1\BaseApiController;
use App\Models\Admin\Bonus;
use App\Models\Admin\Leave;
use App\Models\Admin\PayslipCsvData;
use App\Models\Admin\PayslipCsvMonth;
use App\Models\Audited\Bonus\BonusConfirmed;
use App\Models\Month;
use App\Services\BonusService;
use App\Services\Calendar;
use App\Services\UserService;
use Auth;
use Illuminate\Http\Request;
use Input;

class BonusController extends BaseApiController
{
    public function index(Request $request)
    {
        $id = $request->id;
        $csvMonth = PayslipCsvMonth::find($id);
        if ($csvMonth) {
            $month = $csvMonth->month;
            if ($month < 10) {
                $month = '0' . $month;
            }
            $bonuses = PayslipCsvData::bonuses($id);
            $result = [];
            $result['data'] = $bonuses;
            return $this->successResponse($result);
        } else {
            return $this->failResponse('Invalid link followed');
        }
    }

    public function update($id)
    {
        $user = \Auth::user();

        if (!$user) {
            return $this->failResponse('Invalid access');
        }
        $obj = Bonus::find($id);
        if (!$obj) {
            return $this->failResponse('Invalid #' . $id);
        }

        $input = $this->getRequestBody();

        if (!isset($input['amount'])) {
            return $this->failResponse('Invalid data');
        }

        if (!is_numeric($input['amount'])) {
            return $this->failResponse('Invalid data');
        }

        $obj->amount = $input['amount'];

        if ($obj->save()) {
            return $this->successResponse('Updated successfully');
        }

    }
    public function approvePayment($id)
    {

        $user = \Auth::user();

        if (!$user) {
            return $this->failResponse('Invalid access');
        }
        $obj = Bonus::find($id);

        if (!$obj) {
            return $this->failResponse('Invalid #' . $id);
        }

        if (!($user->hasRole('admin') || $user->hasRole('account-manager'))) {
            return $this->failResponse('Unauthosized access');
        }

        // if ($obj->transaction_id != null) {
        //     return $this->failResponse('The bonus request is already processed for payment');
        // }

        $ifExists = BonusConfirmed::where('bonus_id', $id)->first();
        if ($ifExists) {
            return $this->failResponse('The bonus is already Confirmed');
        }
        $monthObj = Month::where('month', date('m'))->where('year', date('Y'))->first();
        if ($monthObj && $monthObj->bonus_pay_lock_date != null) {
            return $this->failResponse('Bonus Payment Approval is locked for the Month and will be open for Approval Beginning Next Month');
        }

        $response = BonusService::confirmBonus($id);

        if (!$response) {
            return $this->failResponse('Unable to confirm bonus');
        }

        // $transactionId = !empty($response['data']['id']) ? $response['data']['id'] : null;
        $obj->paid_at = date('Y-m-d H:i:s');
        $obj->paid_by = $user->id;
        // $obj->transaction_id = $transactionId;
        $obj->save();
        return $this->successResponse($obj);

    }
    public function getCalender($month, $year)
    {
        $user = \Auth::user();

        if (!$user) {
            return $this->failResponse('Invalid access');
        }
        $userId = $user->id;
        $obj = new Calendar();
        $data = $obj->getCalendar($month, $year);
        foreach ($data['days'] as $index => $day) {
            if ($day) {
                $response = Leave::isOnLeaveToday($userId, $day['date']);
                $data['days'][$index]['user_id'] = $userId;
                if ($response) {
                    $data['days'][$index]['on_leave'] = 'true';
                } else {
                    $data['days'][$index]['on_leave'] = 'false';
                };
                $bonus = BonusService::checkUserBonusDay($userId, $day['date']);
                if ($bonus) {
                    $data['days'][$index]['bonus'] = $bonus;
                } else {
                    $data['days'][$index]['bonus'] = [];
                };
                $onsite = BonusService::onsiteAllowanceEligibility($userId, $day['date']);
                $data['days'][$index]['onsite_eligible'] = $onsite['status'];
                $data['days'][$index]['onsite_allowance'] = $onsite['allowanceObj'];
                $projects = UserService::getTimesheetProjects($userId, $day['date']);
                $data['days'][$index]['assigned_projects'] = $projects;
            }
        }

        return $this->successResponse($data);
    }

    public function getProjectByDate()
    {
        \Log::info('getProjectByDate called' . json_encode(Input::all()));
        $user = \Auth::user();
        $userId = $user->id;

        if (!$user) {
            return $this->failResponse('Unauthosized access');
        }
        if (!($user->hasRole('admin') || $user->hasRole('human-resource')) && $userId != $user->id) {
            return $this->failResponse('Unauthosized access');
        }

        $projects = UserService::getTimesheetProjects($userId, $date);
        return $this->successResponse($projects);

    }
    public function fetchUserBonus($monthYear, $projectId)
    {
        $userObj = Auth::User();

        if (!$userObj) {
            return $this->failResponse('Unauth access');
        }
        $userId = $userObj->id;
        // $month = 12;
        $month = substr($monthYear, 0, 3);
        $year = substr($monthYear, 4);
        // echo "<pre>";
        // print_r($month);
        // print_r($year);
        // echo "</pre>";
        // die;

        $bonus = BonusService::getUserBonus($userId, $month, $year, $projectId);

        return $this->successResponse([$bonus]);
    }

    public function addOnSiteBonus(Request $request)
    {
        $userObj = Auth::User();

        if (!$userObj) {
            return $this->failResponse('Unauth access');
        }
        $dataParam = request([
            'date',
            'type',
            'is_holiday',
            'is_weekend',
            'on_leave',
            'onsite_allowance_id',
            'project_id',
            'duration',
            'redeem',
        ]);

        $dataParam['user_id'] = $userObj->id;

        // $response = BonusService::applyBonus($userObj->id, $dataParam['date'], $dataParam['type'], $dataParam['is_holiday'],
        //                                         $dataParam['is_weekend'], $dataParam['on_leave'], $dataParam['onsite_allowance_id'],
        //                                         $dataParam['project_id'], $dataParam['duration'], $dataParam['redeem'] );

        $response = BonusService::applyBonusRequest($dataParam);

        if (isset($response['error']) && $response['error']) {
            if (!empty($response['message'])) {
                return $this->failResponse($response['message']);
            }
            return $this->failResponse('Something went wrong');
        }
        return $this->successResponse($response['result']);

    }

    public function addAdditionalWorkingBonus(Request $request)
    {
        $userObj = Auth::User();

        if (!$userObj) {
            return $this->failResponse('Unauth access');
        }
        $userId = $userObj->id;
        $dataParam = request([
            'date',
            'projectId',
        ]);
        $response = BonusService::applyForAdditionalWorkingBonus($userId, $dataParam['date'], $dataParam['projectId']);

        if (isset($response['error']) && $response['error']) {
            if (!empty($response['message'])) {
                return $this->failResponse($response['message']);
            }
            return $this->failResponse('Something went wrong');
        }
        return $this->successResponse($response['result']);

    }

    public function deleteOnSiteBonus($id)
    {
        $userObj = Auth::User();

        if (!$userObj) {
            return $this->failResponse('Unauth access');
        }
        $userId = $userObj->id;
        $response = BonusService::deleteRequestForOnSiteBonus($id);
        if (isset($response['error']) && $response['error']) {
            if (!empty($response['message'])) {
                return $this->failResponse($response['message']);
            }
            return $this->failResponse('Something went wrong');
        }
        return $this->successResponse($response);
    }

    public function deleteBonusRequest($id)
    {
        $userObj = Auth::User();

        if (!$userObj) {
            return $this->failResponse('Unauth access');
        }
        $response = BonusService::deleteBonusRequest($id);
        if ($response) {
            return $this->successResponse('Successfully Deleted');
        } else {
            return $this->failResponse('Something went wrong');
        }

    }

    public function deleteAdditionalWorkingBonus($id)
    {
        $userObj = Auth::User();

        if (!$userObj) {
            return $this->failResponse('Unauth access');
        }
        $userId = $userObj->id;
        $response = BonusService::deleteRequestForAdditionWorkingBonus($id);
        if (isset($response['error']) && $response['error']) {
            if (!empty($response['message'])) {
                return $this->failResponse($response['message']);
            }
            return $this->failResponse('Something went wrong');
        }
        return $this->successResponse($response['result']);
    }

    public function addProjectsToBonus(Request $request)
    {
        $userObj = Auth::User();

        if (!$userObj) {
            return $this->failResponse('Unauth access');
        }
        $input = $request->all();

        if ($input['bonus_request_id'] && isset($input['projects'])) {
            $response = BonusService::addProjectsToBonus($userObj->id, $input['bonus_request_id'], $input['projects']);
            if ($response) {
                return $this->successResponse('Successfully added');
            } else {
                return $this->failResponse('Something went wrong');
            }
        } else {
            return $this->failResponse('Invalid Input');
        }

    }
}
