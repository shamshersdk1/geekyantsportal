<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\V1\ResourceController;
use App\Models\Admin\Company;
use App\Models\Admin\Project;
use App\Models\Admin\ProjectTechnologies;
use App\Models\User;
use App\Models\UserTimesheet;
use App\Services\ProjectService;
use App\Services\SlackService\SlackApiServices;
use Auth;
use Carbon\Carbon;
use Config;
use Illuminate\Http\Request;
use Log;

class ProjectController extends ResourceController
{

    protected $model = "App\Models\Admin\Project";

    public function index($query = null)
    {
        if (!$query) {
            $query = $this->obj->buildQuery();
        }
        if (\Input::has('q')) {
            $query = $query->genericSearch(\Input::get('q'));
        }

        if (\Input::has('order_by_field') && \Input::has('order_by_type')) {
            $query = $query->orderBy(\Input::get('order_by_field'), \Input::get('order_by_type'));
        }

        $relationsAsStr = \Input::has('relations') ? \Input::get('relations') : '';
        $relationsAsArr = array_map('trim', array_filter(explode(',', $relationsAsStr)));

        $query->with($relationsAsArr);

        $pagination = \Input::has('pagination') ? \Input::get('pagination') : 1;
        $status = \Input::has('status') ? \Input::get('status') : null;

        if ( $status )
        {
            $query = $query->where('status',$status);
        }

        if ($pagination == 0) {
            
            $sendData = $query->get()->toArray();
            return $this->successResponse($sendData);

        } else {

            $perPage = \Input::has('per_page') ? \Input::get('per_page') : $this->per_page;

            $total = $query->count();

            $paginator = $query->paginate($perPage);

            $result = $paginator->toArray();

            $perPage = $result['per_page'];
            $currentPage = $result['current_page'];

            $metaData = $this->formatMetaData($total, $perPage, $currentPage);
            $sendData = $result['data'];

            return $this->successResponse($sendData, $metaData);

        }
    }

    public function store()
    {
        $data = $this->getRequestBody();

        if (empty($data['token'])) {
            return $this->failResponse('Token not specified');
        }

        if ($data['token'] !== Config::get('app.geekyants_portal_token')) {
            return $this->failResponse('Authentication Failed!');
        }

        $res = ProjectService::saveData($data);

        if ($res['status']) {
            return $this->successResponse($res);
        } else {
            return $this->failResponse($res);
        }
    }

    public function createProject(Request $request)
    {
        $input = $request->all();
        $authUser = Auth::user();
        if (!$authUser) {
            return $this->failResponse('Invalid Access');
        }
        $res = ProjectService::saveDataNew($input);
        if ($res['status']) {
            return $this->successResponse('Saved Successfully');
        } else {
            return $this->failResponse($res['message']);
        }
    }

    public function editProject(Request $request)
    {
        $input = $request->all();
        $authUser = Auth::user();
        if (!$authUser) {
            return $this->failResponse('Invalid Access');
        }
        $res = ProjectService::updateDataNew($input);
        if ($res['status']) {
            return $this->successResponse('Saved Successfully');
        } else {
            return $this->failResponse($res['message']);
        }
    }
    public function show($id)
    {
        $authUser = Auth::user();
        if (!$authUser) {
            return $this->failResponse('Invalid Access');
        }
        $response = Project::where('id', $id)->with(['projectManager', 'bdManager', 'accountManager', 'projectCategory'])->first();
        if ($response) {
            return $this->successResponse($response);
        } else {
            return $this->failResponse('Does Not Exist');
        }
    }
    public function clientDetail($id)
    {
        $authUser = Auth::user();
        if (!$authUser) {
            return $this->failResponse('Invalid Access');
        }
        $project = Project::where('id', $id)->first();
        $response = Company::where('id', $project->company_id)->with('contacts')->first();
        if ($response) {
            return $this->successResponse($response);
        } else {
            return $this->failResponse('Does Not Exist');
        }
    }

    public function projectResourceInfo(Request $request)
    {
        $authUser = Auth::user();
        if (!$authUser) {
            return $this->failResponse('Invalid Access');
        }
        $input = $request->all();
        $project_id = $input['project_id'];
        $return_data = ProjectService::managerInfo($project_id);
        $result['data'] = $return_data;
        return $this->successResponse($result);
    }

    public function projectTechnologies(Request $request)
    {
        $authUser = Auth::user();
        if (!$authUser) {
            return $this->failResponse('Invalid Access');
        }
        $input = $request->all();
        $project_id = $input['project_id'];
        $return_data = Project::with('technologies')->where('id', $project_id)->first();
        $result['data'] = $return_data;
        return $this->successResponse($result);
    }

    public function setProjectTechnologies(Request $request)
    {
        $authUser = Auth::user();
        if (!$authUser) {
            return $this->failResponse('Invalid Access');
        }
        $input = $request->all();
        $project_id = $input['project_id'];
        $technologies = !empty($input['technologies']) ? $input['technologies'] : null;
        ProjectTechnologies::where('project_id', $project_id)->delete();
        if ($technologies) {
            foreach ($technologies as $technology) {
                $count = ProjectTechnologies::where('project_id', $project_id)->where('technology_id', $technology['id'])->count();
                if ($count == 0) {
                    $projectTechnologyObj = new ProjectTechnologies();
                    $projectTechnologyObj->project_id = $project_id;
                    $projectTechnologyObj->technology_id = $technology['id'];
                    $projectTechnologyObj->save();
                }
            }
        }
        return $this->successResponse('Successfully Updated');
    }

    public function timelog($id)
    {
        $data = [];
        $projectres = UserTimesheet::where('project_id', $id)->with('user')->groupBy('user_id')->get();
        foreach ($projectres as $res) {
            $timelog = [];
            $approvedHours = 0;
            $hours = UserTimesheet::where('project_id', $id)->where('user_id', $res->user_id)
                ->where('date', '>=', Carbon::now()->subMonth())
                ->groupBy('user_id')->sum('duration');
            $userTimesheets = UserTimesheet::where('project_id', $id)->where('user_id', $res->user_id)->where('date', '>=', Carbon::now()->subMonth())->with('review')->get();
            foreach ($userTimesheets as $timesheet) {
                if (!empty($timesheet->review->approved_duration)) {
                    $approvedHours += $timesheet->review->approved_duration;
                }
            }
            $timelog['name'] = $res->user->name;
            $timelog['employee_hours'] = $hours;
            $timelog['approved_hours'] = $approvedHours;
            array_push($data, $timelog);
        }
        if (!empty($data)) {
            return $this->successResponse($data);
        } else {
            return $this->failResponse('empty!');
        }
    }

    public function checkBotInChannel(Request $request)
    {
        $authUser = Auth::user();
        if (!$authUser) {
            return $this->failResponse('Invalid Access');
        }
        $channel_id = $request->channel_id;
        
        $res = SlackApiServices::checkBotInChannel($channel_id);
        return $this->successResponse($res);
    }
}
