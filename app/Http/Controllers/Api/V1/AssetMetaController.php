<?php
namespace App\Http\Controllers\Api\V1;

use App\Models\Admin\AssetMeta;
use App\Models\Admin\MyNetworkDevice;
use Auth;

class AssetMetaController extends ResourceController
{
    protected $model = "\App\Models\Admin\AssetMeta";

    public function index($query = null)
    {
        if (!$query) {
            $query = $this->obj->buildQuery();
        }
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }

        $assetId = \Input::has('asset_id') ? \Input::get('asset_id') : 0;
        $query = $query->where('asset_id', $assetId);
        return parent::index($query);
    }
    public function store()
    {

        $authUser = Auth::user();
        if (!$authUser) {
            return $this->failResponse('Invalid Access');
        }
        if (!($authUser->hasRole('admin')) && !($authUser->hasRole('office-admin'))) {
            return $this->failResponse('Unauthorized Access');
        }
        $input = $this->getRequestBody();

        $key = $input['key'];
        $assetId = $input['asset_id'];
        $value = $input['value'];
        if ($key == 'LAN-MAC' || $key == 'WIFI-MAC') {
            $exist = AssetMeta::where('value', $value)->first();
            $existInNetworkDevice = MyNetworkDevice::where('mac_address', $value)->first();

            if ($exist || $existInNetworkDevice) {
                return $this->failResponse('Mac address already assigned to an asset or network device');
            }
        }
        if (empty($key) || empty($assetId)) {
            return $this->failResponse('Empty data');
        }

        $exist = AssetMeta::where('asset_id', $assetId)->where('key', $key)->first();

        if ($exist) {
            return $this->failResponse('Key "' . $key . '" already exist for the asset');
        }

        $assetMetaObj = new AssetMeta();
        $assetMetaObj->key = $key;
        $assetMetaObj->asset_id = $assetId;
        $assetMetaObj->value = $value;
        if (!$assetMetaObj->save()) {
            return $this->failResponse('Unable to save meta key');
        }
        return $this->successResponse($assetMetaObj);

    }
    public function update($id)
    {
        $authUser = \Auth::user();
        if (!$authUser) {
            return $this->failResponse('Invalid Access');
        }
        $input = $this->getRequestBody();
        $assetObj = $this->model::find($id);

        if (!$assetObj) {
            return $this->failResponse('Invalid meta-key # ' . $id);
        }

        $oldKey = $assetObj->key;
        $newKey = $input['key'];
        $assetId = $input['asset_id'];
        $value = $input['value'];

        if (trim($oldKey) != trim($newKey)) {
            $exist = AssetMeta::where('asset_id', $assetId)->where('key', $newKey)->first();
            if ($exist) {
                return $this->failResponse('meta-key "' . $newKey . '" already added');
            }

        }

        if (!$assetObj) {
            return $this->failResponse('Record not found');
        }
        $assetObj->key = $newKey;
        $assetObj->value = $value;

        if (!$assetObj->save()) {
            return $this->failResponse('Unable to update record');
        }
        $assetObj2 = $this->model::find($id);

        return $this->successResponse($assetObj2);
    }
    public function getMeta()
    {
        $authUser = \Auth::User();
        if (!$authUser) {
            return $this->failResponse("Invalid Access");
        }
        if (!($authUser->hasRole('admin')) && !($authUser->hasRole('office-admin'))) {
            return $this->failResponse('Unauthorized Access');
        }
        $metas = AssetMeta::select('key')->distinct('key')->pluck('key')->toArray();
        return $this->successResponse($metas);
    }
}
