<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\V1\ResourceController;
use App\Models\Admin\AssetMeta;
use App\Models\Admin\IpAddressMapper;
use DB;

class MyNetworkDeviceController extends ResourceController
{
    protected $model = "\App\Models\Admin\MyNetworkDevice";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($query = null)
    {
        if (!$query) {
            $query = $this->obj->buildQuery();
        }
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        $userId = \Input::has('user_id') ? \Input::get('user_id') : 0;
        $query = $query->where('user_id', $userId);
        $query = $query->with('ip.userIp');
        return parent::index($query);
    }

    public function store()
    {
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        $data = $this->getRequestBody();
        if (empty($data)) {
            return $this->failResponse("Empty Data sent");
        }
        $isUsed = $this->model::where('mac_address', $data['mac_address'])->first();
        $isUsedInAsset = AssetMeta::where('value', $data['mac_address'])->first();
        if ($isUsed || $isUsedInAsset) {
            return $this->failResponse("Mac Address is already in use");
        }

        $myDeviceObj = new $this->model;
        $response = $this->model::saveData($data, $myDeviceObj);
        return $response;
    }
    public function show($id)
    {
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        $myDeviceObj = $this->model::where('id', $id)->with('ip.userIp')->first();
        if (!$myDeviceObj) {
            return $this->failResponse("Not found");
        } else {
            return $this->successResponse($myDeviceObj);
        }
    }
    public function update($id)
    {
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        $data = $this->getRequestBody();
        if (empty($data)) {
            return $this->failResponse("Empty Data sent");
        }
        $IpAddressMapperObj = IpAddressMapper::where('reference_type', 'App\Models\Admin\MyNetworkDevice')->where('reference_id', $id)->delete();
        $myDeviceObj = $this->model::find($id);
        $response = $this->model::saveData($data, $myDeviceObj);
        return $response;
    }
    public function destroy($id)
    {
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        DB::beginTransaction();
        $myDeviceObj = $this->model::where('id', $id)->delete();
        if (!$myDeviceObj) {
            DB::rollback();
            return $this->failResponse("Unable to delete device");
        }
        $IpAddressMapperObj = IpAddressMapper::where('reference_type', 'App\Models\Admin\MyNetworkDevice')->where('reference_id', $id)->first();
        if ($IpAddressMapperObj) {
            if (!$IpAddressMapperObj->delete()) {
                DB::rollback();
                return $this->failResponse("Unable to release ip from the device");
            };
        }
        DB::commit();
        return $this->successResponse("Successfully Deleted!");
    }

}
