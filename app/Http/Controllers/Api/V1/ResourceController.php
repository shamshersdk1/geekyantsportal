<?php namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\V1\BaseApiController;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use \App\Models\User;

class ResourceController extends BaseApiController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    protected $obj;
    protected $per_page = 10;

    public function __construct()
    {
        $this->obj = new $this->model;
    }

    public function index($query = null)
    {
        if (!$query) {
            $query = $this->obj->buildQuery();
        }
        
        if (Input::has('q')) {
            $query = $query->genericSearch(Input::get('q'));
        }

        if (Input::has('order_by_field') && Input::has('order_by_type')) {
            $query = $query->orderBy(Input::get('order_by_field'), Input::get('order_by_type'));
        }

        $relationsAsStr = Input::has('relations') ? Input::get('relations') : '';
        $relationsAsArr = array_map('trim', array_filter(explode(',', $relationsAsStr)));

        $query->with($relationsAsArr);

        $pagination = Input::has('pagination') ? Input::get('pagination') : 1;
        if ($pagination == 0) {

            $sendData = $query->get()->toArray();
            return $this->successResponse($sendData);

        } else {

            $perPage = Input::has('per_page') ? Input::get('per_page') : $this->per_page;

            $total = $query->count();

            $paginator = $query->paginate($perPage);

            $result = $paginator->toArray();

            $perPage = $result['per_page'];
            $currentPage = $result['current_page'];

            $metaData = $this->formatMetaData($total, $perPage, $currentPage);
            $sendData = $result['data'];

            return $this->successResponse($sendData, $metaData);

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $sendData = array();
        $data = $this->getRequestBody();

        if (!Auth::user()->isAdmin()) {
            $data['account_id'] = Auth::user()->getSessionAccount()->id;
        }

        if (empty($data)) {
            return $this->failResponse('Bad request', 400, ['Empty data sent']);
        }
        \Log::info('DATA' . json_encode($data));
        $obj = $this->obj->fill($data);

        $hash_fields = $this->obj->getHashFields();

        if (!empty($hash_fields)) {
            if (!empty($hash_fields) && count($hash_fields) > 0) {
                foreach ($hash_fields as $field) {
                    if (!empty($data[$field])) {
                        $obj->$field = Hash::make($data[$field]);
                    }

                }
            }
        }

        $json_fields = $this->obj->getJsonFields();

        if (!empty($json_fields) && count($json_fields) > 0) {
            foreach ($json_fields as $field) {
                if (isset($data[$field])) {
                    $obj->$field = json_encode($data[$field]);
                }

            }
        }

        try
        {
            \DB::beginTransaction();

            if (!$obj->save()) {
                $errors = $obj->getErrors();
                return $this->failResponse('Cannot save data.', 417, $errors);
            }
            \DB::commit();
            return $this->successResponse($obj->toArray());
        } catch (\Exception $e) {
            \DB::rollback();
            return \App\Services\ErrorHandlerService::getResponse($e, $this);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {

        if (empty($id)) {
            return $this->failResponse('Bad request', 400);
        }

        $relationsAsStr = Input::has('relations') ? Input::get('relations') : '';
        $relationsAsArr = array_map('trim', array_filter(explode(',', $relationsAsStr)));

        $obj = $this->obj->with($relationsAsArr)->find($id);

        $hash_fields = $this->obj->getHashFields();

        if (!empty($hash_fields)) {
            if (!empty($hash_fields) && count($hash_fields) > 0) {
                foreach ($hash_fields as $field) {
                    //$obj->$field = json_decode($obj->$field, true);
                }
            }
        }

        $json_fields = $this->obj->getJsonFields();

        if (!empty($json_fields) && count($json_fields) > 0) {
            foreach ($json_fields as $field) {
                $obj->$field = json_decode($obj->$field, true);
            }
        }

        if (empty($obj)) {
            return $this->failResponse('Record with id "' . $id . '" not found', 404);
        }

        return $this->successResponse($obj->toArray());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        if (empty($id)) {
            return $this->failResponse('Bad request', 400);
        }

        $obj = $this->obj->find($id);

        if (empty($obj)) {
            return $this->failResponse('Invalid id "' . $id . '". Unable to update the record', 404);
        }

        $data = $this->getRequestBody();

        if (empty($data)) {
            return $this->failResponse('Bad request', 400, ['Empty data sent']);
        }
        \Log::info('DATA' . json_encode($data));
        $obj->fill($data);

        $json_fields = $this->obj->getJsonFields();

        if (!empty($json_fields) && count($json_fields) > 0) {
            foreach ($json_fields as $field) {
                if (isset($data[$field])) {
                    $obj->$field = json_encode($data[$field]);
                }

            }
        }
        try
        {
            \DB::beginTransaction();

            if (!$obj->save()) {
                $errors = $obj->getErrors();
                return $this->failResponse('Cannot save data.', 417, $errors);
            }

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            return \App\Services\ErrorHandlerService::getResponse($e, $this);
        }

        return $this->successResponse($obj->toArray());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if (empty($id)) {
            return $this->failResponse('Bad request', 400);
        }

        $obj = $this->obj->find($id);

        if (empty($obj)) {
            return $this->failResponse('Record not found', 404);
        }

        try {

            \DB::beginTransaction();

            $obj->delete();

            \DB::commit();
        } catch (\Exception $e) {

            \DB::rollback();
            return \App\Services\ErrorHandlerService::getResponse($e, $this);
        }

        return $this->successResponse($obj->toArray());
    }

    public function massDelete()
    {
        $sendData = array();
        $data = $this->getRequestBody();

        if (empty($data)) {
            return $this->failResponse('Bad request', 400);
        }

        if (is_array($data) && count($data) > 0) {

            foreach ($data as $id) {
                $obj = $this->obj->find($id);

                if (empty($obj)) {
                    return $this->failResponse('Record not found', 404);
                }

                try {

                    \DB::beginTransaction();

                    $obj->delete();

                    \DB::commit();

                } catch (\Exception $e) {

                    \DB::rollback();
                    return \App\Services\ErrorHandlerService::getResponse($e, $this);
                }
            }
        }

        return $this->successResponse($obj->toArray());
    }

}
