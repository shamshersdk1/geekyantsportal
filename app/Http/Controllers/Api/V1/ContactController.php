<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\V1\ResourceController;
use App\Models\Admin\Contact;
use Auth;
use Input;

class ContactController extends ResourceController
{

    protected $model = "App\Models\Admin\Contact";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($query = null)
    {

        if (!$query) {
            $query = $this->obj->buildQuery();
        }
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        $company_id = \Input::has('company_id') ? \Input::get('company_id') : 0;
        $query = $query->where('company_id', $company_id);
        $query = $query->orderBy('id', 'DESC');

        $sendData = $query->get()->toArray();
        return $this->successResponse($sendData);

    }
    public function store()
    {
        $authUser = Auth::user();
        if (!$authUser) {
            return $this->failResponse('Invalid Access');
        }

        $input = $this->getRequestBody();
        $contactObj = new Contact();
        $contactObj->first_name = $input['first_name'];
        $contactObj->last_name = $input['last_name'];
        $contactObj->type = $input['type'];
        $contactObj->email = $input['email'];
        $contactObj->mobile = !empty($input['mobile']) ? $input['mobile'] : null;
        $contactObj->company_id = $input['company_id'];
        if (!$contactObj->save()) {
            return $this->failResponse('Unable to save contact');
        }
        return $this->successResponse($contactObj);

    }
    public function destroy($id)
    {
        $userObj = Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if (Contact::find($id)->delete()) {
            return $this->successResponse('Deleted successfully');
        } else {
            return $this->failResponse("Couldn't delete the record");
        }
    }
    public function updateContact()
    {

        $input = $this->getRequestBody();
        $contactObj = Contact::find($input['contact_id']);
        $contactObj->first_name = $input['first_name'];
        $contactObj->last_name = $input['last_name'];
        $contactObj->type = $input['type'];
        $contactObj->email = $input['email'];
        $contactObj->mobile = !empty($input['mobile']) ? $input['mobile'] : null;
        if (!$contactObj->save()) {
            return $this->failResponse('Unable to save contact');
        }
        return $this->successResponse($contactObj);
    }
}
