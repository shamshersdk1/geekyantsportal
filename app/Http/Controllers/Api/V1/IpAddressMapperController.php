<?php
namespace App\Http\Controllers\Api\V1;
use App\Models\Notify;

class IpAddressMapperController extends ResourceController
{
    protected $model = "\App\Models\Admin\IpAddressMapper";

    public function store()
    {
        $authUser = \Auth::user();
        if (!$authUser) {
            return $this->failResponse('Invalid Access');
        }
        if (!($authUser->hasRole('admin')) && !($authUser->hasRole('office-admin'))) {
            return $this->failResponse('Unauthorized Access');
        }
        $input = $this->getRequestBody();
        if (empty($input['reference_id']) || empty($input['reference_type']) || empty($input['user_ip_address_id'])) {
            return $this->failResponse("Empty Data sent");
        }
        $referenceType = !empty($input['reference_type']) ? $input['reference_type'] :null; 
        $referenceId = !empty($input['reference_id']) ? $input['reference_id'] :null;
        $userIpAddressId = !empty($input['user_ip_address_id']) ? $input['user_ip_address_id'] :null;

        $exist = $this->model::where('user_ip_address_id', $userIpAddressId)
                            ->first();
        
        if($exist)
            return $this->failResponse("IP already assigned");

        $obj = new $this->model;
        // if ($input['reference_type'] == 'asset') {
        //     $obj->reference_type = 'App\Models\Admin\AssetMeta';
        // } else {
        //     $obj->reference_type = 'App\Models\Admin\MyNetworkDevice';
        // }
        $obj->reference_type = $referenceType; 
        $obj->reference_id = $referenceId;
        $obj->user_ip_address_id = $userIpAddressId;

        if (!$obj->save()) {
            return $this->failResponse("Unable to assign ip");
        }
        
        $payload = ['dhcp_changed' => 1];
        Notify::saveData('dhcp_update', $payload);

        $sendData = $this->model::with('userIp')->find($obj->id);
        return $this->successResponse($sendData);
    }
}
