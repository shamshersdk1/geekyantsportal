<?php 

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\V1\ResourceController;
use App\Models\Admin\CredentialSharing;
use App\Models\Admin\Credential;
use Validator;
use Config;
use Exception;
use DB;
use Auth;

class CredentialApiController extends ResourceController {
    protected $model = "\App\Models\Admin\Credential";
    
    public function getSharedUsers(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'credential_id' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->failResponse($validator->errors());
        }
        $sharedUsers = CredentialSharing::with('user','credential')->where('credential_id',$request->credential_id)->get();
        $result_array = [];
        if (!empty($sharedUsers) )
        {
            foreach( $sharedUsers as $sharedUser)
            {
                $result_array[] = $sharedUser->user;
            }
            // $result_array = json_encode($result_array);  
            return  $this->successResponse($result_array);
        }
        else{
            return $this->failResponse(['success' => false,'message' => 'Shared User Details not found']);
        }
	}
    public function saveSharedUsers(Request $request)
    {
        if ( !empty($request->selected_user) && !empty($request->credential_id))
        {
            $cred_share = new CredentialSharing();
            $cred_share->user_id = $request->selected_user['id'];
            $cred_share->credential_id = $request->credential_id;
            if(!$cred_share->save())
                return $this->failResponse(['success' => false,'message' => 'Unable to save']);
            return $this->successResponse('Shared User saved');
        }
    }

    public function deleteSharedUser(Request $request)
    {
        if ( !empty($request->deSelected_user) && !empty($request->credential_id))
        {
            $cred_share = CredentialSharing::where('user_id',$request->deSelected_user['id'])->where('credential_id',$request->credential_id)->first();
            if(!$cred_share->delete())
                return $this->failResponse(['success' => false,'message' => 'Unable to delete']);
            return $this->successResponse('Shared User deleted');
        }
    }

    public function deleteCredential($id)
    {
        $credential = Credential::find($id);
        if(!$credential)
            return $this->failResponse("Unable to delete");
        if(!$credential->delete())
        {
            return $this->failResponse("Unable to delete");
        }
        return $this->successResponse(['status' => true,'message' => 'Record Deleted']);
    }
}
