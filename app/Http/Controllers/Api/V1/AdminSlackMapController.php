<?php

namespace App\Http\Controllers\Api\V1;


use App\Http\Controllers\Api\V1\ResourceController;
use App\Models\Admin\SlackUser;
use App\Models\User;
use Illuminate\Http\Request;

class AdminSlackMapController extends ResourceController
{
    protected $model = "\App\Models\Admin\SlackUser";
    public function update($id)
    {
        $user = User::find($id);
        if (!$user) {
            return $this->failResponse('User not found', 404);
        }
        $request = $this->getRequestBody();
        if (empty($request['slack_id'])) {
            $user->slackUsers()->detach();
            return $this->successResponse('Changes saved');
        }
        $slack_user = SlackUser::with('users')->where('id', $request['slack_id'])->first();
        if (empty($slack_user)) {
            return $this->failResponse('Slack id not found', 404);
        }
        if (!empty($slack_user->user) && $slack_user->user->id != $id) {
            return $this->failResponse('Slack user already assigned to a resource', 400);
        } else {
            $user->slackUsers()->detach();
            $user->slackUsers()->attach($request['slack_id']);
            return $this->successResponse('Changes saved');
        }
    }
}
