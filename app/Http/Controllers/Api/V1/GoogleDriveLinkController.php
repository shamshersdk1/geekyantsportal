<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Admin\Designation;
use Illuminate\Http\Request;
use App\Models\Admin\GoogleDriveLink;
use Validator;

class GoogleDriveLinkController extends ResourceController
{
    protected $model = "\App\Models\Admin\GoogleDriveLink";

    public function index($query = null)
    {
        if (!$query) {
            $query = $this->obj->buildQuery();
        }
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if ( !(\Input::has('reference_id')) && !(\Input::has('reference_type')) )
        {
            return $this->failResponse("Missing Arguments");
        }
        $pagination = \Input::has('pagination') ? \Input::get('pagination') : 0;
        $query = $query->where('reference_id', \Input::get('reference_id'))->where('reference_type', \Input::get('reference_type'));
        $query = $query->orderBy('id','DESC');
        if ($pagination == 0) {
            $sendData = $query->get()->toArray();
            return $this->successResponse($sendData);

        } else {
            $perPage = \Input::has('per_page') ? \Input::get('per_page') : $this->per_page;
            $total = $query->count();
            $paginator = $query->paginate($perPage);
            $result = $paginator->toArray();
            $perPage = $result['per_page'];
            $currentPage = $result['current_page'];
            $metaData = $this->formatMetaData($total, $perPage, $currentPage);
            $sendData = $result['data'];
            return $this->successResponse($sendData, $metaData);
        }
    }

    public function store()
    {
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }

        $data = $this->getRequestBody();
        
        if(empty($data))
            return $this->failResponse('Empty data');
        $reference_type = $data['reference_type'];
        $reference_id = $data['reference_id'];
        $name = $data['name'];
        $path = $data['path'];
        $response = $this->model::saveData($reference_id, $reference_type, $name, $path);
        if(!$response['status'])
            return $this->failResponse('Unable to save the data');

        return $this->successResponse("Successfully saved");
    }
    
    public function update($id)
    {
        if (empty($id)) {
            return $this->failResponse('Bad request', 400);
        }
        $obj = $this->obj->find($id);
        if (empty($obj)) {
            return $this->failResponse('Invalid id "' . $id . '". Unable to update the record', 404);
        }
        $data = $this->getRequestBody();
        
        $reference_type = $data['link']['reference_type'];
        $reference_id = $data['link']['reference_id'];
        $name = $data['link']['name'];
        $path = $data['link']['path'];

        if (empty($data)) {
            return $this->failResponse('Bad request', 400, ['Empty data sent']);
        }
        
        $response = $this->model::updateData($id, $reference_id, $reference_type, $name, $path);
        
        if(!$response['status'])
            return $this->failResponse('Unable to update the data');

        return $this->successResponse("Successfully saved");
    }

    public function destroy($id)
    {
        if (empty($id)) {
            return $this->failResponse('Bad request', 400);
        }
        $obj = $this->obj->find($id);
        if (empty($obj)) {
            return $this->failResponse('Invalid id "' . $id . '". Unable to delete the record', 404);
        }
        if ( !$obj->delete() )
            return $this->successResponse("Unable to delete the record");

        return $this->successResponse("Deleted successfully");

    }
}
