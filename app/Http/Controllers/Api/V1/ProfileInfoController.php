<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Profile;
use App\Services\FileService;
use Illuminate\Http\Request;

class ProfileInfoController extends ResourceController
{
    protected $model = "\App\Models\Profile";

    public static function imageStore($id, Request $request)
    {
        $profile_id = Profile::select('id')->where('user_id', $id)->first();
        $file = $request->file;
        $fileSize = $file->getClientSize();
        if ($fileSize > 20000000) {
            return Redirect::back()->withInput()->withErrors(['Filesize exceeds 20MB']);
        }
        $type = $file->extension();

        $fileName = FileService::getFileName($type);

        $originalName = $file->getClientOriginalName();

        $uploadedFile = FileService::uploadFileInStorage($file, $fileName, $profile_id->id, "App\Models\Profile", $originalName, "ProfilePicture");
        //return json_encode($uploadedFile);
    }

    public function show($id)
    {

        $obj = $this->model::where('user_id', $id)->with('profile')->first();

        // if (empty($objs[0]->profile)) {

        // }
        // $objs[0]->profile[0]->path = $_SERVER['HTTP_HOST'] . $objs[0]->profile[0]->path;
        return $this->successResponse($obj);
    }

    public function update($id)
    {
        $input = $this->getRequestBody();
        $profileObj = $this->model::where('user_id', $input['id'])->first();
        $profileObj->title = $input['title'];
        $profileObj->location = $input['location'];
        $profileObj->page_title = $input['page_title'];
        $profileObj->page_path = $input['page_path'];
        if (!$profileObj->save()) {
            return $this->failResponse("Something went wrong");
        }
        $obj = $this->model::select('title', 'location', 'page_title', 'page_path')->where('user_id', $input['id'])->with('user')->get();
        return $this->successResponse($obj);
    }
}
