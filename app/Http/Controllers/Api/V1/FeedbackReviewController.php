<?php 

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
// use App\Http\Controllers\Api\V1\BaseApiController;
use App\Http\Controllers\Api\V1\ResourceController;
use App\Services\FeedbackService;
use App\Models\Admin\FeedbackUser;
use App\Models\Comment;
use Validator;
use Auth;


class FeedbackReviewController extends ResourceController {
    protected $model = "\App\Models\Admin\FeedbackUser";

    public function sendNotificationSingle(Request $request)
    {
        $user = Auth::user();
        if ( !$user )
        {
            return $this->failResponse("Invalid Access");
        }
        if ( !$request->feedback_user_id )
        {
            return $this->failResponse("Please provide feedback user id");
        }
        FeedbackService::sendSingleNotification($request->feedback_user_id);
        return $this->successResponse('Notification sent');
    }

    public function sendNotificationAll(Request $request)
    {
        $user = Auth::user();
        if ( !$user )
        {
            return $this->failResponse("Invalid Access");
        }
        if ( !$request->feedback_user_id )
        {
            return $this->failResponse("Please provide feedback user id");
        }
        FeedbackService::sendNotificationAll($request->feedback_user_id);
        return $this->successResponse('Notification sent');
    }

    public function updateRating(Request $request)
    {
        $user = Auth::User();
        if ( !$user )
        {
            return $this->failResponse("Invalid Access");
        }
        $input = $request->all();
        
        if ( !$input['rating'] && !$input['id'] )
        {
            return $this->failResponse("Invalid input parameters");
        }
        $feedbackUserObj = FeedbackUser::find($input['id']);
        if ( $feedbackUserObj->reviewer_id != $user->id || $feedbackUserObj->feedbackMonth->locked )
        {
            return $this->failResponse("Invalid access");
        }
        $feedbackUserObj->rating = $input['rating'];
        $feedbackUserObj->status = 'completed';
        if ( !$feedbackUserObj->save() )
        {
          return $this->failResponse($feedbackUserObj->getErrors());
        }
        return $this->successResponse($feedbackUserObj);
    }

    public function updateNotAppicable(Request $request)
    {
        $user = Auth::User();
        if ( !$user )
        {
            return $this->failResponse("Invalid Access");
        }
        $input = $request->all();
        
        if ( !$input['id'] )
        {
            return $this->failResponse("Invalid input parameters");
        }
        $feedbackUserObj = FeedbackUser::find($input['id']);
        if ( $feedbackUserObj->reviewer_id != $user->id || $feedbackUserObj->feedbackMonth->locked )
        {
            return $this->failResponse("Invalid access");
        }
        $feedbackUserObj->status = 'not_applicable';

        $feedbackUserObj->rating = null;
        if ( !$feedbackUserObj->save() ) {
          return $this->failResponse($feedbackUserObj->getErrors());
        }

        $commentable_type = 'App\Models\Admin\FeedbackUser\Restricted';
        Comment::where('commentable_id',$input['id'])->where('commentable_type',$commentable_type)->delete();

        return $this->successResponse($feedbackUserObj);
    }

    public function regenerateRecordsUser ( Request $request )
    {
        $user = Auth::User();
        if ( !$user || !$user->hasRole('Admin')  )
        {
            return $this->failResponse("Invalid Access");
        }
        $input = $request->all();
        FeedbackService::generateRecordsForUser($input['user_id'], $input['month_id']);
        return $this->successResponse('Successfully regenerated');
    }

    public function regenerateRecordsAll( Request $request )
    {
        $user = Auth::User();
        if ( !$user || !$user->hasRole('Admin')  )
        {
            return $this->failResponse("Invalid Access");
        }
        $input = $request->all();
        FeedbackService::generateRecords($input['month'], $input['year']);
        return $this->successResponse('Successfully regenerated');
    }
}
