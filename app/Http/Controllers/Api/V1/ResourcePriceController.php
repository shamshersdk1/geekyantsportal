<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Admin\Designation;
use Illuminate\Http\Request;
use App\Models\Admin\ResourcePrice;
use Validator;

class ResourcePriceController extends ResourceController
{
    protected $model = "\App\Models\Admin\ResourcePrice";

    public function index($query = null)
    {
        if (!$query) {
            $query = $this->obj->buildQuery();
        }
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        $project_id = \Input::has('project_id') ? \Input::get('project_id') : 0;
        $pagination = \Input::has('pagination') ? \Input::get('pagination') : 1;
        $query = $query->where('project_id', $project_id);
        $query = $query->orderBy('id','DESC');
        if ($pagination == 0) {

            $sendData = $query->get()->toArray();
            return $this->successResponse($sendData);

        } else {

            $perPage = \Input::has('per_page') ? \Input::get('per_page') : $this->per_page;

            $total = $query->count();

            $paginator = $query->paginate($perPage);

            $result = $paginator->toArray();

            $perPage = $result['per_page'];
            $currentPage = $result['current_page'];

            $metaData = $this->formatMetaData($total, $perPage, $currentPage);
            $sendData = $result['data'];

            return $this->successResponse($sendData, $metaData);

        }
    }

    public function store()
    {
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }

        $input = $this->getRequestBody();
        
        $resourcePriceObj = new ResourcePrice();
        $resourcePriceObj->project_id = $input['project_id'];
        $resourcePriceObj->type = $input['type'];
        $resourcePriceObj->price = $input['price'];
        if (!$resourcePriceObj->save()) {
            return $this->failResponse("Unable to save");
        }
        return $this->successResponse('Successfully saved');    
    }
    
    public function update($id)
    {
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        $input = $this->getRequestBody();
        
        $resourcePriceObj = ResourcePrice::find($id);
        $resourcePriceObj->type = $input['resourceRow']['type'];
        $resourcePriceObj->price = $input['resourceRow']['price'];
        if (!$resourcePriceObj->save()) {
            return $this->failResponse("Unable to update");
        }
        return $this->successResponse('Successfully saved');
    }

    public function destroy($id)
    {
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if( ResourcePrice::find($id)->delete() ) 
        {
            return $this->successResponse('Deleted successfully');
        }
        else {
            return $this->failResponse("Couldn't delete the record");
        }
    }
}
