<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\V1\ResourceController;
use App\Models\Admin\IpAddressMapper;
use Auth;

class UserIpAddressController extends ResourceController
{

    protected $model = "App\Models\Admin\UserIpAddress";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($query = null)
    {
        if (!$query) {
            $query = $this->obj->buildQuery();
        }
        $userObj = \Auth::User();

        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if (!($userObj->hasRole('admin')) && !($userObj->hasRole('system-admin'))) {
            return $this->failResponse('Unauthorized Access');
        }
        $input = \Input::all();
        if (!empty($input['user_id'])) {
            $query = $query->where('user_id', $input['user_id']);
        }
        return parent::index($query);
    }
    public function freeUserIp($id)
    {
        $data = [];
        $UserIpAddress = $this->model::where('user_id', $id)->get();
        foreach ($UserIpAddress as $userIp) {

            $isUsedInMyDevice = IpAddressMapper::where('user_ip_address_id', $userIp->id)->with('userIp')->first();
            if (!$isUsedInMyDevice) {
                array_push($data, $userIp);
            }
        }
        return $this->successResponse($data);
    }

}
