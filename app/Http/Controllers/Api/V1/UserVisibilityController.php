<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Admin\UserProfileVisibility;

class UserVisibilityController extends ResourceController
{
    protected $model = "\App\Models\Admin\UserProfileVisibility";

    public function show($id)
    {
        $visibilityObj = UserProfileVisibility::getUserVisibility($id);
        return $this->successResponse($visibilityObj);
    }

    public function update($id)
    {
        $input = $this->getRequestBody();
        $visibilityObj = UserProfileVisibility::where('user_id', $id)->first();
        switch ($input['title']) {
            case "training":
                $visibilityObj->training = $input['status'];
                break;
            case "basic":
                $visibilityObj->basic = $input['status'];
                break;
            default:
                return $this->failResponse("unable to save");
        }
        if (!$visibilityObj->save()) {
            return $this->failResponse("unable to save");
        }
        return $this->successResponse($visibilityObj);
    }

}
