<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Admin\Question;
use Illuminate\Http\Request;
use Validator;

class FeedbackQuestionController extends ResourceController
{
    protected $model = "\App\Models\Admin\Question";

    public function updateStatus(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->failResponse($validator->errors());
        }
        $id = $input['id'];
        $questionObj = Question::updateStatus($id);
        
        if( $questionObj['status'] ) {
            return $this->successResponse($questionObj);
        } else {
            return $this->failResponse($questionObj['message']);
        }
    }
}
