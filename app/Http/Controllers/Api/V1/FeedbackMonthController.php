<?php 

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
// use App\Http\Controllers\Api\V1\BaseApiController;
use App\Http\Controllers\Api\V1\ResourceController;
use App\Models\Admin\FeedbackMonth;
use App\Services\FeedbackService;
use Validator;
use Auth;

class FeedbackMonthController extends ResourceController {
    
    protected $model = "\App\Models\Admin\FeedbackMonth";

    public function index($query = null)
    {
        $user = Auth::user();
        if ( !$user )
        {
            return $this->failResponse("Invalid access");
        }
        $res = FeedbackService::getMonthList();
        return $this->successResponse($res, '');
    }

    public function lockMonth(Request $request)
    {
        $user = Auth::user();
        if ( !$user )
        {
            return $this->failResponse("Invalid access");
        }
        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->failResponse($validator->errors());
        }
        $feedbackMonthObj = FeedbackMonth::find($input['id']);
        if (!$feedbackMonthObj) {
            return $this->failResponse('No record found');
        }
        $feedbackMonthObj->locked = true;
        if ( $feedbackMonthObj->save() )
            return $this->successResponse('Saved Successfully');
        else
        return $this->failResponse($feedbackMonthObj->getErrors());

    }

    public function toggleLock(Request $request)
    {
        $user = Auth::user();
        if ( !$user )
        {
            return $this->failResponse("Invalid access");
        }
        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->failResponse($validator->errors());
        }
        $id = $input['id'];
        $feedbackMonthObj = FeedbackMonth::find($id);
        $toggle = FeedbackMonth::where('id', $id)->update(['locked' => !$feedbackMonthObj->locked]);
        return $this->successResponse('Saved Successfully');
    }

    public function updateVisibility(Request $request)
    {
        $user = Auth::user();
        if ( !$user )
        {
            return $this->failResponse("Invalid access");
        }
        if ( $user->role != 'admin' )
        {
            return $this->failResponse("Invalid access");
        }
        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->failResponse($validator->errors());
        }
        $id = $input['id'];
        $feedbackMonthObj = FeedbackMonth::find($id);
        $toggle = FeedbackMonth::where('id', $id)->update(['is_visible' => !$feedbackMonthObj->is_visible]);
        return $this->successResponse('Saved Successfully');
    }
}
