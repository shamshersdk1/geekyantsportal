<?php 

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
// use App\Http\Controllers\Api\V1\BaseApiController;
use App\Http\Controllers\Api\V1\ResourceController;
use App\Models\Timesheet;
use App\Models\TimesheetDetail;
use App\Models\Admin\ProjectResource;
use Validator;
use Auth;
use Input;

class TimesheetApiController extends ResourceController {
    protected $model = "\App\Models\Timesheet";
    
    public function index($query = null) {
        if (!$query) {
            $query = $this->obj->buildQuery();
        }
        
        if (!Input::has('month_id') || Input::has('user_id') ) {
            $monthId = Input::get('month_id');
        }
        
        $monthId = Input::get('month_id');
        $userId = Input::get('user_id');

        

    }
	public function store()
    {
        $input = $this->getRequestBody();
        $response = [];
        $status = true;
        $timesheetId = null;
        if ( !$input['project_id'] || !$input['date'] ) 
        {
            return $this->failResponse("Invalid inputs");
        }
        $currentDate = date('Y-m-d');
        if ($input['date'] > $currentDate ) {
            return $this->failResponse("Entry for future dates is not allowed");
        }

		$user = Auth::user();
		$user_id = $user->id;
        $hours = 0;
        
        $selected_date = $input['date'];
		$project_id = $input['project_id'];
		$timesheet_details = $input['timesheet_details'];

        $timesheet = Timesheet::where('user_id', $user_id)
						->where('project_id', $project_id)
                        ->where('date','=', $selected_date)
						->first();

        if ( !empty($timesheet) ) 
        {
            $hours = $timesheet->sumOfHours();
            if (!empty($timesheet->approver_id))
            {
                return $this->failResponse("The timesheet is already approved for this day");
            }            
        }
            
        if ( !$timesheet )
        {
            $project = ProjectResource::where('user_id',$user_id)
                                ->where('project_id',$project_id)
                                ->first();
            if ( !empty($project) ) 
            {
                $timesheet_new = Timesheet::createTimesheet($project, $selected_date, $hours);
                if ( !empty($timesheet_new) ) 
                {
                    $timesheetId = $timesheet_new->id;
                    $timesheetDetailResponse = TimesheetDetail::saveTimesheetDetails($timesheet_new->id, $timesheet_details);
                    if ( !$timesheetDetailResponse['status'] )
                    {
                        return $this->failResponse($timesheetDetailResponse['message']);
                    }
                }
            }
        } 
		else {
            $hours = $timesheet->sumOfHours();
			$timesheet->hours = $hours;
            $timesheet->approved_hours = $hours;	
			if ( !$timesheet->save() )
            {
                return $this->failResponse('Not able to save timesheet');
            }
            // TimesheetDetail::saveTimesheetDetails($timesheet->id, $timesheet_details);
            else
            {
                $timesheetId = $timesheet->id;
                $timesheetDetailResponse = TimesheetDetail::saveTimesheetDetails($timesheet->id, $timesheet_details);
                if ( !$timesheetDetailResponse['status'] )
                {
                    return $this->failResponse($timesheetDetailResponse['message']);
                }
            }
		}             
        $response['status'] = $status;
        $response['message'] = 'Successfully saved'; 
        return $this->successResponse($response);
	}
    public function getUserTimesheet() {

    }

}
