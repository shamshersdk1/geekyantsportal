<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Admin\Designation;
use Illuminate\Http\Request;
use Validator;

class DesignationController extends ResourceController
{
    protected $model = "\App\Models\Admin\Designation";

    public function updateStatus(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->failResponse($validator->errors());
        }
        $id = $input['id'];
        $designationObj = Designation::updateStatus($id);
        
        if( $designationObj['status'] ) {
            return $this->successResponse($designationObj);
        } else {
            return $this->failResponse($designationObj['message']);
        }
    }
    public function index($query = null)
    {
        if (!$query) {
            $query = $this->obj->buildQuery();
        }

        $userObj = \Auth::User();
        if (!$userObj->checkRole('admin') && !$userObj->checkRole('human-resources') && !$userObj->checkRole('reporting-manager')) {
            return $this->failResponse("Invalid Access");
        }

        if (\Input::has('q')) {
            $query = $query->genericSearch(\Input::get('q'));
        }

        if (\Input::has('order_by_field') && \Input::has('order_by_type')) {
            $query = $query->orderBy(\Input::get('order_by_field'), \Input::get('order_by_type'));
        }

        $relationsAsStr = \Input::has('relations') ? \Input::get('relations') : '';
        $relationsAsArr = array_map('trim', array_filter(explode(',', $relationsAsStr)));

        $query->with($relationsAsArr);

        $pagination = \Input::has('pagination') ? \Input::get('pagination') : 1;

        if ($pagination == 0) {

            $sendData = $query->get()->toArray();

            // foreach ($sendData as &$data) {
            //     $data['last_24_hrs'] = SubscriberRegisteredReport::getCount($data['id']);
            // }

            return $this->successResponse($sendData);

        } else {

            $perPage = \Input::has('per_page') ? \Input::get('per_page') : $this->per_page;

            $total = $query->count();

            $paginator = $query->paginate($perPage);

            $result = $paginator->toArray();

            $perPage = $result['per_page'];
            $currentPage = $result['current_page'];

            $metaData = $this->formatMetaData($total, $perPage, $currentPage);
            $sendData = $result['data'];

            return $this->successResponse($sendData, $metaData);

        }
    }
    
}
