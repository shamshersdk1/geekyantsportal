<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\V1\ResourceController;
use App\Models\Admin\IpAddressRange;
use App\Models\Admin\UserIpAddress;
use App\Models\Notify;
use Auth;
use Illuminate\Http\Request;

class IpAddressRangeController extends ResourceController
{

    protected $model = "App\Models\Admin\IpAddressRange";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($query = null)
    {

        if (!$query) {
            $query = $this->obj->buildQuery();
        }
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if (!($userObj->hasRole('admin')) && !($userObj->hasRole('system-admin'))) {
            return $this->failResponse('Unauthorized Access');
        }
        return parent::index($query);

    }
    public function assignIp(Request $request)
    {
        $userObj = Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if (!($userObj->hasRole('admin')) && !($userObj->hasRole('system-admin'))) {
            return $this->failResponse('Unauthorized Access');
        }

        $input = $this->getRequestBody();

        if ($input) {
            $range = IpAddressRange::find($input['range']);

            $start_ip = ip2long($range['start_ip']);
            $end_ip = ip2long($range['end_ip']);

            while ($start_ip <= $end_ip) {
                $ip = long2ip($start_ip);
                $isUsed = UserIpAddress::where('ip_address', $ip)->first();
                if (!$isUsed) {
                    $freeIp = $ip;
                    break;
                }
                $start_ip++;
            }
            if (!empty($freeIp)) {
                $assignIp = new UserIpAddress;
                $assignIp->user_id = !empty($input['user_id']) ? $input['user_id'] : null;
                $assignIp->ip_address = $freeIp;
                $assignIp->ip_address_long = ip2long($freeIp);
                $assignIp->assigned_by = $userObj->id;
                if (!$assignIp->save()) {
                    return $this->failResponse('Unable to assign ip');
                } else {
                    $payload = ['dhcp_changed' => 1];
                    Notify::saveData('dhcp_update', $payload);
                    return $this->successResponse($assignIp);
                }
            } else {
                return $this->failResponse('No ip available in the given range');
            }
        }
    }
    public function updateCompanyAssetStatus(Request $request)
    {
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if (!($userObj->hasRole('admin')) && !($userObj->hasRole('system-admin'))) {
            return $this->failResponse('Unauthorized Access');
        }
        $id = $request->id;
        if (empty($id)) {
            return $this->failResponse('Range id is missing');
        }
        $range = IpAddressRange::find($id);
        if (!$range) {
            return $this->failResponse('Range Not found');
        }
        $range->company_asset = !$range->company_asset;
        if (!$range->save()) {
            return $this->failResponse('Unable to update  Company Assets Status');
        }
        return $this->successResponse(['success' => true, 'message' => 'Successfully Updated Company Assets Status']);
    }

}
