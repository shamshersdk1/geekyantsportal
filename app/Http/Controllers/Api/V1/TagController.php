<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
// use App\Http\Controllers\Api\BaseApiController;
use App\Http\Controllers\Api\V1\ResourceController;

use App\Models\Admin\Goal;
use App\Models\Admin\Tag;


use Auth;
use Redirect;
use View;
use DB;

class TagController extends ResourceController
{
    protected $model = "\App\Models\Admin\Tag";


    public function index($query = null)
    {
        $userObj = Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if (!$query) 
        {
            $query = $this->obj->buildQuery();
        }
        $relationsAsStr = \Input::has('relations') ? \Input::get('relations') : '';
        $relationsAsArr = array_map('trim', array_filter(explode(',', $relationsAsStr)));
        
        $query->with($relationsAsArr);
        $pagination = \Input::has('pagination') ? \Input::get('pagination') : 0;

        if ($pagination == 0) {

            $sendData = $query->pluck('name')->toArray();
            return $this->successResponse($sendData);

        } else {

            $perPage = \Input::has('per_page') ? \Input::get('per_page') : $this->per_page;

            $total = $query->count();

            $paginator = $query->paginate($perPage);

            $result = $paginator->toArray();

            $perPage = $result['per_page'];
            $currentPage = $result['current_page'];

            $metaData = $this->formatMetaData($total, $perPage, $currentPage);
            $sendData = $result['data'];

            return $this->successResponse($sendData, $metaData);
        }
        return $this->failResponse("Something went wrong!");
    }
}