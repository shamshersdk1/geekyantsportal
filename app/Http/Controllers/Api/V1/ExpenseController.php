<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Models\Admin\Expense;
use App\Models\Admin\ExpenseHead;
use App\Services\ExpenseService;
use Validator;

class ExpenseController extends ResourceController
{
    protected $model = "\App\Models\Admin\Expense";

    public function index($query = null)
    {
        if (!$query) {
            $query = $this->obj->buildQuery();
        }
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if ( $userObj && !( $userObj->hasRole('admin') || $userObj->hasRole('office-admin')) )
        {
            return $this->failResponse("Invalid Access");
        }
        // if ( \Input::has('filter') )
        // {
        //     $query = $query->where('name','like','%'.\Input::get('filter').'%')->orWhere('type','like','%'.\Input::get('filter').'%')
        //                    ->orWhere('address','like','%'.\Input::get('filter').'%')->orWhere('pan','like','%'.\Input::get('filter').'%')
        //                    ->orWhere('gst','like','%'.\Input::get('filter').'%');
        // }
        $query = $query->with('expenseHead', 'vendor','expensor','creator','approver','purchaseOrder','paymentMethod');
        $pagination = \Input::has('pagination') ? \Input::get('pagination') : 1;
        $query = $query->orderBy('id','DESC');
        if ($pagination == 0) {

            $sendData = $query->get()->toArray();
            return $this->successResponse($sendData);

        } else {

            $perPage = \Input::has('per_page') ? \Input::get('per_page') : $this->per_page;

            $total = $query->count();

            $paginator = $query->paginate($perPage);

            $result = $paginator->toArray();

            $perPage = $result['per_page'];
            $currentPage = $result['current_page'];

            $metaData = $this->formatMetaData($total, $perPage, $currentPage);
            $sendData = $result['data'];

            return $this->successResponse($sendData, $metaData);

        }
    }

    public function store()
    {
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if ( $userObj && !( $userObj->hasRole('admin') || $userObj->hasRole('office-admin')) )
        {
            return $this->failResponse("Invalid Access");
        }
        $input = $this->getRequestBody();
        $res = ExpenseService::saveData($input['data']);
        if ( $res['status'] )
        {
            return $this->successResponse('Successfully saved');
        }
        else {
            return $this->failResponse("Unable to save");
        }

    }

    public function show($id)
    {
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if ( $userObj && !( $userObj->hasRole('admin') || $userObj->hasRole('office-admin')) )
        {
            return $this->failResponse("Invalid Access");
        }
        $obj = Expense::with('expenseHead', 'purchaseOrder.vendor','expensor','creator','approver','paymentMethod','payer')->find($id);
        if (!$obj) {
            return $this->failResponse("Invalid #" . $id);
        }
        return $this->successResponse($obj);
    }
    
    public function update($id)
    {
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if ( $userObj && !( $userObj->hasRole('admin') || $userObj->hasRole('office-admin') || $userObj->hasRole('account') ) )
        {
            return $this->failResponse("Invalid Access");
        }
        $input = $this->getRequestBody();
        $res = ExpenseService::updateData($id, $input['data']);
        if ( $res['status'] )
        {
            return $this->successResponse('Successfully saved');
        }
        else {
            return $this->failResponse("Unable to save");
        }
    }

    public function destroy($id)
    {
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if ( $userObj && !( $userObj->hasRole('admin') || $userObj->hasRole('office-admin') || $userObj->hasRole('account') ) )
        {
            return $this->failResponse("Invalid Access");
        }
        if( Expense::find($id)->delete() ) 
        {
            return $this->successResponse('Deleted successfully');
        }
        else {
            return $this->failResponse("Couldn't delete the record");
        }
    }

    public function getExpenseHeads()
    {
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if ( $userObj && !( $userObj->hasRole('admin') || $userObj->hasRole('office-admin')) )
        {
            return $this->failResponse("Invalid Access");
        }
        $expenseHeads = ExpenseHead::all();
        return $this->successResponse($expenseHeads);
    }

    public function updateStatus(Request $request)
    {
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if ( $userObj && !( $userObj->hasRole('admin') || $userObj->hasRole('account')) )
        {
            return $this->failResponse("Invalid Access");
        }
        if ( !empty($request->id) && !empty($request->status) )
        {
            $result = ExpenseService::updateStatus($request->id, $request->status);
            if ( $result['status'] )
            {
                return $this->successResponse($result['message']);
            }
            else {
                return $this->failResponse($result['message']);   
            }
        }

    }
}
