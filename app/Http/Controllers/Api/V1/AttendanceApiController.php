<?php 

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\V1\BaseApiController;
use Validator;
use App\Models\User;
use App\Models\Attendance;
use App\Models\UserAttendance;
use App\Models\Admin\Calendar;
use Auth;

class AttendanceApiController extends BaseApiController {

    public function store(Request $request)
    {
        if ( empty($request->token) )
        {
            return $this->failResponse(['success' => false,'message' => 'Token is missing']);
        }
        elseif ( $request->token == env('ATTENDANCE_TOKEN') )
        { 
            $date = date("Y-m-d");
        $is_holiday = false;
        
        if(date('N', strtotime($date)) > 5 || Calendar::whereDate('date', $date)->count() > 0) 
        {
            $is_holiday = true;
		}    
        
        if( $is_holiday )
        {
            return $this->successResponse(['success' => true,'message' => 'No entries made for non working days']);
        }
        else
        {
            $email_list = $request->data;
            $active_users = User::where('is_active', 1)->get();
            $total_users = count($active_users);
            $attendance = Attendance::where('date',$date)->first();
            if ( !empty($attendance) )
            {
                $attendance->date = $date;
                $attendance->total_users = $total_users;
                if(!$attendance->save())
                            return $this->failResponse(['success' => false,'message' => 'Something went wrong!']);   
            }
            else{
                $attendance = new Attendance();
                $attendance->date = $date;
                $attendance->total_users = $total_users;
                if(!$attendance->save())
                            return $this->failResponse(['success' => false,'message' => 'Something went wrong!']);
            }
            foreach( $active_users as $user )
            {
                $is_present = true;
                foreach( $email_list as $email )
                {
                    if( $user->email == $email )
                    {
                        $is_present = false;
                    }
                }
                $user_attendance = UserAttendance::where('attendance_id',$attendance->id)->where('user_id',$user->id)->first();
                if ( !empty($user_attendance) )
                {
                    $user_attendance->attendance_id = $attendance->id;
                    $user_attendance->user_id = $user->id;
                    $user_attendance->is_present = $is_present;
                    if(!$user_attendance->save())
                    return $this->failResponse(['success' => false,'message' => 'Something went wrong!']); 
                }
                else{
                    $user_attendance = new UserAttendance();
                    $user_attendance->attendance_id = $attendance->id;
                    $user_attendance->user_id = $user->id;
                    $user_attendance->is_present = $is_present;
                    if(!$user_attendance->save())
                    return $this->failResponse(['success' => false,'message' => 'Something went wrong!']); 
                }
                   
            }
            return $this->successResponse(['success' => true,'message' => 'Successfull']);
        } 
        }
        else              
            return $this->failResponse(['success' => false,'message' => 'Token mismatch!']);
	}
    


}
