<?php 

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\V1\ResourceController;
use App\Services\SeatAllocationSlackService;
use App\Models\FloorsSeat;
use App\Models\FloorsSeatUser;
use Validator;
use Auth;

class SeatsApiController extends ResourceController {
    protected $model = "\App\Models\FloorsSeat";

    public function store()
    {
        
        $input = $this->getRequestBody();
        $response = [];
        $alreadyExists = FloorsSeatUser::where('user_id',$input['user_id'])->first();
        if($alreadyExists)
        {
            return $this->failResponse($alreadyExists->assigned_user->name." is already assigned a seat on ".$alreadyExists->floors_seat->floor->name." ".$alreadyExists->floors_seat->floor->floor_number." floor (Seat No :".$alreadyExists->floors_seat->name.")");
        }
        $seat = FloorsSeat::where('id',$input['seat_id'])->first();
        $seatCheck = FloorsSeat::where('floor_id',$seat->floor_id)->where('name',$input['seat_name'])->first();
        if($seat->name != $seatCheck->name && $seatCheck)
        {
            return $this->failResponse("Seat No #".$seatCheck->name." already assigned in this floor.");
        }
        if(!$seat['assignable'])
        {
            return $this->failResponse('This seat is unassignable.');
        }
        FloorsSeatUser::freeAllocatedUser($input['user_id']);
        
        if( empty($input['seat_id']) )
        {
            return $this->failResponse('Seat Id not found');
        }

        $floorsSeat = FloorsSeatUser::where('floor_seat_id', $input['seat_id'])->first();

        if ( isset($floorsSeat) ){
            $floorsSeat->user_id = !empty($input['user_id']) ? $input['user_id'] : '';
            $floorsSeat->floor_seat_id = $input['seat_id'];
            $seat->name = !empty($input['seat_name']) ? $input['seat_name'] : '';
            $floorsSeat->save();
            $seat->save();
            $flag = 'Add';
            // if ( !empty($request->user_id) ){
            //     SeatAllocationSlackService::sendMessage($seat, $flag, $request->user_id);
            // }
            $response['status'] = true;
            $response['message'] = 'Successfull saved';
            return $this->successResponse($response);
        }
        else {
            $floorSeatNew = new FloorsSeatUser();
            $floorSeatNew->user_id = !empty($input['user_id']) ? $input['user_id'] : '';
            $floorSeatNew->floor_seat_id = $input['seat_id'];
            $seat->name = !empty($input['seat_name']) ? $input['seat_name'] : '';
            $floorSeatNew->save();
            $seat->save();
            // $flag = 'Add';
            // if ( !empty($request->user_id) ){
            //     SeatAllocationSlackService::sendMessage($seat, $flag, $request->user_id);
            // }
            $response['status'] = true;
            $response['message'] = 'Successfull saved';
            return $this->successResponse($response);
        }
		                  
        return $this->failResponse("Something went wrong");
	}
    // public function release(Request $request)
    // {
        
    //     $validator = Validator::make($request->all(), [
    //         'seat_id' => 'required'
    //     ]);

    //     if ($validator->fails()) {
    //         return $this->failResponse($validator->errors());
    //     }

    //     $floorsSeat = FloorsSeatUser::where('floor_seat_id', $request->seat_id)->first();
    //     $seat = FloorsSeat::where('id',$request->seat_id)->first();


    //     if ( isset($floorsSeat) ){
    //         $previous_user = $floorsSeat->user_id;
    //         $floorsSeat->delete();
    //         // $flag = 'Remove';
    //         // SeatAllocationSlackService::sendMessage($seat, $flag, $previous_user);
    //         return $this->successResponse(['success' => true,'message' => 'Successfull']);
    //     }
		                  
    //     return $this->failResponse(['success' => false,'message' => 'Something went wrong!']);
	// }
    public function destroy($id)
    {
        $floorsSeat = FloorsSeatUser::where('floor_seat_id', $id)->first();
        $seat = FloorsSeat::where('id',$id)->first();


        if ( isset($floorsSeat) ){
            $previous_user = $floorsSeat->user_id;
            $floorsSeat->delete();
            $response['status'] = true;
            $response['message'] = 'Successfull saved';
            return $this->successResponse($response);
        }
		                  
        return $this->failResponse("Unable to release the seat");

    }


}
