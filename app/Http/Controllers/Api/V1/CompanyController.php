<?php 

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\V1\ResourceController;
use App\Models\Admin\Company;
use App\Models\User;
use Auth;
use Hash;
use Validator;
use App\Services\CompanyService;
use Config;


class CompanyController extends ResourceController {

    protected $model = "App\Models\Company";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($query = null)
    {
        
        $userObj = Auth::user();
        if(empty($userObj) )
        {
            return $this->failResponse('Unauthorized');     
        }
        if (!$userObj->hasRole('admin') && !$userObj->hasRole('office-admin')) {
            return $this->failResponse('Unauthorized Access');
        }
        if (!$query) {
            $query = $this->obj->buildQuery();
        }
        if (\Input::has('q')) {
            $query = $query->genericSearch(\Input::get('q'));
        }
        if (\Input::has('order_by_field') && \Input::has('order_by_type')) {
            $query = $query->orderBy(\Input::get('order_by_field'), \Input::get('order_by_type'));
        }
        $relationsAsStr = \Input::has('relations') ? \Input::get('relations') : '';
        $relationsAsArr = array_map('trim', array_filter(explode(',', $relationsAsStr)));
        $query->with($relationsAsArr);
        $pagination = 0;
        if ($pagination == 0) {
            $sendData = $query->get()->toArray();
            return $this->successResponse($sendData);
        } else {
            $perPage = \Input::has('per_page') ? \Input::get('per_page') : $this->per_page;
            $total = $query->count();
            $paginator = $query->paginate($perPage);
            $result = $paginator->toArray();
            $perPage = $result['per_page'];
            $currentPage = $result['current_page'];
            $metaData = $this->formatMetaData($total, $perPage, $currentPage);
            $sendData = $result['data'];
            return $this->successResponse($sendData, $metaData);
        }
    }
}

