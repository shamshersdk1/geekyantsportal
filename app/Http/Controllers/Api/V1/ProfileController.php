<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Profile;
use App\Models\User;

class ProfileController extends ResourceController
{
    protected $model = "\App\Models\Profile";

    public function updateSocialInfo()
    {

        $input = $this->getRequestBody();

        if (empty($input)) {
            return $this->failResponse('Empty data');
        }

        if (!isset($input['data']) || !isset($input['id'])) {
            return $this->failResponse("Invalid data");
        }
        $userId = $input['id'];
        $userObj = Profile::where('user_id', $userId)->get();

        if (!$userObj) {
            return $this->failResponse("Invalid user id");
        }

        $userObj = \Auth::User();

        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if (!$userObj->hasRole('admin')) {
            return $this->failResponse('Unauthorized Access');
        }

        $res = Profile::socialUpdate($input['data'], $input['id']);
        if (!$res['status']) {
            return $this->failResponse($res['message']);
        }
        return $this->successResponse($res);
    }

    public function show($id)
    {
        $query = Profile::where('user_id', $id)->get();

        if (empty($query->toArray())) {
            return $this->failResponse("No record found");
        }
        return $this->successResponse($query);
    }
}
