<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\V1\ResourceController;
use App\Models\Admin\AdditionalWorkDaysBonus;
use App\Models\User;
use Auth;

class DepartmentController extends ResourceController
{

    protected $model = "App\Models\Admin\Department";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($query = null)
    {

        if (!$query) {
            $query = $this->obj->buildQuery();
        }
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }

        $query = $query->orderBy('id', 'ASC');

        return parent::index($query);

    }
    public function store()
    {
        $authUser = Auth::user();
        if (!$authUser) {
            return $this->failResponse('Invalid Access');
        }
        if (!($authUser->hasRole('admin')) && !($authUser->hasRole('account')) && !($authUser->hasRole('reporting-manager'))) {
            return $this->failResponse('Unauthorized Access');
        }
        $input = $this->getRequestBody();
        $AdditionalWorkDaysBonus = new AdditionalWorkDaysBonus();
        $AdditionalWorkDaysBonus->employee_id = $input['user_id'];
        $AdditionalWorkDaysBonus->type = $input['type'];
        $AdditionalWorkDaysBonus->reference_id = $input['project_id'];
        $AdditionalWorkDaysBonus->reference_type = 'App\Models\Admin\Project';
        $AdditionalWorkDaysBonus->amount = $input['amount'];
        $AdditionalWorkDaysBonus->status = 'approved';
        $AdditionalWorkDaysBonus->to_date = !empty(date_to_yyyymmdd($input['to_date'])) ? date_to_yyyymmdd($input['to_date']) : null;
        $AdditionalWorkDaysBonus->from_date = !empty(date_to_yyyymmdd($input['from_date'])) ? date_to_yyyymmdd($input['from_date']) : null;
        $AdditionalWorkDaysBonus->notes = !empty($input['notes']) ? $input['notes'] : null;
        $AdditionalWorkDaysBonus->created_by = $authUser->id;
        if (!$AdditionalWorkDaysBonus->save()) {

            return $this->failResponse('Unable to save contact');
        }
        $AdditionalWorkDaysBonusObj = AdditionalWorkDaysBonus::where('id', $AdditionalWorkDaysBonus->id)->with('user', 'reviewer')->first();
        return $this->successResponse($AdditionalWorkDaysBonusObj);
    }
    public function destroy($id)
    {
        $userObj = Auth::User();

        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if (!$userObj->hasRole('admin') && !$userObj->hasRole('account') && !$userObj->hasRole('reporting-manager')) {
            return $this->failResponse('Unauthorized Access');

        }
        if (AdditionalWorkDaysBonus::find($id)->delete()) {
            return $this->successResponse('Deleted successfully');
        } else {
            return $this->failResponse("Couldn't delete the record");
        }
    }
    public function updateBonus($id)
    {
        $userObj = Auth::User();

        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if (!$userObj->hasRole('admin') && !$userObj->hasRole('account') && !$userObj->hasRole('reporting-manager')) {
            return $this->failResponse('Unauthorized Access');

        }
        $input = $this->getRequestBody();
        $AdditionalWorkDaysBonus = AdditionalWorkDaysBonus::find($input['bonus_id']);
        $AdditionalWorkDaysBonus->employee_id = $input['user_id'];
        $AdditionalWorkDaysBonus->type = $input['type'];
        $AdditionalWorkDaysBonus->reference_id = $input['project_id'];
        $AdditionalWorkDaysBonus->amount = $input['amount'];
        $AdditionalWorkDaysBonus->to_date = !empty(date_to_yyyymmdd($input['to_date'])) ? date_to_yyyymmdd($input['to_date']) : null;
        $AdditionalWorkDaysBonus->from_date = !empty(date_to_yyyymmdd($input['from_date'])) ? date_to_yyyymmdd($input['from_date']) : null;
        $AdditionalWorkDaysBonus->notes = !empty($input['notes']) ? $input['notes'] : null;
        $AdditionalWorkDaysBonus->created_by = $userObj->id;

        if (!$AdditionalWorkDaysBonus->save()) {
            return $this->failResponse('Unable to save contact');
        }
        $AdditionalWorkDaysBonusObj = AdditionalWorkDaysBonus::where('id', $AdditionalWorkDaysBonus->id)->with('user', 'reviewer')->first();
        return $this->successResponse($AdditionalWorkDaysBonusObj);
    }
    public function show($id)
    {
        $userObj = Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        $AdditionalWorkDaysBonusObj = AdditionalWorkDaysBonus::where('id', $id)->with('user', 'reviewer')->first();
        if ($AdditionalWorkDaysBonusObj) {
            return $this->successResponse($AdditionalWorkDaysBonusObj);
        } else {
            return $this->failResponse("No record");
        }
    }
}
