<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Admin\RequestApproval;
use App\Models\Admin\RequestApproveUser;
use Illuminate\Http\Request;
use Validator;

class RequestApprovalController extends ResourceController
{
    protected $model = "\App\Models\Admin\RequestApproval";

    public function approve(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->failResponse($validator->errors());
        }

        $id = $input['id'];
        $referenceObj = RequestApproval::approveRequest($id);
        
        if( $referenceObj['status'] ) {
            return $this->successResponse($referenceObj);
        } else {
            return $this->failResponse($referenceObj['message']);
        }
    }

    public function reject(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->failResponse($validator->errors());
        }
        $id = $input['id'];
        $referenceObj = RequestApproval::rejectRequest($id);
        
        if( $referenceObj['status'] ) {
            return $this->successResponse($referenceObj);
        } else {
            return $this->failResponse($referenceObj['message']);
        }
    }
    
}
