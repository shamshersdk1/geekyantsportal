<?php

namespace App\Http\Controllers\Api\V1;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Admin\IpMacBinding;
use Illuminate\Support\Facades\DB;
use Redirect;
use Validator;


class IpMacBindingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=$request->all();

        $validator=Validator::make($request->all(),[
            'hardware_adress'=>'required',
            'ip'=>'required'
        ]);

        if($validator->fails()){
            return $this->failResponse($validator->errors());

        }

        if( empty($data) )
        {
            return Redirect::back()->withInput()->withErrors(['Missing inputs']);
        }
            $IpMacBinding=new IpMacBinding();
            $IpMacBinding->hardware_adress=$data['hardware_adress'];
            $IpMacBinding->ip=$data['ip'];
      
        if($IpMacBinding->save()){
            return redirect('')->with('message', 'Successfully Added');
        }
        else {
            return redirect::back()->withErrors($IpmacBinding->getErrors())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
