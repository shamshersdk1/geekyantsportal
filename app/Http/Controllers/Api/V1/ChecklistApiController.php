<?php 

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\V1\BaseApiController;
use App\Models\User;
use App\Models\Admin\Project;
use App\Models\Admin\ProjectResource;
use App\Models\Admin\ProjectSlackChannel;
use Validator;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use Config;
use Exception;
use DB;
use Auth;

class ChecklistApiController extends BaseApiController {


	public function userMismatchList(Request $request)
    {
        try{
            $client = new Client(['base_uri' => config('app.slack_url')]);
            $token_value = 'Bearer '.config('app.slack_token');
            $response = $client->request('GET', "users.list", [
                'headers' => [
                    'Authorization' => $token_value
                ]
                ]);
        } catch(Exception $e) {
            echo $e->getMessage();
        }
        $createResponse =  $response->getBody();
        $response_object = json_decode($createResponse);
        
        $userList = User::all();
        $mismatch_details = [];
        $mismatch_data = [];
        foreach ( $response_object->members as $member )
        {
            if ( !$member->deleted )
            {
                $count = 0;
                $mismatch_data = [];
                $email = !empty($member->profile->email) ? $member->profile->email : '';
                foreach ( $userList as $user )
                {
                    if ( $user->email == $email )
                        $count++;
                }
                if ( $count == 0 )
                {
                    $mismatch_data['first_name'] = !empty($member->profile->first_name) ? $member->profile->first_name : $member->name ;
                    $mismatch_data['last_name'] = !empty($member->profile->last_name) ? $member->profile->last_name : '' ;
                    $mismatch_data['email'] = $email;
                    $mismatch_details[] = $mismatch_data;
                }
            }
                        
        }
        return $mismatch_details;
        // return $this->successResponse(['success' => true,'message' => 'Successfull','mismatch_details' => $mismatch_details]);
	}

    public function projectWithoutResources(Request $request)
    {
        $in_progress_projects = Project::where('status','in_progress')->get();
        $in_progress_input_array = [];
        $assigned_projects_array = [];
        $free_projects_array = [];
        foreach ( $in_progress_projects as $project )
        {
            $in_progress_input_array[] = $project->id;
        }
        
        $assigned_projects = ProjectResource::select('project_id')->distinct()
                        ->whereIn('project_id',$in_progress_input_array)->get();

        foreach ( $assigned_projects as $projects )
        {
            $assigned_projects_array[] = $projects->project_id;
        }
        $free_projects_array = array_diff($in_progress_input_array, $assigned_projects_array);
        $free_projects = Project::whereIn('id', $free_projects_array)->get()->toArray();
        return $free_projects;
        // return $this->successResponse(['success' => true,'message' => 'Successfull','free_projects' => $free_projects]);
    }

    public function channelsWithoutProjects(Request $request)
    {
        try{
            $client = new Client(['base_uri' => config('app.slack_url')]);
            $response = $client->request("POST", "channels.list", [
                'form_params' => [
                    'token' => config('app.slack_token')
                ]
                ]);
        } catch(Exception $e) {
            echo $e->getMessage();
        }
        $channelCreateResponse =  $response->getBody();
        $publicChannels = json_decode($channelCreateResponse);
        $associatedChannels = ProjectSlackChannel::where('type','public')->get();
        $unassigned_channels = [];
        $unassigned_channel = [];
        foreach ( $publicChannels->channels as $channel )
        {
            if ( $channel->is_archived != 1 )
            {
                $count = 0;
                $unassigned_channel = [];
                $id = !empty($channel->id) ? $channel->id : '';
                foreach ( $associatedChannels as $associatedChannel )
                {
                    if ( $associatedChannel->slack_id == $id )
                        $count++;
                }
                if ( $count == 0 )
                {
                    $unassigned_channel['id'] = !empty($channel->id) ? $channel->id : '' ;
                    $unassigned_channel['name'] = !empty($channel->name) ? $channel->name : '' ;
                    $unassigned_channel['name_normalized'] = !empty($channel->name_normalized) ? $channel->name_normalized : '' ;
                    $unassigned_channels[] = $unassigned_channel;
                }
            }
                        
        }
        
        return $unassigned_channels;
        // return $this->successResponse(['success' => true,'message' => 'Successfull','unassigned_channels' => $unassigned_channels]);
        
    }
}
