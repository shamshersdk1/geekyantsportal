<?php

namespace App\Http\Controllers\Api\V1\User;

use App\Http\Controllers\Api\V1\ResourceController;
use App\Models\Admin\Miniproject;
use Auth;

class MiniprojectController extends ResourceController
{
    protected $model = "\App\Models\Admin\Miniproject";

    public function store()
    {
        $authUser = Auth::user();
        if (!$authUser) {
            return $this->failResponse('Invalid Access');
        }
        if (!($authUser->hasRole('admin')) && !($authUser->hasRole('reporting-manager'))) {
            return $this->failResponse('Unauthorized Access');
        }
        $input = $this->getRequestBody();
        $res = Miniproject::saveData($input['data']);
        if ($res['status']) {
            return $this->failResponse($res['message']);
        }
        return $this->successResponse($res);
    }

    public function show($id)
    {
        $authUser = Auth::user();
        if (!$authUser) {
            return $this->failResponse('Invalid Access');
        }
        if (!($authUser->hasRole('admin')) && !($authUser->hasRole('reporting-manager'))) {
            return $this->failResponse('Unauthorized Access');
        }

        $data = Miniproject::where('user_id', $id)->get();
        // if (empty($query->toArray())) {
        //     return $this->failResponse("No record found");
        // }
        return $this->successResponse($data);
    }

    public function destroy($id)
    {
        $authUser = Auth::user();
        if (!$authUser) {
            return $this->failResponse('Invalid Access');
        }
        if (!($authUser->hasRole('admin')) && !($authUser->hasRole('reporting-manager'))) {
            return $this->failResponse('Unauthorized Access');
        }
        $miniprojectObj = Miniproject::find($id);
        if (!$miniprojectObj->delete()) {
            return $this->failResponse("unable to delete");
        }
        return $this->successResponse('Successfully Deleted');
    }

}
