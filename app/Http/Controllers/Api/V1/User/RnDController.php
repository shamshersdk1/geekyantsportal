<?php

namespace App\Http\Controllers\Api\V1\User;

use App\Http\Controllers\Api\V1\ResourceController;
use App\Models\Admin\RnD;
use Auth;

class RnDController extends ResourceController
{
    protected $model = "\App\Models\Admin\RnD";

    public function store()
    {
        $authUser = Auth::user();
        if (!$authUser) {
            return $this->failResponse('Invalid Access');
        }
        if (!($authUser->hasRole('admin')) && !($authUser->hasRole('reporting-manager'))) {
            return $this->failResponse('Unauthorized Access');
        }
        $input = $this->getRequestBody();
        $res = RnD::saveData($input['data']);
        if ($res['status']) {
            return $this->failResponse($res['message']);
        }
        return $this->successResponse($res);
    }

    public function show($id)
    {
        $authUser = Auth::user();
        if (!$authUser) {
            return $this->failResponse('Invalid Access');
        }
        if (!($authUser->hasRole('admin')) && !($authUser->hasRole('reporting-manager'))) {
            return $this->failResponse('Unauthorized Access');
        }
        $data = RnD::where('user_id', $id)->get();
        return $this->successResponse($data);
    }

    public function destroy($id)
    {
        $authUser = Auth::user();
        if (!$authUser) {
            return $this->failResponse('Invalid Access');
        }
        if (!($authUser->hasRole('admin')) && !($authUser->hasRole('reporting-manager'))) {
            return $this->failResponse('Unauthorized Access');
        }

        $RnDObj = RnD::find($id);
        if (!$RnDObj->delete()) {
            return $this->failResponse("unable to delete");
        }
        return $this->successResponse('Successfully Deleted');
    }

}
