<?php

namespace App\Http\Controllers\Api\V1\User;
use App\Http\Controllers\Api\V1\ResourceController;
use App\Models\UserTechnologies;
use App\Models\Admin\Technology;
use Auth;



class TechStackController extends ResourceController{
    protected $model = "\App\Models\Admin\TechStack";

    public function index($query = null)
    {
       
    }

    public function store()
    {
        
    }

    public function show($id)
    {
        $authUser = Auth::user();
        if (!$authUser) {
            return $this->failResponse('Invalid Access');
        }
       
        $tech_id=UserTechnologies::where('user_id',$id)->pluck('technology_id');

    //     $response=[];
    //   foreach($tech_id as $id){
    //     //   echo"$tech_id";
    //     //   die;
        if(!$tech_id){
            return $this->failResponse('Technology Not Found');
        }

        $response=UserTechnologies::with('technology')->where('user_id',$id)->get();
      
      return $this->successResponse($response);

    }

    public function update($id)
    {
      $input=$this->getRequestBody();
    
    if(!$input){
       return $this->failResponse("No User Found") ;
    }
 
    $response = $this->model::saveData($input,$id);
    if(!$response)
        return $this->failResponse('Unable to save the data');

    return $this->successResponse("Successfully saved",$response);

   
    }

    public function destroy($id)
    {
        
    }

   
}
