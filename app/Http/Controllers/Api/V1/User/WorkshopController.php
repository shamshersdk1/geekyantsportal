<?php

namespace App\Http\Controllers\Api\V1\User;

use App\Http\Controllers\Api\V1\ResourceController;
use App\Models\Admin\Workshop;
use Auth;

class WorkshopController extends ResourceController
{
    protected $model = "\App\Models\Admin\Workshop";

    public function store()
    {
        $authUser = Auth::user();
        if (!$authUser) {
            return $this->failResponse('Invalid Access');
        }
        if (!($authUser->hasRole('admin')) && !($authUser->hasRole('reporting-manager'))) {
            return $this->failResponse('Unauthorized Access');
        }
        $input = $this->getRequestBody();
        $res = Workshop::saveData($input['data']);
        if ($res['status']) {
            return $this->failResponse($res['message']);
        }
        return $this->successResponse($res);
    }

    public function show($id)
    {
        $authUser = Auth::user();
        if (!$authUser) {
            return $this->failResponse('Invalid Access');
        }
        if (!($authUser->hasRole('admin')) && !($authUser->hasRole('reporting-manager'))) {
            return $this->failResponse('Unauthorized Access');
        }

        $data = Workshop::where('user_id', $id)->get();
        return $this->successResponse($data);
    }

    public function destroy($id)
    {
        $authUser = Auth::user();
        if (!$authUser) {
            return $this->failResponse('Invalid Access');
        }
        if (!($authUser->hasRole('admin')) && !($authUser->hasRole('reporting-manager'))) {
            return $this->failResponse('Unauthorized Access');
        }
        $workshopObj = Workshop::find($id);
        if (!$workshopObj->delete()) {
            return $this->failResponse("unable to delete");
        }
        return $this->successResponse('Successfully Deleted');
    }
}
