<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\V1\ResourceController;
use App\Models\Comment;
use Auth;
use Illuminate\Http\Request;
use Input;

class CommentController extends ResourceController
{
    protected $model = "\App\Models\Comment";
    public function index($query = null)
    {

        if (!$query) {
            $query = $this->obj->buildQuery();
        }
        $authUser = \Auth::User();
        if (!$authUser) {
            return $this->failResponse("Invalid Access");
        }

        if (!$authUser->hasRole('admin')) {
            $query = $query->where('is_private', false);
        }

        if (!Input::has('commentable_id') || !Input::has('commentable_type')) {
            return $this->failResponse("commentable fields missing");
        }

        $commentableId = \Input::has('commentable_id') ? \Input::get('commentable_id') : null;
        $commentableType = \Input::has('commentable_type') ? \Input::get('commentable_type') : null;

        $query = $query->where('commentable_id', $commentableId);
        $query = $query->where('commentable_type', $commentableType);
        $query = $query->with('user');
        $query = $query->orderBy('id', 'DESC');
        return parent::index($query);
    }
    public function getComments(Request $request)
    {
        $authUser = Auth::user();
        $private_flag = false;
        if ($authUser->hasRole('admin')) {
            $private_flag = true;
        }
        $per_page = !empty($request->per_page) ? $request->per_page : 5;
        $comments = Comment::getComments($request->commentable_id, $request->commentable_type, $request->page, $per_page, $private_flag);
        $result['data'] = $comments;
        return $this->successResponse($result);
    }

    public function store()
    {

        $input = $this->getRequestBody();
        $message = '';
        if (!$input['commentable_id'] || !$input['commentable_type']) {
            return $this->failResponse("Invalid Inputs");
        }
        if (!$input['message'] && $input['commentable_type'] == 'App\Models\Admin\FeedbackUser\Restricted') {
            $result = Comment::where('commentable_id', $input['commentable_id'])->where('commentable_type', $input['commentable_type'])->delete();
            return $this->successResponse($result);
        } else {
            $result = Comment::saveComment($input['commentable_id'], $input['commentable_type'], $input['message'], $input['is_private']);
            if ($result['status']) {
                return $this->successResponse($result);
            } else {
                return $this->failResponse($result['message']);
            }
        }

    }
}
