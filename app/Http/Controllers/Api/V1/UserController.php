<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Admin\UserSetting;
use App\Models\User;
use Illuminate\Http\Request;
use Validator;

class UserController extends ResourceController
{
    protected $model = "\App\Models\User";
    public function updateBillable(Request $request)
    {

        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->failResponse($validator->errors());
        }
        $id = $input['id'];
        $user = User::find($id);
        $toggle = User::where('id', $id)->update(['is_billable' => !$user->is_billable]);
        return $this->successResponse(['success' => true, 'message' => 'Successfully Updated']);
    }

    public function index($query = null)
    {
        if (!$query) {
            $query = $this->obj->buildQuery();
        }

        $userObj = \Auth::User();
        if (!$userObj->checkRole('admin') && !$userObj->checkRole('human-resources') && !$userObj->checkRole('reporting-manager')) {
            return $this->failResponse("Invalid Access");
        }

        if (\Input::has('q')) {
            $query = $query->genericSearch(\Input::get('q'));
        }

        if (\Input::has('order_by_field') && \Input::has('order_by_type')) {
            $query = $query->orderBy(\Input::get('order_by_field'), \Input::get('order_by_type'));
        }
        if (\Input::has('is_active')) {
            $query = $query->where('is_active', \Input::get('is_active'));
        }

        $relationsAsStr = \Input::has('relations') ? \Input::get('relations') : '';
        $relationsAsArr = array_map('trim', array_filter(explode(',', $relationsAsStr)));

        $query->with($relationsAsArr);

        $pagination = \Input::has('pagination') ? \Input::get('pagination') : 1;

        if ($pagination == 0) {

            $sendData = $query->get()->toArray();

            // foreach ($sendData as &$data) {
            //     $data['last_24_hrs'] = SubscriberRegisteredReport::getCount($data['id']);
            // }

            return $this->successResponse($sendData);

        } else {

            $perPage = \Input::has('per_page') ? \Input::get('per_page') : $this->per_page;

            $total = $query->count();

            $paginator = $query->paginate($perPage);

            $result = $paginator->toArray();

            $perPage = $result['per_page'];
            $currentPage = $result['current_page'];

            $metaData = $this->formatMetaData($total, $perPage, $currentPage);
            $sendData = $result['data'];

            return $this->successResponse($sendData, $metaData);

        }
    }

    public function getMyUsers($query = null)
    {
        if (!$query) {
            $query = $this->obj->buildQuery();
        }

        $userObj = \Auth::User();
        if (!$userObj->checkRole('admin') && !$userObj->checkRole('reporting-manager')) {
            return $this->failResponse("Invalid Access");
        }
        $sendData = $query->where('parent_id', $userObj->id)->where('is_active', 1)->get()->toArray();
        return $this->successResponse($sendData);
    }

    public function updateFeedbackReview(Request $request)
    {

        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->failResponse($validator->errors());
        }
        $id = $input['id'];
        $user = User::find($id);
        $toggle = User::where('id', $id)->update(['is_feedback_review' => !$user->is_feedback_review]);
        return $this->successResponse(['success' => true, 'message' => 'Successfully Updated']);
    }

    public function getAllActiveUsers($query = null)
    {
        if (!$query) {
            $query = $this->obj->buildQuery();
        }

        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }

        $query->where('is_active', 1);
        $sendData = $query->get()->toArray();
        return $this->successResponse($sendData);
    }

    public function updateRMNotificationReminder(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->failResponse($validator->errors());
        }
        $id = $input['id'];
        $user = User::find($id);

        if (!$user) {
            return $this->failResponse('User not found!');
        }

        $settingObj = $user->RMNotificationReminder;

        if (!$settingObj) {
            $settingObj = new UserSetting();
            $response = $settingObj->setValue($id, 'rm-notification-reminder', 0);

        } else {
            if ($settingObj->value == 1) {
                $x = 0;
            } else {
                $x = 1;
            }
            $response = $settingObj->setValue($id, 'rm-notification-reminder', $x);
        }
        if ($response['status'] == false) {
            return $this->failResponse('User Setting not updated!');
        }

        return $this->successResponse(['success' => true, 'message' => 'Successfully Updated']);
    }

}
