<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Api\V1\BaseApiController;

use App\Models\Admin\PayslipCsvData;
use App\Models\Admin\PayslipCsvMonth;
use App\Models\LoanEmi;
use App\Models\Loan;

use Auth;
use Redirect;
use View;
use DB;

class LoanController extends BaseApiController
{
    public function index(Request $request)
    {
        $id = $request->id;
        $csvMonth = PayslipCsvMonth::find($id);
        if($csvMonth) {
            $month = $csvMonth->month;
            if($month < 10) {
                $month = '0'.$month;
            }
            $loans = Loan::getLoans($id);
            $result = [];
            $result['data'] = $loans;
            $csvMonth->status = "processing";
            $csvMonth->save();
            return $this->successResponse($result);
        } else {
            return $this->failResponse('Invalid link followed');
        }
    }
    public function show()
    {

    }
    public function update(Request $request, $id)
    {
        $status = empty($request->status) ? null : $request->status;
        $user_id = empty($request->user_id) ? null : $request->user_id;
        $amount = empty($request->amount) ? null : $request->amount;
        if(is_null($status) || is_null($user_id) || is_null($amount)) {
            return $this->failResponse('Incomplete request');
        }
        if($status == "approve") {
            $dataObj = PayslipCsvData::where('user_id', $user_id)->where('payslip_csv_month_id', $id)->first();
            if($dataObj) {
                $dataObj->loan = $amount;
                if(!$dataObj->save()) {
                    return $this->failResponse($dataObj->getErrors());
                }
                return $this->successResponse("Saved successfully");    
            }
            return $this->failResponse("Invalid request");
            // $response = LoanEmi::approveLoanEmi($request, $id);
            // if($response['status']) {
            //     return $this->successResponse($response['message']);
            // } else {
            // }
        } elseif($status == "reject") {
            $dataObj = PayslipCsvData::where('user_id', $user_id)->where('payslip_csv_month_id', $id)->first();
            if($dataObj) {
                $dataObj->loan = 0.00;
                if(!$dataObj->save()) {
                    return $this->failResponse($dataObj->getErrors());
                }
                return $this->successResponse("Saved successfully");    
            }
            return $this->failResponse("Invalid request");
            // $response = LoanEmi::rejectLoanEmi($request, $id);
            // if($response['status']) {
            //     return $this->successResponse($response['message']);
            // } else {
            //     return $this->failResponse($response['message']);
            // }
        } else {
            return $this->failResponse('Invalid request');
        }
    }
    public function deduction() {
        
    }
}
