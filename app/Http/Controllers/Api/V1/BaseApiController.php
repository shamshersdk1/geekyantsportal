<?php namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;

class BaseApiController extends Controller
{
    protected function getRequestBody()
    {

        $request = \Request::instance();
        $content = $request->getContent();
        
        $data = json_decode($content, true);
        unset($data['jsData']);

        return $data;

    }

    public function successResponse($data, $meta = array(), $code = 200)
    {
        $returnData = array(
            'result' => $data,
            'meta' => $meta,
        );

        return \Response::json($returnData, $code);

    }

    public function failResponse($message, $code = 400, $errors = null, $data = null, $stackTrace = null)
    {

        $responseArray = [
            'errors' => $errors,
            'message' => $message,
            'result' => $data,
        ];

        // if (!empty(\App\Services\BaseApiService::$meta)) {

        //     $extraInfo = \App\Services\BaseApiService::$meta;
        //     $responseArray['extra'] = $extraInfo;

        // }

        if (!empty($stackTrace)) {

            $responseArray['stackTrace'] = $stackTrace;

        }

        return \Response::json(

            $responseArray,
            $code
        );
    }

    protected function preparePaginationData($total, $perPage, $currentPage, $items)
    {
        return array(
            'meta' => array(
                'total' => $total,
                'per_page' => $perPage,
                'current_page' => $currentPage,
                'page_param_name' => 'page',
            ),
            'result' => $items,
        );

    }

    protected function formatMetaData($total = 0, $perPage = 0, $currentPage = 0)
    {
        $meta = array();

        $meta = array(
            'total' => $total,
            'per_page' => $perPage,
            'current_page' => $currentPage,
            'page_param_name' => 'page',
        );

        return $meta;
    }
    // protected function successResponse($data, $meta = array(), $code = 200)
    // {
    //     $returnData = array(
    //         'result' => $data,
    //         'metaData' => $meta,
    //         'success' => true,
    //     );
    //     return response()->json($returnData, 200);
    // }

    // protected function failResponse($message, $code = 400, $errors = null)
    // {

    //     $response = array(
    //         'errors' => $errors,
    //         'message' => $message,
    //         'success' => false,
    //     );
    //     return response()->json($response, $code);
    // }

}
