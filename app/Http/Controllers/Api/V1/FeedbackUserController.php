<?php 

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
// use App\Http\Controllers\Api\V1\BaseApiController;
use App\Http\Controllers\Api\V1\ResourceController;
use App\Services\FeedbackService;
use App\Models\Admin\FeedbackUser;
use Validator;
use Auth;

class FeedbackUserController extends ResourceController {
    protected $model = "\App\Models\Admin\FeedbackUser";

    public function getUserList()
    {
        $user = Auth::user();
        $feedbackMonthId = 0;
        if (\Input::has('feedback_month_id')) {
            $feedbackMonthId = \Input::get('feedback_month_id');
        }
        if ( $user && $feedbackMonthId != 0 )
        {
            $res = FeedbackService::getUserList($user->id, $feedbackMonthId);
            
            return $this->successResponse($res, '');
        }
        else {
            return $this->failResponse("Input parameters missing");
        }    
    }

    public function getProgressStatus()
    {
        $reviewer = Auth::user();
        $feedbackMonthId = 0;
        if (\Input::has('feedback_month_id') && \Input::has('user_id') ) {
            $feedbackMonthId = \Input::get('feedback_month_id');
            $userId = \Input::get('user_id');
        }
        $pendingCount = FeedbackUser::where('reviewer_id',$reviewer->id)->where('feedback_month_id',$feedbackMonthId)->where('user_id',$userId)
                      ->where('status','pending')->get()->count();

        $totalCount = FeedbackUser::where('reviewer_id',$reviewer->id)->where('feedback_month_id',$feedbackMonthId)->where('user_id',$userId)
                          ->get()->count();
                          
        $percentage_completed = ($totalCount - $pendingCount) / $totalCount * 100;
        return $this->successResponse($percentage_completed);
    }

    public function getMonthlyUserReviews()
    {
        $month_id = \Input::get('month_id');
        $user = Auth::user();
        $result = FeedbackService::getUserMonthlyReviews($user->id, $month_id);

        return $this->successResponse($result);
    }

}
