<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\V1\ResourceController;
//App/Http/Controllers/Api/V1/ResourceController.php
use App\Models\Admin\CalendarYear;
use App\Models\Admin\Leave;
use App\Models\Leave\UserLeaveTransaction;
use App\Models\User;
use App\Services\LeaveService;
use App\Services\UserService;
use Auth;
use DB;
use Illuminate\Http\Request;
use Validator;

class LeaveApiController extends ResourceController
{
    protected $model = "\App\Models\Admin\Leave";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($query = null)
    {
        $userObj = Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if (!$query) {
            $query = $this->obj->buildQuery();
        }

        if (\Input::has('q')) {
            $query = $query->genericSearch(\Input::get('q'));
        }

        if (\Input::has('order_by_field') && \Input::has('order_by_type')) {
            $query = $query->orderBy(\Input::get('order_by_field'), \Input::get('order_by_type'));
        }

        $relationsAsStr = \Input::has('relations') ? \Input::get('relations') : '';
        $relationsAsArr = array_map('trim', array_filter(explode(',', $relationsAsStr)));

        $query->with($relationsAsArr);

        $year = date('Y');
        $yearObj = CalendarYear::where('year', $year)->first();

        if (!$yearObj) {
            return $this->failResponse("Year Not Found !");
        }

        $query = $query->where('user_id', $userObj->id);
        $query = $query->where('calendar_year_id', $yearObj->id);
        $query = $query->orderBy('start_date', 'DESC');

        $pagination = \Input::has('pagination') ? \Input::get('pagination') : 1;

        if ($pagination == 0) {

            $sendData = $query->get()->toArray();

            return $this->successResponse($sendData);

        } else {

            $perPage = \Input::has('per_page') ? \Input::get('per_page') : $this->per_page;

            $total = $query->count();

            $paginator = $query->paginate($perPage);

            $result = $paginator->toArray();

            $perPage = $result['per_page'];
            $currentPage = $result['current_page'];

            $metaData = $this->formatMetaData($total, $perPage, $currentPage);
            $sendData = $result['data'];

            return $this->successResponse($sendData, $metaData);

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $userObj = Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        $leave = Leave::with('user', 'approver', 'projectManagerApprovals', 'overLapRequests')->find($id);

        if (empty($leave)) {
            return $this->failResponse("Leave not found");
        }
        // Validations
        if ($userObj->hasRole('user')) {
            if (!($userObj->hasRole('admin') || $userObj->hasRole('human-resources') || $userObj->hasRole('reporting-manager') || $userObj->hasRole('team-lead'))) {
                if ($leave->user_id != $userObj->id) {
                    return $this->failResponse("Invalid request");
                }
            }
        } else {
            return $this->failResponse("User must have the user role");
        }
        // ---

        $response = [];
        $status = false;
        $data = [];
        $leaveOverlapping = false;
        $overlappingLeaves = [];

        $overlappingLeaves = LeaveService::checkOverlappingLeave($leave->user_id, $leave->start_date, $leave->end_date, $leave->id);

        if (!empty($overlappingLeaves['projects']) && count($overlappingLeaves['projects']) > 0) {
            $leaveOverlapping = true;
        }
        $previousLeaves = Leave::findPreviousLeaves($leave->id);
        $managerStatuses = LeaveService::teamLeadStatuses($leave->id);
        $leadStatuses = LeaveService::isTeamLeadWithStatus($userObj->id, $managerStatuses);

        $status = true;
        $response['status'] = $status;
        $response['data']['leave'] = $leave;
        $response['data']['leaveOverlapping'] = $leaveOverlapping;
        $response['data']['overlappingLeaves'] = $overlappingLeaves;
        $response['data']['previousLeaves'] = $previousLeaves;
        $response['data']['managerStatuses'] = $managerStatuses;
        $response['data']['leadStatuses'] = $leadStatuses;
        return $this->successResponse($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $input = $this->getRequestBody();

        $loggedUser = Auth::user();
        if (!$loggedUser) {
            return $this->failResponse("Invalid Access");
        }
        if (!($loggedUser->hasRole('admin') || $loggedUser->hasRole('human-resources') || $loggedUser->hasRole('user'))) {
            return $this->failResponse("Invalid user role");
        }
        $response = [];

        $status = false;
        $message = '';
        $data = [];
        $leaveOverlapping = false;
        $overlappingLeaves = [];

        //$input = $request->all();
        $skipOverlappingCheck = isset($input['skip_overlapping_check']) ? $input['skip_overlapping_check'] : false;
        if (empty($input['start_date']) || empty($input['end_date'])) {
            return $this->failResponse("Invalid inputs");
        }
        $userId = !empty($input['user_id']) ? $input['user_id'] : $loggedUser->id;
        $input['user_id'] = $userId;

        if ($loggedUser->hasRole('admin') || $loggedUser->hasRole('human-resources') || $loggedUser->hasRole('reporting-manager')) {
            $input['user_id'] = !empty($input['user_id']) ? $input['user_id'] : $loggedUser->id;
        }

        $startDate = $input['start_date'];
        $endDate = $input['end_date'];
        // Check for existing leaves for the user

        $leaveAlreadyAppliedForUser = false;
        $startDateCheck = Leave::where('start_date', '>=', $startDate)->where('start_date', '<=', $endDate)
            ->where('user_id', $userId)->whereNotIn('status', ['rejected', 'cancelled'])->first();
        $endDateCheck = Leave::where('end_date', '>=', $startDate)->where('end_date', '<=', $endDate)
            ->where('user_id', $userId)->whereNotIn('status', ['rejected', 'cancelled'])->first();

        if (!empty($startDateCheck) || !empty($endDateCheck)) {
            $leaveAlreadyAppliedForUser = true;
            return $this->failResponse("You have leave already applied during the date range provided");
        }
        $role = 'user';
        if ($loggedUser->checkRole('admin') || $loggedUser->checkRole('human-resources') || $loggedUser->checkRole('reporting-manager')) {
            $role = 'admin';
        }
        //return leave overlapnig messages
        if ($skipOverlappingCheck) {
            $appliedLeave = LeaveService::saveData($input, $role);
            if ($appliedLeave['status']) {
                $status = true;
                $leave = $appliedLeave;
                $message = "Leave request save succefully";
            } else {
                return $this->failResponse($appliedLeave['errors']);
            }
        } else {
            $overlappingLeaves = LeaveService::checkOverlappingLeave($userId, $startDate, $endDate);

            if (!empty($overlappingLeaves['projects']) && count($overlappingLeaves['projects']) > 0) {
                $leaveOverlapping = true;
            } else {
                $appliedLeave = LeaveService::saveData($input);
                if ($appliedLeave['status']) {
                    $status = true;
                    $leave = $appliedLeave;
                    $message = "Leave request save succefully 12";
                } else {
                    return $this->failResponse($appliedLeave['errors']);
                }
            }
            if (!empty($input['overlap_reasons'])) {
                $appliedLeave = LeaveService::saveData($input);
                if ($appliedLeave['status']) {
                    $message = "Leave request save succefully";
                    // $leaveId = $appliedLeave['id'];
                    // LeaveService::saveOverlappingDetails($input['overlap_reasons'],$leaveId, $userId);
                    $status = true;
                    $leave = $appliedLeave;
                } else {
                    return $this->failResponse($appliedLeave['errors']);
                }
            }
        }
        $response['status'] = $status;
        $response['message'] = $message;
        $response['data']['leave'] = !empty($leave) ? $leave : null;
        $response['data']['leave_overlapping'] = $leaveOverlapping;
        $response['data']['overlapping_leaves'] = $overlappingLeaves;
        return $this->successResponse($response);
    }

    public function leaveRequest(Request $request)
    {
        $loggedUser = Auth::user();
        if (!($loggedUser->hasRole('admin') || $loggedUser->hasRole('human-resources') || $loggedUser->hasRole('user'))) {
            return $this->failResponse("Invalid user role");
        }
        $response = [];

        $status = false;
        $message = '';
        $data = [];
        $leaveOverlapping = false;
        $overlappingLeaves = [];

        $input = $request->all();
        $skipOverlappingCheck = isset($input['skip_overlapping_check']) ? $input['skip_overlapping_check'] : false;

        if (empty($input['start_date']) || empty($input['end_date']) || empty($input['user_id'])) {
            return $this->failResponse("Invalid inputs");
        }

        $userId = $input['user_id'];
        $startDate = $input['start_date'];
        $endDate = $input['end_date'];

        // Check for existing leaves for the user

        $leaveAlreadyAppliedForUser = false;
        $startDateCheck = Leave::where('start_date', '>=', $startDate)->where('start_date', '<=', $endDate)
            ->where('user_id', $userId)->whereNotIn('status', ['rejected', 'cancelled'])->first();
        $endDateCheck = Leave::where('end_date', '>=', $startDate)->where('end_date', '<=', $endDate)
            ->where('user_id', $userId)->whereNotIn('status', ['rejected', 'cancelled'])->first();
        if (!empty($startDateCheck) || !empty($endDateCheck)) {
            $leaveAlreadyAppliedForUser = true;
            return $this->failResponse("Leave already applied during the date range");
        }

        //return leave overlapnig messages
        if ($skipOverlappingCheck) {
            $appliedLeave = LeaveService::saveData($input);
            if ($appliedLeave['status']) {
                $status = true;
                $leave = $appliedLeave;
                $message = "Leave request save succefully";
            } else {
                return $this->failResponse($appliedLeave['errors']);
            }
        } else {
            $overlappingLeaves = LeaveService::checkOverlappingLeave($userId, $startDate, $endDate);

            if (!empty($overlappingLeaves['projects']) && count($overlappingLeaves['projects']) > 0) {
                $leaveOverlapping = true;
            } else {
                $appliedLeave = LeaveService::saveData($input);
                if ($appliedLeave['status']) {
                    $status = true;
                    $leave = $appliedLeave;
                    $message = "Leave request save succefully";
                } else {
                    return $this->failResponse($appliedLeave['errors']);
                }
            }
            if (!empty($input['overlap_reasons'])) {
                $appliedLeave = LeaveService::saveData($input);
                if ($appliedLeave['status']) {
                    $message = "Leave request save succefully";
                    // $leaveId = $appliedLeave['id'];
                    // LeaveService::saveOverlappingDetails($input['overlap_reasons'],$leaveId, $userId);
                    $status = true;
                    $leave = $appliedLeave;
                } else {
                    return $this->failResponse($appliedLeave['errors']);
                }
            }
        }
        $response['status'] = $status;
        $response['message'] = $message;
        $response['data']['leave'] = !empty($leave) ? $leave : null;
        $response['data']['leave_overlapping'] = $leaveOverlapping;
        $response['data']['overlapping_leaves'] = $overlappingLeaves;
        return $this->successResponse($response);
    }
    /**
     * Return available sick and paid leaves
     *
     * @return \Illuminate\Http\Response
     */
    public function getBalance(Request $request)
    {
        $user = User::where('name', $request->employee)->first();

        if (!$user) {
            return $this->failResponse(['success' => false, 'message' => 'User not Found']);
        }

        $availableSickLeaves = 0;
        if (!empty($user->joining_date)) {
            $availableSickLeaves = LeaveService::getRemainingSickLeave($user);
        }

        $availablePaidLeaves = 0;
        if (!empty($user->confirmation_date)) {
            $availablePaidLeaves = LeaveService::getRemainingPaidLeave($user);
        }

        $leaves = [];
        $leaves['sick_leaves'] = $availableSickLeaves;
        $leaves['paid_leaves'] = $availablePaidLeaves;

        return $this->successResponse($leaves);
    }
    public function getDuration(Request $request)
    {
        $user = User::where('name', $request->employee)->first();

        if (!$user) {
            return $this->failResponse(['success' => false, 'message' => 'User not Found']);
        }
        $validator = Validator::make($request->all(), [
            'start_date' => 'required|date',
            'end_date' => 'required|date',
        ]);

        if ($validator->fails()) {
            return $this->failResponse($validator->errors());
        }
        if (!LeaveService::isLeaveValid($request->start_date, $request->end_date)) {
            return $this->failResponse(['success' => false, 'message' => 'Start of Leave should be on or before End Date']);
        }
        return $this->successResponse(LeaveService::calculateWorkingDays($request->start_date, $request->end_date, $user->id));
    }

    /**
     * Check if user for email exists with geekyants domain
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getLeaveDetails($id)
    {
        $leave = Leave::with('user', 'approver', 'projectManagerApprovals', 'overLapRequests')->find($id);

        if (empty($leave)) {
            return $this->failResponse("Leave not found");
        }
        $response = [];
        $status = false;
        $data = [];
        $leaveOverlapping = false;
        $overlappingLeaves = [];

        $overlappingLeaves = LeaveService::checkOverlappingLeave($leave->user_id, $leave->start_date, $leave->end_date);

        if (!empty($overlappingLeaves['projects']) && count($overlappingLeaves['projects']) > 0) {
            $leaveOverlapping = true;
        }
        $previousLeaves = Leave::findPreviousLeaves($leave->id);
        $managerStatuses = LeaveService::teamLeadStatuses($leave->id);
        $status = true;
        $response['status'] = $status;
        $response['data']['leave'] = $leave;
        $response['data']['leaveOverlapping'] = $leaveOverlapping;
        $response['data']['overlappingLeaves'] = $overlappingLeaves;
        $response['data']['previousLeaves'] = $previousLeaves;
        $response['data']['managerStatuses'] = $managerStatuses;
        return $this->successResponse($response);
    }

    public function rejectLeave(Request $request, $id)
    {
        $isMyRM = false;
        $user = Auth::user();
        if (!$user) {
            return $this->failResponse("Invalid Access");
        }
        if (!($user->hasRole('admin') || $user->hasRole('human-resources') || $user->hasRole('reporting-manager') || $user->hasRole('team-lead'))) {
            return $this->failResponse("Invalid user role");
        }
        $data = $request->all();
        $skip = 1;
        $role = 'user';

        $leave = Leave::find($id);
        if ($leave->status != 'pending') {
            return $this->failResponse('Only pending leaves can be rejected');
        }
        // Validation for reporting Manager
        if (!($user->hasRole('admin')) && $user->hasRole('reporting-manager')) {
            $reporting_manager = UserService::getReportingManager($leave->user_id);
            if (!empty($reporting_manager)) {
                if ($user->id != $reporting_manager['id']) {
                    return $this->failResponse("Unauthorized Request");
                }
            }

        }
        // ---
        $leaveUserReportingManager = UserService::getReportingManager($leave->user_id);
        if (!empty($leaveUserReportingManager['id']) && $user->id == $leaveUserReportingManager['id']) {
            $isMyRM = true;
        }
        $isTeamLead = LeaveService::isTeamLeadbyLeaveId($user->id, $id);
        if ($isTeamLead && !$isMyRM) {
            $approval = $leave->projectManagerApprovals()->where('user_id', $user->id)->first();
            if ($approval->status != 'pending') {
                return $this->failResponse('Team Lead action is already taken.');
            }
            $approval->status = "rejected";
            $approval->rejection_reason = $data['cancellation_reason'];
            if ($approval->save()) {
                return $this->successResponse($leave);
            } else {
                return $this->failResponse('Unable to update team lead status');
            }
        } else {
            // Validation for Team Lead
            if ($user->hasRole('team-lead') && !($user->hasRole('admin') || $user->hasRole('human-resources') || $user->hasRole('reporting-manager'))) {
                return $this->failResponse('Team Lead trying to reject a leave');
            }
            // ---
            $leave->cancellation_reason = $data['cancellation_reason'];
            if (!$leave->save()) {
                return $this->failResponse('Unable to reject leave');
            } else {
                $approver_id = Auth::id();
                $action_date = date('Y-m-d');
                $updated = LeaveService::rejectLeave($id, $approver_id, $action_date);
                if (!$updated) {
                    return $this->failResponse('Unable to reject leave');
                } else {
                    $leave = Leave::find($id);
                    return $this->successResponse($leave);
                }
            }
        }

    }
    public function approveLeave(Request $request, $id)
    {
        $isMyRM = false;
        $user = Auth::user();
        if (!$user) {
            return $this->failResponse("Invalid Access");
        }
        if (!($user->hasRole('admin') || $user->hasRole('human-resources') || $user->hasRole('reporting-manager') || $user->hasRole('team-lead'))) {
            return $this->failResponse("Invalid user role");
        }
        $isTeamLead = LeaveService::isTeamLeadbyLeaveId($user->id, $id);
        $leave = Leave::find($id);
        $leaveUserReportingManager = UserService::getReportingManager($leave->user_id);
        if (!empty($leaveUserReportingManager)) {
            if ($user->id == $leaveUserReportingManager['id']) {
                $isMyRM = true;
            }
        }

        if ($leave) {

            if ($isTeamLead && !$isMyRM) {
                $approval = $leave->projectManagerApprovals()->where('user_id', $user->id)->first();
                if ($approval->status != 'pending') {
                    return $this->failResponse('Team Lead action is already taken.');
                }
                $approval->status = "approved";
                if ($approval->save()) {
                    return $this->successResponse($leave);
                } else {
                    return $this->failResponse('Unable to update team lead status');
                }
            } else {
                // Validation for TL
                if ($user->hasRole('team-lead') && !($user->hasRole('admin') || $user->hasRole('human-resources') || $user->hasRole('reporting-manager'))) {
                    return $this->failResponse('Team Lead trying to approve a leave');
                }
                // ---
                if ($leave->status != 'pending') {
                    return $this->failResponse('Only pending leaves can be approved');
                }
                // Validation for RM
                if (!($user->hasRole('admin')) && $user->hasRole('reporting-manager')) {
                    $reporting_manager = UserService::getReportingManager($leave->user_id);
                    if (!empty($reporting_manager)) {
                        if ($user->id != $reporting_manager['id']) {
                            return $this->failResponse("Unauthorized Request");
                        }
                    }
                }
                $approver_id = Auth::id();
                $action_date = date('Y-m-d');

                if ($leave->save()) {

                    $check = LeaveService::approveLeave($leave->id, $approver_id, $action_date);
                    if (!$check) {
                        return $this->failResponse('Unable to save leave');
                    }

                } else {
                    return $this->failResponse('Unable to save leave');
                }
                return $this->successResponse($leave);
            }
        }
        return $this->failResponse('Leave not found');
    }

    public function cancelLeave(Request $request, $id)
    {
        $leave = Leave::find($id);
        $status = $leave->status;

        $approver_id = Auth::id();
        $action_date = date('Y-m-d');
        $cancelLeave = LeaveService::validateLeaveCancel($id);
        DB::beginTransaction();
        try {
            if (!$cancelLeave['status']) {
                return $this->failResponse($cancelLeave['message']);
            } else {
                $updated = LeaveService::cancelLeave($leave->id, $approver_id, $action_date);
                if (!$updated) {
                    return $this->failResponse('Unable to cancel leave');
                } else {

                    $data = [];

                    $data['user_id'] = $leave->user_id;
                    $data['calendar_year_id'] = $leave->calendar_year_id;
                    $data['leave_type_id'] = $leave->leave_type_id;
                    $data['date'] = $leave->end_date;
                    $data['days'] = (-1) * $leave->days;
                    $data['reference_type'] = 'App\Models\Leave\Leave';
                    $data['reference_id'] = $leave->id;

                    if ($status == "approved") {
                        $transObj = UserLeaveTransaction::saveData($data);

                        if (!$transObj) {

                            return $this->failResponse('Leave cannot be added in transaction');

                        }
                    }

                    DB::commit();

                    return $this->successResponse($leave);
                }
            }
        } catch (Exception $e) {
            DB::rollback();

            return $this->failResponse('Leave not cancelled');

        }
    }
    public function updateLeave(Request $request, $id)
    {
        $user = Auth::user();
        if (!$user) {
            return $this->failResponse("Invalid Access");
        }
        if (!($user->hasRole('admin') || $user->hasRole('human-resources') || $user->hasRole('reporting-manager'))) {
            return $this->failResponse("Invalid user role");
        }
        $leave = Leave::find($id);
        if ($leave) {
            // Validation for RM
            if (!($user->hasRole('admin')) && $user->hasRole('reporting-manager')) {
                $reporting_manager = UserService::getReportingManager($leave->user_id);
                if ($user->id != $reporting_manager['id']) {
                    return $this->failResponse("Unauthorized Request");
                }
            }
            // ---
            if ($leave->status != 'pending') {
                return $this->failResponse('Only pending leaves can be updated');
            }
            $leave->start_date = $request->start_date;
            $leave->end_date = $request->end_date;
            $leave->days = LeaveService::calculateWorkingDays($request->start_date, $request->end_date, $leave->user_id);
            $leave->type = $request->type;
            $leave->half = $request->half;
            if (!$leave->save()) {
                return $this->failResponse('Unable to update leave');
            }
            return $this->successResponse($leave);
        }
        return $this->failResponse('Leave not found');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $leave = Leave::find($id);
        $approver_id = Auth::id();
        $action_date = date('Y-m-d');
        $cancelLeave = LeaveService::validateLeaveCancel($id);
        if (!$cancelLeave['status']) {
            return $this->failResponse($cancelLeave['message']);
        } else {
            $updated = LeaveService::cancelLeave($leave->id, $approver_id, $action_date);
            if (!$updated) {
                return $this->failResponse('Unable to cancel leave');
            } else {
                return $this->successResponse($leave);
            }
        }
    }
}
