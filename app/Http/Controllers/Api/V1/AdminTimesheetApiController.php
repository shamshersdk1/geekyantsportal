<?php 

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\V1\ResourceController;
use App\Models\Timesheet;
use App\Models\TimesheetDetail;
use App\Models\Admin\ProjectResource;
use Validator;
use Auth;

class AdminTimesheetApiController extends ResourceController {
    protected $model = "\App\Models\Timesheet";

	public function store()
    {
        $input = $this->getRequestBody();
        $response = [];
        $status = true;
        if ( !$input['timesheet_id'] || !$input['approved_hours'] || !$input['manager_comments'] ) 
        {
            return $this->failResponse("Invalid inputs");
        }

		$user = Auth::user();
		$approver_id = $user->id;
        $timesheet_id = $input['timesheet_id'];
		$approved_hours = $input['approved_hours'];
        $manager_comments = $input['manager_comments'];
        $approval_date = date('Y-m-d');
        // Get timesheet
        $timesheet = Timesheet::where('id', $timesheet_id)->first();
        if ( !empty($timesheet) )
        {
            $timesheet->approver_id = $approver_id;	
		    $timesheet->approved_hours = $approved_hours;
            $timesheet->approval_date = $approval_date;
            $timesheet->manager_comments = $manager_comments;
            if( !$timesheet->save() )
            {
                $status = false;
                return $this->failResponse("Error while saving the details");
            }
            $response['status'] = $status;
            $response['message'] = 'Successfully saved';  
            return $this->successResponse($response);
        }
		return $this->failResponse("No record found");
	}

}
