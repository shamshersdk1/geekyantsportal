<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\V1\ResourceController;
// use App\Http\Controllers\Api\V1\BaseApiController;
use App\Services\TimesheetReportService;
use Auth;
use Input;

class UserTimesheetController extends ResourceController
{
    protected $model = "\App\Models\UserTimesheet";

    public function index($query = null)
    {
        if (!$query) {
            $query = $this->obj->buildQuery();
        }
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        // if ($userObj->hasRole('admin') || $userObj->hasRole('admin') ) {
        //     return $this->failResponse("Invalid Access");
        // }

        $date = \Input::has('date') ? \Input::get('date') : null;
        if ($date) {
            $query = $query->where('date', $date);
        }

        $query = $query->where('user_id', $userObj->id);
        $query = $query->with('user');

        return parent::index($query);
        //$sendData = $query->get()->toArray();
        //return $this->successResponse($sendData);
    }
    public function getUserTimesheet($monthId, $userId)
    {

        $userObj = \Auth::User();
        if (!$userObj->hasRole('admin') && !$userObj->checkRole('reporting-manager')) {
            return $this->failResponse("Invalid Access");
        }

        $data = TimesheetReportService::getUserTimesheetReport($monthId, $userId);

        return $this->successResponse($data);

    }
    public function getUserWeeklyTimesheet($weekId, $userId)
    {

        $userObj = \Auth::User();
        if (!$userObj->hasRole('admin') && !$userObj->checkRole('reporting-manager')) {
            return $this->failResponse("Invalid Access");
        }

        $data = TimesheetReportService::getUserWeeklyTimesheet($weekId, $userId);

        return $this->successResponse($data);

    }
}
