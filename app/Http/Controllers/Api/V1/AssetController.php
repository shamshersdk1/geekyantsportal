<?php
namespace App\Http\Controllers\Api\V1;

use App\Models\Admin\Asset;
use App\Models\Admin\AssetMeta;
use App\Models\Admin\IpAddressMapper;
use App\Models\Admin\IpAddressRange;
use App\Models\Admin\UserIpAddress;
use Auth;
use DB;

class AssetController extends ResourceController
{
    protected $model = "\App\Models\Admin\Asset";

    public function show($id)
    {
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        $obj = $this->model::with('metas.ipMapper.userIp')->find($id);
        if (!$obj) {
            return $this->failResponse("Not found");
        } else {
            return $this->successResponse($obj);
        }
    }
    public function networkAsset()
    {
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        $assetMetas = AssetMeta::where('key', "LAN-MAC")->orWhere('key', "WIFI-MAC")->pluck('asset_id')->toArray();
        $assets = Asset::whereIn('id', $assetMetas)->where('is_assignable', 0)->with('metas.ipMapper.userIp', 'assignmentDetail.user')->get();
        return $this->successResponse($assets);

    }
    public function assignIpNetworkAsset()
    {
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }

        $input = $this->getRequestBody();
        if ($input) {
            DB::beginTransaction();
            $range = IpAddressRange::where('company_asset', 1)->first();

            $start_ip = ip2long($range['start_ip']);
            $end_ip = ip2long($range['end_ip']);

            while ($start_ip <= $end_ip) {
                $ip = long2ip($start_ip);
                $isUsed = UserIpAddress::where('ip_address', $ip)->first();
                if (!$isUsed) {
                    $freeIp = $ip;
                    break;
                }
                $start_ip++;
            }
            if (!empty($freeIp)) {
                $assignIp = new UserIpAddress;
                $assignIp->user_id = 0;//$input['asset_id'];
                $assignIp->ip_address = $freeIp;
                $assignIp->ip_address_long = ip2long($freeIp);
                $assignIp->assigned_by = $userObj->id;
                if (!$assignIp->save()) {
                    DB::rollback();
                    return $this->failResponse("Unable to assign ip");
                } else {
                    $ipAddressMapper = new IpAddressMapper;
                    $ipAddressMapper->reference_type = "App\Models\Admin\AssetMeta";
                    $ipAddressMapper->reference_id = $input['meta_id'];
                    $ipAddressMapper->user_ip_address_id = $assignIp->id;
                    if (!$ipAddressMapper->save()) {
                        DB::rollback();
                        return $this->failResponse("Unable to assign ip to asset");
                    }
                }
            } else {
                return $this->failResponse('No ip available in the given range');
            }
            DB::commit();
            $sendData = IpAddressMapper::with('userIp')->find($ipAddressMapper->id);
            return $this->successResponse($sendData);
        }
    }
    public function releaseIpNetworkAsset($ip_mapper_id)
    {
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        DB::beginTransaction();
        $ipMapperObj = IpAddressMapper::where('id', $ip_mapper_id)->first();
        $user_ip_id = $ipMapperObj->user_ip_address_id;
        if (!$ipMapperObj->delete()) {
            DB::rollback();
            return $this->failResponse("Unable to release ip");
        }
        $userIpObj = UserIpAddress::where('id', $user_ip_id)->delete();
        if (!$userIpObj) {
            DB::rollback();
            return $this->failResponse("Unable to release ip");
        }
        DB::commit();
        return $this->successResponse("Ip released");
    }
}
