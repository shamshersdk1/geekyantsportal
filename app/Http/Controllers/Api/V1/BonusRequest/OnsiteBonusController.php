<?php
namespace App\Http\Controllers\Api\V1\BonusRequest;

use App\Http\Controllers\Api\V1\BaseApiController;
use App\Models\Admin\Bonus;
use App\Models\Admin\OnsiteAllowance;
use App\Models\Admin\OnSiteBonus;
use App\Models\BonusRequest;
use App\Models\Month;
use App\Models\User;
use Auth;
use DB;
use Exception;
use Illuminate\Http\Request;

class OnsiteBonusController extends BaseApiController
{
    public function store(Request $request)
    {
        $userObj = Auth::User();
        if (!$userObj) {
            return $this->failResponse('Unauth access');
        }

        $params = $this->getRequestBody();

        if (empty($params['date']) || empty($params['onsite_allowance_id'])) {
            return $this->failResponse('Invalid data');
        }

        $params['user_id'] = $userObj->id;
        $params['created_by'] = $userObj->id;
        $params['status'] = 'pending';

        $response = OnSiteBonus::saveData($params);
        if (!$response) {
            return $this->failResponse('Something went wrong');
        }
        return $this->successResponse($response);
    }

    public function approve($id)
    {

        $loggedInUser = Auth::user();
        if (!$loggedInUser) {
            return $this->failResponse('Unauthorized Request');
        }
        $onsiteObj = OnSiteBonus::with('reviewer')->find($id);
        if (!$onsiteObj) {
            return $this->failResponse('Invalid Onsite Bonus Id');
        }
        $onsiteUserObj = User::find($onsiteObj->user_id);
        if (!$onsiteUserObj) {
            return $this->failResponse('Invalid User Id');
        }
        if ($onsiteUserObj->reportingManager) {
            $reportingManager = $onsiteUserObj->reportingManager;
        }

        if ($loggedInUser->hasRole('admin') || $reportingManager->id == $loggedInUser->id) {
            if ($onsiteObj->status == "pending") {
                DB::beginTransaction();
                try {
                    $onsiteObj->status = "approved";
                    $onsiteObj->reviewed_by = $loggedInUser->id;
                    $onsiteObj->reviewed_at = date('Y-m-d H:i:s');
                    if (!$onsiteObj->save()) {
                        throw new Exception("Unable to Update Onsite table");
                    }
                    $bonusObj = new Bonus();
                    $monthId = Month::getMonth($onsiteObj->date);
                    $bonusObj->month_id = $monthId;
                    $bonusObj->user_id = $onsiteObj->user_id;
                    $bonusObj->date = $onsiteObj->date;
                    $onsiteallowanceObj = OnsiteAllowance::find($onsiteObj->onsite_allowance_id);
                    if (!$onsiteallowanceObj) {
                        throw new Exception("Invalid Onsite Allowance Object");
                    }
                    $bonusObj->amount = $onsiteallowanceObj->amount;
                    $bonusObj->status = "approved";
                    $bonusObj->reference_id = $onsiteObj->id;
                    $bonusObj->reference_type = "App\Models\Admin\OnSiteBonus";
                    $bonusObj->created_by = $onsiteObj->created_by;
                    $bonusObj->approved_at = date('Y-m-d H:i:s');
                    $bonusObj->approved_by = $loggedInUser->id;
                    if (!$bonusObj->save()) {
                        throw new Exception("Unable to add into Bonus table");
                    }
                    DB::commit();
                } catch (Exception $e) {
                    DB::rollback();
                    return $this->failResponse($e->getMessage());
                }
            } else {
                return $this->failResponse('Only Pending Requests can be Approved');
            }
        } else {
            return $this->failResponse('Unauthorized Access');
        }
        $onsiteObj = OnSiteBonus::with('reviewer')->find($id);
        return $this->successResponse($onsiteObj);
    }

    public function reject($id)
    {
        $loggedInUser = Auth::user();
        if (!$loggedInUser) {
            return $this->failResponse('Unauthorized Request');
        }
        $onsiteObj = OnSiteBonus::find($id);
        if (!$onsiteObj) {
            return $this->failResponse('Invalid Onsite Bonus Id');
        }
        $onsiteUserObj = User::find($onsiteObj->user_id);
        if (!$onsiteUserObj) {
            return $this->failResponse('Invalid User Id');
        }
        if ($onsiteUserObj->reportingManager) {
            $reportingManager = $onsiteUserObj->reportingManager;
        }

        if ($loggedInUser->hasRole('admin') || $reportingManager->id == $loggedInUser->id) {
            if ($onsiteObj->status == "pending") {
                $onsiteObj->status = "rejected";
                $onsiteObj->reviewed_by = $loggedInUser->id;
                $onsiteObj->reviewed_at = date('Y-m-d H:i:s');
                if (!$onsiteObj->save()) {
                    return $this->failResponse("Unable to Update Onsite table");
                }
            } else {
                return $this->failResponse('Only Pending Requests can be Rejected');
            }
        } else {
            return $this->failResponse('Unauthorized Access');
        }
        $onsiteObj = OnSiteBonus::with('reviewer')->find($id);
        return $this->successResponse($onsiteObj);
    }
    public function delete($id)
    {
        $userObj = Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        $obj = OnSiteBonus::find($id);

        if (!$obj) {
            return $this->failResponse("Invalid id # " . $id);
        }

        if ($obj->user_id != $userObj->id) {
            return $this->failResponse("Invalid id $ " . $id);
        }

        if ($obj->status != 'pending') {
            return $this->failResponse("Onsite bonus request is already processed. Cannot delete #" . $id);
        }

        $obj->delete();
        return $this->successResponse($obj);
    }

    public function cancel($id)
    {
        $loggedInUser = Auth::user();
        if (!$loggedInUser) {
            return $this->failResponse('Unauthorized Request');
        }
        $onsiteObj = OnSiteBonus::find($id);
        if (!$onsiteObj) {
            return $this->failResponse('Invalid Onsite Bonus Id');
        }
        $onsiteUserObj = User::find($onsiteObj->user_id);
        if (!$onsiteUserObj) {
            return $this->failResponse('Invalid User Id');
        }
        if ($onsiteUserObj->reportingManager()) {
            $reportingManager = $onsiteUserObj->reportingManager();
        }

        if ($loggedInUser->hasRole('admin') || $reportingManager->id == $loggedInUser->id || $onsiteUserObj->id == $loggedInUser->id) {
            if ($onsiteObj->status == "pending") {
                $onsiteObj->status = "cancelled";
                $onsiteObj->reviewed_by = $loggedInUser->id;
                $onsiteObj->reviewed_at = date('Y-m-d H:i:s');
                if (!$onsiteObj->save()) {
                    return $this->failResponse("Unable to Update Onsite table");
                }
            } else {
                return $this->failResponse('Only Pending Requests can be Cancelled');
            }
        } else {
            return $this->failResponse('Unauthorized Access');
        }
    }
}
