<?php

namespace App\Http\Controllers\Api\V1\BonusRequest;

use App\Http\Controllers\Api\V1\BaseApiController;
use App\Models\Admin\Leave;
use App\Models\Admin\PayslipCsvData;
use App\Models\Admin\PayslipCsvMonth;
use App\Models\Admin\AdditionalWorkDaysBonus;
use App\Models\User;
use App\Models\Month;
use App\Models\Appraisal\Appraisal;
use App\Models\Admin\Bonus;
use App\Services\BonusService;
use App\Services\Calendar;
use App\Services\UserService;
use Auth;
use Illuminate\Http\Request;
use Input;
use DB;
use Exception;


class AdditionalWorkingDayBonusController extends BaseApiController
{

    // public function index(Request $request)
    // {
    //     $id = $request->id;
    //     $csvMonth = PayslipCsvMonth::find($id);
    //     if ($csvMonth) {
    //         $month = $csvMonth->month;
    //         if ($month < 10) {
    //             $month = '0' . $month;
    //         }
    //         $bonuses = PayslipCsvData::bonuses($id);
    //         $result = [];
    //         $result['data'] = $bonuses;
    //         return $this->successResponse($result);
    //     } else {
    //         return $this->failResponse('Invalid link followed');
    //     }
    // }

   public function show($id) {
        $userObj = Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        $obj = AdditionalWorkDaysBonus::find($id);
        
        if(!$obj)
            return $this->failResponse("Invalid id ".$id);

        if($obj->user_id == $userObj->id)
            return $this->failResponse("Invalid id ".$id);

        return $this->successResponse($obj);
    }
    
    public function delete($id) {
        $userObj = Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        $obj = AdditionalWorkDaysBonus::find($id);
        
        if(!$obj)
            return $this->failResponse("Invalid id # ".$id);

        if($obj->user_id != $userObj->id)
            return $this->failResponse("Invalid id $ ".$id);

        if($obj->status != 'pending')
            return $this->failResponse("Additonal bonus request is already processed. Cannot delete #".$id);

        $obj->delete();
        return $this->successResponse($obj);
    }

    // public function getProjectByDate()
    // {
    //     \Log::info('getProjectByDate called' . json_encode(Input::all()));
    //     $user = \Auth::user();
    //     $userId = $user->id;

    //     if (!$user) {
    //         return $this->failResponse('Unauthosized access');
    //     }
    //     if (!($user->hasRole('admin') || $user->hasRole('human-resource')) && $userId != $user->id) {
    //         return $this->failResponse('Unauthosized access');
    //     }

    //     $projects = UserService::getTimesheetProjects($userId, $date);
    //     return $this->successResponse($projects);

    // }
    // public function fetchUserBonus($monthYear, $projectId)
    // {
    //     $userObj = Auth::User();

    //     if (!$userObj) {
    //         return $this->failResponse('Unauth access');
    //     }
    //     $userId = $userObj->id;
    //     // $month = 12;
    //     $month = substr($monthYear, 0, 3);
    //     $year = substr($monthYear, 4);
    //     // echo "<pre>";
    //     // print_r($month);
    //     // print_r($year);
    //     // echo "</pre>";
    //     // die;

    //     $bonus = BonusService::getUserBonus($userId, $month, $year, $projectId);

    //     return $this->successResponse([$bonus]);
    // }

    public function store(Request $request)
    {
        $userObj = Auth::User();

        if (!$userObj) {
            return $this->failResponse('Unauthorized access');
        }
        $params = $this->getRequestBody();

        if(empty($params['date']) || empty($params['projects']) || empty($params['redeem_type'])) {
            return $this->failResponse('Invalid data');
        }

        
        
        // check if date is locked

        //check type of addition bonus - weekend, holiday, leave

        $params['type'] = 'weekend';
        $params['user_id'] = $userObj->id;
        $params['status'] = 'pending';
        $additionalObj = AdditionalWorkDaysBonus::where('user_id',$userObj->id)->where('date', $params['date'])->whereIn('type', ['weekend','holiday','leave'])->first();
        if($additionalObj && ($additionalObj->status == 'pending' || $additionalObj->status == 'approved')) {
            return $this->failResponse('You have already applied for a additional bonus request');
        }

        try {
        $response = AdditionalWorkDaysBonus::saveBonus($params);
        }catch(Expection $e) {

        }
        if(isset($response['status']) && $response['status'] == false){
            return $this->failResponse($response['message']);
        } 

        return $this->successResponse($response['data']);

    }

    public function getTimesheetDetail() {
        $userObj = Auth::User();

        if (!$userObj) {
            return $this->failResponse('Unauthorized access');
        }
        
        $params = $this->getRequestBody();

        if(empty($params['date']) || empty($params['user_id'])) {
            return $this->failResponse('Invalid data');   
        }
        $date = $params['date'];
        $userId = $params['user_id'];

        $projects = UserService::getTimesheetProjects($userId, $date);
        return $this->successResponse($projects);
    }

    public function approveBKP($id)
    {
        $loggedInUser = Auth::user();
        if (!$loggedInUser) {
            return $this->failResponse('Unauthorized Request');
        }
        $additionalBonusObj = AdditionalWorkDaysBonus::find($id);
        if (!$additionalBonusObj) {
            return $this->failResponse('Invalid Additional Bonus Id');
        }
        $userObj = User::find($additionalBonusObj->user_id);
        if (!$userObj) {
            return $this->failResponse('Invalid User Id');
        }
        if ($userObj->reportingManager()) {
            $reportingManager = $userObj->reportingManager();
        }

        if ($loggedInUser->hasRole('admin') || $reportingManager->id == $loggedInUser->id) {
            if ($additionalBonusObj->status == "pending") {
                DB::beginTransaction();
                try {
                    $additionalBonusObj->status = "approved";
                    $additionalBonusObj->reviewed_by = $loggedInUser->id;
                    if (!$additionalBonusObj->save()) {
                        throw new Exception("Unable to Update Onsite table");
                    }
                    $bonusObj = new Bonus();
                    $monthId=Month::getMonth($additionalBonusObj->date);
                    $bonusObj->month_id = $monthId;
                    $bonusObj->user_id = $additionalBonusObj->user_id;
                    $bonusObj->date = $additionalBonusObj->date;
                    $bonusObj->amount = $additionalBonusObj->amount;
                    $bonusObj->status = "approved";
                    $bonusObj->reference_id = $additionalBonusObj->id;
                    $bonusObj->reference_type = "App\Models\Admin\AdditionalWorkDaysBonus";
                    $bonusObj->created_by = $additionalBonusObj->created_by;
                    $bonusObj->approved_at = date('Y-m-d H:i:s');
                    if (!$bonusObj->save()) {
                        throw new Exception("Unable to add into Bonus table");
                    }
                    DB::commit();
                } catch (Exception $e) {
                    DB::rollback();
                    return $this->failResponse($e->getMessage());
                }
            } else {
                return $this->failResponse('Only Pending Requests can be Approved');
            }
        } else {
            return $this->failResponse('Unauthorized Access');
        }
    }


    public function reject($id)
    {
        $loggedInUser = Auth::user();
        if (!$loggedInUser) {
            return $this->failResponse('Unauthorized Request');
        }
        $additionalBonusObj = AdditionalWorkDaysBonus::find($id);
        if (!$additionalBonusObj) {
            return $this->failResponse('Invalid Additional Bonus Id');
        }
        $userObj = User::find($additionalBonusObj->user_id);
        if (!$userObj) {
            return $this->failResponse('Invalid User Id');
        }
        
        if ($loggedInUser->hasRole('admin') || $userObj->parent_id == $loggedInUser->id) {
           
            if ($additionalBonusObj->status == "pending") {
                DB::beginTransaction();
                try {
                    $additionalBonusObj->status = "rejected";
                    $additionalBonusObj->reviewed_by = $loggedInUser->id;
                    if (!$additionalBonusObj->save()) {
                        throw new Exception("Unable to Update Onsite table");
                    }
                   
                    DB::commit();
                } catch (Exception $e) {
                    DB::rollback();
                    return $this->failResponse($e->getMessage());
                }
            } else {
                return $this->failResponse('Only Pending Requests can be Approved');
            }
        } else {
            return $this->failResponse('Unauthorized Access');
        }
        return $this->successResponse($additionalBonusObj);
    }
    // public function storeAdd(Request $request)
    // {
    //     $userObj = Auth::User();

    //     if (!$userObj) {
    //         return $this->failResponse('Unauthorized access');
    //     }

    //     $userId = $userObj->id;
    //     $dataParam = request([
    //         'date',
    //         'projectId',
    //     ]);
    //     $response = BonusService::applyForAdditionalWorkingBonus($userId, $dataParam['date'], $dataParam['projectId']);

    //     if (isset($response['error']) && $response['error']) {
    //         if (!empty($response['message'])) {
    //             return $this->failResponse($response['message']);
    //         }
    //         return $this->failResponse('Something went wrong');
    //     }
    //     return $this->successResponse($response['result']);

    // }

    // public function deleteOnSiteBonus($id)
    // {
    //     $userObj = Auth::User();

    //     if (!$userObj) {
    //         return $this->failResponse('Unauth access');
    //     }
    //     $userId = $userObj->id;
    //     $response = BonusService::deleteRequestForOnSiteBonus($id);
    //     if (isset($response['error']) && $response['error']) {
    //         if (!empty($response['message'])) {
    //             return $this->failResponse($response['message']);
    //         }
    //         return $this->failResponse('Something went wrong');
    //     }
    //     return $this->successResponse($response);
    // }

    // public function deleteBonusRequest($id)
    // {
    //     $userObj = Auth::User();

    //     if (!$userObj) {
    //         return $this->failResponse('Unauth access');
    //     }
    //     $response = BonusService::deleteBonusRequest($id);
    //     if ($response) {
    //         return $this->successResponse('Successfully Deleted');
    //     } else {
    //         return $this->failResponse('Something went wrong');
    //     }

    // }

    // public function deleteAdditionalWorkingBonus($id)
    // {
    //     $userObj = Auth::User();

    //     if (!$userObj) {
    //         return $this->failResponse('Unauth access');
    //     }
    //     $userId = $userObj->id;
    //     $response = BonusService::deleteRequestForAdditionWorkingBonus($id);
    //     if (isset($response['error']) && $response['error']) {
    //         if (!empty($response['message'])) {
    //             return $this->failResponse($response['message']);
    //         }
    //         return $this->failResponse('Something went wrong');
    //     }
    //     return $this->successResponse($response['result']);
    // }

    // public function addProjectsToBonus(Request $request)
    // {
    //     $userObj = Auth::User();

    //     if (!$userObj) {
    //         return $this->failResponse('Unauth access');
    //     }
    //     $input = $request->all();

    //     if ($input['bonus_request_id'] && isset($input['projects'])) {
    //         $response = BonusService::addProjectsToBonus($userObj->id, $input['bonus_request_id'], $input['projects']);
    //         if ($response) {
    //             return $this->successResponse('Successfully added');
    //         } else {
    //             return $this->failResponse('Something went wrong');
    //         }
    //     } else {
    //         return $this->failResponse('Invalid Input');
    //     }

    // }
    public function approve($id)
    {
        $loggedInUser = Auth::user();
        if (!$loggedInUser) {
            return $this->failResponse('Unauthorized Request');
        }
        $additionalObj = AdditionalWorkDaysBonus::find($id);
        if (!$additionalObj) {
            return $this->failResponse('Invalid Additional Bonus Id');
        }
        $userObj = User::find($additionalObj->user_id);
        if (!$userObj) {
            return $this->failResponse('Invalid User Id');
        }
        if ($loggedInUser->hasRole('admin') || $userObj->parent_id == $loggedInUser->id) {
            if ($additionalObj->status == "pending") {
                DB::beginTransaction();
                try {
                    $additionalObj->status = "approved";
                    $additionalObj->reviewed_by = $loggedInUser->id;
                    //$additionalObj->reviewed_at = date('Y-m-d H:i:s');
                    if (!$additionalObj->save()) {
                        throw new Exception("Unable to Update Onsite table");
                    }
                    $bonusObj = new Bonus();
                    //$monthObj = Month::where('month', $additionalObj->date('m', strtotime('date')))->where('year', $additionalObj->date('Y', strtotime('date')))->first();
                    $monthObj = Month::getMonth($additionalObj->date);
                    if (!$monthObj) {
                        throw new Exception("Invalid month");
                    }
                    $bonusObj->month_id = $monthObj;
                    $bonusObj->user_id = $additionalObj->user_id;
                    $bonusObj->date = $additionalObj->date;
                    // $onsiteallowanceObj = OnsiteAllowance::find($additionalObj->onsite_allowance_id);
                    // if (!$onsiteallowanceObj) {
                    //     throw new Exception("Invalid Onsite Allowance Object");
                    // }
                    $oneDaySalary = Appraisal::getBonusAmount($additionalObj->user_id,$additionalObj->date);
                    $data['amount'] = round(($oneDaySalary/8)* $additionalObj->duration);

                    $bonusObj->amount = $data['amount'];//$onsiteallowanceObj->amount;
                    $bonusObj->status = "approved";
                    $bonusObj->type = "additional";
                    $bonusObj->reference_id = $additionalObj->id;
                    $bonusObj->reference_type = "App\Models\Admin\AdditionalWorkDaysBonus";
                    $bonusObj->created_by = $additionalObj->created_by;
                    $bonusObj->approved_by = $loggedInUser->id;
                    $bonusObj->draft_amount = $data['amount'];
                    $bonusObj->approved_at = date('Y-m-d H:i:s');
                    if (!$bonusObj->save()) {
                        throw new Exception("Unable to add into Bonus table");
                    }
                    DB::commit();
                } catch (Exception $e) {
                    DB::rollback();
                    return $this->failResponse($e->getMessage());
                }
            } else {
                return $this->failResponse('Only Pending Requests can be Approved');
            }
        } else {
            return $this->failResponse('Unauthorized Access');
        }

        return $this->successResponse($additionalObj);
    }
}