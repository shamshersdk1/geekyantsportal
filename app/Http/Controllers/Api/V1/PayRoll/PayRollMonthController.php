<?php

namespace App\Http\Controllers\Api\V1\PayRoll;

use App\Http\Controllers\Api\V1\ResourceController;
use App\Jobs\PayRoll\RecalculateUserSalaryJob;

class PayRollMonthController extends ResourceController
{
    protected $model = "\App\Models\Admin\Finance\SalaryUserData";
    public function recalculateSalaryData($id)
    {
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if ($userObj && !($userObj->hasRole('admin') || $userObj->hasRole('account'))) {
            return $this->failResponse("Invalid Access");
        }
        $salaryUser = $this->model::find($id);
        if (!$salaryUser) {
            return $this->failResponse("Salary user data not found");
        }
        $salaryUser->status = "pending";
        if (!$salaryUser->save()) {
            return $this->failResponse("Something went wrong.");
        }
        $job = (new RecalculateUserSalaryJob($id));
        dispatch($job);
        return $this->successResponse('Successfully saved');
    }
}
