<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\V1\ResourceController;
use App\Models\Admin\PerformanceBonus;
use Auth;
use Input;

class PerformanceBonusController extends ResourceController
{

    protected $model = "App\Models\Admin\OnSiteBonus";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($query = null)
    {

        if (!$query) {
            $query = $this->obj->buildQuery();
        }
        // $userObj = \Auth::User();
        // if (!$userObj) {
        //     return $this->failResponse("Invalid Access");
        // }
        $project_id = \Input::has('project_id') ? \Input::get('project_id') : 0;
        $query = $query->where('reference_id', $project_id)->with('user', 'reviewer');
        $query = $query->orderBy('id', 'DESC');

        $sendData = $query->get()->toArray();
        return $this->successResponse($sendData);

    }
    public function store()
    {
        $authUser = Auth::user();
        if (!$authUser) {
            return $this->failResponse('Invalid Access');
        }
        if (!($authUser->hasRole('admin')) && !($authUser->hasRole('account')) && !($authUser->hasRole('reporting-manager'))) {
            return $this->failResponse('Unauthorized Access');
        }
        $input = $this->getRequestBody();
        $OnSiteBonus = new PerformanceBonus();
        $OnSiteBonus->employee_id = $input['user_id'];
        $OnSiteBonus->type = $input['type'];
        $OnSiteBonus->reference_id = $input['project_id'];
        $OnSiteBonus->reference_type = 'App\Models\Admin\Project';
        $OnSiteBonus->amount = $input['amount'];
        $OnSiteBonus->status = 'approved';
        $OnSiteBonus->to_date = !empty(date_to_yyyymmdd($input['to_date'])) ? date_to_yyyymmdd($input['to_date']) : null;
        $OnSiteBonus->from_date = !empty(date_to_yyyymmdd($input['from_date'])) ? date_to_yyyymmdd($input['from_date']) : null;
        $OnSiteBonus->notes = !empty($input['notes']) ? $input['notes'] : null;
        $OnSiteBonus->created_by = $authUser->id;
        if (!$OnSiteBonus->save()) {

            return $this->failResponse('Unable to save contact');
        }
        $OnSiteBonusObj = OnSiteBonus::where('id', $OnSiteBonus->id)->with('user', 'reviewer')->first();
        return $this->successResponse($OnSiteBonusObj);
    }
    public function destroy($id)
    {
        $userObj = Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if (OnSiteBonus::find($id)->delete()) {
            return $this->successResponse('Deleted successfully');
        } else {
            return $this->failResponse("Couldn't delete the record");
        }
    }
    public function updateBonus()
    {
        $authUser = Auth::user();
        if (!$authUser) {
            return $this->failResponse('Invalid Access');
        }
        if (!($authUser->hasRole('admin')) && !($authUser->hasRole('account')) && !($authUser->hasRole('reporting-manager'))) {
            return $this->failResponse('Unauthorized Access');
        }

        $input = $this->getRequestBody();
        $OnSiteBonus = OnSiteBonus::find($input['bonus_id']);
        $OnSiteBonus->employee_id = $input['user_id'];
        $OnSiteBonus->type = $input['type'];
        $OnSiteBonus->reference_id = $input['project_id'];
        $OnSiteBonus->amount = $input['amount'];
        $OnSiteBonus->to_date = !empty(date_to_yyyymmdd($input['to_date'])) ? date_to_yyyymmdd($input['to_date']) : null;
        $OnSiteBonus->from_date = !empty(date_to_yyyymmdd($input['from_date'])) ? date_to_yyyymmdd($input['from_date']) : null;
        $OnSiteBonus->notes = !empty($input['notes']) ? $input['notes'] : null;
        $OnSiteBonus->created_by = $authUser->id;
        if (!$OnSiteBonus->save()) {
            return $this->failResponse('Unable to save contact');
        }
        $OnSiteBonusObj = OnSiteBonus::where('id', $OnSiteBonus->id)->with('user', 'reviewer')->first();
        return $this->successResponse($OnSiteBonusObj);
    }
    public function show($id)
    {
        $userObj = Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        $OnSiteBonusObj = OnSiteBonus::where('id', $id)->with('user', 'reviewer')->first();
        if ($OnSiteBonusObj) {
            return $this->successResponse($OnSiteBonusObj);
        } else {
            return $this->failResponse("Couldn't delete the record");
        }
    }
}
