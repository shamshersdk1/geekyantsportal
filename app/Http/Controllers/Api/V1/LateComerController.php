<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\User;
use App\Models\Admin\LateComer;
use App\Services\LateComerService;
use Illuminate\Http\Request;
use Validator;

class LateComerController extends ResourceController
{
    protected $model = "\App\Models\Admin\LateComer";
    
    public function index($query = null)
    {
        if (!$query) {
            $query = $this->obj->buildQuery();
        }
        
        $userObj = \Auth::User();
        if (!$userObj->checkRole('admin') && !$userObj->checkRole('human-resources') && !$userObj->checkRole('reporting-manager')) {
            return $this->failResponse("Invalid Access");
        }

        if (\Input::has('q')) {
            $query = $query->genericSearch(\Input::get('q'));
        }

        if (\Input::has('order_by_field') && \Input::has('order_by_type')) {
            $query = $query->orderBy(\Input::get('order_by_field'), \Input::get('order_by_type'));
        }
        
        $relationsAsStr = \Input::has('relations') ? \Input::get('relations') : '';
        $relationsAsArr = array_map('trim', array_filter(explode(',', $relationsAsStr)));
        $query->with($relationsAsArr);

        $pagination = \Input::has('pagination') ? \Input::get('pagination') : 1;
        $date = \Input::has('date') ? \Input::get('date') : 1;

        $query = $query->where('date', $date);
        if ($pagination == 0) {

            $sendData = $query->get()->toArray();

            // foreach ($sendData as &$data) {
            //     $data['last_24_hrs'] = SubscriberRegisteredReport::getCount($data['id']);
            // }

            return $this->successResponse($sendData);

        } else {

            $perPage = \Input::has('per_page') ? \Input::get('per_page') : $this->per_page;

            $total = $query->count();

            $paginator = $query->paginate($perPage);

            $result = $paginator->toArray();

            $perPage = $result['per_page'];
            $currentPage = $result['current_page'];

            $metaData = $this->formatMetaData($total, $perPage, $currentPage);
            $sendData = $result['data'];

            return $this->successResponse($sendData, $metaData);

        }
    }

    public function store()
    {
        $input = $this->getRequestBody();
        $reportedBy = \Auth::user();
        if ( empty($input) )
        {
            return $this->failResponse("Invalid inputs");
        }
        if ( !($reportedBy->hasRole('admin') || $reportedBy->hasRole('human-resources')) )
        {
            return $this->failResponse('Invalid Access');
        }
        $response = LateComerService::saveData($input['user']['id'], $input['date'], $reportedBy->id);
        if( $response['status'] )
        {
            return $this->successResponse($response);
        }
        else
        {
            return $this->failResponse($response['message']);
        }
    }

    public function destroy($id)
    {
        $reportedBy = \Auth::user();
        if ( !($reportedBy->hasRole('admin') || $reportedBy->hasRole('human-resources')) )
        {
            return $this->failResponse('Invalid Access');
        }

        $response = LateComerService::delete($id);
        
        if( $response['status'] )
        {
            return $this->successResponse($response);
        }
        else
        {
            return $this->failResponse($response['message']);
        }
        
    }
}
