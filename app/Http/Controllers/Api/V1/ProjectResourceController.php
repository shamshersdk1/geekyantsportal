<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
// use App\Http\Controllers\Api\BaseApiController;
use App\Http\Controllers\Api\V1\ResourceController;
use App\Models\Admin\ProjectResource;
use App\Models\Admin\Project;
use App\Events\Project\DeveloperAssigned;

use Auth;
use Redirect;
use View;
use DB;

class ProjectResourceController extends ResourceController
{
    protected $model = "\App\Models\Admin\ProjectResource";
    
    public function index($query = null)
    {
        if (!$query) {
            $query = $this->obj->buildQuery();
        }
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        $date = date('Y-m-d');
        $project_id = \Input::has('project_id') ? \Input::get('project_id') : 0;
        $query->with('user_data','resource_type','project');
        $query = $query->where('project_id', $project_id);
        $query = $query->orderByRaw('ISNULL(end_date) DESC,  end_date DESC ')->orderBy('start_date', 'DESC');
        $sendData = $query->get()->toArray();
        return $this->successResponse($sendData);
        
    }

    public function store()
    {
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }

        $input = $this->getRequestBody();
        
        $projectResources = new ProjectResource();
        $projectResources->project_id = $input['project_id'];
        $projectResources->user_id = $input['user_id'];
        $projectResources->resource_price_id = $input['resource_price_id'];
        $projectResources->start_date = $input['start_date'];
        $projectResources->end_date = !empty($input['end_date']) ? $input['end_date'] : null;
        if ($projectResources->save()) {
            event(new DeveloperAssigned($projectResources->user_id, $projectResources->project_id, $projectResources->start_date,
                $projectResources->end_date));
            return $this->successResponse('Successfully saved');        
        }
        else {
            return $this->failResponse('Unable to save');    
        }
        
    }
    
    public function update($id)
    {
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        $input = $this->getRequestBody();
        $input = $input['params'];

        $projectResource = ProjectResource::find($id);
        if ( !$projectResource )
        {
            return $this->failResponse("No record found");
        }
        $projectResource->user_id = $input['user_id'];
        $projectResource->resource_price_id = !empty($input['resource_price_id']) ? $input['resource_price_id'] : null;
        $projectResource->start_date = $input['start_date'];
        $projectResource->end_date =!empty($input['end_date']) ? $input['end_date'] : null;
        
        if ($projectResource->save()) {
            return $this->successResponse('Successfully saved');        
        }
        else {
            return $this->failResponse('Unable to save');    
        }
    }

    public function destroy($id)
    {
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if( ProjectResource::find($id)->delete() ) 
        {
            return $this->successResponse('Deleted successfully');
        }
        else {
            return $this->failResponse("Couldn't delete the record");
        }
    }
    public function releaseUser($id)
    {
        $userObj = \Auth::User();
        $date = date('Y-m-d', strtotime("-1 days"));
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        $projectResource =  ProjectResource::find($id);
        if ( $projectResource )
        {
            $projectResource->end_date = $date;
            $projectResource->save();
            return $this->successResponse('Released successfully');
        }
        else {
            return $this->failResponse("Couldn't update the record");
        }
    }  
}