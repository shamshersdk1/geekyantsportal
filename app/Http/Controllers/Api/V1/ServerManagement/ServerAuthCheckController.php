<?php

namespace App\Http\Controllers\Api\V1\ServerManagement;

use App\Http\Controllers\Api\V1\ResourceController;
use App\Models\User;
use App\Models\Audited\Server\Server;
use App\Models\Audited\Server\Account;
use App\Models\Audited\Server\AccountAccess;
use App\Models\SshKey;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ServerAuthCheckController extends ResourceController
{

    protected $model = "App\Models\Audited\Server\Server";

    public function authCheck(Request $request)
    {
        $input = $request->all();
        $input['ip'] = $request->ip();
        $validator = Validator::make($input, [
            'ip' => 'required | ip',
            'account' => 'required',
            'fingerprint' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->failResponse($validator->errors());
        }

        $serverObj = Server::where('primary_ip',$input['ip'])->first();
        if(!$serverObj) {
            return $this->failResponse('Sorry, Your server is not registered on the Portal', 404);
        }

        $sshObj = SshKey::where('fingerprint',$input['fingerprint'])->first();
        if(!$sshObj) {
            return $this->failResponse('Sorry, We are unable to find public ssh key registered on Portal', 404);
        }

        $server_account = Account::where([
            ['server_id','=', $serverObj->id],
            ['username','=',$input['account']]
        ])->first();

        if(!$server_account){
            return $this->failResponse('Sorry, We are unable to find the account name registered with this server on Portal', 404);
        }

        $user = User::where([
            ['id','=',$sshObj->user_id],
            ['is_active','=', 1]
        ])->first();

        if(!$user){
            return $this->failResponse('Sorry, Invalid User Account!', 404);
        }

        $user_server_account_access = AccountAccess::where([
                ['user_id','=', $sshObj->user_id],
                ['account_id','=',$server_account->id]
            ])->first();

        if(!$user_server_account_access){
            return $this->failResponse('Sorry, You are not authorized to access this server');
        }

        return $this->successResponse(['output'=>$sshObj->value]);
    }
}
