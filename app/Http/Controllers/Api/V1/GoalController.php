<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
// use App\Http\Controllers\Api\BaseApiController;
use App\Http\Controllers\Api\V1\ResourceController;

use App\Models\Admin\Goal;
use App\Models\Admin\Tag;
use App\Models\Admin\UserGoalItem;

use Auth;
use Redirect;
use View;
use DB;

class GoalController extends ResourceController
{
    protected $model = "\App\Models\Admin\Goal";
    
    public function store()
    {
        $input = $this->getRequestBody();
        $message = '';

        if(!empty($input['title']) && !empty($input['process'])) 
        {
            $result = Goal::saveGoal($input);
            if($result['status']) 
            {
                $response = $result;
            } else 
            {
                $message = $result['message'];
            }
        } 
        else 
        {
            $message = "Incomplete request";
        }
        if($response['status']) 
        {
            return $this->successResponse($response);
        } 
        else 
        {
            return $this->failResponse($message);
        }
    }

    public function filterGoals(Request $request)
    {
        $name = !empty($request->name) ? $request->name : "";
        $tags = !empty($request->tags) ? $request->tags : null;
        $page = !empty($request->page) ? $request->page : 1;
        $per_page = !empty($request->per_page) ? $request->per_page : 10;
        $goals = Goal::getPublicGoals($name, $tags, $page, $per_page);
        $result['data'] = $goals;
        return $this->successResponse($result);
    }
}