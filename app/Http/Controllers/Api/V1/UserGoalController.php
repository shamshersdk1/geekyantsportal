<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
// use App\Http\Controllers\Api\BaseApiController;
use App\Http\Controllers\Api\V1\ResourceController;

use App\Models\Admin\Goal;
use App\Models\Admin\Tag;
use App\Models\Admin\UserGoalItem;
use App\Models\Admin\UserGoal;


use Auth;
use Redirect;
use View;
use DB;
use stdClass;


class UserGoalController extends ResourceController
{
    protected $model = "\App\Models\Admin\UserGoal";

    public function store()
    {
        $input = $this->getRequestBody();
        
        $loggedUser = Auth::user();
        if (!$loggedUser) {
            return $this->failResponse("Invalid Access");
        }
        if ( !( $loggedUser->hasRole('admin') || $loggedUser->hasRole('human-resources') || $loggedUser->hasRole('user') ) ) {
            return $this->failResponse("Invalid user role");
        }

        $saveGoal = UserGoal::saveGoal($input);
        if( $saveGoal['status'] )
        {
            return $this->successResponse($saveGoal);
        }
        else
        {
            return $this->failResponse($saveGoal['message']);
        }
    }

    public function storeUserGoal(Request $request, $id)
    {
        $response = ['status' => true, 'message' => "Goal Saved"];
        if($request->is_private != false) {
            $result = UserGoalItem::saveItem($id, $result['id'], $request->is_private, $request->is_idp);
        } else {
            $response['message'] = "Goal added to user";
        }
    }

    public function getItems($id, Request $request = null)
    {
        // if(date('n') < 4) {
        //     $start_date = date("Y-04-01", strtotime("-1 year"));
        //     $end_date = date("Y-03-31");
        // } else {
        //     $start_date = date("Y-04-01");
        //     $end_date = date("Y-03-31", strtotime("+1 year"));
        // }
        // if(!is_null($request) && !empty($request->start_date) && !empty($request->end_date)) {
        //     $start_date = date("Y-m-d", strtotime($request->start_date));
        //     $end_date = date("Y-m-d", strtotime($request->end_date));
        // }
        $goals = [];
        $user_goal = UserGoal::with('items')->where('id', $id)->first();
        if($user_goal && $user_goal->items) {
            $goals = [];
            foreach($user_goal->items as $item)
            {
                $tags = $item->tags->pluck('name')->toArray();
                $array = $item->toArray();
                $array['tags'] = $tags;
                $array['is_open'] = false;
                $goals[] = $array;
            }
        }
        usort($goals, array("App\Models\Admin\UserGoalItem", "idpCompare"));
        $result['data'] = $goals;
        return $this->successResponse($result);
    }

    public function show($id)
    {
        $userObj = Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        $userGoal = UserGoal::find($id);
        
        if (empty($userGoal)) {
            return $this->failResponse("User Goal not found");
        }
        $response = [];
        $response['data'] = $userGoal;
        return $this->successResponse($response);
    }
    public function update($id)
    {
        $userObj = Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        $input = $this->getRequestBody();
        if ( empty($input['status']) )
        {
            return $this->failResponse("Invalid input data");
        }
        
        $userGoal = UserGoal::updateStatus($id, $input['status']);
        
        return $this->successResponse($userGoal);
    }
}