<?php

namespace App\Http\Controllers\Api\V1\Timesheet;

use App\Http\Controllers\Api\V1\ResourceController;
// use App\Http\Controllers\Api\V1\BaseApiController;
use App\Models\Admin\Project;
use App\Models\Admin\ProjectResource;
use App\Models\Month;
use App\Models\User;
use App\Models\User\UserMonthlyTimesheet;
use App\Services\BonusService;
use App\Services\ProjectService;
use App\Services\TimesheetReportService;
use App\Services\TimesheetService;
use App\Services\UserService;
use Auth;
use Input;

class UserTimesheetController extends ResourceController
{
    protected $model = "\App\Models\UserTimesheet";

    public function getProjects()
    {
        $user = Auth::user();
        $end_date = date('Y-m-d');
        $start_date = date('Y-m-d', strtotime($end_date . '-45 days'));
        $data = ProjectService::projectsArray($user->id, $start_date, $end_date);
        return $this->successResponse($data['projects']);
    }

    public function index($query = null)
    {
        $superAdmin = null;
        $leadIdFlag = 0;
        $status = 'pending';
        $userObj = Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if (\Input::has('status')) {
            $status = \Input::get('status');
        }

        $superAdmin = ($userObj->role == "admin") ? true : false;

        if (!$query) {
            $query = $this->obj->buildQuery();
        }

        if (\Input::has('project')) {
            $projectAsStr = \Input::get('project');
            $arr = explode(',', trim($projectAsStr));
            $query = $query->where($arr[0], $arr[1]);
        }

        if (\Input::has('order_by_field') && \Input::has('order_by_type')) {
            $query = $query->orderBy(\Input::get('order_by_field'), \Input::get('order_by_type'));
        }
        if (\Input::has('lead_id')) {
            $leadIdFlag = \Input::get('lead_id');
        }

        if (!$superAdmin) {
            if ($leadIdFlag == '1') {
                $projectList = Project::where('project_manager_id', $userObj->id)->distinct()->select('id')->pluck('id')->toArray();
                $query = $query->whereIn('project_id', $projectList);
                $query = $query->where('status', $status);
            } else {
                $query = $query->where('user_id', $userObj->id);
            }
        } else {
            if ($leadIdFlag != '1') {
                $query = $query->where('user_id', $userObj->id);
            } else {
                $query = $query->where('status', $status);
            }

        }

        $relationsAsStr = \Input::has('relations') ? \Input::get('relations') : '';
        $relationsAsArr = array_map('trim', array_filter(explode(',', $relationsAsStr)));
        $query->with($relationsAsArr);

        $query = $query->orderBy('date', 'DESC');

        $pagination = \Input::has('pagination') ? \Input::get('pagination') : 1;

        if ($pagination == 0) {

            $sendData = $query->get()->toArray();

            return $this->successResponse($sendData);

        } else {

            $perPage = \Input::has('per_page') ? \Input::get('per_page') : $this->per_page;

            $total = $query->count();

            $paginator = $query->paginate($perPage);

            $result = $paginator->toArray();

            $perPage = $result['per_page'];
            $currentPage = $result['current_page'];

            $metaData = $this->formatMetaData($total, $perPage, $currentPage);
            $sendData = $result['data'];

            return $this->successResponse($sendData, $metaData);

        }
    }

    public function store()
    {

        $user = \Auth::user();
        $input = $this->getRequestBody();
        $response = [];
        $status = true;
        $timesheetId = null;

        if (!$input['date'] || !$input['projects'] || !$input['tasks'] || !$input['durations']) {
            return $this->failResponse("Invalid inputs");
        }
        $currentDate = date('Y-m-d');

        // check for user mothly timesheet status, if not apporved then only allowed

        $monthId = Month::getMonth($input['date']);

        if ($input['date'] > $currentDate) {
            return $this->failResponse("Entry for future dates is not allowed");
        }
        if ($monthId) {
            $monthlyTimesheet = UserMonthlyTimesheet::where('user_id', $user->id)->where('month_id', $monthId)->first();
            //$monthlyTimesheet->status;
            if ($monthlyTimesheet && $monthlyTimesheet->status == 'approved') {
                return $this->failResponse("Your timesheet is approved for the month. Timesheet Logging is locked");
            }

        }

        $date = $input['date'];
        // echo "$user->id";
        // echo "$date";
        // die;

        if (empty($date)) {
            return $this->failResponse('Date Not Found');

        }

        // $weekObj = Week::where('start_date', '<=', $date)->where('end_date', '>=', $date)->first();

        // if ($weekObj) {
        //     $user_timesheet_week = UserTimesheetWeek::where('user_id', $user->id)->where('week_id', $weekObj->id)->first();
        //     if ($user_timesheet_week && $user_timesheet_week->status === 'approved') {
        //         return $this->failResponse('Timesheet is already apporved for the week. Timesheet can\'t be logged');
        //     }
        //     if ($weekObj->is_locked && $weekObj->is_locked === 1) {
        //         return $this->failResponse('Week Is Already Locked for the date. Timesheet can\'t be logged');
        //     }
        // }

        $checkObj = ProjectResource::canApplyTimesheet($user->id, $date, $input['projects']);

        if ($checkObj['status'] == false) {
            return $this->failResponse($checkObj['message']);

        }

        $res = TimesheetService::saveMulipleTimesheet($input);

        if ($res['status']) {
            return $this->successResponse($res);
        } else {
            return $this->failResponse($res['message']);
        }
    }
    public function update($id)
    {
        $userObj = Auth::user();
        $input = $this->getRequestBody();
        $input = $input['data'];

        $date = $input['date'];

        if ($userObj->id != $input['user_id']) {
            return $this->failResponse('User is not Authorised');
        }

        $checkObj = ProjectResource::canApplyTimesheet($input['user_id'], $date);

        if ($checkObj['status'] == false) {
            return $this->failResponse($checkObj['message']);

        }
        $res = TimesheetService::updateRecord($input);
        if ($res['status']) {
            return $this->successResponse($res);
        } else {
            return $this->failResponse($res['message']);
        }

    }

    public function updateApproveDetails($id)
    {
        $userObj = Auth::user();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        $input = $this->getRequestBody();
        $input = $input['data'];
        $res = TimesheetService::updateApprovalRecord($input, $user->id);
        if ($res['status']) {
            return $this->successResponse($res);
        } else {
            return $this->failResponse($res['message']);
        }
    }

    public function getManagedProjects()
    {
        $user = Auth::user();
        $end_date = date('Y-m-d');
        $start_date = date('Y-m-d', strtotime($end_date . '-5 days'));
        $projects = Project::where('project_manager_id', $user->id)->get();
        return $this->successResponse($projects);
    }

    public function destroy($id)
    {
        $user = Auth::user();
        $res = TimesheetService::deleteUserTimesheet($id, $user->id);
        if ($res['status']) {
            return $this->successResponse('Record successfully deleted');
        } else {
            return $this->failResponse($res['message']);
        }
    }

    public function updateRejectDetails($id)
    {
        $user = Auth::user();

        $input = $this->getRequestBody();
        $input = $input['data'];
        $res = TimesheetService::updateRejectRecord($input, $user->id);
        if ($res['status']) {
            return $this->successResponse($res);
        } else {
            return $this->failResponse($res['message']);
        }
    }

    public function approve($userId, $monthId)
    {

        $loggedInUser = Auth::User();

        if (!$loggedInUser && !$loggedInUser->hasRole('team-lead')) {
            return $this->failResponse("Invalid Access");
        }

        $userObj = User::find($userId);
        $monthObj = Month::find($monthId);

        if (!$userObj || !$monthObj) {
            return $this->failResponse("Invalid UserId or monthId");
        }
        $userTimesheet = UserMonthlyTimesheet::where('user_id', $userId)->where('month_id', $monthId)->first();
        // try {

        // } catch (Exception $e) {

        // }
        if ($userTimesheet && $userTimesheet->status == 'approved') {
            return $this->failResponse("Timesheet is already approved for the month " . $monthObj->formatMonth());
        } elseif ($userTimesheet && $userTimesheet->status == 'pending') {
            $userTimesheet->status = 'approved';
            $userTimesheet->approved_id = $loggedInUser->id;
            $userTimesheet->save();

        } else {
            // create userTimesheet entry and appove
            $userTimesheet = new UserMonthlyTimesheet;
            $userTimesheet->user_id = $userId;
            $userTimesheet->month_id = $monthId;
            $userTimesheet->created_by = $userId;
            $userTimesheet->approved_id = $loggedInUser->id;
            $userTimesheet->status = 'approved';

            if (!$userTimesheet->save()) {
                return $this->failResponse("Unable to save the record");
            }

        }

        $input = $this->getRequestBody();
        $bonusRequest = null;
        if (isset($input['extra_hours_request']) && $input['extra_hours_request'] == true) {
            $input['status'] = 'approved';
            // apply for bonus requset for type : extra
            $bonusRequest = BonusService::applyBonusRequest($input);
            //return $this->successResponse($response);
        }
        $data = TimesheetReportService::getUserTimesheetReport($monthId, $userId);
        $data['bonus_request'] = $bonusRequest;
        return $this->successResponse($data);

    }
    public function getProjectByTimesheet()
    {
        $date = \Input::get('date');
        \Log::info('Date ' . $date);
        if (!Input::has('date')) {
            return $this->failResponse("Invalid data");
        }

        $loggedInUser = Auth::User();
        $userId = $loggedInUser->id;

        $projects = UserService::getTimesheetProjects($userId, $date);
        return $this->successResponse($projects);
    }
    public function getPendingTimesheet()
    {
        $parentUser = Auth::user();
        $data = [];
        if ($parentUser && ($parentUser->hasRole('reporting-manager') || $parentUser->hasRole('admin'))) {
            if ($parentUser->hasRole('admin') && $parentUser->role == 'admin') {
                $users = User::where('is_active', 1)->get();
            } else {
                $users = User::where('is_active', 1)->where('parent_id', $parentUser->id)->get();
            }

            $currentDate = date("Y-m-d");
            $endDate = $currentDate;
            $startDate = date('Y-m-d', strtotime("-65 days", strtotime($currentDate)));

            $userIds = [];
            if (count($users) > 0) {
                foreach ($users as $user) {
                    $userIds[] = $user->id;
                }
                $data = TimesheetReportService::getpendingRequests($userIds, $startDate, $endDate);
            }
        }
        return $this->successResponse(array_values($data));
    }
}
