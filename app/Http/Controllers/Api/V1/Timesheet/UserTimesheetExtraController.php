<?php

namespace App\Http\Controllers\Api\V1\Timesheet;

use App\Http\Controllers\Api\V1\ResourceController;
use App\Models\Admin\AdditionalWorkDaysBonus;
use App\Models\Appraisal\Appraisal;
use App\Models\Month;
use App\Models\User;
use App\Models\User\UserTimesheetExtra;
use App\Models\User\UserTimesheetWeek;
use Auth;
use Input;

class UserTimesheetExtraController extends ResourceController
{

    protected $model = "App\Models\User\UserTimesheetWeek";

    public function index($query = null)
    {

        if (!$query) {
            $query = $this->obj->buildQuery();
        }
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if (!($userObj->hasRole('admin') || $userObj->hasRole('reporting-manager'))) {
            return $this->failResponse("Invalid Access");
        }

        $userId = \Input::has('user_id') ? \Input::get('user_id') : 0;
        $monthId = \Input::has('month_id') ? \Input::get('month_id') : 0;

        $query = $query->where('user_id', $userId)->where('month_id', $monthId);
        $query = $query->orderBy('date', 'DESC');

        return parent::index($query);
    }
    public function storeBKP()
    {
        $authUser = Auth::user();
        if (!$authUser) {
            return $this->failResponse('Invalid Access');
        }
        if (!($authUser->hasRole('admin')) && !($authUser->hasRole('account')) && !($authUser->hasRole('reporting-manager'))) {
            return $this->failResponse('Unauthorized Access');
        }
        $input = $this->getRequestBody();
        $date = !empty($input['date']) ? $input['date'] : null;
        $userId = !empty($input['user_id']) ? $input['user_id'] : null;
        $weekId = !empty($input['week_id']) ? $input['week_id'] : null;
        $status = !empty($input['status']) ? $input['status'] : 0;
        $extraObj = UserTimesheetExtra::whereDate('date', $date)->where('user_id', $userId)->first();

        if ($extraObj && $extraObj->status == 'approved') {
            return $this->failResponse('Cannot add duplicate record for the same date');
        } elseif (!$extraObj || ($extraObj && $extraObj->status == 'rejected')) {
            $extraObj = new UserTimesheetExtra();
        }
        // $monthId = Month::getMonth($date);
        // $monthObj = Month::find($monthId);
        $weekObj = UserTimesheetWeek::where('week_id', '=', $weekId)->where('user_id', '=', $userId)->first();

        if (!$weekObj || $weekObj->status == 'approved') {
            return $this->failResponse('Timesheet for the week is already approved');
        }

        $extraObj->user_id = $input['user_id'];
        $extraObj->extra_hours = $input['extra_hours'];
        $extraObj->project_id = $input['project_id'];
        $extraObj->status = $status;
        //$extraObj->month_id = $monthId;
        $extraObj->week_id = $weekObj->week_id;
        $extraObj->date = $input['date'];
        $extraObj->user_id = $input['user_id'];
        $extraObj->created_by = $authUser->id;
        $extraObj->approved_by = $authUser->id;

        if (!$extraObj->save()) {
            return $this->failResponse('Unbale to save the extra hours');
        }

        if ($extraObj && $extraObj->status == 'approved') {
            $data = [];
            $data['user_id'] = $userId;
            $data['month_id'] = Month::getMonth($date);
            $data['id'] = 1;
            $data['date'] = $date;
            $data['reference_id'] = $extraObj->id;
            $data['reference_type'] = 'App\Models\User\UserTimesheetExtra';
            $data['type'] = 'additional';
            Bonus::saveApprovedBonus($data);

        }
        $userObj = User::find($authUser->id);
        $extraObj = UserTimesheetExtra::with('approver')->whereDate('date', $date)->where('user_id', $userId)->first();
        $userObj = User::find($input['user_id']);
        // echo " $userObj->name";
        // die;
        // apply for bonus request

        return $this->successResponse($extraObj);
    }
    public function destroy($id)
    {
        $userObj = Auth::User();

        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if (!$userObj->hasRole('admin') && !$userObj->hasRole('account') && !$userObj->hasRole('reporting-manager')) {
            return $this->failResponse('Unauthorized Access');

        }
        $obj = UserTimesheetExtra::find($id);
        if (!$obj) {
            return $this->failResponse('Invalid id ' . $id);
        }

        // if ($obj->month->bonus_check) {
        //     return $this->failResponse('Cannot delete the request, Bonus Request for the month ' . $obj->month->formatMonth() . ' is locked');
        // }

        $obj->delete();

        return $this->successResponse('Successfully deleted');

    }
    public function updateBonus($id)
    {
        $userObj = Auth::User();

        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if (!$userObj->hasRole('admin') && !$userObj->hasRole('account') && !$userObj->hasRole('reporting-manager')) {
            return $this->failResponse('Unauthorized Access');

        }
        $input = $this->getRequestBody();
        $AdditionalWorkDaysBonus = AdditionalWorkDaysBonus::find($input['bonus_id']);
        $AdditionalWorkDaysBonus->employee_id = $input['user_id'];
        $AdditionalWorkDaysBonus->type = $input['type'];
        $AdditionalWorkDaysBonus->reference_id = $input['project_id'];
        $AdditionalWorkDaysBonus->amount = $input['amount'];
        $AdditionalWorkDaysBonus->to_date = !empty(date_to_yyyymmdd($input['to_date'])) ? date_to_yyyymmdd($input['to_date']) : null;
        $AdditionalWorkDaysBonus->from_date = !empty(date_to_yyyymmdd($input['from_date'])) ? date_to_yyyymmdd($input['from_date']) : null;
        $AdditionalWorkDaysBonus->notes = !empty($input['notes']) ? $input['notes'] : null;
        $AdditionalWorkDaysBonus->created_by = $userObj->id;

        if (!$AdditionalWorkDaysBonus->save()) {
            return $this->failResponse('Unable to save contact');
        }
        $AdditionalWorkDaysBonusObj = AdditionalWorkDaysBonus::where('id', $AdditionalWorkDaysBonus->id)->with('user', 'reviewer')->first();
        return $this->successResponse($AdditionalWorkDaysBonusObj);
    }
    public function show($id)
    {
        $userObj = Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        $AdditionalWorkDaysBonusObj = AdditionalWorkDaysBonus::where('id', $id)->with('user', 'reviewer')->first();
        if ($AdditionalWorkDaysBonusObj) {
            return $this->successResponse($AdditionalWorkDaysBonusObj);
        } else {
            return $this->failResponse("No record");
        }
    }
    public function getTimesheet($monthId, $userId)
    {
        $userObj = User::find($userId);
        $monthObj = Month::find($monthId);

        if ($userObj) {
            $response['user_name'] = $userObj->name;
        }

        if ($monthObj) {
            $response['month'] = $monthObj->formatMonth();
            $response['month_id'] = $monthObj->id;
        }

        return $this->successResponse($response);
        //$response =
    }

    public function store()
    {

        $authUser = Auth::user();
        if (!$authUser) {
            return $this->failResponse('Invalid Access');
        }
        if (!($authUser->hasRole('admin')) && !($authUser->hasRole('account')) && !($authUser->hasRole('reporting-manager'))) {
            return $this->failResponse('Unauthorized Access');
        }

        $input = $this->getRequestBody();

        $data['date'] = !empty($input['date']) ? $input['date'] : '';
        $data['user_id'] = !empty($input['user_id']) ? $input['user_id'] : '';
        $data['project_id'] = !empty($input['project_id']) ? $input['project_id'] : '';
        $data['extra_hours'] = !empty($input['extra_hours']) ? $input['extra_hours'] : '';
        $data['status'] = !empty($input['status']) ? $input['status'] : '';
        //to be removed
        $data['type'] = 'extra';

        $userTimesheetExtra = UserTimesheetExtra::where('date', $data['date'])->where('user_id', $data['user_id'])->first();
        if ($userTimesheetExtra && $userTimesheetExtra->status == 'approved') {
            return $this->failResponse('Record already  approved!');
        }
        $data['amount'] = 0;
        if ($data['status'] && $data['status'] == 'approved') {
            $oneDaySalary = Appraisal::getBonusAmount($data['user_id'], $data['date']);
            $data['amount'] = round(($oneDaySalary / 8) * $data['extra_hours']);
        }

        $response = UserTimesheetExtra::saveExtra($data);

        if (!$response) {
            return $this->failResponse('Unable to save record in User Timesheet Extra');
        }
        return $this->successResponse($response['data']);
    }
}
