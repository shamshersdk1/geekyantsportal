<?php

namespace App\Http\Controllers\Api\V1\Timesheet;

use App\Http\Controllers\Api\V1\ResourceController;
use App\Models\Admin\AdditionalWorkDaysBonus;
use App\Models\Month;
use App\Models\User;
use App\Models\User\UserTimesheetExtra;
use App\Models\User\UserTimesheetWeek;
use App\Models\Week;
use App\Services\BonusService;
use App\Services\TimesheetReportService;
use Auth;
use Input;

class UserTimesheetWeekController extends ResourceController
{

 protected $model = "App\Models\User\UserTimesheetWeek";

 public function index($query = null)
 {

  if (!$query) {
   $query = $this->obj->buildQuery();
  }
  $userObj = \Auth::User();
  if (!$userObj) {
   return $this->failResponse("Invalid Access");
  }
  if (!($userObj->hasRole('admin') || $userObj->hasRole('reporting-manager'))) {
   return $this->failResponse("Invalid Access");
  }

  $userId  = \Input::has('user_id') ? \Input::get('user_id') : 0;
  $monthId = \Input::has('month_id') ? \Input::get('month_id') : 0;

  $query = $query->where('user_id', $userId)->where('month_id', $monthId);
  $query = $query->orderBy('date', 'DESC');

  return parent::index($query);
 }
 public function store()
 {
  $authUser = Auth::user();
  if (!$authUser) {
   return $this->failResponse('Invalid Access');
  }
  if (!($authUser->hasRole('admin')) && !($authUser->hasRole('account')) && !($authUser->hasRole('reporting-manager'))) {
   return $this->failResponse('Unauthorized Access');
  }
  $input    = $this->getRequestBody();
  $date     = !empty($input['date']) ? $input['date'] : null;
  $userId   = !empty($input['user_id']) ? $input['user_id'] : null;
  $extraObj = UserTimesheetExtra::whereDate('date', $date)->where('user_id', $userId)->first();

  if ($extraObj && $extraObj->status == 'approved') {
   return $this->failResponse('Cannot add duplicate record for the same date');
  } elseif (!$extraObj || ($extraObj && $extraObj->status == 'rejected')) {
   $extraObj = new UserTimesheetExtra();
  }

  $monthId  = Month::getMonth($date);
  $monthObj = Month::find($monthId);

  if (!$monthObj || $monthObj->bonus_check) {
   return $this->failResponse('Bonus Request for the month ' . $monthObj->formatMonth() . ' is locked');
  }

  $extraObj->user_id     = $input['user_id'];
  $extraObj->extra_hours = $input['extra_hours'];
  $extraObj->project_id  = $input['project_id'];
  $extraObj->month_id    = $monthId;
  $extraObj->date        = $input['date'];
  $extraObj->user_id     = $input['user_id'];
  $extraObj->created_by  = $authUser->id;

  if (!$extraObj->save()) {
   return $this->failResponse('Unbale to save the extra hours');
  }

  // apply for bonus request

  return $this->successResponse($extraObj);
 }
 public function destroy($id)
 {
  $userObj = Auth::User();

  if (!$userObj) {
   return $this->failResponse("Invalid Access");
  }
  if (!$userObj->hasRole('admin') && !$userObj->hasRole('account') && !$userObj->hasRole('reporting-manager')) {
   return $this->failResponse('Unauthorized Access');

  }
  $obj = UserTimesheetExtra::find($id);
  if (!$obj) {
   return $this->failResponse('Invalid id ' . $id);
  }

  if ($obj->month->bonus_check) {
   return $this->failResponse('Cannot delete the request, Bonus Request for the month ' . $obj->month->formatMonth() . ' is locked');
  }

  $obj->delete();

  return $this->successResponse('Successfully deleted');

 }
 public function updateBonus($id)
 {
  $userObj = Auth::User();

  if (!$userObj) {
   return $this->failResponse("Invalid Access");
  }
  if (!$userObj->hasRole('admin') && !$userObj->hasRole('account') && !$userObj->hasRole('reporting-manager')) {
   return $this->failResponse('Unauthorized Access');

  }
  $input                                 = $this->getRequestBody();
  $AdditionalWorkDaysBonus               = AdditionalWorkDaysBonus::find($input['bonus_id']);
  $AdditionalWorkDaysBonus->employee_id  = $input['user_id'];
  $AdditionalWorkDaysBonus->type         = $input['type'];
  $AdditionalWorkDaysBonus->reference_id = $input['project_id'];
  $AdditionalWorkDaysBonus->amount       = $input['amount'];
  $AdditionalWorkDaysBonus->to_date      = !empty(date_to_yyyymmdd($input['to_date'])) ? date_to_yyyymmdd($input['to_date']) : null;
  $AdditionalWorkDaysBonus->from_date    = !empty(date_to_yyyymmdd($input['from_date'])) ? date_to_yyyymmdd($input['from_date']) : null;
  $AdditionalWorkDaysBonus->notes        = !empty($input['notes']) ? $input['notes'] : null;
  $AdditionalWorkDaysBonus->created_by   = $userObj->id;

  if (!$AdditionalWorkDaysBonus->save()) {
   return $this->failResponse('Unable to save contact');
  }
  $AdditionalWorkDaysBonusObj = AdditionalWorkDaysBonus::where('id', $AdditionalWorkDaysBonus->id)->with('user', 'reviewer')->first();
  return $this->successResponse($AdditionalWorkDaysBonusObj);
 }
 public function show($id)
 {
  $userObj = Auth::User();
  if (!$userObj) {
   return $this->failResponse("Invalid Access");
  }
  $AdditionalWorkDaysBonusObj = AdditionalWorkDaysBonus::where('id', $id)->with('user', 'reviewer')->first();
  if ($AdditionalWorkDaysBonusObj) {
   return $this->successResponse($AdditionalWorkDaysBonusObj);
  } else {
   return $this->failResponse("No record");
  }
 }
 public function getTimesheet($weekId, $userId)
 {

  $userObj = User::find($userId);
  $weekObj = Week::find($weekId);

  if ($userObj) {
   $response['user_name'] = $userObj->name;
  }

  if ($weekObj) {
   $response['week']    = $weekObj->formatWeek();
   $response['week_id'] = $weekObj->id;
  }

  $response['extra_hour'] = UserTimesheetExtra::where('week_id', $weekId)->where('user_id', $userId)->where('status', 'approved')->sum('extra_hours');
  //$response['extra_hour'] =

  return $this->successResponse($response);
  //$response =
 }
 public function approve($userId, $weekId)
 {

  $loggedInUser = Auth::User();

  if (!$loggedInUser && !$loggedInUser->hasRole('team-lead')) {
   return $this->failResponse("Invalid Access");
  }

  $userObj = User::find($userId);
  $weekObj = Week::find($weekId);

  if (!$userObj || !$weekId) {
   return $this->failResponse("Invalid UserId or weekId");
  }
  $userTimesheet = UserTimesheetWeek::where('user_id', $userId)->where('week_id', $weekId)->first();

  $input = $this->getRequestBody();
  if ($userTimesheet && $userTimesheet->status == 'approved') {
   return $this->failResponse("Timesheet is already approved for the week " . $weekObj->formatWeek());
  } elseif ($userTimesheet && $userTimesheet->status == 'pending') {
   $userTimesheet->status      = 'approved';
   $userTimesheet->extra_hour  = !empty($input['hours']) ? $input['hours'] : 0;
   $userTimesheet->approved_by = $loggedInUser->id;
   $userTimesheet->save();

  } else {
   // create userTimesheet entry and appove
   $userTimesheet              = new UserTimesheetWeek;
   $userTimesheet->user_id     = $userId;
   $userTimesheet->week_id     = $weekId;
   $userTimesheet->extra_hour  = !empty($input['hours']) ? $input['hours'] : 0;
   $userTimesheet->approved_by = $loggedInUser->id;
   $userTimesheet->status      = 'approved';

   if (!$userTimesheet->save()) {
    return $this->failResponse("Unable to save the record");
   }

  }
  $bonusRequest = null;

  $input['status'] = 'approved';
  if ($input['hours'] > 0) {
   $bonusRequest = BonusService::applyBonusRequest($input);

   if ($bonusRequest) {
    $bonus = BonusService::saveToBonus($bonusRequest['result']->id, 0);

   }
  }
  // apply for bonus requset for type : extra

  \Log::info('bonusRequest' . json_encode($bonusRequest));

  UserTimesheetWeek::approveAllTimesheet($weekId, $userId);

  $data                  = TimesheetReportService::getUserWeeklyTimesheet($weekId, $userId);
  $data['bonus_request'] = $bonusRequest;
  return $this->successResponse($data);

 }
}
