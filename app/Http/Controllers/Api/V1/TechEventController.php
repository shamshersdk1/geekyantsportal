<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Admin\TechEvent;
use Illuminate\Http\Request;
use Validator;

class TechEventController extends ResourceController
{
    protected $model = "\App\Models\Admin\TechEvent";

    public function updateStatus(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->failResponse($validator->errors());
        }
        $id = $input['id'];
        $techEvent = TechEvent::updateStatus($id);
        
        if( $techEvent['status'] ) {
            return $this->successResponse($techEvent);
        } else {
            return $this->failResponse($techEvent['message']);
        }
    }
    
}
