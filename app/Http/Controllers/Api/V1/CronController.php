<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Admin\Cron;
use Illuminate\Http\Request;
use App\Models\Admin\ResourcePrice;
use App\Services\CronService\CronService;
use Validator;

class CronController extends ResourceController
{
    protected $model = "\App\Models\Admin\Cron";

    public function index($query = null)
    {
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        $reference_id = \Input::has('reference_id') ? \Input::get('reference_id') : 0;
        $job_handler = \Input::has('job_handler') ? \Input::get('job_handler') : 0;

        // \Log::info($reference_id.'         ' .$job_handler );
        // if ( $reference_id == 0 || $job_handler == 0 )
        // {
        //     return $this->failResponse("Missing input details");
        // }
        
        $sendData = CronService::getData($reference_id, $job_handler);
        return $this->successResponse($sendData['message']);
        
        
    }

    public function store()
    {
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->successResponse("Invalid Access");
        }

        $input = $this->getRequestBody();
        
        $result = CronService::saveData($input);
        
        return $this->successResponse('Successfully saved');
        
        
    }
    
    public function update($id)
    {
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        $input = $this->getRequestBody();
        $input = $input['params'];
        $result = CronService::updateData($id, $input);
        return $this->successResponse($result['message']);
    }

    public function destroy($id)
    {
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if( ResourcePrice::find($id)->delete() ) 
        {
            return $this->successResponse('Deleted successfully');
        }
        else {
            return $this->failResponse("Couldn't delete the record");
        }
    }
}
