<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\V1\ResourceController;
use App\Models\Admin\PurchaseOrderItem;
use PDF;
use App\Services\VendorServices;

class PurchaseOrderController extends ResourceController
{
    protected $model = "App\Models\Admin\PurchaseOrder";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($query = null)
    {

        if (!$query) {
            $query = $this->obj->buildQuery();
        }
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if (!$userObj->hasRole('admin') && !$userObj->hasRole('office-admin')) {
            return $this->failResponse('Unauthorized Access');
        }
        $query = $query->with('company','vendor', 'createdBy', 'approvedBy', 'purchaseItems');
        $query = $query->orderBy('id', 'DESC');
        return parent::index($query);
    }
    public function store()
    {
        $userObj = \Auth::User();
        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }

        if (!$userObj->hasRole('admin') && !$userObj->hasRole('office-admin')) {
            return $this->failResponse('Unauthorized Access');
        }

        $data = $this->getRequestBody();
        
        if(empty($data))
            return $this->failResponse('Empty data');

        $orderObj = new $this->model;
        $response = $this->model::saveData($data, $orderObj);
        return $response;

    }
    public function update($id)
    {
        $data = $this->getRequestBody();
        $orderObj = $this->model::find($id);
        $oldPurchaseItems = PurchaseOrderItem::where('purchase_order_id', $id)->delete();
        if (!$oldPurchaseItems) {
            $response = [
                'result' => "",
                'status' => false,
                'message' => $oldPurchaseItems->getErrors(),
                'errors' => [],
            ];
            return $response;
        }
        $response = $this->model::saveData($data, $orderObj);
        return $response;
    }
    public function show($id)
    {
        $userObj = \Auth::User();

        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if (!$userObj->hasRole('admin') && !$userObj->hasRole('office-admin')) {
            return $this->failResponse('Unauthorized Access');
        }

        $obj = $this->model::with('company','vendor', 'createdBy', 'approvedBy', 'purchaseItems')->find($id);
        if (!$obj) {
            return $this->failResponse("Invalid #" . $id);
        }
        return $this->successResponse($obj);
    }

    public function download($id)
    {
        $userObj = \Auth::User();

        if (!$userObj) {
            return $this->failResponse("Invalid Access");
        }
        if (!$userObj->hasRole('admin') && !$userObj->hasRole('office-admin')) {
            return $this->failResponse('Unauthorized Access');
        }

        $purchaseOrderObj = $this->model::find($id);
        if (!$purchaseOrderObj) {
            return Redirect::back()->withErrors(['Payslip not found.']);
        }
        $vendorContact = VendorServices::getPrimaryContact($purchaseOrderObj->vendor_id);
        
        $pdf = PDF::loadView('purchase-order.purchase-order-pdf', compact('purchaseOrderObj','vendorContact'));
        return $pdf->download('purchase-order.pdf');

    }
}
