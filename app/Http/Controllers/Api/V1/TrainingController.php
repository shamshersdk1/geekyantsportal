<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Admin\Training;

class TrainingController extends ResourceController
{
    protected $model = "\App\Models\Admin\Training";

    public function store()
    {
        $input = $this->getRequestBody();
        $res = Training::saveData($input['data']);
        if ($res['status']) {
            return $this->failResponse($res['message']);
        }
        return $this->successResponse($res);
    }

    public function show($id)
    {
        $query = Training::where('user_id', $id)->get();
        if (empty($query->toArray())) {
            return $this->failResponse("No record found");
        }
        return $this->successResponse($query);
    }

    public function destroy($id)
    {
        $trainingObj = Training::find($id);
        if (!$trainingObj->delete()) {
            return $this->failResponse("unable to delete");
        }
        return $this->successResponse('Successfully Deleted');
    }

}
