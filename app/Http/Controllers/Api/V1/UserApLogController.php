<?php

namespace App\Http\Controllers\Api\V1;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Admin\UserApLog;
use Redirect;
use Validator;

class UserApLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'username'=>'required',
            'ap'=>'required'
        ]);

        if($validator->fails()){
            return $this->failResponse($validator->errors());

        }
        $data=$request->all();

        echo"$request->all()";
        die;
        if( empty($data) )
        {
            return Redirect::back()->withInput()->withErrors(['Missing inputs']);
        }
            $userApLog=new UserApLog();
            $userApLog->username=$data['username'];
            $userApLog->datetime=$data['datetime'];
            $userApLog->ap=$data['ap'];
      
        if($userApLog->save()){
            return redirect('')->with('message', 'Successfully Added');
        }
        else {
            return redirect::back()->withErrors($userApLog->getErrors())->withInput();
        }

    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
