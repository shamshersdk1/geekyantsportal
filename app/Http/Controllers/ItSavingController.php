<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\ItSaving;
use App\Models\Month;
use App\Models\Admin\ItSavingOther;
use App\Models\Appraisal\AppraisalComponentType;
use App\Models\Transaction;
use App\Models\User;
use App\Models\Admin\FinancialYear;
use App\Services\Payroll\ITSavingService;
use Carbon\Carbon;

class ItSavingController extends Controller
{
    public function index()
    {
        $date = date('Y-m-d');
        $authUser = \Auth::user();
        $itSavings = ItSaving::where('user_id',$authUser->id)->orderBy('created_at','desc')->get();
        $currentFinancialYear = false;
        $financialYearObj = FinancialYear::whereDate('start_date','<=',$date)->whereDate('end_date','>=',$date)->first();
        if($financialYearObj){
            $currentFinancialYear = ItSaving::where('financial_year_id',$financialYearObj->id)->where('user_id',$authUser->id)->first();
        }
        return view('pages/it-saving/user/user-view', compact('itSavings','currentFinancialYear'));
    }

    public function show($id)
    {
        $authUser = \Auth::user();
        $ItSavingObj = ItSaving::find($id);
        if (!$ItSavingObj) {
            return redirect()->back()->withErrors("Record not found");
        }
        if($ItSavingObj->user_id != $authUser->id)
        {
            return redirect('/user/it-saving')->withErrors("Not Authorized")->withInput();
        }
        
        $template = trim($ItSavingObj->financialYear->it_saving_template);
        if ($ItSavingObj->financialYear->is_locked == 1) {
            $isLocked = true;
        } else {
            $isLocked = false;
        }
        $user = User::find($ItSavingObj->user_id);
        if ($ItSavingObj) {
            $otherData = ITSavingService::getItSavingOtherData($ItSavingObj->id);
            $ItSavingObj->other_multiple_investments = $otherData['other_multiple_investments'];
            $ItSavingObj->other_multiple_deductions = $otherData['other_multiple_deductions'];
        }

        $data = ITSavingService::getItSavingTransactionData($ItSavingObj->id);
        $vpfAmount = $data['vpfAmount'];
        $pfEmployeeAmount = $data['pfEmployeeAmount'];
        $userEditStatus = true;
        $financialYear = $ItSavingObj->financialYear;
        return view('pages/it-saving/template/' . $template, compact('userEditStatus','vpfAmount','pfEmployeeAmount','financialYear', 'ItSavingObj', 'user', 'isLocked'));
    }

    public function create()
    {
        $authUser = \Auth::user();
        $day_of_month = date('d');

        $month = date('m');
        
        $monthObj = Month::orderBy('year','DESC')->orderBy('month','DESC')->where('month', $month)->first();
        $financialYear = FinancialYear::find($monthObj->financial_year_id);
        if (!$financialYear) {
            return redirect()->back()->withErrors("No Record");
        }
        $template = '2019-2020';
        if ($financialYear) {
            $template = trim($financialYear->it_saving_template);
        }
        $user = User::find($authUser->id);
        $copyButton = true;
        $viewStatus = true;
        $isCreate = true;
        return view('pages/it-saving/template/' . $template, compact('isCreate','viewStatus','copyButton','vpfAmount','pfEmployeeAmount','financialYear', 'ItSavingObj', 'user', 'isLocked'));
    }

    public function copyFromPrevious($id)
    {
        $authUser = \Auth::user();
        $month = date('m');
        $monthObj = Month::orderBy('year','DESC')->orderBy('month','DESC')->where('month', $month)->first();
        $financialYear = FinancialYear::find($monthObj->financial_year_id);
        $ItSavingObj = ItSaving::where('user_id', $id)->orderBy('financial_year_id')->first();
        if(!$ItSavingObj){
            return redirect()->back()->withErrors("No Previous Record");
        }
        if($ItSavingObj->user_id != $authUser->id)
        {
            return redirect('/user/it-saving')->withErrors("Not Authorized")->withInput();
        }
        $viewStatus = true;
        $template = trim($ItSavingObj->financialYear->it_saving_template);
        $user = User::find($ItSavingObj->user_id);
        if ($ItSavingObj) {
            $otherData = ITSavingService::getItSavingOtherData($ItSavingObj->id);
            $ItSavingObj->other_multiple_investments = $otherData['other_multiple_investments'];
            $ItSavingObj->other_multiple_deductions = $otherData['other_multiple_deductions'];
        }

        $data = ITSavingService::getItSavingTransactionData($ItSavingObj->id);
        $vpfAmount = $data['vpfAmount'];
        $pfEmployeeAmount = $data['pfEmployeeAmount'];
        $isCreate = true;
        return view('pages/it-saving/template/' . $template, compact('isCreate','viewStatus','vpfAmount','pfEmployeeAmount','financialYear', 'ItSavingObj', 'user', 'isLocked'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $monthObj = Month::orderBy('year','DESC')->orderBy('month','DESC')->first();
        $data['financial_year_id'] = $monthObj->financial_year_id;
        if (!empty($data)) {
            if(isset($data['status']))
                $data['status'] = 'pending';
            else
                $data['status'] = 'completed';
            $obj = new ItSaving;
            $response = ItSaving::saveData($data, $obj);
            if (isset($response['status']) && $response['status']) {
                return redirect('/user/it-saving')->with('message', "Saved Successfully!");
            } else {
                return redirect()->back()->withErrors($response['message'])->withInput();
            }
        } else {
            return redirect()->back()->withErrors('Empty data sent!')->withInput();
        }
    }
    public function update($id, Request $request)
    {
        $data = $request->all();
        if (!empty($data)) {
            if(isset($data['status']))
                $data['status'] = 'pending';
            else
                $data['status'] = 'completed';
            $obj = ItSaving::find($id);
            $authUser = \Auth::user();
            if($obj->user_id != $authUser->id)
            {
                return redirect('/user/it-saving')->withErrors("Not Authorized")->withInput();
            }
            $response = ItSaving::saveData($data, $obj);
            if (isset($response['status']) && $response['status']) {
                return redirect('/user/it-saving/'.$id)->with('message', "Updated Successfully!");
            } else {
                return redirect()->back()->withErrors($response['message'])->withInput();
            }
        } else {
            return redirect()->back()->withErrors('Empty data sent!')->withInput();
        }

    }

    public function destroy($id)
    {
        $obj = ItSaving::find($id);
        if($obj->forceDelete())
        {
            return redirect('/user/it-saving/')->with('message', "Deleted!");
        }
        return redirect()->back()->withErrors('Unable to delete!')->withInput();
    }
}
