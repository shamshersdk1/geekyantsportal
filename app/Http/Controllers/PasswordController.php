<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Mail\PasswordResetMail;
use Hash;
use App\Models\User;
use App\Models\PasswordReset;
use App\Mail\ResetForgotPassword;
use App\Helpers\Sys;
use Redirect;
use App\Http\Requests;

class PasswordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // echo"hello";
        // die;
        return View("auth/password-reset");

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function  getVerificationError(){
        return redirect('admin')->with('message', 'Verification error');
    }

    public function getForgot()
    {
        return view('pages.dashboard.forget-password');
    }

    public function postForgot(Request $request)
    {
        // set the date and token for the user

        if($request->has('email')) {
            $user = User::where('email', $request->email)->first();
        $email = $request->email;

            if(!$user)
                return redirect('/password/reset')->withErrors(
                'This email does not belong to any user. Please signup first.');
        //get the information for the user and store it of password reset table with token
        $passwordReset = new PasswordReset;
        $passwordReset->email = $email;
        $passwordReset->created_at = date('Y-m-d H:i:s');
        $passwordReset->token = Sys::generateHash(50);
        $passwordReset->save();
        // $data['email'] =
        $data['name'] = $user->name;
        $data['email'] = $request->email;
        $data['token'] = $passwordReset->token;
        //send the mail to the user with template and redirect to the login page
        $mailObj = Mail::to($email)->send(new ResetForgotPassword($data));

            return redirect('/admin')->with('message','Password reset mail has been send on the email');


        }
        return redirect('password/reset')->withErrors('email required');

    }
    public function reset(Request $request,$token)
    {
      // echo "helloabc";
      // echo $token;
      // print_r($request->all());
      // die;
        // return View('auth/reset-forgot-password',['email' => "sachin@gsahusoft.com"]);
        if(isset($request->email) && isset($token)){

            $passwordObj = PasswordReset::where('email',$request->email)->where('token',$token)->first();

        }
        if(isset($passwordObj)){
            // $passwordObj->delete();
            return View('auth/reset-forgot-password',['email' => $request->email]);
        }
        else{

            return redirect('/admin')->withErrors("data Not found");
        }

    }
    public function resetPassword(Request $request)
    {

        if(isset($request->password) && isset($request->password_confirmation) && isset($request->email))
        {
            if($request->password == $request->password_confirmation){
                $resetPassword = User::where('email',$request->email)->first();
                $resetPassword->password = bcrypt($request->password);
                if($resetPassword->save()){
                    $deleteToken = PasswordReset::where('email',$request->email)->delete();
                    return redirect('/admin')->with('message','password has been reset');
                }
                else
                    return redirect('/admin')->withErrors($resetPassword->getErrors());
            }
            else
                return View('auth/reset-forgot-password',['email' => $request->email])->withErrors('password should be same');
        }
        else
             return View('auth/reset-forgot-password',['email' => $request->email]);



    }

}

