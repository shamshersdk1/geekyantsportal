<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Admin\SprintResource;
use App\Models\Admin\ProjectSprints;
use Auth;

class MySprintController extends Controller
{
	public function index() {
		$user = AUth::user();
		if($user){
			$userSprints = SprintResource::where('employee_id',$user->id)->join('project_sprints','sprint_resources.sprint_id','project_sprints.id')->get();
   			return View('pages/user/mySprints', ['userSprints' => $userSprints]);
		}
   	}
}
