<?php namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\BonusRequest;
use App\Models\Month;
use Auth;


class BonusRequestController extends Controller
{

    public function month()
    {
        $months = Month::orderBy('year', 'DESC')->orderBy('month', 'DESC')->get();
        return view('pages/admin/bonus-request/months', compact('months'));

    }
    public function monthBonuses($monthId)
    {
        $user = Auth::User();
        
        if(!($user->hasRole('admin') || $user->hasRole('reporting-manager')))
            return redirect('/admin/bonus-request')->withErrors('Invalid access!');

        $monthObj = Month::find($monthId);
        $monthName = date("M", mktime(0, 0, 0, $monthObj->month, 10)).' '.$monthObj->year;

        $bonuses = BonusRequest::where('month_id', $monthId)->orderBy('id', 'DESC')->get();
        return view('pages/admin/bonus-request/all', compact('monthId','monthName'));

    }
    public function monthBonusesPending($monthId)
    {
        $monthObj = Month::find($monthId);
        $monthName = date("M", mktime(0, 0, 0, $monthObj->month, 10)).' '.$monthObj->year;
        $bonuses = BonusRequest::where('month_id', $monthId)->where('status','pending')->orderBy('id', 'DESC')->get();
        return view('pages/admin/bonus-request/index', compact('monthId','monthName'));

    }
    public function show($id)
    {

        $months = Month::orderBy('id', 'DESC')->get();
        return view('pages/admin/bonus-request/months', compact('months'));
    }
    public function showBonusRequest($id)
    {
        $user = Auth::User();
        
        if(!$user->hasRole('admin'))
            return redirect('/admin/bonus-request')->withErrors('Invalid access.');
        
        $bonusRequest = BonusRequest::with('user', 'approvedBonus.approver', 'approvedBonus.referral')->find($id);
        return view('pages/admin/bonus-request/show', compact('bonusRequest'));

    }
}
