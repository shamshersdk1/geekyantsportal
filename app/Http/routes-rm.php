<?php
Route::group(['roles' => ['reporting-manager','human-resources','hr-associate']], function () {
    Route::get('user-report', 'UserReport\UserReportController@index');
    Route::get('user-report/{userId}', 'UserReport\UserReportController@show');
});
Route::group(['roles' => ['reporting-manager']], function () {
    //put all routes visible to user with any of the above roles
    // -------------------------------------------------------------------------------

    Route::get('my-team', 'ReportingManager\MyTeamController@index');
    Route::get('my-team/{userId}/edit', 'ReportingManager\MyTeamController@editUserDetail');
    Route::put('my-team/{userId}/save', 'ReportingManager\MyTeamController@saveUserLock');

    Route::resource('bonus/additional-work-days', 'AdditionalWorkDaysBonusController');
    Route::resource('bonus/on-site', 'OnSiteBonusController');
    Route::resource('bonus/tech-talk', 'TechTalkBonusController');
    Route::get('bonus/tech-talk/{id}/review', 'TechTalkBonusController@review');
    Route::post('bonus/tech-talk/{id}/review', 'TechTalkBonusController@saveUserReview');
    Route::post('bonus/tech-talk/{id}/save', 'TechTalkBonusController@updateStatus');
    Route::get('bonus/referral/{id}/approve', 'ReferralBonusController@approve');
    Route::get('bonus/referral/{id}/reject', 'ReferralBonusController@reject');
    Route::resource('bonus/referral', 'ReferralBonusController');
    Route::get('bonus/performance/{id}/approve', 'PerformanceBonusController@approve');
    Route::resource('bonus/performance', 'PerformanceBonusController');

    // ------------------------- Timesheet -------------------------
    Route::get('timesheet', 'TimesheetController@index');
    Route::get('timesheet/{id}/show', 'TimesheetController@show');
    Route::get('timesheet/{id}/notify', 'TimesheetController@sendSlackNotification');
    Route::get('timesheet/{id}', 'TimesheetController@approval');
    // --------------------------------------------------------------
    // ------------- Technology ----------------
    Route::resource('technology', 'TechnologyController');
    // -------------------------------------------

    // --------------------------------------------------------------
    // ------------- Loan Request ----------------
    Route::get('pending-loan-requests', 'LoanRequestController@getPendingRequests');
    Route::get('pending-loan-requests/{id}', 'LoanRequestController@showPendingRequest');
    // Route::put('pending-loan-requests/{id}', 'LoanRequestController@action');
    // -------------------------------------------

    // ------- Goal Management -------

    Route::resource('goals', 'GoalController');
    Route::get('goals/{id}/delete', 'GoalController@deleteGoal');
    Route::get('goal-assignment', 'GoalController@assignGoal');
    Route::get('goal-user-list', 'GoalController@userList');
    Route::get('goal-user-list/{user_id}', 'UserGoalController@userGoals');
    Route::get('goal-user-list/{user_id}/{goal_id}/view', 'GoalController@userGoalView');
    Route::get('goal-user-list/{user_id}/{user_goal_id}/edit', 'UserGoalController@editUserGoal');
    Route::get('goal-user-list/{user_id}/{user_goal_id}/review', 'UserGoalController@reviewUserGoal');
    //----------------------------------------------
    //Routes accessible by multiple roles
    //----------------------------------------------
    Route::get('timesheet-review', 'ReportingManager\TimesheetReviewController@showRequests');
    Route::get('seating-arrangement', 'FloorController@showRM');

    // Route::get('timesheet-review', 'ReportingManager\TimesheetReviewController@showWeeks');
    // Route::get('timesheet-review/{weekId}', 'ReportingManager\TimesheetReviewController@showUsers');
    // Route::get('timesheet-review/{weekId}/{userId}', 'ReportingManager\TimesheetReviewController@showWeekyUserReport');
    // Route::get('timesheet-review/{weekId}/{userId}/review', 'ReportingManager\TimesheetReviewController@reviewWeekyUserReport');
    // Route::post('timesheet-review/timesheet-review/{weekId}', 'ReportingManager\TimesheetReviewController@updateUser');
    // Route::post('week-lock/{weekId}', 'ReportingManager\TimesheetReviewController@lockWeek');
    Route::group(['roles' => ['team-lead']], function () {

        Route::get('review-report', 'AdminReviewReport@index');
        Route::get('mis-review-report', 'AdminReviewReport@MisReportMonth');
        Route::get('mis-review-report/{monthId}', 'AdminReviewReport@MisReport');
        Route::get('mis-review-report/{monthId}/{userId}', 'AdminReviewReport@UserReportMIS');
        Route::get('mis-review-report/{monthId}/{userId}/review', 'AdminReviewReport@userReportMISReview');
        Route::get('mis-review-report-user/{monthId}', 'AdminReviewReport@MISReports');

        /*RM leaves */
        Route::get('reporting-manager-leaves', 'ReportingManagerLeaveController@index');
        Route::get('reporting-manager-leaves/{id}', 'ReportingManagerLeaveController@getInfo');
        Route::post('reporting-manager-leaves/{id}', 'ReportingManagerLeaveController@update');
        Route::put('reporting-manager-leaves/reject/{id}', 'ReportingManagerLeaveController@reject');
        Route::put('reporting-manager-leaves/approve/{id}', 'ReportingManagerLeaveController@update');
        Route::post('project-manager-leaves/{id}', 'ReportingManagerLeaveController@updateProjectManager');
        Route::put('project-manager-leaves/reject/{id}', 'ReportingManagerLeaveController@rejectProjectManager');
        Route::resource('feedback/question', 'FeedbackQuestionController');
        Route::get('feedback/review', 'FeedbackReviewController@getList');
        Route::get('feedback/review/{user_id}/{month_id}', 'FeedbackReviewController@getFeedbackListForUser');

        /** Temp Projects */

    });

    Route::group(['roles' => ['account']], function () {
        /* Bonuses */
        Route::resource('bonuses', 'BonusController');
        Route::get('bonus-ledger', 'BonusController@ledger');

        /* Loan Requests */
        Route::get('approved-loan-requests', 'LoanRequestController@getApprovedRequests');
        Route::get('approved-loan-requests/{id}', 'LoanRequestController@showApprovedRequest');
        Route::get('payslip-data', 'PayslipDataController@index');
        Route::get('payslip-data/data-master/{id}', 'PayslipDataController@getPayslipMonthMaster');
        Route::get('payslip-data/data-master/{id}/lock', 'PayslipDataController@lock');
        Route::get('payslip-data/data-item/{id}', 'PayslipDataController@getPayslipMonthItem');
        Route::post('payslip-data/data-master/', 'PayslipDataController@savePayslipMonthMaster');
        Route::post('payslip-data/data-item/', 'PayslipDataController@savePayslipMonthItem');
    });
});
