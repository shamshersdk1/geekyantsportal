<?php namespace App\Services;

use Validator;
use App\Models\Admin\Project;
use App\Models\Admin\Calendar;
use App\Models\Admin\Leave;
use App\Models\Admin\ProjectNotes;
use App\Models\Admin\Timelog;

use App\Models\Admin\ProjectSprints;

use Input;
use Redirect;
use App\Models\User;
use Auth;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use Config;
use Exception;
use Mail;
use App\Mail\PasswordResetMail;

class TimelogService {
	//return the sum of minutes of the user($userId) 
	public static function consumeHours($userId, $date)
	{

		$consumeHours = Timelog::with('project')->where('user_id',$userId)->where('current_date',$date)->sum('minutes');

		if($consumeHours){
			 
			$consumeHours = round($consumeHours/60 , 2);
			return $consumeHours;
		}
		return false;
	}
	

	
		




}

