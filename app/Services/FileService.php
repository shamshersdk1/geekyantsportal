<?php namespace App\Services;

use App\Models\Admin\File;
use Redirect;
use Validator;

class FileService
{

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public static function getFileName($type)
    {

        do {
            $x = rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9);
            $filename = $x . "-" . time() . "." . $type;
        } while (file_exists($filename));

        return $filename;
    }

    public static function getFolder()
    {
        $path = date('Y/m/d/');
        return $path;
    }

    public static function uploadFile($file, $filename, $reference_id, $reference_type, $originalName, $prefix)
    {

        $folder = self::getFolder();
        $destination = public_path('/uploads/') . $prefix . '/' . $folder;
        $file->move($destination, $filename);

        // CODE FOR FILE MODEL ENTRY
        if (!$reference_id || !$reference_type) {
            return Redirect::back()->withInput()->withErrors(['References are missing']);
        }

        $file_object = new File();
        $file_object->name = $originalName;
        $file_object->path = '/uploads/' . $prefix . '/' . $folder . $filename;
        $file_object->reference_type = $reference_type;
        $file_object->reference_id = $reference_id;
        if ($file_object->save()) {
            return $file_object;
        } else {
            return Redirect::back()->withInput()->withErrors(['error while saving']);
        }
    }
    public static function uploadFileInStorage($file, $filename, $reference_id, $reference_type, $originalName, $prefix )
    {
                
        $folder = self::getFolder();
        $destination = storage_path('uploads/').$prefix.'/'.$folder;
        $file->move($destination,$filename);
        
        // CODE FOR FILE MODEL ENTRY
        if (!$reference_id || !$reference_type  ) {
            return Redirect::back()->withInput()->withErrors(['References are missing']);
        }

        $file_object = new File();
        $file_object->name = $originalName;
        $file_object->path = storage_path('uploads/').$prefix.'/'.$folder.$filename;
        $file_object->reference_type = $reference_type;
        $file_object->reference_id = $reference_id;
        if ($file_object->save())
            return $file_object;
        else         
            return Redirect::back()->withInput()->withErrors(['error while saving']);
    }      

    public static function uploadFileInDB($content, $original_name, $type, $reference_id, $reference_type)
    {
        // CODE FOR FILE MODEL ENTRY
        if (!$reference_id || !$reference_type  ) {
            return Redirect::back()->withInput()->withErrors(['References are missing']);
        }

        return File::createRecord($content, $original_name, $type, $reference_id, $reference_type);
    }      

}
