<?php namespace App\Services;

use App\Jobs\Slack\SlackReminder\NotifyBdManagers;
use App\Models\Admin\Project;
use App\Models\Admin\ProjectResource;
use App\Models\Admin\ProjectSlackChannel;
use App\Models\Admin\UserSetting;
use App\Models\Leave\Leave;
use App\Models\NonworkingCalendar;
use App\Models\Timesheet\UserTimesheetLock;
use App\Models\TmpTimesheet;
use App\Models\User;
use App\Models\UserTimesheet;
use App\Services\ProjectResourceService;
use App\Services\SlackService\SlackApiServices;
use App\Services\SlackService\SlackMessageService\SlackMessageService;
use App\Services\TimesheetReportService;
use Config;
use DB;
use Exception;
use View;

class TimesheetService
{
    public static function checkMessageFormat($message)
    {
        $timePos = strrpos($message, '[');
        $timeLastPos = strrpos($message, ']');

        if (empty($timePos) || empty($timeLastPos)) {
            return false;
        }
        return true;
    }

    public static function saveToTempTable($channelId, $userId, $message)
    {
        $status = true;
        $return_message = '';

        $task = self::getTaskFromMessage($message);
        $duration = self::getDurationFromMessage($message);
        $projectId = null;
        $projectSlack = ProjectSlackChannel::where('slack_id', $channelId)->first();
        if (!empty($projectSlack)) {
            $projectId = $projectSlack->project_id;
        }
        $date = date('Y-m-d');

        $res = TmpTimesheet::saveData($userId, $projectId, $channelId, $date, $duration, $task);
        if ($res['status']) {
            $return_message = $res['message'];
        } else {
            $status = false;
            $return_message = $res['message'];
        }

        $response['status'] = $status;
        $response['message'] = $return_message;

        return $response;

    }

    public static function sendSingleResponse($tempTableId)
    {
        $tempTableObj = TmpTimesheet::find($tempTableId);
        if (!empty($tempTableObj)) {
            $dates = self::getDateOptions();

            $options = [
                [
                    "text" => "Today",
                    "value" => $dates[0] . '_' . $tempTableId,
                ],
                [
                    "text" => "Yesterday",
                    "value" => $dates[1] . '_' . $tempTableId,
                ],
                [
                    "text" => date_in_view($dates[2]),
                    "value" => $dates[2] . '_' . $tempTableId,
                ],
                [
                    "text" => date_in_view($dates[3]),
                    "value" => $dates[3] . '_' . $tempTableId,
                ],
                [
                    "text" => date_in_view($dates[4]),
                    "value" => $dates[4] . '_' . $tempTableId,
                ],
                [
                    "text" => date_in_view($dates[5]),
                    "value" => $dates[5] . '_' . $tempTableId,
                ],
                [
                    "text" => date_in_view($dates[6]),
                    "value" => $dates[6] . '_' . $tempTableId,
                ],
            ];
            for ($i = 5; $i < 46; $i++) {
                $options[] = [
                    "text" => date_in_view($dates[$i]),
                    "value" => $dates[$i] . '_' . $tempTableId,
                ];
            }
            $userName = $tempTableObj->user->name;
            $url = config('app.geekyants_portal_url') . "/admin/new-timesheet/history";
            if (empty($tempTableObj->project->name)) {
                $res = SlackApiServices::getChannelName($tempTableObj->channel_id);
                if ($res['status']) {
                    $projectName = $res['data'];

                }
            } else {
                $projectName = $tempTableObj->project->name;
            }

            $task = $tempTableObj->task;
            $duration = $tempTableObj->duration;
            $message = View::make('slack.timesheet.timesheet-single-response', ['userName' => $userName, 'projectName' => $projectName, 'task' => $task, 'duration' => $duration])->render();
            $actions = [
                "name" => "date_list",
                "value" => $tempTableId,
                "text" => "Pick a date",
                "type" => "select",
                "options" => $options,
                "selected_options" => [
                    [
                        "text" => "Today",
                        "value" => $dates[0] . '_' . $tempTableId,
                    ],
                ],
            ];
            $attachment = [
                [
                    "text" => "When was your work done?",
                    "fallback" => "Goto the portal to edit the timesheet",
                    "attachment_type" => "default",
                    "callback_id" => "timesheet_action",
                    "actions" => [$actions,
                        ["name" => "close", "text" => "Confirm", "type" => "button", "value" => $tempTableId, "style" => "primary"],
                        ["name" => "edit", "text" => "Edit", "type" => "button", "style" => "default", "url" => $url],
                        ["name" => "discard", "text" => "Discard", "type" => "button", "value" => $tempTableId, "style" => "danger"],
                    ],
                ],
            ];

            return ["response_type" => "ephemeral", "text" => $message, "attachments" => $attachment];

            // CREATE RESPONSE OBJECT
        }

    }

    public static function sendMulipleResponse($channelId, $userName, $slackUserId)
    {
        $log_url = config('app.geekyants_portal_url') . "/admin/new-timesheet";
        $view_url = config('app.geekyants_portal_url') . "/admin/new-timesheet/history";
        $userName = '@' . $userName;
        $message = View::make('slack.timesheet.timesheet-multi-response', ['userName' => $userName])->render();
        $actions = [
            ["text" => "I want to log", "type" => "button", "url" => $log_url],
            ["text" => "View Timesheet", "type" => "button", "url" => $view_url],
        ];
        $attachment = [
            [
                "text" => "Use the below actions:",
                "fallback" => "Goto the portal to edit the timesheet",
                "attachment_type" => "default",
                "callback_id" => "timesheet_action",
                "actions" => $actions,
            ],
        ];
        return ["response_type" => "ephemeral", "text" => $message, "attachments" => $attachment];
    }

    public static function getTaskFromMessage($message)
    {
        $task = '';
        $arr = explode(' ', trim($message));
        unset($arr[0]);
        unset($arr[1]);
        $task = implode(" ", $arr);
        return $task;
    }

    public static function getDurationFromMessage($message)
    {
        $duration = '';
        $arr = explode(' ', trim($message));
        $duration = trim($arr[1], "[]");
        return $duration;
    }

    public static function getDateOptions()
    {
        $date_array = [];
        $date_array[] = date('Y-m-d');
        for ($i = 1; $i < 46; $i++) {
            $dateObj = date('Y-m-d', strtotime("-$i days"));
            $date_array[] = $dateObj;
        }
        return $date_array;
    }

    public static function updateTempRecordDate($tempTableId, $date)
    {
        $status = true;
        $message = '';

        $tempTableObj = TmpTimesheet::find($tempTableId);
        $tempTableObj->date = $date;
        if (!$tempTableObj->save()) {
            $status = false;
            $message = 'Failed to update the date';
        }

        $response['status'] = $status;
        $response['message'] = $message;

        return $response;
    }

    public static function storeTimesheetDetails($tempTableId)
    {
        $tempTableObj = TmpTimesheet::find($tempTableId);
        if (empty($tempTableObj)) {
            // Enter details in access log table
        } else {
            if ($tempTableObj->project_id) {
                $assigned = ProjectResourceService::isUserAllocated($tempTableObj->project_id, $tempTableObj->user_id);
                if ($assigned) {
                    self::saveTimesheet($tempTableObj->id);
                } else {
                    self::assignUserToProject($tempTableObj->project_id, $tempTableObj->user_id);
                    self::saveTimesheet($tempTableObj->id);
                }
            } else {
                $notifyFlag = self::shouldNotifyStatus($tempTableObj->channel_id);
                if ($notifyFlag) {
                    self::sendNotificationToBdManagers($tempTableObj);
                }
            }

        }

    }

    public static function saveTimesheet($tempTableId)
    {
        $tempTableObj = TmpTimesheet::find($tempTableId);
        if (!empty($tempTableObj)) {
            $timesheetObj = UserTimesheet::enterUserTimesheet($tempTableObj->user_id, $tempTableObj->project_id, $tempTableObj->date,
                $tempTableObj->duration, $tempTableObj->task);
            if ($timesheetObj['status']) {
                self::deleteTmpTimesheet($tempTableObj->id);
                //self::createApprovalRecord($timesheetObj['message']);
            }
        }
    }
    public static function deleteUserTimesheet($timesheetId, $user_id)
    {
        $status = true;
        $message = '';

        $timesheetObj = UserTimesheet::find($timesheetId);

        if ($timesheetObj->user_id == $user_id) {
            if (!$timesheetObj->delete()) {
                $status = false;
                $message = 'Error occurred while deleting the record';
            }
        } else {
            $status = false;
            $message = 'Unauthorized';
        }
        $response['status'] = $status;
        $response['message'] = $message;

        return $response;
    }

    public static function assignUserToProject($project_id, $user_id)
    {
        $start_date = date('Y-m-d');
        $end_date = null;
        $res = ProjectResourceService::assignUserToProject($user_id, $project_id, $start_date, $end_date);

    }

    public static function sendNotificationToBdManagers($tempTableObj)
    {
        $res = SlackApiServices::getChannelName($tempTableObj->channel_id);
        if ($res['status']) {
            $channelName = $res['data'];
            $channelId = $tempTableObj->channel_id;
            $job = (new NotifyBdManagers($channelId, $channelName))->onQueue('notify-bd-managers');
            dispatch($job);
        }
    }

    public static function saveMulipleTimesheet($input)
    {
        $status = true;
        $message = '';

        $user = \Auth::user();
        $date = $input['date'];

        foreach ($input['projects'] as $index => $project) {
            if ($project != null && $input['tasks'][$index] != null && $input['durations'][$index] != null) {
                $timesheetObj = UserTimesheet::enterUserTimesheet($user->id, $project['id'], $date, $input['durations'][$index], $input['tasks'][$index]);
                //self::createApprovalRecord($timesheetObj['message']);
                $message = $timesheetObj['message'];
            }
        }

        $response['status'] = $status;
        $response['message'] = $message;

        return $response;
    }

    public static function updateRecord($input)
    {
        $status = true;
        $message = '';

        $userTimesheetObj = UserTimesheet::find($input['id']);

        if (empty($userTimesheetObj)) {
            $status = false;
            $message = 'No record found';
        } else {
            $isApproved = self::isUserTimesheetApproved($userTimesheetObj->id);
            if ($isApproved) {
                $status = false;
                $message = 'The timesheet is already approved';
            } else {
                $userTimesheetObj->duration = $input['duration'];
                $userTimesheetObj->task = $input['task'];
                if (!$userTimesheetObj->save()) {
                    $status = false;
                    $message = 'Could not update the record';
                }
            }
        }

        $response['status'] = $status;
        $response['message'] = $message;

        return $response;
    }

    public static function isUserTimesheetApproved($id)
    {
        $userTimesheetObj = UserTimesheet::find($id);

        if ($userTimesheetObj) {
            $status = $userTimesheetObj->status;
            if ($status == 'approved') {
                return true;
            }
        }

        return false;
    }

    public static function leadTimesheetDetailsDateWise($teamLeadID, $date)
    {
        $result = [];
        $in_progress = 'in_progress';
        $free = 'free';

        $rawData = DB::select("SELECT distinct p.project_name,p.id , pr.user_id, sum(ut.duration) as duration
                                FROM
                                    projects p,
                                    project_resources pr,
                                    user_timesheets ut
                                WHERE
                                    p.id = pr.project_id AND
                                    pr.user_id = ut.user_id AND
                                    ut.project_id = pr.project_id AND
                                    ut.date = :date AND
                                    p.project_manager_id = :teamLeadId AND
                                    ut.deleted_at IS NULL AND
                                    p.status in (:in_progress,:free)
                                GROUP BY
                                    p.project_name,p.id, pr.user_id", ['date' => $date, 'teamLeadId' => $teamLeadID, 'in_progress' => $in_progress, 'free' => $free]
        );

        $previousId = '';
        $currentRow = [];
        $totalHours = 0;
        $result = [];
        foreach ($rawData as $row) {
            $currentId = $row->id;
            if ($currentId == $previousId) {
                $userObj = User::find($row->user_id);
                if (!empty($userObj)) {
                    $users[] = $userObj->name;
                    $totalHours = $totalHours + $row->duration;
                    $currentRow['totalHours'] = $totalHours;
                }
                $previousId = $currentId;
            } else {
                if ($previousId != '') {
                    $currentRow['users'] = $users;
                    $result[] = $currentRow;
                }
                $previousId = $currentId;
                $totalHours = 0;
                $users = [];
                $currentRow = [];
                $users = [];
                $currentRow['name'] = $row->project_name;
                $userObj = User::find($row->user_id);
                if (!empty($userObj)) {
                    $users[] = $userObj->name;
                    $totalHours = $totalHours + $row->duration;
                }
                $currentRow['totalHours'] = $totalHours;

            }
        }
        if (!empty($currentRow)) {
            $currentRow['users'] = $users;
            $result[] = $currentRow;
        }
        return $result;
    }

    public static function getUserHours($projectId, $userId, $date)
    {
        $hours = 0;
        $hours = UserTimesheet::where('project_id', $projectId)->where('user_id', $userId)->where('date', $date)->sum('duration');
        return $hours;
    }

    public static function updateApprovalRecord($input, $id)
    {
        $status = true;
        $message = '';

        $approver = \Auth::user();
        $userTimesheetObj = UserTimesheet::find($input['id']);

        $review_date = date('Y/m/d h:i:sa');

        $approvalComments = $input['review']['approval_comments'];
        $approvedDuration = $input['review']['approved_duration'];
        $userTimesheetId = $input['id'];
        $userTimesheetObj->review_id = $id;
        $userTimesheetObj->review_at = $review_date;

        $userTimesheetObj = UserTimesheet::find($input['id']);
        $userTimesheetObj->status = 'approved';
        $userTimesheetObj->reviewed_by = $id;
        if (!$userTimesheetObj->save()) {
            $status = false;
            $message = $userTimesheetObj->getErrors();
        }

        $response['status'] = $status;
        $response['message'] = $message;

        return $response;

        // if (!empty($userTimesheetObj->review)) {
        //     $res = UserTimesheetReview::updateData($userTimesheetObj->review->id, $approver->id, $approvedDuration, $approvalComments);
        //     if ($res['status']) {
        //         $userTimesheetObj->status = 'approved';
        //         $userTimesheetObj->save();
        //     }
        // } else {
        //     $res = UserTimesheetReview::saveData($userTimesheetId, $approver->id, $approvedDuration, $approvalComments);
        //     if ($res['status']) {
        //         $userTimesheetObj->status = 'approved';
        //         $userTimesheetObj->save();
        //     }
        // }

    }
    public static function requestAction($tmpTimesheetId, $action)
    {
        $status = true;
        $message = '';

        if ($action == 'close') {
            self::storeTimesheetDetails($tmpTimesheetId);
            $message = 'Timesheet details saved for today';
        } else {
            self::deleteTmpTimesheet($tmpTimesheetId);
            $message = 'The timesheet detail is discarded';
        }

        $response['status'] = $status;
        $response['message'] = $message;

        return $response;
    }
    public static function deleteTmpTimesheet($tmpTimesheetId)
    {
        $tempTableObj = TmpTimesheet::find($tmpTimesheetId);
        if (!empty($tempTableObj)) {
            $tempTableObj->delete();
        }
    }

    public static function updateTmpTimesheet($id, $duration, $task)
    {
        $status = true;
        $message = '';

        $tempTableObj = TmpTimesheet::find($id);
        if (empty($tempTableObj)) {
            $status = true;
            $message = 'No Record found';
        } else {
            $tempTableObj->duration = $duration;
            $tempTableObj->task = $task;
            if (!$tempTableObj->save()) {
                $status = false;
                $message = $tempTableObj->getErrors();
            }
        }
        $response['status'] = $status;
        $response['message'] = $message;

        return $response;
    }

    public static function isChannelPresent($channel_id)
    {
        $tmpTimesheetObj = TmpTimesheet::where('channel_id', $channel_id)->first();
        if (!empty($tmpTimesheetObj)) {
            return true;
        } else {
            return false;
        }
    }

    public static function updateTmpTimesheetProject($tmpTimesheetId, $project_id)
    {
        $tmpTimesheetObj = TmpTimesheet::find($tmpTimesheetId);
        $tmpTimesheetObj->project_id = $project_id;
        $tmpTimesheetObj->save();
    }
    // public static function updateUsersTimesheetLock()
    // {

    //     $response = [];

    //     DB::table('user_timesheet_locks')->update(['is_processed' => 0]);

    //     $users = User::where('is_active', 1)->orderBy('employee_id', 'asc')->get();

    //     if (!$users || empty($users) || !isset($users)) {
    //         $response['status'] = false;
    //         $response['message'] = 'Users not found';
    //     }

    //     $currDate = date('Y-m-d');

    //     foreach ($users as $user) {

    //         $date = self::getPrevWorkingDays($user->id, $currDate);

    //         $projectObj = ProjectResource::where('user_id', $user->id)->where('start_date', '<=', $date)->first();

    //         if ($projectObj && !empty($projectObj) && isset($projectObj)) {
    //             $timesheetLockObj = self::autoApproveAndAddLock($user->id, $date);

    //             if ($timesheetLockObj['status'] == true) {
    //                 $obj = UserTimesheetLock::where('user_id', $user->id)->where('date', $date)->first();
    //                 $obj->is_processed = 1;
    //                 $obj->comment = $timesheetLockObj['message'];
    //                 $obj->save();
    //             }
    //         }
    //     }

    //     $response['status'] = true;
    //     $response['message'] = 'Users timesheet Lock updated';
    //     return $response;
    // }

    public static function updateUsersTimesheetLock()
    {

        $response = [];

        DB::table('user_timesheet_locks')->update(['is_processed' => 0]);

        $users = User::where('is_active', 1)->orderBy('employee_id', 'asc')->get();
        if (!$users || empty($users) || !isset($users)) {
            $response['status'] = false;
            $response['message'] = 'Users not found';
        }

        $currDate = date('Y-m-d');

        foreach ($users as $user) {

            $userSettingObj = UserSetting::where('user_id', $user->id)->where('key', 'timesheet-required')->first();

            if (!$userSettingObj || $userSettingObj->value == 1) {
                $date = self::getPrevWorkingDays($user->id, $currDate);

                $projectObj = null;
                $projects = ProjectResource::where('user_id', $user->id)->where('start_date', '<=', $date)->get();

                foreach ($projects as $project) {
                    if (!$project->end_date || $project->end_date > $date) {
                        $projectObj = $project;
                    }
                }
                if ($projectObj) {
                    $timesheetLockObj = self::autoApproveAndAddLock($user->id, $date);

                    if (isset($timesheetLockObj['status']) && $timesheetLockObj['status'] == true) {

                        $obj = UserTimesheetLock::where('user_id', $user->id)->where('date', $date)->first();
                        if ($obj) {
                            $obj->is_processed = 1;
                            $obj->comment = $timesheetLockObj['message'];
                            if ($obj->save()) {
                                $approveObj = self::approvePreviousLeaves($user->id, $date);
                            }
                        }
                    } else if (isset($timesheetLockObj['status']) && $timesheetLockObj['status'] == false) {

                        $obj = UserTimesheetLock::where('user_id', $user->id)->where('date', $date)->first();
                        $obj->is_processed = 0;
                        $obj->comment = $timesheetLockObj['message'];
                        $obj->save();
                    }
                } else {
                    $userTimesheetObj = UserTimesheetLock::where('user_id', $user->id)->first();

                    if ($userTimesheetObj) {
                        $userTimesheetObj->delete();
                    }
                }
            }
            // $response['status'] = true;
            // $response['message'] = "User timesheet lock updated";

            // return $response;
        }
    }
    public static function autoApproveAndAddLock($userId, $date)
    {
        /*
        1. auto lock the timesheet
        2. auto approves the timesheet for lock date
         */
        $user = User::find($userId);
        $userLockObj = UserTimesheetLock::where('user_id', $userId)->first();
        $response = [];

        DB::beginTransaction();
        try {
            if ($userLockObj) {
                $timesheetLockObj = $userLockObj;
                if ($timesheetLockObj->billing_date && $timesheetLockObj->billing_date > $date) {
                    $date = $timesheetLockObj->billing_date;
                }
            } else {
                $timesheetLockObj = new UserTimesheetLock();
                $timesheetLockObj->user_id = $user->id;
            }

            $timesheetLockObj->date = $date;

            if ($timesheetLockObj->save()) {

                $projectObj = ProjectResource::where('user_id', $user->id)->where('start_date', '<=', $date)->get();

                $totalHours = TimesheetReportService::getUserTotalHour($date, $date, $userId);

                if (isset($totalHours) && $totalHours[0]->total_hour == 0) {
                    //create one entry for most critical
                    $counter = 0;
                    $hours = 0;
                    foreach ($projectObj as $resource) {

                        $counter = $counter + 1;

                        if ($resource->end_date == null || $resource->end_date > $date) {

                            $userTimesheetObj = new UserTimesheet();

                            $userTimesheetObj->user_id = $userId;
                            $userTimesheetObj->project_id = $resource->project_id;
                            $userTimesheetObj->date = $date;
                            if ($counter == count($projectObj)) {
                                $userTimesheetObj->duration = 8 - $hours;
                            } else {
                                $userTimesheetObj->duration = floor(8 / count($projectObj));
                                $hours = $hours + $userTimesheetObj->duration;
                            }

                            $userTimesheetObj->task = 'TIMESHEET IS AUTO APPROVED BY THE SYSTEM FOR 8 HOURS';
                            $userTimesheetObj->comment = 'TIMESHEET IS AUTO APPROVED BY THE SYSTEM FOR 8 HOURS';
                            $userTimesheetObj->status = 'approved';

                            if ($userTimesheetObj->save()) {
                                $response['status'] = true;
                                $response['message'] = 'User\'s Timesheet Lock updated and Timesheet Log added';
                            } else {
                                throw new Exception('User\'s Timesheet not updated');
                            }
                        }
                    }

                } else if (!empty($totalHours[0]->total_hour) && $totalHours[0]->total_hour < 9) {

                    $userTimesheetObjs = UserTimesheet::where('user_id', $userId)->where('status', 'pending')->where('date', $date)->get();

                    if ($userTimesheetObjs && !empty($userTimesheetObjs) && isset($userTimesheetObjs) && count($userTimesheetObjs) > 0) {

                        foreach ($userTimesheetObjs as $userTimesheetObj) {
                            $userTimesheetObj->status = "approved";

                            if (!$userTimesheetObj->save()) {
                                throw new Exception('User\'s Timesheet not updated');
                            }

                            $response['status'] = true;
                            $response['message'] = 'User\'s Timesheet Lock updated and Timesheet Log added';
                        }
                    } else {
                        $response['status'] = true;
                        $response['message'] = 'User\'s Timesheet Lock updated';
                    }
                } else {
                    $response['status'] = true;
                    $response['message'] = 'User\'s Timesheet Lock updated';
                }

                DB::commit();
            } else {
                throw new Exception("Unable to save user timesheet lock");
            }
        } catch (Exception $e) {

            DB::rollback();
            $response['status'] = false;
            $response['message'] = $e->getMessage();
        }
        return $response;
    }

    public static function checkWorkingDate($date, $userId)
    {

        if (!(NonworkingCalendar::is_holiday($date)) && !(NonworkingCalendar::isWeekend($date))) {

            $leaveObj = Leave::where('user_id', $userId)->whereDate('start_date', '<=', $date)->whereDate('end_date', '>=', $date)->where('status', 'approved')->first();

            if (!$leaveObj) {
                return true;
            }
        }

        return false;
    }

    public static function getPrevWorkingDays($userId, $date, $days = 14)
    {
        $checkDate = $date;

        $totalDays = 0;
        while ($totalDays != $days) {
            $checkDate = date('Y-m-d', strtotime('-1 day', strtotime($checkDate)));

            if (!(NonworkingCalendar::is_holiday($checkDate)) && !(NonworkingCalendar::isWeekend($checkDate))) {

                $leaveObj = Leave::where('user_id', $userId)->whereDate('start_date', '<=', $checkDate)->whereDate('end_date', '>=', $checkDate)->where('status', 'approved')->first(); //->where('end_date', '>=', $checkDate)->first();//where('status', 'approved')->first();//->where('end_date', '>=', $checkDate->toDateString());//->where('status', 'approved')->first();

                if (!$leaveObj) {
                    $totalDays++;
                }
            }
        }
        return $checkDate;
    }
    public static function sendUserSlackNotification($user_id, $date)
    {
        $date = self::getPrevWorkingDays($user_id, $date, 4);
        if (!$date) {
            return false;
        }
        $data['message'] = "Your timesheet will be auto locked tonight till " . date_in_view($date) . ". Please log your timesheet.";
        $timesheetCheck = UserSetting::where('user_id', $user_id)->where('key', 'timesheet-required')->where('value', '1')->first();
        if (!$timesheetCheck) {
            return false;
        }
        $duration = UserTimesheet::where('user_id', $user_id)->whereDate('date', $date)->sum('duration');
        if ($duration < 8) {
            SlackMessageService::sendMessageToUser($user_id, $data['message']);
        }
    }

    public static function approvePreviousLeaves($userId, $lockDate)
    {
        $response = [];
        $response['status'] = true;
        $response['message'] = '';
        $lastDate = null;
        $dateArray = [];
        $minDate = null;

        $user = User::find($userId);

        $sql = 'SELECT MIN(date) AS "date" FROM user_timesheets WHERE user_id=? and status="pending" ';
        $lastDate = DB::select($sql, [$userId]);

        if ($lastDate && !empty($lastDate) && isset($lastDate)) {
            if (!empty($lastDate[0]->date)) {
                $minDate = $lastDate[0]->date;
            }

        }

        if (!$minDate || ($minDate > $lockDate) || !$lockDate) {
            \Log::info('No Pending Timesheet found before Lock Date for ' . $user->name);
        }

        if ($minDate && $lockDate) {
            $dateArray = UserTimesheet::where('user_id', $userId)->whereDate('date', '>=', $minDate)->whereDate('date', '<', $lockDate)->get()->pluck('date');
        }

        foreach ($dateArray as $date) {
            $approveObj = self::approveTimesheet($userId, $date);
            if ($approveObj['status'] == false) {
                \Log::info('Error in approving' . $user->name . 'timesheet for date ->' . $date);
            }
        }

        return $response;
    }

    public static function approveTimesheet($userId, $date)
    {
        $user = User::find($userId);
        $response = [];
        $response['status'] = true;

        if (!$user) {
            $response['status'] = false;
            $response['message'] = 'User not found';
            return $response;
        }

        $totalHours = TimesheetReportService::getUserTotalHour($date, $date, $userId);

        if (!empty($totalHours[0]->total_hour) && $totalHours[0]->total_hour < 9) {

            $userTimesheetObjs = UserTimesheet::where('user_id', $userId)->where('status', 'pending')->where('date', $date)->get();

            if ($userTimesheetObjs && !empty($userTimesheetObjs) && isset($userTimesheetObjs) && count($userTimesheetObjs) > 0) {

                foreach ($userTimesheetObjs as $userTimesheetObj) {
                    $userTimesheetObj->status = "approved";

                    if (!$userTimesheetObj->save()) {
                        \Log::info($user->name+'\'s Timesheet is not approved for date' . $date);
                    }
                }
            }
        }

        return $response;
    }
}
