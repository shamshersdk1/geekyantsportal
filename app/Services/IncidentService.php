<?php namespace App\Services;

use Validator;
use App\Models\User;
use App\Models\Admin\Department;
use App\Models\Admin\Incident;
use App\Models\Admin\IncidentUserAccess;
use App\Models\Comment;
use App\Events\Incident\IncidentTransferred;
use App\Events\Incident\IncidentShared;
use Auth;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use Config;
use View;
use App\Jobs\MessagingApp\PostMessageChannelJob;
use App\Jobs\MessagingApp\PostMessageUserJob;
use App\Services\SlackService\SlackMessageService\SlackMessageService;


class IncidentService {

    public static function defaultAssignUser($incidentId)
    {
        $incidentObj = Incident::find($incidentId);
        if ( $incidentObj )
        {
          $incidentObj->assigned_to = !empty($incidentObj->department->first_contact) ? $incidentObj->department->first_contact : '' ;
          $incidentObj->save();
        }
    }
  
    public static function saveSharedUsers($incident_id, $user_ids)
    {
      if ( !empty($user_ids) )
      {
        IncidentUserAccess::where('incident_id',$incident_id)->delete();
        foreach ( $user_ids as $user_id )
        {
          $incidentUserAccessObj = new IncidentUserAccess();
          $incidentUserAccessObj->incident_id = $incident_id;
          $incidentUserAccessObj->user_id = $user_id;
          $incidentUserAccessObj->save();
        }
      }
      return true;
    }
    public static function updateSharedUsers($incident_id, $user_ids)
    {
      if ( !empty($user_ids) )
      {
        IncidentUserAccess::where('incident_id',$incident_id)->delete();
        foreach ( $user_ids as $user_id )
        {
          $incidentUserAccessObj = new IncidentUserAccess();
          $incidentUserAccessObj->incident_id = $incident_id;
          $incidentUserAccessObj->user_id = $user_id;
          $incidentUserAccessObj->save();
        }
        event(new IncidentShared($incident_id));
      }
      return true;
    }
    public static function checkUserAccess( $incident_id, $user_id )
    {
      $access = [];
      $shared_user_flag = false;
      $read_only_flag = true;
      $show_incident_flag = false;

      $incidentObj = Incident::find($incident_id);
      $userObj = User::find($user_id);

      // user has raised the incident
      if ( $userObj->id == $incidentObj->raised_by )
      {
        $read_only_flag = false;
        $show_incident_flag = true;
      }
      // user is departmenrt head or first contact
      elseif ( $userObj->id == $incidentObj->department->head || $userObj->id ==  $incidentObj->department->first_contact )
      {
        $read_only_flag = false;
        $show_incident_flag = true;
      }
      // user is assgined_to incident
      elseif ( $userObj->id == $incidentObj->assigned_to  )
      {
        $read_only_flag = false;
        $show_incident_flag = true;
      }
      // user is admin, office-admin, system-admin, hr , account
      elseif ( $userObj->role == 'admin' || $userObj->hasRole('office-admin') || $userObj->hasRole('account') 
          || $userObj->hasRole('system-admin') || $userObj->hasRole('human-resource') )
      {
        $read_only_flag = false;
        $show_incident_flag = true;
      }
      else {
        $shared_users = $incidentObj->incidentUsers;
        foreach( $shared_users as $shared_user )
        {
          if( $shared_user->user_id == $userObj->id )
          {
            $read_only_flag = true;
            $show_incident_flag = true;
          }
        }
      }
      // incident user access table
     
      
      $access['read_only_flag'] = $read_only_flag;
      $access['show_incident_flag'] = $show_incident_flag;
      return $access;
    }
    
    public static function transferIncidentDepartment($incident_id, $department_id)
    { 
      $status = true;
      $message = '';

      $incidentObj = Incident::find($incident_id);
      $departmentObj = Department::find($department_id);
      if ( $incidentObj && $departmentObj )
      {
        $prevDepartment = $incidentObj->department->type;
        $incidentObj->department_id = $departmentObj->id;
        $incidentObj->assigned_to = !empty($departmentObj->first_contact) ? $departmentObj->first_contact : null ;
        if($incidentObj->save()) {
          $newDepartment = $incidentObj->department->type;
          event(new IncidentTransferred($incidentObj->id));
          $actionDetail = ['text' => "From ".$prevDepartment. " to ".$departmentObj->type];
          $incidentObj->addActivity('transfer_department', $actionDetail);
        }
      }
      else {
        $status = false;
        $message = 'Incident or department not found';
      }

      $res['status'] = $status;
      $res['message'] = $message;

      return $res;

    }

    public static function transferIncidentUser($incident_id, $user_id)
    {
      $status = true;
      $message = '';

      $incidentObj = Incident::find($incident_id);
      $userObj = User::find($user_id);

      if ( $incidentObj && $userObj)
      {
        $prevUserName = $incidentObj->assignee->name; 
        $incidentObj->assigned_to = $user_id;
        if($incidentObj->save()) {
          $newUserName = $incidentObj->assignee->name; 
          event(new IncidentTransferred($incidentObj->id));
          $actionDetail = ['text' => "From ".$prevUserName. " to ".$userObj->name];
          $incidentObj->addActivity('transfer_user', $actionDetail);
        }
      }
      else {
        $status = false;
        $message = 'Incident not found';
      }

      $res['status'] = $status;
      $res['message'] = $message;

      return $res;
    }

    public static function incidentCreated($incident_id)
    {
      $incidentObj = Incident::find($incident_id);
      if ( $incidentObj )
      {
        $template_path = 'messaging-app.slack.incident.incident-created';
        self::sendNotificationOnChannel($incident_id, $template_path);
        $userIds = self::findAllUsersToNotify($incident_id);
        foreach( $userIds as $user_id ) {
          self::sendNotificationUser($incident_id, $user_id, $template_path); 
        }
      }
    }

    public static function getTemplate($incidentId) {
      
      $attachment = [];
      $incidentObj = Incident::find($incidentId);

      if(!$incidentObj) {
        return false;
      }

      $attachment['title'] = $incidentObj->title;
      $attachment['title_link'] = "admin/incident/".$incidentObj->id;
      $attachment['ts'] = strtotime($incidentObj->created_at);
      $attachment['fallback'] = $incidentObj->subject;
      $attachment['pretext'] = $incidentObj->subject;
      $attachment['text'] = $incidentObj->description;
      $assignedTo = $incidentObj->assignee->name;
      $fields = [
              [
                  "title" =>  "Assigned To",
                  "value" =>  $assignedTo,
                  "short" =>  true
              ],
              [
                  "title" => "Department",
                  "value" => $incidentObj->department->type,
                  "short" => true
              ]
          ];
      $attachment['fields'] = $fields;

      return $attachment;
    }


    public static function incidentClosed($incident_id)
    {
      $incidentObj = Incident::find($incident_id);
      if ( $incidentObj )
      {
        $template_path = 'messaging-app.slack.incident.incident-closed';
        self::sendNotificationOnChannel($incident_id, $template_path);
        $userIds = self::findAllUsersToNotify($incident_id);
        foreach( $userIds as $user_id )
        {
          self::sendNotificationUser($incident_id, $user_id, $template_path); 
        }
      }
    }

    public static function incidentReopened($incident_id)
    {
      $incidentObj = Incident::find($incident_id);
      if ( $incidentObj )
      {
        $template_path = 'messaging-app.slack.incident.incident-reopened';
        self::sendNotificationOnChannel($incident_id, $template_path);
        $userIds = self::findAllUsersToNotify($incident_id);
        foreach( $userIds as $user_id )
        {
          self::sendNotificationUser($incident_id, $user_id, $template_path); 
        }
      }
    }

    public static function incidentTransferred($incident_id)
    {
      $incidentObj = Incident::find($incident_id);
      if ( $incidentObj )
      {
        $template_path = 'messaging-app.slack.incident.incident-transferred';
        self::sendNotificationOnChannel($incident_id, $template_path);
        $userIds = self::findAllUsersToNotify($incident_id);
        foreach( $userIds as $user_id )
        {
          self::sendNotificationUser($incident_id, $user_id, $template_path); 
        }
      }
    }

    public static function incidentShared($incident_id)
    {
      $incidentObj = Incident::find($incident_id);
      if ( $incidentObj )
      {
        $template_path = 'messaging-app.slack.incident.incident-shared';
        self::sendNotificationOnChannel($incident_id, $template_path);
        $userIds = self::findAllUsersToNotify($incident_id);
        foreach( $userIds as $user_id )
        {
          self::sendNotificationUser($incident_id, $user_id, $template_path); 
        }
      }
    }

    public static function incidentVerifiedClosed($incident_id)
    {
      $incidentObj = Incident::find($incident_id);
      if ( $incidentObj )
      {
        $template_path = 'messaging-app.slack.incident.incident-verified-closed';
        self::sendNotificationOnChannel($incident_id, $template_path);
        $userIds = self::findAllUsersToNotify($incident_id);
        foreach( $userIds as $user_id )
        {
          self::sendNotificationUser($incident_id, $user_id, $template_path); 
        }
      }
    }

    public static function notifyCommentAdded( $commentId )
    {
      $commentObj = Comment::find($commentId);
      if ( $commentObj )
      {
        $incident_id = $commentObj->commentable_id;

        $template_path = 'messaging-app.slack.incident.incident-comment-added';
        $attachments = self::getTemplate($incident_id);

        if(isset($attachments['fields'])) {
          $field = [
                  "title" =>  "Comment(s)",
                  "value" =>  $commentObj->message,
                  "short" =>  false
              ];
          $attachments['fields'][] = $field;
        }

        self::sendNotificationOnChannel($incident_id, $template_path, $attachments);
        // $userIds = self::findAllUsersToNotify($incident_id);
        // foreach( $userIds as $user_id )
        // {
        //   self::sendNotificationUser($incident_id, $user_id, $template_path, $attachments); 
        // }
      }
    }

    public static function findAllUsersToNotify($incident_id)
    {
      $userIds = [];
      $incidentObj = Incident::find($incident_id);
      if ( $incidentObj )
      {
        $userIds[] = $incidentObj->assigned_to;
        if ( $incidentObj->assigned_to != $incidentObj->raised_by )
        {
          $userIds[] = $incidentObj->raised_by;
        }
        foreach( $incidentObj->incidentUsers as $incidentUser )
        {
          if ( $incidentUser->user_id != $incidentObj->assigned_to && $incidentUser->user_id != $incidentObj->raised_by )
          {
            $userIds[] = $incidentUser->user_id;
          }
        } 
      }
      return $userIds;

    }

    public static function sendNotificationOnChannel($incident_id, $template_path, $attachments = [])
    {
      $channelId = \Config::get('messaging-app.incident_channel');
      $incidentObj = Incident::find($incident_id);
      
      if(!$incidentObj)
            return false;

      $template = View::make($template_path, compact('incidentObj'))->render();

      if(empty($attachments))
        $attachments = self::getTemplate($incident_id);
      $job = (new PostMessageChannelJob($channelId, $template, $attachments));
      dispatch($job);
    }

    public static function sendNotificationUser( $incident_id, $user_id, $template_path, $attachments =[] )
    {

      $incidentObj = Incident::find($incident_id);
      if(!$incidentObj)
            return false;

      $url = config('app.geekyants_portal_url')."/admin/incident/".$incidentObj->id;
      $message = View::make($template_path, ['incidentObj' => $incidentObj, 'url' => $url])->render();
      $actions = [["text" => "View Incident", "type" => "button", "url" => $url]];
      
      //$attachment = [["text" => "Select an action", "fallback" => "Goto the portal to check the incident", "attachment_type" => "default", "callback_id" => "incident_action_user", "actions" => $actions ]];
      if(empty($attachments))
        $attachments = self::getTemplate($incident_id);

      $job = (new PostMessageUserJob($user_id, $message, $attachments));
      dispatch($job);
    
    }

    public static function changeIncidentStatus( $incident_id )
    {
      $incidentObj = Incident::find($incident_id);
      if(!$incidentObj)
            return false;
      if ( $incidentObj->status == 'open' )
      {
        $incidentObj->status = 'in_progress';
        $incidentObj->save();
      } else {
        return;
      }
      
    }
}