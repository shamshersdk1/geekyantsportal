<?php

namespace App\Services\GoogleCalendarService;

use App\Models\Admin\Calendar;
use App\Models\CalendarEvent;
use Carbon\Carbon;
use Exception;
use Spatie\GoogleCalendar\Event;
use \Datetime;

class GoogleCalendarService
{
    public static function saveCalendarEvent($event_name, $start_date_time, $end_date_time, $emails_array, $reference_id, $reference_type)
    {
        $result = ['status' => true, 'message' => ""];
        try {
            $calendar_event = new CalendarEvent();
            $calendar_event->calendar_event_id = null;
            $calendar_event->name = $event_name;
            $calendar_event->start_date_time = $start_date_time;
            $calendar_event->end_date_time = $end_date_time;
            $calendar_event->reference_id = $reference_id;
            $calendar_event->reference_type = $reference_type;
            $calendar_event->status = 'pending';
            if (is_null($emails_array)) {
                $calendar_event->invites = null;
            } else {
                $calendar_event->invites = json_encode($emails_array);
            }
            $calendar_event->response = '';
            $calendar_event->save();

            $event = new Event;
            $event->name = $event_name;
            $event->startDateTime = Carbon::parse($start_date_time, 'Asia/Kolkata');
            $event->endDateTime = Carbon::parse($end_date_time, 'Asia/Kolkata');

            if (!is_null($emails_array)) {
                if (is_array($emails_array)) {
                    foreach ($emails_array as $email) {
                        $event->addAttendee(['email' => $email]);
                    }
                } else {
                    $event->addAttendee(['email' => $emails_array]);
                }
            }
            $response = $event->save();
            if ($response) {
                $calendar_event->calendar_event_id = $response->id;
                $calendar_event->status = $response->status;
                $calendar_event->response = json_encode($response);
                $calendar_event->save();
            }

            $result['message'] = $response->id;
        } catch (Exception $e) {
            \Log::info('saveCalendarEvent Exception ' . json_encode($e->getMessage()));
            $result = ['status' => false, 'message' => $e->getMessage()];
        }
        return $result;
    }

    public static function getEvents()
    {
        $events = Event::get();
        return $events;
    }

    public static function findEvent($id)
    {
        $event = Event::find($id);
        return $event;
    }

    public static function cancelEventById($id)
    {
        $response = ['status' => false, 'message' => ""];
        $event = Event::find($id);
        if ($event) {
            $event->status = "cancelled";
            $event->save();
            $response = ['status' => true, 'message' => "Event cancelled successfully"];
        } else {
            $response = ['status' => false, 'message' => "Event not found"];
        }
        return $response;
    }

    public static function deleteEvent($id)
    {
        $event = Event::find($id);
        if (!empty($event)) {
            $event->delete();
        }
    }

    public static function invite($id, $emails_array)
    {
        $response = ['status' => false, 'message' => ""];
        try {
            $event = Event::find($id);
            $calendar_event = CalendarEvent::where('calendar_event_id', $id)->first();
            if ($event && $calendar_event) {
                $array = json_decode($calendar_event->invites);
                if (is_array($emails_array)) {
                    foreach ($emails_array as $email) {
                        $event->addAttendee(['email' => $email]);
                        $array[] = $email;
                    }
                } else {
                    $event->addAttendee(['email' => $emails_array]);
                    $array[] = $emails_array;
                }
                $calendar_event->invites = json_encode($array);
                $calendar_event->save();
                $event->save();

                $response = ['status' => true, 'message' => "Invite Sent"];
            } else {
                $response['message'] = "Event not found";
            }
        } catch (Exception $e) {
            $response['message'] = $e->getMessage();
        }
        return $response;
    }

    public static function filter($start_date_time, $end_date_time, $name = null)
    {
        $events = Event::get();
        $filtered_events = [];
        $start_date_time = Carbon::parse($start_date_time);
        $end_date_time = Carbon::parse($end_date_time);
        foreach ($events as $event) {
            if (($event->startDateTime <= $end_date_time) || ($event->endDateTime >= $start_date_time)) {
                $filtered_events[] = $event;
            }
        }
        $result = $filtered_events;
        if (!is_null($name) && count($filtered_events) > 0) {
            $result = [];
            foreach ($filtered_events as $event) {
                if (($event->name == $name)) {
                    $result[] = $event;
                }
            }
        }
        return $result;
    }
    public static function getWorkingDays($start_date, $end_date)
    {
        $workingDays = 0;
        $startTimestamp = strtotime($start_date);
        $endTimestamp = strtotime($end_date);
        for ($i = $startTimestamp; $i <= $endTimestamp; $i = $i + (60 * 60 * 24)) {
            if (date("N", $i) <= 5 && !Calendar::whereDate('date', date('Y-m-d', $i))->count()) {
                $workingDays = $workingDays + 1;
            }
        }
        return $workingDays - 1;
    }
    public static function lastWorkingDay($date)
    {
        $date = new DateTime($date);
        $date->modify('last day of this month');
        $lastDateOfMonth = $date->format('Y-m-d H:i:s');

        while (Calendar::whereDate('date', $lastDateOfMonth)->count() > 0 || date('N', strtotime($lastDateOfMonth)) > 5) {
            $lastDateOfMonth = Carbon::parse($lastDateOfMonth)->subDays(1);
        }
        return $lastDateOfMonth;
    }
    public static function firstWorkingDay($date)
    {
        $date = new DateTime($date);
        $date->modify('first day of next month');
        $firstDateOfMonth = $date->format('Y-m-d H:i:s');

        while (Calendar::whereDate('date', $firstDateOfMonth)->count() > 0 || date('N', strtotime($firstDateOfMonth)) > 5) {
            $firstDateOfMonth = Carbon::parse($firstDateOfMonth)->addDays(1);
        }
        return $firstDateOfMonth;
    }
    public static function nextWorkingDay($date)
    {
        while (Calendar::whereDate('date', $date)->count() > 0 || date('N', strtotime($date)) > 5) {
            $date = Carbon::parse($date)->addDays(1);
        }
        return $date;
    }
}
