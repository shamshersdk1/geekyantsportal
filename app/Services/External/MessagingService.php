<?php
namespace App\Services\External;

use App\Jobs\MessagingApp\PostMessageChannelJob;

class MessagingService
{
    public static function sendNotificationOnChannel($template, $data = [])
    {
        $channelId = $data['channel_id'];
        if (!empty($data)) {
            $attachments = self::getTemplate($data);
        }
        $job = (new PostMessageChannelJob($channelId, $template, $attachments));
        dispatch($job);
    }
    public static function getTemplate($data)
    {

        $attachment = [];

        $attachment['url'] = \Config::get('app.geekyants_leads_url');
        $attachment['title'] = !empty($data['name']) ? $data['name'] : null;
        $attachment['title_link'] = "lead/" . $data['lead_id'];
        $attachment['ts'] = !empty($data['created_at']) ? strtotime($data['created_at']) : null;
        $attachment['fallback'] = $data['name'] . " " . "(" . $data['email'] . ")";
        $attachment['pretext'] = !empty($data['name']) ? $data['name'] : null;
        $attachment['footer'] = "GeekyAnts Leads";

        $fields = [
            [
                "title" => "Company",
                "value" => !empty($data['company']) ? $data['company'] : null,
                "short" => true,
            ],
        ];
        if (!empty($data['skype'])) {
            $skype = [
                "title" => "Skype",
                "value" => $data['skype'],
                "short" => true,
            ];
            array_push($fields, $skype);
        }
        if (!empty($data['requirement'])) {
            $requirement = [
                "title" => "Requirement",
                "value" => $data['requirement'],
                "short" => true,
            ];
            array_push($fields, $requirement);
        }
        $attachment['fields'] = $fields;

        return $attachment;
    }

}
