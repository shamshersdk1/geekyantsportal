<?php namespace App\Services;

use App\Events\Project\AccountManagerAssigned;
use App\Events\Project\BdManagerAssigned;
use App\Events\Project\ProjectManagerAssigned;
use App\Events\Timesheet\SlackChannelLinked;
use App\Models\Admin\Calendar;
use App\Models\Admin\Project;
use App\Models\Admin\ProjectLead;
use App\Models\Admin\ProjectResource;
use App\Models\Admin\ProjectSlackChannel;
use App\Models\Admin\ProjectSprints;
use App\Models\Timesheet;
use App\Models\TimesheetDetail;
use App\Models\User;
use App\Models\UserRole;
use App\Services\LeaveService;
use App\Services\ProjectService;
use App\Services\TimesheetService;
use DB;

class ProjectService
{

    public static function allocatedHours($userId, $date)
    {

        $allocatedHours = ProjectSprints::where('project_sprints.start_date', '<=', $date)->where('project_sprints.end_date', '>=', $date)->join('sprint_resources', 'project_sprints.id', '=', 'sprint_resources.sprint_id')->where('sprint_resources.employee_id', $userId)->leftJoin('projects', 'project_sprints.project_id', '=', 'projects.id')->get();

        if (count($allocatedHours)) {
            return $allocatedHours;
        }
        return false;
    }
    public static function ProjectDetails($projectId)
    {

        $allocatedHours = Project::with('projectSprints.sprintResource.user')->where('id', $projectId)->first();
        if (count($allocatedHours) > 0) {
            return $allocatedHours;
        } else {
            return false;
        }

        return false;
    }
    public static function freeHours($userId, $date)
    {

        $allocatedHours = ProjectSprints::where('project_sprints.start_date', '<=', $date)->where('project_sprints.end_date', '>=', $date)->join('sprint_resources', 'project_sprints.id', '=', 'sprint_resources.sprint_id')->where('sprint_resources.employee_id', $userId)->leftJoin('projects', 'project_sprints.project_id', '=', 'projects.id')->sum('sprint_resources.hours');
        // $leave = LeaveService::()
        $globalLeave = LeaveService::globalLeaveCount($date);
        $personalLeave = LeaveService::personalLeaveCount($userId, $date);
        $holiday = $globalLeave + $personalLeave;

        $avaliableHours = 40 - ($holiday * 8);
        $freeHours = $avaliableHours - $allocatedHours;

        $temp['avaliableHours'] = $avaliableHours;
        $temp['freeHours'] = $freeHours;

        return $temp;
    }

    public static function findAssignProject($userId, $date)
    {
        $record = array();
        $name = array();
        $userProject = DB::select(DB::raw("SELECT * FROM `timesheets` WHERE (user_id = '$userId' AND date = '$date') "));
        if ($userProject) {
            $userProject = json_decode(json_encode($userProject), true);
            foreach ($userProject as $key => $value) {
                $value['options']['project_id'] = $value['project_id'];
                $value['options']['hours'] = $value['hours'];
                $value['options']['approved_by'] = $value['approver_id'];
                $value['options']['date'] = $value['date'];
                $record[] = $value['options'];
            }
            return $record;
        } else {
            $userProject = DB::select(DB::raw("SELECT * FROM `project_resources` WHERE (user_id = '$userId' AND start_date >= '$date') OR (user_id = '$userId' AND start_date >= '$date' AND end_date <= '$date')"));
            if ($userProject) {
                $userProject = json_decode(json_encode($userProject), true);
                foreach ($userProject as $key => $value) {
                    $value['options']['project_id'] = $value['project_id'];
                    $value['options']['hours'] = 0;
                    $value['options']['approved_by'] = null;
                    $value['options']['date'] = $date;
                    $record[] = $value['options'];
                }
                return $record;
            } else {
                return $record;
            }
        }
    }

    public static function projectsArray($userId, $start_date, $end_date)
    {
        $user = User::find($userId);
        if ($user) {
            $resources = ProjectResource::with('project')->where('user_id', $userId)->where('start_date', '<=', $end_date)
                ->where(function ($query) use ($start_date) {
                    $query->where('end_date', '>=', $start_date)
                        ->orWhere('end_date', null);
                })->get();
            $max = date_diff(date_create($start_date), date_create($end_date))->format('%a');
            $result = [];
            $projects = [];
            $dates = [];
            $flag = 0;

            foreach ($resources as $resource) {
                $project = $resource->project;
                $name = $project->project_name;
                $project_id = $project->id;
                $date = $start_date;
                $data = [];
                for ($i = 0; $i <= $max; $i++) {
                    $is_fillable = true;
                    $is_assigned = true;
                    $is_holiday = false;
                    $on_leave = false;
                    if (date('N', strtotime($date)) > 5 || Calendar::whereDate('date', $date)->count() > 0) {
                        $is_holiday = true;
                        $is_fillable = false;
                    }
                    if ($user->leaves()->where('start_date', '<=', $date)->where('end_date', '>=', $date)->where('status', '=', 'approved')->count() > 0) {
                        $on_leave = true;
                        $is_fillable = false;
                    }
                    $stop_date = new \DateTime();
                    if (!$resource->end_date) {
                        $stop_date->format('Y-m-d');
                        $stop_date->modify('+1 day');
                    }
                    if ((!empty($resource->end_date) ? $resource->end_date : $stop_date) < $date) {
                        $is_assigned = false;
                    }
                    if ($resource->start_date > $date) {
                        $is_assigned = false;
                    }

                    $hours = 0;
                    $timesheet_id = null;
                    $timesheet_data = null;
                    $timesheetObj = Timesheet::where('user_id', $userId)->where('date', '=', $date)->where('project_id', $project->id)->first();
                    if ($timesheetObj) {
                        $timesheet_id = !empty($timesheetObj->id) ? $timesheetObj->id : null;
                    }

                    if ($timesheet_id) {
                        $details = Timesheet::with('timesheet_detail')->where('id', $timesheet_id)->get();
                        $timesheet_data = $details[0]['timesheet_detail'];
                    }
                    $hours = 0;
                    $approved_hours = 0;
                    $approver_id = null;
                    $manager_comments = '';
                    if (isset($timesheetObj)) {
                        $hours = $timesheetObj->sumOfHours();
                        $approved_hours = $timesheetObj->approved_hours;
                        $approver_id = $timesheetObj->approver_id;
                        $manager_comments = !empty($timesheetObj->manager_comments) ? $timesheetObj->manager_comments : '';
                    }

                    $timesheet_details = !empty($timesheet_data) ? $timesheet_data : null;

                    $data[] = ['is_fillable' => $is_fillable, 'is_holiday' => $is_holiday, 'on_leave' => $on_leave, 'is_assigned' => $is_assigned,
                        'date' => $date, 'user_id' => $user->id, 'timesheet_id' => $timesheet_id, 'project_id' => $project->id,
                        'hours' => $hours, 'timesheet_details' => $timesheet_details, 'approved_hours' => $approved_hours, 'approver_id' => $approver_id,
                        'manager_comments' => $manager_comments];
                    if ($flag == 0) {
                        $dates[] = date('D, j M', strtotime($date));
                    }
                    $date = date('Y-m-d', strtotime($date . ' +1 day'));
                }
                $flag = 1;
                $projects[] = ['id' => $project_id, 'name' => $name, 'data' => $data];
            }
            $result['dates'] = $dates;
            $result['projects'] = $projects;
            return $result;
        } else {
            return null;
        }
    }
    public static function currentLastWeekHours($user_id, $current_date)
    {
        $hours_data = [];
        $date = date("Y-m-d", strtotime("previous monday"));
        $applied_total = 0;
        $approved_total = 0;
        $max = date_diff(date_create($date), date_create($current_date))->format('%a');
        for ($i = 0; $i <= $max; $i++) {
            $timesheet_id = null;
            $timesheet_data = null;
            $timesheets = Timesheet::where('user_id', $user_id)->where('date', '=', $date)->get();

            if (!empty($timesheets)) {
                foreach ($timesheets as $timesheet) {
                    if ($timesheet->approver_id != null) {
                        $approved_total = $approved_total + $timesheet->approved_hours;
                    }
                    $timesheet_details = TimesheetDetail::where('timesheet_id', $timesheet->id)->get();
                    if (!empty($timesheet_details)) {
                        foreach ($timesheet_details as $timesheet_detail) {
                            $applied_total = $applied_total + $timesheet_detail->duration;
                        }
                    }
                }
            }
            $date = date('Y-m-d', strtotime($date . ' +1 day'));
        }
        $hours_data['current_applied_total'] = $applied_total;
        $hours_data['current_approved_total'] = $approved_total;

        $start_date = date('Y-m-d', strtotime('-2 Monday'));
        $end_date = date('Y-m-d', strtotime($start_date . ' +6 day'));
        $max = date_diff(date_create($start_date), date_create($end_date))->format('%a');
        $applied_total = 0;
        $approved_total = 0;
        for ($i = 0; $i <= $max; $i++) {

            $timesheet_id = null;
            $timesheet_data = null;
            $timesheets = Timesheet::where('user_id', $user_id)->where('date', '=', $start_date)->get();

            if (!empty($timesheets)) {
                foreach ($timesheets as $timesheet) {
                    if ($timesheet->approver_id != null) {
                        $approved_total = $approved_total + $timesheet->approved_hours;
                    }
                    $timesheet_details = TimesheetDetail::where('timesheet_id', $timesheet->id)->get();
                    if (!empty($timesheet_details)) {
                        foreach ($timesheet_details as $timesheet_detail) {
                            $applied_total = $applied_total + $timesheet_detail->duration;
                        }
                    }
                }
            }
            $start_date = date('Y-m-d', strtotime($start_date . ' +1 day'));
        }
        $hours_data['last_week_applied_total'] = $applied_total;
        $hours_data['last_week_approved_total'] = $approved_total;

        return $hours_data;
    }
    public static function currentAllocatedResources($input_data)
    {

        $date = date('Y-m-d');
        $result = [];
        $active_users = [];
        foreach ($input_data['projects'] as $project) {
            $usersWithEndDate = [];
            $usersWithoutEndDate = [];
            $userList = [];
            $userArray = [];
            $users = '';
            $usersWithEndDate = ProjectResource::where('project_id', $project->id)
                ->where('start_date', '<=', $date)
                ->where('end_date', '>=', $date)->get();

            $usersWithoutEndDate = ProjectResource::where('project_id', $project->id)
                ->where('start_date', '<=', $date)
                ->whereNull('end_date')->get();

            $userList = $usersWithEndDate->merge($usersWithoutEndDate);
            if (count($userList) > 0) {
                foreach ($userList as $user) {
                    $userArray[] = !empty($user->user_data->name) ? $user->user_data->name : null;
                    $users = implode($userArray, ' | ');
                }
            }
            // print_r($users);
            // die;
            $active_users[] = $users;
        }
        $result['status'] = $input_data['status'];
        $result['projects'] = $input_data['projects'];
        $result['active_users'] = $active_users;
        $result['search'] = $input_data['search'];
        return $result;
    }

    public static function projectsArrayWithProject($userId, $start_date, $end_date, $filter_project_id)
    {
        $user = User::find($userId);
        if ($user) {
            $resources = ProjectResource::with('project')->where('user_id', $userId)->where('project_id', $filter_project_id)->where('start_date', '<=', $end_date)
                ->where(function ($query) use ($start_date) {
                    $query->where('end_date', '>=', $start_date)
                        ->orWhere('end_date', null);
                })->get();
            $max = date_diff(date_create($start_date), date_create($end_date))->format('%a');
            $result = [];
            $projects = [];
            $dates = [];
            $flag = 0;

            foreach ($resources as $resource) {
                $project = $resource->project;
                $name = $project->project_name;
                $project_id = $project->id;

                $date = $start_date;
                $data = [];
                for ($i = 0; $i <= $max; $i++) {
                    $is_fillable = true;
                    $is_assigned = true;
                    $is_holiday = false;
                    $on_leave = false;
                    if (date('N', strtotime($date)) > 5 || Calendar::whereDate('date', $date)->count() > 0) {
                        $is_holiday = true;
                        $is_fillable = false;
                    }
                    if ($user->leaves()->where('start_date', '<=', $date)->where('end_date', '>=', $date)->where('status', '=', 'approved')->count() > 0) {
                        $on_leave = true;
                        $is_fillable = false;
                    }
                    $stop_date = new \DateTime();
                    if (!$resource->end_date) {
                        $stop_date->format('Y-m-d');
                        $stop_date->modify('+1 day');
                    }
                    if ((!empty($resource->end_date) ? $resource->end_date : $stop_date) < $date) {
                        $is_assigned = false;
                    }
                    if ($resource->start_date > $date) {
                        $is_assigned = false;
                    }

                    $hours = 0;
                    $timesheet_id = null;
                    $timesheet_data = null;
                    $timesheetObj = Timesheet::where('user_id', $userId)->where('date', '=', $date)->where('project_id', $project->id)->first();
                    if ($timesheetObj) {
                        $timesheet_id = !empty($timesheetObj->id) ? $timesheetObj->id : null;
                    }

                    if ($timesheet_id) {
                        $details = Timesheet::with('timesheet_detail')->where('id', $timesheet_id)->get();
                        $timesheet_data = $details[0]['timesheet_detail'];
                    }
                    $hours = 0;
                    $approved_hours = 0;
                    $approver_id = null;
                    $manager_comments = '';
                    if (isset($timesheetObj)) {
                        $hours = $timesheetObj->sumOfHours();
                        $approved_hours = $timesheetObj->approved_hours;
                        $approver_id = $timesheetObj->approver_id;
                        $manager_comments = !empty($timesheetObj->manager_comments) ? $timesheetObj->manager_comments : '';
                    }

                    $timesheet_details = !empty($timesheet_data) ? $timesheet_data : null;

                    $data[] = ['is_fillable' => $is_fillable, 'is_holiday' => $is_holiday, 'on_leave' => $on_leave, 'is_assigned' => $is_assigned,
                        'date' => $date, 'user_id' => $user->id, 'timesheet_id' => $timesheet_id, 'project_id' => $project->id,
                        'hours' => $hours, 'timesheet_details' => $timesheet_details, 'approved_hours' => $approved_hours, 'approver_id' => $approver_id,
                        'manager_comments' => $manager_comments];
                    if ($flag == 0) {
                        $dates[] = date('D, j M', strtotime($date));
                    }
                    $date = date('Y-m-d', strtotime($date . ' +1 day'));
                }
                $flag = 1;
                $projects[] = ['id' => $project_id, 'name' => $name, 'data' => $data];

                $result['dates'] = $dates;
                $result['projects'] = $projects;
                return $result;
            }

        } else {
            return null;
        }
    }

    public static function getProjectSlackReport($publicChannels)
    {
        $projects = Project::with('projectSlackChannels')->whereIn('status', ['in_progress', 'open', 'free'])->get();
        $project_details = [];
        foreach ($projects as $project) {
            $project_detail = [];
            $project_detail['name'] = $project->project_name;
            if (count($project->projectSlackChannels) > 0) {
                $slack_channels_array = [];
                foreach ($project->projectSlackChannels as $channel) {
                    foreach ($publicChannels as $slack_channel) {
                        if ($channel->slack_id == $slack_channel->id) {
                            $slack_channels_array[] = $slack_channel->name_normalized;
                        }
                    }
                }
                $slack_channels = implode($slack_channels_array, ' | ');
                $project_detail['slack_channels'] = $slack_channels;
            }
            $project_detail['slack_channels'] = !empty($project_detail['slack_channels']) ? $project_detail['slack_channels'] : '';
            $project_details[] = $project_detail;
        }
        return $project_details;
    }
    public static function saveData($data)
    {
        $status = true;
        $message = '';

        if ($data['project_manager_id']) {
            $find = User::where('id', $data['project_manager_id'])->first();
            $managerName = $find->name;
        } else {
            $managerName = " ";
        }

        $project = new Project();
        $project->company_id = $data['company_id'];
        $project->project_name = $data['project_name'];
        $project->project_manager = $managerName;
        $project->bd_manager_id = $data['bd_manager'];
        $project->account_manager_id = !empty($data['account_manager']) ? $data['account_manager'] : null;
        $project->project_category_id = !empty($data['project_category']) ? $data['project_category'] : null;
        $project->project_manager_id = !empty($data['project_manager_id']) ? $data['project_manager_id'] : null;
        $project->amount = !empty($data['amount']) ? $data['amount'] : null;
        $project->notes = !empty($data['notes']) ? $data['notes'] : null;

        if ($data['start_date']) {
            $project->start_date = date_to_yyyymmdd($data['start_date']);
        } else {
            $project->start_date = date("Y-m-d");
        }
        if ($data['end_date']) {
            if (strtotime($data['start_date']) > strtotime($data['end_date'])) {
                $status = false;
                $message = 'Start Date can\'t be greater than end date';

                $response['message'] = $message;
                $response['status'] = $status;

                return $response;
            }
            $project->end_date = date_to_yyyymmdd($data['end_date']);
        } else {
            $project->end_date = null;
        }
        if (!empty($data['status'])) {
            $project->status = $data['status'];
        } else {
            $project->status = 'open';
        }

        $val = ($data['reminder'] == "yes") ? 1 : 0;
        $project->email_reminder = $val;

        if ($project->save()) {
            if (!empty($data['account_manager'])) {
                event(new AccountManagerAssigned($data['account_manager'], $project->id));
            }
            if (!empty($data['bd_manager'])) {
                event(new BdManagerAssigned($data['bd_manager'], $project->id));
            }
            $message = 'Successfully Added';
            if ($project->project_manager_id != null) {
                event(new ProjectManagerAssigned($project->project_manager_id, $project->id));
                $manager = $project->projectManager;
                if (!$manager->hasRole('team-lead')) {
                    if (!UserRole::createUserRole($manager->id, 'team-lead')) {
                        $message = $message . " but unable to assign role to the team lead";
                    }
                }
            }
            $flag = 0;
            if (isset($data['channels'])) {
                foreach ($data['channels'] as $channel) {
                    $exists = ProjectSlackChannel::where('slack_id', $channel)->count();
                    if (empty($exists) || $exists == 0) {
                        $slackChannel = new ProjectSlackChannel();
                        $slackChannel->project_id = $project->id;
                        $slackChannel->slack_id = $channel;
                        $slackChannel->save();
                        $tmpTableUpdateNeeded = TimesheetService::isChannelPresent($channel);
                        if ($tmpTableUpdateNeeded) {
                            event(new SlackChannelLinked($channel));
                        }
                    } else {
                        $flag = 1;
                    }
                }
            }
            if (isset($data['private_channels'])) {
                foreach ($data['private_channels'] as $private_channel) {
                    $exists = ProjectSlackChannel::where('slack_id', $private_channel)->count();
                    if (empty($exists) || $exists == 0) {
                        $slackChannel = new ProjectSlackChannel();
                        $slackChannel->project_id = $project->id;
                        $slackChannel->slack_id = $private_channel;
                        $slackChannel->type = 'private';
                        $slackChannel->save();
                    } else {
                        $flag = 1;
                    }
                }
            }
            if ($flag == 1) {
                $message = $message . " but some channels are associated with other projects hence, were not added";
            }

            $response['message'] = $message;
            $response['status'] = $status;
            $response['data'] = $project;
            return $response;

        } else {
            $status = false;
            $message = $project->getErrors();

            $response['message'] = $message;
            $response['status'] = $status;
            return $response;
        }
    }

    public static function saveDataNew($input)
    {
        $status = true;
        $message = '';

        $project = new Project();
        $project->start_date = $input['start_date'];
        $project->end_date = !empty($input['end_date']) ? $input['end_date'] : null;
        $project->project_name = $input['project_name'];
        $project->project_manager = $input['project_manager'];
        $project->project_manager_id = !empty($input['project_manager_id']) ? $input['project_manager_id'] : null;
        $project->account_manager_id = !empty($input['account_manager_id']) ? $input['account_manager_id'] : null;
        $project->company_id = $input['company_id'];
        $project->project_category_id = !empty($input['project_category_id']) ? $input['project_category_id'] : null;
        $project->bd_manager_id = !empty($input['bd_manager_id']) ? $input['bd_manager_id'] : null;
        $project->code_lead_id = !empty($input['code_lead_id']) ? $input['code_lead_id'] : null;
        $project->delivery_lead_id = !empty($input['delivery_lead_id']) ? $input['delivery_lead_id'] : null;
        $project->email_reminder = ($input['email_reminder'] == '1') ? 1 : 0;
        $project->is_critical = ($input['is_critical'] == '1') ? 1 : 0;
        $project->status = $input['status'];
        if (!$project->save()) {
            $status = false;
            $message = $project->getErrors();
        }
        // Save Slack Public Channels
        if (!empty($input['slack_public_channels']) && count($input['slack_public_channels']) > 0) {
            $type = 'public';
            self::saveSlackChannels($project->id, $input['slack_public_channels'], $type);

        }
        // Save Slack Private Channels
        if (!empty($input['slack_private_channels']) && count($input['slack_private_channels']) > 0) {
            $type = 'private';
            self::saveSlackChannels($project->id, $input['slack_private_channels'], $type);

        }
        // Save Project Leads Info
        self::saveProjectLeads($project->id, $input['project_leads'], $input['lead_type']);

        $response['message'] = $message;
        $response['status'] = $status;
        return $response;
    }

    public static function saveSlackChannels($projectId, $slackChannels, $type)
    {
        ProjectSlackChannel::where('project_id', $projectId)->where('type', $type)->delete();
        foreach ($slackChannels as $channel) {
            $exists = ProjectSlackChannel::where('slack_id', $channel['id'])->count();
            if ($exists == 0) {
                $slackChannel = new ProjectSlackChannel();
                $slackChannel->project_id = $projectId;
                $slackChannel->slack_id = $channel['id'];
                $slackChannel->type = $type;
                $slackChannel->save();
            }
        }
    }

    public static function saveProjectLeads($project_id, $project_leads, $lead_types)
    {
        if ($lead_types['ui_ux'] == 'client') {
            $projectLead = new ProjectLead();
            $projectLead->project_id = $project_id;
            $projectLead->key = 'ui_ux_lead';
            $projectLead->is_ours = false;
            $projectLead->value = $project_leads['ui_ux_lead'];
            $projectLead->save();
        } elseif ($lead_types['ui_ux'] == 'geekyants') {
            $projectLead = new ProjectLead();
            $projectLead->project_id = $project_id;
            $projectLead->key = 'ui_ux_lead';
            $projectLead->is_ours = true;
            $projectLead->value = $project_leads['ui_ux_lead']['id'];
            $projectLead->save();
        }
        if ($lead_types['dev_op'] == 'client') {
            $projectLead = new ProjectLead();
            $projectLead->project_id = $project_id;
            $projectLead->key = 'dev_op_lead';
            $projectLead->is_ours = false;
            $projectLead->value = $project_leads['dev_op_lead'];
            $projectLead->save();
        } elseif ($lead_types['dev_op'] == 'geekyants') {
            $projectLead = new ProjectLead();
            $projectLead->project_id = $project_id;
            $projectLead->key = 'dev_op_lead';
            $projectLead->is_ours = true;
            $projectLead->value = $project_leads['dev_op_lead']['id'];
            $projectLead->save();
        }
        if ($lead_types['architect'] == 'client') {
            $projectLead = new ProjectLead();
            $projectLead->project_id = $project_id;
            $projectLead->key = 'architect_lead';
            $projectLead->is_ours = false;
            $projectLead->value = $project_leads['architect_lead'];
            $projectLead->save();
        } elseif ($lead_types['architect'] == 'geekyants') {
            $projectLead = new ProjectLead();
            $projectLead->project_id = $project_id;
            $projectLead->key = 'architect_lead';
            $projectLead->is_ours = true;
            $projectLead->value = $project_leads['architect_lead']['id'];
            $projectLead->save();
        }
        if ($lead_types['deployment'] == 'client') {
            $projectLead = new ProjectLead();
            $projectLead->project_id = $project_id;
            $projectLead->key = 'deployment_lead';
            $projectLead->is_ours = false;
            $projectLead->value = $project_leads['deployment_lead'];
            $projectLead->save();
        } elseif ($lead_types['deployment'] == 'geekyants') {
            $projectLead = new ProjectLead();
            $projectLead->project_id = $project_id;
            $projectLead->key = 'deployment_lead';
            $projectLead->is_ours = true;
            $projectLead->value = $project_leads['deployment_lead']['id'];
            $projectLead->save();
        }
        if ($lead_types['qa'] == 'client') {
            $projectLead = new ProjectLead();
            $projectLead->project_id = $project_id;
            $projectLead->key = 'qa_lead';
            $projectLead->is_ours = false;
            $projectLead->value = $project_leads['qa_lead'];
            $projectLead->save();
        } elseif ($lead_types['qa'] == 'geekyants') {
            $projectLead = new ProjectLead();
            $projectLead->project_id = $project_id;
            $projectLead->key = 'qa_lead';
            $projectLead->is_ours = true;
            $projectLead->value = $project_leads['qa_lead']['id'];
            $projectLead->save();
        }
    }

    public static function updateDataNew($input)
    {
        $status = true;
        $message = '';
        $project = Project::find($input['project_id']);
        $project->start_date = $input['start_date'];
        $project->end_date = !empty($input['end_date']) ? $input['end_date'] : null;
        $project->project_name = $input['project_name'];
        $project->project_manager = !empty($input['project_manager']) ? $input['project_manager'] : null;
        $project->project_manager_id = !empty($input['project_manager_id']) ? $input['project_manager_id'] : null;
        $project->account_manager_id = !empty($input['account_manager_id']) ? $input['account_manager_id'] : null;
        $project->company_id = $input['company_id'];
        $project->project_category_id = !empty($input['project_category_id']) ? $input['project_category_id'] : null;
        $project->bd_manager_id = !empty($input['bd_manager_id']) ? $input['bd_manager_id'] : null;
        $project->code_lead_id = !empty($input['code_lead_id']) ? $input['code_lead_id'] : null;
        $project->delivery_lead_id = !empty($input['delivery_lead_id']) ? $input['delivery_lead_id'] : null;
        if(isset($input['email_reminder']))
            $project->email_reminder = ($input['email_reminder'] == '1') ? 1 : 0;
        if(isset($input['is_critical']))
            $project->is_critical = ($input['is_critical'] == '1') ? 1 : 0;

        $project->status = $input['status'];
        if (!$project->save()) {
            $status = false;
            $message = $project->getErrors();
        }
        // Save Slack Public Channels
        if (isset($input['slack_public_channels'])) {
            $type = 'public';
            self::saveSlackChannels($project->id, $input['slack_public_channels'], $type);

        }
        // Save Slack Private Channels
        if (isset($input['slack_private_channels'])) {
            $type = 'private';
            self::saveSlackChannels($project->id, $input['slack_private_channels'], $type);

        }
        // Save Project Leads Info
        self::updateProjectLeads($project->id, $input['project_leads'], $input['lead_type']);

        $response['message'] = $message;
        $response['status'] = $status;
        return $response;
    }

    public static function updateProjectLeads($project_id, $project_leads, $lead_types)
    {
        // Delete Previous Records for the project
        $res = ProjectLead::where('project_id', $project_id)->delete();

        if ($lead_types['ui_ux'] == 'client') {
            $projectLead = new ProjectLead();
            $projectLead->project_id = $project_id;
            $projectLead->key = 'ui_ux_lead';
            $projectLead->is_ours = false;
            $projectLead->value = $project_leads['ui_ux_lead'];
            $projectLead->save();
        } elseif ($lead_types['ui_ux'] == 'geekyants') {
            $projectLead = new ProjectLead();
            $projectLead->project_id = $project_id;
            $projectLead->key = 'ui_ux_lead';
            $projectLead->is_ours = true;
            $projectLead->value = $project_leads['ui_ux_lead'];
            $projectLead->save();
        }
        if ($lead_types['dev_op'] == 'client') {
            $projectLead = new ProjectLead();
            $projectLead->project_id = $project_id;
            $projectLead->key = 'dev_op_lead';
            $projectLead->is_ours = false;
            $projectLead->value = $project_leads['dev_op_lead'];
            $projectLead->save();
        } elseif ($lead_types['dev_op'] == 'geekyants') {
            $projectLead = new ProjectLead();
            $projectLead->project_id = $project_id;
            $projectLead->key = 'dev_op_lead';
            $projectLead->is_ours = true;
            $projectLead->value = $project_leads['dev_op_lead'];
            $projectLead->save();
        }
        if ($lead_types['architect'] == 'client') {
            $projectLead = new ProjectLead();
            $projectLead->project_id = $project_id;
            $projectLead->key = 'architect_lead';
            $projectLead->is_ours = false;
            $projectLead->value = $project_leads['architect_lead'];
            $projectLead->save();
        } elseif ($lead_types['architect'] == 'geekyants') {
            $projectLead = new ProjectLead();
            $projectLead->project_id = $project_id;
            $projectLead->key = 'architect_lead';
            $projectLead->is_ours = true;
            $projectLead->value = $project_leads['architect_lead'];
            $projectLead->save();
        }
        if ($lead_types['deployment'] == 'client') {
            $projectLead = new ProjectLead();
            $projectLead->project_id = $project_id;
            $projectLead->key = 'deployment_lead';
            $projectLead->is_ours = false;
            $projectLead->value = $project_leads['deployment_lead'];
            $projectLead->save();
        } elseif ($lead_types['deployment'] == 'geekyants') {
            $projectLead = new ProjectLead();
            $projectLead->project_id = $project_id;
            $projectLead->key = 'deployment_lead';
            $projectLead->is_ours = true;
            $projectLead->value = $project_leads['deployment_lead'];
            $projectLead->save();
        }
        if ($lead_types['qa'] == 'client') {
            $projectLead = new ProjectLead();
            $projectLead->project_id = $project_id;
            $projectLead->key = 'qa_lead';
            $projectLead->is_ours = false;
            $projectLead->value = $project_leads['qa_lead'];
            $projectLead->save();
        } elseif ($lead_types['qa'] == 'geekyants') {
            $projectLead = new ProjectLead();
            $projectLead->project_id = $project_id;
            $projectLead->key = 'qa_lead';
            $projectLead->is_ours = true;
            $projectLead->value = $project_leads['qa_lead'];
            $projectLead->save();
        }
    }

    public static function managerInfo($project_id)
    {
        $projectObj = Project::find($project_id);
        $returnObj = [];

        $returnObj['sales_manager'] = !empty($projectObj->bdManager->name) ? $projectObj->bdManager->name : '';
        $returnObj['project_coordinator'] = !empty($projectObj->accountManager->name) ? $projectObj->accountManager->name : '';
        $returnObj['project_manager'] = !empty($projectObj->projectManager->name) ? $projectObj->projectManager->name : '';
        $returnObj['code_lead'] = !empty($projectObj->codeLead->name) ? $projectObj->codeLead->name : '';
        $returnObj['delivery_lead'] = !empty($projectObj->deliveryLead->name) ? $projectObj->deliveryLead->name : '';
        $returnObj['ui_ux_lead'] = self::getProjectLead($project_id, 'ui_ux_lead');
        $returnObj['dev_op_lead'] = self::getProjectLead($project_id, 'dev_op_lead');
        $returnObj['architect_lead'] = self::getProjectLead($project_id, 'architect_lead');
        $returnObj['deployment_lead'] = self::getProjectLead($project_id, 'deployment_lead');
        $returnObj['qa_lead'] = self::getProjectLead($project_id, 'qa_lead');

        return $returnObj;
    }

    public static function getProjectLead($project_id, $key)
    {
        $lead = '';
        $leadObj = ProjectLead::where('project_id', $project_id)->where('key', $key)->first();
        if ($leadObj && !$leadObj->is_ours) {
            $lead = $leadObj->value;
        } elseif ($leadObj && $leadObj->is_ours) {
            $userObj = User::find($leadObj->value);
            $lead = $userObj->name;
        }
        return $lead;

    }

    public static function getCalendarArray($monthString, $year)
    {
        $month = date('m', strtotime($monthString));
        if ($month < 1 || $month > 12) {
            throw new Exception("Invalid Month");
        }

        $start_date = $year . '-' . $month . '-01';
        $no_of_days = date("t", strtotime($start_date));

        $calendar = [];
        $currentArray = ProjectService::getWeekArray();
        for ($day = 1; $day <= $no_of_days; $day++) {
            $name_of_the_day = date('D', strtotime($year . '-' . $month . '-' . $day));
            $number_of_the_day = date('N', strtotime($year . '-' . $month . '-' . $day));
            if (strlen($day) === 1) {
                $day = "0" . $day;
            }
            $currentArray[$name_of_the_day] = $year . '-' . $month . '-' . $day;
            if ($number_of_the_day % 7 == 0 && $no_of_days != $day) {
                $calendar[] = $currentArray;
                $currentArray = ProjectService::getWeekArray();
            }
        }
        $calendar[] = $currentArray;

        return $calendar;
    }

    public static function getWeekArray()
    {
        return ['Mon' => 0, 'Tue' => 0, 'Wed' => 0, 'Thu' => 0, 'Fri' => 0, 'Sat' => 0, 'Sun' => 0];
    }
}
