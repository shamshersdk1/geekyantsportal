<?php namespace App\Services\CronService;

use Validator;
use App\Models\Admin\Cron;
use App\Jobs\Cron\CronExecutorJob;

use DB;
use Input;
use Redirect;
use App\Models\User;
use Auth;


class CronService {

	public static function getEligibleRecords($minute, $hour, $day_of_month, $month_of_year, $day_of_week)
	{
        $today = date('Y-m-d');
        // Filter records which have crossed end_on or number of occurances

        $filteredRecords = Cron::whereRaw('( end_on >= ? or end_on is null) AND
                                 ( occurrence_counter > executed_counter or occurrence_counter is null or executed_counter is null )', [$today])->get();
                                 
        foreach ( $filteredRecords as $record )
        {
            $minuteFlag = false;
            $hourFlag = false;
            $domFlag = false;
            $moyFlag = false;
            $dowFlag = false;
            $onceExecuted = true;
            // Check for Minutes
            if( !$record->minutes )
            {
                $minuteFlag = true;
            } else {
                $minArray =  explode(",", trim($record->minutes));
                $minuteFlag = in_array($minute, $minArray);
            }

            // Check for Hours
            if( !$record->hours )
            {
                $hourFlag = true;
            } else {
                $hourArray =  explode(",", trim($record->hours));
                $hourFlag = in_array($hour, $hourArray);
            }

            // Check for Day of Month
            if( !$record->day_of_month )
            {
                $domFlag = true;
            } else {
                $domArray =  explode(",", trim($record->day_of_month));
                $domFlag = in_array($day_of_month, $domArray);
            }

            // Check for Month of year
            if( !$record->month_of_year )
            {
                $moyFlag = true;
            } else {
                $moyArray =  explode(",", trim($record->month_of_year));
                $moyFlag = in_array($month_of_year, $moyArray);
            }

            // Check for Day of week
            
            if( !$record->day_of_week )
            {
                $dowFlag = true;
            } else {
                $dowArray =  explode(",", trim($record->day_of_week));
                $dowFlag = in_array($day_of_week, $dowArray);
            }
            if ( $record->event_type == 'once' && $record->last_processed_at != null )
            {
                $onceExecuted = false;
            }

            if ( $minuteFlag && $hourFlag && $domFlag && $moyFlag && $dowFlag && $onceExecuted  )
            {
                $job = (new CronExecutorJob($record->id))->onQueue('default');
                dispatch($job);
            }
        }	
    }
    
    public static function saveData($input)
    {
        $status = true;
        $message = '';
        $today = date('Y-m-d');
        $tomorrow = date('Y-m-d', strtotime('tomorrow'));
        // ONCE
        if ( $input['event_type'] == 'once' )
        {
            if ( $input['schedule_date'] < $today )
            {
                $message = 'Seleceted Date cannot be a past day';
            } else {
                $cronObj = new Cron();
                $cronObj->reference_id = $input['reference_id'];
                $cronObj->reference_type = !empty($input['reference_type']) ? $input['reference_type'] : null;
                $cronObj->event_type = $input['event_type'];
                $cronObj->reference_data_json = !empty($input['reference_data_json']) ? $input['reference_data_json'] : null;
                $cronObj->minutes = !empty($input['minutes']) ? $input['minutes'] : null;
                $cronObj->hours = !empty($input['hours']) ? $input['hours'] : null;
                $cronObj->day_of_month = date('d', strtotime($input['schedule_date']));
                $cronObj->month_of_year = self::getMonthOfYear($input['schedule_date']);
                $cronObj->day_of_week = !empty(self::getDayOfWeek($input['selected_days'])) ? self::getDayOfWeek($input['selected_days']) : null ;
                $cronObj->occurrence_counter = !empty($input['number_of_occurrences']) ? $input['number_of_occurrences'] : null;
                $cronObj->end_on = !empty($input['ends_on_date']) ? $input['ends_on_date'] : null;
                $cronObj->job_handler = !empty($input['job_handler']) ? $input['job_handler'] : null;
                if (  !$cronObj->save() )
                {
                    $message = $cronObj->getErrors();
                }
            }    
        }
        else 
        {
            if ( !empty($input['ends_on_date']) && ( $input['ends_on_date'] < $tomorrow) )
            {
                $message = 'Ends on date should be a future date';
            } else {
                $cronObj = new Cron();
                $cronObj->reference_id = $input['reference_id'];
                $cronObj->reference_type = !empty($input['reference_type']) ? $input['reference_type'] : null;
                $cronObj->event_type = $input['event_type'];
                $cronObj->repeat_type = $input['repeat_type'];
                $cronObj->reference_data_json = !empty($input['reference_data_json']) ? $input['reference_data_json'] : null;
                $cronObj->minutes = !empty($input['minutes']) ? $input['minutes'] : null;
                $cronObj->hours = !empty($input['hours']) ? $input['hours'] : null;
                $cronObj->day_of_month = !empty(self::getDayOfMonth($input['selected_dates'])) ? self::getDayOfMonth($input['selected_dates']) : null ;
                $cronObj->month_of_year =  !empty($input['schedule_date']) ?  self::getMonthOfYear($input['schedule_date']) : null ;
                $cronObj->day_of_week = !empty(self::getDayOfWeek($input['selected_days'])) ? self::getDayOfWeek($input['selected_days']) : null;
                $cronObj->occurrence_counter = !empty($input['number_of_occurrences']) ? $input['number_of_occurrences'] : null;
                $cronObj->end_on = !empty($input['ends_on_date']) ? $input['ends_on_date'] : null;
                $cronObj->job_handler = !empty($input['job_handler']) ? $input['job_handler'] : null;   
                if (  !$cronObj->save() )
                {
                    $status = false;
                    $message = $cronObj->getErrors();
                }
            }
        }  
        
        $response = [];
        $response['message'] = $message;
        return $response;
    }

    public static function getDayOfWeek($input)
    {
        $domVar = [];
        if ( $input['sun'] )
            $domVar[] = 0;
        if ( $input['mon'] )
            $domVar[] = 1;
        if ( $input['tue'] )
            $domVar[] = 2;
        if ( $input['wed'] )
            $domVar[] = 3;
        if ( $input['thu'] )
            $domVar[] = 4;
        if ( $input['fri'] )
            $domVar[] = 5;
        if ( $input['sat'] )
            $domVar[] = 6;
        $string = implode(",",$domVar);
        return $string;
    }

    public static function getDayOfMonth($input)
    {
        $dowVar = [];
        for( $i = 1; $i < 32; $i++ )
        {
            if ( $input[$i] ) {
                $dowVar[] = $i;
            }
        }
        $string = implode(",",$dowVar);
        return $string;
    }

    public static function getMonthOfYear($date)
    {
        return  date('m', strtotime($date));
    }

    public static function getData( $reference_id, $job_handler )
    {
        $status = true;
        $message = '';
        $record = [];
        $day_of_month = [];
        $day_of_week = [];
        $cronObj = Cron::where('reference_id',$reference_id)->where('job_handler',$job_handler)->first();
        if ( !$cronObj )
        {
            $status = false;
            $message = 'No records found';
        }
        else {
            $record['id'] = $cronObj->id;
            $record['event_type'] = $cronObj->event_type;
            $record['repeat_type'] = !empty($cronObj->repeat_type) ? $cronObj->repeat_type : null;
            $record['minutes'] = $cronObj->minutes;
            $record['hours'] = $cronObj->hours;
            if ( !empty($cronObj->day_of_month) )
                $day_of_month =  explode(",",trim($cronObj->day_of_month));
            
            $record['day_of_month'] = $day_of_month;
            if ( !empty($cronObj->day_of_week) )
                $day_of_week =  explode(",",trim($cronObj->day_of_week));
            
            $record['day_of_week'] = $day_of_week;
            $record['month_of_year'] = !empty($cronObj->month_of_year) ? $cronObj->month_of_year : null;
            $record['occurrence_counter'] = !empty($cronObj->occurrence_counter) ? $cronObj->occurrence_counter : null;
            $record['executed_counter'] = !empty($cronObj->executed_counter) ? $cronObj->executed_counter : null;
            $record['ends_on'] = !empty($cronObj->end_on) ? $cronObj->end_on : null;
            $record['repeat_cycle'] = self::getRepeatCycle($cronObj->id);
            $message = $record;
        }
        $response['status'] = $status;
        $response['message'] = $message;
        return $response;
    }

    public static function updateData($id, $input)
    {
        $status = true;
        $message = '';
        $today = date('Y-m-d');
        $tomorrow = date('Y-m-d', strtotime('tomorrow'));
        // ONCE
        if ( $input['event_type'] == 'once' )
        {
            if ( $input['schedule_date'] < $today )
            {
                $message = 'Seleceted Date cannot be a past day';
            } else {
                $cronObj = Cron::find($id);
                if( !$cronObj )
                {
                    $message = 'Cron record not found';
                }
                else
                {
                    $cronObj->reference_id = $input['reference_id'];
                    $cronObj->reference_type = !empty($input['reference_type']) ? $input['reference_type'] : null;
                    $cronObj->event_type = $input['event_type'];
                    $cronObj->repeat_type =  null ;
                    $cronObj->reference_data_json = !empty($input['reference_data_json']) ? $input['reference_data_json'] : null;
                    $cronObj->minutes = !empty($input['minutes']) ? $input['minutes'] : null;
                    $cronObj->hours = !empty($input['hours']) ? $input['hours'] : null;
                    $cronObj->day_of_month = date('d', strtotime($input['schedule_date']));
                    $cronObj->month_of_year = self::getMonthOfYear($input['schedule_date']);
                    $cronObj->day_of_week =  null ;
                    $cronObj->occurrence_counter = !empty($input['number_of_occurrences']) ? $input['number_of_occurrences'] : null;
                    $cronObj->end_on = !empty($input['ends_on_date']) ? $input['ends_on_date'] : null;
                    $cronObj->job_handler = !empty($input['job_handler']) ? $input['job_handler'] : null;
                    if (  !$cronObj->save() )
                    {
                        $message = $cronObj->getErrors();
                    }
                }
                
            }    
        }
        else 
        {
            if ( !empty($input['ends_on_date']) && ( $input['ends_on_date'] < $tomorrow) )
            {
                $message = 'Ends on date should be a future date';
            } else {
                $cronObj = Cron::find($id);
                if( !$cronObj )
                {
                    $message = 'Cron record not found';
                }
                else
                {
                    $cronObj->reference_id = $input['reference_id'];
                    $cronObj->reference_type = !empty($input['reference_type']) ? $input['reference_type'] : null;
                    $cronObj->event_type = $input['event_type'];
                    $cronObj->repeat_type = $input['repeat_type'];
                    $cronObj->reference_data_json = !empty($input['reference_data_json']) ? $input['reference_data_json'] : null;
                    $cronObj->minutes = !empty($input['minutes']) ? $input['minutes'] : null;
                    $cronObj->hours = !empty($input['hours']) ? $input['hours'] : null;
                    $cronObj->day_of_month = !empty(self::getDayOfMonth($input['selected_dates'])) ? self::getDayOfMonth($input['selected_dates']) : null ;
                    $cronObj->month_of_year = null ;
                    $cronObj->day_of_week = !empty( self::getDayOfWeek($input['selected_days'])) ? self::getDayOfWeek($input['selected_days']) : null;
                    $cronObj->occurrence_counter = !empty($input['number_of_occurrences']) ? $input['number_of_occurrences'] : null;
                    $cronObj->end_on = !empty($input['ends_on_date']) ? $input['ends_on_date'] : null;
                    $cronObj->job_handler = !empty($input['job_handler']) ? $input['job_handler'] : null;   
                    if (  !$cronObj->save() )
                    {
                        $status = false;
                        $message = $cronObj->getErrors();
                    }
                }
            }
        }  
        
        $response = [];
        $response['message'] = self::getData( $cronObj->reference_id, $cronObj->job_handler );
        return $response;
    }

    public static function getRepeatCycle($id)
    {
        $cronObj = Cron::find($id);
        $displayText = '';
        $time = $cronObj->hours.':'.$cronObj->minutes;
        if ( $cronObj->event_type == 'once' )
        {
            // This job will run once at {{$time}}	 on {{$date}}
            $date = $cronObj->day_of_month.'-'.date('M', mktime(0, 0, 0, $cronObj->month_of_year, 10));
            $displayText = 'This job will run once at '.$time.' on '.$date;
        }
        else {
            if ( $cronObj->repeat_type == 'daily' )
            {
                // This job repeats every {{ daily }} at {{time}}
                $displayText = 'Every day at '.$time;
            }
            elseif ( $cronObj->repeat_type == 'weekly' )
            {
                $days = self::getDaysText( $cronObj->day_of_week );
                $displayText = 'Every week on '.$days. ' at ' .$time;
            }
            else {
                // This job repeats every month on {{ dates }} at {{time}}
                $displayText = 'Every month on '.$cronObj->day_of_month. ' at ' .$time;
            }
        }
        return $displayText;
    }

    public static function getDaysText( $day_of_week )
    {
        $days_array = explode(",",trim($day_of_week));
        $days_name_array = [];
        foreach ( $days_array as $days )
        {
            switch ($days) {
                case "0":
                    $days_name_array[] = 'Sun';
                    break;
                case "1":
                    $days_name_array[] = 'Mon';
                    break;
                case "2":
                    $days_name_array[] = 'Tue';
                    break;
                case "3":
                    $days_name_array[] = 'Wed';
                    break;
                case "4":
                    $days_name_array[] = 'Thu';
                    break;
                case "5":
                    $days_name_array[] = 'Fri';
                    break;
                case "6":
                    $days_name_array[] = 'Sat';
                    break;    
                default:
                    $text = $text;
            }
        }
        $text = implode(",",$days_name_array);
        return $text;
    }
}