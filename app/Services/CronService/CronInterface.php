<?php
namespace  App\Services\CronService;

interface CronInterface {
    public static function handle($reference_id, $reference_type, $reference_data_json);
}
