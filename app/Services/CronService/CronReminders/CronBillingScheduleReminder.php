<?php
namespace App\Services\CronService\CronReminders;
use App\Services\CronService\CronInterface;
use Illuminate\Http\Request;

use App\Models\Admin\BillingScheduleReminder;
use App\Jobs\GenerateInvoice;
use App\Models\Admin\ProjectSlackChannel;


class CronBillingScheduleReminder implements CronInterface
{
    public static function handle($reference_id, $reference_type, $reference_data_json)
    {
        $date = date('Y-m-d');
        $billingReminderObj =  new BillingScheduleReminder();
        $billingReminderObj->project_id = $reference_id;
        $billingReminderObj->invoice_date = $date;
        if($billingReminderObj->save()) {
            $channels = ProjectSlackChannel::where('project_id', $billingReminderObj->project_id)->where('type', 'private')->get();
            if (count($channels) > 0) {
                foreach ($channels as $channel) {
                    $channelId = $channel->slack_id;
                    $job = (new GenerateInvoice($billingReminderObj->id, $channelId))->onQueue('default');
                    dispatch($job);
                }
            } else {
                $channelId = \Config::get('slack.bd_channel_id');
                $job = (new GenerateInvoice($billingReminderObj->id, $channelId))->onQueue('default');
                dispatch($job);
            }
        }
	}
}