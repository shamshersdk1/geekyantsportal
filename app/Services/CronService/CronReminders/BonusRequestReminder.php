<?php namespace App\Services\CronService\CronReminders;

use App\Services\CronService\CronInterface;
use App\Models\Month;
use App\Models\Admin\FinancialYear;
use App\Services\BonusService;

class BonusRequestReminder implements CronInterface
{
    public static function handle($referenceId, $referenceType, $data)
    {
        $yearObj = FinancialYear::where('status','running')->first();
        
        if(!$yearObj)
            return false;

        $month = date('M');

        $monthObj = Month::where('financial_year_id',$yearObj->id)->where('month',$month)->first();

        if($monthObj) {
            BonusService::sendPendingReminder($monthObj->id);
        }
    }
}