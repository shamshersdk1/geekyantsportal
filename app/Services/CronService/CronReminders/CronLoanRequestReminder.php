<?php namespace App\Services\CronService\CronReminders;

use Carbon\Carbon;

use App\Services\CronService\CronInterface;

use App\Models\Admin\LoanRequest;
use App\Services\LoanRequestService;


class CronLoanRequestReminder implements CronInterface
{

    public static function handle($referenceId, $referenceType, $data)
    {
        $minutes = 1440; //24hrs
        $loanRequests = LoanRequest::whereIn('status',['pending','submitted','review','reconcile'])->where('created_at','<', Carbon::now()->subMinutes($minutes))->get();

        foreach ($loanRequests as $loanRequest) {
            LoanRequestService::sendReminder($loanRequest->id);
        }
    }
}