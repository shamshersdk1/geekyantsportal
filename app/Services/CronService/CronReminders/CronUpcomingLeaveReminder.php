<?php namespace App\Services\CronService\CronReminders;

use Carbon\Carbon;

use App\Services\CronService\CronInterface;

use App\Models\Admin\Leave;
use App\Services\LeaveService;
use App\Services\UserService;
use App\Jobs\MessagingApp\PostMessageChannelJob;


class CronUpcomingLeaveReminder implements CronInterface
{

    public static function handle($referenceId, $referenceType, $data)
    {
        $channelId = \Config::get('messaging-app.leave_channel');
        $nextFifthDate = date('Y-m-d', strtotime('+5 days'));
        $nextThirdDate = date('Y-m-d', strtotime('+3 days'));
        $tommorrow = date('Y-m-d', strtotime('+1 days'));
        $today = date('Y-m-d', strtotime('+0 days'));
        $display_text = '';
        // Get leaves for next 5 days
        $fiveDaysLeaves = Leave::whereDate('start_date', $nextFifthDate)->where('status','approved')->get();
        foreach( $fiveDaysLeaves as $fiveDaysLeave )
        {
            $display_text = 'after 5 days';
            $reportingManager = $fiveDaysLeave->user->reportingManager;
            if ( $reportingManager )
            {
                LeaveService::sendReminder($reportingManager->id, $fiveDaysLeave->user->name, $fiveDaysLeave->id, $display_text);
            }
            $teamLeads = UserService::getMyTeamLeads($fiveDaysLeave->user_id);
            if ( count($teamLeads) > 0 )
            {
                foreach( $teamLeads as $teamLead )
                {
                if ( $teamLead->id != $reportingManager->id )
                {
                    LeaveService::sendReminder($teamLead->id, $fiveDaysLeave->user->name, $fiveDaysLeave->id, $display_text);  
                }
                }
            } 
            $message = View::make('messaging-app.slack.leave.notify', ['user_name' => $fiveDaysLeave->user->name, 'leaveObj' => $fiveDaysLeave, 'display_text' => $display_text])->render();
            $job = (new PostMessageChannelJob($channelId, $message));
            dispatch($job);
        }
        // Get leaves for next 3 days
        $thirdDaysLeaves = Leave::whereDate('start_date', $nextThirdDate)->where('status','approved')->get();
        foreach( $thirdDaysLeaves as $thirdDaysLeave )
        {
            $display_text = 'after 3 days';
            $reportingManager = $thirdDaysLeave->user->reportingManager;
            if ( $reportingManager )
            {
                LeaveService::sendReminder($reportingManager->id, $thirdDaysLeave->user->name, $thirdDaysLeave->id, $display_text);
            }
            $teamLeads = UserService::getMyTeamLeads($thirdDaysLeave->user_id);
            if ( count($teamLeads) > 0 )
            {
                foreach( $teamLeads as $teamLead )
                {
                if ( $teamLead->id != $reportingManager->id )
                {
                    LeaveService::sendReminder($teamLead->id, $thirdDaysLeave->user->name, $thirdDaysLeave->id, $display_text);  
                }
                }
            } 
            $message = View::make('messaging-app.slack.leave.notify', ['user_name' => $thirdDaysLeave->user->name, 'leaveObj' => $thirdDaysLeave, 'display_text' => $display_text])->render();
            $job = (new PostMessageChannelJob($channelId, $message));
            dispatch($job);
        }

        // Get leaves for next day
        $nextDayLeaves = Leave::whereDate('start_date', $tommorrow)->where('status','approved')->get();
        foreach( $nextDayLeaves as $nextDayLeave )
        {
            $display_text = 'tomorrow';
            $reportingManager = $nextDayLeave->user->reportingManager;
            if ( $reportingManager )
            {
                LeaveService::sendReminder($reportingManager->id, $nextDayLeave->user->name, $nextDayLeave->id, $display_text);
            }
            $teamLeads = UserService::getMyTeamLeads($nextDayLeave->user_id);
            if ( count($teamLeads) > 0 )
            {
                foreach( $teamLeads as $teamLead )
                {
                if ( $teamLead->id != $reportingManager->id )
                {
                    LeaveService::sendReminder($teamLead->id, $nextDayLeave->user->name, $nextDayLeave->id, $display_text);  
                }
                }
            } 
            $message = View::make('messaging-app.slack.leave.notify', ['user_name' => $nextDayLeave->user->name, 'leaveObj' => $nextDayLeave, 'display_text' => $display_text])->render();
            $job = (new PostMessageChannelJob($channelId, $message));
            dispatch($job);
        }

        $todayLeaves = Leave::whereDate('start_date', $today)->where('status','approved')->get();
        foreach( $todayLeaves as $todayLeave )
        {
            $display_text = 'today';
            $reportingManager = $todayLeave->user->reportingManager;
            if ( $reportingManager )
            {
                LeaveService::sendReminder($reportingManager->id, $todayLeave->user->name, $todayLeave->id, $display_text);
            }
            $teamLeads = UserService::getMyTeamLeads($todayLeave->user_id);
            if ( count($teamLeads) > 0 )
            {
                foreach( $teamLeads as $teamLead )
                {
                if ( $teamLead->id != $reportingManager->id )
                {
                    LeaveService::sendReminder($teamLead->id, $todayLeave->user->name, $todayLeave->id, $display_text);  
                }
                }
            } 
            $message = View::make('messaging-app.slack.leave.notify', ['user_name' => $todayLeave->user->name, 'leaveObj' => $todayLeave, 'display_text' => $display_text])->render();
            $job = (new PostMessageChannelJob($channelId, $message));
            dispatch($job);
        }
    }
}