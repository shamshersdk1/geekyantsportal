<?php
namespace App\Services;

use App\Models\Admin\AdditionalWorkDaysBonus;
use App\Models\Admin\Calendar as Holiday;
use App\Models\Admin\Leave;
use App\Models\Admin\OnSiteBonus;
use App\Models\Admin\Project;
use App\Models\BonusRequest;
use App\Models\Month;
use App\Models\NonworkingCalendar;
use App\Models\User;
use App\Models\UserTimesheet;
use App\Models\User\UserTimesheetExtra;
use App\Models\User\UserTimesheetWeek;
use App\Models\Week;
use DB;
use App\Models\NonworkingCalender;

class TimesheetReportService
{
 public static function getMisProjectTimesheet($month)
 {
  return "Pending";
 }

 public static function getUserDayWiseExtraHour($start_date, $end_date, $userIds = 0)
 {
  $userId = implode(',', $userIds);
  $sql    = "SELECT ut.project_id,ut.user_id, ut.date, sum(duration) -8 as extra_hours, sum(duration) as total FROM `user_timesheets` ut WHERE ut.deleted_at is null and ut.user_id in ($userId) and ut.date BETWEEN ? and ? GROUP BY ut.date,ut.user_id having total > 8 order by date asc";

  $extraHour = DB::select($sql, [$start_date, $end_date]);

  return $extraHour;
 }

 public static function getUserTimesheet($userId, $date)
 {

  $userTimesheet = UserTimesheet::with('project')->where('user_id', $userId)->where('date', $date)->get();

  $data = [];

  if (count($userTimesheet) > 0) {
   foreach ($userTimesheet as $timesheet) {

    $tempTimesheet                            = [];
    $data[$timesheet->project->id]['name']    = $timesheet->project->project_name;
    $data[$timesheet->project->id]['project_id']    = $timesheet->project->id;
    $tempTimesheet['task']                    = $timesheet->task;
    $tempTimesheet['duration']                = $timesheet->duration;
    $data[$timesheet->project->id]['tasks'][] = $tempTimesheet;
   }
  }
  return array_values($data);

 }
 public static function getpendingRequests($userIds, $start_date, $end_date)
 {

    $data = [];
    $usersExtras = self::getUserDayWiseExtraHour($start_date, $end_date, $userIds);

    foreach ($usersExtras as $usersExtra) {
        $extraObj = UserTimesheetExtra::where('user_id', $usersExtra->user_id)->whereDate('date',$usersExtra->date)->first();
        if (!$extraObj && !empty($usersExtra->user_id)) {
            $data[$usersExtra->user_id]['dates'][$usersExtra->date]['extra'] = $usersExtra->extra_hours;
            $onLeave=Leave::isOnLeaveToday($usersExtra->user_id,$usersExtra->date);
            $userOnLeave=false;
            if($onLeave){
                $userOnLeave=true;
            }
            $data[$usersExtra->user_id]['dates'][$usersExtra->date]['on_leave'] = $userOnLeave;
            $data[$usersExtra->user_id]['dates'][$usersExtra->date]['is_weekend'] = NonworkingCalendar::isWeekend($usersExtra->date);
            $isHoliday=NonworkingCalendar::is_holiday($usersExtra->date);
            $holiday=false;
            if($isHoliday){
                $holiday=true;

            }
            $data[$usersExtra->user_id]['dates'][$usersExtra->date]['is_holiday'] = $holiday;
            $data[$usersExtra->user_id]['dates'][$usersExtra->date]['user_id'] = $usersExtra->user_id;
            $data[$usersExtra->user_id]['dates'][$usersExtra->date]['project_id'] = $usersExtra->project_id;
            $data[$usersExtra->user_id]['dates'][$usersExtra->date]['timesheet'] = self::getUserTimesheet($usersExtra->user_id, $usersExtra->date);
            $totalHours                                                 = self::getUserTotalHour($usersExtra->date, $usersExtra->date, $usersExtra->user_id);

            if (count($totalHours) > 0) {
                $data[$usersExtra->user_id]['dates'][$usersExtra->date]['total_hours'] = $totalHours[0]->total_hour;
                $data[$usersExtra->user_id]['name'] = User::find($usersExtra->user_id)->name;
                $data[$usersExtra->user_id]['id'] = User::find($usersExtra->user_id)->id;

            }
        }

    }
    $onsiteBonuses = self::getOnSiteBonusRequest($start_date, $end_date, $userIds);
    foreach ($onsiteBonuses as $onsiteBonus) {
        $data[$onsiteBonus->user_id]['dates'][$onsiteBonus->date]['onsite_bonus'] = $onsiteBonus;
        $onLeave=Leave::isOnLeaveToday($onsiteBonus->user_id,$onsiteBonus->date);
            $userOnLeave=false;
            if($onLeave){
                $userOnLeave=true;
            }
            $data[$onsiteBonus->user_id]['dates'][$onsiteBonus->date]['on_leave'] = $userOnLeave;
            $data[$onsiteBonus->user_id]['dates'][$onsiteBonus->date]['is_weekend'] = NonworkingCalendar::isWeekend($onsiteBonus->date);
            $isHoliday=NonworkingCalendar::is_holiday($onsiteBonus->date);
            $holiday=false;
            if($isHoliday){
                $holiday=true;

            }
            $data[$onsiteBonus->user_id]['dates'][$onsiteBonus->date]['is_holiday'] = $holiday;
        $data[$onsiteBonus->user_id]['dates'][$onsiteBonus->date]['user_id'] = $onsiteBonus->user_id;
        $data[$onsiteBonus->user_id]['dates'][$onsiteBonus->date]['timesheet']    = self::getUserTimesheet($onsiteBonus->user_id, $onsiteBonus->date);
        $totalHours                                                      = self::getUserTotalHour($onsiteBonus->date, $onsiteBonus->date, $onsiteBonus->user_id);

        if (count($totalHours) > 0) {
            $data[$onsiteBonus->user_id]['dates'][$onsiteBonus->date]['total_hours'] = $totalHours[0]->total_hour;
            $data[$onsiteBonus->user_id]['name'] = User::find($onsiteBonus->user_id)->name;
            $data[$usersExtra->user_id]['id'] = User::find($usersExtra->user_id)->id;

        }
    }
    $additionalBonuses = self::getAdditionalWorkDaysBonus($start_date, $end_date, $userIds);
    foreach ($additionalBonuses as $additionalBonus) {
        $data[$additionalBonus->user_id]['dates'][$additionalBonus->date]['additional_bonus'] = $additionalBonus;
        $onLeave=Leave::isOnLeaveToday($additionalBonus->user_id,$additionalBonus->date);
            $userOnLeave=false;
            if($onLeave){
                $userOnLeave=true;
            }
            $data[$additionalBonus->user_id]['dates'][$additionalBonus->date]['on_leave'] = $userOnLeave;
            $data[$additionalBonus->user_id]['dates'][$additionalBonus->date]['is_weekend'] = NonworkingCalendar::isWeekend($additionalBonus->date);
            $isHoliday=NonworkingCalendar::is_holiday($additionalBonus->date);
            $holiday=false;
            if($isHoliday){
                $holiday=true;

            }
            $data[$additionalBonus->user_id]['dates'][$additionalBonus->date]['is_holiday'] = $holiday;      
              $data[$additionalBonus->user_id]['dates'][$additionalBonus->date]['user_id'] = $additionalBonus->user_id;
        $data[$additionalBonus->user_id]['dates'][$additionalBonus->date]['timesheet']        = self::getUserTimesheet($additionalBonus->user_id, $additionalBonus->date);
        $totalHours                                                                  = self::getUserTotalHour($additionalBonus->date, $additionalBonus->date, $additionalBonus->user_id);

        if (count($totalHours) > 0) {
            $data[$additionalBonus->user_id]['dates'][$additionalBonus->date]['total_hours'] = $totalHours[0]->total_hour;
            $data[$additionalBonus->user_id]['name'] = User::find($additionalBonus->user_id)->name;
            $data[$usersExtra->user_id]['id'] = User::find($usersExtra->user_id)->id;

        }
    }

    return $data;
 }
 public static function getOnSiteBonusRequest($startdate, $enddate, $userIds = 0)
 {
  $onSiteBonus = OnSiteBonus::whereIn('user_id', $userIds)->whereBetween('date', [$startdate, $enddate])->where('status', 'pending')->get();
  return $onSiteBonus;
 }

 public static function getAdditionalWorkDaysBonus($startDate, $endDate, $userIds = 0)
 {
  $additionalBonus = AdditionalWorkDaysBonus::whereIn('user_id', $userIds)->whereBetween('date', [$startDate, $endDate])->where('status', 'pending')->get();
  return $additionalBonus;

 }

 public static function getEmployeeTimesheeetDetail($start_date, $end_date, $userIds = 0)
 {
  $single = false;
  if ($userIds != 0 && count($userIds) < 0) {
   $single = true;
  }
  $data['extra_hour'] = self::getUserExtraHour($start_date, $end_date, $userIds);
  $data['total_hour'] = self::getUserTotalHour($start_date, $end_date, $userIds);
  $response           = [];

  if ($single) {
   $response['user_id']    = !empty($data['total_hour'][0]) ? $data['total_hour'][0]->user_id : 0;
   $response['name']       = !empty($data['total_hour'][0]) ? $data['total_hour'][0]->name : 0;
   $response['total_hour'] = !empty($data['total_hour'][0]) ? $data['total_hour'][0]->total_hour : 0;
   $response['extra_hour'] = 0;
   if (!empty($data['extra_hour'])) {
    $response['extra_hour'] = $data['extra_hour'][0]->total_extra_hour;
   }

  } else {
   foreach ($data['total_hour'] as $totalHour) {
    $temp = [];
    $key  = array_search($totalHour->user_id, array_column($data['extra_hour'], 'user_id'));

    $temp['user_id']    = $totalHour->user_id;
    $temp['name']       = $totalHour->name;
    $temp['total_hour'] = $totalHour->total_hour;
    $temp['extra_hour'] = 0;

    if ($key > -1) {
     $temp['extra_hour'] = $data['extra_hour'][$key]->total_extra_hour;
    } else {
     $temp['extra_hour'] = 0;

    }
    $response[] = $temp;
  
}
  }

  return $response;

 }
 public static function getUserExtraHour($start_date, $end_date, $userIds = 0)
 {

  if ($userIds == 0) {

   $sql        = "SELECT u.name, master.user_id, SUM(extra_hours) as total_extra_hour from (SELECT ut.user_id, ut.date, sum(duration) -8 as extra_hours, sum(duration) as total FROM `user_timesheets` ut WHERE ut.deleted_at is null and  ut.date BETWEEN ? and ? GROUP BY ut.date,ut.user_id having total > 8) as master, users as u where u.id = master.user_id and u.is_active = 1 GROUP by user_id";
   $extra_hour = DB::select($sql, [$start_date, $end_date]);

  } else {
      $userId;
      if(count($userIds)>1){
        $userId= implode(',', $userIds);
        }
        else{
            $userId=$userIds;
        }
    // $userIds = join(",",$userIds);   
    $sql        = "SELECT u.name, master.user_id,  SUM(extra_hours) as total_extra_hour from (SELECT ut.user_id, ut.date, sum(duration) -8 as extra_hours, sum(duration) as total FROM `user_timesheets` ut WHERE ut.deleted_at is null and ut.user_id IN ($userId) and ut.date BETWEEN ? and ? GROUP BY ut.date,ut.user_id having total > 8) as master, users as u where u.id = master.user_id and u.is_active = 1 GROUP by user_id";
    $extra_hour = DB::select($sql, [$start_date, $end_date]);
  }

  return $extra_hour;

 }
 public static function getUserTotalHour($start_date, $end_date, $userIds = 0)
 {
  if ($userIds == 0) {
   $sql        = "SELECT u.name, master.user_id, sum(total) as total_hour from (SELECT ut.user_id, ut.date, sum(duration) -8 as extra_hours, sum(duration) as total FROM `user_timesheets` ut WHERE ut.deleted_at is null and  ut.date BETWEEN ? and ? GROUP BY ut.date,ut.user_id) as master, users as u where u.id = master.user_id and u.is_active = 1 GROUP by user_id";
   $total_hour = DB::select($sql, [$start_date, $end_date]);

  } else {
    $userId;
    if(count($userIds)>1){
      $userId= implode(',', $userIds);
      }
      else{
          $userId=$userIds;
      }
    // $userIds = join(",",$userIds);   
   $sql        = "SELECT u.name, master.user_id, sum(total) as total_hour from (SELECT ut.user_id, ut.date, sum(duration) -8 as extra_hours, sum(duration) as total FROM `user_timesheets` ut WHERE  ut.deleted_at is null and ut.user_id in ($userId) and ut.date BETWEEN ? and ? GROUP BY ut.date,ut.user_id) as master, users as u where u.id = master.user_id and u.is_active = 1 GROUP by user_id";
   $total_hour = DB::select($sql, [$start_date, $end_date]);
  
  }

  return (array) $total_hour;

 }
 public static function getProjectWiseTimesheetDetail($start_date, $end_date, $user_id)
 {
  $sql     = "SELECT master.user_id , u.project_name , master.total , master.extra_hours from (SELECT ut.user_id, ut.project_id, sum(duration) -8 as extra_hours, sum(duration) as total FROM `user_timesheets` ut WHERE ut.deleted_at is null and ut.user_id=? and   ut.date BETWEEN ? and ? GROUP BY ut.project_id,ut.user_id ) as master, projects as u where master.project_id =u.id";
  $project = DB::select($sql, [$user_id, $start_date, $end_date]);
  return (array) $project;
 }

 public static function getDayWiseUserTimesheeetDetail($userId, $start_date, $end_date)
 {
  $year = date('Y', strtotime($start_date));

  $month = date('m', strtotime($start_date));

  $monthObj = Month::where('year', $year)->where('month', $month)->first();

  if (!$monthObj) {
   return 0;
  }

//   $data['bonuses'] = BonusService::getUserBonusByMonthId($userId, $start_date, $end_date);
  $data['bonus_requests'] = BonusService::getUserBonusByMonth($userId, $start_date, $end_date);
//   echo"<pre>";
//   print_r($data['bonuses']);
//   die;
  $data['timesheets']             = self::getUserTimesheetReport($start_date, $end_date, $userId);
  $data['pending_bonuse_request'] = BonusRequest::where('month_id', $monthObj->id)->where('user_id', $userId)->where('status', 'pending')->count();

  return $data;

 }
 public static function getUserProjectWiseTotalHour($start_date, $end_date, $project_id)
 {

  $sql    = "SELECT master.user_id,us.name, u.project_name , master.total  from (SELECT  ut.user_id,ut.project_id, sum(duration) -8 as extra_hours, sum(duration) as total FROM `user_timesheets` ut WHERE ut.deleted_at is null and  ut.project_id=? and ut.date BETWEEN ? and ? GROUP BY ut.project_id,ut.user_id ) as master,users as us,projects as u where master.project_id =u.id and us.id=master.user_id";
  $result = DB::select($sql, [$project_id, $start_date, $end_date]);
  return (array) $result;
 }

 public static function getUserProjectWiseExtraHour($start_date, $end_date, $project_id)
 {

  $sql    = "SELECT u.name, master.user_id, SUM(extra_hours) as total_extra_hour from (SELECT ut.user_id, ut.date, sum(duration) -8 as extra_hours, sum(duration) as total FROM `user_timesheets` ut WHERE ut.deleted_at is null and ut.project_id=? and   ut.date BETWEEN ? and ? GROUP BY ut.date,ut.user_id having total > 8) as master, users as u where u.id = master.user_id and u.is_active = 1 GROUP by user_id";
  $result = DB::select($sql, [$project_id, $start_date, $end_date]);
  return (array) $result;
 }

 public static function getUserProjectWiseDetails($start_date, $end_date, $project_id)
 {

  $data['extra_hour'] = self::getUserProjectWiseExtraHour($start_date, $end_date, $project_id);
  $data['total_hour'] = self::getUserProjectWiseTotalHour($start_date, $end_date, $project_id);
  $response           = [];

  foreach ($data['total_hour'] as $totalHour) {
   $temp = [];
   $key  = array_search($totalHour->user_id, array_column($data['extra_hour'], 'user_id'));

   $temp['user_id']    = $totalHour->user_id;
   $temp['name']       = $totalHour->name;
   $temp['total_hour'] = $totalHour->total;
   $temp['extra_hour'] = 0;

   if ($key > -1) {
    $temp['extra_hour'] = $data['extra_hour'][$key]->total_extra_hour;
   } else {
    $temp['extra_hour'] = 0;

   }
   $response[] = $temp;

  }

  return $response;
 }

 public static function getProjectWise($project_id, $month)
 {
  $selectedMonth = Month::find($month);
  //Output Default
  $output                      = array();
  $output['success']           = false;
  $output['error']             = false;
  $output['error_message']     = '';
  $output['month_id']          = $selectedMonth->id;
  $output['month']             = $selectedMonth->month;
  $output['year']              = $selectedMonth->year;
  $output['project_id']        = $project_id;
  $output['month_string']      = '';
  $output['total_working_hrs'] = 0;

  //Validation
  if ($selectedMonth->month < 1 || $selectedMonth->month > 12) {
   $output['error']         = true;
   $output['error_message'] = "Invalid Month";
  } else {
   $output['month_string'] = \DateTime::createFromFormat('!m', $selectedMonth->month)->format('F');
  }

  $last_day_given_month = date('t', strtotime($output['month_string'] . ' 01, ' . $selectedMonth->year));
  $month_start_date     = date('Y-m-d', strtotime($output['month_string'] . ' 01, ' . $selectedMonth->year)); //First day of the month;
  $month_end_date       = date('Y-m-d', strtotime($output['month_string'] . ' ' . $last_day_given_month . ', ' . $selectedMonth->year)); // Last day of the month;

  $output['month_start_date'] = $month_start_date;
  $output['month_end_date']   = $month_end_date;

  //Business Logic
  if ($project_id !== null && $output['error'] === false) {
   $project = Project::where('id', $project_id)->with(['projectManager', 'bdManager', 'accountManager'])->first();
   if ($project) {
    $output['success'] = true;
    $output['project'] = $project;
    //Get User Timesheet for this day;
    $output['user_timesheet_old'] = UserTimesheet::where([
     ['project_id', '=', $project_id],
     ['date', '>=', $month_start_date],
     ['date', '<=', $month_end_date],
    ])->with(['user', 'review'])->orderBy('date', 'ASC')->orderBy('user_id', 'ASC')->get();

    $temp  = null;
    $count = null;
    foreach ($output['user_timesheet_old'] as $index => $timesheet) {
     $extraObj = UserTimesheetExtra::where('user_id', $timesheet->user_id)->where('project_id', $timesheet->project_id)->where('date', $timesheet->date)->first();
     if ((($temp ? $temp->date : null) == $timesheet->date) && (($temp ? $temp->user_id : null) == $timesheet->user_id)) {

      $count++;
      $timesheet->task     = ($temp ? $temp->task : null) . "<br>" . $timesheet->task;
      $timesheet->duration = ($temp ? $temp->duration : null) + $timesheet->duration;
      //$timesheet->extra_hours = ($temp ? $temp->extra_hours : null) + $timesheet->extra_hours;
      if ($extraObj) {
       $timesheet->extra_hour = $extraObj;
      }

      $timesheet->extra_approver_id = $timesheet->extra_approver_id;

      if ($timesheet->review) {
       $timesheet->review->approved_duration = ($temp->review ? $temp->review->approved_duration : null) + ($timesheet->review ? $timesheet->review->approved_duration : null);
      }

      $output['user_timesheet'][$index - $count] = $timesheet;
     } else {
      $count = 0;
      if ($extraObj) {
       $timesheet->extra_hour = $extraObj;
      }

      $output['user_timesheet'][$index] = $timesheet;
     }
     $temp = $timesheet;
    }
    $output['user_wise_timesheet'] = [];
    if (!empty($output['user_timesheet'])) {
     foreach ($output['user_timesheet'] as $timesheet) {
      $output['user_wise_timesheet'][$timesheet->user_id][] = $timesheet;
     }
    }

    $output['total_approved_hrs']  = 0;
    $output['total_employees_hrs'] = 0;

    $output['user_timesheet_summary'] = [];
    foreach ($output['user_wise_timesheet'] as $timesheet_user_id => $timesheet) {
     $user_summary                 = array();
     $user_summary['user']         = $timesheet[0]->user;
     $user_summary['approved_hrs'] = 0;
     $user_summary['employee_hrs'] = 0;
     foreach ($timesheet as $entry) {
      if (($entry->review ? $entry->review->approver_id : null) > 0) {
       $user_summary['approved_hrs'] += $entry->review->approved_duration;
       $output['total_approved_hrs'] += $entry->review->approved_duration;
      }
      $user_summary['employee_hrs'] += $entry->duration;
      $output['total_employees_hrs'] += $entry->duration;
      $user_summary['employee_extra_hour'] = 0;
      $extraHours                          = UserTimesheetExtra::where('user_id', $entry->user_id)->whereBetween('date', [$month_start_date, $month_end_date])->sum('extra_hours');

      if ($extraHours > 0) {
       $user_summary['employee_extra_hour'] = $extraHours;
      }
     }

     $output['user_timesheet_summary'][] = $user_summary;
    }
   } else {
    $output['error']         = true;
    $output['error_message'] = "Unable to find project id";
   }

  } else {
   $output['error']         = true;
   $output['error_message'] = "Missing Project ID";
  }

  return $output;

 }

 public static function getEmployeeWiseTimesheetSummary($user_id, $month)
 {
  $selectedMonth = Month::find($month);

  $month_string         = \DateTime::createFromFormat('!m', $selectedMonth->month)->format('F');
  $last_day_given_month = date('t', strtotime($month_string . ' 01, ' . $selectedMonth->year));
  $month_start_date     = date('Y-m-d', strtotime($month_string . ' 01, ' . $selectedMonth->year));
  $month_end_date       = date('Y-m-d', strtotime($month_string . ' ' . $last_day_given_month . ', ' . $selectedMonth->year));

  $output['user_timesheet'] = UserTimesheet::where([
   ['user_id', '=', $user_id],
   ['date', '>=', $month_start_date],
   ['date', '<=', $month_end_date],
  ])->with(['project', 'review'])->get();

  $approved_total_time = 0;
  $employee_total_time = 0;

  foreach ($output['user_timesheet'] as $user_timesheet) {
   if ($user_timesheet->review ? $user_timesheet->review->approver_id : null > 0) {
    $approved_total_time += $user_timesheet->review->approved_duration;
   }
   $employee_total_time += $user_timesheet->duration;
  }
  return ['approved_hrs' => $approved_total_time, 'employee_hrs' => $employee_total_time];
 }

 public static function getEmployeeWise($employee_id, $monthId)
 {

  //Output Default
  $output                      = array();
  $output['success']           = 0;
  $output['error']             = false;
  $output['error_message']     = '';
  $output['employee_id']       = $employee_id;
  $output['month_string']      = '';
  $output['total_working_hrs'] = 0;

  $monthObj = Month::find($monthId);
  if (!$monthObj) {
   return $output;
  }
  $output['month'] = $monthObj->month;

  $output['month_string'] = $monthObj->formatMonth();
  $last_day_given_month   = date('t', strtotime($output['month_string'] . ' 01, ' . date('Y')));
  $monthEndDate           = $monthObj->getLastDay();
  $month_start_date       = $monthObj->getFirstDay();
  $month_end_date         = $monthEndDate; //date('Y-m-d', strtotime($output['month_string'] . ' ' . $last_day_given_month . ', ' . date('Y'))); // Last day of the month;

  $output['month_start_date'] = $month_start_date;
  $output['month_end_date']   = $month_end_date;

  //Business Logic
  if ($employee_id !== null && $output['error'] === false) {
   $user = User::where('employee_id', $employee_id)->first();
   if ($user) {
    $output['success'] = 1;
    $output['user']    = $user;
    //Get User Timesheet for given month;
    $output['user_timesheet'] = UserTimesheet::where([
     ['user_id', '=', $user->id],
     ['date', '>=', $month_start_date],
     ['date', '<=', $month_end_date],
    ])->with(['project', 'review'])->get();

    //Project Mapping
    $project_map = [];
    foreach ($output['user_timesheet'] as $user_timesheet) {
     if (!isset($project_map[$user_timesheet->project_id])) {
      $project_map[$user_timesheet->project_id] = array("project" => $user_timesheet->project, "approved_total_time" => 0, "employee_total_time" => 0);
     }
     if ($user_timesheet->approved_by > 0) {
      $project_map[$user_timesheet->project_id]['approved_total_time'] += $user_timesheet->review->approved_duration;
     }
     $project_map[$user_timesheet->project_id]['employee_total_time'] += $user_timesheet->duration;
    }

    //Date wise Mapping
    $output['project_map']     = $project_map;
    $grant_approved_total_time = 0;
    $grant_employee_total_time = 0;
    $date_map                  = [];
    for ($i = 1; $i <= $last_day_given_month; $i++) {
     $date_key            = date('Y') . "-" . $output['month'] . "-" . $i;
     $date_key            = date('Y-m-d', strtotime($date_key));
     $date_map[$date_key] = array('projects' => [], 'approved_total_time' => 0, 'employee_total_time' => 0, 'leave' => null, 'is_weekend' => false, 'is_holiday' => null);

     $date_map[$date_key]['is_holiday'] = NonworkingCalendar::is_holiday($date_key);
     $date_map[$date_key]['leave']      = Leave::isOnLeaveToday($user->id, $date_key);
     if (date('D', strtotime($date_key)) == 'Sat' || date('D', strtotime($date_key)) == 'Sun') {
      $date_map[$date_key]['is_weekend'] = true;
     }
     $approved_total_time = 0;
     $employee_total_time = 0;
     foreach ($project_map as $project_key => $project_value) {
      $date_map[$date_key]['projects'][$project_key] = [];
      foreach ($output['user_timesheet'] as $user_timesheet) {
       if ($user_timesheet->project_id == $project_key && strtotime($user_timesheet->date) == strtotime($date_key)) {
        $date_map[$date_key]['projects'][$project_key][] = $user_timesheet;

        if ($user_timesheet->approved_by > 0) {
         $grant_approved_total_time += $user_timesheet->review->approved_duration;
         $approved_total_time += $user_timesheet->review->approved_duration;
        }

        $grant_employee_total_time += $user_timesheet->duration;
        $employee_total_time += $user_timesheet->duration;
       }
      }
     }

     $date_map[$date_key]['approved_total_time'] = $approved_total_time;
     $date_map[$date_key]['employee_total_time'] = $employee_total_time;

    }

    $output['date_map']                  = $date_map;
    $output['grant_employee_total_time'] = $grant_employee_total_time;
    $output['grant_approved_total_time'] = $grant_approved_total_time;

    //Calculation for total working days & hrs in this month.
    $total_working_hrs  = 0;
    $total_working_days = 0;
    foreach ($output['date_map'] as $day) {
     if ($day['is_holiday'] !== false) {
      continue;
     }

     if ($day['is_weekend'] === true) {
      continue;
     }

     $total_working_hrs += 8;
     $total_working_days++;
    }
    $output['total_working_hrs']  = $total_working_hrs;
    $output['total_working_days'] = $total_working_days;

   } else {
    $output['error']         = true;
    $output['error_message'] = "Unable to find employee id";
   }

  }

  return $output;
 }
 public static function getUserTimesheetReport($startDate, $endDate, $userId)
 {
  $userObj = User::find($userId);

  $project_map = [];

  $userTimesheetProjects = UserTimesheet::where('user_id', $userId)
   ->where('date', '>=', $startDate)
   ->where('date', '<=', $endDate)
   ->groupBy('project_id')
   ->get();

  $startCondition = $startDate;
  $endCondition   = $endDate;
  $amount         = 0;
  $lop_counter    = 0;
  //$days[] = $weekStartDate;
  while (strtotime($startCondition) <= strtotime($endCondition)) {
   //$startCondition = date("Y-m-d", strtotime("+1 days", strtotime($startCondition)));
   $data['days'][] = [
    'date'       => $startCondition,
    'day'        => "s", //$day,
    'is_weekend' => (date('N', strtotime($startCondition)) >= 6) ? 'true' : 'false',
    'is_holiday' => (Holiday::whereDate('date', $startCondition)->count() > 0) ? 'true' : 'false',
    'holiday'    => Holiday::whereDate('date', $startCondition)->first(),
   ];
   $startCondition = date("Y-m-d", strtotime("+1 days", strtotime($startCondition)));
  }

  $data['user_id']    = $userObj->id;
  $data['name']       = $userObj->name;
  $data['start_date'] = $endCondition;
  $data['end_date']   = $endCondition;
  // $data['user_monthly_timesheet'] = $userTimesheetWeek;

  foreach ($data['days'] as $index => $day) {
   if ($day) {
    $daysData = [];
    $response = Leave::isOnLeaveToday($userObj->id, $day['date']);
    if ($response) {
     $data['days'][$index]['on_leave'] = 'true';
    } else {
     $data['days'][$index]['on_leave'] = 'false';
    };
    $data['days'][$index]['approve_extra_hour'] = false; //$userTimesheetExtra->extra_hours;
    $data['days'][$index]['extra_hours']        = 0;

    $userTimesheets = UserTimesheet::with('review', 'project')->where('user_id', $userObj->id)->where('date', $day['date'])->groupBy('project_id')->get();

    if (count($userTimesheets) > 0) {
     foreach ($userTimesheets as $userTimesheet) {

      $projectObj           = Project::find($userTimesheet->project_id);
      $projectUserTimesheet = UserTimesheet::with('review')->where('project_id', $projectObj->id)->where('user_id', $userTimesheet->user_id)->where('date', $day['date'])->get();

      $projectData['id']              = $projectObj->id;
      $projectData['project_name']    = $projectObj->project_name;
      $projectData['user_timesheets'] = $projectUserTimesheet;

      $data['days'][$index]['projects'][]          = $projectData;
      $total                                       = UserTimesheet::where('user_id', $userObj->id)->where('date', $day['date'])->sum('duration');
      $data['days'][$index]['approved_total_time'] = $total;
      $data['days'][$index]['employee_total_time'] = $total;

     }
     $data['days'][$index]['extra_hour'] = null;
     if (!empty($data['days'][$index]['employee_total_time'])) {

        
      $userTimesheetExtra = UserTimesheetExtra::with('approver')->where('user_id', $userObj->id)->where('date', $day['date'])->first();
      if ($userTimesheetExtra) {
        
        $data['days'][$index]['approve_extra_hour'] = true; //$userTimesheetExtra->extra_hours;
        $data['days'][$index]['extra_hour']         = $userTimesheetExtra->toArray();
        $data['days'][$index]['extra_hours']        = $userTimesheetExtra->extra_hours;
        $data['days'][$index]['reviewed_at']        = $userTimesheetExtra->created_at;
 
        $data['days'][$index]['project_id'] = $userTimesheetExtra->project_id;
      } elseif ($data['days'][$index]['employee_total_time'] > 8) {
       if (count($userTimesheets) == 1) {
        $data['days'][$index]['extra_hour']['project_id'] = $userTimesheet->project_id;
       }

       $data['days'][$index]['extra_hour']['extra_hours'] = $userTimesheet->project_id;
       $data['days'][$index]['approve_extra_hour']        = false;
       $data['days'][$index]['approved_total_time']       = 8;
       $data['days'][$index]['extra_hour']['extra_hours'] = $data['days'][$index]['employee_total_time'] - 8;

      }
     }
    }
    $data['days'][$index]['onsite']=OnSiteBonus::with('reviewer')->where('user_id',$userObj->id)->where('date',$day['date'])->first();
    $data['days'][$index]['additional']=AdditionalWorkDaysBonus::with('projects','reviewer')->where('user_id',$userObj->id)->where('date',$day['date'])->first();

  //   $bonusRequests = BonusRequest::with('approvedBy', 'approvedBonus.approver')
  //    ->where('user_id', $userObj->id)
  //    ->where('date', $day['date'])
  //    ->whereIn('type', ['additional', 'onsite'])
  //    ->whereIn('status', ['pending', 'approved', 'rejected'])
  //    ->get();
  //   if (count($bonusRequests) > 0) {
  //    foreach ($bonusRequests as $bonusRequest) {
  //     $br                                       = $bonusRequest->toArray();
  //     $br['redeem_info']                        = json_decode($bonusRequest->redeem_type);
  //     $data['days'][$index]['bonus_requests'][] = $br;
  //    }
  //   }
   }
  }
  // $data['total_extra_hour'] = UserTimesheetExtra::where('user_id', $userObj->id)->whereBetween('date', [$startDate, $endDate])->sum('extra_hours');

  // $extraHours                  = 0;
  // $bonusRequest                = BonusRequest::where('user_id', $userId)->whereBetween('date', [$startDate, $endDate])->where('type', 'extra')->first();
  // $data['is_applied']          = false;
  // $data['extra_bonus_request'] = null;
  // $data['extra_hours']         = 0;

  // if ($bonusRequest) {
  //  $extraHours                  = json_decode($bonusRequest->redeem_type);
  //  $data['is_applied']          = true;
  //  $data['extra_bonus_request'] = $bonusRequest;
  //  $data['extra_hours']         = $extraHours->hours;
  // }

  $data['project_map'] = $project_map;

  $data['expected_hours'] = 10;

  // $data['extra_hours']         = UserTimesheetExtra::where('user_id', $userId)->whereBetween('date', [$startDate, $endDate])->sum('extra_hours');
  // $data['extra_hours_details'] = UserTimesheetExtra::where('user_id', $userId)->whereBetween('date', [$startDate, $endDate])->get();

  // echo"<pre>";
  // print_r($data);
  // die;
  return $data;
  }
 // public static function getUserTimesheetReport($monthId, $userId)
 // {

 //     $monthObj = Month::find($monthId);
 //     $userObj = User::find($userId);

 //     if (!$monthObj) {
 //         return false;
 //     }

 //     $project_map = [];
 //     $month_start_date = $monthObj->getFirstDay();
 //     $month_end_date = $monthObj->getLastDay();

 //     $userMonthlyTimesheet = UserMonthlyTimesheet::where('user_id', $userId)->where('month_id', $monthId)->first();
 //     if (!$userMonthlyTimesheet) {
 //         $userTimesheet = new UserMonthlyTimesheet;
 //         $userTimesheet->user_id = $userId;
 //         $userTimesheet->month_id = $monthId;
 //         $userTimesheet->created_by = $userId;
 //         $userTimesheet->status = 'pending';
 //         $userTimesheet->save();
 //         $userMonthlyTimesheet = UserMonthlyTimesheet::where('user_id', $userId)->where('month_id', $monthId)->first();
 //     }

 //     $userTimesheetProjects = UserTimesheet::where('user_id', $userObj->id)
 //         ->where('date', '>=', $month_start_date)
 //         ->where('date', '<=', $month_end_date)
 //         ->groupBy('project_id')
 //         ->get();
 //     foreach ($userTimesheetProjects as $userTimesheetProject) {
 //         $project_map[$userTimesheetProject->project_id] = array("project" => $userTimesheetProject->project, "approved_total_time" => 0, "employee_total_time" => 0);
 //     }

 //     $obj = new Calendar();
 //     $data = $obj->getCalendar($monthObj->month, $monthObj->year);
 //     $data['user_id'] = $userObj->id;
 //     $data['name'] = $userObj->name;
 //     $data['month_id'] = $monthObj->id;
 //     $data['end_date'] = $monthObj->getLastDay();
 //     $data['end_date'] = $monthObj->getLastDay();
 //     $data['user_monthly_timesheet'] = $userMonthlyTimesheet;

 //     foreach ($data['days'] as $index => $day) {
 //         if ($day) {
 //             $daysData = [];
 //             $response = Leave::isOnLeaveToday($userObj->id, $day['date']);
 //             if ($response) {
 //                 $data['days'][$index]['on_leave'] = 'true';
 //             } else {
 //                 $data['days'][$index]['on_leave'] = 'false';
 //             };
 //             $data['days'][$index]['approve_extra_hour'] = false; //$userTimesheetExtra->extra_hours;
 //             $data['days'][$index]['extra_hours'] = 0;

 //             $userTimesheets = UserTimesheet::with('review', 'project')->where('user_id', $userObj->id)->where('date', $day['date'])->groupBy('project_id')->get();

 //             if (count($userTimesheets) > 0) {
 //                 // $userTimesheets = UserTimesheet::with('review','project')->where('user_id',$userObj->id)->where('date',$day['date'])->groupBy('project_id')->get();

 //                 foreach ($userTimesheets as $userTimesheet) {

 //                     $projectObj = Project::find($userTimesheet->project_id);
 //                     $projectUserTimesheet = UserTimesheet::with('review')->where('project_id', $projectObj->id)->where('user_id', $userTimesheet->user_id)->where('date', $day['date'])->get();

 //                     $projectData['id'] = $projectObj->id;
 //                     $projectData['project_name'] = $projectObj->project_name;
 //                     $projectData['user_timesheets'] = $projectUserTimesheet;

 //                     $data['days'][$index]['projects'][] = $projectData;
 //                     // $additionalBonusRequest = BonusRequest::with('approvedBy','approvedBonus.approver')->where('user_id',$userTimesheet->user_id)->where('date',$day['date'])->whereIn('type',['additional','onsite'])->whereIn('status',['pending','approved','rejected'])->get();
 //                     // if($additionalBonusRequest) {
 //                     //     $data['days'][$index]['bonus_requests'] = $additionalBonusRequest;
 //                     //     //$data['days'][$index]['additional_bonus_request']->redeem_type = json_decode($additionalBonusRequest->redeem_type)  ;
 //                     // }
 //                     // $onsiteBonusRequest = BonusRequest::with('approvedBy')->where('user_id',$userTimesheet->user_id)->where('date',$day['date'])->where('type','onsite')->whereIn('status',['pending','approved','rejected'])->get();
 //                     // if($onsiteBonusRequest) {
 //                     //     //$data['days'][$index]['bonus_request'] = $onsiteBonusRequest;
 //                     //     //$data['days'][$index]['bonus_request']->redeem_type = json_decode($onsiteBonusRequest->redeem_type)  ;
 //                     // }

 //                     // if(!isset($data['days'][$index]['approved_total_time'])) {

 //                     //     $data['days'][$index]['approved_total_time'] = 0;
 //                     //     $data['days'][$index]['employee_total_time'] = 0;//$total;

 //                     // }
 //                     $total = UserTimesheet::where('user_id', $userObj->id)->where('date', $day['date'])->sum('duration');
 //                     $data['days'][$index]['approved_total_time'] = $total;
 //                     $data['days'][$index]['employee_total_time'] = $total;

 //                 }
 //                 $data['days'][$index]['extra_hour'] = null;
 //                 if (!empty($data['days'][$index]['employee_total_time'])) {
 //                     $userTimesheetExtra = UserTimesheetExtra::where('user_id', $userObj->id)->where('date', $day['date'])->first();
 //                     if ($userTimesheetExtra) {
 //                         $data['days'][$index]['approve_extra_hour'] = true; //$userTimesheetExtra->extra_hours;
 //                         $data['days'][$index]['extra_hour'] = $userTimesheetExtra;
 //                         $data['days'][$index]['extra_hours'] = $userTimesheetExtra->extra_hours;
 //                         $data['days'][$index]['project_id'] = $userTimesheetExtra->project_id;
 //                     } elseif ($data['days'][$index]['employee_total_time'] > 8) {
 //                         if (count($userTimesheets) == 1) {
 //                             $data['days'][$index]['extra_hour']['project_id'] = $userTimesheet->project_id;
 //                         }

 //                         $data['days'][$index]['extra_hour']['extra_hours'] = $userTimesheet->project_id;
 //                         $data['days'][$index]['approve_extra_hour'] = false;
 //                         $data['days'][$index]['approved_total_time'] = 8;
 //                         $data['days'][$index]['extra_hour']['extra_hours'] = $data['days'][$index]['employee_total_time'] - 8;

 //                     }
 //                 }
 //             }

 //             $bonusRequests = BonusRequest::with('approvedBy', 'approvedBonus.approver')
 //                 ->where('user_id', $userObj->id)
 //                 ->where('date', $day['date'])
 //                 ->whereIn('type', ['additional', 'onsite'])
 //                 ->whereIn('status', ['pending', 'approved', 'rejected'])
 //                 ->get();
 //             if (count($bonusRequests) > 0) {
 //                 foreach ($bonusRequests as $bonusRequest) {
 //                     $br = $bonusRequest->toArray();
 //                     $br['redeem_info'] = json_decode($bonusRequest->redeem_type);
 //                     $data['days'][$index]['bonus_requests'][] = $br;
 //                 }

 //                 //$data['days'][$index]['additional_bonus_request']->redeem_type = json_decode($additionalBonusRequest->redeem_type)  ;
 //             }
 //             // foreach ($project_map as $projectId => $project) {
 //             //     $userTimesheets = UserTimesheet::with('review','project')->where('user_id',$userObj->id)->where('date',$day['date'])->where('project_id', $projectId)->get();

 //             //         $data['days'][$index]['projects'][$projectId] = $userTimesheets;

 //             //     if (count($userTimesheets) > 0 ) {
 //             //         foreach ($userTimesheets as $userTimesheet) {
 //             //             if(!isset($data['days'][$index]['approved_total_time'])) {
 //             //                 $data['days'][$index]['approved_total_time'] = 0;
 //             //                 $data['days'][$index]['employee_total_time'] = 0;
 //             //             }
 //             //             $data['days'][$index]['approved_total_time'] += $userTimesheet->review->approved_duration;
 //             //             $data['days'][$index]['employee_total_time'] += $userTimesheet->duration;

 //             //         }
 //             //         if(!empty($data['days'][$index]['employee_total_time'])) {
 //             //             $userTimesheetExtra = UserTimesheetExtra::where('user_id',$userObj->id)->where('date',$day['date'])->first();
 //             //             if($userTimesheetExtra) {
 //             //                 $data['days'][$index]['approve_extra_hour'] = true; //$userTimesheetExtra->extra_hours;
 //             //                 $data['days'][$index]['extra_hour'] =  $userTimesheetExtra;
 //             //                 $data['days'][$index]['extra_hours'] =  $userTimesheetExtra->extra_hours;
 //             //                 $data['days'][$index]['project_id'] =  $userTimesheetExtra->project_id;
 //             //             } elseif($data['days'][$index]['employee_total_time'] > 10) {
 //             //                 $data['days'][$index]['approve_extra_hour'] = false;
 //             //                 $data['days'][$index]['approved_total_time'] = 8;
 //             //                 $data['days'][$index]['extra_hours'] =  $data['days'][$index]['employee_total_time'] - 8;

 //             //             }
 //             //         }
 //             //     }
 //             // }
 //         }
 //     }
 //     $data['total_extra_hour'] = $userTimesheetExtra = UserTimesheetExtra::where('user_id', $userObj->id)->where('month_id', $monthId)->sum('extra_hours');

 //     $data['user_monthly_timesheet'] = UserMonthlyTimesheet::with('approver')->where('user_id', $userId)->where('month_id', $monthId)->first();

 //     $extraHours = 0;
 //     $bonusRequest = BonusRequest::where('user_id', $userId)->where('month_id', $monthId)->where('type', 'extra')->first();
 //     $data['is_applied'] = false;
 //     $data['extra_bonus_request'] = null;
 //     $data['extra_hours'] = 0;

 //     if ($bonusRequest) {
 //         $extraHours = json_decode($bonusRequest->redeem_type);
 //         $data['is_applied'] = true;
 //         $data['extra_bonus_request'] = $bonusRequest;
 //         $data['extra_hours'] = $extraHours->hours;
 //     }
 //     //$data['extra_hour_bonus'] = $extraHours->days;

 //     $data['project_map'] = $project_map;
 //     $data['can_approve'] = !$monthObj->timesheet;
 //     // $data['grant_approved_total_time'] = self::getUserApproveHours($userId, $monthId);
 //     $data['grant_employee_total_time'] = self::getUserEmloyeeHours($userId, $monthId);
 //     return $data; 
 // }
  
 public static function getUserWeeklyTimesheet($weekId, $userId)
 {

  $weekObj = Week::find($weekId);
  $userObj = User::find($userId);

  if (!$weekObj) {
   return false;
  }

  $project_map   = [];
  $weekStartDate = $weekObj->start_date;
  $weekEndDate   = $weekObj->end_date;

  $userTimesheetWeek = UserTimesheetWeek::where('user_id', $userId)->where('week_id', $weekId)->first();
  if (!$userTimesheetWeek) {
   $userTimesheet          = new UserTimesheetWeek;
   $userTimesheet->user_id = $userId;
   $userTimesheet->week_id = $weekId;
   //$userTimesheet->created_by = $userId;
   $userTimesheet->status = 'pending';
   $userTimesheet->save();
   $userTimesheetWeek = UserTimesheetWeek::where('user_id', $userId)->where('week_id', $weekId)->first();
  }

  $userTimesheetProjects = UserTimesheet::where('user_id', $userObj->id)
   ->where('date', '>=', $weekStartDate)
   ->where('date', '<=', $weekEndDate)
   ->groupBy('project_id')
   ->get();

  // foreach ($userTimesheetProjects as $userTimesheetProject) {
  //     $project_map[$userTimesheetProject->project_id] = array("project" => $userTimesheetProject->project, "approved_total_time" => 0, "employee_total_time" => 0);
  // }

  // $obj = new Calendar();
  // $data = $obj->getCalendar($monthObj->month, $monthObj->year);
  /*
  $start_condition = strtotime($leaveDeduction->leave->start_date) < strtotime($leaveDeduction->month->getFirstDay()) ? $leaveDeduction->month->getFirstDay() : $leaveDeduction->leave->start_date;
  $end_condition = strtotime($leaveDeduction->leave->end_date) > strtotime($leaveDeduction->month->getLastDay()) ? $leaveDeduction->month->getLastDay() : $leaveDeduction->leave->end_date;
  $amount = 0;
  $lop_counter = 0;
  while (strtotime($start_condition) <= strtotime($end_condition)) {
  if (date("m", strtotime($start_condition)) == ($leaveDeduction->month ? $leaveDeduction->month->month : null) && (!(NonworkingCalendar::is_holiday($start_condition) || NonworkingCalendar::isWeekend($start_condition)))) {
  $salary = Appraisal::getOneDaySalary($leaveDeduction->leave->user_id, $start_condition);
  $amount += $salary;
  $lop_counter += 1;
  }
  $start_condition = date("Y-m-d", strtotime("+1 days", strtotime($start_condition)));
  }
   */
  $startCondition = $weekStartDate;
  $endCondition   = $weekEndDate;
  $amount         = 0;
  $lop_counter    = 0;
  //$days[] = $weekStartDate;
  while (strtotime($startCondition) <= strtotime($endCondition)) {
   //$startCondition = date("Y-m-d", strtotime("+1 days", strtotime($startCondition)));
   $data['days'][] = [
    'date'       => $startCondition,
    'day'        => "s", //$day,
    'is_weekend' => (date('N', strtotime($startCondition)) >= 6) ? 'true' : 'false',
    'is_holiday' => (Holiday::whereDate('date', $startCondition)->count() > 0) ? 'true' : 'false',
    'holiday'    => Holiday::whereDate('date', $startCondition)->first(),
   ];
   $startCondition = date("Y-m-d", strtotime("+1 days", strtotime($startCondition)));
  }

  $data['user_id'] = $userObj->id;
  $data['name']    = $userObj->name;
  //$data['month_id'] = $monthObj->id;
  $data['week_id']                = $weekObj->id;
  $data['end_date']               = $startCondition;
  $data['end_date']               = $endCondition;
  $data['user_monthly_timesheet'] = $userTimesheetWeek;

  foreach ($data['days'] as $index => $day) {
   if ($day) {
    $daysData = [];
    $response = Leave::isOnLeaveToday($userObj->id, $day['date']);
    if ($response) {
     $data['days'][$index]['on_leave'] = 'true';
    } else {
     $data['days'][$index]['on_leave'] = 'false';
    };
    $data['days'][$index]['approve_extra_hour'] = false; //$userTimesheetExtra->extra_hours;
    $data['days'][$index]['extra_hours']        = 0;

    $userTimesheets = UserTimesheet::with('review', 'project')->where('user_id', $userObj->id)->where('date', $day['date'])->groupBy('project_id')->get();

    if (count($userTimesheets) > 0) {
     // $userTimesheets = UserTimesheet::with('review','project')->where('user_id',$userObj->id)->where('date',$day['date'])->groupBy('project_id')->get();

     foreach ($userTimesheets as $userTimesheet) {

      $projectObj           = Project::find($userTimesheet->project_id);
      $projectUserTimesheet = UserTimesheet::with('review')->where('project_id', $projectObj->id)->where('user_id', $userTimesheet->user_id)->where('date', $day['date'])->get();

      $projectData['id']              = $projectObj->id;
      $projectData['project_name']    = $projectObj->project_name;
      $projectData['user_timesheets'] = $projectUserTimesheet;

      $data['days'][$index]['projects'][] = $projectData;

      $total                                       = UserTimesheet::where('user_id', $userObj->id)->where('date', $day['date'])->sum('duration');
      $data['days'][$index]['approved_total_time'] = $total;
      $data['days'][$index]['employee_total_time'] = $total;

     }
     $data['days'][$index]['extra_hour'] = null;
     if (!empty($data['days'][$index]['employee_total_time'])) {
      $userTimesheetExtra = UserTimesheetExtra::with('approver')->where('user_id', $userObj->id)->where('date', $day['date'])->first();
      if ($userTimesheetExtra) {
       $data['days'][$index]['approve_extra_hour'] = true; //$userTimesheetExtra->extra_hours;
       $data['days'][$index]['extra_hour']         = $userTimesheetExtra;
       $data['days'][$index]['extra_hours']        = $userTimesheetExtra->extra_hours;
       $data['days'][$index]['project_id']         = $userTimesheetExtra->project_id;
      } elseif ($data['days'][$index]['employee_total_time'] > 8) {
       if (count($userTimesheets) == 1) {
        $data['days'][$index]['extra_hour']['project_id'] = $userTimesheet->project_id;
       }

       $data['days'][$index]['extra_hour']['extra_hours'] = $userTimesheet->project_id;
       $data['days'][$index]['approve_extra_hour']        = false;
       $data['days'][$index]['approved_total_time']       = 8;
       $data['days'][$index]['extra_hour']['extra_hours'] = $data['days'][$index]['employee_total_time'] - 8;

      }
     }
    }

    $bonusRequests = BonusRequest::with('approvedBy', 'approvedBonus.approver')
     ->where('user_id', $userObj->id)
     ->where('date', $day['date'])
     ->whereIn('type', ['additional', 'onsite'])
     ->whereIn('status', ['pending', 'approved', 'rejected'])
     ->get();
    if (count($bonusRequests) > 0) {
     foreach ($bonusRequests as $bonusRequest) {
      $br                                       = $bonusRequest->toArray();
      $br['redeem_info']                        = json_decode($bonusRequest->redeem_type);
      $data['days'][$index]['bonus_requests'][] = $br;
     }

     //$data['days'][$index]['additional_bonus_request']->redeem_type = json_decode($additionalBonusRequest->redeem_type)  ;
    }

   }
  }
  //$data['total_extra_hour'] = $userTimesheetExtra = UserTimesheetExtra::where('user_id', $userObj->id)->where('month_id', $monthId)->sum('extra_hours');
  $data['total_extra_hour'] = UserTimesheetExtra::where('user_id', $userObj->id)->whereBetween('date', [$weekStartDate, $weekEndDate])->sum('extra_hours');

  //$data['user_monthly_timesheet'] = UserMonthlyTimesheet::with('approver')->where('user_id', $userId)->where('month_id', $monthId)->first();
  $data['user_weekly_timesheet'] = UserTimesheetWeek::with('approver')->where('user_id', $userId)->where('week_id', $weekId)->first();

  $extraHours                  = 0;
  $bonusRequest                = BonusRequest::where('user_id', $userId)->whereBetween('date', [$weekStartDate, $weekEndDate])->where('type', 'extra')->first();
  $data['is_applied']          = false;
  $data['extra_bonus_request'] = null;
  $data['extra_hours']         = 0;

  if ($bonusRequest) {
   $extraHours                  = json_decode($bonusRequest->redeem_type);
   $data['is_applied']          = true;
   $data['extra_bonus_request'] = $bonusRequest;
   $data['extra_hours']         = $extraHours->hours;
  }

  $data['project_map'] = $project_map;

  $data['expected_hours'] = 10;
  $data['employee_hours'] = self::getUserEmloyeeWeekHours($userId, $weekId);
  // $data['approved_hours'] = self::getUserApproveWeekHours($userId, $weekId);

  $data['extra_hours']         = UserTimesheetExtra::where('user_id', $userId)->whereBetween('date', [$weekStartDate, $weekEndDate])->sum('extra_hours');
  $data['extra_hours_details'] = UserTimesheetExtra::where('user_id', $userId)->whereBetween('date', [$weekStartDate, $weekEndDate])->get();

  return $data;
 }

 public static function getUserApproveWeekHours($userId, $weekId)
 {
  $weekObj = Week::find($weekId);

  if (!$weekObj) {
   return 0;
  }

  $week_start_date = $weekObj->start_date;
  $week_end_date   = $weekObj->end_date;

  $hours = UserTimesheet::where('user_id', $userId)
   ->where('date', '>=', $week_start_date)
   ->where('date', '<=', $week_end_date)
   ->sum('approved_duration');
  return $hours;
 }

 public static function getUserEmloyeeWeekHours($userId, $weekId)
 {
  $weekObj = Week::find($weekId);

  if (!$weekObj) {
   return 0;
  }

  $week_start_date = $weekObj->start_date;
  $week_end_date   = $weekObj->end_date;

  $hours = UserTimesheet::where('user_id', $userId)
   ->where('date', '>=', $week_start_date)
   ->where('date', '<=', $week_end_date)
   ->sum('duration');
  return $hours;
 }

 public static function getUserApproveHours($userId, $monthId)
 {
  $monthObj = Month::find($monthId);

  if (!$monthObj) {
   return 0;
  }

  $month_start_date = $monthObj->getFirstDay();
  $month_end_date   = $monthObj->getLastDay();

  $hours = UserTimesheet::where('user_id', $userId)
   ->where('date', '>=', $month_start_date)
   ->where('date', '<=', $month_end_date)
   ->sum('approved_duration');
  return $hours;
 }

 public static function getUserEmloyeeHours($userId, $monthId)
 {
  $monthObj = Month::find($monthId);

  if (!$monthObj) {
   return 0;
  }

  $month_start_date = $monthObj->getFirstDay();
  $month_end_date   = $monthObj->getLastDay();

  $hours = UserTimesheet::where('user_id', $userId)
   ->where('date', '>=', $month_start_date)
   ->where('date', '<=', $month_end_date)
   ->sum('duration');
  return $hours;
 }
 public static function getUserTimesheetDetail($userId, $monthId)
 {

  $data['approved_hours']         = self::getUserApproveHours($userId, $monthId);
  $data['employee_hours']         = self::getUserEmloyeeHours($userId, $monthId);
  $data['bonuses']                = BonusService::getUserBonusByMonthId($userId, $monthId);
  $data['timesheets']             = self::getUserTimesheetReport($monthId, $userId);
  $data['pending_bonuse_request'] = BonusRequest::where('month_id', $monthId)->where('user_id', $userId)->where('status', 'pending')->count();

  return $data;

 }

 public static function autoApproveCheck($weekId, $parentId)
 {

  $userWeeklyTimesheet = UserTimesheetWeek::where('week_id', $weekId)->where('parent_id', $parentId)->get();

  $weekObj   = Week::find($weekId);
  $startDate = $weekObj->start_date;
  $endDate   = $weekObj->end_date;

  foreach ($userWeeklyTimesheet as $userWeeklyTimesheet) {
   $user_id = $userWeeklyTimesheet->user_id;

   $weeklyHour = UserTimesheet::where('user_id', $user_id)
    ->whereBetween('date', [$startDate, $endDate])
    ->sum('duration');

   $bonusRequest = BonusRequest::where('user_id', $user_id)->where('status', 'pending')->whereBetween('date', [$startDate, $endDate])->get();

//    echo"<pre>";
   //    print_r($bonusRequest);
   //    echo"$weeklyHour";

   if (count($bonusRequest) == 0 && $weeklyHour == 40) {

    UserTimesheetWeek::approveAllTimesheet($weekId, $user_id);

   }

  }

 }
}
