<?php
namespace App\Services\SalaryService;
use App\Models\Audited\Salary\PrepSalaryComponentType;
use App\Models\Audited\Salary\PrepSalaryComponent;


class DependencyCheckComponent{
    
    private $componentId;
    private $prepSalaryId;

    public function __construct($componentId,$prepSalaryId){
       $this->componentId=$componentId;
       $this->prepSalaryId=$prepSalaryId;

    }

    public function checkDependency(){
        
        $response=[];$queue=[];

        $prepComponent=PrepSalaryComponentType::find($this->componentId);

        if(!$prepComponent){
            $response='Prep Salary Component Not Found';
        }

        $queue[]=$prepComponent;
       
        while(count($queue) != 0 ){
            foreach($queue as $queuesObj){
                $prepComponentDependencies=$queuesObj->dependsOn;
                array_splice($queue,0,1);
                if(count($prepComponentDependencies) > 0){
                    foreach($prepComponentDependencies as $dependencies){
                        if($dependencies->dependentOn) {
                            
                            // $response[]=$dependencies->dependentOn->toArray();
                            $prepSalaryComponent=PrepSalaryComponent::where('prep_salary_id',$this->prepSalaryId)->where('prep_salary_component_type_id',$dependencies->dependentOn->id)->first();
                            if($prepSalaryComponent->is_generated !==1){
                                return false;
                            }
                            $queue[]=$dependencies->dependentOn;
                        }
                    }    
                }
            }
      
        }
        return true;

    }
}