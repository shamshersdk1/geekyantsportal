<?php
namespace App\Services\SalaryService;
use App\Services\SalaryService\PrepSalaryComponentInterface;
use App\Models\Audited\Salary\PrepUser;
use App\Models\User;
use App\Models\Audited\Salary\PrepSalaryComponent;
use App\Jobs\PrepareSalary\Components\UserSalaryJob;

class UserSalaryComponent implements PrepSalaryComponentInterface {
    
    private $userId;
    private $month;
    private $year;
    private $componentId;

    public function getValue() {
        return array("Zack"=>"Zara", "Anthony"=>"Any",  
                  "Ram"=>"Rani", "Salim"=>"Sara",  
                  "Raghav"=>"Ravina"); 
    }
    public function setMonthYear($month, $year) {
        $this->month = $month;
        $this->year = $year;
    }
    public function setComponent($componentId) {
        $this->componentId = $componentId;
    }
    public function setUserId($userId) {
        $this->userId = $userId;
    }
    public function generate(){
        $response['errors'] = "";
        $response['status'] = true;
        $response['message'] = "Generated Success";
        $componentObj = PrepSalaryComponent::find($this->componentId);
        if($componentObj->salary->month->prepUserSetting?$componentObj->salary->month->prepUserSetting->value == "open":true)
        {
            $response['errors'] = "Prep User not locked";
            $response['status'] = false;
            return $response;
        }
        dispatch(new UserSalaryJob($componentObj->prep_salary_id));
        return $response;
    }
    public function  getHtml(){
        return  ;
    }

    public function queue()
    {
        
    }
}