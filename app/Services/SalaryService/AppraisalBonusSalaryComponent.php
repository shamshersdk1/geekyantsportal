<?php
namespace App\Services\SalaryService;

use App\Models\Appraisal\AppraisalBonus;
use App\Models\Audited\Appraisal\PrepAppraisal;
use App\Models\Audited\MonthlyVariableBonus\MonthlyVariableBonus;
use App\Models\Audited\Salary\PrepAppraisalBonus;
use App\Models\Audited\Salary\PrepSalary;
use App\Models\Audited\Salary\PrepSalaryComponent;
use App\Models\Audited\Salary\PrepUser;
use App\Models\Month;
use App\Services\SalaryService\PrepSalaryComponentInterface;
use App\Models\Audited\VariablePay\VariablePay;
use App\Models\Audited\FixedBonus\FixedBonus;
use App\Jobs\PrepareSalary\Components\AppraisalBonusJob;
use App\Models\Audited\Salary\PrepSalaryComponentType;
use App\Models\Audited\Salary\PrepSalaryExecution;

class AppraisalBonusSalaryComponent implements PrepSalaryComponentInterface
{

    private $userId;
    private $month;
    private $year;
    private $componentId;

    public function getValue()
    {

    }
    public function setComponent($componentId)
    {
        $this->componentId = $componentId;
    }
    public function setMonthYear($month, $year)
    {
        $this->month = $month;
        $this->year = $year;
    }
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }
    public function getHtml()
    {
        $componentObj = PrepSalaryComponent::find($this->componentId);

        $prepAppraisalBonusObj = PrepAppraisalBonus::where('prep_salary_id', $componentObj->salary->id)->where('user_id', $this->userId)->get();

        $response = [];
        if (count($prepAppraisalBonusObj) > 0) {
            foreach ($prepAppraisalBonusObj as $appraisalBonus) {
                $response[$appraisalBonus->id]['bonus_type'] = $appraisalBonus->appraisalBonus->appraisalBonusType->description;
                $response[$appraisalBonus->id]['amount'] = $appraisalBonus->value;
            }
        }

        return $response;

    }

    public function generate()
    {

        $response['errors'] = "";
        $response['status'] = true;
        $response['message'] = "";

        $componentObj = PrepSalaryComponent::find($this->componentId);
        if (!count($componentObj) > 0) {
            $response['errors'] = "No component found";
            $response['status'] = false;
            return $response;
        }

        PrepAppraisalBonus::where('prep_salary_id', $componentObj->prep_salary_id)->delete();

        $prepUsers = PrepUser::where('prep_salary_id', $componentObj->salary->id)->get();

        if (!count($prepUsers) > 0) {
            $response['errors'] = "No Prep User found";
            $response['status'] = false;
            return $response;
        }

        $monthObj = Month::find($componentObj->salary->month_id);
        if ($monthObj->fixedBonusSetting ? $monthObj->fixedBonusSetting->value == "open" : true) {
            $response['errors'] = "Fixed Bonus Month not locked";
            $response['status'] = false;
            return $response;
        }
        if ($monthObj->variablePaySetting ? $monthObj->variablePaySetting->value == "open" : true) {
            $response['errors'] = "Variable Pay Month not locked";
            $response['status'] = false;
            return $response;
        }
        foreach ($prepUsers as $prepUser) {
            dispatch(new AppraisalBonusJob($prepUser->user_id,$componentObj->prep_salary_id));
        }
        return $response;
    }

    public function getCurrentTotalAppraisalBonus($prepSalaryId)
    {

        $totalAppraisalBonus = 0;

        $prepSalary = PrepSalary::find($prepSalaryId);
        $financialYear = $prepSalary->month->year;

        $appraisalBonuses = PrepAppraisalBonus::where('prep_salary_id', $prepSalaryId)->where('user_id', $this->userId)->get();

        foreach ($appraisalBonuses as $bonus) {
            $appraisalBonus = $bonus->appraisalBonus;
            $year = date('Y', strtotime($appraisalBonus->value_date));
            if ($financialYear == $year) {

                // if ($appraisalBonus->appraisalBonusType->code == 'annual-bonus' || $appraisalBonus->appraisalBonusType->code == 'confirmation') {
                $totalAppraisalBonus = $totalAppraisalBonus + $bonus->value;
                // }
            }
        }

        return $totalAppraisalBonus;

    }

    public function getAnnualBonus($prepSalaryId, $userId)
    {
        $currBonus = PrepAppraisalBonus::where('prep_salary_id', $prepSalaryId)->where('user_id', $userId)->sum('value');
        return $currBonus;
    }

    public function queue()
    {
        $prepSalaryComponent = PrepSalaryComponent::find($this->componentId) ;
        if(!$prepSalaryComponent)
            return false;
        $prepSalaryObj = PrepSalary::find($prepSalaryComponent->prep_salary_id);
        $userComponentTypeId = PrepSalaryComponentType::where('code','user')->first();
        if(!$userComponentTypeId)
            return false;
        $appraisalBonusComponentType = PrepSalaryComponentType::where('code','appraisal-bonus')->first();
        if(!$appraisalBonusComponentType)
            return false;
        $appraisalBonusComponent = $prepSalaryObj->components->where('prep_salary_component_type_id',$appraisalBonusComponentType->id)->first();
        if(!$appraisalBonusComponent)
            return false;
            
        foreach($prepSalaryObj->prepUsers as $prepUser)
        {
            $prepSalaryExecution = PrepSalaryExecution::where('prep_salary_id',$prepSalaryComponent->prep_salary_id)->where('component_id',$appraisalBonusComponent->id)->where('user_id',$prepUser->user_id)->first();
            if(!$prepSalaryExecution)
            {
                $prepSalaryExecutionObj = PrepSalaryExecution::create(['prep_salary_id' => $prepSalaryComponent->prep_salary_id,'component_id' => $appraisalBonusComponent->id,'user_id' => $prepUser->user_id,'status' => 'init','counter' => 0]);
                if (!$prepSalaryExecutionObj->isValid()) {
                    continue;  
                }
            }
            if($prepSalaryExecution->status !== "completed"){
                dispatch(new AppraisalBonusJob($prepUser->user_id, $prepSalaryComponent->prep_salary_id));
                }
        }   
        
    }
}
