<?php

namespace App\Services\SalaryService;

use App\Jobs\PrepareSalary\Components\AdhocPaymentJob;
use App\Models\Audited\Salary\PrepSalaryComponent;
use App\Models\Audited\Salary\PrepAdhocPayment;
use App\Models\Audited\Salary\PrepAdhocPaymentComponent;
use App\Models\Audited\Salary\PrepUser;
use App\Models\Month;
use App\Models\Audited\AdhocPayments\AdhocPayment;
use App\Models\Audited\Salary\PrepSalaryComponentType;
use App\Models\Audited\Salary\PrepSalaryExecution;
use App\Models\Audited\Salary\PrepSalary;

class AdhocPaymentSalaryComponent implements PrepSalaryComponentInterface
{
    public function getValue()
    {
        return [];
    }
    public function setComponent($componentId)
    {
        $this->componentId = $componentId;
    }
    public function setMonthYear($month, $year)
    {
        $this->month = $month;
        $this->year = $year;
    }
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }
    public function getHtml()
    {
        
        $componentObj = PrepSalaryComponent::find($this->componentId);
        $adhocPaymentValue=PrepAdhocPayment::where('prep_salary_id',$componentObj->salary->id)->where('user_id',$this->userId)->first();
        $response = [];
        if($adhocPaymentValue)
        {
            $response = $adhocPaymentValue->components->toArray();
        }
        return $response;
        
    }

    public function generate()
    {

        $response['errors'] = "";
        $response['status'] = true;
        $response['message'] = "";

        $componentObj = PrepSalaryComponent::find($this->componentId);
        if (!count($componentObj) > 0) {
            $response['errors'] = "No component found";
            $response['status'] = false;
            return $response;
        }

        PrepAdhocPayment::where('prep_salary_id', $componentObj->prep_salary_id)->get()->each(function ($prepAdhocPaymentObj) {
            $prepAdhocPaymentObj->delete();
        });

        $prepUsers = PrepUser::where('prep_salary_id', $componentObj->salary->id)->get();

        if (!count($prepUsers) > 0) {
            $response['errors'] = "No Prep User found";
            $response['status'] = false;
            return $response;
        }

        $monthObj = Month::find($componentObj->salary->month_id);
        if ($monthObj->adhocPaymentSetting ? $monthObj->adhocPaymentSetting->value == "open" : true) {
            $response['errors'] = "Adhoc Payment Month not locked";
            $response['status'] = false;
            return $response;
        }

        $startDate = $monthObj->getFirstDay();
        $endDate = $monthObj->getLastDay();

        foreach ($prepUsers as $prepUser) {
            $adhocPaymentObjs = AdhocPayment::where('user_id',$prepUser->user_id)->where('month_id',$monthObj->id)->get();
            if(!count($adhocPaymentObjs)>0)
            {
                continue;
            }
            $prepAdhocPayment = PrepAdhocPayment::create(['prep_salary_id' => $prepUser->prep_salary_id ,'user_id' => $prepUser->user_id]);
            if(!$prepAdhocPayment->isValid())
            {
                $response['errors'] = $response['errors'].$prepAdhocPayment->getErrors();
                $response['status'] = false;
            }
            foreach($adhocPaymentObjs as $adhocPaymentObj)
            {
                $prepAdhocPaymentComponent = PrepAdhocPaymentComponent::create(['prep_adhoc_payment_id' => $prepAdhocPayment->id, 'key' => $adhocPaymentObj->component->key,'value' => $adhocPaymentObj->amount,'adhoc_payment_id' => $adhocPaymentObj->id]);
                if(!$prepAdhocPaymentComponent->isValid())
                {
                    $response['errors'] = $response['errors'].$prepAdhocPaymentComponent->getErrors();
                    $response['status'] = false;
                }
            }
        }
        return $response;
    }

    public function getAdhocPayments($prepSalaryId, $userId)
    {
        $prepAdhocObj = PrepAdhocPayment::where('prep_salary_id', $prepSalaryId)->where('user_id', $userId)->first();
        $amount = 0;
        foreach($prepAdhocObj->components as $component)
        {
            if($component->value > 0)
            {
                $amount += $component->value;
            }
        }
        return $amount;
    }

    public function queue()
    {
        $prepSalaryComponent = PrepSalaryComponent::find($this->componentId) ;
        if(!$prepSalaryComponent)
            return false;
        $prepSalaryObj = PrepSalary::find($prepSalaryComponent->prep_salary_id);
        $userComponentTypeId = PrepSalaryComponentType::where('code','user')->first();
        if(!$userComponentTypeId)
            return false;
        $adhocPaymentComponentType = PrepSalaryComponentType::where('code','adhoc-payment')->first();
        if(!$adhocPaymentComponentType)
            return false;
        $adhocPaymentComponent = $prepSalaryObj->components->where('prep_salary_component_type_id',$adhocPaymentComponentType->id)->first();
        if(!$adhocPaymentComponent)
            return false;
            
        foreach($prepSalaryObj->prepUsers as $prepUser)
        {
            $prepSalaryExecution = PrepSalaryExecution::where('prep_salary_id',$prepSalaryComponent->prep_salary_id)->where('component_id',$adhocPaymentComponent->id)->where('user_id',$prepUser->user_id)->first();
            if(!$prepSalaryExecution)
            {
                $prepSalaryExecutionObj = PrepSalaryExecution::create(['prep_salary_id' => $prepSalaryComponent->prep_salary_id,'component_id' => $adhocPaymentComponent->id,'user_id' => $prepUser->user_id,'status' => 'init','counter' => 0]);
                if (!$prepSalaryExecutionObj->isValid()) {
                    continue;  
                }
            }
            if($prepSalaryExecution->status !== "completed"){
                dispatch(new AdhocPaymentJob($prepUser->user_id, $prepSalaryComponent->prep_salary_id));
                }
        }   
        
    }

}