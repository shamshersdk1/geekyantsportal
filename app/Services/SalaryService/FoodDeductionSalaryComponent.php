<?php
namespace App\Services\SalaryService;
use App\Services\SalaryService\PrepSalaryComponentInterface;
use App\Models\Audited\Salary\PrepUser;
use App\Models\Audited\Salary\PrepSalaryComponent;
use App\Models\User;
use App\Models\Month;
use App\Models\Admin\Insurance\Insurance;
use App\Models\Audited\FoodDeduction\FoodDeduction;
use App\Models\Audited\Salary\PrepFoodDeduction;
use App\Jobs\PrepareSalary\Components\FoodDeductionJob;
use App\Models\Audited\Salary\PrepSalaryComponentType;
use App\Models\Audited\Salary\PrepSalaryExecution;
use App\Models\Audited\Salary\PrepSalary;

class FoodDeductionSalaryComponent implements PrepSalaryComponentInterface {
    
    private $userId;
    private $month;
    private $year;
    private $componentId;

    public function getValue() {
        return array("Zack"=>"Zara", "Anthony"=>"Any",  
                  "Ram"=>"Rani", "Salim"=>"Sara",  
                  "Raghav"=>"Ravina"); 
    }
    public function setMonthYear($month, $year) {
        $this->month = $month;
        $this->year = $year;
    }
    public function setComponent($componentId) {
        $this->componentId = $componentId;
    }
    public function setUserId($userId) {
        $this->userId = $userId;
    }
    public function generate(){
        $response['errors'] = "";
        $response['status'] = true;
        $response['message'] = "";

        $componentObj = PrepSalaryComponent::find($this->componentId);
        if(!count($componentObj)>0)
        {
            $response['errors'] = "No component found";
            $response['status'] = false;
            return $response;
        }
        PrepFoodDeduction::where('prep_salary_id',$componentObj->prep_salary_id)->delete();
        $prepUsers = PrepUser::where('prep_salary_id',$componentObj->prep_salary_id)->get();
        if(!count($prepUsers)>0)
        {
            $response['errors'] = "No Prep User found";
            $response['status'] = false;
            return $response;
        }
        $monthObj = Month::where("month",$this->month)->where('year',$this->year)->first();
        if(!count($monthObj)>0)
        {
            $response['errors'] = "No Month found";
            $response['status'] = false;
            return $response;
        }
        if($monthObj->monthlyFoodDeductionSetting?$monthObj->monthlyFoodDeductionSetting->value == "open":true)
        {
            $response['errors'] = "Food deduction Month not locked";
            $response['status'] = false;
            return $response;
        }
        foreach($prepUsers as $prepUser )
        {
            dispatch(new FoodDeductionJob($prepUser->user_id, $componentObj->prep_salary_id));
        }
        return $response;
    }

    public function  getHtml(){
        $componentObj = PrepSalaryComponent::find($this->componentId);
        $foodDeductionValue=PrepFoodDeduction::where('prep_salary_id',$componentObj->salary->id)->where('user_id',$this->userId)->first()->value;
        $response=[];
        $response['amount']= $foodDeductionValue;
        return $response;
    }

    public static function getFoodDeduction($prepSalaryId,$userId){
        $foodDeductionObj=PrepFoodDeduction::where('prep_salary_id',$prepSalaryId)->where('user_id',$userId)->first();
        return $foodDeductionObj->value;
    }

    public function queue()
    {
        $prepSalaryComponent = PrepSalaryComponent::find($this->componentId) ;
        if(!$prepSalaryComponent)
            return false;

        $prepSalaryObj = PrepSalary::find($prepSalaryComponent->prep_salary_id);
        $userComponentTypeId = PrepSalaryComponentType::where('code','user')->first();
        if(!$userComponentTypeId)
            return false;
        $foodComponentType = PrepSalaryComponentType::where('code','food-deduction')->first();
        if(!$foodComponentType)
            return false;
        $foodComponent = $prepSalaryObj->components->where('prep_salary_component_type_id',$foodComponentType->id)->first();
        if(!$foodComponent)
            return false;
        foreach($prepSalaryObj->prepUsers as $prepUser)
        {
            $prepSalaryExecution = PrepSalaryExecution::where('prep_salary_id',$prepSalaryComponent->prep_salary_id)->where('component_id',$foodComponent->id)->where('user_id',$prepUser->user_id)->first();
            if(!$prepSalaryExecution)
            {
                $prepSalaryExecutionObj = PrepSalaryExecution::create(['prep_salary_id' => $prepSalaryComponent->prep_salary_id,'component_id' => $foodComponent->id,'user_id' => $prepUser->user_id,'status' => 'init','counter' => 0]);
                if (!$prepSalaryExecutionObj->isValid()) {
                    continue;  
                }
            }
            if(!$prepSalaryExecution->status=="completed")
                dispatch(new FoodDeductionJob($prepUser->user_id, $prepSalaryComponent->prep_salary_id));
        }  
        
    }
    
}
