<?php
namespace App\Services\SalaryService;

use App\Models\Month;
use App\Models\User;
use DB;
use Redirect;

class TransactionService
{
    public static function transactionSummary($id = null)
    {
        if ($id != null) {
            $monthObj = Month::find($id);
            if (!$monthObj) {
                return Redirect::back()->withErrors(['Month not found']);
            }
        }
        $users = User::orderBy('employee_id', 'ASC')->get();

        $amount = DB::select(DB::raw("SELECT user_id,
            sum(amount) as total,
            sum(case when amount < 0 then amount else 0 end) as debits,
            sum(case when amount > 0 then amount else 0 end) as credits
            from transactions WHERE is_company_expense = 0 AND month_id = $id group by user_id order by user_id"));

        $data = [];
        foreach ($amount as $key => $value) {
            $user = User::find($value->user_id);
            if (!$user) {
                return [];
            }

            $data[$user->id]['total'] = $value->total;
            $data[$user->id]['debit'] = $value->debits;
            $data[$user->id]['credit'] = $value->credits;
        }

        return $data;
    }
}
