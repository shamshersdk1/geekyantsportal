<?php
namespace App\Services\SalaryService;

use App\Jobs\PrepareSalary\Components\GrossToBePaidJob;
use App\Models\Admin\FinancialYear;
use App\Models\Appraisal\AppraisalBonusType;
use App\Models\Appraisal\AppraisalComponentType;
use App\Models\Audited\Appraisal\PrepAppraisal;
use App\Models\Audited\Salary\PrepGrossToBePaid;
use App\Models\Audited\Salary\PrepGrossToBePaidItem;
use App\Models\Audited\Salary\PrepSalary;
use App\Models\Audited\Salary\PrepSalaryComponent;
use App\Models\Audited\Salary\PrepUser;
use App\Models\Month;
use App\Services\SalaryService\ApprisalSalaryComponent;
use App\Services\SalaryService\GrossToBePaidComponent;
use App\Services\SalaryService\PrepSalaryComponentInterface;
use App\Models\Audited\Salary\PrepSalaryComponentType;
use App\Models\Audited\Salary\PrepSalaryExecution;

class GrossToBePaidComponent implements PrepSalaryComponentInterface
{
    private $userId;
    private $month;
    private $year;
    private $componentId;

    public function getValue()
    {
        return [];
    }

    public function setMonthYear($month, $year)
    {
        $this->month = $month;
        $this->year = $year;
    }

    public function setComponent($componentId)
    {
        $this->componentId = $componentId;
    }

    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    public function generate()
    {
        $response = [];
        $response['message'] = "";
        $response['errors'] = "";
        $response['status'] = true;

        $appraisalComponentTypes = AppraisalComponentType::where('is_computed', 0)->pluck('code');
        $appraisalbonusTypes = AppraisalBonusType::get()->pluck('code');

        $keys = array_merge($appraisalComponentTypes->toArray(), $appraisalbonusTypes->toArray());

        $prepComponent = PrepSalaryComponent::find($this->componentId);

        $prepSalaryObj = $prepComponent->salary;

        $prepUserObj = PrepUser::where('prep_salary_id', $prepSalaryObj->id)->get();
        if (!$prepUserObj || count($prepUserObj) == 0) {
            $response['status'] = false;
            $response['errors'] = "Prep User not found!";
            return $response;
        }

        PrepGrossToBePaid::where('prep_salary_id', $prepSalaryObj->id)->get()->each(function ($analytic) {
            $analytic->delete();
        });

        foreach ($prepUserObj as $user) {
            dispatch(new GrossToBePaidJob($user->user_id, $prepSalaryObj->id));
        }

        if ($response['status'] == true) {
            $response['message'] = "Saved Successfully";
        }

        return $response;
    }

    public function getAnnualGrossSalary($prepSalaryId, $userId, $key)
    {
        //AnnualGrossSalary
        $prepSalaryObj = PrepSalary::find($prepSalaryId);
        $monthNumber = $prepSalaryObj->month->month_number;
        $remainingMonths = 12 - $monthNumber;

        $prepAppraisal = PrepAppraisal::where('prep_salary_id', $prepSalaryId)->where('user_id', $userId)->orderBy('end_date', 'desc')->first();

        $totalGrossSalary = 0;

        if ($prepAppraisal) {
            $grossSalary = ApprisalSalaryComponent::getComponentValueByKey($prepAppraisal, $key, $remainingMonths);
        }
        return round($grossSalary);
    }

    public function getAppraisalComponentValue($prepSalaryId, $userId, $key)
    {
        $prepSalaryObj = PrepSalary::find($prepSalaryId);
        $remainingMonths = 0;
        if ($prepSalaryObj) {
            $remainingMonths = FinancialYear::getRemainingMonths($prepSalaryObj->month->id);
        }

        $totalGrossSalary = 0;
        $grossSalary = 0;

        $prepAppraisal = PrepAppraisal::where('prep_salary_id', $prepSalaryId)->where('user_id', $userId)->orderBy('end_date', 'desc')->first();

        if ($prepAppraisal) {
            if ($key == 'confirmation-bonus' || $key == 'variable-bonus' || $key == 'year-end' || $key == 'open-source-contribution-em' || $key == 'annual-variable' || $key == 'tech-talk-video-em' || $key == 'tech-articles-em' || $key == 'study-jam-em' || $key == 'case-study-em') {
                $grossSalary = ApprisalSalaryComponent::getBonusValueByKey($prepAppraisal, $key);
            } else {
                $grossSalary = ApprisalSalaryComponent::getComponentValueByKey($prepAppraisal, $key, $remainingMonths);
                $grossSalary *= $remainingMonths;
            }
        }
        return round($grossSalary);
    }

    public function calculateValue($currentGrossId, $key)
    {
        $currentGrossObj = PrepGrossToBePaid::find($currentGrossId);
        $prepSalaryId = $currentGrossObj->prep_salary_id;
        $userId = $currentGrossObj->user_id;

        if ($key == 'annual-gross-salary') {
            $key = ['basic', 'hra', 'food-allowance', 'car-allowance', 'lta', 'special-allowance'];
            $grossSalary = self::getAnnualGrossSalary($prepSalaryId, $userId, $key);
        } else {
            $grossSalary = self::getAppraisalComponentValue($prepSalaryId, $userId, $key);
        }
        return $grossSalary;
    }

    public function getAnnualValueByKeys($keys, $prepSalaryId, $userId)
    {
        $totalAnnual = 0;

        foreach ($keys as $key) {
            $totalAnnual += $this->getValueByKey($key, $prepSalaryId, $userId);
        }

        return $totalAnnual;
    }

    public function getValueByKey($key, $prepSalaryId, $userId)
    {
        $keyValue = 0;
        $futureObj = PrepGrossToBePaid::where('prep_salary_id', $prepSalaryId)->where('user_id', $userId)->first();
        if ($futureObj) {
            $futureValueObj = $futureObj->itemByKey($key);
            $keyValue = $futureValueObj->value;
        }
        return $keyValue;
    }

    public function getTotalAppraisalBonus($prepSalaryId, $userId)
    {
        // $prepSalaryObj = PrepSalary::find($prepSalaryId);
        // $monthObj = Month::find($prepSalaryObj->month_id);
        // $financialYearObj = FinancialYear::find($monthObj->financial_year_id);

        $keyValue = 0;

        $appraisalbonusTypes = AppraisalBonusType::get()->pluck('code');
        $keys = $appraisalbonusTypes->toArray();

        $futureObj = PrepGrossToBePaid::where('prep_salary_id', $prepSalaryId)->where('user_id', $userId)->first();
        if ($futureObj) {
            foreach ($keys as $key) {
                $futureValueObj = $futureObj->itemByKey($key);
                // $valueDate = $futureValueObj->value_date;
                // if ($valueDate <= $financialYearObj->end_date && $valueDate >= $monthObj->getLastDay()) {
                $keyValue += $futureValueObj->value;
                // }
            }
        }
        return $keyValue;
    }

    public function getHtml()
    {
        $componentObj = PrepSalaryComponent::find($this->componentId);

        $PrepGrossToBePaidObj = PrepGrossToBePaid::where('prep_salary_id', $componentObj->salary->id)->where('user_id', $this->userId)->first();

        $response = [];
        if ($PrepGrossToBePaidObj) {
            $prepGrossToBePaidItems = $PrepGrossToBePaidObj->items;
            if (count($prepGrossToBePaidItems) > 0) {
                foreach ($prepGrossToBePaidItems as $item) {
                    $temp = [];
                    $temp['key'] = $item->key;
                    $temp['value'] = $item->value;
                    $response[] = $temp;
                }
            }
        }
        return $response;
    }

    public function generateUserData($salaryId)
    {
        $response = [];
        $response['status'] = true;
        $response['errors'] = '';

        $appraisalComponentTypes = AppraisalComponentType::where('is_computed', 0)->pluck('code');
        $appraisalbonusTypes = AppraisalBonusType::get()->pluck('code');

        $keys = array_merge($appraisalComponentTypes->toArray(), $appraisalbonusTypes->toArray());
        PrepGrossToBePaid::where('prep_salary_id', $salaryId)->where('user_id', $this->userId)->get()->each(function ($prepGrossObj) {
            $prepGrossObj->delete();
        });
        $grossSalObj = new PrepGrossToBePaid;
        $grossSalObj->prep_salary_id = $salaryId;
        $grossSalObj->user_id = $this->userId;

        if (!$grossSalObj->save()) {
            $response['status'] = false;
            $response['errors'] = $response['errors'] . "Something went wrong for " . $this->userId . '<br/>';

        }
        foreach ($keys as $key) {

            $classObj = new GrossToBePaidComponent;
            $getValue = $classObj->calculateValue($grossSalObj->id, $key);

            $checkItem = PrepGrossToBePaidItem::where('prep_gross_to_be_paid_id', $grossSalObj->id)->where('key', $key)->first();

            if ($checkItem) {
                $response['status'] = false;
                $response['errors'] = $response['errors'] . "Prep Gross To Be Paid Item Record - " . $grossSalObj->id . " already exist <br/>";
                continue;
            }

            $paidObj = new PrepGrossToBePaidItem;
            $paidObj->prep_gross_to_be_paid_id = $grossSalObj->id;
            $paidObj->key = $key;
            $paidObj->value = $getValue;

            if (!$paidObj->save()) {
                $response['status'] = false;
                $response['errors'] = $response['errors'] . $key . " key not saved for " . $grossSalObj->id . " and user id is - " . $this->userId . " - errors - " . $paidObj->getErrors() . '<br/>';
            }
        }
        return $response;
    }

    public function queue()
    {
        $prepSalaryComponent = PrepSalaryComponent::find($this->componentId) ;
        if(!$prepSalaryComponent)
            return false;

        $prepSalaryObj = PrepSalary::find($prepSalaryComponent->prep_salary_id);
        $userComponentTypeId = PrepSalaryComponentType::where('code','user')->first();
        if(!$userComponentTypeId)
            return false;
        $grossComponentType = PrepSalaryComponentType::where('code','gross-to-be-paid')->first();
        if(!$grossComponentType)
            return false;
        $grossComponent = $prepSalaryObj->components->where('prep_salary_component_type_id',$grossComponentType->id)->first();
        if(!$grossComponent)
            return false;
            
        foreach($prepSalaryObj->prepUsers as $prepUser)
        {
            $prepSalaryExecution = PrepSalaryExecution::where('prep_salary_id',$prepSalaryComponent->prep_salary_id)->where('component_id',$grossComponent->id)->where('user_id',$prepUser->user_id)->first();
            if(!$prepSalaryExecution)
            {
                $prepSalaryExecutionObj = PrepSalaryExecution::create(['prep_salary_id' => $prepSalaryComponent->prep_salary_id,'component_id' => $grossComponent->id,'user_id' => $prepUser->user_id,'status' => 'init','counter' => 0]);
                if (!$prepSalaryExecutionObj->isValid()) {
                    continue;  
                }
            }
            if($prepSalaryExecution->status !== "completed"){
                dispatch(new GrossToBePaidJob($prepUser->user_id, $prepSalaryComponent->prep_salary_id));
                }
        } 
        
    }
}
