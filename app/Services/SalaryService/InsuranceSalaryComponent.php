<?php
namespace App\Services\SalaryService;
use App\Services\SalaryService\PrepSalaryComponentInterface;
use App\Models\Audited\Salary\PrepUser;
use App\Models\Audited\Salary\PrepSalaryComponent;
use App\Models\Audited\Salary\PrepInsurance;
use App\Models\User;
use App\Models\Month;
use App\Models\Admin\Insurance\Insurance;
use App\Models\Admin\Insurance\InsuranceDeduction;
use App\Jobs\PrepareSalary\Components\InsuranceJob;
use App\Models\Audited\Salary\PrepSalaryComponentType;
use App\Models\Audited\Salary\PrepSalaryExecution;
use App\Models\Audited\Salary\PrepSalary;

class InsuranceSalaryComponent implements PrepSalaryComponentInterface {
    
    private $userId;
    private $month;
    private $year;
    private $componentId;

    public function getValue() {
        return array("Zack"=>"Zara", "Anthony"=>"Any",  
                  "Ram"=>"Rani", "Salim"=>"Sara",  
                  "Raghav"=>"Ravina"); 
    }
    public function setMonthYear($month, $year) {
        $this->month = $month;
        $this->year = $year;
    }
    public function setComponent($componentId) {
        $this->componentId = $componentId;
    }
    public function setUserId($userId) {
        $this->userId = $userId;
    }
    public function generate(){
        $response['errors'] = "";
        $response['status'] = true;
        $response['message'] = "";

        $componentObj = PrepSalaryComponent::find($this->componentId);
        if(!count($componentObj)>0)
        {
            $response['errors'] = "No component found";
            $response['status'] = false;
            return $response;
        }
        PrepInsurance::where('prep_salary_id',$componentObj->prep_salary_id)->delete();
        $prepUsers = PrepUser::where('prep_salary_id',$componentObj->prep_salary_id)->get();
        if(!count($prepUsers)>0)
        {
            $response['errors'] = "No Prep User found";
            $response['status'] = false;
            return $response;
        }
        $monthObj = Month::where("month",$this->month)->where('year',$this->year)->first();
        if(!count($monthObj)>0)
        {
            $response['errors'] = "No Month found";
            $response['status'] = false;
            return $response;
        }
        foreach($prepUsers as $prepUser )
        {
            dispatch(new InsuranceJob($prepUser->user_id, $componentObj->prep_salary_id));
        }
        return $response;
    }

    public function  getHtml(){

        $componentObj = PrepSalaryComponent::find($this->componentId);
        
        $prepInsuranceObj=PrepInsurance::where('prep_salary_id',$componentObj->salary->id)->where('user_id',$this->userId)->cursor();

        $insuranceAmount=0;

        $response=[];
        if(count($prepInsuranceObj)>0){
            foreach($prepInsuranceObj as $insuranceObj){
              $response['amount']=$insuranceAmount + $insuranceObj->value;

            }
        }
        return $response;
    }

    public function queue()
    {
        $prepSalaryComponent = PrepSalaryComponent::find($this->componentId) ;
        if(!$prepSalaryComponent)
            return false;
        $prepSalaryObj = PrepSalary::find($prepSalaryComponent->prep_salary_id);
        $userComponentTypeId = PrepSalaryComponentType::where('code','user')->first();
        if(!$userComponentTypeId)
            return false;
        $insuranceComponentType = PrepSalaryComponentType::where('code','insurance')->first();
        if(!$insuranceComponentType)
            return false;
        $insuranceComponent = $prepSalaryObj->components->where('prep_salary_component_type_id',$insuranceComponentType->id)->first();
        if(!$insuranceComponent)
            return false;
            
        foreach($prepSalaryObj->prepUsers as $prepUser)
        {
            $prepSalaryExecution = PrepSalaryExecution::where('prep_salary_id',$prepSalaryComponent->prep_salary_id)->where('component_id',$insuranceComponent->id)->where('user_id',$prepUser->user_id)->first();
            if(!$prepSalaryExecution)
            {
                $prepSalaryExecutionObj = PrepSalaryExecution::create(['prep_salary_id' => $prepSalaryComponent->prep_salary_id,'component_id' => $insuranceComponent->id,'user_id' => $prepUser->user_id,'status' => 'init','counter' => 0]);
                if (!$prepSalaryExecutionObj->isValid()) {
                    continue;  
                }
            }
            if($prepSalaryExecution->status !== "completed"){
                dispatch(new InsuranceJob($prepUser->user_id, $prepSalaryComponent->prep_salary_id));
                }
        }   
        
    }
    
}
