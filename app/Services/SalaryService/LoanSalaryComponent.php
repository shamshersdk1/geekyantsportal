<?php
namespace App\Services\SalaryService;
use App\Models\Audited\Salary\PrepSalaryComponent;
use App\Models\Audited\Salary\PrepUser;
use App\Models\Audited\Salary\PrepLoanEmi;
use App\Models\Month;
use App\Models\LoanEmi;
use App\Jobs\PrepareSalary\Components\LoanJob;
use App\Models\Audited\Salary\PrepSalaryComponentType;
use App\Models\Audited\Salary\PrepSalaryExecution;
use App\Models\Audited\Salary\PrepSalary;

class LoanSalaryComponent implements PrepSalaryComponentInterface {
    private $userId;
    private $month;
    private $year;
    private $componentId;

    public function getValue() {
        return array("Zack"=>"Zara", "Anthony"=>"Any",  
                  "Ram"=>"Rani", "Salim"=>"Sara",  
                  "Raghav"=>"Ravina"); 
    }

    public function setMonthYear($month, $year) {
        $this->month = $month;
        $this->year = $year;
    }

    public function setComponent($componentId) {
        $this->componentId = $componentId;
    }

    public function setUserId($userId) {
        $this->userId = $userId;
    }

    public function generate(){
        $response['errors'] = "";
        $response['status'] = true;
        $response['message'] = "";
        $componentObj = PrepSalaryComponent::find($this->componentId);
        if(!count($componentObj)>0)
        {
            $response['errors'] = "No component found";
            $response['status'] = false;
            return $response;
        }
        PrepLoanEmi::where('prep_salary_id',$componentObj->prep_salary_id)->delete();
        $prepUsers = PrepUser::where('prep_salary_id',$componentObj->salary->id)->get();

        $monthObj = Month::where("month",$this->month)->where('year',$this->year)->first();
        if(!count($monthObj)>0)
        {
            $response['errors'] = "No Month found";
            $response['status'] = false;
            return $response;
        }
       
        if(!count($prepUsers)>0)
        {
            $response['errors'] = "No Prep User found";
            $response['status'] = false;
            return $response;
        }
        if($monthObj->loanSetting?$monthObj->loanSetting->value == "open":true)
        {
            $response['errors'] = "Loan deduction Month not locked";
            $response['status'] = false;
            return $response;
        }
       
        foreach($prepUsers as $prepUser )
        {
            dispatch(new LoanJob($prepUser->user_id, $componentObj->prep_salary_id));
        }
        return $response;
    }

    public function  getHtml(){

        $componentObj = PrepSalaryComponent::find($this->componentId);

        $prepLoanObj=PrepLoanEmi::where('prep_salary_id',$componentObj->salary->id)->where('user_id',$this->userId)->cursor();

        $response=[];
        if(count($prepLoanObj)>0){
            foreach($prepLoanObj as $loanObj){
              $response[$loanObj->id]['amount']= $loanObj->loan->amount;
              $response[$loanObj->id]['remaining_amount']= $loanObj->loan->remaining;
              $response[$loanObj->id]['emi']=$loanObj->emi_amount;  

            }
        }
        return $response;
    }

    public function queue()
    {
        $prepSalaryComponent = PrepSalaryComponent::find($this->componentId) ;
        if(!$prepSalaryComponent)
            return false;
        $prepSalaryObj = PrepSalary::find($prepSalaryComponent->prep_salary_id);
        $userComponentTypeId = PrepSalaryComponentType::where('code','user')->first();
        if(!$userComponentTypeId)
            return false;
        $loanComponentType = PrepSalaryComponentType::where('code','loan')->first();
        if(!$loanComponentType)
            return false;
        $loanComponent = $prepSalaryObj->components->where('prep_salary_component_type_id',$loanComponentType->id)->first();
        if(!$loanComponent)
            return false;
            
        foreach($prepSalaryObj->prepUsers as $prepUser)
        {
            $prepSalaryExecution = PrepSalaryExecution::where('prep_salary_id',$prepSalaryComponent->prep_salary_id)->where('component_id',$loanComponent->id)->where('user_id',$prepUser->user_id)->first();
            if(!$prepSalaryExecution)
            {
                $prepSalaryExecutionObj = PrepSalaryExecution::create(['prep_salary_id' => $prepSalaryComponent->prep_salary_id,'component_id' => $loanComponent->id,'user_id' => $prepUser->user_id,'status' => 'init','counter' => 0]);
                if (!$prepSalaryExecutionObj->isValid()) {
                    continue;  
                }
            }
            if($prepSalaryExecution->status !== "completed"){
                dispatch(new LoanJob($prepUser->user_id, $prepSalaryComponent->prep_salary_id));
                }
        } 
        
    }
}