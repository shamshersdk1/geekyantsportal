<?php
namespace App\Services\SalaryService;

use App\Models\Admin\ItSaving;
use App\Models\Admin\ItSavingOther;
use App\Models\Admin\ItSavingMonth;
use App\Models\Admin\ItSavingMonthOther;
use App\Models\Audited\Salary\PrepItSaving;
use App\Models\Audited\Salary\PrepItSavingComponent;
use App\Models\Audited\Salary\PrepSalaryComponent;
use App\Models\Audited\Salary\PrepUser;
use App\Models\Audited\Salary\PrepSalary;
use App\Models\Month;
use App\Services\SalaryService\GrossPaidComponent;
use App\Services\SalaryService\PrepSalaryComponentInterface;
use App\Services\SalaryService\VpfSalaryComponent;
use App\Models\Audited\Appraisal\PrepAppraisal;
use App\Models\Appraisal\AppraisalComponentType;
use App\Jobs\PrepareSalary\Components\ItSavingSalaryJob;
use App\Models\Audited\Salary\PrepSalaryComponentType;
use App\Models\Audited\Salary\PrepSalaryExecution;

class ItSavingSalaryComponent implements PrepSalaryComponentInterface
{

    private $userId;
    private $month;
    private $year;
    private $componentId;

    public function getValue()
    {
        return array("Zack" => "Zara", "Anthony" => "Any",
            "Ram" => "Rani", "Salim" => "Sara",
            "Raghav" => "Ravina");
    }
    public function setMonthYear($month, $year)
    {
        $this->month = $month;
        $this->year = $year;
    }
    public function setComponent($componentId)
    {
        $this->componentId = $componentId;
    }
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    private function saveData($prep_it_saving_id, $key, $value)
    {
        $response['errors'] = "";
        $response['status'] = true;

        $prepItSavingComponentObj = new PrepItSavingComponent();
        $prepItSavingComponentObj->prep_it_saving_id = $prep_it_saving_id;
        $prepItSavingComponentObj->key = $key;
        $prepItSavingComponentObj->value = $value;
        if ($prepItSavingComponentObj->isValid()) {
            if (!$prepItSavingComponentObj->save()) {
                $response['errors'] = "Something went wrong";
                $response['status'] = false;
            }
        } else {
            $response['errors'] = $prepItSavingComponentObj->getErrors();
            $response['status'] = false;
        }
        return $response;
    }

    public function generate()
    {
        $response['errors'] = "";
        $response['status'] = true;
        $response['message'] = "";

        $componentObj = PrepSalaryComponent::find($this->componentId);
        if (!count($componentObj) > 0) {
            $response['errors'] = "No component found";
            $response['status'] = false;
            return $response;
        }
        PrepItSaving::where('prep_salary_id', $componentObj->prep_salary_id)->get()->each(function ($prepItSavingObj) {
            $prepItSavingObj->delete();
        });
        $prepUsers = PrepUser::where('prep_salary_id', $componentObj->salary->id)->get();
        if (!count($prepUsers) > 0) {
            $response['errors'] = "No Prep User found";
            $response['status'] = false;
            return $response;
        }
        $monthObj = Month::find($componentObj->salary->month_id);
        if(!count($monthObj)>0)
        {
            $response['errors'] = "No Month found";
            $response['status'] = false;
            return $response;
        }
        if($monthObj->itSavingMonthSetting?$monthObj->itSavingMonthSetting->value == "open":true)
        {
            $response['errors'] = "IT Saving Month not locked";
            $response['status'] = false;
            return $response;
        }
        foreach ($prepUsers as $prepUser) {
            dispatch(new ItSavingSalaryJob($prepUser->user_id,$componentObj->prep_salary_id));
        }
        return $response;
    }

    public function getHtml()
    {

        $componentObj = PrepSalaryComponent::find($this->componentId);

        $prepItSavingObj = PrepItSaving::where('prep_salary_id', $componentObj->salary->id)->where('user_id', $this->userId)->first();

        $response = [];
        $prepItSaving = [];
        if ($prepItSavingObj) {
            $prepItSaving = $prepItSavingObj->components;
        }

        $itSavingComponents = ItSaving::GetTableColumns();

        foreach ($itSavingComponents as $component) {
            foreach ($prepItSaving as $prepSaving) {
                if ($prepSaving['key'] == $component->COLUMN_NAME) {
                    $response[$component->COLUMN_NAME] = $prepSaving['value'];
                    break;
                } else {
                    $response[$component->COLUMN_NAME] = 0;
                }
            }
        }
        return $response;
    }

    public function getValueByKey($key, $prepSalaryId, $userId)
    {
        $amount = 0;
        $prepItSavingObj = PrepItSaving::where('prep_salary_id', $prepSalaryId)->where('user_id', $userId)->first();
        if($prepItSavingObj) {
            $rentObj = $prepItSavingObj->componentByKey($key);
            $amount = $rentObj->value;
        }
        return $amount;
    }

    public function getItSavingInvestment($prepSalaryId, $userId)
    {
        $prepItSavingObj = PrepItSaving::where('prep_salary_id', $prepSalaryId)->where('user_id', $userId)->first();        
        $prepItComponents=[];
        if($prepItSavingObj){
            $prepItComponents = $prepItSavingObj->components;
        }
        $itInvestment = 0;
        foreach ($prepItComponents as $component) {
            if ($component->key == 'pf' || $component->key == 'pension_scheme_1' || $component->key == 'pension_scheme_1b' || $component->key == 'ppf' || $component->key == 'central_pension_fund' || $component->key == 'lic' || $component->key == 'housing_loan_repayment' || $component->key == 'term_deposit' || $component->key == 'national_saving_scheme' || $component->key == 'tax_saving' || $component->key == 'children_expense' || $component->key == 'other_investment' || $component->key == 'other_multiple_investments') {
                $itInvestment = $itInvestment + $component->value;
            }
        }
        return $itInvestment;
    }
    public function getItSavingDeduction($prepSalaryId, $userId)
    {
        $prepItSavingObj = PrepItSaving::where('prep_salary_id', $prepSalaryId)->where('user_id', $userId)->first();
       
        $prepItComponents=[];
        if($prepItSavingObj){
            $prepItComponents = $prepItSavingObj->components;

        }

        $itDeduction = 0;

        foreach ($prepItComponents as $component) {
            if ($component->key == 'lta' || $component->key == 'medical_insurance_premium' || $component->key == 'medical_treatment_expense' || $component->key == 'educational_loan' || $component->key == 'donation' || $component->key == 'rent_without_receipt' || $component->key == 'physical_disablity' || $component->key == 'other_deduction' || $component->key == 'other_multiple_deductions') {
                $itDeduction = $itDeduction + $component->value;
            }
        }

        return $itDeduction;
    }
    public function generateUserData($prepSalaryId) {

        $response['status'] = true;
        $response['errors'] = '';
        $prepSalary = PrepSalary::find($prepSalaryId);

        if($prepSalary && $prepSalary->status == 'completed' )
            return false;
        
        if(!$prepSalary)
            return $response;

        $monthObj = Month::find($prepSalary->month_id);
        $itSaving = ItSavingMonth::where('user_id', $this->userId)->where('month_id', $monthObj->id)->first();
        if (count($itSaving) > 0) {
            PrepItSaving::where('prep_salary_id', $prepSalaryId)->where('user_id',$this->userId)->get()->each(function ($prepItSaving) {
                        $prepItSaving->delete();
                    });

            $prepItSavingObj = new PrepItSaving();
            $prepItSavingObj->prep_salary_id = $prepSalaryId;
            $prepItSavingObj->user_id = $this->userId;
            $prepItSavingObj->it_savings_id = $itSaving->id;
            if ($prepItSavingObj->isValid()) {
                if (!$prepItSavingObj->save()) {
                    $response['errors'] = $response['errors'] . "Something went wrong";
                    $response['status'] = false;
                }
                foreach ($itSaving->toArray() as $key => $value) {
                    if (!empty($value) && !($key == "total_pf"  || $key == "total_investments" || $key == "total_deductions"  || $key == "id" || $key == "financial_year_id" || $key == "user_id" || $key == "created_at" || $key == "updated_at" || $key == "other_multiple_investments" || $key == "other_multiple_deductions")) {
                        $res = self::saveData($prepItSavingObj->id, $key, $value);
                        if ($res['status']) {
                            $response['errors'] = $response['errors'] . $res['errors'];
                        }
                    }
                }
                $otherMultipleInvestment = ItSavingMonthOther::where('it_saving_month_id', $itSaving->id)->where('type', 'other_multiple_investments')->sum('value');
                if (isset($otherMultipleInvestment) && !$otherMultipleInvestment == 0) {
                    $res = self::saveData($prepItSavingObj->id, 'other_multiple_investments', $otherMultipleInvestment);
                    if ($res['status']) {
                        $response['errors'] = $response['errors'] . $res['errors'];
                    }
                }

                $otherMultipleDeduction = ItSavingMonthOther::where('it_saving_month_id', $itSaving->id)->where('type', 'other_multiple_deductions')->sum('value');
                if (isset($otherMultipleDeduction) && !$otherMultipleDeduction == 0) {
                    $res = self::saveData($prepItSavingObj->id, 'other_multiple_deductions', $otherMultipleDeduction);
                    if ($res['status']) {
                        $response['errors'] = $response['errors'] . $res['errors'];
                    }
                }
            } else {
                $response['errors'] = $response['errors'] . $prepItSavingObj->getErrors();
                $response['status'] = false;
            }
        }
        return $response;
    }

    public function queue()
    {
        $prepSalaryComponent = PrepSalaryComponent::find($this->componentId) ;
        if(!$prepSalaryComponent)
            return false;
        $prepSalaryObj = PrepSalary::find($prepSalaryComponent->prep_salary_id);
        $userComponentTypeId = PrepSalaryComponentType::where('code','user')->first();
        if(!$userComponentTypeId)
            return false;
        $itSavingComponentType = PrepSalaryComponentType::where('code','it-saving')->first();
        if(!$itSavingComponentType)
            return false;
        $itSavingComponent = $prepSalaryObj->components->where('prep_salary_component_type_id',$itSavingComponentType->id)->first();
        if(!$itSavingComponent)
            return false;
        foreach($prepSalaryObj->prepUsers as $prepUser)
        {
            $prepSalaryExecution = PrepSalaryExecution::where('prep_salary_id',$prepSalaryComponent->prep_salary_id)->where('component_id',$itSavingComponent->id)->where('user_id',$prepUser->user_id)->first();
            if(!$prepSalaryExecution)
            {
                $prepSalaryExecutionObj = PrepSalaryExecution::create(['prep_salary_id' => $prepSalaryComponent->prep_salary_id,'component_id' => $itSavingComponent->id,'user_id' => $prepUser->user_id,'status' => 'init','counter' => 0]);
                if (!$prepSalaryExecutionObj->isValid()) {
                    continue;  
                }
            }
            if($prepSalaryExecution->status !== "completed"){
                dispatch(new ItSavingSalaryJob($prepUser->user_id, $prepSalaryComponent->prep_salary_id));
                }
        }   
        
    }
}
