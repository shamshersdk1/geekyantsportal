<?php
namespace App\Services\SalaryService;

use App\Models\Audited\Bonus\BonusConfirmed;
use App\Models\Audited\Bonus\PrepBonus;
use App\Models\Audited\Salary\PrepSalaryComponent;
use App\Services\SalaryService\PrepSalaryComponentInterface;
use App\Models\Audited\Salary\PrepGrossPaid;
use App\Models\Audited\Salary\PrepUser;
use App\Jobs\PrepareSalary\Components\BonusJob;
use App\Models\Audited\Salary\PrepSalary;
use App\Models\Audited\Salary\PrepSalaryComponentType;
use App\Models\Audited\Salary\PrepSalaryExecution;

class BonusSalaryComponent implements PrepSalaryComponentInterface
{
    private $userId;
    private $month;
    private $year;
    private $componentId;
    public function getValue()
    {
        return array("Zack" => "Zara", "Anthony" => "Any",
            "Ram" => "Rani", "Salim" => "Sara",
            "Raghav" => "Ravina");
    }
    public function setMonthYear($month, $year)
    {
        $this->month = $month;
        $this->year = $year;
    }
    public function setComponent($componentId)
    {
        $this->componentId = $componentId;
    }
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }
    public function generate()
    {
        $response['message'] = "";
        $response['errors'] = "";
        $response['status'] = true;

        $prepSalaryComponentObj = PrepSalaryComponent::find($this->componentId);
        if(!count($prepSalaryComponentObj)>0)
        {
            $response['errors'] = "No component found";
            $response['status'] = false;
            return $response;
        }
        PrepBonus::where('prep_salary_id',$prepSalaryComponentObj->prep_salary_id)->delete();
        $prepUsers = PrepUser::where('prep_salary_id',$prepSalaryComponentObj->prep_salary_id)->get();
        foreach ($prepUsers as $prepUser) {
            dispatch(new BonusJob($prepUser->user_id, $prepSalaryComponentObj->prep_salary_id));
        }
        return $response;
    }
    public function  getHtml(){
      
        $componentObj = PrepSalaryComponent::find($this->componentId);

        $prepBonusObj=PrepBonus::where('prep_salary_id',$componentObj->salary->id)->where('user_id',$this->userId)->get();

        $extra=0;$additional=0;$onsite=0;$techtalk=0;$referral=0;$performance=0;
        
        $response=[];
        if(count($prepBonusObj)>0){
            foreach($prepBonusObj as $bonus){
                $bonusObj=$bonus->bonus;
                if($bonusObj){
                    if($bonusObj->reference_type=='App\Models\Admin\OnSiteBonus'){
                        $onsite+=$bonusObj->amount;
                    }
                    elseif($bonusObj->reference_type=='App\Models\Admin\AdditionalWorkDaysBonus'){
                        $additional+=$bonusObj->amount;
                    }elseif($bonusObj->reference_type=='App\Models\User\UserTimesheetExtra'){
                        $extra+=$bonusObj->amount;
                    }elseif($bonusObj->reference_type=='App\Models\Admin\TechTalkBonus'){
                        $techtalk+=$bonusObj->amount;
                    }
                    elseif($bonusObj->reference_type=='App\Models\Admin\ReferralBonus'){
                        $referral+=$bonusObj->amount;
                    }
                    elseif($bonusObj->reference_type=='App\Models\Admin\PerformanceBonus'){
                        $performance+=$bonusObj->amount;
                    }
                    else{
                       
                    }

                }
            }
        }
        $response['bonus']['onsite']=$onsite;
        $response['bonus']['additional']=$additional;
        $response['bonus']['extra']=$extra;
        $response['bonus']['techtalk']=$techtalk;
        $response['bonus']['referral']=$referral;
        $response['bonus']['performance']=$performance;

        return $response;

    }

    public static function getTotalBonus($prepSalaryId,$userId) {
        $bonuses=PrepBonus::where('prep_salary_id',$prepSalaryId)->where('user_id',$userId)->get();
        $paidObj = PrepGrossPaid::where('prep_salary_id', $prepSalaryId)->where('user_id', $userId)->first();

        $overallMonthlyBonus=0;

        if(count($bonuses) > 0){
            foreach($bonuses as $bonus){
                $overallMonthlyBonus=$overallMonthlyBonus + $bonus->bonus->amount;
            }
            
        }
        return $overallMonthlyBonus;
    }

    public function queue()
    {
        $prepSalaryComponent = PrepSalaryComponent::find($this->componentId) ;
        if(!$prepSalaryComponent)
            return false;
        $prepSalaryObj = PrepSalary::find($prepSalaryComponent->prep_salary_id);
        $userComponentTypeId = PrepSalaryComponentType::where('code','user')->first();
        if(!$userComponentTypeId)
            return false;
        $bonusComponentType = PrepSalaryComponentType::where('code','bonus')->first();
        if(!$bonusComponentType)
            return false;
        $bonusComponent = $prepSalaryObj->components->where('prep_salary_component_type_id',$bonusComponentType->id)->first();
        if(!$bonusComponent)
            return false;
        foreach($prepSalaryObj->prepUsers as $prepUser)
        {
            $prepSalaryExecution = PrepSalaryExecution::where('prep_salary_id',$prepSalaryComponent->prep_salary_id)->where('component_id',$bonusComponent->id)->where('user_id',$prepUser->user_id)->first();
            if(!$prepSalaryExecution)
            {
                $prepSalaryExecutionObj = PrepSalaryExecution::create(['prep_salary_id' => $prepSalaryComponent->prep_salary_id,'component_id' => $bonusComponent->id,'user_id' => $prepUser->user_id,'status' => 'init','counter' => 0]);
                if (!$prepSalaryExecutionObj->isValid()) {
                    continue;  
                }
            }
            if(!$prepSalaryExecution->status=="completed")
                dispatch(new BonusJob($prepUser->user_id, $prepSalaryComponent->prep_salary_id));
        }   
        
    }


}
