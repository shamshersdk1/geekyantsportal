<?php
namespace App\Services\SalaryService;

use App\Jobs\PrepareSalary\Components\WorkingDayJob;
use App\Models\Audited\Salary\PrepSalaryComponent;
use App\Models\Audited\Salary\PrepUser;
use App\Models\Audited\Salary\PrepWorkingDay;
use App\Models\Month;
use App\Services\Leave\UserLeaveService;
use App\Services\SalaryService\PrepSalaryComponentInterface;
use App\Models\Audited\Salary\PrepSalary;
use App\Models\Audited\Salary\PrepSalaryComponentType;
use App\Models\Audited\Salary\PrepSalaryExecution;
use App\Services\NewLeaveService;
use App\Models\Audited\WorkingDay\UserWorkingDay;

class WorkingDayComponent implements PrepSalaryComponentInterface
{

    private $userId;
    private $month;
    private $year;
    private $componentId;

    public function getValue()
    {
        return array("Zack" => "Zara", "Anthony" => "Any",
            "Ram" => "Rani", "Salim" => "Sara",
            "Raghav" => "Ravina");
    }
    public function setMonthYear($month, $year)
    {
        $this->month = $month;
        $this->year = $year;
    }
    public function setComponent($componentId)
    {
        $this->componentId = $componentId;
    }
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }
    public function generate()
    {
        $response['errors'] = "";
        $response['status'] = true;
        $response['message'] = "";

        $componentObj = PrepSalaryComponent::find($this->componentId);
        if (!count($componentObj) > 0) {
            $response['errors'] = "No component found";
            $response['status'] = false;
            return $response;
        }
        PrepWorkingDay::where('prep_salary_id', $componentObj->prep_salary_id)->delete();
        $prepUsers = PrepUser::where('prep_salary_id', $componentObj->prep_salary_id)->get();
        if (!count($prepUsers) > 0) {
            $response['errors'] = "No Prep User found";
            $response['status'] = false;
            return $response;
        }
        $monthObj = Month::where("month", $this->month)->where('year', $this->year)->first();
        if (!count($monthObj) > 0) {
            $response['errors'] = "No Month found";
            $response['status'] = false;
            return $response;
        }
        if ($monthObj->workingDaySetting ? $monthObj->workingDaySetting->value == "open" : true) {
            $response['errors'] = "User Working Day Month not locked";
            $response['status'] = false;
            return $response;
        }

        foreach ($prepUsers as $prepUser) {
            dispatch(new WorkingDayJob($prepUser->user_id, $componentObj->prep_salary_id));
        }
        return $response;
    }

    public function generateUserData($prepSalaryId)
    {
        $response['status'] = true;
        $response['errors'] = '';
        PrepWorkingDay::where('prep_salary_id', $prepSalaryId)->where('user_id',$this->userId)->delete();
        $prepSalary = PrepSalary::find($prepSalaryId);
        $monthObj = Month::find($prepSalary->month_id);
        $startDate = date('Y-m-d', strtotime($monthObj->getFirstDay()));
        $endDate = date('Y-m-d', strtotime($monthObj->getLastDay()));
        $prepWorkingDayObj = new PrepWorkingDay();
        $prepWorkingDayObj->sl_count = NewLeaveService::getTotalLeaves($startDate, $endDate, $this->userId, 'sick');
        $prepWorkingDayObj->pl_count = NewLeaveService::getTotalLeaves($startDate, $endDate, $this->userId, 'paid');
        $prepWorkingDayObj->company_working_days = $monthObj->working_days;
        $userWorkingObj = UserWorkingDay::where('user_id',$this->userId)->where('month_id',$monthObj->id)->first();
        $prepWorkingDayObj->user_worked_days = $userWorkingObj->working_days - abs($userWorkingObj->lop);
        $prepWorkingDayObj->user_working_days = $userWorkingObj->working_days;
        $prepWorkingDayObj->user_id = $this->userId;
        $prepWorkingDayObj->prep_salary_id = $prepSalaryId;
        if (!$prepWorkingDayObj->save() && !$prepWorkingDayObj->isValid()) {
            $response['status'] = false;
            $response['errors'] = $prepWorkingDayObj->getErrors();
        }
        return $response;
    }
    public function getHtml()
    {
        $componentObj = PrepSalaryComponent::find($this->componentId);
        $response = [];
        $data = PrepWorkingDay::where('prep_salary_id', $componentObj->salary->id)->where('user_id', $this->userId)->first();
        if ($data && isset($data)) {
            $response = $data->toArray();
        }

        return $response;
    }

    public static function getWorkingDaysByDate($startDate, $enddate, $userId)
    {

        return UserLeaveService::calculateWorkingDays($startDate, $enddate, $userId);
    }

    public function getTotalWorkingDays($prepSalaryId, $userId)
    {
        $prepWorkingDay = PrepWorkingDay::where('prep_salary_id', $prepSalaryId)->where('user_id', $userId)->first();

        $totalWorkingDays = 0;
        if ($prepWorkingDay) {
            $totalWorkingDays = $prepWorkingDay['working_days'];
        }
        return $totalWorkingDays;
    }

    public function queue()
    {
        $prepSalaryComponent = PrepSalaryComponent::find($this->componentId) ;
        if(!$prepSalaryComponent)
            return false;
        $prepSalaryObj = PrepSalary::find($prepSalaryComponent->prep_salary_id);
        $userComponentTypeId = PrepSalaryComponentType::where('code','user')->first();
        if(!$userComponentTypeId)
            return false;
        $workingDayComponentType = PrepSalaryComponentType::where('code','working-day')->first();
        if(!$workingDayComponentType)
            return false;
        $workingDayComponent = $prepSalaryObj->components->where('prep_salary_component_type_id',$workingDayComponentType->id)->first();
        if(!$workingDayComponent)
            return false;
        foreach($prepSalaryObj->prepUsers as $prepUser)
        {
            $prepSalaryExecution = PrepSalaryExecution::where('prep_salary_id',$prepSalaryComponent->prep_salary_id)->where('component_id',$workingDayComponent->id)->where('user_id',$prepUser->user_id)->first();
            if(!$prepSalaryExecution)
            {
                $prepSalaryExecutionObj = PrepSalaryExecution::create(['prep_salary_id' => $prepSalaryComponent->prep_salary_id,'component_id' => $workingDayComponent->id,'user_id' => $prepUser->user_id,'status' => 'init','counter' => 0]);
                if (!$prepSalaryExecutionObj->isValid()) {
                    continue;  
                }
            }
            if(!$prepSalaryExecution->status=="completed")
                dispatch(new WorkingDayJob($prepUser->user_id, $prepSalaryComponent->prep_salary_id));
        }   
    }

}
