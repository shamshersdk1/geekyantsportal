<?php namespace App\Services;

use Validator;
use App\Models\Admin\LateComer;
use App\Models\User;

use App\Events\Late\LateMarked;
use App\Events\Late\LateUnmarked;

use Input;
use Redirect;
use Auth;
use Config;
use Exception;


class LateComerService
{
  public static function saveData($user_id, $date, $reportedBy_id)
  {
    $status = true;
    $message = '';
    $isValidated = self::validateSave($user_id, $date);
    if ( $isValidated )
    {
      $lateComerObj = new LateComer();
      $lateComerObj->user_id = $user_id;
      $lateComerObj->reported_by_id = $reportedBy_id;
      $lateComerObj->date = $date;

      if (!$lateComerObj->save()) {
        $status = false;
        $message = $lateComerObj->getErrors();
      }
    }
    else
    {
      $status = false;
      $message = 'User is already added in the late comers list';
    }
    if ( $status )
    {
      event(new LateMarked($lateComerObj->user_id, $lateComerObj->reported_by_id, $lateComerObj->date));
    }
    $response['status'] = $status;
    $response['message'] = $message;

    return $response;
  }

  public static function validateSave($user_id, $date)
  {
    $status = true;
    
    $count = LateComer::where('user_id', $user_id)->where('date', $date)->count();
    if ( $count > 0 )
    {
      $status = false;
    }
    
    return $status;
  }

  public static function delete($id)
  {
    $status = true;
    $message = '';
    
    $lateComerObj = LateComer::find($id);
    
    if ( !empty($lateComerObj) )
    {
      if ( !$lateComerObj->delete() )
      {
        $status = false;
        $message = $lateComerObj->getErrors();
      }
    }
    else
    {
      $status = false;
      $message = 'Late comer record not found';
    }
    if ( $status )
    {
      event(new LateUnmarked($lateComerObj->user_id, $lateComerObj->reported_by_id, $lateComerObj->date));
    }
    $response['status'] = $status;
    $response['message'] = $message;

    return $response;
  }

}