<?php namespace App\Services;

use Validator;
use App\Models\User;
use App\Models\FloorsSeatUser;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use App\Models\Floor;
use App\Models\FloorsSeat;

use DB;
use Input;
use Redirect;
use Auth;

class FloorService {

	public static function floorDetails($floorSeats)
	{
        // dd($floorSeats->toArray());
        $result = [];
        foreach ( $floorSeats as $floorSeat)
        {
            $result_obj = [];
            $user_details = '';
            $user_details = FloorsSeatUser::with('assigned_user','user_profile','floors_seat')->where('floor_seat_id', $floorSeat->id)->first();
            if ( isset($user_details) ){
                $user_details = $user_details->toArray();
            }
            $result_obj['id'] = $floorSeat->id;
            $result_obj['floor_id'] = $floorSeat->floor_id;
            $result_obj['name'] = $floorSeat->name;
            $result_obj['assignable'] = $floorSeat->assignable;
            $result_obj['user_details'] = !empty($user_details) ? $user_details : '' ;
            $result[] = $result_obj;
        }
        return $result;
	}
    public static function getSeatArragement($floorId)
    {
        $floors = Floor::all();
        foreach ($floors as $floors) {
            $data = [];
            $floor = Floor::find($floorId);
            if($floor) {
                $seats = $floor->seats->toArray();
                $rows = array_chunk($seats, 6);
                if(count($rows)>0) {
                    foreach ($rows as $seats) {
                        $temp =[];
                        foreach ($seats as $seat) {
                            $temp[] = FloorsSeat::find($seat['id']);
                        }
                        $data[] = $temp;
                    }
                }
            }
        }
        
        return $data;
    }
    public static function formatFloorDetails($floor_details_unformatted)
    {
        $floor_details = [];
        $j = 0;
        $k = 0;
        for ($i=0; $i < count($floor_details_unformatted); $i++) 
        {
            $floor_details[$j][$k] = $floor_details_unformatted[$i];
            $k++;
            if ( ($k == 6)  )
            {
                $j++;
                $k = 0;
            }            
        }
        return $floor_details;
    }
}