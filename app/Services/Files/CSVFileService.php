<?php
namespace App\Services\Files;

use Log;
use Mail;
use Exception;
use Config;
use Excel;

use App\Models\AccessLog;
use App\Models\User;
use App\Models\Admin\PayslipData;
use App\Models\Admin\PayroleGroupRule;

class CSVFileService
{
    // protected $user;

    public function __construct()
    {
        // $this->user = $user;
    }
    public static function csvParser($filePath) {
        
        $handle = fopen($filePath, "r");
        $first = true;
        $headers = [];
        $data = [];
        while ($csvLine = fgetcsv($handle, 2000, ",")) {
            
            if($first) {
                $first = false;
                $headers = $csvLine;
            } else {
                $data[] = $csvLine;
            }
        }

        $headerResponse = self::validateHeader($headers);
        //if($headerResponse['status'] == true)
        $data = self::parseCSVData($data);
        $response['status'] = $headerResponse['status'];
        $response['data'] = $data;

        return $response;
    }

    public static function validateHeader($header) {
        
        $message = '';
        $response['status'] = false;
        $response['message'] = $message;
        
        if(count($header) < 55) {
            $error = true;
            $message = "Invalid number of columns in the CSV";
        }

        if($header[1] =="Employee Code"
            && $header[2] =="Employee Name"
            && $header[3] =="Email Address"
            && $header[4] =="PAN No"
            && $header[5] =="DOB"
            && $header[6] =="DOJ"
            && $header[7] =="Designation"
            && $header[8] =="Gender"
            && $header[9] =="Bank A/c. No."
            && $header[10] =="Bank IFSC Code"
            && $header[11] =="PF No"
            && $header[12] =="UAN No"
            && $header[13] =="ESI No"
            && $header[14] =="Month"
            && $header[15] =="Year"
            && $header[16] =="Total Working Day in Month"
            && $header[17] =="No of Days Worked"
            && $header[18] =="Basic"
            && $header[19] =="HRA"
            && $header[20] =="Conveyance Allowance"
            && $header[21] =="Car Allowance"
            && $header[22] =="Medical Allowance"
            && $header[23] =="Food Allowance"
            && $header[24] =="Special Allowance"
            && $header[25] =="Gross Salary"
            && strpos($header[26], 'P.F.') !== false
            && strpos($header[27], 'E.S.I.') !== false
            && $header[28] =="VPF"
            && $header[29] =="Professional Tax"
            && $header[30] =="Food Deduction"
            && $header[31] =="Medical Insurance"
            && $header[32] =="T.D.S."
            && $header[33] =="LOAN"
            && $header[34] =="TOTAL DEDUCTION FOR MONTH"
            && $header[35] =="NET SALARY FOR THE MONTH"
            && $header[36] =="Arrear Salary"
            && strpos($header[37], 'Bonus') !== false
            && strpos($header[38], 'Variable') !== false // Qtr Variable Bonus
            && strpos($header[39], 'Performance') !== false // Performance Bonus
            && $header[40] =="Non-Cash Incentive"
            && $header[41] =="Gross Earnings"
            && $header[42] =="Amount Payable"
            && $header[43] =="SL(Op Bal)"
            && $header[44] =="CL (Op Bal)"
            && $header[45] =="PL (Op Bal)"
            && $header[46] =="CME SL"
            && $header[47] =="CME CL"
            && $header[48] =="CME PL"
            && $header[49] =="SL Availed(utilised)"
            && $header[50] =="CL Availed"
            && $header[51] =="PL Availed(utilised)"
            && $header[52] =="Bal c/f SL"
            && $header[53] =="Bal c/f CL"
            && $header[54] =="Bal c/f PL"
            && $header[55] =="Leave Travel Allowance"
            // && $header[55] =="Advance Salary"
        ) {
            $response['status'] = true;
            $response['message'] = "CSV Column";
        }else {
            $response['status'] = false;
            $response['message'] = "CSV Column mismatch";
        }
        return $response;
    }
    public static function parseCSVData($dataArr) {
        $processData =[];
        // for ( $i=7; $i < count($dataArr) - 1; $i++ ) {
        //     if( empty($dataArr[$i][1]) ) {
        //         break;
        //     }
        foreach ($dataArr as $data) {
            
            if(empty($data[1]))
                break;
            //$data =$dataArr[$i]; 
            $tempData =[];
            $tempData['employee_id'] = $data[1];
            $tempData['name'] = $data[2];
            $tempData['email'] = trim($data[3])==='-'?null:trim($data[3]);
            //$tempData['payslip_month_id'] = $payslipMonthId;
            $tempData['pan_no'] = $data[4];
            $tempData['date_of_birth'] = $data[5];
            $tempData['date_of_joining'] = $data[6];
            $tempData['designation'] = $data[7];
            $tempData['gender'] = $data[8];
            $tempData['bank_account_no'] = $data[9];
            $tempData['bank_ifsc'] = $data[10];
            $tempData['pf_no'] = $data[11];
            $tempData['uan_no'] = $data[12];
            $tempData['esi_no'] = $data[13];
            $tempData['month'] = $data[14];
            $tempData['year'] = $data[15];
            $tempData['working_day_month'] = $data[16];
            $tempData['days_worked'] = $data[17];
            //integer values, default should be 0
            $tempData['basic'] = trim($data[18])==='-'?0:trim($data[18]);
            $tempData['hra'] = trim($data[19])==='-'?0:trim($data[19]);
            $tempData['conveyance_allowance'] = trim($data[20])==='-'?0:trim($data[20]);
            $tempData['car_allowance'] = trim($data[21])==='-'?0:trim($data[21]);
            $tempData['medical_allowance'] = trim($data[22])==='-'?0:trim($data[22]);
            $tempData['food_allowance'] = trim($data[23])==='-'?0:trim($data[23]);
            $tempData['special_allowance'] = trim($data[24])==='-'?0:trim($data[24]);
            $tempData['gross_salary'] = trim($data[25])==='-'?0:trim($data[25]);
            $tempData['pf_12_employee'] = trim($data[26])==='-'?0:trim($data[26]);
            $tempData['esi_1_75_employee'] = trim($data[27]) === '-'?0:trim($data[27]);
            $tempData['vpf'] = trim($data[28]) === '-' ? 0: trim($data[28]);
            $tempData['professional_tax'] = trim($data[29]) === '-' ? 0: trim($data[29]);
            $tempData['food_deduction'] = trim($data[30]) === '-' ? 0: trim($data[30]);
            $tempData['medical_insurance'] = trim($data[31]) === '-' ? 0: trim($data[31]);
            $tempData['tds'] = trim($data[32]) === '-' ? 0: trim($data[32]);
            $tempData['loan'] = trim($data[33]) === '-' ? 0: trim($data[33]);
            $tempData['total_deduction_month'] = trim($data[34]) === '-' ? 0: trim($data[34]);
            $tempData['net_salary_month'] = trim($data[35]) === '-' ? 0: trim($data[35]);
            $tempData['arrear_salary'] = trim($data[36]) === '-' ? 0: trim($data[36]);
            $tempData['annual_bonus'] = trim($data[37]) === '-' ? 0: trim($data[37]);
            $tempData['qtr_variable_bonus'] = trim($data[38]) === '-' ? 0: trim($data[38]);
            $tempData['performance_bonus'] = trim($data[39]) === '-' ? 0: trim($data[39]);
            $tempData['non_cash_incentive'] = trim($data[40]) === '-' ? 0: trim($data[40]);
            $tempData['gross_earnings'] = trim($data[41]) === '-' ? 0: trim($data[41]);
            $tempData['amount_payable'] = trim($data[42]) === '-' ? 0: trim($data[42]);
            $tempData['sl_op_bal'] = trim($data[43]) === '-' ? 0: trim($data[43]);
            $tempData['cl_op_bal'] = trim($data[44]) === '-' ? 0: trim($data[44]);
            $tempData['pl_op_bal'] = trim($data[45]) === '-' ? 0: trim($data[45]);
            $tempData['cme_sl'] = trim($data[46]) === '-' ? 0: trim($data[46]);
            $tempData['cme_cl'] = trim($data[47]) === '-' ? 0: trim($data[47]);
            $tempData['cme_pl'] = trim($data[48]) === '-' ? 0: trim($data[48]);
            $tempData['sl_availed'] = trim($data[49]) === '-' ? 0: trim($data[49]);
            $tempData['cl_availed'] = trim($data[50]) === '-' ? 0: trim($data[50]);
            $tempData['pl_availed'] = trim($data[51]) === '-' ? 0: trim($data[51]);
            $tempData['bal_cf_sl'] = trim($data[52]) === '-' ? 0: trim($data[52]);
            $tempData['bal_cf_cl'] = trim($data[53]) === '-' ? 0: trim($data[53]);
            $tempData['bal_cf_pl'] = trim($data[54]) === '-' ? 0: trim($data[54]);
            if (!empty($data[55]))
                $tempData['lta'] = trim($data[55]) === '-' ? 0: trim($data[55]);
            if (!empty($data[56]))
                $tempData['advance_salary'] = trim($data[56]) === '-' ? 0: trim($data[56]);
            $processData[] = $tempData;
        }
        return $processData;
    }
    public static function createAndStoreExcel($data) {

        $path = 'excel/exports';
        $filename = "test9";
        $type = 'csv';
        Excel::create($filename, function ($excel) use ($data) {
            $excel->sheet('mySheet', function ($sheet) use ($data) {
                $sheet->fromArray($data);
            });
        })->store($type, storage_path($path));
        return $path.'/'.$filename.".".$type;
    }
}
