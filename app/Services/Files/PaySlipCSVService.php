<?php
namespace App\Services\Files;

use App\Models\Admin\Appraisal;
use App\Models\Admin\AppraisalBonus;
use App\Models\Admin\Bonus;
use App\Models\Admin\Finance\AdvanceSalary;
use App\Models\Admin\Finance\AdvanceSalaryDeduction;
use App\Models\Admin\Finance\ItSavingUserData;
use App\Models\Admin\Finance\PayroleGroupRuleMonth;
use App\Models\Admin\Finance\SalaryUserData;
use App\Models\Admin\Finance\SalaryUserDataKey;
use App\Models\Admin\Insurance\InsuranceDeduction;
use App\Models\Admin\ItSaving;
use App\Models\Admin\VPF;
use App\Models\Admin\VpfDeduction;
use App\Models\Loan;
use App\Models\LoanEmi;
use App\Models\Month;
use App\Models\Transaction;
use App\Models\User;
use Log;

class PaySlipCSVService
{
    // protected $user;

    public function __construct()
    {
        // $this->user = $user;
    }
    public static function csvParser($filePath, $date)
    {

        $handle = fopen($filePath, "r");
        $first = true;
        $second = false;
        $headers = [];
        $data = [];
        while ($csvLine = fgetcsv($handle, 2000, ",")) {
            if ($first) {
                $first = false;
                $second = true;
                $headers = $csvLine;
            } else {
                if ($second) {
                    $second = false;
                } else {
                    $data[] = $csvLine;
                }

            }
        }
        //$headerResponse = self::validateHeader($headers);
        //if($headerResponse['status'] == true)
        $data = self::parseCSVData($data, $date);
        $response['status'] = $headerResponse['status'];
        $response['data'] = $data;

        return $response;
    }
    public static function validatePayslipHeader($header)
    {
        $message = '';
        $response['status'] = false;
        $response['message'] = $message;

        if (count($header) < 55) {
            $error = true;
            $message = "Invalid number of columns in the CSV";
        }

        //sif($header[1] =="Employee Code")
    }
    public static function validateHeader($header)
    {

        $message = '';
        $response['status'] = false;
        $response['message'] = $message;

        if (count($header) < 55) {
            $error = true;
            $message = "Invalid number of columns in the CSV";
        }

        if ($header[1] == "Employee Code"
            && $header[2] == "Employee Name"
            && $header[3] == "Email Address"
            && $header[4] == "PAN No"
            && $header[5] == "DOB"
            && $header[6] == "DOJ"
            && $header[7] == "Designation"
            && $header[8] == "Gender"
            && $header[9] == "Bank A/c. No."
            && $header[10] == "Bank IFSC Code"
            && $header[11] == "PF No"
            && $header[12] == "UAN No"
            && $header[13] == "ESI No"
            && $header[14] == "Month"
            && $header[15] == "Year"
            && $header[16] == "Total Working Day in Month"
            && $header[17] == "No of Days Worked"
            && $header[18] == "Basic"
            && $header[19] == "HRA"
            && $header[20] == "Conveyance Allowance"
            && $header[21] == "Car Allowance"
            && $header[22] == "Medical Allowance"
            && $header[23] == "Food Allowance"
            && $header[24] == "Special Allowance"
            && $header[25] == "Gross Salary"
            && strpos($header[26], 'P.F.') !== false
            && strpos($header[27], 'E.S.I.') !== false
            && $header[28] == "VPF"
            && $header[29] == "Professional Tax"
            && $header[30] == "Food Deduction"
            && $header[31] == "Medical Insurance"
            && $header[32] == "T.D.S."
            && $header[33] == "LOAN"
            && $header[34] == "TOTAL DEDUCTION FOR MONTH"
            && $header[35] == "NET SALARY FOR THE MONTH"
            && $header[36] == "Arrear Salary"
            && strpos($header[37], 'Bonus') !== false
            && strpos($header[38], 'Variable') !== false// Qtr Variable Bonus
             && strpos($header[39], 'Performance') !== false// Performance Bonus
             && $header[40] == "Non-Cash Incentive"
            && $header[41] == "Gross Earnings"
            && $header[42] == "Amount Payable"
            && $header[43] == "SL(Op Bal)"
            && $header[44] == "CL (Op Bal)"
            && $header[45] == "PL (Op Bal)"
            && $header[46] == "CME SL"
            && $header[47] == "CME CL"
            && $header[48] == "CME PL"
            && $header[49] == "SL Availed(utilised)"
            && $header[50] == "CL Availed"
            && $header[51] == "PL Availed(utilised)"
            && $header[52] == "Bal c/f SL"
            && $header[53] == "Bal c/f CL"
            && $header[54] == "Bal c/f PL"
            && $header[55] == "Leave Travel Allowance"
            // && $header[55] =="Advance Salary"
        ) {
            $response['status'] = true;
            $response['message'] = "CSV Column";
        } else {
            $response['status'] = false;
            $response['message'] = "CSV Column mismatch";
        }
        return $response;
    }
    public static function saveSalaryData($item, $date)
    {
        $monthId = Month::getMonth($date);

        //$monthId = 9; //DEC
        $user = User::where('employee_id', $item[1])->first();

        if (!$user) {
            return false;
        }

        $data = [];
        if (!empty($item[1]) && !empty($item[46])) {
            $data['date'] = $date;
            $data['month_id'] = $monthId;
            $data['financial_year_id'] = 1;
            $data['user_id'] = $user->id;
            $data['reference_type'] = 'App\Models\Admin\Finance\PayroleGroupRuleMonth';
            $appraisal = Appraisal::getAppraisalByDate($user->id, $date);
            if ($appraisal) {
                $groupId = $appraisal->payrole_group_id;
                $salaryData['user_id'] = $user->id;
                $salaryData['employee_id'] = $user->employee_id;
                $salaryData['appraisal_id'] = $appraisal->id;
                $salaryData['month_id'] = $monthId;
                $salaryData['days_worked'] = $item[5];
                $salaryData['monthly_ctc'] = $item[3];
                $salaryData['monthly_variable_bonus'] = $item[31];
                $salaryData['annual_bonus'] = $item[30];
                $salaryKeyId = SalaryUserData::saveData($salaryData);
                $salaryDataKey['salary_user_data_id'] = $salaryKeyId;

                if (!empty($item[6])) {
                    // Basic

                    $ruleObj = PayroleGroupRuleMonth::where('payrole_group_id', $groupId)->where('name', 'basic')->where('month_id', $monthId)->first();
                    if ($ruleObj) {
                        $amount = $item[6];
                        $data['reference_id'] = $ruleObj->id;
                        $data['type'] = 'credit';
                        $data['amount'] = $amount;
                        if ($salaryKeyId) {
                            $salaryDataKey['amount'] = $amount;
                            $salaryDataKey['key_id'] = $ruleObj->id;
                            SalaryUserDataKey::saveData($salaryDataKey);
                            Transaction::saveTransaction($data);
                        }
                    }
                }
                if (!empty($item[7])) {
                    // HRA
                    $ruleObj = PayroleGroupRuleMonth::where('payrole_group_id', $groupId)->where('name', 'hra')->where('month_id', $monthId)->first();
                    if ($ruleObj) {
                        $amount = $item[7];
                        $data['reference_id'] = $ruleObj->id;
                        $data['type'] = 'credit';
                        $data['amount'] = $amount;
                        if ($salaryKeyId) {
                            $salaryDataKey['amount'] = $amount;
                            $salaryDataKey['key_id'] = $ruleObj->id;
                            SalaryUserDataKey::saveData($salaryDataKey);
                            Transaction::saveTransaction($data);
                        }
                    }
                }
                if (!empty($item[8])) {
                    // car_allowance
                    $ruleObj = PayroleGroupRuleMonth::where('payrole_group_id', $groupId)->where('name', 'car_allowance')->where('month_id', $monthId)->first();
                    if ($ruleObj) {
                        $amount = $item[8];
                        $data['reference_id'] = $ruleObj->id;
                        $data['type'] = 'credit';
                        $data['amount'] = $amount;
                        if ($salaryKeyId) {
                            $salaryDataKey['amount'] = $amount;
                            $salaryDataKey['key_id'] = $ruleObj->id;
                            SalaryUserDataKey::saveData($salaryDataKey);
                            Transaction::saveTransaction($data);
                        }
                    }
                }
                if (!empty($item[9])) {
                    // food_allowance
                    $ruleObj = PayroleGroupRuleMonth::where('payrole_group_id', $groupId)->where('name', 'food_allowance')->where('month_id', $monthId)->first();
                    if ($ruleObj) {
                        $amount = $item[9];
                        $data['reference_id'] = $ruleObj->id;
                        $data['type'] = 'credit';
                        $data['amount'] = $amount;
                        if ($salaryKeyId) {
                            $salaryDataKey['amount'] = $amount;
                            $salaryDataKey['key_id'] = $ruleObj->id;
                            SalaryUserDataKey::saveData($salaryDataKey);
                            Transaction::saveTransaction($data);
                        }
                    }
                }
                if (!empty($item[10])) {
                    // leave_travel_allowance
                    $ruleObj = PayroleGroupRuleMonth::where('payrole_group_id', $groupId)->where('name', 'leave_travel_allowance')->where('month_id', $monthId)->first();
                    if ($ruleObj) {
                        $amount = $item[10];
                        $data['reference_id'] = $ruleObj->id;
                        $data['type'] = 'credit';
                        $data['amount'] = $amount;
                        if ($salaryKeyId) {
                            $salaryDataKey['amount'] = $amount;
                            $salaryDataKey['key_id'] = $ruleObj->id;
                            SalaryUserDataKey::saveData($salaryDataKey);
                            Transaction::saveTransaction($data);
                        }
                    }
                }
                if (!empty($item[11])) {
                    // special_allowance

                    $ruleObj = PayroleGroupRuleMonth::where('payrole_group_id', $groupId)->where('name', 'special_allowance')->where('month_id', $monthId)->first();
                    if ($ruleObj) {
                        $amount = $item[11];
                        $data['reference_id'] = $ruleObj->id;
                        $data['type'] = 'credit';
                        $data['amount'] = $amount;
                        if ($salaryKeyId) {
                            $salaryDataKey['amount'] = $amount;
                            $salaryDataKey['key_id'] = $ruleObj->id;
                            SalaryUserDataKey::saveData($salaryDataKey);
                            Transaction::saveTransaction($data);
                        }
                    }
                }
                // $item[12]   - Arrear salary

                if (!empty($item[13])) {
                    // gross_earning_for_the_month

                    $ruleObj = PayroleGroupRuleMonth::where('payrole_group_id', $groupId)->where('name', 'gross_earning_for_the_month')->where('month_id', $monthId)->first();
                    if ($ruleObj) {
                        $amount = $item[13];
                        $data['reference_id'] = $ruleObj->id;
                        $data['type'] = 'credit';
                        $data['amount'] = $amount;
                        if ($salaryKeyId) {
                            $salaryDataKey['amount'] = $amount;
                            $salaryDataKey['key_id'] = $ruleObj->id;
                            SalaryUserDataKey::saveData($salaryDataKey);
                            Transaction::saveTransaction($data);

                        }
                    }
                }
                if (!empty($item[14])) {
                    // pf_employee
                    $ruleObj = PayroleGroupRuleMonth::where('payrole_group_id', $groupId)->where('name', 'pf_employee')->where('month_id', $monthId)->first();
                    if ($ruleObj) {
                        $amount = $item[14];
                        $data['reference_id'] = $ruleObj->id;
                        $data['type'] = 'debit';
                        $data['amount'] = -1 * $amount;
                        if ($salaryKeyId) {
                            $salaryDataKey['amount'] = $amount;
                            $salaryDataKey['key_id'] = $ruleObj->id;
                            SalaryUserDataKey::saveData($salaryDataKey);
                            Transaction::saveTransaction($data);
                        }
                    }
                }
                if (!empty($item[15])) {
                    // pf_employeer
                    $ruleObj = PayroleGroupRuleMonth::where('payrole_group_id', $groupId)->where('name', 'pf_employeer')->where('month_id', $monthId)->first();
                    if ($ruleObj) {
                        $amount = $item[15];
                        $data['reference_id'] = $ruleObj->id;
                        $data['type'] = 'debit';
                        $data['amount'] = -1 * $amount;
                        if ($salaryKeyId) {
                            $salaryDataKey['amount'] = $amount;
                            $salaryDataKey['key_id'] = $ruleObj->id;
                            SalaryUserDataKey::saveData($salaryDataKey);
                            Transaction::saveTransaction($data);
                        }
                    }
                }
                if (!empty($item[16])) {
                    // pf_other
                    $ruleObj = PayroleGroupRuleMonth::where('payrole_group_id', $groupId)->where('name', 'pf_other')->where('month_id', $monthId)->first();
                    if ($ruleObj) {
                        $amount = $item[16];
                        $data['reference_id'] = $ruleObj->id;
                        $data['type'] = 'debit';
                        $data['amount'] = -1 * $amount;
                        if ($salaryKeyId) {
                            $salaryDataKey['amount'] = $amount;
                            $salaryDataKey['key_id'] = $ruleObj->id;
                            SalaryUserDataKey::saveData($salaryDataKey);
                            Transaction::saveTransaction($data);
                        }
                    }
                }
                if (!empty($item[17])) {
                    // other_deduction
                    $amount = $item[17];
                    $vpfObj = VPF::where('user_id', $user->id)->first();
                    if ($vpfObj) {
                        $vpfDeduction = new VpfDeduction;
                        $vpfDeduction->month_id = $monthId;
                        $vpfDeduction->user_id = $user->id;
                        $vpfDeduction->vpf_id = $vpfObj->id;
                        $vpfDeduction->amount = $amount;
                        if ($vpfDeduction->save()) {
                            $data['reference_id'] = $vpfDeduction->id;
                            $data['reference_type'] = 'App\Models\Admin\VpfDeduction';
                            $data['type'] = 'debit';
                            $data['amount'] = -1 * $amount;
                            Transaction::saveTransaction($data);

                        }
                    }

                }
                $data['reference_type'] = 'App\Models\Admin\Finance\PayroleGroupRuleMonth';
                if (!empty($item[18])) {
                    // esi_175
                    $ruleObj = PayroleGroupRuleMonth::where('payrole_group_id', $groupId)->where('name', 'esi_175')->where('month_id', $monthId)->first();
                    if ($ruleObj) {
                        $amount = $item[18];
                        $data['reference_id'] = $ruleObj->id;
                        $data['type'] = 'debit';
                        $data['amount'] = -1 * $amount;
                        if ($salaryKeyId) {
                            $salaryDataKey['amount'] = $amount;
                            $salaryDataKey['key_id'] = $ruleObj->id;
                            SalaryUserDataKey::saveData($salaryDataKey);
                            Transaction::saveTransaction($data);
                        }
                    }
                }
                if (!empty($item[19])) {
                    // esi_475
                    $ruleObj = PayroleGroupRuleMonth::where('payrole_group_id', $groupId)->where('name', 'esi_475')->where('month_id', $monthId)->first();
                    if ($ruleObj) {
                        $amount = $item[19];
                        $data['reference_id'] = $ruleObj->id;
                        $data['type'] = 'debit';
                        $data['amount'] = -1 * $amount;
                        if ($salaryKeyId) {
                            $salaryDataKey['amount'] = $amount;
                            $salaryDataKey['key_id'] = $ruleObj->id;
                            SalaryUserDataKey::saveData($salaryDataKey);
                            Transaction::saveTransaction($data);
                        }
                    }
                }
                if (!empty($item[20])) {
                    // professional_tax
                    $ruleObj = PayroleGroupRuleMonth::where('payrole_group_id', $groupId)->where('name', 'professional_tax')->where('month_id', $monthId)->first();
                    if ($ruleObj) {
                        $amount = $item[20];
                        $data['reference_id'] = $ruleObj->id;
                        $data['type'] = 'debit';
                        $data['amount'] = -1 * $amount;
                        if ($salaryKeyId) {
                            $salaryDataKey['amount'] = $amount;
                            $salaryDataKey['key_id'] = $ruleObj->id;
                            SalaryUserDataKey::saveData($salaryDataKey);
                            Transaction::saveTransaction($data);
                        }
                    }
                }
                if (!empty($item[21])) {
                    // food_deduction
                    $ruleObj = PayroleGroupRuleMonth::where('payrole_group_id', $groupId)->where('name', 'food_deduction')->where('month_id', $monthId)->first();
                    if ($ruleObj) {
                        $amount = $item[21];
                        $data['reference_id'] = $ruleObj->id;
                        $data['type'] = 'debit';
                        $data['amount'] = -1 * $amount;
                        if ($salaryKeyId) {
                            $salaryDataKey['amount'] = $amount;
                            $salaryDataKey['key_id'] = $ruleObj->id;
                            SalaryUserDataKey::saveData($salaryDataKey);
                            Transaction::saveTransaction($data);
                        }
                    }
                }
                if (!empty($item[22])) {
                    // medical_insurance InsuranceDeduction
                    $amount = $item[22];
                    $insuranceDeductionObj = new InsuranceDeduction;
                    $insuranceDeductionObj->month_id = $monthId;
                    $insuranceDeductionObj->user_id = $user->id;
                    $insuranceDeductionObj->amount = $amount;
                    $insuranceDeductionObj->insurance_policy_id = 43;
                    if ($insuranceDeductionObj->save()) {
                        $data['reference_id'] = $insuranceDeductionObj->id;
                        $data['reference_type'] = 'App\Models\Admin\Insurance\InsuranceDeduction';
                        $data['type'] = 'debit';
                        $data['amount'] = -1 * $amount;
                        Transaction::saveTransaction($data);
                    }

                    // $ruleObj = PayroleGroupRuleMonth::where('payrole_group_id',$groupId)->where('name', 'medical_insurance')->where('month_id',$monthId)->first();
                    // if($ruleObj) {
                    //     $amount = $item[22];
                    //     $data['reference_id'] = $ruleObj->id;
                    //     $data['type'] = 'debit';
                    //     $data['amount'] = -1 * $amount;
                    //     if($salaryKeyId) {
                    //         $salaryDataKey['amount'] = $amount;
                    //         $salaryDataKey['key_id'] = $ruleObj->id;
                    //         SalaryUserDataKey::saveData($salaryDataKey);
                    //         Transaction::saveTransaction($data);
                    //     }
                    // }
                }
                $data['reference_type'] = 'App\Models\Admin\Finance\PayroleGroupRuleMonth';
            }
        } else {
            \Log::info('Record not added ' . json_encode($item));
        }
    }

    public static function saveTDSAndBonuses($item, $date)
    {
        $monthId = Month::getMonth($date);
        $monthObj = Month::find($monthId);
        $user = User::where('employee_id', $item[1])->first();

        if (!$user) {
            return false;
        }

        if (!empty($item[1])) {
            $data['date'] = $date;
            $data['month_id'] = $monthId;
            $data['financial_year_id'] = 1;
            $data['user_id'] = $user->id;
            $data['reference_type'] = 'App\Models\Admin\Finance\PayroleGroupRuleMonth';
            $appraisal = Appraisal::getAppraisalByDate($user->id, $date);
            if ($appraisal) {
                $groupId = $appraisal->payrole_group_id;
                // $salaryData['user_id'] = $user->id;
                // $salaryData['employee_id'] = $user->employee_id;
                // $salaryData['appraisal_id'] = $appraisal->id;
                // $salaryData['month_id'] = $monthId;
                // $salaryKeyId =  SalaryUserData::saveData($salaryData);
                // $salaryDataKey['salary_user_data_id'] = $salaryKeyId;
                // if (!empty($item[17])) {
                //     // other_deduction
                //     $amount = $item[17];
                //     $insuranceDeduction = new InsuranceDeduction;
                //     $insuranceDeduction->user_id = $user->id;
                //     $insuranceDeduction->month_id = $monthId;
                //     $insuranceDeduction->amount = $amount;
                //     if ($insuranceDeduction->save()) {
                //         $data['reference_id'] = $insuranceDeduction->id;
                //         $data['reference_type'] = 'App\Models\Admin\Insurance\InsuranceDeduction';
                //         $data['type'] = 'debit';
                //         $data['amount'] = -1 * $amount;
                //         Transaction::saveTransaction($data);
                //     }
                // }
                if (!empty($item[23]) && $item[23] != 0) {
                    // tds - it_saving
                    $userIt = ItSaving::where('user_id', $user->id)->first();
                    if (!$userIt) {
                        $userIt = new ItSaving;
                        $userIt->user_id = $user->id;
                        $userIt->financial_year_id = 1;
                        $userIt->save();
                    }
                    $totalGrossSalaryPaid = 0;

                    // $lastMonthDate = date("Y-n-j", strtotime("first day of previous month"));
                    // $prevMonthId = Month::getMonth($lastMonthDate);
                    // $pevItSavingObj = ItSaving::where('month_id', $prevMonthId)->where('user_id', $user->id)->first();

                    // if($pevItSavingObj)
                    //     $totalGrossSalaryPaid = $pevItSavingObj->total_gross_salary_paid;

                    $itUserData = new ItSavingUserData;
                    $itUserData->it_saving_id = $userIt->id;
                    $itUserData->user_id = $user->id;
                    $itUserData->month_id = $monthId;

                    // $noOfRemaingMonth = FinancialYear::getRemainingMonths($monthId);
                    // $grossSalaryForTheMonth = PayrollService::getGrossSalaryForMonth($user->id, $monthId);
                    // $totalBonuses = PayrollService::getBonusesForMonth($user->id, $monthId);
                    // $lastMonthGrossPaid = PayrollService::getBonusesForMonth($user->id, $monthId);
                    // $currentMonthGross = $appraisal->annual_gross_salary / 12;
                    // $itUserData->annual_salary_to_be_paid = $grossSalaryForTheMonth + ($grossSalaryForTheMonth * $noOfRemaingMonth) + $totalBonuses;
                    // $itUserData->total_earning_for_the_moth = $grossSalaryForTheMonth + $totalBonuses;

                    // $itUserData->annual_salary_paid = PayrollService::annualSalaryPaid($user->id, $monthId);
                    //$itUserData->to_be_paid = $data['gross_annual']['']
                    //To Be paid : (800000 *11)/12 - for the finacial year

                    $itUserData->tds_amount = (-1) * $item[23];
                    if ($itUserData->save()) {
                        $amount = $item[23];
                        $data['reference_id'] = $itUserData->id;
                        $data['reference_type'] = 'App\Models\Admin\Finance\ItSavingUserData';
                        $data['type'] = 'debit';
                        $data['amount'] = -1 * $amount;
                        Transaction::saveTransaction($data);
                    }
                }
                if (!empty($item[24])) {
                    // deduction loan
                    $loanObj = Loan::where('user_id', $user->id)->orderBy('id', 'DESC')->first();
                    if (!$loanObj) {
                        $loanObj = new Loan;
                        $loanObj->user_id = $user->id;
                        $loanObj->amount = 999999999;
                        $loanObj->remaining = 999999999;
                        $loanObj->type = 'monthly';
                        $loanObj->emi = $item[24];
                        $loanObj->application_date = date('Y-m-d');
                        $loanObj->save();
                    }
                    $balance = $loanObj->balance - $item[24];
                    $loanEmiObj = new LoanEmi;
                    $loanEmiObj->loan_id = $loanObj->id;
                    $loanEmiObj->user_id = $user->id;
                    $loanEmiObj->amount = $item[24];
                    $loanEmiObj->actual_amount = $item[24];
                    $loanEmiObj->balance = $balance;
                    $loanEmiObj->due_date = $date;
                    $loanEmiObj->paid_on = $date;
                    $loanEmiObj->status = 'approved';
                    $loanEmiObj->month_id = $monthId;
                    if ($loanEmiObj->save()) {
                        $loanObj->balance = $balance;
                        $loanObj->save();
                        //Interest as per SBI rate applicable as on 01.04.2018 - 12.65%
                        $loanEmiObj->interest_amount = round(0.0105 * $balance);
                        $loanEmiObj->save();

                        $amount = $item[24];
                        $data['reference_id'] = $loanEmiObj->id;
                        $data['reference_type'] = 'App\Models\LoanEmi';
                        $data['type'] = 'debit';
                        $data['amount'] = -1 * $amount;
                        Transaction::saveTransaction($data);
                    }

                }
                if (!empty($item[25])) {
                    // Advance salary
                    $amount = $item[25];
                    $userAdvance = AdvanceSalary::where('user_id', $user->id)->first();
                    if (!$userAdvance) {
                        $adObj = new AdvanceSalary;
                        $adObj->user_id = $user->id;
                        $adObj->date = date('Y-m-d');
                        $adObj->status = 'in_progress';
                        $adObj->amount = $amount;
                        $adObj->save();
                    } else {
                        $userAdvance->amount += $amount;
                        $userAdvance->save();
                    }

                    $advanceDeduction = new AdvanceSalaryDeduction;
                    $advanceDeduction->advance_salary_id = $userAdvance->id;
                    $advanceDeduction->month_id = $monthId;
                    $advanceDeduction->amount = $amount;
                    $advanceDeduction->status = 'approved';

                    if ($advanceDeduction->save()) {
                        $data['reference_id'] = $advanceDeduction->id;
                        $data['reference_type'] = 'App\Models\Admin\Finance\AdvanceSalaryDeduction';
                        $data['type'] = 'debit';
                        $data['amount'] = -1 * $amount;
                        Transaction::saveTransaction($data);

                    }
                }
                if (!empty($item[30])) {
                    // annual bonus
                    $amount = $item[30];
                    $appBonus = new AppraisalBonus;
                    $appBonus->appraisal_id = $appraisal->id;
                    $appBonus->month_id = $monthId;
                    $appBonus->date = $date;
                    $appBonus->amount = $amount;
                    $appBonus->type = 'annual';
                    $appBonus->status = 'approved';
                    if ($appBonus->save()) {
                        $data['reference_id'] = $appBonus->id;
                        $data['reference_type'] = 'App\Models\Admin\AppraisalBonus';
                        $data['type'] = 'credit';
                        $data['amount'] = $amount;
                        Transaction::saveTransaction($data);
                    }

                }
                if (!empty($item[31])) {
                    // monthly variable bonus
                    $amount = $item[31];
                    $appBonus = new AppraisalBonus;
                    $appBonus->appraisal_id = $appraisal->id;
                    $appBonus->month_id = $monthId;
                    $appBonus->date = $date;
                    $appBonus->amount = $amount;
                    $appBonus->type = 'monthly_variable';
                    $appBonus->status = 'approved';
                    if ($appBonus->save()) {
                        $data['reference_id'] = $appBonus->id;
                        $data['reference_type'] = 'App\Models\Admin\AppraisalBonus';
                        $data['type'] = 'credit';
                        $data['amount'] = $amount;
                        Transaction::saveTransaction($data);
                    }
                }
                if (!empty($item[32])) {
                    // extra workinf day
                    $amount = $item[32];
                    $bonusObj = new Bonus();
                    $bonusObj->user_id = $user->id;
                    $bonusObj->month_id = $monthId;
                    $bonusObj->amount = $amount;
                    $bonusObj->status = 'approved';
                    $bonusObj->date = $date;
                    $bonusObj->type = 'additional';
                    $bonusObj->sub_type = 'weekend'; //leave weekend holiday
                    $bonusObj->reference_id = null; //leave weekend holiday
                    $bonusObj->reference_type = 'App\Models\Admin\AdditionalWorkDaysBonus'; //leave weekend holiday
                    $bonusObj->bonus_request_id = 999999999;
                    $bonusObj->created_by = 33;
                    $bonusObj->paid_at = $date;

                    if ($bonusObj->save()) {
                        //Save Transaction
                        $data['reference_id'] = $bonusObj->id;
                        $data['reference_type'] = 'App\Models\Admin\Bonus';
                        $data['type'] = 'credit';
                        $data['amount'] = $amount;
                        Transaction::saveTransaction($data);
                    }

                }
                if (!empty($item[33])) {
                    //National Holiday Working Bonus
                    $amount = $item[33];
                    $bonusObj = new Bonus();
                    $bonusObj->user_id = $user->id;
                    $bonusObj->month_id = $monthId;
                    $bonusObj->amount = $amount;
                    $bonusObj->status = 'approved';
                    $bonusObj->date = $date;
                    $bonusObj->type = 'additional';
                    $bonusObj->sub_type = 'weekend'; //leave weekend holiday
                    $bonusObj->bonus_request_id = 999999999;
                    $bonusObj->reference_id = null; //leave weekend holiday
                    $bonusObj->reference_type = 'App\Models\Admin\AdditionalWorkDaysBonus'; //leave weekend holiday
                    $bonusObj->created_by = 33;
                    $bonusObj->paid_at = $date;

                    if ($bonusObj->save()) {
                        //Save Transaction
                        $data['reference_id'] = $bonusObj->id;
                        $data['reference_type'] = 'App\Models\Admin\Bonus';
                        $data['type'] = 'credit';
                        $data['amount'] = $amount;
                        Transaction::saveTransaction($data);
                    }
                }
                if (!empty($item[34])) {
                    // ON SITE
                    $amount = $item[34];
                    $bonusObj = new Bonus();
                    $bonusObj->user_id = $user->id;
                    $bonusObj->month_id = $monthId;
                    $bonusObj->amount = $amount;
                    $bonusObj->status = 'approved';
                    $bonusObj->date = $date;
                    $bonusObj->type = 'onsite';
                    $bonusObj->sub_type = 'weekend'; //leave weekend holiday
                    $bonusObj->bonus_request_id = 999999999;
                    $bonusObj->reference_id = 99999999; //leave weekend holiday
                    $bonusObj->reference_type = 'App\Models\Admin\OnSiteBonus'; //leave weekend holiday
                    $bonusObj->created_by = 33;
                    $bonusObj->paid_at = $date;

                    if ($bonusObj->save()) {
                        //Save Transaction
                        $data['reference_id'] = $bonusObj->id;
                        $data['reference_type'] = 'App\Models\Admin\Bonus';
                        $data['type'] = 'credit';
                        $data['amount'] = $amount;
                        Transaction::saveTransaction($data);
                    }
                }
                if (!empty($item[35])) {
                    // Tech Talk Bonus
                    $amount = $item[35];
                    // $techData['employee_id'] = $user->id;
                    // $techData['topic'] = 'TechTalk';
                    // $techData['date'] = $date;
                    // $techData['amount'] = $amount;
                    // $techData['status'] = 'approved';
                    // $techObj = TechTalkBonus::saveData($techData);
                    $bonusObj = new Bonus();
                    $bonusObj->user_id = $user->id;
                    $bonusObj->month_id = $monthId;
                    $bonusObj->amount = $amount;
                    $bonusObj->status = 'approved';
                    $bonusObj->date = $date;
                    $bonusObj->type = 'techtalk';
                    $bonusObj->reference_id = null; //$techObj->id;
                    $bonusObj->reference_type = 'App\Models\Admin\TechTalkBonus'; //leave weekend holiday
                    //$bonusObj->sub_type = ''; //leave weekend holiday
                    $bonusObj->bonus_request_id = 999999999;
                    $bonusObj->created_by = 33;
                    $bonusObj->paid_at = $date;

                    if ($bonusObj->save()) {
                        //Save Transaction
                        $data['reference_id'] = $bonusObj->id;
                        $data['reference_type'] = 'App\Models\Admin\Bonus';
                        $data['type'] = 'credit';
                        $data['amount'] = $amount;
                        Transaction::saveTransaction($data);
                    }
                }
                if (!empty($item[36])) {
                    // PERformacnce bonus
                    $amount = $item[36];
                    $bonusObj = new Bonus();
                    $bonusObj->user_id = $user->id;
                    $bonusObj->month_id = $monthId;
                    $bonusObj->amount = $amount;
                    $bonusObj->status = 'approved';
                    $bonusObj->date = $date;
                    $bonusObj->type = 'performance';
                    //$bonusObj->sub_type = ''; //leave weekend holiday
                    $bonusObj->bonus_request_id = 999999999;
                    $bonusObj->reference_id = null; //$techObj->id;
                    $bonusObj->reference_type = 'App\Models\Admin\PerformanceBonus'; //leave weekend holiday

                    $bonusObj->created_by = 33;
                    $bonusObj->paid_at = $date;

                    if ($bonusObj->save()) {
                        //Save Transaction
                        $data['reference_id'] = $bonusObj->id;
                        $data['reference_type'] = 'App\Models\Admin\Bonus';
                        $data['type'] = 'credit';
                        $data['amount'] = $amount;
                        Transaction::saveTransaction($data);
                    }
                }
                if (!empty($item[37])) {
                    // Confirmation bonus
                    $amount = $item[37];
                    $appBonus = new AppraisalBonus;
                    $appBonus->appraisal_id = $appraisal->id;
                    $appBonus->month_id = $monthId;
                    $appBonus->date = $date;
                    $appBonus->amount = $amount;
                    $appBonus->type = 'confirmation';
                    $appBonus->status = 'approved';
                    if ($appBonus->save()) {
                        $data['reference_id'] = $appBonus->id;
                        $data['reference_type'] = 'App\Models\Admin\AppraisalBonus';
                        $data['type'] = 'credit';
                        $data['amount'] = $amount;
                        Transaction::saveTransaction($data);
                    }
                }
                if (!empty($item[38])) {
                    // Refferal bonus
                    $amount = $item[38];
                    $bonusObj = new Bonus();
                    $bonusObj->user_id = $user->id;
                    $bonusObj->month_id = $monthId;
                    $bonusObj->amount = $amount;
                    $bonusObj->status = 'approved';
                    $bonusObj->date = $date;
                    $bonusObj->type = 'referral';
                    //$bonusObj->sub_type = ''; //leave weekend holiday
                    $bonusObj->bonus_request_id = 999999999;
                    $bonusObj->reference_id = null; //$techObj->id;
                    $bonusObj->reference_type = 'App\Models\Admin\ReferralBonus'; //leave weekend holiday

                    $bonusObj->created_by = 33;
                    $bonusObj->paid_at = $date;

                    if ($bonusObj->save()) {
                        //Save Transaction
                        $data['reference_id'] = $bonusObj->id;
                        $data['reference_type'] = 'App\Models\Admin\Bonus';
                        $data['type'] = 'credit';
                        $data['amount'] = $amount;
                        Transaction::saveTransaction($data);
                    }
                }
                if (!empty($item[40]) && $monthId == 11) {
                    // Refferal bonus
                    $amount = $item[40];
                    $bonusObj = new Bonus();
                    $bonusObj->user_id = $user->id;
                    $bonusObj->month_id = $monthId;
                    $bonusObj->amount = $amount;
                    $bonusObj->status = 'approved';
                    $bonusObj->date = $date;
                    $bonusObj->type = 'leave_encash';
                    //$bonusObj->sub_type = ''; //leave weekend holiday
                    $bonusObj->bonus_request_id = 999999999;
                    $bonusObj->reference_id = null; //$techObj->id;
                    $bonusObj->reference_type = 'App\Models\Admin\UserLeaveBalance'; //leave weekend holiday
                    $bonusObj->created_by = 33;
                    $bonusObj->paid_at = $date;
                    if ($bonusObj->save()) {
                        //Save Transaction
                        $data['reference_id'] = $bonusObj->id;
                        $data['reference_type'] = 'App\Models\Admin\Bonus';
                        $data['type'] = 'credit';
                        $data['amount'] = $amount;
                        Transaction::saveTransaction($data);
                    }
                }

            }
        }
    }

    public static function parseCSVData($dataArr, $date)
    {
        foreach ($dataArr as $data) {
            self::saveSalaryData($data, $date);
            self::saveTDSAndBonuses($data, $date);
        }
        die;
        $processData = [];
        // for ( $i=7; $i < count($dataArr) - 1; $i++ ) {
        //     if( empty($dataArr[$i][1]) ) {
        //         break;
        //     }
        foreach ($dataArr as $data) {

            if (empty($data[1])) {
                break;
            }

            //$data =$dataArr[$i];
            $tempData = [];
            $tempData['employee_id'] = $data[1];
            $tempData['name'] = $data[2];
            $tempData['email'] = trim($data[3]) === '-' ? null : trim($data[3]);
            //$tempData['payslip_month_id'] = $payslipMonthId;
            $tempData['pan_no'] = $data[4];
            $tempData['date_of_birth'] = $data[5];
            $tempData['date_of_joining'] = $data[6];
            $tempData['designation'] = $data[7];
            $tempData['gender'] = $data[8];
            $tempData['bank_account_no'] = $data[9];
            $tempData['bank_ifsc'] = $data[10];
            $tempData['pf_no'] = $data[11];
            $tempData['uan_no'] = $data[12];
            $tempData['esi_no'] = $data[13];
            $tempData['month'] = $data[14];
            $tempData['year'] = $data[15];
            $tempData['working_day_month'] = $data[16];
            $tempData['days_worked'] = $data[17];
            //integer values, default should be 0
            $tempData['basic'] = trim($data[18]) === '-' ? 0 : trim($data[18]);
            $tempData['hra'] = trim($data[19]) === '-' ? 0 : trim($data[19]);
            $tempData['conveyance_allowance'] = trim($data[20]) === '-' ? 0 : trim($data[20]);
            $tempData['car_allowance'] = trim($data[21]) === '-' ? 0 : trim($data[21]);
            $tempData['medical_allowance'] = trim($data[22]) === '-' ? 0 : trim($data[22]);
            $tempData['food_allowance'] = trim($data[23]) === '-' ? 0 : trim($data[23]);
            $tempData['special_allowance'] = trim($data[24]) === '-' ? 0 : trim($data[24]);
            $tempData['gross_salary'] = trim($data[25]) === '-' ? 0 : trim($data[25]);
            $tempData['pf_12_employee'] = trim($data[26]) === '-' ? 0 : trim($data[26]);
            $tempData['esi_1_75_employee'] = trim($data[27]) === '-' ? 0 : trim($data[27]);
            $tempData['vpf'] = trim($data[28]) === '-' ? 0 : trim($data[28]);
            $tempData['professional_tax'] = trim($data[29]) === '-' ? 0 : trim($data[29]);
            $tempData['food_deduction'] = trim($data[30]) === '-' ? 0 : trim($data[30]);
            $tempData['medical_insurance'] = trim($data[31]) === '-' ? 0 : trim($data[31]);
            $tempData['tds'] = trim($data[32]) === '-' ? 0 : trim($data[32]);
            $tempData['loan'] = trim($data[33]) === '-' ? 0 : trim($data[33]);
            $tempData['total_deduction_month'] = trim($data[34]) === '-' ? 0 : trim($data[34]);
            $tempData['net_salary_month'] = trim($data[35]) === '-' ? 0 : trim($data[35]);
            $tempData['arrear_salary'] = trim($data[36]) === '-' ? 0 : trim($data[36]);
            $tempData['annual_bonus'] = trim($data[37]) === '-' ? 0 : trim($data[37]);
            $tempData['qtr_variable_bonus'] = trim($data[38]) === '-' ? 0 : trim($data[38]);
            $tempData['performance_bonus'] = trim($data[39]) === '-' ? 0 : trim($data[39]);
            $tempData['non_cash_incentive'] = trim($data[40]) === '-' ? 0 : trim($data[40]);
            $tempData['gross_earnings'] = trim($data[41]) === '-' ? 0 : trim($data[41]);
            $tempData['amount_payable'] = trim($data[42]) === '-' ? 0 : trim($data[42]);
            $tempData['sl_op_bal'] = trim($data[43]) === '-' ? 0 : trim($data[43]);
            $tempData['cl_op_bal'] = trim($data[44]) === '-' ? 0 : trim($data[44]);
            $tempData['pl_op_bal'] = trim($data[45]) === '-' ? 0 : trim($data[45]);
            $tempData['cme_sl'] = trim($data[46]) === '-' ? 0 : trim($data[46]);
            $tempData['cme_cl'] = trim($data[47]) === '-' ? 0 : trim($data[47]);
            $tempData['cme_pl'] = trim($data[48]) === '-' ? 0 : trim($data[48]);
            $tempData['sl_availed'] = trim($data[49]) === '-' ? 0 : trim($data[49]);
            $tempData['cl_availed'] = trim($data[50]) === '-' ? 0 : trim($data[50]);
            $tempData['pl_availed'] = trim($data[51]) === '-' ? 0 : trim($data[51]);
            $tempData['bal_cf_sl'] = trim($data[52]) === '-' ? 0 : trim($data[52]);
            $tempData['bal_cf_cl'] = trim($data[53]) === '-' ? 0 : trim($data[53]);
            $tempData['bal_cf_pl'] = trim($data[54]) === '-' ? 0 : trim($data[54]);
            if (!empty($data[55])) {
                $tempData['lta'] = trim($data[55]) === '-' ? 0 : trim($data[55]);
            }

            if (!empty($data[56])) {
                $tempData['advance_salary'] = trim($data[56]) === '-' ? 0 : trim($data[56]);
            }

            $processData[] = $tempData;
        }
        return $processData;
    }
    public static function headerCode($header)
    {
        //$header[0] = ﻿Name for "Software";

        $header[1] = "A";
        $header[2] = "B";
        $header[3] = "C";
        $header[4] = "D";
        $header[5] = "E";
        $header[6] = "F";
        $header[7] = "G";
        $header[8] = "H";
        $header[9] = "I";
        $header[10] = "J";
        $header[11] = "K";
        $header[12] = "L";
        $header[13] = "M";
        $header[14] = "N";
        $header[15] = "O";
        $header[16] = "P";
        $header[17] = "Q";
        $header[18] = "R";
        $header[19] = "S";
        $header[20] = "T";
        $header[21] = "U";
        $header[22] = "V";
        $header[23] = "W";
        $header[24] = "X";
        $header[25] = "Y";
        $header[26] = "Z";
        $header[27] = "AA";
        $header[28] = "BB";
        $header[29] = "CC";
        $header[30] = "DD";
        $header[31] = "EE";
        $header[32] = "FF";
        $header[33] = "GG";
        $header[34] = "HH";
        $header[35] = "II";
        $header[36] = "JJ";
        $header[37] = "KK";
        $header[38] = "";
        $header[39] = "LL";
        $header[40] = "MM";
        $header[41] = "NN";
        $header[42] = "OO";
        $header[43] = "PP";
        $header[44] = "QQ";
        $header[45] = "DDD";
        $header[46] = "EEE";
    }

    public static function getAppraisalByDate($userId, $date)
    {
        $grossSalary = 0;
        $date = date('Y-m-d', strtotime($date));
        $appraisal = Appraisal::where('user_id', $userId)->where('effective_date', '>=', $date)
            ->where(function ($query) use ($date) {
                $query->where('end_date', null)
                    ->orWhere('end_date', '<=', $date);
            })->first();

        if ($appraisal) {
            $grossSalary = $appraisal->annual_gross_salary ?: 0;
        }
        return round($grossSalary);
    }
    public static function getMonthKeyId()
    {
        $id = 1;
        return $id;
    }
    public static function saveAllData($amount, $keyname, $monthId, $groupId, $salaryKeyId)
    {
        // Basic
        $ruleObj = PayroleGroupRuleMonth::where('payrole_group_id', $groupId)->where('name', $keyname)->where('month_id', $monthId)->first();
        if ($ruleObj) {
            $amount = $item[6];
            $data['reference_type'] = $ruleObj->id;
            $data['reference_id'] = $ruleObj->id;
            $data['type'] = 'credit';
            $data['amount'] = $amount;
            if ($salaryKeyId) {
                $salaryDataKey['amount'] = $amount;
                $salaryDataKey['key_id'] = $ruleObj->id;
                SalaryUserDataKey::saveData($salaryDataKey);
                Transaction::saveTransaction($data);
                echo "ADDED";
            }
        }
    }
}
