<?php namespace App\Services;

use Validator;
use App\Models\Admin\Expense;
use App\Models\Admin\ExpenseHead;
use App\Models\User;

use Redirect;
use Auth;


class ExpenseService {

	public static function saveData($input)
	{
        $status = true;
        $message = '';
        $result = [];

        $expenseObj = new Expense();
        $expenseObj->po_id = !empty($input['po_id']) ?$input['po_id'] : null;
        $expenseObj->expense_head_id = !empty($input['expense_head_id']) ? $input['expense_head_id'] : null;
        $expenseObj->amount = !empty($input['amount'])?$input['amount']:null;
        $expenseObj->payment_status = !empty($input['payment_status']) ? $input['payment_status'] : 'pending';
        $expenseObj->payment_method_id = !empty($input['payment_method_id']) ? $input['payment_method_id'] : null;
        $expenseObj->invoice_number = !empty($input['invoice_number']) ? $input['invoice_number'] : null;
        $expenseObj->invoice_date = !empty($input['invoice_date']) ? date_to_yyyymmdd($input['invoice_date']) : null;
        $expenseObj->expense_by = !empty($input['expense_by']) ? $input['expense_by'] : null;
        $expenseObj->payment_by = !empty($input['payment_by']) ? $input['payment_by'] : null;
        $expenseObj->payment_reference_number = !empty($input['payment_reference_number']) ? $input['payment_reference_number'] : null;
        $expenseObj->payment_date = !empty($input['payment_date']) ? date_to_yyyymmdd($input['payment_date']) : null;
        $expenseObj->other_info = !empty($input['other_info']) ? $input['other_info'] : null;
        $expenseObj->created_by = Auth::user()->id;
        if ( !$expenseObj->save() )
        {
            $status = false;
            $message = $expenseObj->getErrors();
        }
        else{
            $expenseObj->addActivity($expenseObj->payment_status);
        }
        $result['status'] = $status;
        $result['message'] = $message;
        return $result;
    }

    public static function updateData($id, $input)
	{
        $status = true;
        $message = '';
        $result = [];

        $expenseObj = Expense::find($id);
        if ( !$expenseObj )
        {
            $status = false;
            $message = 'No record found';
        }
        else
        {
            $expenseObj->po_id = !empty($input['po_id']) ?$input['po_id'] : null;
            $expenseObj->expense_head_id = !empty($input['expense_head_id']) ? $input['expense_head_id'] : null;
            $expenseObj->amount = !empty($input['amount'])?$input['amount']:null;
            $expenseObj->payment_status = !empty($input['payment_status']) ? $input['payment_status'] : 'pending';
            $expenseObj->payment_method_id = !empty($input['payment_method_id']) ? $input['payment_method_id'] : null;
            $expenseObj->invoice_number = !empty($input['invoice_number']) ? $input['invoice_number'] : null;
            $expenseObj->invoice_date = !empty($input['invoice_date']) ? date_to_yyyymmdd($input['invoice_date']) : null;
            $expenseObj->expense_by = !empty($input['expense_by']) ? $input['expense_by'] : null;
            $expenseObj->payment_by = !empty($input['payment_by']) ? $input['payment_by'] : null;
            $expenseObj->payment_reference_number = !empty($input['payment_reference_number']) ? $input['payment_reference_number'] : null;
            $expenseObj->payment_date = !empty($input['payment_date']) ? date_to_yyyymmdd($input['payment_date']) : null;
            $expenseObj->other_info = !empty($input['other_info']) ? $input['other_info'] : null;
            if ( !$expenseObj->save() )
            {
                $status = false;
                $message = $expenseObj->getErrors();
            }
            else {
                $expenseObj->addActivity($expenseObj->payment_status);
            }
        }
        

        $result['status'] = $status;
        $result['message'] = $message;
        return $result;
    }

    public static function updateStatus( $id, $payment_status )
    {
        $status = true;
        $message = '';
        $expenseObj = Expense::find($id);
        if ( !$expenseObj )
        {
            $status = false;
            $message = 'No record found';
        }
        else
        {
            $expenseObj->payment_status = $payment_status;
            $message=$expenseObj->payment_status;
            if ( !$expenseObj->save() )
            {
                $status = false;
                $message = $expenseObj->getErrors();
            }
            else {
                $expenseObj->addActivity($expenseObj->payment_status);
            }
        }
        $result['status'] = $status;
        $result['message'] = $message;
        return $result;
    }
}