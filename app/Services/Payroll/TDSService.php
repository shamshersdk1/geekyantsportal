<?php
namespace App\Services\Payroll;

use App\Models\Admin\Finance\SalaryUserDataTDS;
use App\Models\Admin\FinancialYear;
use App\Models\Admin\Insurance\InsuranceDeduction;
use App\Models\Admin\ItSaving;
use App\Models\Admin\VpfDeduction;
use App\Models\Appraisal\AppraisalBonusType;
use App\Models\Appraisal\AppraisalComponentType;
use App\Models\Audited\AdhocPayments\AdhocPayment;
use App\Models\Audited\AdhocPayments\AdhocPaymentComponent;
use App\Models\Audited\Bonus\BonusConfirmed;
use App\Models\Audited\FoodDeduction\FoodDeduction;
use App\Models\Audited\Salary\PrepTds;
use App\Models\Audited\WorkingDay\UserWorkingDay;
use App\Models\LoanEmi;
use App\Models\Month;
use App\Models\Transaction;
use App\Services\Payroll\AppraisalService;
use App\Services\Payroll\PayrollService;

class TDSService
{

    public static function getTDSSummary($userId, $finacialYearID)
    {   
        $response = [];
        $response['data'] = null;
        $response['status'] = false;
        $response['errors'] = null;

        $months = Month::where('financial_year_id', $finacialYearID)->where('status', 'processed')->get();
        $components = AppraisalComponentType::get();
        $bonuses = AppraisalBonusType::get();

        $data = [];
        $monthRow = [];

        $workingId = AppraisalComponentType::getIdByKey('working-days');
        
        foreach ($months as $month) {
            foreach ($components as $component) {
                $obj = Transaction::where('reference_type', 'App\Models\Appraisal\AppraisalComponentType')->where('reference_id', $component->id)->where('user_id', $userId)->where('month_id', $month->id)->first();
                if ($obj) {
                    $data[$month->year . '_' . $month->month]['appraisal_components'][$component->id] = $obj->amount;
                }
            }
            $data[$month->year . '_' . $month->month]['month'] = $month->formatMonth();
            $data[$month->year . '_' . $month->month]['working_days'] = $month->working_days;
            if ($workingId != null) {
                $data[$month->year . '_' . $month->month]['worked_days'] = Transaction::getMontlyPaidByType($userId, $month->id, 'App\Models\Appraisal\AppraisalComponentType', $workingId);
            } else {
                $data[$month->year . '_' . $month->month]['worked_days'] = 0;
            }
            $data[$month->year . '_' . $month->month]['monthly_gross_salary'] = Transaction::getMonthyGrossSalary($userId, $month->id);
            $data[$month->year . '_' . $month->month]['monthly_bonus'] = Transaction::getAnnualBonusPaid($userId, $month->id);
            $data[$month->year . '_' . $month->month]['vpf'] = Transaction::getMontlyPaidByType($userId, $month->id, 'App\Models\Admin\VpfDeduction');
            $data[$month->year . '_' . $month->month]['food_deduction'] = Transaction::getMontlyPaidByType($userId, $month->id, 'App\Models\Audited\FoodDeduction\FoodDeduction');
            $data[$month->year . '_' . $month->month]['insurance'] = Transaction::getMontlyPaidByType($userId, $month->id, 'App\Models\Admin\Insurance\InsuranceDeduction');
            $data[$month->year . '_' . $month->month]['loan'] = Transaction::getMontlyPaidByType($userId, $month->id, 'App\Models\LoanEmi');

            $apprBonus = Transaction::where('reference_type', 'App\Models\Appraisal\AppraisalBonus')->where('user_id', $userId)->where('month_id', $month->id)->get();
            $data[$month->year . '_' . $month->month]['appraisal_bonuses']['other'] = 0;
            foreach ($apprBonus as $bonus) {
                if ($bonus->reference_id == null || $bonus->reference_id == 0) {
                    $data[$month->year . '_' . $month->month]['appraisal_bonuses']['other'] += $bonus->amount;
                } else {
                    $obj = $bonus->reference;
                    if ($obj) {
                        $data[$month->year . '_' . $month->month]['appraisal_bonuses'][$obj->appraisal_bonus_type_id] = $bonus->amount;
                    }
                }
            }
            // foreach ($bonuses as $bonus) {
            //     $obj = Transaction::where('reference_type', 'App\Models\Appraisal\AppraisalBonus')->where('reference_id', $bonus->id)->where('user_id', $userId)->where('month_id', $month->id)->first();
            //     if ($obj) {
            //         $data[$month->year . '_' . $month->month]['appraisal_bonuses'][$bonus->id] = $obj->amount;
            //     }
            // }

            $objs = Transaction::where('reference_type', 'App\Models\Audited\AdhocPayments\AdhocPayment')->where('user_id', $userId)->where('month_id', $month->id)->get(); 
            foreach ($objs as $obj) {
                $adhocPayment = AdhocPayment::find($obj->reference_id);
                $data[$month->year . '_' . $month->month]['adhoc_payments'][$adhocPayment->component->id] = $obj->amount;
            }
            $otherBonuses = Transaction::getMontlyOtherBonus($userId, $month->id, 'App\Models\Admin\Bonus');

            $data[$month->year . '_' . $month->month]['bonuses']['onsite'] = 0; // Transaction::getMontlyPaidByType($userId, $month->id, 'App\Models\Admin\OnSiteBonus');
            $data[$month->year . '_' . $month->month]['bonuses']['additional'] = 0; // Transaction::getMontlyPaidByType($userId, $month->id, 'App\Models\Admin\AdditionalWorkDaysBonus');
            $data[$month->year . '_' . $month->month]['bonuses']['extra_hour'] = 0; // Transaction::getMontlyPaidByType($userId, $month->id, 'App\Models\User\UserTimesheetExtra');
            $data[$month->year . '_' . $month->month]['bonuses']['tech_talk'] = 0; // Transaction::getMontlyPaidByType($userId, $month->id, 'App\Models\Admin\TechTalk');
            $data[$month->year . '_' . $month->month]['bonuses']['performance'] = 0; // Transaction::getMontlyPaidByType($userId, $month->id, 'App\Models\Admin\Performance');
            $data[$month->year . '_' . $month->month]['bonuses']['referral'] = 0; // Transaction::getMontlyPaidByType($userId, $month->id, 'App\Models\Admin\ReferralBonus');
            $data[$month->year . '_' . $month->month]['bonuses']['other'] = 0;

            foreach ($otherBonuses as $bonus) {
                if ($bonus->reference_id == null || $bonus->reference_id == 0) {
                    $data[$month->year . '_' . $month->month]['bonuses']['other'] += $bonus->amount;
                } elseif ($bonus->bonusType->type == 'onsite') {
                    $data[$month->year . '_' . $month->month]['bonuses']['onsite'] += $bonus->amount;
                } elseif ($bonus->bonusType->type == 'referral') {
                    $data[$month->year . '_' . $month->month]['bonuses']['referral'] += $bonus->amount;
                } elseif ($bonus->bonusType->type == 'techtalk') {
                    $data[$month->year . '_' . $month->month]['bonuses']['tech_talk'] += $bonus->amount;
                } elseif ($bonus->bonusType->type == 'additional') {
                    $data[$month->year . '_' . $month->month]['bonuses']['additional'] += $bonus->amount;
                } elseif ($bonus->bonusType->type == 'extra') {
                    $data[$month->year . '_' . $month->month]['bonuses']['extra_hour'] += $bonus->amount;
                } elseif ($bonus->bonusType->type == 'performance') {
                    $data[$month->year . '_' . $month->month]['bonuses']['performance'] += $bonus->amount;
                }
            }
        }

        $monthObj = Month::where('status', 'in_progress')->where('financial_year_id', $finacialYearID)->orderBy('year', 'ASC')->orderBy('month', 'ASC')->first();
        if (!$monthObj) {
            $currMonth = date('m');
            $monthObj = Month::where('month', $currMonth)->where('financial_year_id', $finacialYearID)->first();
        } else {
            $currMonth = $monthObj->month;
        }

        $appraisalList = AppraisalService::getCurrentMonthAppraisal($userId, $monthObj->id);

        if ($appraisalList == null || !isset($appraisalList[0])) {
            $response['errors'] = 'No Appraisal found!';
            $response['data'] = $data;
            return $response;
        }

        $currData = AppraisalService::generateCurrentData($appraisalList, $monthObj->id, $userId);
        if ($monthObj && $monthObj->status == 'in_progress') {

            $data[$monthObj->year . '_' . $monthObj->month]['worked_days'] = $currData['no_of_working'];
            $data[$monthObj->year . '_' . $monthObj->month]['working_days'] = $monthObj->working_days;
            $data[$monthObj->year . '_' . $monthObj->month]['monthly_gross_salary'] = $currData['monthly_gross_salary'];
            $data[$monthObj->year . '_' . $monthObj->month]['monthly_bonus'] = $currData['monthly_bonus'];
            $data[$monthObj->year . '_' . $monthObj->month]['vpf'] = VpfDeduction::getCurrMonthVpf($monthObj->id, $userId);
            $data[$monthObj->year . '_' . $monthObj->month]['food_deduction'] = FoodDeduction::getCurrMonthDeduction($monthObj->id, $userId);
            $data[$monthObj->year . '_' . $monthObj->month]['insurance'] = InsuranceDeduction::getCurrMonthDeduction($monthObj->id, $userId);
            $data[$monthObj->year . '_' . $monthObj->month]['loan'] = LoanEmi::getCurrMonthDeduction($monthObj->id, $userId);

            $adhoc_payements = AdhocPayment::where('user_id', $userId)->where('month_id', $monthObj->id)->get();
            foreach ($adhoc_payements as $adhocPayment) {
                $data[$monthObj->year . '_' . $monthObj->month]['adhoc_payments'][$adhocPayment->component->id] = $adhoc_payements->where('adhoc_payment_component_id', $adhocPayment->component->id)->sum('amount');
            }
            foreach ($components as $component) {
                $data[$monthObj->year . '_' . $monthObj->month]['appraisal_components'][$component->id] = $currData[$component->id];
            }
            foreach ($bonuses as $bonus) {
                $data[$monthObj->year . '_' . $monthObj->month]['appraisal_bonuses'][$bonus->id] = $currData['appraisal_bonus'][$bonus->id];
            }

            $data[$monthObj->year . '_' . $monthObj->month]['bonuses'] = BonusConfirmed::getCurrentMonthBonus($userId, $monthObj->id);
        }
        $futureAppraisal = $appraisalList->last();
        $futureData = AppraisalService::calculateFutureAppraisal($futureAppraisal->id, $monthObj->id, $userId);

        $upcomingMonths = FinancialYear::getUpcomingMonth($finacialYearID, $monthObj->month);
        foreach ($upcomingMonths as $upcomingMonth) {
            $data[$upcomingMonth->year . '_' . $upcomingMonth->month]['working_days'] = $futureData['no_of_working'][$upcomingMonth->month];
            $data[$upcomingMonth->year . '_' . $upcomingMonth->month]['monthly_gross_salary'] = $futureData[$upcomingMonth->month]['monthly_gross_salary'];
            $data[$upcomingMonth->year . '_' . $upcomingMonth->month]['monthly_bonus'] = $futureData[$upcomingMonth->month]['monthly_bonus'];

            foreach ($components as $component) {
                $data[$upcomingMonth->year . '_' . $upcomingMonth->month]['appraisal_components'][$component->id] = $futureData[$upcomingMonth->month][$component->id];
            }
            foreach ($bonuses as $bonus) {
                $data[$upcomingMonth->year . '_' . $upcomingMonth->month]['appraisal_bonuses'][$bonus->id] = $futureData[$upcomingMonth->month]['appraisal_bonus'][$bonus->id];
            }
        }

        $response['status'] = true;
        $response['data'] = $data;
        return $response;
    }
    public static function storeTDS($salaryObj)
    {
        // if ($value == 'hold') {

        // } else if ($value == 'approve') {

        // }
        // SalaryUserData

        // $user = SalaryUserData::where('month_id', $monthId)->where('user_id', $id);
        $tdsData = PayrollService::getTDSDataNew($salaryObj->id);

        $tdsObj = new SalaryUserDataTDS();
        $tdsObj->user_id = $salaryObj->user_id; //$user ? $user->user_id : "";
        $tdsObj->tds_for_the_month = $tdsData['tax_payable'];
        $tdsObj->gross_tax_payable = $tdsData['income_tax_for_the_month'];
        $tdsObj->month_id = $salaryObj->month_id; //$monthId; //$user->month_id;

        $tdsObj->save();
    }
    public static function getAnnualSummary($tdsData, $financialYearId, $userId)
    {
        $components = AppraisalComponentType::get();
        $tdsId = AppraisalComponentType::getIdByKey('tds');

        $summary = [];

        $summary['gross_annual_salary'] = 0;
        $summary['gross_pf_other'] = 0;
        $summary['gross_pf_employeer'] = 0;
        $summary['gross_professional_tax'] = 0;

        $totalOtherBonus = 0;
        $totalAnnualBonus = 0;

        foreach ($tdsData as $data) {
            $summary['gross_annual_salary'] += $data['monthly_gross_salary'];
            $totalAnnualBonus += $data['monthly_bonus'];
            foreach ($data['bonuses'] as $key => $bonus) {
                $totalOtherBonus += $bonus;
            }
        }

        foreach ($components as $component) {
            foreach ($tdsData as $data) {
                if ($component->code == 'pf-other') {
                    $summary['gross_pf_other'] += (-1) * $data['appraisal_components'][$component->id];
                }
                if ($component->code == 'pf-employeer') {
                    $summary['gross_pf_employeer'] += (-1) * $data['appraisal_components'][$component->id];
                }
                if ($component->code == 'professional-tax') {
                    $summary['gross_professional_tax'] += (-1) * $data['appraisal_components'][$component->id];
                }
            }
        }

        $rentYearly = self::getRentYearly($financialYearId, $userId);
        $summary['hra_exemption'] = self::getHRAExemption($summary['gross_annual_salary'], $rentYearly);

        $summary['standard_deduction'] = self::getStandardDeduction();
        $summary['total_deduction'] = self::getDeductions(
            $summary['hra_exemption'],
            $summary['standard_deduction'],
            $summary['gross_professional_tax'],
            $summary['gross_pf_other'],
            $summary['gross_pf_employeer']
        );

        $monthObj = Month::where('status', 'in_progress')->where('financial_year_id', $financialYearId)->orderBy('year', 'ASC')->orderBy('month', 'ASC')->first();
        if (!$monthObj) {
            $currMonth = date('m');
            $monthObj = Month::where('month', $currMonth)->where('financial_year_id', $financialYearId)->first();
        } else {
            $currMonth = $monthObj->month;
        }

        $summary['vpf_paid_till_date'] = 0;
        $summary['vpf_paid_till_date'] = Transaction::getPaidTillDate($userId, $financialYearId, 'App\Models\Admin\VpfDeduction');
        if (($monthObj->status == "in_progress" && $monthObj->vpfSetting->value == "locked") || ($monthObj->month == date('m') && $monthObj->vpfSetting->value == "locked")) {
            $summary['vpf_paid_till_date'] += VpfDeduction::getCurrMonthVpf($monthObj->id, $userId);
        }
        if ($summary['vpf_paid_till_date'] < 0) {
            $summary['vpf_paid_till_date'] = $summary['vpf_paid_till_date'] * -1;
        }

        if ($tdsId != null) {
            $tdsPaidTillDate = Transaction::getPaidTillDate($userId, $financialYearId, 'App\Models\Appraisal\AppraisalComponentType', $tdsId);
        } else {
            $tdsPaidTillDate = 0;
        }

        $noOfRemaingMonth = FinancialYear::getRemainingMonths($monthObj->id);

        $itSavingObj = ItSaving::where('financial_year_id', $financialYearId)->where('user_id', $userId)->first();
        $totalInvestment = round($itSavingObj->getTotalInvestmentsAttribute());

        $summary['it_saving_investment'] = min($totalInvestment, 150000);
        $summary['it_saving_deduction'] = round($itSavingObj->getTotalDeductionsAttribute());

        $summary['gross_annual_bonus'] = round($totalAnnualBonus);
        $summary['other_bonus'] = round($totalOtherBonus);
        $summary['other_payments'] = Transaction::getPaidTillDate($userId, $financialYearId, 'App\Models\Audited\AdhocPayments\AdhocPayment') + AdhocPayment::getCurrMonthValue($monthObj->id, $userId);

        $summary['total_salary'] = $summary['gross_annual_salary'] + $summary['gross_annual_bonus'] + $summary['other_bonus'] + round(($summary['other_payments'] > 0) ? $summary['other_payments'] : 0);

        $summary['net_taxable_salary'] = $summary['total_salary'] - $summary['total_deduction'];

        $summary['taxable_income'] = $summary['net_taxable_salary'] - ($summary['it_saving_deduction'] + $summary['it_saving_investment']);

        $summary['gross_tds'] = self::getTDS($summary['taxable_income']);

        $summary['edu_cess'] = round($summary['gross_tds'] * 0.04);

        $summary['tax_payable'] = $summary['edu_cess'] + $summary['gross_tds'];
        $summary['rebate'] = 0;
        $summary['tax_payable_after_rebate'] = 0;
        if ($summary['taxable_income'] < 500000) {
            $summary['tax_payable_after_rebate'] = max($summary['tax_payable'] - 12500, 0);
            $summary['rebate'] = 12500;
        } else {
            $summary['tax_payable_after_rebate'] = $summary['tax_payable'];
        }

        if ($tdsPaidTillDate < 0) {
            $tdsPaidTillDate = (-1) * $tdsPaidTillDate;
        }
        $summary['tds_paid_till_date'] = round($tdsPaidTillDate);

        $summary['balance_tax_payable'] = $summary['tax_payable_after_rebate'] - round($tdsPaidTillDate);

        $summary['tds'] = round($summary['balance_tax_payable'] / ($noOfRemaingMonth + 1));

        return $summary;
    }

    public static function getTDS($taxableIncome)
    {
        //$tds = self::incomeTaxPayableSlab($data['taxable_income']);

        // Income Tax Payable on Total Income
        // Rs 0 - Rs 250000
        // Rs 250001 - Rs 500000 @ 5%
        // Rs 500001- Rs 1000000  @ 20%
        // Above Rs 1000000 @ 30%
        $taxableAmount = $taxableIncome;

        $incomeTax = 0;
        if ($taxableAmount < 250000) {
            return 0;
        }

        if ($taxableAmount > 1000000) {
            $incomeTax = 12500 + 100000 + (($taxableAmount - 1000000) * 0.3);
        } elseif ($taxableAmount > 500000) {
            $incomeTax = 12500 + (($taxableAmount - 500000) * 0.2);
        } elseif ($taxableAmount > 250000) {
            $incomeTax = ($taxableAmount - 250000) * 0.05;
        }

        return round($incomeTax);
    }

    public static function getHRAExemption($grossAnnualSalary, $rentYearly = 0)
    {
        $basic = $grossAnnualSalary * 0.4;
        $hra = $basic * 0.4;
        $exemption1 = $hra;
        $exemption2 = (40 / 100) * $basic;
        $exemption3 = 0;
        if ($rentYearly > 0) {
            $basicRent = $basic * 0.1;
            $exemption3 = $rentYearly - $basicRent;
        }
        $exemption = min($exemption1, $exemption2, $exemption3);

        if ($exemption < 0) {
            $exemption = 0;
        }
        return round($exemption);
    }
    public static function getRentYearly($financialYearId, $userId)
    {
        $itSavingObj = ItSaving::where('financial_year_id', $financialYearId)->where('user_id', $userId)->first();

        if ($itSavingObj) {
            return $itSavingObj->rent_yearly;
        }
        return 0;
    }
    public static function getStandardDeduction()
    {
        return 50000;
    }
    public static function getDeductions($hraExemption, $standardDeduction, $professionalTax, $pfOther, $pfEmployeer)
    {
        $deduction = $hraExemption + $standardDeduction + (($professionalTax < 0) ? ((-1) * $professionalTax) : $professionalTax); // + $pfOther + $pfEmployeer;
        return round($deduction);
    }

    public static function getUserTDSSummary($userId, $financialYearID)
    {
        $response = [];
        $response['data'] = null;
        $response['status'] = false;
        $response['errors'] = null;

        $months = Month::where('financial_year_id', $financialYearID)->where('status', 'processed')->get();
        $components = AppraisalComponentType::get();
        $bonuses = AppraisalBonusType::get();
        $adhocPaymentComponents = AdhocPaymentComponent::all();

        $data = [];
        $monthRow = [];

        $workingId = AppraisalComponentType::getIdByKey('working-days');

        //flags
        $data['fl_appr_bonus_head'] = 0;
        $data['fl_appr_bonus_other'] = 0;
        foreach ($bonuses as $bonus) {
            $data['fl_appr_bonus' . $bonus->id] = 0;
        }
        $data['fl_adhoc_pay_head'] = 0;
        foreach ($adhocPaymentComponents as $adhoc) {
            $data['fl_adhoc_pay' . $adhoc->id] = 0;
        }
        $data['fl_bonus_head'] = 0;
        $data['fl_bonus_onsite'] = 0;
        $data['fl_bonus_additional'] = 0;
        $data['fl_bonus_extra_hour'] = 0;
        $data['fl_bonus_tech_talk'] = 0;
        $data['fl_bonus_performance'] = 0;
        $data['fl_bonus_referral'] = 0;
        $data['fl_bonus_other'] = 0;

        $data['fl_deduct_head'] = 0;
        $data['fl_deduct_vpf'] = 0;
        $data['fl_deduct_food_deduction'] = 0;
        $data['fl_deduct_insurance'] = 0;
        $data['fl_deduct_loan'] = 0;

        foreach ($months as $month) {
            foreach ($components as $component) {
                $obj = Transaction::where('reference_type', 'App\Models\Appraisal\AppraisalComponentType')->where('reference_id', $component->id)->where('user_id', $userId)->where('month_id', $month->id)->first();
                if ($obj) {
                    $data[$month->year . '_' . $month->month]['appraisal_components'][$component->id] = $obj->amount;
                }
            }
            $data[$month->year . '_' . $month->month]['month'] = $month->formatMonth();
            $workingDayObj = UserWorkingDay::where('month_id', $month->id)->where('user_id', $userId)->first();
            if ($workingDayObj) {
                $data[$month->year . '_' . $month->month]['worked_days'] = $workingDayObj->working_days + $workingDayObj->lop;
            } else {
                $data[$month->year . '_' . $month->month]['worked_days'] = 0;
            }

            $data[$month->year . '_' . $month->month]['monthly_gross_salary'] = Transaction::getMonthyGrossSalary($userId, $month->id);
            $data[$month->year . '_' . $month->month]['monthly_bonus'] = Transaction::getAnnualBonusPaid($userId, $month->id);

            $vpf = Transaction::getMontlyPaidByType($userId, $month->id, 'App\Models\Admin\VpfDeduction');
            if ($vpf != 0) {
                $data['fl_deduct_head'] = 1;
                $data['fl_deduct_vpf'] = 1;
                $data[$month->year . '_' . $month->month]['vpf'] = $vpf;
                $vpf = 0;
            }

            $food = Transaction::getMontlyPaidByType($userId, $month->id, 'App\Models\Audited\FoodDeduction\FoodDeduction');
            if ($food != 0) {
                $data['fl_deduct_head'] = 1;
                $data['fl_deduct_food_deduction'] = 1;
                $data[$month->year . '_' . $month->month]['food_deduction'] = $food;
                $food = 0;
            }

            $insurance = Transaction::getMontlyPaidByType($userId, $month->id, 'App\Models\Admin\Insurance\InsuranceDeduction');
            if ($insurance != 0) {
                $data['fl_deduct_head'] = 1;
                $data['fl_deduct_insurance'] = 1;
                $data[$month->year . '_' . $month->month]['insurance'] = $insurance;
                $insurance = 0;
            }

            $loan = Transaction::getMontlyPaidByType($userId, $month->id, 'App\Models\LoanEmi');
            if ($loan != 0) {
                $data['fl_deduct_head'] = 1;
                $data['fl_deduct_loan'] = 1;
                $data[$month->year . '_' . $month->month]['loan'] = $loan;
                $loan = 0;
            }

            $apprBonus = Transaction::where('reference_type', 'App\Models\Appraisal\AppraisalBonus')->where('user_id', $userId)->where('month_id', $month->id)->get();
            $data[$month->year . '_' . $month->month]['appraisal_bonuses']['other'] = 0;
            foreach ($apprBonus as $bonus) {
                if ($bonus->amount != 0) {
                    if ($bonus->reference_id == null || $bonus->reference_id == 0) {
                        $data['fl_appr_bonus_head'] = 1;
                        $data['fl_appr_bonus_other'] = 1;
                        $data[$month->year . '_' . $month->month]['appraisal_bonuses']['other'] += $bonus->amount;
                    } else {
                        $obj = $bonus->reference;
                        if ($obj) {
                            $data['fl_appr_bonus_head'] = 1;
                            $data['fl_appr_bonus' . $obj->appraisal_bonus_type_id] = 1;
                            $data[$month->year . '_' . $month->month]['appraisal_bonuses'][$obj->appraisal_bonus_type_id] = $bonus->amount;
                        }
                    }
                }
            }

            $objs = Transaction::where('reference_type', 'App\Models\Audited\AdhocPayments\AdhocPayment')->where('user_id', $userId)->where('month_id', $month->id)->get();
            foreach ($objs as $obj) {
                $adhocPayment = AdhocPayment::find($obj->reference_id);
                if ($obj->amount != 0 && $adhocPayment->component->id != null) {
                    $data['fl_adhoc_pay_head'] = 1;
                    $data['fl_adhoc_pay' . $adhocPayment->component->id] = 1;
                    $data[$month->year . '_' . $month->month]['adhoc_payments'][$adhocPayment->component->id] = $obj->amount;
                }
            }
            $otherBonuses = Transaction::getMontlyOtherBonus($userId, $month->id, 'App\Models\Admin\Bonus');

            $data[$month->year . '_' . $month->month]['bonuses']['onsite'] = 0;
            $data[$month->year . '_' . $month->month]['bonuses']['additional'] = 0;
            $data[$month->year . '_' . $month->month]['bonuses']['extra_hour'] = 0;
            $data[$month->year . '_' . $month->month]['bonuses']['tech_talk'] = 0;
            $data[$month->year . '_' . $month->month]['bonuses']['performance'] = 0;
            $data[$month->year . '_' . $month->month]['bonuses']['referral'] = 0;
            $data[$month->year . '_' . $month->month]['bonuses']['other'] = 0;

            foreach ($otherBonuses as $bonus) {
                if ($bonus->amount != 0) {
                    $data['fl_bonus_head'] = 1;
                    if ($bonus->reference_id == null || $bonus->reference_id == 0) {
                        $data['fl_bonus_other'] = 1;
                        $data[$month->year . '_' . $month->month]['bonuses']['other'] += $bonus->amount;
                    } elseif ($bonus->bonusType->type == 'onsite') {
                        $data['fl_bonus_onsite'] = 1;
                        $data[$month->year . '_' . $month->month]['bonuses']['onsite'] += $bonus->amount;
                    } elseif ($bonus->bonusType->type == 'referral') {
                        $data['fl_bonus_referral'] = 1;
                        $data[$month->year . '_' . $month->month]['bonuses']['referral'] += $bonus->amount;
                    } elseif ($bonus->bonusType->type == 'techtalk') {
                        $data['fl_bonus_tech_talk'] = 1;
                        $data[$month->year . '_' . $month->month]['bonuses']['tech_talk'] += $bonus->amount;
                    } elseif ($bonus->bonusType->type == 'additional') {
                        $data['fl_bonus_additional'] = 1;
                        $data[$month->year . '_' . $month->month]['bonuses']['additional'] += $bonus->amount;
                    } elseif ($bonus->bonusType->type == 'extra') {
                        $data['fl_bonus_extra_hour'] = 1;
                        $data[$month->year . '_' . $month->month]['bonuses']['extra_hour'] += $bonus->amount;
                    } elseif ($bonus->bonusType->type == 'performance') {
                        $data['fl_bonus_performance'] = 1;
                        $data[$month->year . '_' . $month->month]['bonuses']['performance'] += $bonus->amount;
                    }
                }
            }
        }

        $monthObj = Month::where('status', 'processed')->where('financial_year_id', $financialYearID)->orderBy('year', 'desc')->orderBy('month', 'desc')->first();

        $appraisalList = AppraisalService::getCurrentMonthAppraisal($userId, $monthObj->id);

        if ($appraisalList == null || !isset($appraisalList[0])) {
            $response['errors'] = 'No Appraisal found!';
            $response['data'] = $data;
            return $response;
        }

        $futureAppraisal = $appraisalList->last();
        $futureData = AppraisalService::calculateFutureAppraisal($futureAppraisal->id, $monthObj->id, $userId);

        $upcomingMonths = FinancialYear::getUpcomingMonth($financialYearID, $monthObj->month);

        foreach ($upcomingMonths as $upcomingMonth) {
            $data[$upcomingMonth->year . '_' . $upcomingMonth->month]['worked_days'] = $futureData['no_of_working'][$upcomingMonth->month];
            $data[$upcomingMonth->year . '_' . $upcomingMonth->month]['monthly_gross_salary'] = $futureData[$upcomingMonth->month]['monthly_gross_salary'];
            $data[$upcomingMonth->year . '_' . $upcomingMonth->month]['monthly_bonus'] = $futureData[$upcomingMonth->month]['monthly_bonus'];

            foreach ($components as $component) {
                $data[$upcomingMonth->year . '_' . $upcomingMonth->month]['appraisal_components'][$component->id] = $futureData[$upcomingMonth->month][$component->id];
            }
            foreach ($bonuses as $bonus) {
                if (isset($futureData[$upcomingMonth->month]['appraisal_bonus'][$bonus->id]) && $futureData[$upcomingMonth->month]['appraisal_bonus'][$bonus->id] != 0) {
                    $data['fl_appr_bonus_head'] = 1;
                    $data['fl_appr_bonus' . $bonus->id] = 1;
                    $data[$upcomingMonth->year . '_' . $upcomingMonth->month]['appraisal_bonuses'][$bonus->id] = $futureData[$upcomingMonth->month]['appraisal_bonus'][$bonus->id];
                }
            }
        }

        $response['status'] = true;
        $response['data'] = $data;
        return $response;
    }

    public static function getUserAnnualSummary($tdsData, $financialYearId, $userId)
    {
        $summary = [];
        $prepSalariesArr = self::getUserPrepSalaries($userId, $financialYearId);
        $months = Month::where('financial_year_id', $financialYearId)->where('status', 'processed')->get();
        foreach ($months as $month) {
            if (!empty($prepSalariesArr) && isset($prepSalariesArr[$month->id])) {
                $prepSalId = $prepSalariesArr[$month->id];
                $tdsObj = PrepTds::where('prep_salary_id', $prepSalId)->where('user_id', $userId)->first();
                if ($tdsObj->components != null && count($tdsObj->components) > 0) {
                    foreach ($tdsObj->components as $component) {
                        $summary[$month->year . '_' . $month->month][$component->key] = $component->value;
                    }
                }
            }
        }

        return $summary;
    }
    public static function getUserPrepSalaries($userId, $financialId)
    {
        $arr = [];
        $months = Month::where('financial_year_id', $financialId)->where('status', 'processed')->get();

        foreach ($months as $month) {
            $transObj = Transaction::where('user_id', $userId)->where('month_id', $month->id)->where('prep_salary_id', '!=', 0)->first();
            if ($transObj) {
                $arr[$month->id] = $transObj->prep_salary_id;
            }
        }

        return $arr;
    }
}
