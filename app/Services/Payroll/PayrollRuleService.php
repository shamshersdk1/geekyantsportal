<?php
namespace App\Services\Payroll;

use App\Models\Admin\Appraisal;
use App\Models\Admin\Finance\PayroleGroupRuleMonth;

class PayrollRuleService
{
    protected $appraisalId;
    protected $payroleGroupRuleID;

    public function __construct($appraisalId, $payroleGroupRuleID)
    {
        // $this->userId = $userId;
        $this->appraisalId = $appraisalId;
        //$this->keyId = $keyId;
        $this->payroleGroupRuleID = $payroleGroupRuleID;

    }
    public function getCalculatedOn() {
        $appraisalId = $this->appraisalId;
        $appraisalObj = Appraisal::find($appraisalId);
        $ruleObj = PayroleGroupRuleMonth::find($this->payroleGroupRuleID);
        $calculateOn = round($appraisalObj->annual_gross_salary) / 12;
    }
    public function calculateAmount()
    {
        $appraisalId = $this->appraisalId;
        $appraisalObj = Appraisal::find($appraisalId);
        $ruleObj = PayroleGroupRuleMonth::find($this->payroleGroupRuleID);
        $calculateOn = round($appraisalObj->annual_gross_salary) / 12;

        $amount = 0;
        if ($ruleObj) {
            if ($ruleObj->name == 'basic' || $ruleObj->name == 'intern_stipend') {
                $calculateOn = round($appraisalObj->annual_gross_salary / 12);
            } elseif ($ruleObj->name == 'pf_employee' || $ruleObj->name == 'pf_employeer') {
                if ($calculateOn > 15000) {
                    $calculateOn = 15000;
                }
            } elseif ($ruleObj->calculated_upon != '0' && $ruleObj->calculated_upon != '-1') {

                $calculateUponObj = PayroleGroupRuleMonth::find($ruleObj->calculated_upon);
                if ($calculateUponObj) {
                    $payrollRuleSubObj = new PayrollRuleService($appraisalId, $calculateUponObj->id);
                    $calculateOn = $payrollRuleSubObj->calculateAmount();
                } else {
                }
            } else {
                $calculateOn = $ruleObj->calculation_value;
            }

            if ($ruleObj->calculation_type == '%') {
                $amount = round(($ruleObj->calculation_value * $calculateOn) / 100);
                if ($ruleObj->name == 'pf_other' && $amount > 150) {
                    $amount = 150;
                }

            } else if ($ruleObj->calculation_type == '+') {
                $amount = $ruleObj->calculation_value;
            } else if ($ruleObj->calculation_type == '-') {
                $amount = -1 * $ruleObj->calculation_value;
            }
        }
        return $amount;
    }
}
