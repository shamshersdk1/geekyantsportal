<?php
namespace App\Services\Payroll;

use App\Jobs\PayRoll\RecalculateUserSalaryJob;
use App\Models\Admin\Appraisal;
use App\Models\Admin\AppraisalBonus;
use App\Models\Admin\Bonus;
use App\Models\Admin\Finance\PayroleGroupRuleMonth;
use App\Models\Admin\Finance\SalaryUserData;
use App\Models\Admin\Finance\SalaryUserDataKey;
use App\Models\Admin\Finance\SalaryUserDataTDS;
use App\Models\Admin\FinancialYear;
use App\Models\Admin\ItSaving;
use App\Models\Admin\LeaveDeductionMonth;
use App\Models\Month;
use App\Models\Transaction;
use App\Models\User;
use App\Services\Payroll\PayrollRuleService;

class PayrollService
{
    public static function getHRAExemption($grossAnnualSalary, $rentYearly = 0)
    {
        $basic = round($grossAnnualSalary * 0.4);
        $hra = round($basic * 0.4);
        $exemption1 = $hra;
        $exemption2 = round((40 / 100) * $basic);
        $exemption3 = 0;
        if ($rentYearly > 0) {
            $basicRent = round($basic * 0.1);
            $exemption3 = $rentYearly - $basicRent;
        }
        $exemption = min($exemption1, $exemption2, $exemption3);
        if ($exemption < 0) {
            $exemption = 0;
        }
        return $exemption;
    }

    /*
        Get all Paid till date component from Transaction

    */
    public static function getUserMonthlySalaryComponent($userId, $monthId) {
        return Transaction::where('user_id', $userId)->where('month_id', $monthId)->get();
    }

    public static function getUserGrossSalaryPaidTillDate($userId, $date = date('Y-m-d')) {
        $monthObj = Month::getMonthByDate($date);
        
        if(!$monthObj)
            return 0;

        $previousMonths = Month::where('financial_year_id', $monthObj->financial_year_id)->where('month_number', < , $monthObj->month_number)->get();
        $monthGrossSalary = 0;
        
        foreach ($previousMonths as $previousMonth) {
            $monthGrossSalary += Transaction::getMonthyGrossSalary($userId, $previousMonth->id);
        }

        return $monthGrossSalary;
    }

    public static function getTotalUserTdsPaid($userId, $date = date('Y-m-d')) {
        $monthObj = Month::getMonthByDate($date);
        $tdsPaid = 0;

        if(!$monthObj)
            return 0;

        $previousMonths = Month::where('financial_year_id', $monthObj->financial_year_id)->where('month_number', < , $monthObj->month_number)->get();

        foreach ($previousMonths as $previousMonth) {
            $tdsPaid += Transaction::getUserTDSForTheMonth($userId, $previousMonth->id);
        }

        return $tdsPaid;
    }
    public static function createMonthlyPayroll($month, $year)
    {
        $monthObj = Month::where('month', $month)->where('year', $year)->first();
        \Log::info('creating payroll for the month of' . $month);

        if (!$monthObj) {
            return false;
        }
        \Log::info('Financial Year found ' . $monthObj->financialYear->year);
        $noOfRemaingMonth = FinancialYear::getRemainingMonths($monthObj->id);
        $userSalaryCount = SalaryUserData::where('month_id', $monthObj->id)->count();

        if ($userSalaryCount > 0) {
            \Log::info('Already generated for the month of ' . $month . ' Year ' . $year);
            return false;
        }
        $users = User::where('is_active', 1)->get();
        try {
            foreach ($users as $user) {
                $date = date('Y-m-d', strtotime($monthObj->formatMonth()));
                //$date = '2018-12-25';
                $userId = $user->id;
                \Log::info('Checking Apprisal for UserId ' . $userId . ' on the date :' . $date);
                if ($user) {
                    $lopDays = LeaveDeductionMonth::getLOPDays($userId, $monthObj->id);
                }

                //$userAppraisal = Appraisal::getAppraisalByDate($userId, $date);
                $userAppraisal = $user->activeAppraisal;
                if ($userAppraisal) {

                    // $userAppraisal = $user->activeAppraisal;

                    // if ($userAppraisal == null) {
                    //     continue;
                    // }

                    $monthySalary = round($userAppraisal->annual_gross_salary / 12);
                    $monthVariableBonus = round($userAppraisal->monthly_var_bonus / 12);
                    $annualMonthBonus = round($userAppraisal->annual_bonus / 12);

                    $monthlySalaryToBePaid = round($monthySalary * $noOfRemaingMonth);
                    $monthVariableBonusToBePaid = round($monthVariableBonus * $noOfRemaingMonth);
                    $annualMonthBonusToBePaid = round($annualMonthBonus * $noOfRemaingMonth);

                    // $appraisalId = $userAppraisal->id;

                    // $appraisalBonus = AppraisalBonus::where('appraisal_id', $userAppraisal->id)->where('month_id', $monthObj->id)->where('type', 'monthly_valriable')->first();
                    // $appraisalBonus = new AppraisalBonus;
                    // dd($appraisalBonus);

                    /*
                    $appraisalBonus->appraisal_id = $userAppraisal->id;
                    $appraisalBonus->month_id = $monthObj->id;
                    $appraisalBonus->type = "monthly_variable";
                    $appraisalBonus->amount = $monthVariableBonus;
                    $appraisalBonus->save();
                     */

                    $salaryData = new SalaryUserData;
                    $salaryData->month_id = $monthObj->id;
                    $salaryData->user_id = $user->id;
                    $salaryData->employee_id = $user->employee_id;
                    $salaryData->days_worked = $monthObj->working_days - $lopDays;
                    $salaryData->appraisal_id = $userAppraisal->id;

                    $salaryData->monthly_gross_salary = $monthySalary;
                    $monthySalaryAfterLop = $monthySalary;

                    if ($lopDays > 0) {
                        $monthySalaryAfterLop = round($monthySalary * ($salaryData->days_worked / $monthObj->working_days));
                    }

                    $salaryData->monthly_gross_after_lop = $monthySalaryAfterLop;
                    $salaryData->annual_monthly_bonus = $annualMonthBonus;

                    $result = self::grossAmountPaidTillMonth($user->id, $monthObj->id);
                    $salaryData->annual_gross_salary = $monthlySalaryToBePaid + $result['salaryPaidTillDate'] + $monthySalaryAfterLop;
                    $salaryData->annual_monthly_variable_bonus = $monthVariableBonusToBePaid + $result['monthlyBonusPaidTillDate'] + $monthVariableBonus;
                    $salaryData->annual_gross_bonus = $annualMonthBonusToBePaid + $result['annualMonthlyBonusPaid'] + $annualMonthBonus;

                    $salaryData->other_bonus = $result['otherBonus'];

                    $salaryData->tds_for_the_month = 0;
                    $salaryData->tds_annual = 0;

                    if ($salaryData->save()) {
                        //self::calculateUserSalary($salaryData->id);
                        $job = (new RecalculateUserSalaryJob($salaryData->id));
                        dispatch($job);
                    }
                }
            }

            //DB::commit();

        } catch (Exception $e) {
            //DB::rollback();
        }

    }
    public static function calculatePayrollKeyAmount($appraisalId, $payroleGroupRuleId)
    {
        $payrollRuleObj = new PayrollRuleService($appraisalId, $payroleGroupRuleId);
        $amount = $payrollRuleObj->calculateAmount();
        return $amount;
    }

    public static function calculateUserSalary($salaryDataId)
    {
        $obj = SalaryUserData::with('appraisal.group')->find($salaryDataId);

        if (!$obj) {
            return false;
        }
        $appraisalId = $obj->appraisal_id;
        $factor = 1;
        if ($obj->days_worked < $obj->month->working_days) {
            $factor = round($obj->days_worked / $obj->month->working_days);
        }

        $rules = PayroleGroupRuleMonth::where('payrole_group_id', $obj->appraisal->group->id)->where('month_id', $obj->month_id)->get();
        if (count($rules) > 0) {
            foreach ($rules as $rule) {

                $amount = self::calculatePayrollKeyAmount($appraisalId, $rule->id);
                $keyObj = new SalaryUserDataKey;
                $keyObj->salary_user_data_id = $salaryDataId;
                $keyObj->key_id = $rule->id;
                $keyObj->amount = round($factor * $amount);
                $keyObj->save();
            }
        }
    }
    public static function creditAppraisals($monthId, $userId)
    {
        $data['user_id'] = $userId;
        $data['month_id'] = $monthId;
        $monthObj = Month::find($monthId);

        if (!$monthObj) {
            return;
        }

        $userObj = User::find($userId);

        if (!$userObj) {
            return false;
        }

        if ($userObj->activeAppraisal) {
            $data['reference_id'] = $userObj->activeAppraisal->id;
            $data['reference_type'] = 'App\Models\Admin\Appraisal';
        }
        $data['amount'] = $userObj->activeAppraisal->annual_gross_salary;
        $data['type'] = 'credit';
        $response = Transaction::saveTransaction($data);
        return $response;
    }
    public static function getTDSDataNew($salaryDataId)
    {
        $salaryData = SalaryUserData::find($salaryDataId);
        if (!$salaryData) {
            return false;
        }

        $monthObj = Month::find($salaryData->month_id);
        $appraisalObj = Appraisal::find($salaryData->appraisal_id);

        if (!$monthObj || !$appraisalObj) {
            return 0;
        }

        $financialyearId = $salaryData->month->financialYear->id;

        $pfOther = self::grossAnnualAmountByKey($salaryData->user_id, $salaryData->month_id, 'pf_other');
        $pfEmployeer = self::grossAnnualAmountByKey($salaryData->user_id, $salaryData->month_id, 'pf_employeer');

        $data['pf_other'] = $pfOther;
        $data['pf_employeer'] = $pfEmployeer;

        //change
        $data['gross_annual'] = self::grossAnnualSalary($salaryData->user_id, $salaryData->month_id);

        //doubt and change
        $grossAnnualSalary = $data['gross_annual']['gross_annual_salary']; //$salaryData->annual_ctc;
        $data['hra_exemption'] = self::getHRAExemption($salaryData->user_id, $grossAnnualSalary, $financialyearId);
        $data['standard_deduction'] = self::getStandardDeduction($salaryDataId);
        $data['lta'] = self::grossAnnualAmountByKey($salaryData->user_id, $salaryData->month_id, 'leave_travel_allowance');

        $data['professional_tax'] = (200 * 12);
        
        $data['deductions'] = $data['hra_exemption']['exemption'] + $data['standard_deduction'] + $data['professional_tax'] + $pfOther + $pfEmployeer;

        $data['it_saving'] = ItSaving::getITSavingInfo($salaryData->user_id, $monthObj->id);

        $data['total_it_saving'] = $data['it_saving']['investment'] + $data['it_saving']['deduction'];
        
        
        $totalSalary = $data['gross_annual']['gross_annual_salary'] + $data['gross_annual']['gross_annual_fixed_bonus'] + $data['gross_annual']['monthly_variable_bonus'] + $data['gross_annual']['confirmation_bonus'] + $data['gross_annual']['other_bonuses'];


        // $data['total_salary'] = $totalSalary;
        $data['net_taxable_salary'] = $totalSalary - $data['deductions'];


        $data['taxable_income'] = $data['net_taxable_salary'] - $data['total_it_saving'];
        $tds = self::incomeTaxPayableSlab($data['taxable_income']);

        $data['income_tax'] = $tds;
        $eduCess = round($tds * 0.04);
        $data['edu_cess'] = $eduCess;
        $noOfRemaingMonth = FinancialYear::getRemainingMonths($salaryData->month_id);
        $data['tax_payable'] = round($eduCess + $tds);
        $data['balance_tax_payable'] = $data['tax_payable'] - $data['gross_annual']['tds_paid_till_date'];
        $data['income_tax_for_the_month'] = round($data['balance_tax_payable'] / ($noOfRemaingMonth + 1));

        return $data;
    }
    public static function getTDSData($salaryDataId)
    {

        $salaryData = SalaryUserData::find($salaryDataId);
        if (!$salaryData) {
            return false;
        }

        $monthObj = Month::find($salaryData->month_id);
        $appraisalObj = Appraisal::find($salaryData->appraisal_id);

        if (!$monthObj || !$appraisalObj) {
            return 0;
        }

        // $foodAllowance = self::grossAnnualAmountByKey($salaryData->user_id, $salaryData->month_id, 'food_allowance');
        $pfOther = self::grossAnnualAmountByKey($salaryData->user_id, $salaryData->month_id, 'pf_other');

        $pfEmployeer = self::grossAnnualAmountByKey($salaryData->user_id, $salaryData->month_id, 'pf_employeer');
        //echo "food_allowance".$foodAllowance;
        //die;
        // $data['hra'] = SalaryUserDataKey::getAmountByKey($salaryDataId, 'hra');
        //$data['food_allowance'] = $foodAllowance;

        $data['pf_other'] = $pfOther;
        $data['pf_employeer'] = $pfEmployeer;
        //$data['gross_earning'] = self::grossEarningForMonth($appraisalObj->id);
        $data['gross_annual'] = self::grossAnnualSalary($salaryData->user_id, $salaryData->month_id);
        $data['hra_exemption'] = self::getHRAExemption($data['gross_annual']['gross_annual_salary'], $salaryDataId);
        $data['standard_deduction'] = self::getStandardDeduction($salaryDataId);
        //$data['lta'] = self::getLTA($salaryDataId);
        $data['lta'] = self::grossAnnualAmountByKey($salaryData->user_id, $salaryData->month_id, 'leave_travel_allowance');

        $data['professional_tax'] = (200 * 12);
        // $data['total_bonus'] = Bonus::where('user_id', $salaryData->user_id)->where('month_id', $salaryData->month_id)->sum('amount');
        // $data['total_bonus'] += 1582.00;

        // echo $salaryData->user_id;
        // echo "## ".$salaryData->month_id;

        // echo "<pre>";
        // print_r($bonusAmount);
        // die;
        // echo "Bonus ".$bonusAmount;
        // die;
        $data['deductions'] = $data['hra_exemption']['exemption'] + $data['standard_deduction'] + $data['professional_tax'] + $pfOther + $pfEmployeer;
        $data['it_saving'] = ItSaving::getITSavingInfo($salaryData->user_id, $monthObj->id);
        // echo "<pre>";
        // print_r($data['it_saving']);
        // die;
        // C = Salary from GeeyAnts Monthly. // NET salary
        // Net taxable Salary : C = A - B

        // D = we need to calculate 'C' for each month (12 months) and add them up to get 'Salary from GeeyAnts for the financial Year'

        // E = INVESTMENTS U/S 80C, capped at Rs 1.5 Lac(max 1.5 lac)
        // 150000
        // F = Other DEDUCTIONS

        // G = Total IT Saving = E+F

        // $response['gross_annual_salary'] = $grossPaidTillDate + $grossToToPaid;
        // $response['gross_annual_fixed_bonus'] = $annualFixedBonusPaidTillDate + $annualFixedBonusToToPaid;
        // $response['monthly_variable_bonus'] = $montlyVarBonusPaidTillDate + $montlyVarBonusToBePaidDate;
        // $response['confirmation_bonus'] = $confirmationBonus;
        // $response['other_bonuses'] = $otherBonusPaidTillDate;

        $data['total_it_saving'] = $data['it_saving']['investment'] + $data['it_saving']['deduction'];
        $totalSalary = $data['gross_annual']['gross_annual_salary'] + $data['gross_annual']['gross_annual_fixed_bonus'] + $data['gross_annual']['monthly_variable_bonus'] + $data['gross_annual']['confirmation_bonus'] + $data['gross_annual']['other_bonuses'];
        $data['total_salary'] = $totalSalary;
        $data['net_taxable_salary'] = $totalSalary - $data['deductions'];
        // echo "675sfDG8";
        // Taxable Income for the financial Year = (D) - (G)
        $data['taxable_income'] = $data['net_taxable_salary'] - $data['total_it_saving'];
        $tds = self::incomeTaxPayableSlab($data['taxable_income']);

        $data['income_tax'] = $tds;
        //Add : Edu790lop[=]
        //. Cess & SHEC @ 4%
        $eduCess = round($tds * 0.04);
        $data['edu_cess'] = $eduCess;
        $noOfRemaingMonth = FinancialYear::getRemainingMonths($salaryData->month_id);
        $data['tax_payable'] = round($eduCess + $tds);
        $data['balance_tax_payable'] = $data['tax_payable'] - $data['gross_annual']['tds_paid_till_date'];
        $data['income_tax_for_the_month'] = round($data['balance_tax_payable'] / ($noOfRemaingMonth));

        //Salary from GeeyAnts Monthly
        //$data['last_month_tds'] = self::getLastMonthTDS($salaryDataId);

        // echo date("Y-n-j", strtotime("first day of previous month"));
        // echo date("Y-n-j", strtotime("last day of previous month"));

        // echo "<pre>";
        // print_r($data);
        // die;

        return $data;

    }
    public static function incomeTaxPayableSlab($taxableAmount)
    {

        // Income Tax Payable on Total Income
        // Rs 0 - Rs 250000
        // Rs 250001 - Rs 500000 @ 5%
        // Rs 500001- Rs 1000000  @ 20%
        // Above Rs 1000000 @ 30%

        $incomeTax = 0;
        if ($taxableAmount < 250000) {
            return 0;
        }

        if ($taxableAmount > 1000000) {
            $incomeTax = 12500 + 100000 + (($taxableAmount - 1000000) * 0.3);
        } elseif ($taxableAmount > 500000) {
            $incomeTax = 12500 + (($taxableAmount - 500000) * 0.2);
        } elseif ($taxableAmount > 250000) {
            $incomeTax = ($taxableAmount - 250000) * 0.05;
        }
        return round($incomeTax);

    }
    public static function grossEarningForMonth($appraisalId)
    {
        $appraisalObj = Appraisal::find($appraisalId);

        if (!$appraisalObj) {
            return 0;
        }

        $annualGrossSalary = $appraisalObj->annual_gross_salary;
        // Gross Earning for the Year Projected (GEY)
        // = (Monthly Gross *12 + Annual Bonus: Chargable for current F.Y. + Monthly Var Bonus + other Income (Bonuses) )

        // $grossEarning = 0;

        // $appraisal = self::getAppraisalByDate($userId, $date);

        //if ($appraisal) {
        $grossEarning = $appraisalObj->annual_gross_salary + $appraisalObj->annual_bonus + $appraisalObj->monthly_var_bonus;
        //$amount = Bonus::where('user_id', $this->user_id)->where('month_id', $monthId)->sum('amount');
        //}

        return $grossEarning;
    }
    public static function getHRAExemption($grossAnnualSalary, $rentYearly = 0;)
    {
        $basic = round($grossAnnualSalary * 0.4);
        $hra = round($basic * 0.4);
        $exemption1 = $hra;
        $exemption2 = round((40 / 100) * $basic);
        $exemption3 = 0;
        if ($rentYearly > 0) {
            $basicRent = round($basic * 0.1);
            $exemption3 = $rentYearly - $basicRent;
        }
        $exemption = min($exemption1, $exemption2, $exemption3);
        if ($exemption < 0) {
            $exemption = 0;
        }
        return $exemption;
    }
    public static function calculateHRAExemption($userId, $grossAnnualSalary, $financialyearId)
    {

        $basic = round($grossAnnualSalary * 0.4);
        $hra = round($basic * 0.4);
        //
        //$hra = SalaryUserDataKey::getAmountByKey($salaryDataId, 'hra');
        $data['exemption_1'] = $hra;
        $data['exemption_2'] = round((40 / 100) * $basic);
        $data['exemption_3'] = 0;

        $itSavingobj = ItSaving::where('user_id', $userId)->where('financial_year_id', $financialyearId)->first();

        if ($itSavingobj) {
            $rentYearly = $itSavingobj->rent_yearly;
            $basicRent = round($basic * 0.1);
            $data['exemption_3'] = $rentYearly - $basicRent;
        }

        $data['basic_value'] = $basic;
        $data['hra_value'] = $hra;
        $exemption = min($data['exemption_1'], $data['exemption_2'], $data['exemption_3']);

        if ($exemption < 0) {
            $exemption = 0;
        }

        $data['exemption'] = $exemption;

        return $data;

    }
    public static function getStandardDeduction()
    {
        $amount = 40000;
        return $amount;
    }
    public static function getLastMonthTDS($userId, $monthId)
    {
        $lastMonthDate = date("Y-n-j", strtotime("first day of previous month"));
        $monthObj = Month::find($monthId);

        $lastMonthId = Month::getMonth($lastMonthDate);
        if ($lastMonthId) {
            $itSavingUserObj = SalaryUserDataTDS::where('user_id', $salaryData->user_id)->where('month_id', $lastMonthId)->first();
            if ($itSavingUserObj) {
                return $itSavingUserObj->tds_amount;
            }
        }

    }
    public static function getLTA()
    {
        $amount = 0;
        return $amount;
    }

    public static function grossAnnualSalary($userId, $monthId)
    {
        $response = [];
        // $grossPaidTillDate = 0;
        // $annualFixedBonusPaidTillDate = 0;
        // $montlyVarBonusPaidTillDate = 0;
        $tdsPaidTillDate = 0;
        $confirmationBonus = 0;
        $otherBonusPaidTillDate = 0;
        $salaryObj = SalaryUserData::where('month_id', $monthId)->where('user_id', $userId)->first();
        // SalaryUserData::where('month_id', $monthId)->where('user_id', $userId)->sum('monthly_gross_salary');
        // dd($salaryObj->annual_gross_salary);
        $response['gross_annual_salary'] = $salaryObj->annual_gross_salary; //$grossPaidTillDate + $grossToToPaid;
        $response['gross_annual_fixed_bonus'] = $salaryObj->annual_gross_bonus; //$annualFixedBonusPaidTillDate + $annualFixedBonusToToPaid;
        $response['monthly_variable_bonus'] = $salaryObj->annual_monthly_variable_bonus; //$montlyVarBonusPaidTillDate + $montlyVarBonusToBePaidDate;
        // $response['confirmation_bonus'] = $salaryObj->annual_monthly_variable_bonus; //$montlyVarBonusPaidTillDate + $montlyVarBonusToBePaidDate;

        // grossPaidTillDate
        // montlyVarBonusPaidTillDate
        // annualFixedBonusPaidTillDate
        // otherBonusPaidTillDate
        // tdsPaidTillDate

        $monthObj = Month::find($monthId);
        //echo "Current monthObj ".$monthObj->formatMonth();

        // if ($monthObj->id > 13) {
        //     $response['gross_annual_salary'] = 0; //$salaryObj->;
        //     $response['gross_annual_fixed_bonus'] = 0; //$salaryObj->
        //     $response['monthly_variable_bonus'] = 0; //$salaryObj->
        //     $response['confirmation_bonus'] = 0; //$salaryObj->
        //     $response['other_bonuses'] = 0; //$salaryObj->
        //     $response['tds_paid_till_date'] = 0; //(-1) * $salaryObj->
        //     //total annual gross salary
        //     return $response;
        // }
        if ($monthObj) {
            $months = Month::where('financial_year_id', $monthObj->financial_year_id)->where('month_number', '<', $monthObj->month_number)->orderBy('month_number')->get();
            foreach ($months as $month) {
                // $salaryObj = SalaryUserData::where('month_id', $month->id)->where('user_id', $userId)->first();
                // if ($salaryObj && $salaryObj->appraisal) {
                //     $grossPaidTillDate += $salaryObj->monthly_gross_salary;
                //     //$montlyVarBonusPaidTillDate += $salaryObj->monthly_variable_bonus;
                //$annualFixedBonusPaidTillDate += $salaryObj->annual_bonus;
                $itUserSavingObj = SalaryUserDataTDS::where('month_id', $month->id)->where('user_id', $userId)->first();
                if ($itUserSavingObj) {
                    $tdsPaidTillDate += $itUserSavingObj->tds_amount;
                }
                //$otherBonusPaidTillDate += Bonus::where('user_id', $salaryObj->user_id)->where('month_id', $month->id)->sum('amount');
                // add 5 days extra salary
                // $grossPaidTillDate += round($salaryObj->appraisal->annual_gross_salary / 12);
                // if ($month->month == 8 && $month->year == 2018) {
                //     $advanceSalary = round($salaryObj->monthly_gross_salary / 21) * 5;
                //     $grossPaidTillDate += $advanceSalary;
                // }

                // if (count($salaryObj->appraisal->bonuses) > 0) {
                //     $montlyVarBonusPaidTillDate += AppraisalBonus::where('appraisal_id', $salaryObj->appraisal->id)->where('month_id', $month->id)->where('type', 'monthly_variable')->sum('amount');
                $confirmationBonus += AppraisalBonus::where('appraisal_id', $salaryObj->appraisal->id)->where('month_id', $month->id)->where('type', 'confirmation')->sum('amount');
                //$annualFixedBonusPaidTillDate += AppraisalBonus::where('appraisal_id', $salaryObj->appraisal->id)->where('month_id', $month->id)->where('type', 'annual')->sum('amount');
                // }
                //financial_year_id
                // if ($monthId != $month->id) {
                //     $itUserSavingObj = ITSavingUserData::where('month_id', $month->id)->where('user_id', $userId)->first();
                //     if ($itUserSavingObj) {
                //         $tdsPaidTillDate += $itUserSavingObj->tds_amount;
                //     }
                // $itSavingObj = ITSaving::where('financial_year_id', $monthObj->financial_year_id)->where('user_id', $userId)->first();
                // if ($itSavingObj) {
                //     $itUserSavingObj = ITSavingUserData::where('month_id', $month->id)->where('it_saving_id', $itSavingObj->id)->first();
                //     if ($itUserSavingObj) {
                //         $tdsPaidTillDate += (-1) * $itUserSavingObj->tds_amount;
                //     }
                // }
                // }
                //$tdsPaidTillDate = -18755.00
                // $annualFixedBonusPaidTillDate += round($salaryObj->appraisal->annual_bonus / 12);
                $otherBonusPaidTillDate += Bonus::where('user_id', $salaryObj->user_id)->where('month_id', $month->id)->where('status', 'approved')->sum('amount');
            }
            $prevYear = FinancialYear::getPreviousYearId(2019);
            $prevMarch = Month::getMonthId($prevYear, 3);
            $otherBonusPaidTillDate += Bonus::where('user_id', $salaryObj->user_id)->where('month_id', $prevMarch->id)->where('status', 'approved')->sum('amount');

        }
        // $grossToToPaid = 0;
        // $annualFixedBonusToToPaid = 0;
        // $date = $monthObj->getLastDay();
        // $appraisalObj = Appraisal::getAppraisalByDate($userId, $date);
        // $noOfRemaingMonth = FinancialYear::getRemainingMonths($monthId);
        // $otherBonusPaidTillDate += Bonus::where('user_id', $userId)->where('month_id', $monthId)->sum('amount');
        // //Loand
        // //$otherBonusPaidTillDate += 1582;
        // if ($noOfRemaingMonth > 0) {

        //     $grossToToPaid = round(($appraisalObj->annual_gross_salary / 12) * $noOfRemaingMonth);
        //     $montlyVarBonusToBePaidDate = round(($appraisalObj->monthly_var_bonus / 12) * $noOfRemaingMonth);
        //     $annualFixedBonusToToPaid = round(($appraisalObj->annual_bonus / 12) * $noOfRemaingMonth);
        // }

        $response['confirmation_bonus'] = $confirmationBonus;
        $response['other_bonuses'] = $otherBonusPaidTillDate;
        $response['tds_paid_till_date'] = (-1) * $tdsPaidTillDate;
        //total annual gross salary
        return $response;
    }
    public static function annualSalaryPaid($userId, $monthId)
    {
        $annaulPaidTillDate = 0;

        $monthObj = Month::find($monthId);
        //echo "Current monthObj ".$monthObj->formatMonth();
        if ($monthObj) {
            $months = Month::where('financial_year_id', $monthObj->financial_year_id)->where('month_number', '<', $monthObj->month_number)->orderBy('month_number')->get();
            foreach ($months as $month) {
                $itSavingObj = ITSaving::where('financial_year_id', $month->financial_year_id)->where('user_id', $userId)->first();
                if ($itSavingObj) {
                    $itUserSavingObj = SalaryUserDataTDS::where('month_id', $month->id)->where('it_saving_id', $itSavingObj->id)->first();
                    if ($itUserSavingObj) {
                        $annaulPaidTillDate += $itUserSavingObj->total_earning_for_the_moth;
                    }
                }
            }
        }

        return $annaulPaidTillDate;
    }
    public static function annualTDSPaid($userId, $monthId)
    {
        $tdsPaidTillDate = 0;

        $monthObj = Month::find($monthId);
        //echo "Current monthObj ".$monthObj->formatMonth();
        if ($monthObj) {
            $months = Month::where('financial_year_id', $monthObj->financial_year_id)->where('month_number', '<', $monthObj->month_number)->orderBy('month_number')->get();
            foreach ($months as $month) {
                $itUserSavingObj = SalaryUserDataTDS::where('month_id', $month->id)->where('user_id', $userId)->first();
                if ($itUserSavingObj) {
                    $tdsPaidTillDate += $itUserSavingObj->tds_amount;
                }

            }
        }

        return $tdsPaidTillDate;
    }
    public static function getGrossSalaryForMonth($userId, $monthId)
    {
        $response = [];
        $grossSalaryMonth = 0;
        $monthlyVarBonusToBePaidDate = 0;
        $annualFixedBonusToToPaid = 0;
        $confirmationBonus = 0;

        $monthObj = Month::find($monthId);
        if ($monthObj) {
            $grossToToPaid = 0;
            $annualFixedBonusToToPaid = 0;
            $date = $monthObj->getLastDay();
            $appraisalObj = Appraisal::getAppraisalByDate($userId, $date);
            $noOfRemaingMonth = FinancialYear::getRemainingMonths($monthId);
            if ($noOfRemaingMonth > 0) {
                $grossToToPaid = round(($appraisalObj->annual_gross_salary / 12));
            }
            if (count($appraisalObj->bonuses) > 0) {
                $monthlyVarBonusToBePaidDate += AppraisalBonus::where('appraisal_id', $appraisalObj->id)->where('month_id', $monthId)->where('type', 'monthly_variable')->sum('amount');
                $confirmationBonus += AppraisalBonus::where('appraisal_id', $appraisalObj->id)->where('month_id', $monthId)->where('type', 'confirmation')->sum('amount');
            }
            $monthlyVarBonusToBePaidDate = round(($appraisalObj->monthly_var_bonus / 12));
            $annualFixedBonusToToPaid = round(($appraisalObj->annual_bonus / 12));
        }

        //$totalSalary = 46500 + $data['gross_annual']['gross_annual_salary'] + $data['gross_annual']['gross_annual_fixed_bonus'] + $data['gross_annual']['monthly_variable_bonus'] + $data['gross_annual']['confirmation_bonus'] + $data['gross_annual']['other_bonuses'];
        $grossSalaryMonth = $grossToToPaid + $annualFixedBonusToToPaid + $monthlyVarBonusToBePaidDate;
        return $grossSalaryMonth;
    }
    public static function getBonusesForMonth($userId, $monthId)
    {
        $bonusesForTheMonth = Bonus::where('user_id', $userId)->where('month_id', $monthId)->where('status', 'approved')->sum('amount');
        return $bonusesForTheMonth;
    }
    public static function getTotalSalaryToBePaid($userId, $monthId)
    {
        $response = [];
        $grossPaidTillDate = 0;
        $annualFixedBonusPaidTillDate = 0;
        $montlyVarBonusPaidTillDate = 0;
        $tdsPaidTillDate = 0;
        $confirmationBonus = 0;
        $otherBonusToBePaid = 0;

        $monthObj = Month::find($monthId);
        //echo "Current monthObj ".$monthObj->formatMonth();
        if ($monthObj) {

            $grossToToPaid = 0;
            $annualFixedBonusToToPaid = 0;
            $date = $monthObj->getLastDay();
            $appraisalObj = Appraisal::getAppraisalByDate($userId, $date);
            $noOfRemaingMonth = FinancialYear::getRemainingMonths($monthId);
            if ($noOfRemaingMonth > 0) {
                $grossToToPaid = round(($appraisalObj->annual_gross_salary / 12) * $noOfRemaingMonth);
            }
            if (count($appraisalObj->bonuses) > 0) {
                $montlyVarBonusPaidTillDate += AppraisalBonus::where('appraisal_id', $appraisalObj->id)->where('month_id', $monthId)->where('type', 'monthly_variable')->sum('amount');
                $confirmationBonus += AppraisalBonus::where('appraisal_id', $appraisalObj->id)->where('month_id', $monthId)->where('type', 'confirmation')->sum('amount');
            }

            // $montlyVarBonusToBePaidDate = round(($appraisalObj->monthly_var_bonus / 12) * $noOfRemaingMonth, 2);
            // $annualFixedBonusToToPaid = round(($appraisalObj->annual_bonus / 12) * $noOfRemaingMonth, 2);
            // $otherBonusToBePaid += Bonus::where('user_id', $userId)->where('month_id', $monthId)->sum('amount');
        }

        //$totalSalary = 46500 + $data['gross_annual']['gross_annual_salary'] + $data['gross_annual']['gross_annual_fixed_bonus'] + $data['gross_annual']['monthly_variable_bonus'] + $data['gross_annual']['confirmation_bonus'] + $data['gross_annual']['other_bonuses'];
        $totalSalary = $grossToToPaid + $annualFixedBonusToToPaid + $montlyVarBonusToBePaidDate + $confirmationBonus + $otherBonusToBePaid;

        return $totalSalary;
    }
    //monthly vriable bonuses
    public static function grossAnnualMVBSalary($userId, $monthId)
    {
        $paidTillDate = 0;
        $monthObj = Month::find($monthId);
        //echo "Current monthObj ".$monthObj->formatMonth();
        if ($monthObj) {
            //$bonuses = Bonus::where('user_id',$userId)->where('month_id',$monthId)->where(->get();
            if (count($bonuses) > 0) {
                foreach ($bonuses as $key => $value) {
                    # code...
                }
            }
            $months = Month::where('financial_year_id', $monthObj->financial_year_id)->where('month_number', '<=', $monthObj->month_number)->orderBy('month_number')->get();
            foreach ($months as $month) {
                //echo "<br/>Month Numbers".$month->month_number;
                $salaryObj = SalaryUserData::where('month_id', $month->id)->where('user_id', $userId)->first();
                if ($salaryObj && $salaryObj->appraisal) {
                    $annualGrossSalary = $salaryObj->appraisal->annual_gross_salary + $salaryObj->appraisal->annual_bonus;
                    $paidTillDate += round($annualGrossSalary / 12);
                }
            }

            $toToPaid = 0;
            $date = $monthObj->getLastDay();
            $appraisalObj = Appraisal::getAppraisalByDate($userId, $date);
            $noOfRemaingMonth = FinancialYear::getRemainingMonths($monthId);
            if ($noOfRemaingMonth > 0) {
                $toToPaid = round(($appraisalObj->annual_gross_salary * $noOfRemaingMonth) / 12);
            }

        }

        //total annual gross salary
        return $paidTillDate + $toToPaid;
    }
    public static function grossAmountPaidTillMonth($userId, $monthId)
    {
        $result['salaryPaidTillDate'] = 0;
        $result['monthlyBonusPaidTillDate'] = 0;
        $result['annualMonthlyBonusPaid'] = 0;
        $result['otherBonus'] = 0;

        $monthObj = Month::find($monthId);
        if ($monthObj) {
            $months = Month::where('financial_year_id', $monthObj->financial_year_id)->where('month_number', '<', $monthObj->month_number)->orderBy('month_number')->get();
            foreach ($months as $month) {

                $salaryObj = SalaryUserData::where('month_id', $month->id)->where('user_id', $userId)->first();

                if ($salaryObj) {
                    $appraisalBonus = AppraisalBonus::where('appraisal_id', $salaryObj->appraisal_id)->where('month_id', $month->id)->where('type', 'monthly_valriable')->first();
                    $result['salaryPaidTillDate'] += $salaryObj->monthly_gross_after_lop;
                    $result['monthlyBonusPaidTillDate'] += $appraisalBonus->amount;
                    $result['annualMonthlyBonusPaid'] += $salaryObj->annual_monthly_bonus;
                }
            }

            $result['otherBonus'] = Bonus::where('user_id', $userId)->where('month_id', $monthId)->where('status', 'approved')->sum('amount');
        }

        return $result;
    }
    public static function grossAnnualAmountByKey($userId, $monthId, $key)
    {

        $paidTillDate = 0;
        $amountForCurrentMonth = 0;
        $monthObj = Month::find($monthId);
        if ($monthObj) {
            $months = Month::where('financial_year_id', $monthObj->financial_year_id)->where('month_number', '<', $monthObj->month_number)->orderBy('month_number')->get();
            foreach ($months as $month) {
                $salaryObj = SalaryUserData::where('month_id', $month->id)->where('user_id', $userId)->first();
                if ($salaryObj) {
                    $paidTillDate += SalaryUserDataKey::getAmountByKey($salaryObj->id, $key);
                    $paidTillDate = round($paidTillDate);
                }
            }

            $amountForRemainingMonth = 0;
            $salaryObj = SalaryUserData::where('month_id', $monthId)->where('user_id', $userId)->first();
            if ($salaryObj) {
                $amountForCurrentMonth = SalaryUserDataKey::getAmountByKey($salaryObj->id, $key);
            }

            // $date = $monthObj->getLastDay();
            //$appraisalObj = Appraisal::getAppraisalByDate($userId, $date);
            $noOfRemaingMonth = FinancialYear::getRemainingMonths($monthId);

            if ($noOfRemaingMonth > 0) {
                $amountForRemainingMonth = round(($amountForCurrentMonth * $noOfRemaingMonth));
            }

        }

        //annual amount salary for the $key
        return $paidTillDate + $amountForCurrentMonth + $amountForRemainingMonth;
    }

    public static function getGrossPaidTillLastMonth($userId, $financialyearId)
    {
        $totalAmountPaid = 0;
        $itSavingObj = ItSaving::where('user_id', $userId)->where('financial_year_id', $financialyearId)->first();
        if ($itSavingObj) {
            $lastMonthObj = Month::where('financial_year_id', $financialyearId)->orderBy('year', 'DESC')->orderBy('month', 'DESC')->first();
            if ($lastMonthObj) {
                $lastMonthTdsObj = SalaryUserDataTDS::where('it_saving_id', $itSavingObj->id)->where('month_id', $lastMonthObj->id)->first();
                if ($lastMonthTdsObj) {
                    return $lastMonthTdsObj->total_salary_paid;
                }

            }
        }
        return $totalAmountPaid;
    }
    // public static function getGrossSalaryPaid($userId, $financialyearId) {

    //     $financialYearObj = FinancialYear::find($financialyearId);
    //     $totalGrossPaid = 0;

    //     if($financialYearObj) {
    //         if(count($financialYearObj->months) > 0) {
    //             foreach ($financialYearObj->months as $month) {
    //                 $salaryObj = SalaryUserData::where('user_id', $userId)->where('month_id',$month)->first();
    //                 if($salaryObj) {
    //                     $totalGrossPaid += ($salaryObj->appraisal->annual_gross_salary/12);
    //                     // $ruleObj = PayroleGroupRuleMonth::where('payrole_group_id', $salaryObj->appraisal->group->id)->where('name', $keyName)->where('month_id',$month->id)->first();
    //                     // $basicObj = SalaryUserDataKey::where('key_id',$ruleObj->id)->where('salary_user_data_id', $salaryObj->id)->first();
    //                     // if($basicObj) {
    //                     //      $totalBasic += $basicObj->amount;
    //                     // }
    //                 }
    //             }
    //         }
    //     }
    //     return $totalGrossPaid;
    // }
    public static function calculateMonthlyTDS($month, $year)
    {
        $monthObj = Month::where('month', $month)->first();
        \Log::info('creating payroll for the month of' . $month);

        if (!$monthObj) {
            return false;
        }
        $salaryDatas = SalaryUserData::where('month_id', $monthObj->id)->get();
        if (count($salaryDatas) > 0) {
            foreach ($salaryDatas as $salaryData) {

                $itSavingObj = ItSaving::where('financial_year_id', $monthObj->financial_year_id)->where('user_id', $salaryData->user_id)->first();
                if ($itSavingObj) {
                    $noOfRemaingMonth = FinancialYear::getRemainingMonths($monthObj->id);
                    $grossSalaryForTheMonth = PayrollService::getGrossSalaryForMonth($salaryData->user_id, $monthObj->id);
                    $totalBonuses = PayrollService::getBonusesForMonth($salaryData->user_id, $monthObj->id);
                    $annualSalaryPaid = PayrollService::annualSalaryPaid($salaryData->user_id, $monthObj->id);
                    $tdsData = self::getTDSData($salaryData->id);
                    $itUserData = SalaryUserDataTDS::where('user_id', $salaryData->user_id)->where('month_id', $monthObj->id)->first();
                    if ($itUserData) {
                        $itUserData->delete();
                    }
                    $itUserData = new SalaryUserDataTDS;
                    $itUserData->it_saving_id = $itSavingObj->id;
                    $itUserData->user_id = $itSavingObj->user_id;
                    $itUserData->month_id = $monthObj->id;
                    // $itUserData->annual_salary_to_be_paid = $grossSalaryForTheMonth + ($grossSalaryForTheMonth * $noOfRemaingMonth) + $totalBonuses;
                    // $itUserData->total_earning_for_the_moth = $grossSalaryForTheMonth + $totalBonuses;
                    //$itUserData->annual_salary_paid = $annualSalaryPaid;
                    $itUserData->tds_amount = $tdsData['income_tax_for_the_month'];
                    $itUserData->total_gross_salary_paid = 000; //$tdsData['income_tax_for_the_month'];
                    //$itUserData->tds_amount = $tdsData['income_tax_for_the_month'];
                    // $itUserData->tds_paid_till_date = self::annualTDSPaid($itSavingObj->user_id, $monthObj->id);
                    // $itUserData->tax_payable = $tdsData['tax_payable'];
                    if ($itUserData->save()) {
                        echo "SAVE";
                    } else {
                        echo "not SAVE";
                    }

                }
            }
        }

    }
}
