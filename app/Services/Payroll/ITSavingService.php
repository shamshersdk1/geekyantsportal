<?php
namespace  App\Services\Payroll;

use App\Models\Admin\ItSaving;
use App\Models\Admin\ItSavingOther;
use App\Models\Admin\VpfDeduction;
use App\Models\Appraisal\AppraisalComponentType;
use App\Models\Month;
use App\Models\Transaction;
use OwenIt\Auditing\Models\Audit;

class ITSavingService {
    public static function getTdsAmount($totalGrossSalaryPaid, $toBePaid, $monthId) {
        //
        $amount = 0;
        return $amount;

    }
    public static function getTotalGrossSalaryPaid($monthId) {
        $itSavingUserData = ITSavingUserData::where('user_id',$userId)->where('month_id',$monthId)->first();

    }
    public static function getAudits()
    {
        $fromDate = date('Y-m-d', strtotime("0 days"));
        $toDate = date('Y-m-d');
        $audits = Audit::orderBy('created_at')->whereIn('auditable_type',['App\Models\Admin\ItSaving','App\Models\Admin\ItSavingOther'])->orderBy('created_at', 'DESC')->get;
        $data = [];
        foreach($audits as $audit)
        {
            $userId = NULL;
            if(isset($audit->auditable->user_id)){
                $userId = $audit->auditable->user_id;
            } else {
                $userId = $audit->auditable->itsaving->user_id;
            }
            $data[$userId][] = $audit;
        }
        return $data;
    }
    public static function getItSavingOtherData($itSavingId) {
        $otherData = [];
        $ItSavingObj = ItSaving::find($itSavingId);
        if (!$ItSavingObj) {
            return false;
        }
        if ($ItSavingObj) {
            $data = ItSavingOther::where('it_savings_id',$ItSavingObj->id)->where('type','other_multiple_investments')->pluck('value','title')->toArray();
            if(count($data)>0)
            {
                $array['name']=[];
                $array['amount']=[];
                foreach($data as $key => $value){
                    $array['name'][]=$key;
                $array['amount'][]=$value;
                }
                $otherData['other_multiple_investments'] = $array;
            }

            $data = ItSavingOther::where('it_savings_id',$ItSavingObj->id)->where('type','other_multiple_deductions')->pluck('value','title')->toArray();
            if(count($data)>0)
            {
                $array['name']=[];
                $array['amount']=[];
                foreach($data as $key => $value){
                    $array['name'][]=$key;
                $array['amount'][]=$value;
                }
                $otherData['other_multiple_deductions'] = $array;
            }
        }
        return $otherData;
    }

    public static function getItSavingTransactionData($itSavingId)
    {
        $data = [];
        $ItSavingObj = ItSaving::find($itSavingId);
        if (!$ItSavingObj) {
            return false;
        }
        $monthObj = Month::where('status', 'in_progress')->where('financial_year_id', $ItSavingObj->financial_year_id)->orderBy('year','ASC')->orderBy('month','ASC')->first();
        $pfEmployee = AppraisalComponentType::getIdByKey('pf-employee');
        $pfEmployeeAmount = 0;
        $vpfAmount = 0;
        $pfEmployeeAmount = Transaction::getPaidTillDate($ItSavingObj->user_id, $ItSavingObj->financial_year_id, 'App\Models\Appraisal\AppraisalComponentType', $pfEmployee);
        $vpfAmount = Transaction::getPaidTillDate($ItSavingObj->user_id, $ItSavingObj->financial_year_id, 'App\Models\Admin\VpfDeduction');
       if($monthObj->status == "in_progress"){
            $appraisalList = AppraisalService::getCurrentMonthAppraisal($ItSavingObj->user_id, $monthObj->id);
            if (!$appraisalList == null || isset($appraisalList[0])) {
                $currData = AppraisalService::generateCurrentData($appraisalList, $monthObj->id, $ItSavingObj->user_id);
                $pfEmployeeAmount += $currData[$pfEmployee];
            }
            $vpfAmount += VpfDeduction::getCurrMonthVpf($monthObj->id, $ItSavingObj->user_id);
        }

        $data['vpfAmount'] = $vpfAmount;
        $data['pfEmployeeAmount'] = $pfEmployeeAmount;
        return $data;
    }


    
}