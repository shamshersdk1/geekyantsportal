<?php
namespace App\Services\Payroll;

use App\Models\Admin\FinancialYear;
use App\Models\Appraisal\Appraisal;
use App\Models\Appraisal\AppraisalBonusType;
use App\Models\Appraisal\AppraisalComponent;
use App\Models\Audited\FixedBonus\FixedBonus;
use App\Models\Audited\VariablePay\VariablePay;
use App\Models\Month;
use App\Models\MonthSetting;
use App\Services\Leave\UserLeaveService;
use OwenIt\Auditing\Models\Audit;
use App\Models\Appraisal\AppraisalBonus;
use App\Models\User;

class AppraisalService
{
    public static function getCurrentMonthAppraisal($userId, $monthId)
    {
        $response = [];
        $response['status'] = true;
        $response['errors'] = "";

        $monthObj = Month::find($monthId);
        if (!$monthObj) {
            $response['status'] = false;
            $response['errors'] = "Month not found!";
            return $response;
        }

        $firstDay = $monthObj->getFirstDay();
        $lastDay = $monthObj->getLastDay();

        $appraisalList = [];

        $appraisalObj = Appraisal::where('user_id', $userId)->where('effective_date', '>=', $firstDay)->where('effective_date', '<=', $lastDay)->orderBy('effective_date', 'ASC')->get();
        if ($appraisalObj == null || !isset($appraisalObj[0])) {
            $appraisalList = Appraisal::where('user_id', $userId)->where('effective_date', '<', $firstDay)->orderBy('effective_date', 'DESC')->limit(1)->get();
        } elseif ($appraisalObj[0]->effective_date != $firstDay) {
            $appraisalPrevObj = Appraisal::where('user_id', $userId)->where('effective_date', '<', $appraisalObj[0]->effective_date)->orderBy('effective_date', 'DESC')->limit(1)->get();
            $appraisalList = $appraisalPrevObj->merge($appraisalObj);
        } else {
            $appraisalList = $appraisalObj;
        }

        return $appraisalList;
    }

    public static function generateCurrentData($appraisalList, $monthId, $userId)
    {
        $data = [];

        $monthlyGross = [];
        $monthlyGrossSalary = 0;

        $monthObj = Month::find($monthId);

        $firstDay = $monthObj->getFirstDay();
        $lastDay = $monthObj->getLastDay();

        $totalBonus = 0;

        foreach ($appraisalList as $key => $appraisal) {
            //getting start date of appraisal
            if ($appraisal->effective_date < $firstDay) {
                $data['start_date'][$appraisal->id] = $firstDay;
            } else {
                $data['start_date'][$appraisal->id] = $appraisal->effective_date;
            }
            //getting end date of appraisal
            if ($appraisalList[$key + 1] == null || !isset($appraisalList[$key + 1])) {
                $data['end_date'][$appraisal->id] = $lastDay;
            } else {
                $dt = date('Y-m-d', strtotime($appraisalList[$key + 1]->effective_date . ' -1 day'));
                $data['end_date'][$appraisal->id] = $dt;
            }
            //no of working days in appraisal
            $data['no_of_working'][$appraisal->id] = UserLeaveService::calculateWorkingDays($data['start_date'][$appraisal->id], $data['end_date'][$appraisal->id], $userId);
            $noOfWorking += $data['no_of_working'][$appraisal->id];

            foreach ($appraisal->appraisalComponent as $component) {
                $value = ($component->value / $monthObj->getWorkingDays()) * $data['no_of_working'][$appraisal->id];
                // $data[$monthObj->id][$appraisal->id][$component->appraisalComponentType->code] = $value;
                $monthlyGross[$component->appraisalComponentType->id] += $value;
                if ($component->appraisalComponentType->code == 'basic' || $component->appraisalComponentType->code == 'lta' || $component->appraisalComponentType->code == 'hra' || $component->appraisalComponentType->code == 'car-allowance' || $component->appraisalComponentType->code == 'food-allowance' || $component->appraisalComponentType->code == 'special-allowance' || $component->appraisalComponentType->code == 'stipend') {
                    $monthlyGrossSalary += $value;
                }
            }

            // foreach ($appraisal->appraisalBonus as $bonus) {
            //     if ($bonus->value_date != null) {
            //         if ($bonus->value_date >= $firstDay && $bonus->value_date <= $lastDay) {
            //             // $value = $bonus->value;
            //             //change here value
            //             $monthlyGross['appraisal_bonus'][$bonus->appraisalBonusType->id] += $value;
            //             $totalBonus += $value;
            //         }
            //     } else {
            //         // $value = $bonus->value / 12;
            //         //change here
            //         $monthlyGross['appraisal_bonus'][$bonus->appraisalBonusType->id] += $value;
            //         $totalBonus += $value;
            //     }
            // }
        }

        $variableBonusSetting = MonthSetting::where('month_id', $monthId)->where('key', 'variable-pay')->first();
        if ($variableBonusSetting && $variableBonusSetting->value == 'locked') {

            $variableAppraisalBonuses = VariablePay::where('user_id', $userId)->where('month_id', $monthId)->get();

            if ($variableAppraisalBonuses && isset($variableAppraisalBonuses)) {
                foreach ($variableAppraisalBonuses as $appraisalBonus) {

                    foreach ($appraisalBonus->lastMonthPayComponents as $bonus) {
                        $type = AppraisalBonusType::where('code', $bonus->key)->first();
                        if ($type) {
                            $monthlyGross['appraisal_bonus'][$type->id] += $bonus->value;
                            $totalBonus += $bonus->value;
                        }
                    }
                }
            }
        }

        $fixedBonusSetting = MonthSetting::where('month_id', $monthId)->where('key', 'fixed-bonus')->first();
        if ($fixedBonusSetting && $fixedBonusSetting->value == 'locked') {
            $fixedAppraisalBonuses = FixedBonus::where('user_id', $userId)->where('month_id', $monthId)->get();
            if ($fixedAppraisalBonuses && isset($fixedAppraisalBonuses)) {
                foreach ($fixedAppraisalBonuses as $appraisalBonus) {
                    foreach ($appraisalBonus->monthlyDeductionComponents as $bonus) {
                        $type = AppraisalBonusType::where('code', $bonus->key)->first();
                        if ($type) {
                            $monthlyGross['appraisal_bonus'][$type->id] += $bonus->value;
                            $totalBonus += $bonus->value;
                        }
                    }
                }
            }
        }

        $monthlyGross['no_of_working'] = $noOfWorking;
        $monthlyGross['monthly_gross_salary'] = $monthlyGrossSalary;
        $monthlyGross['monthly_bonus'] = $totalBonus;
        // $data['monthly_gross_salary'] = $monthlyGrossSalary;
        return $monthlyGross;
    }

    public static function calculateFutureAppraisal($appraisalId, $monthId, $userId)
    {
        $response = [];
        $response['status'] = true;
        $response['errors'] = "";

        $monthObj = Month::find($monthId);
        if (!$monthObj) {
            $response['status'] = false;
            $response['errors'] = "Month not found!";
            return $response;
        }

        $monthList = [];
        // if ($monthObj->month == 1 || $monthObj->month == 2) {
        //     $monthList = Month::where('year', $monthObj->year)->where('month', '>', $monthObj->month)->where('month', '<', 4)->orderBy('month', 'ASC')->get();
        // } else {
        //     $currMonthList = Month::where('year', $monthObj->year)->where('month', '>', $monthObj->month)->orderBy('month', 'ASC')->get();
        //     $nextMonthList = Month::where('year', $monthObj->year + 1)->where('month', '<=', 3)->orderBy('month', 'ASC')->orderBy('month', 'ASC')->get();

        //     $monthList = $currMonthList->merge($nextMonthList);
        // }
        $monthList = FinancialYear::getUpcomingMonth($monthObj->financial_year_id, $monthObj->month);
  

        $data = [];

        $appraisal = Appraisal::find($appraisalId);
        $annualGross = 0;

        foreach ($monthList as $month) {
            $monthlyGrossSalary = 0;
            $totalBonus = 0;

            $firstDate = date('Y-m-01', strtotime($month->year . '-' . $month->month . '-01'));
            $last = date('Y-m-d', strtotime($month->year . '-' . $month->month . '-01'));
            $lastDate = date("Y-m-t", strtotime($last));

            $data['start_date'][$month->month] = $firstDate;
            $data['end_date'][$month->month] = $lastDate;
            // print_r($firstDate);
            // print_r($lastDate);
            $data['no_of_working'][$month->month] = UserLeaveService::calculateWorkingDays($data['start_date'][$month->month], $data['end_date'][$month->month], $userId);

            foreach ($appraisal->appraisalComponent as $component) {
                $value = $component->value;
                $data[$month->month][$component->appraisalComponentType->id] = $value;

                if ($component->appraisalComponentType->code == 'basic' || $component->appraisalComponentType->code == 'lta' || $component->appraisalComponentType->code == 'hra' || $component->appraisalComponentType->code == 'car-allowance' || $component->appraisalComponentType->code == 'food-allowance' || $component->appraisalComponentType->code == 'special-allowance' || $component->appraisalComponentType->code == 'stipend') {
                    $monthlyGrossSalary += $value;
                }
            }

            foreach ($appraisal->appraisalBonus as $bonus) {
                if ($bonus->value_date != null) {
                    if ($bonus->value_date >= $firstDate && $bonus->value_date <= $lastDate) {
                        $value = $bonus->value;
                        $data[$month->month]['appraisal_bonus'][$bonus->appraisalBonusType->id] += $value;
                        $totalBonus += $value;
                    }
                }
                // else {
                //     $value = $bonus->value;
                //     $data[$month->month]['appraisal_bonus'][$bonus->appraisalBonusType->id] += $value;
                //     $totalBonus += $value;
                // }
            }

            $data[$month->month]['monthly_gross_salary'] = $monthlyGrossSalary;
            $data[$month->month]['monthly_bonus'] = $totalBonus;

            $annualGross += $data[$month->month]['monthly_gross_salary'];
        }
        $data['annual_gross_salary'] = $annualGross;

        return $data;
    }

    public static function getAudits()
    {
        $fromDate = date('Y-m-d', strtotime("-1 days"));
        $toDate = date('Y-m-d');
        $appraisalAudits = Audit::orderBy('created_at')->whereBetween('created_at', [$fromDate.' 00:00:00', $toDate.' 06:00:00'])->whereIn('auditable_type',['App\Models\Appraisal\Appraisal','App\Models\Appraisal\AppraisalComponent','App\Models\Appraisal\AppraisalBonus'])->get();
        $data = [];
        foreach($appraisalAudits as $audit)
        {
            $userId = NULL;
            if(isset($audit->auditable->user_id)){
                $userId = $audit->auditable->user_id;
            } else {
                $userId = $audit->auditable->appraisal->user_id;
            }
            $data[$userId][] = $audit;
        }
        return $data;
    }

}
