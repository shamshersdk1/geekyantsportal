<?php namespace App\Services;

use App\Models\Admin\Calendar as Holiday;
use App\Models\BonusRequest;
use App\Models\Month;

class Calendar
{

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->naviHref = htmlentities($_SERVER['PHP_SELF']);
    }

    /********************* PROPERTY ********************/
    private $dayLabels = array("Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun");

    private $currentYear = 0;

    private $currentMonth = 0;

    private $currentDay = 0;

    private $currentDate = null;

    private $daysInMonth = 0;

    private $calendarHeader = '';

    private $next = false;

    private $previous = true;

    private $naviHref = null;

    /********************* PUBLIC **********************/

    /**
     * print out the calendar
     */
    public function show()
    {
        //$year  = "2018";

        // /$month =     12;

        // if(null==$year&&isset($_GET['year'])){

        //     $year = $_GET['year'];

        // }else if(null==$year){

        $year = date("Y", time());

        // }

        // if(null==$month&&isset($_GET['month'])){

        //     $month = $_GET['month'];

        // }else if(null==$month){

        $month = date("m", time());

        // }

        $this->currentYear = $year;
        echo "<pre>";
        $this->currentMonth = $month;
        print_r($this->currentMonth);
        $this->daysInMonth = $this->_daysInMonth($month, $year);
        print_r($this->_createNavi());

        //$days = $this->_createLabels();
        //$weeksInMonth = $this->_weeksInMonth($month,$year);
        $this->data($month, $year);
        die;

        // print_r($days);
        // print_r($weeksInMonth);
        //die;

        $content = '<div id="calendar">' .
        '<div class="box">' .
        $this->_createNavi() .
        '</div>' .
        '<div class="box-content">' .
        '<ul class="label">' . $this->_createLabels() . '</ul>';
        $content .= '<div class="clear"></div>';
        $content .= '<ul class="dates">';

        $weeksInMonth = $this->_weeksInMonth($month, $year);
        // Create weeks in a month
        for ($i = 0; $i < $weeksInMonth; $i++) {

            //Create days in a week
            for ($j = 1; $j <= 7; $j++) {
                $dat = $this->_showDay($i * 7 + $j);
                print_r($dat);
                //die;
                $content .= $this->_showDay($i * 7 + $j);
            }
        }

        $content .= '</ul>';

        $content .= '<div class="clear"></div>';

        $content .= '</div>';

        $content .= '</div>';
        return $content;
    }

    /********************* PRIVATE **********************/
    /**
     * create the li element for ul
     */
    private function _showDay($cellNumber)
    {

        if ($this->currentDay == 0) {

            $firstDayOfTheWeek = date('N', strtotime($this->currentYear . '-' . $this->currentMonth . '-01'));

            if (intval($cellNumber) == intval($firstDayOfTheWeek)) {

                $this->currentDay = 1;

            }
        }

        if (($this->currentDay != 0) && ($this->currentDay <= $this->daysInMonth)) {

            $this->currentDate = date('Y-m-d', strtotime($this->currentYear . '-' . $this->currentMonth . '-' . ($this->currentDay)));

            $cellContent = $this->currentDay;

            $this->currentDay++;

        } else {

            $this->currentDate = null;

            $cellContent = null;
        }
        return $cellContent;

        return '<li id="li-' . $this->currentDate . '" class="' . ($cellNumber % 7 == 1 ? ' start ' : ($cellNumber % 7 == 0 ? ' end ' : ' ')) .
            ($cellContent == null ? 'mask' : '') . '">' . $cellContent . '</li>';
    }

    /**
     * create navigation
     */
    private function _createNavi()
    {

        $nextMonth = $this->currentMonth == 12 ? 1 : intval($this->currentMonth) + 1;

        $nextYear = $this->currentMonth == 12 ? intval($this->currentYear) + 1 : $this->currentYear;

        $preMonth = $this->currentMonth == 1 ? 12 : intval($this->currentMonth) - 1;

        $preYear = $this->currentMonth == 1 ? intval($this->currentYear) - 1 : $this->currentYear;

        return
        '<div class="header">' .
        '<a class="prev" href="' . $this->naviHref . '?month=' . sprintf('%02d', $preMonth) . '&year=' . $preYear . '">Prev</a>' .
        '<span class="title">' . date('Y M', strtotime($this->currentYear . '-' . $this->currentMonth . '-1')) . '</span>' .
        '<a class="next" href="' . $this->naviHref . '?month=' . sprintf("%02d", $nextMonth) . '&year=' . $nextYear . '">Next</a>' .
            '</div>';
    }

    /**
     * create calendar week labels
     */
    private function _createLabels()
    {

        $content = '';
        $days = [];
        foreach ($this->dayLabels as $index => $label) {
            $days[] = $label;
            // echo "<br/> string : ".$label;

            $content .= '<li class="' . ($label == 6 ? 'end title' : 'start title') . ' title">' . $label . '</li>';

        }
        return $days;
        return $content;
    }

    public function getCalendar($month, $year)
    {
        $data = [];
        $this->currentYear = $year;
        $this->currentMonth = $month;
        $this->daysInMonth = $this->_daysInMonth($month, $year);
        $labels = $this->_createLabels();
        $weeksInMonth = $this->_weeksInMonth($month, $year);
        $this->calendarHeader = $this->_calendarHeader($month, $year);
        $this->next = $this->_next($month, $year);
        $this->previous = $this->_previous($month, $year);
        $locked = BonusRequest::isMonthLocked($month, $year);
        $nextMonth = $month + 1;
        $nextYear = $year;
        if ($nextMonth > 12) {
            $nextMonth = 1;
            $nextYear = $nextYear + 1;
        }
        $nextLocked = BonusRequest::isMonthLocked($nextMonth, $nextYear);
        $days = [];
        for ($i = 0; $i < $weeksInMonth; $i++) {

            //Create days in a week
            for ($j = 1; $j <= 7; $j++) {
                $day = $this->_showDay($i * 7 + $j);
                $date = "";
                if ($day) {
                    $dt = $year . '-' . $month . '-' . $day;
                    $date = [
                        'date' => date('Y-m-d', strtotime($year . '-' . $month . '-' . $day)),
                        'day' => $day,
                        'is_weekend' => (date('N', strtotime($year . '-' . $month . '-' . $day)) >= 6) ? 'true' : 'false',
                        'is_holiday' => (Holiday::whereDate('date', $dt)->count() > 0) ? 'true' : 'false',
                        'holiday' => Holiday::whereDate('date', $dt)->first(),
                    ];
                }
                $days[] = $date;

                //$content.=$this->_showDay($i*7+$j);
            }
        }
        $data['is_next_locked'] = $nextLocked;
        $data['is_locked'] = $locked;
        $data['calendarHeader'] = $this->calendarHeader;
        $data['previous'] = $this->previous;
        $data['next'] = $this->next;
        $data['weeksInMonth'] = $weeksInMonth;
        $data['labels'] = $labels;
        $data['days'] = $days;
        return $data;
    }

    /**
     * calculate number of weeks in a particular month
     */
    private function _weeksInMonth($month = null, $year = null)
    {

        if (null == ($year)) {
            $year = date("Y", time());
        }

        if (null == ($month)) {
            $month = date("m", time());
        }

        // find number of days in this month
        $daysInMonths = $this->_daysInMonth($month, $year);

        $numOfweeks = ($daysInMonths % 7 == 0 ? 0 : 1) + intval($daysInMonths / 7);

        $monthEndingDay = date('N', strtotime($year . '-' . $month . '-' . $daysInMonths));

        $monthStartDay = date('N', strtotime($year . '-' . $month . '-01'));

        if ($monthEndingDay < $monthStartDay) {

            $numOfweeks++;

        }

        return $numOfweeks;
    }

    /**
     * calculate number of days in a particular month
     */
    private function _daysInMonth($month = null, $year = null)
    {

        if (null == ($year)) {
            $year = date("Y", time());
        }

        if (null == ($month)) {
            $month = date("m", time());
        }

        return date('t', strtotime($year . '-' . $month . '-01'));
    }

    private function _calendarHeader($month = null, $year = null)
    {
        if (null == ($year)) {
            $year = date("Y", time());
        }

        if (null == ($month)) {
            $month = date("m", time());
        }

        $monthString = date('F', strtotime($year . '-' . $month . '-01'));
        return ($monthString . " " . $year);
    }

    private function _next($month = null, $year = null)
    {
        if (null == ($year)) {
            $year = date("Y", time());
        }
        if (null == ($month)) {
            $month = date("m", time());
        }
        $monthObj = Month::where('month', $month)->where('year', $year)->first();
        if (!$monthObj) {
            return false;
        }

        if ($monthObj->next()) {
            return true;
        }

        return false;

    }

    private function _previous($month = null, $year = null)
    {
        if (null == ($year)) {
            $year = date("Y", time());
        }
        if (null == ($month)) {
            $month = date("m", time());
        }
        $monthObj = Month::where('month', $month)->where('year', $year)->first();
        if (!$monthObj) {
            return false;
        }

        if ($monthObj->prevoius()) {
            return true;
        }

        return false;
    }
}
