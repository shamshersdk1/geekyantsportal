<?php namespace App\Services;

use App\Jobs\MessagingApp\PostMessageChannelJob;
use App\Models\Admin\Calendar;
use App\Models\Admin\Leave;
use App\Models\Attendance\UserAttendanceRegister;
use App\Models\User;
use App\Models\UserAttendance;

class AttendanceReportService
{

    public static function getDetails($start_date, $end_date)
    {

        $max = date_diff(date_create($start_date), date_create($end_date))->format('%a') + 1;
        $date = $start_date;
        $result_data = [];
        $result = [];
        $holidays = Calendar::whereDate('date', '>=', $start_date)->whereDate('date', '<=', $end_date)->pluck('date')->toArray();
        $active_users = User::where('is_active', 1)->orderBy('employee_id')->get();

        foreach ($active_users as $user) {
            $result_row_obj = [];
            $result_row_obj[] = $user->employee_id;
            $result_row_obj[] = $user->name;
            $date = $start_date;
            $sl_count = 0;
            $pl_count = 0;
            $other_count = 0;
            $dates = [];
            $code = [];
            $dates[] = 0;
            $dates[] = 0;
            $code[] = 0;
            $code[] = 0;
            for ($i = 0; $i < $max; $i++) {
                $is_holiday = false;
                $dates[] = date('D, j M', strtotime($date));
                if (date('N', strtotime($date)) > 5) {
                    $code[] = 1;
                    $is_holiday = true;
                } else {
                    if (in_array($date, $holidays)) {
                        $code[] = 1;
                        $is_holiday = true;
                    } else {
                        $code[] = 0;
                    }
                }
                if ($is_holiday) {
                    $is_user_present = 'H';
                } else {
                    $leave = Leave::isOnLeaveToday($user->id, $date);
                    if (empty($leave)) {
                        $is_user_present = 'P';
                    } else {
                        if ($leave->isLeaveType('sick')) {
                            $is_user_present = 'SL';
                            if ($leave->half) {
                                $is_user_present = 'SL-H';
                                $sl_count = $sl_count + 0.5;
                            } else {
                                $sl_count++;
                            }
                        } else if ($leave->isLeaveType('paid')) {
                            $is_user_present = 'PL';
                            if ($leave->half) {
                                $is_user_present = 'PL-H';
                                $pl_count = $pl_count + 0.5;
                            } else {
                                $pl_count++;
                            }
                        } else {
                            $is_user_present = $leave->leaveType->title;
                            $other_count++;
                        }
                    }
                }
                $result_row_obj[] = $is_user_present;
                $date = date('Y-m-d', strtotime($date . ' +1 day'));
            }
            $result_row_obj[] = $sl_count;
            $result_row_obj[] = $pl_count;
            $result_row_obj[] = $other_count;
            $code[] = 0;
            $code[] = 0;
            $result_data[] = $result_row_obj;
        }
        $result['dates'] = $dates;
        $result['code'] = $code;
        $result['result_data'] = $result_data;
        return $result;
    }
    public static function getTemplate()
    {

        $attachment['title'] = "List of user present today";
        $attachment['title_link'] = "admin/attendance-overview";
        $attachment['ts'] = strtotime(date('Y-m-d H:i:s'));
        $attachment['fallback'] = "List of user present today";
        $attachment['pretext'] = "List of user present today";
        return $attachment;
    }
    public static function updateActiveUserStatusOnSlack($template_path, $userIds)
    {
        $attachments = self::getTemplate();

        $channelId = \Config::get('messaging-app.user_attendance_channel');
        $users = User::whereIn('id', $userIds)->get();
        $template = "Attendance Report";
        //$template = "*".ucwords($userObj->name). "*" ." is present today";
        //$template = View::make($template_path, compact('users'))->render();
        $text = '';
        foreach ($users as $user) {
            $text .= $user->employee_id . ' | ' . $user->name . "\n";
        }
        $attachments['text'] = $text;
        $job = (new PostMessageChannelJob($channelId, $template, $attachments));
        dispatch($job);
        // foreach ($userIds as $userId) {
        //     $userObj = User::find($userId);
        //     if($userObj){

        //     }
        // }

    }
    public static function updateUserStatusOnSlack($userIds, $offset = 0, $limit = 5)
    {
        $channelId = \Config::get('messaging-app.user_attendance_channel');
        $template = "User Attendance";

        $users = User::whereIn('id', $userIds)->take($limit)->offset($offset)->take(45)->get();
        if (count($users) > 0) {
            foreach ($users as $user) {
                $temp = [];
                $temp[] =
                    [
                    "type" => "section",
                    "block_id" => $user->slack_user->slack_id,
                    "text" => [
                        "type" => "mrkdwn",
                        "text" => "*" . $user->name . "* is not yet present in the office",
                    ],
                    "accessory" => [
                        "action_id" => "user_attendance_action",
                        "type" => "static_select",
                        "placeholder" => [
                            "type" => "plain_text",
                            "emoji" => true,
                            "text" => "Unknown Status",
                        ],
                        "options" => [
                            [
                                "text" => [
                                    "type" => "plain_text",
                                    "emoji" => true,
                                    "text" => "On Site",
                                ],
                                "value" => "is_onsite",
                            ],
                            [
                                "text" => [
                                    "type" => "plain_text",
                                    "emoji" => true,
                                    "text" => "Working from Home",
                                ],
                                "value" => "is_wfh",
                            ],
                            [
                                "text" => [
                                    "type" => "plain_text",
                                    "emoji" => true,
                                    "text" => "On Site Meeting",
                                ],
                                "value" => "is_onsite_meeting",
                            ],
                            [
                                "text" => [
                                    "type" => "plain_text",
                                    "emoji" => true,
                                    "text" => "Ignored",
                                ],
                                "value" => "is_ignored",
                            ],
                        ],
                    ],
                ];
                //$arr[] = $temp;
                $attachments['blocks'] = json_encode($temp);
                $job = (new PostMessageChannelJob($channelId, $template, $attachments));
                dispatch($job);
            }

        }
        //$arr[] = ["type" => 'divider'];

    }
    public static function generateAttendanceRegister($date)
    {

        $prevDate = date('Y-m-d', strtotime($date . ' -1 day'));

        // Generate the attendance register form prevoius
        $dataPreviousDay = UserAttendance::getReport($prevDate);

        // List of ppl present today
        // check the index $viewData['active_users];
        if (isset($dataPreviousDay['active_users']) && count($dataPreviousDay['active_users']) > 0) {
            foreach ($dataPreviousDay['active_users'] as $activeUser) {
                $userAttendance = $activeUser['UserAttendance'];
                $copy = $userAttendance->toArray();
                $obj = UserAttendanceRegister::where('date', $prevDate)->where('user_id', $copy['user_id'])->first();
                if (!$obj) {
                    $userAttendance = $activeUser['UserAttendance'];
                    $status = 'present_in_office';
                    $copy['out_time'] = !empty($copy['updated_at']) ? $copy['updated_at'] : null;
                    if ($copy['is_onsite']) {
                        $status = 'present_onsite';
                    } elseif ($copy['is_wfh']) {
                        $status = 'present_wfh';
                    } elseif ($copy['is_onsite_meeting']) {
                        $status = 'present_meeting';
                    }
                    $copy['status'] = $status;
                    $registerObj = new UserAttendanceRegister;
                    $registerObj = $registerObj->fill($copy);
                    if (!$registerObj->save()) {
                        \Log::info('ERROR saving register' . json_encode($registerObj->getErrors()));
                    }
                }
            }
        }

        // on leave users register
        if (isset($dataPreviousDay['users_on_leave']) && count($dataPreviousDay['users_on_leave']) > 0) {
            foreach ($dataPreviousDay['users_on_leave'] as $onLeaveUser) {
                $data['date'] = $prevDate;
                $data['user_id'] = $onLeaveUser->user_id;
                $data['status'] = 'on_leave';
                $obj = UserAttendanceRegister::where('date', $prevDate)->where('user_id', $onLeaveUser->user_id)->first();
                if ($obj) {
                    $obj->status = 'present_on_leave';
                    $obj->save();
                } else {
                    $registerObj = new UserAttendanceRegister;
                    $registerObj = $registerObj->fill($data);
                    if (!$registerObj->save()) {
                        \Log::info('ERROR saving register' . json_encode($registerObj->getErrors()));
                    }
                }
            }
        }

        // assuming the script will run at 5:55 am
        // check for overtime on next days after 00:00am

        $dataCurrentDay = UserAttendance::getReport($date);
        if (isset($dataCurrentDay['active_users']) && count($dataCurrentDay['active_users']) > 0) {
            foreach ($dataCurrentDay['active_users'] as $activeUser) {
                $userAttendance = $activeUser['UserAttendance'];
                $obj = UserAttendanceRegister::where('date', $prevDate)->where('user_id', $userAttendance->user_id)->first();
                if ($obj) {
                    $obj->tick_count += $userAttendance->tick_count;
                    $obj->active_count += $userAttendance->active_count;
                    $obj->out_time = $userAttendance->updated_at;
                    $obj->save();
                } else {
                    $copy = $userAttendance->toArray();
                    $copy['out_time'] = !empty($copy['updated_at']) ? $copy['updated_at'] : null;
                    $copy['date'] = $prevDate;
                    $status = 'present_in_office';
                    if ($copy['is_onsite']) {
                        $status = 'present_onsite';
                    } elseif ($copy['is_wfh']) {
                        $status = 'present_wfh';
                    } elseif ($copy['is_onsite_meeting']) {
                        $status = 'present_meeting';
                    }
                    $copy['status'] = $status;
                    $registerObj = new UserAttendanceRegister;
                    $registerObj = $registerObj->fill($copy);
                    if (!$registerObj->save()) {
                        \Log::info('ERROR saving register' . json_encode($registerObj->getErrors()));
                    }
                }
            }
        }
    }
}
