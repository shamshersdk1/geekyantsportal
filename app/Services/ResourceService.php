<?php namespace App\Services;

use Validator;
use App\Models\Admin\Project;
use App\Models\Admin\ProjectResource;
use App\Models\Timesheet;
use App\Models\TimesheetDetail;
use App\Models\Admin\Calendar;

use App\Models\Admin\ProjectSprints;

use DB;
use Input;
use Redirect;
use App\Models\User;
use Auth;

class ResourceService {

	public static function resourceAllocationReport($start_date, $end_date, $users, $filter, $projects, $input_projects_flag)
	{
		
		$resourceDataList = [];
		$resourceList = User::whereIn('id',$users)->get();
		$dates = [];
		$result = [];
		$is_holiday_data = [];
		$max= date_diff(date_create($start_date), date_create($end_date))->format('%a');
		$date = $start_date;
		$project_array = [];
		
		$len = count($projects);
		if ( !empty($projects) ){
			for ($i=0;$i<$len; $i++ )
			{
				$project_array[] = $projects[$i];
			}
		}
		
		
		for($i=0;$i<=$max;$i++)
		{
			if(date('N', strtotime($date)) > 5 || Calendar::whereDate('date', $date)->count() > 0) 
			{
				$is_holiday_data[] = 1;
			} else {
				$is_holiday_data[] = 0;
			}
			$resourceNameList = [];
			$dailyResourceList =[];
			foreach($resourceList as $resource)
			{
				$projects = '';
				$is_holiday = false;
				$on_leave = false;
				$currentProjectsEndDated = [];
				$currentProjectsWithNull = [];
				$currentProjects = [];
				$is_free_project = false;
				
				if(date('N', strtotime($date)) > 5 || Calendar::whereDate('date', $date)->count() > 0) {
					$is_holiday = true;
				}
				if($resource->leaves()->where('start_date', '<=', $date)
									  ->where('end_date', '>=', $date)
									  ->where('status','=','approved')
									  ->count() > 0) 
				{
					$on_leave = true;
				}
				$currentProjectsEndDated = ProjectResource::with('project')
							->where('user_id',$resource['id'])
							->where('start_date','<=',$date)
							->where('end_date','>=',$date)
							->whereIn('project_id',$project_array)->get();
				
				$currentProjectsWithNull = ProjectResource::with('project')
									->where('user_id',$resource['id'])
									->where('start_date','<=',$date)
									->whereNull('end_date')
									->whereIn('project_id',$project_array)->get();
				
				$currentProjects = $currentProjectsEndDated->merge($currentProjectsWithNull);
				
				if ( count($currentProjects) > 0 ) {
					foreach ( $currentProjects as $currentProject ){
						if ( $currentProject->project->status == 'free' )
						{
							$is_free_project = true;
						}
						$projectsArray[] = $currentProject->project->project_name;
						$projects = implode($projectsArray,', <br> ');
					}
				} 
				if (count($currentProjects) < 1 ){
					
				}
				
				$resourceData['date'] = $date;
				$resourceData['projects'] = $projects;
				// $resourceData['user_name'] = $resource['name'];
				$resourceData['is_holiday'] = $is_holiday;
				$resourceData['on_leave'] = $on_leave;
				$resourceData['is_free_project'] = $is_free_project;
				$dailyResourceList[] = $resourceData;
				$resourceNameList[] = $resource->name;
				$projectsArray = [];
				
			}
			
			$resourceDataList[] = $dailyResourceList;
			$dates[]= date('D, j M', strtotime($date));
			$date = date('Y-m-d', strtotime($date . ' +1 day'));
		}
		
		
			$result['dates'] = $dates;
			$result['holiday_data'] = $is_holiday_data;
			$result['resource_name'] = $resourceNameList;
			$result['resourceDataList'] = $resourceDataList;
			
			$counts = [];
			$free_count = [];
			
			for ($i=0;$i<count($result['resource_name']);$i++ )
			{
				$counts[$i] = 0;
				$free_count[$i] = 0;
			}
			if ( $input_projects_flag )
			{   
				foreach( $result['resourceDataList'] as $index => $resource_data )
				{
					for ($i=0;$i<count($resource_data);$i++ )
					{
						if ($resource_data[$i]['projects'] != '' )
						{
							$counts[$i] = $counts[$i] + 1;
						} else {
							$counts[$i] = $counts[$i] + 0;
						}
					}
				}
			}
			if ( $input_projects_flag )
			{
				for ( $i=0; $i < count($counts); $i++ )
				{
					if ($counts[$i] == 0)
					{
						unset($result['resource_name'][$i]);
						foreach ( $result['resourceDataList'] as $resource_data )
						{
							unset($resource_data[$i]);
						}
					}
				}

			}
			
			$max_working_day = 0;
			
			if ( $filter == 'free' )
			{
				for($i=0;$i<=$max;$i++)
				{
					if ( $result['holiday_data'][$i] != 1 )
					{
						$max_working_day = $max_working_day + 1;
						$resource_data = $result['resourceDataList'][$i];
						for ($j=0;$j<count($resource_data);$j++ )
						{
							if ($resource_data[$j]['projects'] != '' )
							{
								$free_count[$j] = $free_count[$j] + 1;
							} else {
								$free_count[$j] = $free_count[$j] + 0;
							}
						}
					}	
				}
				
				for ( $k=0; $k < count($free_count); $k++ )
				{
					
					if ($free_count[$k] == $max_working_day)
					{
						
						unset($result['resource_name'][$k]);
						foreach ( $result['resourceDataList'] as $resource_data )
						{
							unset($resource_data[$k]);
						}
					}
				}

			}
			return $result;
		
		
	}

	public static function resourceAllocationReportWithHours($start_date, $end_date, $users, $projects) 
	{
		$resourceDataList = [];
		$resourceList = User::whereIn('id',$users)->get();
		$dates = [];
		$result = [];
		$max= date_diff(date_create($start_date), date_create($end_date))->format('%a');
		$date = $start_date;
		$project_array = [];

		$len = count($projects);
		if ( !empty($projects) ){
			for ($i=0;$i<$len; $i++ )
			{
				$project_array[] = $projects[$i];
			}
		}

		for($i=0;$i<=$max;$i++)
		{
			$resourceNameList = [];
			$dailyResourceList =[];
			foreach($resourceList as $resource)
			{
				$projects = '';
				$on_leave = false;

				$currentProjectsEndDated = [];
				$currentProjectsWithNull = [];
				$currentProjects = [];
				
				if($resource->leaves()->where('start_date', '<=', $date)
									  ->where('end_date', '>=', $date)
									  ->where('status','=','approved')
									  ->count() > 0) 
				{
					$on_leave = true;
				}
				
				$currentProjectsEndDated = ProjectResource::with('project')
							->where('user_id',$resource['id'])
							->where('start_date','<=',$date)
							->where('end_date','>=',$date)
							->whereIn('project_id',$project_array)->get();							

				$currentProjectsWithNull = ProjectResource::with('project')
									->where('user_id',$resource['id'])
									->where('start_date','<=',$date)
									->whereNull('end_date')
									->whereIn('project_id',$project_array)->get();
				
				$currentProjects = $currentProjectsEndDated->merge($currentProjectsWithNull);                                                               
				
				if ( count($currentProjects) > 0 ) {
					foreach ( $currentProjects as $currentProject ){
						$projectsArray[] = $currentProject->project->project_name;
						$projects = implode($projectsArray,', <br> ');
					}
				}

				$approved_hours = Timesheet::where('user_id',$resource['id'])
											->where('date','=',$date)
											->sum('approved_hours');

				$timsheetObjs = Timesheet::where('user_id',$resource['id'])
											->where('date','=',$date)->get();
				
				$applied_hours = 0;
				if ( count($timsheetObjs) > 0 ) {
					foreach ( $timsheetObjs as $timsheetObj ){
						$timesheet_detail_objs = TimesheetDetail::where('timesheet_id',$timsheetObj->id)->get();
						if ( count($timesheet_detail_objs) > 0 ) {
							$applied_hours = 0;
							foreach ( $timesheet_detail_objs as $timesheet_detail_obj ){
								$applied_hours = $applied_hours + $timesheet_detail_obj->duration;
							}
						}
					}
				}


				$resourceData['date'] = $date;
				$resourceData['projects'] = $projects;
				$resourceData['approved_hours'] = $approved_hours;
				$resourceData['applied_hours'] = $applied_hours;
				$resourceData['on_leave'] = $on_leave;
				$dailyResourceList[] = $resourceData;
				$resourceNameList[] = $resource->name;
				$projectsArray = [];
			}
			$resourceDataList[] = $dailyResourceList;
			$dates[]= date('D, j M', strtotime($date));
			$date = date('Y-m-d', strtotime($date . ' +1 day'));
		}
		$result['dates'] = $dates;
		$result['resource_name'] = $resourceNameList;
		$result['resourceDataList'] = $resourceDataList;
		return $result;
	}

	public static function csvReportParser($start_date, $end_date, $user_ids, $project_ids)
	{
		$date = $start_date;
		$dates = [];
		$tasks = [];
		$durations = [];
		$total_durations = [];
		while($date <= $end_date)
		{	
			foreach($user_ids as $user_id)
			{
				$total_durations[$user_id] = isset($total_durations[$user_id])? $total_durations[$user_id] : 0;
				$timesheet = Timesheet::with('timesheet_detail')->where('date', $date)
										->where('user_id', $user_id)
										->whereIn('project_id', $project_ids)->first();
				if($timesheet && $timesheet->timesheet_detail) {
					$details = $timesheet->timesheet_detail;
					$task = "";
					$duration = "";
					$daily_duration = 0;
					foreach($details as $timesheet_entry)
					{
						$duration = $duration.$timesheet_entry->duration." + ";
						$task = $task.$timesheet_entry->reason.", ";
						$total_durations[$user_id] += $timesheet_entry->duration;
						$daily_duration += $timesheet_entry->duration;
					}
					$durations[$date][$user_id] = substr($duration, 0, -3)." = ".$daily_duration;
					$tasks[$date][$user_id] = substr($task, 0, -2);
				} else {
					$durations[$date][$user_id] = null;
					$tasks[$date][$user_id] = null;
				}
			}
			$dates[] = ["date" => $date, "duration" => $durations[$date], "task" => $tasks[$date]];
			$date = date("Y-m-d", strtotime($date." +1 day"));
		}
		return ['dates' => $dates, 'durations' => $durations, 'tasks' => $tasks, 'total_durations' => $total_durations];
	}

	public static function csvLogParser($id, $start_date, $end_date)
	{
		$project = Project::with('timesheets')->find($id);
		$timesheets = $project->timesheets()->where('date', '>=', $start_date)
						->where('date', '<=', $end_date)->pluck('id')->toArray();
		$details = TimesheetDetail::with('timesheet')->whereIn('timesheet_id', $timesheets)->orderBy('created_at', 'desc')->get();
		$count = 0;
		$rows = [];
		$head = ["Sl. no", "Name", "Task", "Duration", "Time"];
		foreach($details as $item)
		{
			$rows[] = [++$count, $item->timesheet->user->name, $item->reason, $item->duration." hours", datetime_in_view($item->created_at)];
		}
		return ['head' => $head, "rows" => $rows];
	}
}