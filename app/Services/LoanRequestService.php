<?php namespace App\Services;

use App\Jobs\Loan\LoanInterestJob;
use App\Jobs\MessagingApp\PostMessageChannelJob;
use App\Jobs\MessagingApp\PostMessageUserJob;
use App\Models\Admin\LoanRequest;
use App\Models\Admin\LoanRequestActivity;
use App\Models\Loan;
use App\Models\Month;
use App\Models\Transaction;
use View;

class LoanRequestService
{

    public static function sendReminder($loanId)
    {

        $channelId = \Config::get('messaging-app.hr_channel');
        $loan = LoanRequest::find($loanId);

        if (!$loan) {
            return false;
        }

        $url = config('app.geekyants_portal_url') . "/admin/";

        switch ($loan->status) {
            case 'pending':
                $url .= "pending-loan-requests/" . $loan->id;
                $name = !empty($loan->user->reportingManager->name) ? $loan->user->reportingManager->name : "";
                $nextActionTeam = "Reporting Manager " . "(" . $name . ")";
                break;
            case 'submitted':
                $url .= "submitted-loan-requests/" . $loan->id;
                $nextActionTeam = 'Human Resource Department';
                break;
            case 'review':
                $url .= "reviewed-loan-requests/" . $loan->id;
                $nextActionTeam = 'Management (Pratik)';
                break;
            case 'reconcile':
                $url .= "reconciled-loan-requests/" . $loan->id;
                $nextActionTeam = 'Human Resource Department';
                break;

            default:
                $url .= "loan-applications";
                $nextActionTeam = '';
                break;
        }

        $actions = [["text" => "View Loan Request", "type" => "button", "url" => $url]];
        $attachment = [["text" => "", "fallback" => "Goto the portal to Loan Request", "attachment_type" => "default", "actions" => $actions]];

        $templete = View::make('messaging-app.slack.loan.loan-request-status', compact('loan', 'nextActionTeam'))->render();
        $job = (new PostMessageChannelJob($channelId, $templete, $attachment));
        dispatch($job);
    }

    public static function updateLoanRequestActivities($loan_request_id, $status, $role, $comments)
    {
        $userObj = \Auth::user();
        $loanRequestObj = LoanRequest::find($loan_request_id);
        if ($loanRequestObj && $userObj) {
            $result = LoanRequestActivity::saveActivity($loanRequestObj->id, $status, $role, $userObj->id, $comments);
        }

    }

    public static function updateLoanTable($loan_request_id, $data_array)
    {
        $loanRequestObj = LoanRequest::find($loan_request_id);
        if ($loanRequestObj) {
            $loanObj = new Loan;
            $loanObj->user_id = $loanRequestObj->user_id;
            $loanObj->amount = $loanRequestObj->approved_amount;
            $loanObj->remaining = $loanRequestObj->approved_amount;
            $loanObj->appraisal_bonus_id = $loanRequestObj->appraisal_bonus_id;
            $loanObj->emi = $loanRequestObj->emi;
            if ($loanRequestObj->appraisal_bonus_id != null) {
                $loanObj->emi = $loanRequestObj->amount;
            }
            $loanObj->description = $loanRequestObj->description;
            $loanObj->comments = $loanRequestObj->description;
            $loanObj->emi_start_date = $loanRequestObj->emi_start_date;
            $loanObj->application_date = $loanRequestObj->application_date;
            $loanObj->payment_details = $data_array['payment_detail'];
            $loanObj->payment_mode = $data_array['payment_mode'];
            $loanObj->transaction_id = $data_array['transaction_id'];
            $loanObj->payment_date = $data_array['payment_date'];
            $loanObj->loan_request_id = $loan_request_id;
            $loanObj->status = 'in_progress';
            if ($loanObj->save()) {
                //Entry To Transaction Table
                $currentDate = date("Y-m-d");
                $monthObj = Month::getMonthByDate($currentDate);
                $transactionObj = Transaction::create(['prep_salary_id' => 0, 'month_id' => $monthObj->id, 'financial_year_id' => $monthObj->financial_year_id, 'user_id' => $loanRequestObj->user_id, 'reference_id' => $loanObj->id, 'reference_type' => 'App\Models\Loan', 'amount' => $loanObj->amount, 'type' => 'credit']);
                if ($transactionObj) {
                    $loanObj->transaction_id = $transactionObj->id;
                    $loanObj->save();
                }
                dispatch(new LoanInterestJob($loanObj->user_id));
            }
        }

    }
    public static function getTemplate($loanRequestId)
    {

        $attachment = [];
        $loanRequestObj = LoanRequest::find($loanRequestId);
        $loanAmount = $loanRequestObj->approved_amount;

        if (!$loanRequestObj) {
            return false;
        }
        $attachment['color'] = "good";
        $status = "";
        if ($loanRequestObj->status == "pending") {
            $loanAmount = $loanRequestObj->amount;
            $attachment['title_link'] = "admin/pending-loan-requests/" . $loanRequestObj->id;
            $status = "Waiting for reporting manager.";
        } else if ($loanRequestObj->status == "submitted") {
            $attachment['title_link'] = "admin/submitted-loan-requests/" . $loanRequestObj->id;
            $status = "Waiting for HR.";
        } else if ($loanRequestObj->status == "review") {
            $attachment['title_link'] = "admin/reviewed-loan-requests/" . $loanRequestObj->id;
            $status = "Waiting for Management.";
        } else if ($loanRequestObj->status == "reconcile") {
            $attachment['title_link'] = "admin/reconciled-loan-requests/" . $loanRequestObj->id;
            $status = "Waiting for HR.";
        } else if ($loanRequestObj->status == "approved") {
            $attachment['title_link'] = "admin/approved-loan-requests/" . $loanRequestObj->id;
            $status = "Pending for payment transfer.";
        } else if ($loanRequestObj->status == "disbursed") {
            $attachment['title_link'] = "admin/loan-applications/" . $loanRequestObj->id;
            $status = "Loan Disbursed.";
        } else if ($loanRequestObj->status == "rejected") {
            $attachment['title_link'] = "admin/loan-applications/" . $loanRequestObj->id;
            $attachment['color'] = "danger";
            $status = "Loan Rejected.";
        } else {}
        $attachment['title'] = "Loan Requested";
        $attachment['ts'] = strtotime($loanRequestObj->created_at);
        $attachment['fallback'] = "Loan Request #" . $loanRequestId;
        $attachment['pretext'] = $loanRequestObj->amount;
        // $attachment['text'] = $loanRequestObj->description;
        // $assignedTo = $loanRequestObj->assignee->name;
        $fields = [
            [
                "title" => "Loanee Name",
                "value" => $loanRequestObj->user->name,
                "short" => true,
            ],
            [
                "title" => "Employee Id",
                "value" => $loanRequestObj->user->employee_id,
                "short" => true,
            ],
            [
                "title" => "Amount",
                "value" => $loanAmount,
                "short" => true,
            ],
            [
                "title" => "Status",
                "value" => $status,
                "short" => true,
            ],

        ];
        $attachment['fields'] = $fields;

        return $attachment;
    }

    public static function loanRequested($loanRequestId)
    {
        $obj = LoanRequest::find($loanRequestId);
        if (count($obj) > 0) {
            $channelMsg = 'New Loan Has Been Requested.';
            $userMsg = "Your loan request is successfully submitted.\nRef Id: #" . $loanRequestId;
            $attachments = self::getTemplate($loanRequestId);
            self::sendNotificationUser($loanRequestId, $obj->user->id, $userMsg, $attachments);
            self::sendNotificationOnChannel($loanRequestId, $channelMsg, $attachments);
        }

    }

    public static function loanRejected($loanRequestId)
    {
        $obj = LoanRequest::find($loanRequestId);
        if (count($obj) > 0) {
            $channelMsg = "Loan Rejected By " . $obj->activities()->orderBy('id', 'desc')->first()->role . ".";
            $userMsg = "Your loan has been rejected.";
            $attachments = self::getTemplate($loanRequestId);
            self::sendNotificationUser($loanRequestId, $obj->user->id, $userMsg, $attachments);
            self::sendNotificationOnChannel($loanRequestId, $channelMsg, $attachments);
        }
    }

    public static function loanSubmitted($loanRequestId)
    {
        $obj = LoanRequest::find($loanRequestId);
        if (count($obj) > 0) {
            $channelMsg = 'Loan request updated by ' . $obj->activities()->orderBy('id', 'desc')->first()->role . ".";
            $attachments = self::getTemplate($loanRequestId);
            self::sendNotificationOnChannel($loanRequestId, $channelMsg, $attachments);
        }
    }

    public static function loanReviewed($loanRequestId)
    {
        $obj = LoanRequest::find($loanRequestId);
        if (count($obj) > 0) {
            $channelMsg = 'Loan request updated by ' . $obj->activities()->orderBy('id', 'desc')->first()->role . ".";
            $attachments = self::getTemplate($loanRequestId);
            self::sendNotificationOnChannel($loanRequestId, $channelMsg, $attachments);
        }
    }

    public static function loanReconciled($loanRequestId)
    {
        $obj = LoanRequest::find($loanRequestId);
        if (count($obj) > 0) {
            $channelMsg = 'Loan request updated by ' . $obj->activities()->orderBy('id', 'desc')->first()->role . ".";
            $attachments = self::getTemplate($loanRequestId);
            self::sendNotificationOnChannel($loanRequestId, $channelMsg, $attachments);
        }
    }

    public static function loanApproved($loanRequestId)
    {
        $obj = LoanRequest::find($loanRequestId);
        if (count($obj) > 0) {
            $channelMsg = 'Loan approved. Please initiate the bank transfer.';
            $attachments = self::getTemplate($loanRequestId);
            self::sendNotificationOnChannel($loanRequestId, $channelMsg, $attachments);
        }
    }

    public static function loanDisbursed($loanRequestId)
    {
        $obj = LoanRequest::find($loanRequestId);
        if (count($obj) > 0) {
            $channelMsg = 'Loan request updated by ' . $obj->activities()->orderBy('id', 'desc')->first()->role . ".";
            $userMsg = "Your loan has been disbursed";
            $attachments = self::getTemplate($loanRequestId);
            self::sendNotificationUser($loanRequestId, $obj->user->id, $userMsg, $attachments);
            self::sendNotificationOnChannel($loanRequestId, $channelMsg, $attachments);
        }
    }

    public static function sendNotificationOnChannel($loanRequestId, $template_path, $attachments = [])
    {
        //'debug' => env('APP_DEBUG', false),
        $channelId = \Config::get('messaging-app.loan_request_channel');
        $job = (new PostMessageChannelJob($channelId, $template_path, $attachments));
        dispatch($job);
    }
    public static function sendNotificationUser($loanRequestId, $user_id, $message, $attachments = [])
    {
        $job = (new PostMessageUserJob($user_id, $message, $attachments));
        dispatch($job);

    }
}
