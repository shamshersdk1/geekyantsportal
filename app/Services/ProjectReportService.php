<?php namespace App\Services;

use App\User;
use Validator;
use App\Models\Admin\Calendar;
use App\Models\Admin\Project;
use Config;
use DB;

class ProjectReportService {

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */

    public static function exportCsv($projectId)
	{

		header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=data.csv');


        $projectSprints = DB::table('timelogs')
                   ->join('users', 'timelogs.user_id', '=', 'users.id')
                   ->leftjoin('projects', 'timelogs.project_id', '=', 'projects.id')
                   ->leftJoin('project_sprints', function($join)
                               {
                                    $join->on('project_sprints.start_date','<=','timelogs.current_date');
                                    $join->on('project_sprints.end_date','>=','timelogs.current_date');
                               })
                   ->where('timelogs.project_id',$projectId)
                   ->select('timelogs.id as Timelog_id','projects.project_name as Project_Name','project_sprints.id as Sprint_ID','users.name as Name','timelogs.message as Task','timelogs.minutes as Minutes','timelogs.current_date as Date')
                   ->groupBy('timelogs.id')
                   ->get();
        $rows = $projectSprints->toArray();
        $rows = json_decode(json_encode((array) $rows), true);

         ob_end_clean();
        
        $out = fopen('php://output', 'w');
        foreach ($rows as $row) {
            
            $arr = [];
            foreach ($row as $key=>$value) {
                array_push($arr, $key);
            }
            $line = $arr;
            fputcsv($out, $line);
            break;
        }
        
        foreach ($rows as $row) {
            
            $arr = [];
            foreach ($row as $key=>$value) {
                array_push($arr, $value);
            }
            $line = $arr;
            fputcsv($out, $line);
        }
        die;
        fclose($out);
	}
}