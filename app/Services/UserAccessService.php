<?php namespace App\Services;

use App\Models\UserRole;

class UserAccessService {
    
    public function grantAccess()
    {
        if(\Auth::user()->role == 'admin'|| UserRole::where('user_id', \Auth::id())->count() > 0) {
            return true;
        } else {    
        return false;
        }
    }    

}