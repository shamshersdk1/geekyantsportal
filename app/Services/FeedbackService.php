<?php namespace App\Services;

use App\Jobs\Slack\SlackReminder\FeedbackReview\NotifyMultiple;
use App\Jobs\Slack\SlackReminder\FeedbackReview\NotifySingle;
use App\Models\Admin\FeedbackMonth;
use App\Models\Admin\FeedbackQuestionDesignationTemplate;
use App\Models\Admin\FeedbackQuestionTemplate;
use App\Models\Admin\FeedbackUser;
use App\Models\Admin\Project;
use App\Models\Admin\Question;
use App\Models\Admin\QuestionMapping;
use App\Models\Admin\Role;
use App\Models\User;
use App\Models\UserTimesheet;
use DB;

class FeedbackService
{
    public static function updateFeedbackQuestionTemplate($id, $input)
    {
        $status = true;
        $message = '';

        $feedbackQuestionObj = FeedbackQuestionTemplate::find($id);
        if (!$feedbackQuestionObj) {
            $status = false;
            $message = 'No record found';
        } else {
            $feedbackQuestionObj->question = $input['question'];
            $feedbackQuestionObj->role_type = !empty($input['role']) ? $input['role'] : '';
            $feedbackQuestionObj->status = $input['status'];
            if ($feedbackQuestionObj->save()) {
                if (!empty($input['designations'])) {
                    $res = self::saveFeedbackQuestionDesignationTemplate($feedbackQuestionObj->id, $input['designations']);
                    if (!$res['status']) {
                        $status = false;
                        $message = $res['message'];
                    }
                }
            } else {
                $status = false;
                $message = $feedbackQuestionObj->getErrors();
            }
        }

        $response['status'] = $status;
        $response['message'] = $message;

        return $response;
    }

    public static function saveFeedbackQuestionDesignationTemplate($feedback_question_template_id, $designations)
    {
        $status = true;
        $message = '';
        $recordExists = FeedbackQuestionDesignationTemplate::where('feedback_question_template_id', $feedback_question_template_id)->first();
        if ($recordExists) {
            FeedbackQuestionDesignationTemplate::where('feedback_question_template_id', $feedback_question_template_id)->delete();
        }
        foreach ($designations as $designation) {
            $feedbackQuestionDesignationTemplateObj = new FeedbackQuestionDesignationTemplate();
            $feedbackQuestionDesignationTemplateObj->feedback_question_template_id = $feedback_question_template_id;
            $feedbackQuestionDesignationTemplateObj->designation_id = $designation;
            if (!$feedbackQuestionDesignationTemplateObj->save()) {
                $status = false;
                $message = $feedbackQuestionDesignationTemplateObj->getErrors();
            }
        }

        $response['status'] = $status;
        $response['message'] = $message;

        return $response;
    }

    public static function getTlDetails($teamLeadId, $feedback_month_id)
    {
        $myProjects = Project::where('project_manager_id', $teamLeadId)->whereIn('status', ['in_progress', 'free'])->get();
        $projectIdList = Project::whereIn('status', ['in_progress', 'free'])->where('project_manager_id', $teamLeadId)
            ->distinct()->select('id')->pluck('id')->toArray();

    }

    public static function updateFeedBackRating($id, $rating)
    {
        $status = true;
        $message = '';

        $feedbackUserObj = FeedbackUser::find($id);
        if (!$feedbackUserObj) {
            $status = false;
            $message = 'No record found';
        } else {
            $feedbackUserObj->rating = $rating;
            if (!$feedbackUserObj->save()) {
                $status = false;
                $message = $feedbackUserObj->getErrors();
            }
        }
        $response['status'] = $status;
        $response['message'] = $message;

        return $response;
    }

    public static function generateRecords($month, $year)
    {
        $feedbackMonthObj = FeedbackMonth::where('month', $month)->where('year', $year)->first();
        // Create Feedback Month if not available
        if (!$feedbackMonthObj) {
            $feedbackMonthObj = new FeedbackMonth();
            $feedbackMonthObj->month = $month;
            $feedbackMonthObj->year = $year;
            $feedbackMonthObj->save();
        }
        // Delete Feedback User details if already generated for the month
        FeedbackUser::where('feedback_month_id', $feedbackMonthObj->id)->delete();

        $users = User::where('is_active', 1)->where('is_feedback_review', 1)->whereNotNull('designation')->get();
        foreach ($users as $user) {
            self::generateRecordsForUser( $user->id, $feedbackMonthObj->id );
        }
    }

    public static function generateRecordsForUser( $user_id, $feedback_month_id )
    {
        $feedbackMonthObj = FeedbackMonth::find($feedback_month_id);
        $tempDateString = $feedbackMonthObj->year.'-'.$feedbackMonthObj->month.'-1';
        $first_date =  date("Y-m-d", strtotime($tempDateString)) ;
        $last_date = date("Y-m-t", strtotime($first_date));
        $user = User::find($user_id);
        $reviewer_id = '';
        $questionList = $user->userDesignation->questions;
        // Delete all previous records for the given month
        FeedbackUser::where('feedback_month_id', $feedbackMonthObj->id)->where('user_id', $user->id)->delete();
        // Get projects with entries of more than 20 hrs of timesheet entry
        $projectList = UserTimesheet::where('user_id', $user->id)->where('date', '>=', $first_date)->where('date', '<=', $last_date)->groupBy('project_id')->havingRaw('SUM(duration) >= 20 ')
                ->select('project_id')->pluck('project_id')->toArray();
        foreach ($questionList as $questionObj) {
            foreach ($projectList as $projectId) {
                $projectObj = Project::find($projectId);
                if ($questionObj && $feedbackMonthObj) {
                    $reviewer_id = self::getReviewerId($projectObj->id, $questionObj->question->role);
                    if ($reviewer_id != '' && $user->id != $reviewer_id) {
                        $feedbackUserObj = new FeedbackUser();
                        $feedbackUserObj->user_id = $user->id;
                        $feedbackUserObj->reviewer_id = $reviewer_id;
                        $feedbackUserObj->project_id = $projectObj->id;
                        $feedbackUserObj->reviewer_type = $questionObj->question->role->id;
                        $feedbackUserObj->feedback_month_id = $feedbackMonthObj->id;
                        $feedbackUserObj->question = $questionObj->question->question;
                        $feedbackUserObj->rating = null;
                        $feedbackUserObj->status = 'pending';
                        $feedbackUserObj->save();
                    }
                }
            }
        }
        // GENERATE FOR All ROLE 
        if (!empty($user->roles) && count($user->roles) > 0) {
            foreach ($user->roles as $role) {
                if(!empty($role->questions)) {
                    $questionList = $role->questions;
                    foreach ($questionList as $questionObj) {
                        // $projectList = Project::where('project_manager_id', $user->id)->where('status', 'in_progress')->get();
                        $projectList = UserTimesheet::where('user_id', $user->id)->where('date', '>=', $first_date)->where('date', '<=', $last_date)->groupBy('project_id')
                                        ->select('project_id')->pluck('project_id')->toArray();
                        foreach ($projectList as $projectId) {
                            $projectObj = Project::find($projectId);
                            if ($questionObj && $feedbackMonthObj && $projectObj) {
                                $reviewer_id = self::getReviewerId($projectObj->id, $questionObj->question->role);
                                if ($reviewer_id != '' && $user->id != $reviewer_id) {
                                    $feedbackUserObj = new FeedbackUser();
                                    $feedbackUserObj->user_id = $user->id;
                                    $feedbackUserObj->reviewer_id = $reviewer_id;
                                    $feedbackUserObj->project_id = $projectObj->id;
                                    $feedbackUserObj->reviewer_type = $questionObj->question->role->id;
                                    $feedbackUserObj->feedback_month_id = $feedbackMonthObj->id;
                                    $feedbackUserObj->question = $questionObj->question->question;
                                    $feedbackUserObj->rating = null;
                                    $feedbackUserObj->status = 'pending';
                                    $feedbackUserObj->save();
                                }
                            }
                        }
                    }
                }
            }
        }

    }

    public static function getUserList($userId, $feedbackMonthId)
    {
        $userIdList = FeedbackUser::where('reviewer_id', $userId)->where('feedback_month_id', $feedbackMonthId)
            ->distinct()->select('user_id')->pluck('user_id')->toArray();
        $userList = User::whereIn('id', $userIdList)->get();
        $return_array = [];
        foreach ($userList as $user) {
            $row = [];
            $row['id'] = $user->id;
            $row['name'] = $user->name;

            $pendingCount = FeedbackUser::where('reviewer_id', $userId)->where('feedback_month_id', $feedbackMonthId)->where('user_id', $user->id)
                ->where('status', 'pending')->get()->count();

            $totalCount = FeedbackUser::where('reviewer_id', $userId)->where('feedback_month_id', $feedbackMonthId)->where('user_id', $user->id)
                ->get()->count();

            $percentage_completed = ($totalCount - $pendingCount) / $totalCount * 100;
            $row['percentage_completed'] = $percentage_completed;
            $return_array[] = $row;

        }
        return $return_array;
    }
    public static function getMonthList()
    {
        $return_array = [];
        $month_name_array = [
            1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September',
            10 => 'Ocotber', 11 => 'November', 12 => 'December',
        ];

        $months = FeedbackMonth::orderBy('year', 'DESC')->orderBy('month', 'DESC')->get();
        foreach ($months as $month) {
            $row = [];
            $row['id'] = $month->id;
            $row['month'] = $month->month;
            $row['year'] = $month->year;
            $row['display_name'] = $month_name_array[$month->month] . ', ' . $month->year;
            $return_array[] = $row;
        }
        return $return_array;
    }

    public static function sendSingleNotification($feedbackUserId)
    {
        $job = (new NotifySingle($feedbackUserId))->onQueue('feedback-review-reminder');
        dispatch($job);
    }

    public static function sendNotificationAll($feedbackUserId)
    {
        $feedbackUserObj = FeedbackUser::find($feedbackUserId);

        $month = $feedbackUserObj->feedbackMonth->month;
        $year = $feedbackUserObj->feedbackMonth->year;

        $month_id = $feedbackUserObj->feedback_month_id;
        $user_id = $feedbackUserObj->user_id;

        $reviewerIdList = FeedbackUser::where('user_id', $user_id)->where('feedback_month_id', $month_id)->select('reviewer_id')
                                        ->distinct()->pluck('reviewer_id')->toArray();

        foreach ($reviewerIdList as $reviewerId) {
            $pendingCount = FeedbackUser::where('reviewer_id', $reviewerId)->where('feedback_month_id', $month_id)->where('user_id', $user_id)
                                        ->where('status', 'pending')->get()->count();
            if ( $pendingCount > 0 ) {
                $job = (new NotifyMultiple($user_id, $reviewerId, $month, $year))->onQueue('feedback-review-reminder');
                dispatch($job);
            }

        }
    }
    public static function getQuestions($selectedDesignations, $selectedRoles)
    {
        if (count($selectedRoles) > 0 && count($selectedDesignations) > 0) {
            $questions = FeedbackQuestionTemplate::with(['feedbackdesignations' => function ($query) use ($selectedDesignations) {
                $query->whereIn('designation_id', $selectedDesignations);
            }])->whereIn('role_type', $selectedRoles)->orderBy('id', 'DESC')->paginate(50);
        } elseif (count($selectedRoles) > 0 && count($selectedDesignations) == 0) {
            $questions = FeedbackQuestionTemplate::whereIn('role_type', $selectedRoles)->orderBy('id', 'DESC')->paginate(50);
        } elseif (count($selectedRoles) == 0 && count($selectedDesignations) > 0) {
            $questions = FeedbackQuestionTemplate::with(['feedbackdesignations' => function ($query) use ($selectedDesignations) {
                $query->whereIn('designation_id', $selectedDesignations);
            }])->orderBy('id', 'DESC')->paginate(50);

        } elseif (count($selectedRoles) == 0 && count($selectedDesignations) == 0) {
            $questions = FeedbackQuestionTemplate::orderBy('id', 'DESC')->paginate(50);
        } else {
            $questions = FeedbackQuestionTemplate::orderBy('id', 'DESC')->paginate(50);
        }
        return $questions;
    }

    public static function updateQuestion($id, $input)
    {
        $status = true;
        $message = '';

        $questionObj = Question::find($id);
        if (!$questionObj) {
            $status = false;
            $message = 'No record found';
        } else {
            $questionObj->question = $input['question'];
            $questionObj->reviewer_type = !empty($input['role']) ? $input['role'] : '';
            $questionObj->status = $input['status'];
            $questionObj->category_type = !empty($input['category_type']) ? $input['category_type'] : '';
            if ($input['category_type'] == 'designation' && !empty($input['designations'])) {
                $reference_type = 'App\Models\Admin\Designation';
                // Delete Earlier mapping
                QuestionMapping::where('question_id', $questionObj->id)->delete();
                foreach ($input['designations'] as $designation) {
                    $newMappedQuestion = new QuestionMapping();
                    $newMappedQuestion->question_id = $questionObj->id;
                    $newMappedQuestion->reference_type = $reference_type;
                    $newMappedQuestion->reference_id = $designation;
                    $newMappedQuestion->save();
                }
            } elseif ($input['category_type'] == 'role' && !empty($input['roles'])) {
                $reference_type = 'App\Models\Admin\Role';
                // Delete Earlier mapping
                QuestionMapping::where('question_id', $questionObj->id)->delete();
                foreach ($input['roles'] as $role) {
                    $newMappedQuestion = new QuestionMapping();
                    $newMappedQuestion->question_id = $questionObj->id;
                    $newMappedQuestion->reference_type = $reference_type;
                    $newMappedQuestion->reference_id = $role;
                    $newMappedQuestion->save();
                }
            }
            if (!$questionObj->save()) {
                $status = false;
                $message = $questionObj->getErrors();
            }
        }

        $response['status'] = $status;
        $response['message'] = $message;

        return $response;
    }
    // NEW FEEDBACK FLOW

    public static function getAllReviewerType($reviewer_id, $month_id)
    {
        if ($reviewer_id == 0) {
            $reviewerIds = FeedbackUser::where('feedback_month_id', $month_id)->distinct()->select('reviewer_type')->pluck('reviewer_type')->toArray();
        } else {
            $reviewerIds = FeedbackUser::where('feedback_month_id', $month_id)->where('reviewer_id', $reviewer_id)
                ->distinct()->select('reviewer_type')->pluck('reviewer_type')->toArray();
        }
        $roles = Role::whereIn('id', $reviewerIds)->orderBy('code', 'DESC')->get();
        return $roles;
    }

    public static function getProjectReview($user_id, $reviewer_id, $reviewer_type, $project_id, $feedback_month_id)
    {
        $projectReviews = FeedbackUser::where('user_id', $user_id)->where('reviewer_id', $reviewer_id)->where('reviewer_type', $reviewer_type)
            ->where('project_id', $project_id)->where('feedback_month_id', $feedback_month_id)->get();
        return $projectReviews;
    }

    public static function getReviewDetail($user_id, $feedback_month_id)
    {
        $reviewDetails = FeedbackUser::with('reviewer', 'project', 'roleType')->where('user_id', $user_id)->where('feedback_month_id', $feedback_month_id)
            ->groupBy('reviewer_id')->groupBy('project_id')->groupBy('reviewer_type')->get()->toArray();
        return $reviewDetails;
    }

    public static function getOverallCompletionStatus($user_id, $month_id)
    {
        $pendingCount = FeedbackUser::where('user_id', $user_id)->where('feedback_month_id', $month_id)->where('status', 'pending')->get()->count();
        if ($pendingCount < 1) {
            return true;
        } else {
            return false;
        }

    }

    public static function getReviewedProjectAdmin($user_id, $reviewer_id, $project_id, $feedback_month_id)
    {
        $projectReviews = FeedbackUser::with('project', 'roleType')->where('user_id', $user_id)->where('reviewer_id', $reviewer_id)
            ->where('feedback_month_id', $feedback_month_id)->where('project_id', $project_id)->get();
        return $projectReviews;
    }

    public static function getReviewerId($project_id, $reviewer_type)
    {
        \Log::info('Role id'.$reviewer_type);
        $roleObj = Role::find($reviewer_type->id);

        $projectObj = Project::find($project_id);
        $reviewer_id = '';
        switch ($roleObj->code) {
            case 'team-lead':
                $reviewer_id = !empty($projectObj->project_manager_id) ? $projectObj->project_manager_id : '';
                break;
            case 'account-manager':
                $reviewer_id = !empty($projectObj->account_manager_id) ? $projectObj->account_manager_id : '';
                break;
            case 'sales-manager':
                $reviewer_id = !empty($projectObj->bd_manager_id) ? $projectObj->bd_manager_id : '';
                break;
            case 'code-lead':
                $reviewer_id = !empty($projectObj->code_lead_id) ? $projectObj->code_lead_id : '';
                break;
            case 'delivery-lead':
                $reviewer_id = !empty($projectObj->delivery_lead_id) ? $projectObj->delivery_lead_id : '';
                break;
            case 'project-coordinator':
                $reviewer_id = !empty($projectObj->project_coordinator_id) ? $projectObj->project_coordinator_id : '';
                break;
            default:$reviewer_id = '';
        }
        \Log::info('reviewer_id'.$reviewer_id);
        return $reviewer_id;
    }

    public static function getUserRating($month_id, $user_id){
        $userFeedbacks = FeedbackUser::where('feedback_month_id',$month_id)->where('user_id', $user_id)->get();
        if(count($userFeedbacks) <=0) return -2;

        $rating = 0;
        $count = 0;
        $score = 0;
        foreach($userFeedbacks as $feedback){
            if($feedback->rating == null)
                return -1;
            $count++;
            $score += $feedback->rating;
        }

        $rating = round($score/$count, 2);
        return $rating;
    }
    
    public static function getUserMonthList($user_id)
    {
        $result_array = [];
        $months = FeedbackMonth::where('is_visible',1)->orderBy('year', 'DESC')->orderBy('month', 'DESC')->get();;

        foreach( $months as $month )
        {
            $temp = [];
            $temp['id'] = $month->id;
            $temp['month'] = date('M', mktime(0, 0, 0, $month->month, 10));
            $temp['year'] = $month->year;
            $temp['rating'] = FeedbackUser::where('user_id', $user_id)->where('feedback_month_id',$month->id)
                                ->where('status','completed')->avg('rating');
            
            $temp['monthly_variable'] = 4000; // Write a function here
            $temp['variable_pay'] = 3200; // Write a function here
            $result_array[] = $temp;                    
        }
        
        return $result_array;
    }

    public static function getUserMonthlyReviews($user_id, $feedback_month_id)
    {
        $reviews = [];

        $feedbackMonthObj = FeedbackMonth::find($feedback_month_id);
        if ( !$feedbackMonthObj || !$feedbackMonthObj->is_visible )
        {
            return $reviews;
        }
        $tempArray = FeedbackUser::where('user_id',$user_id)->where('feedback_month_id',$feedback_month_id)
                    ->groupBy('project_id')->get();
            
        
        foreach ($tempArray as $tempRecord) 
        {
            $feedbackUser = [];
            $feedbackUser['project'] = $tempRecord->project->toArray();
            $reviewerIdList = FeedbackUser::where('user_id',$user_id)->where('feedback_month_id',$feedback_month_id)->where('project_id',$tempRecord->project_id)
                                            ->select()->distinct('reviewer_id')->pluck('reviewer_id')->toArray();
            $feedbackUser['project']['rating'] = FeedbackUser::where('user_id', $user_id)->where('feedback_month_id',$feedback_month_id)->where('project_id',$tempRecord->project_id)
                                            ->where('status','completed')->avg('rating');
                                                    
            foreach( $reviewerIdList as $reviewerId )
            {
                $feedbackUser['project']['reviewer'][$reviewerId] = User::find($reviewerId)->toArray();
                $feedbackUser['project']['reviewer'][$reviewerId]['project_reviews'] = FeedbackService::getReviewedProjectAdmin($user_id, $reviewerId, $tempRecord->project->id, $feedback_month_id);
            }
            $reviews[] = $feedbackUser;
        }
        return $reviews;
    }
}
