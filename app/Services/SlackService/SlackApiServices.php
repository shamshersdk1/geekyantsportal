<?php namespace App\Services\SlackService;

use Config;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class SlackApiServices
{

    public static function getPublicChannels()
    {
        $publicChannels = [];
        try {
            $client = new Client(['base_uri' => config('app.slack_url')]);
            $response = $client->request("POST", "channels.list", [
                'form_params' => [
                    'exclude_archived' => true,
                    'token' => config('app.slack_token'),
                ],
            ]);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        $channelCreateResponse = $response->getBody();
        $publicChannels = json_decode($channelCreateResponse)->channels;

        return $publicChannels;
    }

    public static function getPrivateChannels()
    {
        $privateChannels = [];
        try {
            $client = new Client(['base_uri' => config('app.slack_url')]);
            $response = $client->request("POST", "groups.list", [
                'form_params' => [
                    'exclude_archived' => true,
                    'token' => config('app.slack_token'),
                ],
            ]);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        $channelCreateResponse = $response->getBody();
        $privateChannels = json_decode($channelCreateResponse)->groups;

        return $privateChannels;
    }

    public static function getUsers()
    {

    }

    public static function addUser($channelId, $userId)
    {
        try {
            $client = new Client(['base_uri' => config('app.slack_url')]);
            $response = $client->request("POST", "channels.invite", [
                'form_params' => [
                    'token' => config('app.slack_token'),
                    'channel' => $channelId,
                    'user' => $userId,
                ],
            ]);
            $addUserResponse = $response->getBody();
            $success = json_decode($addUserResponse)->ok;
            return $success;
        } catch (Exception $e) {
            \Log::info($e->getMessage());
        }
    }

    public static function removeUser($channelId, $userId)
    {
        try {
            $client = new Client(['base_uri' => config('app.slack_url')]);
            $response = $client->request("POST", "channels.kick", [
                'form_params' => [
                    'token' => config('app.slack_token'),
                    'channel' => $channelId,
                    'user' => $userId,
                ],
            ]);
            $removeUserResponse = $response->getBody();
            $success = json_decode($removeUserResponse)->ok;
            return $success;
        } catch (Exception $e) {
            \Log::info($e->getMessage());
        }
    }

    public static function isChannelPrivate($channel_id)
    {
        $result = ['status' => false, 'data' => "error"];
        try {
            $client = new Client(['base_uri' => config('app.slack_url')]);
            $response = $client->request("POST", "channels.info", [
                'form_params' => [
                    'token' => config('app.slack_token'),
                    'channel' => $channel_id,
                ],
            ]);
            $channelInfoResponse = $response->getBody();
            $info = json_decode($channelInfoResponse);
            if ($info->ok) {
                $result['status'] = true;
                $result['data'] = $info->channel->is_private;
            }
        } catch (Exception $e) {
            \Log::info($e->getMessage());
        }
        return $result;
    }

    public static function getChannelName($channel_id)
    {
        $result = ['status' => false, 'data' => "error"];
        try {
            $client = new Client(['base_uri' => config('app.slack_url')]);
            $response = $client->request("POST", "channels.info", [
                'form_params' => [
                    'token' => config('app.slack_token'),
                    'channel' => $channel_id,
                ],
            ]);
            $channelInfoResponse = $response->getBody();
            $info = json_decode($channelInfoResponse);
            if ($info->ok) {
                $result['status'] = true;
                $result['data'] = $info->channel->name;
            }
        } catch (Exception $e) {
            \Log::info($e->getMessage());
        }
        return $result;
    }
    public static function getPublicChannelName($channel_id)
    {
        $result = ['status' => false, 'data' => "error"];
        try {
            $client = new Client(['base_uri' => config('app.slack_url')]);
            $response = $client->request("POST", "channels.info", [
                'form_params' => [
                    'token' => config('app.slack_token'),
                    'channel' => $channel_id,
                ],
            ]);
            $channelInfoResponse = $response->getBody();
            $info = json_decode($channelInfoResponse);
            if ($info->ok) {
                $result['status'] = true;
                $result['data'] = $info->channel->name;
            }
        } catch (Exception $e) {
            \Log::info($e->getMessage());
        }
        return $result;
    }
    public static function getPrivateChannelName($channel_id)
    {
        $result = ['status' => false, 'data' => "error"];
        try {
            $client = new Client(['base_uri' => config('app.slack_url')]);
            $response = $client->request("POST", "groups.info", [
                'form_params' => [
                    'token' => config('app.slack_token'),
                    'channel' => $channel_id,
                ],
            ]);
            $channelInfoResponse = $response->getBody();
            $info = json_decode($channelInfoResponse);
            if ($info->ok) {
                $result['status'] = true;
                $result['data'] = $info->group->name;
            }
        } catch (Exception $e) {
            \Log::info($e->getMessage());
        }
        return $result;
    }

    public static function checkBotInChannel($channel_id)
    {
        $status = false;
        \Log::info('channel_id   '.$channel_id);
        try {
            $client = new Client(['base_uri' => config('app.slack_url')]);
            $response = $client->request("POST", "groups.info", [
                'form_params' => [
                    'token' => config('app.slack_bot_token'),
                    'channel' => $channel_id,
                ],
            ]);
        } catch (Exception $e) {
            $status = false;
            echo $e->getMessage();
        }
        $channelInfoResponse = $response->getBody();
        $info = json_decode($channelInfoResponse);
        if ($info->ok) {
            $status = true;
        } else {
            $status = false;
        }
        return $status;
    }
}
