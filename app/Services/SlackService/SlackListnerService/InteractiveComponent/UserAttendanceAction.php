<?php
namespace App\Services\SlackService\SlackListnerService\InteractiveComponent;

use App\Models\Admin\SlackUser;
use App\Models\User;
use App\Models\UserAttendance;
use App\Services\SlackService\SlackConstants;
use App\Services\SlackService\SlackListnerService\InteractiveComponentInterface;
use Illuminate\Http\Request;
use View;

class UserAttendanceAction implements InteractiveComponentInterface
{
    public static function main(Request $request = null)
    {

        $payload = json_decode($request->payload, true);
        $data = !empty($payload['actions'][0]) ? $payload['actions'][0] : [];
        $slackUserId = !empty($data['block_id']) ? $data['block_id'] : null;
        $mark = !empty($data['selected_option']['value']) ? $data['selected_option']['value'] : null;
        //$user_slack_id = $payload['user']['id'];
        $slackUserObj = SlackUser::where('slack_id', $slackUserId)->first();
        $status = false;
        $date = date('Y-m-d');

        if (!$slackUserObj) {
            $error = SlackConstants::NOT_FOUND_IN_DB;
            $text = View::make('slack.error.error', compact('error'))->render();
            $response = ["response_type" => "ephemeral", "replace_original" => $status, "text" => $text];
        } else {
            if ($mark != 'is_ignored') {
                $userId = $slackUserObj->user->id;
                // is_onsite, is_wfh, is_onsite_meeting
                $status = UserAttendance::createUserAttendanceMarkedAs($userId, $date, $mark);
                if (!$status) {
                    return ["user_id" => $user->id, "reference_id" => "Unable to update the record with status" . $mark, 'response' => $response];
                }
            }
        }

        if ($mark == 'is_wfh') {
            $markStatus = '`working from home`';
        } elseif ($mark == 'is_onsite') {
            $markStatus = 'on `Onsite`';
        } elseif ($mark == 'is_onsite_meeting') {
            $markStatus = 'on `onsite meeting`';
        } elseif ($mark == 'is_ignored') {
            $markStatus = '`ignored`';
        }

        $text = "*" . ucwords($slackUserObj->user->name) . "* is marked as " . $markStatus;

        $responseUrl = !empty($payload['response_url']) ? $payload['response_url'] : null;
        if ($responseUrl) {
            $responseData['replace_original'] = true;
            $responseData['text'] = $text;
            self::sendResponseToSlack($responseUrl, $responseData);
        }

        $response = ["response_type" => "ephemeral", "replace_original" => $status, "text" => $text];

        return ["user_id" => $user->id, "reference_id" => 'UserAttendance', 'response' => $response];
    }
    public static function sendResponseToSlack($url, $data)
    {
        $data_string = json_encode($data);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );
        $result = curl_exec($ch);

        //curl request
        /*
    curl -X POST -H 'Content-type: application/json' --data '{"delete_original": "true","text":"Hello, World!&&"}' https://hooks.slack.com/actions/T02DTKZJA/658283502659/4Gh5LBmtRfpQUob6JBglxKmW
     */
    }
}
