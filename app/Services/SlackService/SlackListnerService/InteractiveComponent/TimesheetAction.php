<?php
namespace App\Services\SlackService\SlackListnerService\InteractiveComponent;
use App\Services\SlackService\SlackListnerService\InteractiveComponentInterface;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

use App\Models\Admin\SlackUser;
use App\Models\User;
use App\Models\TmpTimesheet;
use App\Services\TimesheetService;

use App\Services\SlackService\SlackConstants;
use App\Services\SlackService\SlackApiServices;

use View;
use Exception;

class TimesheetAction implements InteractiveComponentInterface
{
	public static function main(Request $request = null) {
		
		$payload = json_decode($request->payload, true);
		$data = $payload['actions'][0];
		$user_slack_id = $payload['user']['id'];
		$slack_user = SlackUser::with('users')->where('slack_id', $user_slack_id)->first();
		$user = $slack_user->user;
		$status = false;
		$channelId = $payload['channel']['id'];
		
		if(!$user) {
			$error = SlackConstants::NOT_FOUND_IN_DB;
			$text = View::make('slack.error.error', compact('error'))->render();
			$response = ["response_type" => "ephemeral", "replace_original" => $status, "text" => $text];
		} else {
			try {
				if ( $data['type'] == 'button' )
				{
					$selectedAction = $data['name'];
					$tmpTimesheetId = $data['value'];
					$tmpTimesheetObj = TmpTimesheet::find($tmpTimesheetId);
					if ( empty($tmpTimesheetObj) ) {
						$error = 'Record is already updated. Please goto the portal to edit the entry';
						$text = View::make('slack.error.error', compact('error'))->render();
						$response = ["response_type" => "ephemeral", "replace_original" => false, "text" => $text];
					}
					else {
						$res = TimesheetService::requestAction($tmpTimesheetId, $selectedAction);
						$status = $res['status'];
						if( $status )
						{
							$message = $res['message'];
							$text = View::make('slack.action', compact('message'))->render();
							$response = ["response_type" => "ephemeral", "replace_original" => $status, "text" => $text];
						}
						else
						{
							$error = $res['message'];
							$text = View::make('slack.error.error', compact('error'))->render();
							$response = ["response_type" => "ephemeral", "replace_original" => $status, "text" => $text];
						}
					}
				}
				else
				{
					$selected_otpions = $data['selected_options'][0]['value'];
					$arr = explode("_", $selected_otpions);
					$date = $arr[0];
					$tmpTimesheetId = $arr[1];
					$tmpTimesheetObj = TmpTimesheet::find($tmpTimesheetId);
					if ( empty($tmpTimesheetObj) ) {
						$error = 'Invalid Entry';
						$text = View::make('slack.error.error', compact('error'))->render();
						$response = ["response_type" => "ephemeral", "replace_original" => false, "text" => $text];
					}
					else {
						$res = TimesheetService::updateTempRecordDate($tmpTimesheetObj->id, $date);
						$status = $res['status'];
						if( $status )
						{
							$message = 'Timesheet details successfully saved';
							$text = View::make('slack.action', compact('message'))->render();
							$response = ["response_type" => "ephemeral", "replace_original" => $status, "text" => $text];
							TimesheetService::storeTimesheetDetails($tmpTimesheetObj->id);
						}
						else
						{
							$error = $res['message'];
							$text = View::make('slack.error.error', compact('error'))->render();
							$response = ["response_type" => "ephemeral", "replace_original" => $status, "text" => $text];
						}
					}
				}
			} catch(Exception $e) {
				\Log::info('Timesheet Action'.$e->getMessage());
				// \Log::info('Exception');
				$error = SlackConstants::SOMETHING_WENT_WRONG;
				$text = View::make('slack.error.error', compact('error'))->render();
				$status = false;
				$response = ["response_type" => "ephemeral", "replace_original" => $status, "text" => $text];
			}
		}
		return ["user_id" => $user->id, "reference_id" => "Timesheet ", 'response' => $response];
	}
}