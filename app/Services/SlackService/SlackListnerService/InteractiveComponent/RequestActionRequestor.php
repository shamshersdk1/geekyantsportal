<?php
namespace App\Services\SlackService\SlackListnerService\InteractiveComponent;
use App\Services\SlackService\SlackListnerService\InteractiveComponentInterface;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

use App\Models\Admin\SlackUser;
use App\Models\User;
use App\Models\Admin\RequestApproveUser;

use App\Services\SlackService\SlackConstants;
use App\Services\SlackService\SlackApiServices;
use App\Services\SlackService\SlackMessageService\Reminder\Leave;

use View;
use Exception;

class RequestActionRequestor implements InteractiveComponentInterface
{
	public static function main(Request $request = null) {
		
		$payload = json_decode($request->payload, true);
		$data = $payload['actions'][0];
		$user_slack_id = $payload['user']['id'];
		$slack_user = SlackUser::with('users')->where('slack_id', $user_slack_id)->first();
		$user = $slack_user->user;
		$status = false;
		
		if(!$user) {
			$error = SlackConstants::NOT_FOUND_IN_DB;
			$text = View::make('slack.error.error', compact('error'))->render();
			$response = ["response_type" => "ephemeral", "replace_original" => $status, "text" => $text];
		} else {
			try {
				$requestId = $data['value'];
				$action = $data['name'];
				$response = RequestApproveUser::requestAction($requestId, $user->id, $action);
				$text = "";
				if($response['status']) {
					
					$message = $response['message'];
					$text = View::make('slack.leave.action', compact('message'))->render();
					$status = true;
				} else {
					$error = $response['message'];
					$text = View::make('slack.error.error', compact('error'))->render();
				}
				$response =  ["response_type" => "ephemeral", "replace_original" => $status, "text" => $text];
			} catch(Exception $e) {
				\Log::info('RequestActionRequestor'.$e->getMessage());
				$error = SlackConstants::SOMETHING_WENT_WRONG;
				$text = View::make('slack.error.error', compact('error'))->render();
				$status = false;
				$response = ["response_type" => "ephemeral", "replace_original" => $status, "text" => $text];
			}
		}
		return ["user_id" => $user->id, "reference_id" => "Request id ".$requestId, 'response' => $response];
	}
}