<?php
namespace App\Services\SlackService\SlackListnerService\InteractiveComponent;
use App\Services\SlackService\SlackListnerService\InteractiveComponentInterface;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

use App\Models\Admin\SlackUser;
use App\Models\User;
use App\Models\SshKey;

use App\Services\SlackService\SlackConstants;
use App\Services\SlackService\SlackApiServices;

use View;
use Exception;

class SshAction implements InteractiveComponentInterface
{
	public static function main(Request $request = null) {
		
		$payload = json_decode($request->payload, true);
		$data = $payload['actions'][0];
		$user_slack_id = $payload['user']['id'];
		$slack_user = SlackUser::with('users')->where('slack_id', $user_slack_id)->first();
		$user = $slack_user->user;
		$status = false;
		
		if(!$user) {
			$error = SlackConstants::NOT_FOUND_IN_DB;
			$text = View::make('slack.error.error', compact('error'))->render();
			$response = ["response_type" => "ephemeral", "replace_original" => $status, "text" => $text];
		} else {
			try {
				$sshKeyId = $data['value'];
				$action = $data['name'];
				$response = SshKey::requestAction($sshKeyId, $action);
				$text = "";
				if($response['status']) {
					$message = $response['message'];
					$url = config('app.geekyants_portal_url')."/admin/ssh-keys/my-ssh-key/edit";
					$text = View::make('slack.action', compact('message'))->render();
					$actions = [["text" => "View", "type" => "button", "url" => $url]];
                	$attachment = [["text" => "Follow the link", "fallback" => "Goto the portal to check the request", "attachment_type" => "default", "callback_id" => "request_action_user", "actions" => $actions ]];
					$status = true;
				} else {
					$error = $response['message'];
					$attachment = [];
					$text = View::make('slack.error.error', compact('error'))->render();
				}
				$response =  ["response_type" => "ephemeral", "replace_original" => $status, "text" => $text, "attachments" => $attachment];
			} catch(Exception $e) {
				// \Log::info('RequestActionRequestor'.$e->getMessage());
				$error = SlackConstants::SOMETHING_WENT_WRONG;
				$text = View::make('slack.error.error', compact('error'))->render();
				$status = false;
				$response = ["response_type" => "ephemeral", "replace_original" => $status, "text" => $text];
			}
		}
		return ["user_id" => $user->id, "reference_id" => "SSH Key id ".$sshKeyId, 'response' => $response];
	}
}