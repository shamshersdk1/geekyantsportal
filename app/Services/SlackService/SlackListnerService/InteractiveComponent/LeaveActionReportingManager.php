<?php
namespace App\Services\SlackService\SlackListnerService\InteractiveComponent;
use App\Services\SlackService\SlackListnerService\InteractiveComponentInterface;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

use App\Models\Admin\SlackUser;
use App\Models\User;
use App\Models\Admin\Leave;
use App\Models\ProjectManagerLeaveApproval;

use App\Services\SlackService\SlackConstants;
use App\Services\SlackService\SlackApiServices;

use View;
use Exception;

class LeaveActionReportingManager implements InteractiveComponentInterface
{
	public static function main(Request $request = null) {
		$payload = json_decode($request->payload, true);
		$data = $payload['actions'][0];
		$user_slack_id = $payload['user']['id'];
		$slack_user = SlackUser::with('users')->where('slack_id', $user_slack_id)->first();
		$user = $slack_user->user;
		$status = false;
		if(!$user) {
			$error = SlackConstants::NOT_FOUND_IN_DB;
			$text = View::make('slack.error.error', compact('error'))->render();
			$response = ["response_type" => "ephemeral", "replace_original" => $status, "text" => $text];
		} else {
			try {
				$leave_id = $data['value'];
				$action = $data['name'];
				$response = Leave::saveLeave($leave_id, $user->id, $action);
				$text = "";
				if($response['status']) {
					// Leave::notifyLeaveApplicationToAdmin($leave_id);
					$message = $response['message'];
					$text = View::make('slack.leave.action', compact('message'))->render();
					$status = true;
				} else {
					$error = $response['message'];
					$text = View::make('slack.error.error', compact('error'))->render();
				}
				$response =  ["response_type" => "ephemeral", "replace_original" => $status, "text" => $text];
			} catch(Exception $e) {
				\Log::info($e->getMessage());
				$error = SlackConstants::SOMETHING_WENT_WRONG;
				$text = View::make('slack.error.error', compact('error'))->render();
				$status = false;
				$response = ["response_type" => "ephemeral", "replace_original" => $status, "text" => $text];
			}
		}
		return ["user_id" => $user->id, "reference_id" => "Leave id ".$leave_id, 'response' => $response];
	}
}