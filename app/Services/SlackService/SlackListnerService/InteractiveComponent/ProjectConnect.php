<?php
namespace App\Services\SlackService\SlackListnerService\InteractiveComponent;
use App\Services\SlackService\SlackListnerService\InteractiveComponentInterface;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

use App\Models\Admin\SlackUser;
use App\Models\User;
use App\Models\Admin\ProjectSlackChannel;
use App\Models\Admin\ProjectResource;
use App\Models\Timesheet;
use App\Models\TimesheetDetail;

use App\Services\SlackService\SlackConstants;
use App\Services\SlackService\SlackApiServices;

use View;
use Exception;

class ProjectConnect implements InteractiveComponentInterface
{
    public static function main(Request $request = null) {
		$payload = json_decode($request->payload, true);
		$project_id = $payload['actions'][0]['selected_options'][0]['value'];
		$channel_id = $payload['channel']['id'];
		$user_slack_id = $payload['user']['id'];
		$slack_user = SlackUser::with('users')->where('slack_id', $user_slack_id)->first();
		$user = $slack_user->user;
		$status = false;
		if(!$user || !$user->isAdmin()) {
			$error = SlackConstants::ACCESS_DENIED;
			$text = View::make('slack.error.error', compact('error'))->render();
			$response = ["response_type" => "ephemeral", "replace_original" => $status, "text" => $text];
		} else {
			try {
				$response = ProjectSlackChannel::connect($project_id, $channel_id);
				$message = $response['message'];
				$text = View::make('slack.connect.success', compact('message'))->render();
				$status = true;
				$response = ["response_type" => "ephemeral", "replace_original" => $status, "text" => $text];
			} catch(Exception $e) {
				\Log::info($e->getMessage());
				$error = SlackConstants::SOMETHING_WENT_WRONG;
				$text = View::make('slack.error.error', compact('error'))->render();
				$status = false;
				$response = ["response_type" => "ephemeral", "replace_original" => $status, "text" => $text];
			}
		}
		return ["user_id" => $user->id, "reference_id" => "Project id ".$project_id, 'response' => $response];
	}
}