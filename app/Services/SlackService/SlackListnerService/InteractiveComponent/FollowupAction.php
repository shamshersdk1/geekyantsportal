<?php
namespace App\Services\SlackService\SlackListnerService\InteractiveComponent;
use App\Services\SlackService\SlackListnerService\InteractiveComponentInterface;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

use App\Models\Admin\SlackUser;
use App\Models\User;


use App\Services\SlackService\SlackConstants;
use App\Services\SlackService\SlackApiServices;
use App\Services\LeadsService;

use View;
use Exception;


class FollowupAction implements InteractiveComponentInterface
{
	public static function main(Request $request = null) {
		
		$payload = json_decode($request->payload, true);
		$data = $payload['actions'][0];
		$user_slack_id = $payload['user']['id'];
		$slack_user = SlackUser::with('users')->where('slack_id', $user_slack_id)->first();
		$user = $slack_user->user;
		$status = false;
		$channelId = $payload['channel']['id'];
		
		if(!$user) {
			$error = SlackConstants::NOT_FOUND_IN_DB;
			$text = View::make('slack.error.error', compact('error'))->render();
			$response = ["response_type" => "ephemeral", "replace_original" => $status, "text" => $text];
		} else {
			try {
				if ( $data['type'] == 'button' )
				{
                    $leadId = $data['value'];
                    $action = $data['name'];
                    $res = LeadsService::requestAction($leadId, $action, $user->id);
                    \Log::info(json_encode($res));
                    $status = $res['status'];
                    if ( $status )
                    {
                        $text = $res['message'];
                        $attachment = null;
                        $response =  ["response_type" => "ephemeral", "replace_original" => $status, "text" => $text, "attachments" => $attachment];
                    }
                    else
                    {
                        $error = $res['return_message'];
                        $text = View::make('slack.error.error', compact('error'))->render();
                        $status = false;
                        $response = ["response_type" => "ephemeral", "replace_original" => $status, "text" => $text];	
                    }
				}
				else
				{
					$selected_otpions = $data['selected_options'][0]['value'];
					$arr = explode("_", $selected_otpions);
					$date = $arr[0];
                    $leadId = $arr[1];
                    $res = LeadsService::updateLeadRecord($leadId, $date , $user->id);
                    $status = $res['status'];
                    if ( $status )
                    {
                        $text = $res['message'];
                        $attachment = null;
                        $response =  ["response_type" => "ephemeral", "replace_original" => $status, "text" => $text, "attachments" => $attachment];
                    }
                    else
                    {
                        $error = $res['return_message'];
                        $text = View::make('slack.error.error', compact('error'))->render();
                        $status = false;
                        $response = ["response_type" => "ephemeral", "replace_original" => $status, "text" => $text];	
                    }

                }
			} catch(Exception $e) {
				\Log::info('FollowupAction'.$e->getMessage());
				$error = SlackConstants::SOMETHING_WENT_WRONG;
				$text = View::make('slack.error.error', compact('error'))->render();
				$status = false;
				$response = ["response_type" => "ephemeral", "replace_original" => $status, "text" => $text];
			}
		}
		return ["user_id" => $user->id, "reference_id" => "Lead Id ".$leadId, 'response' => $response];
	}
}