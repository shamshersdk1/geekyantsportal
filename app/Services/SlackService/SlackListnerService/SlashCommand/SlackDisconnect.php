<?php
namespace App\Services\SlackService\SlackListnerService\SlashCommand;
use App\Services\SlackService\SlackListnerService\SlashCommandInterface;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

use App\Models\Admin\SlackUser;
use App\Models\User;
use App\Models\Admin\ProjectSlackChannel;
use App\Models\Admin\Project;
use App\Models\Admin\ProjectResource;
use App\Models\Timesheet;
use App\Models\TimesheetDetail;

use App\Services\SlackService\SlackConstants;

use Exception;
use View;

class SlackDisconnect implements SlashCommandInterface
{
    public static function help() {
        return [["title" => "Disconnect this channel from the existing project",
                "text" => "/portal disconnect"]];
    }
    public static function main(Request $request = null)
    {
        $slack_id = $request->input('user_id');
        $requestedBy = SlackUser::with('users')->where('slack_id', $slack_id)->first();
        if(!$requestedBy || !$requestedBy->user || !$requestedBy->user->isAdmin() ) {
            return ["response_type" => "ephemeral", "text" => SlackConstants::ACCESS_DENIED];
        }
        $channel_id = $request->input('channel_id');
        $projectSlack = ProjectSlackChannel::with('project')->where('slack_id', $channel_id)->first();
        $projects = Project::select('project_name as text', 'id as value')->whereIn('status', ['open', 'in_progress', 'free'])->get()->toArray();
        $options = json_decode(json_encode($projects), true);
        if(!empty($projectSlack) && !empty($projectSlack->project)) {
            $project = $projectSlack->project;
            $confirm = ["title" => "Are you sure?", "text" => $project->project_name." is already connected to this channel", "ok_text" => "Yes", "dismiss_text" => "No"];
            
            $actions = [["name" => "project_disconnect",
                         "text" => "Disconnect",
                         "type" => "button",
                         "style" => "primary",
                         "value" => "disconnect"],
                        ["name" => "project_disconnect",
                         "text" => "Cancel",
                         "type" => "button",
                         "style" => "danger",
                         "value" => "cancel"]];

            $response = ["text" => "",
                         "response_type" => "ephemeral",
                         "attachments" => [["text" => "Are you sure you want to disconnect this channel from ".$project->project_name,
                                            "attachment_type" => "default",
                                            "callback_id" => "project_disconnect",
                                            "actions" => $actions ]]];
        } else {
            $error = SlackConstants::CHANNEL_NOT_LINKED;
            $text = View::make('slack.error.error', compact('error'))->render();
            return ["response_type" => "ephemeral", "text" => $text];
        }
        return $response;
    }
}
