<?php
namespace App\Services\SlackService\SlackListnerService\SlashCommand;
use App\Services\SlackService\SlackListnerService\SlashCommandInterface;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

use App\Models\Admin\SlackUser;
use App\Models\User;
use App\Models\Admin\LateComer;

use App\Services\SlackService\SlackConstants;
use App\Services\SlackService\SlackApiServices;
use App\Services\LateComerService;

use Exception;
use View;

class SlackRemovelate implements SlashCommandInterface
{
    public static function help() {
        return [["title" => "Remove users from the late comers list",
                "text" => "/portal removelate @username1 @username2 ..."]];
    }
    public static function main(Request $request = null)
    {
        $channelId = $request->input('channel_id');
        $slack_id = $request->input('user_id');
        $requestedBy = SlackUser::with('users')->where('slack_id', $slack_id)->first();
        if(!$requestedBy || !$requestedBy->user) {
            return ["response_type" => "ephemeral", "text" => SlackConstants::ACCESS_DENIED];
        }
        if ( !($requestedBy->user->hasRole('admin') || $requestedBy->user->hasRole('human-resources')) )
        {
            return ["response_type" => "ephemeral", "text" => SlackConstants::ACCESS_DENIED];
        }
        $current_date = date("Y-m-d");
        $message = $request->input('text');
        $arr = explode(' ',trim($message));
        unset($arr[0]);
        $result = [];
        foreach($arr as $item)
        {
            if(strlen($item) > 0 && $item[0] == '@') {
                $name = str_replace(["@", ","], "", $item);
                $slackUser = SlackUser::with('users')->where('name', $name)->first();
                if($slackUser && $slackUser->user) {
                    $user = $slackUser->user;
                    
                    $lateComerObj = LateComer::where('user_id',$user->id)->where('date',$current_date)->first();
                    if ( empty($lateComerObj) )
                    {
                        $result[] = SlackConstants::NOT_FOUND_IN_DB;
                    }
                    $deleteResponse = LateComerService::delete($lateComerObj->id);

                    if(!$deleteResponse['status']) {
                        $result[] = $lateComerObj->getErrors();
                    } else {
                        $text = SlackConstants::userRemovedFromLateList($name);
                        $result[] = $text;
                    }
                    
                } else {
                    $result[] = SlackConstants::userNotFound($name);
                }
            }
        }
        $message =  View::make('slack.add.list', compact('result'))->render();
        return ["response_type" => "ephemeral", "text" => $message];
    }
}