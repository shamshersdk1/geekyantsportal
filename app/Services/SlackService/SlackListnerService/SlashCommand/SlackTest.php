<?php
namespace App\Services\SlackService\SlackListnerService\SlashCommand;
use App\Services\SlackService\SlackListnerService\SlashCommandInterface;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

use App\Models\Admin\SlackUser;
use App\Models\User;
use App\Models\Admin\ProjectSlackChannel;
use App\Models\Admin\ProjectResource;
use App\Models\Timesheet;
use App\Models\TimesheetDetail;

use App\Services\SlackService\SlackConstants;

use Exception;

class SlackTest implements SlashCommandInterface
{
    public static function help() {
        return [["title" => "Just a test message",
                "text" => "/portal test"]];
    }
    public static function main(Request $request = null)
    {
        return "it worked";
    }
}