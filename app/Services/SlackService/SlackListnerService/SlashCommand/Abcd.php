<?php
namespace App\Services\SlackService\SlackListnerService\SlashCommand;
use App\Services\SlackService\SlackListnerService\SlashCommandInterface;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

use App\Models\Admin\SlackUser;
use App\Models\User;
use App\Models\Admin\ProjectSlackChannel;
use App\Models\Admin\ProjectResource;
use App\Models\Timesheet;
use App\Models\TimesheetDetail;

use App\Services\SlackService\SlackConstants;

use Exception;
use View;

class Abcd implements SlashCommandInterface
{
    public static function help() {
        return [
                ["title" => "Fill the timesheet for today for the project associated with this channel",
                "text" => "/portal timesheet [duration in hours] description of the task"],
                ["title" => "Get the link to fill the timesheet",
                "text" => "/portal timesheet"],
                ["title" => "Get the information about timesheet for the given date(default date is today)",
                "text" => "/portal timesheet view date(optional)"]
            ];
    }
    public static function main(Request $request = null)
    {
        $channelId = $request->input('channel_id');
        $channelName = $request->input('channel_name');
        $userName = $request->input('user_name');
        $slackUserId = $request->input('user_id');
        $current = date('Y-m-d');

        $data = [];
        $message = $request->input('text');
        $arr = explode(' ',trim($message));
        unset($arr[0]);
        $message = implode(" ", $arr);
        
        $userId;
        $user = null;
        $slackUser = SlackUser::where('slack_id',$slackUserId)->first();
        if(!empty($slackUser) && !empty($slackUser->user)) {
            $user = $slackUser->user;
        }
        if(empty($user)) {
            try{
                $client = new Client(['base_uri' => config('app.slack_url')]);
                $response = $client->request("POST", "users.info", [
                    'form_params' => [
                        'token' => config('app.slack_token'),
                        'user' => $slackUserId
                        ]
                ]);
            } catch(Exception $e) {
                $error = SlackConstants::apiMethodDidnotWork("users.info");
                $message = View::make('slack.timesheet.timesheet-error', compact('error'))->render();
                return ["response_type" => "ephemeral", "text" => $message];  
            }
            $userCreateResponse =  $response->getBody();
            $response = json_decode($userCreateResponse);
            $slackUser = !empty($response) && (!empty($response->user)) ? $response->user : null ;
            
            if($slackUser) {
                $data['email'] = $slackUser->profile->email;
            }
            if(!empty($data['email'])) {
                $slackEmail = User::where('email',$data['email'])->first();
                if(!$slackEmail) {
                    $error = SlackConstants::NOT_FOUND_IN_DB;
                    $message = View::make('slack.timesheet.timesheet-error', compact('error'))->render();
                    $error = SlackConstants::CONTACT_RM;
                    $attachmentText = View::make('slack.timesheet.timesheet-error', compact('error'))->render();
                    return ["response_type" => "ephemeral", "text" => $message, "attachments" => [["text" => $attachmentText]], "icon_emoji" => ":fearful:" ];  
                } else {
                    $userId = $slackEmail->id;
                    $user = $slackEmail;
                }
            } else {
                $error = SlackConstants::NOT_FOUND_IN_SLACK;
                $message = View::make('slack.timesheet.timesheet-error', compact('error'))->render();
                $error = SlackConstants::CONTACT_RM;
                $attachmentText = View::make('slack.timesheet.timesheet-error', compact('error'))->render();
                return ["response_type" => "ephemeral", "text" => $message, "attachments" => [["text" => $attachmentText]], "icon_emoji" => ":fearful:" ];  
            }
        } else {
            $userId = $user->id;
        }
        $projectSlack = ProjectSlackChannel::with('project')->where('slack_id',$channelId)->first();
        if(!$projectSlack || empty($projectSlack->project)){
            $error = SlackConstants::CHANNEL_NOT_LINKED;
            $message = View::make('slack.timesheet.timesheet-error', compact('error'))->render();
            return ["response_type" => "ephemeral", "text" => $message]; 
        }
        $is_allowed = ProjectResource::where('user_id',$userId)->where('project_id',$projectSlack->project->id)->where(function ($query) use ($current) {
            $query->where('end_date', '>=', $current)->orWhere('end_date', null);
        })->first();
        if(!$is_allowed) {
            $error = SlackConstants::NOT_IN_PROJECT;
            $message = View::make('slack.timesheet.timesheet-error', compact('error'))->render();
            return ["response_type" => "ephemeral", "text" => $message];  
        }
        if(trim($message) == "") {
            $portal_url = config('app.geekyants_portal_url');
            $id = $projectSlack->project->id;
            return View::make('slack.timesheet.timesheet-link', compact('portal_url', 'id'))->render();
        }
        
        $arr = explode(' ',trim($message));
        if(strtolower($arr[0]) == "view") {
            $response = SlackTimesheet::viewTimesheet($message, $userId, $projectSlack->project->id);
            return $response;
        }


        $timePos = strrpos($message,'[');
        $timeLastPos = strrpos($message,']');
        
        $taskPos = strpos($message,']');
        $taskLastPos = strlen($message) - 1;

        if((!empty($timePos) || $timePos == 0) && $timeLastPos && $timeLastPos > $timePos+1){
            $length = $timeLastPos - $timePos - 1;
            try
            {
                $num = (float)substr($message,$timePos+1,$length);
                if(!preg_match('/^\d*(\.\d{0,2})?$/', $num)) {
                    $error = SlackConstants::INVALID_DURATION;
                    $message = View::make('slack.timesheet.timesheet-error', compact('error'))->render();
                    return ["response_type" => "ephemeral", "text" => $message];
                }
                $time = $num;
            } 
            catch(Exception $e)
            {
                return ["response_type" => "ephemeral", "text" => $e->getMessage()];  
            }
        } else {
            $error = SlackConstants::DURATION_NOT_FOUND;
            $message = View::make('slack.timesheet.timesheet-error', compact('error'))->render();
            return ["response_type" => "ephemeral", "text" => $message];
        }
        if($taskPos && $taskLastPos && $taskLastPos > $taskPos){
            $length = $taskLastPos - $taskPos;
            $desc = substr($message,$taskPos+1,$length);
            if (preg_match('/[\'^"£$%&*()}{@#~?><>,|=_+¬-]/', $desc)) {
                return ["response_type" => "ephemeral", "text" => "Special characters not allowed !"];
            }
        } else {
            $error = SlackConstants::DESCRIPTION_NOT_FOUND;
            $message = View::make('slack.timesheet.timesheet-error', compact('error'))->render();
            return ["response_type" => "ephemeral", "text" => $message];
        }
        
        $timesheet = Timesheet::where('date', $current)->where('user_id', $userId)->where('project_id', $projectSlack->project->id)->first();
        $timesheetId;
        if(!empty($timesheet->id)) {
            if (isset($timesheet->approver_id) ) {
                return ["response_type" => "ephemeral", "text" => "your timesheet for today has already been approved"];
            }
            $timesheetId = $timesheet->id;
        } else {
            try{
                $timesheetObj = new Timesheet();
                $timesheetObj->user_id = $userId;
                $timesheetObj->project_id = $projectSlack->project->id;
                $timesheetObj->date = $current;
                $timesheetObj->hours = 0;
                $timesheetObj->save();
                $timesheetId = $timesheetObj->id;
            } catch (Exception $e) {
                $error = SlackConstants::unableToSaveTimesheet(config('app.geekyants_portal_url'));
                $message = View::make('slack.timesheet.timesheet-error', compact('error'))->render();
                return ["response_type" => "ephemeral", "text" => $message, "attachments" => [["text" => $e->getMessage()]]];
            } 
        }
        
        if($projectSlack && $user){
            try {
                $timesheetDetail = new TimesheetDetail();
                $timesheetDetail->timesheet_id = $timesheetId;
                $timesheetDetail->duration = $time;
                $timesheetDetail->reason = trim($desc);
                $timesheetDetail->save();
                $timesheetDetail->timesheet->approved_hours = $timesheetDetail->timesheet->sumOfHours();
                $timesheetDetail->timesheet->save();
                $code = SlackConstants::timesheetSaved($timesheetDetail->reason, $timesheetDetail->duration, $timesheetDetail->created_at);
                $message = View::make('slack.timesheet.timesheet-success', compact('code'))->render();
                return ["response_type" => "ephemeral", "text" => $message];
            } catch (Exception $e) {
                $error = SlackConstants::unableToSaveTimesheet(config('app.geekyants_portal_url'));
                $message = View::make('slack.timesheet.timesheet-error', compact('error'))->render();
                return ["response_type" => "ephemeral", "text" => $message, "attachments" => [["text" => $e->getMessage()]]];
            }
        } else {
            $error = SlackConstants::PROJECT_NOT_FOUND;
            $message = View::make('slack.timesheet.timesheet-error', compact('error'))->render();
            return ["response_type" => "ephemeral", "text" => $message]; 
        }
    }
    public static function viewTimesheet($message, $userId, $projectId)
    {
        $message = str_ireplace("view", '', $message);
        $response = ["response_type" => "ephemeral", "text" => ""]; 
        if(trim($message) == "") {
            $date = date("Y-m-d");
        } else {
            try
            {
                $date = date("Y-m-d", strtotime($message));
            } catch(Exception $e) {
                $response['text'] = $e->getMessage();
                return $response;
            }
        }
        if($date == "1970-01-01") {
            $response['text'] = SlackConstants::ENTER_VALID_DATE;
            return $response;
        }
        $timesheet = Timesheet::with('timesheet_detail')->where('date', $date)->where('user_id', $userId)->where('project_id', $projectId)->first();
        if(empty($timesheet) || empty($timesheet->timesheet_detail))
        {
            $response['text'] = SlackConstants::noTimesheetEntriesFound($date);
            return $response;
        }
        $details = $timesheet->timesheet_detail;
        $attachments = [];
        $total = 0;
        foreach($details as $task)
        {
            $attachments[] = ["text" => trim($task->reason).", [".$task->duration." hours]"];
            $total = $total + $task->duration;
        }
        $attachments[] = ["text" => "Total time spent: *".$total."* hours"];
        return ["response_type" => "ephemeral", "text" => "Your timesheet details for: *".date_in_view($date)."*", "attachments" => $attachments];
    }
}