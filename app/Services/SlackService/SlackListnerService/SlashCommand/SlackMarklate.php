<?php
namespace App\Services\SlackService\SlackListnerService\SlashCommand;
use App\Services\SlackService\SlackListnerService\SlashCommandInterface;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

use App\Models\Admin\SlackUser;
use App\Models\User;
use App\Models\Admin\LateComer;

use App\Services\SlackService\SlackConstants;
use App\Services\SlackService\SlackApiServices;
use App\Services\LateComerService;

use Exception;
use View;

class SlackMarklate implements SlashCommandInterface
{
    public static function help() {
        return [["title" => "Add users to the late comers list",
                "text" => "/portal marklate @username1 @username2 ..."]];
    }
    public static function main(Request $request = null)
    {
        $channelId = $request->input('channel_id');
        $slack_id = $request->input('user_id');
        $requestedBy = SlackUser::with('users')->where('slack_id', $slack_id)->first();
        if(!$requestedBy || !$requestedBy->user) {
            return ["response_type" => "ephemeral", "text" => SlackConstants::ACCESS_DENIED];
        }
        if ( !($requestedBy->user->hasRole('admin') || $requestedBy->user->hasRole('human-resources')) )
        {
            return ["response_type" => "ephemeral", "text" => SlackConstants::ACCESS_DENIED];
        }
        $message = $request->input('text');
        $arr = explode(' ',trim($message));
        unset($arr[0]);
        $result = [];
        foreach($arr as $item)
        {
            if(strlen($item) > 0 && $item[0] == '@') {
                $name = str_replace(["@", ","], "", $item);
                $slackUser = SlackUser::with('users')->where('name', $name)->first();
                if($slackUser && $slackUser->user) {
                    $user = $slackUser->user;
                    $date = date("Y-m-d");
                    $lateComerSave = LateComerService::saveData($user->id, $date, $requestedBy->user->id);
                    if(!$lateComerSave['status']) {
                        $result[] = $lateComerSave['message'];

                    } else {
                        $text = SlackConstants::userAddedToLateList($name);
                        $result[] = $text;
                    }
                } else {
                    $result[] = SlackConstants::userNotFound($name);
                }
            }
        }
        $message =  View::make('slack.add.list', compact('result'))->render();
        return ["response_type" => "ephemeral", "text" => $message];
    }
}