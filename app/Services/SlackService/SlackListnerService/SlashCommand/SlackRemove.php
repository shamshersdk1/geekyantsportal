<?php
namespace App\Services\SlackService\SlackListnerService\SlashCommand;
use App\Services\SlackService\SlackListnerService\SlashCommandInterface;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

use App\Models\Admin\SlackUser;
use App\Models\User;
use App\Models\Admin\ProjectSlackChannel;
use App\Models\Admin\ProjectResource;
use App\Models\Timesheet;
use App\Models\TimesheetDetail;

use App\Services\SlackService\SlackConstants;
// use App\Services\SlackService\SlackApiServices;

use View;
use Exception;

class SlackRemove implements SlashCommandInterface
{
    public static function help() {
        return [["title" => "Remove a user(s) from the project associated with this channel",
                "text" => "/portal remove @username1 @username2 ..."]];
    }
    public static function main(Request $request = null)
    {
        $channelId = $request->input('channel_id');
        $slack_id = $request->input('user_id');
        $requestedBy = SlackUser::with('users')->where('slack_id', $slack_id)->first();
        if(!$requestedBy || !$requestedBy->user || !$requestedBy->user->isAdmin() ) {
            return ["response_type" => "ephemeral", "text" => SlackConstants::ACCESS_DENIED];
        }
        $projectSlack = ProjectSlackChannel::with('project')->where('slack_id',$channelId)->first();
        if(!$projectSlack || empty($projectSlack->project)){
            $error = SlackConstants::CHANNEL_NOT_LINKED;
            $message = View::make('slack.error.error', compact('error'))->render();
            return ["response_type" => "ephemeral", "text" => $message]; 
        }
        $project = $projectSlack->project;
        $message = $request->input('text');
        $arr = explode(' ',trim($message));
        unset($arr[0]);
        $result = [];
        foreach($arr as $item)
        {
            if(strlen($item) > 0 && $item[0] == '@') {
                $name = str_replace(["@", ","], "", $item);
                $slackUser = SlackUser::with('users')->where('name', $name)->first();
                if($slackUser && $slackUser->user) {
                    $user = $slackUser->user;
                    $resource = ProjectResource::where('project_id', $project->id)->where('user_id', $user->id)->where(function ($query){
                        $query->where('end_date', null)->orWhere('end_date', '>', date('Y-m-d'));
                    })->first();
                    if(!$resource) {
                        $result[] = SlackConstants::userNotAdded($name);
                    } else {
                        $resource->end_date = date("Y-m-d");
                        $resource->updated_at = date("Y-m-d H:i:s");
                        if(!$resource->save()) {
                            $result[] = $resource->getErrors();
                        } else {
                            $text = SlackConstants::userRemoved($name);
                            $result[] = $text;
                            // $response = SlackApiServices::removeUser($channelId, $slackUser->slack_id);
                        }
                    }
                } else {
                    $result[] = SlackConstants::userNotFound($name);
                }
            }
        }
        $message =  View::make('slack.remove.list', compact('result'))->render();
        return ["response_type" => "ephemeral", "text" => $message];
    }
}