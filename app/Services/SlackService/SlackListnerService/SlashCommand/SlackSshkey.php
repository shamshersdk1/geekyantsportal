<?php
namespace App\Services\SlackService\SlackListnerService\SlashCommand;
use App\Services\SlackService\SlackListnerService\SlashCommandInterface;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

use App\Models\Admin\SlackUser;
use App\Models\User;
use App\Models\SshKey;

use App\Services\SlackService\SlackConstants;
use App\Services\SlackService\SlackApiServices;
use App\Services\LateComerService;
use App\Jobs\Slack\SlackReminder\SshKey\NotifyUserToUpdate;

use Exception;
use View;

class SlackSshkey implements SlashCommandInterface
{
    public static function help() {
        return [["title" => "Display ssh-key of the user",
                "text" => "/portal ssh-key {@username1} {@username2} ..."]];
    }
    public static function main(Request $request = null)
    {
        $channelId = $request->input('channel_id');
        $slack_id = $request->input('user_id');
        $requestedBy = SlackUser::with('users')->where('slack_id', $slack_id)->first();
        if(!$requestedBy || !$requestedBy->user) {
            return ["response_type" => "ephemeral", "text" => SlackConstants::ACCESS_DENIED];
        }
        $message = $request->input('text');
        $arr = explode(' ',trim($message));
        unset($arr[0]);
        $result = [];
        \Log::info(json_encode($arr));
        if(count($arr) == 0 )
        {
            if ( $requestedBy  && $requestedBy->user )
            {
                $user = $requestedBy->user;
                $res = SshKey::getSshKey($user->id);
                if( $res['status'] ) {
                    $text  = "Your ssh-key ```".$res['message']. "```";
                    $result[] = $text;
                    $url = config('app.geekyants_portal_url')."/admin/ssh-keys/my-ssh-key/edit";
                    $actions = [["text" => "Click here to update your ssh-key", "type" => "button", "url" => $url]];
                    $message =  View::make('slack.add.list', compact('result'))->render();
                    $attachment = [["text" => "", "fallback" => "Goto the portal to check the request", "attachment_type" => "default", "callback_id" => "request_action_user", "actions" => $actions ]];
                    return ["response_type" => "ephemeral", "text" => $message, "attachments" => $attachment];
                    
                }
                else {
                    $message = "Your ".$res['message'];
					$url = config('app.geekyants_portal_url')."/admin/ssh-keys/my-ssh-key/add";
					$text = View::make('slack.action', compact('message'))->render();
					$actions = [["text" => "Click here to configure your ssh-key on portal", "type" => "button", "url" => $url]];
                	$attachment = [["text" => "", "fallback" => "Goto the portal to check the request", "attachment_type" => "default", "callback_id" => "request_action_user", "actions" => $actions ]];
                    $status = true;
                    return  ["response_type" => "ephemeral", "replace_original" => $status, "text" => $text, "attachments" => $attachment];
                }
            } else {
                $result[] = SlackConstants::userNotFound($name);
            }
            
        }
        foreach($arr as $item)
        {
            if(strlen($item) > 0 && $item[0] == '@') 
            {
                $name = str_replace(["@", ","], "", $item);
                $slackUser = SlackUser::with('users')->where('name', $name)->first();
                if($slackUser && $slackUser->user) {
                    $user = $slackUser->user;
                    $date = date("Y-m-d");
                    $res = SshKey::getSshKey($user->id);
                    if( $res['status'] )
                    {
                        $text  = "*".$user->name."'s* ssh-key ```".$res['message']."```" ;
                    }
                    else
                    {
                        $text = "*".$user->name."* has not updated his ssh-key on portal";
                    }
                    $result[] = $text;
                } else {
                    $result[] = SlackConstants::userNotFound($name);
                }
            }
        }
        $message =  View::make('slack.add.list', compact('result'))->render();
        return ["response_type" => "ephemeral", "text" => $message];
    }
}