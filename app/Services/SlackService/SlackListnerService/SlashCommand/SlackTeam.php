<?php
namespace App\Services\SlackService\SlackListnerService\SlashCommand;
use App\Services\SlackService\SlackListnerService\SlashCommandInterface;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

use App\Models\Admin\ProjectSlackChannel;

use App\Services\SlackService\SlackConstants;

use Exception;
use View;

class SlackTeam implements SlashCommandInterface
{
    public static function help() {
        return [["title" => "Get the information about the project associated with this channel",
                "text" => "/portal team"]];
    }
    public static function main(Request $request = null)
    {
        $channelId = $request->input('channel_id');
        $slackUserId = $request->input('user_id');
        $today = date('Y-m-d');

        $projectSlack = ProjectSlackChannel::with('project')->where('slack_id', $channelId)->first();
        if($projectSlack && $projectSlack->project) {
            $project = $projectSlack->project;
            $manager = $project->projectManager;
            $resources = $project->resources()->where(function ($query) use($today) {
                $query->where('end_date', null)->orWhere('end_date', '>=', $today);
            })->get();
            
            $message = View::make('slack.team.message')->render();
            if($manager) {
                $name = $manager->name;
            } else {
                $name = SlackConstants::MANAGER_NOT_FOUND;
            }
            $teamLeadArray = ["title" => "Project Manager",
                               "text" => $name];
            
            
            if($resources) {
                $names = View::make('slack.team.users', compact('resources'))->render();
                $names = substr($names, 0, -2);
            } else {
                $names = SlackConstants::USERS_NOT_FOUND;
            }

            $usersArray = ["title" => "Developers",
                               "text" => $names];

            return ["response_type" => "ephemeral", "text" => $message, "attachments" => [$teamLeadArray, $usersArray]];                  
        } else {
            return ["response_type" => "ephemeral", "text" => SlackConstants::CHANNEL_NOT_LINKED];
        }
    }
}