<?php
namespace App\Services\SlackService\SlackListnerService\SlashCommand;
use App\Services\SlackService\SlackListnerService\SlashCommandInterface;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

use App\Models\Admin\SlackUser;
use App\Models\User;
use App\Models\Admin\ProjectSlackChannel;
use App\Models\Admin\ProjectResource;
use App\Models\Timesheet;
use App\Models\TimesheetDetail;

use App\Services\SlackService\SlackConstants;
use App\Services\TimesheetService;

use Exception;
use View;

class SlackTimesheet implements SlashCommandInterface
{
    public static function help() {
        return [
                ["title" => "Fill the timesheet for the project associated with this channel",
                "text" => "/portal timesheet [duration in hours] description of the task"],
                ["title" => "Get the link to fill the timesheet",
                "text" => "/portal timesheet"]
            ];
    }
    public static function main(Request $request = null)
    {
        $channelId = $request->input('channel_id');
        $channelName = $request->input('channel_name');
        $userName = $request->input('user_name');
        $slackUserId = $request->input('user_id');
        $current = date('Y-m-d');

        $data = [];
        $message = $request->input('text');
        $arr = explode(' ',trim($message));
        // unset($arr[0]);
        // $message = implode(" ", $arr);
        $slackUser = SlackUser::where('slack_id',$slackUserId)->first();
        if(empty($slackUser) && empty($slackUser->user)) 
        {
            $error = SlackConstants::NOT_FOUND_IN_DB;
            $message = View::make('slack.timesheet.timesheet-error', compact('error'))->render();
            return ["response_type" => "ephemeral", "text" => $message];     
        }
        $user = $slackUser->user;
        if ( count($arr) > 1 )
        {
            $tempTable = TimesheetService::saveToTempTable($channelId, $user->id, $message);
            if ( !$tempTable['status'] )
            {
                $error = $tempTable['message'];
                $message = View::make('slack.timesheet.timesheet-error', compact('error'))->render();
                return ["response_type" => "ephemeral", "text" => $message]; 
            }
            $response = TimesheetService::sendSingleResponse($tempTable['message']);
            return $response;
        }
        else
        {
            $response = TimesheetService::sendMulipleResponse($channelId, $userName, $slackUserId);
            return $response;
        }
    }
}