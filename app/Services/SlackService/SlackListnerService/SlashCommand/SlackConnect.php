<?php
namespace App\Services\SlackService\SlackListnerService\SlashCommand;
use App\Services\SlackService\SlackListnerService\SlashCommandInterface;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

use App\Models\Admin\SlackUser;
use App\Models\User;
use App\Models\Admin\ProjectSlackChannel;
use App\Models\Admin\Project;
use App\Models\Admin\ProjectResource;
use App\Models\Timesheet;
use App\Models\TimesheetDetail;

use App\Services\SlackService\SlackConstants;

use Exception;
use View;

class SlackConnect implements SlashCommandInterface
{
    public static function help() {
        return [["title" => "Assign this channel to an existing project",
                "text" => "/portal connect"]];
    }
    public static function main(Request $request = null)
    {
        $slack_id = $request->input('user_id');
        $requestedBy = SlackUser::with('users')->where('slack_id', $slack_id)->first();
        if(!$requestedBy || !$requestedBy->user || !$requestedBy->user->isAdmin() ) {
            return ["response_type" => "ephemeral", "text" => SlackConstants::ACCESS_DENIED];
        }
        $channelId = $request->input('channel_id');
        $projectSlack = ProjectSlackChannel::with('project')->where('slack_id', $channelId)->first();
        $projects = Project::select('project_name as text', 'id as value')->whereIn('status', ['open', 'in_progress', 'free'])->get()->toArray();
        $options = json_decode(json_encode($projects), true);
        if(!empty($projectSlack) && !empty($projectSlack->project)) {
            $project = $projectSlack->project;
            $confirm = ["title" => "Are you sure?", "text" => $project->project_name." is already connected to this channel", "ok_text" => "Yes", "dismiss_text" => "No"];
            $actions = [["name" => "projects_list", "text" => "Select a project", "type" => "select", "options" => $options, "confirm" => $confirm]];
        } else {
            $actions = [["name" => "projects_list", "text" => "Select a project", "type" => "select", "options" => $options]];
        }
        $response = ["text" => "Select a project from the list below.", "response_type" => "ephemeral",  "attachments" => [["text" => "", "attachment_type" => "default", "callback_id" => "project_connect", "actions" => $actions ]]];
        return $response;
    }
}