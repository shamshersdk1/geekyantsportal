<?php
namespace  App\Services\SlackService\SlackListnerService;

interface SlashCommandInterface {
    public static function main();
    public static function help();
}
