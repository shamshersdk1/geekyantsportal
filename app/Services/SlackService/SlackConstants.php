<?php namespace App\Services\SlackService;

use Config;


class SlackConstants
{
    //  Use "" instead of ''
    const NO_RESPONSE = "Response not recieved"; 
    const NO_USER = "User not found or User's slack id not found";
    const NOT_FOUND_IN_DB = "Couldn't find your credentials in GeekyAnts DB";
    const NOT_FOUND_IN_SLACK = "Couldn't find your credentials in Slack";
    const CHANNEL_NOT_LINKED = "This channel is not linked to any project";
    const NOT_IN_PROJECT = "You are not a part of this project";
    const INVALID_DURATION = "The duration you have entered is invalid";
    const CONTACT_RM = "Contact your Reporting Manager";
    const DURATION_NOT_FOUND = "You didn't enter the duration or used invalid format";
    const DESCRIPTION_NOT_FOUND = "Please provide some task description";
    const PROJECT_NOT_FOUND = "Project not found in GeekyAnts DB";
    const TIMESHEET_SET = "Timesheet has been set";
    const NO_COMMAND = "You didn't type a command, use /portal help for available options";
    const INCORRECT_FORMAT = "Message sent was in an incorrect format, use /portal help for available options";
    const MANAGER_NOT_FOUND = "Could not find this manager for this project";
    const USERS_NOT_FOUND = "Could not find any users for this project";
    const ENTER_VALID_DATE = "Please enter a valid date";
    const ACCESS_DENIED = "You don't have the permission to perform this task";
    const CHANNEL_ALREADY_ASSIGNED = "This channel is already assigned to another project";
    const CHANNEL_NOT_FOUND = "Channel not found in slack";
    const SOMETHING_WENT_WRONG = "Something went wrong";
    
    const SLASH_COMMAND_NAMESPACE =  "\App\Services\SlackService\SlackListnerService\SlashCommand";
    const INTERACTIVE_COMPONENT_NAMESPACE =  "\App\Services\SlackService\SlackListnerService\InteractiveComponent";

    public static function unableToSaveTimesheet($portal_url)
    {
        return "Unable to save your timesheet, try to save it through\n".$portal_url."/user/timesheet";
    }

    public static function apiMethodDidnotWork($method_name)
    {
        return "Slack API\'s ".$method_name." method didn't work";
    }

    public static function timesheetSaved($desc, $duration, $time)
    {
        return "Your timesheet has been updated for your task ".$desc." with ".$duration." hours as your timespent on it (Saved at ".datetime_in_view($time).")";
    }

    public static function noTimesheetEntriesFound($date)
    {
        return "You dont have any timesheet entry for: ".date_in_view($date);
    }

    public static function userExists($name)
    {
        return "@".$name." is already assigned to this project";
    }

    public static function userNotFound($name)
    {
        return "@".$name." not found in GeekyAnts DB";
    }

    public static function userAdded($name)
    {
        return "@".$name." has been added to the project";
    }

    public static function userNotAdded($name)
    {
        return "@".$name." is not assigned to this project";
    }

    public static function userRemoved($name)
    {
        return "@".$name." has been removed from the project, use ```/remove @".$name."``` to remove user from this channel";
    }

    public static function projectAdded($name)
    {
        return $name." has been connected to this channel";
    }

    public static function projectRemoved($name)
    {
        return $name." has been disconnected from this channel";
    }

    public static function userAddedToLateList($name)
    {
        return "@".$name." has been added to the late comers list";
    }
    public static function userRemovedFromLateList($name)
    {
        return "@".$name." has been removed from the late comers list";
    }
    public static function userAlreadyAddedInLateList($name)
    {
        return "@".$name." is already added in the late comers list";
    }
}