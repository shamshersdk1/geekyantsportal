<?php namespace App\Services\SlackService\SlackMessageService;

use App\Models\AccessLog;
use App\Models\User;
use App\Services\SlackService\SlackConstants;
use Exception;
use GuzzleHttp\Client;

class SlackMessageService
{
    public static function sendMessageToUser($userId, $message, $attachments = null)
    {
        $result = ['status' => false, 'error_code' => '', 'message' => ''];
        $user = User::find($userId);
        if (!empty($user) && !empty($user->slackUser) && !empty($user->slackUser->slack_id)) {
            try
            {

                if ($attachments != null) {
                    $attachments = json_encode($attachments);
                }
                $responseText = null;
                $client = new Client(['base_uri' => config('app.slack_url')]);
                $response = $client->request("POST", "chat.postMessage", [
                    'form_params' => [
                        'token' => config('app.slack_bot_token'),
                        'channel' => '@' . $user->slackUser->slack_id,
                        'text' => $message,
                        'attachments' => $attachments,
                        'as_user' => true,
                    ],
                ]);
                $responseText = json_decode($response->getBody());
                if (empty($responseText)) {
                    $result['message'] = SlackConstants::NO_RESPONSE;
                    return $result;
                } elseif ($responseText->ok === false) {
                    $result['message'] = $responseText->error;
                    return $result;
                }
            } catch (Exception $e) {
                AccessLog::accessLog(null, 'App\Services\SlackService\SlackMessageService', 'SlackMessageService', 'sendMessageToUser', 'catch-block', $e->getMessage());
                $result['message'] = $e->getMessage();
                return $result;
            }
        } else {
            $result['message'] = SlackConstants::NO_USER;
        }
        $result['status'] = true;
        return $result;
    }

    public static function sendMessageToChannel($channelId, $message, $linkNames = null, $asUser = true, $attachments = null)
    {
        $result = ['status' => false, 'error_code' => '', 'message' => ''];
        try
        {
            $responseText = null;
            if ($linkNames) {
                $linkNames = true;
            } else {
                $linkNames = false;
            }
            if ($attachments != null) {
                $attachments = json_encode($attachments);
            }
            $client = new Client(['base_uri' => config('app.slack_url')]);
            $response = $client->request("POST", "chat.postMessage", [
                'form_params' => [
                    'token' => config('app.slack_bot_token'),
                    'channel' => $channelId,
                    'text' => $message,
                    'as_user' => $asUser,
                    'link_names' => $linkNames,
                    'attachments' => $attachments,
                ],
            ]);

            $responseText = json_decode($response->getBody());
            if (empty($responseText)) {
                $result['message'] = SlackConstants::NO_RESPONSE;
                return $result;
            } elseif ($responseText->ok === false) {
                $result['message'] = $responseText->error;
                return $result;
            }
        } catch (Exception $e) {
            AccessLog::accessLog(null, 'App\Services\SlackService\SlackMessageService', 'SlackMessageService', 'sendMessageToChannel', 'catch-block', $e->getMessage());
            $result['message'] = $e->getMessage();
            return $result;
        }
        $result['status'] = true;
        return $result;
    }
    
}
