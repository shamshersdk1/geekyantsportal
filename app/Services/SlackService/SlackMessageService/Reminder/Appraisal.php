<?php namespace App\Services\SlackService\SlackMessageService\Reminder;

use App\Models\Admin\Appraisal as AdminAppraisal;

use App\Models\Admin\ProjectResource;
use App\Models\Admin\Project;
use App\Jobs\Slack\SlackReminder\AppraisalReminder;

use Exception;
use View;

class Appraisal
{
    public static function remind($id)
    {
        $appraisal = AdminAppraisal::with('user')->find($id);
        if($appraisal && $appraisal->user) {
            $user = $appraisal->user;
            $url = config('app.geekyants_portal_url')."/admin/users/".$user->id."/appraisals";
            $today = date('Y-m-d');
            $resources = ProjectResource::where('user_id', $user->id)->where('start_date', '<=', $today)->where(function ($query) use ($today){
                            $query->where('end_date', null)->orWhere('end_date', '>=', $today);
                        })->pluck('project_id')->toArray();
            $projects = Project::with('projectManager')->whereIn('id', $resources)->get();
            $teamLeads = [];
            foreach($projects as $project)
            {
                if($project->projectManager) {
                    $teamLeads[] = ['id' => $project->projectManager->id, 'name' => $project->projectManager->name];
                }
            }
            if(count($teamLeads) > 0) {
                $teamLeads = array_map("unserialize", array_unique(array_map("serialize", $teamLeads)));
            }
            $reportingManager = $user->reportingManager;
            if($reportingManager) {
                $rmId = $user->reportingManager->id;
                $message = View::make('slack.reminder.appraisal.appraisal-reminder', ['manager' => $reportingManager->name, 'user' => $user->name, 'url' => $url])->render();
                $data = ["userId" => $reportingManager->id, "message" => $message];
                $job = (new AppraisalReminder($data))->onQueue('appraisal-reminder');
                dispatch($job);
            } else {
                $rmId = "-1";
            }
            foreach($teamLeads as $lead)
            {
                if($rmId == $lead['id'] && $lead['id'] != "-1") {
                    continue;
                }
                $message = View::make('slack.reminder.appraisal.appraisal-reminder', ['manager' => $lead['name'], 'user' => $user->name, 'url' => $url])->render();
                $data = ["userId" => $lead['id'], "message" => $message];
                $job = (new AppraisalReminder($data))->onQueue('appraisal-reminder');
                dispatch($job);
            }
        }
    }
}