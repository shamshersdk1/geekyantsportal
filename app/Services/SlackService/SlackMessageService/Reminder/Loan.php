<?php namespace App\Services\SlackService\SlackMessageService\Reminder;

use App\Models\Loan as LoanModel;
use App\Models\LoanEmi;
use App\Jobs\Slack\SlackReminder\LoanReminder;
use Exception;
use View;

class Loan
{
    public static function loanReminder($loanId)
    {
        $loan = LoanModel::with(['user'])->find($loanId);
        if($loan && $loan->user && $loan->user->reportingManager) {
            $user = $loan->user;
            $manager = $user->reportingManager->name;
            $user = $user->name;
            if($loan->status == 'pending') {
                $message = View::make('slack.reminder.loan.pending', compact('manager', 'user', 'loan'))->render();
            } elseif($loan->status == 'approved') {
                if (substr($user, -1) == "s") {
                    $username = $user."'";
                } else {
                    $username = $user."'s";
                }
                $message = View::make('slack.reminder.loan.approved', compact('manager', 'username', 'loan'))->render();
            }
            if(!empty($message)) {
                $data = ['userId' => $loan->user->id, 'message' => $message];
                $job = (new LoanReminder($data))->onQueue('loan-reminder');
                dispatch($job);
            }
        }
    }
    public static function emiReminder($emiId)
    {
        $emi = LoanEmi::with(['loan'])->find($emiId);
        if($emi->status == 'paid' && $emi->loan) {
            $loan = $emi->loan;
            if($loan && $loan->user && $loan->user->reportingManager) {
                $user = $loan->user;
                $manager = $user->reportingManager->name;
                if($user->id == $user->parent_id) {
                    $user = "you";
                } else {
                    $user = $user->name;
                }
                $message = View::make('slack.reminder.loan.emi-paid', compact('manager', 'user', 'emi'))->render();
                $data = ['userId' => $loan->user->id, 'message' => $message];
                $job = (new LoanReminder($data))->onQueue('loan-emi-reminder');
                dispatch($job);
            }
        }
    }
}