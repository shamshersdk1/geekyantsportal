<?php namespace App\Services\SlackService\SlackMessageService\Reminder;

use App\Models\User;
use App\Models\Admin\Calendar;
use App\Models\Admin\ProjectResource;

use App\Jobs\Slack\SlackReminder\TimesheetReminder;

use Exception;

class Timesheet
{
    public static function remind()
    {
        $today = date('Y-m-d');
        if(date('N', strtotime($today)) > 5 || Calendar::where('date', $today)->count()) {
            return;
        }
        if(date('N') < 4) {
            $start_date = date('Y-m-d', strtotime("-5 days", time()));
        } else {
            $start_date = date('Y-m-d', strtotime("-3 days", time()));
        }
        $holidays = Calendar::whereDate('date', '>=', $start_date)->whereDate('date', '<=', $today)->pluck('date')->toArray();
        foreach($holidays as $key => $date)
        {
            if(date('N', $date) > 5) {
                unset($holidays[$key]);
            }
        }
        while(count($holidays) != 0) {
            $temp = $start_date;
            $start_date = date('Y-m-d', strtotime("-".count($holidays)." days", strtotime($start_date)));
            $day = date('N', strtotime($start_date));
            if($day > 5) {
                $day = 5-$day;
                $start_date = date('Y-m-d', strtotime($day." days", strtotime($start_date)));
            }
            $holidays = Calendar::whereDate('date', '>=', $start_date)->whereDate('date', '<=', $end_date)->pluck('date')->toArray();
            foreach($holidays as $key => $date)
            {
                if(date('N', $date) > 5) {
                    unset($holidays[$key]);
                }
            }
        }
        $resources = ProjectResource::with('user_data')->where('start_date', '<=', $start_date)->where(function ($query) use ($today){
						$query->where('end_date', null)->orWhere('end_date', '>=', $today);
                    })->pluck('user_id')->toArray();
        $resources = array_unique($resources);
        $data = [];
        foreach($resources as $resource)
        {
            $activeProjects = ProjectResource::getActiveProjectIdArray($resource, $start_date, $today);
            $projectIds = [];
            foreach($activeProjects as $project)
            {   
                $timesheet = \App\Models\Timesheet::where('user_id', $resource)->where('date', '>=', $start_date)->where('date', '<=', $today)->where('project_id', $project)->count();
                if(empty($timesheet) || $timesheet == 0) {
                    $projectIds[] = $project;
                }
            }
            $data = ['user_id' => $resource, 'projects' => $projectIds]; 
            $job = (new TimesheetReminder($data))->onQueue('timesheet-reminder');
            dispatch($job);
        }
    }
}