<?php namespace App\Services\SlackService\SlackMessageService\Reminder;

use App\Jobs\Slack\SlackReminder\ProjectResourceReminder;
use App\Models\Admin\ProjectResource as ProjectResourceModel;
use Exception;
use View;
use Config;

class ProjectResource
{
    public static function userAdded($id)
    {
        $resourceObj = ProjectResourceModel::with(['project', 'resource'])->find($id);
        if($resourceObj && $resourceObj->project && $resourceObj->user_data) {
            $project = $resourceObj->project->project_name;
            $channelId = Config::get('slackchannels.projectResourceChannelId');
            if(!empty($channelId)) {
                $user = $resourceObj->user_data->name;
                $date = $resourceObj->start_date;
                $message = View::make('slack.reminder.project-resource.user-added', compact('user', 'project', 'date'))->render();
                $data = ['channelId' => $channelId, 'message' => $message];
                $job = (new ProjectResourceReminder($data))->onQueue('resource-added-reminder');
                dispatch($job);
            }
        }
    }
    public static function userReleased($id)
    {
        $resourceObj = ProjectResourceModel::with(['project', 'resource'])->find($id);
        if($resourceObj && $resourceObj->project && $resourceObj->user_data) {
            $project = $resourceObj->project->project_name;
            $channelId = Config::get('slackchannels.projectResourceChannelId');
            if(!empty($channelId)) {
                $user = $resourceObj->user_data->name;
                $date = $resourceObj->end_date;
                $message = View::make('slack.reminder.project-resource.user-released', compact('user', 'project', 'date'))->render();
                $data = ['channelId' => $channelId, 'message' => $message];
                $job = (new ProjectResourceReminder($data))->onQueue('resource-released-reminder');
                dispatch($job);
            }
        }
    }
}