<?php namespace App\Services\SlackService\SlackMessageService\Reminder;

use App\Models\User;
use App\Models\Admin\Project;
use App\Models\Admin\ProjectResource;
use App\Models\ProjectManagerLeaveApproval;

use App\Jobs\Slack\SlackReminder\LeaveReminder;

use Exception;

use View;

class Leave
{
    public static function remindUserAndTeamLead($leaveId)
    {
        $leave = \App\Models\Admin\Leave::find($leaveId);
        $user = $leave->user;
        $admin_url = config('app.geekyants_portal_url')."/admin/reporting-manager-leaves";
        $user_url = config('app.geekyants_portal_url')."/user/leaves";
        $resources = ProjectResource::where('user_id', $user->id)->where('start_date', '<=', $leave->end_date)->where(function ($query) use ($leave){
						$query->where('end_date', null)->orWhere('end_date', '>=', $leave->start_date);
					})->pluck('project_id')->toArray();
        $projects = Project::with('projectManager')->whereIn('id', $resources)->get();
        $teamLeads = [];
        foreach($projects as $project)
        {
            if($project->projectManager) {
                $teamLeads[] = ['id' => $project->projectManager->id, 'name' => $project->projectManager->name];
            }
        }
        if(count($teamLeads) > 0) {
            $teamLeads = array_map("unserialize", array_unique(array_map("serialize", $teamLeads)));
        }
        if($leave->status == 'pending') {
            // $message = View::make('slack.reminder.leave.application.leave-user', ['user' => $user->name, 'leave' => $leave, 'url' => $user_url])->render();
            // $data = ["userId" => $user->id, "message" => $message];
            // $job = (new LeaveReminder($data))->onQueue('leave-reminder');
            // dispatch($job);

            if($teamLeads && count($teamLeads) > 0) {
                foreach($teamLeads as $lead)
                {
                    if($lead['id'] == $user->id) {
                        continue;
                    }
                    if($user->reportingManager && $user->reportingManager->id == $lead['id']) {
                        continue;
                    }
                    $message = View::make('slack.reminder.leave.application.leave-team-lead', ['lead' => $lead['name'], 'user' => $user->name, 'leave' => $leave, 'url' => $admin_url])->render();
                    $actions = [["name" => "approved", "text" => "Approve", "type" => "button", "value" => $leave->id, "style" => "primary"],
                                ["name" => "rejected", "text" => "Reject", "type" => "button", "value" => $leave->id, "style" => "danger"],
                                ["text" => "View", "type" => "button", "url" => $admin_url]                               
                                ];
                    $attachment = [["text" => "Select an action", "fallback" => "Goto the portal to approve/reject the leave", "attachment_type" => "default", "callback_id" => "leave_action_team_lead", "actions" => $actions ]];
                    $data = ["userId" => $lead['id'], "message" => $message, "attachment" => $attachment, "as_bot" => true];
                    $job = (new LeaveReminder($data))->onQueue('leave-reminder');
                    dispatch($job);
                }
            }
        } else {
            if($leave->approver) {
                $approver = $leave->approver->name;
            } else {
                $approver = "*unavailable*";
            }
            if($approver == $user->name) {
                $approver = "you";
            }
            $message = View::make('slack.reminder.leave.notification.leave-user', ['user' => $user->name, 'leave' => $leave, 'approver' => $approver])->render();
            $data = ["userId" => $user->id, "message" => $message];
            $job = (new LeaveReminder($data))->onQueue('leave-reminder');
            dispatch($job);

            if($leave->approver) {
                $approver = $leave->approver->name;
            } else {
                $approver = "*unavailable*";
            }
            if (substr($user->name, -1)=="s") {
                $userName=$user->name."'";
            } else {
                $userName=$user->name."'s";
            }
            if(count($teamLeads) > 0) {
                foreach($teamLeads as $lead)
                {
                    if($lead['id'] == $user->id) {
                        continue;
                    }
                    if($user->reportingManager && $user->reportingManager->id == $lead['id']) {
                        continue;
                    }
                    if($approver == $lead['name']) {
                        $approver = "you";
                    }
                    $message = View::make('slack.reminder.leave.notification.leave-team-lead', ['lead' => $lead['name'], 'user' => $userName, 'leave' => $leave, 'url' => $admin_url, 'approver' => $approver])->render();
                    $data = ["userId" => $lead['id'], "message" => $message];
                    $job = (new LeaveReminder($data))->onQueue('leave-reminder');
                    dispatch($job);
                }
            }
        }
    }
    public static function remindReportingManager($leaveId)
    {
        $leave = \App\Models\Admin\Leave::find($leaveId);
        if($leave && $leave->user) {
            $user = $leave->user;
            $admin_url = config('app.geekyants_portal_url')."/admin/reporting-manager-leaves";
            if($user->reportingManager) {
                if($leave->status == 'pending') {
                    // $pendingApprovals = ProjectManagerLeaveApproval::where('leave_id', $leaveId)->where('status', 'pending')->count();
                    // if($pendingApprovals == 0) {
                        $message = View::make('slack.reminder.leave.application.leave-reporting-manager', ['manager' => $user->reportingManager->name, 'user' => $user->name, 'leave' => $leave, 'url' => $admin_url])->render();
                        $actions = [["name" => "approved", "text" => "Approve", "type" => "button", "value" => $leave->id, "style" => "primary"],
                                    ["name" => "rejected", "text" => "Reject", "type" => "button", "value" => $leave->id, "style" => "danger"],
                                    ["text" => "View", "type" => "button", "url" => $admin_url]                               
                                    ];
                        $attachment = [["text" => "Select an action", "fallback" => "Goto the portal to approve/reject the leave", "attachment_type" => "default", "callback_id" => "leave_action_reporting_manager", "actions" => $actions ]];
                        $data = ["userId" => $user->reportingManager->id, "message" => $message, "attachment" => $attachment, "as_bot" => true];
                        $job = (new LeaveReminder($data))->onQueue('leave-reminder');
                        dispatch($job);
                    // }
                } else {
                    if($leave->approver) {
                        if($leave->approver->id == $user->reportingManager->id) {
                            $approver = "you";
                        } else {
                            $approver = $leave->approver->name;
                        }
                    } else {
                        $approver = "*unavailable*";
                    }
                    if (substr($user->name, -1)=="s") {
                        $userName=$user->name."'";
                    } else {
                        $userName=$user->name."'s";
                    }
                    $message = View::make('slack.reminder.leave.notification.leave-reporting-manager', ['manager' => $user->reportingManager->name, 'user' => $userName, 'leave' => $leave, 'approver' => $approver])->render();
                    $data = ["userId" => $user->reportingManager->id, "message" => $message];
                    $job = (new LeaveReminder($data))->onQueue('leave-reminder');
                    dispatch($job);
                }
            }
        }
    }
}