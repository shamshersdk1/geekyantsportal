<?php namespace App\Services;

use App\Models\User;
use App\Models\AccessLog;

use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;

use View;
use Config;
use Exception;


class LeadsService {

    public static function requestAction($lead_id, $action, $user_id)
    {
        if ( $action == 'follow_up' )
        {
           $response = self::followUP($lead_id, $action, $user_id);
        }
        // else if ( $action == 'spam')
        // {
        //    $response = self::markAsSpam($lead_id, $action, $user_id);
        // }
        else if ( $action == 'close' )
        {
            $arr = explode("_", $lead_id);
            $date = $arr[0];
            $leadId = $arr[1]; 
            $response = self::updateLeadRecord($leadId, $date, $user_id);
        }
        else 
        {
           $response = self::markStatus($lead_id, $action, $user_id);
        }

        return $response;
    }

    public static function followUp($lead_id, $action, $user_id)
    {
        $status = true;
        $return_message = '';

        $assignedUser = User::find($user_id);
        $res = self::updateLeadFollowUP($lead_id, $user_id);

        if( $res['status'] )
        {
            $res = json_decode($res['responseBody'], true);
            $leadName = $res['result']['name'];
            $leadEmail = $res['result']['email'];
            $leadCompany = $res['result']['company'];
            $message = View::make('slack.leads.follow-up-response',['leadName' => $leadName,'leadEmail' => $leadEmail,'leadCompany' => $leadCompany,'assignedUser' => $assignedUser])->render();
        }
        else
        {
            $status = false;
            $return_message = $res['errorMessage'];
        }

        $response['status'] = $status;
        $response['message'] = !empty($message) ? $message : '';
        $response['return_message'] = $return_message;

        return $response;

    }

    public static function markAsSpam($lead_id, $action, $user_id)
    {
        $status = true;
        $return_message = '';
        
        $spamReporter = User::find($user_id);
        $res = self::updateLeadSpam($lead_id);
        if( $res['status'] )
        {
            $res = json_decode($res['responseBody'], true);
            $leadName = $res['result']['name'];
            $leadEmail = $res['result']['email'];
            $leadCompany = $res['result']['company'];
            $message = View::make('slack.leads.mark-spam-response',['leadName' => $leadName,'leadEmail' => $leadEmail,'leadCompany' => $leadCompany,'spamReporter' => $spamReporter])->render();
        }
        else
        {
            $status = false;
            $return_message = $res['errorMessage'];
        }

        $response['status'] = $status;
        $response['message'] = !empty($message) ? $message : '';
        $response['return_message'] = $return_message;

        return $response;
    }

    public static function updateLeadFollowUP($lead_id, $user_id)
    {
        $status = true;
        $errorMessage = '';
        try {
            $client = new Client();
            $url = Config::get('leads.lead_api_post_url').'assigned-to';
            $token = Config::get('leads.token');
            \Log::info('url'.$url);
            $res = $client->request('PUT',$url, [
                'form_params' => [
                    'token' => $token,
                    'lead_id' => $lead_id,
                    'user_id' => $user_id,
                ]
            ]);
            $responseBody = $res->getBody()->getContents();
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $status = false;
                $err = Psr7\str($e->getResponse());
                dump(Psr7\str($e->getRequest()));
                dump($err);
                $errorMessage = 'Error while updating the details in leads database';
                AccessLog::accessLog(null, 'App\Services\LeadsService', 'LeadsService', 'follow-up', 'catch-block', $err);    
            }
        }
        $response['status'] = $status;
        $response['errorMessage'] = !empty($errorMessage) ? $errorMessage : '';
        $response['responseBody'] = !empty($responseBody) ? $responseBody : '';
        return $response;
    }

    public static function updateLeadSpam($lead_id)
    {
        $status = true;
        try {
            $client = new Client();
            $url = Config::get('leads.lead_api_post_url').'spam';
            $token = Config::get('leads.token');
            
            $res = $client->request('PUT',$url, [
                'form_params' => [
                    'token' => $token,
                    'lead_id' => $lead_id
                ]
            ]);
            $responseBody = $res->getBody()->getContents();
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $status = false;
                $err = Psr7\str($e->getResponse());
                $errorMessage = 'Error while updating the details in leads database';
                AccessLog::accessLog(null, 'App\Services\LeadsService', 'LeadsService', 'mark-spam', 'catch-block', $err);
            }
        }
        $response['status'] = $status;
        $response['errorMessage'] = !empty($errorMessage) ? $errorMessage : '';
        $response['responseBody'] = !empty($responseBody) ? $responseBody : '';
        return $response;
    }
    public static function markStatus ($lead_id, $action, $user_id)
    {
        $status = true;
        $return_message = '';
        
        $statusReporter = User::find($user_id);
        $res = self::updateLeadStatus($lead_id , $action);
        if( $res['status'] )
        {
            $res = json_decode($res['responseBody'], true);
            $leadName = $res['result']['name'];
            $leadEmail = $res['result']['email'];
            $leadCompany = $res['result']['company'];
            $message = View::make('slack.leads.mark-status-response',['leadName' => $leadName,'leadEmail' => $leadEmail,'leadCompany' => $leadCompany,'action' => $action,'statusReporter' => $statusReporter])->render();
        }
        else
        {
            $status = false;
            $return_message = $res['errorMessage'];
        }

        $response['status'] = $status;
        $response['message'] = !empty($message) ? $message : '';
        $response['return_message'] = $return_message;

        return $response;
    }
     public static function updateLeadStatus($lead_id , $action)
    {
        $status = true;
        try {
            $client = new Client();
            $url = Config::get('leads.lead_api_post_url').'update-status';
            $token = Config::get('leads.token');
            
            $res = $client->request('PUT',$url, [
                'form_params' => [
                    'token' => $token,
                    'lead_id' => $lead_id,
                    'status' => $action
                ]
            ]);
            $responseBody = $res->getBody()->getContents();
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $status = false;
                $err = Psr7\str($e->getResponse());
                $errorMessage = 'Error while updating the details in leads database';
                AccessLog::accessLog(null, 'App\Services\LeadsService', 'LeadsService', 'mark-spam', 'catch-block', $err);
            }
        }
        $response['status'] = $status;
        $response['errorMessage'] = !empty($errorMessage) ? $errorMessage : '';
        $response['responseBody'] = !empty($responseBody) ? $responseBody : '';
        return $response;
    }
    public static function updateLeadRecord($lead_id, $date , $user_id)
    {
        $status = true;
        $errorMessage = '';
        $return_message = '';

        try {
            $client = new Client();
            $url = Config::get('leads.lead_api_post_url').'follow-up-after';
            $token = Config::get('leads.token');
            \Log::info('url'.$url);
            $res = $client->request('PUT',$url, [
                'form_params' => [
                    'token' => $token,
                    'lead_id' => $lead_id,
                    'date' => $date
                ]
            ]);
            $responseBody = $res->getBody()->getContents();
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $status = false;
                $err = Psr7\str($e->getResponse());
                $errorMessage = 'Error while updating the details in leads database';
                AccessLog::accessLog(null, 'App\Services\LeadsService', 'LeadsService', 'follow-up', 'catch-block', $err);    
            }
        }
        if( $status )
        {
            $res = json_decode($responseBody, true);
            $leadName = $res['result']['name'];
            $leadEmail = $res['result']['email'];
            $leadCompany = $res['result']['company'];
            $message = View::make('slack.leads.follow-up-after-message',['leadName' => $leadName,'leadEmail' => $leadEmail,'leadCompany' => $leadCompany,'date' => $date])->render();
        }
        else
        {
            $return_message = $errorMessage;
        }

        $response['status'] = $status;
        $response['message'] = !empty($message) ? $message : '';
        $response['return_message'] = $return_message;

        return $response;
    }
}