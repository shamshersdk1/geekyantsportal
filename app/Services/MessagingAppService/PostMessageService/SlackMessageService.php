<?php namespace App\Services\MessagingAppService\PostMessageService;

use App\Models\AccessLog;
use App\Models\Admin\SlackAccessLog;
use App\Models\User;
use App\Services\SlackService\SlackConstants;
use Exception;
use GuzzleHttp\Client;

class SlackMessageService implements PostMessageInterface
{
    public static function postMessageToUser($userId, $message, $attachments = null)
    {
        $result = ['status' => false, 'error_code' => '', 'message' => ''];
        $user = User::find($userId);
        if (!empty($user) && !empty($user->slackUser) && !empty($user->slackUser->slack_id)) {
            try
            {

                if ($attachments != null) {
                    $attachments = json_encode($attachments);
                }
                $responseText = null;
                $client = new Client(['base_uri' => config('app.slack_url')]);
                $response = $client->request("POST", "chat.postMessage", [
                    'form_params' => [
                        'token' => config('app.slack_bot_token'),
                        'channel' => '@' . $user->slackUser->slack_id,
                        'text' => $message,
                        'attachments' => $attachments,
                        'as_user' => true,
                    ],
                ]);
                $responseText = json_decode($response->getBody());
                if (empty($responseText)) {
                    $result['message'] = SlackConstants::NO_RESPONSE;
                    return $result;
                } elseif ($responseText->ok === false) {
                    $result['message'] = $responseText->error;
                    return $result;
                }
            } catch (Exception $e) {
                AccessLog::accessLog(null, 'App\Services\SlackService\SlackMessageService', 'SlackMessageService', 'sendMessageToUser', 'catch-block', $e->getMessage());
                $result['message'] = $e->getMessage();
                return $result;
            }
        } else {
            $result['message'] = SlackConstants::NO_USER;
        }
        $result['status'] = true;
        return $result;
    }

    public static function postMessageToChannel($channelId, $message, $linkNames = null, $asUser = true, $data = null)
    {

        
        $blocks = NULL;
        $attachments = NULL;
        
        if(!empty($data['blocks'])) {
            $blocks = $data['blocks'];
        }else
            $attachments = $data;

        $result = ['status' => false, 'error_code' => '', 'message' => ''];
        try
        {
            $responseText = null;
            if ($linkNames) {
                $linkNames = true;
            } else {
                $linkNames = false;
            }
            
            if ($attachments != null && $blocks == NULL) {
                $attachments = self::makeAttachment($attachments);
                //$attachments = json_encode($attachments);
                \Log::info('json_decode() - ' . json_encode($attachments));
            }
            
            $client = new Client(['base_uri' => config('app.slack_url')]);
            $response = $client->request("POST", "chat.postMessage", [
                'form_params' => [
                    'token' => config('app.slack_bot_token'),
                    'channel' => $channelId,
                    'text' => $message,
                    'as_user' => $asUser,
                    'link_names' => $linkNames,
                    'attachments' => $attachments,
                    'blocks' => $blocks,
                ],
            ]);

            $responseText = json_decode($response->getBody());
            if (empty($responseText)) {
                $result['message'] = SlackConstants::NO_RESPONSE;
            } elseif ($responseText->ok === false) {
                $result['message'] = $responseText->error;
            }
            \Log::info('postMessageToChannel called2'.json_encode($responseText));
            $from = $asUser ? "User" : "Bot";
            $to = $channelId;
            $response = SlackAccessLog::saveData($from, $to, json_encode(['message' => $message]), json_encode($responseText));
            //return $result;

        } catch (Exception $e) {
            AccessLog::accessLog(null, 'App\Services\SlackService\SlackMessageService', 'SlackMessageService', 'sendMessageToChannel', 'catch-block', $e->getMessage());
            $result['message'] = $e->getMessage();
            return $result;
        }
        $result['status'] = true;
        return $result;
    }

    public static function makeAttachment($attachments = [])
    {
        $appUrl = \Config::get('app.url');
        if (!empty($attachments['url'])) {
            $url = $attachments['url'];
        } else {
            $url = \Config::get('app.geekyants_portal_url');
        }

        $color = !empty($attachments['color']) ? $attachments['color'] : '#B22222';

        $authorName = null;
        $authorLink = null;
        $authorIcon = null;

        if (!empty($attachments['author_id'])) {
            $userObj = User::find($attachments['author_id']);
            if ($userObj) {
                $authorName = $userObj->name;
                $authorLink = $userObj->user_profile->profile_url;
                $authorIcon = $userObj->user_profile->image;
            }
        }
        $title = !empty($attachments['title']) ? $attachments['title'] : null;
        $titleLink = !empty($attachments['title_link']) ? $attachments['title_link'] : null;
        $text = !empty($attachments['text']) ? $attachments['text'] : null;
        $ts = !empty($attachments['ts']) ? $attachments['ts'] : strtotime(time());
        $fallback = !empty($attachments['fallback']) ? $attachments['fallback'] : null;
        $pretext = !empty($attachments['pretext']) ? $attachments['pretext'] : null;
        $fields = !empty($attachments['fields']) ? $attachments['fields'] : [];
        $footer = !empty($attachments['footer']) ? $attachments['footer'] : "GeekyAnts Portal";

        $attachments = [
            [
                "fallback" => $fallback,
                "color" => $color,
                //"pretext"     =>  "Optional text that appears above the attachment block",
                "author_name" => $authorName,
                "author_link" => $appUrl . '/' . $authorLink,
                "author_icon" => $authorIcon,
                "title" => $fallback,
                "title_link" => $url . '/' . $titleLink,
                "text" => $text,
                "fields" => $fields,
                //"image_url"     =>  $appUrl.'/'."images/logo-icon-lg.png",
                //"thumb_url"     =>  $appUrl.'/'."images/logo-icon-lg.png",
                "footer" => $footer,
                "footer_icon" => $appUrl . '/' . "images/logo-icon-lg.png",
                "ts" => $ts,
            ],
        ];
        return json_encode($attachments);
    }
}
