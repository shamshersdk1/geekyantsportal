<?php namespace  App\Services\MessagingAppService\PostMessageService;
class PostUserMessageService {

    public static function postMessageToUser($userId, $message, $attachments = null) {
        $service = \Config::get('messaging-app.default');
        if(!empty($service)) {
            $namespace  = \Config::get('messaging-app.services.'.$service.'.namespace');
            $token      = \Config::get('messaging-app.services.'.$service.'.token');
            $response = $namespace::postMessageToUser($userId, $message, $attachments);
        }
    }
    public static function postMessageToChannel($channelId, $message, $links = null, $usUser = false,  $params = null) {
        $service = \Config::get('messaging-app.default');
        if(!empty($service)) {
            $namespace  = \Config::get('messaging-app.services.'.$service.'.namespace');
            $token      = \Config::get('messaging-app.services.'.$service.'.token');
            $response = $namespace::postMessageToChannel($channelId, $message, null, false, $params);
        }
    }
}