<?php namespace App\Services\MessagingAppService\PostMessageService;

interface PostMessageInterface {
    public static function postMessageToUser($userId, $message, $params = null);
    public static function postMessageToChannel($channelId, $message, $params);
}
