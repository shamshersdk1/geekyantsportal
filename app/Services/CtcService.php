<?php
namespace App\Services;

use Log;
use Mail;
use Exception;
use Config;

use App\Models\AccessLog;
use App\Models\User;
use App\Models\Admin\Bonus;
use App\Models\Admin\PayslipData;
use App\Models\Admin\Appraisal;
use App\Models\Admin\PayroleGroupRule;
use App\Models\Admin\PayrollRuleForCsv;

class CtcService
{
    // protected $user;

    public function __construct()
    {
        // $this->user = $user;
    }

    public function downloadUserPayslip($userId)
    {
        try {
            $response =  [
                        'status'    => true,
                        'message'   => "success" ,
                        'id'        => null,
                        'log'       => [],
                        'errors'    => [],
                        'data'      => []
                    ];

            $userObj = User::find($userId);
            if (!$userObj) {
                $response['status'] = false;
                $response['log'][] = 'User not found';
                return $response;
            }
            if (empty($userObj->appraisals)||empty($userObj->activeAppraisal)||empty($userObj->activeAppraisal->in_hand)) {
                $response['status'] = false;
                $response['log'][] = 'User in hand not found';
                return $response;
            }
            if (empty($userObj->activeAppraisal->group)) {
                $response['status'] = false;
                $response['log'][] = 'No group found for the user';
                return $response;
            }

            if (empty($userObj->activeAppraisal->group->rules)) {
                $response['status'] = false;
                $response['log'][] = 'No rules found for the group';
                return $response;
            }
            $monthlyInHand = $userObj->activeAppraisal->in_hand/12;
            $earning=0;
            $deduction=0;
            $net=0;
            $c=0;

            foreach ($userObj->activeAppraisal->group->rules->sortBy('id') as $index => $rule) {
                $ruleName = empty($rule->csvValue)?$rule->name:$rule->csvValue->value;
                $response['data']['names'][] = $ruleName;
                if (empty($rule->calculation_type)) {
                    $monthlyResult = $rule->calculation_value;
                    $yearlyResult = $rule->calculation_value;
                } else {
                    $monthlyResult = 0;
                    switch ($rule->calculation_type) {
                        case "*":
                            if ($rule->calculated_upon==0) {
                                $monthlyResult = $monthlyInHand * $rule->calculation_value;
                            } else {
                                $monthlyResult = $response['data']['monthly'][array_search($rule->calc_upon->name, $response['data']['names'])] * $rule->calculation_value;
                            }
                            break;

                        case "+";
                            $monthlyResult = $rule->calculation_value;
                            break;

                        case "-";
                            $monthlyResult = $rule->calculation_value;
                            break;

                        case "/";
                            if ($rule->calculated_upon==0) {
                                $monthlyResult = $monthlyInHand / $rule->calculation_value;
                            } else {
                                $monthlyResult = $response['data']['monthly'][array_search($rule->calc_upon->name, $response['data']['names'])] / $rule->calculation_value;
                            }
                            break;

                        case "%";
                            if ($rule->calculated_upon==0) {
                                $monthlyResult = $monthlyInHand / 100 * $rule->calculation_value;
                            } else {
                                $monthlyResult = $response['data']['monthly'][array_search($rule->calc_upon->name, $response['data']['names'])] / 100 * $rule->calculation_value;
                                if($ruleName == PayrollRuleForCsv::getValue('pf_employeer') || $ruleName == PayrollRuleForCsv::getValue('pf_employee')) {
                                    $monthlyResult = ($monthlyResult > 1800) ? 1800 : $monthlyResult;
                                }
                                if($ruleName == PayrollRuleForCsv::getValue('pf_other')) {
                                    $monthlyResult = ($monthlyResult > 173) ? 173 : $monthlyResult;
                                }
                            }
                                break;
                    }
                    $monthlyResult = round($monthlyResult);
                }
                if ($rule->type=='credit') {
                    $response['data']['monthly'][] =$monthlyResult;
                    $response['data']['yearly'][] =$monthlyResult*12;
                    $earning+=$monthlyResult;
                    $c++;
                } else {
                    $response['data']['monthly'][] ="-".$monthlyResult;
                    $response['data']['yearly'][] ="-".$monthlyResult*12;
                    $deduction+=$monthlyResult;
                }
                // if ($rule->calculation_type=='%') {
                //     $of = $rule->calculated_upon==0? 'CTC': PayroleGroupRule::where('id', $rule->calculated_upon)->first()->name;
                //     $response['data']['type'][] ="".$rule->calculation_value.' % of '.$of;
                // } else {
                //     $response['data']['type'][]="";
                // }
            }
                // $specialAlowanceIndex = array_search('Special Allowance', $response['data']['names']);
                
                // // $response['data']['names'][] =  'Special Allowance';
                // $response['data']['monthly'][$specialAlowanceIndex] = $monthlyInHand-$specialAlowance;
                // $response['data']['yearly'][$specialAlowanceIndex] = ($monthlyInHand - $specialAlowance)*12;
                
                // $response['data']['monthly'][] =$earning;
                // $response['data']['yearly'][] =$earning*12;
                $special = $monthlyInHand - $earning;
                $response['data']['names'][] =  'Special Allowance';
                $response['data']['monthly'][] = $special;
                $response['data']['yearly'][] =($special)*12;
                // $response['data']['type'][]="";
                $earning += $special;
                $response['data']['names'][] =  'Gross Earning';
                $response['data']['monthly'][] =$earning;
                $response['data']['yearly'][] =$earning*12;
                // $response['data']['type'][]="";
                $extra = array_splice($response['data']['names'], count($response['data']['names'])-2, 2);
                $credit = array_splice($response['data']['names'], 0, $c);
                $response['data']['names'] = array_merge($credit, $extra, $response['data']['names']);
                $extra = array_splice($response['data']['monthly'], count($response['data']['monthly'])-2, 2);
                $credit = array_splice($response['data']['monthly'], 0, $c);
                $response['data']['monthly'] = array_merge($credit, $extra, $response['data']['monthly']);
                $extra = array_splice($response['data']['yearly'], count($response['data']['yearly'])-2, 2);
                $credit = array_splice($response['data']['yearly'], 0, $c);
                $response['data']['yearly'] = array_merge($credit, $extra, $response['data']['yearly']);
                // $extra = array_splice($response['data']['type'], count($response['data']['type'])-2, 2);
                // $credit = array_splice($response['data']['type'], 0, $c);
                // $response['data']['type'] = array_merge($credit, $extra, $response['data']['type']);

                $month = date('Y-m');
                $qtr = 0;
                $annual = 0;
                $bonusArray = Appraisal::getBonus($userObj->activeAppraisal->id, 1);
                if(isset($bonusArray['Qtr'])&&isset($bonusArray['Qtr'][$month.'-01'])) {
                    $temp = $bonusArray['Qtr'][$month.'-01'];
                    $qtr = round($temp, 2);
                }
                if(isset($bonusArray['annual'])&&($bonusArray['annual'] == $month.'-25')) {
                    $temp = $userObj->activeAppraisal->year_end;
                    $annual = round($temp, 2);
                }

                $response['data']['names'][] =  'Total Deduction';
                $response['data']['monthly'][] ="-".$deduction;
                $response['data']['yearly'][] ="-".$deduction*12;
                // $response['data']['type'][]="";
                $arrearSalaryIndex = array_search(PayrollRuleForCsv::getValue('arrear_salary'), $response['data']['names']);
                if($arrearSalaryIndex === false) {
                    $arrearSalary = 0;
                } else {
                    $arrearSalary = isset($response['data']['monthly'][$arrearSalaryIndex]) ? $response['data']['monthly'][$arrearSalaryIndex] : 0;
                }
                $netSalary = $monthlyInHand - $deduction + $arrearSalary;
                $response['data']['names'][] =  'Net Salary';
                $response['data']['monthly'][] =$netSalary;
                $response['data']['yearly'][] =($netSalary)*12;
                // $response['data']['type'][]="";
                $response['data']['names'][] =  'Annual Bonus';
                $response['data']['monthly'][] = $annual;
                $response['data']['yearly'][] = $annual;
                // $response['data']['type'][]="";
                $response['data']['names'][] =  'Qtr Variable Bonus';
                $response['data']['monthly'][] = $qtr;
                $response['data']['yearly'][] = $qtr * 4;
                // $response['data']['type'][]="";
                $response['data']['names'][] =  'Performance Bonus';
                $response['data']['monthly'][] = 0;
                $response['data']['yearly'][] = 0;
                // $response['data']['type'][]="";
                $response['data']['names'][] =  'Non-Cash Incentive';
                $response['data']['monthly'][] = 0;
                $response['data']['yearly'][] = 0;
                // $response['data']['type'][]="";
                $response['data']['names'][] =  'AMOUNT PAYABLE';
                $response['data']['monthly'][] =$netSalary + $qtr + $annual;
                $response['data']['yearly'][] =($netSalary)*12 + $qtr * 4 + $annual;

                $pfEmployerIndex = array_search(PayrollRuleForCsv::getValue('pf_employeer'), $response['data']['names']);
                if($pfEmployerIndex === false) {
                    $pfEmployer = 0;
                } else {
                    $pfEmployer = isset($response['data']['monthly'][$pfEmployerIndex]) ? $response['data']['monthly'][$pfEmployerIndex] : 0;
                }
                $pfOtherChargesIndex = array_search(PayrollRuleForCsv::getValue('pf_other'), $response['data']['names']);
                if($pfOtherChargesIndex === false) {
                    $pfother = 0;
                } else {
                    $pfother = isset($response['data']['monthly'][$pfOtherChargesIndex]) ? $response['data']['monthly'][$pfOtherChargesIndex] : 0;
                }
                $esiEmployeerIndex = array_search(PayrollRuleForCsv::getValue('esi_475'), $response['data']['names']);
                if($esiEmployeerIndex === false) {
                    $esiEmployeer = 0;
                } else {
                    $esiEmployeer = isset($response['data']['monthly'][$esiEmployeerIndex]) ? $response['data']['monthly'][$esiEmployeerIndex] : 0;
                }
                $adjustmentValues = $pfEmployer + $pfother + $esiEmployeer;
                $response['data']['names'][] =  'Adjusted Gross Salary';
                $response['data']['monthly'][] =$earning + $adjustmentValues;
                $response['data']['yearly'][] =($earning + $adjustmentValues)*12;
                $response['data']['names'][] =  'Adjusted Special Allowance';
                $response['data']['monthly'][] =$special + $adjustmentValues;
                $response['data']['yearly'][] =($special + $adjustmentValues)*12;
                $response['data']['names'][] =  'Adjusted Deductions';
                $response['data']['monthly'][] =$deduction + $adjustmentValues;
                $response['data']['yearly'][] =($deduction + $adjustmentValues)*12;
                return $response;
        } catch (Exception $e) {
            AccessLog::accessLog(null, 'App\Services', 'CtcService', 'downloadUserPayslip', 'catch-block', $e->getMessage());
        }
    }

    public static function generatePayslip($id, $month, $year)
    {
        $user = User::find($id);
        $total = PayrollRuleForCsv::count();
        if(!$total) {
            $total = 33;
        }
        if((!$user) || (empty($user->appraisals) || empty($user->activeAppraisal)|| empty($user->activeAppraisal->in_hand))|| (empty($user->activeAppraisal->group)) || (empty($user->activeAppraisal->group->rules))) {
            return array_fill(0, $total, 0);
        } else {
            $monthlyInHand = $user->activeAppraisal->in_hand/12;
            $result =  array_fill(0, $total, 0);
            $earning = 0;
            $deduction = 0;
            $temp = 0;
            $calculated = [];
            $fields_Array = PayrollRuleForCsv::orderBy('id')->pluck('key')->toArray();
            // $fields_Array = ['Basic','HRA','Conveyance Allowance','Car Allowance',
            //                 'Medical Allowance','Food Allowance','Special Allowance',
            //                 'Gross Salary', 'P.F. @12% Employee','E.S.I. @1.75% Employee',
            //                 'VPF', 'Professional Tax','Food Deduction','Medical Insurance',
            //                 'T.D.S.','LOAN','TOTAL DEDUCTION FOR MONTH','NET SALARY FOR THE MONTH',
            //                 'Arrear Salary','Annual Bonus','Qtr Variable Bonus','Performance Bonus',
            //                 'Non-Cash Incentive','Gross Earnings','Amount Payable'];
            $rules = $user->activeAppraisal->group->rules;
            foreach($rules as $rule)
            {   
                if(array_search($rule->name, $fields_Array) !== false) {
                    $temp = 0;
                    if (empty($rule->calculation_type)) {
                        $temp = $rule->calculation_value;
                        $result[array_search($rule->name, $fields_Array)] = $temp;
                        $calculated[$rule->id] = $temp;
                    } else {
                        $temp = CtcService::getCalculatedValue($rule, $monthlyInHand, $calculated);
                        $result[array_search($rule->name, $fields_Array)] = $temp;
                        $calculated[$rule->id] = $temp;
                    }
                    if ($rule->type=='credit') {
                        $earning += $temp;
                    } else {
                        $deduction += $temp;
                    }
                }
            }
            $bonuses = 0;
            $bonusArray = CtcService::getBonus($user, $month, $year);
            if(isset($bonusArray['Qtr'])) {
                $temp = $bonusArray['Qtr'];
                $qtrIndex = array_search('qtr_variable_bonus', $fields_Array);
                if(!$qtrIndex === false) {
                    $result[$qtrIndex] = $temp;
                    $bonuses += $temp;    
                }
            }
            if(isset($bonusArray['annual'])) {
                $temp = $bonusArray['annual'];
                $annualIndex = array_search('annual_bonus', $fields_Array);
                if(!$annualIndex === false) {
                    $result[$annualIndex] = $temp;
                    $bonuses += $temp;    
                }
                // $result[array_search('Annual Bonus', $fields_Array)] = $temp;    
                // $bonuses += $temp;
            }
            if(isset($bonusArray['performance']) && ($bonusArray['performance'] != 0)) {
                $performanceIndex = array_search('performance_bonus', $fields_Array);
                $temp = $bonusArray['performance'];
                if(!$performanceIndex === false) {
                    $result[$performanceIndex] = $temp;
                    $bonuses += $temp;    
                }
                
            }


            $adjustment_deductions = $result[array_search('pf_employeer', $fields_Array)]
                             + $result[array_search('pf_other', $fields_Array)]
                             + $result[array_search('esi_475', $fields_Array)];
            $arrearSalaryIndex = array_search('arrear_salary', $fields_Array);
            if($arrearSalaryIndex === false) {
                $arrearSalary = 0;
            } else {
                $arrearSalary = $result[$arrearSalaryIndex];
            }
            $result[array_search('gross_earning_for_the_month', $fields_Array)] = $monthlyInHand;
            $result[array_search('special_allowance', $fields_Array)] = $monthlyInHand - $earning;
            // $result[array_search('Gross Salary', $fields_Array)] = $earning + $bonuses;
            $result[array_search('total_deduction_for_the_month', $fields_Array)] = $deduction - $adjustment_deductions;
            $result[array_search('net_salary_for_the_month', $fields_Array)] = $monthlyInHand - $deduction + $arrearSalary;
            $result[array_search('amount_payable', $fields_Array)] = $monthlyInHand + $bonuses + $arrearSalary - $deduction;
            $result[array_search('adjusted_gross_salary', $fields_Array)] = $monthlyInHand - $adjustment_deductions;
            $result[array_search('adjusted_special_allowance', $fields_Array)] = $monthlyInHand - $adjustment_deductions - $earning;
            $result[array_search('adjusted_deduction', $fields_Array)] = $deduction;
            return $result;
        }
    }
    public static function getCalculatedValue($rule, $monthlyInHand, $calculated)
    {
        switch ($rule->calculation_type) {
            case "*":
                if ($rule->calculated_upon==0) {
                    $monthlyResult = $monthlyInHand * $rule->calculation_value;
                } else {
                    if(isset($calculated[$rule->calculated_upon])) {
                    $monthlyResult = $calculated[$rule->calculated_upon] * $rule->calculation_value;
                    } else {
                        $monthlyResult = 0;
                    }
                }
                break;

            case "+";
                $monthlyResult = $rule->calculation_value;
                break;

            case "-";
                $monthlyResult = $rule->calculation_value;
                break;

            case "/";
                if ($rule->calculated_upon==0) {
                    $monthlyResult = $monthlyInHand / $rule->calculation_value;
                } else {
                    if(isset($calculated[$rule->calculated_upon])) {
                        $monthlyResult = $calculated[$rule->calculated_upon] / $rule->calculation_value;
                    } else {
                        $monthlyResult = 0;
                    }
                }
                break;

            case "%";
                if ($rule->calculated_upon==0) {
                    $monthlyResult = $monthlyInHand / 100 * $rule->calculation_value;
                } else {
                    if(isset($calculated[$rule->calculated_upon])) {
                        $monthlyResult = $calculated[$rule->calculated_upon] * $rule->calculation_value / 100;
                        if($rule->name == 'pf_employeer' || $rule->name == 'pf_employee') {
                            $monthlyResult = ($monthlyResult > 1800) ? 1800 : $monthlyResult;
                        }
                        if($rule->name == 'pf_other') {
                            $monthlyResult = ($monthlyResult > 173) ? 173 : $monthlyResult;
                        }
                    } else {
                        $monthlyResult = 0;
                    }
                }
        }
        return $monthlyResult;
    }
    public static function getBonus($user, $month, $year)
    {
        $qtr = 0;
        $appraisal = $user->activeAppraisal;
        $max = empty($appraisal->qtr_bonus) ? 0 : $appraisal->qtr_bonus/4;
        $annual = 0;
        $date = $appraisal->effective_date;
        if($user->confirmation_date > $date) {
            $date = $user->confirmation_date;
        }
        if(in_array($month, ["01", "04", "07", "10"])) {
            if($month == "01") {
                $max_cutoff = ($year-1)."-11-01";
            } else {
                $max_cutoff = $year."-0".($month-2)."-01";
            }
            if($month == "01") {
                $second_cutoff = ($year-1)."-12-01";
            } else {
                $second_cutoff = $year."-0".($month-1)."-01";
            }
            $min_cutoff = $year."-".substr("0".$month, -2)."-01";
            if($date < $max_cutoff) {
                $qtr = $max;
            } elseif($date < $second_cutoff) {
                $qtr = round(((2*$max)/3), 2);
            }
             elseif($date < $min_cutoff) {
                $qtr = round(($max/3), 2);
            }
        }
        $date = $user->confirmation_date;
        if((date("Y", strtotime($date)) < $year) && date("n", strtotime($date)) == $month) {
            $annual = empty($appraisal->year_end) ? 0 : $appraisal->year_end;
        }
        $date = date("Y-m-d", strtotime($year."-".substr("0".$month, -2)."-25"));
        $performance = 0;
        $performance = Bonus::where('user_id', $user->id)->where('date', $date)->where('status', 'approved')->sum('amount');
        return ['Qtr' => $qtr, 'annual' => $annual, 'performance' => $performance];
    }
}
