<?php namespace App\Services;

use Validator;
use App\Models\Admin\Project;
use App\Models\Admin\ProjectNotes;
use Input;
use Redirect;
use App\Models\User;
use Auth;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use Config;
use Exception;
use Mail;
use App\Mail\PasswordResetMail;

class SlashCommandService {

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	public static function saveNotes($request) {
	    $projectId = $request->input('channel_id');
	    $channelName = $request->input('channel_name');
	    $message = $request->input('text');
	    $userName = $request->input('user_name');
	    $slackUserId = $request->input('user_id');
	    $current = date('Y-m-d');
	    $data = [];
	    $user = null;

	    $userId = null;
	    try{
	        $client = new Client(['base_uri' => config('app.slack_url')]);
	        $response = $client->request("POST", "users.info", [
	                'form_params' => [
	                    'token' => config('app.slack_token'),
	                    'user' => $slackUserId
	                ]
	        ]);
	    } catch(Exception $e) {
	        return ['response_type' => 'ephemeral', 'text' => "Slack user info api not worked." ];
	    }
	    $userCreateResponse =  $response->getBody();
	    $response = json_decode($userCreateResponse);
	    $slackUsers = $response->user;

	    if($slackUsers) {
	        $data['name'] = $slackUsers->name;
	        $data['email'] = $slackUsers->profile->email;
	        $data['password'] = rand(10,1000);
	    } else {
	    	//did not recived slack user detail
	    }
	    if(isset($data['email']))
	    	$user = User::where('email',$data['email'])->first();
	    //Retrieving User Id
	    if(!$user) {
	        if($data['email']) {
	            $slackEmail = User::where('email',$data['email'])->first();
	            if(!$slackEmail) {
	               if($data['name'] && $data['email'] && $data['password'] && $slackUsers) {
	                    $user = User::saveData($data);
	                    //send mail
	                    $userId = $user['id'];
	                    \Log::info('sending mail');
	                    Mail::to($user['user'])->send(new PasswordResetMail($data));
	                    \Log::info('sent mail');
	                } else {
	                    return ['text' => "No sufficient data of user to save in our db." ];
	                }
	            } else {
	                $userId = $slackEmail->id;
	                $user = $slackEmail;
	            }
	        }
	    } else {
	        $userId = $user->id;
	    }

	    \Log::info('slack id'.$userId);

	    $project = Project::where('channel_for_developer',$channelName)->first();

	    if(!$project){
	        return ['text' => "Project not found."];
	    }

	    $text = trim($message);
	    $user = User::find($userId);
	    if($user) {
		    if(strtolower(substr($text, 0, 7)) == "private") {
		    	if($user->isAdmin()) {
		    		$request = [];
		    		$request['comment'] = substr($text, 7);
		    		$request['user_name'] = $user->name;
		    		$request['project_id'] = $project->id;
		    		$request['private_to_admin'] = 1;
		    		$result = ProjectNotes::saveData($request);
		    		if($result['status'])
		    		{
		    			return ['text' => "Note Successfully saved." ];
		    		}
		    		else{
		    			return ['text' => 'Unsufficient data.'];
		    			// return redirect::back()->withErrors($result['errors'])->withInput();
		    		}
		    	}
		    	else {
		    		return ['text' => "You are not authorized to save private notes." ];
		    	}
		    }
		    else{
		    	if($text) {
		    		$request = [];
		    		$request['comment'] = $text;
		    		$request['user_name'] = $user->name;
		    		$request['project_id'] = $project->id;
		    		$request['private_to_admin'] = 0;
		    		$result = ProjectNotes::saveData($request);
		    		if($result['status'])
		    		{
		    			return ['text' => "Note Successfully saved." ];

		    		}
		    		else{
		    			return ['text' => 'Unsufficient data.'];
		    			// return redirect::back()->withErrors($result['errors'])->withInput();
		    		}
		    	}
		    	else {
		    		return ['text' => "No Note Added."];
		    	}
		    }
		}
		else {
			return ['text' => "User not saved." ];
		}
	}

}