<?php namespace App\Services;

// use Config;
use Exception;
use GuzzleHttp\Client;

// use Illuminate\Http\Request;

class SalesforceService
{
    public static function postForm($data)
    {

        $postUrl = "https://webto.salesforce.com/";
        //$postUrl = "http://geekyants-portal.local.geekydev.com/test";

        $oid = !empty($data['oid']) ? $data['oid'] : "00D6F0000026mir";
        $retURL = !empty($data['retURL']) ? $data['retURL'] : 'https://geekyants.com/thankyou';

        $firstName = !empty($data['first_name']) ? $data['first_name'] : null;
        $lastName = !empty($data['last_name']) ? $data['last_name'] : null;
        $email = !empty($data['email']) ? $data['email'] : null;
        $company = !empty($data['company']) ? $data['company'] : null;
        $leadSource = !empty($data['lead_source']) ? $data['lead_source'] : null;
        $URL = !empty($data['URL']) ? $data['URL'] : null;
        try
        {
            $responseText = null;
            $client = new Client(['base_uri' => $postUrl]);
            $response = $client->request("POST", "/servlet/servlet.WebToLead?encoding=UTF-8", [
                'form_params' => [
                    'oid' => $oid,
                    'retURL' => $retURL,
                    'first_name' => $firstName,
                    'last_name' => $lastName,
                    'email' => $email,
                    'company' => $company,
                    'lead_source' => $leadSource,
                    'URL' => $URL,
                ],
            ]);
            $responseText = json_decode($response->getBody());
            \Log::info('responseText ' . json_encode($responseText));
            \Log::info('responseText ' . json_encode($response));

        } catch (Exception $e) {
            \Log::info('Exception ');
            \Log::info($e->getMessage());
        }
    }
}
