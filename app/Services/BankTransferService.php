<?php namespace App\Services;

use App\Models\Admin\File;
use App\Models\Admin\FileContent;
use App\Models\Audited\BankTransfer\BankTransferUser;
use App\Services\FileService;
use Excel;

class BankTransferService
{
    public static function uploadFile($file, $id)
    {
        $response = ['status' => false, 'message' => "", 'data' => ""];
        $fileSize = $file->getClientSize();
        if ($fileSize > 20000000) {
            $response['message'] = "Filesize exceeds 20MB";
            return $response;
        }

        $content = file_get_contents($file->getRealPath());
        $type = $file->getClientOriginalExtension();
        if (substr($type, 0, 2) !== "xl") {
            $response['message'] = "File is not in .XLxx format";
            return $response;
        }

        $fileName = FileService::getFileName($type);
        $originalName = $file->getClientOriginalName();
        $existingFile = File::where('reference_type', 'App\Models\Audited\BankTransfer')->where('reference_id', $id)->first();
        if ($existingFile) {
            $fileContent = FileContent::where('file_id', $existingFile->id)->first();
            if ($fileContent) {
                $fileContent->delete();
            }
            $existingFile->delete();
        }
        $store = FileService::uploadFileInDB($content, $originalName, $type, $id, "App\Models\Audited\BankTransfer");

        return ['status' => true, 'message' => "File Added", 'data' => $store->id];
    }

    public static function transferData($file, $bankTransferId)
    {
        $response = [];
        $response['status'] = true;
        $response['message'] = "";

        if (!$file) {
            $response['status'] = false;
            $response['message'] = "File invalid!";
            return $response;
        }

        $parsed = (Excel::load($file, function ($reader) {
            $sheet = $reader->noHeading()->skipRows(7)->skipColumns(1)->toArray();
        })->parsed);
        foreach ($parsed as $parse) {

            $desciptionArr = explode("/", $parse[4]);

            $transactionId = isset($parse[0]) ? $parse[0] : null;
            $transactionValueDate = isset($parse[1]) ? $parse[1] : null;
            $transactionPostedDate = isset($parse[2]) ? $parse[2] : null;
            $transactionChequeNo = isset($parse[3]) ? $parse[3] : null;
            $transactionDescription = isset($parse[4]) ? $parse[4] : null;
            $transactionType = isset($parse[5]) ? $parse[5] : null;
            $transactionAmount = isset($parse[6]) ? $parse[6] : null;
            $transactionBalance = isset($parse[7]) ? $parse[7] : null;
            $transactionAccount = $desciptionArr[2];

            $bankTransferUserObj = BankTransferUser::where('bank_acct_number', $transactionAccount)->where('bank_transfer_id', $bankTransferId)->first();
            if ($bankTransferUserObj) {
                $bankTransferUserObj->comment = $transactionDescription;
                $bankTransferUserObj->txn_posted_date = $transactionPostedDate;
                $bankTransferUserObj->bank_transaction_id = $transactionId;
                $bankTransferUserObj->transaction_amount = $transactionAmount;
                $bankTransferUserObj->mode_of_transfer = $desciptionArr[1] ? $desciptionArr[1] : null;

                if (!$bankTransferUserObj->save()) {

                }
            }
        }
        return $response;
    }
}
