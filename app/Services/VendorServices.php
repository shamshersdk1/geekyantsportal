<?php namespace App\Services;

use Validator;
use App\Models\Admin\Vendor;
use App\Models\Admin\VendorContact;
use App\Models\Admin\VendorService;
use App\Models\Admin\VendorServicesName;
use App\Models\User;

use Redirect;
use Auth;


class VendorServices {

	public static function saveData($input)
	{
        $status = true;
        $message = '';
        $result = [];

        $vendorObj = new Vendor();
        $vendorObj->type = $input['type'];
        $vendorObj->name = $input['name'];
        $vendorObj->address = !empty($input['address']) ? $input['address'] : null;
        $vendorObj->pan = !empty($input['pan']) ? $input['pan'] : null;
        $vendorObj->gst = !empty($input['gst']) ? $input['gst'] : null;
        $vendorObj->landline_number = !empty($input['landline_number']) ? $input['landline_number'] : null;
        if ( !$vendorObj->save() )
        {
            $status = false;
            $message = $vendorObj->getErrors();
        }
        else {
            if ( !empty($input['vendor_contacts']) )
            {
                self::saveVendorContacts($vendorObj->id, $input['vendor_contacts']);
            }
            if ( !empty($input['vendor_services']) )
            {
                self::saveVendorServices($vendorObj->id, $input['vendor_services']);
            }
        }

        $result['status'] = $status;
        $result['message'] = $message;
        return $result;
    }
    
    public static function saveVendorContacts($vendor_id, $vendor_contacts)
    {
        VendorContact::where('vendor_id', $vendor_id)->delete();
        foreach( $vendor_contacts as $contact )
        {
            $contactObj = new VendorContact();
            $contactObj->vendor_id = $vendor_id;
            $contactObj->name = $contact['name'];
            $contactObj->mobile = !empty($contact['mobile']) ? $contact['mobile'] : null ;
            $contactObj->email = !empty($contact['email']) ? $contact['email'] : null ;
            $contactObj->designation = !empty($contact['designation']) ? $contact['designation'] : null ;
            $contactObj->is_primary_contact = !empty($contact['is_primary_contact']) ? $contact['is_primary_contact'] : 0 ;
            $contactObj->save();
        }
    }

    public static function saveVendorServices($vendor_id, $vendor_services)
    {
        VendorService::where('vendor_id',$vendor_id)->delete();
        foreach( $vendor_services as $service )
        {
            $service_id = self::checkIfServiceExists($service);
            $vendorServiceObj = new VendorService();
            $vendorServiceObj->vendor_id = $vendor_id;
            $vendorServiceObj->vendor_services_name_id = $service_id;
            $vendorServiceObj->save();
        }
    }

    public static function checkIfServiceExists($name)
    {
        $serviceObj = VendorServicesName::where('name',$name)->first();
        if ( !$serviceObj )
        {
            $service_id = self::createService($name);
            return $service_id ;
        }
        else {
            return $serviceObj->id;
        }
    }

    public static function createService($name)
    {
        $serviceObj = new VendorServicesName();
        $serviceObj->name = $name;
        $serviceObj->save();
        return $serviceObj->id;
    }

    public static function updateData($id, $input)
	{
        $status = true;
        $message = '';
        $result = [];

        $vendorObj = Vendor::find($id);
        if ( !$vendorObj )
        {
            $status = false;
            $message = 'No record found';
        }
        else
        {
            $vendorObj->type = $input['type'];
            $vendorObj->name = $input['name'];
            $vendorObj->address = !empty($input['address']) ? $input['address'] : null;
            $vendorObj->pan = !empty($input['pan']) ? $input['pan'] : null;
            $vendorObj->gst = !empty($input['gst']) ? $input['gst'] : null;
            $vendorObj->rating = !empty($input['rating']) ? $input['rating'] : null;
            $vendorObj->landline_number = !empty($input['landline_number']) ? $input['landline_number'] : null;
            if ( !$vendorObj->save() )
            {
                $status = false;
                $message = $vendorObj->getErrors();
            }
            else {
                if ( !empty($input['vendor_contacts']) )
                {
                    self::saveVendorContacts($vendorObj->id, $input['vendor_contacts']);
                }
                if ( !empty($input['vendor_services']) )
                {
                    self::saveVendorServices($vendorObj->id, $input['vendor_services']);
                }
            }
        }
        $result['status'] = $status;
        $result['message'] = $message;
        return $result;
    }

    public static function getPrimaryContact($vendor_id)
    {
        // Returns primary contact or first record found for a vendor
        $count = VendorContact::where('vendor_id',$vendor_id)->where('is_primary_contact',1)->count();
        if ( $count > 0 ){
            $vendorContactObj = VendorContact::where('vendor_id',$vendor_id)->where('is_primary_contact',1)->first();
        } else {
            $vendorContactObj = VendorContact::where('vendor_id',$vendor_id)->first();
        }
        return $vendorContactObj;
    }

}