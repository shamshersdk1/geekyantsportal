<?php namespace  App\Services;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Admin\Company;
use App\Models\Admin\Project;
use App\Models\Admin\ProjectCategory;
use App\Models\Admin\Contact;
use App\Models\User;
use DB;
use Redirect;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use Illuminate\Http\Request;

class CompanyService 
{
    public static function saveData($data)
    {  
        $status = true;
        $message = '';
		
        $company = new Company();
		$company->name = $data['name'];
		$company->email = $data['email'];
		$company->website = $data['website'];
		$company->work_phone = !empty ($data['work_phone'])? $data['work_phone'] : null ;
		$company->mobile = !empty ($data['mobile'])? $data['mobile'] : null ;	
		$company->billing_add_street = !empty ($data['billing_add_street'])? $data['billing_add_street'] : null ;
		$company->billing_add_city = !empty ($data['billing_add_city'])? $data['billing_add_city'] : null ;
		$company->billing_add_state = !empty ($data['billing_add_State'])? $data['billing_add_State'] : null ;
		$company->billing_add_zip_code = !empty ($data['billing_add_zip_code'])? $data['billing_add_zip_code'] : null ;
		$company->billing_add_country = !empty ($data['billing_add_country'])? $data['billing_add_country'] : null ;
		$company->billing_add_fax = !empty ($data['billing_add_fax'])? $data['billing_add_fax'] : null ;
		$company->shipping_add_street = !empty ($data['shipping_add_street'])? $data['shipping_add_street'] : null ;
		$company->shipping_add_city = !empty ($data['shipping_add_city'])? $data['shipping_add_city'] : null ;
		$company->shipping_add_state = !empty ($data['shipping_add_State'])? $data['shipping_add_State'] : null ;
		$company->shipping_add_zip_code = !empty ($data['shipping_add_zip_code'])? $data['shipping_add_zip_code'] : null ;
		$company->shipping_add_country = !empty ($data['shipping_add_country'])? $data['shipping_add_country'] : null ;
		$company->shipping_add_fax = !empty ($data['shipping_add_fax'])? $data['shipping_add_fax'] : null ;

		if($company->save()){
			if(!empty($data['first_name'])){
				$contact = new Contact();
				$contact->first_name = $data['first_name'];
				$contact->last_name = $data['last_name'];
				$contact->primary = 1;
				$contact->company_id = $company->id;
                if($contact->save())
                {
                    $message="Successfully Saved along with contact!";
				}
                else
                {
                    $status = false;
                    $message= $contact->getErrors();
				}
			}
            else
            {
                $message="Successfully Saved!";
			}
		}
        else
        {
             $status = false;
             $message= $company->getErrors();
        }
        $response['status'] = $status;
		$response['message'] = $message;
		$response['data'] = $company;

        return $response;
    }
}	   


