<?php
namespace App\Services;

use App\Events\Leave\LeaveApplied;
use App\Events\Leave\LeaveApproved;
use App\Events\Leave\LeaveCancelled;
use App\Events\Leave\LeaveRejected;
use App\Jobs\MessagingApp\PostMessageUserJob;
use App\Models\Admin\Calendar;
use App\Models\Admin\Leave;
use App\Models\Admin\Project;
use App\Models\Admin\ProjectResource;
use App\Models\LeaveOverlapRequest;
use App\Models\Leave\UserLeaveTransaction;
use App\Models\Month;
use App\Models\ProjectManagerLeaveApproval;
use App\Models\SystemSetting;
use App\Models\User;
use App\Models\WorkingDayGroup;
use Auth;
use Carbon\Carbon;
use DB;
use Log;
use View;

class LeaveService
{

    public static function getLeaveYearlyDuration()
    {

        $from_year = date("Y", strtotime("-1 year")); // From Last Year
        $to_year = date("Y"); // To Current Year
        $from_date_string = $from_year . '-12-25'; // From 25th Dec Last Year
        $to_date_string = $to_year . '-12-25'; // From 25th Dec Current Year

        $dates['from_date'] = date($from_date_string);
        $dates['to_date'] = date($to_date_string);

        return $dates;
    }
    public static function getUserLeaveReport($userId)
    {
        $user = User::find($userId);

        if (!$user) {
            return false;
        }

        $leaveDuration = self::getLeaveYearlyDuration();

        // $totalSickLeaves = SystemSetting::where('key', 'max_sick_leave')->first();
        // $totalPaidLeaves = SystemSetting::where('key', 'max_paid_leave')->first();

        // $from_year = !empty($leaveDuration['leaveDuration']['from_year']) ?  $leaveDuration['leaveDuration']['from_year'] :  date("Y",strtotime("-1 year")); // From Last Year
        // $to_year = !empty($leaveDuration['leaveDuration']['to_year']) ?  $leaveDuration['leaveDuration']['to_year'] :   date("Y"); // To Current Year
        // $from_date_string = !empty($leaveDuration['leaveDuration']['from_date_string']) ?  $leaveDuration['leaveDuration']['from_date_string'] :    $from_year.'-12-25'; // From 25
        // $to_date_string = !empty($leaveDuration['leaveDuration']['to_date_string']) ?  $leaveDuration['leaveDuration']['to_date_string'] :    $to_year.'-12-25';
        $from_date = !empty($leaveDuration['from_date']) ? $leaveDuration['from_date'] : date($from_date_string);
        $to_date = !empty($leaveDuration['to_date']) ? $leaveDuration['to_date'] : date($to_date_string);

        // if ($totalSickLeaves) {
        //     $totalSickLeaves = $totalSickLeaves->value;
        // }
        // if ($totalPaidLeaves) {
        //     $totalPaidLeaves = $totalPaidLeaves->value;
        // }
        //echo $user->joining_date;
        if (isset($user->joining_date)) {
            $leaves = $user->leaves->where('type', 'sick')->where('status', 'approved')->where('end_date', '>=', $from_date)->where('start_date', '<=', $to_date);
            $joiningDate = $user->joining_date;
            $totalSickLeaves = LeaveService::maxSickLeave($joiningDate);
            $consumedSickLeaves = 0;
            foreach ($leaves as $leave) {
                $consumedSickLeaves = $consumedSickLeaves + $leave->days;
            }
        } else {
            $totalSickLeaves = 0;
            $consumedSickLeaves = 0;
        }
        if (isset($user->confirmation_date)) {
            $leaves = $user->leaves->whereIn('type', ['paid', 'half'])->where('status', 'approved')->where('end_date', '>=', $from_date)->where('start_date', '<=', $to_date);
            $joiningDate = $user->joining_date;
            $totalPaidLeaves = LeaveService::maxPersonalLeave($joiningDate);
            $consumedPaidLeaves = 0;
            foreach ($leaves as $leave) {
                $consumedPaidLeaves = $consumedPaidLeaves + $leave->days;
            }
        } else {
            $totalPaidLeaves = 0;
            $consumedPaidLeaves = 0;
        }

        $consumedUnpaidLeaves = 0;
        $unpaidLeaves = $user->leaves->where('type', 'unpaid')->where('status', 'approved');
        foreach ($unpaidLeaves as $leave) {
            $consumedUnpaidLeaves = $consumedUnpaidLeaves + $leave->days;
        }
        $result['consumedSickLeaves'] = $consumedSickLeaves;
        $result['totalSickLeaves'] = $totalSickLeaves;
        $result['consumedPaidLeaves'] = $consumedPaidLeaves;
        $result['totalPaidLeaves'] = $totalPaidLeaves;
        $result['consumedUnpaidLeaves'] = $consumedUnpaidLeaves;
        return $result;
    }
    //old name : managerDetails
    public static function getUserReportee($userId)
    {
        $user = User::find($userId);
        $data = [];
        $reporting_manager = empty($user->reportingManager) ? null : $user->reportingManager;
        if ($reporting_manager) {
            $data['reporting_manager_name'] = $reporting_manager->name;
            $data['reporting_manager_phone'] = empty($reporting_manager->profile) || empty($reporting_manager->profile->phone) ? "Unavailable" : $reporting_manager->phone;
        }
        $data['team_leads'] = [];
        $projectResources = ProjectResource::with('project')
            ->where('user_id', $user->id)
            ->where('start_date', '<=', date('Y-m-d'))
            ->where(function ($query) {
                $query->where('end_date', null)
                    ->orWhere('end_date', '>=', date('Y-m-d'));
            })->get();
        if (!empty($projectResources)) {
            foreach ($projectResources as $resource) {
                if ($resource->project && $resource->project->projectManager && $resource->project->projectManager->id != $user->id) {
                    $tl = $resource->project->projectManager;
                    $data['team_leads'][$tl->name] = empty($tl->profile) || empty($tl->profile->phone) ? "Unavailable" : $tl->profile->phone;
                }
            }
        }
        return $data;
    }
    //accept the starting day of the week and return the count of holiday in that week
    public static function globalLeaveCount($date)
    {
        $nextdate = strtotime($date);
        $nextdate = date("Y-m-d", strtotime("+7 day", $nextdate));
        $globalLeave = Calendar::where('date', '>=', $date)->where('date', '<=', $nextdate)->get();
        if (count($globalLeave) > 0) {
            return count($globalLeave);
        }

        return false;
    }
    //accept the starting day of the week and return the count of leave in that week
    public static function personalLeaveCount($userId, $date)
    {
        $response['status'] = false;
        $response['data'] = null;
        $count = 0;
        for ($i = 0; $i < 5; $i++) {
            $personalLeave = Leave::where('start_date', '<=', $date)->where('end_date', '>=', $date)->where('user_id', $userId)->get();
            if (count($personalLeave) > 0) {
                $count++;
            }
            $date = strtotime($date);
            $date = date("Y-m-d", strtotime("+1 day", $date));
        }
        if ($count > 0) {
            return $count;
        }
        return false;
    }

    /**
     * Calculate total number of sick leaves eligible based upon joining date
     *
     * @param $joiningDate => date string
     *
     * @return number
     */
    public static function maxSickLeave($joiningDate)
    {
        $totalSickLeave = SystemSetting::where('key', 'max_sick_leave')->first();
        if ($totalSickLeave) {
            $totalSickLeave = $totalSickLeave->value;
        }

        $workingYr = LeaveService::getUpcomingYearEnd(date('Y-m-d'));

        $ts1 = strtotime($joiningDate);
        $ts2 = strtotime($workingYr);
        $year1 = date('Y', $ts1);
        $year2 = date('Y', $ts2);
        $month1 = date('m', $ts1);
        $month2 = date('m', $ts2);
        $diff = (($year2 - $year1) * 12) + ($month2 - $month1);
        if ($diff <= 11) {
            $totalSickLeave = LeaveService::calculateLeaves($totalSickLeave, $diff);
        }
        return round($totalSickLeave, 2);
    }

    /**
     * Calculate total number of paid leaves eligible based upon confirmation date
     *
     * @param string $confirmationDate => date string
     *
     * @return number
     */
    public static function maxPersonalLeave($joiningDate)
    {
        $totalPersonalLeave = SystemSetting::where('key', 'max_paid_leave')->first();
        if ($totalPersonalLeave) {
            $totalPersonalLeave = $totalPersonalLeave->value;
        }

        $workingYr = LeaveService::getUpcomingYearEnd(date('Y-m-d'));
        $ts1 = strtotime($joiningDate);
        $ts2 = strtotime($workingYr);
        $year1 = date('Y', $ts1);
        $year2 = date('Y', $ts2);
        $month1 = date('m', $ts1);
        $month2 = date('m', $ts2);
        $diff = (($year2 - $year1) * 12) + ($month2 - $month1) + 1;
        if ($diff <= 11) {
            $totalPersonalLeave = LeaveService::calculateLeaves($totalPersonalLeave, $diff);
        }
        return round($totalPersonalLeave, 2);
    }

    public static function calculateWorkingDays($startDate, $endDate, $id)
    {
        $user = User::find($id);
        $count = 0;
        if (!empty($user->workingDayGroup) && !empty($user->workingDayGroup->workingDays)) {
            $group = $user->workingDayGroup->workingDays;
            $nonWorkingDays = [];
            if (!empty($group)) {
                if (!$group->monday) {
                    $nonWorkingDays[] = 1;
                }
                if (!$group->tuesday) {
                    $nonWorkingDays[] = 2;
                }
                if (!$group->wednesday) {
                    $nonWorkingDays[] = 3;
                }
                if (!$group->thursday) {
                    $nonWorkingDays[] = 4;
                }
                if (!$group->friday) {
                    $nonWorkingDays[] = 5;
                }
                if (!$group->saturday) {
                    $nonWorkingDays[] = 6;
                }
                if (!$group->sunday) {
                    $nonWorkingDays[] = 7;
                }
            } else {
                $nonWorkingDays = [6, 7];
            }
        } else {
            $nonWorkingDays = [6, 7];
        }

        $startDate = date('Y-m-d', strtotime($startDate));
        $endDate = date('Y-m-d', strtotime($endDate));

        $holidays = Calendar::whereDate('date', '>=', $startDate)->whereDate('date', '<=', $endDate)->where('type', 'Holiday')->pluck('date')->toArray();
        for ($date = $startDate; strtotime($date) <= strtotime($endDate); $date = date('Y-m-d', strtotime($date . '+1 day'))) {
            if (in_array(date('N', strtotime($date)), $nonWorkingDays) || in_array(date('Y-m-d', strtotime($date)), $holidays)) {
                continue;
            } else {
                $count++;
            }
        }
        return $count;
    }

    /**
     * Calculate total number of holidays in between leaves
     *
     * @param $startDate => date string
     * @param $endDate => date string
     *
     * @return number
     */
    public static function calculateNonWorkingDays($startDate, $endDate)
    {
        $startDate = date('Y-m-d', strtotime($startDate));
        $endDate = date('Y-m-d', strtotime($endDate));
        $holidayCount = Calendar::whereDate('date', '>=', $startDate)->whereDate('date', '<=', $endDate)->count();
        return $holidayCount;
    }

    /**
     * Calculate total number of leaves applied(Working and Nonworking)
     *
     * @param $startDate => date string
     * @param $endDate => date string
     *
     * @return number
     */
    public static function calculateTotalNumberOfDays($startDate, $endDate)
    {
        $datediff = strtotime($startDate) - strtotime($endDate);
        return abs(floor($datediff / (60 * 60 * 24))) + 1;
    }

    /**
     * The function returns the no. of business days between two dates and it skips the holidays
     *
     * @param $startDate => (date)
     * @param $endDate => (date)
     * @param $holidayCount => (number) The number of holidays between start and end date
     *
     * @return number
     */
    public static function getWorkingDays($startDate, $endDate, $holidayCount)
    {
        // do strtotime calculations just once
        $endDate = strtotime($endDate);
        $startDate = strtotime($startDate);

        //The total number of days between the two dates. We compute the no. of seconds and divide it to 60*60*24
        //We add one to inlude both dates in the interval.
        $days = ($endDate - $startDate) / 86400 + 1;

        $no_full_weeks = floor($days / 7);
        $no_remaining_days = fmod($days, 7);

        //It will return 1 if it's Monday,.. ,7 for Sunday
        $the_first_day_of_week = date("N", $startDate);
        $the_last_day_of_week = date("N", $endDate);

        //---->The two can be equal in leap years when february has 29 days, the equal sign is added here
        //In the first case the whole interval is within a week, in the second case the interval falls in two weeks.
        if ($the_first_day_of_week <= $the_last_day_of_week) {
            if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week) {
                $no_remaining_days--;
            }
            if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week) {
                $no_remaining_days--;
            }
        } else {
            // (edit by Tokes to fix an edge case where the start day was a Sunday
            // and the end day was NOT a Saturday)

            // the day of the week for start is later than the day of the week for end
            if ($the_first_day_of_week == 7) {
                // if the start date is a Sunday, then we definitely subtract 1 day
                $no_remaining_days--;

                if ($the_last_day_of_week == 6) {
                    // if the end date is a Saturday, then we subtract another day
                    $no_remaining_days--;
                }
            } else {
                // the start date was a Saturday (or earlier), and the end date was (Mon..Fri)
                // so we skip an entire weekend and subtract 2 days
                $no_remaining_days -= 2;
            }
        }

        //The no. of business days is: (number of weeks between the two dates) * (5 working days) + the remainder
        //---->february in none leap years gave a remainder of 0 but still calculated weekends between first and last day, this is one         way to fix it
        $workingDays = $no_full_weeks * 5;
        if ($no_remaining_days > 0) {
            $workingDays += $no_remaining_days;
        }

        return $workingDays - $holidayCount;
    }

    /**
     * Checks For paid leave eligibility i.e.
     * Paid leave should be applied atleast 15 days before Leave Start.
     *
     * @param $startDate => string(date)
     * @param $endDate => string(date)
     *
     * @return boolean
     */
    public static function isPersonalLeaveEligible($startDate, $endDate, $requestedDays = 0)
    {

        //Number of days before a paid leave can be applied
        $paidLeavesInAdvance = SystemSetting::where('key', 'paid_leave_in_advance')->first();
        $paidLeavesInAdvance = $paidLeavesInAdvance ? $paidLeavesInAdvance : 15;
        $today = date('Y-m-d', strtotime("now"));
        $currentDate = date_create($today);
        $startDate = date('Y-m-d', strtotime($startDate));
        $startDate = date_create($startDate);
        $dateDifference = date_diff($currentDate, $startDate);
        $dateDifference = $dateDifference->days;

        if ($requestedDays == 1 && $dateDifference < 2) {
            return false;
        } elseif ($requestedDays == 2 && $dateDifference < 4) {
            return false;
        } elseif ($requestedDays == 3 && $dateDifference < 6) {
            return false;
        } elseif ($requestedDays > 3 && $dateDifference < $paidLeavesInAdvance) {
            return false;
        } else {
            return true;
        }
    }

    /**  Not Is Use (NIU)
     * Checks for no. of leaves applied
     *
     * @param $startDate => string(date)
     * @param $endDate => string(date)
     *
     * @return boolean
     */
    public static function isLeaveLengthValid($startDate, $endDate)
    {
        //Maximum number of consecutive leaves a user can take
        $maxConsecutiveLeaves = SystemSetting::where('key', 'max_consecutive_leaves')->first();
        $maxConsecutiveLeaves = $maxConsecutiveLeaves ? $maxConsecutiveLeaves->value : 14;

        $numberOfDays = LeaveService::calculateTotalNumberOfDays($startDate, $endDate);
        if ($numberOfDays > $maxConsecutiveLeaves) {
            return false;
        }

        return true;
    }

    public static function getUpcomingYearEnd($date)
    {
        $month = date('m', strtotime($date));
        if ($month <= 03) {
            return date('Y', strtotime($date)) . "-03-31";
        } else {
            return (date('Y', strtotime($date)) + 1) . "-03-31";
        }
    }

    public static function getLastApril($date)
    {
        $month = date('m', strtotime($date));
        if ($month <= 03) {
            return (date('Y', strtotime($date)) - 1) . "-04-01";
        } else {
            return (date('Y', strtotime($date))) . "-04-01";
        }
    }

    /**
     * Calculate leaves.If the leave is in decimal format then truncate accordingly
     *
     * @param $totalLeaves => number
     * @param $numberOfMonths => number
     *
     * @return number
     */
    public static function calculateLeaves($totalLeaves, $numberOfMonths)
    {
        $leavesPerMonth = $totalLeaves / 12;
        $leavesEligible = $leavesPerMonth * $numberOfMonths;

        //Truncate to nearest .5
        if (fmod($leavesEligible, 1) < .5) {
            $leavesEligible = floor($leavesEligible);
        } else {
            $leavesEligible = floor($leavesEligible) + 1;
        }
        return $leavesEligible;
    }

    /**
     * Check if start date is greater than end date
     *
     * @param $startDate => date string
     * @param $endDate => date string
     *
     * @return boolean
     */
    public static function isLeaveValid($startDate, $endDate)
    {
        $startDate = date("Y-m-d", strtotime($startDate));
        $endDate = date("Y-m-d", strtotime($endDate));

        //If start date comes after end date
        if ($startDate > $endDate) {
            return false;
        }

        return true;
    }

    /**
     * Check if leave is applied for current working year(1st April to 31st March)
     *
     * @param $startDate => date string
     * @param $endDate => date string
     * @param $role =>  string
     *
     * @return boolean
     */
    public static function isLeaveAppliedForCurrentWorkingYear($startDate, $endDate, $role = "user")
    {
        $maximumDate = LeaveService::getUpcomingYearEnd(date('Y-m-d'));
        //User can apply leave in between current date to upcoming March
        // if ($role == 'user') {
        //     $minimumDate = date('Y-m-d', strtotime("now"));
        //     $startDate = date("Y-m-d", strtotime($startDate));
        //     $endDate = date("Y-m-d", strtotime($endDate));
        //     if ($startDate < $minimumDate) {
        //         return false;
        //     }

        //     if (($startDate > $maximumDate) || ($endDate > $maximumDate)) {
        //         return false;
        //     }

        //     return true;
        // }

        // //Admin can apply leave in between Last April to upcoming March
        // if ($role == 'admin') {
        $minimumDate = LeaveService::getLastApril(date('Y-m-d'));
        $startDate = date("Y-m-d", strtotime($startDate));
        $endDate = date("Y-m-d", strtotime($endDate));
        if ($startDate < $minimumDate) {
            return false;
        }

        if (($startDate > $maximumDate) || ($endDate > $maximumDate)) {
            return false;
        }

        return true;
        //}
    }

    /**
     * Check if leave already exists for given date or overlaps
     *
     * @param $startDate => date string
     * @param $endDate => date string
     *
     * @return boolean
     */
    public static function isLeaveOverlapping($startDate, $endDate, $user)
    {
        // return true;        //disables leave overlap check
        $pstart = date("Y-m-d", strtotime($startDate . " -26 day"));
        $pend = date("Y-m-d", strtotime($endDate . " +26 day"));
        $days = LeaveService::calculateWorkingDays($pstart, $startDate, $user->id); //userid
        while ($days < 20) {
            $pstart = date("Y-m-d", strtotime($pstart . " -1 day"));

            $days = LeaveService::calculateWorkingDays($pstart, $startDate, $user->id); //userid
        }
        $days = LeaveService::calculateWorkingDays($endDate, $pend, $user->id); //userid
        while ($days < 20) {
            $pend = date("Y-m-d", strtotime($pend . " +1 day"));

            $days = LeaveService::calculateWorkingDays($endDate, $pend, $user->id); //userid
        }
        $startDate = $pstart;
        $endDate = $pend;
        $leaveOverlapCount = 0;

        //If any leave overlaps inbetween start and end date
        $leaveOverlapCount = Leave::where('user_id', $user->id)->whereDate('start_date', '>=', $startDate)
            ->whereDate('start_date', '<=', $endDate)
            ->whereIn('status', ['approved', 'pending'])
            ->whereIn('type', ['paid', 'sick'])
            ->count();
        if ($leaveOverlapCount > 0) {
            return false;
        }

        $leaveOverlapCount = Leave::where('user_id', $user->id)->whereDate('end_date', '>=', $startDate)
            ->whereDate('end_date', '<=', $endDate)
            ->whereIn('status', ['approved', 'pending'])
            ->whereIn('type', ['paid', 'sick'])
            ->count();
        if ($leaveOverlapCount > 0) {
            return false;
        }

        return true;
    }

    /**
     * Returns number of sick leaves remaining
     *
     * @param $user => user object
     *
     * @return number
     */
    public static function getRemainingSickLeave($user)
    {
        $leaves = $user->leaves->where('type', 'sick')->whereIn('status', ['approved', 'pending']);
        $joiningDate = $user->joining_date;
        $canTakeLeave = LeaveService::maxSickLeave($joiningDate);
        $leaveDays = 0;
        foreach ($leaves as $leave) {
            $leaveDays = $leaveDays + $leave->days;
        }
        $leftLeaves = $canTakeLeave - $leaveDays;

        return $leftLeaves;
    }

    /**
     * Returns number of paid leaves remaining
     *
     * @param $user => user object
     *
     * @return boolean
     */
    public static function getRemainingPaidLeave($user)
    {
        $leaves = $user->leaves->whereIn('status', ['approved', 'pending'])->whereIn('type', ['paid', 'half']);
        $joiningDate = $user->joining_date;
        if ($joiningDate == null) {
            $canTakeLeave = 0;
        } else {
            $canTakeLeave = LeaveService::maxPersonalLeave($joiningDate);
        }
        $leaveDays = 0;
        foreach ($leaves as $leave) {
            $leaveDays = $leaveDays + $leave->days;
        }
        $leftLeaves = $canTakeLeave - $leaveDays;
        return $leftLeaves;
    }

    /**
     * Filters Leave
     *
     * @param $user => user object
     *
     * @return boolean
     */
    public static function filterLeave($input)
    {
        $leaveList = Leave::with('user');

        $today = date("Y-m-d");
        $after7days = date('Y-m-d', strtotime('7 days', strtotime($today)));

        if (!empty($input['user'])) {
            $leaveList->where('user_id', $input['user']);
        }

        if (!empty($input['start_date'])) {
            $fromDate = date("Y-m-d", strtotime($input['start_date']));
        } else {
            $fromDate = $today;
        }

        if (!empty($input['end_date'])) {
            $toDate = date("Y-m-d", strtotime($input['end_date']));
        } else {
            $toDate = $after7days;
        }

        $leaveList = $leaveList->where('start_date', '>=', $fromDate);
        $leaveList = $leaveList->where('end_date', '<=', $toDate);

        $leaveList = $leaveList->orderBy('id', 'DESC')->paginate(10);
        return $leaveList;
    }
    public static function verifyLeave($user, $leave)
    {
        $response = array('status' => true, 'message' => "success", 'id' => null);
        if ($leave->type == 'paid') {
            if ($leave->days > LeaveService::getRemainingPaidLeave($user)) {
                $response['status'] = false;
                $response['errors'] = "Not enough paid leaves left";
                return $response;
            }
            if ($leave->days > 3) {
                $noticeLength = Carbon::parse($leave->start_date)->diffInDays(Carbon::now()) + 1;
                if ($noticeLength < 15) {
                    $response['status'] = false;
                    $response['errors'] = "Request should be made atleast 15 days before leave start date";
                    return $response;
                }
            } else {
                $noticeLength = Carbon::parse($leave->start_date)->diffInDays(Carbon::now()) + 1;
                if ($noticeLength < 2 * ($leave->days)) {
                    $response['status'] = false;
                    $response['errors'] = "Request should be made atleast two days prior against every day of leave";
                    return $response;
                }
            }
        } elseif ($leave->type == 'sick' && $leave->days > LeaveService::getRemainingSickLeave($user)) {
            $response['status'] = false;
            $response['errors'] = "Not enough sick leaves left";
            return $response;
        }
    }
    public static function getWorkingDay($date)
    {
        $day = date('N', strtotime($date));
        if ($day < 6) {
            $holiday = Calendar::whereDate('date', $date)->get();
            if (empty($holiday)) {
                return date("d-M-Y H:i:s", strtotime($date));
            } else {
                if ($day == 1) {
                    $date = date("d M Y", strtotime("-3 day", strtotime($date)));
                } else {
                    $date = date("d M Y", strtotime("-1 day", strtotime($date)));
                }
            }
        } else {
            if ($day == 6) {
                $date = date("d M Y", strtotime("-1 day", strtotime($date)));
            } else {
                $date = date("d M Y", strtotime("-2 day", strtotime($date)));
            }
        }
    }
    public static function getTotalLeaves($start_date, $end_date, $user_id, $type)
    {
        $days = Leave::where('user_id', $user_id)->where(function ($query) use ($start_date, $end_date) {
            $query->where('start_date', '<=', $end_date)->orWhere('end_date', '<=', $start_date);
        })->where('status', 'approved');
        if ($type != 'all') {
            $days = $days->where('type', $type);
        }
        $days = $days->sum('days');
        return $days;
    }

    /*
    function : checkOverlappingLeave
    check if the emaployee leave overlaps with their collegue working on same project in a team.
    response :
     */
    public static function checkOverlappingLeave($userId, $startDate, $endDate, $leaveId = null)
    {

        $user = User::find($userId);
        $startDate = date_to_yyyymmdd($startDate);
        $endDate = date_to_yyyymmdd($endDate);
        $projects = $user->projects()->with('resources')->where('project_resources.start_date', '<=', $endDate)
            ->where(function ($query) use ($startDate, $endDate) {
                $query->where('project_resources.end_date', null)
                    ->orWhere('project_resources.end_date', '>=', $startDate);
            })->get();
        $data = [];
        $max = date_diff(date_create($startDate), date_create($endDate))->format('%a');
        $dates = [];
        $code = [];
        $holidays = Calendar::whereDate('date', '>=', $startDate)->whereDate('date', '<=', $endDate)->pluck('date')->toArray();
        $date = $startDate;
        for ($i = 0; $i <= $max; $i++) {
            $dates[] = date('D j M', strtotime($date));
            if (date('N', strtotime($date)) > 5) {
                $code[] = 1;
            } else {
                if (in_array($date, $holidays)) {
                    $code[] = 1;
                } else {
                    $code[] = 0;
                }
            }
            $date = date('Y-m-d', strtotime($date . ' +1 day'));
        }
        $count = 0;
        foreach ($projects as $project) {
            $coworkers = $project->resources()->with('leaves')->whereNotIn('project_resources.user_id', [$userId])
                ->where('start_date', '<=', $endDate)
                ->where(function ($query) use ($startDate, $endDate) {
                    $query->where('end_date', null)
                        ->orWhere('end_date', '>=', $startDate);
                })->get();
            $dataArray = [];
            $f = 0;
            if (!empty($coworkers)) {
                foreach ($coworkers as $coworker) {
                    $userDates = array_fill(0, count($dates), 0);
                    $flag = 0;
                    $leaves = $coworker->leaves()->where('leaves.start_date', '<=', $endDate)
                        ->where('leaves.end_date', '>=', $startDate)
                        ->whereIn('leaves.status', ['pending', 'approved'])->get();
                    if (!empty($leaves) && count($leaves) > 0) {
                        $flag = 1;
                        foreach ($leaves as $leave) {
                            $start = strtotime($leave->start_date) > strtotime($startDate) ?
                            date_diff(date_create($leave->start_date), date_create($startDate))->format('%a') :
                            0;
                            $end = strtotime($endDate) > strtotime($leave->end_date) ?
                            date_diff(date_create($leave->end_date), date_create($startDate))->format('%a') :
                            count($dates) - 1;
                            for ($i = $start; $i <= $end; $i++) {
                                $userDates[$i] = ucfirst($leave->type);
                            }
                        }
                    }
                    if ($flag == 1) {
                        $f = 1;
                        $leaveDetails = $coworker->leaves()->where('leaves.start_date', '<=', $endDate)
                            ->where('leaves.end_date', '>=', $startDate)->whereIn('leaves.status', ['pending', 'approved'])
                            ->first();
                        $name = $coworker->name;
                        $dataArray[] = ['name' => $name, 'dates' => $userDates, 'leave' => $leaveDetails];
                    }
                }
                if ($f == 1) {
                    $count++;
                    $name = $project->project_name;
                    $overlap_reason = '';
                    if ($leaveId != null) {
                        $overlap = LeaveOverlapRequest::where('project_id', $project->id)
                            ->where('leave_id', $leaveId)->where('user_id', $userId)->first();
                        $overlap_reason = !empty($overlap) ? $overlap->reason : '';
                    }
                    $data[] = ['name' => $name, 'users' => $dataArray, 'id' => $project->id, 'overlap_reason' => $overlap_reason];
                }
            }
        }
        return ['projects' => $data, 'count' => $count, 'dates' => $dates, 'code' => $code];
    }

    public static function saveData($request, $role = "user")
    {
        try {
            // DB::beginTransaction();
            $loggedUser = Auth::user();
            if (!($loggedUser->hasRole('admin') || $loggedUser->hasRole('human-resources'))) {
                $request['status'] = 'pending';
            }
            $response = array('status' => true, 'message' => "success", 'id' => null);
            $action_date = date("Y-m-d");
            $leave = new Leave();
            $leave->user_id = $request['user_id'];

            if ($request['start_date']) {
                $leave->start_date = date("Y-m-d", strtotime($request['start_date']));
            }
            if ($request['end_date']) {
                $leave->end_date = date("Y-m-d", strtotime($request['end_date']));
            }

            $leave->status = !empty($request['status']) ? $request['status'] : "pending";
            $leave->reason = isset($request['reason']) ? $request['reason'] : '';
            $leave->type = isset($request['type']) ? $request['type'] : null;
            if ($leave->status == 'approved') {
                $approver_id = \Auth::id();
            }
            $user = User::find($leave->user_id);
            if ($leave->type == "half") {
                $leave->half = $request['half'];
                $leave->days = 0.5;
            } else {
                $leave->days = LeaveService::calculateWorkingDays($request['start_date'], $request['end_date'], $user->id); //userid
            }
            if ($role != 'admin') {
                $response = Leave::ValidateLeave($user->id, $request, 0, $role);
                if ($response['status'] == false) {
                    return $response;
                }
            }
            if ($leave->save()) {
                if ($leave->status == "pending") {
                    $resources = ProjectResource::where('user_id', $leave->user->id)->where('start_date', '<=', $leave->end_date)->where(function ($query) use ($leave) {
                        $query->where('end_date', null)->orWhere('end_date', '>=', $leave->start_date);
                    })->pluck('project_id')->toArray();
                    $projects = Project::with('projectManager')->whereIn('id', $resources)->get();
                    $array = [];
                    foreach ($projects as $project) {
                        if (empty($project->projectManager)) {
                            // $response['status'] = false;
                            // $response['errors'] =  'Project Manager not assigned';
                            // return $response;
                            continue;
                        }
                        if (in_array($project->projectManager->id, $array)) {
                            continue;
                        }
                        if ($leave->user->id == $project->projectManager->id || $leave->user->parent_id == $project->projectManager->id) {
                            continue;
                        }
                        $approval = new ProjectManagerLeaveApproval();
                        $approval->user_id = $project->projectManager->id;
                        $approval->leave_id = $leave->id;
                        $approval->status = $leave->status;
                        $approval->save();
                        $array[] = $project->projectManager->id;
                    }
                    if (!empty($request['overlap_reasons'])) {
                        self::saveOverlappingDetails($request['overlap_reasons'], $leave->id, $leave->user_id);
                    }
                    $response['id'] = $leave->id;
                    $response['leave'] = $leave;
                    // Leave::notifyLeaveApplicationToAdmin($leave->id);

                } else {
                    $response['id'] = $leave->id;
                    $response['leave'] = $leave;
                    self::approveLeave($leave->id, $approver_id, $action_date);

                    //Leave::notifyLeaveAction($leave->id);
                }
            } else {
                $response['status'] = false;
                $response['errors'] = $leave->getErrors();
                return $response;
            }
            if ($leave->status == 'pending') {
                event(new LeaveApplied($leave->id));
            }
            return $response;
            // DB::commit();
        } catch (Exception $e) {
            \Log::info($e);

            // DB::rollback();
        }
    }
    public static function saveOverlappingDetails($overlap_reasons, $leave_id, $user_id)
    {
        foreach ($overlap_reasons as $overlap_reason) {
            try {
                // DB::beginTransaction();
                $leaveOverlapObj = new LeaveOverlapRequest();
                $leaveOverlapObj->user_id = $user_id;
                $leaveOverlapObj->leave_id = $leave_id;
                $leaveOverlapObj->project_id = $overlap_reason['project_id'];
                $leaveOverlapObj->reason = !empty($overlap_reason['comment']) ? $overlap_reason['comment'] : null;
                if (!$leaveOverlapObj->save()) {
                    \Log::info(json_encode($leaveOverlapObj->getErrors()));
                }
                // DB::commit();
            } catch (Exception $e) {
                \Log::info($e);
                // DB::rollback();
            }
        }
    }

    public static function teamLeadStatuses($leave_id)
    {
        $leave = Leave::with('user', 'projectManagerApprovals')->where('id', $leave_id)->first();
        $managerInfo = [];
        $projectIds = ProjectResource::getActiveProjectIdArray($leave->user->id, $leave->start_date, $leave->end_date);
        if (!empty($leave->projectManagerApprovals) && !empty($projectIds)) {
            foreach ($leave->projectManagerApprovals as $item) {
                if (!empty($item->manager)) {
                    $projects = Project::where('project_manager_id', $item->user_id)->whereIn('id', $projectIds)->pluck('project_name')->toArray();
                    $projects = implode(" | ", $projects);
                    if (empty($projects)) {
                        $projects = "No active projects";
                    }
                    $managerInfo[] = array('name' => $item->manager->name, 'id' => $item->manager->id, 'status' => $item->status, 'projects' => $projects, 'date' => date_in_view($item->updated_at));
                }
            }
        }
        return $managerInfo;
    }
    public static function previousLeaves($userId, $year)
    {
        $data = Leave::where('user_id', $userId)->where('end_date', '>=', $year)->where('status', 'approved')->orderBy('start_date', 'desc')->get();
        return $data;
    }

    public static function sickLeaves($userId, $year)
    {
        $data = Leave::where('user_id', $userId)->where('end_date', '>=', $year)->where('start_date', '<=', date('Y-m-d', strtotime('+1 year', strtotime($year))))->where('type', 'sick')->where('status', 'approved')->sum('days');
        return $data;
    }

    public static function earnedLeaves($userId, $year)
    {
        $data = Leave::where('user_id', $userId)->where('end_date', '>=', $year)->where('start_date', '<=', date('Y-m-d', strtotime('+1 year', strtotime($year))))->where('type', 'paid')->where('status', 'approved')->sum('days');
        return $data;
    }

    public static function getTeamLeads($leaveId)
    {
        $data = [];
        $leave = Leave::find($leaveId);
        $teamLeadsArray = $leave->projectManagerApprovals()->pluck('user_id')->toArray();
        $data = User::whereIn('id', $teamLeadsArray)->get();
        return $data;
    }

    public static function getProjectsManagers($leaveId)
    {
        $projectManagers = [];
        $projectManager = [];
        $leave = Leave::find($leaveId);
        if (empty($leave)) {
            return [];
        }
        $projectResources = ProjectResource::with('project')->where('user_id', $leave->user_id)
            ->where('start_date', '<=', $leave->end_date)
            ->where(function ($query) use ($leave) {
                $query->where('end_date', null)
                    ->orWhere('end_date', '>=', $leave->start_date);
            })->get();

        if (!empty($projectResources)) {
            foreach ($projectResources as $projectResource) {
                $projectManager['name'] = !empty($projectResource->project->projectManager->name) ? $projectResource->project->projectManager->name : '';
                +$projectManager['email'] = !empty($projectResource->project->projectManager->email) ? $projectResource->project->projectManager->email : '';
                $projectManagers[] = $projectManager;
                $projectManager = [];
            }
        }
        return $projectManagers;
    }

    public static function approveLeave($leave_id, $approver_id, $action_date)
    {
        $leave = Leave::find($leave_id);
        DB::beginTransaction();
        try {
            if ($leave) {
                $leave->approver_id = $approver_id;
                $leave->action_date = $action_date;
                $leave->status = "approved";

                if ($leave->save()) {

                    $data['user_id'] = $leave->user_id;
                    $data['calendar_year_id'] = $leave->calendar_year_id;
                    $data['leave_type_id'] = $leave->leave_type_id;
                    $data['date'] = $leave->end_date;
                    $data['days'] = $leave->days;
                    $data['reference_type'] = 'App\Models\Leave\Leave';
                    $data['reference_id'] = $leave->id;

                    $transObj = UserLeaveTransaction::saveData($data);

                    if ($transObj) {
                        DB::commit();
                        event(new LeaveApproved($leave->id));
                        return true;
                    }
                }
            }
        } catch (Exception $e) {
            DB::rollback();
        }
        return false;
    }

    public static function cancelLeave($leave_id, $approver_id, $action_date)
    {
        $leave = Leave::find($leave_id);
        if ($leave) {
            $leave->approver_id = $approver_id;
            $leave->action_date = $action_date;
            $leave->status = "cancelled";
            if ($leave->save()) {
                event(new LeaveCancelled($leave->id));
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    public static function rejectLeave($leave_id, $approver_id, $action_date)
    {
        $leave = Leave::find($leave_id);
        if ($leave) {
            $leave->approver_id = $approver_id;
            $leave->action_date = $action_date;
            $leave->status = "rejected";
            if ($leave->save()) {
                event(new LeaveRejected($leave->id));
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static function isTeamLeadWithStatus($user_id, $teamLeadsArray)
    {
        $response = [];
        $response['is_team_lead'] = false;
        foreach ($teamLeadsArray as $teamLead) {

            if ($user_id == $teamLead['id']) {
                $response['is_team_lead'] = true;
                $response['lead_status'] = !empty($teamLead['status']) ? $teamLead['status'] : '';
            }
        }
        return $response;
    }

    public static function isTeamLeadbyLeaveId($user_id, $leave_id)
    {
        $flag = false;
        $leave = Leave::with('user', 'projectManagerApprovals')->where('id', $leave_id)->first();

        if (!empty($leave->projectManagerApprovals)) {
            foreach ($leave->projectManagerApprovals as $item) {
                if ($item->user_id == $user_id) {
                    $flag = true;
                }
            }
        }
        return $flag;
    }

    public static function validateLeaveCancel($leave_id)
    {
        $message = '';
        $status = true;
        $response = [];
        $leave = Leave::find($leave_id);
        if (!$leave) {
            $status = false;
            $message = 'Leave not found';
        } else {
            if ($leave->status == 'rejected' || $leave->status == 'cancelled') {
                $status = false;
                $message = 'Leave is no longer in pending or approved state and hence can not be cancelled';
            } else {
                $user = Auth::user();
                if (!$user) {
                    $status = false;
                    $message = 'Invalid access';
                } else {
                    $current_date = date('Y-m-d');
                    // LEAVE START DATE CHECK
                    // if (strtotime($leave->start_date) < strtotime($current_date) && !($user->hasRole('admin'))) {
                    //     $status = false;
                    //     $message = 'The leave is already in progress';
                    // } else {
                    // USER ROLE VALIDATIONS
                    if ($user->hasRole('user')) {
                        if (!($user->hasRole('admin') || $user->hasRole('human-resources') || $user->hasRole('reporting-manager'))) {
                            if ($leave->user_id != $user->id) {
                                $status = false;
                                $message = 'Invalid Request';
                            }
                        }
                    } else {
                        $status = false;
                        $message = 'User must have the user role';
                    }
                    // RM ROLE VALIDATIONS
                    if (!($user->hasRole('admin')) && $user->hasRole('reporting-manager') && $user->id != $leave->user_id) {
                        $reporting_manager = UserService::getReportingManager($leave->user_id);
                        if ($user->id != $reporting_manager['id']) {
                            $status = false;
                            $message = 'Unauthorized Request';
                        }
                    }
                    // }
                }
            }
        }
        $response['status'] = $status;
        $response['message'] = $message;
        return $response;
    }

    public static function sendReminder($lead_id, $user_name, $leave_id, $display_text)
    {
        $leaveObj = Leave::find($leave_id);
        if (!$leaveObj) {
            return;
        }

        $message = View::make('messaging-app.slack.leave.notify', ['user_name' => $user_name, 'leaveObj' => $leaveObj, 'display_text' => $display_text])->render();
        $attachment = null;
        $job = (new PostMessageUserJob($lead_id, $message, $attachment));
        dispatch($job);

    }

}
