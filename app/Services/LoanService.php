<?php namespace App\Services;

use Validator;
use App\Models\User;
use App\Models\Loan;
use App\Models\Admin\LoanRequest;
use App\Models\LoanTransaction;
use App\Models\Admin\File;
use App\Jobs\Loan\LoanApplicationApproveJob;
use App\Events\LoanCompleted;

use Input;
use Redirect;
use Auth;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use Config;
use Exception;
use Carbon\Carbon;
use App\Models\Appraisal\Appraisal;
use App\Models\Appraisal\AppraisalBonusType;
use App\Models\Appraisal\AppraisalBonus;

class LoanService {

    public static function saveLoanRequest($input, $status, $file_id)
    {
      $response = ['status' => false, 'message' => []];
      if($input['emi'] > $input['amount']) 
      {
          $response['message'] = "EMI cant be greater than loan amount";
          return $response;
      }
      // Save Details in Loan Request table
      $loan_request = new LoanRequest;
      $loan_request->user_id = $input['user_id'];
      $loan_request->approved_amount = $input['amount'];
      $loan_request->amount = $input['amount'];
      $loan_request->type = $input['type'];
      $loan_request->description = empty($input['description']) ? null : $input['description'];
      $loan_request->emi = $input['emi'];
      $loan_request->status = $status;
      if (!$loan_request->save() )
      {
          $response['message'] = "Loan Request could not be raised";
          return $response;
      }
      if ( $file_id )
      {
        $file = File::find($file_id);
        $file->reference_id = $loan_request->id;
        $file->save();
      }

      if( empty($input['payment_detail']) || empty($input['payment_mode']) || empty($input['transaction_id']) 
          || empty($input['payment_date']) )  
      {
        $response['message'] = "Payment details missing";
        return $response;
      }
      $data_array = ['payment_detail' => $input['payment_detail'], 'payment_mode' => $input['payment_mode'], 
                      'transaction_id' => $input['transaction_id'], 'payment_date' => date_to_yyyymmdd($input['payment_date']),
                      'loan_request_id' => $loan_request->id];
      // $job = (new LoanRequestDisburseJob($loan_request->id))->onQueue('loan-request');
      // dispatch($job);
      $job = (new LoanApplicationApproveJob($loan_request->id, $data_array))->onQueue('loan-request');
      dispatch($job);

      $response['status'] = true;
      return $response;
    }

    public static function updateLoanRequest($loanId, $input, $file_id)
    {
        $response = ['status' => false, 'message' => []];
        $status = 'disbursed';
        if($input['emi'] > $input['amount']) 
        {
            $response['message'] = "EMI cant be greater than loan amount";
            return $response;
        }
        if( empty($input['payment_detail']) || empty($input['payment_mode']) || empty($input['transaction_id']) 
            || empty($input['payment_date']) )  
        {
            $response['message'] = "Payment details missing";
            return $response;
        }
        // Update Loan Table
        $loanObj = Loan::find($loanId);
        
        if ( !empty($loanObj->loan_request_id) )
        {
            if ( $file_id )
            {
                $file = File::find($file_id);
                $exisitngFile = File::where('reference_id',$loanObj->loan_request_id)
                                    ->where('reference_type','App\Models\Admin\LoanRequest')->first();
                if ( !empty($exisitngFile) )
                {
                    $exisitngFile->delete();
                }
                $file->reference_id = $loanObj->loan_request_id;
                $file->save();
            }            
        }
        else
        {
            // Save Details in Loan Request table
            $loan_request = new LoanRequest;
            $loan_request->user_id = $input['user_id'];
            $loan_request->approved_amount = $input['amount'];
            $loan_request->amount = $input['amount'];
            $loan_request->type = $input['type'];
            $loan_request->description = empty($input['description']) ? null : $input['description'];
            $loan_request->emi = $input['emi'];
            $loan_request->status = $status;
            if (!$loan_request->save() )
            {
                $response['message'] = "Loan Request could not be raised";
                return $response;
            }
            
            $loanObj->user_id = $input['user_id'];
            $loanObj->amount = $input['amount'];
            $loanObj->type = $input['type'];
            $loanObj->emi = $input['emi'];
            $loanObj->description = $input['description'];
            $loanObj->application_date = $input['application_date'];
            $loanObj->payment_details = $input['payment_detail'];
            $loanObj->payment_mode = $input['payment_mode'];
            $loanObj->transaction_id = $input['transaction_id'];
            $loanObj->payment_date =$input['payment_date'];
            $loanObj->loan_request_id = $loan_request->id;
            if($loanObj->save()) 
            {
                // add a loan disbursal transaction detail
                $data['loan_id'] = $loanObj->id;
                $data['amount'] = $loanObj->amount;
                $data['type'] = 'debit';
                $data['comment'] = "Loan disbursed to user";
                $data['paid_on'] = $loanObj->payment_date;
                LoanTransaction::saveToDB($data);
                $response['status'] = true;
            }
            if ( $file_id )
            {
                $file = File::find($file_id);
                $file->reference_id = $loanObj->loan_request_id;
                $file->save();
            }
        }
        $response['status'] = true;
        return $response;
    }

    public static function createLoanTransaction($loan_id, $input)
    {
        $status = true;
        $message = '';
        $loanTransactionObj = new LoanTransaction();
        $loanTransactionObj->loan_id = $loan_id;
        $loanTransactionObj->amount = $input['amount'];
        $loanTransactionObj->paid_on = $input['pay_date'];
        $loanTransactionObj->status = 'paid';
        $loanTransactionObj->method = $input['method'];
        $loanTransactionObj->comment = !empty($input['comment']) ? $input['comment'] : '' ;
        $loanTransactionObj->type = !empty($input['type']) ? $input['type'] : 'credit' ;
        if (!$loanTransactionObj->save())
        {
            $status = false;
            $message = $loanTransactionObj->getErrors();
        }
        else
        {
            $loanObj = Loan::find($loan_id);
            $loanObj->remaining = $loanObj->remaining - $input['amount'];
            $loanObj->save();
        }
        $response['status'] = $status;
        $response['message'] = $message;
        return $response;
    }

    public static function updateLoanStatus($loan_id, $loan_status)
    {
        $status = true;
        $message = '';

        $loanObj = Loan::find($loan_id);
        if( empty($loanObj) )
        {
            $status = false;
            $message = 'Loan not found';
        }

        $loanObj->status = $loan_status;
        if (!$loanObj->save())
        {
            $status = false;
            $message = $loanObj->getErrors();
        }
        if ( $loan_status == 'completed' )
        {
            event(new LoanCompleted($loan_id));
        }
        $response['status'] = $status;
        $response['message'] = $message;
        return $response;
    }
    
    public static function completeLoanPayment($loan_id, $input)
    {
        $status = true;
        $message = '';

        $loanObj = Loan::find($loan_id);
        if( empty($loanObj) )
        {
            $status = false;
            $message = 'Loan not found';
        }
        $input['amount'] = $loanObj->remaining;
        $input['type'] = 'credit';
        $result = self::createLoanTransaction($loan_id, $input);
        
        if ( !$result['status'] )
        {
            $status = false;
            $message = $result['message'];
        }
        else
        {
            $loan_status = 'completed';
            $updateStatus = self::updateLoanStatus($loan_id, $loan_status);
            if (!$updateStatus['status'])
            {
                $status = false;
                $message = $updateStatus['message'];
            }
        }

        $response['status'] = $status;
        $response['message'] = $message;
        return $response;
    }

    public static function loanEligibleCheck($userId,$appraisalBonusId,$amount)
    {
        $response['status'] = false;
        $userObj = User::find($userId);
        if(!$userObj) {
            $response['error'] = "Invalid User #".$userId;
                $response['status'] = true;
            return $response;
        }
        $effectiveAppraisal = Appraisal::where('user_id',$userId)->whereDate('effective_date','<=',date('Y-m-d'))->first();
        if(!$effectiveAppraisal)
        {
            $response['error'] = "No Appraisal Found!";
            $response['status'] = true;
            return $response;
        }
        if(!$appraisalBonusId){
            $response['appraisal_bonus_id'] = null;
            return $response;
        }
        $appraisalBonuTypeObj = AppraisalBonusType::find($appraisalBonusId);
        if(!$appraisalBonuTypeObj) {
            $response['error'] = "Invalid Bonus Type #".$appraisalBonusId;
            $response['status'] = true;
            return $response;
        }
        $appraisalBonusObj = AppraisalBonus::where('appraisal_id',$effectiveAppraisal->id)->where('appraisal_bonus_type_id',$appraisalBonusId)->first();
        if(!$appraisalBonusObj) {
            $response['error'] = "Not elegible to apasdply for ".$appraisalBonuTypeObj->description;
            $response['status'] = true;
            return $response;

        }
        if($amount > $appraisalBonusObj->value) {
            $response['error'] = "Loan amount cannot exceed more than ".$appraisalBonuTypeObj->description ." amount";
            $response['status'] = true;
            return $response;

        }
        $response['appraisal_bonus_id'] = $appraisalBonusObj->id;
        return $response;
    }
}