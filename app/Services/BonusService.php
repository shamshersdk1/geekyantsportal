<?php namespace App\Services;

use App\Events\BonusRequest\BonusRequestApproved;
use App\Events\BonusRequest\BonusRequestRaised;
use App\Events\BonusRequest\BonusRequestRejected;
use App\Jobs\MessagingApp\PostMessageChannelJob;
use App\Jobs\MessagingApp\PostMessageUserJob;
use App\Models\Admin\AdditionalWorkDaysBonus;
use App\Models\Appraisal\Appraisal;
use App\Models\Admin\Bonus;
use App\Models\Admin\Leave;
use App\Models\Admin\LeaveType;
use App\Models\Admin\OnsiteAllowance;
use App\Models\Admin\OnSiteBonus;
use App\Models\Audited\Bonus\BonusConfirmed;
use App\Models\BonusRequest;
use App\Models\BonusRequestProject;
use App\Models\Bonus\AdditionalWorkDayProject;
use App\Models\Leave\UserLeaveBalance;
use App\Models\Month;
use App\Models\NonworkingCalendar;
use App\Models\Transaction;
use App\Models\User;
use App\Models\UserTimesheet;
use App\Models\User\UserTimesheetExtra;
use App\Models\Week;
use App\Services\Calendar as UserCalendar;
use Auth;
use DB;
use Exception;

class BonusService
{

    public static function getUserBonus($userId, $month, $year, $projectId)
    {
        $dataArray = [];
        $yr = date('Y', strtotime($year));
        $mon = date('m', strtotime($month));

        for ($d = 1; $d <= 31; $d++) {
            $time = mktime(12, 0, 0, $mon, $d, $yr);
            if (date('m', $time) == $mon) {
                $date = date('Y-m-d', $time);
                // echo "Date ==>". $date;
                $dataArray[$date] = [
                    'weekend' => self::checkUserHasWorked($userId, $date, $projectId),
                    'on_site' => self::checkUserOnSite($userId, $date, $projectId),
                ];
            }
        }

        return $dataArray;
        // echo "<pre>";
        // print_r($dataArray);
        // echo "</pre>";
    }

    public static function checkUserHasWorked($userId, $date, $projectId)
    {
        // echo "DATE in checkUserHasWorked".$date;
        $bonusObj = AdditionalWorkDaysBonus::where('employee_id', $userId)->where('reference_id', $projectId)->whereDate('from_date', date('Y-m-d', strtotime($date)))->first();
        if ($bonusObj) {
            // echo "<pre>";
            // print_r($bonusObj->toArray());
            // echo "</pre>";
            return [
                "id" => $bonusObj->id,
                "value" => true,
                "status" => $bonusObj->status,
                "data" => $bonusObj,
            ];
        }
        return [
            "id" => null,
            "value" => false,
            "status" => null,
            "data" => null,
        ];

    }

    public static function checkUserOnSite($userId, $date, $projectId)
    {
        // echo "DATE in checkUserHasWorked".$userId;
        $bonusObj = OnSiteBonus::where('employee_id', $userId)->where('reference_id', $projectId)->whereDate('from_date', date('Y-m-d', strtotime($date)))->first();
        if ($bonusObj) {
            // echo "<pre>";
            // print_r($bonusObj->toArray());
            // echo "</pre>";
            return [
                "id" => $bonusObj->id,
                "value" => true,
                "status" => $bonusObj->status,
                "data" => $bonusObj,
            ];
        }
        return [
            "id" => null,
            "value" => false,
            "status" => null,
            "data" => null,
        ];

    }

    public static function applyForOnSiteBonus($userId, $date, $onSiteType, $projectId)
    {
        $alreadyApplied = OnSiteBonus::where('employee_id', $userId)->where('reference_id', $projectId)->whereDate('date', date('Y-m-d', strtotime($date)))->first();
        if (!$alreadyApplied) {
            $onSiteBonus = new OnSiteBonus();
            $onSiteBonus->employee_id = $userId;
            $onSiteBonus->type = $onSiteType;
            $onSiteBonus->reference_id = $projectId;
            $onSiteBonus->reference_type = 'App\Models\Admin\Project';
            $onSiteBonus->amount = 0.00;
            $onSiteBonus->status = 'pending';
            $onSiteBonus->to_date = !empty(date_to_yyyymmdd($date)) ? date_to_yyyymmdd($date) : null;
            $onSiteBonus->from_date = !empty(date_to_yyyymmdd($date)) ? date_to_yyyymmdd($date) : null;
            $onSiteBonus->date = !empty(date_to_yyyymmdd($date)) ? date_to_yyyymmdd($date) : null;
            $onSiteBonus->notes = "";
            $onSiteBonus->created_by = $userId;
            if (!$onSiteBonus->save()) {
                return ['success' => false, 'error' => true, 'result' => null, 'message' => "Unable to add request for on site bonus."];
            }
            $onSiteBonusObj = OnSiteBonus::where('id', $onSiteBonus->id)->with('user', 'reviewer')->first();
            return ['success' => true, 'error' => false, 'result' => $onSiteBonusObj];
        } else {
            return ['success' => false, 'error' => true, 'result' => null, 'message' => "Data already exists."];
        }

    }

    public static function applyForAdditionalWorkingBonus($userId, $date, $projectId)
    {
        $alreadyApplied = AdditionalWorkDaysBonus::where('employee_id', $userId)->where('reference_id', $projectId)->whereDate('date', date('Y-m-d', strtotime($date)))->first();
        if (!$alreadyApplied) {
            $additionalWorkDaysBonus = new AdditionalWorkDaysBonus();
            $additionalWorkDaysBonus->employee_id = $userId;
            $additionalWorkDaysBonus->type = "weekend";
            $additionalWorkDaysBonus->reference_id = $projectId;
            $additionalWorkDaysBonus->reference_type = 'App\Models\Admin\Project';
            $additionalWorkDaysBonus->amount = 0.00;
            $additionalWorkDaysBonus->status = 'pending';
            $additionalWorkDaysBonus->to_date = !empty(date_to_yyyymmdd($date)) ? date_to_yyyymmdd($date) : null;
            $additionalWorkDaysBonus->from_date = !empty(date_to_yyyymmdd($date)) ? date_to_yyyymmdd($date) : null;
            $additionalWorkDaysBonus->date = !empty(date_to_yyyymmdd($date)) ? date_to_yyyymmdd($date) : null;
            $additionalWorkDaysBonus->notes = "";
            $additionalWorkDaysBonus->created_by = $userId;
            if (!$additionalWorkDaysBonus->save()) {
                return ['success' => false, 'error' => true, 'result' => null, 'message' => "Unable to add request for on site bonus."];
            }
            $additionalWorkDaysBonusObj = AdditionalWorkDaysBonus::where('id', $additionalWorkDaysBonus->id)->with('user', 'reviewer')->first();
            return ['success' => true, 'error' => false, 'result' => $additionalWorkDaysBonusObj];
        } else {
            return ['success' => false, 'error' => true, 'result' => null, 'message' => "Data already exists."];
        }

    }

    public static function deleteRequestForOnSiteBonus($id)
    {
        $data = OnSiteBonus::find($id);
        if ($data) {
            if ($data->delete()) {
                // return redirect('/admin/asset-management/asset')->with('message', 'Successfully Deleted');
                return ['success' => true, 'error' => false, 'result' => $data];
            } else {
                return ['success' => true, 'error' => false, 'result' => $data, 'message' => 'Unable to delete'];
            }
        } else {
            return ['success' => true, 'error' => false, 'result' => $data, 'message' => 'Data already deleted.'];
        }
    }

    public static function deleteRequestForAdditionWorkingBonus($id)
    {
        $data = AdditionalWorkDaysBonus::find($id);
        if ($data) {
            if ($data->delete()) {
                // return redirect('/admin/asset-management/asset')->with('message', 'Successfully Deleted');
                return ['success' => true, 'error' => false, 'result' => $data];
            } else {
                return ['success' => true, 'error' => false, 'result' => $data, 'message' => 'Unable to delete'];
            }
        } else {
            return ['success' => true, 'error' => false, 'result' => $data, 'message' => 'Data already deleted.'];
        }
    }

    public static function checkUserBonusDay($user_id, $date)
    {
        $response['additional_bonus'] = AdditionalWorkDaysBonus::with('projects')->where('user_id', $user_id)->whereDate('date', $date)->whereIn('status', ['pending', 'approved'])->first();
        $response['onsite_bonus'] = OnSiteBonus::with('onsiteAllowance.project')->where('user_id', $user_id)->whereDate('date', $date)->whereIn('status', ['pending', 'approved'])->first();

        $response['extra_bonus'] = [];
        return $response;
        //$additionalBonuses = AdditionalWorkDaysBonus::where('user_id',$user_id)->whereDate('date', $date)->whereIn('status',['pending','approved'])->first();

        // $count = BonusRequest::where('user_id', $user_id)->where('date', $date)->count();
        // if ($count > 0) {
        //     $bonuses = BonusRequest::with('bonusRequestProjects')->where('user_id', $user_id)->where('date', $date)->get()->toArray();
        //     return $bonuses;
        // } else {
        //     return null;
        // }
    }

    public static function applyBonusRequest($data)
    {
        $response['status'] = false;
        $response['error'] = false;
        $response['result'] = null;
        $response['message'] = null;

        $date = !empty($data['date']) ? $data['date'] : null;
        $userId = !empty($data['user_id']) ? $data['user_id'] : null;
        $type = !empty($data['type']) ? $data['type'] : null;
        $isHoliday = !empty($data['is_holiday']) ? $data['is_holiday'] : null;
        $isWeekend = !empty($data['is_weekend']) ? $data['is_weekend'] : null;
        $onLeave = !empty($data['on_leave']) ? $data['on_leave'] : null;
        $redeem = !empty($data['redeem']) ? $data['redeem'] : null;
        $duration = !empty($data['duration']) ? $data['duration'] : null;
        $onsiteAllowanceId = !empty($data['onsite_allowance_id']) ? $data['onsite_allowance_id'] : null;

        $month_id = Month::getMonth($date);
        if ($month_id == 0) {
            $response['error'] = true;
            $response['message'] = "Month not found";
            return $response;
        }
        $data['month_id'] = $month_id;
        $data['user_id'] = $userId;
        $day = date('d', strtotime($date));
        $month = date('m', strtotime($date));
        $year = date('Y', strtotime($date));

        $isMonthLocked = BonusRequest::isMonthLocked($month, $year);
        if ($isMonthLocked && $day < 26) {
            $response['error'] = true;
            $response['message'] = "Bonus Request is locked for the month";
            return $response;
        }
        $alreadyApplied = BonusRequest::where('user_id', $userId)->where('type', $type)->whereDate('date', date('Y-m-d', strtotime($date)))->where('status', '!=', 'cancelled')->first();
        if ($alreadyApplied) {
            $response['error'] = true;
            $response['message'] = "Bonus Request already applied for the given date";
            return $response;
        }

        $subType = "holiday";
        $redeem_info = [];

        // $bonusRequest = new BonusRequest();
        // $bonusRequest->user_id = $userId;
        // $bonusRequest->month_id = $month_id;
        // $bonusRequest->type = $type;
        // $bonusRequest->status = 'pending';
        // $bonusRequest->date = !empty(date_to_yyyymmdd($date)) ? date_to_yyyymmdd($date) : null;
        if (!in_array($type, ['additional', 'refferal', 'techtalk', 'extra', 'onsite'])) {
            $response['error'] = true;
            $response['message'] = "Invalid bonus type : " . $type;
            return $response;
        }
        if ($type == 'onsite') {
            $onsiteAllowanceObj = OnsiteAllowance::find($onsiteAllowanceId);
            if ($onsiteAllowanceObj) {
                $data['sub_type'] = $onsiteAllowanceObj->type;
                //$bonusRequest->sub_type = $onsiteAllowanceObj->type;
                $redeem_data = [];
                $redeem_data['onsite_allowance_id'] = $onsiteAllowanceId ?: null;
                $data['projects'][] = $onsiteAllowanceObj->project_id;
                $data['redeem_type'] = $redeem_data;
                //$add = self::addProjectsToBonus($userId, $bonusRequest->id, $projects);
                //$bonusRequest->save();
            }
        } else if ($type == 'additional') {
            if ($isHoliday == 'true') {
                $subType = 'holiday';
            } else if ($isWeekend == 'true') {
                $subType = 'weekend';
            } else if ($onLeave == 'true') {
                $subType = 'leave';
            }

            $redeem_info['redeem_type'] = $redeem ?: null;
            $redeem_info['duration'] = $duration ?: null;
            $data['redeem_type'] = $redeem_info;
        } else if ($type == 'refferal') {
            //
        } else if ($type == 'techtalk') {
            //
        }

        $savedObj = BonusRequest::saveData($data);

        if (!$savedObj) {
            return ['success' => false, 'error' => true, 'result' => null, 'message' => "Unable to add request for  bonus."];
        }
        $bonusRequestObj = BonusRequest::with('bonusRequestProjects.project')->find($savedObj->id);
        return ['success' => true, 'error' => false, 'result' => $bonusRequestObj];

    }
    public static function applyForBonus($userId, $date, $type, $is_holiday, $is_weekend, $on_leave, $onsite_allowance_id, $project_id, $duration, $redeem)
    {
        $month_id = Month::getMonth($date);
        if ($month_id == 0) {
            return ['success' => false, 'error' => true, 'result' => null, 'message' => "Month not present"];
        }
        $day = date('d', strtotime($date));
        $month = date('m', strtotime($date));
        $year = date('Y', strtotime($date));
        $isMonthLocked = BonusRequest::isMonthLocked($month, $year);

        if ($isMonthLocked && $day < 26) {
            return ['success' => false, 'error' => true, 'result' => null, 'message' => "Month is locked"];
        }
        $alreadyApplied = BonusRequest::where('user_id', $userId)->where('type', $type)->whereDate('date', date('Y-m-d', strtotime($date)))->where('status', '!=', 'cancelled')->first();
        if (!$alreadyApplied) {
            $bonusRequest = new BonusRequest();
            $bonusRequest->user_id = $userId;
            $bonusRequest->month_id = $month_id;
            $bonusRequest->type = $type;
            $bonusRequest->status = 'pending';
            $bonusRequest->date = !empty(date_to_yyyymmdd($date)) ? date_to_yyyymmdd($date) : null;
            if ($type != 'onsite') {
                if ($is_holiday == 'true') {
                    $bonusRequest->sub_type = 'holiday';
                } else if ($is_weekend == 'true') {
                    $bonusRequest->sub_type = 'weekend';
                } else if ($on_leave == 'true') {
                    $bonusRequest->sub_type = 'leave';
                }
                $redeem_info = [];
                $redeem_info['redeem_type'] = $redeem ?: null;
                $redeem_info['duration'] = $duration ?: null;
                $bonusRequest->redeem_type = json_encode($redeem_info);
            }
            if (!$bonusRequest->save()) {
                return ['success' => false, 'error' => true, 'result' => null, 'message' => "Unable to add request for  bonus."];
            }
            event(new BonusRequestRaised($bonusRequest->id));
            $projects = [];
            if ($bonusRequest->type == 'onsite') {
                $onsiteAllowanceObj = OnsiteAllowance::find($onsite_allowance_id);
                if ($onsiteAllowanceObj) {
                    $bonusRequest->sub_type = $onsiteAllowanceObj->type;
                    $redeem_data = [];
                    $redeem_data['onsite_allowance_id'] = $onsite_allowance_id ?: null;
                    $bonusRequest->redeem_type = json_encode($redeem_data);
                    $projects[] = $onsiteAllowanceObj->project_id;
                    $add = self::addProjectsToBonus($userId, $bonusRequest->id, $projects);
                    $bonusRequest->save();
                }
            } else {
                $add = self::addProjectsToBonus($userId, $bonusRequest->id, $project_id);
            }
            // self::addProjectsToBonusFromTimesheet($bonusRequest->id);
            $bonusRequestObj = BonusRequest::with('bonusRequestProjects')->where('id', $bonusRequest->id)->first();
            return ['success' => true, 'error' => false, 'result' => $bonusRequestObj];
        } else {
            return ['success' => false, 'error' => true, 'result' => null, 'message' => "Data already exists."];
        }
    }

    public static function deleteBonusRequest($id)
    {
        $bonusRequestObj = BonusRequest::find($id);
        if ($bonusRequestObj && $bonusRequestObj->status != 'approved') {
            if ($bonusRequestObj->delete()) {
                return true;
            } else {
                return false;
            }

        } else {
            return false;
        }
    }

    public static function addProjectsToBonus($user_id, $bonus_request_id, $projects)
    {
        \Log::info(json_encode($user_id . $bonus_request_id));
        \Log::info(count($projects));
        $bonusRequestObj = BonusRequest::find($bonus_request_id);
        if (!$bonusRequestObj) {
            return false;
        }

        if ($bonusRequestObj->user_id != $user_id) {
            return false;
        }

        BonusRequestProject::where('bonus_request_id', $bonus_request_id)->delete();
        if (count($projects) > 0) {
            foreach ($projects as $project_id) {
                $requestProject = new BonusRequestProject();
                $requestProject->bonus_request_id = $bonus_request_id;
                $requestProject->project_id = $project_id;
                $requestProject->save();
            }
        }
        return true;
    }

    public static function sendPendingReminder($monthId)
    {
        $template_path = 'messaging-app.slack.bonus.pending-reminder';
        $pending = BonusRequest::where('month_id', $monthId)->where('status', 'pending')->count();
        if (count($pending) == 0) {
            return false;
        }
        $total = BonusRequest::where('month_id', $monthId)->count();
        $attachment = [];
        $attachment['title'] = "Please process the Pending Bonus Request";
        $attachment['fallback'] = "Please process the Pending Bonus Request";
        $attachment['title_link'] = "admin/bonus-request";
        $attachment['text'] = "";
        $fields = [
            [
                "title" => "Pending",
                "value" => $pending,
                "short" => true,
            ],
            [
                "title" => "Total",
                "value" => $total,
                "short" => true,
            ],
        ];
        $attachment['fields'] = $fields;
        self::sendNotificationOnChannel($template_path, $attachment);
    }
    public static function getTemplate($bonusRequestId, $status, $comments)
    {

        $attachment = [];
        $bonusRequestObj = BonusRequest::find($bonusRequestId);

        if (!$bonusRequestObj) {
            return false;
        }

        $now = date("Y-m-d", strtotime("last day of -1 month"));
        $monthId = Month::getMonth($now);
        $attachment['title'] = "Bonus Request Created";
        $attachment['title_link'] = "admin/mis-review-report/" . $monthId . "/" . $bonusRequestId;
        $attachment['ts'] = strtotime($bonusRequestObj->created_at);
        $attachment['fallback'] = "Bonus Request #" . $bonusRequestId;
        $attachment['pretext'] = $bonusRequestObj->amount;
        if ($status == "pending") {
            $attachment['color'] = "warning";
        } elseif ($status == "rejected") {
            $attachment['color'] = "danger";
        } elseif ($status == "approved") {
            $attachment['color'] = "good";
        } else {
            $attachment['color'] = "#FF0000";
        }

        $attachment['text'] = $bonusRequestObj->description;
        $assignedTo = $bonusRequestObj->user->name;
        $fields = [
            [
                "title" => "Type",
                "value" => ucwords($bonusRequestObj->type),
                "short" => true,
            ],
            [
                "title" => "Date",
                "value" => date_in_view($bonusRequestObj->date),
                "short" => true,
            ],
        ];
        $attachment['fields'] = $fields;

        return $attachment;
    }
    public static function sendNotificationOnChannel($bonusRequestId, $message, $template_path, $attachments = [])
    {
        $channelId = \Config::get('messaging-app.bonus_channel');
        $bonusRequestObj = BonusRequest::find($bonusRequestId);

        if (!$bonusRequestObj) {
            return false;
        }
        if (empty($attachments)) {
            $attachments = self::getTemplate($bonusRequestId, $bonusRequestObj->status, $bonusRequestObj->status, '');
        }
        $job = (new PostMessageChannelJob($channelId, $message, $attachments));
        dispatch($job);
    }
    public static function sendNotificationUser($bonusRequestId, $user_id, $message, $template_path, $attachments = [])
    {

        $bonusRequestObj = BonusRequest::find($bonusRequestId);
        if (!$bonusRequestObj) {
            return false;
        }

        if (empty($attachments)) {
            $attachments = self::getTemplate($bonusRequestId, $bonusRequestObj->status, $bonusRequestObj->status, '');
        }

        $job = (new PostMessageUserJob($user_id, $message, $attachments));
        dispatch($job);

    }
    public static function addProjectsToBonusFromTimesheet($bonus_request_id)
    {
        $bonusRequestObj = BonusRequest::find($bonus_request_id);
        if ($bonusRequestObj) {
            $user_id = $bonusRequestObj->user_id;
            $date = $bonusRequestObj->date;
            $projectIds = UserTimesheet::where('user_id', $user_id)->whereDate('date', $date)->where('status', '!=', 'rejected')->distinct()->select('project_id')->pluck('project_id')->toArray();
            if ($projectIds && count($projectIds) > 0) {
                $status = self::addProjectsToBonus($user_id, $bonus_request_id, $projectIds);
            }
        }
    }

    public static function bonusRequestApproved($bonusRequestId)
    {
        $obj = BonusRequest::find($bonusRequestId);
        $user = User::find($obj->approver_id);

        if ($obj) {
            $templatePath = 'messaging-app.slack.bonus-request.created';
            $obj->type == "additional" ? $obj->type = "additional work day" : $obj->type; //?//
            $channelMessage = "Bonus request #" . $bonusRequestId . " is *approved* by *" . $user->name . "*.";
            $userMessage = "Your *" . ucwords($obj->type) . "* bonus request for " . date_in_view($obj->date) . " is approved by " . $user->name . ".";
            self::sendNotificationOnChannel($bonusRequestId, $channelMessage, $templatePath);
            self::sendNotificationUser($bonusRequestId, $obj->user_id, $userMessage, $templatePath);
        }

    }

    public static function bonusRequestRaised($bonusRequestId)
    {
        \Log::info('Service bonusRequestRaised');
        $obj = BonusRequest::find($bonusRequestId);
        $user = User::find($obj->user_id);

        if ($obj) {
            $templatePath = 'messaging-app.slack.bonus-request.created';
            $obj->type == "additional" ? $obj->type = "additional work day" : $obj->type; //?//
            $channelMessage = "A New bonus request #" . $bonusRequestId . " is created by *" . $user->name . "*.";
            $userMessage = "Your *" . ucwords($obj->type) . "* bonus request for " . date_in_view($obj->date) . "  is created successfully.";
            self::sendNotificationOnChannel($bonusRequestId, $channelMessage, $templatePath);
            self::sendNotificationUser($bonusRequestId, $obj->user_id, $userMessage, $templatePath);
        }

    }

    public static function bonusRequestRejected($bonusRequestId)
    {
        $obj = BonusRequest::find($bonusRequestId);
        $user = User::find($obj->approver_id);

        if ($obj) {
            $templatePath = 'messaging-app.slack.bonus-request.created';
            $channelMessage = "Bonus request #" . $bonusRequestId . " is *approved* by *" . $user->name . "*.";
            $obj->type == "additional" ? $obj->type = "additional work day" : $obj->type; //?//
            $userMessage = "Your *" . ucwords($obj->type) . "* bonus request for " . date_in_view($obj->date) . " is rejected by " . $user->name . ".";
            self::sendNotificationOnChannel($bonusRequestId, $channelMessage, $templatePath);
            self::sendNotificationUser($bonusRequestId, $obj->user_id, $userMessage, $templatePath);
        }

    }

    public static function onsiteAllowanceEligibility($user_id, $date)
    {
        $status = 'false';
        $allowanceObj = [];
        $onsiteAllowanceList = OnsiteAllowance::with('project')->where('user_id', $user_id)->whereDate('start_date', '<=', $date)->whereDate('end_date', '>=', $date)
            ->where('status', 'approved')->get();
        if (count($onsiteAllowanceList) > 0) {
            $status = 'true';
            $allowanceObj = $onsiteAllowanceList->toArray();
        }
        $result['status'] = $status;
        $result['allowanceObj'] = $allowanceObj;
        return $result;
    }

    public static function pendingBonusReminder($total)
    {
        $job = (new PostMessageChannelJob($channelId, $message, $attachments));
        dispatch($job);

    }

    public static function approveAdditionalBonusRequest($input)
    {
        $userObj = \Auth::user();
        $saveStatus = false;
        $bonusRequestId = $input['id'];

        $bonusRequestObj = BonusRequest::find($bonusRequestId);
        if ($bonusRequestObj) {
            $amount = self::getBonusAmount($bonusRequestId);

        }
        $bonusRequestObj->status = "approved";
        $bonusRequestObj->notes = !empty($input['notes']) ? $input['notes'] : null;
        $bonusRequestObj->approver_id = $userObj->id;
        // $amount = self::getBonusAmount(); // getOneDatGrossSalaryByDate
        if ($bonusRequestObj->save()) {
            $saveStatus = self::saveToBonus($bonusRequestObj->id, $amount);
        }
        return $saveStatus;
    }
    public static function getBonusAmount($bonusRequestId)
    {
        $bonusRequest = BonusRequest::find($bonusRequestId);
        $amount = 0;
        if (!$bonusRequest) {
            return $amount;
        }

        if ($bonusRequest->type == 'additional') {
            $amount = Appraisal::getBonusAmount($bonusRequest->user_id, $bonusRequest->date);
        } elseif ($bonusRequest->type == 'onsite') {
            $redeemType = json_decode($bonusRequest->redeem_type, true);

            if (isset($redeemType['onsite_allowance_id'])) {
                $onsiteAllowanceId = $redeemType['onsite_allowance_id'];
                $onSiteAllowanceObj = OnsiteAllowance::find($onsiteAllowanceId);
                if ($onSiteAllowanceObj) {
                    \Log::info('TEST 2' . json_encode($redeemType));
                    $amount = $onSiteAllowanceObj->amount ? $onSiteAllowanceObj->amount : 0;
                }
            }

        } elseif ($bonusRequest->type == 'extra') {
            $redeemType = json_decode($bonusRequest->redeem_type, true);
            if (!empty($redeemType['hours'])) {
                $amount = round(($redeemType['hours'] / 8) * $bonusRequest->user->one_day_salary);
            }
        } elseif ($bonusRequest->type == 'performance') {
            $redeemType = json_decode($bonusRequest->redeem_type, true);
            if (!empty($redeemType['amount'])) {
                $amount = $redeemType['amount'];
            }

        } else {
            $redeemType = json_decode($bonusRequest->redeem_type, true);
            if (!empty($redeemType['amount'])) {
                $amount = $redeemType['amount'];
            }
        }
        return $amount;
    }
    public static function approveOnsiteBonusRequest($input)
    {
        $saveStatus = false;
        $userObj = \Auth::user();
        $bonusRequestObj = BonusRequest::find($input['id']);
        $onsite_allowance_id = $input['redeem_info']['onsite_allowance_id'];
        $onSiteAllowanceObj = OnsiteAllowance::find($onsite_allowance_id);
        $redeem_data = [];
        $redeem_data['onsite_allowance_id'] = $onsite_allowance_id ?: null;
        $bonusRequestObj->redeem_type = json_encode($redeem_data);
        $bonusRequestObj->sub_type = $onSiteAllowanceObj->type;
        $bonusRequestObj->status = "approved";
        $bonusRequestObj->notes = !empty($input['notes']) ? $input['notes'] : null;
        $bonusRequestObj->approver_id = $userObj->id;
        $amount = $onSiteAllowanceObj->amount ?: 0;
        if ($bonusRequestObj->save()) {
            $saveStatus = self::saveToBonus($bonusRequestObj->id, $amount);
            // $saveStatus = true;
        }
        return $saveStatus;
    }
    public static function approveBonusRequest($bonusRequestId)
    {

        $bonusRequestObj = BonusRequest::find($bonusRequestId);
        if (!$bonusRequestObj) {
            return false;
        }

        $redeemType = json_decode($bonusRequestObj->redeem_type, true);
        $bonusRequestObj->status = "approved";
        $amount = 0;
        if ($bonusRequestObj->type == 'additional' && (isset($redeemType['redeem_type']) && $redeemType['redeem_type'] == 'pl')) {
            $leaveTypeObj = LeaveType::where('code', 'paid')->first();

            if (!$leaveTypeObj) {

            }
            $data['leave_type_id'] = $leaveTypeObj->id;
            $data['days'] = 1;
            $data['reference_type'] = "App\Models\Admin\Bonus";
            $data['reference_id'] = $bonusRequestObj->id;
            $data['user_id'] = $bonusRequestObj->user_id;
            // UserLeaveLog::updateUserAllowedLeave($data);

            $PL = UserLeaveBalance::addUserLeaves($data);
        } else {
            $amount = self::getBonusAmount($bonusRequestId);
        }

        if ($bonusRequestObj->save()) {
            $saveStatus = self::saveToBonus($bonusRequestObj->id, $amount);
        }
        return $saveStatus;
    }

    public static function saveToBonus($bonus_request_id, $amount = null)
    {
        $status = false;
        $bonusRequestObj = BonusRequest::find($bonus_request_id);
        if (!$amount) {
            $amount = self::getBonusAmount($bonus_request_id);
        }

        if (!$bonusRequestObj) {
            return false;
        }

        $bonus = new Bonus;
        $bonus->month_id = $bonusRequestObj->month_id ?: 0;
        $bonus->user_id = $bonusRequestObj->user_id;
        $bonus->date = $bonusRequestObj->date;
        $bonus->type = $bonusRequestObj->type;
        $bonus->status = 'approved';
        $bonus->bonus_request_id = $bonusRequestObj->id;
        $bonus->sub_type = $bonusRequestObj->sub_type;
        $bonus->title = $bonusRequestObj->title;
        $bonus->referral_for = !empty($bonusRequestObj->referral_for) ? $bonusRequestObj->referral_for : null;
        $bonus->draft_amount = $amount;
        $bonus->amount = $amount;
        $bonus->notes = !empty($bonusRequestObj->notes) ? $bonusRequestObj->notes : null;
        $bonus->approved_by = \Auth::id();
        $bonus->created_by = \Auth::id();
        $bonus->approved_at = date('Y-m-d H:i:s');

        if (!$bonus->save()) {
            return false;
        }
        return true;

    }
    public static function getUserBonuses($userId, $monthId)
    {

        $monthObj = Month::find($monthId);

        if (!$monthObj) {
            return false;
        }

        $month = $monthObj->month;
        $year = $monthObj->year;

        $obj = new UserCalendar();
        $data = $obj->getCalendar($month, $year);
        foreach ($data['days'] as $index => $day) {
            if ($day) {
                $response = Leave::isOnLeaveToday($userId, $day['date']);
                if ($response) {
                    $data['days'][$index]['on_leave'] = 'true';
                } else {
                    $data['days'][$index]['on_leave'] = 'false';
                };
                $data['days'][$index]['bonus'] = BonusService::getUserBonusByDate($userId, $day['date']);
                $onsite = BonusService::onsiteAllowanceEligibility($userId, $day['date']);
                $data['days'][$index]['onsite_eligible'] = $onsite['status'];
                $data['days'][$index]['onsite_allowance'] = $onsite['allowanceObj'];
                $projects = UserService::getTimesheetProjects($userId, $day['date']);
                $data['days'][$index]['assigned_projects'] = $projects;
            }
        }
        return $data;
    }
    public static function getUserBonusByDate($userId, $date)
    {
        $bonusRequests = [];
        //Onsite
        $bonusRequests['onsite_bonuses'] = OnSiteBonus::where('employee_id', $userId)->where('date', $date)->get()->toArray();

        //Additional
        $bonusRequests['additional_bonuses'] = AdditionalWorkDaysBonus::where('employee_id', $userId)->where('date', $date)->get()->toArray();

        //Extra
        $bonusRequests['extra_bonuses'] = UserTimesheetExtra::where('user_id', $userId)->where('date', $date)->get()->toArray();

        //Referral
        //$extraBonuses = UserTimesheetExtra::where('user_id',$userId)->where('date',$date)->get();
        //TechTalk
        //$extraBonuses = UserTimesheetExtra::where('user_id',$userId)->where('date',$date)->get();
        return $bonusRequests;

    }
    public static function getUserBonusByMonth($userId, $start_date, $end_date)
    {
        $data = [];

        $bonus = Bonus::with('approver', 'payer')->where('user_id', $userId)->whereBetween('date', [$start_date, $end_date])->get();
        if (count($bonus) > 0) {
            foreach ($bonus as $index => $userBonus) {
                if ($userBonus->reference_type == 'App\Models\User\UserTimesheetExtra') {
                    $data['bonus'][$index]['type'] = 'extra';
                }
                if ($userBonus->reference_type == 'App\Models\Admin\AdditionalWorkDaysBonus') {
                    $data['bonus'][$index]['type'] = 'additional';
                }
                if ($userBonus->reference_type == 'App\Models\Admin\OnSiteBonus') {
                    $data['bonus'][$index]['type'] = 'onsite';
                }
                $data['bonus'][$index]['bonus'] = $userBonus;

            }
        }

        return $data;
    }

    public static function getUserBonusByMonthId($userId, $start_date, $end_date)
    {

        $data = [];
        // $monthObj = Month::find($monthId);

        // if (!$monthObj) {
        //     return $data;
        // // }

        // $startDate = $monthObj->getFirstDay();
        // $endDate = $monthObj->getLastDay();
        // $data['bonus_requests'] = BonusRequest::where('user_id', $userId)->where('month_id', $monthId)->get();
        $data['bonus_requests'] = BonusRequest::with('approver')->where('user_id', $userId)->whereBetween('date', [$start_date, $end_date])->get();

        $data['extra_hours'] = UserTimesheetExtra::whereBetween('date', [$start_date, $end_date])->where('user_id', $userId)->get();
        return $data;

    }
    public static function getDueBonues($userId)
    {
        $duesOverviews = Bonus::with('reference', 'bonusConfirm', 'salaryProcess')->where('user_id', $userId)->where('status', 'approved')->where('transaction_id', null)->orderBy('type')->get();
        // $unpaidBonuses = Bonus::with('reference')->where('user_id', $userId)->where('transaction_id', null)->orderBy('type')->get();
        // $paidBonuses = Bonus::with('reference')->where('user_id', $userId)->where('transaction_id', '!=', null)->whereMonth('paid_at', date('m'))->whereYear('paid_at', date('Y'))->orderBy('type')->get();
        // $duesOverviews = $paidBonuses->merge($unpaidBonuses);
        $requestType = "due";

        $dueBonues['onsite']['bonuses'] = [];
        $dueBonues['onsite']['days'] = 0;
        $dueBonues['onsite']['amount'] = 0;

        $dueBonues['additional']['bonuses'] = [];
        $dueBonues['additional']['days'] = 0;
        $dueBonues['additional']['amount'] = 0;

        $dueBonues['extra']['bonuses'] = [];
        $dueBonues['extra']['hours'] = 0;
        $dueBonues['extra']['amount'] = 0;

        $dueBonues['performance']['bonuses'] = [];
        $dueBonues['performance']['days'] = 0;
        $dueBonues['performance']['amount'] = 0;

        foreach ($duesOverviews as $duesOverview) {

            if ($duesOverview->reference_type == 'App\Models\Admin\OnSiteBonus') {

                $onSiteTemp = [];
                $onSiteTemp = $duesOverview;
                if ($duesOverview->reference && $duesOverview->reference->onsite_allowance_id) {$onSiteObj = OnSiteAllowance::with('project', 'approver')->find($duesOverview->reference->onsite_allowance_id);
                    $onSiteTemp['rate'] = $onSiteObj->amount;
                    $onSiteTemp['project'] = $onSiteObj->project;
                    $onSiteTemp['approver'] = $onSiteObj->approver->name;
                    $onSiteTemp['description'] = "From " . date('d M,Y', strtotime($onSiteObj->start_date)) . ' to ' . date('d M,Y', strtotime($onSiteObj->end_date));
                    if (NonworkingCalendar::isWeekend($duesOverview->date)) {
                        $onSiteTemp['weekend'] = true;
                    } elseif (NonworkingCalendar::is_holiday($duesOverview->date)) {
                        $onSiteTemp['holiday'] = true;
                    } else {
                        $extraTemp['on_leave'] = Leave::isOnLeaveToday($duesOverview->user_id, $duesOverview->date);
                    }
                    $dueBonues['onsite']['amount'] += $duesOverview->amount;
                    $dueBonues['onsite']['draft_amount'] = $onSiteObj->amount;
                }
                $dueBonues['onsite']['days']++;
                $dueBonues['onsite']['bonuses'][] = $onSiteTemp;
            } elseif ($duesOverview->reference_type == 'App\Models\Admin\AdditionalWorkDaysBonus') {
                $additionalTemp = [];
                $additionalTemp = $duesOverview;
                $additionalAppraisal = Appraisal::getBonusAmount($duesOverview->user_id, $duesOverview->date);
                $additionalTemp['rate'] = $additionalAppraisal;

                $additionalTemp['approver'] = $duesOverview->reference->approved_by;
                $appraisalObj = Appraisal::getAppraisalByDate($duesOverview->user_id, $duesOverview->date);
                $additionalTemp['annual_gross_salary'] = null;
                if ($appraisalObj) {

                    $additionalTemp['annual_gross_salary'] = $appraisalObj->getAnnualGrossSalary();
                }

                $reference = $duesOverview->reference;
                if ($reference && $reference->redeem_type == 'encash') {
                    $additionalTemp['description'] = 'Encash | Hours : ' . $reference->duration;
                    $dueBonues['additional']['amount'] += $duesOverview->amount;
                } elseif ($reference && $reference->redeem_type == 'comp-off') {
                    $additionalTemp['description'] = 'Paid Leave';
                }
                $additionalTemp['weekend'] = false;
                $additionalTemp['holiday'] = false;
                $additionalTemp['on_leave'] = false;
                if (NonworkingCalendar::isWeekend($duesOverview->date)) {
                    $additionalTemp['weekend'] = true;
                } elseif (NonworkingCalendar::is_holiday($duesOverview->date)) {
                    $additionalTemp['holiday'] = true;
                } else {
                    $extraTemp['on_leave'] = Leave::isOnLeaveToday($duesOverview->user_id, $duesOverview->date);
                }
                // }

                $dueBonues['additional']['draft_amount'] = $reference->amount;

                $dueBonues['additional']['days']++;
                $dueBonues['additional']['bonuses'][] = $additionalTemp;
            } elseif ($duesOverview->reference_type == 'App\Models\User\UserTimesheetExtra') {
                $extraTemp = [];
                $extraTemp = $duesOverview;
                $extraRate = Appraisal::getBonusAmount($duesOverview->user_id, $duesOverview->date);
                $extraTemp['rate'] = round($extraRate / 8);
                // $extraTemp['approver'] = $duesOverview->referance->approved_by;
                $appraisalObj = Appraisal::getAppraisalByDate($duesOverview->user_id, $duesOverview->date);
                $extraTemp['annual_gross_salary'] = null;
                if ($appraisalObj) {

                    $extraTemp['annual_gross_salary'] = $appraisalObj->getAnnualGrossSalary();
                }
                if (NonworkingCalendar::isWeekend($duesOverview->date)) {
                    $extraTemp['weekend'] = true;
                } elseif (NonworkingCalendar::is_holiday($duesOverview->date)) {
                    $extraTemp['holiday'] = true;
                } else {
                    $extraTemp['on_leave'] = Leave::isOnLeaveToday($duesOverview->user_id, $duesOverview->date);
                }
                $reference = $duesOverview->reference;

                if ($reference) {
                    if ($reference->redeem_type == 'encash') {
                        $additionalTemp['description'] = 'Encash | Hours : ' . $reference->duration;
                        $dueBonues['additional']['amount'] += $duesOverview->amount;
                    } elseif ($reference->redeem_type == 'comp-off') {
                        $additionalTemp['description'] = 'Paid Leave';
                    }

                    $approver = User::find($reference->approved_by);
                    if ($approver) {
                        $dueBonues['extra']['amount'] += $duesOverview->amount;

                        $dueBonues['extra']['approver'] = $approver->name;
                    }
                    $extraTemp['redeem_type'] = $reference->extra_hours;
                    $extraTemp['description'] = 'Extra Hours ' . $reference->extra_hours;
                    $dueBonues['extra']['draft_amount'] = $reference->amount;

                    if ($reference->extra_hours) {
                        $dueBonues['extra']['hours'] += $reference->extra_hours;

                    }
                }

                $dueBonues['extra']['amount'] += $duesOverview->amount;

                $dueBonues['extra']['bonuses'][] = $extraTemp;
            } elseif ($duesOverview->type == 'performance') {
                $performanceTemp = [];
                $performanceTemp = $duesOverview;
                //$extraRate = Appraisal::getOneDaySalary($duesOverview->user_id, $duesOverview->date);
                $performanceTemp['rate'] = 'NA';
                $performanceTemp['approver'] = $duesOverview->approver->name;
                $appraisalObj = Appraisal::getAppraisalByDate($duesOverview->user_id, $duesOverview->date);
                $performanceTemp['annual_gross_salary'] = null;
                if ($appraisalObj) {
                    $performanceTemp['annual_gross_salary'] = $appraisalObj->getAnnualGrossSalary();
                }
                if (NonworkingCalendar::isWeekend($duesOverview->date)) {
                    $performanceTemp['weekend'] = true;
                } elseif (NonworkingCalendar::is_holiday($duesOverview->date)) {
                    $performanceTemp['holiday'] = true;
                } else {
                    $performanceTemp['on_leave'] = Leave::isOnLeaveToday($duesOverview->user_id, $duesOverview->date);
                }
                if ($duesOverview->bonusRequest != null && !empty($duesOverview->bonusRequest)) {
                    $redeem_type = json_decode($duesOverview->bonusRequest->redeem_type);

                    if ($redeem_type != null && isset($redeem_type->amount)) {
                        //$performanceTemp['redeem_type'] = $redeem_type->hours;
                        $performanceTemp['description'] = 'Performance Bonus' . $redeem_type->amount;
                        $dueBonues['performance']['amount'] += $redeem_type->amount;
                    }
                }
                $dueBonues['performance']['days']++;
                $dueBonues['performance']['bonuses'][] = $performanceTemp;

            }

        }

        return $dueBonues;
    }

    public static function approveForPayment($bonusId)
    {

        $obj = Bonus::with('month')->get()->find($bonusId);
        if (!$obj) {
            return false;
        }
        $data['month_id'] = $obj->month_id;
        $data['financial_year_id'] = $obj->month ? $obj->month->financial_year_id : 0;
        $data['user_id'] = $obj->user_id;
        $data['reference_id'] = $obj->id;
        $data['reference_type'] = 'App\Models\Admin\Bonus';
        $data['amount'] = $obj->amount;
        $data['type'] = 'credit';
        $transactionObj = Transaction::saveTransaction($data);

        if (!$transactionObj['status'] || $transactionObj['id'] == null) {
            return false;
        }
        return $transactionObj;
    }
    public static function confirmBonus($bonusId)
    {
        $obj = Bonus::with('month')->get()->find($bonusId);
        if (!$obj) {
            return false;
        }
        $confirmBonusObj = new BonusConfirmed();
        $confirmBonusObj->bonus_id = $bonusId;
        $confirmBonusObj->user_id = $obj->user_id;
        if (!$confirmBonusObj->save()) {
            return false;
        }
        return $confirmBonusObj;

    }
    public static function getMonthlyBonusTransactions($userId, $monthId)
    {
        $userObj = User::find($userId);
        if (!$userObj) {
            return Redirect::back()->withErrors('Invalid User Id');
        }
        $monthObj = Month::find($monthId);
        if (!$monthObj) {
            return Redirect::back()->withErrors('Invalid Month Id');
        }
        $transactions = Transaction::where('user_id', $userId)->where('month_id', $monthObj->id)->where('reference_type', "App\Models\Admin\Bonus")->get();
        $data = [];
        $total["onsite"]["amount"] = 0;
        $total["additional"]["amount"] = 0;
        $total["extra"]["amount"] = 0;
        $total["onsite"]["days"] = 0;
        $total["additional"]["days"] = 0;
        $total["extra"]["hours"] = 0;
        foreach ($transactions as $index => $transaction) {
            $bonusObj = Bonus::with('approver', 'payer')->find($transaction->reference_id);
            if ($bonusObj) {
                $bonusRequest = BonusRequest::find($bonusObj->bonus_request_id);
                $data[$bonusObj->type][$index]["transaction"] = $transaction;
                $data[$bonusObj->type][$index]["bonus"] = $bonusObj;
                if ($bonusRequest) {
                    $data[$bonusObj->type][$index]["bonusRequest"] = $bonusRequest;
                }
                $total[$bonusObj->type]["amount"] += $transaction->amount;
                if ($bonusObj->type == "extra") {
                    if ($bonusRequest && $bonusRequest->redeem_type) {
                        $redeem_type = json_decode($bonusRequest->redeem_type);
                        if ($redeem_type && $redeem_type->hours) {
                            $total[$bonusObj->type]["hours"] += $redeem_type->hours;
                        }
                    }
                } else {
                    $total[$bonusObj->type]["days"]++;
                }
            }
        }
        $returnData["data"] = $data;
        $returnData["total"] = $total;
        return $returnData;
    }
    public static function approveBonus($data)
    {
        if (!$data) {
            return false;
        }
        $userData = [];
        $type = !empty($data['type']) ? $data['type'] : null;
        $userId = !empty($data['user_id']) ? $data['user_id'] : null;
        if ($type == 'onsite') {
            // Bonus Requets
            $userData['month_id'] = Month::getMonth($data['date']);
            $userData['user_id'] = !empty($data['user_id']) ? $data['user_id'] : null;
            $userData['date'] = !empty($data['date']) ? $data['date'] : null;
            $userData['type'] = 'onsite'; //!empty($data['type']) ? $data['type'] : null;
            $userData['status'] = 'approved';
            //$userData['status'] = 'onsite';
            $userData['redeem_type'] = !empty($data['redeem_type']) ? $data['redeem_type'] : null;
            $userData['projects'] = !empty($data['projects']) ? json_encode($data['projects']) : [];
            $response = BonusRequest::saveData($userData);
            self::approveBonusRequest($response->id);
            // $userData['reference_id'] = !empty($data['id']) ? $data['id'] : null;
            // $userData['reference_type'] = 'App\Models\Admin\OnSiteBonus';
            //$response = Bonus::saveApprovedBonus($userData);
            //return $response;

        }
        if ($type == 'extra') {
            // Bonus Requets
            $userData['month_id'] = Month::getMonth($data['date']);
            $userData['user_id'] = !empty($data['user_id']) ? $data['user_id'] : null;
            $userData['date'] = !empty($data['date']) ? $data['date'] : null;
            $userData['type'] = 'extra'; //!empty($data['type']) ? $data['type'] : null;
            $userData['status'] = 'approved';
            //$userData['status'] = 'onsite';
            $userData['redeem_type'] = !empty($data['redeem_type']) ? $data['redeem_type'] : null;
            $userData['projects'] = !empty($data['projects']) ? json_encode($data['projects']) : [];
            $response = BonusRequest::saveData($userData);
            self::approveBonusRequest($response->id);
            // $userData['reference_id'] = !empty($data['id']) ? $data['id'] : null;
            // $userData['reference_type'] = 'App\Models\Admin\OnSiteBonus';
            //$response = Bonus::saveApprovedBonus($userData);
            //return $response;

        }
        if ($type == 'additional') {
            $userData['month_id'] = Month::getMonth($data['date']);
            $userData['user_id'] = !empty($data['user_id']) ? $data['user_id'] : null;
            $userData['amount'] = !empty($data['amount']) ? $data['amount'] : null;
            $userData['date'] = !empty($data['date']) ? $data['date'] : null;
            $userData['type'] = 'additional'; //!empty($data['type']) ? $data['type'] : null;
            $userData['status'] = 'approved';
            $userData['redeem_type'] = !empty($data['redeem_type']) ? $data['redeem_type'] : null;
            // $userData['reference_id'] = !empty($data['id']) ? $data['id'] : null;
            // $userData['reference_type'] = 'App\Models\Admin\AdditionalWorkDaysBonus';
            //$date = !empty($data['date']) ? $data['date'] : null;
            $date = $userData['date'];
            if (NonworkingCalendar::isWeekend($date)) {
                $userData['subType'] = 'weekend';

            }
            if (NonworkingCalendar::is_holiday($date)) {
                $userData['subType'] = 'holiday';

            }
            $response = BonusRequest::saveData($userData);
            //$response = Bonus::saveApprovedBonus($userData);
            self::approveBonusRequest($response->id);
            //return $response;

        }
        if ($type == 'performance') {
            $userData['month_id'] = Month::getMonth($data['date']);
            $userData['user_id'] = !empty($data['user_id']) ? $data['user_id'] : null;
            $userData['amount'] = !empty($data['amount']) ? $data['amount'] : null;
            $userData['date'] = !empty($data['date']) ? $data['date'] : null;
            $userData['type'] = 'performance';
            $userData['redeem_type'] = !empty($data['redeem_type']) ? $data['redeem_type'] : null;
            $response = BonusRequest::saveData($userData);
            // $userData['reference_id'] = !empty($data['id']) ? $data['id'] : null;
            // $userData['reference_type'] = 'App\Models\Admin\OnSiteBonus';
            self::approveBonusRequest($response->id);
            //$response = Bonus::saveApprovedBonus($userData);
            //return $response;

        }

        $userData = self::getDueBonues($userId);
        return $userData;

    }
    public static function addNewBonus($input)
    // add bonus of all types from modal, restricted only for admin //
    {
        $response['status'] = false;
        $response['message'] = '';

        $user = Auth::user();
        $type = $input['type'];
        if (!$type) {
            $response['message'] = 'missing bonus type';
            return $response;
        }
        DB::beginTransaction();
        try
        {
            $obj = new Bonus();
            if ($type == "additional") {
                $ifExists = AdditionalWorkDaysBonus::where('user_id', $input['user_id'])->where('date', $input['date'])->where('status', "approved")->first();
                if ($ifExists) {
                    throw new Exception('user already has a bonus on this day');
                }
                $bonusObj = new AdditionalWorkDaysBonus();
                $onLeave = Leave::isOnLeaveToday($input['user_id'], $input['date']);
                $is_weekend = NonworkingCalendar::isWeekend($input['date']);
                $is_holiday = NonworkingCalendar::is_holiday($input['date']);
                if ($is_weekend) {
                    $bonusObj->type = 'weekend';
                } elseif ($is_holiday) {
                    $bonusObj->type = 'holiday';
                } elseif ($onLeave) {
                    $bonusObj->type = 'leave';
                } else {
                    throw new Exception('User is not on Leave and/or Applied Date is not a Weekend or a Holiday');
                }
                $bonusObj->user_id = $input['user_id'] ? $input['user_id'] : '';
                $bonusObj->month_id = Month::getMonth($input['date']);
                $bonusObj->project_id = $input['additional_project'] ? $input['additional_project'] : '';
                $bonusObj->date = $input['date'] ? $input['date'] : '';
                $bonusObj->status = 'approved';
                $bonusObj->notes = 'added by admin';
                $bonusObj->duration = $input['hours'] ? $input['hours'] : '';
                $bonusObj->redeem_type = "encash";
                $bonusObj->created_by = $user->id;
                $bonusObj->reviewed_by = $user->id;
                $bonusObj->created_at = date('Y-m-d H:i:s');
                $bonusObj->reviewed_at = date('Y-m-d H:i:s');
                $amount = Appraisal::getBonusAmount($input['user_id'], $input['date']);
                $bonusObj->amount = round($amount / 8 * $input['hours']);
                if (!$bonusObj->save()) {
                    throw new Exception('error while saving additional bonus');
                }
                $additonalProjectObj = new AdditionalWorkDayProject;
                $additonalProjectObj->additional_workday_id = $bonusObj->id;
                $additonalProjectObj->project_id = $bonusObj->project_id;
                if (!$additonalProjectObj->save()) {
                    throw new Exception('error while saving additional project bonus');
                }
                $obj->reference_type = "App\Models\Admin\AdditionalWorkDaysBonus";
                $obj->amount = $bonusObj->amount;
            } elseif ($type == "onsite") {
                $ifExists = OnSiteBonus::where('user_id', $input['user_id'])->where('date', $input['date'])->where('status', "approved")->first();
                if ($ifExists) {
                    throw new Exception('user already has a bonus on this day');
                }
                $bonusObj = new OnSiteBonus();
                $bonusObj->user_id = $input['user_id'] ? $input['user_id'] : '';
                $bonusObj->onsite_allowance_id = $input['onsite_allowance'] ? $input['onsite_allowance'] : '';
                $bonusObj->date = $input['date'] ? $input['date'] : '';
                $bonusObj->status = 'approved';
                $bonusObj->notes = 'added by admin';
                $bonusObj->created_by = $user->id;
                $bonusObj->created_at = date('Y-m-d H:i:s');
                $bonusObj->reviewed_by = $user->id;
                $bonusObj->reviewed_at = date('Y-m-d H:i:s');
                $onsiteAllowanceObj = OnsiteAllowance::find($input['onsite_allowance']);
                // $bonusObj->amount = round($amount / 8 * $input['hours']);??
                if (!$bonusObj->save()) {
                    throw new Exception('error while saving onsite bonus');
                }
                $obj->reference_type = "App\Models\Admin\OnSiteBonus";
                if (!$onsiteAllowanceObj) {
                    throw new Exception('error finding onsite allowance details');
                }
                $obj->amount = $onsiteAllowanceObj->amount;
            } elseif ($type == "extra") {
                $ifExists = UserTimesheetExtra::where('user_id', $input['user_id'])->where('date', $input['date'])->where('status', "approved")->first();
                if ($ifExists) {
                    throw new Exception('user already has a bonus on this day');
                }
                $bonusObj = new UserTimesheetExtra();
                $bonusObj->user_id = $input['user_id'] ? $input['user_id'] : '';
                $bonusObj->month_id = Month::getMonth($input['date']);
                $weekObj = Week::where('start_date', '<=', $input['date'])->where('end_date', '>=', $input['date'])->first();
                $bonusObj->week_id = $weekObj ? $weekObj->id : 0;
                $bonusObj->project_id = $input['extra_project'] ? $input['extra_project'] : '';
                $amount = Appraisal::getBonusAmount($input['user_id'], $input['date']);
                $bonusObj->amount = round($amount / 8 * $input['hours']);
                $bonusObj->date = $input['date'] ? $input['date'] : '';
                $bonusObj->extra_hours = $input['hours'] ? $input['hours'] : '';
                $bonusObj->created_by = $user->id;
                $bonusObj->approved_by = $user->id;
                $bonusObj->status = 'approved';
                $bonusObj->created_at = date('Y-m-d H:i:s');
                if (!$bonusObj->save()) {
                    throw new Exception('error while saving extra bonus');
                }
                $obj->reference_type = "App\Models\User\UserTimesheetExtra";
                $obj->amount = $bonusObj->amount;
            } elseif ($type == "performance") {
                $obj->referral_for = $input['referral_for'];
            } else {
                throw new Exception('invalid bonus type');

            }
            //add to bonus
            $obj->month_id = Month::getMonth($input['date']);
            $obj->user_id = $input['user_id'] ? $input['user_id'] : '';
            $obj->date = $input['date'] ? $input['date'] : '';
            $obj->status = "approved";
            $obj->approved_by = $user->id;
            $obj->type = $type;
            $obj->reference_id = $bonusObj->id;
            $obj->created_by = $user->id;
            $obj->approved_at = date('Y-m-d H:i:s');
            $obj->draft_amount = $obj->amount;
            if (!$obj->save()) {
                throw new Exception('error while saving bonus');
            }
            DB::commit();
            $response['message'] = "successfully added bonus";
            $response['status'] = true;
        } catch (Exception $e) {
            DB::rollback();
            $response['message'] = $e->getMessage();
            return $response;
        }
        return $response;
    }
}
