<?php 
namespace App\Services;

use Log;
use Mail;
use Exception;
use Config;

use App\Models\AccessLog;
use App\Models\User;
use App\Models\Admin\PayslipData;
use App\Models\Admin\PayslipMonth;
use App\Services\Files\CSVFileService;

class PayslipService 
{
    // protected $user;

    public function __construct()
    {
        // $this->user = $user;
    }

    public static function processPayslipData($id) {

        $obj =  
        ::find($id);
        
        if(!$obj && $obj->status != 'in_progress')
            return;

        //if($obj) {

        if (!file_exists($obj->file_path) || !is_readable($obj->file_path)) {

            $obj->comment = 'File does not exists or not readable';
            $obj->status = 'rejected';
            $obj->save();

            AccessLog::accessLog(NULL, 'App\Services', 'PayslipService', 'sendUserVerificationMail', 'catch-block', 'File does not exists or not readable .');
            return;
        }
        
        $parsedData = CSVFileService::csvParser($obj->file_path);
        
        if(!empty($parsedData['data']) && count($parsedData['data']) > 0 ) {
            $obj->json_data = json_encode($parsedData['data']);
            $workingDayMonth = isset($parsedData[0]) ? $parsedData[0]['working_day_month'] : 0;
            $obj->working_day_month = $workingDayMonth;
            $obj->status = 'pending';
            $obj->save();
        } else {
            // $obj->status = 'rejected';
            $obj->comment = 'No data found in the file';
            $obj->save();
        }
        
        PayslipMonth::matchCSVData($obj->id);
    }
    public function savePayslipData($payslipMonth)
    {
        try {

            if ( empty($payslipMonth) || empty($payslipMonth->file_path) ) {
                $payslipMonth->status = 'rejected';
                $payslipMonth->save();
                AccessLog::accessLog(NULL, 'App\Services', 'PayslipService', 'sendUserVerificationMail', 'catch-block', 'file or payslip not found.');
                return;
            }

            if (!file_exists($payslipMonth->file_path) || !is_readable($payslipMonth->file_path)) {
                $payslipMonth->status = 'rejected';
                $payslipMonth->save();
                AccessLog::accessLog(NULL, 'App\Services', 'PayslipService', 'sendUserVerificationMail', 'catch-block', 'File does not exists or not readable .');
                return;
            }

            $file = file_get_contents($payslipMonth->file_path);
            $data = array_map("str_getcsv", preg_split('/\r*\n+|\r+/', $file));

            if( empty($data) ) {
                $payslipMonth->status = 'rejected';
                $payslipMonth->save();
                AccessLog::accessLog(NULL, 'App\Services', 'PayslipService', 'sendUserVerificationMail', 'catch-block', 'No data found in the file.');
                return;
            }

            for ( $i=7; $i < count($data) - 1; $i++ ) { 

                if( empty($data[$i][1]) ) {
                    continue;
                }
                
                $userObj = User::where('employee_id', $data[$i][1])->where('is_active', 1)->first();
                
                if( !$userObj ) {
                    continue;
                    AccessLog::accessLog(NULL, 'App\Services', 'PayslipService', 'sendUserVerificationMail', 'catch-block', 'User not found for the payslip details entry'.$data[$i][1]);
                }
                $savePayslipDataUser = PayslipData::saveData($data[$i], $payslipMonth->id, $userObj->id);

                if( !$savePayslipDataUser['status'] ) {
                    AccessLog::accessLog(NULL, 'App\Services', 'PayslipService', 'sendUserVerificationMail', 'catch-block', 'User payslip not saved'.$data[$i][1]);
                }
            }
            return true;

        } catch ( Exception $e ) {
            AccessLog::accessLog(NULL, 'App\Services', 'PayslipService', 'sendUserVerificationMail', 'catch-block', $e->getMessage());
        }
    }

    public static function checkCSVHeader($header) {

    }
}