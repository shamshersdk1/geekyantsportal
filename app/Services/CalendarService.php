<?php namespace App\Services;

use App\Models\Admin\Calendar;
use App\Models\Admin\Leave;
use App\Models\User;

class CalendarService
{
    /**
     * Gives lastest working day for a user from current date
     *
     * @return date
     */

    public static function getLastestTwoWorkingDay($userId)
    {
        $isHoliday = false;
        $date = date("Y-m-d", strtotime("-1 days"));
        $dates = [];
        $count = 0;
        while ($count < 2) {
            $date = self::isDateEligible($date, $userId);
            $dates[] = $date;
            $date = date('Y-m-d', strtotime($date . ' -1 day'));
            $count = $count + 1;
        }
        return $dates;
    }

    /**
     * Check if the given date is a company holiday
     *
     * @return date
     */
    public static function isDateEligible($date, $userId)
    {

        $isHoliday = false;
        $isWeekend = false;
        $isLeave = false;
        if (Calendar::whereDate('date', $date)->count() > 0) {
            $isHoliday = true;
        } elseif (date('N', strtotime($date)) >= 6) {
            $isWeekend = true;
        } elseif (Leave::where('start_date', '<=', $date)->where('end_date', '>=', $date)->where('user_id', $userId)->count() > 0) {
            $isLeave = true;
        }

        if ($isHoliday || $isWeekend || $isLeave) {
            $date = date('Y-m-d', strtotime($date . ' -1 day'));
            return self::isDateEligible($date, $userId);
        } else {
            return $date;
        }

    }
    public static function getWorkingDays($month, $year)
    {

        if (null == ($year)) {
            $year = date("Y", time());
        }

        if (null == ($month)) {
            $month = date("m", time());
        }

        $startDate = date('Y-m-d', strtotime($year . '-' . $month . '-01'));
        $endDate = date("Y-m-t", strtotime($startDate));

        $count = 0;
        $nonWorkingDays = [6, 7];
        $holidays = Calendar::whereDate('date', '>=', $startDate)->whereDate('date', '<=', $endDate)->where('type', 'Holiday')->pluck('date')->toArray();
        for ($date = $startDate; strtotime($date) <= strtotime($endDate); $date = date('Y-m-d', strtotime($date . '+1 day'))) {
            if (in_array(date('N', strtotime($date)), $nonWorkingDays) || in_array(date('Y-m-d', strtotime($date)), $holidays)) {
                continue;
            } else {
                $count++;
            }
        }
        return $count;

        // $holidays = Calendar::where('date', '>=', $startDate)->where('date', '<=', $endDate)->get();
        // // do strtotime calculations just once
        // $endDate = strtotime($endDate);
        // $startDate = strtotime($startDate);

        // //The total number of days between the two dates. We compute the no. of seconds and divide it to 60*60*24
        // //We add one to inlude both dates in the interval.
        // $days = ($endDate - $startDate) / 86400 + 1;

        // $no_full_weeks = floor($days / 7);
        // $no_remaining_days = fmod($days, 7);

        // //It will return 1 if it's Monday,.. ,7 for Sunday
        // $the_first_day_of_week = date("N", $startDate);
        // $the_last_day_of_week = date("N", $endDate);

        // //---->The two can be equal in leap years when february has 29 days, the equal sign is added here
        // //In the first case the whole interval is within a week, in the second case the interval falls in two weeks.
        // if ($the_first_day_of_week <= $the_last_day_of_week) {
        //     if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week) {
        //         $no_remaining_days--;
        //     }

        //     if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week) {
        //         $no_remaining_days--;
        //     }

        // } else {
        //     // (edit by Tokes to fix an edge case where the start day was a Sunday
        //     // and the end day was NOT a Saturday)

        //     // the day of the week for start is later than the day of the week for end
        //     if ($the_first_day_of_week == 7) {
        //         // if the start date is a Sunday, then we definitely subtract 1 day
        //         $no_remaining_days--;

        //         if ($the_last_day_of_week == 6) {
        //             // if the end date is a Saturday, then we subtract another day
        //             $no_remaining_days--;
        //         }
        //     } else {
        //         // the start date was a Saturday (or earlier), and the end date was (Mon..Fri)
        //         // so we skip an entire weekend and subtract 2 days
        //         $no_remaining_days -= 2;
        //     }
        // }

        // //The no. of business days is: (number of weeks between the two dates) * (5 working days) + the remainder
        // //---->february in none leap years gave a remainder of 0 but still calculated weekends between first and last day, this is one way to fix it
        // $workingDays = $no_full_weeks * 5;
        // if ($no_remaining_days > 0) {
        //     $workingDays += $no_remaining_days;
        // }

        // //We subtract the holidays
        // foreach ($holidays as $holiday) {
        //     $time_stamp = strtotime($holiday->date);
        //     //If the holiday doesn't fall in weekend
        //     if ($startDate <= $time_stamp && $time_stamp <= $endDate && date("N", $time_stamp) != 6 && date("N", $time_stamp) != 7) {
        //         $workingDays--;
        //     }

        // }
        // return $workingDays;
    }

    // public static function getWorkingDays($fromDate, $toDate, $userId = null) {
    //     $holidayCount = Calendar::where('date','>=',$date)->where('date','<=',$toDate)->count();
    //     if($holidayCount) {
    //         //self::getWorkingDays
    //     }

    // }
    // public static function generateCalendar($month, $year) {
    //     $day_num=date("j"); //If today is September 29, $day_num=29
    //     //$month_num = date("m"); //If today is September 29, $month_num=9
    //     $month_num = $month;
    //     //$year = date("Y"); //4-digit year
    //     $date_today = getdate(mktime(0,0,0,$month_num,1,$year)); //Returns array of date info for 1st day of this month
    //     $month_name = $date_today["month"]; //Example: "September" - to label the Calendar
    //     $first_week_day = $date_today["wday"]; //"wday" is 0-6, 0 being Sunday. This is for day 1 of this month

    //     $cont = true;
    //     $today = 27; //The last day of the month must be >27, so start here
    //     while (($today <= 32) && ($cont)) //At 32, we have to be rolling over to the next month
    //     {
    //         //Iterate through, incrementing $today
    //         //Get the date information for the (hypothetical) date $month_num/$today/$year
    //         $date_today = getdate(mktime(0,0,0,$month_num,$today,$year));
    //         //Once $date_today's month ($date_today["mon"]) rolls over to the next month, we've found the $lastday
    //         if ($date_today["mon"] != $month_num)
    //         {
    //         $lastday = $today - 1; //If we just rolled over to the next month, need to subtract 1 to get our $lastday
    //         $cont = false; //This kicks us out of the while loop
    //         }
    //         $today++;
    //     }
    //     $data =[];
    //     $day = 1; //This variable will track the day of the month
    //     $wday = $first_week_day; //This variable will track the day of the week (0-6, with Sunday being 0)
    //     $firstweek = true; //Initialize $firstweek variable so we can deal with it first
    //     while ( $day <= $lastday) //Iterate through all days of the month
    //     {
    //         //Special case - first week (remember we initialized $first_week_day above)
    //         if ($firstweek)
    //         {
    //         //echo "<tr align=left>";
    //             for ($i=1; $i<=$first_week_day; $i++)
    //             {
    //                 //echo "<td> </td>";
    //                 //Put a blank cell for each day until you hit $first_week_day
    //                 $data[] = "-";
    //             }
    //             $firstweek = false; //Great, we're done with the blank cells
    //         }
    //         if ($wday==0) //Start a new row every Sunday
    //             //echo "<tr align=left>";

    //     //Now we are just outputting a cell for each day of the week showing $day.

    //     echo "<td";
    //     if($day==$day_num) echo " bgcolor='yellow'"; //highlight TODAY in yellow
    //     echo ">$day</td>";
    //     if ($wday==6)
    //     echo "</tr>"; //If today is Saturday, close this row

    //     $wday++; //Increment $wday
    //     $wday = $wday % 7; //Make sure $wday stays between 0 and 6 (so when $wday++ == 7, this will take it back to 0)
    //     $day++; //Increment $day
    //     }

    //     Now all that’s left to do is end the final row of the month with blank cells.

    //     while($wday <=6 ) //Until we get through Saturday
    //     {
    //     echo "<td> </td>"; //Output an empty cell
    //     $wday++;
    //     }
    //     echo "</tr></table>";

    // }

    public static function getWorkingDaysByDate($month, $year, $startDate, $enddate)
    {

        if (null == ($year)) {
            $year = date("Y", time());
        }

        if (null == ($month)) {
            $month = date("m", time());
        }

        $startDate = date('Y-m-d', strtotime($startDate));
        $endDate = date("Y-m-t", strtotime($enddate));

        $holidays = Calendar::where('date', '>=', $startDate)->where('date', '<=', $endDate)->get();
        // do strtotime calculations just once
        $endDate = strtotime($endDate);
        $startDate = strtotime($startDate);

        //The total number of days between the two dates. We compute the no. of seconds and divide it to 60*60*24
        //We add one to inlude both dates in the interval.
        $days = ($endDate - $startDate) / 86400 + 1;

        $no_full_weeks = floor($days / 7);
        $no_remaining_days = fmod($days, 7);

        //It will return 1 if it's Monday,.. ,7 for Sunday
        $the_first_day_of_week = date("N", $startDate);
        $the_last_day_of_week = date("N", $endDate);

        //---->The two can be equal in leap years when february has 29 days, the equal sign is added here
        //In the first case the whole interval is within a week, in the second case the interval falls in two weeks.
        if ($the_first_day_of_week <= $the_last_day_of_week) {
            if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week) {
                $no_remaining_days--;
            }

            if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week) {
                $no_remaining_days--;
            }

        } else {
            // (edit by Tokes to fix an edge case where the start day was a Sunday
            // and the end day was NOT a Saturday)

            // the day of the week for start is later than the day of the week for end
            if ($the_first_day_of_week == 7) {
                // if the start date is a Sunday, then we definitely subtract 1 day
                $no_remaining_days--;

                if ($the_last_day_of_week == 6) {
                    // if the end date is a Saturday, then we subtract another day
                    $no_remaining_days--;
                }
            } else {
                // the start date was a Saturday (or earlier), and the end date was (Mon..Fri)
                // so we skip an entire weekend and subtract 2 days
                $no_remaining_days -= 2;
            }
        }

        //The no. of business days is: (number of weeks between the two dates) * (5 working days) + the remainder
        //---->february in none leap years gave a remainder of 0 but still calculated weekends between first and last day, this is one way to fix it
        $workingDays = $no_full_weeks * 5;
        if ($no_remaining_days > 0) {
            $workingDays += $no_remaining_days;
        }

        //We subtract the holidays
        foreach ($holidays as $holiday) {
            $time_stamp = strtotime($holiday->date);
            //If the holiday doesn't fall in weekend
            if ($startDate <= $time_stamp && $time_stamp <= $endDate && date("N", $time_stamp) != 6 && date("N", $time_stamp) != 7) {
                $workingDays--;
            }

        }
        return $workingDays;
    }

}
