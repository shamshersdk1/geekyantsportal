<?php namespace App\Services;

use Validator;
use App\Models\Admin\Project;
use App\Models\Admin\AssetAssignmentDetail;

use DB;
use Input;
use Redirect;
use App\Models\User;
use Auth;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use Config;
use Exception;

class AssetService {

	public static function getAssignedUsers($assetCategories, $search)
	{
        $return_asset_categories = [];
        $return_object = [];
        $return_asset_object = [];
        $asset_category = [];
        foreach ( $assetCategories as $assetCategory )
        {
            $asset_category['id'] = $assetCategory->id;
            $asset_category['name'] = $assetCategory->name;
            $asset_category['assets'] = [];
            if ( !empty( $assetCategory->assets ) )
            {
                foreach ($assetCategory->assets as $index => $asset)
                {
                    $assigned_to = AssetAssignmentDetail::assignedUser($asset->id)->first();
                    $assigned_to = isset($assigned_to->user->name) ? $assigned_to->user->name : '';
                    if ( $search != '' )
                    {
                        if ((stripos ( $asset->name, $search ) !== false ) ||
                            (stripos( $asset->serial_number, $search ) !== false ) || 
                            (stripos( $asset->invoice_number, $search ) !== false )|| 
                            (stripos( $assigned_to, $search ) !== false ) )
                            {
                                $return_object['id'] = $asset->id;
                                $return_object['name'] = $asset->name;
                                $return_object['serial_number'] = $asset->serial_number;
                                $return_object['is_working'] = $asset->is_working;
                                $return_object['invoice_number'] = $asset->invoice_number;
                                $return_object['purchase_date'] = $asset->purchase_date;
                                $return_object['meta'] = $asset->metas()->get();
                                $return_object['assigned_to'] = $assigned_to;
                            }
                    }
                    else
                    {
                        $return_object['id'] = $asset->id;
                        $return_object['name'] = $asset->name;
                        $return_object['serial_number'] = $asset->serial_number;
                        $return_object['is_working'] = $asset->is_working;
                        $return_object['invoice_number'] = $asset->invoice_number;
                        $return_object['purchase_date'] = $asset->purchase_date;
                        $return_object['meta'] = $asset->metas()->get();
                        $return_object['assigned_to'] = $assigned_to;
                    }
                    if ( !empty($return_object) ){
                        $return_asset_object[] = $return_object;
                    }
                    $return_object = []; 
                }
            $asset_category['assets'] = $return_asset_object;
            $return_asset_object = [];
            }
            $return_asset_categories[] = $asset_category;
            $asset_category = [];
        }
        
        return $return_asset_categories;
	}
}