<?php namespace App\Services;

use App\Models\Admin\CalendarYear;
use App\Models\Admin\LeaveType;
use App\Models\Admin\Project;
use App\Models\Admin\ProjectResource;
use App\Models\Leave\LeaveCalendarYear;
use App\Models\Leave\UserLeaveBalance;
use App\Models\Leave\UserLeaveTransaction;
use App\Models\User;
use App\Models\UserTimesheet;
use App\Services\Leave\UserLeaveService;
use DB;
use App\Models\Admin\AssetAssignmentDetail;
use App\Models\Admin\MyNetworkDevice;
use App\Models\Admin\UserIpAddress;
use App\Models\SshKey;
use App\Models\Admin\Insurance\Insurance;

class UserService
{

    public static function deactivatePreCheck($userId){
        $response['status']=true;
        $response['error']=[];

        $user=User::find($userId);
        if(!$user){
            return ;
        }

        $assets=AssetAssignmentDetail::getAssets($userId);

        if(count($assets) > 0){
        $response['status']=false;
        $response['error'][]='Please release all the asset assigned to the user ';
        }

        $isNetworkDevice=MyNetworkDevice::where('user_id',$userId)->get();

        if(count($isNetworkDevice) > 0){
            $response['status']=false;
            $response['error'][]='Please release all the network devices assigned to the user ';
         } 

        $userProject=ProjectResource::getActiveProjectIdArray($userId);

        if(count($userProject) > 0){
            $response['status']=false;
            $response['error'][]='Please release all the project assigned to the user ';
         } 

        $userIp= UserIpAddress::userFreeIp($userId);
        if($userIp['status']==true){
            $response['status']=false;
            $response['error'][]='Please release all the Ip assigned to the user ';
         } 

         $ssh=SshKey::getSshKey($userId);
         
         if($ssh['status'] == true){
            $response['status']=false;
            $response['error'][]='Please release ssh of the user ';
 
         }

        $insurances=Insurance::where('insurable_type','App\Models\User')->where('insurable_id',$userId)->get();
        if(count($insurances) > 0){
            $response['status']=false;
            $response['error'][]='Please release all the insurance assigned to the user ';
        }

        return $response;
     }

    public static function getReportingManager($userId)
    {
        $reporting_manager = [];
        $user = User::find($userId);

        if (!empty($user) && !empty($user->reportingManager)) {
            $reporting_manager['id'] = $user->reportingManager->id;
            $reporting_manager['name'] = $user->reportingManager->name;
            $reporting_manager['email'] = $user->reportingManager->email;
        }

        return $reporting_manager;
    }

    public static function getSalesExecutiveList()
    {
        return User::whereHas('roles', function ($query) {
            $query->where('code', 'sales-manager');
        })->where('is_active', 1)->get();
    }

    public static function getProjectCoordinatorList()
    {
        return User::whereHas('roles', function ($query) {
            $query->where('code', 'account-manager');
        })->where('is_active', 1)->get();
    }

    public static function getProjectManagerList()
    {
        return User::whereHas('roles', function ($query) {
            $query->where('code', 'team-lead');
        })->where('is_active', 1)->get();
    }

    public static function getCodeLeadList()
    {
        return User::whereHas('roles', function ($query) {
            $query->where('code', 'code-lead');
        })->where('is_active', 1)->get();
    }

    public static function getDeliveryLeadList()
    {
        return User::whereHas('roles', function ($query) {
            $query->where('code', 'delivery-lead');
        })->where('is_active', 1)->get();
    }

    public static function getAllActiveUserList()
    {
        return User::where('is_active', 1)->get();
    }

    public static function getMyTeamLeads($user_id)
    {
        $date = date('Y-m-d');
        $teamLeads = [];
        $userObj = User::find($user_id);
        if ($userObj) {
            $projectsEndDated = ProjectResource::with('project')
                ->where('start_date', '<=', $date)
                ->where('end_date', '>=', $date)
                ->where('user_id', $user_id)->get();

            $projectsEndNull = ProjectResource::with('project')
                ->where('start_date', '<=', $date)
                ->whereNull('end_date')
                ->where('user_id', $user_id)->get();

            $projects = $projectsEndDated->merge($projectsEndNull);
            foreach ($projects as $project) {
                if ($project->project->projectManager) {
                    $teamLeads[] = $project->project->projectManager;
                }
            }
        }
        $teamLeads = array_values(array_unique($teamLeads));

        return $teamLeads;
    }

    public static function updateUserLeaveBalance($user_id, $year)
    {
        $year = $year ?: date('Y');
        $calendarYearObj = CalendarYear::where('year', $year)->first();
        $user = User::find($user_id);

        if (!$user || !$calendarYearObj) {
            return;
        }

        if ($user->confirmation_date != null && strtotime($user->confirmation_date) <= strtotime(date('Y-m-d'))) {
            $leaveCalendarYears = LeaveCalendarYear::where('calendar_year_id', $calendarYearObj->id)->get();

            foreach ($leaveCalendarYears as $leaveCalendarYear) {
                if ($leaveCalendarYear->leaveType) {
                    $data = [];
                    $data['leave_type_id'] = $leaveCalendarYear->leave_type_id;
                    $data['reference_type'] = "App\\Models\\Leave\\LeaveCalendarYear";
                    $data['reference_id'] = $leaveCalendarYear->calendar_year_id;
                    $data['user_id'] = $user_id;
                    $data['calendar_year_id'] = $leaveCalendarYear->calendar_year_id;
                    $leaveTransactionObj = UserLeaveTransaction::where('leave_type_id', $leaveCalendarYear->leave_type_id)->where('reference_type', $data['reference_type'])->where('reference_id', $data['reference_id'])->where('user_id', $user_id)->where('calendar_year_id', $data['calendar_year_id'])->first();
                    if(!$leaveTransactionObj) {
                        if (date('Y', strtotime($user->joining_date)) == $year) {
                            $data['days'] = NewLeaveService::getUserProRataLeaves($user->id, $leaveCalendarYear->leave_type_id);
                        } else {
                            $data['days'] = $leaveCalendarYear->max_allowed_leave;
                        }
                        $data['days'] = $data['days'];
                        $data['calculation_date'] = date('Y-m-d');
                        UserLeaveTransaction::firstOrCreate($data);
                    }
                }
            }
            //check if user is confirmed before the current year and call getCarryForward if true
            if (date('Y', strtotime($user->confirmation_date)) != $year) {
                self::getCarryForward($user->id, $year);
            }
        }
    }

    public static function getCarryForward($user_id, $currYear)
    {
        $currYear = $currYear ?: date('Y');
        $currYearObj = CalendarYear::where('year', $currYear)->first();
        if (!$currYearObj) {
            return;
        }
        $prevYear = $currYear - 1;
        $prevYearObj = CalendarYear::where('year', $prevYear)->first();
        if (!$prevYearObj) {
            return;
        }
        $userObj = User::find($user_id);
        if (!$userObj) {
            return;
        }
        $PLObj = LeaveType::where('code', 'paid')->first();
        if (!$PLObj) {
            return;
        }
        $prevPLObj = UserLeaveService::getUserAvailableLeaveBalance($prevYear, $userObj->id);
        $currPLObj = UserLeaveService::getUserAvailableLeaveBalance($currYear, $userObj->id);
        $prevPLCount = $prevPLObj["paid"];
        $currPLCount = $currPLObj["paid"];
        if ($currPLCount + $prevPLCount > 26) {
            $carryForward = 26 - $currPLCount;
        } else {
            $carryForward = $prevPLCount;
        }
        if ($carryForward > 0) {
            $data['leave_type_id'] = $PLObj->id;
            $data['days'] = $carryForward;
            $data['reference_type'] = "App\Models\Leave\LeaveCalendarYear";
            $data['reference_id'] = $prevYearObj->id;
            $data['user_id'] = $user_id;
            $data['calendar_year_id'] = $currYearObj->id;
            UserLeaveBalance::addUserLeaves($data);
        }
        return;
    }

    public static function updateProRataUserLeaveBalance($user_id)
    {
        $year = date('Y');
        $userObj = User::find($user_id);
        $calendarYearObj = CalendarYear::where('year', $year)->first();
        $leaveCalendarYears = LeaveCalendarYear::where('calendar_year_id', $calendarYearObj->id)->get();
        try {
            DB::beginTransaction();
            foreach ($leaveCalendarYears as $leaveCalendarYear) {
                $proRataLeaves = NewLeaveService::getUserProRataLeaves($userObj->id, $leaveCalendarYear->leave_type_id);
                $data['leave_type_id'] = $leaveCalendarYear->leave_type_id;
                $data['days'] = $proRataLeaves;
                $data['reference_type'] = "App\Models\Admin\LeaveCalendarYear";
                $data['reference_id'] = $leaveCalendarYear->calendar_year_id;
                $data['user_id'] = $user_id;
                $data['calendar_year_id'] = $leaveCalendarYear->calendar_year_id;
                if (!UserLeaveBalance::addUserLeaves($data)) {
                    throw new Exception("error crediting leaves to user"); //?
                }
            }
        } catch (Exception $e) {
            DB::rollBack();
        } finally {
            DB::commit();
        }
    }

    public static function getAssignedProjects($user_id, $date)
    {
        $date = $date ?: date('Y-m-d');
        $projects = [];
        $userObj = User::find($user_id);
        if ($userObj) {
            $projectsEndDated = ProjectResource::with('project')
                ->where('start_date', '<=', $date)
                ->where('end_date', '>=', $date)
                ->where('user_id', $user_id)->get();

            $projectsEndNull = ProjectResource::with('project')
                ->where('start_date', '<=', $date)
                ->whereNull('end_date')
                ->where('user_id', $user_id)->get();

            $projectsCombined = $projectsEndDated->merge($projectsEndNull);
            foreach ($projectsCombined as $project_row) {
                if ($project_row->project) {
                    $projects[] = $project_row->project;
                }
            }
        }
        // $teamLeads = array_values(array_unique($teamLeads));

        return $projects;
    }

    public static function getTimesheetProjects($user_id, $date)
    {
        $date = $date ?: date('Y-m-d');
        $projects = [];
        $userObj = User::find($user_id);
        if ($userObj) {
            $timesheets = UserTimesheet::where('user_id', $userObj->id)->whereDate('date', $date)->distinct('project_id')->get();
            foreach ($timesheets as $timesheet_row) {
                if ($timesheet_row->project) {
                    $projects[] = $timesheet_row->project;
                }
            }
        }
        return $projects;
    }
}
