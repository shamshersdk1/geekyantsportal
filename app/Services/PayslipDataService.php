<?php namespace App\Services;

use App\Models\Admin\PayslipDataItem;
use App\Models\Admin\PayslipDataMaster;
use App\Models\Admin\PayslipDataMonth;
use App\Models\User;
use Auth;

class PayslipDataService
{

    public static function getSalaryByUserIdAndMonth($user_id, $month, $year)
    {

        $month_string = \DateTime::createFromFormat('!m', $month)->format('F');
        $last_day_given_month = date('t', strtotime($month_string . ' 01, ' . $year));
        $month_start_date = date('Y-m-d', strtotime($month_string . ' 01, ' . $year)); //First day of the month;
        $month_end_date = date('Y-m-d', strtotime($month_string . ' ' . $last_day_given_month . ', ' . date('Y'))); // Last day of the month;

        $payslipMonth = PayslipDataMonth::where([
            ['date', '>=', $month_start_date],
            ['date', '<=', $month_end_date],
        ])->first();
        if ($payslipMonth) {
            $payslipMaster = PayslipDataMaster::where('payslip_data_month_id', $payslipMonth->id)->where('user_id', '=', $user_id)->first();
            $payslipItems = PayslipDataItem::where('payslip_data_month_id', $payslipMonth->id)->where('user_id', '=', $user_id)->first();

            if(!$payslipMaster){
              echo "Missing Data for ".$user_id;
              die;
            }
            
            if (!$payslipItems) {
               echo "Missing Data for ".$user_id;
               die;
            }

            
            if ($payslipItems->domestic_onsite_local == null) {
                $payslipItems->domestic_onsite_local = 0;
            }
            if ($payslipItems->domestic_onsite_outstation == null) {
                $payslipItems->domestic_onsite_outstation = 0;
            }

            if ($payslipItems->international_onsite == null) {
                $payslipItems->international_onsite = 0;
            }

            if ($payslipItems->tech_talk_bonus == null) {
                $payslipItems->tech_talk_bonus = 0;
            }
            if ($payslipItems->annual_bonus == null) {
                $payslipItems->annual_bonus = 0;
            }

            if ($payslipItems->confirmation_bonus == null) {
                $payslipItems->confirmation_bonus = 0;
            }

            if ($payslipItems->extra_working_day == null) {
                $payslipItems->extra_working_day = 0;
            }

            if ($payslipItems->national_holiday_working_bonus == null) {
                $payslipItems->national_holiday_working_bonus = 0;
            }
        

            return array('payslipMonth' => $payslipMonth, 'payslipMaster' => $payslipMaster, 'payslipItems' => $payslipItems);
        }

        return false;
    }

    public static function createRecords($payslipDataMonthId)
    {
        $createdBy = Auth::user();
        $users = User::where('is_active', 1)->orderBy('employee_id')->get();
        // Check for last month records
        $lastMonth = date("m", strtotime("first day of previous month"));
        $lastMonthPayslipObj = PayslipDataMonth::whereMonth('date', '=', $lastMonth)->first();
        if ($lastMonthPayslipObj) {
            foreach ($users as $user) {
                $lastMonthMasterObj = PayslipDataMaster::where('payslip_data_month_id', $lastMonthPayslipObj->id)->where('user_id', $user->id)->first();
                $payslipMaster = new PayslipDataMaster();
                $payslipMaster->payslip_data_month_id = $payslipDataMonthId;
                $payslipMaster->user_id = $user->id;
                $payslipMaster->employee_id = $user->employee_id;
                $payslipMaster->variable_pay = !empty($lastMonthMasterObj->variable_pay) ? $lastMonthMasterObj->variable_pay : '';
                $payslipMaster->annual_in_hand = !empty($lastMonthMasterObj->annual_in_hand) ? $lastMonthMasterObj->annual_in_hand : '';
                $payslipMaster->created_by = $createdBy->id;
                $payslipMaster->save();
            }
        } else {
            foreach ($users as $user) {
                $payslipMaster = new PayslipDataMaster();
                $payslipMaster->payslip_data_month_id = $payslipDataMonthId;
                $payslipMaster->user_id = $user->id;
                $payslipMaster->employee_id = $user->employee_id;
                $payslipMaster->created_by = $createdBy->id;
                $payslipMaster->save();
            }

        }
    }

    public static function createItemRecords($payslipDataMonthId)
    {
        $createdBy = Auth::user();
        $users = User::where('is_active', 1)->orderBy('employee_id')->get();
        // Check for last month records
        $lastMonth = date("m", strtotime("first day of previous month"));
        $lastMonthPayslipObj = PayslipDataMonth::whereMonth('date', '=', $lastMonth)->first();
        if ($lastMonthPayslipObj) {
            foreach ($users as $user) {
                $lastMonthItemObj = PayslipDataItem::where('payslip_data_month_id', $lastMonthPayslipObj->id)->where('user_id', $user->id)->first();
                $payslipItem = new PayslipDataItem();
                $payslipItem->payslip_data_month_id = $payslipDataMonthId;
                $payslipItem->user_id = $user->id;
                $payslipItem->employee_id = $user->employee_id;
                $payslipItem->tech_talk_bonus = !empty($lastMonthItemObj->tech_talk_bonus) ? $lastMonthItemObj->tech_talk_bonus : '';
                $payslipItem->annual_bonus = !empty($lastMonthItemObj->annual_bonus) ? $lastMonthItemObj->annual_bonus : '';
                $payslipItem->confirmation_bonus = !empty($lastMonthItemObj->confirmation_bonus) ? $lastMonthItemObj->confirmation_bonus : '';
                $payslipItem->domestic_onsite_outstation = !empty($lastMonthItemObj->domestic_onsite_outstation) ? $lastMonthItemObj->domestic_onsite_outstation : '';
                $payslipItem->domestic_onsite_local = !empty($lastMonthItemObj->domestic_onsite_local) ? $lastMonthItemObj->domestic_onsite_local : '';
                $payslipItem->international_onsite = !empty($lastMonthItemObj->international_onsite) ? $lastMonthItemObj->international_onsite : '';
                $payslipItem->extra_working_day = !empty($lastMonthItemObj->extra_working_day) ? $lastMonthItemObj->extra_working_day : '';
                $payslipItem->national_holiday_working_bonus = !empty($lastMonthItemObj->national_holiday_working_bonus) ? $lastMonthItemObj->national_holiday_working_bonus : '';
                $payslipItem->created_by = $createdBy->id;
                $payslipItem->save();
            }
        } else {
            foreach ($users as $user) {
                $payslipItem = new PayslipDataItem();
                $payslipItem->payslip_data_month_id = $payslipDataMonthId;
                $payslipItem->user_id = $user->id;
                $payslipItem->employee_id = $user->employee_id;
                $payslipItem->created_by = $createdBy->id;
                $payslipItem->save();
            }
        }

    }

    public static function updateMasterDetails($input, $createdBy)
    {
        $payslipMonthId = $input['payslip_data_month_id'];

        foreach ($input['variable_pay'] as $index => $row) {

            if (!empty($input['salary_on_hold']) && in_array($index, $input['salary_on_hold'])) {
                $salary_on_hold = true;
            } else {
                $salary_on_hold = false;
            }
            $payslipMaster = PayslipDataMaster::find($index);
            $payslipMaster->variable_pay = $input['variable_pay'][$index];
            $payslipMaster->annual_in_hand = $input['annual_inhand'][$index];
            $payslipMaster->salary_on_hold = $salary_on_hold;
            $payslipMaster->created_by = $createdBy;
            $payslipMaster->save();
        }
    }

    public static function updateItemDetails($input, $createdBy)
    {
        $payslipMonthId = $input['payslip_data_month_id'];

        foreach ($input['tech_talk_bonus'] as $index => $row) {
            $payslipItem = PayslipDataItem::find($index);
            $payslipItem->tech_talk_bonus = $input['tech_talk_bonus'][$index];
            $payslipItem->annual_bonus = $input['annual_bonus'][$index];
            $payslipItem->confirmation_bonus = $input['confirmation_bonus'][$index];
            $payslipItem->domestic_onsite_outstation = $input['domestic_onsite_outstation'][$index];
            $payslipItem->domestic_onsite_local = $input['domestic_onsite_local'][$index];
            $payslipItem->international_onsite = $input['international_onsite'][$index];
            $payslipItem->extra_working_day = $input['extra_working_day'][$index];
            $payslipItem->national_holiday_working_bonus = $input['national_holiday_working_bonus'][$index];
            $payslipItem->created_by = $createdBy;
            $payslipItem->save();
        }
    }

}
