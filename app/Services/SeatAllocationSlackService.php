<?php namespace App\Services;

use Validator;
use App\Models\User;
use App\Models\Seat;
use App\Models\Floor;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;

use DB;
use Input;
use Redirect;
use Auth;

class SeatAllocationSlackService {

	public static function sendMessage($seat, $flag, $user_id = null )
	{
        $channel_id = 'C9FCCUK88';
		$user = User::where('id', $user_id)->first();
        $floor = Floor::where('id',$seat->floor_id)->first();
        if ( $flag == 'Add' )
        {
            $allocated_seat = $seat->name;
            switch ($floor->floor_number) 
            {
                case 1:
                    $slack_message = $user->name.' has been assigned a seat '.$allocated_seat.' on '.$floor->floor_number.'st floor';
                    break;
                case 2:
                    $slack_message = $user->name.' has been assigned a seat '.$allocated_seat.' on '.$floor->floor_number.'nd floor';
                    break;
                case 3:
                    $slack_message = $user->name.' has been assigned a seat '.$allocated_seat.' on '.$floor->floor_number.'rd floor';
                    break;
                default:
                    $slack_message = $user->name.' has been assigned a seat '.$allocated_seat.' on '.$floor->floor_number.'th floor';
            }
        }
        else
        {
            $unallocated_seat = $seat->name;
            switch ($floor->floor_number) 
            {
                case 1:
                    $slack_message = 'Seat '.$unallocated_seat.' on '. $floor->floor_number.'st floor assigned to '.$user->name.' has been released';
                    break;
                case 2:
                    $slack_message = 'Seat '.$unallocated_seat.' on '. $floor->floor_number.'nd floor assigned to '.$user->name.' has been released';
                    break;
                case 3:
                    $slack_message = 'Seat '.$unallocated_seat.' on '. $floor->floor_number.'rd floor assigned to '.$user->name.' has been released';
                    break;
                default:
                    $slack_message = 'Seat '.$unallocated_seat.' on '. $floor->floor_number.'th floor assigned to '.$user->name.' has been released';
            }
        }
          $client = new Client();
          $url = config('app.geekyants_portal_url');
          $url = $url.'/api/v1/slack-message-on-channel';
          $response = $client->request("POST", $url, [
                'form_params' => [
                    'channel_id' => $channel_id,
                    'message' => $slack_message
                ]
                ]);
	}

}