<?php 
namespace App\Services;

use App\Models\User;
use App\Models\Admin\FeedbackMonth;
use App\Models\Admin\FeedbackUser;
use App\Models\NonworkingCalendar;
use Config;
use Exception;
use View;
use DB;
use Auth;

class VariablePayService
{
    public static function getEmployeeVariablePayAmount($user_id, $month, $year){
        $nextMonth = date('m', strtotime('+1 month', strtotime($year."-".$month."-01")));
        $nextMonthYear = date('Y', strtotime('+1 month', strtotime($year."-".$month."-01")));
        $salary = PayslipDataService::getSalaryByUserIdAndMonth($user_id, $nextMonth, $nextMonthYear);
        if(isset($salary['payslipMaster'])){
            return $salary['payslipMaster']->variable_pay;
        }
        return 0;
    }

    public static function getEmployeeVariablePay($user_id, $month, $year){
        
        $employeeRating = self::getEmployeeFeedback($user_id, $month, $year);
        $employeeCalendar = NonworkingCalendar::getUserCalendarDatewise($user_id, $month, $year);

        $annualAmount = self::getEmployeeVariablePayAmount($user_id, $month, $year);
        $monthlyAmount = round($annualAmount/12, 2);

        if(is_array($employeeCalendar) && isset($employeeCalendar['summary'])){
            $workingDays = count($employeeCalendar['summary']['workingDays']);
            $workedDays = $workingDays;
            foreach($employeeCalendar['summary']['leaves'] as $leaves){
                if($leaves->type == 'unpaid')
                    $workedDays--;
            }
        }


        $timesheetAmount = ($workedDays / $workingDays) * ($monthlyAmount * 0.25); //25% of timesheet;
        $ratingAmount = $employeeRating['score'] * ($monthlyAmount * 0.75); //75% of timesheet;

        return [
            'workingDays'=> $workingDays, 
            'workedDays'=> $workedDays, 
            'employeeRating'=>$employeeRating['score'], 
            'timesheetAmount' => round($timesheetAmount,2), 
            'ratingAmount'=> round($ratingAmount,2), 
            'totalAmount' => ($timesheetAmount+$ratingAmount),
            'employeeRating' => $employeeRating
        ];
    }

    public static function getEmployeeFeedback($user_id, $month, $year){
        $return = array('success'=>false, 'score'=>0, 'feedbacks' => []);
        $success = false;
        $score = 0;
        $feedbackMonth = FeedbackMonth::where('month', $month)->where('year', $year)->first();
        if($feedbackMonth){
            $feedbacks = FeedbackUser::where('user_id', $user_id)->where('feedback_month_id', $feedbackMonth->id)->get();
            $totalScore = 0;
            $maxScore = 0;
            $success = true;
            foreach($feedbacks as $feedback){
                $maxScore += 10;
                if($feedback->status == 'pending'){
                    $rating = 5;
                }else{
                    $rating = $feedback->rating;
                }
                $totalScore += $rating;
            }
            if($totalScore == 0 && $maxScore == 0 ){
                $success = false;
            }else{
                $score = round($totalScore/$maxScore,2);
            }
            $return['success'] = $success;
            $return['score'] = $score;
            $return['feedbacks'] = $feedbacks;
        }
        return $return;
    }
}