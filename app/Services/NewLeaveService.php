<?php
namespace App\Services;

use App\Events\Leave\LeaveApplied;
use App\Events\Leave\LeaveApproved;
use App\Events\Leave\LeaveCancelled;
use App\Events\Leave\LeaveRejected;
use App\Jobs\Leave\LeaveDeductionMonthJob;
use App\Jobs\MessagingApp\PostMessageUserJob;
use App\Models\Admin\Appraisal;
use App\Models\Admin\Calendar;
use App\Models\Admin\CalendarYear;
use App\Models\Admin\Leave;
use App\Models\Admin\LeaveCalendarYear;
use App\Models\Admin\LeaveDeduction;
use App\Models\Admin\LeaveDeductionMonth;
use App\Models\Admin\LeaveType;
use App\Models\Admin\Project;
use App\Models\Admin\ProjectResource;
use App\Models\LeaveOverlapRequest;
use App\Models\Leave\UserLeaveBalance;
use App\Models\Leave\UserLeaveTransaction;
use App\Models\Month;
use App\Models\NonworkingCalendar;
use App\Models\ProjectManagerLeaveApproval;
use App\Models\SystemSetting;
use App\Models\User;
use App\Models\WorkingDayGroup;
use App\Services\Leave\UserLeaveService;
use App\Services\NewLeaveService;
use Auth;
use Carbon\Carbon;
use DB;
use Log;
use View;

class NewLeaveService
{

    public static function getLeaveYearlyDuration()
    {

        $from_year = date("Y", strtotime("-1 year")); // From Last Year
        $to_year = date("Y"); // To Current Year
        $from_date_string = $from_year . '-12-25'; // From 25th Dec Last Year
        $to_date_string = $to_year . '-12-25'; // From 25th Dec Current Year

        $dates['from_date'] = date($from_date_string);
        $dates['to_date'] = date($to_date_string);

        return $dates;
    }
    public static function getUserLeaveReport($userId)
    {
        $user = User::find($userId);
        $year = date('Y');
        $calendarYearObj = CalendarYear::where('year', $year)->first();

        if (!$user) {
            return false;
        }

        $leaveDuration = self::getLeaveYearlyDuration();

        // $totalSickLeaves = SystemSetting::where('key', 'max_sick_leave')->first();
        // $totalPaidLeaves = SystemSetting::where('key', 'max_paid_leave')->first();

        // $from_year = !empty($leaveDuration['leaveDuration']['from_year']) ?  $leaveDuration['leaveDuration']['from_year'] :  date("Y",strtotime("-1 year")); // From Last Year
        // $to_year = !empty($leaveDuration['leaveDuration']['to_year']) ?  $leaveDuration['leaveDuration']['to_year'] :   date("Y"); // To Current Year
        // $from_date_string = !empty($leaveDuration['leaveDuration']['from_date_string']) ?  $leaveDuration['leaveDuration']['from_date_string'] :    $from_year.'-12-25'; // From 25
        // $to_date_string = !empty($leaveDuration['leaveDuration']['to_date_string']) ?  $leaveDuration['leaveDuration']['to_date_string'] :    $to_year.'-12-25';
        $from_date = !empty($leaveDuration['from_date']) ? $leaveDuration['from_date'] : date($from_date_string);
        $to_date = !empty($leaveDuration['to_date']) ? $leaveDuration['to_date'] : date($to_date_string);

        // if ($totalSickLeaves) {
        //     $totalSickLeaves = $totalSickLeaves->value;
        // }
        // if ($totalPaidLeaves) {
        //     $totalPaidLeaves = $totalPaidLeaves->value;
        // }
        //echo $user->joining_date;
        if (isset($user->joining_date)) {
            $leaves = $user->leaves->where('type', 'sick')->where('status', 'approved')->where('end_date', '>=', $from_date)->where('start_date', '<=', $to_date);
            $joiningDate = $user->joining_date;
            $totalSickLeaves = NewLeaveService::getAllowedLeave($user->id, 'sick');
            $consumedSickLeaves = 0;
            foreach ($leaves as $leave) {
                $consumedSickLeaves = $consumedSickLeaves + $leave->days;
            }
        } else {
            $totalSickLeaves = 0;
            $consumedSickLeaves = 0;
        }
        if (isset($user->confirmation_date)) {
            $leaves = $user->leaves->whereIn('type', ['paid', 'half'])->where('status', 'approved')->where('end_date', '>=', $from_date)->where('start_date', '<=', $to_date);
            $joiningDate = $user->joining_date;
            $totalPaidLeaves = NewLeaveService::getAllowedLeave($user->id, 'paid');
            $consumedPaidLeaves = 0;
            foreach ($leaves as $leave) {
                $consumedPaidLeaves = $consumedPaidLeaves + $leave->days;
            }
        } else {
            $totalPaidLeaves = 0;
            $consumedPaidLeaves = 0;
        }

        $consumedUnpaidLeaves = 0;
        $unpaidLeaves = $user->leaves->where('type', 'unpaid')->where('status', 'approved');
        foreach ($unpaidLeaves as $leave) {
            $consumedUnpaidLeaves = $consumedUnpaidLeaves + $leave->days;
        }
        $result['consumedSickLeaves'] = $consumedSickLeaves;
        $result['totalSickLeaves'] = $totalSickLeaves;
        $result['consumedPaidLeaves'] = $consumedPaidLeaves;
        $result['totalPaidLeaves'] = $totalPaidLeaves;
        $result['consumedUnpaidLeaves'] = $consumedUnpaidLeaves;
        return $result;
    }
    //old name : managerDetails
    public static function getUserReportee($userId)
    {
        $user = User::find($userId);
        $data = [];
        $reporting_manager = empty($user->reportingManager) ? null : $user->reportingManager;
        if ($reporting_manager) {
            $data['reporting_manager_name'] = $reporting_manager->name;
            $data['reporting_manager_phone'] = empty($reporting_manager->profile) || empty($reporting_manager->profile->phone) ? "Unavailable" : $reporting_manager->phone;
        }
        $data['team_leads'] = [];
        $projectResources = ProjectResource::with('project')
            ->where('user_id', $user->id)
            ->where('start_date', '<=', date('Y-m-d'))
            ->where(function ($query) {
                $query->where('end_date', null)
                    ->orWhere('end_date', '>=', date('Y-m-d'));
            })->get();
        if (!empty($projectResources)) {
            foreach ($projectResources as $resource) {
                if ($resource->project && $resource->project->projectManager && $resource->project->projectManager->id != $user->id) {
                    $tl = $resource->project->projectManager;
                    $data['team_leads'][$tl->name] = empty($tl->profile) || empty($tl->profile->phone) ? "Unavailable" : $tl->profile->phone;
                }
            }
        }
        return $data;
    }
    //accept the starting day of the week and return the count of holiday in that week
    public static function globalLeaveCount($date)
    {
        $nextdate = strtotime($date);
        $nextdate = date("Y-m-d", strtotime("+7 day", $nextdate));
        $globalLeave = Calendar::where('date', '>=', $date)->where('date', '<=', $nextdate)->get();
        if (count($globalLeave) > 0) {
            return count($globalLeave);
        }

        return false;
    }
    //accept the starting day of the week and return the count of leave in that week
    public static function personalLeaveCount($userId, $date)
    {
        $response['status'] = false;
        $response['data'] = null;
        $count = 0;
        for ($i = 0; $i < 5; $i++) {
            $personalLeave = Leave::where('start_date', '<=', $date)->where('end_date', '>=', $date)->where('user_id', $userId)->get();
            if (count($personalLeave) > 0) {
                $count++;
            }
            $date = strtotime($date);
            $date = date("Y-m-d", strtotime("+1 day", $date));
        }
        if ($count > 0) {
            return $count;
        }
        return false;
    }

    /**
     * Calculate total number of sick leaves eligible based upon joining date
     *
     * @param $joiningDate => date string
     *
     * @return number
     */
    public static function maxSickLeave($user_id, $year_id)
    {
        $leave_balances = UserLeaveBalance::where('user_id', $user_id)->where('calendar_year_id', $year_id)->first();
        if ($leave_balances) {
            return $leave_balances->sick_leave_balance;
        } else {
            return 0;
        }
    }

    /**
     * Calculate total number of paid leaves eligible based upon confirmation date
     *
     * @param string $confirmationDate => date string
     *
     * @return number
     */
    public static function maxPersonalLeave($user_id, $year_id)
    {
        $leave_balances = UserLeaveBalance::where('user_id', $user_id)->where('calendar_year_id', $year_id)->first();
        if ($leave_balances) {
            return $leave_balances->paid_leave_balance;
        } else {
            return 0;
        }
    }

    public static function calculateWorkingDays($startDate, $endDate, $id)
    {
        $user = User::find($id);
        $count = 0;
        if (!empty($user->workingDayGroup) && !empty($user->workingDayGroup->workingDays)) {
            $group = $user->workingDayGroup->workingDays;
            $nonWorkingDays = [];
            if (!empty($group)) {
                if (!$group->monday) {
                    $nonWorkingDays[] = 1;
                }
                if (!$group->tuesday) {
                    $nonWorkingDays[] = 2;
                }
                if (!$group->wednesday) {
                    $nonWorkingDays[] = 3;
                }
                if (!$group->thursday) {
                    $nonWorkingDays[] = 4;
                }
                if (!$group->friday) {
                    $nonWorkingDays[] = 5;
                }
                if (!$group->saturday) {
                    $nonWorkingDays[] = 6;
                }
                if (!$group->sunday) {
                    $nonWorkingDays[] = 7;
                }
            } else {
                $nonWorkingDays = [6, 7];
            }
        } else {
            $nonWorkingDays = [6, 7];
        }

        $startDate = date('Y-m-d', strtotime($startDate));
        $endDate = date('Y-m-d', strtotime($endDate));

        $holidays = Calendar::whereDate('date', '>=', $startDate)->whereDate('date', '<=', $endDate)->where('type', 'Holiday')->pluck('date')->toArray();
        for ($date = $startDate; strtotime($date) <= strtotime($endDate); $date = date('Y-m-d', strtotime($date . '+1 day'))) {
            if (in_array(date('N', strtotime($date)), $nonWorkingDays) || in_array(date('Y-m-d', strtotime($date)), $holidays)) {
                continue;
            } else {
                $count++;
            }
        }
        return $count;
    }

    /**
     * Calculate total number of holidays in between leaves
     *
     * @param $startDate => date string
     * @param $endDate => date string
     *
     * @return number
     */
    public static function calculateNonWorkingDays($startDate, $endDate)
    {
        $startDate = date('Y-m-d', strtotime($startDate));
        $endDate = date('Y-m-d', strtotime($endDate));
        $holidayCount = Calendar::whereDate('date', '>=', $startDate)->whereDate('date', '<=', $endDate)->count();
        return $holidayCount;
    }

    /**
     * Calculate total number of leaves applied(Working and Nonworking)
     *
     * @param $startDate => date string
     * @param $endDate => date string
     *
     * @return number
     */
    public static function calculateTotalNumberOfDays($startDate, $endDate)
    {
        $datediff = strtotime($startDate) - strtotime($endDate);
        return abs(floor($datediff / (60 * 60 * 24))) + 1;
    }

    /**
     * The function returns the no. of business days between two dates and it skips the holidays
     *
     * @param $startDate => (date)
     * @param $endDate => (date)
     * @param $holidayCount => (number) The number of holidays between start and end date
     *
     * @return number
     */
    public static function getWorkingDays($startDate, $endDate, $holidayCount)
    {
        // do strtotime calculations just once
        $endDate = strtotime($endDate);
        $startDate = strtotime($startDate);

        //The total number of days between the two dates. We compute the no. of seconds and divide it to 60*60*24
        //We add one to inlude both dates in the interval.
        $days = ($endDate - $startDate) / 86400 + 1;

        $no_full_weeks = floor($days / 7);
        $no_remaining_days = fmod($days, 7);

        //It will return 1 if it's Monday,.. ,7 for Sunday
        $the_first_day_of_week = date("N", $startDate);
        $the_last_day_of_week = date("N", $endDate);

        //---->The two can be equal in leap years when february has 29 days, the equal sign is added here
        //In the first case the whole interval is within a week, in the second case the interval falls in two weeks.
        if ($the_first_day_of_week <= $the_last_day_of_week) {
            if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week) {
                $no_remaining_days--;
            }
            if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week) {
                $no_remaining_days--;
            }
        } else {
            // (edit by Tokes to fix an edge case where the start day was a Sunday
            // and the end day was NOT a Saturday)

            // the day of the week for start is later than the day of the week for end
            if ($the_first_day_of_week == 7) {
                // if the start date is a Sunday, then we definitely subtract 1 day
                $no_remaining_days--;

                if ($the_last_day_of_week == 6) {
                    // if the end date is a Saturday, then we subtract another day
                    $no_remaining_days--;
                }
            } else {
                // the start date was a Saturday (or earlier), and the end date was (Mon..Fri)
                // so we skip an entire weekend and subtract 2 days
                $no_remaining_days -= 2;
            }
        }

        //The no. of business days is: (number of weeks between the two dates) * (5 working days) + the remainder
        //---->february in none leap years gave a remainder of 0 but still calculated weekends between first and last day, this is one         way to fix it
        $workingDays = $no_full_weeks * 5;
        if ($no_remaining_days > 0) {
            $workingDays += $no_remaining_days;
        }

        return $workingDays - $holidayCount;
    }

    /**
     * Checks For paid leave eligibility i.e.
     * Paid leave should be applied atleast 15 days before Leave Start.
     *
     * @param $startDate => string(date)
     * @param $endDate => string(date)
     *
     * @return boolean
     */
    public static function isPersonalLeaveEligible($startDate, $endDate, $requestedDays = 0)
    {

        //Number of days before a paid leave can be applied
        $paidLeavesInAdvance = SystemSetting::where('key', 'paid_leave_in_advance')->first();
        $paidLeavesInAdvance = $paidLeavesInAdvance ? $paidLeavesInAdvance : 15;
        $today = date('Y-m-d', strtotime("now"));
        $currentDate = date_create($today);
        $startDate = date('Y-m-d', strtotime($startDate));
        $startDate = date_create($startDate);
        $dateDifference = date_diff($currentDate, $startDate);
        $dateDifference = $dateDifference->days;

        if ($requestedDays == 1 && $dateDifference < 2) {
            return false;
        } elseif ($requestedDays == 2 && $dateDifference < 4) {
            return false;
        } elseif ($requestedDays == 3 && $dateDifference < 6) {
            return false;
        } elseif ($requestedDays > 3 && $dateDifference < $paidLeavesInAdvance) {
            return false;
        } else {
            return true;
        }
    }

    /**  Not Is Use (NIU)
     * Checks for no. of leaves applied
     *
     * @param $startDate => string(date)
     * @param $endDate => string(date)
     *
     * @return boolean
     */
    public static function isLeaveLengthValid($startDate, $endDate)
    {
        //Maximum number of consecutive leaves a user can take
        $maxConsecutiveLeaves = SystemSetting::where('key', 'max_consecutive_leaves')->first();
        $maxConsecutiveLeaves = $maxConsecutiveLeaves ? $maxConsecutiveLeaves->value : 14;

        $numberOfDays = NewLeaveService::calculateTotalNumberOfDays($startDate, $endDate);
        if ($numberOfDays > $maxConsecutiveLeaves) {
            return false;
        }

        return true;
    }

    public static function getUpcomingYearEnd($date)
    {
        $month = date('m', strtotime($date));
        if ($month <= 03) {
            return date('Y', strtotime($date)) . "-03-31";
        } else {
            return (date('Y', strtotime($date)) + 1) . "-03-31";
        }
    }

    public static function getLastApril($date)
    {
        $month = date('m', strtotime($date));
        if ($month <= 03) {
            return (date('Y', strtotime($date)) - 1) . "-04-01";
        } else {
            return (date('Y', strtotime($date))) . "-04-01";
        }
    }

    /**
     * Calculate leaves.If the leave is in decimal format then truncate accordingly
     *
     * @param $totalLeaves => number
     * @param $numberOfMonths => number
     *
     * @return number
     */
    public static function calculateLeaves($totalLeaves, $numberOfMonths)
    {
        $leavesPerMonth = $totalLeaves / 12;
        $leavesEligible = $leavesPerMonth * $numberOfMonths;

        //Truncate to nearest .5
        if (fmod($leavesEligible, 1) < .5) {
            $leavesEligible = floor($leavesEligible);
        } else {
            $leavesEligible = floor($leavesEligible) + 1;
        }
        return $leavesEligible;
    }

    /**
     * Check if start date is greater than end date
     *
     * @param $startDate => date string
     * @param $endDate => date string
     *
     * @return boolean
     */
    public static function isLeaveValid($startDate, $endDate)
    {
        $startDate = date("Y-m-d", strtotime($startDate));
        $endDate = date("Y-m-d", strtotime($endDate));

        //If start date comes after end date
        if ($startDate > $endDate) {
            return false;
        }

        return true;
    }

    /**
     * Check if leave is applied for current working year(1st April to 31st March)
     *
     * @param $startDate => date string
     * @param $endDate => date string
     * @param $role =>  string
     *
     * @return boolean
     */
    public static function isLeaveAppliedForCurrentWorkingYear($startDate, $endDate, $role = "user")
    {
        $maximumDate = NewLeaveService::getUpcomingYearEnd(date('Y-m-d'));
        //User can apply leave in between current date to upcoming March
        // if ($role == 'user') {
        //     $minimumDate = date('Y-m-d', strtotime("now"));
        //     $startDate = date("Y-m-d", strtotime($startDate));
        //     $endDate = date("Y-m-d", strtotime($endDate));
        //     if ($startDate < $minimumDate) {
        //         return false;
        //     }

        //     if (($startDate > $maximumDate) || ($endDate > $maximumDate)) {
        //         return false;
        //     }

        //     return true;
        // }

        // //Admin can apply leave in between Last April to upcoming March
        // if ($role == 'admin') {
        $minimumDate = NewLeaveService::getLastApril(date('Y-m-d'));
        $startDate = date("Y-m-d", strtotime($startDate));
        $endDate = date("Y-m-d", strtotime($endDate));
        if ($startDate < $minimumDate) {
            return false;
        }

        if (($startDate > $maximumDate) || ($endDate > $maximumDate)) {
            return false;
        }

        return true;
        //}
    }

    /**
     * Check if leave already exists for given date or overlaps
     *
     * @param $startDate => date string
     * @param $endDate => date string
     *
     * @return boolean
     */
    public static function isLeaveOverlapping($startDate, $endDate, $user)
    {
        // return true;        //disables leave overlap check
        $pstart = date("Y-m-d", strtotime($startDate . " -26 day"));
        $pend = date("Y-m-d", strtotime($endDate . " +26 day"));
        $days = NewLeaveService::calculateWorkingDays($pstart, $startDate, $user->id); //userid
        while ($days < 20) {
            $pstart = date("Y-m-d", strtotime($pstart . " -1 day"));

            $days = NewLeaveService::calculateWorkingDays($pstart, $startDate, $user->id); //userid
        }
        $days = NewLeaveService::calculateWorkingDays($endDate, $pend, $user->id); //userid
        while ($days < 20) {
            $pend = date("Y-m-d", strtotime($pend . " +1 day"));

            $days = NewLeaveService::calculateWorkingDays($endDate, $pend, $user->id); //userid
        }
        $startDate = $pstart;
        $endDate = $pend;
        $leaveOverlapCount = 0;

        //If any leave overlaps inbetween start and end date
        $leaveOverlapCount = Leave::where('user_id', $user->id)->whereDate('start_date', '>=', $startDate)
            ->whereDate('start_date', '<=', $endDate)
            ->whereIn('status', ['approved', 'pending'])
            ->whereIn('type', ['paid', 'sick'])
            ->count();
        if ($leaveOverlapCount > 0) {
            return false;
        }

        $leaveOverlapCount = Leave::where('user_id', $user->id)->whereDate('end_date', '>=', $startDate)
            ->whereDate('end_date', '<=', $endDate)
            ->whereIn('status', ['approved', 'pending'])
            ->whereIn('type', ['paid', 'sick'])
            ->count();
        if ($leaveOverlapCount > 0) {
            return false;
        }

        return true;
    }

    /**
     * Filters Leave
     *
     * @param $user => user object
     *
     * @return boolean
     */
    public static function filterLeave($input)
    {
        $leaveList = Leave::with('user');

        $today = date("Y-m-d");
        $after7days = date('Y-m-d', strtotime('7 days', strtotime($today)));

        if (!empty($input['user'])) {
            $leaveList->where('user_id', $input['user']);
        }

        if (!empty($input['start_date'])) {
            $fromDate = date("Y-m-d", strtotime($input['start_date']));
        } else {
            $fromDate = $today;
        }

        if (!empty($input['end_date'])) {
            $toDate = date("Y-m-d", strtotime($input['end_date']));
        } else {
            $toDate = $after7days;
        }

        $leaveList = $leaveList->where('start_date', '>=', $fromDate);
        $leaveList = $leaveList->where('end_date', '<=', $toDate);

        $leaveList = $leaveList->orderBy('id', 'DESC')->paginate(10);
        return $leaveList;
    }
    public static function verifyLeave($user, $leave)
    {
        $response = array('status' => true, 'message' => "success", 'id' => null);
        if ($leave->type == 'paid') {
            if ($leave->days > NewLeaveService::getUserAvailableLeave($user->id, 'paid')) {
                $response['status'] = false;
                $response['errors'] = "Not enough paid leaves left";
                return $response;
            }
            if ($leave->days > 3) {
                $noticeLength = Carbon::parse($leave->start_date)->diffInDays(Carbon::now()) + 1;
                if ($noticeLength < 15) {
                    $response['status'] = false;
                    $response['errors'] = "Request should be made atleast 15 days before leave start date";
                    return $response;
                }
            } else {
                $noticeLength = Carbon::parse($leave->start_date)->diffInDays(Carbon::now()) + 1;
                if ($noticeLength < 2 * ($leave->days)) {
                    $response['status'] = false;
                    $response['errors'] = "Request should be made atleast two days prior against every day of leave";
                    return $response;
                }
            }
        } elseif ($leave->type == 'sick' && $leave->days > NewLeaveService::getUserAvailableLeave($user->id, 'sick')) {
            $response['status'] = false;
            $response['errors'] = "Not enough sick leaves left";
            return $response;
        }
    }
    public static function getWorkingDay($date)
    {
        $day = date('N', strtotime($date));
        if ($day < 6) {
            $holiday = Calendar::whereDate('date', $startDate)->get();
            if (empty($holiday)) {
                return date("d-M-Y H:i:s", strtotime($date));
            } else {
                if ($day == 1) {
                    $date = date("d M Y", strtotime("-3 day", strtotime($date)));
                } else {
                    $date = date("d M Y", strtotime("-1 day", strtotime($date)));
                }
            }
        } else {
            if ($day == 6) {
                $date = date("d M Y", strtotime("-1 day", strtotime($date)));
            } else {
                $date = date("d M Y", strtotime("-2 day", strtotime($date)));
            }
        }
    }
    public static function getTotalLeaves($start_date, $end_date, $user_id, $type)
    {
        $days = Leave::where('user_id', $user_id)->where(function ($query) use ($start_date, $end_date) {
            $query->where('start_date', '<=', $end_date)->orWhere('end_date', '<=', $start_date);
        })->where('status', 'approved');
        if ($type != 'all') {
            $days = $days->where('type', $type);
        }
        $days = $days->sum('days');
        return $days;
    }

    /*
    function : checkOverlappingLeave
    check if the emaployee leave overlaps with their collegue working on same project in a team.
    response :
     */
    public static function checkOverlappingLeave($userId, $startDate, $endDate, $leaveId = null)
    {

        $user = User::find($userId);
        $startDate = date_to_yyyymmdd($startDate);
        $endDate = date_to_yyyymmdd($endDate);
        $projects = $user->projects()->with('resources')->where('project_resources.start_date', '<=', $endDate)
            ->where(function ($query) use ($startDate, $endDate) {
                $query->where('project_resources.end_date', null)
                    ->orWhere('project_resources.end_date', '>=', $startDate);
            })->get();
        $data = [];
        $max = date_diff(date_create($startDate), date_create($endDate))->format('%a');
        $dates = [];
        $code = [];
        $holidays = Calendar::whereDate('date', '>=', $startDate)->whereDate('date', '<=', $endDate)->pluck('date')->toArray();
        $date = $startDate;
        for ($i = 0; $i <= $max; $i++) {
            $dates[] = date('D j M', strtotime($date));
            if (date('N', strtotime($date)) > 5) {
                $code[] = 1;
            } else {
                if (in_array($date, $holidays)) {
                    $code[] = 1;
                } else {
                    $code[] = 0;
                }
            }
            $date = date('Y-m-d', strtotime($date . ' +1 day'));
        }
        $count = 0;
        foreach ($projects as $project) {
            $coworkers = $project->resources()->with('leaves')->whereNotIn('project_resources.user_id', [$userId])
                ->where('start_date', '<=', $endDate)
                ->where(function ($query) use ($startDate, $endDate) {
                    $query->where('end_date', null)
                        ->orWhere('end_date', '>=', $startDate);
                })->get();
            $dataArray = [];
            $f = 0;
            if (!empty($coworkers)) {
                foreach ($coworkers as $coworker) {
                    $userDates = array_fill(0, count($dates), 0);
                    $flag = 0;
                    $leaves = $coworker->leaves()->where('leaves.start_date', '<=', $endDate)
                        ->where('leaves.end_date', '>=', $startDate)
                        ->whereIn('leaves.status', ['pending', 'approved'])->get();
                    if (!empty($leaves) && count($leaves) > 0) {
                        $flag = 1;
                        foreach ($leaves as $leave) {
                            $start = strtotime($leave->start_date) > strtotime($startDate) ?
                            date_diff(date_create($leave->start_date), date_create($startDate))->format('%a') :
                            0;
                            $end = strtotime($endDate) > strtotime($leave->end_date) ?
                            date_diff(date_create($leave->end_date), date_create($startDate))->format('%a') :
                            count($dates) - 1;
                            for ($i = $start; $i <= $end; $i++) {
                                $userDates[$i] = ucfirst($leave->type);
                            }
                        }
                    }
                    if ($flag == 1) {
                        $f = 1;
                        $leaveDetails = $coworker->leaves()->where('leaves.start_date', '<=', $endDate)
                            ->where('leaves.end_date', '>=', $startDate)->whereIn('leaves.status', ['pending', 'approved'])
                            ->first();
                        $name = $coworker->name;
                        $dataArray[] = ['name' => $name, 'dates' => $userDates, 'leave' => $leaveDetails];
                    }
                }
                if ($f == 1) {
                    $count++;
                    $name = $project->project_name;
                    $overlap_reason = '';
                    if ($leaveId != null) {
                        $overlap = LeaveOverlapRequest::where('project_id', $project->id)
                            ->where('leave_id', $leaveId)->where('user_id', $userId)->first();
                        $overlap_reason = !empty($overlap) ? $overlap->reason : '';
                    }
                    $data[] = ['name' => $name, 'users' => $dataArray, 'id' => $project->id, 'overlap_reason' => $overlap_reason];
                }
            }
        }
        return ['projects' => $data, 'count' => $count, 'dates' => $dates, 'code' => $code];
    }

    public static function saveData($request, $role = "user")
    {
        try {
            // DB::beginTransaction();
            $loggedUser = Auth::user();
            if (!($loggedUser->hasRole('admin') || $loggedUser->hasRole('human-resources'))) {
                $request['status'] = 'pending';
            }
            $response = array('status' => true, 'message' => "success", 'id' => null);
            $action_date = date("Y-m-d");
            $leave = new Leave();
            $leave->user_id = $request['user_id'];

            if ($request['start_date']) {
                $leave->start_date = date("Y-m-d", strtotime($request['start_date']));
            }
            if ($request['end_date']) {
                $leave->end_date = date("Y-m-d", strtotime($request['end_date']));
            }

            $leave->status = !empty($request['status']) ? $request['status'] : "pending";
            $leave->reason = isset($request['reason']) ? $request['reason'] : '';

            $leave->leave_type_id = isset($request['type']) ? $request['type'] : null;
            \Log::info('LEAVE' . json_encode($request));
            if ($leave->status == 'approved') {
                $approver_id = \Auth::id();
            }
            $user = User::find($leave->user_id);

            $leave->days = NewLeaveService::calculateWorkingDays($request['start_date'], $request['end_date'], $user->id); //userid
            $leaveTypeObj = LeaveType::find($leave->leave_type_id);
            if (!$leaveTypeObj) {
                $response['status'] = false;
                $response['message'] = 'Invalid leave type';
                return $response;

            }
            if ($leaveTypeObj->code == 'sick' && $leave->days == 1) {
                $half = !empty($request['half']) ? $request['half'] : null;
                if ($half && $half != 'full') {
                    $leave->half = $half;
                    $leave->days = 0.5;
                }
            }
            if ($role == 'user') {
                $requestLeaveType = LeaveType::find($request['type']);
                if (!$requestLeaveType) {
                    $response['status'] = false;
                    $response['message'] = 'Leave Type not found';
                    return $response;
                }
                if (empty($user->confirmation_date)) {
                    $response['status'] = false;
                    $response['message'] = 'Leave cannot be applied during probation period.';
                    return $response;
                }
                if ($requestLeaveType->code == 'paid') {
                    $response = NewLeaveService::validatePaidLeave($user->id, $request['start_date'], $request['end_date'], $requestLeaveType->id);
                    if (!$response['status']) {
                        return $response;
                    }
                }
                // $response = Leave::ValidateLeave($user->id, $request, 0, $role);
                // if ($response['status'] == false) {
                //     return $response;
                // }
            }
            if ($leave->save()) {
                if ($leave->status == "pending") {
                    $resources = ProjectResource::where('user_id', $leave->user->id)->where('start_date', '<=', $leave->end_date)->where(function ($query) use ($leave) {
                        $query->where('end_date', null)->orWhere('end_date', '>=', $leave->start_date);
                    })->pluck('project_id')->toArray();
                    $projects = Project::with('projectManager')->whereIn('id', $resources)->get();
                    $array = [];
                    foreach ($projects as $project) {
                        if (empty($project->projectManager)) {
                            // $response['status'] = false;
                            // $response['errors'] =  'Project Manager not assigned';
                            // return $response;
                            continue;
                        }
                        if (in_array($project->projectManager->id, $array)) {
                            continue;
                        }
                        if ($leave->user->id == $project->projectManager->id || $leave->user->parent_id == $project->projectManager->id) {
                            continue;
                        }
                        $approval = new ProjectManagerLeaveApproval();
                        $approval->user_id = $project->projectManager->id;
                        $approval->leave_id = $leave->id;
                        $approval->status = $leave->status;
                        $approval->save();
                        $array[] = $project->projectManager->id;
                    }
                    if (!empty($request['overlap_reasons'])) {
                        self::saveOverlappingDetails($request['overlap_reasons'], $leave->id, $leave->user_id);
                    }
                    $response['id'] = $leave->id;
                    $response['leave'] = $leave;
                    // Leave::notifyLeaveApplicationToAdmin($leave->id);

                } else {
                    $response['id'] = $leave->id;
                    $response['leave'] = $leave;
                    self::approveLeave($leave->id, $approver_id, $action_date);

                    //Leave::notifyLeaveAction($leave->id);
                }
            } else {
                $response['status'] = false;
                $response['errors'] = $leave->getErrors();
                return $response;
            }
            if ($leave->status == 'pending') {
                event(new LeaveApplied($leave->id));
            }
            return $response;
            // DB::commit();
        } catch (Exception $e) {
            \Log::info($e);

            // DB::rollback();
        }
    }
    public static function saveOverlappingDetails($overlap_reasons, $leave_id, $user_id)
    {
        foreach ($overlap_reasons as $overlap_reason) {
            try {
                // DB::beginTransaction();
                $leaveOverlapObj = new LeaveOverlapRequest();
                $leaveOverlapObj->user_id = $user_id;
                $leaveOverlapObj->leave_id = $leave_id;
                $leaveOverlapObj->project_id = $overlap_reason['project_id'];
                $leaveOverlapObj->reason = !empty($overlap_reason['comment']) ? $overlap_reason['comment'] : null;
                if (!$leaveOverlapObj->save()) {
                    \Log::info(json_encode($leaveOverlapObj->getErrors()));
                }
                // DB::commit();
            } catch (Exception $e) {
                \Log::info($e);
                // DB::rollback();
            }
        }
    }

    public static function teamLeadStatuses($leave_id)
    {
        $leave = Leave::with('user', 'projectManagerApprovals')->where('id', $leave_id)->first();
        $managerInfo = [];
        $projectIds = ProjectResource::getActiveProjectIdArray($leave->user->id, $leave->start_date, $leave->end_date);
        if (!empty($leave->projectManagerApprovals) && !empty($projectIds)) {
            foreach ($leave->projectManagerApprovals as $item) {
                if (!empty($item->manager)) {
                    $projects = Project::where('project_manager_id', $item->user_id)->whereIn('id', $projectIds)->pluck('project_name')->toArray();
                    $projects = implode(" | ", $projects);
                    if (empty($projects)) {
                        $projects = "No active projects";
                    }
                    $managerInfo[] = array('name' => $item->manager->name, 'id' => $item->manager->id, 'status' => $item->status, 'projects' => $projects, 'date' => date_in_view($item->updated_at));
                }
            }
        }
        return $managerInfo;
    }
    public static function previousLeaves($userId, $year)
    {
        $data = Leave::where('user_id', $userId)->where('end_date', '>=', $year)->where('status', 'approved')->orderBy('start_date', 'desc')->get();
        return $data;
    }

    public static function sickLeaves($userId, $year)
    {
        $data = Leave::where('user_id', $userId)->where('end_date', '>=', $year)->where('start_date', '<=', date('Y-m-d', strtotime('+1 year', strtotime($year))))->where('type', 'sick')->where('status', 'approved')->sum('days');
        return $data;
    }

    public static function earnedLeaves($userId, $year)
    {
        $data = Leave::where('user_id', $userId)->where('end_date', '>=', $year)->where('start_date', '<=', date('Y-m-d', strtotime('+1 year', strtotime($year))))->where('type', 'paid')->where('status', 'approved')->sum('days');
        return $data;
    }

    public static function getTeamLeads($leaveId)
    {
        $data = [];
        $leave = Leave::find($leaveId);
        $teamLeadsArray = $leave->projectManagerApprovals()->pluck('user_id')->toArray();
        $data = User::whereIn('id', $teamLeadsArray)->get();
        return $data;
    }

    public static function getProjectsManagers($leaveId)
    {
        $projectManagers = [];
        $projectManager = [];
        $leave = Leave::find($leaveId);
        if (empty($leave)) {
            return [];
        }
        $projectResources = ProjectResource::with('project')->where('user_id', $leave->user_id)
            ->where('start_date', '<=', $leave->end_date)
            ->where(function ($query) use ($leave) {
                $query->where('end_date', null)
                    ->orWhere('end_date', '>=', $leave->start_date);
            })->get();

        if (!empty($projectResources)) {
            foreach ($projectResources as $projectResource) {
                $projectManager['name'] = !empty($projectResource->project->projectManager->name) ? $projectResource->project->projectManager->name : '';
                +$projectManager['email'] = !empty($projectResource->project->projectManager->email) ? $projectResource->project->projectManager->email : '';
                $projectManagers[] = $projectManager;
                $projectManager = [];
            }
        }
        return $projectManagers;
    }

    public static function approveLeave($leave_id, $approver_id, $action_date)
    {
        $leave = Leave::find($leave_id);
        if ($leave) {
            $leave->approver_id = $approver_id;
            $leave->action_date = $action_date;
            $leave->status = "approved";
            if ($leave->save()) {
                event(new LeaveApproved($leave->id));
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static function cancelLeave($leave_id, $approver_id, $action_date)
    {
        $leave = Leave::find($leave_id);
        if ($leave) {
            $leave->approver_id = $approver_id;
            $leave->action_date = $action_date;
            $leave->status = "cancelled";
            if ($leave->save()) {
                event(new LeaveCancelled($leave->id));
                self::deleteLeaveDeductions($leave->id);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    public static function rejectLeave($leave_id, $approver_id, $action_date)
    {
        $leave = Leave::find($leave_id);
        if ($leave) {
            $leave->approver_id = $approver_id;
            $leave->action_date = $action_date;
            $leave->status = "rejected";
            if ($leave->save()) {
                event(new LeaveRejected($leave->id));
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static function isTeamLeadWithStatus($user_id, $teamLeadsArray)
    {
        $response = [];
        $response['is_team_lead'] = false;
        foreach ($teamLeadsArray as $teamLead) {

            if ($user_id == $teamLead['id']) {
                $response['is_team_lead'] = true;
                $response['lead_status'] = !empty($teamLead['status']) ? $teamLead['status'] : '';
            }
        }
        return $response;
    }

    public static function isTeamLeadbyLeaveId($user_id, $leave_id)
    {
        $flag = false;
        $leave = Leave::with('user', 'projectManagerApprovals')->where('id', $leave_id)->first();

        if (!empty($leave->projectManagerApprovals)) {
            foreach ($leave->projectManagerApprovals as $item) {
                if ($item->user_id == $user_id) {
                    $flag = true;
                }
            }
        }
        return $flag;
    }

    public static function validateLeaveCancel($leave_id)
    {
        $message = '';
        $status = true;
        $response = [];
        $leave = Leave::find($leave_id);
        if (!$leave) {
            $status = false;
            $message = 'Leave not found';
        } else {
            if ($leave->status == 'rejected' || $leave->status == 'cancelled') {
                $status = false;
                $message = 'Leave is no longer in pending or approved state and hence can not be cancelled';
            } else {
                $user = Auth::user();
                if (!$user) {
                    $status = false;
                    $message = 'Invalid access';
                } else {
                    // HR AND ADMIN
                    if ($user->hasRole('admin') || $user->hasRole('human-resources')) {
                        $response['status'] = $status;
                        $response['message'] = $message;
                        return $response;
                    }
                    // RM
                    elseif ($user->hasRole('reporting-manager')) {
                        // Check for other leave types and applicable roles
                        if ($leave->leaveType->code != 'paid' && $leave->leaveType->code != 'sick') {
                            if (!$user->hasRole('admin') && !$user->hasRole('human-resources')) {
                                $status = false;
                                $message = "Only management team can cancel this leave request";
                            }
                        }
                        if (!($user->hasRole('admin')) && $user->hasRole('reporting-manager') && $user->id != $leave->user_id) {
                            $reporting_manager = UserService::getReportingManager($leave->user_id);
                            if ($user->id != $reporting_manager['id']) {
                                $status = false;
                                $message = 'Unauthorized Request';
                            }
                        }
                    }
                    // USER
                    elseif ($user->hasRole('user')) {
                        $current_date = date('Y-m-d');
                        // LEAVE START DATE CHECK
                        if (strtotime($leave->start_date) < strtotime($current_date)) {
                            $status = false;
                            $message = 'Leave cannot be cancelled for today';
                        }
                        if ($leave->user_id != $user->id) {
                            $status = false;
                            $message = 'Invalid Request';
                        }

                    } else {
                        $status = false;
                        $message = 'Unauthorized Access';
                    }
                }
            }
        }
        $response['status'] = $status;
        $response['message'] = $message;
        return $response;
    }

    public static function sendReminder($lead_id, $user_name, $leave_id, $display_text)
    {
        $leaveObj = Leave::find($leave_id);
        if (!$leaveObj) {
            return;
        }

        $message = View::make('messaging-app.slack.leave.notify', ['user_name' => $user_name, 'leaveObj' => $leaveObj, 'display_text' => $display_text])->render();
        $attachment = null;
        $job = (new PostMessageUserJob($lead_id, $message, $attachment));
        dispatch($job);

    }

    public static function validatePaidLeave($user_id, $start_date, $end_date, $leave_type_id)
    {
        $status = true;
        $message = '';
        $duration = self::calculateWorkingDays($start_date, $end_date, $user_id);

        // Duration check
        if ($duration > 10) {
            $status = false;
            $message = 'More than 10 paid leaves can not be applied at a single instance.';
            $response['status'] = $status;
            $response['message'] = $message;
            return $response;
        }
        // Check for leave Balance
        $year = date("Y");
        $calendarYear = CalendarYear::where('year', $year)->first();
        $from_date = $year . '-01-01';
        $to_date = $year . '12-31';
        $count1 = 0;
        $count2 = 0;
        $daysOfLeavesTaken = Leave::where('user_id', $user_id)->whereDate('start_date', '>=', $from_date)->whereDate('end_date', '<=', $to_date)
            ->where('leave_type_id', $leave_type_id)->where('status', 'approved')->sum('days');
        $previousToThisYearLeave = Leave::where('user_id', $user_id)->whereDate('start_date', '<', $from_date)->whereDate('end_date', '>=', $from_date)->first();
        $thisToNextYearLeave = Leave::where('user_id', $user_id)->whereDate('start_date', '<=', $to_date)->whereDate('end_date', '>', $to_date)->first();
        if ($previousToThisYearLeave) {
            $count1 = self::calculateWorkingDays($from_date, $previousToThisYearLeave->end_date, $user_id);
        }
        if ($thisToNextYearLeave) {

            $count2 = self::calculateWorkingDays($thisToNextYearLeave->start_date, $to_date, $user_id);
        }

        $daysOfLeavesTaken = $daysOfLeavesTaken + $count1 + $count2;
        $availableLeaves = UserLeaveBalance::where('user_id', $user_id)->where('calendar_year_id', $calendarYear->id)->first();
        if (($availableLeaves->paid_leave_balance - $daysOfLeavesTaken - $duration) < 0) {
            $status = false;
            $message = 'Not Enough leaves left';
            $response['status'] = $status;
            $response['message'] = $message;
            return $response;
        }
        // Check for any leaves with end_date in last 20 days
        $days = 20;
        $endDate = strtotime($end_date);
        $startDate = strtotime($start_date);
        for ($i = $days; $i > 0; $i--) {
            $checkDate = date('Y-m-d', strtotime('-' . $i . 'days', $endDate));
            $leaveCount = Leave::where('user_id', $user_id)->whereDate('end_date', $checkDate)->where('status', 'approved')
                ->where('leave_type_id', $leave_type_id)->count();
            if ($leaveCount > 0) {
                $status = false;
                $message = 'Paid Leave was applied in the last 20 days.';
                $response['status'] = $status;
                $response['message'] = $message;
                return $response;
            }
        }
        // check for any leaves with start_date in next 20 days
        for ($i = 1; $i <= $days; $i++) {
            $checkDate = date('Y-m-d', strtotime('+' . $i . 'days', $startDate));
            $leaveCount = Leave::where('user_id', $user_id)->whereDate('end_date', $checkDate)->whereIn('status', ['approved', 'pending'])
                ->where('leave_type_id', $leave_type_id)->count();
            if ($leaveCount > 0) {
                $status = false;
                $message = 'Paid Leave is already applied in the next 20 days.';
                $response['status'] = $status;
                $response['message'] = $message;
                return $response;
            }
        }

        if ($duration <= 3) {
            $checkDate = date('Y-m-d', strtotime('+3 days'));
            if ($start_date < $checkDate) {
                $status = false;
                $message = 'Paid Leave for less than 3 days should be applied 3 days in advance';
                $response['status'] = $status;
                $response['message'] = $message;
                return $response;
            }
        } else {
            $checkDate = date('Y-m-d', strtotime('+15 days'));
            if ($start_date < $checkDate) {
                $status = false;
                $message = 'Paid Leave for more 3 days should be applied 15 days in advance';
                $response['status'] = $status;
                $response['message'] = $message;
                return $response;
            }
        }

        $response['status'] = $status;
        $response['message'] = $message;
        return $response;
    }

    public static function updateLeaveDeductions($leave_id, $deduction_type, $duration, $status)
    {
        // Check if row already exists
        \Log::info('updateLeaveDeductions' . $deduction_type);
        $leaveDeductionObj = LeaveDeduction::where('leave_id', $leave_id)->first();
        if (!$leaveDeductionObj) {
            $leaveDeductionObj = new LeaveDeduction();
            $leaveDeductionObj->leave_id = $leave_id;
        }

        $leaveDeductionObj->loss_of_pay = 0;
        $leaveDeductionObj->paid_leave = 0;
        $leaveDeductionObj->sick_leave = 0;
        $leaveDeductionObj->other_paid = 0;

        switch ($deduction_type) {
            case "lop":
                $leaveDeductionObj->loss_of_pay = $duration;
                break;
            case "sl":
                $leaveDeductionObj->sick_leave = $duration;
                break;
            case "pl":
                $leaveDeductionObj->paid_leave = $duration;
                break;
            case "slpl":
                $leaveDeductionObj->sick_leave = $duration;
                break;
            case "other_paid":
                $leaveDeductionObj->other_paid = $duration;
                break;
            default:
                $leaveDeductionObj->loss_of_pay = 0;
                $leaveDeductionObj->paid_leave = 0;
                $leaveDeductionObj->sick_leave = 0;
                $leaveDeductionObj->other_paid = 0;

        }
        \Log::info('updateLeaveDeductions' . json_encode($leaveDeductionObj));
        $leaveDeductionObj->save();
    }

    public static function getAnnualCarryForward($user_id, $year)
    {
        $total = 0;
        $userObj = User::find($user_id);
        $previousYear = $year - 1;
        $previousYearObj = CalendarYear::where('year', $year)->first();
        if ($previousYearObj) {
            $checkDate = $year . '-01-01';
            if ($userObj->confirmation_date && (strtotime($userObj->confirmation_date) < strtotime($checkDate))) {
                $totalPaidLeaves = self::getAllowedLeave($user_id, 'paid', $previousYear);
                $consumedPaidLeaves = self::getUserLeaveTaken($user_id, 'paid', $previousYear);
                $total = $totalPaidLeaves - $consumedPaidLeaves;
            }
        }
        if ($total < 0) {
            $total = 0;
        }
        return $total;

    }

    public static function deleteLeaveDeductions($leave_id)
    {
        $leaveObj = Leave::find($leave_id);
        if ($leaveObj && $leaveObj->status == 'cancelled') {
            $leaveDeductionObj = LeaveDeduction::where('leave_id', $leave_id)->first();
            if ($leaveDeductionObj) {
                $leaveDeductionObj->delete();
            }
        }
    }

    public static function getUserLeavesInfo($user_id)
    {
        // $year = date("Y");
        // $maxSickLeaves = self::getAllowedLeave($user_id, 'sick');
        // $sickLeavesTaken = self::getUserLeaveTaken($user_id, 'sick');
        // $maxPaidLeaves = self::getAllowedLeave($user_id, 'paid');
        // $paidLeavesTaken = self::getUserLeaveTaken($user_id, 'paid');
        $carryForward = self::getCarryForwardLeaves($user_id);

        // $response['maxSickLeaves'] = $maxSickLeaves;
        // $response['sickLeavesTaken'] = $sickLeavesTaken;
        // $response['maxPaidLeaves'] = $maxPaidLeaves;
        // $response['paidLeavesTaken'] = $paidLeavesTaken;
        // $response['carryForward'] = $carryForward;

        $consumed = UserLeaveService::getUserConsumedLeaveBalance($user_id);
        // $available = UserLeaveService::getUserRemainingLeaves($user_id);
        $allowed = UserLeaveService::getUserAllowedLeaveBalance($user_id);

        // $avaliableSickLeaves = $available['sick']; //UserLeaveService::getUserRemainingLeave($user->id, 'sick');
        $totalSickLeaves = $allowed['sick']; //UserLeaveService::getAllowedLeave($user->id, 'sick');
        // $avaliablePaidLeaves = $available['paid']; //UserLeaveService::getUserRemainingLeave($user->id, 'paid');
        $totalPaidLeaves = $allowed['paid']; //UserLeaveService::getAllowedLeave($user->id, 'paid');

        $consumedSickLeaves = (-1) * $consumed['sick']; //UserLeaveService::getUserRemainingLeave($user->id, 'sick');
        $consumedPaidLeaves = (-1) * $consumed['paid']; //UserLeaveService::getUserRemainingLeave($user->id, 'paid');

        // $consumedSickLeaves = $totalSickLeaves - $avaliableSickLeaves;
        // $consumedPaidLeaves = $totalPaidLeaves - $avaliablePaidLeaves;

        $response['maxSickLeaves'] = $totalSickLeaves;
        $response['sickLeavesTaken'] = $consumedSickLeaves;
        $response['maxPaidLeaves'] = $totalPaidLeaves;
        $response['paidLeavesTaken'] = $consumedPaidLeaves;
        $response['carryForward'] = $carryForward;

        return $response;
    }

    // getAllowedLeave replaces maxSickLeave, maxPersonalLeave, getAnnualPaidLeaves, getAnnualSickLeaves functions
    // User Leave Balance needs to be updated
    public static function getAllowedLeave($userId, $leaveTypeCode, $year = null)
    {
        $total = 0;
        $year = $year ?: date('Y');
        $calendaYearObj = CalendarYear::where('year', $year)->first();
        if ($calendaYearObj) {
            $userLeaveBalancesObj = UserLeaveBalance::where('user_id', $userId)->where('calendar_year_id', $calendaYearObj->id)->first();
            if ($userLeaveBalancesObj) {
                if ($leaveTypeCode == 'sick') {
                    $total = $userLeaveBalancesObj->sick_leave_balance;
                } else if ($leaveTypeCode == 'paid') {
                    $total = $userLeaveBalancesObj->paid_leave_balance;
                }
            }
        }
        return $total;
    }

    // getUserLeaveTaken replaces getPaidLeaveCount, getSickLeaveCount, getPaidLeaveCountYear functions
    public static function getUserLeaveTaken($userId, $leaveTypeCode, $year = null)
    {
        $leaveTypeObj = LeaveType::where('code', $leaveTypeCode)->first();
        $year = $year ?: date('Y');
        $from_date = $year . '-01-01';
        $to_date = $year . '-12-31';
        $count1 = 0;
        $count2 = 0;
        $daysOfLeavesTaken = Leave::where('user_id', $userId)->whereDate('start_date', '>=', $from_date)->whereDate('end_date', '<=', $to_date)
            ->where('leave_type_id', $leaveTypeObj->id)->where('status', 'approved')->sum('days');
        $previousToThisYearLeave = Leave::where('user_id', $userId)->whereDate('start_date', '<', $from_date)->whereDate('end_date', '>=', $from_date)
            ->where('leave_type_id', $leaveTypeObj->id)->where('status', 'approved')->first();
        $thisToNextYearLeave = Leave::where('user_id', $userId)->whereDate('start_date', '<=', $to_date)->whereDate('end_date', '>', $to_date)
            ->where('leave_type_id', $leaveTypeObj->id)->where('status', 'approved')->first();
        if ($previousToThisYearLeave) {
            $count1 = self::calculateWorkingDays($from_date, $previousToThisYearLeave->end_date, $userId);
        }
        if ($thisToNextYearLeave) {

            $count2 = self::calculateWorkingDays($thisToNextYearLeave->start_date, $to_date, $userId);
        }

        $daysOfLeavesTaken = $daysOfLeavesTaken + $count1 + $count2;
        return $daysOfLeavesTaken;
    }

    // getUserAvailableLeave replaces getRemainingSickLeave, getRemainingPaidLeave, getAvailableSickLeaves, getAvailablePaidLeaves functions
    // User Leave Balance needs to be updated
    public static function getUserAvailableLeave($userId, $leaveTypeCode, $year = null)
    {
        $year = $year ?: date('Y');
        $totalPaidLeaves = self::getAllowedLeave($userId, $leaveTypeCode, $year);
        $consumedPaidLeaves = self::getUserLeaveTaken($userId, $leaveTypeCode, $year);
        return ($totalPaidLeaves - $consumedPaidLeaves);
    }

    public static function getProRataLeaves($userId)
    {
        $result = [];
        $paid_leaves = 0;
        $sick_leaves = 0;

        $userObj = User::find($userId);
        if ($userObj) {
            $year = date('Y');
            $calendarYearObj = CalendarYear::where('year', $year)->first();
            $leaveCalendarYearObj = LeaveCalendarYear::where('calendar_year_id', $calendarYearObj->id)->first();
            $joiningMonth = date('m', (strtotime($userObj->joining_date)));
            $joiningYear = date('Y', (strtotime($userObj->joining_date)));
            if ($joiningYear < $year) {
                $paid_leaves = $leaveCalendarYearObj->max_paid_leaves;
                $sick_leaves = $leaveCalendarYearObj->max_sick_leaves;
            } else {
                $paid_leaves = ceil(($leaveCalendarYearObj->max_paid_leaves * (13 - $joiningMonth)) / 12);
                $sick_leaves = ceil(($leaveCalendarYearObj->max_sick_leaves * (13 - $joiningMonth)) / 12);
            }

        }
        $result['paid_leaves'] = $paid_leaves;
        $result['sick_leaves'] = $sick_leaves;
        return $result;
    }
    public static function getUserProRataLeaves($userId, $leaveTypeId)
    {
        $userObj = User::find($userId);
        if ($userObj) {
            $year = date('Y');
            $calendarYearObj = CalendarYear::where('year', $year)->first();
            if (!$calendarYearObj) {
                return false;
            }
            $leaveCalendarYearObj = LeaveCalendarYear::where('calendar_year_id', $calendarYearObj->id)->where('leave_type_id', $leaveTypeId)->first();
            if (!$leaveCalendarYearObj) {
                return false;
            }
            $joiningMonth = date('m', (strtotime($userObj->joining_date)));
            $proRataLeave = ceil(($leaveCalendarYearObj->max_allowed_leave * (13 - $joiningMonth)) / 12);
            return $proRataLeave;

        }
    }
    public static function getSettleProRataLeaves($userId, $leaveTypeId)
    {
        $userObj = User::find($userId);
        if ($userObj) {
            $year = date('Y');
            $calendarYearObj = CalendarYear::where('year', $year)->first();
            if (!$calendarYearObj) {
                return false;
            }
            $leaveCalendarYearObj = LeaveCalendarYear::where('calendar_year_id', $calendarYearObj->id)->where('leave_type_id', $leaveTypeId)->first();
            if (!$leaveCalendarYearObj) {
                return false;
            }
            if (date('Y', strtotime($userObj->release_date)) > date('Y', strtotime($userObj->confirmation_date))) {
                $releaseMonth = date('m', (strtotime($userObj->release_date)));
                $proRataLeave = ceil(($leaveCalendarYearObj->max_allowed_leave * (13 - (12 - $releaseMonth))) / 12);
            } else {
                $releaseMonth = date('m', (strtotime($userObj->release_date)));
                $joiningMonth = date('m', (strtotime($userObj->joining_date)));
                $proRataLeave = ceil(($leaveCalendarYearObj->max_allowed_leave * ($releaseMonth - $joiningMonth)) / 12);
            }
            return $proRataLeave;

        }
    }

    public static function getCarryForwardLeaves($userId, $year = null)
    {
        $year = $year ?: date('Y');
        $calendaYearObj = CalendarYear::where('year', $year)->first();
        $balanceObj = UserLeaveBalance::where('user_id', $userId)->where('calendar_year_id', $calendaYearObj->id)->first();
        if ($balanceObj) {
            return $balanceObj->carry_forward;
        } else {
            return 0;
        }

    }

    public static function getTotalLeaveDaysMonthwise($startDate, $endDate, $monthId)
    {
        $monthObj = Month::find($monthId);
        $firstDay = strtotime($monthObj->getFirstDay());
        $lastDay = strtotime($monthObj->getLastDay());
        $startDay = strtotime($startDate);
        $endDay = strtotime($endDate);
        $leaveCount = 0;
        $startCondition = $startDay;
        $endCondition = $endDay;

        if ($startDay < $firstDay) {
            $startCondition = $firstDay;
        }

        if ($endDay > $lastDay) {
            $endCondition = $lastDay;
        }

        while ($startCondition <= $endCondition) {
            $startCondition = strtotime('+1 day', $startCondition);
            if (!(NonworkingCalendar::is_holiday(date("Y-m-d", $startCondition)) || NonworkingCalendar::isWeekend(date("Y-m-d", $startCondition)))) {
                $leaveCount++;
            }
        }
        return $leaveCount;
    }

    public static function getMonthlyLeaveReport($monthId)
    {
        $result = [];
        $monthView = [];
        $uid = null;
        $month = Month::find($monthId);

        if (!$month) {
            return false;
        }

        $leaveQuery = "select * from leaves where (" . $month->month . " BETWEEN MONTH(`start_date`) AND MONTH(`end_date`)) AND (" . $month->year . " BETWEEN YEAR(`start_date`) AND YEAR(`end_date`)) order by user_id ASC";
        $leaves = DB::select($leaveQuery);
        foreach ($leaves as $leave) {
            if ($leave->status == "approved") {
                $result[$leave->user_id][] = $leave;
            }
        }

        foreach ($result as $userId => $userResult) {

            $user = User::find($userId);
            if (!($user)) {
                return false;
            }
            $monthView[$userId]["id"] = $user->id;
            $monthView[$userId]["user_name"] = $user->name;
            $monthView[$userId]["employee_id"] = $user->employee_id;
            $monthView[$userId]["paid"] = 0;
            $monthView[$userId]["sick"] = 0;
            $monthView[$userId]["emergency"] = 0;
            $monthView[$userId]["marriage"] = 0;
            $monthView[$userId]["maternity"] = 0;
            $monthView[$userId]["paternity"] = 0;
            $monthView[$userId]["comp-off"] = 0;
            $monthView[$userId]["unpaid"] = 0;
            $monthView[$userId]["other"] = 0;
            $monthView[$userId]["total"] = 0;
            $yearlyLeave = self::getYearlyLeaves($month->year, $userId);
            if ($yearlyLeave) {
                $monthView[$userId]["remaining_sl"] = $yearlyLeave["remaining_sl"];
                $monthView[$userId]["remaining_pl"] = $yearlyLeave["remaining_pl"];
                $monthView[$userId]["availableSickLeaves"] = $yearlyLeave["available_sl"];
                $monthView[$userId]["availablePaidLeaves"] = $yearlyLeave["available_pl"];}

            foreach ($userResult as $rowId => $row) {
                $totalDays = NewLeaveService::getTotalLeaveDaysMonthwise($row->start_date, $row->end_date, $monthId);
                $leaveType = LeaveType::find($row->leave_type_id);
                if ($leaveType->code == "paid") {
                    $monthView[$userId]["paid"] += $totalDays;
                } elseif ($leaveType->code == "sick") {
                    $monthView[$userId]["sick"] += $totalDays;
                } elseif ($leaveType->code == "unpaid") {
                    $monthView[$userId]["unpaid"] += $totalDays;
                } else {
                    $monthView[$userId]["other"] += $totalDays;
                    $monthView[$userId][$leaveType->code] = 1;
                }
                $monthView[$userId]["total"] += $totalDays;
            }
        }

        if ($monthView) {
            return $monthView;
        } else {
            return false;
        }

    }
    public static function getYearlyLeaveReport($monthId, $userId)
    {
        $result = [];
        $yearlyView = [];
        $uid = null;
        $month = Month::find($monthId);
        $monthlyTotal["paid"] = 0;
        $monthlyTotal["sick"] = 0;
        $allLeaveTotal = 0;

        if (!$month) {
            return false;
        }

        $user = User::find($userId);
        if (!($user)) {
            return false;
        }

        $leaveQuery = "select * from leaves where (" . $userId . " = `user_id`) AND (" . $month->month . " BETWEEN MONTH(`start_date`) AND MONTH(`end_date`)) AND (" . $month->year . " BETWEEN YEAR(`start_date`) AND YEAR(`end_date`)) order by user_id ASC";
        $leaves = DB::select($leaveQuery);
        if ($leaves) {
            foreach ($leaves as $index => $leave) {
                if ($leave->status == "approved") {
                    $yearlyView[$index]["from_date"] = $leave->start_date ? $leave->start_date : '';
                    $yearlyView[$index]["to_date"] = $leave->end_date ? $leave->end_date : '';
                    $leaveType = LeaveType::find($leave->leave_type_id);
                    $yearlyView[$index]["type"] = $leaveType->title ? $leaveType->title : '';
                    $approver = User::find($leave->approver_id);
                    $yearlyView[$index]["approved_by"] = $approver ? $approver->name : '';
                    $yearlyView[$index]["approved_date"] = $leave->action_date ? $leave->action_date : '';
                    $totalDays = NewLeaveService::getTotalLeaveDaysMonthwise($leave->start_date, $leave->end_date, $monthId);
                    if ($leaveType->code == "paid") {
                        $monthlyTotal["paid"] += $totalDays;
                        $allLeaveTotal += $totalDays;
                    } elseif ($leaveType->code == "sick") {
                        $monthlyTotal["sick"] += $totalDays;
                        $allLeaveTotal += $totalDays;
                    } else {
                        $allLeaveTotal += $totalDays;
                    }
                }
            }}

        $result["yearlyView"] = $yearlyView;
        $result["monthlyTotal"] = $monthlyTotal;
        $result["allLeaveTotal"] = $allLeaveTotal;

        if ($monthlyTotal) {
            return $result;
        } else {
            return false;
        }
        // $response['status'] = false;
        // $response['yearly_view'] = $yearlyView;
        // $response['monthly_total'] = $monthlyTotal;
        // die;
        // return $response;

    }

    public static function leaveDeduction($monthId)
    {
        LeaveDeductionMonth::where('month_id', $monthId)->delete();
        $month = Month::find($monthId);
        if ($month->month == 1) {
            $prevMonth = Month::where('month', '12')->where('year', $month->year - 1)->first();
        } else {
            $prevMonth = Month::where('month', $month->month - 1)->where('year', $month->year)->first();
        }
        if ($prevMonth) {
            $prevLeaveDeductions = LeaveDeductionMonth::where('month_id', $prevMonth->id)->where('status', 'forwarded')->get();
            foreach ($prevLeaveDeductions as $prevLeaveDeduction) {
                $deductionObj = new LeaveDeductionMonth();
                $deductionObj->user_id = $prevLeaveDeduction->user_id;
                $deductionObj->leave_id = $prevLeaveDeduction->leave_id;
                $deductionObj->month_id = $monthId;
                $deductionObj->duration = $prevLeaveDeduction->duration;
                $deductionObj->amount = $prevLeaveDeduction->amount;
                $deductionObj->save();
            }
        }
        $query = "select * from leaves where (" . $month->month . " BETWEEN MONTH(`start_date`) AND MONTH(`end_date`)) AND (" . $month->year . " BETWEEN YEAR(`start_date`) AND YEAR(`end_date`)) AND status = 'approved' order by user_id, start_date";
        $leaves = DB::select($query);
        foreach ($leaves as $leave) {
            $userObj = User::select('confirmation_date')->where('id', $leave->user_id)->first();
            if ($userObj->confirmation_date == null || ($userObj->confirmation_date != null && $leave->leave_type_id != 1 && $leave->leave_type_id != 2)) {
                $deductionId = LeaveDeductionMonth::saveDeduction($leave->user_id, $leave->leave_type_id, $month->id);
                if ($deductionId) {
                    // job fire with id leaveDeductionObj->id
                    $job = (new LeaveDeductionMonthJob($deductionId));
                    dispatch($job);
                    //NewLeaveService::deductLeave($deductionId);
                }
            }
        }
    }

    public static function deductLeave($id)
    {
        $leaveDeduction = LeaveDeductionMonth::find($id);
        if (!$leaveDeduction) {
            return false;
        }
        $start_condition = strtotime($leaveDeduction->leave->start_date) < strtotime($leaveDeduction->month->getFirstDay()) ? $leaveDeduction->month->getFirstDay() : $leaveDeduction->leave->start_date;
        $end_condition = strtotime($leaveDeduction->leave->end_date) > strtotime($leaveDeduction->month->getLastDay()) ? $leaveDeduction->month->getLastDay() : $leaveDeduction->leave->end_date;
        $amount = 0;
        $lop_counter = 0;
        while (strtotime($start_condition) <= strtotime($end_condition)) {
            if (date("m", strtotime($start_condition)) == ($leaveDeduction->month ? $leaveDeduction->month->month : null) && (!(NonworkingCalendar::is_holiday($start_condition) || NonworkingCalendar::isWeekend($start_condition)))) {
                $salary = Appraisal::getOneDaySalary($leaveDeduction->leave->user_id, $start_condition);
                $amount += $salary;
                $lop_counter += 1;
            }
            $start_condition = date("Y-m-d", strtotime("+1 days", strtotime($start_condition)));
        }
        $leaveDeduction->duration = $lop_counter;
        $leaveDeduction->amount = $amount;
        $leaveDeduction->save();
    }
    public static function getYearlyLeaves($year, $userId)
    {
        $yearlyLeave["remaining_sl"] = NewLeaveService::getAllowedLeave($userId, 'sick');
        $yearlyLeave["remaining_pl"] = NewLeaveService::getAllowedLeave($userId, 'paid');
        $yearlyLeave["available_sl"] = self::getUserAvailableLeave($userId, "sick", $year);
        $yearlyLeave["available_pl"] = self::getUserAvailableLeave($userId, "paid", $year);
        $yearlyLeave["used_sl"] = $yearlyLeave["remaining_sl"] - $yearlyLeave["available_sl"];
        $yearlyLeave["used_pl"] = $yearlyLeave["remaining_pl"] - $yearlyLeave["available_pl"];
        return $yearlyLeave ? $yearlyLeave : false;
    }

    public static function getUpcomingLeaves($userId)
    {
        $user = User::find($userId);
        if (!($user)) {
            return false;
        }
        $currDate = date('Y-m-d', time());
        $leaves = DB::table('leaves')
            ->select(DB::raw('*'))
            ->where('user_id', $userId)
            ->where('start_date', '>', $currDate)
            ->where('status', "approved")
            ->get();

        return $leaves;

    }

    public static function leaveDataMigration($year = null)
    {
        $year = $year ?: date('Y');
        $leaveCalendarYear = CalendarYear::where('year', $year)->first();
        if (!$leaveCalendarYear) {
            return;
        }
        $query = "select * from leaves where (" . $year . " BETWEEN YEAR(`start_date`) AND YEAR(`end_date`)) AND status = 'approved'";
        $oldLeaveData = DB::select($query);
        foreach ($oldLeaveData as $oldLeave) {
            $userObj = User::find($oldLeave->user_id);
            if (!$userObj) {
                return false;
            }
            // if($leaveCalendarYear->leaveType)
            $leaveEntryExists = UserLeaveTransaction::where('reference_type', "App\Models\Leave\Leave")->where('reference_id', $oldLeave->id)->first();
            if ($leaveEntryExists) {
                continue;
            }
            $data['user_id'] = $oldLeave->user_id;
            $data['calendar_year_id'] = $leaveCalendarYear->id;
            $data['leave_type_id'] = $oldLeave->leave_type_id ? $oldLeave->leave_type_id : 0;
            $data['date'] = date('Y-m-d', strtotime($oldLeave->to_date));
            $data['created_at'] = date('Y-m-d', strtotime($oldLeave->created_at));
            $data['type'] = 'debit';

            if ($oldLeave->leave_type_id == 2 && ($oldLeave->half == "first" || $oldLeave->half == "second")) {
                $data['days'] = $oldLeave->days;
            } else {
                $data['days'] = self::getTotalLeaveDaysYearwise($oldLeave->start_date, $oldLeave->end_date, $year);
            }
            $data['days'] = $data['days'];
            $data['reference_type'] = "App\Models\Leave\Leave";
            $data['reference_id'] = $oldLeave->id;
            if (!UserLeaveBalance::addUserLeaves($data)) {
                $response['message'] = 'error processing data';
                $response['leave_id'] = $oldLeave->id;
                return $response;
            }
        }
    }
    public static function getTotalLeaveDaysYearwise($startDate, $endDate, $year)
    {
        $firstDay = strtotime($year . "-01-01");
        $lastDay = strtotime($year . "-12-31");
        $startDay = strtotime($startDate);
        $endDay = strtotime($endDate);
        $leaveCount = 0;
        $startCondition = $startDay;
        $endCondition = $endDay;

        if ($startDay < $firstDay) {
            $startCondition = $firstDay;
        }

        if ($endDay > $lastDay) {
            $endCondition = $lastDay;
        }

        while ($startCondition <= $endCondition) {
            if (!(NonworkingCalendar::is_holiday(date("Y-m-d", $startCondition)) || NonworkingCalendar::isWeekend(date("Y-m-d", $startCondition)))) {
                $leaveCount++;
            }
            $startCondition = strtotime('+1 day', $startCondition);
        }
        return $leaveCount;
    }

    public static function deductUserLeaveByType($deductType, $leaveId)
    {
        $response['status'] = false;
        $response['error'] = null;
        $response['data'] = null;
        $leaveObj = Leave::find($leaveId);
        if (!$leaveObj || $leaveObj->status != "approved") {
            $response['error'] = "Invalid Leave Id or Leave not Approved";
            $response['data'] = $leaveId;
            return $response;
        }
        $userObj = User::find($leaveObj->user_id);
        if (!$userObj) {
            $response['error'] = "Invalid User Id";
            $response['data'] = $userId;
            return $response;
        }
        $year = date('Y');
        $calendarYearObj = CalendarYear::where('year', $year)->first();
        if (!$calendarYearObj) {
            $response['error'] = "Invalid Year";
            $response['data'] = $year;
            return $response;
        }

        if ($deductType == 'PL') {
            $res = self::deductUserPaidLeaves($leaveObj->id);
            if ($res['status']) {
                $response['status'] = true;
            } else {
                $response['error'] = $res['error'];
                $response['data'] = $res['data'];

            }
            return $response;
        } elseif ($deductType == 'SL') {
            $res = self::deductUserSickLeaves($leaveObj->id);
            if ($res['status']) {
                $response['status'] = true;
            } else {
                $response['error'] = $res['error'];
                $response['data'] = $res['data'];

            }
            return $response;
        } elseif ($deductType == 'SLPL') {
            $totalLeaveDays = $leaveObj->days;
            $sickObj = LeaveType::where('code', "sick")->first();
            $paidObj = LeaveType::where('code', "paid")->first();
            $otherObj = LeaveType::find($leaveObj->leave_type_id);
            if ($totalLeaveDays > 0) {
                $userAvailableLeave = UserLeaveService::getUserRemainingLeaves($year, $leaveObj->user_id);
                $availableSL = $userAvailableLeave['sick'];
                $sickDays = min($availableSL, $totalLeaveDays);
                if ($sickDays > 0) {
                    $res = self::deductUserSickLeaves($leaveObj->id, $sickDays);
                    if (!$res['status']) {
                        $response['error'] = $res['error'];
                        $response['data'] = $res['data'];
                        return $response;
                    } else {
                        $totalLeaveDays -= $sickDays;
                    }
                }
            }
            if ($totalLeaveDays > 0) {
                $userAvailableLeave = UserLeaveService::getUserRemainingLeaves($year, $leaveObj->user_id);
                $availablePL = $userAvailableLeave['paid'];
                $paidDays = min($availablePL, $totalLeaveDays);
                if ($paidDays > 0) {
                    $res = self::deductUserPaidLeaves($leaveObj->id, $paidDays);
                    if (!$res['status']) {
                        $response['error'] = $res['error'];
                        $response['data'] = $res['data'];
                        return $response;
                    } else {
                        $totalLeaveDays -= $paidDays;
                    }
                }
            }
            if ($totalLeaveDays > 0) {
                $res = self::deductUserOtherLeaves($leaveObj->id, $totalLeaveDays);
                if (!$res['status']) {
                    $response['error'] = $res['error'];
                    $response['data'] = $res['data'];
                    return $response;
                } else {
                    $response['status'] = true;
                }
            }
        } else {
            $response['error'] = "Invalid deductType";
            $response['data'] = $deductType;
            return $response;
        }
        return $response;
    }

    public static function deductUserSickLeaves($leaveId, $deductDays = null)
    {
        //deduct User's Sick Leave based on (leaveId) and/or (deductDays)
        $response['status'] = false;
        $response['error'] = null;
        $response['data'] = null;
        $leaveObj = Leave::find($leaveId);
        if (!$leaveObj || $leaveObj->status != "approved") {
            $response['error'] = "Invalid Leave Id or Leave not Approved";
            $response['data'] = $leaveId;
            return $response;
        }
        $userObj = User::find($leaveObj->user_id);
        if (!$userObj) {
            $response['error'] = "Invalid User Id";
            $response['data'] = $userId;
            return $response;
        }
        $year = date('Y');
        $calendarYearObj = CalendarYear::where('year', $year)->first();
        if (!$calendarYearObj) {
            $response['error'] = "Invalid Year";
            $response['data'] = $year;
            return $response;
        }
        $sickObj = LeaveType::where('code', "sick")->first();
        if (!$sickObj) {
            $response['error'] = "Invalid leave type";
            $response['data'] = "code: sick";
            return $response;
        }
        if ($deductDays == null && $deductDays != 0) {
            $deductDays = $leaveObj->days;
        }
        if ($deductDays < 0.5) {
            $response['error'] = "Leave Count is less than 0.5";
            $response['data'] = $deductDays;
            return $response;
        }
        $userAvailableLeave = UserLeaveService::getUserRemainingLeaves($year, $leaveObj->user_id);
        $availableSL = $userAvailableLeave['sick'];
        if ($availableSL < $deductDays) {
            $response['error'] = "Available SL Balance is less than the applied for days";
            $response['data'] = $availableSL;
            return $response;
        } else {
            $data['user_id'] = $leaveObj->user_id;
            $data['calendar_year_id'] = $calendarYearObj->id;
            $data['leave_type_id'] = $sickObj->id;
            $data['date'] = date('Y-m-d');
            $data['days'] = $deductDays;
            $data['reference_type'] = "App\Models\Leave\Leave";
            $data['reference_id'] = $leaveObj->id;
            $res = UserLeaveTransaction::saveData($data);
            if ($res) {
                $response['status'] = true;
            } else {
                $response['error'] = "Unable to save Transaction";
                $response['data'] = $data;
            }
            return $response;
        }
    }

    public static function deductUserPaidLeaves($leaveId, $deductDays = null)
    {
        //deduct User's Paid Leave based on (leaveId) and/or (deductDays)
        $response['status'] = false;
        $response['error'] = null;
        $response['data'] = null;
        $leaveObj = Leave::find($leaveId);
        if (!$leaveObj || $leaveObj->status != "approved") {
            $response['error'] = "Invalid Leave Id or Leave not Approved";
            $response['data'] = $leaveId;
            return $response;
        }
        $userObj = User::find($leaveObj->user_id);
        if (!$userObj) {
            $response['error'] = "Invalid User Id";
            $response['data'] = $userId;
            return $response;
        }
        $year = date('Y');
        $calendarYearObj = CalendarYear::where('year', $year)->first();
        if (!$calendarYearObj) {
            $response['error'] = "Invalid Year";
            $response['data'] = $year;
            return $response;
        }
        $paidObj = LeaveType::where('code', "paid")->first();
        if (!$paidObj) {
            $response['error'] = "Invalid leave type";
            $response['data'] = "code: paid";
            return $response;
        }

        if ($deductDays == null && $deductDays != 0) {
            $deductDays = $leaveObj->days;
        }

        if ($deductDays <= 0) {
            $response['error'] = "Leave Count is less than 0.5";
            $response['data'] = $deductDays;
            return $response;
        }
        $userAvailableLeave = UserLeaveService::getUserRemainingLeaves($year, $leaveObj->user_id);
        $availablePL = $userAvailableLeave['paid'];

        if ($availablePL < $deductDays) {
            $response['error'] = "Available PL Balance is less than the applied for days";
            $response['data'] = $availablePL;
            return $response;
        } else {
            $data['user_id'] = $leaveObj->user_id;
            $data['calendar_year_id'] = $calendarYearObj->id;
            $data['leave_type_id'] = $paidObj->id;
            $data['date'] = date('Y-m-d');
            $data['days'] = $deductDays;
            $data['reference_type'] = "App\Models\Leave\Leave";
            $data['reference_id'] = $leaveObj->id;
            $res = UserLeaveTransaction::saveData($data);
            if ($res) {
                $response['status'] = true;
            } else {
                $response['error'] = "Unable to save Transaction";
                $response['data'] = $data;
            }
            return $response;
        }
    }

    public static function deductUserOtherLeaves($leaveId, $deductDays = null)
    {
        //deduct User's Other Leave based on (leaveId) and/or (deductDays)
        $response['status'] = false;
        $response['error'] = null;
        $response['data'] = null;
        $leaveObj = Leave::find($leaveId);
        if (!$leaveObj || $leaveObj->status != "approved") {
            $response['error'] = "Invalid Leave Id or Leave not Approved";
            $response['data'] = $leaveId;
            return $response;
        }
        $userObj = User::find($leaveObj->user_id);
        if (!$userObj) {
            $response['error'] = "Invalid User Id";
            $response['data'] = $userId;
            return $response;
        }
        $year = date('Y');
        $calendarYearObj = CalendarYear::where('year', $year)->first();
        if (!$calendarYearObj) {
            $response['error'] = "Invalid Year";
            $response['data'] = $year;
            return $response;
        }
        $otherObj = LeaveType::find($leaveObj->leave_type_id);
        if (!$otherObj) {
            $response['error'] = "Invalid leave type";
            $response['data'] = $leaveObj->leave_type_id;
            return $response;
        }
        $deductDays = $deductDays ?: $leaveObj->days;
        if ($deductDays <= 0) {
            $response['error'] = "Leave Count is less than 0.5";
            $response['data'] = $deductDays;
            return $response;
        }
        $data['user_id'] = $leaveObj->user_id;
        $data['calendar_year_id'] = $calendarYearObj->id;
        $data['leave_type_id'] = $otherObj->id;
        $data['date'] = date('Y-m-d');
        $data['days'] = $deductDays;
        $data['reference_type'] = "App\Models\Leave\Leave";
        $data['reference_id'] = $leaveObj->id;
        $res = UserLeaveTransaction::saveData($data);
        if ($res) {
            $response['status'] = true;
        } else {
            $response['error'] = "Unable to save Transaction";
            $response['data'] = $data;
        }
        return $response;
    }
    public static function getMonthlyLeaves($monthId)
    {
        $result = [];
        $month = Month::find($monthId);
        if (!$month) {
            return false;
        }
        $calendarYear = CalendarYear::where('year', $month->year)->first();
        if (!$calendarYear) {
            return false;
        }
        $currDate = date('Y-m-d');
        $monthlyLeaves = DB::table('user_leave_transactions')
            ->select('user_id', 'leave_type_id', DB::raw('SUM(days) as days'))
            ->where('calendar_year_id', $calendarYear->id)
            ->where('calculation_date', '<=', $currDate)
            ->groupBy('user_id', 'leave_type_id')
            ->havingRaw('SUM(days) < 0')
            ->get();
        $users = User::where('release_date', '!=', null)->pluck('id');
        $resignedLeaves = DB::table('user_leave_transactions')
            ->select('user_id', 'leave_type_id', DB::raw('SUM(days) as days'))
            ->where('calendar_year_id', $calendarYear->id)
            ->where('calculation_date', '<=', $currDate)
            ->whereIn('user_id', $users)
            ->groupBy('user_id', 'leave_type_id')
            ->havingRaw('SUM(days) > 0')
            ->get();
        $leaveTypes = LeaveType::get();
        $users = User::where('is_active', 1)->get();
        $finalData = [];
        foreach ($monthlyLeaves as $monthlyLeave) {
            $user = User::find($monthlyLeave->user_id);
            $finalData[$monthlyLeave->user_id]["username"] = $user->name;
            $finalData[$monthlyLeave->user_id][$monthlyLeave->leave_type_id] = $monthlyLeave->days;
        }
        foreach ($resignedLeaves as $resignedLeave) {
            $user = User::find($resignedLeave->user_id);
            $finalData[$resignedLeave->user_id]["username"] = $user->name;
            $finalData[$resignedLeave->user_id][$resignedLeave->leave_type_id] = $resignedLeave->days;
        }
        return $finalData;
    }

    public static function settleLeavesAfterResign($userId)
    {
        $response['status'] = false;
        $response['message'] = '';
        $response['data'] = null;
        $userObj = User::find($userId);
        if (!$userObj) {
            $response['message'] = "Invalid User Id";
            $response['data'] = $userId;
            return $response;
        }
        if ($userObj->release_date == null || $userObj->confirmation_date == null || date('Y', strtotime($userObj->release_date)) < date('Y', strtotime($userObj->confirmation_date))) {
            $response['message'] = "Invalid confirmation date/release date";
            $response['data']['confirmation_date'] = $userObj->confirmation_date;
            $response['data']['release_date'] = $userObj->release_date;
            return $response;
        }
        $leaveTypes = LeaveType::get();
        if (!$leaveTypes) {
            $response['message'] = "leave types not found";
            $response['data'] = $leaveTypes;
            return $response;
        }
        $calendarYear = CalendarYear::where('year', date('Y'))->first();
        if (!$calendarYear) {
            $response['message'] = "calendar year not found";
            $response['data'] = $calendarYear;
            return $response;
        }
        $monthObj = Month::where('month', date('m'))->where('year', date('Y'))->first();
        if (!$monthObj) {
            $response['message'] = "month object not found";
            $response['data'] = date('Y-m');
        }
        DB::beginTransaction();
        try
        {
            foreach ($leaveTypes as $leaveType) {
                $deductLeaves = self::getSettleProRataLeaves($userId, $leaveType->id);
                $deductionResponse = LeaveDeductionMonth::saveDeduction($userId, $leaveType->id, $monthObj->id, $deductLeaves, "deduction for resignation");
                if (!$deductionResponse) {
                    throw new Exception("error saving data in leave deduction month");
                }
                if ($deductLeaves > 0) {
                    $data['user_id'] = $userId;
                    $data['leave_type_id'] = $leaveType->id;
                    $data['type'] = 'debit';
                    $data['days'] = $deductLeaves;
                    $data['reference_type'] = "App\Models\Leave\LeaveCalendarYear";
                    $data['reference_id'] = $calendarYear->id;
                    $ifExists = UserLeavetransaction::where('user_id', $data['user_id'])->where('leave_type_id', $data['leave_type_id'])->where('reference_type', $data['reference_type'])->where('reference_id', $data['reference_id'])->where('days', '<', 0)->first();
                    if (!$ifExists) {
                        $result = UserLeaveTransaction::saveData($data);
                        if (!$result) {
                            throw new Excpetion("error saving data in user leave transaction");
                        }
                    }
                }
            }
            DB::commit();
        } catch (Exception $e) {
            $response['message'] = "error while processing leave debits";
            return $response;
            DB::rollback();
        }
        $response['status'] = true;
        return $response;
    }

    /*
    function : getUserWorkingDays
    Returns working days of user considering joining date and release date.
    response : int(working days)
     */
    public static function getUserWorkingDays($startDate,$endDate,$userId)
    {
        $user = User::find($userId);
        if(!$user)
            return false;
        if(($user->joining_date > $startDate) && ($user->joining_date < $endDate))
            $startDate = $user->joining_date;
        if(($user->release_date > $startDate) && ($user->release_date < $endDate))
            $endDate = $user->release_date;
        $days_worked = self::calculateWorkingDays($startDate, $endDate, $userId);
        return $days_worked;
    }

}
