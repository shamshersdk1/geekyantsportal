<?php namespace App\Services;

use App\Models\Admin\BillingSchedule;
use App\Models\Admin\BillingScheduleItem;
use App\Models\Admin\Calendar;
use App\Services\GoogleCalendarService\GoogleCalendarService;

class BillingScheduleService
{
    public static function generateBill($id, $fromDate, $billDate)
    {
        $BillingSchedule = BillingSchedule::find($id);
        $BillingScheduleItemObj = new BillingScheduleItem;
        $BillingScheduleItemObj->billing_schedule_id = $BillingSchedule->id;
        if (count($BillingSchedule->items) > 0) {
            $latestBillingScheduleItem = BillingScheduleItem::where('billing_schedule_id', $id)->orderBy('from_date', 'desc')->first();
            $arr = explode("_", $latestBillingScheduleItem->title);
            $word = $arr[0];
            $index = $arr[1] + 1;
            $BillingScheduleItemObj->title = $word . "_" . $index;
        } else {
            $BillingScheduleItemObj->title = 'Milestone_1';
        }
        $BillingScheduleItemObj->from_date = $fromDate;
        $BillingScheduleItemObj->to_date = $billDate;
        if (Calendar::whereDate('date', $billDate)->count() > 0 || date('N', strtotime($billDate)) > 5) {
            $BillingScheduleItemObj->reminder_date = GoogleCalendarService::nextWorkingDay($billDate);
        } else {
            $BillingScheduleItemObj->reminder_date = $billDate;
        }
        if ($BillingScheduleItemObj->save()) {
            return $BillingScheduleItemObj;
        }
        return $BillingScheduleItemObj->getErrors();
    }
    public static function getFromDate($id)
    {
        $BillingSchedule = BillingSchedule::find($id);
        if (count($BillingSchedule->items) > 0) {
            $latestBillingScheduleItem = BillingScheduleItem::where('billing_schedule_id', $id)->orderBy('from_date', 'desc')->first();
            $fromDate = $latestBillingScheduleItem->to_date;
        } else {
            $fromDate = $BillingSchedule->from_date;
        }
        return $fromDate;
    }
    public static function checkCycle($id)
    {
        $BillingSchedule = BillingSchedule::find($id);
        if ($BillingSchedule->mon) {
            return 'next mon';
        } else if ($BillingSchedule->tue) {
            return 'next tue';

        } else if ($BillingSchedule->wed) {
            return 'next wed';

        } else if ($BillingSchedule->thu) {
            return 'next thu';

        } else if ($BillingSchedule->fri) {
            return 'next fri';
        } else if ($BillingSchedule->first_working_day) {
            return 'first day of month';
        } else if ($BillingSchedule->last_working_day) {
            return 'last day of month';
        }
    }
}
