<?php namespace App\Services;

use App\Models\Admin\ReferenceNumber;
use Config;


class ReferenceService {

  public static function generateReference()
	{
		$reference_string = Config::get('app.reference_string');
		$year = date('y');
		$month = date('m');
    $sl_no = self::generateSerialNumber($year, $month);
    $formatted_sl_no = sprintf('%04d', $sl_no);

    $reference_string = $reference_string.$year.'/'.$month.'/'.$formatted_sl_no;

    $result['year'] = $year;
    $result['month'] = $month;
    $result['sl_no'] = $sl_no;
    $result['reference_string'] = $reference_string;

		return $result;
	}

  public static function generateSerialNumber($year, $month)
  {
    $maxSlNo = ReferenceNumber::where('month', $month)->where('year', $year)->max('sl_no');
    if( empty($maxSlNo) ) {
      $newSlNo = 1;
    } else {
      $newSlNo = $maxSlNo + 1;
    }
    
    return $newSlNo;

  }

}