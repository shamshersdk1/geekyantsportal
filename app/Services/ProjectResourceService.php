<?php namespace App\Services;

use Validator;
use App\Models\Admin\Project;
use App\Models\Admin\ProjectResource;
use App\Models\Timesheet;
use App\Models\TimesheetDetail;
use App\Models\Admin\Calendar;

use App\Models\Admin\ProjectSprints;
use App\Events\Project\DeveloperAssigned;

use DB;
use Input;
use Redirect;
use App\Models\User;
use Auth;

class ProjectResourceService {

	public static function projectResourceReport($start_date, $end_date, $users, $projects)
	{
		
		$projectResourceDataList = [];
		$resourceList = User::whereIn('id',$users)->get();
		$projectList = Project::whereIn('id',$projects)->get();
		$dates = [];
		$result = [];
		$max= date_diff(date_create($start_date), date_create($end_date))->format('%a');
		
		$date = $start_date;
		$users_array = [];
		
		$len = count($users);
		if ( !empty($users) ){
			for ($i=0;$i<$len; $i++ )
			{
				$users_array[] = $users[$i];
			}
		}
		
		
		for($i=0;$i<=$max;$i++)
		{
			// if(date('N', strtotime($date)) > 5 || Calendar::whereDate('date', $date)->count() > 0) 
			// {
			// 	$is_holiday_data[] = 1;
			// } else {
			// 	$is_holiday_data[] = 0;
			// }
			$projectNameList = [];
			$dailyProjectResourceList =[];
			foreach($projectList as $project)
			{
				$users = '';
				$currentUsersEndDated = [];
				$currentUsersWithNull = [];
				$currentUsers = [];
				
				$currentUsersEndDated = ProjectResource::with('user_data')
							->where('project_id',$project['id'])
							->where('start_date','<=',$date)
							->where('end_date','>=',$date)
							->whereIn('user_id',$users_array)->get();
				
				$currentUsersWithNull = ProjectResource::with('user_data')
									->where('project_id',$project['id'])
									->where('start_date','<=',$date)
									->whereNull('end_date')
									->whereIn('user_id',$users_array)->get();
				
				$currentUsers = $currentUsersEndDated->merge($currentUsersWithNull);
				
				if ( count($currentUsers) > 0 ) {
					foreach ( $currentUsers as $currentUser ){
						$users_details_array[] = $currentUser->user_data->name;
						$users = implode($users_details_array,', <br> ');
					}
				} 
				
				$projectResourceData['date'] = $date;
				$projectResourceData['users'] = $users;
				$dailyProjectResourceList[] = $projectResourceData;
				$projectNameList[] = $project->project_name;
				$users_details_array = [];
				
			}
			
			$projectResourceDataList[] = $dailyProjectResourceList;
			$dates[]= date('D, j M', strtotime($date));
			$date = date('Y-m-d', strtotime($date . ' +1 day'));
		}
		
		$result['dates'] = $dates;
		$result['project_name'] = $projectNameList;
		$result['projectResourceDataList'] = $projectResourceDataList;
		return $result;	
	}

	public static function isUserAllocated($project_id, $user_id)
	{
		$date = date('Y-m-d');
		$currentUserEndDated = ProjectResource::where('project_id',$project_id)
												->where('end_date','>=',$date)
												->where('user_id',$user_id)->first();
				
		$currentUserWithNull = ProjectResource::where('project_id',$project_id)
												->whereNull('end_date')
												->where('user_id',$user_id)->first();
			
		
		if ( !empty($currentUserEndDated) || !empty($currentUserWithNull) ) {
			return true;
		} else {
			return false;
		}
	}

	public static function assignUserToProject($user_id, $project_id, $start_date, $end_date = null)
	{
		$status = true;
		$message = '';
		$projectResourcesObj = new ProjectResource();
		$projectResourcesObj->project_id = $project_id;
		$projectResourcesObj->user_id = $user_id;
		$projectResourcesObj->start_date = $start_date;
		$projectResourcesObj->end_date = $end_date;
		if (!$projectResourcesObj->save()) 
		{
			$status = false;
			$message = $projectResourcesObj->getErrors();
		}
		event(new DeveloperAssigned($projectResourcesObj->user_id, $projectResourcesObj->project_id, $projectResourcesObj->start_date, 
                                                $projectResourcesObj->end_date));
		$response['status'] = $status;
		$response['message'] = $message;
		return $response;
	}
}