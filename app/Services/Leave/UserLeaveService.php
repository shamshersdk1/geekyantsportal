<?php
namespace App\Services\Leave;

use App\Events\Leave\LeaveApplied;
use App\Events\Leave\LeaveApproved;
use App\Models\Admin\Calendar;
use App\Models\Admin\CalendarYear;
use App\Models\Admin\LeaveType;
use App\Models\Admin\Project;
use App\Models\Admin\ProjectResource;
use App\Models\LeaveOverlapRequest;
use App\Models\Leave\Leave;
use App\Models\Leave\LeaveWarning;
use App\Models\Leave\UserLeaveTransaction;
use App\Models\NonworkingCalendar;
use App\Models\ProjectManagerLeaveApproval;
use App\Models\User;
use App\Models\WorkingDayGroup;
use Auth;
use DB;
use Exception;
use Log;

class UserLeaveService
{

    //change
    public static function applyLeave($data)
    {
        $response = array('status' => false, 'message' => "", 'data' => null);

        DB::beginTransaction();
        try {
            $resultLeave = Leave::saveData($data);
            $resultUserLeaveTransaction = UserLeaveTransaction::saveData($data);
        } catch (Exception $e) {
            \Log::info('Catch block' . json_encode($e->getMessage()));
            DB::rollback();
        }
        DB::commit();
        $response['status'] = true;
        $response['message'] = 'success';
        $response['data'] = $resultLeave['data'];
        return $response;
    }

    public static function calculateWorkingDays($startDate, $endDate, $id)
    {
        $user = User::find($id);
        $count = 0;
        if (!empty($user->workingDayGroup) && !empty($user->workingDayGroup->workingDays)) {
            $group = $user->workingDayGroup->workingDays;
            $nonWorkingDays = [];
            if (!empty($group)) {
                if (!$group->monday) {
                    $nonWorkingDays[] = 1;
                }
                if (!$group->tuesday) {
                    $nonWorkingDays[] = 2;
                }
                if (!$group->wednesday) {
                    $nonWorkingDays[] = 3;
                }
                if (!$group->thursday) {
                    $nonWorkingDays[] = 4;
                }
                if (!$group->friday) {
                    $nonWorkingDays[] = 5;
                }
                if (!$group->saturday) {
                    $nonWorkingDays[] = 6;
                }
                if (!$group->sunday) {
                    $nonWorkingDays[] = 7;
                }
            } else {
                $nonWorkingDays = [6, 7];
            }
        } else {
            $nonWorkingDays = [6, 7];
        }

        $startDate = date('Y-m-d', strtotime($startDate));
        $endDate = date('Y-m-d', strtotime($endDate));

        $holidays = Calendar::whereDate('date', '>=', $startDate)->whereDate('date', '<=', $endDate)->where('type', 'Holiday')->pluck('date')->toArray();
        for ($date = $startDate; strtotime($date) <= strtotime($endDate); $date = date('Y-m-d', strtotime($date . '+1 day'))) {
            if (in_array(date('N', strtotime($date)), $nonWorkingDays) || in_array(date('Y-m-d', strtotime($date)), $holidays)) {
                continue;
            } else {
                $count++;
            }
        }
        return $count;
    }

    public static function getUserAvailableLeaveBalance($calendarYear, $userId)
    {
        $leaveTypes = LeaveType::all();
        $count = 0;
        $leaves = [];

        foreach ($leaveTypes as $type) {
            $id = $type->id;
            $count = self::countAvailableLeaves($calendarYear, $userId, $id);
            $leaves[$type->code] = number_format($count, 1);
        }

        return $leaves;
    }

    public static function getUserRemainingLeaves($userId, $year = null)
    {
        $leaveTypes = LeaveType::all();
        $count = 0;
        $leaves = [];

        foreach ($leaveTypes as $type) {
            $id = $type->id;
            $count = self::countAvailableLeaves($year, $userId, $id);
            if ($count < 0) {
                $leaves[$type->code] = 0;
            } else {
                $leaves[$type->code] = $count;
            }
        }
        return $leaves;
    }

    public static function countAvailableLeaves($calendarYear, $userId, $leaveTypeId)
    {
        $calendarYear = $calendarYear ?: date('Y');

        $calendarYearObj = CalendarYear::where('year', $calendarYear)->first();

        $calendarYearId = $calendarYearObj->id;
        if ($calendarYearId && $userId && $leaveTypeId) {
            $transactionSum = UserLeaveTransaction::where('calendar_year_id', $calendarYearId)->where('user_id', $userId)->where('leave_type_id', $leaveTypeId)->sum('days');
            return $transactionSum;
        }

        return null;
    }
    public static function getUserConsumedLeaveBalance($userId, $calendarYear = null)
    {
        $leaveTypes = LeaveType::all();
        $count = 0;
        $leaves = [];

        foreach ($leaveTypes as $type) {
            $id = $type->id;
            $count = self::consumedLeaves($calendarYear, $userId, $id);
            $leaves[$type->code] = $count;
        }

        return $leaves;
    }

    public static function consumedLeaves($calendarYear, $userId, $leaveTypeId)
    {
        $response = [];

        $calendarYear = $calendarYear ?: date('Y');

        $calendarYearObj = CalendarYear::where('year', $calendarYear)->first();

        if (!$calendarYearObj) {
            $response['status'] = false;
            $response['message'] = 'Calendar Year not found';
            return $response;
        }

        $calendarYearId = $calendarYearObj->id;

        if ($calendarYearId && $userId && $leaveTypeId) {
            $transactionSum = UserLeaveTransaction::where('calendar_year_id', $calendarYearId)->where('user_id', $userId)->where('leave_type_id', $leaveTypeId)->where('reference_type', 'App\Models\Leave\Leave')->sum('days');
            return $transactionSum;
        }

        return null;
    }

    public static function getUserAllowedLeaveBalance($userId, $year = null)
    {
        $leaveTypes = LeaveType::all();
        $count = 0;
        $leaves = [];

        foreach ($leaveTypes as $type) {
            $code = $type->code;
            $count = self::countAllowedLeaves($userId, $code, $year);
            $leaves[$type->code] = $count;
        }

        return $leaves;
    }

    public static function countAllowedLeaves($userId, $leaveTypeCode, $year = null)
    {
        $total = 0;
        $year = $year ?: date('Y');
        $obj = LeaveType::where('code', $leaveTypeCode)->first();
        $id = $obj->id;
        $calendaYearObj = CalendarYear::where('year', $year)->first();
        if ($calendaYearObj) {
            $userLeaveTransactionObj = UserLeaveTransaction::where('user_id', $userId)->where('calendar_year_id', $calendaYearObj->id)->where('leave_type_id', $id)
            // ->where('reference_type', 'App\Models\Leave\LeaveCalendarYear')
                ->whereIn('reference_type', ['App\Models\Leave\LeaveCalendarYear', 'App\Models\Admin\Bonus', 'App\Models\Admin\LeaveBalanceManager'])
                ->sum('days');
            if ($userLeaveTransactionObj) {
                $total = $userLeaveTransactionObj;
            }
        }

        return ceil($total);
    }

    public static function getAllUserRemainingLeaves($yearId, $userId = null, $startDate = null, $endDate = null)
    {
        $response = [];
        $response['status'] = false;
        $response['message'] = '';
        $response['data'] = '';

        $yearObj = CalendarYear::where('id', $yearId)->first();
        if (!$yearObj) {
            $response['status'] = false;
            $response['message'] = 'Calendar Year not found';
            return $response;
        }

        if ($userId == null) {
            if ($startDate != null && $endDate != null) {
                $remainingLeaves = DB::table('user_leave_transactions')
                    ->select('user_id', 'leave_type_id', DB::raw('SUM(days) as days'))
                    ->where('calendar_year_id', $yearObj->id)
                    ->whereDate('created_at', '>=', $startDate)
                    ->whereDate('created_at', '<=', $endDate)
                    ->groupBy('user_id', 'leave_type_id')
                    ->get();
            } else {
                $remainingLeaves = DB::table('user_leave_transactions')
                    ->select('user_id', 'leave_type_id', DB::raw('SUM(days) as days'))
                    ->where('calendar_year_id', $yearObj->id)
                    ->groupBy('user_id', 'leave_type_id')
                    ->get();
            }
            foreach ($remainingLeaves as $remainingLeave) {
                $user = User::find($remainingLeave->user_id);
                if (!$user) {
                    $response['status'] = false;
                    $response['message'] = 'User not found';
                    return $response;
                }
                $leaveObj = LeaveType::find($remainingLeave->leave_type_id);
                if (!$leaveObj) {
                    $response['status'] = false;
                    $response['message'] = 'Leave type not found';
                    return $response;
                }
                $leave_code = $leaveObj->code;

                $response[$remainingLeave->user_id]["username"] = $user->name;
                $response[$remainingLeave->user_id]["userId"] = $user->id;
                $response[$remainingLeave->user_id]["employee_id"] = $user->employee_id;
                $response[$remainingLeave->user_id][$leave_code] = number_format($remainingLeave->days, 1);
            }
            $response['status'] = true;

        } else {
            $user = User::find($userId);

            if (!$user) {
                $response['status'] = false;
                $response['message'] = 'User not found';
                return $response;
            }
            if ($startDate != null && $endDate != null) {
                $remainingLeaves = DB::table('user_leave_transactions')
                    ->select('user_id', 'leave_type_id', DB::raw('SUM(days) as days'))
                    ->where('calendar_year_id', $yearObj->id)
                    ->whereDate('created_at', '>=', $startDate)
                    ->whereDate('created_at', '<=', $endDate)
                    ->where('user_id', $userId)
                    ->groupBy('user_id', 'leave_type_id')
                    ->get();
            } else {
                $remainingLeaves = DB::table('user_leave_transactions')
                    ->select('user_id', 'leave_type_id', DB::raw('SUM(days) as days'))
                    ->where('calendar_year_id', $yearObj->id)
                    ->where('user_id', $userId)
                    ->groupBy('user_id', 'leave_type_id')
                    ->get();
            }

            if (count($remainingLeaves) > 0) {
                foreach ($remainingLeaves as $remainingLeave) {

                    $leaveObj = LeaveType::find($remainingLeave->leave_type_id);

                    if (!$leaveObj) {
                        $response['status'] = false;
                        $response['message'] = 'Leave type not found';
                        return $response;
                    }
                    $leave_code = $leaveObj->code;

                    $response[$remainingLeave->user_id]["username"] = $user->name;
                    $response[$remainingLeave->user_id]["userId"] = $user->id;
                    $response[$remainingLeave->user_id]["employee_id"] = $user->employee_id;
                    $response[$remainingLeave->user_id][$leave_code] = number_format($remainingLeave->days, 1);
                }
            } else {
                $leaves = LeaveType::all();

                foreach ($leaves as $leave) {
                    $response[$userId][$leave->code] = 0;
                }
            }
            $response['status'] = true;
        }
        return $response;
    }

    public static function getUserUsedLeavesAll($yearId, $userId = null, $startDate = null, $endDate = null)
    {
        $response = [];
        $response['status'] = false;
        $response['message'] = '';
        $response['data'] = '';

        $yearObj = CalendarYear::where('id', $yearId)->first();
        if (!$yearObj) {
            $response['status'] = false;
            $response['message'] = 'Calendar Year not found';
            return $response;
        }

        if ($userId == null) {
            if ($startDate != null && $endDate != null) {
                $usedLeaves = UserLeaveTransaction::where('calendar_year_id', $yearObj->id)->whereDate('calculation_date', '>=', $startDate)
                    ->whereDate('calculation_date', '<=', $endDate)->get();

            } else {
                $usedLeaves = UserLeaveTransaction::where('calendar_year_id', $yearObj->id)->get();
            }

            foreach ($usedLeaves as $usedLeave) {
                $user = User::find($usedLeave->user_id);
                if (!$user) {
                    $response['status'] = false;
                    $response['message'] = 'User not found';
                    return $response;
                }
                $leaveObj = LeaveType::find($usedLeave->leave_type_id);
                if (!$leaveObj) {
                    $response['status'] = false;
                    $response['message'] = 'Leave type not found';
                    return $response;
                }
                $leave_code = $leaveObj->code;
                if ($usedLeave->days < 0) {
                    if ($response[$usedLeave->user_id]["userId"] == $user->id) {
                        $response[$usedLeave->user_id][$leave_code] += number_format($usedLeave->days, 1);

                    } else {
                        $response[$usedLeave->user_id]["username"] = $user->name;
                        $response[$usedLeave->user_id]["userId"] = $user->id;
                        $response[$usedLeave->user_id]["employee_id"] = $user->employee_id;
                        $response[$usedLeave->user_id][$leave_code] = number_format($usedLeave->days, 1);
                    }
                }
            }
            $response['status'] = true;

        } else {
            $user = User::find($userId);

            if (!$user) {
                $response['status'] = false;
                $response['message'] = 'User not found';
                return $response;
            }
            if ($startDate != null && $endDate != null) {
                $usedLeaves = UserLeaveTransaction::where('calendar_year_id', $yearObj->id)->whereDate('calculation_date', '>=', $startDate)
                    ->whereDate('calculation_date', '<=', $endDate)->get();

            } else {
                $usedLeaves = UserLeaveTransaction::where('calendar_year_id', $yearObj->id)->get();

            }
            if (count($remainingLeaves) > 0) {
                foreach ($usedLeaves as $usedLeave) {

                    $leaveObj = LeaveType::find($usedLeave->leave_type_id);

                    if (!$leaveObj) {
                        $response['status'] = false;
                        $response['message'] = 'Leave type not found';
                        return $response;
                    }
                    $leave_code = $leaveObj->code;

                    $response[$usedLeave->user_id]["username"] = $user->name;
                    $response[$usedLeave->user_id]["userId"] = $user->id;
                    $response[$usedLeave->user_id]["employee_id"] = $user->employee_id;
                    $response[$usedLeave->user_id][$leave_code] = number_format($usedLeave->days, 1);
                }
            } else {
                $leaves = LeaveType::all();

                foreach ($leaves as $leave) {
                    $response[$userId][$leave->code] = 0;
                }

            }
            $response['status'] = true;
        }
        return $response;
    }

    public static function getAllUserAllowedLeaves($yearId, $userId = null)
    {
        $response = [];
        $response['status'] = false;
        $response['message'] = '';
        $response['data'] = '';

        $yearObj = CalendarYear::where('id', $yearId)->first();
        if (!$yearObj) {
            $response['status'] = false;
            $response['message'] = 'Calendar Year not found';
            return $response;
        }

        if ($userId == null) {
            $allowedLeaves = DB::table('user_leave_transactions')
                ->select('user_id', 'leave_type_id', DB::raw('SUM(days) as days'))
                ->where('calendar_year_id', $yearObj->id)
                ->where('reference_type', '!=', "App\Models\Leave\Leave")
                ->groupBy('user_id', 'leave_type_id')
                ->get();

            foreach ($allowedLeaves as $allowedLeave) {
                $user = User::find($allowedLeave->user_id);
                if (!$user) {
                    $response['status'] = false;
                    $response['message'] = 'User not found';
                    return $response;
                }
                $leaveObj = LeaveType::find($allowedLeave->leave_type_id);
                if (!$leaveObj) {
                    $response['status'] = false;
                    $response['message'] = 'Leave type not found';
                    return $response;
                }
                $leave_code = $leaveObj->code;

                $response[$allowedLeave->user_id]["username"] = $user->name;
                $response[$allowedLeave->user_id]["userId"] = $user->id;
                $response[$allowedLeave->user_id]["employee_id"] = $user->employee_id;
                $response[$allowedLeave->user_id][$leave_code] = number_format($allowedLeave->days, 1);
            }
            $response['status'] = true;

        } else {
            $user = User::find($userId);

            if (!$user) {
                $response['status'] = false;
                $response['message'] = 'User not found';
                return $response;
            }

            $allowedLeaves = DB::table('user_leave_transactions')
                ->select('user_id', 'leave_type_id', DB::raw('SUM(days) as days'))
                ->where('calendar_year_id', $yearObj->id)
                ->where('user_id', $userId)
                ->where('reference_type', '!=', "App\Models\Leave\Leave")
                ->groupBy('user_id', 'leave_type_id')
                ->get();

            foreach ($allowedLeaves as $allowedLeave) {

                $leaveObj = LeaveType::find($allowedLeave->leave_type_id);

                if (!$leaveObj) {
                    $response['status'] = false;
                    $response['message'] = 'Leave type not found';
                    return $response;
                }
                $leave_code = $leaveObj->code;

                $response[$allowedLeave->user_id]["username"] = $user->name;
                $response[$allowedLeave->user_id]["userId"] = $user->id;
                $response[$allowedLeave->user_id]["employee_id"] = $user->employee_id;
                $response[$allowedLeave->user_id][$leave_code] = number_format($allowedLeave->days, 1);
            }
            $response['status'] = true;
        }
        return $response;

    }

    public static function getAllUserUsedLeaves($yearId, $userId = null)
    {
        $response = [];
        $response['status'] = false;
        $response['message'] = '';
        $response['data'] = '';

        $yearObj = CalendarYear::where('id', $yearId)->first();
        if (!$yearObj) {
            $response['status'] = false;
            $response['message'] = 'Calendar Year not found';
            return $response;
        }

        if ($userId == null) {
            $usedLeaves = DB::table('user_leave_transactions')
                ->select('user_id', 'leave_type_id', DB::raw('SUM(days) as days'))
                ->where('calendar_year_id', $yearObj->id)
                ->where('reference_type', "App\Models\Leave\Leave")
                ->groupBy('user_id', 'leave_type_id')
                ->get();

            foreach ($usedLeaves as $usedLeave) {
                $user = User::find($usedLeave->user_id);
                if (!$user) {
                    $response['status'] = false;
                    $response['message'] = 'User not found';
                    return $response;
                }
                $leaveObj = LeaveType::find($usedLeave->leave_type_id);
                if (!$leaveObj) {
                    $response['status'] = false;
                    $response['message'] = 'Leave type not found';
                    return $response;
                }
                $leave_code = $leaveObj->code;

                $response[$usedLeave->user_id]["username"] = $user->name;
                $response[$usedLeave->user_id]["userId"] = $user->id;
                $response[$usedLeave->user_id]["employee_id"] = $user->employee_id;
                $response[$usedLeave->user_id][$leave_code] = number_format($usedLeave->days, 1);
            }
            $response['status'] = true;

        } else {
            $user = User::find($userId);

            if (!$user) {
                $response['status'] = false;
                $response['message'] = 'User not found';
                return $response;
            }

            $usedLeaves = DB::table('user_leave_transactions')
                ->select('user_id', 'leave_type_id', DB::raw('SUM(days) as days'))
                ->where('calendar_year_id', $yearObj->id)
                ->where('user_id', $userId)
                ->where('reference_type', "App\Models\Leave\Leave")
                ->groupBy('user_id', 'leave_type_id')
                ->get();

            foreach ($usedLeaves as $usedLeave) {

                $leaveObj = LeaveType::find($usedLeave->leave_type_id);

                if (!$leaveObj) {
                    $response['status'] = false;
                    $response['message'] = 'Leave type not found';
                    return $response;
                }
                $leave_code = $leaveObj->code;

                $response[$usedLeave->user_id]["username"] = $user->name;
                $response[$usedLeave->user_id]["userId"] = $user->id;
                $response[$usedLeave->user_id]["employee_id"] = $user->employee_id;
                $response[$usedLeave->user_id][$leave_code] = number_format($usedLeave->days, 1);
            }
            $response['status'] = true;
        }
        return $response;
    }
    /*--------------------------------------*/

    public static function saveData($request, $role = "user")
    {
        DB::beginTransaction();
        try {
            $loggedUser = Auth::user();
            if (!($loggedUser->hasRole('admin') || $loggedUser->hasRole('human-resources'))) {
                $request['status'] = 'pending';
            }
            $response = array('status' => true, 'message' => "success", 'id' => null);
            $action_date = date("Y-m-d");
            $leaveTypeId = isset($request['leave_type_id']) ? $request['leave_type_id'] : null;
            $leaveTypeObj = LeaveType::find($leaveTypeId);

            if (!$leaveTypeObj) {
                $response['status'] = false;
                $response['message'] = 'Leave Type not found';
                return $response;
            }

            $leave = new Leave();
            $leave->user_id = $request['user_id'];

            if ($request['start_date']) {
                $leave->start_date = date("Y-m-d", strtotime($request['start_date']));
            }
            if ($request['end_date']) {
                $leave->end_date = date("Y-m-d", strtotime($request['end_date']));
            }

            $leave->status = !empty($request['status']) ? $request['status'] : "pending";
            $leave->reason = isset($request['reason']) ? $request['reason'] : '';

            $leave->leave_type_id = isset($request['leave_type_id']) ? $request['leave_type_id'] : null;
            if ($leave->status == 'approved') {
                $approver_id = \Auth::id();
            }
            $user = User::find($leave->user_id);
            if ($leaveTypeObj->code == 'maternity' || $leaveTypeObj->code == 'paternity') {
                $leave->days = UserLeaveService::calculateMaternityDays($request['start_date'], $request['end_date'], $user->id); //userid
            } else {
                $leave->days = UserLeaveService::calculateWorkingDays($request['start_date'], $request['end_date'], $user->id);
            }
            //userid
            $leaveTypeObj = LeaveType::find($leave->leave_type_id);
            if ($leaveTypeObj->code == 'optional-holiday') {
                $leave->days = 1;
                $userPreviousLeave = UserLeaveService::checkUserOptionaLeave($user->id);
                if ($userPreviousLeave) {
                    //return $this->failResponse('You have already consumed your optional leave on '.date('dS M, Y', strtotime($userPreviousLeave->start_date)));
                    $response['status'] = false;
                    $response['message'] = 'You have already consumed your optional leave on ' . date('dS M, Y', strtotime($userPreviousLeave->start_date));
                    return $response;
                }

            }
            if ($leaveTypeObj->code == 'sick' && $leave->days == 1) {

                $half = !empty($request['half']) ? $request['half'] : null;
                if ($half && $half != 'full') {
                    $leave->half = $half;
                    $leave->days = 0.5;
                }
            }
            $year = date('Y');
            $calendarObj = CalendarYear::where('year', $year)->first();
            $leave->calendar_year_id = $calendarObj->id;
            if ($role == 'user') {

                $leaveTypeId = isset($request['leave_type_id']) ? $request['leave_type_id'] : null;
                $requestLeaveType = LeaveType::find($leaveTypeId);
                if (!$requestLeaveType) {
                    $response['status'] = false;
                    $response['message'] = 'Leave Type not found';
                    return $response;
                }
                if (empty($user->confirmation_date)) {
                    $response['status'] = false;
                    $response['message'] = 'Leave cannot be applied during probation period.';
                    return $response;
                }
                if ($requestLeaveType->code == 'paid') {
                    $response = UserLeaveService::validatePaidLeave($user->id, $request['start_date'], $request['end_date'], $requestLeaveType->id);
                    if (!$response['status']) {

                        return $response;
                    }
                }
            }

            if ($leave->save()) {

                $warning = UserLeaveService::getWarnings($leave->start_date, $leave->end_date, $leave->user_id, $leave->leave_type_id);

                if ($warning != null) {
                    $warningObj = LeaveWarning::saveData($leave->id, $warning);

                    if (!$warningObj) {
                        $response['status'] = false;
                        $response['message'] = 'Leave Warning not saved.';
                        return $response;
                    }
                }

                if ($leave->status == "pending") {

                    $resources = ProjectResource::where('user_id', $leave->user->id)->where('start_date', '<=', $leave->end_date)->where(function ($query) use ($leave) {
                        $query->where('end_date', null)->orWhere('end_date', '>=', $leave->start_date);
                    })->pluck('project_id')->toArray();
                    $projects = Project::with('projectManager')->whereIn('id', $resources)->get();
                    $array = [];
                    foreach ($projects as $project) {
                        if (empty($project->projectManager)) {
                            continue;
                        }
                        if (in_array($project->projectManager->id, $array)) {
                            continue;
                        }
                        if ($leave->user->id == $project->projectManager->id || $leave->user->parent_id == $project->projectManager->id) {
                            continue;
                        }
                        $approval = new ProjectManagerLeaveApproval();
                        $approval->user_id = $project->projectManager->id;
                        $approval->leave_id = $leave->id;
                        $approval->status = $leave->status;
                        if (!$approval->save()) {
                            $response['status'] = false;
                        } else {
                            $array[] = $project->projectManager->id;
                        }
                    }
                    if (!empty($request['overlap_reasons'])) {
                        self::saveOverlappingDetails($request['overlap_reasons'], $leave->id, $leave->user_id);
                    }
                    $response['id'] = $leave->id;
                    $response['leave'] = $leave;

                    // $request['calendar_year_id'] = $calendarObj->id;
                    // // $request['leave_type_id'] = $request['type'];
                    // $request['date'] = $request['end_date'];
                    // $request['days'] = $leave->days;
                    // $request['reference_type'] = 'App\Models\Leave\Leave';
                    // $request['reference_id'] = $leave->id;

                    // $transObj = UserLeaveTransaction::saveData($request);
                    // if (!$transObj) {
                    //     $response['status'] = false;
                    //     $response['errors'] = $leave->getErrors();
                    // }

                } else {

                    $response['id'] = $leave->id;
                    $response['leave'] = $leave;
                    self::approveLeave($leave->id, $approver_id, $action_date);
                }
            } else {

                $response['status'] = false;
                $response['errors'] = $leave->getErrors();
                return $response;
            }

            if ($leave->status == 'approved') {
                $request['calendar_year_id'] = $calendarObj->id;
                // $request['leave_type_id'] = $request['type'];
                $request['date'] = $request['end_date'];
                $request['days'] = $leave->days;
                $request['reference_type'] = 'App\Models\Leave\Leave';
                $request['reference_id'] = $leave->id;

                $transObj = UserLeaveTransaction::saveData($request);
                if (!$transObj) {
                    $response['status'] = false;
                    $response['errors'] = $leave->getErrors();
                }
            }

            if ($leave->status == 'pending') {
                event(new LeaveApplied($leave->id));
            }
            DB::commit();
            return $response;

        } catch (Exception $e) {
            $response['status'] = false;
            $response['errors'] = $leave->getErrors();
            $response['message'] = "Unable to add leave.";
            DB::rollback();
        }
        return $response;
    }

    public static function checkOverlappingLeave($userId, $startDate, $endDate, $leaveId = null)
    {

        $user = User::find($userId);
        $startDate = date_to_yyyymmdd($startDate);
        $endDate = date_to_yyyymmdd($endDate);
        $projects = $user->projects()->with('resources')->where('project_resources.start_date', '<=', $endDate)
            ->where(function ($query) use ($startDate, $endDate) {
                $query->where('project_resources.end_date', null)
                    ->orWhere('project_resources.end_date', '>=', $startDate);
            })->get();
        $data = [];
        $max = date_diff(date_create($startDate), date_create($endDate))->format('%a');
        $dates = [];
        $code = [];
        $holidays = Calendar::whereDate('date', '>=', $startDate)->whereDate('date', '<=', $endDate)->pluck('date')->toArray();
        $date = $startDate;
        for ($i = 0; $i <= $max; $i++) {
            $dates[] = date('D j M', strtotime($date));
            if (date('N', strtotime($date)) > 5) {
                $code[] = 1;
            } else {
                if (in_array($date, $holidays)) {
                    $code[] = 1;
                } else {
                    $code[] = 0;
                }
            }
            $date = date('Y-m-d', strtotime($date . ' +1 day'));
        }
        $count = 0;
        foreach ($projects as $project) {
            $coworkers = $project->resources()->with('leaves')->whereNotIn('project_resources.user_id', [$userId])
                ->where('start_date', '<=', $endDate)
                ->where(function ($query) use ($startDate, $endDate) {
                    $query->where('end_date', null)
                        ->orWhere('end_date', '>=', $startDate);
                })->get();
            $dataArray = [];
            $f = 0;
            if (!empty($coworkers)) {
                foreach ($coworkers as $coworker) {
                    $userDates = array_fill(0, count($dates), 0);
                    $flag = 0;
                    $leaves = $coworker->leaves()->where('leaves.start_date', '<=', $endDate)
                        ->where('leaves.end_date', '>=', $startDate)
                        ->whereIn('leaves.status', ['pending', 'approved'])->get();
                    if (!empty($leaves) && count($leaves) > 0) {
                        $flag = 1;
                        foreach ($leaves as $leave) {
                            $start = strtotime($leave->start_date) > strtotime($startDate) ?
                            date_diff(date_create($leave->start_date), date_create($startDate))->format('%a') :
                            0;
                            $end = strtotime($endDate) > strtotime($leave->end_date) ?
                            date_diff(date_create($leave->end_date), date_create($startDate))->format('%a') :
                            count($dates) - 1;
                            for ($i = $start; $i <= $end; $i++) {
                                $userDates[$i] = ucfirst($leave->type);
                            }
                        }
                    }
                    if ($flag == 1) {
                        $f = 1;
                        $leaveDetails = $coworker->leaves()->where('leaves.start_date', '<=', $endDate)
                            ->where('leaves.end_date', '>=', $startDate)->whereIn('leaves.status', ['pending', 'approved'])
                            ->first();
                        $name = $coworker->name;
                        $dataArray[] = ['name' => $name, 'dates' => $userDates, 'leave' => $leaveDetails];
                    }
                }
                if ($f == 1) {
                    $count++;
                    $name = $project->project_name;
                    $overlap_reason = '';
                    if ($leaveId != null) {
                        $overlap = LeaveOverlapRequest::where('project_id', $project->id)
                            ->where('leave_id', $leaveId)->where('user_id', $userId)->first();
                        $overlap_reason = !empty($overlap) ? $overlap->reason : '';
                    }
                    $data[] = ['name' => $name, 'users' => $dataArray, 'id' => $project->id, 'overlap_reason' => $overlap_reason];
                }
            }
        }
        return ['projects' => $data, 'count' => $count, 'dates' => $dates, 'code' => $code];
    }

    public static function validatePaidLeave($user_id, $start_date, $end_date, $leave_type_id)
    {
        $status = true;
        $message = '';
        // $duration = self::calculateWorkingDays($start_date, $end_date, $user_id);

        // // Duration check
        // if ($duration > 10) {
        //     $status = false;
        //     $message = 'More than 10 paid leaves can not be applied at a single instance.';
        //     $response['status'] = $status;
        //     $response['message'] = $message;
        //     return $response;
        // }
        // // Check for leave Balance
        // $year = date("Y");
        // $calendarYear = CalendarYear::where('year', $year)->first();
        // $from_date = $year . '-01-01';
        // $to_date = $year . '12-31';

        // $availableLeaves = self::countAvailableLeaves($year, $user_id, 1);
        // if (($availableLeaves - $duration) < 0) {
        //     $status = false;
        //     $message = 'Not Enough leaves left';
        //     $response['status'] = $status;
        //     $response['message'] = $message;
        //     return $response;
        // }
        // // Check for any leaves with end_date in last 20 days
        // $days = 20;
        // $endDate = strtotime($end_date);
        // $startDate = strtotime($start_date);
        // for ($i = $days; $i > 0; $i--) {
        //     $checkDate = date('Y-m-d', strtotime('-' . $i . 'days', $endDate));
        //     $leaveCount = Leave::where('user_id', $user_id)->whereDate('end_date', $checkDate)->where('status', 'approved')
        //         ->where('leave_type_id', $leave_type_id)->count();
        //     if ($leaveCount > 0) {
        //         $status = false;
        //         $message = 'Paid Leave was applied in the last 20 days.';
        //         $response['status'] = $status;
        //         $response['message'] = $message;
        //         return $response;
        //     }
        // }
        // // check for any leaves with start_date in next 20 days
        // for ($i = 1; $i <= $days; $i++) {
        //     $checkDate = date('Y-m-d', strtotime('+' . $i . 'days', $startDate));
        //     $leaveCount = Leave::where('user_id', $user_id)->whereDate('end_date', $checkDate)->whereIn('status', ['approved', 'pending'])
        //         ->where('leave_type_id', $leave_type_id)->count();
        //     if ($leaveCount > 0) {
        //         $status = false;
        //         $message = 'Paid Leave is already applied in the next 20 days.';
        //         $response['status'] = $status;
        //         $response['message'] = $message;
        //         return $response;
        //     }
        // }

        // if ($duration <= 3) {
        //     $checkDate = date('Y-m-d', strtotime('+3 days'));
        //     if ($start_date < $checkDate) {
        //         $status = false;
        //         $message = 'Paid Leave for less than 3 days should be applied 3 days in advance';
        //         $response['status'] = $status;
        //         $response['message'] = $message;
        //         return $response;
        //     }
        // } else {
        //     $checkDate = date('Y-m-d', strtotime('+15 days'));
        //     if ($start_date < $checkDate) {
        //         $status = false;
        //         $message = 'Paid Leave for more 3 days should be applied 15 days in advance';
        //         $response['status'] = $status;
        //         $response['message'] = $message;
        //         return $response;
        //     }
        // }

        $response['status'] = $status;
        $response['message'] = $message;
        return $response;
    }

    public static function approveLeave($leave_id, $approver_id, $action_date)
    {
        $leave = Leave::find($leave_id);
        if ($leave) {
            $leave->approver_id = $approver_id;
            $leave->action_date = $action_date;
            $leave->status = "approved";
            if ($leave->save()) {
                event(new LeaveApproved($leave->id));
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static function saveOverlappingDetails($overlap_reasons, $leave_id, $user_id)
    {
        foreach ($overlap_reasons as $overlap_reason) {
            try {
                $leaveOverlapObj = new LeaveOverlapRequest();
                $leaveOverlapObj->user_id = $user_id;
                $leaveOverlapObj->leave_id = $leave_id;
                $leaveOverlapObj->project_id = $overlap_reason['project_id'];
                $leaveOverlapObj->reason = !empty($overlap_reason['comment']) ? $overlap_reason['comment'] : null;
                if (!$leaveOverlapObj->save()) {
                }
            } catch (Exception $e) {
            }
        }
    }
    public static function getOptionaLeaves()
    {
        $startDate = date('Y-01-01');
        $endDate = date('Y-12-31');
        $optionalLeaves = NonworkingCalendar::whereBetween('date', [$startDate, $endDate])->where('type', 'Others')->orderBy('date', 'ASC')->get();
        return $optionalLeaves;
    }

    public static function checkUserOptionaLeave($userId, $year = '2019')
    {
        $userOptionalLeave = false;
        $userObj = User::find($userId);
        $leaveTypeObj = LeaveType::where('code', 'optional-holiday')->first();
        $calendarYearId = CalendarYear::getCalenderYear($year);
        if (!$userObj || !$leaveTypeObj || !$calendarYearId) {
            return $userOptionalLeave;
        }

        if ($leaveTypeObj && $calendarYearId) {
            $userOptionalLeave = Leave::where('calendar_year_id', $calendarYearId)->whereIn('status', ['pending', 'approved'])->where('user_id', $userId)->where('leave_type_id', $leaveTypeObj->id)->first();
        }

        return $userOptionalLeave;
    }

    public static function getWarnings($startDate, $endDate, $userId, $leaveTypeId = null)
    {
        $presentDate = date('Y-m-d');
        // if ($createdAt == null) {
        //     $presentDate = date('Y-m-d');
        //     dd(date('Y-m-d'));
        // } else {
        //     $presentDate = $createdAt;
        // }

        if ($endDate < $presentDate) {
            return null;
        }
        $duration = self::calculateWorkingDays($startDate, $endDate, $userId);

        $leaveTypeCode = " ";

        if ($leaveTypeId != null) {
            $leaveTypeObj = LeaveType::find($leaveTypeId);
            if ($leaveTypeObj) {
                $leaveTypeCode = $leaveTypeObj->code;
            }
        }
        if ($leaveTypeId != null && $leaveTypeCode == 'paid') {
            if ($duration > 10) {
                $message = 'More than 10 paid leaves can not be applied at a single instance.';
                return $message;
            }

            $year = date("Y");

            $availableLeaves = self::countAvailableLeaves($year, $userId, 1);

            if (($availableLeaves - $duration) < 0) {
                $message = 'Not Enough leaves left';
                return $message;
            }

            $days = 20;
            $end_date = strtotime($endDate);
            $start_date = strtotime($startDate);
            for ($i = $days; $i > 0; $i--) {
                $check_date = date('Y-m-d', strtotime('-' . $i . 'days', $end_date));
                $leaveCount = Leave::where('user_id', $userId)->whereDate('end_date', $check_date)->where('status', 'approved')
                    ->where('leave_type_id', $leaveTypeId)->count();
                if ($leaveCount > 0) {
                    $message = 'Paid Leave was applied in the last 20 days.';
                    return $message;
                }
            }

            for ($i = 1; $i <= $days; $i++) {
                $check_date = date('Y-m-d', strtotime('+' . $i . 'days', $start_date));
                $leaveCount = Leave::where('user_id', $userId)->whereDate('end_date', $check_date)->whereIn('status', ['approved', 'pending'])
                    ->where('leave_type_id', $leaveTypeId)->count();
                if ($leaveCount > 0) {
                    $message = 'Paid Leave is already applied in the next 20 days.';
                    return $message;
                }
            }

        }
        if ($duration <= 3) {
            $checkDate = self::getWorkingDays($presentDate, 3);
            if ($startDate < $checkDate) {
                $message = 'Leave for less than or equal to 3 days should be applied 3 working days in advance';
                return $message;
            }
        } else {
            $checkDate = self::getWorkingDays($presentDate, 15);
            // dd($presentDate);
            if ($startDate < $checkDate) {
                $message = 'Leave for more 3 days should be applied 15 working days in advance';
                return $message;
            }
        }
        return null;
    }

    public static function getWorkingDays($date, $days)
    {
        $checkDate = $date;
        $totalDays = 0;
        while ($totalDays != $days) {
            $checkDate = strtotime($checkDate . '+1 day');
            if (!(NonworkingCalendar::is_holiday(date("Y-m-d", $checkDate)) || NonworkingCalendar::isWeekend(date("Y-m-d", $checkDate)))) {
                $totalDays++;
            }
            $checkDate = date('Y-m-d', $checkDate);
        }
        return $checkDate;
    }

    public static function calculateMaternityDays($startDate, $endDate, $id)
    {
        $user = User::find($id);
        $count = 0;

        $startDate = date('Y-m-d', strtotime($startDate));
        $endDate = date('Y-m-d', strtotime($endDate));

        $holidays = Calendar::whereDate('date', '>=', $startDate)->whereDate('date', '<=', $endDate)->where('type', 'Holiday')->pluck('date')->toArray();
        for ($date = $startDate; strtotime($date) <= strtotime($endDate); $date = date('Y-m-d', strtotime($date . '+1 day'))) {
            $count++;
        }
        return $count;
    }

    public static function getUserLeavesCount($startDate, $endDate, $userId)
    {
        $getLeaves = Leave::where('user_id', $userId)->get(); //sum('days');

        $count = 0;
        foreach ($getLeaves as $leave) {
            if ($leave->start_date >= $startDate && $leave->start_date <= $endDate && $leave->end_date >= $startDate && $leave->end_date <= $endDate) {
                $count += self::calculateWorkingDays($leave->start_date, $leave->end_date, $userId);
            } elseif ($leave->start_date >= $startDate && $leave->start_date <= $endDate) {
                $count += self::calculateWorkingDays($leave->start_date, $endDate, $userId);
            } elseif ($leave->end_date >= $startDate && $leave->end_date <= $endDate) {
                $count += self::calculateWorkingDays($startDate, $leave->end_date, $userId);
            }
        }
        return $count;
    }

    public static function getUsersLeaveReport($yearId, $tillDate = null)
    {
        $response = [];
        $response['status'] = false;
        $response['message'] = '';
        $response['data'] = '';

        $usedLeavesList = null;

        $yearObj = CalendarYear::where('id', $yearId)->first();
        if (!$yearObj) {
            $response['status'] = false;
            $response['message'] = 'Calendar Year not found';
            return $response;
        }

        $yearStartDate = $yearObj->getFirstDay();

        if ($tillDate != null) {
            $usedLeaves = Leave::where('calendar_year_id', $yearObj->id)->whereDate('end_date', '<=', $tillDate)->where('status', 'approved')->get();
            $tillDataLeaves = Leave::whereDate('start_date', '<=', $tillDate)->whereDate('end_date', '>', $tillDate)->where('status', 'approved')->get();

            $usedLeavesList = $usedLeaves->merge($tillDataLeaves);

        } else {
            $usedLeavesList = Leave::where('calendar_year_id', $yearObj->id)->where('status', 'approved')->get();
        }

        foreach ($usedLeavesList as $usedLeave) {
            $user = User::find($usedLeave->user_id);
            if (!$user) {
                $response['status'] = false;
                $response['message'] = 'User not found';
                return $response;
            }
            $leaveObj = LeaveType::find($usedLeave->leave_type_id);
            if (!$leaveObj) {
                $response['status'] = false;
                $response['message'] = 'Leave type not found';
                return $response;
            }

            $leave_code = $leaveObj->code;
            if ($response[$usedLeave->user_id]["userId"] == $user->id) {

                if ($tillDate != null && $usedLeave->end_date > $tillDate) {
                    $interval = self::calculateWorkingDays($usedLeave->start_date, $tillDate, $user->id);
                    $response[$usedLeave->user_id][$leave_code] += $interval;
                } else {
                    $response[$usedLeave->user_id][$leave_code] += number_format($usedLeave->days, 1);
                }
            } else {
                $response[$usedLeave->user_id]["username"] = $user->name;
                $response[$usedLeave->user_id]["userId"] = $user->id;
                $response[$usedLeave->user_id]["employee_id"] = $user->employee_id;

                if ($tillDate != null && $usedLeave->end_date > $tillDate) {
                    // $datetime1 = new DateTime($tillDate);
                    // $datetime2 = new DateTime($usedLeave->start_date);
                    // $interval = $datetime1->diff($datetime2);
                    $interval = self::calculateWorkingDays($usedLeave->start_date, $tillDate, $user->id);
                    $response[$usedLeave->user_id][$leave_code] = $interval; //$interval->days + 1;
                } else {
                    $response[$usedLeave->user_id][$leave_code] = number_format($usedLeave->days, 1);
                }
            }

        }
        $response['status'] = true;

        return $response;
    }
    public static function getLeaveSummary($startDate, $endDate, $rmId =null) {
        $response = [];
        // if(empty($request->start_date)||empty($request->end_date)) {
        //     $start_date = date('Y-m-d');
        //     $end_date = date('Y-m-d', strtotime($start_date.'+30 days'));
        // } else {
        //     $start_date = $request->start_date;
        //     $end_date = $request->end_date;
        // }
        if($rmId != NULL) {
            $userIds = User::where('parent_id', $rmId)->where('is_active', 1)->pluck('id')->toArray();
        }
        $max = date_diff(date_create($startDate), date_create($endDate))->format('%a')+1;
        if($rmId != NULL){
            $leaves = Leave::with('user')->where('start_date', '<=', $endDate)->where('end_date', '>=', $startDate)->where('status', 'approved')->whereIn('user_id',$userIds)->get();
        }
        else {
            $leaves = Leave::with('user')->where('start_date', '<=', $endDate)->where('end_date', '>=', $startDate)->where('status', 'approved')->get();
        }

        $users  = $leaves->map(function ($leave){
                return $leave->user;
            })->unique();

        $dataArray = [];
        $date=$startDate;

        $count = array_fill(0,$max,0);

        $code=[];

        $holidays = Calendar::whereDate('date', '>=', $startDate)->whereDate('date', '<=', $endDate)->pluck('date')->toArray();


        for($i=0;$i<$max;$i++)
        {
            $temp =[];
            $temp['day'] = date('j', strtotime($date));
            $temp['date'] = date('D, j M', strtotime($date));
            $dates[] = $temp;
            if(date('N', strtotime($date)) > 5) {
                $code[] = 1;
            } else {
                if(in_array($date, $holidays)) {
                    $code[] = 1;
                } else {
                    $code[] = 0;
                }
            }
            $date = date('Y-m-d', strtotime($date . ' +1 day'));
        }
        $response['code'] = $code;
        $response['dates'] = $dates;
        foreach($users as $user)
        {
            // $user = $leave->user;
            $userDates = array_fill(0,count($dates),0);
            $leaves = $user->leaves()->where('leaves.start_date', '<=', $endDate)
                ->where('leaves.end_date', '>=', $startDate)
                ->where('leaves.status', 'approved')->get();
                foreach($leaves as $leave)
                {
                    $start = strtotime($leave->start_date) > strtotime($startDate) ?
                        date_diff(date_create($leave->start_date), date_create($startDate))->format('%a') :
                            0; 
                    $end = strtotime($endDate) > strtotime($leave->end_date) ?
                        date_diff(date_create($leave->end_date), date_create($startDate))->format('%a') :
                            $max-1;
                    for($i=$start;$i<=$end;$i++)
                    {
                        switch($leave->leaveType->code) {
                            case 'paid': $userDates[$i] = "PL";
                                         break;
                            case 'sick': $userDates[$i] = "SL";
                                        break;
                            case 'emergency': $userDates[$i] = "EL";
                                        break;
                            case 'marriage': $userDates[$i] = "MRGL";
                                        break;
                            case 'maternity': $userDates[$i] = "MTL";
                                        break;
                            case 'paternity': $userDates[$i] = "PTL";
                                        break;
                            case 'unpaid': $userDates[$i] = "UL";
                                        break;
                            case 'comp-off': $userDates[$i] = "CL";
                                        break;
                            case 'optional-holiday': $userDates[$i] = "OP";
                                        break;
                            default : $userDates[$i] = "UNKNOW";
                        }
                        $count[$i]++;
                    }
                }
            $name = $user->name;
            if(strlen($name) > 15) {
                $name = substr($name, 0, 13).'...';
            }
            $id = $user->employee_id != null ? $user->employee_id : 'ID: '.$user->id;
            $dataArray[] = ['name' => $name, 'dates' => $userDates, 'id' => $id];
        }
        $response['count'] = $count;
        $response['dataArray'] = $dataArray;
        return $response;
    }
}
