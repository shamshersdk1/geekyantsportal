<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Session;
use Exception;
use Config;
use Log;

class LeaveNotificationEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $array;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($array)
    {
        $this->array=$array;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        try {
            return $this->from(Config::get('app.noreply-email'), Config::get('app.name'))->subject('Employee Leave Application')->view('emails.employee-leave-handled');
        } catch (Exception $e) {
            \Log::info('LeaveNotificationEmail error');
            // print_r($e->getMessage());
            // \Log::info('Leave notification email');
            Session::flash('message', 'Mail not sent!');
            Session::flash('alert-class', 'alert-danger');
        }
    }
}
