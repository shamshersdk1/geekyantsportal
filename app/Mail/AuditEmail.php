<?php

namespace App\Mail;

use Session;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use OwenIt\Auditing\Models\Audit;
use App\Services\Payroll\AppraisalService;

use App\Models\AccessLog;

class AuditEmail extends Mailable
{
    use Queueable, SerializesModels;
    
    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = "Daily Audit Report for GeekyAnts: ".($this->data["date"] ? $this->data["date"] : "");
        $data = $this->data;
        $pdf = \PDF::loadView('pages.admin.audit.pdf', compact('data'))->setPaper('a4', 'portrait');
        try {
            return $this->from(
                config('app.noreply-email'), 
                config('app.name')
            )->subject(
                $subject
            )->view('emails.audit')
            ->attachData($pdf->output(), "Audit Report ".$this->data["date"].'.pdf');

        } catch (Exception $e) {

            AccessLog::accessLog(null, 'App\Mail', 'AuditEmail', 'build', 'catch-block', $e->getMessage());
            Session::flash('message', 'Mail not sent!');
            Session::flash('alert-class', 'alert-danger');
        }
    }
}
