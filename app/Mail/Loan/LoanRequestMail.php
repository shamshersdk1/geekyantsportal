<?php

namespace App\Mail\Loan;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Session;
use Exception;
use Config;

class LoanRequestMail extends Mailable
{
    use Queueable, SerializesModels;
    public $array;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($array)
    {
        $this->array=$array;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        try {
            $array = $this->array;
            return $this->from(Config::get('app.noreply-email'), Config::get('app.name'))->subject('Loan Request')->view('emails.Loan.loan-request');
        } catch (Exception $e) {
            Session::flash('message', 'Mail not sent!');
            Session::flash('alert-class', 'alert-danger');
        }
    }
}
