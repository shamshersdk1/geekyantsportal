<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactUsMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $data;

    public function __construct($data)
    {
        //
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        /*Mail::send('emails.message',array('content'=>$request->get('message'),'phone'=>$phone,'email'=>$request->get('email'),'name'=>$name), function($message)
                {
                    $message->to('leads@sahusoft.com','Leads')->subject('GeekyAnts-Website');
                    $message->from('support@geekyants.com');
                });*/

        return $this->view('emails.message')->with([
                        'content' => $this->data['content'],
                        'phone' => $this->data['phone'],
                        'email' => $this->data['email'],
                        'name' => $this->data['name'],
                    ]);

    }
}
