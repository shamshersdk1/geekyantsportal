<?php namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\Inspire',
        'App\Console\Commands\SynchronizeSlackUser',
        'App\Console\Commands\SlackReminderCommand',
        'App\Console\Commands\DeactivateUserCommand',
        'App\Console\Commands\SlackLeaveReminderCommand',
        'App\Console\Commands\SlackTimesheetReminderCommand',
        'App\Console\Commands\RequestApproval\SendSlackNotification',
        'App\Console\Commands\Timesheet\SlackTeamLeadReminderCommand',
        'App\Console\Commands\Timesheet\SlackUserReminderCommand',
        'App\Console\Commands\Timesheet\SlackRMReminderCommand',
        'App\Console\Commands\Timesheet\TimesheetLegacyRecordFixCommand',
        'App\Console\Commands\AddUserSettings',
        'App\Console\Commands\Feedback\GenerateFeedbackUserRecordCommand',
        'App\Console\Commands\Timesheet\TimesheetUpdateStatusCommand',
        'App\Console\Commands\Timesheet\TimesheetManagerUpdateCommand',
        'App\Console\Commands\Cron\CronCheckCommand',
        'App\Console\Commands\AssignUserIP',
        'App\Console\Commands\Payroll\GenerateMonthlyPayroll',
        'App\Console\Commands\Payroll\CalculateTDS',
        'App\Console\Commands\LeaveUpdateUserBalanceTable',
        'App\Console\Commands\UserLeaveDeduction',
        'App\Console\Commands\MonthCreater',
        'App\Console\Commands\WeekCreatorCommand',
        'App\Console\Commands\Payroll\GeneratePayrollGroupMonth',
        'App\Console\Commands\SlackBonusReminder',
        'App\Console\Commands\UserItSavingMonthly',
        'App\Console\Commands\MigrateBonusRequest',
        //'App\Console\Commands\Timesheet\UserTimesheetLockUpdateCommand',
        'App\Console\Commands\SendSlackNotificationToCritical',
        'App\Console\Commands\FingerPrintUpdate',
        'App\Console\Commands\MoveUserDetails',
        'App\Console\Commands\Attendance\SlackUserAttendance',
        'App\Console\Commands\Attendance\SlackPresentUserAttendance',
        'App\Console\Commands\Attendance\SlackFemalePresentUserAttendance',
        'App\Console\Commands\Attendance\GenerateUserRegisterAttendance',
        'App\Console\Commands\Attendance\DeletePrevoiusAttendanceData',
        'App\Console\Commands\UpdateLeaveBalance',
        'App\Console\Commands\AuditEmail',
        'App\Console\Commands\Loan\SlackLoanCloserReminder',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('inspire')->hourly();
        $schedule->command('synchronize-slack-user')->twiceDaily(1, 13);
        $schedule->command('slack:pending-bonus-request-reminder')->monthlyOn(15, '12:00');
        $schedule->command('slack:send-request-notifications')->twiceDaily(11, 15);
        $schedule->command('slack:send-team-lead-timesheet-notification')->weekdays()->at('12:30');
        $schedule->command('slack:send-user-timesheet-notification')->weekdays()->at('11:00');
        $schedule->command('slack:send-rm-timesheet-notification')->weekdays()->at('11:00');
        // $schedule->command('command:generate-feedback-user-records')->monthlyOn(1, '12:00');
        // $schedule->command('slack-reminder')->dailyAt("18:00");
        // $schedule->command('deactivate-user')->dailyAt("21:00");
        // $schedule->command('reminder:timesheet')->dailyAt("12:00");
        // $schedule->command('command:generate-invoice')->weekdays()->at('11:00');
        $schedule->command('command:check-for-cron-jobs')->everyMinute();
        $schedule->command('command:generatePayrollGroupMonthly')->monthly();
        $schedule->command('command:updateItSavingMonth')->monthlyOn(21, '01:00');

        //$schedule->command('command:update-users-timesheet-lock')->dailyAt('00:01');
        $schedule->command('command:send-slack-notification-to-critical')->dailyAt('10:00');
        $schedule->command('slack:user-attendance-status-update')->twiceDaily(13, 15);
        $schedule->command('slack:user-attendance-present-list')->dailyAt(20);
        $schedule->command('command:generate-user-attendance-register')->dailyAt('5:55');
        $schedule->command('command:attendance-delete-previuos-data')->dailyAt('2');
        $schedule->command('command:create-month-record')->monthly();
        $schedule->command('slack:user-female-attendance-present-list')->dailyAt('19:35');
        $schedule->command('slack:loan-closer-reminder')->monthlyOn(1, '11:00');
        $schedule->command('command:audit-email')->dailyAt('09:00');

    }
}
