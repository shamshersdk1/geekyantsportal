<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use violuke\RsaSshKeyFingerprint\FingerprintGenerator;
use App\Events\SshKey\StoreSshFingerprint;
use Log;
use App\Models\SshKey;

class FingerPrintUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:update-ssh-fingerprint';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sshKeys = SshKey::where('fingerprint',null)->orWhere('fingerprint','')->get();
        if(!$sshKeys->count()){
            return false;
        }
        foreach($sshKeys as $sshKey)
        {
            try{
                $fingerprint = FingerprintGenerator::getFingerprint($sshKey->value);
            }
            catch (\Exception $exception) {
                $fingerprint = null;
            }
            $sshKey->fingerprint = $fingerprint;
            $sshKey->update();
        }
    }
}
