<?php

namespace App\Console\Commands;

use App\Services\UserService;
use Illuminate\Console\Command;

class UpdateLeaveBalance extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:update-leave-balance {userId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add leave balance of new employees.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $year = date('Y');
        UserService::updateUserLeaveBalance($this->argument('userId'), $year);
    }
}
