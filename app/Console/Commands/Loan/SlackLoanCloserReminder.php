<?php

namespace App\Console\Commands\Loan;

use App\Jobs\Loan\LoanCloserNotification;
use Illuminate\Console\Command;
use App\Models\Loan;
use App\Services\SlackService\SlackMessageService\SlackMessageService;

class SlackLoanCloserReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'slack:loan-closer-reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends notification on slack whose loan closer is remaining.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $channelId = \Config::get('messaging-app.loan_request_channel');
        $url = \Config::get('app.geekyants_portal_url');
        $loans = Loan::where('status','in_progress')->get();
        $msg = "List of <".$url."/admin/loans|Loan(s)> to be closed: \n";
        $status = false;
        foreach($loans as $loan)
        {
            $remainingAmount = $loan->emis()->where('status','paid')->sum('amount')+$loan->amount;
            if($remainingAmount<=0)
            {
                $msg = $msg.$loan->user->name." (".$loan->user->employee_id.")"." | "."Loan : <".$url."/admin/loans/".$loan->id." | #".$loan->id." > | Loan Amount : ".$loan->amount."\n";
                $status = true;
            }
        }
        if($status)
            SlackMessageService::sendMessageToChannel($channelId,$msg);
    }
}
