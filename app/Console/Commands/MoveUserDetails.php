<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Models\UserDetail;
use Illuminate\Console\Command;

class MoveUserDetails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:move-user-detail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Move User\'s Permanent Details from User table to User Details table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $response = [];
        $response['status'] = true;
        $response['errors'] = '';
        $users = User::all();
        if ($users && count($users) > 0) {
            foreach ($users as $user) {
                $detailObj = UserDetail::where('user_id', $user->id)->first();
                if (!$detailObj) {
                    $detailObj = new UserDetail;
                    $detailObj->user_id = $user->id;
                }
                $detailObj->aadhar_no = null;
                $detailObj->pan = $user->pan;
                $detailObj->bank_ac_no = $user->bank_ac_no;
                $detailObj->bank_ifsc_code = $user->bank_ifsc_code;
                $detailObj->pf_no = $user->pf_no;
                $detailObj->uan_no = $user->uan_no;
                $detailObj->esi_no = $user->esi_no;

                if (!$detailObj->save()) {
                    $response['status'] = false;
                    $response['errors'] = $response['errors'] . "User Detail for " . $user->id . "not saved \n";
                }
            }
        }
        \Log::info($response);
    }
}
