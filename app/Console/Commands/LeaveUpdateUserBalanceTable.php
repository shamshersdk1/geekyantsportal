<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Services\UserService;
use Illuminate\Console\Command;

class LeaveUpdateUserBalanceTable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:update-user-leave-balance-table';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates the user leave balance table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $year = date('Y');
        // $year = 2018;
        $users = User::Where('is_active', 1)->get();
        $chunks = $users->chunk(10);
        // foreach ($chunks as $chunk) {
        // foreach ($chunk as $user) {
        foreach ($users as $user) {
            UserService::updateUserLeaveBalance($user->id, $year);
        }
        // }
        // }
    }
}
