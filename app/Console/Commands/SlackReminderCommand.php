<?php

namespace App\Console\Commands;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use Config;
use Exception;
use App\Models\Admin\SlackUser;
use App\Models\Admin\Timelog;
use App\Models\Admin\Project;
use App\Models\Admin\ProjectSprints;
use App\Models\Admin\SprintResource;
use App\Models\User;
use App\Models\Admin\Calendar;
use App\Jobs\Slack\SlackReminder;
use Log;
class SlackReminderCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'slack-reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'remindes project allocated user if they have not time logged for the day';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now = date("Y-m-d");
        $calendar = Calendar::where('date',$now)->first();
        if($calendar) {
            \Log::info('Non working day'.$now);
        } else {
            $projects = Project::where('email_reminder',1)->get();

            foreach ($projects as $project) {
                if(empty($project->channel_for_developer)) {
                    continue;
                }
                $sprints = ProjectSprints::where('project_id',$project->id)->where('start_date','<=',$now)->where('end_date','>=',$now)->get();

                foreach ($sprints as $sprint) {
                    $resources = SprintResource::where('sprint_id',$sprint->id)->get();

                    //check if user is not on leave and its working day
                    foreach ($resources as $resource) {
                        $user = User::find($resource->employee_id);
                        if($user) {
                             Log::info('Job created'.$user->name);

                            $data['message'] = config('app.log_reminder');
                            $data['userEmail'] = $user->email;
                            $data['channelName'] = $project->channel_for_developer;
                            
                            dispatch(new SlackReminder($data));

                            Log::info('Job pushed');
                        }
                    }
                }
            }
        }
    }
}
