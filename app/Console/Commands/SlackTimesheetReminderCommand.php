<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\SlackService\SlackMessageService\Reminder\Timesheet;

class SlackTimesheetReminderCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:timesheet';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends a slack message to all users and their team lead who haven\'t filled the timesheet in the last 3 working days';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Timesheet::remind();
    }
}
