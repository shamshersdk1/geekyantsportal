<?php

namespace App\Console\Commands\RequestApproval;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use Config;
use Exception;
use App\Models\Admin\RequestApproval;
use App\Models\Admin\RequestApproveUser;
use App\Jobs\Slack\SlackReminder\RequestApproval\NotifyRequestor;

class SendSlackNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'slack:send-request-notifications';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send out all the pending request approval notifications';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $current_date = date('Y-m-d');
        $pendingRequestList = RequestApproval::where('status','pending')->where('reminder_date','<=',$current_date)->get();
        foreach( $pendingRequestList as $pendingRequest )
        {
            $job = (new NotifyRequestor($pendingRequest->id))->onQueue('request-approval');
            dispatch($job);
        }

    }
}
