<?php

namespace App\Console\Commands;

use App\Models\Admin\ItSavingMonth;
use Illuminate\Console\Command;

class UserItSavingMonthly extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:updateItSavingMonth';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add user\'s it savings in it-saving-monthly model';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ItSavingMonth::createRecord();

    }
}
