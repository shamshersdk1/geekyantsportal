<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\User;


class DeactivateUserCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deactivate-user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deactivates users at the end of the day when their release date has passed';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::with('profile')->whereNotNull('release_date')->where('release_date', '!=', "0000-00-00")->where('release_date', '<=', date('Y-m-d'))->where('is_active', 1)->get();
        foreach($users as $user)
        {
            $user->profile->is_active = 0;
            $user->profile->save();
            $user->slackUsers()->detach();
            $user->is_active = 0;
            if(!$user->save()) {
                \Log::info($user->getErrors());
            }
        }
    }
}
