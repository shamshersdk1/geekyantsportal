<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Services\TimesheetService;
use Illuminate\Console\Command;

class SendSlackNotificationToCritical extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:send-slack-notification-to-critical';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send notification to users whoes timesheet is not filled.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $currentDate = date('Y-m-d');
        $users = User::where('is_active', '1')->get();
        if (!$users->count() > 0) {
            return false;
        }
        foreach ($users as $user) {
            $userSetting = $user->settings->where('key', 'timesheet-required')->where('value', '1');
            $userProject = $user->projects->where('is_critical', '1');
            if ($userSetting->count() > 0 && $userProject->count() > 0) {
                TimesheetService::sendUserSlackNotification($user->id, $currentDate);
            }
        }
    }
}
