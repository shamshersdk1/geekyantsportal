<?php

namespace App\Console\Commands;

use App\Models\Admin\BillingSchedule;
use App\Services\BillingScheduleService;
use App\Services\GoogleCalendarService\GoogleCalendarService;
use Carbon\Carbon;
use Illuminate\Console\Command;

class AutoGenerateInvoice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:auto-generate-invoice';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'auto generate invoice for the projects after specific time';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $BillingSchedules = BillingSchedule::where('status', 'ongoing')->where('to_date', '>=', Carbon::today())->orwhere('to_date', null)->with('items')->get();
        if ($BillingSchedules) {
            foreach ($BillingSchedules as $BillingSchedule) {
                if ($BillingSchedule->type == 'fixed') {
                    $fromDate = $BillingSchedule->from_date;
                    $billDate = $BillingSchedule->to_date;
                } else {
                    $fromDate = BillingScheduleService::getFromDate($BillingSchedule->id);
                    $cycle = BillingScheduleService::checkCycle($BillingSchedule->id);
                    if ($BillingSchedule->term == 'weekly') {
                        $billDate = date('Y-m-d', strtotime($cycle, strtotime($fromDate)));
                    }
                    if ($BillingSchedule->term == 'bi-weekly') {
                        $fromDate1 = Carbon::parse($fromDate)->addWeeks(1);
                        $billDate = date('Y-m-d', strtotime($cycle, strtotime($fromDate1)));
                    }
                    if ($BillingSchedule->term == 'monthly') {
                        if ($cycle == 'first day of month') {
                            $billDate = GoogleCalendarService::firstWorkingDay($fromDate);
                        } else if ($cycle == 'last day of month') {
                            $billDate = GoogleCalendarService::lastWorkingDay($fromDate);
                        }
                    }
                }
                if (Carbon::today() == $billDate) {
                    $res = BillingScheduleService::generateBill($BillingSchedule->id, $fromDate, $billDate);
                }
            }
        }
    }

}
