<?php

namespace App\Console\Commands\Attendance;

use App\Services\AttendanceReportService;
use App\Models\UserAttendance;
use App\Models\NonworkingCalendar;
use Illuminate\Console\Command;
use App\Jobs\MessagingApp\PostMessageChannelJob;

class SlackFemalePresentUserAttendance extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'slack:user-female-attendance-present-list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notify on slack channel for user attendance status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = date('Y-m-d');
        $isWorkingDay = false;
        if(NonworkingCalendar::is_holiday($date) || NonworkingCalendar::isWeekend($date)) { 
            $isWorkingDay = true;
        }
        if (!$isWorkingDay) {
            $userAttendances = UserAttendance::where('updated_at','>=', date('Y-m-d 19:30'))->get();
            $userIds = [];
            $channelId = \Config::get('messaging-app.user_attendance_channel');    
            $template = "Attendance Report";
            $text ='';
            $count =0;
            if(count($userAttendances)>0) {
                foreach ($userAttendances as $userAttendance) {
                    if($userAttendance->user->gender =='female') {
                        $count++;
                        $text .= $userAttendance->user->employee_id . ' | ' . $userAttendance->user->name . "\n";
                    }
                }
            }
            if($count > 0) {
                $attachments['title'] = 'List of Female Employee present in the office after 7:30pm';
                $attachments['fallback'] = 'List of Female Employee present in the office after 7:30pm';
                $attachments['pretext'] = 'List of Female Employee present in the office after 7:30pm';
                $attachments['text'] = $text;
                $job = (new PostMessageChannelJob($channelId, $template, $attachments));
                dispatch($job);
            }
        }
    }
}
