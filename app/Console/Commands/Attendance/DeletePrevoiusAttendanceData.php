<?php

namespace App\Console\Commands\Attendance;

use App\Services\AttendanceReportService;
use App\Models\UserAttendance;
use App\Models\GatewayIpConnection;
use App\Models\GatewayArp;
use Illuminate\Console\Command;

class DeletePrevoiusAttendanceData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:attendance-delete-previuos-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete the old record from user_attendance and gateway_ip_connections table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date =  date('Y-m-d', strtotime('-7 days'));
        UserAttendance::where('date','<', $date)->delete();
        GatewayIpConnection::where('date','<',$date)->delete();
        GatewayArp::where('date','<',$date)->delete();
    }
}
