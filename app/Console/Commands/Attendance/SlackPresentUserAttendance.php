<?php

namespace App\Console\Commands\Attendance;

use App\Services\AttendanceReportService;
use App\Models\UserAttendance;
use App\Models\NonworkingCalendar;
use Illuminate\Console\Command;

class SlackPresentUserAttendance extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'slack:user-attendance-present-list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notify on slack channel for user attendance status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = date('Y-m-d');
        if(NonworkingCalendar::is_holiday($date) || NonworkingCalendar::isWeekend($date)) { 
            $viewData = UserAttendance::getReport($date);
            // List of ppl present today
            // check the index $viewData['active_users];
            if (isset($viewData['active_users']) && count($viewData['active_users']) > 0) {
                $userIds = [];
                foreach ($viewData['active_users'] as $activeUser) {
                    $userIds[] = $activeUser['UserAttendance']->user->id;
                }
                $template_path = 'messaging-app.slack.user-attendance.present-list';
                AttendanceReportService::updateActiveUserStatusOnSlack($template_path, $userIds);
            }
        }
    }
}
