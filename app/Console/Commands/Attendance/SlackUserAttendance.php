<?php

namespace App\Console\Commands\Attendance;

use App\Services\AttendanceReportService;
use App\Models\UserAttendance;
use App\Models\NonworkingCalendar;
use Illuminate\Console\Command;

class SlackUserAttendance extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'slack:user-attendance-status-update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notify on slack channel for user attendance status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = date('Y-m-d');
        $isWorkingDay = false;
        if(NonworkingCalendar::is_holiday($date) || NonworkingCalendar::isWeekend($date)) { 
            $isWorkingDay = true;
        }
        if (!$isWorkingDay) {
            $viewData = UserAttendance::getReport($date);
            // List of ppl with unknown status
            // check the index $viewData['non_active_users];
            if (isset($viewData['non_active_users']) && count($viewData['non_active_users']) > 0) {
                $userIds = [];
                foreach ($viewData['non_active_users'] as $nonActiveUser) {
                    $userIds[] = $nonActiveUser->id;
                }
                AttendanceReportService::updateUserStatusOnSlack($userIds);
            }
        }
    }
}
