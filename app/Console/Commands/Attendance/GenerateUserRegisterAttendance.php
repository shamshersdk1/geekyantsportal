<?php

namespace App\Console\Commands\Attendance;

use App\Services\AttendanceReportService;
use App\Models\UserAttendance;
use App\Models\NonworkingCalendar;
use Illuminate\Console\Command;

class GenerateUserRegisterAttendance extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:generate-user-attendance-register';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate the user attendance report at 6am daily';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = date('Y-m-d');
        AttendanceReportService::generateAttendanceRegister($date);
    }
}
