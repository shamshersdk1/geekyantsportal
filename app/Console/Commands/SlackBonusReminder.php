<?php

namespace App\Console\Commands;

use App\Jobs\MessagingApp\PostMessageChannelJob;
use App\Models\BonusRequest;
use App\Models\Month;
use Illuminate\Console\Command;

class SlackBonusReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'slack:pending-bonus-request-reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reminds Channel: If there are any pending bonus requests';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $channelId = \Config::get('messaging-app.bonus_channel');
        $now = date("Y-m-d", strtotime("last day of -1 month"));
        $monthObj = Month::getMonthByDate($now);
        $pendingBR = BonusRequest::where('month_id', $monthObj->id)->where('status', "pending")->count();

        $url = config('app.geekyants_portal_url') . "/admin/bonus-request-pending/" . $monthObj->id;
        $attachment['title'] = "Pending Bonus Request";
        $attachment['fallback'] = "View Bonus Request";
        $attachment['title_link'] = "admin/bonus-request-pending/" . $monthObj->id;
        $attachment['color'] = "warning";
        $attachment['actions'] = [["type" => "button", "name" => "View", "text" => "View Pending Bonus Requests", "url" => $url, "style" => "primary"]];

        if ($pendingBR > 0) {
            $message = "Reminder to process pending bonus request(s) for the month of *" . $monthObj->formatMonth() . "*";
            $job = (new PostMessageChannelJob($channelId, $message, $attachment));
            dispatch($job);
        }
    }
}
