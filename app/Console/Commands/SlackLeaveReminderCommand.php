<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Admin\Leave;
use App\Models\User;
use App\Models\ProjectManagerLeaveApproval;

use App\Services\SlackService\SlackMessageService\SlackMessageService;

use stdClass;

class SlackLeaveReminderCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'slack-leave-reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remindes the Project managers and Reporting managers if they have pending leave requests';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $today = date('Y-m-d 00:00:00');
        $result = [];
        $pendingPMApprovals = ProjectManagerLeaveApproval::with('manager')->where('status', 'pending')->where('created_at', '<', $today)->groupBy('user_id')->get();
        foreach($pendingPMApprovals as $obj)
        {
            if(!empty($obj->manager)) {
                $pendingApprovals = ProjectManagerLeaveApproval::with('leave')->where('user_id', $obj->manager->id)->where('status', 'pending')->where('created_at', '<', $today)->get();
                foreach($pendingApprovals as $approval)
                {
                    if(!empty($approval->leave) && ($approval->leave->status == 'pending')) {
                        if($approval->leave->type == 'half') {
                            $text = "*Half-day Leave* on ".date_to_ddmmyyyy($approval->leave->start_date)." in ".ucfirst($approval->leave->half)." Half";    
                        } else {
                            $type = ucfirst($approval->leave->type)." Leave" ;
                            $text = "*".$type."* from ".date_to_ddmmyyyy($approval->leave->start_date)." to ".date_to_ddmmyyyy($approval->leave->end_date);
                        }
                        if($text != "") {
                            $attachment = new stdClass;
                            $attachment->fallback = "Unable to fetch leave details, please check manually";
                            $attachment->color = "#EF3B40";
                            $attachment->title = $approval->leave->user->name;
                            $attachment->text = $text;
                            $attachment->mrkdwn_in = ["text"];
                            $result[] = $attachment;
                        }
                    }
                }
            }
        }
        $uniqueUsers = User::with('reportingManager')->where('is_active', 1)->groupBy('parent_id')->get();
        foreach($uniqueUsers as $obj)
        {
            if(($obj->reportingManager) && !empty($obj->reportingManager->children)) {
                $userArray = $obj->reportingManager->children()->pluck('id')->toArray();
                $pendingLeaves = Leave::with('user', 'projectManagerApprovals')->whereIn('user_id', $userArray)->where('status', 'pending')->where('created_at', '<', $today)->get();
                foreach($pendingLeaves as $leave)
                {
                    if($leave->projectManagerApprovals()->where('status', 'pending')->count() == 0) {
                        if($leave->type == 'half') {
                            $text = "*Half-day Leave* on ".date_to_ddmmyyyy($leave->start_date)." in ".ucfirst($leave->half)." Half";    
                        } else {
                            $type = ucfirst($leave->type)." Leave" ;
                            $text = "*".$type."* from ".date_to_ddmmyyyy($leave->start_date)." to ".date_to_ddmmyyyy($leave->end_date);
                        }
                        if($text != "") {
                            $attachment = new stdClass;
                            $attachment->fallback = "Unable to fetch leave details, please check manually";
                            $attachment->color = "#EF3B40";
                            $attachment->title = $leave->user->name;
                            $attachment->text = $text;
                            $attachment->mrkdwn_in = ["text"];
                            $result[] = $attachment;
                        }
                    }
                }
            }
        }    
        if(!empty($result)) {
            $name = $obj->reportingManager->name;
            $url = config('app.geekyants_portal_url')."/admin/reporting-manager-leaves";
            $message = "Hey ".$name.", please goto\n".$url."\nand approve or reject the following leaves";
            SlackMessageService::sendMessageToUser($obj->reportingManager->id, $message, $result);
        }
    }
}
