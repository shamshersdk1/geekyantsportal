<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use Config;
use Exception;
use App\Models\Admin\SlackUser;
use App\Models\Admin\Timelog;

class SynchronizeSlackUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'synchronize-slack-user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synchorizes the Slack Users to slack_user table in database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try{
            $client = new Client(['base_uri' => config('app.slack_url')]);
            $response = $client->request("POST", "users.list", [
                'form_params' => [
                    'token' => config('app.slack_token'),
                    'limit' => 100
                ]
                ]);
        } catch(Exception $e) {
            echo $e->getMessage();
        }
        $userCreateResponse =  $response->getBody();
        $response = json_decode($userCreateResponse);
        $slackUsers = $response->members;
        $message = SlackUser::saveSlackUsers($slackUsers);
        if($message != null) {
            echo $message;
        }
        if(!empty($response->response_metadata) && !empty($response->response_metadata->next_cursor)) {
            while(!empty($response->response_metadata->next_cursor))
            {
                try{
                    $client = new Client(['base_uri' => config('app.slack_url')]);
                    $response = $client->request("POST", "users.list", [
                        'form_params' => [
                            'token' => config('app.slack_token'),
                            'limit' => 100,
                            'cursor' => $response->response_metadata->next_cursor
                        ]
                    ]);
                } catch(Exception $e) {
                    echo $e->getMessage();
                }
                $userCreateResponse =  $response->getBody();
                $response = json_decode($userCreateResponse);
                $slackUsers = $response->members;
                $message = SlackUser::saveSlackUsers($slackUsers);
                if($message != null) {
                    echo $message;
                }
            }
        }
    }
}
