<?php

namespace App\Console\Commands;

use App\Models\Admin\IpAddressRange;
use App\Models\Admin\UserIpAddress;
use App\Models\User;
use Illuminate\Console\Command;
use Log;

class AssignUserIP extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:assign-ip-to-all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'auto assign 10 ip addresses to all users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // $rangeId = \Config::get('ip-address.id');
        $rangeId = 2;
        $range = IpAddressRange::find($rangeId);
        $start_ip = ip2long($range['start_ip']);
        $end_ip = ip2long($range['end_ip']);

        $users = User::where('is_active', 1)->get();
        foreach ($users as $user) {
            for ($i = 0; $i < 10; $i++) {
                while ($start_ip <= $end_ip) {
                    $ip = long2ip($start_ip);
                    $isUsed = UserIpAddress::where('ip_address', $ip)->first();
                    if (!$isUsed) {
                        $freeIp = $ip;
                        break;
                    }
                    $start_ip++;
                }
                if (!empty($freeIp)) {
                    $assignIp = new UserIpAddress;
                    $assignIp->user_id = $user->id;
                    $assignIp->ip_address = $freeIp;
                    $assignIp->ip_address_long = ip2long($freeIp);
                    $assignIp->assigned_by = 0;
                    $assignIp->save();
                    $start_ip = ip2long($freeIp);
                    $freeIp = null;
                }
            }
        }
        \Log::info('No more ip available in the given range');
    }
}
