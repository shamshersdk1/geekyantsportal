<?php

namespace App\Console\Commands\Payroll;

use App\Services\Payroll\PayrollService;
use Illuminate\Console\Command;

class CalculateTDS extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:payroll-calculate-monthly-tds';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Monthly Payroll';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $month = 4; //date('m');
        $year = 2018; //date('Y');
        PayrollService::calculateMonthlyTDS($month, $year);

    }
}
