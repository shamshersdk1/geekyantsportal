<?php

namespace App\Console\Commands\Payroll;

use App\Models\MonthSetting;
use App\Services\Payroll\PayrollService;
use Illuminate\Console\Command;

class GenerateMonthlyPayroll extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:generate-monthly-payroll';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Monthly Payroll';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $month = date('m');
        $year = date('Y');
        PayrollService::createMonthlyPayroll($month, $year);

        $monthSettingObj1 = new MonthSetting();
        $monthSettingObj1->month_id = $month;
        $monthSettingObj1->key = "payroll-rule-data";
        $monthSettingObj1->value = "pending";
        $monthSettingObj1->save();

        $monthSettingObj2 = new MonthSetting();
        $monthSettingObj1->month_id = $month;
        $monthSettingObj1->key = "feedback-review";
        $monthSettingObj1->value = "pending";
        $monthSettingObj1->save();

    }
}
