<?php

namespace App\Console\Commands\Payroll;

use Illuminate\Console\Command;
use App\Models\Admin\Finance\PayroleGroupRuleMonth;
use Illuminate\Support\Facades\Log;

class GeneratePayrollGroupMonth extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:generatePayrollGroupMonth';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $response = PayroleGroupRuleMonth::generatePayrollGroupRuleMonthly();
        if($response)
            Log::info("Successfully added Payroll");
        else  
            Log::error("Failed to add Payroll");  
    }
}
