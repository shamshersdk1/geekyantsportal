<?php

namespace App\Console\Commands;

use App\Models\Month;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Exception;
use App\Models\AccessLog;

class MonthCreater extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:create-month-record';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try{
            $response = Month::monthCreate();
            if ($response['status'] == false) {
                Log::error("Month Entry Failed" . $response['message']);
                throw new Exception($response['message']);
            }
        }
        catch (Exception $e) {
            AccessLog::accessLog(null, 'App\Console\Commands\MonthCreater', 'GenerateMonth', 'handle', 'catch-block', $e->getMessage());
        }
    }
}
