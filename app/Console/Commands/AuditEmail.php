<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use OwenIt\Auditing\Models\Audit;
use App\Jobs\AuditMail;
use App\Services\Payroll\AppraisalService;

class AuditEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:audit-email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '*** Send audit report ***';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $fromDate = date('Y-m-d', strtotime("-1 days"));
        $toDate = date('Y-m-d');
        $job = (new AuditMail($fromDate,$toDate));
        dispatch($job);
    }
}
