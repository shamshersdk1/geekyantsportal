<?php

namespace App\Console\Commands;

use App\Models\Week;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class WeekCreatorCommand extends Command
{
 /**
  * The name and signature of the console command.
  *
  * @var string
  */
 protected $signature = 'command:create-week-record';

 /**
  * The console command description.
  *
  * @var string
  */
 protected $description = 'Create Week based on given date';

 /**
  * Create a new command instance.
  *
  * @return void
  */
 public function __construct()
 {
  parent::__construct();
 }

 /**
  * Execute the console command.
  *
  * @return mixed
  */
 public function handle()
 {
  $now      = date("Y-m-d");
  $response = Week::addWeek($now);
  if ($response['error']) {
   Log::error("Week Entry Failed");
   Log::error($response['error']);
  } else {
   Log::info($response);
  }

 }
}
