<?php

namespace App\Console\Commands\Timesheet;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use Config;
use Exception;
use App\Models\Admin\RequestApproval;
use App\Models\Admin\RequestApproveUser;
use App\Jobs\Slack\SlackReminder\Timesheet\NotifyTL;
use App\Models\Admin\Project;

class SlackTeamLeadReminderCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'slack:send-team-lead-timesheet-notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send out the summary for all the user timesheet for the last day';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    { 
        $teamLeadIdList = Project::whereIn('status',['in_progress','free'])->distinct()->select('project_manager_id')->pluck('project_manager_id')->toArray();
        foreach( $teamLeadIdList as $teamLeadId )
        {
            $job = (new NotifyTL($teamLeadId))->onQueue('timesheet-reminder');
            dispatch($job);
        }

    }
}
