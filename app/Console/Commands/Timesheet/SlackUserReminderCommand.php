<?php

namespace App\Console\Commands\Timesheet;

use App\Jobs\Slack\SlackReminder\Timesheet\NotifyUser;
use App\Models\Admin\Calendar;
use App\Models\Admin\Leave;
use App\Models\TmpTimesheet;
use App\Models\User;
use App\Models\UserTimesheet;
use App\Services\CalendarService;
use Illuminate\Console\Command;

class SlackUserReminderCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'slack:send-user-timesheet-notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remind users to fill their timesheets';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $currentDate = date('Y-m-d');
        if (Calendar::whereDate('date', $currentDate)->count() == 0) {
            $userList = User::Where('is_active', 1)->whereHas('settings', function ($query) {
                $query->where('key', 'user_timesheet_reminder')->where('value', 1);
            })->get();

            //weekend
            $nonWorkingDays = [6, 7];
            //holiday check
            $holiday = Calendar::whereDate('date', $currentDate)->where('type', 'Holiday')->first();
            if (!$holiday && !(in_array(date('N', strtotime($currentDate)), $nonWorkingDays))) {
                foreach ($userList as $user) {
                    //leave check
                    $leaveCheck = Leave::where('user_id', $user->id)->where('start_date', '<=', $currentDate)->where('end_date', '>=', $currentDate)->where('status', 'approved')->first();
                    if ($leaveCheck) {
                        continue;
                    } else {
                        $dates = CalendarService::getLastestTwoWorkingDay($user->id);
                        $tmpTimesheetEntry = TmpTimesheet::where('user_id', $user->id)->where('date', $dates[0])->first();
                        $timesheetEntry = UserTimesheet::where('user_id', $user->id)->where('date', $dates[0])->first();
                        if (!$tmpTimesheetEntry && !$timesheetEntry) {
                            $job = (new NotifyUser($user->id))->onQueue('timesheet-reminder');
                            dispatch($job);
                        }
                    }
                }
            }

        }
    }
}
