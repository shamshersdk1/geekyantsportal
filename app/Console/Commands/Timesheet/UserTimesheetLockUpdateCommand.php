<?php

namespace App\Console\Commands\Timesheet;

use App\Services\TimesheetService;
use Illuminate\Console\Command;

class UserTimesheetLockUpdateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:update-users-timesheet-lock';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates user\'s timesheet lock';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $response = TimesheetService::updateUsersTimesheetLock();
        if ($response['status']) {
        }
    }
}
