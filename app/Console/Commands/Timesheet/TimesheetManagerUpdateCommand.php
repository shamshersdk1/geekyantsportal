<?php
namespace App\Console\Commands\Timesheet;

use App\Models\User\UserTimesheetWeek;
use Illuminate\Console\Command;

class TimesheetManagerUpdateCommand extends Command
{
 /**
  * The name and signature of the console command.
  *
  * @var string
  */
 protected $signature = 'command:update-user-timesheet-manager-status';

 /**
  * The console command description.
  *
  * @var string
  */
 protected $description = 'The command makes sure that reporting manager is updated of the user whose timesheet are pending and the reporting manager is changed between a week ';

 /**
  * Create a new command instance.
  *
  * @return void
  */
 public function __construct()
 {
  parent::__construct();
 }

 /**
  * Execute the console command.
  *
  * @return mixed
  */
 public function handle()
 {
  $userTimesheets = UserTimesheetWeek::where('status', 'pending')->get();

  if (count($userTimesheets) > 0) {
   foreach ($userTimesheets as $userTimesheet) {
    if ($userTimesheet->user->parent_id && $userTimesheet->parent_id !== $userTimesheet->user->parent_id) {
     $userTimesheet->parent_id = $userTimesheet->user->parent_id;
     $userTimesheet->save();
    }
   }
  }

 }
}
