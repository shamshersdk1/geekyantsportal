<?php

namespace App\Console\Commands\Timesheet;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use Config;
use Exception;
use App\Models\UserTimesheet;


use DB;

class TimesheetUpdateStatusCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:update-user-timesheet-status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'One time command to take care of user timesheets records for new status column introduction';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    { 
        $userTimesheetList = UserTimesheet::all();
        $status = 'pending';
        foreach ( $userTimesheetList as $userTimesheet )
        {
        if ( !$userTimesheet->review->approver_id || ( $userTimesheet->review->approver_id && $userTimesheet->review->approver_id == 0 ) )
        {
            
            $status = 'pending';
        }
        elseif ( $userTimesheet->review->approved_duration == 0 )
        {
            $status = 'rejected';
        }
        elseif ( $userTimesheet->review->approved_duration > 0 )
        {
            $status = 'approved';
        }
        $userTimesheet->status = $status;
        $userTimesheet->save();
        } 
    }
}
