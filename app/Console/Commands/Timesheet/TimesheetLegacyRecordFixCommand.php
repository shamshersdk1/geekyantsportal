<?php

namespace App\Console\Commands\Timesheet;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use Config;
use Exception;
use App\Models\TmpTimesheet;
use App\Models\UserTimesheet;
use App\Services\TimesheetService;

use DB;

class TimesheetLegacyRecordFixCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:update-default-timesheet-review';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'One time command to take card of user timesheets entries with no entries in review table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    { 
        $timesheetIdList = DB::select( 
            'SELECT distinct ut.id
              FROM
                user_timesheets ut 
              WHERE NOT EXISTS (
                SELECT * FROM user_timesheet_reviews utr
                WHERE ut.id = utr.user_timesheet_id
              )'
              );
        
        foreach ( $timesheetIdList as $timesheetId )
        {
            TimesheetService::createApprovalRecord($timesheetId->id);
        }
    }
}
