<?php

namespace App\Console\Commands\Timesheet;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use Config;
use Exception;
use App\Models\User;
use App\Models\TmpTimesheet;
use App\Models\UserTimesheet;
use App\Models\Admin\Calendar;
use App\Models\Admin\Leave;
use App\Services\CalendarService;

use App\Jobs\Slack\SlackReminder\Timesheet\NotifyRM;

class SlackRMReminderCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'slack:send-rm-timesheet-notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remind RM about users not filling timesheet for 2 consecutive days';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    { 
        $currentDate = date('Y-m-d');
        if ( Calendar::whereDate('date', $currentDate)->count() == 0 ) 
        {
            $userList = User::where('is_active',1)->get();
            foreach( $userList as $user )
            {
                $dates = CalendarService::getLastestTwoWorkingDay($user->id);
                $tmpTimesheetEntry1 = TmpTimesheet::where('user_id',$user->id)->where('date',$dates[0])->first();
                $tmpTimesheetEntry2 = TmpTimesheet::where('user_id',$user->id)->where('date',$dates[1])->first();
                $timesheetEntry1 = UserTimesheet::where('user_id',$user->id)->where('date',$dates[0])->first();
                $timesheetEntry2 = UserTimesheet::where('user_id',$user->id)->where('date',$dates[1])->first();
                if ( !$tmpTimesheetEntry1 && !$timesheetEntry1 && !$tmpTimesheetEntry2 && !$timesheetEntry2 )
                {
                    $job = (new NotifyRM($user->id))->onQueue('timesheet-reminder');
                    dispatch($job);
                }
            }
        }     
    }
}
