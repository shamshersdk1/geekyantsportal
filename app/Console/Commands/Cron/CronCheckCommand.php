<?php

namespace App\Console\Commands\Cron;

use App\Models\User;
use Illuminate\Console\Command;
use App\Services\CronService\CronService;

class CronCheckCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:check-for-cron-jobs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check and run eligible cron jobs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $minute = date('i');
        $hour = date('H');
        $day_of_month = date('d');
        $month_of_year = date('m');
        $day_of_week = date('w');
        $result = CronService::getEligibleRecords($minute, $hour, $day_of_month, $month_of_year, $day_of_week);
    }
}
