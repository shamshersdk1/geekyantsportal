<?php

namespace App\Console\Commands;

use App\Models\Month;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Models\BonusRequest;
use App\Models\Admin\Bonus;
use App\Models\Admin\OnSiteBonus;
use App\Models\Admin\OnSiteAllowance;
use App\Models\Admin\AdditionalWorkDaysBonus;
use App\Models\User\UserTimesheetExtra;


class MigrateBonusRequest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:migrate-bonus-request-record';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = ' Migrating Bonus Request to respective tables';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        UserTimesheetExtra::where('status','approved')->chunk(50, function ($userTimesheetExtra) {
            foreach($userTimesheetExtra as $extra){
                UserTimesheetExtra::saveToBonus($extra);
            }
        });

        BonusRequest::chunk(50, function ($bonusRequest) {
            foreach ($bonusRequest as $bonus) {
                if($bonus->type=='onsite'){
                    $onSiteObj=OnSiteBonus::where('user_id',$bonus->user_id)->where('date',$bonus->date)->where('status',$bonus->status)->first();
                    if(!$onSiteObj){
                        OnSiteBonus::migrateBonusRequestToOnSite($bonus->id);
                    }
                }
                if($bonus->type=='additional'){
                    $additionalObj=AdditionalWorkDaysBonus::where('user_id',$bonus->user_id)->where('date',$bonus->date)->where('status',$bonus->status)->first();
                    if(!$additionalObj){
                        \Log::info('ADD called'.$bonus->id);
                        $bonusRequestId=$bonus->id;
                        AdditionalWorkDaysBonus::migrateBonusRequestToAdditional($bonus->id);
                    }
                }
            }
        });

        // $bonusRequest=BonusRequest::all();
        
        // foreach($bonusRequest as $bonus){
        //     if($bonus->type=='onsite'){
        //         $onSiteObj=OnSiteBonus::where('user_id',$bonus->user_id)->where('date',$bonus->date)->where('status',$bonus->status)->first();
        //         if(!$onSiteObj){
        //             OnSiteBonus::migrateBonusRequestToOnSite($bonus->id);
        //         }
        //     }
        //     if($bonus->type=='additional'){
        //         $additionalObj=AdditionalWorkDaysBonus::where('user_id',$bonus->user_id)->where('date',$bonus->date)->where('status',$bonus->status)->first();
        //         if(!$additionalObj){
        //             $bonusRequestId=$bonus->id;
        //             AdditionalWorkDaysBonus::migrateBonusRequestToAdditional($bonus->id);
        //         }
        //     }
        // }
    }
}
