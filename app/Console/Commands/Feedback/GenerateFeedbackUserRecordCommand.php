<?php

namespace App\Console\Commands\Feedback;

use App\Models\User;
use Illuminate\Console\Command;
use App\Services\FeedbackService;

class GenerateFeedbackUserRecordCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:generate-feedback-user-records';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Feedback User Records for review by TL and RM';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $last_month_date = date('Y-m-d', strtotime('first day of last month'));
        $month = date('m', strtotime($last_month_date));
        $year = date('Y', strtotime($last_month_date));

        FeedbackService::generateRecords($month, $year);
    }
}
