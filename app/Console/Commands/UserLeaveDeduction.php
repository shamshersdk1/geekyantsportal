<?php

namespace App\Console\Commands;

use App\Models\Admin\LeaveDeductionMonth;
use App\Models\Transaction;
use DB;
use Illuminate\Console\Command;

class UserLeaveDeduction extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:user-leave-deduction';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add a Transaction for each User Deduction Entry';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $leaveDeductions = LeaveDeductionMonth::where('transaction_id', null)->where('status', "approved")->get();
        if ($leaveDeductions) {
            foreach ($leaveDeductions as $leaveDeduction) {
                DB::beginTransaction();
                try {
                    $transactionObj = new Transaction;
                    $transactionObj->reference_type = "App\Models\Admin\LeaveDeductionMonth";
                    $transactionObj->reference_id = $leaveDeduction->id;
                    $transactionObj->type = "debit";
                    $transactionObj->amount = 0;
                    $transactionObj->user_id = $leaveDeduction->user_id;
                    $transactionObj->save();
                    if (!$transactionObj) {
                        throw new Exception("Unable to save transaction data");
                    }
                    $leaveDeduction->transaction_id = $transactionObj->id;
                    $leaveDeduction->save();
                    if (!$transactionObj) {
                        throw new Exception("Unable to update leaveDeduction data");
                    }
                    DB::commit();
                } catch (Exception $e) {
                    DB::rollback();
                }
            }
        }
    }
}
