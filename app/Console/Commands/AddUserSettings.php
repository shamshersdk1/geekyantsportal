<?php

namespace App\Console\Commands;

use App\Models\Admin\UserSetting;
use App\Models\User;
use Illuminate\Console\Command;

class AddUserSettings extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:add-to-user-settings';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add active users to user-settings table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::Where('is_active', 1)->get();
        $chunks = $users->chunk(10);

        foreach ($chunks as $chunk) {
            foreach ($chunk as $user) {

                $userObj = new UserSetting;
                $userObj['user_id'] = $user['id'];
                $userObj['key'] = 'user_timesheet_reminder';
                $userObj['value'] = 1;
                $userObj->save();
            }
        }
    }
}
