<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

use Watson\Validating\ValidatingTrait;

use App\Models\AccessLog;
use App\Models\Admin\PayroleGroup;

class UserCtc extends Model
{
    use ValidatingTrait;

    use SoftDeletes;

    protected $table = 'users_ctc';

    protected $dates = ['deleted_at'];

    // public $timestamps = true;

    private $rules = array(

        );
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'id', 'user_id');
    }
    public function group()
    {
        return $this->hasOne('App\Models\Admin\PayroleGroup', 'id', 'payrole_group_id');
    }
    public static function saveData($request)
    {
        try {
            $response =  [
                            'status'    => true,
                            'message'   => "success" ,
                            'id'        => null,
                            'log'       => [],
                            'errors'    => []
                        ];

            if (empty($request->name)) {
                $response['status'] = false;
                $response['log'][] = 'Name is required';
                return $response;
            }

            $payroleGroupObj = new PayroleGroup();
            $payroleGroupObj->name = $request->name;

            if (!$payroleGroupObj->save()) {
                $response['status'] = false;
                $response['log'][] = 'Group not saved';
                return $response;
            }
            return $response;
        } catch (\Exception $e) {
            AccessLog::accessLog(null, 'App\Models', 'Product', 'saveData', 'catch-block', $e->getMessage());
        }
    }
}
