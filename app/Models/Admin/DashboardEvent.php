<?php

namespace App\Models\Admin;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use Validator;

class DashboardEvent extends Model
{
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
    public static function saveData(Request $request, $id)
    {
        $response =  array('status' => false,'message' => "success",'error'=>'');
        if (!$request->user) {
            $response['error']='Invalid Link Followed';
            return $response;
        }

        $validator = Validator::make($request->all(), [
        'title' => 'required',
        'date' => 'required|date',
        'time' => 'date_format:H:i',
        'name' => 'required',
        ]);
        $user=User::where('name', $request->name)->first();
        if (empty($user)) {
            $response['error']='User not found';
            return $response;
        }
        if ($validator->fails()) {
            $response['error']=$validator->errors();
            return $response;
        }
        if ($id==0) {
            $event = new DashboardEvent;
            $event->user_id=$user->id;
        } else {
            $event=DashboardEvent::find($id);
            if (empty($event)) {
                $response['error']='Invalid Link Followed';
                return $response;
            }
        }
        $event->title=$request->title;
        $event->date=$request->date;
        $event->time=empty($request->time)?null:$request->time;
        $event->save();
        $response['status']=true;
        $response['message']='Saved Successfully';
        return $response;
    }
}
