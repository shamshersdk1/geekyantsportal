<?php namespace App\Models\Admin;

use App\Models\BaseModel;
use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Activity;

class Expense extends BaseModel
{

	use ValidatingTrait;

	protected $table = 'expenses';
	public $timestamps = true;
	use SoftDeletes;
	protected $appends = ['last_updated'];

	private $rules = array(
            'expense_head_id'  => 'required',
            'amount'  => 'required',
            'payment_status'  => 'required',
            'payment_method_id'  => 'required',
            'invoice_number'  => 'required',
            'invoice_date'  => 'required',
            'expense_by'  => 'required',
            'payment_reference_number'  => 'required',
			'payment_date'  => 'required'
	    );

	public function expenseHead()
   	{
       return $this->hasOne('App\Models\Admin\ExpenseHead','id','expense_head_id');
   	}
   	public function vendor()
   	{
       return $this->hasOne('App\Models\Admin\Vendor','id','vendor_id');
	}
	public function purchaseOrder()
   	{
       return $this->hasOne('App\Models\Admin\PurchaseOrder','id','po_id');
	}
	public function paymentMethod()
   	{
       return $this->hasOne('App\Models\Admin\PaymentMethod','id','payment_method_id');
	}
	public function expensor()
   	{
       return $this->hasOne('App\Models\User','id','expense_by');
	}
	public function payer()
   	{
       return $this->hasOne('App\Models\User','id','payment_by');
	}
	public function creator()
   	{
       return $this->hasOne('App\Models\User','id','created_by');
	}
	public function approver()
   	{
       return $this->hasOne('App\Models\User','id','approved_by');
	}
	public function getLastUpdatedAttribute()
    {
		$activityObj = Activity::with('user')->where('reference_type','App\Models\Admin\Expense')->where('reference_id',$this->id)->orderBy('created_at','DESC')->first();
		return $activityObj;
    }

	public function addActivity($action, $actionDetail = NULL){
        
        $userObj = \Auth::User();
        $userId=null;

        if($userObj) {
            $userId = $userObj->id;
        }
        $data['action'] = $action;
        $data['reference_id'] = $this->id;
        $data['reference_type'] = 'App\Models\Admin\Expense';
        $data['action_details'] = [$actionDetail];
        $data['user_id'] = $userId;

        Activity::storeData($data);
        
    }
	   
}
