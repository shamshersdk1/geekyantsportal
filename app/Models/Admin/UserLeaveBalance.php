<?php namespace App\Models\Admin;

use App\Models\Admin\UserLeaveLog;
use App\Models\BaseModel;
use App\Models\User;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserLeaveBalance extends BaseModel
{

    protected $table = 'user_leave_balances';
    public $timestamps = true;
    use SoftDeletes;

    public function scopeBuildQuery($query)
    {
        return $query;
    }

    public function calendarYear()
    {
        return $this->belongsTo('App\Models\Admin\CalendarYear', 'calendar_year_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public static function addUserLeaves($data)
    {
        if (empty($data)) {
            return false;
        }

        $user = User::find($data['user_id']);
        if (!$user) {
            return false;
        }
        // $year =  date('Y');
        $calendarYearObj = CalendarYear::where('year', date('Y'))->first();

        $userLeaveObject = UserLeaveBalance::where('calendar_year_id', $calendarYearObj->id)->where('user_id', $data['user_id'])->first();
        if (!$userLeaveObject) {
            $userLeaveObject = new UserLeaveBalance();
            $userLeaveObject->user_id = $data['user_id'];
            $userLeaveObject->calendar_year_id = $calendarYearObj->id;
            $userLeaveObject->paid_leave_balance = 0;
            $userLeaveObject->sick_leave_balance = 0;
            $userLeaveObject->carry_forward = 0;

        }

        $leaveLog = new UserLeaveLog();
        // $leaveLog->user_leave_balance_id = $userLeaveObject->id;
        $leaveLog->days = $data['days'];
        $leaveLog->status = "approved";
        $leaveLog->reference_type = $data['reference_type'];
        $leaveLog->reference_id = $data['reference_id'];

        if ($data['leave_type_id'] == "PL") {
            $leaveLog->leave_type_id = '1';
            $userLeaveObject->paid_leave_balance += $data['days'];
            $userLeaveObject->sick_leave_balance += 0;
            $userLeaveObject->carry_forward += 0;

        } else if ($data['leave_type_id'] == "SL") {
            $leaveLog->leave_type_id = '2';
            $userLeaveObject->sick_leave_balance += $data['days'];
            $userLeaveObject->paid_leave_balance += 0;
            $userLeaveObject->carry_forward += 0;

        } else if ($data['leave_type_id'] == "CF") {
            $leaveLog->leave_type_id = '1';
            $leaveLog->carry_forward = true;
            $userLeaveObject->paid_leave_balance += $data['days'];
            $userLeaveObject->sick_leave_balance += 0;
            $userLeaveObject->carry_forward += $data['days'];
        } else {
            return false;
        }
        if ($userLeaveObject->save()) {
            $userLeaveObject = UserLeaveBalance::where('calendar_year_id', $calendarYearObj->id)->where('user_id', $data['user_id'])->first();
            $leaveLog->user_leave_balance_id = $userLeaveObject->id;
        }
        if ($leaveLog->save()) {
            return true;
        } else {
            return false;
        }
    }

    // public static function updateAdditionalPL($userId, $year = date('Y')) {

    //     $calendarYearObj  = CalendarYear::where('year', $year)->first();
    //     if($calendarYearObj && $calendarYearObj->leaveCalendar) {
    //         $userBalanceObj = UserLeaveBalance::where('user_id', $userId)->where('calendar_year_id', $calendarYearObj->leaveCalendar->id)->first();
    //         if($userBalanceObj) {

    //         }

    //     }

    // }
}
