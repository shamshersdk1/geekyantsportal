<?php namespace App\Models\Admin;

use App\Models\BaseModel;
use Watson\Validating\ValidatingTrait;

class ExpenseHead extends BaseModel
{

	use ValidatingTrait;

	protected $table = 'expense_heads';
	public $timestamps = true;

	private $rules = array(
			'title'  => 'required'
	    );

	public function lowCostApprover()
   	{
       return $this->hasOne('App\Models\User','id','low_cost_approver');
   	}
   	public function highCostApprover()
   	{
       return $this->hasOne('App\Models\User','id','high_cost_approver');
   	}
}
