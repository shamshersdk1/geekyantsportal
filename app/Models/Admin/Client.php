<?php namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;
use App\Models\BaseModel;
use OwenIt\Auditing\Contracts\Auditable;
class Client extends BaseModel implements Auditable
{
    use ValidatingTrait;
    use \OwenIt\Auditing\Auditable;
    private $rules = array(
            'name' => 'required',
            'email'  => 'required|email',
            'website' => 'required'
            // 'email'  => 'required|email',
            // .. more rules here ..
        );

    public $timestamps = false;

    public function contacts()
    {
        return $this->hasMany('App\Models\Admin\Contact', 'company_id', 'id');
    }
    public static function getClients()
    {
        $companies=Client::all();
        $result=[];
        foreach ($companies as $company) {
            $obj = (object) array();
            $obj->label = $company->name;
            $obj->value = $company->email;
            $result[]=$obj;
        }
        return json_encode($result);
    }
}
