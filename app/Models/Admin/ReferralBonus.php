<?php

namespace App\Models\Admin;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReferralBonus extends BaseModel
{
    protected $table = 'referral_bonuses';
	public $timestamps = true;
	use SoftDeletes;

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
    public function referral()
    {
        return $this->belongsTo('App\Models\User', 'referral_id', 'id');
    }
    public function approver()
    {
        return $this->belongsTo('App\Models\User', 'approver_id', 'id');
    }
}
