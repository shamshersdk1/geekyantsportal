<?php namespace App\Models\Admin;

use App\Models\BaseModel;
use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\Admin\File;
use App\Services\FileService;

class FeedbackMonth extends BaseModel
{
	protected $table = 'feedback_months';
	public $timestamps = true;

    public static function getMonthStartAndEnd($month_id){
        $row = self::find($month_id);
        if($row){
            $start_date = $row->year."-".$row->month."-01";
            $end_date = $row->year."-".$row->month."-".cal_days_in_month(CAL_GREGORIAN, $row->month, $row->year);
            return ['start_date'=>$start_date, 'end_date'=>$end_date];
        }

        return false;
    }
}
