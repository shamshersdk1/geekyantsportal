<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use App\Models\BaseModel;
use DB;

class UserGoalItem extends BaseModel
{
    public function tags()
    {
        return $this->belongsToMany('App\Models\Admin\Tag', 'tag_user_goal_item', 'user_goal_item_id', 'tag_id');
    }

    public static function idpCompare($a, $b)
    {
        if ($a['is_idp'] == $b['is_idp']) {
            return 0;
        }
        return ($a['is_idp'] < $b['is_idp']) ? -1 : 1;
    }

    // public static function saveItem($user_goal_id, $goal_id, $is_private, $is_idp)
    // {
    //     $response = ["status" => true, "message" => ""];
    //     $item = new UserGoalItem();
    //     $item->user_goal_id = $user_goal_id;
    //     $item->goal_id = $goal_id;
    //     $item->is_private = $is_private;
    //     $item->is_idp = $is_idp;
    //     if(!$item->save()) {
    //         $response = ["status" => false, "message" => $item->getMessage()];
    //     }
    //     return $response;
    // }

    public static function saveItem($user_goal_id, $goal)
    {
        $response = ["status" => true, "message" => ""];
        $item = new UserGoalItem();
        $item->user_goal_id = $user_goal_id;
        $item->title = $goal['title'];
        $item->success_metric = $goal['success_metric'];
        $item->process = $goal['process'];
        // $item->is_private = $is_private;
        $item->is_idp = $goal['is_idp'];
        if(!$item->save()) {
            $response = ["status" => false, "message" => $item->getMessage()];
        }
        $tags = is_array($goal['tags']) ? $goal['tags'] : explode(",", $goal['tags']);
        if($tags && count($tags) > 0) {
            UserGoalItem::attachTags($tags, $item->id);
        }
        return $response;
    }

    public static function updateItem($goal)
    {
        $response = ["status" => true, "message" => ""];
        $item = UserGoalItem::find($goal['id']);
        if($item) {
            $item->title = $goal['title'];
            $item->success_metric = $goal['success_metric'];
            $item->process = $goal['process'];
            $item->review_comment = !empty($goal['review_comment']) ? $goal['review_comment'] : '' ;
            $item->points = !empty($goal['points']) ? $goal['points'] : '' ;
            // $item->is_private = $is_private;
            $item->is_idp = $goal['is_idp'];
            $existing = $item->tags()->pluck('name')->toArray();
            $add = array_diff($goal['tags'], $existing);
            $subtract = array_diff($existing, $goal['tags']);
            UserGoalItem::attachTags($add, $item->id);
            UserGoalItem::detachTags($subtract, $item->id);
            if(!$item->save()) {
                $response = ["status" => false, "message" => $item->getMessage()];
            }
        } else {
            $response = ["status" => false, "message" => "User goal item not found"];
        }
        return $response;
    }
    public static function attachTags($tags, $user_goal_item_id)
    {
        foreach($tags as $tag)
        {
            $tag = trim($tag);
            $db_tag = Tag::where('name', $tag)->first();
            if($db_tag) {
                $db_tag->user_goal_items()->attach($user_goal_item_id);
            } else {
                $tag_id = Tag::saveTag($tag);
                if(is_null($tag_id)) {
                    continue;
                } else {
                    DB::table('tag_user_goal_item')->insert(['tag_id' => $tag_id, 'user_goal_item_id' => $user_goal_item_id]);
                }
            }
        }
    }
    public static function detachTags($tags, $user_goal_item_id)
    {
        foreach($tags as $tag)
        {
            $tag = trim($tag);
            $db_tag = Tag::where('name', $tag)->first();
            if($db_tag) {
                $db_tag->user_goal_items()->detach($user_goal_item_id);
            }
        }
    }
    public static function updateReviewComment($id,$review_comment)
    {
        $status = true;
        $message = '';
        $user_goal_item = UserGoalItem::find($id);
        if( empty($user_goal_item) )
        {
            $status = false;
            $message = 'Goal Item not found';
        }
        $user_goal_item->review_comment = $review_comment;
        if ( !$user_goal_item->save() )
        {
            $status = false;
            $message = 'Unable to update record';
        }
        $response['status'] = $status;
        $response['message'] = $message;
        return $response;

    }
    public static function updatePoints($id,$points)
    {
        $status = true;
        $message = '';
        $user_goal_item = UserGoalItem::find($id);
        if( empty($user_goal_item) )
        {
            $status = false;
            $message = 'Goal Item not found';
        }
        $user_goal_item->points = $points;
        if ( !$user_goal_item->save() )
        {
            $status = false;
            $message = 'Unable to update record';
        }
        $response['status'] = $status;
        $response['message'] = $message;
        return $response;
    }

    public static function removeTemporaryRecords($goal_id)
    {
        $status = true;
        $message = '';
        $toBeDeletedObjs = UserGoalItem::where('user_goal_id', $goal_id)->get();
        foreach( $toBeDeletedObjs as $toBeDeletedObj )
        {
            $toBeDeletedObj->delete();
        }
        $response['status'] = $status;
        $response['message'] = $message;
        return $response;
    }
}
