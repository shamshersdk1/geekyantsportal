<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class ProjectCategory extends Model
{
    //

	use ValidatingTrait;
	protected $table = 'project_categories';
	

	private $rules = array(
		    'project_type' => 'required'
	    );
        
}
