<?php

namespace App\Models\Admin;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProfileBuilder extends BaseModel
{

    use SoftDeletes;

    protected $dates = ['created_at'];
    protected $table = 'user_profiles_basic';
    public $timestamps = true;
}
