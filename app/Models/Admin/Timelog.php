<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;
use Illuminate\Http\Request;
use App\Models\Admin\Company;
use App\Models\Admin\Project;
use App\Models\Admin\ProjectNotes;
use App\Models\Admin\ProjectFiles;
use App\Models\Admin\ProjectSprints;
use App\Models\Admin\SprintResource;
use App\Models\Admin\Technology;
use App\Models\Admin\CmsTechnology;
use App\Models\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Redirect;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use Config;
use Exception;


class Timelog extends Model
{
    
    use ValidatingTrait;
	protected $table = 'timelogs';
	

	private $rules = array(
		    'user_id' => 'required',
		    'project_id' => 'required',
		    
	    );
	public function project() {
		return $this->hasOne('App\Models\Admin\Project', 'id', 'project_id');	
	}

	public function user() {
		return $this->hasOne('App\Models\user', 'id', 'user_id');	
	}
}
