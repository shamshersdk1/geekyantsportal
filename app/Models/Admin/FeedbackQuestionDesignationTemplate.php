<?php namespace App\Models\Admin;

use App\Models\BaseModel;
use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\Admin\File;
use App\Services\FileService;

class FeedbackQuestionDesignationTemplate extends BaseModel
{

	use ValidatingTrait;

	protected $table = 'feedback_question_designation_templates';
	public $timestamps = true;
	use SoftDeletes;

	private $rules = array(
			'feedback_question_template_id'  => 'required',
	    );
	public function designation()
	{
			return $this->belongsTo('App\Models\Admin\Designation','designation_id','id');
	}
	// public function designation()
	// {
	// 		return $this->belongsTo('App\Models\Admin\Designations','designation_id','id');
	// }
}
