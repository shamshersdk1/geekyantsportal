<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

use App\Models\User;
use App\Services\CtcService;
use App\Services\LeaveService;

use DB;

class Payslip extends Model
{
    //
    use ValidatingTrait;

    protected $fillable = [
    				'id',
            'employee_number',
            'employee_name',
            'gender',
            'payroll_month',
            'status',
            'status_description',
            'actual_payable_days',
            'working_days',
            'loss_of_pay_days',
            'days_payable',
            'basic',
            'transport_allowance',
            'car_allowance',
            'hra',
            'medical_reimbursement',
            'food_allowance',
            'special_allowance',
            'telephone_allowance',
            'perfomance_bonus',
            'annual_bonus',
            'grossa',
            'gross_less_bonus',
            'pf_employee',
            'pf_employer',
            'pf_other_charges',
            'arrear_pf_employee',
            'arrear_pf_employer',
            'arrear_pf_other_charges',
            'total_contributionsb',
            'professional_tax',
            'special_medical_insurance',
            'deduction_food_allowance',
            'total_income_tax',
            'loan_emi',
            'total_deductionsc',
            'net_payd_a_b_c',
            'total_reimbursementse',
            'total_net_payde',
            'gross_earning',
            'total_deduction',
            'net_pay_check',
            'arrear_deduction',
            'adjusted_gross_salary',
            'special_allowance_adjusted',
            'total_deduction_payslip',
            'email_id',
            'user_id'

    					];
    					
		private $rules = array(
	        'employee_number' 		=> 'required',
	        'employee_name'  	=> 'required',
	        'payroll_month' 			=> 'required',
	        'working_days' 		=> 'required',
	        'basic' 		=> 'required'
	    );
            
    public static function generateCsvData($month, $year, $total_days, $id = null)
    {
        $users = User::where('is_active', 1)->orderBy('employee_id')->get();
        $result = [];
        $head = ['Sl No','Ant Id','Name','Email ID','PAN No','DOB','DOJ','Designation','Gender','Bank A/c. No.','Bank IFSC Code','PF No','UAN No','ESI No','Month','Year','Total Working Day in Month','No of Days Worked'];
        $editable = array_fill(0,count($head)-1,false);
        $total = PayrollRuleForCsv::count();
        if(!$total) {
            $total = 33;
        }
        $editable = array_merge($editable, [true], array_fill(0,$total,true));
        $table_head = array_merge($head, PayrollRuleForCsv::orderBy('id')->pluck('key')->toArray());
        $head = array_merge($head, PayrollRuleForCsv::orderBy('id')->pluck('value')->toArray());
        $head = array_merge($head, ['SL(Op Bal)','CL (Op Bal)','PL (Op Bal)','CME SL','CME CL','CME PL','SL Availed(utilised)','CL Availed','PL Availed(utilised)','Bal c/f SL','Bal c/f CL','Bal c/f PL']);
        $table_head = array_merge($table_head, ['SL(Op Bal)','CL (Op Bal)','PL (Op Bal)','CME SL','CME CL','CME PL','SL Availed(utilised)','CL Availed','PL Availed(utilised)','Bal c/f SL','Bal c/f CL','Bal c/f PL']);
        $editable = array_merge($editable, array_fill(0,count($head)-count($editable),true));
        $result[] = $head;
        $count=1;
        $start_date = ($month=='01') ? '21-12-'.($year-1) : ( ($month > 10) ? '21-'.($month-1)."-".$year : '21-0'.($month-1)."-".$year);
        $end_date = '20-'.$month."-".$year;
        $dateObj   = \DateTime::createFromFormat('!m', $month);
        $monthName = $dateObj->format('F');
        $existingUsers = PayslipCsvData::where('payslip_csv_month_id', $id)->get();
        $skipUsers = [];
        foreach($existingUsers as $existing)
        {
            $arr = [];
            $data = json_decode(json_encode($existing), true);
            unset($data['id']);
            unset($data['payslip_csv_month_id']);
            $skipUsers[] = $data['user_id'];
            unset($data['user_id']);
            unset($data['created_at']);
            unset($data['updated_at']);
            foreach($data as $val)
            {
                if(is_null($val)) {
                    $arr[] = "-";
                } else {
                    $arr[] = $val;
                }
            }
            $result[] = $arr;
            $count++;
        }
        foreach($users as $user)
        {
            if(in_array($user->id, $skipUsers)) {
                continue;
            }
            $arr = [];
            $employee_id = empty($user->employee_id) ? "-" : $user->employee_id;
            $employee_name = $user->first_name;
            $email = $user->email;
            $pan = empty($user->pan) ? "-" : $user->pan;
            $dob = empty($user->dob) ? "-" : $user->dob;
            $joining_date = empty($user->joining_date) ? "-" : $user->joining_date;
            $designation = empty($user->profile) ? "-" : ( empty($user->profile->designation) ? "-" : $user->profile->designation);
            $gender = empty($user->gender) ? "-" : $user->gender;
            $account_no = empty($user->bank_ac_no) ? "-" : $user->bank_ac_no;
            $ifsc = empty($user->bank_ifsc_code) ? "-" : $user->bank_ifsc_code;
            $pf = empty($user->pf_no) ? "-" : $user->pf_no;
            $uan = empty($user->uan_no) ? "-" : $user->uan_no;
            $esi = empty($user->esi_no) ? "-" : $user->esi_no;
            // $total_days = LeaveService::calculateWorkingDays($start_date, $end_date, $user->id);
            $days_worked = $total_days - LeaveService::getTotalLeaves($start_date, $end_date, $user->id, 'all');
            $data = [$count, $employee_id, $employee_name, $email, $pan, $dob, $joining_date, $designation, $gender, $account_no, $ifsc, $pf, $uan, $esi, $monthName, $year, $total_days, $days_worked];
            $response = CtcService::generatePayslip($user->id, $month, $year);
            $data = array_merge($data, $response);
            $sl_availed = LeaveService::getTotalLeaves($start_date, $end_date, $user->id, 'sick');
            $pl_availed = LeaveService::getTotalLeaves($start_date, $end_date, $user->id, 'paid');
            $sl_bal = LeaveService::getRemainingSickLeave($user);
            $pl_bal = LeaveService::getRemainingPaidLeave($user);
            $sl_opbal = $sl_bal + $sl_availed;
            $pl_opbal = $pl_bal + $pl_availed;
            $leaves = [$sl_opbal, "-", $pl_opbal, "-", "-", "-", $sl_availed, "-", $pl_availed, $sl_bal, "-", $pl_bal];
            $data = array_merge($data, $leaves);
            if($id != null) {
                $existing = PayslipCsvData::where('payslip_csv_month_id', $id)->where('user_id', $user->id)->first();
                if(!$existing) {
                    for($i=0;$i<count($table_head);$i++)
                    {
                        $key = $table_head[$i];
                        $key = preg_replace('/[\(\)\/]/', null, $key);
                        $key = preg_replace('/[^a-zA-Z0-9_]/', '_', $key);
                        $key = strtolower($key);
                        if($key == "ant_id") {
                            $key = "employee_code";
                        }
                        if($key == "name") {
                            $key = "employee_name";
                        }
                        $arr[$key] = ($data[$i] == "-") ? null : $data[$i];
                    }
                    $arr = array_merge(['payslip_csv_month_id' => $id, 'user_id' => $user->id], $arr);
                    DB::table('payslip_csv_data')->insert($arr);
                }
            }
            $result[] = $data;
            $count++;
        }
        return ['data' => $result, 'editable' => $editable];
    }

}
