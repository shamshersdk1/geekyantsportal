<?php namespace App\Models\Admin;

use App\Models\BaseModel;
use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentMethod extends BaseModel
{

	use ValidatingTrait;

	protected $table = 'payment_methods';
	public $timestamps = true;
	use SoftDeletes;
	
	private $rules = array(
			'name'  => 'required'
	    );

}
