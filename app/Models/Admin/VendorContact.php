<?php namespace App\Models\Admin;

use App\Models\BaseModel;
use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class VendorContact extends BaseModel
{

	use ValidatingTrait;

	protected $table = 'vendor_contacts';
	public $timestamps = false;

	private $rules = array(
			'vendor_id'  => 'required',
			'name'  => 'required'
	    );

	public function vendor()
   	{
	   return $this->belongsTo('App\Models\Admin\Vendor','vendor_id','id');
   	}
}
