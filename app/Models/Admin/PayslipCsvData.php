<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use App\Models\Loan;
use App\Models\LoanEmi;
use App\Models\Admin\Appraisal;
use StdClass;
use Exception;

class PayslipCsvData extends Model
{
    protected $table = 'payslip_csv_data';

    public static function appraisals($id)
    {
        $csvMonth = PayslipCsvMonth::find($id);
        $userIdArray = PayslipCsvData::userIdArray($id);
        if($csvMonth->month == 1) {
            $startDate = ($csvMonth->year-1)."-12-20";
        } else {
            $startDate = $csvMonth->year."-".substr("00".($csvMonth->month-1), -2)."-21";
        }
        $endDate = $csvMonth->year."-".substr("00".($csvMonth->month), -2)."-20";
        $appraisals = Appraisal::with('user')->where('effective_date','>=',$startDate)->where('effective_date','<=',$endDate)->whereIn('user_id', $userIdArray)->where('is_active', 1)->get();
        $array = [];
        foreach($appraisals as $appraisal)
        {
            $user = $appraisal->user;
            if($user) {
                try {
                    $data = new StdClass();
                    $data->name = $user->name;
                    $data->updated_on = $appraisal->effective_date;
                    $previous = Appraisal::where('user_id', $user->id)->where('effective_date', '<', $startDate)->orderBy('effective_date', 'desc')->first();
                    if($previous) {
                        $data->previous = round($previous->in_hand / 12, 2);
                    } else {
                        $data->previous = "Not Available";
                    }
                    $data->current = round($appraisal->in_hand / 12, 2);
                    $array[] = $data;
                } catch(Exception $e) {
                    continue;
                }
            }
        }
        return $array;
    }
    public static function userIdArray($id)
    {
        $array = PayslipCsvData::where('payslip_csv_month_id', $id)->pluck('user_id')->toArray();
        return $array;
    }
    public static function addLoan($id, $emi_id, $current_amount)
    {
        try {
            $emi = LoanEmi::find($emi_id);
            $loan = $emi->loan;
            $user = $loan->user;
            $dataObj = PayslipCsvData::where('payslip_csv_month_id', $id)->where('user_id', $user->id)->first();
            $add=["loan", "total_deduction_for_the_month"];
            $subtract = ["amount_payable"];
            foreach($add as $property)
            {
                $dataObj->$property = is_null($dataObj->$property) ? 0 : $dataObj->$property;
                $dataObj->$property = $dataObj->$property - $current_amount;
                $dataObj->$property = $dataObj->$property + $emi->actual_amount;
            }
            foreach($subtract as $property)
            {
                $dataObj->$property = is_null($dataObj->$property) ? 0 : $dataObj->$property;
                $dataObj->$property = $dataObj->$property + $current_amount;
                $dataObj->$property = $dataObj->$property - $emi->actual_amount;
            }
            if($dataObj->save()) {
                return true;
            }
        } catch(Exception $e) {
        }
        return false;
    }
    public static function bonuses($id)
    {
        $csvMonth = PayslipCsvMonth::find($id);
        $userIdArray = PayslipCsvData::userIdArray($id);
        $date = $csvMonth->year."-".substr("00".$csvMonth->month, -2)."-25";
        $bonuses = Bonus::with('user')->where('date', $date)->where('status', 'approved')->whereIn('user_id', $userIdArray)->where('paid', '!=', 1)->get();
        $array = [];
        foreach($bonuses as $bonus)
        {
            $user = $bonus->user;
            $code = $user->employee_id;
            $name = $user->name;
            $amount = $bonus->amount;
            $date = date_in_view($bonus->created_at);
            $notes = $bonus->notes;
            $array[] = ['code' => $code, 'name' => $name, 'amount' => $amount, 'date' => $date, 'notes' => $notes]; 
        }
        return $array;
    }
    public static function getLoanStatus($id, $user_id)
    {
        $csvData = PayslipCsvData::where('user_id', $user_id)->where('payslip_csv_month_id', $id)->first();
        if(!$csvData) {
            return ['status' => "error", 'value' => 0.00];
        } else {
            if(empty($csvData->loan) || $csvData->loan == null) {
                return ['status' => "unpaid", 'value' => 0.00];
            } else {
                return ['status' => "paid", 'value' => $csvData->loan];
            }
        }
    }
}
