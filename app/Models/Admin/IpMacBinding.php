<?php

namespace App\Models\Admin;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;

class IpMacBinding extends BaseModel
{
    protected $table='ip_mac_binding';
}
