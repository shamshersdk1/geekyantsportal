<?php

namespace App\Models\Admin;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class IpAddressMapper extends BaseModel
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $table = 'ip_address_mapper';
    protected $fillable = ['reference_type', 'reference_id', 'user_ip_address_id'];
    public function reference()
    {
        return $this->morphTo();
    }
    public function userIp()
    {
        return $this->belongsTo('App\Models\Admin\UserIpAddress', 'user_ip_address_id', 'id');

    }
}
