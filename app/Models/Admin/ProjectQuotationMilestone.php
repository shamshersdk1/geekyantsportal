<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class ProjectQuotationMilestone extends Model
{
    use ValidatingTrait;

    protected $table = 'project_quotation_milestones';
    public $timestamps = true;

    private $rules = array(
            'project_quotation_id' => 'required',
            'details' => 'required',
            'delivery_date' => 'required', 

        );

    protected $fillable =  ['project_quotation_id', 'details', 'delivery_date','cost'];


    public function sprints() {
    	return $this->belongsToMany('App\Models\Admin\ProjectSprints', 'milestone_sprint', 'milestone_id', 'sprint_id')->withPivot('project_id');
    }
}
