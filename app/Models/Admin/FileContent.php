<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class FileContent extends Model
{
    protected $table = 'file_contents';
                        
    protected $fillable = [
            'id',
            'file_id',
            'content',
    ];

	private $rules = array(
		'content' => 'required',
    );

    public function file()
    {
        return $this->belongsTo(File::class);
    }
}
