<?php

namespace App\Models\Admin;

use App\Models\BaseModel;
use Auth;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Events\Comment\CommentStored;

class GoogleDriveLink extends BaseModel
{
    use SoftDeletes;

    protected $table = 'google_drive_links';

    public function reference()
    {
        return $this->morphTo();
    }

    public function creator()
    {
        return $this->hasOne('App\Models\User', 'id', 'added_by');
    }

    public static function saveData($reference_id, $reference_type, $name, $path, $is_private = null)
    {
        $status = true;
        $message = '';
        $user = Auth::user();

        $linkObj = new GoogleDriveLink();
        $linkObj->reference_id = $reference_id;
        $linkObj->reference_type = $reference_type;
        $linkObj->name = $name;
        $linkObj->path = $path;
        $linkObj->is_private_to_admin = $is_private == "true" ? true : false;
        $linkObj->added_by = $user->id;
        if (!$linkObj->save()) {
            $status = false;
            $message = $linkObj->getMessage();
        }
        $response['status'] = $status;
        $response['message'] = $message;
        
        return $response;
    }

    public static function updateData($id, $reference_id, $reference_type, $name, $path, $is_private = null)
    {
        $status = true;
        $message = '';
        $user = Auth::user();

        $linkObj = GoogleDriveLink::find($id);
        $linkObj->reference_id = $reference_id;
        $linkObj->reference_type = $reference_type;
        $linkObj->name = $name;
        $linkObj->path = $path;
        $linkObj->is_private_to_admin = $is_private == "true" ? true : false;
        $linkObj->added_by = $user->id;
        if (!$linkObj->save()) {
            $status = false;
            $message = $linkObj->getMessage();
        }
        $response['status'] = $status;
        $response['message'] = $message;
        
        return $response;
    }
}
