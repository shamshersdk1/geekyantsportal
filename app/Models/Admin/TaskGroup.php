<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaskGroup extends Model
{
    
    use SoftDeletes;

    public function tasks()
    {
        return $this->hasMany('App\Models\UserTask');
    }

    public function sharedTasks()
    {
        return $this->hasMany('App\Models\SharedTask');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function creator()
    {
        return $this->belongsTo('App\Models\User', 'created_by');
    }

    public static function createNew($user_id, $owner_id)
    {
        $group = new TaskGroup();
        $group->user_id = $user_id;
        $group->created_by = $owner_id;
        if($group->save()) {
            return $group->id;
        } else {
            return null;
        }
    }
            
}
