<?php

namespace App\Models\Admin;

use App\Jobs\Leave\UserLeavePLAvailableJob;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserLeaveLog extends Model
{
    use SoftDeletes;
    protected $table = 'user_leave_logs';

    public function leaveType()
    {
        return $this->belongsTo('App\Models\Admin\LeaveType');
    }
    public function reference()
    {
        return $this->morphTo();
    }

    public function leaveBalance()
    {
        return $this->belongsTo('App\Models\Admin\UserLeaveBalance', 'id', 'user_leave_balance_id');

    }

    public static function updateUserAllowedLeave($data)
    {
        if (empty($data)) {
            return false;
        }
        $year = date('Y');
        $calendar = CalendarYear::where('year', $year)->first();
        if (!$calendar) {
            return false;
        }
        $user_leave_obj = UserLeaveBalance::where('user_id', $data['user_id'])->where('calendar_year_id', $calendar->id)->first();
        if (!$user_leave_obj) {
            return false;
        }
        $userLeaveObj = new UserLeaveLog();
        $userLeaveObj->user_leave_balance_id = $user_leave_obj->id;
        $userLeaveObj->leave_type_id = !empty($data['leave_type_id']) ? $data['leave_type_id'] : null;
        $userLeaveObj->days = !empty($data['days']) ? $data['days'] : null;
        $userLeaveObj->reference_type = !empty($data['reference_type']) ? $data['reference_type'] : null;
        $userLeaveObj->reference_id = !empty($data['reference_id']) ? $data['reference_id'] : null;
        $userLeaveObj->status = "approved";
        if (!$userLeaveObj->save()) {
            return false;
        }
        $job = (new UserLeavePLAvailableJob($userLeaveObj->id));
        dispatch($job);
        return true;
    }
}
