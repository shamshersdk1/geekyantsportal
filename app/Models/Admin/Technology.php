<?php

namespace App\Models\Admin;

use App\Models\BaseModel;
use Watson\Validating\ValidatingTrait;

class Technology extends BaseModel
{
     use ValidatingTrait;
	protected $table = 'technologies';
	public $timestamps = false;
	

	private $rules = array(
		    'name' => 'required',
	    );

	public function category() {
		return $this->belongsTo('App\Models\Admin\TechnologyCategory', 'technology_category_id');
	}

	public static function getList()
    {
        $allUsers = Technology::all();
        $jsonuser=[];
        
        foreach ($allUsers as $user) {
            array_push($jsonuser, $user->name);
        }
        return json_encode($jsonuser);
    }
}
