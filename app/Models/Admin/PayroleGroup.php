<?php

namespace App\Models\Admin;

use App\Models\AccessLog;
use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class PayroleGroup extends Model
{
    use ValidatingTrait;

    protected $table = 'payrole_group';

    // public $timestamps = true;

    private $rules = array(

    );

    public function rules()
    {
        return $this->hasMany('App\Models\Admin\PayroleGroupRule', 'payrole_group_id', 'id');
    }

    public static function saveData($request)
    {
        try {
            $response = [
                'status' => true,
                'message' => "success",
                'id' => null,
                'log' => [],
                'errors' => [],
            ];

            if (empty($request->name)) {
                $response['status'] = false;
                $response['log'][] = 'Name is required';
                return $response;
            }

            $payroleGroupObj = new PayroleGroup();
            $payroleGroupObj->name = $request->name;
            // create a default group

            if (!$payroleGroupObj->save()) {

                $response['status'] = false;
                $response['log'][] = 'Group not saved';
                return $response;
            }
            return $response;

        } catch (\Exception $e) {

            AccessLog::accessLog(null, 'App\Models', 'Product', 'saveData', 'catch-block', $e->getMessage());
        }
    }

}
