<?php

namespace App\Models\Admin;

use App\Models\BaseModel;
use App\Models\Timesheet\UserTimesheetLock;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;

class ProjectResource extends BaseModel
{

    use ValidatingTrait;
    protected $table = 'project_resources';
    public $timestamps = true;
    use SoftDeletes;

    private $rules = array(
        'project_id' => 'required',
        'user_id' => 'required',
    );
    public function resource()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    public function user_data()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
    public function project()
    {
        return $this->hasOne('App\Models\Admin\Project', 'id', 'project_id');
    }
    public function resource_type()
    {
        return $this->hasOne('App\Models\Admin\ResourcePrice', 'id', 'resource_price_id');
    }

    public static function getActiveProjectIdArray($user_id, $start_date = null, $end_date = null)
    {
        if ($start_date == null) {
            $start_date = date("Y-m-d");
        }
        if ($end_date == null) {
            $end_date = date("Y-m-d");
        }
        $resources = ProjectResource::where('user_id', $user_id)->where('start_date', '<=', $end_date)->where(function ($query) use ($start_date) {
            $query->where('end_date', null)->orWhere('end_date', '>=', $start_date);
        })->pluck('project_id')->toArray();
        $projects = Project::whereIn('id', $resources)->pluck('id')->toArray();
        return $projects;
    }
    public static function getUserProjects($projectId, $date)
    {
        // if ($start_date == null) {
        //     $start_date = date("Y-m-d");
        // }
        // if ($end_date == null) {
        //     $end_date = date("Y-m-d");
        // }

        $resources = ProjectResource::where('project_id', $projectId)->where('start_date', '<=', $date)
            ->where(function ($query) use ($date) {
                $query->where('end_date', null)
                    ->orWhere('end_date', '>=', $date);
            })->get();
        return $resources->toArray();
    }

    public static function canApplyTimesheet($userId, $date, $projects = null)
    {
        $res = [];

        $lockObj = UserTimesheetLock::where('user_id', $userId)->first();

        if ($lockObj) {
            if ($date <= $lockObj->date) {
                $res['status'] = false;
                $res['message'] = 'Timesheet is auto locked.';
                return $res;
            }
        }

        $res['status'] = true;

        if ($projects && count($projects) > 0) {
            foreach ($projects as $project) {
                $checkProject = self::getUserActiveProjects($userId, $project['id'], $date);

                if (!$checkProject || count($checkProject) == 0) {
                    $res['status'] = false;
                    $res['message'] = 'You are not assigned to ' . $project['name'] . ' project.';
                }
            }
        }

        return $res;
    }

    public static function getUserActiveProjects($userId, $projectId, $date)
    {
        $resources = ProjectResource::where('user_id', $userId)->where('project_id', $projectId)->where('start_date', '<=', $date)
            ->where(function ($query) use ($date) {
                $query->where('end_date', null)
                    ->orWhere('end_date', '>=', $date);
            })->get();

        return $resources->toArray();
    }
}
