<?php namespace App\Models\Admin;

use App\Models\BaseModel;
use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vendor extends BaseModel
{

	use ValidatingTrait;

	protected $table = 'vendors';
	public $timestamps = true;
	protected $appends = ['services_name'];
	use SoftDeletes;

	private $rules = array(
			'type'  => 'required',
			'name'  => 'required'
	    );

	public function vendorContacts()
   	{
		return $this->hasMany('App\Models\Admin\VendorContact', 'vendor_id', 'id' )->orderBy('is_primary_contact','DESC');
	}
	public function vendorServices()
   	{
		return $this->hasMany('App\Models\Admin\VendorService', 'vendor_id', 'id' );
	}
	public function getServicesNameAttribute()
    {
		$data = [];
		$string = '';
		if ( $this->vendorServices )
		{
			foreach ( $this->vendorServices as $vendor_service )
			{
				$data[] = $vendor_service->vendorServicesName->name;
			}
			$string = implode(",",$data);
		}
		return $string;
    }
	   
}
