<?php namespace App\Models\Admin;

use App\Models\BaseModel;
use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use App\Models\Admin\File;
use App\Services\FileService;
use App\Models\Admin;
class TechTalk extends BaseModel 
{

	// use ValidatingTrait;  // Use For Validation.Basically creates a validate() method which is then used in out tech class
	
	protected $table = 'tech_talk';
	public $timestamps = true;
	use SoftDeletes;

	public function user()
	{
		return $this->belongsTo('App\Models\User','employee_id');
	}
	


}
