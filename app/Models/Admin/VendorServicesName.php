<?php namespace App\Models\Admin;

use App\Models\BaseModel;
use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class VendorServicesName extends BaseModel
{

	use ValidatingTrait;

	protected $table = 'vendor_services_names';
	public $timestamps = false;

	private $rules = array(
			'name'  => 'required'
	    );

}
