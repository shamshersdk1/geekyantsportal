<?php

namespace App\Models\Admin;

use App\Events\LoanDisbursed;
use App\Jobs\Loan\LoanRequestJob;
use App\Models\AccessLog;
use App\Models\Loan;
use App\Models\LoanTransaction;
use App\Services\FileService;
use Auth;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use App\Events\Loan\LoanRequested;
use App\Traits\ValidationTrait;
use App\Models\User;


class LoanRequest extends Model
{
    use SoftDeletes;
    use ValidationTrait;


    protected $fillable = ['user_id','approved_amount','amount','emi','emi_start_date','application_date','appraisal_bonus_id','description','status'];

    protected $rules= array(
        'user_id' => 'required | exists:users,id',
        'amount' => 'required | numeric',
    );
 
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }


    public function activities()
    {
        return $this->hasMany('App\Models\Admin\LoanRequestActivity');
    }

    public function submittedBy()
    {
        $activity = $this->activities()->where('status', "approved")->where('role', "Reporting Manager")->orderBy('id', "desc")->first();
        return (!empty($activity) && !empty($activity->user->name)) ? $activity->user->name : null;
    }

    // public function reviewedBy()
    // {
    //     $activity = $this->activities()->where('status', "approved")->where('role', "Human Resources")->orderBy('created_at', "asc")->first();
    //     return (!empty($activity) && !empty($activity->user->name)) ? $activity->user->name : null;
    // }

    public function reconciledBy()
    {
        $activity = $this->activities()->where('status', "approved")->where('role', "Management")->orderBy('id', "desc")->first();
        return (!empty($activity) && !empty($activity->user->name)) ? $activity->user->name : null;
    }

    public function approvedBy()
    {
        $activity = $this->activities()->where('status', "approved")->where('role', "Human Resources")->orderBy('id', "desc")->first();
        return (!empty($activity) && !empty($activity->user->name)) ? $activity->user->name : null;
    }

    public function loan()
    {
        return $this->hasOne('App\Models\Loan', 'loan_request_id', 'id');
    }

    public function appraisalBonus()
    {
        return $this->belongsTo('App\Models\Appraisal\AppraisalBonus');

    }

    public static function saveLoanRequest($data)
    {
        $response = ['status' => false, 'message' => [],'data' => null];
      
            $loanRequestObj=new LoanRequest();
            $loanRequestObj->user_id = $data['user_id'];
            $loanRequestObj->approved_amount = 0.00;
            $loanRequestObj->amount = $data['amount'];
            $loanRequestObj->appraisal_bonus_id = $data['appraisal_bonus_id'];
            $loanRequestObj->description = $data['description'];
            $loanRequestObj->emi = $data['emi'] ? $data['emi'] : null;
            $loanRequestObj->emi_start_date = $data['emi_start_date'] ? $data['emi_start_date'] : null;
            $loanRequestObj->application_date = $data['application_date'];
            $loanRequestObj->status = 'pending';
    
            if(!$loanRequestObj->save()){
                $response['status']=false;
                $response['message']='Something Went Wrong';
            }
            $role = isset($data['admin']) ? 'Admin' : 'User';
            $response['data'] = $loanRequestObj->id;
            event(new LoanRequested($loanRequestObj->id, 'approved', $role, $loanRequestObj->description));
    
            $response['status'] = true;
            $response['message']='Loan Applied Successfully';
     
            
        return $response;
    }

    // copy the loan-request data to loans table
    public static function processLoanApplication($id, $data_array)
    {
        $response = ['status' => false, 'message' => []];
        try {
            $obj = self::find($id);
            if (!$obj) {
                return false;
            }

            $loanObj = new Loan;
            $loanObj->user_id = $obj->user_id;
            $loanObj->amount = $obj->approved_amount;
            $loanObj->remaining = $obj->approved_amount;
            $loanObj->appraisal_bonus_id = $obj->appraisal_bonus_id;
            $loanObj->emi = $obj->emi;
            $loanObj->description = $obj->description;
            $loanObj->comments = $obj->description;
            $loanObj->emi_start_date = $obj->emi_start_date;
            $loanObj->application_date = $obj->application_date;
            $loanObj->payment_details = $data_array['payment_detail'];
            $loanObj->payment_mode = $data_array['payment_mode'];
            $loanObj->transaction_id = $data_array['transaction_id'];
            $loanObj->payment_date = $data_array['payment_date'];
            $loanObj->loan_request_id = $data_array['loan_request_id'];
            if ($loanObj->save()) {
                // add a loan disbursal transaction detail
                $data['loan_id'] = $loanObj->id;
                $data['amount'] = $loanObj->amount;
                $data['type'] = 'debit';
                $data['comment'] = "Loan disbursed to user";
                $data['paid_on'] = $loanObj->payment_date;
                LoanTransaction::saveToDB($data);
                $response['status'] = true;
                event(new LoanDisbursed($loanObj->id));
            }
        } catch (Exception $e) {
            $response['message'] = $e->getMessage();
            // Rollback
            AccessLog::accessLog(null, 'App\Models', 'Loan', 'saveLoan', 'catch-block', $e->getMessage());
        }
    }
    public static function uploadFile(Request $request, $id)
    {
        $response = ['status' => false, 'message' => ""];
        $file = $request->file;
        $fileSize = $file->getClientSize();
        if ($fileSize > 20000000) {
            $response['message'] = "Filesize exceeds 20MB";
            return $response;
        }
        
        $content = file_get_contents($file->getRealPath());
        $type = $file->extension();
        $fileName = FileService::getFileName($type);
        $originalName = $file->getClientOriginalName();
        $store = FileService::uploadFileInDB($content, $originalName, $type, $id, "App\Models\Admin\LoanRequest");
        // $upload = FileService::uploadFileInStorage($file, $fileName, $id, "App\Models\Admin\LoanRequest", $originalName, "LoanRequest");
        
        return ['status' => true, 'message' => "File Added"];
    }

    public static function updateLoanRequest($id,$data)
    {
        $response = ['status' => false, 'message' => []];
            
        $loanRequestObj = LoanRequest::find($id);
            if (!$loanRequestObj) {
                $response['message'] = "Invalid Loan request";
                return $response;
            }
          
            if (!$loanRequestObj->status == "pending") {
                $response['message']='Only Pending Loan request Can Be Edited';
            } 

            $loanRequestObj->amount = $data['amount'];
            $loanRequestObj->appraisal_bonus_id = $data['appraisal_bonus_id']?$data['appraisal_bonus_id']:null;
            $loanRequestObj->description = $data['description'];
            $loanRequestObj->emi = $data['emi'];
    
            if(!$loanRequestObj->save()){
                $response['status']=false;
                $response['message']='Something Went Wrong';
            }

            $role = ($loanRequestObj->status == "pending" && $loanRequestObj->user_id == Auth::id()) ? "User" : "Admin";
            $result = LoanRequestActivity::saveActivity($loanRequestObj->id, "approved", $role, Auth::id(), "Edited");
            if ($result['status']) {
                $response['status'] = true;
            } else {
                $response['message'] = $result['message'];
            }
       
        return $response;
    }

    public static function  isAllowed($status, $parent_id)
    {
        $isAllowed = true;
        switch ($status) {
            case "pending":if ($parent_id != Auth::id()) {
                    $isAllowed = false;
                }
                break;
            case "submitted":if (!(Auth::user()->hasRole("human-resources"))) {
                    $isAllowed = false;
                }
                break;
            case "review":if (!Auth::user()->hasRole("management")) {
                    $isAllowed = false;
                }
                break;
            case "reconcile":if (!(Auth::user()->hasRole("human-resources"))) {
                    $isAllowed = false;
                }
                break;
            case "approved":if (!(Auth::user()->hasRole("account"))) {
                    $isAllowed = false;
                }
                break;
            default:$isAllowed = false;
        }
        if (Auth::user()->isAdmin()) {
            $isAllowed = true;
        }
        return $isAllowed;
    }

    public static function getLoanRequests($userId)
    {
        $user = User::find($userId);
        $pendingLoans = null;
        if($user->hasRole("admin") || $user->hasRole("human-resources") ||  $user->hasRole("account") || $user->hasRole("management"))
        {
            $pendingLoans = LoanRequest::whereNotIn('status' ,['disbursed','rejected'])->get();
        }
        else if($user->hasRole("reporting-manager"))
        {
            $pendingLoans = LoanRequest::whereNotIn('status' ,['disbursed','rejected'])->whereIn('user_id', $user->reportees->pluck('id'))->get();
        }
        else{
        }
        return $pendingLoans;
    }

    public function file()
    {
        return $this->morphOne(File::class, 'reference');
    }
    
}
