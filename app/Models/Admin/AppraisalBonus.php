<?php

namespace App\Models\Admin;

use App\Models\Month;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class AppraisalBonus extends Model
{
    protected $table = 'appraisal_bonuses';
    public function transactions()
    {
        return $this->morphMany('App\Models\Transaction', 'reference');
    }
    public function appraisal()
    {
        return $this->belongsTo('App\Models\Admin\Appraisal', 'appraisal_id', 'id');

    }
    public static function store($request, $monthId)
    {
        foreach ($request->new_amount as $index => $appraisal) {
            $update = AppraisalBonus::find($index);
            $update->amount = $appraisal;
            if ($update->save()) {} else {
                return false;
            }
        }
        return true;
    }

    public static function addAnnualBonus($monthId, $userId)
    {
        $response['status'] = false;
        $response['message'] = null;
        $response['data'] = null;
        // $userObj = User::find($userId)->with('activeAppraisal.userAnnualBonus')->get();
        $userObj = User::find($userId);
        $monthObj = Month::find($monthId);

        if (!$userObj || !$monthObj) {
            $response['message'] = "invalid userid or monthid";
        }

        if ($userObj->activeAppraisal && $userObj->activeAppraisal->userAnnualBonus) {
            // echo ("<pre>");
            // print_r($userObj->activeAppraisal->userAnnualBonus);
            // print_r("in");
            // die;
            $response['message'] = "Annual Bonus already Credited ";
            return $response;

        }
        if ($userObj->activeAppraisal->annual_bonus <= 0) {
            $response['message'] = "No Annual Bonus awaited for the user";
            return $response;

        }
        $appraisalObj = new AppraisalBonus();
        $appraisalObj->appraisal_id = $userObj->activeAppraisal->id;
        $appraisalObj->month_id = $monthId;
        $appraisalObj->date = date('Y-m-d');
        $appraisalObj->amount = $userObj->activeAppraisal->annual_bonus;
        $appraisalObj->type = "annual";
        $appraisalObj->status = "pending";
        if (!$appraisalObj->save()) {
            $errors[] = $appraisalObj->getErrors();
        }
        $response['status'] = true;
        $response['message'] = "Annual Appraisal Credited";
        $response['data'] = null;
        return $response;

    }

    public static function regenerateAppraisal($month_id)
    {
        $response['status'] = false;
        $response['message'] = null;
        $response['data'] = null;

        $monthObj = Month::find($month_id);

        if (!$monthObj) {
            $response['message'] = "Invalid month id " . $month_id;
            return $response;
        }
        if ($monthObj->vpfSetting == 'locked') {
            $response['message'] = "Cannot regenrate. Month is locked";
            return $response;
        }
        AppraisalBonus::where('month_id', $month_id)->delete(); //?
        $appraisals = Appraisal::where('is_active', 1)->where('monthly_var_bonus', '>', 0)->orderBy('user_id', 'ASC')->get();
        $errors = [];
        foreach ($appraisals as $appraisal) {
            $appraisalObj = new AppraisalBonus();
            $appraisalObj->appraisal_id = $appraisal->id;
            $appraisalObj->month_id = $month_id;
            $appraisalObj->date = date('Y-m-d');
            $appraisalObj->amount = $appraisal->monthly_var_bonus / 12;
            $appraisalObj->type = "monthly_variable";
            $appraisalObj->status = "pending";
            if (!$appraisalObj->save()) {
                $errors[] = $appraisalObj->getErrors();
            }
        }
        $response['status'] = true;
        $response['message'] = "Appraisal Bonus regenerated";
        $response['data'] = null;
        return $response;
    }
}
