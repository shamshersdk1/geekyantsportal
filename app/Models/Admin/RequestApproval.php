<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Admin\RequestApproveUser;
// use App\Events\RequestApproval\RequestRaised;
use App\Events\RequestApproval\RequestApproved;
use App\Events\RequestApproval\RequestRejected;
use Auth;

class RequestApproval extends Model
{

	protected $table = 'request_approvals';
	public $timestamps = true;
	use SoftDeletes;
	

	public function approvalUsers() {
		return $this->hasMany('App\Models\Admin\RequestApproveUser', 'request_id', 'id' );
	}

	public function requestedBy()
	{
			return $this->belongsTo('App\Models\User', 'user_id', 'id');
	}
	
	public static function saveData($input)
	{
		$status = true;
		$message = '';

		$user = Auth::user();
		$date = date('Y-m-d');
		$requestApprovalObj = new RequestApproval();
		$requestApprovalObj->user_id = $user->id;
		$requestApprovalObj->description = $input['description'];
		$requestApprovalObj->comment = $input['comment'];
		$requestApprovalObj->status = 'pending';
		$requestApprovalObj->notify_to =  !empty($input['notify_to']) ? json_encode($input['notify_to']) : '' ;
		$requestApprovalObj->reminder_date = $date;
		if( $requestApprovalObj->save() )
		{
			$message = $requestApprovalObj->id;
			$res = RequestApproveUser::saveApprovers($requestApprovalObj->id, $input['request_to']);
			if ( !$res['status'] )
			{
				$status = false;
				$message = $res['message'];
			}
		}
		else
		{	
			$status = false;
			$message = $requestApprovalObj->getErrors();
		}
		// if( $status ) {
		// 	event(new RequestRaised($requestApprovalObj->id));
		// }
		$response['status'] = $status;
		$response['message'] = $message;
		return $response;
	}

	public static function updateData($id, $input)
	{
		$status = true;
		$message = '';

		$requestApprovalObj = RequestApproval::find($id);
		if( empty($requestApprovalObj) )
		{
			$status = false;
			$message = 'No record found';
		}
		else
		{
			$requestApprovalObj->description = $input['description'];
			$requestApprovalObj->comment = $input['comment'];
			$requestApprovalObj->notify_to = json_encode($input['notify_to']);
			if( $requestApprovalObj->save() )
			{
				$message = $requestApprovalObj->id;
				$res = RequestApproveUser::saveApprovers($requestApprovalObj->id, $input['request_to']);
				if ( !$res['status'] )
				{
					$status = false;
					$message = $res['message'];
				}
			}
			else
			{	
				$status = false;
				$message = $requestApprovalObj->getErrors();
			}

		}
		$response['status'] = $status;
		$response['message'] = $message;
		return $response;
	}

	public static function isMyRequest($id)
	{
		$user = Auth::user();
		$requestApproveObj = RequestApproveUser::where('user_id',$user->id)->where('request_id',$id)->first();
		if ( empty($requestApproveObj) )
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	public static function checkOverallApproveStatus($id)
	{
		$requestApproveObj = RequestApproveUser::where('request_id',$id)->where('status','pending')->first();

		if ( empty($requestApproveObj) )
		{
			$requestObj = RequestApproval::find($id);
			$requestObj->status = 'approved';
			$requestObj->save();
			event(new RequestApproved($requestObj->id));
		}
		
	}

	public static function approveRequest($id)
	{
		$status = true;
		$message = '';

		$user = Auth::user();
		$requestApproveObj = RequestApproveUser::where('user_id',$user->id)->where('request_id',$id)->first();

		if ( empty($requestApproveObj) )
		{
			$status = false;
			$message = 'No record found';
		}
		else
		{
			$requestApproveObj->status = 'approved';
			if ( $requestApproveObj->save() )
			{
				$res = self::checkOverallApproveStatus($id);
			}
			else
			{
				$status = false;
				$message = $requestApproveObj->getErrors();
			}
		}
		$response['status'] = $status;
		$response['message'] = $message;
		return $response;
	}

	public static function rejectRequest($id)
	{
		$status = true;
		$message = '';

		$user = Auth::user();
		$requestApproveObj = RequestApproveUser::where('user_id',$user->id)->where('request_id',$id)->first();

		if ( empty($requestApproveObj) )
		{
			$status = false;
			$message = 'No record found';
		}
		else
		{
			$requestApproveObj->status = 'rejected';
			if ( $requestApproveObj->save() )
			{
				$requestObj = RequestApproval::find($id);
				$requestObj->status = 'rejected';
				$requestObj->save();
				event(new RequestRejected($requestObj->id));
			}
			else
			{
				$status = false;
				$message = $requestApproveObj->getErrors();
			}
		}
		$response['status'] = $status;
		$response['message'] = $message;
		return $response;
	}

	public static function isResponded($id, $user_id)
	{
		$requestApproveObj = RequestApproveUser::where('user_id',$user_id)->where('request_id',$id)->first();
		if ( empty($requestApproveObj) )
		{
			return true;
		}
		else
		{
			if ( $requestApproveObj->status == 'pending' ) {
				return false;
			}
			else {
				return true;
			}
		}
	}
}
