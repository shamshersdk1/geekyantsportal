<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class ProfileProject extends Model
{
    protected $table = 'profile_project';
    public $timestamps = false;

    public static function saveData($profileId, $projectId) {

        if(isset($profileId) && isset($projectId)) {

            $exists = ProfileProject::where('profile_id',$profileId)->where('project_id',$projectId)->first();

            if(count($exists) < 1) {
                $projectProfile = new ProfileProject();
                $projectProfile->profile_id = $profileId;
                $projectProfile->project_id = $projectId;
                if($projectProfile->save()) {
                    return true;
                }
            }
        }
        return false;
    }

    public static function saveEditData($profileId, $projectId) {

        if(isset($profileId) && isset($projectId)) {

            $exists = ProfileProject::where('profile_id',$profileId)->where('project_id',$projectId)->first();
            if(count($exists) < 1) {
                $projectProfile = new ProfileProject();
                $projectProfile->profile_id = $profileId;
                $projectProfile->project_id = $projectId;
                if($projectProfile->save()) {
                    return true;
                }
            }
        }
        return false;
    }

    public static function deleteData($profileId, $projectIds) {


        if(isset($profileId) && is_array($projectIds)) {
            foreach ($projectIds as $projectId) {
                    $exists = ProfileProject::where('profile_id',$profileId)->where('project_id',$projectId)->first();
                if(count($exists) >= 1) {
                    ProfileProject::where('profile_id',$profileId)->where('project_id',$projectId)->delete();
                }
            }
        }
        return false;
    }
}
