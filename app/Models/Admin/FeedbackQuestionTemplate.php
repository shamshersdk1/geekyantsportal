<?php namespace App\Models\Admin;

use App\Models\BaseModel;
use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\Admin\File;
use App\Services\FileService;

class FeedbackQuestionTemplate extends BaseModel
{

	use ValidatingTrait;

	protected $table = 'feedback_question_templates';
	public $timestamps = true;
	use SoftDeletes;

	private $rules = array(
			'question'  => 'required',
	    );

	public function scopeBuildQuery($query)
	{
		return $query;
	}
	public function feedbackdesignations()
	{
			return $this->hasMany('App\Models\Admin\FeedbackQuestionDesignationTemplate','feedback_question_template_id','id');
	}
	public function role()
	{
			return $this->belongsTo('App\Models\Admin\Role','role_type','id');
	}

	public function scopeGenericSearch($query, $value)
    {
        $query = $query->orWhere('question', 'LIKE', '%'.$value.'%');
        return $query;
	}
	public static function updateStatus($id)
	{
		$status = true;
		$message = '';
		$question = FeedbackQuestionTemplate::find($id);
		if ( empty($question) )
		{
			$status = false;
			$message = 'No record found';
		}
		$toggle = FeedbackQuestionTemplate::where('id', $id)->update(['status' => !$question->status]);

		$response['status'] = $status;
		$response['message'] = $message;
		return $response;

	}

}
