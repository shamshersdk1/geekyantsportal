<?php

namespace App\Models\Admin;
use App\Services\LeaveService;
use Watson\Validating\ValidatingTrait;

use App\Models\BaseModel;

use App\Models\User;
use Mail;

class Role extends BaseModel
{

	use ValidatingTrait;

    protected $table = 'roles';
    public $timestamps = true;
    private $rules = array(
            'name' => 'required',
        );

    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'user_roles', 'role_id', 'user_id');
    }

    public function questions()
    {
        return $this->morphMany('App\Models\Admin\QuestionMapping', 'reference');
    }
}
