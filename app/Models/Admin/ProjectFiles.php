<?php


namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use App\Models\BaseModel;
use Watson\Validating\ValidatingTrait;

class ProjectFiles extends BaseModel
{
    use ValidatingTrait;

    protected $table = 'project_files';

    private $rules = array(
		
	        'project_id' => 'required',
	        'path' => 'required',

	        // 'description'  => 'required'
	    );

    public function projects(){
    	return $this->belongsTo('App\Models\Admin\Project');
    }
      public function scopeBuildQuery($query)
    {
        return $query;
    }
}

