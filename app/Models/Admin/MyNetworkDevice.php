<?php

namespace App\Models\Admin;

use App\Models\AccessLog;
use App\Models\Admin\IpAddressMapper;
use App\Models\BaseModel;
use App\Models\Notify;
use DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class MyNetworkDevice extends BaseModel
{
    use SoftDeletes;

    protected $table = 'my_network_devices';
    protected $dates = ['deleted_at'];

    public function scopeBuildQuery($query)
    {
        return $query;
    }

    public function getURI(){
        return '/user/assets';
    }

    public function ip()
    {
        return $this->morphOne('App\Models\Admin\IpAddressMapper', 'reference');
    }
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
    public function addedBy()
    {
        return $this->belongsTo('App\Models\User', 'added_by', 'id');
    }
    public static function saveData($data, $myDeviceObj)
    {
        $userObj = \Auth::User();
        DB::beginTransaction();
        try {
            $response = [
                'result' => "",
                'status' => true,
                'message' => "success",
                'errors' => [],
            ];
            $myDeviceObj->name = !empty($data['name']) ? $data['name'] : null;
            $myDeviceObj->user_id = !empty($data['user_id']) ? $data['user_id'] : null;
            $myDeviceObj->mac_address = !empty($data['mac_address']) ? $data['mac_address'] : null;
            $myDeviceObj->added_by = $userObj->id;
            if (!$myDeviceObj->save()) {
                $response['status'] = false;
                $response['message'] = "Unable to save new Device";
                $response['errors'] = $myDeviceObj->getErrors();
                DB::rollback();
                return $response;
            } else {
                $oldIp = IpAddressMapper::where('reference_type', 'App\Models\Admin\MyNetworkDevice')->where('reference_id', $myDeviceObj->id)->first();
                if ($oldIp) {
                    if (!$oldIp->delete()) {
                        $response['status'] = false;
                        $response['message'] = "Unable to assign new ip to the device";
                        $response['errors'] = "Unable to assign new ip to the device";
                        DB::rollback();
                        return $response;
                    }
                }
                $ipAddressMapperObj = new IpAddressMapper;
                $ipAddressMapperObj->reference_type = "App\Models\Admin\MyNetworkDevice";
                $ipAddressMapperObj->reference_id = $myDeviceObj->id;
                $ipAddressMapperObj->user_ip_address_id = !empty($data['user_ip_address_id']) ? $data['user_ip_address_id'] : null;
                if (!$ipAddressMapperObj->save()) {
                    $response['status'] = false;
                    $response['message'] = "Unable to assign ip to the device";
                    $response['errors'] = $ipAddressMapperObj->getErrors();
                    DB::rollback();
                    return $response;
                }
                $payload = ['dhcp_changed' => 1];
                Notify::saveData('dhcp_update', $payload);
            }
            DB::commit();
            $response['status'] = true;
            $response['message'] = "Saved!";
            $response['result'] = MyNetworkDevice::where('id', $myDeviceObj->id)->with('ip.userIp')->first();
            return $response;
        } catch (\Exception $e) {
            $response['status'] = false;
            $response['message'] = $e->getMessage();
            AccessLog::accessLog(null, ' App\Models\Admin\MyNetworkDevice', 'MyNetworkDevice', 'saveData', 'catch-block', $e->getMessage());
            DB::rollback();
        }
        return $response;
    }
}
