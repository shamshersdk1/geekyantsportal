<?php

namespace App\Models\Admin;

use App\Models\BaseModel;

class AssetMeta extends BaseModel
{

    protected $table = 'asset_meta';
    public $timestamps = false;

    protected $fillable = ['asset_id', 'key', 'value'];

    public function scopeBuildQuery($query)
    {
        return $query;
    }
    public function ipMapper()
    {
        return $this->morphOne('App\Models\Admin\IpAddressMapper', 'reference');
    }
    public function asset()
    {
        return $this->belongsTo('App\Models\Admin\Asset');
    }
    // public function asset()
    // {
    // }
}
