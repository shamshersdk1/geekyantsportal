<?php

namespace App\Models\Admin;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\BaseModel;

use Auth;
use DB;
use Config;

class PayslipDataMonth extends BaseModel
{
    protected $dates = ['deleted_at'];
    protected $table = 'payslip_data_months';
    public $timestamps = true;
    use SoftDeletes;

    public function payslipDataMaster() {
        return $this->hasMany('App\Models\Admin\PayslipDataMaster', 'payslip_data_month_id', 'id');
    }
    public function payslipDataItems() {
        return $this->hasMany('App\Models\Admin\PayslipDataItem', 'payslip_data_month_id', 'id');
    }
}
