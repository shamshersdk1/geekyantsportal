<?php

namespace App\Models\Admin;

use App\Models\BaseModel;

class ResourcePrice extends BaseModel
{

    public $timestamps = false;
    protected $hidden = array('price');
    public function scopeBuildQuery($query)
    {
        return $query;
    }
    public function project()
    {
        return $this->hasOne('App\Models\Admin\Project', 'id', 'project_id');
    }
}
