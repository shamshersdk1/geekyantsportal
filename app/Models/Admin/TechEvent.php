<?php namespace App\Models\Admin;

use App\Models\BaseModel;
use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\Admin\File;
use App\Services\FileService;

class TechEvent extends BaseModel 
{

	use ValidatingTrait;
	
	protected $table = 'tech_events';
	public $timestamps = true;
	use SoftDeletes;

	private $rules = array(
			'description'  => 'required',
			'location'  => 'required',
			'url' => 'required',
			'start_date' => 'required',
			'end_date' => 'required'
	    );

	
	public function image()
	{
		return $this->hasOne('App\Models\Admin\File', 'reference_id', 'id')->where('reference_type','App\Models\Admin\TechEvent');
	}
	public function logoImage()
	{
		return $this->hasOne('App\Models\Admin\File', 'reference_id', 'id')->where('reference_type','App\Models\Admin\TechEventLogo');
	}
	public static function saveData($input)
	{
		$status = true;
		$message = '';
		
		$techEventObj = new TechEvent();
		$techEventObj->title = $input['title'];
		$techEventObj->location = $input['location'];
		$techEventObj->status = $input['status'];
		$techEventObj->category = $input['category'];
		$techEventObj->description = $input['description'];
		$techEventObj->url = $input['url'];
		$techEventObj->start_date = date_to_yyyymmdd($input['start_date']);
		$techEventObj->end_date = date_to_yyyymmdd($input['end_date']);
		
		if( $techEventObj->save() )
		{
			$message = $techEventObj->id;
			$reference_type = 'App\Models\Admin\TechEvent';
			$prefix = 'tech_event';
			$reference_id = $techEventObj->id;
			$single_file = $input['display_file'];
			$type = $single_file->extension();
			$fileName = FileService::getFileName($type);
			$originalName = $single_file->getClientOriginalName();
			$upload = FileService::uploadFile($single_file, $fileName, $reference_id, $reference_type, $originalName, $prefix);
			if( !empty($input['logo_file']) )
			{
				$reference_type_logo = 'App\Models\Admin\TechEventLogo';
				$prefix_logo = 'tech_event';
				$reference_id_logo = $techEventObj->id;
				$single_file_logo = $input['logo_file'];
				$type_logo = $single_file_logo->extension();
				$fileName_logo = FileService::getFileName($type_logo);
				$originalName_logo = $single_file_logo->getClientOriginalName();
				$upload_logo = FileService::uploadFile($single_file_logo, $fileName_logo, $reference_id_logo, $reference_type_logo, $originalName_logo, $prefix_logo);
			}
		}
		else
		{
			$status = false;
			$message = $techEventObj->getErrors()->toArray();
		}
		
		$response['status'] = $status;
		$response['message'] = $message;
		
		return $response;
	}

	public static function updateData($id, $input)
	{
		$status = true;
		$message = '';

		$techEventObj = TechEvent::find($id);
		if( empty($techEventObj) )
		{
			$status = false;
			$message = 'No record found';
		}

		$techEventObj->title = $input['title'];
		$techEventObj->location = $input['location'];
		$techEventObj->status = $input['status'];
		$techEventObj->category = $input['category'];
		$techEventObj->description = $input['description'];
		$techEventObj->url = $input['url'];
		$techEventObj->start_date = date_to_yyyymmdd($input['start_date']);
		$techEventObj->end_date = date_to_yyyymmdd($input['end_date']);

		if( $techEventObj->save() )
		{
			$message = $techEventObj->id;
			if ( !empty($input['display_file']) )
			{
				$reference_type = 'App\Models\Admin\TechEvent';
				$prefix = 'tech_event';
				
				$file = File::where('reference_type',$reference_type)->where('reference_id',$id)->first();
				if( isset($file) ){
						$file->delete();
				}
				$reference_id = $techEventObj->id;
				$single_file = $input['display_file'];
				$type = $single_file->extension();
				$fileName = FileService::getFileName($type);
				$originalName = $single_file->getClientOriginalName();
				$upload = FileService::uploadFile($single_file, $fileName, $reference_id, $reference_type, $originalName, $prefix);
			}
			if( !empty($input['logo_file']) )
			{
				$reference_type_logo = 'App\Models\Admin\TechEventLogo';
				$prefix_logo = 'tech_event';
				$fileLogo = File::where('reference_type',$reference_type_logo)->where('reference_id',$id)->first();
				if( isset($fileLogo) ){
						$fileLogo->delete();
				}
				$reference_id_logo = $techEventObj->id;
				$single_file_logo = $input['logo_file'];
				$type_logo = $single_file_logo->extension();
				$fileName_logo = FileService::getFileName($type_logo);
				$originalName_logo = $single_file_logo->getClientOriginalName();
				$upload_logo = FileService::uploadFile($single_file_logo, $fileName_logo, $reference_id_logo, $reference_type_logo, $originalName_logo, $prefix_logo);
			}
		}
		else
		{
			$status = false;
			$message = $techEventObj->getErrors()->toArray();
		}

		$response['status'] = $status;
		$response['message'] = $message;
		
		return $response;
	}

	public static function deleteRecord($id)
	{
		$status = true;
		$message = '';

		$techEventObj = TechEvent::find($id);
		if( empty($techEventObj) )
		{
			$status = false;
			$message = 'No record found';
		}

		if( !$techEventObj->delete() )
		{
			$status = false;
			$message = $techEventObj->getErrors();
		}
		$response['status'] = $status;
		$response['message'] = $message;
		
		return $response;
	}

	public static function updateStatus($id)
	{
		$status = true;
		$message = '';
		$techEvent = TechEvent::find($id);
		if ( empty($techEvent) )
		{
			$status = false;
			$message = 'No record found';
		}
		$toggle = TechEvent::where('id', $id)->update(['status' => !$techEvent->status]);
		
		$response['status'] = $status;
		$response['message'] = $message;
		return $response;

	}
}
