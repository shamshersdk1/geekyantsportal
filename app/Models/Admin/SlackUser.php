<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class SlackUser extends Model
{
    protected $table = 'slack_user';
    public $timestamps = false;

    public function users()
    {
      return $this->belongsToMany('App\Models\User', 'user_slack_user', 'slack_user_id', 'user_id');
    }
    public function getUserAttribute()
    {
        return $this->users()->first();
    }
    public static function saveSlackUsers($slackUsers)
    {
      foreach($slackUsers as $slackUser)
      {
        if(array_key_exists("email", $slackUser->profile)) {
            $dbUser = SlackUser::where('email', $slackUser->profile->email)->first();
            if($dbUser) {
                $dbUser->slack_id = empty($slackUser->id) ? null : $slackUser->id ;
                $dbUser->team_id = empty($slackUser->team_id) ? null : $slackUser->team_id ;
                $dbUser->name = empty($slackUser->name) ? null : $slackUser->name ;
                $dbUser->deleted = empty($slackUser->deleted) ? null : $slackUser->deleted ;
                $dbUser->status = empty($slackUser->status) ? null : $slackUser->status ;
                $dbUser->color = empty($slackUser->color) ? null : $slackUser->color ;
                $dbUser->first_name = empty($slackUser->profile->first_name) ? null : $slackUser->profile->first_name ;
                $dbUser->last_name = empty($slackUser->profile->last_name) ? null : $slackUser->profile->last_name ;
                $dbUser->avatar_hash = empty($slackUser->profile->avatar_hash) ? null : $slackUser->profile->avatar_hash ;
                $dbUser->email = empty($slackUser->profile->email) ? null : $slackUser->profile->email ;
                $dbUser->is_admin = empty($slackUser->is_admin) ? null : $slackUser->is_admin ;
                $dbUser->is_owner = empty($slackUser->is_owner) ? null : $slackUser->is_owner ;
                $dbUser->is_primary_owner = empty($slackUser->is_primary_owner) ? null : $slackUser->is_primary_owner ;
                $dbUser->is_restricted = empty($slackUser->is_restricted) ? null : $slackUser->is_restricted ;
                if($dbUser->save()){
                    $user = User::where('email', $dbUser->email)->first();
                    // if(!empty($user)) {
                    //     if(empty($user->slackUsers) || count($user->slackUsers) == 0) {
                    //         // $user->slackUsers()->attach($dbUser->id);
                    //     }
                    // } else {
                    //     // $dbUser->delete();
                    // }
                }
                else {
                    return "Slack user with id=".$dbuser->id."not updated\n";
                }
            }
            else {
                $newUser = new SlackUser();
                $newUser->slack_id = empty($slackUser->id) ? null : $slackUser->id ;
                $newUser->team_id = empty($slackUser->team_id) ? null : $slackUser->team_id ;
                $newUser->name = empty($slackUser->name) ? null : $slackUser->name ;
                $newUser->deleted = empty($slackUser->deleted) ? null : $slackUser->deleted ;
                $newUser->status = empty($slackUser->status) ? null : $slackUser->status ;
                $newUser->color = empty($slackUser->color) ? null : $slackUser->color ;
                $newUser->first_name = empty($slackUser->profile->first_name) ? null : $slackUser->profile->first_name ;
                $newUser->last_name = empty($slackUser->profile->last_name) ? null : $slackUser->profile->last_name ;
                $newUser->avatar_hash = empty($slackUser->profile->avatar_hash) ? null : $slackUser->profile->avatar_hash ;
                $newUser->email = empty($slackUser->profile->email) ? null : $slackUser->profile->email ;
                $newUser->is_admin = empty($slackUser->is_admin) ? null : $slackUser->is_admin ;
                $newUser->is_owner = empty($slackUser->is_owner) ? null : $slackUser->is_owner ;
                $newUser->is_primary_owner = empty($slackUser->is_primary_owner) ? null : $slackUser->is_primary_owner ;
                $newUser->is_restricted = empty($slackUser->is_restricted) ? null : $slackUser->is_restricted ;
                if($newUser->save()){
                    // $user = User::where('email', $newUser->email)->first();
                    // if(!empty($user)) {
                    //     // $user->slackUsers()->attach($newUser->id);
                    // } else {
                    //     // $newUser->delete();
                    // }
                }
                else{
                    return "User with slack_id = ".$newUser->slack_id."not saved\n";
                }
            }
        }           
      }
      return null;
    }
}
