<?php

namespace App\Models\Admin;

use App\Models\AccessLog;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Collection;
use Watson\Validating\ValidatingTrait;

class AssetAssignmentDetail extends BaseModel
{

    use ValidatingTrait;
    protected $table = 'asset_assignment_details';
    public $timestamps = false;

    private $rules = array(
        'user_id' => 'required',
        'asset_id' => 'required',
        'from_date' => 'required',
        'assignee_id' => 'required',
    );
    protected $fillable = ['asset_id', 'user_id', 'to_date', 'from_date'];

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
    public function asset()
    {
        return $this->hasOne('App\Models\Admin\Asset', 'id', 'asset_id');
    }
    public function assignee()
    {
        return $this->hasOne('App\Models\User', 'id', 'assignee_id');
    }
    public function releasedBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'released_by');
    }
    // public function ipMapper()
    // {
    //     return $this->morphOne('App\Models\Admin\IpAddressMapper', 'reference');
    // }
    public function scopeBuildQuery($query)
    {
        return $query;
    }
    public static function checkAvailability($id, $start_date)
    {
        $assigned_flag = false;
        $assignList = AssetAssignmentDetail::where('asset_id', $id)->get();
        foreach ($assignList as $assign) {
            if ($assign->from_date <= $start_date) {
                if (empty($assign->to_date) || $assign->to_date == '0000-00-00') {
                    $assigned_flag = true;
                }
                if (isset($assign->to_date)) {
                    if ($assign->to_date >= $start_date) {
                        $assigned_flag = true;
                    }
                    if ($assign->to_date < $start_date) {
                        $assigned_flag = false;
                    }
                }
            }
        }
        return ($assigned_flag);
    }
    public static function assignedAssets($user_id)
    {
        $date = date('Y-m-d');

        $assignedAssetsEndDated = AssetAssignmentDetail::with('asset')
            ->where('user_id', $user_id)
            ->where('from_date', '<=', $date)
            ->where('to_date', '>=', $date)->get();

        $assignedAssetsWithNull = AssetAssignmentDetail::with('asset')
            ->where('user_id', $user_id)
            ->where('from_date', '<=', $date)
            ->whereNull('to_date')->get();

        $assignedAssetsWithDefaultEntries = AssetAssignmentDetail::with('asset')
            ->where('user_id', $user_id)
            ->where('from_date', '<=', $date)
            ->where('to_date', '=', '0000-00-00')->get();

        $assignedAssets = new Collection;
        $assignedAssets = $assignedAssets->merge($assignedAssetsEndDated);
        $assignedAssets = $assignedAssets->merge($assignedAssetsWithNull);
        $assignedAssets = $assignedAssets->merge($assignedAssetsWithDefaultEntries);

        return $assignedAssets;
    }

    public static function assignedUser($asset_id)
    {
        $date = date('Y-m-d');

        $assignedUsersEndDated = AssetAssignmentDetail::with('user')
            ->where('asset_id', $asset_id)
            ->where('from_date', '<=', $date)
            ->where('to_date', '>=', $date)->get();

        $assignedUsersWithNull = AssetAssignmentDetail::with('user')
            ->where('asset_id', $asset_id)
            ->where('from_date', '<=', $date)
            ->whereNull('to_date')->get();

        $assignedUsersWithDefaultEntries = AssetAssignmentDetail::with('user')
            ->where('asset_id', $asset_id)
            ->where('from_date', '<=', $date)
            ->where('to_date', '=', '0000-00-00')->get();

        $assignedUsers = new Collection;
        $assignedUsers = $assignedUsers->merge($assignedUsersEndDated);
        $assignedUsers = $assignedUsers->merge($assignedUsersWithNull);
        $assignedUsers = $assignedUsers->merge($assignedUsersWithDefaultEntries);
        return $assignedUsers;
    }

    public static function assetsHistory($user_id)
    {
        $assets = AssetAssignmentDetail::with('asset')
            ->where('user_id', $user_id)->get();

        return $assets;
    }
    public static function getCurrentAsset($user_id)
    {
        $assets = AssetAssignmentDetail::with('asset.metas.ipMapper.userIp')
            ->where('user_id', $user_id)
            ->where(function ($query) {
                $query->where('to_date', '>=', date("Y-m-d"))
                    ->orWhereNull('to_date');
            })
            ->get();

        return $assets;
    }
    public static function getPrevoiusAsset($user_id)
    {
        $assets = AssetAssignmentDetail::with('asset.metas.ipMapper.userIp')
            ->where('user_id', $user_id)
            ->whereDate('to_date', '<=', date('Y-m-d'))
            ->get();

        return $assets;
    }

    public static function currentuser($asset_id)
    {
        $date = date('Y-m-d');
        $assets = AssetAssignmentDetail::with('asset.metas.ipMapper.userIp')
            ->where('asset_id', $asset_id)
            ->orderBy('from_date', 'DESC')->first();

        return $assets;
    }
    public static function saveData($data, $assetObj)
    {
        try {
            $response = [
                'result' => "",
                'status' => true,
                'message' => "",
                'errors' => [],
            ];
            $assetObj->user_id = $data['user_id'];
            $assetObj->asset_id = $data['asset_id'];
            $assetObj->to_date = !empty(($data['to_date'])) ? date_to_yyyymmdd($data['to_date']) : null;
            $assetObj->from_date = date_to_yyyymmdd($data['from_date']);
            $assetObj->assignee_id = \Auth::user()->id;
            if (!$assetObj->save()) {
                $response['status'] = false;
                $response['message'] = "Unable to save new Device";
                $response['errors'] = $assetObj->getErrors();
                return $response;
            }
            $response['result'] = AssetAssignmentDetail::where('id', $assetObj->id)->with('user', 'assignee', 'releasedBy', 'asset.metas.ipMapper.userIp')->first();
            return $response;
        } catch (\Exception $e) {
            $response['status'] = false;
            $response['message'] = $e->getMessage();
            AccessLog::accessLog(null, ' App\Models\Admin\AssetAssignmentDetail', 'Asset assignment detail', 'saveData', 'catch-block', $e->getMessage());
        }
        return $response;

    }
    public static function getAssets($userId){

        $date = date('Y-m-d');

        $response = AssetAssignmentDetail::where('user_id', $userId)->where('from_date', '<=', $date)->where(function ($query) use ($date) {
            $query->where('to_date', null)->orWhere('to_date', '>=', $date);
        })->whereNull('released_by')->get();


        return $response;
    }   
}
