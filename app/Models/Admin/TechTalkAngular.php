<?php namespace App\Models\Admin;

use App\Models\BaseModel;
use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class TechTalkAngular extends BaseModel{
    protected $table="tech_talk";
    public $timestamps = true;
    use SoftDeletes;


}

	