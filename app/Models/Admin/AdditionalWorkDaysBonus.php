<?php

namespace App\Models\Admin;

use App\Models\BaseModel;
use App\Models\Bonus\AdditionalWorkDayProject;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\BonusRequestProject;
use App\Models\BonusRequest;
use App\Models\Appraisal\Appraisal;
use App\Models\NonworkingCalendar;
use App\Services\TimesheetService;

class AdditionalWorkDaysBonus extends BaseModel
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $table = 'additional_work_days_bonuses';
    //public $timestamps = false;

    public function scopeBuildQuery($query)
    {
        return $query;
    }

    public function reference()
    {
        return $this->morphTo('reference');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'employee_id', 'id');
    }
    public function projects()
    {
        return $this->hasMany('App\Models\Bonus\AdditionalWorkDayProject', 'additional_workday_id', 'id');
    }
    public function reviewer()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
    }

    public static function saveBonus($data)
    {
   
        $response['status'] = false;
        $response['message'] = '';
        $additionalBonusRequest = new AdditionalWorkDaysBonus();

        $additionalBonusRequest->user_id = $data['user_id'];
        $additionalBonusRequest->type = $data['type'];
        $additionalBonusRequest->project_id = !empty($data['project_id']) ? $data['project_id'] : null;
        $additionalBonusRequest->date = $data['date'];
        $additionalBonusRequest->status = !empty($data['status']) ? $data['status']: 'pending';
        $additionalBonusRequest->redeem_type = !empty($data['redeem_type'])? $data['redeem_type'] : 'encash';
        //$additionalBonusRequest->duration = substr($data['duration'],1,1);
        $additionalBonusRequest->duration = !empty($data['duration']) ? $data['duration'] :'';
        $additionalBonusRequest->amount = !empty($data['amount'])? $data['amount'] :'';
        $additionalBonusRequest->reviewed_by = !empty($data['reviewed_by']) ? $data['reviewed_by'] : '';
        $additionalBonusRequest->created_by = !empty($data['created_by']) ? $data['created_by'] : '';

        if(!empty($data['created_at'])){
            $additionalBonusRequest->created_at=$data['created_at'];
        }
        if(!empty($data['updated_at'])){
            $additionalBonusRequest->updated_at=$data['updated_at'];
        }

        if ($additionalBonusRequest->save()) {
            //bonus_request_projects 
            if(!empty($data['projects']) && count($data['projects'])>0) {
                foreach ($data['projects'] as $projectId) {
                    $additonalProjectObj = new AdditionalWorkDayProject;
                    $additonalProjectObj->additional_workday_id = $additionalBonusRequest->id;
                    $additonalProjectObj->project_id = $projectId;
                    $additonalProjectObj->save();
                }
            }
            
            //::where('bonus_request_id',$additionalBonusRequest)->
            $response['data'] = $additionalBonusRequest;
            $response['status'] = true;
            $response['message'] = 'Successfully saved';
        }
        return $response;

    }
    public  static function migrateBonusRequestToAdditional($bonusRequestId){
        $bonus =BonusRequest::find($bonusRequestId);
        if(!$bonus) 
            return false;
        $data = [];
        $data['user_id']=$bonus->user_id ? $bonus->user_id :'';
        $data['month_id']=$bonus->month_id ? $bonus->month_id : '';
        $data['date']=$bonus->date  ? $bonus->date :'';
        $data['status']=$bonus->status;
        $data['notes']=$bonus->notes ? $bonus->notes : '';
        $data['reviewed_by']=$bonus->approver_id ? $bonus->approver_id :'';
        $data['created_by']=$bonus->user_id ? $bonus->user_id :'';
        $data['created_at']= $bonus->created_at ? $bonus->created_at :'';
        $data['updated_at']=$bonus->updated_at ? $bonus->updated_at :'';
        $data['approved_at']=$bonus->updated_at ? $bonus->updated_at :'';

        if($bonus){
            $redeem_type= json_decode($bonus->redeem_type);
            // print_r($matches[0][0]);
            // die;
            if(empty($redeem_type->duration)){
                \Log::info('Bonus Req : '.$bonus->id. ' duration  not found');
             }
             else{
                
                //preg_match_all('!\d+!', $redeem_type->duration, $matches);
                //$data['duration']=$matches[0][0];
                $duration = str_replace('h','',$redeem_type->duration);
                // \Log::info('duration T -> : '.$t);
                //$data['duration'] = str_replace('h','',$redeem_type->duration);
                $data['duration'] =$duration;
                $amount=Appraisal::getBonusAmount($data['user_id'],$data['user_id']);
                $data['amount']=round($amount/8*$data['duration']);
                
             }
             if(empty($redeem_type->redeem_type)){
                \Log::info('Bonus Req : '.$bonus->id. ' redeem_type  not found');
             }
             else{
                $data['redeem_type']=$redeem_type->redeem_type;
             }

        }

        $bonusProject=BonusRequestProject::where('bonus_request_id',$bonus->id)->get();
        
        // $additionalProject=[];
        $data['projects']=[];
        if(count($bonusProject) > 0 ){
            foreach($bonusProject as $project)
            $data['projects'][] =$project->project_id;
        }

        $data['type']='weekend';

        $is_weekend=NonworkingCalendar::isWeekend($data['date']);
        $is_holiday=NonworkingCalendar::is_holiday($data['date']);
        if($is_weekend)
            $data['type']='weekend';
        elseif(!$is_holiday)
            $data['type']='holiday';
        else {
            $data['type']= 'leave';
        }
        ///$data['created_by']=$data['user_id'];
        $additionalObj=self::saveBonus($data);

        $additional= $additionalObj['data'];

        if( $additional && $additional->status =='approved' ){
            $data['reference_id']=$additional->id;
            $data['reference_type']='App\Models\Admin\AdditionalWorkDaysBonus';
            $data['type']='additional';
            Bonus::saveApprovedBonus($data);

        }



    }

}
