<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class PayrollRuleForCsv extends Model
{
    public function key()
    {
        return $this->morphMany('App\Models\Transaction', 'reference');
    }
    public static function getValue($key)
    {
        $rule = PayrollRuleForCsv::where('key', $key)->first();
        if($rule) {
            return $rule->value;
        } else {
            return null;
        }
    }

}
