<?php namespace App\Models\Admin;

use App\Models\BaseModel;
use OwenIt\Auditing\Contracts\Auditable;

class PurchaseOrderItem extends BaseModel implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    protected $table = 'purchase_order_items';
    public $timestamps = false;

}
