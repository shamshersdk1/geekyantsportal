<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class Calendar extends Model
{

	use ValidatingTrait;
    protected $table = 'non_working_calendar';
    public $timestamps = true;

    private $rules = array(
            'date' => 'required',
            'type'  => 'required',
        );
    
    public static function saveData($request) {
    	$response =  array('status' => true,'message' => "success" ,'id' => NULL);

    	$calendar = new Calendar();
    	if($request['date'])
    	$calendar->date = date("Y-m-d", strtotime($request['date']));
    	$calendar->type = $request['type'];
    	$calendar->reason = $request['reason'];
    	$calendar->color = $request['color'];
    	if($calendar->save())
    	{
    		return $response;
    	}
    	else{
    		$response['status'] = false;
    		$response['errors'] =  $leave->getErrors();
    		return $response;
    	}
    }
}
