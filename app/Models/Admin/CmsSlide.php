<?php namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class CmsSlide extends Model 
{

	use ValidatingTrait;
	
	protected $table = 'cms_slides';
	public $timestamps = true;
	use SoftDeletes;

	private $rules = array(
	        'cms_technology_id' => 'required',
	        'title'  => 'required',
			'description'  => 'required',
			'user_name'  => 'required',
	        'status' => 'required',
	    );

	public function cms_developer()
	{
		return $this->hasOne('App\Models\Admin\CmsDeveloper', 'id', 'cms_technology_id');
	}
	public function image()
	{
		return $this->hasOne('App\Models\Admin\File', 'reference_id', 'id');
	}
}
