<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class ProjectQuotation extends Model
{
    use ValidatingTrait;

    protected $table = 'project_quotation';
    public $timestamps = true;
    protected $validationMessages = [
        'client_id.required' => "Please select client name"
    ];

    private $rules = array(
            'name' => 'required',
            'client_id'  => 'required',
        );

    protected $fillable =  ['name', 'client_id', 'description'];

    public function projectQuotationMilestones() {
    	return $this->hasMany('App\Models\Admin\ProjectQuotationMilestone', 'project_quotation_id');
    }

    public function client() {
        return $this->hasOne('App\Models\Admin\Client', 'id','client_id');
    }

}
