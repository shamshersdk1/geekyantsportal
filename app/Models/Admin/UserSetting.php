<?php

namespace App\Models\Admin;

use App\Models\BaseModel;

class UserSetting extends BaseModel
{
    public $timestamps = false;
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
    public function setValue($userId, $key, $value)
    {
        $response = [];
        $response['status'] = false;
        $response['message'] = '';

        $settingObj = UserSetting::where('user_id', $userId)->where('key', $key)->first();

        if ($settingObj) {
            $settingObj->value = $value;

            if ($settingObj->save()) {
                $response['status'] = true;
                $response['message'] = 'User\'s key updated.';
            }
        } else {

            $this->user_id = $userId;
            $this->key = $key;
            $this->value = $value;

            if ($this->save()) {
                $response['status'] = true;
                $response['message'] = 'User\'s key added';
            }
        }

        return $response;
    }
    public function getValue($userId, $key)
    {
        $settingObj = UserSetting::where('user_id', $userId)->where('key', $key)->first();

        if ($settingObj) {
            return $settingObj->value;
        }

        return null;
    }
}
