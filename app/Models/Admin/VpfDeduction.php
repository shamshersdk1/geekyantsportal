<?php

namespace App\Models\Admin;

use App\Models\Admin\VPF;
use App\Models\Month;
use App\Models\MonthSetting;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Model\Admin;
use Illuminate\Database\Eloquent\SoftDeletes;

class VpfDeduction extends Model
{
    use SoftDeletes;
    //protected $table = 'vpf_deductions';
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
    public function vpf()
    {
        return $this->belongsTo('App\Models\Admin\VPF');
    }
    public static function getVPFPaidTillDate($userId, $monthId)
    {
        $paidTillDate = 0;
        $monthObj = Month::find($monthId);
        if ($monthObj) {
            $months = Month::where('financial_year_id', $monthObj->financial_year_id)->where('month_number', '<', $monthObj->month_number)->orderBy('month_number')->get();
            foreach ($months as $month) {
                $vpfDeduction = self::where('month_id', $month->id)->where('user_id', $userId)->first();
                if ($vpfDeduction) {
                    $paidTillDate += $vpfDeduction->amount;
                }
            }
        }
        return $paidTillDate;
    }
    public static function getCurrMonthVpf($monthId, $userId)
    {
        $monthObj = Month::find($monthId);
        if ($monthObj && $monthObj->vpfSetting->value == 'locked') {
           
            $vpfObj = VpfDeduction::where('month_id', $monthId)->where('user_id', $userId)->first();
            if ($vpfObj) {
                return $vpfObj->amount;
            } else {
                return 0;
            }
        }
        return null;
    }
    public static function regenerateVpf($month_id)
    {
        $response['status'] = false;
        $response['message'] = null;
        $response['data'] = null;

        $monthObj = Month::find($month_id);

        if (!$monthObj) {
            $response['message'] = "Invalid month id " . $month_id;
            return $response;
        }
        if ($monthObj->vpfSetting == 'locked') {
            $response['message'] = "Cannot regenrate. Month is locked";
            return $response;
        }
        VpfDeduction::where('month_id', $month_id)->delete();

        //  $response = MonthSetting::toggleMonth($month_id);
        //  $response = MonthSetting::toggleMonth($month_id);

        $vpfs = VPF::where('parent_id', null)->get();
        $errors = [];
        foreach ($vpfs as $vpf) {
            $deductionObj = new VpfDeduction();
            $deductionObj->month_id = $month_id;
            $deductionObj->user_id = $vpf->user_id;
            $deductionObj->amount = -$vpf->amount;
            $deductionObj->vpf_id = $vpf->id;
            if (!$deductionObj->save()) {
                $errors[] = $deductionObj->getErrors();
            }
        }
        $response['status'] = true;
        $response['message'] = "VPF deduction regenerated";
        $response['data'] = null;
        return $response;
    }

    public static function store($request, $monthId)
    {
        foreach ($request->new_amount as $index => $vpf) {
            $vpr = VpfDeduction::find($index);
            $vpr->amount = $request->new_amount[$index];
            if ($vpr->save()) {} else {
                return false;
            }

        }
        return true;
    }

}
