<?php

namespace App\Models\Admin;

use App\Models\AccessLog;
use Illuminate\Database\Eloquent\Model;
use Validator;

class PayroleGroupRule extends Model
{
    // use ValidatingTrait;

    protected $table = 'payrole_group_rule';

    public function group()
    {
        return $this->belongsTo('App\Models\Admin\PayroleGroup', 'payrole_group_id', 'id');
    }
    public function key()
    {
        return $this->belongsTo('App\Models\Admin\PayrollRuleForCsv', 'payroll_rule_for_csv_id', 'id');
    }
    public function calc_upon()
    {
        return $this->hasOne('App\Models\Admin\PayroleGroupRule', 'id', 'calculated_upon');
    }
    public function csvValue()
    {
        return $this->hasOne('App\Models\Admin\PayrollRuleForCsv', 'key', 'name');
    }

    // public $timestamps = true;

    private $rules = array(

    );

    public static function saveData($request, $id)
    {
        try {
            $response = [
                'status' => true,
                'message' => "success",
                'id' => null,
                'log' => [],
                'errors' => [],
            ];

            $payroleGroupObj = PayroleGroup::find($id);
            if (!$payroleGroupObj) {
                $response['status'] = false;
                $response['errors'][] = 'Group not found';
                return $response;
            }
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'type' => 'required|in:Credit,Debit',
                'calculation_type' => 'in:+,-,/,*,%',
                'calculation_value' => 'required|regex:/^\d*(\.\d{0,2})?$/',
            ]);

            if ($validator->fails()) {
                $response['status'] = false;
                $response['errors'] = $validator->errors();
                return $response;
            }
            $rule = PayrollRuleForCsv::where('key', $request->name)->count();
            if ($rule == 0) {
                $response['status'] = false;
                $response['errors'] = 'Invalid name, please select one from the suggestions';
                return $response;
            }
            $ruleObj = new PayroleGroupRule;
            $ruleObj->payrole_group_id = $payroleGroupObj->id;
            $ruleObj->name = $request->name;
            $ruleObj->type = !empty($request->type) ? $request->type : 'Debit';
            $ruleObj->calculation_type = !empty($request->calculation_type) ? $request->calculation_type : null;
            $ruleObj->calculation_value = !empty($request->calculation_value) ? $request->calculation_value : 0;
            $ruleObj->calculated_upon = !empty($request->calculated_upon) ? $request->calculated_upon : 0;
            if (!$ruleObj->save()) {
                $response['status'] = false;
                $response['errors'][] = $ruleObj->getErrors();
                return $response;
            } else {
                $response['message'] = "Rule saved successfully";
            }
            return $response;
        } catch (\Exception $e) {
            AccessLog::accessLog(null, 'App\Models', 'Product', 'saveData', 'catch-block', $e->getMessage());
        }
    }
    public static function updateData($request, $id)
    {
        try {
            $response = [
                'status' => true,
                'message' => "success",
                'id' => null,
                'log' => [],
                'errors' => [],
            ];

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'type' => 'required|in:Credit,Debit',
                'calculation_type' => 'in:+,-,/,*,%',
                'calculation_value' => 'required|regex:/^\d*(\.\d{0,2})?$/',
            ]);

            if ($validator->fails()) {
                $response['status'] = false;
                $response['errors'] = $validator->errors();
                return $response;
            }
            $ruleObj = PayroleGroupRule::find($id);
            if (!$ruleObj) {
                $response['status'] = false;
                $response['errors'][] = 'Rule not found';
                return $response;
            }
            $rule = PayrollRuleForCsv::where('key', $request->name)->count();
            if ($rule == 0) {
                $response['status'] = false;
                $response['errors'] = 'Invalid name, please select one from the suggestions';
                return $response;
            }

            $key = $request->name;
            $keyObj = PayrollRuleForCsv::where('key', $key)->first();
            if ($keyObj) {
                $ruleObj->payroll_rule_for_csv_id = $keyObj->id;
            }

            $ruleObj->name = $key;
            $ruleObj->type = !empty($request->type) ? $request->type : 'Debit';
            $ruleObj->calculation_type = !empty($request->calculation_type) ? $request->calculation_type : null;
            $ruleObj->calculation_value = !empty($request->calculation_value) ? $request->calculation_value : 0;
            $ruleObj->calculated_upon = !empty($request->calculated_upon) ? $request->calculated_upon : 0;
            if (!$ruleObj->save()) {
                $response['status'] = false;
                $response['errors'][] = $ruleObj->getErrors();
                return $response;
            } else {
                $response['message'] = "Rule updated successfully";
            }
            return $response;
        } catch (\Exception $e) {
            AccessLog::accessLog(null, 'App\Models', 'Product', 'saveData', 'catch-block', $e->getMessage());
        }
    }
    public static function getRules()
    {

        $rules = PayrollRuleForCsv::get();
        $result = [];
        foreach ($rules as $rule) {
            $obj = (object) array();
            $obj->key = $rule->key;
            $obj->value = $rule->value;
            $result[] = $obj;
        }
        return json_encode($result);
        // return(json_encode(['Basic','HRA','Conveyance Allowance','Car Allowance',
        //                     'Medical Allowance','Food Allowance','Special Allowance',
        //                     'P.F. @12% Employee','E.S.I. @1.75% Employee','VPF',
        //                     'Professional Tax','Food Deduction','Medical Insurance']));
    }

}
