<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

use App\Models\Admin\Project;
use App\Models\Admin\ProjectSlackChannel;
use App\Services\SlackService\SlackConstants;
use App\Services\SlackService\SlackApiServices;

class ProjectSlackChannel extends Model
{
    use ValidatingTrait;
	protected $table = 'project_slack_channels';
	public $timestamps = false;
	

	private $rules = array(
		    'project_id' => 'required'
	    );

	public function project() {
		return $this->belongsTo('App\Models\Admin\Project', 'project_id', 'id');
	}

	public static function connect($project_id, $channel_id)
	{
		$response = ['status' => false, 'message' => ""];
		$exists = ProjectSlackChannel::where('slack_id', $channel_id)->delete();
		$project = Project::find($project_id);
		if(!$project) {
			$response['message'] = SlackConstants::PROJECT_NOT_FOUND;
			return $response;
		}
		$result = SlackApiServices::isChannelPrivate($channel_id);
		if($result['status']) {
			$obj = new ProjectSlackChannel();
			$obj->project_id = $project_id;
			$obj->slack_id = $channel_id;
			$obj->type = $result['data'] ? "private" : "public";
			$obj->created_at = date("Y-m-d H:i:s");
			$obj->updated_at = date("Y-m-d H:i:s");
			$obj->save();
			$response['status'] = true;
			$response['message'] = SlackConstants::projectAdded($project->project_name);
			return $response;
		} else {
			$response['message'] = SlackConstants::CHANNEL_NOT_FOUND;
			return $response;
		}
	}
}
