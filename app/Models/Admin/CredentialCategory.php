<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class CredentialCategory extends Model
{

	use ValidatingTrait;
	protected $table = 'credential_categories';
	public $timestamps = false;

	private $rules = array(
			'name' => 'required'
		);
		
	public function credentials() {
        return $this->hasMany('App\Models\Admin\Credential', 'credential_category_id', 'id' );
    }
}
