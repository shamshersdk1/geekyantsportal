<?php namespace App\Models\Admin;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class CalendarYear extends BaseModel
{

    protected $table = 'calendar_years';
    public $timestamps = true;
    use SoftDeletes;

    public function scopeBuildQuery($query)
    {
        return $query;
    }

    public static function getCalenderYear($year)
    {
        $yearObj = CalendarYear::where('year', $year)->first();
        return $yearObj->id;
    }

    //returns first day of the year
    public function getFirstDay()
    {
        return date('Y-01-01', strtotime($this->year . '-01-01'));
    }

    //returns last day of the year
    public function getLastDay()
    {
        return date('Y-12-31', strtotime($this->year . '-12-31'));
    }
}
