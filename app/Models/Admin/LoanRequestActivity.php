<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class LoanRequestActivity extends Model
{
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public static function saveActivity($id, $status, $role, $user_id, $comment)
    {
        $activity = new LoanRequestActivity();
        $activity->loan_request_id = $id;
        $activity->status = $status;
        $activity->role = $role;
        $activity->user_id = $user_id;
        $activity->comment = $comment;
        if($activity->save()) {
            return ['status' => true, 'message' => ""];
        } else {
            return ['status' => false, 'message' => $activity->getErrors()];
        }
    }
}
