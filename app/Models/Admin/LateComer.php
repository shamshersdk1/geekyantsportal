<?php

namespace App\Models\Admin;

use App\Models\BaseModel;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Loan;
use App\Models\LoanTransaction;
use App\Services\FileService;
use App\Jobs\Loan\LoanRequestJob;
use App\Models\AccessLog;
use Auth;
use Exception;

class LateComer extends BaseModel
{  
    
	protected $table = 'late_comers';
    public $timestamps = true;
    
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function reportedBy()
    {
        return $this->belongsTo('App\Models\User', 'reported_by_id', 'id');
    }

    public static function todayLateComers()
    {
        $date = date('Y-m-d');
        $lateComerList = LateComer::where('date', $date)->get();
        return $lateComerList;
    }
    
}
