<?php

namespace App\Models\Admin;

use App\Models\BaseModel;

class Cron extends BaseModel
{

    public function scopeBuildQuery($query)
    {
        return $query;
    }
    public function reference()
    {
        return $this->morphTo('reference');
    }
    public function reminders()
    {
        return $this->hasMany('App\Models\Admin\BillingScheduleReminder', 'cron_id', 'id');
    }
}
