<?php

namespace App\Models\Admin;;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PayslipCsvMonth extends Model
{
    
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'payslip_csv_month';

    public function creator()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }
    public function approver()
    {
        return $this->hasOne('App\Models\User', 'id', 'approved_by');
    }
}
