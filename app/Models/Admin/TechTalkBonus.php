<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TechTalkBonus extends Model
{
 protected $dates = ['deleted_at'];
 use SoftDeletes;

 public function user()
 {
  return $this->belongsTo('App\Models\User', 'created_by', 'id');
 }
 public function techtalkuser()
 {
  return $this->hasMany('App\Models\Admin\TechTalkBonusesUser', 'tech_talk_id', 'id');
 }
 public function reviewer()
 {
  return $this->belongsTo('App\Models\User', 'created_by', 'id');
 }
 public static function savaData($data)
 {
  $obj              = new self;
  $obj->employee_id = !empty($data['employee_id']) ? $data['employee_id'] : null;
  $obj->topic       = !empty($data['topic']) ? $data['topic'] : null;
  $obj->date        = !empty($data['date']) ? $data['date'] : null;
  $obj->amount      = !empty($data['amount']) ? $data['amount'] : null;
  $obj->status      = !empty($data['status']) ? $data['status'] : 'pending';

  if (!$obj->save()) {
   return false;
  }

  return $obj;

 }
}
