<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssetCategory extends Model
{

	use ValidatingTrait;
	protected $table = 'asset_categories';
	public $timestamps = true;
	use SoftDeletes;
	

	private $rules = array(
			'name' => 'required'
		);
		
	public function assets() {
        return $this->hasMany('App\Models\Admin\Asset', 'asset_category_id', 'id' );
    }
}
