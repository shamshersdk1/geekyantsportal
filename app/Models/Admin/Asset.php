<?php

namespace App\Models\Admin;

use App\Models\Admin\AssetAssignmentDetail;
use App\Models\BaseModel;

use Illuminate\Database\Eloquent\SoftDeletes;
use Input;
use Watson\Validating\ValidatingTrait;
use App\Models\Admin\AssetMeta;

class Asset extends BaseModel
{

    use ValidatingTrait;
    protected $table = 'assets';
    public $timestamps = true;
    use SoftDeletes;

    private $rules = array(
        'name' => 'required',
        'serial_number' => 'required',
        'is_working' => 'required',
        'invoice_number' => 'required',
        'asset_category_id' => 'required',
    );

    public function getURI(){
        return '/admin/asset-management/asset/'.$this->id."?";
    }
    
    public function assetCategory()
    {
        return $this->belongsTo('App\Models\Admin\AssetCategory', 'asset_category_id', 'id');
    }
    public function assignmentDetail()
    {
        return $this->belongsTo('App\Models\Admin\AssetAssignmentDetail', 'id', 'asset_id')->where('to_date', null);
    }
    public function metas()
    {
        return $this->hasMany('App\Models\Admin\AssetMeta', 'asset_id');
    }
    public function ipMapper()
    {
        return $this->morphOne('App\Models\Admin\IpAddressMapper', 'reference');
    }
    public function insurances()
    {
        return $this->morphMany('App\Models\Admin\Insurance\Insurance', 'insurable');
    }

    public static function findByHwAddress($hw_address){
        $assetMeta = AssetMeta::where('value',$hw_address)->first();
        if($assetMeta)
            return Asset::find($assetMeta->asset_id);
    }

    public static function filter()
    {
        $search = "";
        $arrayId = [];
        $result = array('status' => false, 'errors' => "", 'message' => "");
        $assets = Asset::orderBy('purchase_date', 'DESC');
        $return_object = [];
        $return_asset_object = [];
        if (Input::get('filter') == '') {
            $assetst = $assets->get();
            foreach ($assetst as $asset) {
                if ($asset->is_working == 1) {
                    array_push($arrayId, $asset->id);
                }
            }
            $assets = $assets->whereIn('id', $arrayId);
        }
        if (Input::get('filter') != '') {
            if (preg_replace('/[\s]+.*/', '', Input::get('filter')) == "damaged") {
                $assetst = $assets->get();
                foreach ($assetst as $asset) {
                    if ($asset->is_working == 0) {
                        array_push($arrayId, $asset->id);
                    }
                }
                $assets = $assets->whereIn('id', $arrayId);
            }
        }
        if (Input::get('filter') != '') {
            if (preg_replace('/[\s]+.*/', '', Input::get('filter')) == "all") {
                $assetst = $assets->get();
                foreach ($assetst as $asset) {
                    array_push($arrayId, $asset->id);
                }
                $assets = $assets->whereIn('id', $arrayId);
            }
        }
        if (Input::get('q') != '') {
            $search = str_replace('+', ' ', Input::get('q'));
            $assetst = $assets->get();
            $arrayId = [];
            foreach ($assetst as $asset) {
                $assigned_to = AssetAssignmentDetail::assignedUser($asset->id)->first();
                $assigned_to = isset($assigned_to->user->name) ? $assigned_to->user->name : '';
                if ((stripos($asset->name, $search) !== false) || (stripos($asset->serial_number, $search) !== false) || (stripos($asset->invoice_number, $search) !== false) || (stripos($assigned_to, $search) !== false)) {
                    array_push($arrayId, $asset->id);
                }
            }

            $assets = $assets->whereIn('id', $arrayId)->get();
            foreach ($assets as $asset) {
                $assigned_to = AssetAssignmentDetail::assignedUser($asset->id)->first();
                $return_object['id'] = $asset->id;
                $return_object['name'] = $asset->name;
                $return_object['serial_number'] = $asset->serial_number;
                $return_object['is_working'] = $asset->is_working;
                $return_object['invoice_number'] = $asset->invoice_number;
                $return_object['purchase_date'] = $asset->purchase_date;
                $return_object['assigned_to'] = (isset($assigned_to->user->name) ? $assigned_to->user->name : '');

                $return_asset_object[] = $return_object;
            }

            $result = ['status' => true, 'assets' => $return_asset_object, 'search' => $search];
        } else {
            $assets = $assets->get();
            foreach ($assets as $asset) {
                $assigned_to = AssetAssignmentDetail::assignedUser($asset->id)->first();
                $return_object['id'] = $asset->id;
                $return_object['name'] = $asset->name;
                $return_object['serial_number'] = $asset->serial_number;
                $return_object['is_working'] = $asset->is_working;
                $return_object['invoice_number'] = $asset->invoice_number;
                $return_object['purchase_date'] = $asset->purchase_date;
                $return_object['assigned_to'] = (isset($assigned_to->user->name) ? $assigned_to->user->name : '');

                $return_asset_object[] = $return_object;
            }
            $result = ['status' => true, 'assets' => $return_asset_object, 'search' => $search];
        }
        return $result;
    }

    public function files()
    {
        return $this->morphMany(File::class, 'reference');
    }
    
}
