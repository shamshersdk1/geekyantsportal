<?php

namespace App\Models\Admin;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Validator;

class Appraisal extends Model
{
    protected $appends = ['monthly_ctc', 'annual_ctc'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function creator()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }
    public function bonuses()
    {
        return $this->hasMany('App\Models\Admin\AppraisalBonus', 'appraisal_id', 'id');
    }
    public function userAnnualBonus()
    {
        return $this->hasOne('App\Models\Admin\AppraisalBonus', 'appraisal_id', 'id')->where('type', '=', 'annual');
    }
    public function confirmationBonus()
    {
        return $this->hasOne('App\Models\Admin\AppraisalBonus', 'id', 'created_by')->where('type', '=', 'confirmation')->first();
    }
    public function monthlyVariableBonus()
    {
        return $this->hasMany('App\Models\Admin\AppraisalBonus', 'id', 'created_by')->where('type', '=', 'monthly_variable')->get();
    }
    public function group()
    {
        return $this->hasOne('App\Models\Admin\PayroleGroup', 'id', 'payrole_group_id');
    }
    public function getMonthlyCtcAttribute()
    {
        return round($this->annual_gross_salary / 12, 2) + round($this->monthly_var_bonus / 12, 2) + round($this->annual_bonus / 12, 2);
    }
    public function getAnnualCtcAttribute()
    {
        return $this->annual_gross_salary + $this->monthly_var_bonus + $this->annual_bonus;
    }
    public static function getOneDaySalary($userId, $date)
    {
        return 0;
        //remove above line
        $amount = 2000;
        $userObj = User::find($userId);
        $month = date('m', strtotime($date));

        $year = date('Y', strtotime($date));
        if (!$userObj) {
            return 0;
        }

        $appraisal = Appraisal::getAppraisalByDate($userId, $date);
        $noOfWorkingDay = CalendarService::getWorkingDays($month, $year);

        if ($appraisal) {
            $noOfWorkingDay = CalendarService::getWorkingDays($month, $year);
            $annualGrossSalary = $appraisal->annual_gross_salary;
            $oneDaySalary = round(($annualGrossSalary / 12) / $noOfWorkingDay, 2);
            if ($oneDaySalary > $amount) {
                $amount = $oneDaySalary;
            }

        }

        return $amount;
    }
    public static function getAnnualGrossSalaryByDate($userId, $date)
    {
        $grossSalary = 0;
        $date = date('Y-m-d', strtotime($date));
        $appraisal = self::where('user_id', $userId)->where('effective_date', '>=', $date)
            ->where(function ($query) use ($date) {
                $query->where('end_date', null)
                    ->orWhere('end_date', '<=', $date);
            })->first();

        if ($appraisal) {
            $grossSalary = $appraisal->annual_gross_salary ?: 0;
        }
        return round($grossSalary);
    }
    public static function getAppraisalByDate($userId, $date)
    {
        return 0;
        //remove above line
        $appraisal = self::where('user_id', $userId)->where('effective_date', '<=', $date)
            ->where(function ($query) use ($date) {
                $query->where('end_date', null)
                    ->orWhere('end_date', '>=', $date);
            })->first();
        $appraisal = self::where('user_id', $userId)->where('effective_date', '<=', $date)->orderBy('effective_date','DESC')->first();

        if (!$appraisal) {
            return false;
        }
        return $appraisal;
    }
    public static function validateAppraisal(Request $request, $user, $skip, $limit)
    {
        $response = array('status' => false, 'errors' => "", 'message' => "");
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'date',
            'annual_gross_salary' => 'required|regex:/^\d+(\.\d{0,2})?$/',
            'monthly_var_bonus' => 'regex:/^\d+(\.\d{0,2})?$/',
            'annual_bonus' => 'regex:/^\d+(\.\d{0,2})?$/',
            'group' => 'required',
        ]);
        if ($validator->fails()) {
            $response['errors'] = $validator->errors();
            return $response;
        }
        if (!empty($request->end_date) && $request->start_date > $request->end_date) {
            $response['errors'] = "Effective date should be less than effective till date";
            return $response;
        }
        if ($skip == 0) {
            if (count(Appraisal::where('user_id', $user->id)->where('name', 'Joining')->get()) > 0) {
                if (count(Appraisal::where('user_id', $user->id)->where('name', 'Confirmation')->get()) == 0) {
                    if ($request->name != "Confirmation") {
                        $response['errors'] = "Add confirmation appraisal first";
                        return $response;
                    }
                }
            } else {
                if ($request->name != "Joining") {
                    $response['errors'] = "Add joining appraisal first";
                    return $response;
                }
            }
            if ($request->name == "Joining" && $request->start_date != $user->joining_date) {
                $response['errors'] = "Joining date doesn't match";
                return $response;
            }

            if ($request->name == "Confirmation" && $request->start_date != $user->confirmation_date) {
                $response['errors'] = "Confirmation date doesn't match";
                return $response;
            }
        }
        if ($request->is_active == 1 && Appraisal::where('user_id', $user->id)->sum('is_active') > $limit) {
            $response['errors'] = "User already has an active appraisal";
            return $response;
        }
        $response['status'] = true;
        return $response;
    }
    public static function saveAppraisal(Request $request, $user)
    {

        $response = Appraisal::validateAppraisal($request, $user, 0, 0);
        if ($response['status']) {
            $appraisal = new Appraisal;
            $appraisal->name = $request->name;
            $appraisal->user_id = $user->id;
            $appraisal->payrole_group_id = $request->group;
            $appraisal->annual_gross_salary = $request->annual_gross_salary;
            $appraisal->annual_bonus = $request->annual_bonus;
            $appraisal->monthly_var_bonus = $request->monthly_var_bonus;
            $appraisal->effective_date = $request->start_date;
            $appraisal->created_by = \Auth::id();
            //$appraisal->in_hand = 0;
            //$request->in_hand;
            //$appraisal->qtr_bonus = empty($request->qtr_bonus) ? 0.00 : $request->qtr_bonus;
            //$appraisal->year_end = empty($request->year_end) ? 0.00 : $request->year_end;
            $appraisal->is_active = false;
            $appraisal->end_date = empty($request->end_date) ? null : $request->end_date;
            if ($appraisal->save()) {
                $response['status'] = 'true';
                return $response;
            } else {
                $response['errors'] = "Unable to save";
                return $response;
            }
        } else {
            return $response;
        }
    }
    public static function updateAppraisal(Request $request, $user, $id)
    {
        $response = array('status' => false, 'errors' => "", 'message' => "");
        $appraisal = Appraisal::find($id);
        if (empty($appraisal)) {
            $response['errors'] = 'invalid link followed';
            return $response;
        }
        $limit = $appraisal->is_active;
        $response = Appraisal::validateAppraisal($request, $user, 1, $limit);
        if ($response['status']) {
            $appraisal->name = $request->name;
            $appraisal->user_id = $user->id;
            $appraisal->payrole_group_id = $request->group;
            $appraisal->effective_date = $request->start_date;
            $appraisal->annual_gross_salary = $request->annual_gross_salary;
            $appraisal->annual_bonus = $request->annual_bonus;
            $appraisal->monthly_var_bonus = $request->monthly_var_bonus;
            // $appraisal->in_hand = 0;
            // $appraisal->qtr_bonus = 0;
            // $appraisal->year_end = 0;
            $appraisal->end_date = empty($request->end_date) ? null : $request->end_date;
            if ($appraisal->save()) {
                $response['status'] = true;
                return $response;
            } else {
                $response['errors'] = "Unable to save";
                return $response;
            }
        } else {
            return $response;
        }
    }
    public static function getBonus($id, $qtrs)
    {
        $appraisal = Appraisal::find($id);
        $arr = [];
        $currQtr = (3 * ceil((date('n') - 1) / 3) + 1) % 12;
        for ($i = 0; $i < $qtrs; $i++) {
            if ($currQtr < 10) {
                $currQtr = '0' . $currQtr;
            }
            $cutoff = ($currQtr + 10) % 12;
            $currQtrDate = (date('Y-' . $currQtr . '-01'));
            if ($cutoff < 10) {
                $cutoff = '0' . $cutoff;
            }
            $cutoffDate = date('Y-m-d', strtotime('-2 months', strtotime($currQtrDate)));
            $bonus = $appraisal->qtr_bonus / 4;
            if ($appraisal->effective_date < $cutoffDate) {
                $val = $bonus;
            } else {
                $val = $bonus - $bonus / 3 * ((date('n', strtotime($appraisal->effective_date)) - 1) % 3);
                if ($appraisal->effective_date >= $currQtrDate) {
                    $val = 0;
                }
            }
            $arr[$currQtrDate] = $val;
            $currQtr = ($currQtr + 3) % 12;
        }
        if (!empty($appraisal->user->confirmation_date)) {
            $month = date('m', strtotime($appraisal->user->confirmation_date));
            $annual = date('Y-' . $month . '-25');
            if ($annual < date("Y-m-d")) {
                $annual = date('Y-m-d', strtotime('+1 year', strtotime($annual)));
            }
            while (date('Y', strtotime($annual)) <= date('Y', strtotime($appraisal->user->confirmation_date))) {
                $annual = date('Y-m-d', strtotime('+1 year', strtotime($annual)));
            }
        } else {
            $annual = 'Not confirmed yet';
        }
        return ['Qtr' => $arr, 'annual' => $annual];
    }

    public static function getCurrentSalary($user_id)
    {
        return Appraisal::where('is_active', 1)->where('user_id', $user_id)->first();
    }
    public function grossEarningForMonth($month)
    {
        $monthObj = Month::find($month);
        if (!$month) {
            return 0;
        }

        $monthId = $monthObj->id;
        // Gross Earning for the Year Projected (GEY)
        // = (Monthly Gross *12 + Annual Bonus: Chargable for current F.Y. + Monthly Var Bonus + other Income (Bonuses) )
        // $grossEarning = 0;
        // $appraisal = self::getAppraisalByDate($userId, $date);
        //if ($appraisal) {
        $grossEarning = $this->annual_gross_salary + $this->annual_bonus + $this->monthly_var_bonus;
        $amount = Bonus::where('user_id', $this->user_id)->where('month_id', $monthId)->sum('amount');
        //}

        return $grossEarning;
    }
    public function getAnnualHRA($month)
    {
        // Actual HRA received paid annually

    }
    public function getRentPaid($month)
    {
        // Rent paid = 10% of Basic + DA
        $monthObj = Month::where('month', $month)->first();
        if (!$monthObj) {
            return 0;
        }

    }
    public function getOtherIncome($month)
    {
        // getOtherIncome
    }
    public function getHRAExemption($month)
    {

        $monthObj = Month::find($month);
        if (!$month) {
            return 0;
        }
        $userSalary = SalaryUserData::where('user_id', $this->user_id)->where('month_id', $monthId)->first();

        if (!$userSalary) {
            return 0;
        }
        $basicObj = PayrollRuleForCsv::where('key', 'basic')->first();
        $hraObj = PayrollRuleForCsv::where('key', 'hra')->first();
        $basicObj = PayrollRuleForCsv::where('key', 'basic')->first();

        $basic = SalaryUserData::where('salary_user_data_id', $userSalary->id)->where('key', 'basic')->first();
        $basic = SalaryUserData::where('salary_user_data_id', $userSalary->id)->where('key', 'basic')->first();
        $basic = SalaryUserData::where('salary_user_data_id', $userSalary->id)->where('key', 'basic')->first();
        $monthId = $monthObj->id;
        //     Gross Earning for the Year Projected (GEY)
        // = (Monthly Gross *12 + Annual Bonus: Chargable for current F.Y. + Monthly Var Bonus + other Income (Bonuses) )
        // $grossEarning = 0;
        // $appraisal = self::getAppraisalByDate($userId, $date);
        //if ($appraisal) {
        $grossEarning = $this->annual_gross_salary + $this->annual_bonus + $this->monthly_var_bonus;
        $amount = Bonus::where('user_id', $this->user_id)->where('month_id', $monthId)->sum('amount');
        //}

        return $grossEarning;
    }
}
