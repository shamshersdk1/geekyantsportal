<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\Admin\Tag;
use App\Models\Admin\UserGoal;
use App\Models\BaseModel;

use Auth;
use DB;

class Goal extends BaseModel
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function tags()
    {
        return $this->belongsToMany('App\Models\Admin\Tag', 'goal_tag', 'goal_id', 'tag_id');
    }

    public static function saveGoal($request)
    {
        $response = ['status' => true, 'message' => "", "id" => null];
        $goal = new Goal();
        $goal->title = $request['title'];
        $goal->success_metric = $request['success_metric'];
        $goal->process = $request['process'];
        $goal->is_active = true;
        $tags = is_array($request['tags']) ? $request['tags'] : explode(",", $request['tags']);
        if(!$goal->save()) {
            $response['status'] = false;
            $response['message'] = $goal->getErrors();
        } else {
            $response['id'] = $goal->id;
        } 
        if($tags && count($tags) > 0) {
            Goal::attachTags($tags, $goal->id);
        }
        return $response;
    }

    public static function updateGoal($request, $id)
    {
        $response = ['status' => true, 'message' => ""];
        $goal = Goal::find($id);
        if($goal) {
            $goal->title = $request['title'];
            $goal->success_metric = $request['success_metric'];
            $goal->process = $request['process'];
            $tags = is_array($request['tags']) ? $request['tags'] : explode(",", $request['tags']);
            if(!$goal->save()) {
                $response = ['status' => false, 'message' => $goal->getErrors()];
            }
            $existing = $goal->tags()->pluck('name')->toArray();
            $add = array_diff($tags, $existing);
            $remove = array_diff($existing, $tags);
            Goal::attachTags($add, $goal->id);
            Goal::detachTags($remove, $goal->id);
            return $response;
        } else {
            $response = ['status' => false, 'message' => "Invalid link followed"];
        }
    }

    public static function updateIdp($request, $id)
    {
        $response = ['status' => true, 'message' => ""];
        $goal = Goal::find($id);
        if($goal) {
            $goal->title = $request['title'];
            $goal->process = $request['process'];
            if(!$goal->save()) {
                $response = ['status' => false, 'message' => $goal->getErrors()];
            }
            return $response;
        } else {
            $response = ['status' => false, 'message' => "Invalid link followed"];
        }
    }

    public static function getPublicGoals($name = "", $tags = null, $page=1, $per_page=10)
    {
        if(!is_null($tags)) {
            $tags = Tag::whereIn('name', $tags)->get();
        }
        $goals = Goal::with('tags')->where('title', 'like', "%".$name."%")->get();
        foreach($goals as $key => $goal)
        {
            if($goal->is_private) {
                $goals->forget($key);
                continue;
            }
            if(!is_null($tags)) {
                foreach($tags as $tag) {
                    if(!($tag->goals()->where('goals.id', $goal->id)->exists())) {
                        $goals->forget($key);
                        break;
                    }
                }
            }
        }
        $goals = $goals->values();
        $goals = $goals->slice(($page-1)*$per_page)->take($per_page);
        return $goals;
    }

    public static function attachTags($tags, $goal_id)
    {
        foreach($tags as $tag)
        {
            $tag = trim($tag);
            $db_tag = Tag::where('name', $tag)->first();
            if($db_tag) {
                $db_tag->goals()->attach($goal_id);
            } else {
                $tag_id = Tag::saveTag($tag);
                if(is_null($tag_id)) {
                    continue;
                } else {
                    DB::table('goal_tag')->insert(['goal_id' => $goal_id, 'tag_id' => $tag_id]);
                }
            }
        }
    }
    public static function detachTags($tags, $goal_id)
    {
        foreach($tags as $tag)
        {
            $tag = trim($tag);
            $db_tag = Tag::where('name', $tag)->first();
            if($db_tag) {
                $db_tag->goals()->detach($goal_id);
            }
        }
    }
}
