<?php namespace App\Models\Admin;

use App\Models\BaseModel;
use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Designation extends BaseModel
{

	use ValidatingTrait;

	protected $table = 'designations';
	public $timestamps = true;
	use SoftDeletes;

	private $rules = array(
			'designation'  => 'required'
	    );

	public function feedbackDesignationTemplate()
   	{
       return $this->hasMany('App\Models\Admin\FeedbackQuestionDesignationTemplate','id','designation_id');
   	}
   	public function questions()
    {
        return $this->morphMany('App\Models\Admin\QuestionMapping', 'reference');
    }

	public static function updateStatus($id)
	{
		$status = true;
		$message = '';
		$designation = Designation::find($id);
		if ( empty($designation) )
		{
			$status = false;
			$message = 'No record found';
		}
		$toggle = Designation::where('id', $id)->update(['status' => !$designation->status]);

		$response['status'] = $status;
		$response['message'] = $message;
		return $response;

	}
}
