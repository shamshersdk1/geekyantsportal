<?php namespace App\Models\Admin;

use App\Models\AccessLog;
use App\Models\Admin\PurchaseOrderItem;
use App\Models\BaseModel;
use DB;
use OwenIt\Auditing\Contracts\Auditable;

class PurchaseOrder extends BaseModel implements Auditable
{
    use \OwenIt\Auditing\Auditable;    
    protected $table = 'purchase_orders';
    public $timestamps = true;

    public function vendor()
    {
        return $this->hasOne('App\Models\Admin\Vendor', 'id', 'vendor_id');
    }
    public function company()
    {
        return $this->hasOne('App\Models\Company', 'id', 'company_id');
    }
    public function scopeBuildQuery($query)
    {
        return $query;
    }
    public function createdBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }
    public function approvedBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'approved_by');
    }
    public function purchaseItems()
    {
        return $this->hasMany('App\Models\Admin\PurchaseOrderItem', 'purchase_order_id', 'id');
    }
    public function scopeGenericSearch($query, $value)
    {
        $query = $query->where('reference_number', 'LIKE', '%' . $value . '%')
            ->orWhere('payment_terms', 'LIKE', '%' . $value . '%')
            ->orWhere('status', 'LIKE', '%' . $value . '%')
            ->orWhereHas('vendor', function ($q) use ($value) {
                if ($value) {
                    $q->where('name', 'like', '%' . $value . '%');
                }
            });
        return $query;
    }
    public static function saveData($data, $oderObj)
    {
        $userObj = \Auth::User();
        DB::beginTransaction();
        try {
            $response = [
                'result' => "",
                'status' => true,
                'message' => "success",
                'errors' => [],
            ];
            $userObj = \Auth::User();

            if (!$userObj) {
                $response['status'] = false;
                $response['message'] = "Invalid Access";
                return $response;
            }
            if (!$userObj->hasRole('admin') && !$userObj->hasRole('office-admin')) {
                $response['status'] = false;
                $response['message'] = 'Unauthorized Access';
                return $response;
            }
            if (empty($data)) {
                $response['status'] = false;
                $response['message'] = 'Empty data sent';
                return $response;
            }
            $oderObj->vendor_id = !empty($data['vendor_id']) ? $data['vendor_id'] : null;
            $oderObj->company_id = !empty($data['company_id']) ? $data['company_id'] : null;
            $oderObj->reference_number = !empty($data['reference_number']) ? $data['reference_number'] : null;
            $oderObj->payment_terms = !empty($data['payment_terms']) ? $data['payment_terms'] : null;
            $oderObj->delivery_date = !empty($data['delivery_date']) ? date_to_yyyymmdd($data['delivery_date']) : null;
            $oderObj->created_by = $userObj->id;
            $oderObj->status = "raised";
            $oderObj->total = !empty($data['total']) ? $data['total'] : null;
            if ($data['gst_included'] == "yes") {
                $oderObj->gst_included = 1;
            } else {
                $oderObj->gst_included = 0;
            }
            if (!$oderObj->save()) {
                $response['status'] = false;
                $response['message'] = "Unable to save Purchase Order";
                $response['errors'] = $oderObj->getErrors();
                DB::rollback();
                return $response;
            } else {
                if (!empty($data['purchase_items'])) {
                    foreach ($data['purchase_items'] as $item) {
                        $purchaseItem = new PurchaseOrderItem();
                        $purchaseItem->purchase_order_id = $oderObj->id;
                        $purchaseItem->name = !empty($item['name']) ? $item['name'] : null;
                        $purchaseItem->unit_price = !empty($item['unit_price']) ? $item['unit_price'] : null;
                        $purchaseItem->quantity = !empty($item['quantity']) ? $item['quantity'] : null;
                        $purchaseItem->total = $purchaseItem->quantity * $purchaseItem->unit_price;
                        if (!$purchaseItem->save()) {
                            $response['status'] = false;
                            $response['message'] = "Unable to save Purchase Item";
                            $response['errors'] = $purchaseItem->getErrors();
                            DB::rollback();
                            return $response;
                        }
                    }
                }
            }
            DB::commit();
            $response['status'] = true;
            $response['message'] = "Saved!";
            $response['result'] = PurchaseOrder::where('id', $oderObj->id)->with('vendor', 'createdBy', 'approvedBy')->first();
            return $response;
        } catch (\Exception $e) {
            $response['status'] = false;
            $response['message'] = $e->getMessage();
            AccessLog::accessLog(null, ' App\Models\Admin\PurchaseOrder', 'Purchase Order', 'saveData', 'catch-block', $e->getMessage());
            DB::rollback();
        }
        return $response;
    }
}
