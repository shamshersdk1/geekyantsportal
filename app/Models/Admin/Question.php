<?php namespace App\Models\Admin;

use App\Models\BaseModel;
use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\Admin\File;
use App\Services\FileService;

class Question extends BaseModel
{

	use ValidatingTrait;

	protected $table = 'questions';
	public $timestamps = true;
	use SoftDeletes;

	private $rules = array(
			'question'  => 'required',
	    );

	public function scopeBuildQuery($query)
	{
		return $query;
	}
	public function mappedQuestion()
	{
			return $this->hasMany('App\Models\Admin\QuestionMapping','question_id','id');
	}
	public function role()
	{
			return $this->hasOne('App\Models\Admin\Role','id','reviewer_type');
	}
	public static function updateStatus($id)
	{
		$status = true;
		$message = '';
		$question = Question::find($id);
		if ( empty($question) )
		{
			$status = false;
			$message = 'No record found';
		}
		$toggle = Question::where('id', $id)->update(['status' => !$question->status]);

		$response['status'] = $status;
		$response['message'] = $message;
		return $response;
	}

}
