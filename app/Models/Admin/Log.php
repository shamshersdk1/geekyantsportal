<?php

namespace App\Models\Admin;

use Exception;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;
use Input;
use App\Models\User;

class Log extends Model
{
    use ValidatingTrait;

    protected $fillable = [];

    protected $table = 'logs'; 

    public $timestamps = true;

    protected $rules = [
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

   public static function saveData($userId, $userRole, $requestBody, $ipDetails, $routePath)
   {

        $logData = new Log();
        $logData->user_id = $userId;
        $logData->user_role = $userRole;
        $logData->raw_post = json_encode($requestBody);
        $logData->raw_get = null;
        $logData->ip = $ipDetails;
        $logData->route_path = $routePath;
        $logData->save();
    }



    public static function filter()
    {
        $result=array('status' => false,'errors'=>"",'message' => "");
        $logList = Log::with('user')->orderBy('id', 'DESC');
        if (Input::get('searchuser')!='' && (Input::get('created_at') == '')) {
            $key=str_replace('+', ' ', Input::get('searchuser'));
            $searchUserId = User::where('name', $key)->first();
            if (empty($searchUserId)) {
                $result['errors']="Select a valid User";
                return $result;
            }
            $name=$searchUserId->name;
            $searchUserId=$searchUserId->id;
            $logList=$logList->where('user_id', $searchUserId);
        }
        if (Input::get('searchuser')!='' && (Input::get('created_at')!='' )) {
            $startDate = Input::get('created_at');
            $key=str_replace('+', ' ', Input::get('searchuser'));
            $searchUserId = User::where('name', $key)->first();
            if (empty($searchUserId)) {
                $result['errors']="Select a valid User";
                return $result;
            }
            $name=$searchUserId->name;
            $searchUserId=$searchUserId->id;
            $logList=$logList->where('user_id', $searchUserId)->whereDate('created_at', '=', $startDate);
        }
        if (Input::get('searchuser') =='' && (Input::get('created_at')!='' )) {
            $startDate = Input::get('created_at');
            $logList=$logList->whereDate('created_at', '=', $startDate);
        }         
            $logList = $logList->paginate(20, ['*'], 'logList');
            $result=['status'=>true, 'logList' => $logList];
            return $result;

    }

}

