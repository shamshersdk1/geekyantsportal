<?php namespace App\Models\Admin;

use App\Models\BaseModel;
use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\Admin\File;
use App\Services\FileService;

class QuestionMapping extends BaseModel
{

	use ValidatingTrait;

	protected $table = 'questions_mapping';
	public $timestamps = true;
	use SoftDeletes;

	private $rules = array(
			'question_id'  => 'required',
	    );
	public function reference()
	{
			$this->morphTo();
	}
	public function question()
	{
			return $this->belongsTo('App\Models\Admin\Question','question_id','id');
	}
	public function designation()
	{
			return $this->hasOne('App\Models\Admin\Designation','id','reference_id');
	}
	public function role()
	{
			return $this->hasOne('App\Models\Admin\Role','id','reference_id');
	}
	
}
