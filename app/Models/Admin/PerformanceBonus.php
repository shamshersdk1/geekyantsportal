<?php

namespace App\Models\Admin;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class PerformanceBonus extends BaseModel
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    public $timestamps = false;

    public function month()
    {
        return $this->belongsTo('App\Models\Month', 'month_id', 'id');
    }
    public function scopeBuildQuery($query)
    {
        return $query;
    }
    public function reference()
    {
        return $this->morphTo('reference');
    }
    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
    public function createdBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }
    public function reviewedBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'reviewed_by');
    }
    public function month_reference()
    {
        return $this->hasOne('App\Models\Month', 'id', 'month_id');
    }
    public function reviewer()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
    }

    public static function getByMonthRangeAndUser($user_id, $start_date, $end_date)
    {

    }
    public static function saveData($data)
    {
        $performanceObj = new performanceBonus();

        $performanceObj->user_id = $data['user_id'];
        $performanceObj->month_id = $data['month_id'];
        $performanceObj->status = $data['status'];
        $performanceObj->amount = $data['amount'];
        $performanceObj->comment = !empty($data['comment']) ? $data['comment'] : null;
        $performanceObj->created_by = $data['user_id'];
        $performanceObj->created_at = date('Y-m-d');

        if ($performanceObj->save()) {
            return $performanceObj;
        }
        return false;
    }
}
