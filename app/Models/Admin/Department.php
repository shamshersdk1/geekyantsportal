<?php namespace App\Models\Admin;

use App\Models\BaseModel;
use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Department extends BaseModel
{

	use ValidatingTrait;

	protected $table = 'departments';
	public $timestamps = true;
	use SoftDeletes;

	private $rules = array(
			'head'  => 'required',
			'first_contact'  => 'required',
			'type'  => 'required'
	    );

	public function departmentHead()
   	{
       return $this->hasOne('App\Models\User','id','head');
   	}
   	public function departmentFirstContact()
   	{
       return $this->hasOne('App\Models\User','id','first_contact');
   	}
}
