<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Admin\RequestApproval;

class RequestApproveUser extends Model
{

	protected $table = 'request_approve_users';
	public $timestamps = true;
	use SoftDeletes;
	
	public function request() {
        return $this->hasOne('App\Models\Admin\RequestApproval', 'request_id', 'id' );
	}
	public function user()
	{
		return $this->belongsTo('App\Models\User', 'user_id', 'id');
	}
	public static function saveApprovers($id, $users)
	{
		$status = true;
		$message = '';
		
		foreach($users as $user)
		{
			$exist = RequestApproveUser::where('request_id',$id)->where('user_id',$user)->first();
			if ( empty($exist) )
			{
				$requestApproveUserObj = new RequestApproveUser();
				$requestApproveUserObj->request_id = $id;
				$requestApproveUserObj->user_id = $user;
				$requestApproveUserObj->status = 'pending';
				if ( !$requestApproveUserObj->save() )
				{
					$status = false;
					$message = $requestApproveUserObj->getErrors();
				}
			}
		}
		
		$response['status'] = $status;
		$response['message'] = $message;
		return $response;
	}

	public static function requestAction( $requestId, $userId, $action )
	{
		$status = true;
		$message = 'Updated Successfully';

		$requestApproveObj = RequestApproveUser::where('user_id',$userId)->where('request_id',$requestId)->first();

		if ( empty($requestApproveObj) )
		{
			$status = false;
			$message = 'No record found';
		}
		else
		{
			if ( $action == 'approved' ) 
			{
				$requestApproveObj->status = 'approved';
				if ( $requestApproveObj->save() )
				{
					$res = RequestApproval::checkOverallApproveStatus($requestId);
				}
				else
				{
					$status = false;
					$message = $requestApproveObj->getErrors();
				}
			} 
			elseif ( $action == 'rejected' ) 
			{
				$requestApproveObj->status = 'rejected';
				if ( $requestApproveObj->save() )
				{
					$requestObj = RequestApproval::find($requestId);
					$requestObj->status = 'rejected';
					$requestObj->save();
				}
				else
				{
					$status = false;
					$message = $requestApproveObj->getErrors();
				}
			} 
			elseif ( $action == 'one_day' )
			{
				$requestObj = RequestApproval::find($requestId);
				$reminder_date = strtotime($requestObj->reminder_date);
				$reminder_date = date("Y-m-d", strtotime("+1 day", $reminder_date));
				$requestObj->reminder_date = $reminder_date;
				$requestObj->save();
			}
			else
			{
				$requestObj = RequestApproval::find($requestId);
				$reminder_date = strtotime($requestObj->reminder_date);
				$reminder_date = date("Y-m-d", strtotime("next monday", $reminder_date));
				$requestObj->reminder_date = $reminder_date;
				$requestObj->save();
			}

		}
		$response['status'] = $status;
		$response['message'] = $message;
		return $response;
	}
}
