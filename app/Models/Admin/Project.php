<?php

namespace App\Models\Admin;

use App\Models\BaseModel;
use Auth;
use Illuminate\Database\Eloquent\SoftDeletes;
use Input;
use Watson\Validating\ValidatingTrait;

class Project extends BaseModel
{
    use ValidatingTrait;

    protected $table = 'projects';
    public $timestamps = false;

    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $hidden = array('amount', 'notes', 'approver_id');
    protected $validationMessages = [
        'company_id.required' => "Company name is required.",
    ];

    private $rules = array(
        'project_name' => 'required',
        'start_date' => 'required',
        'company_id' => 'required',
    );

    protected $fillable = ['company_id', 'project_name', 'start_date', 'end_date', 'bd_manager', 'account_manager', 'project_manager', 'status', 'notes', 'channel_for_manager', 'channel_for_developer'];

    public function companies()
    {
        return $this->hasOne('App\Models\Admin\Client', 'id', 'company_id');
    }

    public function projectSprints()
    {
        return $this->hasMany('App\Models\Admin\ProjectSprints', 'project_id');
    }
    public function projectNotes()
    {
        return $this->hasMany('App\Models\Admin\ProjectNotes', 'project_id');
    }
    public function projectFiles()
    {
        return $this->hasMany('App\Models\Admin\ProjectFiles', 'project_id', 'id');
    }
    public function projectTechnologies()
    {
        return $this->hasMany('App\Models\Admin\ProjectTechnologies', 'project_id');
    }
    public function technologies()
    {
        return $this->belongsToMany('App\Models\Admin\Technology', 'project_technology', 'project_id', 'technology_id');
    }
    public function resources()
    {
        return $this->belongsToMany('App\Models\User', 'project_resources', 'project_id', 'user_id');
    }
    public function Timelog()
    {
        return $this->hasMany('App\Models\Admin\Timelog', 'project_id');
    }
    public function projectManager()
    {
        return $this->hasOne('App\Models\User', 'id', 'project_manager_id');
    }
    public function bdManager()
    {
        return $this->hasOne('App\Models\User', 'id', 'bd_manager_id');
    }
    public function accountManager()
    {
        return $this->hasOne('App\Models\User', 'id', 'account_manager_id');
    }
    public function projectSlackChannels()
    {
        return $this->hasMany('App\Models\Admin\ProjectSlackChannel', 'project_id', 'id');
    }

    public function timesheets()
    {
        return $this->hasMany('App\Models\Timesheet', 'project_id', 'id');
    }
    public function feedbackUsers()
    {
        return $this->hasMany('App\Models\Admin\FeedbackUser', 'project_id', 'id');
    }
    public function bonus()
    {
        return $this->morphMany('App\Models\Admin\OnSiteBonus', 'reference');
    }
    public function extraDaysBonus()
    {
        return $this->morphMany('App\Models\Admin\AdditionalWorkDaysBonus', 'reference');
    }
    public function reminder()
    {
        return $this->morphMany('App\Models\Admin\Cron', 'reference');
    }
    public function projectLeads()
    {
        return $this->hasMany('App\Models\Admin\ProjectLead', 'project_id', 'id');
    }
    public function client()
    {
        return $this->hasOne('App\Models\Admin\Client', 'id', 'company_id');
    }
    public function salesManager()
    {
        return $this->hasOne('App\Models\User', 'id', 'sales_manager_id');
    }
    public function codeLead()
    {
        return $this->hasOne('App\Models\User', 'id', 'code_lead_id');
    }
    public function deliveryLead()
    {
        return $this->hasOne('App\Models\User', 'id', 'delivery_lead_id');
    }
    public function projectCoordinator()
    {
        return $this->hasOne('App\Models\User', 'id', 'project_coordinator_id');
    }
    public function projectCategory()
    {
        return $this->hasOne('App\Models\Admin\ProjectCategory', 'id', 'project_category_id');
    }
    public function googleLinks()
    {
        return $this->morphMany('App\Model\Admin\GoogleDriveLink', 'reference');
    }

    public static function filter()
    {
        $search = "";
        $arrayId = [];
        $current_user = Auth::user();
        $result = array('status' => false, 'errors' => "", 'message' => "");

        if ((Input::get('status') != '') || empty(Input::get('status'))) {
            $projects = Project::orderBy('id', 'ASC');
            $projectst = $projects->get();
            foreach ($projectst as $project) {
                if ($project->status == 'in_progress') {
                    array_push($arrayId, $project->id);
                }
            }
            $projects = $projects->whereIn('id', $arrayId);
        }
        if (preg_replace('/[\s]+.*/', '', Input::get('status')) == "my") {
            $projects = Project::orderBy('id', 'ASC');
            $projectst = $projects->get();
            $arrayId = [];
            foreach ($projectst as $project) {
                if ($current_user->id == $project->project_manager_id) {
                    array_push($arrayId, $project->id);
                }
            }

            $projects = $projects->whereIn('id', $arrayId);
        }

        if (preg_replace('/[\s]+.*/', '', Input::get('status')) == "completed") {
            $projects = Project::orderBy('id', 'ASC');
            $projectst = $projects->get();

            $arrayId = [];
            foreach ($projectst as $project) {
                if ($project->status == 'completed') {
                    array_push($arrayId, $project->id);
                }
            }
            $projects = $projects->whereIn('id', $arrayId);
        }
        if (preg_replace('/[\s]+.*/', '', Input::get('status')) == "failed") {
            $projects = Project::orderBy('id', 'ASC');
            $projectst = $projects->get();
            $arrayId = [];
            foreach ($projectst as $project) {
                if ($project->status == 'failed') {
                    array_push($arrayId, $project->id);
                }
            }
            $projects = $projects->whereIn('id', $arrayId);
        }
        if (preg_replace('/[\s]+.*/', '', Input::get('status')) == "all") {
            $projects = Project::orderBy('id', 'ASC');
            $projectst = $projects->get();
            $arrayId = [];

            foreach ($projectst as $project) {
                array_push($arrayId, $project->id);
            }

            $projects = $projects->whereIn('id', $arrayId);
        }

        if (Input::get('searchprojects') != '') {
            $search = str_replace('+', ' ', Input::get('searchprojects'));
            $projects = Project::orderBy('id', 'ASC');
            $projectst = $projects->get();
            $arrayId = [];
            foreach ($projectst as $project) {
                if (stripos($project->project_name, $search) !== false) {
                    array_push($arrayId, $project->id);
                }
            }
            $projects = $projects->whereIn('id', $arrayId);
        }

        $projects = $projects->paginate(50)->appends(Input::get());
        $result = ['status' => true, 'projects' => $projects, 'search' => $search];
        return $result;
    }

    // public static function saveData($data){

    //     //return $data;
    //     $project = new Project();
    //     $project->fill($data);
    public static function getUserProjects($userId)
    {
        $response = [];
        $date = date('Y-m-d');
        $projects = Project::where('project_manager_id', $userId)->where('status', 'in_progress')->get();
        if (count($projects) > 0) {
            foreach ($projects as $project) {
                $data['project'] = $project;
                $projectId = $project->id;
                $data['resources'] = ProjectResource::where('project_id', $projectId)->where('end_date', null)->where(function ($query) use ($date, $projectId) {
                    $query->where('project_id', $projectId)
                        ->orWhere('end_date', '>=', $date);
                })->get();
                $response[] = $data;
            }

        }
        return $response;
    }
}
