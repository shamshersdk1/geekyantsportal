<?php namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class CmsTechnology extends Model {

	use ValidatingTrait;
	
	protected $table = 'cms_technologies';
	public $timestamps = false;

	private $rules = array(
	        'name' => 'required',
	        'logo'  => 'required',
	        'short_description' => 'required',
	    );


	public function cmsProjects() {
		return $this->belongsToMany('App\Models\Admin\CmsProject', 'cms_project_cms_technology', 'cms_technology_id', 'cms_project_id')->take(3);
	}

	public static function saveData($request)
	{
		$response =  array('status' => true,'message' => "success" ,'id' => NULL);

		$technology = new CmsTechnology();
		$technology->name = $request['name'];
		$technology->logo = $request['image_name'];
		$technology->slug = strtolower($request['name']);
		$technology->short_description = $request['short_description'];
		$technology->detailed_description = $request['detailed_description'];
		$technology->is_featured = $request['is_featured'];
		if($technology->save())
		{
			return $response;
		}
		else{
			$response['status'] = false;
			$response['errors'] =  $technology->getErrors();
			return $response;
		}
	}

	
}
