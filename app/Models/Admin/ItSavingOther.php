<?php

namespace App\Models\Admin;

use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Support\Arr;

class ItSavingOther extends Model implements Auditable
{
    use ValidatingTrait;
    use \OwenIt\Auditing\Auditable;

    protected $table = 'it_savings_others';
    public $timestamps = false;
    protected $auditExclude = [ 'id'];

    protected $rules = [
		 'it_savings_id' => 'required | exists:it_savings,id',
         'title' => 'required| string',
         'value' => 'required| numeric',
         'type'  => 'required| string'
    ];

    protected $fillable = ['it_savings_id','title','value','type'];

    public function transformAudit(array $data): array
    {
        if (Arr::has($data, 'new_values.it_savings_id')) {
            $code = $data['new_values']['type'];

            if(!empty($data['new_values']['title'])){
                $newValue = $data['new_values']['title'];
                $newValue .= ' : '. $data['new_values']['value'];
            }   
            if(!empty($data['old_values']['title'])){
                $oldValue = $data['old_values']['title'];
                $oldValue .= ' : '. $data['new_values']['tivaluele'];
            }
                
            $data['new_values'][$code] = $newValue;
            $data['old_values'][$code] = $oldValue;
            
        }
        unset($data['old_values']['it_savings_id']);
        unset($data['new_values']['it_savings_id']);
        unset($data['old_values']['value']);
        unset($data['new_values']['value']);
        unset($data['old_values']['title']);
        unset($data['new_values']['title']);
        unset($data['old_values']['type']);
        unset($data['new_values']['type']);
        return $data;
    }
    
    public function itsaving(){
        return $this->hasOne('App\Models\Admin\ItSaving','id','it_savings_id');
    }
}
