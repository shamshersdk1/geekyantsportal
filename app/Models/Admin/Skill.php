<?php namespace App\Models\Admin;

use App\Models\BaseModel;
use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Skill extends BaseModel 
{

	// use ValidatingTrait;  // Use For Validation.Basically creates a validate() method which is then used in out tech class
    protected $table='skills';

	public $timestamps = true;
	use SoftDeletes;

    public static  function saveData($input){
        
        $skill= new Skill();

        $skill->skill_name=$input;
    
    
        $response['status']=false;
        $response['message']='';
        $response['data']=null;
        if(!$skill->save()){
           $response['message']='Not Saved';
           $response['data']=$tech->getErrors();
        }
        else{
            $response['status']=true;
            $response['message']= 'Saved';
            $response['data']=$skill;
        }
       return $response;
    
    
        
        }	
    

}
