<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Training extends Model
{
    use SoftDeletes;

    public static function saveData($data)
    {
        $res['status'] = false;
        $res['message'] = null;
        $trainingObj = new Training();
        $trainingObj->user_id = $data['user_id'];
        $trainingObj->title = $data['title'];
        $trainingObj->description = $data['description'];
        if (!$trainingObj->save()) {
            $res['status'] = true;
            $res['message'] = $trainingObj->getErrors();
            return $res;
        }
        return $trainingObj;
    }
}
