<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\User;

class UserContact extends Model
{

	use ValidatingTrait;
	use SoftDeletes;

	protected $dates = ['deleted_at'];
	protected $table = 'user_contacts';
	public $timestamps = true;

	private $rules = array(
			'user_id' => 'required',
			'mobile' => 'required',
			'emergency_contact_number' => 'required',
			'personal_email' => 'required',
			'office_email' => 'required'
		);
	
	public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
	public function profile()
    {
        return $this->belongsTo('App\Models\Profile', 'user_id', 'user_id');
    }
	public static function saveData($input_details)
	{
		$userContact = new UserContact();
        $userContact->user_id = $input_details['user_id'];
        $userContact->mobile = $input_details['mobile'];
		$userContact->status = 1;
        $userContact->emergency_contact_number = $input_details['emergency_contact_number'];
        $userContact->personal_email = $input_details['personal_email'];
        $userContact->office_email = $input_details['office_email'];
		$userContact->skype_id = !empty($input_details['skype_id']) ? $input_details['skype_id'] : '';
        $userContact->git_hub_url = !empty($input_details['git_hub_url']) ? $input_details['git_hub_url'] : '';
		$userContact->bit_bucket_url = !empty($input_details['bit_bucket_url']) ? $input_details['bit_bucket_url'] : '';
        $userContact->apple_id = !empty($input_details['apple_id']) ? $input_details['apple_id'] : '';
		$userContact->address_line_1 = !empty($input_details['address_line_1']) ? $input_details['address_line_1'] : '';
        $userContact->address_line_2 = !empty($input_details['address_line_2']) ? $input_details['address_line_2'] : '';
		$userContact->city = !empty($input_details['city']) ? $input_details['city'] : '';
        $userContact->state = !empty($input_details['state']) ? $input_details['state'] : '';
		$userContact->pin_code = !empty($input_details['pin_code']) ? $input_details['pin_code'] : '';
        
        if( $userContact->save() )
			return true;
		else
			return false;	
	}
	public static function updateData($input_details)
	{
		$userContact = UserContact::where('user_id',$input_details['user_id'])->first();
		$userContact->user_id = $input_details['user_id'];
        $userContact->mobile = $input_details['mobile'];
        $userContact->emergency_contact_number = $input_details['emergency_contact_number'];
        $userContact->personal_email = $input_details['personal_email'];
        $userContact->office_email = $input_details['office_email'];
		$userContact->skype_id = !empty($input_details['skype_id']) ? $input_details['skype_id'] : '';
        $userContact->git_hub_url = !empty($input_details['git_hub_url']) ? $input_details['git_hub_url'] : '';
		$userContact->bit_bucket_url = !empty($input_details['bit_bucket_url']) ? $input_details['bit_bucket_url'] : '';
        $userContact->apple_id = !empty($input_details['apple_id']) ? $input_details['apple_id'] : '';
		$userContact->address_line_1 = !empty($input_details['address_line_1']) ? $input_details['address_line_1'] : '';
        $userContact->address_line_2 = !empty($input_details['address_line_2']) ? $input_details['address_line_2'] : '';
		$userContact->city = !empty($input_details['city']) ? $input_details['city'] : '';
        $userContact->state = !empty($input_details['state']) ? $input_details['state'] : '';
		$userContact->pin_code = !empty($input_details['pin_code']) ? $input_details['pin_code'] : '';
        
        if( $userContact->save() )
			return true;
		else
			return false;	
	}

	public static function isAlreadyExist($user_id)
	{
		$userContact = UserContact::where('user_id',$user_id)->first();
        if( !empty($userContact) )
			return true;
		else
			return false;	
	}
}
