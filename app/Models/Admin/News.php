<?php

namespace App\Models\Admin;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

use Validator;

class News extends Model
{
    public static function saveData(Request $request, $id)
    {
        $response =  array('status' => false,'message' => "success",'error'=>'');
        if (!$request->user) {
            $response['error']='Invalid Link Followed';
            return $response;
        }
        $validator = Validator::make($request->all(), [
        'title' => 'required',
        'date' => 'required|date',
        'description' => 'required',
        ]);

        if ($validator->fails()) {
            $response['error']='Invalid Link Followed';
            return $response;
        }
        if ($id==0) {
            $news = new News;
        } else {
            $news=News::find($id);
            if (empty($news)) {
                $response['error']='Invalid Link Followed';
                return $response;
            }
        }
        $news->title=$request->title;
        $news->date=$request->date;
        $news->description=$request->description;
        $news->uploaded_by=\Auth::id();
        $news->save();
        $response['status']=true;
        $response['message']='Saved Successfully';
        return $response;
    }
}
