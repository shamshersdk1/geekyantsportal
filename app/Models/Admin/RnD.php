<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RnD extends Model
{
    use SoftDeletes;
    protected $table = 'r_and_d';
    private $rules = [];
    public function scopeBuildQuery($query)
    {
        return $query;
    }
    public static function saveData($data)
    {
        $res['status'] = false;
        $res['message'] = null;
        $RnDObj = new RnD();
        $RnDObj->user_id = $data['user_id'];
        $RnDObj->title = $data['title'];
        $RnDObj->description = $data['description'];
        if (!$RnDObj->save()) {
            $res['status'] = true;
            $res['message'] = $RnDObj->getErrors();
            return $res;
        }
        return $RnDObj;
    }
}
