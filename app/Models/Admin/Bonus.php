<?php

namespace App\Models\Admin;

use App\Models\Month;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Input;
use Validator;
use Watson\Validating\ValidatingTrait;

class Bonus extends Model
{
    //protected $hidden = array('amount');
    use ValidatingTrait;
    protected $fillable = ['month_id','user_id','date','amount_old','notes','status','approved_by','type','reference_id','reference_type','created_by','amount','draft_amount'];
    protected $appends = ["draft_amount"];

    protected $rules = array(
        'month_id' => 'required|exists:months,id',
        'user_id' => 'required|exists:users,id',
        'date' => 'required',
        'status' => 'required',
       'approved_by' => 'required',
        'reference_id' => 'required',
        'reference_type' => 'required',
        'created_by' => 'required',
    );


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function month()
    {
        return $this->belongsTo('App\Models\Month', 'month_id', 'id');
    }
    public function creator()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }
    public function getDraftAmountAttribute()
    {
        if ($this->reference_type == "App\Models\Admin\OnSiteBonus") {
            return $this->reference->onsiteAllowance->amount;
        } elseif ($this->reference_type == "App\Models\Admin\AdditionalWorkDaysBonus") {
            return $this->reference->amount;
        } elseif ($this->reference_type == "App\Models\User\UserTimesheetExtra") {
            return $this->reference->id;
        } else {
            return 0;
        }
    }
    public function reference()
    {
        return $this->morphTo('reference');
    }
    public function approver()
    {
        return $this->hasOne('App\Models\User', 'id', 'approved_by');
    }
    public function payer()
    {
        return $this->hasOne('App\Models\User', 'id', 'paid_by');
    }
    public function bonusConfirm()
    {
        return $this->hasOne('App\Models\Audited\Bonus\BonusConfirmed', 'bonus_id', 'id');
    }
    public function salaryProcess()
    {
        return $this->belongsTo('App\Models\Audited\Bonus\PrepBonus', 'id', 'bonus_id');
    }
    public function referral()
    {
        return $this->belongsTo('App\Models\User', 'referral_for', 'id');
    }
    public function bonusRequest()
    {
        return $this->belongsTo('App\Models\BonusRequest', 'bonus_request_id', 'id');
    }

    public static function validateBonus(Request $request, $skip)
    {
        $response = array('status' => false, 'errors' => "", 'message' => "");
        $maxYear = Carbon::now()->year + 1;
        if ($skip == 1) {
            $validator = Validator::make($request->all(), [
                'month' => 'required|integer|between:1,12',
                'year' => 'required|integer|between:2000,' . $maxYear,
                'amount' => 'required|regex:/^\d+(\.\d{0,2})?$/',
            ]);
        } else {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'month' => 'required|integer|between:1,12',
                'year' => 'required|integer|between:2000,' . $maxYear,
                'amount' => 'required|regex:/^\d+(\.\d{0,2})?$/',
            ]);
        }
        if ($validator->fails()) {
            $response['errors'] = $validator->errors();
            return $response;
        }
        if ($skip != 1) {
            $user = User::where('name', $request->name)->first();
            if (empty($user)) {
                $response['errors'] = "Enter a valid user";
                return $response;
            }
        }
        $response['status'] = true;
        return $response;
    }

    public static function saveBonus(Request $request)
    {
        $response = Bonus::validateBonus($request, 0);
        if ($response['status']) {
            $user = User::where('name', $request->name)->first();
            $bonus = new Bonus;
            $bonus->user_id = $user->id;
            $bonus->date = \Carbon\Carbon::createFromDate($request->year, $request->month, 25);
            $bonus->amount = $request->amount;
            $bonus->notes = $request->note;
            $bonus->status = 'pending';
            $bonus->created_by = \Auth::id();
            if ($bonus->save()) {
                $response['status'] = 'true';
                return $response;
            } else {
                $response['errors'] = "Unable to save";
                return $response;
            }
        } else {
            return $response;
        }
    }
    public static function updateBonus(Request $request, $id)
    {
        $response = array('status' => false, 'errors' => "", 'message' => "");
        $bonus = Bonus::find($id);
        if (empty($bonus)) {
            $response['errors'] = 'invalid link followed';
            return $response;
        }
        $response = Bonus::validateBonus($request, 1);
        if ($response['status']) {
            $bonus->date = \Carbon\Carbon::createFromDate($request->year, $request->month, 25);
            $bonus->amount = $request->amount;
            $bonus->notes = $request->note;
            if ($bonus->save()) {
                $response['status'] = 'true';
                return $response;
            } else {
                $response['errors'] = "Unable to save";
                return $response;
            }
        } else {
            return $response;
        }
    }
    public static function filter()
    {
        $result = ['status' => false, 'bonuses' => ''];
        $month = [];
        $year = [];
        $arrayId = [];
        if (trim(Input::get('status')) == 'paid') {
            $arrayId = Bonus::pluck('id')->toArray();
        } else {
            $arrayId = Bonus::where('paid', false)->pluck('id')->toArray();
        }
        if (Input::get('month') != '' && trim(Input::get('month')) != "0") {
            $month = Bonus::whereMonth('date', '=', trim(Input::get('month')))->pluck('id')->toArray();
            array_push($month, '-1');
        }
        if (Input::get('year') != '' && trim(Input::get('year')) != "0") {
            $year = Bonus::whereYear('date', '=', trim(Input::get('year')))->pluck('id')->toArray();
            array_push($year, '-1');
        }
        if (count($month) > 0) {
            $arrayId = array_intersect($arrayId, $month);
        }
        if (count($year) > 0) {
            $arrayId = array_intersect($arrayId, $year);
        }
        if (count($arrayId) > 0) {
            $bonuses = Bonus::whereIn('id', $arrayId)->paginate(10)->appends(Input::except('page'));
            $result = ['status' => true, 'bonuses' => $bonuses];
        }
        $result['status'] = true;
        return $result;
    }
    public static function total()
    {
        $total = Bonus::where('paid', 1)->sum('amount');
        return $total;
    }
    public static function customTotal($start_date, $end_date)
    {
        $total = Bonus::where('date', '>=', $start_date)->where('date', '<=', $end_date)->where('paid', 1)->sum('amount');
        return $total;
    }
    public static function saveApprovedBonus($data)
    {
        $response = ['status' => false, 'message' => ""];
        $bonus = new Bonus;
        $bonus->month_id = !empty($data['month_id']) ? $data['month_id'] : 0;
        $bonus->user_id = !empty($data['user_id']) ? $data['user_id'] : null;
        $bonus->date = !empty($data['date']) ? $data['date'] : null;
        $bonus->type = !empty($data['type']) ? $data['type'] : null;
        $bonus->status = 'approved';
        $bonus->bonus_request_id = !empty($data['id']) ? $data['id'] : null;
        $bonus->sub_type = !empty($data['sub_type']) ? $data['sub_type'] : null;
        $bonus->title = !empty($data['title']) ? $data['title'] : null;
        $bonus->referral_for = !empty($data['referral_for']) ? $data['referral_for'] : null;
        $bonus->amount = !empty($data['amount']) ? $data['amount'] : null;
        $bonus->notes = !empty($data['notes']) ? $data['notes'] : null;
        $bonus->approved_by = !empty($data['approved_by']) ? $data['approved_by'] : \Auth::id();
        $bonus->created_by = !empty($data['created_by']) ? $data['created_by'] : \Auth::id();
        if (!empty($data['created_at'])) {
            $bonus->created_at = $data['created_at'];
        }
        if (!empty($data['updated_at'])) {
            $bonus->updated_at = $data['updated_at'];
        }
        $bonus->approved_at = date('Y-m-d H:i:s');

        $bonus->reference_type = !empty($data['reference_type']) ? $data['reference_type'] : null;
        $bonus->reference_id = !empty($data['reference_id']) ? $data['reference_id'] : null;

        if ($bonus->save()) {
            $response['status'] = true;
            $response['message'] = 'Saved!';
            return $response;
        }
        return $response;
    }
    public static function getPreviousMonthBonues($userId, $currentMonthId)
    {
        $bonuses = [];
        $monthObj = Month::find($currentMonthId);

        if ($monthObj) {
            $prevMonthObj = Month::where('month_number', '<', $monthObj->month_number)->orderBy('month_number', 'DESC')->first();
            if ($prevMonthObj) {
                $bonuses = Bonus::where('user_id', $userId)->where('month_id', $prevMonthObj->id)->where('status', 'approved')->get();
            }
        }

        return $bonuses;
    }
}
