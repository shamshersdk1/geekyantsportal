<?php

namespace App\Models\Admin;

use App\Models\BaseModel;

class UserProfileVisibility extends BaseModel
{
    protected $table = 'user_profile_visibility';

    public static function getUserVisibility($userId)
    {
        $visibilityObj = UserProfileVisibility::where('user_id', $userId)->first();
        if ($visibilityObj) {
            return $visibilityObj;
        } else {
            $obj = new UserProfileVisibility;
            $obj->user_id = $userId;

            if ($obj->save()) {
                return $obj;
            }

        }
    }
}
