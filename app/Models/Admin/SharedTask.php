<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Exception;

class SharedTask extends Model
{

    use SoftDeletes;

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function owner()
    {
        return $this->belongsTo('App\Models\User', 'owner_id', 'id');
    }

    public function creator()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
    }

    public function taskGroup()
    {
        return $this->belongsTo('App\Models\Admin\TaskGroup');
    }

    public static function assign($user_id, $shared_array, $created_by)
    {
        $response = ['status' => true, 'message' => ""];
        try {
            $owner_id = \Auth::id();
            $current_shared = SharedTask::where('user_id', $user_id)->where('owner_id', '!=', $owner_id)->pluck('owner_id')->toArray();
            $add = array_diff($shared_array, $current_shared);
            $subtract = array_diff($current_shared, $shared_array);
            $delete = SharedTask::where('user_id', $user_id)->whereIn('owner_id', $subtract)->delete();
            foreach($add as $owner_id)
            {
                $shared = new SharedTask();
                $shared->user_id = $user_id;
                $shared->owner_id = $owner_id;
                $shared->created_by = $created_by;
                $shared->save();
            }
            return $response;
        } catch(Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return $response;
        }
    }
}
