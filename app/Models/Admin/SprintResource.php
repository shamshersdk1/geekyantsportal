<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class SprintResource extends Model
{
    //

	use ValidatingTrait;
	protected $table = 'sprint_resources';
	

	private $rules = array(
		    'sprint_id' => 'required',
	        'employee_id' => 'required',
	        'hours' => 'Integer'


	        // 'description'  => 'required'
	    );
	public function user() {
            return $this->hasOne('App\Models\User','id','employee_id');
        }
	public function projectSprints() {
            return $this->hasOne('App\Models\Admin\ProjectSprints','id','sprint_id');
        }

}
