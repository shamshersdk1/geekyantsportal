<?php

namespace App\Models\Admin;

use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LeaveDeductionMonth extends Model
{
    protected $table = 'leave_deduction_month';
    use SoftDeletes;

    public function leave()
    {
        return $this->belongsTo('App\Models\Admin\Leave', 'leave_id', 'id');
    }
    public function month()
    {
        return $this->belongsTo('App\Models\Month', 'month_id', 'id');
    }
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
    public static function saveDeduction($userId, $leaveTypeId, $monthId, $duration = null, $comment = null)
    {
        $currentUser = Auth::user();
        $leaveDeductionObj = new LeaveDeductionMonth();
        $leaveDeductionObj->user_id = $userId;
        $leaveDeductionObj->leave_type_id = $leaveTypeId;
        $leaveDeductionObj->month_id = $monthId;
        $leaveDeductionObj->duration = $duration ? $duration : null;
        $leaveDeductionObj->status = "approved";
        $leaveDeductionObj->approver_id = !empty($currentUser) ? $currentUser->id : null;
        $leaveDeductionObj->comments = !empty($comment) ? $comment : null;
        $leaveDeductionObj->status = "approved";

        if ($leaveDeductionObj->save()) {
            return $leaveDeductionObj->id;
        }

        return false;
    }
    public static function getLOPDays($userId, $monthId)
    {
        $obj = self::where('user_id', $userId)->where('month_id', $monthId)->where('status', 'approved')->first();
        if ($obj) {
            return $obj->duration;
        }

        return 0;
    }
}
