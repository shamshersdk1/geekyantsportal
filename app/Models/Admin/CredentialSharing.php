<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class CredentialSharing extends Model
{

	use ValidatingTrait;
	protected $table = 'credential_sharing';
	public $timestamps = false;

	private $rules = array(
			'user_id' => 'required'
		);
		
	public function credential() {
        return $this->hasOne('App\Models\Admin\Credential', 'id', 'credential_id' );
    }
	public function user() {
        return $this->hasOne('App\Models\User', 'id', 'user_id' );
    }
}
