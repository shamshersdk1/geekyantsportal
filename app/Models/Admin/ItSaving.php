<?php

namespace App\Models\Admin;

use App\Models\Admin\VpfDeduction;
use App\Models\BaseModel;
use App\Models\Month;
use Auth;
use DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Appraisal\AppraisalComponentType;
use App\Models\Transaction;
use App\Services\Payroll\AppraisalService;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Support\Arr;

class ItSaving extends BaseModel implements Auditable
{
    use SoftDeletes;
    use \OwenIt\Auditing\Auditable;
    protected $dates = ['deleted_at'];
    protected $table = 'it_savings';

    protected $appends = ['total_investments','total_deductions','total_pf'];
    protected $auditExclude = [ 'id','user_id','financial_year_id','created_at', 'updated_at'];
    public function transformAudit(array $data): array
    {
        if (Arr::has($data, 'new_values.agree')) {
            $data['old_values']['agree'] = $data['old_values']['agree'] ? 'Yes' : 'No';
            $data['new_values']['agree'] = $data['new_values']['agree'] ? 'Yes' : 'No';
        }
        return $data;
    }
    public function financialYear()
    {
        return $this->belongsTo('App\Models\Admin\FinancialYear', 'financial_year_id', 'id');
    }
    
    public function getTotalDeductionsAttribute()
    {
        $amount = $this->medical_insurance_premium + $this->medical_treatment_expense + $this->educational_loan + $this->donation + $this->rent_without_receipt +
        $this->physical_disablity + $this->political_party + $this->lta;
        $amount += $this->others->where('type','other_multiple_deductions')->sum('value');
        return $amount;

    }

    public function getTotalInvestmentsAttribute()
    {
        $monthObj = Month::where('status', 'in_progress')->where('financial_year_id', $this->financial_year_id)->orderBy('year','ASC')->orderBy('month','ASC')->first();
        $pfEmployee = AppraisalComponentType::getIdByKey('pf-employee');
        $pfEmployeeAmount = 0;
        $vpfAmount = 0;
        $amount = 0;
        $pfEmployeeAmount = Transaction::getPaidTillDate($this->user_id, $this->financial_year_id, 'App\Models\Appraisal\AppraisalComponentType', $pfEmployee);
        $vpfAmount = Transaction::getPaidTillDate($this->user_id, $this->financial_year_id, 'App\Models\Admin\VpfDeduction');
        if($monthObj->status == "in_progress"){
            $appraisalList = AppraisalService::getCurrentMonthAppraisal($this->user_id, $monthObj->id);
            if ($appraisalList == null || !isset($appraisalList[0])) {
                $response['errors'] = 'No Appraisal found!';
                $response['data'] = $data;
                return $response;
            }
            $currData = AppraisalService::generateCurrentData($appraisalList, $monthObj->id, $this->user_id);

            $pfEmployeeAmount += $currData[$pfEmployee];
            $vpfAmount += VpfDeduction::getCurrMonthVpf($monthObj->id, $this->user_id);

            if($pfEmployeeAmount < 0 )
                $pfEmployeeAmount *= -1;
            if($vpfAmount < 0 )
                $vpfAmount *= -1;

        }
        $amount = $vpfAmount + $pfEmployeeAmount + $this->pf + $this->pension_scheme_1 + $this->pension_scheme_1b + $this->ppf + $this->central_pension_fund +
        $this->lic + $this->housing_loan_repayment + $this->term_deposit + $this->national_saving_scheme + $this->tax_saving + $this->children_expense;
        $amount += $this->others->where('type','other_multiple_investments')->sum('value');
        return ($amount < 150000) ? $amount : 150000;
    }

    public function getTotalPfAttribute()
    {
        $monthObj = Month::where('status', 'in_progress')->where('financial_year_id', $this->financial_year_id)->orderBy('year','ASC')->orderBy('month','ASC')->first();
        $pfEmployee = AppraisalComponentType::getIdByKey('pf-employee');
        $pfEmployeeAmount = 0;
        $vpfAmount = 0;
        $amount = 0;
        $pfEmployeeAmount = Transaction::getPaidTillDate($this->user_id, $this->financial_year_id, 'App\Models\Appraisal\AppraisalComponentType', $pfEmployee);
        $vpfAmount = Transaction::getPaidTillDate($this->user_id, $this->financial_year_id, 'App\Models\Admin\VpfDeduction');
        if($monthObj->status == "in_progress"){
            $appraisalList = AppraisalService::getCurrentMonthAppraisal($this->user_id, $monthObj->id);
            if ($appraisalList == null || !isset($appraisalList[0])) {
                $response['errors'] = 'No Appraisal found!';
                $response['data'] = $data;
                return $response;
            }
            $currData = AppraisalService::generateCurrentData($appraisalList, $monthObj->id, $this->user_id);

            $pfEmployeeAmount += $currData[$pfEmployee];
            $vpfAmount += VpfDeduction::getCurrMonthVpf($monthObj->id, $this->user_id);

            if($pfEmployeeAmount < 0 )
                $pfEmployeeAmount *= -1;
            if($vpfAmount < 0 )
                $vpfAmount *= -1;
        }
        $amount = $vpfAmount + $pfEmployeeAmount + $this->pf;
        return $amount;
    }

     public function getMonthTotalPf($monthId)
    {
        $monthObj = Month::find($monthId);
        $pfEmployee = AppraisalComponentType::getIdByKey('pf-employee');
        $pfEmployeeAmount = 0;
        $vpfAmount = 0;
        $amount = 0;
        $pfEmployeeAmount = Transaction::getPaidTillDate($this->user_id, $this->financial_year_id, 'App\Models\Appraisal\AppraisalComponentType', $pfEmployee);
        $vpfAmount = Transaction::getPaidTillDate($this->user_id, $this->financial_year_id, 'App\Models\Admin\VpfDeduction');
        $appraisalList = AppraisalService::getCurrentMonthAppraisal($this->user_id, $monthObj->id);
        if ($appraisalList == null || !isset($appraisalList[0])) {
            $response['errors'] = 'No Appraisal found!';
            $response['data'] = $data;
            // return $response;
        }
        $currData = AppraisalService::generateCurrentData($appraisalList, $monthObj->id, $this->user_id);
        $currentPfAmount = abs($currData[$pfEmployee]);
        $currentVpfAmount = abs(VpfDeduction::getCurrMonthVpf($monthObj->id, $this->user_id));
        $currentFuturePf = $currentPfAmount * FinancialYear::getRemainingMonths($monthObj->id);
        $amount = $currentVpfAmount + $currentPfAmount + $currentFuturePf + abs($vpfAmount) + abs($pfEmployeeAmount) + abs($this->pf);
        return $amount;
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
    public function userData()
    {
        return $this->hasMany('App\Models\Fincance\ItSavingUserData', 'it_saving_id', 'id');
    }
    public function others()
    {
        return $this->hasMany('App\Models\Admin\ItSavingOther', 'it_savings_id', 'id');
    }
    public static function saveData($data, $ItSavingObj)
    {

        $multipleInv = [];
        $response = ['status' => false,
            'message' => "",
            'data' => ""];
        $ItSavingObj->user_id = !empty($data['user_id']) ? $data['user_id'] : null;
        $ItSavingObj->financial_year_id = !empty($data['financial_year_id']) ? $data['financial_year_id'] : null;
        $ItSavingObj->rent_monthly = !empty($data['rent_monthly']) ? $data['rent_monthly'] : null;
        $ItSavingObj->rent_yearly = !empty($data['rent_yearly']) ? $data['rent_yearly'] : null;
        $ItSavingObj->lta = !empty($data['lta']) ? $data['lta'] : 0;
        $ItSavingObj->pf = !empty($data['pf']) ? $data['pf'] : null;
        $ItSavingObj->pension_scheme_1 = !empty($data['pension_scheme_1']) ? $data['pension_scheme_1'] : null;
        $ItSavingObj->pension_scheme_1b = !empty($data['pension_scheme_1b']) ? $data['pension_scheme_1b'] : null;
        $ItSavingObj->ppf = !empty($data['ppf']) ? $data['ppf'] : null;
        $ItSavingObj->central_pension_fund = !empty($data['central_pension_fund']) ? $data['central_pension_fund'] : null;
        $ItSavingObj->lic = !empty($data['lic']) ? $data['lic'] : null;
        $ItSavingObj->housing_loan_repayment = !empty($data['housing_loan_repayment']) ? $data['housing_loan_repayment'] : null;
        $ItSavingObj->term_deposit = !empty($data['term_deposit']) ? $data['term_deposit'] : null;
        $ItSavingObj->national_saving_scheme = !empty($data['national_saving_scheme']) ? $data['national_saving_scheme'] : null;
        $ItSavingObj->tax_saving = !empty($data['tax_saving']) ? $data['tax_saving'] : null;
        $ItSavingObj->children_expense = !empty($data['children_expense']) ? $data['children_expense'] : null;
        $ItSavingObj->other_investment = !empty($data['other_investment']) ? $data['other_investment'] : null;
        $ItSavingObj->medical_insurance_premium = !empty($data['medical_insurance_premium']) ? $data['medical_insurance_premium'] : null;
        $ItSavingObj->medical_treatment_expense = !empty($data['medical_treatment_expense']) ? $data['medical_treatment_expense'] : null;
        $ItSavingObj->educational_loan = !empty($data['educational_loan']) ? $data['educational_loan'] : null;
        $ItSavingObj->donation = !empty($data['donation']) ? $data['donation'] : null;
        $ItSavingObj->rent_without_receipt = !empty($data['rent_without_receipt']) ? $data['rent_without_receipt'] : null;
        $ItSavingObj->physical_disablity = !empty($data['physical_disablity']) ? $data['physical_disablity'] : null;
        $ItSavingObj->other_deduction = !empty($data['other_deduction']) ? $data['other_deduction'] : null;
        $ItSavingObj->salary_paid = !empty($data['salary_paid']) ? $data['salary_paid'] : null;
        $ItSavingObj->tds = !empty($data['tds']) ? $data['tds'] : null;
        $ItSavingObj->status = !empty($data['status']) ? $data['status'] : null;
        //print_r($data['previous_form_16_12b']);
        if (isset($data['previous_form_16_12b']) && $data['previous_form_16_12b'] == 'false') {
            $ItSavingObj->previous_form_16_12b = 0;
        } elseif (isset($data['previous_form_16_12b']) && $data['previous_form_16_12b'] == true) {
            $ItSavingObj->previous_form_16_12b = 1;
        }
        if (isset($data['agree']) && $data['agree'] == 'false') {
            $ItSavingObj->agree = 0;
        } elseif (isset($data['agree']) && $data['agree'] == true) {
            $ItSavingObj->agree = 1;
        }
        else{
            $ItSavingObj->agree = 0;
        }
        if (!$ItSavingObj->save()) {
            $response['message'] = $ItSavingObj->getErrors();
            return $response;
        }

        ItSavingOther::where('it_savings_id', $ItSavingObj->id)->delete();
        if (!empty($data['other_multiple_investments'])) {

            $data['other_multiple_investments'] += ['type' => 'other_multiple_investments'];
            foreach ($data['other_multiple_investments']['name'] as $key => $value) {
                if ($value) {
                    $itSavingObj = ItSavingOther::create(['it_savings_id' => $ItSavingObj->id, 'title' => $value, 'value' => $data['other_multiple_investments']['amount'][$key], 'type' => $data['other_multiple_investments']['type']]);
                }
            }

            if (isset($itSavingObj) && count($itSavingObj->getErrors()) > 0) {
                $response['message'] = $itSavingObj->getErrors();
                return $response;
            }
        }
        if (!empty($data['other_multiple_deductions'])) {
            $data['other_multiple_deductions'] += ['type' => 'other_multiple_deductions'];
            foreach ($data['other_multiple_deductions']['name'] as $key => $value) {
                if ($value) {
                    $itSavingObj = ItSavingOther::create(['it_savings_id' => $ItSavingObj->id, 'title' => $value, 'value' => $data['other_multiple_deductions']['amount'][$key], 'type' => $data['other_multiple_deductions']['type']]);
                }
            }
            if (isset($itSavingObj) && count($itSavingObj->getErrors()) > 0) {
                $response['message'] = $itSavingObj->getErrors();
                return $response;
            }
        }

        $month = date('m');
        $monthObj = Month::where('month', $month)->first();
        if (!$monthObj) {
            $response['status'] = true;
            $response['message'] = "Saved Successfully!";
            $response['data'] = $ItSavingObj;
            return $response;
        }
        
        $response['status'] = true;
        $response['message'] = "Saved Successfully!";
        $response['data'] = $ItSavingObj;
        return $response;
    }

    // role is admin or not
    // date range is 21 - 30
    public static function checkForUpdate()
    {
        $day = date('d');
        $dateCheck = 0;
        $roleCheck = 0;
        if ($day >= 21 && $day <= 31) {
            $dateCheck = true;
        }
        $roleCheck = Auth::user()->role == 'admin';

        return $dateCheck && $roleCheck;
    }

    public static function getITSavingInfo($userId, $monthId = null, $financialYearId = null)
    {

        $response['investment'] = 0;
        $response['deduction'] = 0;
        $response['previous_salary'] = 0;
        $response['salary_paid'] = 0;
        $response['tds'] = 0;
        $currentVpfAmount = 0;

        $monthObj = Month::find($monthId);

        // $financialYear = $monthObj->financial_year_id;

        if (!$monthObj && !$financialYearId) {
            return $response;
        }

        if ($monthObj) {
            $itSavingObj = ItSaving::where('user_id', $userId)->where('financial_year_id', $monthObj->financial_year_id)->first();
        } else {
            $itSavingObj = ItSaving::where('user_id', $userId)->where('financial_year_id', $financialYearId)->first();
        }

        if (!$itSavingObj) {
            return $response;
        }

        if ($monthId == null) {
            $month = date('m');
            $monthObj = Month::where('month', $month)->where('financial_year_id', $financialYearId)->first();
            $monthId = $monthObj->id;
        }
        //VPF
        $vpfPaid = VpfDeduction::getVPFPaidTillDate($userId, $monthId);
        $currentMonthVpf = VpfDeduction::where('month_id', $monthId)->where('user_id', $userId)->first();

        if ($currentMonthVpf) {
            $currentVpfAmount = $currentMonthVpf->amount;
        }

        $investment = $itSavingObj->pf +
        $itSavingObj->pension_scheme_1 +
        $itSavingObj->pension_scheme_1b +
        $itSavingObj->ppf +
        $itSavingObj->central_pension_fund +
        $itSavingObj->lic +
        $itSavingObj->housing_loan_repayment +
        $itSavingObj->term_deposit +
        $itSavingObj->national_saving_scheme +
        $itSavingObj->tax_saving +
        $itSavingObj->children_expense +
        $itSavingObj->other_investment;
        $totalInvestment = $investment + $currentVpfAmount;

        // if ($financialYear == 1) {
        if ($itSavingObj->other_multiple_investments != null) {
            $investmentArray = json_decode($itSavingObj->other_multiple_investments, true);

            if (is_array($investmentArray) && count($investmentArray) > 0) {
                if (isset($investmentArray['amount']) && is_array($investmentArray['amount'])) {
                    foreach ($investmentArray['amount'] as $amount) {
                        if (is_numeric($amount)) {
                            $totalInvestment += $amount;
                        }

                    }
                }

            }
        }
        // print_r($totalInvestment);
        // print_r(count(json_decode($itSavingObj->other_multiple_investments)->amount));
        // print_r($itSavingObj->other_multiple_deductions);
        // die;
        // }
        $response['investment'] = min($totalInvestment, 150000);

        $deduction = $itSavingObj->medical_insurance_premium +
        $itSavingObj->medical_treatment_expense +
        $itSavingObj->educational_loan +
        $itSavingObj->donation +
        $itSavingObj->rent_without_receipt +
        $itSavingObj->physical_disablity +
        $itSavingObj->other_deduction;

        // $deduction += $itSavingObj->lta;

        if ($itSavingObj->other_multiple_deductions != null) {
            $deductionArray = json_decode($itSavingObj->other_multiple_deductions, true);

            if (is_array($deductionArray) && count($deductionArray) > 0) {
                if (isset($deductionArray['amount']) && is_array($deductionArray['amount'])) {
                    foreach ($deductionArray['amount'] as $amount) {
                        if (is_numeric($amount)) {
                            $deduction += $amount;
                        }

                    }
                }

            }
        }

        // foreach (json_decode($itSavingObj->other_multiple_deductions)->amount as $amount) {
        //     $deduction += $amount;
        // }
        //$lta claimed to lta_deduction for the it sving -> which every is lower;

        $response['vpf_paid'] = $vpfPaid;
        $response['deduction'] = $deduction;
        $response['salary_paid'] = $itSavingObj->other_deduction;
        $response['tds'] = $itSavingObj->tds;
        return $response;

    }
    public static function GetTableColumns()
    {

        $sql = "Select DISTINCT COLUMN_NAME  from information_schema.columns where table_name='it_savings' AND COlUMN_NAME NOT IN ('id','user_id','financial_year_id','rent_monthly','salary_paid','previous_form_16_12b','status','created_at','updated_at','deleted_at')";
        $itSavingComponents = DB::select($sql);

        return $itSavingComponents;
    }

    public static function getItInvestment($userId, $financialYearId)
    {
        $investment = 0;

        $itSavingObj = ItSaving::where('user_id', $userId)->where('financial_year_id', $financialYearId)->first();
        if (!$itSavingObj) {
            return 0;
        }

        $investment = $itSavingObj->pf +
        $itSavingObj->pension_scheme_1 +
        $itSavingObj->pension_scheme_1b +
        $itSavingObj->ppf +
        $itSavingObj->central_pension_fund +
        $itSavingObj->lic +
        $itSavingObj->housing_loan_repayment +
        $itSavingObj->term_deposit +
        $itSavingObj->national_saving_scheme +
        $itSavingObj->tax_saving +
        $itSavingObj->children_expense +
        $itSavingObj->other_investment;

        if ($itSavingObj->other_multiple_investments != null) {
            $investmentArray = json_decode($itSavingObj->other_multiple_investments, true);

            if (is_array($investmentArray) && count($investmentArray) > 0) {
                if (isset($investmentArray['amount']) && is_array($investmentArray['amount'])) {
                    foreach ($investmentArray['amount'] as $amount) {
                        if (is_numeric($amount)) {
                            $investment += $amount;
                        }
                    }
                }
            }
        }

        return $investment;
    }

    public static function getItDeduction($userId, $financialYearId)
    {
        $deduction = 0;

        $itSavingObj = ItSaving::where('user_id', $userId)->where('financial_year_id', $financialYearId)->first();
        if (!$itSavingObj) {
            return 0;
        }

        $deduction = $itSavingObj->medical_insurance_premium +
        $itSavingObj->medical_treatment_expense +
        $itSavingObj->educational_loan +
        $itSavingObj->donation +
        $itSavingObj->rent_without_receipt +
        $itSavingObj->physical_disablity +
        $itSavingObj->other_deduction;

        if ($itSavingObj->other_multiple_deductions != null) {
            $deductionArray = json_decode($itSavingObj->other_multiple_deductions, true);

            if (is_array($deductionArray) && count($deductionArray) > 0) {
                if (isset($deductionArray['amount']) && is_array($deductionArray['amount'])) {
                    foreach ($deductionArray['amount'] as $amount) {
                        if (is_numeric($amount)) {
                            $deduction += $amount;
                        }
                    }
                }
            }
        }

        return $deduction;
    }
}
