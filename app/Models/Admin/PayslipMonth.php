<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

use App\Jobs\Payslip\ReadPayslipMonthDataJob;
use App\Jobs\Payslip\ProcessPayslipDataJob;
use App\Models\User;

class PayslipMonth extends Model
{
    use ValidatingTrait;

    protected $table = 'payslip_month';

    public $timestamps = true;

    private $rules = array(

        );

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
    public function month()
    {
        return $this->belongsTo('App\Models\Month', 'user_id', 'id');
    }
    public function payslipData()
    {
        return $this->hasMany('App\Models\Admin\PayslipData', 'payslip_month_id', 'id');
    }
    public function approver()
    {
        return $this->hasOne('App\Models\User', 'id', 'approved_by');
    }
    public function formatMonth() {
        $dateObj = \DateTime::createFromFormat('!m', $this->month);
        $monthName = $dateObj->format('F');
        return $monthName . ', '. $this->year;
        //return $dt->format('F');
    }
    public static function saveData($request)
    {
        try {
            $response =  [
                            'status'    => true,
                            'message'   => "success" ,
                            'id'        => null,
                            'log'       => [],
                            'errors'    => []
                        ];

            if (empty( $request->csv_file) || (empty($request->month) || !is_numeric($request->month)) || (empty($request->year) || !is_numeric($request->year))) {
                $response['status'] = false;
                $response['log'][] = 'Please enter a valid csv file, month and year';
                return $response;
            }
            $extensionType = $request->file('csv_file')->getClientOriginalExtension();
            $fileName = $request->file('csv_file')->getClientOriginalName();
            if ($extensionType != 'csv') {
                $response['status'] = false;
                $response['log'][] = 'Please enter a valid csv file';
                return $response;
            }
            
            $payslipMonth = PayslipMonth::where('month', $request->month)->where('year', $request->year)->where('status', '=', 'approved')->first();

            if ($payslipMonth) {
                $response['status'] = false;
                $response['log'][] = 'Payslip already exists for this month and year';
                return $response;
            }

            $payslipMonthObj = new PayslipMonth();
            $payslipMonthObj->month = $request->month;
            $payslipMonthObj->year = $request->year;
            $payslipMonthObj->user_id = $request->user->id;
            $payslipMonthObj->template = $request->template;
            $payslipMonthObj->status = 'in_progress';
            $file = $request->file('csv_file');
            $file->move(storage_path('payslip'), $fileName);
            $payslipMonthObj->file_path = storage_path('payslip').'/'.$fileName;
            if (!$payslipMonthObj->save()) {
                $response['status'] = false;
                $response['log'][] = 'Something went wrong and payslip was not saved.';
                return $response;
            }

            //$job = (new ReadPayslipMonthDataJob($payslipMonthObj))->onQueue('save-payslip-data');
            //dispatch($job);
            $job = (new ProcessPayslipDataJob($payslipMonthObj))->onQueue('save-payslip-data');
            dispatch($job);

            $response['message'] = 'Payslip successfully saved';
            return $response;
        } catch (\Exception $e) {
            Log::info('Error here '.$e->getMessage());
        }
    }
    public static function matchCSVData($id) {
        /*  match each record of CSV with the list of activated users and 
            add status for each record
        */

        $obj =  PayslipMonth::find($id);
        if(!$obj)
            return;
        
        if(!$obj->json_data)
            return false;

        $csvData = json_decode($obj->json_data, true);
        $overAllStatus = 'in_progress';
        
        foreach ($csvData as &$data) {
            $empId = isset($data['employee_id'])?$data['employee_id']:null;
            if(!$empId)  {
                $data['comment'] = "Employee ID field missing in CSV";
                $data['status'] = 'rejected';
                $overAllStatus = 'rejected';
                continue;
            }
            $userObj = User::where('employee_id', $empId)->first();
            if(!$userObj) {
                $data['comment'] = "Invalid Employee ID ".$empId ." found in CSV";
                $data['status'] = 'rejected';
                //$overAllStatus = 'rejected';
                continue;
            }
            if(!$userObj->is_active) {
                $data['comment'] = "Employee ID ".$empId ." INACTIVE";
                $data['status'] = 'inactive';
                //$overAllStatus = 'rejected';
                continue;
            }
            $data['comment'] = "Accepted";
            $data['status'] = "approved";

        }
        usort($csvData, function($a, $b) {
            return $a['status'] == 'approved';
        });
        $users = User::where('is_active',1)->get();
        if(count($users) > 0) {
            foreach ($users as $user) {
                $empId = $user->employee_id;
                $newData = [];
                
                $found_key = array_search($empId, array_column($csvData, 'employee_id'));
                if ($found_key === false) {
                    $newData['employee_id'] = $empId;
                    $newData['name'] = $user->name;
                    $newData['email'] = $user->email;
                    $newData['comment'] = "Employee ID ".$empId ." not found in CSV";   
                    $newData['status'] = 'rejected';
                    $overAllStatus = 'rejected';
                    $csvData[] = $newData;
                }                
            }
        }
        $obj->json_data = json_encode($csvData);
        $obj->status = 'pending';
        if($overAllStatus == 'rejected') {
            //$obj->status = 'rejected';
            $obj->comment = 'Rejected by the system';
        }
        $obj->save();
    }
}
