<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model\Admin;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;
use OwenIt\Auditing\Contracts\Auditable;

class VPF extends Model implements Auditable
{
    use SoftDeletes;
    protected $table = 'vpfs';
    use \OwenIt\Auditing\Auditable;

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function creator()
    {
        return $this->belongsTo('App\Models\User', 'created_by');
    }
    public function children()
    {
        return $this->belongsTo('App\Models\Admin\VPF','parent_id','id');
    }

    public static function saveData($request)
    {
        
        $data = $request->all();
        $ifExits = VPF::where('parent_id', null)->where('user_id',$data['user_id'])->first();
        if($ifExits)
            return false;
        else
        {
            if(!empty($data)){
            $vpf = new VPF();
            $vpf->user_id = $data['user_id'];
            $vpf->amount = $data['amount'];
            $vpf->created_by = Auth::id();
            if ($vpf->save())
                return true;
            else
                return false;
            }
        }
    }

    public static function updateRequest($request, $id)
    {
        $vpf = VPF::find($id);
        if(isset($vpf)){
            if($request->amount)
            {
                $new_vpf = new VPF();
                $new_vpf->user_id = $vpf->user_id;
                $new_vpf->amount = $request->amount;
                $new_vpf->created_by = $vpf->created_by;
                $new_vpf->updated_by = $vpf->updated_by;
                $new_vpf->status = $vpf->status;
                $new_vpf->save();
                $vpf->parent_id = $new_vpf->id; 
                 if ( $vpf->save() )
                 {
                     return true;
                     //return redirect('/admin/vpf')->with('message', 'Saved!');
                 } else {
                     return $vpf->getErrors();
                 }  
            }
        }   

    }

}
