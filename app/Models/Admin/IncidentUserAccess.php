<?php namespace App\Models\Admin;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;
use App\Models\Admin\Department;

class IncidentUserAccess extends BaseModel
{

    use ValidatingTrait;

    protected $table = 'incident_user_accesses';
    public $timestamps = true;
    use SoftDeletes;
    
    private $rules = array(
        'incident_id' => 'required',
        'user_id' => 'required',
    );

    public function incident()
    {
        return $this->belongsTo('App\Models\Admin\Incident', 'incident_id', 'id');
    }
    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
    
}
