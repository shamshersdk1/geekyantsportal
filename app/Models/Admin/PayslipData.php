<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

use App\Models\AccessLog;
use App\Models\Admin\PayslipMonth;
use App\Models\User;
use App\Events\LoanEmiPaid;

use DB;

class PayslipData extends Model
{
    use ValidatingTrait;

    protected $table = 'payslip_data';

    // public $timestamps = true;

    private $rules = array(

        );

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
    public function payslip_month()
    {
        return $this->belongsTo('App\Models\Admin\PayslipMonth', 'payslip_month_id', 'id');
    }


    public static function processPayslipMonthData ($payslipMonthId, $userId, $employeeIds)
    {
      DB::beginTransaction();
      try {
        $response =  [
                        'status'    => true,
                        'message'   => "success" ,
                        'id'        => null,
                        'log'       => [],
                        'errors'    => []
                    ];
        $playslipMonthObj = PayslipMonth::find($payslipMonthId);

        if (empty($payslipMonthId) || empty($userId)) {
            $response['status'] = false;
            $response['log'][] = 'Data incomplete';
            return $response;
        }

        if ( $playslipMonthObj->status === 'pending' )
        {
            $json_data = json_decode($playslipMonthObj['json_data'],true);

            foreach ($employeeIds as $employeeId) {
                foreach ($json_data as $key => $value) {
                    if($employeeId == $value['employee_id'])
                      $response = self::saveData($value, $payslipMonthId, $userId);
                    if ( $response['status'] === false ) {
                        DB::rollback();
                        break;
                    }
                }
            }
          // $json_data = json_decode($playslipMonthObj['json_data'],true);
          // foreach ($json_data as $key => $value) {
          //   $response = self::saveData($value, $payslipMonthId, $userId);
          //   if ( $response['status'] === false ) {
          //     DB::rollback();
          //     break;
          //   }
          // }
          DB::commit();
        }
      } catch (\Exception $e) {
          AccessLog::accessLog(null, 'App\Models', 'Product', 'getPayslipMonthData', 'catch-block', $e->getMessage());
          DB::rollback();
      }
      return $response;
    }

    public static function saveData($data, $payslipMonthId, $userId)
    {

        try {
            $response =  [
                            'status'    => true,
                            'message'   => "success" ,
                            'id'        => null,
                            'log'       => [],
                            'errors'    => []
                        ];
            if (empty($data) || empty($payslipMonthId) || empty($userId)) {
                $response['status'] = false;
                $response['log'][] = 'Data incomplete';
                return $response;
            }
            
            $user_id = User::where('employee_id',trim($data['employee_id']))->first();
            
            $payslipDataObj = new PayslipData();
            $payslipDataObj->user_id = !empty($user_id->id) ? $user_id->id : 0;
            // $payslipDataObj->comments = json_encode($data);
            $payslipDataObj->email = !empty($data['email']) ? $data['email'] : null;
            $payslipDataObj->payslip_month_id = $payslipMonthId;
            $payslipDataObj->pan_no = !empty($data['pan_no']) ? $data['pan_no'] : null;
            $payslipDataObj->date_of_birth = !empty($data['date_of_birth']) ? $data['date_of_birth'] : null;
            $payslipDataObj->date_of_joining = !empty($data['date_of_joining']) ? $data['date_of_joining'] : null;
            $payslipDataObj->designation = !empty($data['designation']) ? $data['designation'] : null;
            $payslipDataObj->gender = !empty($data['gender']) ? $data['gender'] : null;
            $payslipDataObj->bank_account_no = !empty($data['bank_account_no']) ? $data['bank_account_no'] : null;
            $payslipDataObj->bank_ifsc = !empty($data['bank_ifsc']) ? $data['bank_ifsc'] : null;
            $payslipDataObj->pf_no = !empty($data['pf_no']) ? $data['pf_no'] : null;
            $payslipDataObj->uan_no = !empty($data['uan_no']) ? $data['uan_no'] : null;
            $payslipDataObj->esi_no = !empty($data['esi_no']) ? $data['esi_no'] : null;
            $payslipDataObj->month = !empty($data['month']) ? $data['month'] : null;
            $payslipDataObj->year = !empty($data['year']) ? $data['year'] : null;
            $payslipDataObj->working_day_month = !empty($data['working_day_month']) ? $data['working_day_month'] : null;
            $payslipDataObj->days_worked = !empty($data['days_worked']) ? $data['days_worked'] : null;

            //integer values, default should be 0
            $payslipDataObj->basic = !empty($data['basic']) ? $data['basic'] : 0;
            $payslipDataObj->hra = !empty($data['hra']) ? $data['hra'] : 0;
            $payslipDataObj->conveyance_allowance = !empty($data['conveyance_allowance']) ? $data['conveyance_allowance'] : 0;
            $payslipDataObj->car_allowance = !empty($data['car_allowance']) ? $data['car_allowance'] : 0;
            $payslipDataObj->medical_allowance = !empty($data['medical_allowance']) ? $data['medical_allowance'] : 0;
            $payslipDataObj->food_allowance = !empty($data['food_allowance']) ? $data['food_allowance'] : 0;
            $payslipDataObj->special_allowance = !empty($data['special_allowance']) ? $data['special_allowance'] : 0;
            $payslipDataObj->gross_salary = !empty($data['gross_salary']) ? $data['gross_salary'] : 0;
            $payslipDataObj->pf_12_employee = !empty($data['pf_12_employee']) ? $data['pf_12_employee'] : 0;
            $payslipDataObj->esi_1_75_employee = !empty($data['esi_1_75_employee']) ? $data['esi_1_75_employee'] : 0;
            $payslipDataObj->vpf = !empty($data['vpf']) ? $data['vpf'] : 0;
            $payslipDataObj->professional_tax = !empty($data['professional_tax']) ? $data['professional_tax'] : 0;
            $payslipDataObj->food_deduction = !empty($data['food_deduction']) ? $data['food_deduction'] : 0;
            $payslipDataObj->medical_insurance = !empty($data['medical_insurance']) ? $data['medical_insurance'] : 0;
            $payslipDataObj->tds = !empty($data['tds']) ? $data['tds'] : 0;
            $payslipDataObj->loan = !empty($data['loan']) ? $data['loan'] : 0;
            $payslipDataObj->total_deduction_month = !empty($data['total_deduction_month']) ? $data['total_deduction_month'] : 0;
            $payslipDataObj->net_salary_month = !empty($data['net_salary_month']) ? $data['net_salary_month'] : 0;
            $payslipDataObj->arrear_salary = !empty($data['arrear_salary']) ? $data['arrear_salary'] : 0;
            $payslipDataObj->annual_bonus = !empty($data['annual_bonus']) ? $data['annual_bonus'] : 0;
            $payslipDataObj->qtr_variable_bonus = !empty($data['qtr_variable_bonus']) ? $data['qtr_variable_bonus'] : 0;
            $payslipDataObj->performance_bonus = !empty($data['performance_bonus']) ? $data['performance_bonus'] : 0;
            $payslipDataObj->non_cash_incentive = !empty($data['non_cash_incentive']) ? $data['non_cash_incentive'] : 0;
            $payslipDataObj->gross_earnings = !empty($data['gross_earnings']) ? $data['gross_earnings'] : 0;
            $payslipDataObj->amount_payable = !empty($data['amount_payable']) ? $data['amount_payable'] : 0;
            $payslipDataObj->sl_op_bal = !empty($data['sl_op_bal']) ? $data['sl_op_bal'] : 0;
            $payslipDataObj->cl_op_bal = !empty($data['cl_op_bal']) ? $data['cl_op_bal'] : 0;
            $payslipDataObj->pl_op_bal = !empty($data['pl_op_bal']) ? $data['pl_op_bal'] : 0;
            $payslipDataObj->cme_sl = !empty($data['cme_sl']) ? $data['cme_sl'] : 0;
            $payslipDataObj->cme_cl = !empty($data['cme_cl']) ? $data['cme_cl'] : 0;
            $payslipDataObj->cme_pl = !empty($data['cme_pl']) ? $data['cme_pl'] : 0;
            $payslipDataObj->sl_availed = !empty($data['sl_availed']) ? $data['sl_availed'] : 0;
            $payslipDataObj->cl_availed = !empty($data['cl_availed']) ? $data['cl_availed'] : 0;
            $payslipDataObj->pl_availed = !empty($data['pl_availed']) ? $data['pl_availed'] : 0;
            $payslipDataObj->bal_cf_sl = !empty($data['bal_cf_sl']) ? $data['bal_cf_sl'] : 0;
            $payslipDataObj->bal_cf_cl = !empty($data['bal_cf_cl']) ? $data['bal_cf_cl'] : 0;
            $payslipDataObj->bal_cf_pl = !empty($data['bal_cf_pl']) ? $data['bal_cf_pl'] : 0;
            $payslipDataObj->advance_salary = !empty($data['advance_salary']) ? $data['advance_salary'] : 0;
            $payslipDataObj->lta = !empty($data['lta']) ? $data['lta'] : 0;
            if (!$payslipDataObj->save()) {
                $response['status'] = false;
                $response['log'][] = 'User Data not saved';
                return $response;
            }
            
            if ( $payslipDataObj->loan > 0 )
            {
                event(new LoanEmiPaid($payslipDataObj));
            }

        } catch (\Exception $e) {
            AccessLog::accessLog(null, 'App\Models', 'Product', 'saveData', 'catch-block', $e->getMessage());
        }
        return $response;
    }
    public function getNetSalaryWithAdvanceAttribute()
    {
        $net = $this->net_salary_month;
        $advance = $this->advance_salary;
        if(!empty($net) && !empty($advance) && $net != "-" && $advance != "-") {
            $net = str_replace( ",", "", $net);
            $advance = str_replace( ",", "", $advance);
            $net = $net + $advance;
            for($i=3;$i<strlen((string)$net);$i=$i+3)
            {
                $net = substr_replace($net,',' ,-$i, 0);
            }
            return $net;
        } else return 0;
    }

}
