<?php

namespace App\Models\Admin;

use App\Models\BaseModel;

class BillingScheduleReminder extends BaseModel
{

    public function scopeBuildQuery($query)
    {
        return $query;
    }
    public function project()
    {
        return $this->hasOne('App\Models\Admin\Project', 'id', 'project_id');
    }
}
