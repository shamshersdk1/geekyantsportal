<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserEducation extends Model
{
    use SoftDeletes;

    public function degree()
    {
        return $this->belongsTo('App\Models\Admin\Education', 'degree_id', 'id');
    }

    public static function updateStatus($visibility, $id)
    {
        $res['status'] = false;
        $res['message'] = "error";

        $educationObj = UserEducation::where('id', $id)->first();

        if ($educationObj) {
            $educationObj->visibility = $visibility;
        } else {
            $res['message'] = "education item not found";
            return $res;
        }

        if ($educationObj->save()) {
            return true;
        } else {
            $res['message'] = $educationObj->getErrors();
            return $res;
        }
        return $res;

    }
}
