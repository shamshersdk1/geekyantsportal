<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CmsProjectUser extends Model
{
    use SoftDeletes;

    protected $table = 'cms_project_users';

    public function userProject()
    {
        return $this->belongsTo('App\Models\Admin\CmsProject', 'cms_project_id', 'id');
    }

    public static function saveData($data)
    {
        $res['status'] = false;
        $res['message'] = null;
        $proObj = CmsProject::where('project_title', $data['selectedProject']['project_title'])->first();
        $cmsObj = new CmsProjectUser();
        $cmsObj->user_id = $data['user_id'];
        $cmsObj->cms_project_id = $proObj->id;
        if (!$cmsObj->save()) {
            $res['status'] = true;
            $res['message'] = $cmsObj->getErrors();
            return $res;
        }
        $res['id'] = $cmsObj->id;
        return $res;
    }

}
