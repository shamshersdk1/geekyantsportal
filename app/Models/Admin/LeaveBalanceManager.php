<?php

namespace App\Models\Admin;

use App\Models\BaseModel;
use App\Models\Leave\UserLeaveTransaction;
use DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class LeaveBalanceManager extends BaseModel
{

    public $timestamps = true;
    use SoftDeletes;

    public function leaveType()
    {
        return $this->belongsTo('App\Models\Admin\LeaveType', 'leave_type_id', 'id');
    }

    public static function saveData($data, $monthId)
    {
        $response['status'] = false;
        $response['message'] = '';
        $leaveBalanceObj = new LeaveBalanceManager();

        $leaveBalanceObj->user_id = $data['user_id'];
        $leaveBalanceObj->month_id = $monthId;
        $leaveBalanceObj->leave_type_id = $data['leave_type_id'];
        $leaveBalanceObj->days = $data['days'];
        $leaveBalanceObj->type = $data['type'];
        $leaveBalanceObj->balanced_by = $data['balanced_by'];
        $leaveBalanceObj->reason = $data['reason'];

        DB::beginTransaction();
        try {
            if (!$leaveBalanceObj->save()) {
                throw new Exception('Leave balance table not updated');
            }
            $data['reference_type'] = 'App\Models\Admin\LeaveBalanceManager';
            $data['reference_id'] = $leaveBalanceObj->id;
            $data['date'] = date('Y-m-d');
            $transObj = UserLeaveTransaction::saveData($data);

            if (!$transObj) {
                throw new Exception('User leave transaction table not updated');
            }
            DB::commit();
            $response['status'] = true;
            $response['message'] = 'Updated successfully';

            // return Redirect::back(); //('/admin/leave-section/leave-balance-manager')->with('message', 'leave balanced succesfully');

        } catch (Exception $e) {
            DB::rollback();
            $response['status'] = false;
            $response['message'] = $e;
            // return redirect::to('/admin/leave-section/leave-balance-manager')->withErrors('leave not balanced');
        }
        return $response;
    }

}
