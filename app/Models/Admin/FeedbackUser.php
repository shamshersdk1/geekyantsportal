<?php namespace App\Models\Admin;

use App\Models\BaseModel;
use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Comment;


class FeedbackUser extends BaseModel
{
	protected $table = 'feedback_users';
    public $timestamps = true;
    protected $appends = ['comments','rating_comments','percentage','reviewer_percentage','is_private'];
	use SoftDeletes;
    
	public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
	}
	public function reviewer()
    {
        return $this->hasOne('App\Models\User', 'id', 'reviewer_id');
    }
    public function project()
    {
        return $this->hasOne('App\Models\Admin\Project', 'id', 'project_id');
    }
    public function feedbackMonth()
    {
        return $this->hasOne('App\Models\Admin\FeedbackMonth', 'id', 'feedback_month_id');
    }
    public function roleType()
    {
        return $this->hasOne('App\Models\Admin\Role', 'id', 'reviewer_type');
    }
    public function getRmAttribute($value)
    {
        $feedbackUsers = FeedbackUser::with('reviewer')->where('user_id', $this->user_id)->where('reviewer_type',3)->first();
        return $feedbackUsers;
    }
    public function getTlsAttribute($value)
    {
        $userId = $this->user_id;
        $feedbackUsers = FeedbackUser::with('reviewer','project')->where('user_id', $this->user_id)->where('reviewer_type',4)->groupBy('reviewer_id')->groupBy('project_id')->get();
        return $feedbackUsers;
    }
    public function getProjectReviewAttribute()
    {

        $feedbackUsers = FeedbackUser::where('user_id', $this->user_id)->where('reviewer_id',$this->reviewer_id)->where('reviewer_type',4)->where('project_id', $this->project_id)->where('feedback_month_id',$this->feedback_month_id)->get();
        return $feedbackUsers;
        //return $feedbackUsers->toArray();
        //return $this->attributes['project_review'] == $feedbackUsers;
    }
    
    public function getPercentageAttribute() {
        $totalCount = FeedbackUser::where('reviewer_id',$this->reviewer_id)->where('feedback_month_id',$this->feedback_month_id)->where('user_id',$this->user_id)->get()->count();

        $pendingCount = FeedbackUser::where('reviewer_id',$this->reviewer_id)->where('feedback_month_id',$this->feedback_month_id)->where('user_id',$this->user_id)
                      ->where('status','pending')->get()->count();
        $percentage =  ($totalCount - $pendingCount) / $totalCount * 100;
        return round($percentage,2);

    }
    public function getReviewerPercentageAttribute() {
        
        $totalCount = FeedbackUser::where('reviewer_type',$this->reviewer_type)->where('reviewer_id',$this->reviewer_id)->where('user_id',$this->user_id)->where('feedback_month_id',$this->feedback_month_id)->get()->count();


        $pendingCount = FeedbackUser::where('reviewer_type',$this->reviewer_type)->where('reviewer_id',$this->reviewer_id)->where('feedback_month_id',$this->feedback_month_id)->where('user_id',$this->user_id)
                      ->where('status','pending')->get()->count();
        $percentage = ($totalCount - $pendingCount) / $totalCount * 100;       
        return round($percentage,2);
    }
    
    public function getCommentsAttribute()
    {
        return Comment::where('commentable_type','App\Models\Admin\FeedbackUser\General')->where('commentable_id',$this->id)->pluck('message')->first();
	}
	public function getRatingCommentsAttribute()
    {
        return Comment::where('commentable_type','App\Models\Admin\FeedbackUser\Restricted')->where('commentable_id',$this->id)->pluck('message')->first();
    }
    public function getIsPrivateAttribute()
    {
        return Comment::where('commentable_type','App\Models\Admin\FeedbackUser\Restricted')->where('commentable_id',$this->id)->pluck('is_private')->first();
    }

    public static function getUsersSummary($month_id){
         $usersFeedbacks = FeedbackUser::where('feedback_month_id',$month_id)->get();
         $output = [];
         foreach($usersFeedbacks as $userfeedback){
            if(!isset($output[$userfeedback['user_id']])){
                $output[$userfeedback['user_id']] = array();
                
            }
            $output[$userfeedback['user_id']][] = $userfeedback;
         }
         return $output;
    }

}
