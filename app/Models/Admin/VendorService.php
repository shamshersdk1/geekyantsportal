<?php namespace App\Models\Admin;

use App\Models\BaseModel;
use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class VendorService extends BaseModel
{

	use ValidatingTrait;

	protected $table = 'vendor_services';
	public $timestamps = false;

	private $rules = array(
			'vendor_id'  => 'required'
	    );

	public function vendor()
	{
		return $this->belongsTo('App\Models\Admin\Vendor','vendor_id','id');
	}
   	public function vendorServicesName()
   	{
       return $this->belongsTo('App\Models\Admin\VendorServicesName','vendor_services_name_id','id');
	}
}
