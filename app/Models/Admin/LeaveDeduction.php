<?php namespace App\Models\Admin;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class LeaveDeduction extends BaseModel
{

	protected $table = 'leave_deductions';
	public $timestamps = true;
	use SoftDeletes;
	
	public function scopeBuildQuery($query)
	{
		return $query;
	}
	
	public function leave()
	{
		return $this->belongsTo('App\Models\Admin\Leave', 'leave_id', 'id');
	}

}
