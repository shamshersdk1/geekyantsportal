<?php

namespace App\Models\Admin;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\BaseModel;

use Auth;
use DB;
use Config;

class PayslipDataMaster extends BaseModel
{
    protected $dates = ['deleted_at'];
    protected $table = 'payslip_data_master';
    public $timestamps = true;
    use SoftDeletes;

    public function user() {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
    public function payslipDataMonth()
    {
        return $this->hasOne('App\Models\Admin\PayslipDataMonth', 'id', 'payslip_data_month_id');
    }
    public function createdBy() {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }

}
