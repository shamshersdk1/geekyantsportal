<?php

namespace App\Models\Admin;

use App\Models\Admin\TechTalkBonus;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TechTalkBonusesUser extends Model
{
 protected $table = 'tech_talk_bonuses_user';
 use SoftDeletes;

 public function user()
 {
  return $this->belongsTo('App\Models\User', 'user_id', 'id');
 }
//  public function techtalk()
 //   {
 //    return $this->belongsTo('App\Models\Admin\TechTalkBonus', 'tech_talk_id', 'id');
 //   }
 public static function saveTechTalkData($req)
 {
  $response['status']  = false;
  $response['message'] = '';

  DB::beginTransaction();
  try {
   $techTalkBonus             = new TechTalkBonus();
   $techTalkBonus->topic      = $req['topic'];
   $techTalkBonus->status     = $req['status'];
   $techTalkBonus->date       = $req['tech_talk_date'];
   $techTalkBonus->notes      = $req['notes'];
   $techTalkBonus->created_by = $req['created_by'];

   if (!$techTalkBonus->save()) {
    throw new Exception('Tech Talk Bonus Not updated');
   }
   $techTalkBonusUser = new self();

   $techTalkId = $req['user_id'];

   if (count($techTalkId > 0)) {
    foreach ($techTalkId as $id) {
     $techTalkBonusUser               = new self();
     $techTalkBonusUser->tech_talk_id = $techTalkBonus->id;
     $techTalkBonusUser->user_id      = $id;

     if (!$techTalkBonusUser->save()) {
      throw new Exception('Tech Talk Bonus User Not updated');
     }
    }
   }

   DB::commit();
   $response['status']  = true;
   $response['message'] = 'Tech Talk Bonus Applied';

  } catch (Exception $e) {
   DB::rollback();
   $response['status']  = false;
   $response['message'] = $e;
  }
  return $response;
 }
}
