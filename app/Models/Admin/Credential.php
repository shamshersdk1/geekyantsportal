<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;
use App\Models\Admin\CredentialSharing;
use App\Models\User;

class Credential extends Model
{

	use ValidatingTrait;
	protected $table = 'credentials';
	public $timestamps = false;

	private $rules = array(
			'name' => 'required',
			'credential_category_id' => 'required',
			'json_data' => 'required'
		);
		
	public function createdBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }

	public function credentialCategory() {
        return $this->belongsTo('App\Models\Admin\CredentialCategory', 'credential_category_id', 'id');
    }
    public function sharedUsers(){
    	return $this->hasMany('App\Models\Admin\CredentialSharing','id','credential_id');
    }
	public static function getAppTemplate()
	{
		$var = 
		[
			['key' => "login_url",
			'title' => "Login URL",
			'value' => ""
			],
			['key' => "user_name",
			'title' => "User Name",
			'value' => ""
			],
			['key' => "password",
			'title' => "Password",
			'value' => ""
			]
		];
		$var = json_encode($var);
		return $var;
	}
	public static function getFormattedRecords()
	{
			$credentials = Credential::all();
			$credentials_array = [];
			foreach( $credentials as $credential )
			{
					$credentials_array_row = [];
					$shared_users = [];
					$shared_creds = CredentialSharing::where('credential_id',$credential->id)->get();
					foreach($shared_creds as $creds  )
					{
						$shared_users[] =   $creds->user_id;

					}
					$credentials_array_row['id'] = $credential->id;
					$credentials_array_row['credential_category_id'] = $credential->credential_category_id;
					$credentials_array_row['name'] = $credential->name;
					$details = json_decode($credential->json_data,true);
					$credentials_array_row['details'] = $details;
					$credentials_array_row['shared_users'] = User::whereIn('id',$shared_users)->get()->toArray();
					$credentials_array_row['created_by'] = $credential->createdBy ? $credential->createdBy->name : null;
					$credentials_array[] = $credentials_array_row;
			}
			return $credentials_array;
	}
	public static function getSharedRecords($user_id)
	{
			$credentials_array = [];
			$shared_credentials = CredentialSharing::where('user_id',$user_id)->get();
			$shared_credentials_array = [];
			foreach( $shared_credentials as $shared_credential )
			{
				$shared_credentials_array[] = $shared_credential->credential_id;
			}
			$credentials = Credential::whereIn('id',$shared_credentials_array)->get();
			foreach( $credentials as $credential )
			{
					$credentials_array_row = [];
					$shared_users = [];
					$shared_creds = CredentialSharing::where('credential_id',$credential->id)->get();
					foreach($shared_creds as $creds  )
					{
						$shared_users[] = $creds->user_id;
					}
					$credentials_array_row['id'] = $credential->id;
					$credentials_array_row['credential_category_id'] = $credential->credential_category_id;
					$credentials_array_row['name'] = $credential->name;
					$details = json_decode($credential->json_data,true);
					$credentials_array_row['details'] = $details;
					$credentials_array_row['shared_users'] = $shared_users;
					$credentials_array[] = $credentials_array_row;
			}
			return $credentials_array;
	}

}
