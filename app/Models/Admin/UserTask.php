<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\Admin\Calendar;
use App\Models\Admin\Project;
use App\Models\Admin\ProjectResource;
use App\Models\Admin\SharedTask;
use App\Models\User;

use Auth;
use Input;

class UserTask extends Model
{

    use SoftDeletes;

    public function user()
    {
		return $this->hasOne('App\Models\User');	
    }

    public function group()
    {
        return $this->belongsTo('App\Models\TaskGroup');
    }
    
    public static function getData()
    {
        $response = ['status' => false, 'message' => "", 'data' => null];
        $data = [];
        $exclude = [];
        if(Auth::check()) {    
            $manager = Auth::user();
            $shared = SharedTask::with('user', 'owner')->where('is_removed', 0)->where('owner_id', $manager->id)->get();
            $start_date = Input::get('start_date');
            $end_date = Input::get('end_date');
            $my_users_id = [];
            if(is_null($start_date)) {
                $start_date = date('Y-m-d');
                $day = date('N', strtotime($start_date));
                $start_date = date('Y-m-d', strtotime("-".($day-1)." days", strtotime($start_date)));
            }
            $day = date('N', strtotime($start_date));
            if(is_null($end_date)) {
                $end_date = date('Y-m-d', strtotime('+13 days', strtotime($start_date)));
            }
            $diff = (strtotime($end_date) - strtotime($start_date))/(60*60*24);
            if($diff < 0) {
                $response['message'] = "Start date must be greater than or equal to end date";
                return $response;
            }
            $dates = array_fill(0, $diff+1, "available");
            $i = 7;
            if($day == 7) {
                $dates[0] = "weekend";
                $i = 14;
            }
            for(; $i-$day-1<=$diff; $i = $i+7)
            {
                $dates[$i-$day-1] = "weekend";
                $dates[$i-$day] = "weekend";
            }
            if(isset($dates[$diff+1])) {
                unset($dates[$diff+1]);
            }
            $holidays = $holidays = Calendar::whereDate('date', '>=', $start_date)->whereDate('date', '<=', $end_date)->pluck('date')->toArray();
            foreach($holidays as $holiday)
            {
                $index = (strtotime($holiday) - strtotime($start_date))/(60*60*24);
                $dates[$index] = "weekend";
            }

            $tasks = array_fill(0, $diff+1, "");
            
            $exclude[] = $manager->id;
            $temp = UserTask::getUserArray($dates, $tasks, $start_date, $end_date, $manager, $manager, $manager, 0);
            $data[] = $temp;
            if($manager->children()->count()) {
                $users = $manager->children()->with('leaves')->where('is_active', 1)->get();
                foreach($users as $user)
                {
                    $my_users_id[] = $user->id;
                    $exclude[] = $user->id;
                    $data[] = UserTask::getUserArray($dates, $tasks, $start_date, $end_date, $user, $manager, $manager, 0);
                }
            }
            if(!empty($shared)) {
                foreach($shared as $item)
                {
                    $user = $item->user;
                    $creator = $item->creator;
                    if(!$user || !$creator) {
                        continue;
                    }
                    $exclude[] = $user->id;
                    $data[] = UserTask::getUserArray($dates, $tasks, $start_date, $end_date, $user, $manager, $creator, 0);
                }    
            }
        }
        $date_code = $dates;
        $dates = [];
        $count = 0;
        while($start_date <= $end_date)
        {
            if($start_date == date("Y-m-d")) {
                $dates[] = "today";
                $date_code[$count] = "today";
            } else {
                $dates[] = date('Y-m-d' ,strtotime($start_date));
            }
            $start_date = date('Y-m-d', strtotime('+1 day', strtotime($start_date)));
            $count++;
        }
        $response = ['status' => true, 'message' => "", 'data' => $data, 'dates' => $dates, 'date_code' => $date_code, 'exclude' => $exclude];
        return $response;
    }
    
    public static function saveData($date, $developer_id, $text, $is_note)
    {
        $response = ['status' => false, 'message' => "Invalid request"];
        if(Auth::check()) {
            $developer = User::find($developer_id);
            if(empty($developer)) {
                return $response;
            }
            $allowed = SharedTask::where('user_id', $developer_id)->where('owner_id', Auth::id())->count();
            if($allowed == 0 && $developer->parent_id != Auth::id() && $developer_id != Auth::id()) {
                $response['message'] = "Add the user first";
                return $response;
            }
            if($is_note) {
                $task = UserTask::where('is_note', true)->where('user_id', $developer_id)->first();
                if($task) {
                    $task->task = $text;
                    $task->created_by = Auth::id();
                    $task->save();
                    $response = ['status' => true, 'message' => "Updated Successfully"];
                } else {
                    $task = new UserTask();
                    $task->user_id = $developer->id;
                    $task->task = $text;
                    $task->is_note = true;
                    $task->date = null;
                    $task->created_by = Auth::id();
                    $task->save();
                    $response = ['status' => true, 'message' => "Saved Successfully"];
                }
            } else {
                $task = UserTask::where('date', $date)->where('user_id', $developer_id)->first();
                if($task) {
                    $task->task = $text;
                    $task->created_by = Auth::id();
                    $task->save();
                    $response = ['status' => true, 'message' => "Updated Successfully"];
                } else {
                    $task = new UserTask();
                    $task->user_id = $developer->id;
                    $task->task = $text;
                    $task->is_note = false;
                    $task->date = $date;
                    $task->created_by = Auth::id();
                    $task->save();
                    $response = ['status' => true, 'message' => "Saved Successfully"];
                }
            }
        }
        return $response;
    }

    public static function getUserArray($user_dates, $user_tasks, $start_date, $end_date, $user, $manager, $creator, $is_removed)
    {
        $leaves = $user->leaves()->where('start_date', '<=', $end_date)->where('end_date', '>=', $start_date)->where('status', 'approved')->get();
        $leave_indices = [];
        $diff = (strtotime($end_date) - strtotime($start_date))/(60*60*24);
        foreach($leaves as $leave)
        {
            $start_index = ($leave->start_date <= $start_date) ? 0 : ((strtotime($leave->start_date) - strtotime($start_date))/(60*60*24));
            $end_index = ($leave->end_date >= $end_date) ? $diff : ((strtotime($leave->end_date) - strtotime($start_date))/(60*60*24));
            for(;$start_index <= $end_index;$start_index++)
            {
                $leave_indices[] = $start_index;
            }
        }
        $leave_indices = array_unique($leave_indices);
        foreach($leave_indices as $index)
        {
            $user_dates[$index] = "on-leave";
        }
        $existing = UserTask::where('is_note', 0)->where('user_id', $user->id)->where('date', '>=', $start_date)->where('date', '<=', $end_date)->where('is_removed', $is_removed)->get();
        foreach($existing as $task)
        {
            $index = (strtotime($task->date) - strtotime($start_date))/(60*60*24);
            $user_tasks[$index] = $task->task;
        }
        $notes = UserTask::where('is_note', true)->where('user_id', $user->id)->where('date', null)->where('is_removed', $is_removed)->first();
        if($notes) {
            $notes = $notes->task;
        } else {
            $notes = "";
        }
        $is_dev = false;
        $name = ($user->id == $manager->id) ? "You" : $user->name;
        if($name == "You") {
            $is_dev = true;
        }
        $is_rm = ($user->parent_id == Auth::id()) ? true : false;
        $owner_array = SharedTask::where('user_id', $user->id)->where('owner_id', '!=', Auth::id())->where('is_removed', $is_removed)->pluck('owner_id')->toArray();
        $parent_id = is_null($user->parent_id) ? 0 : $user->parent_id;
        return ['user' => $name, 'dates' => $user_dates, 'tasks' => $user_tasks, 'user_id' => $user->id, 'owner_array' => $owner_array, 'notes' => $notes, 'creator_id' => $creator->id, 'parent_id' => $parent_id, 'is_rm' => $is_rm, 'is_removed' => $is_removed, 'is_dev' => $is_dev];
    }
}
