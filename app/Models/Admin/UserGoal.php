<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use App\Models\BaseModel;
use App\Events\Goal\GoalSet;
use App\Events\Goal\GoalConfirmed;
use App\Events\Goal\GoalFreezed;

class UserGoal extends BaseModel
{
    public function items()
    {
        return $this->hasMany('App\Models\Admin\UserGoalItem');
    }
    public function goals()
    {
        return $this->belongsToMany(
            'App\Models\Admin\Goal',
            'user_goal_items',
            'user_goal_id',
            'goal_id'
        );
    }
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public static function saveGoal($input)
    {
        $status = true;
        $message = '';
        $goal = new UserGoal();
        $goal->user_id = $input['user_id'];
        $goal->from_date = $input['from_date'];
        $goal->to_date = $input['to_date'];
        $goal->status = $input['status'];
        if(!$goal->save()) {
            $status =  false; 
            $message = $item->getMessage();
        }
        if( $goal->status == 'submitted' )
        {
            event(new GoalSet($goal->id));
        }
        $response['status'] = $status;
        $response['message'] = $message;
        $response['goalObj'] = $goal;
        return $response;
    }

    public static function updateOverallFeedback($goal_id, $overall_feedback)
    {
        $response = ["status" => true, "message" => ""];
        $goal = UserGoal::find($goal_id);
        $goal->overall_feedback = $overall_feedback;
        if(!$goal->save()) {
            $response = ["status" => false, "message" => $item->getMessage()];
        }
        else
        {
            $response['message'] = $goal->id;
        }
        return $response;
    }

    public static function updateStatus($goal_id, $status)
    {
        $response = ["status" => true, "message" => ""];
        $goal = UserGoal::find($goal_id);
        $goal->status = $status;
        if(!$goal->save()) {
            $response = ["status" => false, "message" => $item->getMessage()];
        }
        else
        {
            $response['message'] = $goal->id;
        }
        if( $goal->status == 'submitted' )
        {
            event(new GoalSet($goal->id));
        }
        if( $goal->status == 'confirmed' )
        {
            event(new GoalConfirmed($goal->id));
        }
        if( $goal->status == 'running' )
        {
            event(new GoalFreezed($goal->id));
        }
        return $response;
    }
}
