<?php namespace App\Models\Admin;

use App\Models\BaseModel;
use App\Models\Activity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;
use App\Models\Admin\Department;
use App\Services\IncidentService;
use App\Events\Incident\IncidentCreated;
use App\Events\Incident\IncidentClosed;
use App\Events\Incident\IncidentReopened;
use App\Events\Incident\IncidentVerifiedClosed;

class Incident extends BaseModel
{

    use ValidatingTrait;

    protected $table = 'incidents';
    public $timestamps = true;
    use SoftDeletes;
    protected $fillable = ['subject', 'department_id'];
    protected $appends = ['shared_with','last_updated'];

    private $rules = array(
        'subject' => 'required',
        'department_id' => 'required',
    );
    public function activities()
    {
        return $this->morphMany('App\Models\Activity', 'reference');
    }
    public function assignee()
    {
        return $this->hasOne('App\Models\User', 'id', 'assigned_to');
    }
    public function raisedBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'raised_by');
    }
    public function department()
    {
        return $this->hasOne('App\Models\Admin\Department', 'id', 'department_id');
    }
    public function comments()
    {
        return $this->morphMany('App\Models\Comment', 'commentable');
    }
    public function incidentUsers()
    {
        return $this->hasMany('App\Models\Admin\IncidentUserAccess', 'incident_id', 'id');
    }
    public function getSharedWithAttribute()
    {
		$data = [];
		$string = '';
		if ( $this->incidentUsers )
		{
			foreach ( $this->incidentUsers as $shared_user )
			{
                if ( $shared_user->user ){
                    $data[] = $shared_user->user->name;
                }
				
			}
			$string = implode(",",$data);
		}
		return $string;
    }

    public function getLastUpdatedAttribute()
    {
		$activityObj = Activity::with('user')->where('reference_type','App\Models\Admin\Incident')->where('reference_id',$this->id)->orderBy('created_at','DESC')->first();
		return $activityObj;
    }
    public static function saveData($data)
    {
        $assignedTo = null;
        $userObj = \Auth::User();

        if(empty($data['department_id']) || empty($data['subject']) )
            return false;

        $departmentId = !empty($data['department_id']) ? $data['department_id'] : null;

        if(!$departmentId)
            return false;

        $departmentObj = Department::find($departmentId);
        if($departmentObj) {
            $assignedTo = $departmentObj->first_contact;

        }

        if(!$assignedTo)
            return false;
        
        $obj = new Incident;
        $obj->department_id = $departmentId;
        $obj->subject = !empty($data['subject']) ? $data['subject'] : null;
        $obj->description = !empty($data['description']) ? $data['description'] : null;
        $obj->status = "open";
        $obj->assigned_to = $assignedTo;
        $obj->raised_by = $userObj->id;

        if (!$obj->save()) {
            $errors = $obj->getErrors();
            return false;
        }
        $obj->addActivity('open');
        // event add activity log
        $response = IncidentService::saveSharedUsers($obj->id, $data['shared_users']);
        event(new IncidentCreated($obj->id));
        // dispatch a job to post slack notification
        return true;
    }

    public static function updateData($id, $data)
    {
        $assignedTo = null;
        $userObj = \Auth::User();

        if(empty($data['department_id']) || empty($data['subject']) )
            return false;

        $departmentId = !empty($data['department_id']) ? $data['department_id'] : null;

        if(!$departmentId)
            return false;

        $departmentObj = Department::find($departmentId);
        if($departmentObj) {
            $assignedTo = $departmentObj->first_contact;
        }

        if(!$assignedTo)
            return false;
        
        $obj = Incident::find($id);
        $obj->department_id = $departmentId;
        $obj->subject = !empty($data['subject']) ? $data['subject'] : null;
        $obj->description = !empty($data['description']) ? $data['description'] : null;
        $obj->assigned_to = $assignedTo;
        if (!$obj->save()) {
            $errors = $obj->getErrors();
            return false;
        }

        // $obj->addActivity('open');
        $response = IncidentService::updateSharedUsers($obj->id, $data['shared_users']);
        // dispatch a job to post slack notification
        return true;
    }
    public static function updateStatus($id, $status)
    {
        $assignedTo = null;
        $userObj = \Auth::User();

        $obj = Incident::find($id);
        $obj->status = $status;
        if (!$obj->save()) {
            $errors = $obj->getErrors();
            return false;
        }
        $obj->addActivity($status);
        switch($status)
        {
            case 'reopen':
                event(new IncidentReopened($obj->id));
                break;
            
            case 'closed':
                event(new IncidentClosed($obj->id));
                break;
            
            case 'verified_closed':
                event(new IncidentVerifiedClosed($obj->id));
                break;
            default:
                break;
        }
        // dispatch a job to post slack notification
        return true;
    }
    public function addActivity($action, $actionDetail = NULL){
        
        $userObj = \Auth::User();
        $userId=null;

        if($userObj) {
            $userId = $userObj->id;
        }
        $data['action'] = $action;
        $data['reference_id'] = $this->id;
        $data['reference_type'] = 'App\Models\Admin\Incident';
        $data['action_details'] = [$actionDetail];
        $data['user_id'] = $userId;

        Activity::storeData($data);
        
        
        // if($key == 'incident_open') {

        //     $details = [
        //         {
        //             "key":"created_by",
        //             "value":"Varun",
        //         },
        //         {
        //             "key":"received_quantity",
        //             "value":"1.0","type":"number"}
        //         ,{
        //             "key":"item_condition",
        //             "value":"item_still_in_original_packaging",
        //             "type":"status"
        //         },
        //         {
        //             "key":"reason_received",
        //             "value":"item_and_packaging_damaged",
        //             "type":"status"
        //         }
        //     ];
        // }


        // $id = $this->id;
        // $action_details = ['key'   => $key, 'value' => $id];

        //     $data['action']            = 'in_transit';
        //     $data['action_details']    = json_encode($action_details);
        //     $shipped                   = Log::storeData($data);


        // Activity::storeData($data)
    }
}
