<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

use Input;

class SlackAccessLog extends Model
{
    public static function saveData($from, $to, $requestBody, $raw_response)
   {
        $log = new SlackAccessLog();
        $log->from = $from;
        $log->to = $to;
        $log->request = $requestBody;
        $log->raw_response = $raw_response;
        $log->save();
    }
    public static function filter()
    {
        $logs = SlackAccessLog::orderBy('id', 'DESC');
        if (Input::get('created_at')!='' ) {
            $date = date_to_yyyymmdd(Input::get('created_at'));
            $logs=$logs->whereDate('created_at', '=', $date);
        }         
        $logs = $logs->paginate(20);
        return $logs;
    }
}
