<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use App\Models\BaseModel;

use DB;

class Tag extends BaseModel
{
    public function goals()
    {
        return $this->belongsToMany('App\Models\Admin\Goal', 'goal_tag', 'tag_id', 'goal_id');
    }

    public function user_goal_items()
    {
        return $this->belongsToMany('App\Models\Admin\UserGoalItem', 'tag_user_goal_item', 'tag_id', 'user_goal_item_id');
    }
    // public static function attachTags($tags, $goal_id)
    // {
    //     foreach($tags as $tag)
    //     {
    //         $tag = trim($tag);
    //         $db_tag = Tag::where('name', $tag)->first();
    //         if($db_tag) {
    //             $db_tag->goals()->attach($goal_id);
    //         } else {
    //             $tag_id = Tag::saveTag($tag);
    //             if(is_null($tag_id)) {
    //                 continue;
    //             } else {
    //                 DB::table('goal_tag')->insert(['goal_id' => $goal_id, 'tag_id' => $tag_id]);
    //             }
    //         }
    //     }
    // }

    public static function saveTag($name)
    {
        $id = null;
        $tag = new Tag();
        $tag->name = $name;
        if($tag->save()) {
            $id = $tag->id;
        }
        return $id;
    }

    // public static function detachTags($tags, $goal_id)
    // {
    //     foreach($tags as $tag)
    //     {
    //         $tag = trim($tag);
    //         $db_tag = Tag::where('name', $tag)->first();
    //         if($db_tag) {
    //             $db_tag->goals()->detach($goal_id);
    //         }
    //     }
    // }
}
