<?php namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class CmsTechnologyDeveloper extends Model 
{

	use ValidatingTrait;
	
	protected $table = 'cms_technology_developers';
	public $timestamps = true;
	use SoftDeletes;

	private $rules = array(
	        'cms_technology_id' => 'required',
	        'user_id'  => 'required',
	        'status' => 'required',
	    );

	public function user() {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
	} 
	public function cms_developer()
	{
		return $this->hasOne('App\Models\Admin\CmsDeveloper', 'id', 'cms_technology_id');
	}
	public function image()
	{
		return $this->hasOne('App\Models\Admin\File', 'reference_id', 'id');
	}
}
