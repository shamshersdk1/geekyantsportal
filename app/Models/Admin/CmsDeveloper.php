<?php namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class CmsDeveloper extends Model {

	use ValidatingTrait;
	
	protected $table = 'cms_developers';
	public $timestamps = true;
	use SoftDeletes;

	private $rules = array(
	        'name' => 'required',
	        'title'  => 'required',
	        'description' => 'required',
            'status' => 'required',
			'slug' => 'required',
	    );

	public static function checkSlug($slug)
	{
		$flag = 0;
		$cmsDevelopers = CmsDeveloper::all();
		foreach ( $cmsDevelopers as $cmsDeveloper )
		{
			if ( $slug == $cmsDeveloper->slug )
			{
				$flag = 1;
			}
		}
		if ( $flag == 1 )
			return true;
		else
			return false;
	}
}
