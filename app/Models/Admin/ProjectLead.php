<?php

namespace App\Models\Admin;

use App\Models\BaseModel;
use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProjectLead extends BaseModel
{

    use ValidatingTrait;
    protected $table = 'project_leads';
    public $timestamps = true;
    use SoftDeletes;

    private $rules = array(
        'project_id' => 'required',
    );
    
    public function project()
    {
        return $this->belongsTo('App\Models\Admin\Project', 'project_id');
    }
}
