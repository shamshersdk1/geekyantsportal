<?php namespace App\Models\Admin;

use App\Models\BaseModel;
use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use App\Models\Admin\File;
use App\Services\FileService;
use App\Models\Admin;
use  App\Models\UserTechnologies;
class TechStack extends BaseModel 
{

	// use ValidatingTrait;  // Use For Validation.Basically creates a validate() method which is then used in out tech class
	
	public $timestamps = true;
	use SoftDeletes;

    public static  function saveData($input,$id){
    $tech_id=$input['tec_id'];
    
    $tech=UserTechnologies::where('user_id',$id)->where('technology_id',$tech_id)->first();

    $tech->isVisible=$input['isVisible'];

    $response['status']='false';
    $response['message']='';
    $response['data']=null;
    if(!$tech->save()){
       $response['message']='Not Saved';
       $response['data']=$tech->getErrors();
    }
    else{
        $response['status']=true;
        $response['message']='Not Saved';
        $response['data']=$tech;
    }
   return $response;


    
    }	


}
