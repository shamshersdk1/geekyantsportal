<?php namespace App\Models\Admin;

use App\Models\BaseModel;

class LeaveType extends BaseModel
{

	protected $table = 'leave_types';
	public $timestamps = true;
	
	public function scopeBuildQuery($query)
	{
		return $query;
	}
	
}
