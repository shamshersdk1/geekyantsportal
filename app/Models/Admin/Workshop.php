<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Workshop extends Model
{
    use SoftDeletes;
    // protected $table = 'workshop';

    public static function saveData($data)
    {
        $res['status'] = false;
        $res['message'] = null;
        $workshopObj = new Workshop();
        $workshopObj->user_id = $data['user_id'];
        $workshopObj->title = $data['title'];
        $workshopObj->description = $data['description'];
        if (!$workshopObj->save()) {
            $res['status'] = true;
            $res['message'] = $workshopObj->getErrors();
            return $res;
        }
        return $workshopObj;
    }
}
