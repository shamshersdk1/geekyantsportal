<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;
use App\Models\Admin\Company;
use App\Models\Admin\Project;
use App\Models\Admin\ProjectNotes;
use App\Models\Admin\ProjectFiles;
use App\Models\Admin\ProjectSprints;
use App\Models\Admin\SprintResource;
use App\Models\Admin\Technology;


class ProjectSprints extends Model
{
    use ValidatingTrait;
	protected $table = 'project_sprints';
	public $timestamps = false;
	

	private $rules = array(
		    'project_id' => 'required',
	        'title' => 'required',
	        'start_date' => 'required',
	        'end_date' => 'required',

	        // 'description'  => 'required'
	    );

	public function project() {
		return $this->hasOne('App\Models\Admin\Project', 'id', 'project_id');	
	}

	public function sprintResource() {
            return $this->hasMany('App\Models\Admin\SprintResource','sprint_id', 'id');
        }

    public function milestones() {
    	return $this->belongsToMany('App\Models\Admin\ProjectQuotationMilestone', 'milestone_sprint', 'sprint_id', 'milestone_id')->withPivot('project_id');
    }
}
