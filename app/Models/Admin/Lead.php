<?php namespace App\Models\Admin;
use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class Lead extends Model {

	
	use ValidatingTrait;
	protected $table = 'leads';
	

	private $rules = array(
			'name' => 'required',
	        'email' => 'required|email',
	        'subject' => 'required',
	        'message' => 'required',
	        'status' => 'required',
	        'source' => 'required',

	        // 'description'  => 'required'
	    );

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	// protected $fillable = ['name', 'email', 'password'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	//public
}
