<?php

namespace App\Models\Admin;

use App\Models\Admin\FinancialYear;
use App\Models\Admin\ItSaving;
use App\Models\Admin\ItSavingMonth;
use App\Models\BaseModel;
use App\Models\Month;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Admin\ItSavingMonthOther;
use App\Models\Appraisal\AppraisalComponentType;
use App\Models\Transaction;
use App\Services\Payroll\AppraisalService;
use App\Models\Admin\VpfDeduction;


class ItSavingMonth extends BaseModel
{
    //use SoftDeletes;
    protected $appends = ['total_investments','total_deductions'];

    public function getTotalDeductionsAttribute()
    {
        $amount = $this->medical_insurance_premium + $this->medical_treatment_expense + $this->educational_loan + $this->donation + $this->rent_without_receipt +
        $this->physical_disablity + $this->political_party + $this->lta;
        $amount += $this->others->where('type','other_multiple_deductions')->sum('value');
        return $amount;
    }

    public function getTotalInvestmentsAttribute()
    {
        $monthObj = Month::where('status', 'in_progress')->where('financial_year_id', $this->financial_year_id)->orderBy('year','ASC')->orderBy('month','ASC')->first();
        $pfEmployee = AppraisalComponentType::getIdByKey('pf-employee');
        $pfEmployeeAmount = 0;
        $vpfAmount = 0;
        $amount = 0;
        $pfEmployeeAmount = Transaction::getPaidTillDate($this->user_id, $this->financial_year_id, 'App\Models\Appraisal\AppraisalComponentType', $pfEmployee);
        $vpfAmount = Transaction::getPaidTillDate($this->user_id, $this->financial_year_id, 'App\Models\Admin\VpfDeduction');
        if($monthObj->status == "in_progress"){
            $appraisalList = AppraisalService::getCurrentMonthAppraisal($this->user_id, $monthObj->id);
            if ($appraisalList == null || !isset($appraisalList[0])) {
                $response['errors'] = 'No Appraisal found!';
                $response['data'] = $data;
                return $response;
            }
            $currData = AppraisalService::generateCurrentData($appraisalList, $monthObj->id, $this->user_id);

            $pfEmployeeAmount += $currData[$pfEmployee];
            $vpfAmount += VpfDeduction::getCurrMonthVpf($monthObj->id, $this->user_id);

            if($pfEmployeeAmount < 0 )
                $pfEmployeeAmount *= -1;
            if($vpfAmount < 0 )
                $vpfAmount *= -1;

        }
        $amount = $vpfAmount + $pfEmployeeAmount + $this->pf + $this->pension_scheme_1 + $this->pension_scheme_1b + $this->ppf + $this->central_pension_fund +
        $this->lic + $this->housing_loan_repayment + $this->term_deposit + $this->national_saving_scheme + $this->tax_saving + $this->children_expense;
        $amount += $this->others->where('type','other_multiple_investments')->sum('value');
        return ($amount < 150000) ? $amount : 150000;
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function others()
    {
        return $this->hasMany('App\Models\Admin\ItSavingMonthOther', 'it_saving_month_id', 'id');
    }

    public static function createRecord()
    {
        $month = date('m');
        $monthObj = Month::orderBy('year','DESC')->orderBy('month','DESC')->where('month', $month)->first();

        $year = date('Y');
        $yearObj = FinancialYear::where('year', $year)->first();
        $financialYearId = $yearObj->id;

        $savings = ItSaving::where('financial_year_id', $financialYearId)->get();

        foreach ($savings as $saving) {

            $monthlyEntry = ItSavingMonth::where('financial_year_id', $financialYearId)->where('month_id', $monthObj->id)->where('user_id', $saving->user_id)->first();
            if ($monthlyEntry == null) {
                $monthlySavingId = ItSavingMonth::createMonthlyRecord($monthObj->id, $saving);
                ItSavingMonthOther::createMonthlyRecord($saving,$monthlySavingId->id);
            }
        }
    }
    public static function createMonthlyRecord($monthId, $saving)
    {
        $monthlySaving = new ItSavingMonth;

        $monthlySaving->month_id = $monthId;
        $monthlySaving->user_id = $saving->user_id;
        $monthlySaving->financial_year_id = $saving->financial_year_id;
        $monthlySaving->rent_monthly = null;
        $monthlySaving->rent_yearly = $saving->rent_yearly;
        $monthlySaving->lta = $saving->lta;
        $monthlySaving->pf = $saving->pf;
        $monthlySaving->pension_scheme_1 = $saving->pension_scheme_1;
        $monthlySaving->pension_scheme_1b = $saving->pension_scheme_1b;
        $monthlySaving->ppf = $saving->ppf;
        $monthlySaving->central_pension_fund = $saving->central_pension_fund;
        $monthlySaving->lic = $saving->lic;
        $monthlySaving->housing_loan_repayment = $saving->housing_loan_repayment;
        $monthlySaving->term_deposit = $saving->term_deposit;
        $monthlySaving->national_saving_scheme = $saving->national_saving_scheme;
        $monthlySaving->tax_saving = $saving->tax_saving;
        $monthlySaving->children_expense = $saving->children_expense;
        $monthlySaving->other_investment = $saving->other_investment;
        $monthlySaving->other_multiple_investments = $saving->other_multiple_investments;
        $monthlySaving->medical_insurance_premium = $saving->medical_insurance_premium;
        $monthlySaving->medical_treatment_expense = $saving->medical_treatment_expense;
        $monthlySaving->educational_loan = $saving->educational_loan;
        $monthlySaving->donation = $saving->donation;
        $monthlySaving->rent_without_receipt = $saving->rent_without_receipt;
        $monthlySaving->physical_disablity = $saving->physical_disablity;
        $monthlySaving->other_deduction = $saving->other_deduction;
        $monthlySaving->other_multiple_deductions = $saving->other_multiple_deductions;
        $monthlySaving->salary_paid = $saving->salary_paid;
        $monthlySaving->tds = $saving->tds;
        $monthlySaving->agree = $saving->agree;

        $monthlySaving->save();
        return $monthlySaving;
    }

    public static function GetTableColumns()
    {

        $sql = "Select DISTINCT COLUMN_NAME  from information_schema.columns where table_name='it_saving_months' AND COlUMN_NAME NOT IN ('id','user_id','financial_year_id','month_id','status','created_at','updated_at','deleted_at')";
        $itSavingMonthComponents = \DB::select($sql);

        return $itSavingMonthComponents;
    }
}
