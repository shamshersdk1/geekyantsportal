<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Models\User;

use App\Services\LeaveService;

use Validator;

class CompOff extends Model
{
    /**
     * Relation for user
     * 
     * @return User
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
    /**
     * Validate the inputs
     * 
     * @param Request $request => the request object
     * 
     * @return $response
     */
    public static function validateCompOff(Request $request)
    {
        $response =  array('status' => false,'errors'=>"",'message' => "",'id' =>'');
        $validator = Validator::make(
            $request->all(), [
            'name' => 'required',
            'days' => 'required|integer'
            ]
        );
        if ($validator->fails()) {
            $response['errors']=$validator->errors();
            return $response;
        }
        $user=User::where('name', $request->name)->first();
        if (empty($user)) {
            $response['errors']="Enter a valid user";
            return $response;
        }
        $response['id']=$user->id;
        $response['status']=true;
        return $response;
    }
    /**
     * Save the inputs
     * 
     * @param Request $request => the request object
     * @param int     $id      => user id
     * 
     * @return $response
     */
    public static function saveCompOff(Request $request, $id)
    {
        $response =  array('status' => false,'errors'=>"",'message' => "");
        $compOff = new CompOff;
        $compOff->user_id=$id;
        $compOff->days=$request->days;
        $compOff->description=$request->note;
        $compOff->created_by=\Auth::id();
        if($compOff->save()) {
            $response['status']=true;
            return $response;
        } else {
            $response['errors'] = 'Unable to save';
            return $response;
        }
    }
    /**
     * Update the CompOff
     * 
     * @param Request $request => the request object
     * @param int     $id      => CompOff id
     * @param int     $user_id => user id
     * 
     * @return $response
     */
    public static function updateCompOff(Request $request, $id, $user_id)
    {
        $response =  array('status' => false,'errors'=>"",'message' => "");
        $compOff = CompOff::find($id);
        if($compOff) {
            $compOff->user_id=$user_id;
            $compOff->days=$request->days;
            $compOff->description=$request->note;
            if($compOff->save()) {
                $response['status']=true;
                return $response;
            } else {
                $response['errors'] = 'Unable to save';
                return $response;
            }
        } else {
            $response['errors'] = 'CompOff not found';
            return $response;
        }
    }
}
