<?php namespace App\Models\Admin;

use App\Models\BaseModel;
use Auth;
use App\Services\ReferenceService;


class ReferenceNumber extends BaseModel 
{

	protected $table = 'reference_numbers';
	public $timestamps = true;
	

	public function uploadedBy()
	{
		return $this->hasOne('App\Models\User', 'id', 'user_id');
	}
	public function documentType()
	{
		return $this->hasOne('App\Models\DocumentType', 'id', 'document_type_id');
	}
	
	public static function saveData($input)
	{
		$status = true;
		$message = '';
		$uploader = Auth::user();
		
		$referenceDetails = ReferenceService::generateReference();
		
		$referenceObj = new ReferenceNumber();
		$referenceObj->user_id = $uploader->id;
		$referenceObj->year = $referenceDetails['year'];
		$referenceObj->month = $referenceDetails['month'];
		$referenceObj->sl_no = $referenceDetails['sl_no'];
		$referenceObj->value = $referenceDetails['reference_string'];
		$referenceObj->for = $input['for'];
		$referenceObj->signed_by = $input['signed_by'];
		$referenceObj->document_type_id = $input['document_type_id'];
		$referenceObj->info = $input['info'];
		$referenceObj->status = 'new';
		if( !$referenceObj->save() )
		{
			$status = false;
			$message = $referenceObj->getErrors()->toArray();
		}
		$response['status'] = $status;
		if( $status){
			$response['id'] = $referenceObj->id;
		}
		$response['message'] = $referenceObj->value;
		
		return $response;
	}

	public static function updateData($id, $input)
	{
		$status = true;
		$message = '';
		$uploader = Auth::user();
		
		$referenceObj = ReferenceNumber::find($id);
		if( empty($referenceObj) )
		{
			$status = false;
			$message = 'Reference not found';
		}
		else{
			$referenceObj->user_id = $uploader->id;
			$referenceObj->for = $input['for'];
			$referenceObj->signed_by = $input['signed_by'];
			$referenceObj->document_type = $input['document_type'];
			$referenceObj->info = $input['info'];
			$referenceObj->status = $input['status'];
			if( !$referenceObj->save() )
			{
				$status = false;
				$message = $referenceObj->getErrors()->toArray();
			}
		}

		$response['status'] = $status;
		$response['message'] = $referenceObj->id;
		
		return $response;
	}

	public static function cancelReference($id)
	{
		$status = true;
		$message = '';
		$referenceObj = ReferenceNumber::find($id);
		if ( empty($referenceObj) )
		{
			$status = false;
			$message = 'No record found';
		}
		$referenceObj->status = 'cancelled';
		if ( !$referenceObj->save() )
		{
			$status = false;
			$message = $referenceObj->getErrors()->toArray();
		}
		
		$response['status'] = $status;
		$response['message'] = $message;
		return $response;

	}
	
}
