<?php

namespace App\Models\Admin;

use App\Models\Admin\IpAddressMapper;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserIpAddress extends BaseModel
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $table = 'user_ip_addresses';

    public function scopeBuildQuery($query)
    {
        return $query;
    }
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
    public function assignee()
    {
        return $this->belongsTo('App\Models\User', 'assigned_by', 'id');

    }
    public function ipMapper()
    {
        return $this->morphOne('App\Models\Admin\IpAddressMapper', 'reference');
    }
    public static function userFreeIp($id)
    {
        $response = [
            'result' => "",
            'status' => true,
            'message' => "success",
        ];
        $freeIp = "";

        $UserIpAddress = UserIpAddress::where('user_id', $id)->get();
        foreach ($UserIpAddress as $userIp) {
            $isUsedInMyDevice = IpAddressMapper::where('user_ip_address_id', $userIp->id)->first();
            if (!$isUsedInMyDevice) {
                $freeIp = $userIp;
                break;
            }
        }
        if (!$freeIp) {
            $response['status'] = false;
            $response['message'] = "No free Ip available. Please contact admin for ip assignment.";
        }
        $response['result'] = $freeIp;
        return $response;

    }
    public static function freeUserIpList($id)
    {
        $data = [];
        $UserIpAddress = UserIpAddress::where('user_id', $id)->get();
        foreach ($UserIpAddress as $userIp) {

            $isUsedInMyDevice = IpAddressMapper::where('user_ip_address_id', $userIp->id)->with('userIp')->first();
            if (!$isUsedInMyDevice) {
                array_push($data, $userIp);
            }
        }
        return $data;
    }

    public static function findByIpAddress($ip){
        return self::where('ip_address',$ip)->first();
    }
}
