<?php

namespace App\Models\Admin;

use App\Jobs\GoogleCalendarEvents\CreateCalendarEventJob;
use App\Jobs\LeaveNotificationMail;
use App\Jobs\LeaveRequestMail;
use App\Models\Activity;
use App\Models\Admin\LeaveType;
use App\Models\BaseModel;
use App\Models\CalendarEvent;
use App\Models\User;
use App\Services\LeaveService;
use App\Services\NewLeaveService;
use App\Services\SlackService\SlackMessageService\Reminder\Leave as SlackLeaveReminder;
use Input;
use Mail;
use Watson\Validating\ValidatingTrait;

class Leave extends BaseModel
{

    use ValidatingTrait;

    protected $table = 'leaves';
    public $timestamps = true;
    protected $appends = array('sickLeaves', 'paidLeaves', 'maxSickLeaves', 'maxPaidLeaves');

    private $rules = array(
        'user_id' => 'required',
        'start_date' => 'required',
        'end_date' => 'required',
    );

    public function scopeGenericSearch($query, $value)
    {
        //$query = $query->orWhere('question', 'LIKE', '%'.$value.'%');
        return $query;
    }
    public function getSickLeavesAttribute()
    {
        $user = User::where('id', $this->user_id)->first();
        $total = NewLeaveService::getUserLeaveTaken($user->id, 'sick');
        return $total;
    }
    public function getPaidLeavesAttribute()
    {
        $user = User::where('id', $this->user_id)->first();
        $total = NewLeaveService::getUserLeaveTaken($user->id, 'paid');
        return $total;
    }
    public function getMaxSickLeavesAttribute()
    {
        $total = NewLeaveService::getAllowedLeave($this->user_id, 'sick');
        return $total;
    }
    public function getMaxPaidLeavesAttribute()
    {
        $total = NewLeaveService::getAllowedLeave($this->user_id, 'paid');
        return $total;
    }
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function calendarEvent()
    {
        return $this->belongsTo('App\Models\CalendarEvent', 'user_id', 'id');
    }

    public function approver()
    {
        return $this->belongsTo('App\Models\User', 'approver_id', 'id');
    }
    public function projectManagerApprovals()
    {
        return $this->hasMany('App\Models\ProjectManagerLeaveApproval', 'leave_id', 'id');
    }
    public function overLapRequests()
    {
        return $this->hasMany('App\Models\LeaveOverlapRequest', 'leave_id', 'id');
    }
    public function calendarEvents()
    {
        return $this->morphMany('App\Models\CalendarEvent', 'events');
    }
    public function leaveType()
    {
        return $this->belongsTo('App\Models\Admin\LeaveType', 'leave_type_id', 'id');
    }
    public function isLeaveType($code)
    {
        if ($this->leaveType()->where('code', $code)->count()) {
            return true;
        }
        return false;
    }
    public function leaveDeduction()
    {
        return $this->hasOne('App\Models\Admin\LeaveDeduction', 'leave_id', 'id');
    }

    public static function validateLeave($userId, $request, $skip, $role = "user")
    {
        $user = User::find($userId);

        $response = array('status' => false, 'errors' => "", 'message' => "");
        // $leaveLength=Carbon::parse($request['end_date'])->diffInDays(Carbon::parse($request['start_date']));
        // if ($leaveLength>13&&$request['type']!='unpaid') {
        //     $response['errors']="Duration of leave should not be more than 14 calender days ";
        //     return $response;
        //     //return redirect::back()->withErrors("Duration of leave should not be more than 14 calender days ")->withInput($request->input());
        // }

        if (!LeaveService::isLeaveValid($request['start_date'], $request['end_date'])) {
            $response['errors'] = 'Start of Leave should be on or before End Date';
            return $response;
        }
        // if (($request['type']=="paid")&&$skip==0) {
        //     if (!LeaveService::isLeaveOverlapping($request['start_date'], $request['end_date'], $user)) {
        //         $response['errors'] = 'A leave already exists within/after 20 working days of the applied leave';
        //         return $response;
        //     }
        // }

        if (abs(LeaveService::calculateWorkingDays($request['start_date'], $request['end_date'], $userId)) == 0) { //pass user id
            $response['errors'] = 'There are no working days in the given date range';
            return $response;
        }

        // if (!LeaveService::isLeaveAppliedForCurrentWorkingYear($request['start_date'], $request['end_date'], $role)) {
        //     // if ($role == 'user') {
        //     //     $response['errors'] = 'Leave can be applied only for current or upcoming dates';
        //     // }

        //     // if ($role == 'admin') {
        //         $response['errors'] = 'Leave can be applied only for current working year';
        //     return $response;
        //     //}
        // }
        if ($request['type'] == 'paid' || $request['type'] == 'half') {
            if ($role = 'admin') {
                $skip = 1;
            }
            $response = Leave::validatePaidLeave($user, $request, $skip);
            return $response;
        }

        if ($request['type'] == 'sick') {
            $response = Leave::validateSickLeave($user, $request);
            return $response;
        }
        if ($request['type'] == 'compoff') {
            $requestedDays = LeaveService::calculateWorkingDays($request['start_date'], $request['end_date'], $userId);
            $totalAdded = CompOff::where('user_id', $user->id)->sum('days');
            $availed = Leave::where('user_id', $user->id)->where('type', 'compoff')->whereIn('status', ['approved', 'pending'])->sum('days');
            if ($requestedDays > $totalAdded - $availed) {
                $response['errors'] = 'Not enough Comp-Offs left';
                return $response;
            }
            $response['status'] = true;
            return $response;
        }

        $response['status'] = true;
        return $response;
    }

    public static function validateSickLeave($user, $request)
    {
        $response = array('status' => false, 'errors' => "", 'message' => "");

        if ($user->joining_date == null) {
            $response['errors'] = 'Joining Date unavailable';
            return $response;
        }
        $response['status'] = true;
        return $response;
        // $leftLeaves = LeaveService::getRemainingSickLeave($user)>0?LeaveService::getRemainingSickLeave($user):0;
        // $requestedDays = LeaveService::calculateWorkingDays($request['start_date'], $request['end_date'], $user->id); //user id

        // if ($leftLeaves < abs($requestedDays)) {
        //     $response['errors'] = 'Not enough sick leaves left';
        //     return $response;
        // }
    }

    public static function validatePaidLeave($user, $request, $skip)
    {
        $response = array('status' => false, 'errors' => "", 'message' => "");

        if ($user->confirmation_date == null) {
            $response['errors'] = 'Confirmation Date unavailable';
            return $response;
        }
        $response['status'] = true;
        return $response;
        // if ($request['type']=='half') {
        //     $requestedDays = 0.5;
        // } else {
        //     $requestedDays = LeaveService::calculateWorkingDays($request['start_date'], $request['end_date'], $user->id); //userid
        // }
        // if ($skip==0) {
        //     if (!LeaveService::isPersonalLeaveEligible($request['start_date'], $request['end_date'], $requestedDays)) {

        // if ($requestedDays <= 3) {
        //     $response['errors'] = $requestedDays." paid leave should be applied atleast ".(2*$requestedDays)." days prior to leave.";
        //     return $response;
        // } else {
        //     $response['errors'] = 'Paid Leave should be applied atleast 15 days prior to Leave.';
        //     return $response;
        // }
        //     }
        // }
        // if (!LeaveService::isLeaveLengthValid($request['start_date'], $request['end_date'])) {
        //     $response['errors'] = 'You cannot apply paid leave for more than 14 days.';
        //     return $response;
        // }

        // $leftLeaves = LeaveService::getRemainingPaidLeave($user);

        // if ($leftLeaves < $requestedDays) {
        //     $response['errors'] = 'Not enough paid leaves left';
        //     return $response;
        // }
    }

    /**
     * Send mail to notify application of leave to admin
     *
     * @param int $id => ID of leave applied
     *
     * @return null
     */
    public static function notifyLeaveApplicationToAdmin($id)
    {
        // return;
        $leave = Leave::find($id);
        if ($leave) {
            $job = (new LeaveRequestMail($leave))->onQueue('leave-mail-to-admin');
            dispatch($job);
            SlackLeaveReminder::remindUserAndTeamLead($id);
            SlackLeaveReminder::remindReportingManager($id);
        }
    }

    /**
     * Send mail to notify acceptance/reject of leave to user as well as admin
     *
     * @param int $leaveId => number. Contains ID of leave approved or rejected
     *
     * @return null
     */
    public static function notifyLeaveAction($leaveId)
    {
        // return;
        $leave = Leave::find($leaveId);
        if ($leave) {
            if ($leave->status == "approved") {
                $job = (new CreateCalendarEventJob($leaveId))->onQueue('calendar-event');
                dispatch($job);
            }
            $job = (new LeaveNotificationMail($leave))->onQueue('leave-notification-mail');
            dispatch($job);
            SlackLeaveReminder::remindUserAndTeamLead($leaveId);
            SlackLeaveReminder::remindReportingManager($leaveId);
        }
    }
    public static function notifyClient($email, Leave $leave)
    {
        $_2days = LeaveService::getWorkingDay(date("d M Y", strtotime("-2 day", strtotime($leave->start_date))));
        $day = LeaveService::getWorkingDay(date("d M Y", strtotime($leave->start_date)));
        Activity::add('leave-email-client', $leave->id, 'Email to client 2 days before leave', json_encode($email), $_2days);
        Activity::add('leave-email-client', $leave->id, 'Email to client on leave start', json_encode($email), $day);
    }
    public static function arrayId()
    {
        $arrayId = [];
        $today = date("Y-m-d");
        $after15day = date('Y-m-d', strtotime('15 days', strtotime($today)));
        $x = $today;
        for ($i = 1; $i <= 15; $i++) {
            $ids = Leave::where('status', "approved")->where('start_date', '<=', $x)->where('end_date', '>=', $x)->select('id')->get();
            foreach ($ids as $id) {
                array_push($arrayId, $id->id);
            }
            $x = date('Y-m-d', strtotime('1 days', strtotime($x)));
        }
        return array_unique($arrayId);
    }
    public static function filter()
    {
        $result = array('status' => false, 'errors' => "", 'message' => "");
        $title = "Leave Dashboard";
        $arrayId = Leave::arrayId();
        $pendingLeaves = Leave::with('user', 'leaveType')->where('status', "pending")->orderBy('start_date', 'DESC');
        $upcomingLeaves = Leave::with('user', 'leaveType')->whereIn('id', $arrayId)->orderBy('start_date', 'DESC');
        $leaveList = Leave::with('user', 'leaveType')->orderBy('start_date', 'DESC');
        if (Input::get('searchuser') != '' && (Input::get('start_date') == '' && Input::get('end_date') == '')) {
            $key = str_replace('+', ' ', Input::get('searchuser'));
            $searchUserId = User::where('name', $key)->where('is_active', 1)->first();
            if (empty($searchUserId)) {
                $result['errors'] = "Select a valid User";
                return $result;
            }
            $name = $searchUserId->name;
            $searchUserId = $searchUserId->id;
            $pendingLeaves = $pendingLeaves->where('user_id', $searchUserId);
            $upcomingLeaves = $upcomingLeaves->where('user_id', $searchUserId);
            $leaveList = $leaveList->where('user_id', $searchUserId);
        }
        if (Input::get('searchuser') != '' && (Input::get('start_date') != '' && Input::get('end_date') != '')) {
            $startDate = Input::get('start_date');
            $endDate = Input::get('end_date');
            $key = str_replace('+', ' ', Input::get('searchuser'));
            $searchUserId = User::where('name', $key)->where('is_active', 1)->first();
            if (empty($searchUserId)) {
                $result['errors'] = "Select a valid User";
                return $result;
            }
            $name = $searchUserId->name;
            $searchUserId = $searchUserId->id;
            $pendingLeaves = $pendingLeaves->where('user_id', $searchUserId)->where('start_date', '<=', $endDate)->where('end_date', '>=', $startDate);
            $upcomingLeaves = $upcomingLeaves->where('user_id', $searchUserId)->where('start_date', '<=', $endDate)->where('end_date', '>=', $startDate);
            $leaveList = $leaveList->where('user_id', $searchUserId)->where('start_date', '<=', $endDate)->where('end_date', '>=', $startDate);
        }
        if (Input::get('searchuser') == '' && (Input::get('start_date') != '' && Input::get('end_date') != '')) {
            $startDate = Input::get('start_date');
            $endDate = Input::get('end_date');
            $pendingLeaves = $pendingLeaves->where('start_date', '<=', $endDate)->where('end_date', '>=', $startDate);
            $upcomingLeaves = $upcomingLeaves->where('start_date', '<=', $endDate)->where('end_date', '>=', $startDate);
            $leaveList = $leaveList->where('start_date', '<=', $endDate)->where('end_date', '>=', $startDate);
        }
        if (Input::get('type') != '') {
            if (Input::get('type') == 'paid') {
                $leaveTypeObj = LeaveType::where('code', 'paid')->first();
                $pendingLeaves = $pendingLeaves->where('leave_type_id', $leaveTypeObj->id);
                $upcomingLeaves = $upcomingLeaves->where('leave_type_id', $leaveTypeObj->id);
                $leaveList = $leaveList->where('leave_type_id', $leaveTypeObj->id);
            } else if (Input::get('type') == 'sick') {
                $leaveTypeObj = LeaveType::where('code', 'sick')->first();
                $pendingLeaves = $pendingLeaves->where('leave_type_id', $leaveTypeObj->id);
                $upcomingLeaves = $upcomingLeaves->where('leave_type_id', $leaveTypeObj->id);
                $leaveList = $leaveList->where('leave_type_id', $leaveTypeObj->id);
            } else {
                $notInIds = LeaveType::whereIn('code', ['sick', 'paid'])->select('id')->pluck('id')->toArray();
                $pendingLeaves = $pendingLeaves->whereNotIn('leave_type_id', $notInIds);
                $upcomingLeaves = $upcomingLeaves->whereNotIn('leave_type_id', $notInIds);
                $leaveList = $leaveList->whereNotIn('leave_type_id', $notInIds);
            }

        }
        if (Input::get('status') != '') {
            $leaveList = $leaveList->where('status', Input::get('status'));
        }
        $pendingLeaves = $pendingLeaves->paginate(10, ['*'], 'pendingLeaves');
        $upcomingLeaves = $upcomingLeaves->paginate(10, ['*'], 'upcomingLeaves');
        $leaveList = $leaveList->paginate(20, ['*'], 'leaveList');

        $result = ['status' => true, 'pendingLeaves' => $pendingLeaves, 'upcomingLeaves' => $upcomingLeaves, 'leaveList' => $leaveList,
            'title' => $title];
        return $result;
    }

    public static function findUserLeaves($userId)
    {
        $leaveData = Leave::whereUserId($userId)
            ->whereIn('type', ['sick', 'paid', 'unpaid'])
            ->whereIn('status', ['pending', 'approved', 'rejected', 'cancelled'])
            ->orderBy('id', 'desc')
            ->take(50)
            ->get();
        $response = $leaveData ? $leaveData : null;
        return $response;
    }
    public static function isOnLeaveToday($userId, $date)
    {
        $leaveData = Leave::where('user_id', $userId)
            ->where('start_date', '<=', $date)
            ->where('end_date', '>=', $date)
            ->where('status', 'approved')->first();
        $response = $leaveData ? $leaveData : null;
        return $response;
    }
    public static function saveLeave($leave_id, $user_id, $action)
    {
        $response = ['status' => false, 'message' => ""];
        $leave = Leave::find($leave_id);
        $user = User::find($user_id);
        $action_date = date('Y-m-d');
        if ($leave && $user) {
            if ($leave->user->parent_id == $user_id || $user->isAdmin()) {
                if ($leave->status == "pending") {
                    if ($action == 'approved') {
                        LeaveService::approveLeave($leave->id, $user_id, $action_date);
                    } elseif ($action == 'rejected') {
                        LeaveService::rejectLeave($leave->id, $user_id, $action_date);
                    } elseif ($action == 'cancelled') {
                        LeaveService::cancelLeave($leave->id, $user_id, $action_date);
                    } else {
                        $response['message'] = "Inappropriate action";
                    }
                    $response = ['status' => true, 'message' => "Leave " . $action];
                } else {
                    $response['message'] = "The leave has already been " . $leave->status;
                }
            } else {
                $response['message'] = "You can't approve/reject this leave";
            }
        } else {
            $response['message'] = "Invalid link followed";
        }
        return $response;
    }

    public static function findPreviousLeaves($leaveId)
    {
        $leave = Leave::find($leaveId);
        $year = date('Y');
        $previousLeaves = Leave::with('leaveType')->where('user_id', $leave->user_id)->where('start_date', '<', $leave->start_date)->whereDate('end_date', '>=', $year . '-01-01')
            ->whereIn('status', ['pending', 'approved'])->get();
        return $previousLeaves;
    }

}
