<?php

namespace App\Models\Admin;
use Watson\Validating\ValidatingTrait;
use App\Models\BaseModel;

class ProjectTechnologies extends BaseModel
{
	use ValidatingTrait;
	protected $table = 'project_technology';
	

	private $rules = array(
		    'project_id' => 'required',
	        'technology_id' => 'required',

	        // 'description'  => 'required'
	    );
	public function technology() 
	{	
		return $this->hasOne('App\Models\Admin\Technology','id');
	}
	
	public function project()
	{
		return $this->belongsTo('App\Models\Admin\Project', 'project_id');
	}
}
