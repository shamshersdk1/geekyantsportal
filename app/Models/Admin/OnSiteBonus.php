<?php

namespace App\Models\Admin;

use App\Models\BaseModel;
use Auth;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Admin\Bonus;
use App\Models\BonusRequest;

class OnSiteBonus extends BaseModel
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $table = 'on_site_bonuses';
    public $timestamps = true;
    protected $hidden = array('password', 'token');
    public function scopeBuildQuery($query)
    {
        return $query;
    }
    public function reference()
    {
        return $this->morphTo('reference');
    }
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'employee_id', 'id');
    }
    public function reviewer()
    {
        return $this->belongsTo('App\Models\User', 'reviewed_by', 'id');
    }
    public function onsiteAllowance()
    {
        return $this->belongsTo('App\Models\Admin\OnsiteAllowance', 'onsite_allowance_id', 'id');
    }
    

    public static function getByMonthRangeAndUser($user_id, $start_date, $end_date)
    {

    }
    public static function saveData($data)
    {
        $obj = new self;
        $obj->user_id = !empty($data['user_id']) ? $data['user_id'] : null;
        $obj->date = !empty($data['date']) ? $data['date'] : date('Y-m-d');
        $obj->status = !empty($data['status']) ? $data['status'] : 'pending';
        $obj->created_by = !empty($data['created_by']) ? $data['created_by'] : null;//$userObj->id;
        $obj->onsite_allowance_id = !empty($data['onsite_allowance_id']) ? $data['onsite_allowance_id'] : null;
        $obj->reviewed_by = !empty($data['reviewed_by']) ? $data['reviewed_by'] : null;
        $obj->notes = !empty($data['notes']) ? $data['notes'] : null;
        
        if(!empty($data['created_at'])){
            $obj->created_at=$data['created_at'];
        }
        if(!empty($data['updated_at'])){
            $obj->updated_at=$data['updated_at'];
        }
        // if(!empty($data['created_at']))
        //     $obj->created_at = $data['created_at'];
        // if(!empty($data['created_at']))
        //     $obj->created_at = $data['created_at'];

        if (!$obj->save()) {
            return false;
        }
        return $obj;
    }

    public static  function migrateBonusRequestToOnSite($bonusRequestId){
        $bonus = BonusRequest::find($bonusRequestId);
        if(!$bonus) 
            return false;

        $data = [];
        $data['user_id']=$bonus->user_id ? $bonus->user_id :'';
        $data['month_id']=$bonus->month_id ? $bonus->month_id : '';
        $data['date']=$bonus->date  ? $bonus->date :'';
        $data['status']=$bonus->status;
        $data['notes']=$bonus->notes ? $bonus->notes : '';
        $data['reviewed_by']=$bonus->approver_id ? $bonus->approver_id :'';
        $data['created_by']=$bonus->user_id ? $bonus->user_id :'';
        $data['created_at']=$bonus->created_at ? $bonus->created_at :'';
        $data['updated_at']=$bonus->updated_at ? $bonus->updated_at :'';
        $data['approved_at']=$bonus->updated_at ? $bonus->updated_at :'';

        
        $redeem_type=json_decode($bonus->redeem_type);
            if(empty($redeem_type->onsite_allowance_id)){
                \Log::info('Bonus Req : '.$bonus->id. ' onsiteAllowanceId  not found');
            } else {
             $onsiteAllowanceId=$redeem_type->onsite_allowance_id;
            $data['onsite_allowance_id']=$onsiteAllowanceId;
            $onSiteObj=self::saveData($data);
            if($onSiteObj && $onSiteObj->status == 'approved') {
            $data['reference_id']=$onSiteObj->id;
            $data['reference_type']='App\Models\Admin\OnSiteBonus';
            $data['approved_by']=$onSiteObj->reviewed_by;
            $data['type']='onsite';
            $data['amount']=$onSiteObj->onsiteAllowance->amount;
                if($bonus->status==='approved'){
                 Bonus::saveApprovedBonus($data);
                } else {
                     \Log::info('Bonus Req : '.$bonus->id. ' onsiteAllowanceObj amount not found');
                }
            }
                                      
        } 
    }
}
