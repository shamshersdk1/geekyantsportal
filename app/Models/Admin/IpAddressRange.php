<?php

namespace App\Models\Admin;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class IpAddressRange extends BaseModel
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $table = 'ip_address_ranges';

    public function scopeBuildQuery($query)
    {
        return $query;
    }
    public static function saveIpRange($data)
    {
        $response = ['status' => false, 'message' => ""];

        $IpAddressRange = new IpAddressRange();
        $IpAddressRange->name = !empty($data['name']) ? $data['name'] : null;
        $IpAddressRange->start_ip = !empty($data['start_ip']) ? $data['start_ip'] : null;
        $IpAddressRange->end_ip = !empty($data['end_ip']) ? $data['end_ip'] : null;
        $IpAddressRange->net_mask_ip = !empty($data['net_mask']) ? $data['net_mask'] : null;

        if (!ip2long($data['start_ip']) || empty($data['start_ip']) || empty($data['end_ip']) || !ip2long($data['end_ip'])) {
            $response['message'] = "Please enter a valid Ip address!";
            return $response;
        }
        if (($data['net_mask']) && !ip2long($data['net_mask'])) {
            $response['message'] = "Please enter a valid Ip address!";
            return $response;
        }

        $IpAddressRange->start_ip_long = ip2long($data['start_ip']);
        $IpAddressRange->end_ip_long = ip2long($data['end_ip']);
        $IpAddressRange->net_mask_ip_long = !empty($data['net_mask']) ? ip2long($data['net_mask']) : null;

        if (!$IpAddressRange->save()) {
            $response['message'] = $IpAddressRange->getErrors();
        } else {
            $response['status'] = true;
            $response['message'] = "Saved Successfully!";
        }
        return $response;
    }
    public static function updateIpRange($data, $id)
    {
        $response = ['status' => false, 'message' => ""];
        $IpAddressRange = IpAddressRange::find($id);
        $IpAddressRange->name = !empty($data['name']) ? $data['name'] : null;
        $IpAddressRange->start_ip = !empty($data['start_ip']) ? $data['start_ip'] : null;
        $IpAddressRange->end_ip = !empty($data['end_ip']) ? $data['end_ip'] : null;
        $IpAddressRange->net_mask_ip = !empty($data['net_mask']) ? $data['net_mask'] : null;

        if (!ip2long($data['start_ip']) || empty($data['start_ip']) || empty($data['end_ip']) || !ip2long($data['end_ip'])) {
            $response['message'] = "Please enter a valid Ip address!";
            return $response;
        }
        if (($data['net_mask']) && !ip2long($data['net_mask'])) {
            $response['message'] = "Please enter a valid Ip address!";
            return $response;
        }
        $IpAddressRange->start_ip_long = ip2long($data['start_ip']);
        $IpAddressRange->end_ip_long = ip2long($data['end_ip']);
        $IpAddressRange->net_mask_ip_long = !empty($data['net_mask']) ? ip2long($data['net_mask']) : null;

        if (!$IpAddressRange->save()) {
            $response['message'] = $IpAddressRange->getErrors();
        } else {
            $response['status'] = true;
            $response['message'] = "Updated Successfully!";
        }
        return $response;
    }
    public static function findIprange($ip_address)
    {
        $ip = ip2long($ip_address);
        $IpAddressRange = IpAddressRange::All();
        $rangeId = '';
        foreach ($IpAddressRange as $range) {
            if (($range->start_ip_long <= $ip) && ($ip <= $range->end_ip_long)) {
                $rangeId = $range->name;
            }
        }
        return $rangeId;
    }
}
