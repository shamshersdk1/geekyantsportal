<?php namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class TechTalkVideo extends Model {
	
	public $timestamps = true;
	use SoftDeletes;
}
