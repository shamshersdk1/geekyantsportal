<?php namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;
use Input;

class CmsProject extends Model {

	use ValidatingTrait;

	protected $table = 'cms_projects';
	public $timestamps = false;

	private $rules = array(
	        'project_title' => 'required',
	        'project_image'  => 'required',
	        'project_desc' => 'required',
	        'project_type' => 'required',
	    );

	public function cmsTechnologies() {
		return $this->belongsToMany('App\Models\Admin\CmsTechnology', 'cms_project_cms_technology', 'cms_project_id', 'cms_technology_id');
	}
	public function users(){
		return $this->hasMany('App\Models\User');
	}

	public function profiles() {
		return $this->belongsToMany('App\Models\Admin\CmsTechnology', 'profile_project', 'project_id', 'profile_id');
	}
	public static function filter()
    {
        $search="";
        $arrayId = [];
        $result=array('status' => false,'errors'=>"",'message' => "");
        $projects = CmsProject::orderBy('id', 'DESC');

        if (Input::get('searchprojects')!='') {
            $search=str_replace('+', ' ', Input::get('searchprojects'));
			$projectst=$projects->get();
            foreach ($projectst as $project) {
                if ( stripos ( $project->project_title, $search ) !== false )  {
                    array_push($arrayId, $project->id);
                }
            }
            $projects=$projects->whereIn('id', $arrayId);
        }
        $projects=$projects->paginate(10);
		$result=['status'=>true, 'projects' => $projects,'search'=>$search];
		return $result;
    }

}
