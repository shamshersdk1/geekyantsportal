<?php namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

use App\Models\User;
use Carbon\Carbon;

class Dashboard extends Model
{
    public static function appraisals($limit)
    {
        $array = [];
        $result = [];
        $users = User::where('is_active',1)->whereNotIn('parent_id',[0,'null'])->get();
        $date = Carbon::now();
        foreach ($users as $user) {
            if (empty($user->appraisals->max('effective_date'))) {
                if (!empty($user->confirmation_date)) {
                    $array['name'][]= $user->name;
                    $array['date'][]=$user->confirmation_date;
                } elseif (!empty($user->joining_date)) {
                    $array['name'][]= $user->name;
                    $array['date'][]=$user->joining_date;
                }
            } else {
                $array['name'][]= $user->name;
                $array['date'][]=$user->appraisals->max('effective_date');
            }
        }
        if(count($array) > 0 ) {
            array_multisort($array['date'], $array['name']);
            for ($i=0; $i<$limit && $i<count($array['name']); $i++) {
                $result[$i]['name']=$array['name'][$i];
                $result[$i]['date']=$array['date'][$i];
                $diff=Carbon::parse($array['date'][$i])->diff($date);
                $str="";
                if ($diff->y>0) {
                    $str=$diff->y.'years, ';
                }
                if ($diff->m>0) {
                    $str=$str.$diff->m.'months, ';
                }
                if ($diff->d>0) {
                    $str=$str.$diff->d.'days';
                }
                if ($str=="") {
                    $str="0 days";
                }
                    $result[$i]['diff']=$str;
            }
        }
        
        return $result;
    }
    public static function confirmations($limit)
    {
        $array = [];
        $result = [];
        $users = User::where('is_active',1)->where('confirmation_date', null)->orWhere('confirmation_date', '0000-00-00')->get();
        $date = Carbon::now();
        foreach ($users as $user) {
            if (!empty($user->joining_date)) {
                $array['name'][]= $user->name;
                $array['date'][]=$user->joining_date;
            }
        }
        if(count($array) > 0 ) {
            array_multisort($array['date'], $array['name']);
            for ($i=0; $i<$limit && $i<count($array['name']); $i++) {
                $result[$i]['name']=$array['name'][$i];
                $result[$i]['date']=$array['date'][$i];
                $diff=Carbon::parse($array['date'][$i])->diff($date);
                $str="";
                if ($diff->y>0) {
                    $str=$diff->y.'years, ';
                }
                if ($diff->m>0) {
                    $str=$str.$diff->m.'months, ';
                }
                if ($diff->d>0) {
                    $str=$str.$diff->d.'days';
                }
                if ($str=="") {
                    $str="0 days";
                }
                $result[$i]['diff']=$str;
            }
        }
        return $result;
    }
}
