<?php

namespace App\Models\Admin;

use App\Models\BaseModel;
use App\Models\Month;
use DateTime;
use DateInterval;
use DatePeriod;

class FinancialYear extends BaseModel
{

    // use ValidatingTrait;
    // public $timestamps = true;
    // use SoftDeletes;
    protected $table = 'financial_years';
    private $rules = [];
    public function months()
    {
        return $this->hasMany('App\Models\Month', 'financial_year_id', 'id');
    }
    public static function getRemainingMonths($monthId)
    {
        $monthObj = Month::find($monthId);
        if ($monthObj) {
            if ($monthObj->month >= 4 && $monthObj->month <= 12) {
                return (12 - ($monthObj->month - 3));
            } else {
                return (12 - ($monthObj->month + 9));
            }
        }
    }

    public static function insertFinacialYear($data)
    {
        $exist = FinancialYear::where('year', $data['year'])->first();
        if ($exist) {
            return false;
        }

        $yearObj = new FinancialYear();
        $yearObj->year = !empty($data['year']) ? $data['year'] : null;
        $yearObj->status = "pending";
        $yearObj->is_locked = "0";
        $yearObj->start_date = date('Y').'-04-01';
        $yearObj->end_date = (date('Y')+1).'-03-31';
        if (!$yearObj->save()) {
            //$response['message'] = $yearObj->getErrors();
            return false;
        }
        return true;
    }

    public static function getPreviousYearId($currentYear)
    {
        $prevYear = $currentYear - 1;
        $prevObj = FinancialYear::where('year', $prevYear)->first();
        return $prevObj;
    }

    public static function getAllFinancialMonth($financialYearId) {
        //$startDate = 
        $obj = self::find($financialYearId);
        if($obj) {
            $startYear = $obj->year;
            $endYear = $obj->year + 1;
        }
        
        $startDate = date($startYear.'-04-01');
        $endDate = date($endYear.'-03-01');
        $months = [];
        $start    = (new DateTime($startDate))->modify('first day of this month');
        $end      = (new DateTime($endDate))->modify('first day of next month');
        $interval = DateInterval::createFromDateString('1 month');
        $period   = new DatePeriod($start, $interval, $end);

        foreach ($period as $dt) {
            $months[] = ['month' => $dt->format("n"),'year' => $dt->format("Y")];
        }
        //dd($months);
        return $months;
    }
    public static function getUpcomingMonth($financialYearId, $currMonth = null)
    {
        $financialYearObj = FinancialYear::find($financialYearId);
        if(!$currMonth)
            $currMonth = date('m');

        $upcomingMonths = [];

        if ($financialYearObj->start_date <= date('Y-m-d') && $financialYearObj->end_date >= date('Y-m-d')) {

            if ($currMonth >= 4 && $currMonth <= 12) {
                for ($i = $currMonth + 1; $i <= 12; $i++) {
                    $upcomingMonths[$i]->year = $financialYearObj->year;
                    $upcomingMonths[$i]->month = $i;
                }

                for ($i = 1; $i <= 3; $i++) {
                    $upcomingMonths[$i]->year = $financialYearObj->year + 1;
                    $upcomingMonths[$i]->month = $i;
                }
            } else {
                for ($i = $currMonth + 1; $i <= 3; $i++) {
                    $upcomingMonths[$i]->year = $financialYearObj->year + 1;
                    $upcomingMonths[$i]->month = $i;
                }
            }
        }
        return $upcomingMonths;
    }
}
