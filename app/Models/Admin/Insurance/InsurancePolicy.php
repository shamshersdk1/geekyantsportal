<?php namespace App\Models\Admin\Insurance;

use App\Models\Admin\Insurance\Insurance;
use App\Models\BaseModel;
use App\Models\User;
use Auth;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;

class InsurancePolicy extends BaseModel
{

    use ValidatingTrait;

    protected $table = 'insurance_policies';
    //public $timestamps = true;
    use SoftDeletes;
    protected $fillable = ['vendor_id', 'name', 'policy_number', 'start_date', 'end_date', 'coverage_amount', 'amount_paid', 'premium_amount'];
    // protected $appends = ['shared_with','last_updated'];
    protected $appends = ['has_sub_group'];
    private $rules = array(

    );
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function insurances()
    {
        return $this->hasMany('App\Models\Admin\Insurance\Insurance', 'insurance_policy_id', 'id')->orderBy('id', 'ASC');
    }
    // public function requestedBy()
    // {
    //     return $this->belongsTo(User::class, 'requested_by','id');
    // }
    // public function approvedBy()
    // {
    //     return $this->belongsTo(User::class, 'approved_by','id');
    // }
    // public function closedBy()
    // {
    //     return $this->belongsTo(User::class, 'closed_by','id');
    // }
    public function subGroups()
    {
        return $this->hasMany('App\Models\Admin\Insurance\InsurancePolicy', 'parent_id', 'id');
    }
    // public function lastUpdatedRec()
    // {
    //     return $this->hasOne('App\Models\Admin\Insurance\InsurancePremium','id','parent_id');
    // }
    public function getHasSubGroupAttribute()
    {
        if (count($this->subGroups) > 0) {
            return true;
        }
        return false;
    }

    // public function addActivity() {
    //     $data['user_id'] = \Auth::User()->id;
    //     $data['reference_id'] = $this->id;
    //     $data['reference_type'] = "App\Models\Admin\Insurance\InsurancePremium";
    //     $data['action'] =  $this->status;
    //     Activity::storeData($data);
    // }
    public static function saveData($data)
    {
        $user = \Auth::User();
        $obj = new self;
        $obj->vendor_id = !empty($data['vendor_id']) ? $data['vendor_id'] : null;
        $obj->name = !empty($data['name']) ? $data['name'] : null;
        $obj->policy_number = !empty($data['policy_number']) ? $data['policy_number'] : null;
        $obj->start_date = !empty($data['start_date']) ? $data['start_date'] : null;
        $obj->end_date = !empty($data['end_date']) ? $data['end_date'] : null;
        $obj->coverage_amount = !empty($data['coverage_amount']) ? $data['coverage_amount'] : null;
        $obj->premium_amount = !empty($data['premium_amount']) ? $data['premium_amount'] : null;
        $obj->amount_paid = !empty($data['amount_paid']) ? $data['amount_paid'] : null;
        $obj->created_by = $user->id;
        $obj->accessible_to = !empty($data['accessible_to']) ? $data['accessible_to'] : null;
        $obj->applicable_for = !empty($data['applicable_for']) ? $data['applicable_for'] : null;
        $obj->parent_id = !empty($data['parent_id']) ? $data['parent_id'] : null;
        $obj->sub_group = !empty($data['sub_group']) ? $data['sub_group'] : false;
        if ($obj->save()) {
            // if(isset($data['sub_groups']) && count($data['sub_groups'])>0) {
            //     foreach ($data['sub_groups'] as $subGruop) {
            //         $subObj = $obj->replicate();
            //         $subObj->name = !empty($subGruop['name']) ? $subGruop['name'] : null;
            //         $subObj->premium_amount = !empty($subGruop['premium_amount']) ? $subGruop['premium_amount'] : null;
            //         $subObj->parent_id = $obj->id;
            //         $obj->created_by = $user->id;
            //         $subObj->save();
            //     }
            // }
            return true;
        } else {
            return false;
        }
    }
}
