<?php namespace App\Models\Admin\Insurance;

use App\Models\BaseModel;
use App\Models\Month;
use App\Models\MonthSetting;
use Illuminate\Database\Eloquent\SoftDeletes;

class InsuranceDeduction extends BaseModel
{
    //use ValidatingTrait;
    use SoftDeletes;
    private $rules = array();

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
    public function insurance()
    {
        return $this->belongsTo('App\Models\Admin\Insurance\InsurancePolicy', 'insurance_policy_id');
    }

    public static function regenerateInsurance($month_id)
    {
        $response['status'] = false;
        $response['message'] = null;
        $response['data'] = null;
        $monthObj = Month::find($month_id);
        if (!$monthObj) {
            $response['message'] = "Invalid month id " . $month_id;
            return $response;
        }
        if ($monthObj->insuranceSetting == 'locked') {
            $response['message'] = "Cannot regenrate. Month is locked";
            return $response;
        }
        InsuranceDeduction::where('month_id', $month_id)->delete();
        $errors = [];
        $insurancePolicies = InsurancePolicy::where('parent_id', null)->where('status', 'approved')->get();
        foreach ($insurancePolicies as $insurancePolicy) {
            foreach ($insurancePolicy->subGroups as $subCategory) {
                foreach ($subCategory->insurances as $insurance) {
                    $deductionObj = new InsuranceDeduction();
                    $deductionObj->month_id = $month_id;
                    $deductionObj->user_id = $insurance->insurable_id;
                    $deductionObj->insurance_policy_id = $insurance->insurance_policy_id;
                    $deductionObj->amount = -$subCategory->premium_amount;
                    if (!$deductionObj->save()) {
                        $errors[] = $deductionObj->getErrors();
                    }
                }
            }
        }

        $response['status'] = true;
        $response['message'] = "Insurance deduction regenerated";
        $response['data'] = null;
        return $response;
    }

    public static function getCurrMonthDeduction($monthId, $userId)
    {
        $insurance = 0;
        $checkLock = MonthSetting::where('key', 'insurance-deduction')->first();
        if ($checkLock && $checkLock->value == 'locked') {
            $insurance = self::where('month_id', $monthId)->where('user_id', $userId)->sum('amount');
            return $insurance;
        }
        return null;
    }
}
