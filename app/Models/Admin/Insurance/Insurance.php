<?php namespace App\Models\Admin\Insurance;

use App\Models\BaseModel;
use App\Models\Activity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;

class Insurance extends BaseModel
{

    use ValidatingTrait;

    //protected $table = 'incidents';
    //public $timestamps = true;
    use SoftDeletes;
    // protected $fillable = ['subject', 'department_id'];
    // protected $appends = ['shared_with','last_updated'];
    public function insurable()
    {
        return $this->morphTo();
    }
    public function addedBy()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
    }
    public function policy()
    {
        return $this->belongsTo('App\Models\Admin\Insurance\InsurancePolicy', 'insurance_policy_id', 'id');
    }
    private $rules = array(
        'insurance_policy_id' => 'required',
        'insurable_id' => 'required',
        'insurable_type' => 'required',
    );
    public static function saveData($data) {
        $response['status'] = false;
        $response['error'] = false;
        $response['message'] = null;

        $insurancePolicyId = !empty($data['insurance_policy_id']) ? $data['insurance_policy_id'] : null;
        $insurableId = !empty($data['insurable_id']) ? $data['insurable_id'] : null;
        $insurableType = !empty($data['insurable_type']) ? $data['insurable_type'] : null;

        $policyObj = InsurancePolicy::find($insurancePolicyId);

        $exist = self::where('insurable_id',$insurableId)->where('insurance_policy_id',$insurancePolicyId)->where('insurable_type',$insurableType)->first();
        
        if($exist){
            $response['error'] = true;
            $response['message'] = "Insurance Policy already assigned to the selected entity";
            return $response;
        }
        if($policyObj && $policyObj->accessible_to == 'individual') {
            $countMemeber = Insurance::where('insurance_policy_id', $insurancePolicyId)->whereIn('status',['pending','approved'])->count();
            if($countMemeber >= 1) {
                $response['error'] = true;
                $response['message'] = "Insurance Policy can only be assined to a single entity";
                return $response;
            }
        }

        $user = \Auth::User();
        $obj = new self;
        $obj->insurance_policy_id = !empty($data['insurance_policy_id']) ? $data['insurance_policy_id'] : null;
        $obj->insurable_id = !empty($data['insurable_id']) ? $data['insurable_id'] : null;
        $obj->insurable_type = !empty($data['insurable_type']) ? $data['insurable_type'] : null;
        $obj->created_by = $user->id;
        $obj->status = 'approved';
        if(!$obj->save()) {
            $response['error'] = true;
            $response['message'] = "Unable to save the record";
            return $response;
        }
        $response['status'] = true;
        $response['message'] = "Record save successfully";
        $response['id'] = $obj->id;
        
        return $response;
        
    }
}
