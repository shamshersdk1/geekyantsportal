<?php

namespace App\Models\Admin;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class OnsiteAllowance extends BaseModel
{
    use SoftDeletes;
	public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $hidden = array('amount', 'notes','approver_id');
    public function scopeBuildQuery($query)
    {
        return $query;
    }
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
    public function approver()
    {
        return $this->belongsTo('App\Models\User', 'approver_id', 'id');
	}
	public function project()
    {
        return $this->belongsTo('App\Models\Admin\Project', 'project_id', 'id');
    }

}
