<?php


namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class ProjectNotes extends Model
{
    use ValidatingTrait;

    protected $table = 'project_notes';
    private $rules = array(
		
	        'project_id' => 'required',
	        'note' => 'required',
	        

	        // 'description'  => 'required'
	    );


    public static function saveData($request)
    {
    	$response =  array('status' => true,'message' => "success" ,'id' => NULL);

    	$projectNotes = new ProjectNotes();
    	$projectNotes->project_id = $request['project_id'];
    	$projectNotes->note = $request['comment'];
    	$projectNotes->user_name = $request['user_name'];
    	$projectNotes->private_to_admin = $request['private_to_admin'];
    	if($projectNotes->save())
    	{
    		return $response;
    	}
    	else{
    		$response['status'] = false;
    		$response['errors'] =  $projectNotes->getErrors();
    		return $response;
    	}
    }
// ,'projectNotes','projectFiles','projectTechnologies.technology'

}
