<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class Template extends Model
{
    use ValidatingTrait;

    protected $table = 'templates';

    private $rules = array(
            'name' => 'required',
            'content'  => 'required',
            // 'email'  => 'required|email',
            // .. more rules here ..
        );

    public static function saveData($request)
    {
    	$response =  array('status' => true,'message' => "success" ,'id' => NULL);

    	$template = new Template();
    	$template->name = $request['name'];
    	$template->content = $request['content'];
    	if($template->save())
    	{
    		return $response;
    	}
    	else{
    		$response['status'] = false;
    		$response['errors'] =  $template->getErrors();
    		return $response;
    	}
    }
}
