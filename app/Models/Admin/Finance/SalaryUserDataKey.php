<?php

namespace App\Models\Admin\Finance;

use App\Models\BaseModel;
use App\Models\Admin\PayrollRuleForCsv;

class SalaryUserDataKey extends BaseModel
{

    //protected $dates = ['deleted_at'];
    protected $table = 'salary_user_data_keys';
    public $timestamps = false;
    public function groupKey()
    {
        return $this->belongsTo('App\Models\Admin\Finance\PayroleGroupRuleMonth', 'key_id', 'id');
    }
    public function scopeBuildQuery($query)
    {
        return $query;
    }
    public function transactions()
    {
        return $this->morphMany('App\Models\Transaction', 'reference');
    }
    public static function getAmountByKey($salaryUserId, $key) {
        
        $keyObj = PayrollRuleForCsv::where('key',$key)->first();
        $amount = 0;
        $userData = SalaryUserData::find($salaryUserId);
        if($userData) {
            foreach ($userData->salaryKeys as $salaryKey) {
                if($salaryKey->groupKey->payroll_rule_for_csv_id == $keyObj->id) {
                    $amount = $salaryKey->amount;
                    break;
                }
            }
        }
        
        return $amount;
    }
    public static function getToBePaidAmountKey($salaryUserId, $key) {
        
        $keyObj = PayrollRuleForCsv::where('key',$key)->first();

        $amount = 0;
        $userData = SalaryUserData::find($salaryUserId);
        if($userData) {
            foreach ($userData->salaryKeys as $salaryKey) {
                if($salaryKey->groupKey->payroll_rule_for_csv_id == $keyObj->id) {
                    $amount = $salaryKey->amount;
                    break;
                }
            }
        }
        
        return $amount;
    }
    
    public function getAmount()
    {

        $namespace = "App\Services\SalaryService";
        $className = $this->class_name;
        $className = $namespace . '\\' . $className;
        $obj = new $className($userId, 3, $key->id);
        $amount = $obj->get();
        return $amount;
    }
    public static function saveData($data) {
        $keyObj = new SalaryUserDataKey();
        $keyObj->salary_user_data_id =  !empty($data['salary_user_data_id']) ?$data['salary_user_data_id'] : 0;
        $keyObj->key_id =  !empty($data['key_id']) ?$data['key_id'] : 0;
        $keyObj->amount =  !empty($data['amount']) ?$data['amount'] : 0;
        $keyObj->save();
    }
}
