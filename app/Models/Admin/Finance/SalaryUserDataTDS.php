<?php

namespace App\Models\Admin\Finance;

use App\Models\Admin\Finance\SalaryUserDataTDS;
use App\Models\Admin\ItSaving;
use App\Models\BaseModel;

class SalaryUserDataTDS extends BaseModel
{
    protected $table = 'salary_user_data_tds';

    public static function getTds($userId, $monthId)
    {
        $response['annual_gross_salary'] = 0;
        $response['hra'] = 0;
        $response['standard_deduction'] = 40000;
        $response['professional_tax'] = 2400;
        $response['annual_gross_salary'] = 0;
        $userObj = User::find($userId);
        $monthObj = Month::find($monthId);
        if ($monthObj && $userObj) {
            $itSavingData = ItSaving::getITSavingInfo($userId, $financialYearId);

            if ($monthObj->month != 4) { //april is the satrt of the financial year
                // itSaving for the last month should be zero
                SalaryUserDataTDS::where('user_id', $userId)->where('month_id', $monthId)->first();
            }
        }
    }
}
