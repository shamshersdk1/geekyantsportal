<?php

namespace App\Models\Admin\Finance;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Admin\PayroleGroupRule;

class AdvanceSalaryDeduction extends BaseModel
{
    protected $table = 'advance_salary_deductions';
}
