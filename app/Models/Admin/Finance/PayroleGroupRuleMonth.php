<?php

namespace App\Models\Admin\Finance;

use App\Models\Admin\PayroleGroupRule;
use App\Models\Admin\PayroleGroup;
use App\Models\BaseModel;
use App\Models\Month;
use Illuminate\Database\Eloquent\SoftDeletes;

class PayroleGroupRuleMonth extends BaseModel
{
    use SoftDeletes;
    public function scopeBuildQuery($query)
    {
        return $query;
    }
    public function key()
    {
        return $this->belongsTo('App\Models\Admin\PayrollRuleForCsv', 'payroll_rule_for_csv_id', 'id');
    }
    public function calc_upon()
    {
        return $this->belongsTo('App\Models\Admin\Finance\PayroleGroupRuleMonth', 'calculated_upon', 'id');
    }
    public function csvValue()
    {
        return $this->hasOne('App\Models\Admin\PayrollRuleForCsv', 'id', 'payroll_rule_for_csv_id');
    }
    // public function user()
    // {
    //     return $this->belongsTo('App\Models\User', 'employee_id', 'id');
    // }
    // public function reviewer()
    // {
    //     return $this->belongsTo('App\Models\User', 'created_by', 'id');
    // }
    public static function getMonthRules($groupId, $monthId)
    {
        $rules = self::where('payrole_group_id', $groupId)->where('month_id', $monthId)->get();
        return $rules;
        //return $this->hasMany('App\Models\Admin\PayroleGroupRule', 'payrole_group_id', 'id')->where('month_id', $monthId);
    }
    public static function getMonthRuleByKey($groupId, $monthId, $key)
    {
        $keyObj = PayrollRuleForCsv::where('key',$key)->first();
        
        if($keyObj) {
            $rule = self::where('payrole_group_id', $groupId)->where('month_id', $monthId)->where('payroll_rule_for_csv_id',$keyObj->id)->first();
            return $rule;
        }
        //return $this->hasMany('App\Models\Admin\PayroleGroupRule', 'payrole_group_id', 'id')->where('month_id', $monthId);
    }
    public static function addGroupRule($monthId, $groupId)
    {
        $rules = PayroleGroupRule::where('payrole_group_id', $groupId)->orderBy('id', 'ASC')->get();
        try {

            if (count($rules) > 0) {
                foreach ($rules as $rule) {
                    $obj = new self();
                    $obj->month_id = $monthId;
                    $obj->payrole_group_id = $rule->payrole_group_id;
                    $obj->payroll_rule_for_csv_id = $rule->payroll_rule_for_csv_id;
                    $obj->name = $rule->name;
                    $obj->type = $rule->type;
                    $obj->calculation_type = $rule->calculation_type;
                    $obj->calculation_value = $rule->calculation_value;
                    if ($rule->calculated_upon > 0) {
                        $calcuatedRule = PayroleGroupRule::find($rule->calculated_upon);
                        if ($calcuatedRule) {
                            $csvRuleId = $calcuatedRule->payroll_rule_for_csv_id;
                            $calculatedUpon = self::where('month_id', $monthId)->where('payrole_group_id', $rule->payrole_group_id)->where('payroll_rule_for_csv_id', $csvRuleId)->first();
                            if ($calculatedUpon) {
                                $obj->calculated_upon = $calculatedUpon->id;
                            }
                        }
                    } else {
                        $obj->calculated_upon = $rule->calculated_upon;
                    }
                    if (!$obj->save()) {
                        \Log::info('Unable to save PayroleGroupRuleMonth');
                    }
                }
            }

        } catch (Exception $e) {
            \Log::info('Exception PayroleGroupRuleMonth');
        }

    }

    public static function generatePayrollGroupRuleMonthly(){
        
        $getMonth = Month::orderBy('year','DESC')->orderBy('month','DESC')->first();
        $exits = PayroleGroupRuleMonth::where('month_id',$getMonth->id)->first();
        if($exits)
            return false;
        $payroleGroups = PayroleGroupRule::all();
        foreach($payroleGroups as $payroleGroup)
        {
           // echo $payroleGroup;
            //die;
            $payroleObj = new PayroleGroupRuleMonth();
            $payroleObj->payrole_group_id = $payroleGroup->payrole_group_id ? $payroleGroup->payrole_group_id : null;
            $payroleObj->payroll_rule_for_csv_id = $payroleGroup->payroll_rule_for_csv_id;
            $payroleObj->month_id = $getMonth->id ? $getMonth->id : null;
            $payroleObj->name = $payroleGroup->name ? $payroleGroup->name : null;
            $payroleObj->type = $payroleGroup->type ? $payroleGroup->type : null;
            $payroleObj->calculation_type = $payroleGroup->calculation_type ? $payroleGroup->calculation_type : null;
            $payroleObj->calculation_value = $payroleGroup->calculation_value ? $payroleGroup->calculation_value : null;
            $payroleObj->calculated_upon = $payroleGroup->calculated_upon ;
            if(!$payroleObj->save())
                return false;
        }
        return true;
    }

}
