<?php

namespace App\Models\Admin\Finance;

use App\Models\Admin\Finance\ItSavingUserData;
use App\Models\Admin\Insurance\InsuranceDeduction;
use App\Models\Admin\VpfDeduction;
use App\Models\BaseModel;
use App\Models\Loan;
use App\Models\LoanEmi;
use App\Models\Month;

class SalaryUserData extends BaseModel
{
    //protected $dates = ['deleted_at'];
    protected $table = 'salary_user_data';
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
    public function month()
    {
        return $this->belongsTo('App\Models\Month', 'month_id');
    }
    public function appraisal()
    {
        return $this->belongsTo('App\Models\Admin\Appraisal', 'appraisal_id');
    }
    public function salaryKeys()
    {
        return $this->hasMany('App\Models\Admin\Finance\SalaryUserDataKey', 'salary_user_data_id');
    }
    public function lastAppriasal()
    {
        $lastMonthObj = Month::orderBy('year', 'DESC')->orderBy('month', 'DESC')->skip(1)->take(1)->first();

        if ($lastMonthObj) {
            // return $lastMonthObj->id;
            return self::where('user_id', $this->user_id)->where('month_id', $lastMonthObj->id)->first();
        }
    }
    public function scopeBuildQuery($query)
    {
        return $query;
    }
    public static function saveData($data)
    {

        $obj = new self;
        $obj->user_id = !empty($data['user_id']) ? $data['user_id'] : 0;
        $obj->employee_id = !empty($data['employee_id']) ? $data['employee_id'] : 0;
        $obj->appraisal_id = !empty($data['appraisal_id']) ? $data['appraisal_id'] : 0;
        $obj->month_id = !empty($data['month_id']) ? $data['month_id'] : 0;
        $obj->days_worked = !empty($data['days_worked']) ? $data['days_worked'] : 0;
        $obj->monthly_ctc = !empty($data['monthly_ctc']) ? $data['monthly_ctc'] : 0;
        $obj->monthly_variable_bonus = !empty($data['monthly_variable_bonus']) ? $data['monthly_variable_bonus'] : 0;
        $obj->annual_bonus = !empty($data['annual_bonus']) ? $data['annual_bonus'] : 0;
        if ($obj->save()) {

            return $obj->id;
        }
        return false;
    }

    public function getSpecialAllowanceInternal()
    {
        /*Special Allowance Internal:   Monthly CTC – (Basic – HRA – Car Allowance – Food Allowance – LTA) */
        $specialAllowance = 0;//$this->monthly_ctc;

        foreach ($this->salaryKeys as $salaryKey) {
            if ($salaryKey->groupKey->name == 'basic' || $salaryKey->groupKey->name == 'hra' || $salaryKey->groupKey->name == 'car_allowance' || $salaryKey->groupKey->name == 'food_allowance' || $salaryKey->groupKey->name == 'leave_travel_allowance') {
                $specialAllowance += $salaryKey->amount;
            }
        }
        return $this->monthly_gross_salary -$specialAllowance;
    }
    public function getSpecialAllowance()
    {
        /*  Special Allowance :
        Monthly Gross Earnings – (Basic – HRA – Car Allowance – Food Allowance – LTA)
         */
        $specialAllowance = 0;//$this->getMonthlyGrossEarning();
        foreach ($this->salaryKeys as $salaryKey) {
            if ($salaryKey->groupKey->name == 'basic' || $salaryKey->groupKey->name == 'hra' || $salaryKey->groupKey->name == 'car_allowance' || $salaryKey->groupKey->name == 'food_allowance' || $salaryKey->groupKey->name == 'leave_travel_allowance') {
                $specialAllowance += $salaryKey->amount;
            }
        }
        return $this->monthly_gross_salary -$specialAllowance;
    }

    public function getNetSalaryForTheMonth()
    {
        //Net Salary for the Month :  Monthly Gross Earnings - Adjusted Total Monthly Deductions + Total Bonus for the Month
        return $this->getMonthlyGrossEarning() - $this->getTotalDeduction();
    }
    public function getMonthlyGrossEarning()
    {
        //Monthly Gross Earnings: Monthly CTC – (Employer PF + PF Admin Charges + Employer ESI)
        $earning = 0;

        // pf_employeer + pf_other +  esi_175
        foreach ($this->salaryKeys as $salaryKey) {
            if ($salaryKey->groupKey->name == 'pf_employeer' || $salaryKey->groupKey->name == 'pf_other' || $salaryKey->groupKey->name == 'esi_175') {
                $earning += $salaryKey->amount;
            }
        }
        $earning = $this->monthly_gross_salary;// - $earning;
        return $earning;
    }

    public function getTotalDeduction()
    {
        $deduction = 0;
        // Total Monthly Deductions:    Total Monthly Deductions Internal – (PF Employer + PF Admin + ESI Employer)
        $totalMonthlyDeductionsInternal = $this->getTotalDeductionInternal();
        foreach ($this->salaryKeys as $salaryKey) {
            if ($salaryKey->groupKey->name == 'pf_employeer' || $salaryKey->groupKey->name == 'pf_other' || $salaryKey->groupKey->name == 'esi_175') {
                $deduction += $salaryKey->amount;
            }
        }
        return $totalMonthlyDeductionsInternal - $deduction;
    }
    // Total Monthly Deductions Internal
    public function getTotalDeductionInternal()
    {
        /* Total Monthly Deductions Internal:
        PF Employee + PF Employer +
        PF Admin + ESI Employer +
        ESI Employee +
        P.T. +
        Food Deductions +
        Medical Insurance +
        T.D.S. +
        Loan Deductions +
        Deductions Leave Without Pay +
        Other Deductions
         */

        $deduction = 0;
        foreach ($this->salaryKeys as $salaryKey) {
            if ($salaryKey->groupKey->name == 'pf_employee' ||
                $salaryKey->groupKey->name == 'pf_employeer' ||
                $salaryKey->groupKey->name == 'pf_other' ||
                $salaryKey->groupKey->name == 'esi_175' ||
                $salaryKey->groupKey->name == 'esi_475' ||
                $salaryKey->groupKey->name == 'food_deduction' ||
                $salaryKey->groupKey->name == 'professional_tax'

            ) {
                if ($salaryKey->amount < 0) {
                    $deduction += (-1) * $salaryKey->amount;
                } else {
                    $deduction += $salaryKey->amount;
                }

            }
        }
        //$deduction += 224;
        $insuranceDeductions = InsuranceDeduction::where('month_id', $this->month_id)->where('user_id', $this->user_id)->get();
        if (count($insuranceDeductions) > 0) {
            foreach ($insuranceDeductions as $insuranceDeduction) {
                if ($insuranceDeduction->insurance) {
                    $deduction += $insuranceDeduction->amount;
                }

            }
        }
        //vpf + LoanEMI + TDS +
        $tdsObj = ItSavingUserData::where('month_id', $this->month_id)->where('user_id', $this->user_id)->first();
        if ($tdsObj) {
            $deduction += (-1) * $tdsObj->tds_amount;

        }
        $vpdDeductionObj = VpfDeduction::where('month_id', $this->month_id)->where('user_id', $this->user_id)->first();
        if ($vpdDeductionObj) {
            $deduction += $vpdDeductionObj->amount;
        }
        $loans = Loan::where('user_id', $this->user_id)->get();
        if (count($loans) > 0) {
            foreach ($loans as $loan) {
                $emiObj = LoanEmi::where('loan_id', $loan->id)->where('month_id', $this->month_id)->first();
                if ($emiObj) {
                    $deduction += $emiObj->actual_amount;
                }
            }
        }

        return $deduction;
    }
}
