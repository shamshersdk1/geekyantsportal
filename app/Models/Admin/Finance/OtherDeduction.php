<?php

namespace App\Models\Admin\Finance;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Admin\PayroleGroupRule;

class OtherDeduction extends BaseModel
{
    protected $table = 'other_deductions';
}
