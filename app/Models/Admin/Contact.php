<?php namespace App\Models\Admin;

use Watson\Validating\ValidatingTrait;
use App\Models\BaseModel;
//use ValidatingTrait;
class Contact extends BaseModel
{

    protected $table = 'contacts';
    public $timestamps = false;
    
      public function scopeBuildQuery($query)
    {
        return $query;
    }

    public function companies()
    {
        return $this->belongsTo('App\Models\Admin\Client');
    }

}
