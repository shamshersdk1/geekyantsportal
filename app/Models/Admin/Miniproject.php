<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Miniproject extends Model
{
    use SoftDeletes;

    public static function saveData($data)
    {
        $res['status'] = false;
        $res['message'] = null;
        $miniprojectObj = new Miniproject();
        $miniprojectObj->user_id = $data['user_id'];
        $miniprojectObj->title = $data['title'];
        $miniprojectObj->description = $data['description'];
        if (!$miniprojectObj->save()) {
            $res['status'] = true;
            $res['message'] = $miniprojectObj->getErrors();
            return $res;
        }
        return $miniprojectObj;
    }
}
