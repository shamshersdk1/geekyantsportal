<?php

namespace App\Models\Admin;

use App\Models\BaseModel;

class UserSkills extends BaseModel
{
    public $timestamps = false;
    protected $table='user_skills';
    

    public static function saveData($skill_id,$user_id){
        
       
        $user_skill= new UserSkills();

        $user_skill->user_id=$user_id;
        $user_skill->skill_id=$skill_id[0];

    
        $response['status']=false;
        $response['message']='';
        $response['data']=null;

        if(!$user_skill->save()){
           $response['message']='Not Saved';
           $response['data']=$tech->getErrors();
        }
        else{
            $response['status']=true;
            $response['message']= 'Saved';
            $response['data']=$user_skill;
        }
       return $response;
    
    }
}
