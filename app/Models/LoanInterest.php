<?php

namespace App\Models;

use App\Jobs\Loan\LoanInterestJob;
use App\Models\Month;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LoanInterest extends Model
{
    use SoftDeletes;

    public function loan()
    {
        return $this->belongsTo(Loan::class, 'loan_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function month()
    {
        return $this->belongsTo('App\Models\Month', 'month_id', 'id');
    }

    public static function getGrossLoanInterest($monthId, $userId)
    {
        $monthObj = Month::find($monthId);

        $totalInterest = 0;
        $totalInterest = LoanInterest::where('financial_year_id', $monthObj->financial_year_id)->where('user_id', $userId)->sum('interest');

        return $totalInterest;
    }

    public static function updateData($data, $monthId)
    {
        $response['status'] = false;
        $response['message'] = null;
        $response['data'] = null;
        foreach ($data as $index => $loan) {
            $interestObj = LoanInterest::find($index);
            $interestObj->interest = $data[$index];
            if (!$interestObj->save()) {
                $errors[] = $interestObj->getErrors();
                return $response;
            }
        }
        $response['status'] = true;
        $response['message'] = "Loan interest income saved!";
        $response['data'] = null;
        return $response;
    }

    public static function regenerateInterest($month_id)
    {
        $response['status'] = false;
        $response['message'] = null;
        $response['data'] = null;
        $monthObj = Month::find($month_id);
        if (!$monthObj) {
            $response['message'] = "Invalid month id " . $month_id;
            return $response;
        }
        if ($monthObj->loanInterestSetting == 'locked') {
            $response['message'] = "Cannot regenrate. Month is locked";
            return $response;
        }

        $users = User::orderBy('employee_id', 'ASC')->get();

        foreach ($users as $user) {
            if ($user->loans) {
                $job = (new LoanInterestJob($user->id, $month_id));
                dispatch($job);
            }
        }

        $response['status'] = true;
        $response['message'] = "Loan interest income regenerated";
        $response['data'] = null;
        return $response;
    }
}
