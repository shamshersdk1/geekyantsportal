<?php

namespace App\Models;

use Illuminate\Http\Request;
use App\Models\BaseModel;

class ITSavingUserData extends BaseModel
{
    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}