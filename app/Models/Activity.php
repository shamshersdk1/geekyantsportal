<?php

namespace App\Models;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    /*
    $type = Activity type eg:leaves,appraisal,bonus,etc.
    $aid= Activity Id
    $desc = Description of activity
    $json = raw Json in plain text
    $dt = Timestamp of activity
    */
    // [{"key":"affected_rma","value":10000,"type":"number"}]
    protected $fillable = [
        'reference_id', 'reference_type',
        'action', 'action_details', 'user_id',
    ];
    public function reference()
    {
        return $this->morphTo();
    }
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
    public function scopeBuildQuery($query)
    {
        return $query;
    }
    public static function storeData($data)
    {
        $data['action_time'] = Carbon::now();
        //$log                 = self::create($data);
        $obj = new Activity;
        $obj->reference_id = $data['reference_id'];
        $obj->reference_type = $data['reference_type'];
        $obj->action = $data['action'];

        if(!empty($data['action_details']))
            $obj->action_details = json_encode($data['action_details']);

        $obj->user_id = $data['user_id'];

        if ($obj->save()) {
            return true;
        } else {
            throw new Exception('Error Processing Request', 1);
        }
    }
    public static function add($type, $aid, $desc, $json, $dt)
    {
        $response=['status'=>false,'error'=>'','message'=>''];
        if (empty($type)) {
            $response['error']="Activity type is empty";
            return $response;
        }
        if (empty($aid)) {
            $response['error']="Activity id is empty";
            return $response;
        }
        if (empty($dt)) {
            $response['error']="Activity timestamp is empty";
            return $response;
        }
        $activity= new Activity;
        $activity->type=$type;
        $activity->activity_id=$aid;
        $activity->description=empty($desc)?null:$desc;
        $activity->raw_json=empty($json)?null:json_encode($json);
        $activity->time=$dt;
        if ($activity->save()) {
            $response['status']=true;
            $response['message']="Saved successfully";
            return $response;
        }
            $response['error']="Unable to save";
            return $response;
    }
}
