<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\User;

class BadLeadsFilter extends Model
{

	use ValidatingTrait;
	protected $table = 'bad_leads_filter';
	public $timestamps = false;

	private $rules = array(
			'field_name' => 'required'
		);
		
	
}
