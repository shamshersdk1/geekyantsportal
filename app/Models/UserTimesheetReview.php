<?php

namespace App\Models;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\BaseModel;


use Auth;
use DB;
use Config;

class UserTimesheetReview extends BaseModel
{
    protected $dates = ['deleted_at'];
    protected $table = 'user_timesheet_reviews';
    public $timestamps = true;
    use SoftDeletes;

    public function approver() {
        return $this->hasOne('App\Models\User', 'id', 'approver_id');
    }

    public function timesheet() {
        return $this->belongsTo('App\Models\UserTimesheet', 'id', 'user_timesheet_id');
    }


    public static function saveData($userTimesheetId, $approvedId, $approvedDuration, $approvalComments)
    {
        $status = true;
		$message = '';
        
        $reviewObj = new UserTimesheetReview();
        $reviewObj->user_timesheet_id = $userTimesheetId;
        $reviewObj->approver_id = $approvedId;
        $reviewObj->approved_duration = $approvedDuration;
        $reviewObj->approval_comments = $approvalComments;
        if ( !$reviewObj->save() )
        {
            $status = false;
            $message = $reviewObj->getErrors();
        }
        
		$response['status'] = $status;
		$response['message'] = $message;
		
		return $response;
    }

    public static function updateData($id, $approvedId, $approvedDuration, $approvalComments)
    {
        $status = true;
		$message = '';
        
        $reviewObj = UserTimesheetReview::find($id);
        if ( !empty($reviewObj) )
        {
            $reviewObj->approver_id = $approvedId;
            $reviewObj->approved_duration = $approvedDuration;
            $reviewObj->approval_comments = $approvalComments;
            if ( !$reviewObj->save() )
            {
                $status = false;
                $message = $reviewObj->getErrors();
            }
        }
        else{
            $status = false;
            $message = 'No record found';   
        }
        
		$response['status'] = $status;
		$response['message'] = $message;
		
		return $response;
    }

}
