<?php

namespace App\Models;

use App\Models\Admin\UserSetting;
use App\Models\User\UserTimesheetWeek;
use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;

class Week extends Model
{
 use ValidatingTrait;
 use SoftDeletes;
 protected $fillable = ['start_date', 'end_date'];

 public $timestamps = true;

 protected $rules = [
 ];
 public function formatWeek()
 {
  return date('D, jS F', strtotime($this->start_date)) . ' - ' . date('D, jS F', strtotime($this->end_date));
 }
 public function getStartDay()
 {
  return $this->start_date;
 }
 public function getEndDay()
 {
  return $this->end_date;
 }
 public function pending()
 {
  $authUser = Auth::User();
  if ($authUser->role == 'admin') {
   return $this->hasMany('App\Models\User\UserTimesheetWeek', 'week_id', 'id')->where('status', 'pending');
  } else {
   return $this->hasMany('App\Models\User\UserTimesheetWeek', 'week_id', 'id')->where('status', 'pending')->where('parent_id', $authUser->id);
  }

 }
 public function weekUser()
 {
  $authUser = Auth::User();
  if ($authUser->role == 'admin') {
   return $this->hasMany('App\Models\User\UserTimesheetWeek', 'week_id', 'id');
  } else {
   return $this->hasMany('App\Models\User\UserTimesheetWeek', 'week_id', 'id')->where('parent_id', $authUser->id);
  }
 }

 public static function addWeek($date)
 {

  if (date('D', strtotime($date)) === 'Mon') {
   $startDate = $date;
  } else {
   $startDate = date('Y-m-d', strtotime('previous monday', strtotime($date)));
  }
  if (date('D', strtotime($date)) === 'Sun') {
   $endDate = $date;
  } else {
   $endDate = date('Y-m-d', strtotime('next sunday', strtotime($date)));
  }
  // $dayOfTheWeek = Carbon::now()->dayOfWeek;
  // $startDate = Carbon::now()->subDays($dayOfTheWeek - 1);
  // $endDate = Carbon::now()->addDays(7 - $dayOfTheWeek);

  $week = Week::where('start_date', $startDate)->first();

  if (!$week) {
   $week = Week::firstOrCreate(['start_date' => $startDate], ['end_date' => $endDate]);
   if ($week) {
    $users = User::where('is_active', 1)->where('parent_id', '!=', null)->get();
    foreach ($users as $user) {
     $timesheetAllowed = UserSetting::where('user_id', $user->id)->where('key', 'user_timesheet_reminder')->first();
     if (!$timesheetAllowed || $timesheetAllowed->value == 1) {
      $userWeeklyTimeobj            = new UserTimesheetWeek;
      $userWeeklyTimeobj->week_id   = $week->id;
      $userWeeklyTimeobj->user_id   = $user->id;
      $userWeeklyTimeobj->parent_id = $user->parent_id;
      $userWeeklyTimeobj->save();

     }
    }
   }
  }

  return $week;
 }
}
