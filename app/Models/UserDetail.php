<?php
namespace App\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;
use Watson\Validating\ValidatingTrait;

class UserDetail extends BaseModel implements Auditable
{
    use ValidatingTrait;
    use SoftDeletes;
    use \OwenIt\Auditing\Auditable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    // protected $dates = ['deleted_at'];

    private $rules = array(
        'user_id' => 'required|exists:users,id',
    );

    protected $fillable = ['user_id', 'aadhar_no', 'pan', 'bank_ac_no', 'bank_ifsc_code', 'pf_no', 'uan_no', 'esi_no'];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
