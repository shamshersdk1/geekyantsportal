<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\UserEvent;
use Auth;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{
    use SoftDeletes;
    protected $table = 'events';
    protected $fillable = ['name'];

    public function user_event()
    {
        return $this->hasMany(UserEvent::class);
    }
    
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function creator()
    {
        return $this->belongsTo('App\Models\User', 'created_by');
    }


    public static function saveData($data)
    {
        $response['status'] = false;
        $response['message'] = null;
        $response['data'] = null;
        if($data){
            $event=new Event;
            $event->img_url=!empty($data['img_url'])?$data['img_url'] : null;
            $event->title = $data['title'];
            $event->name = $data['name'];
            $event->location = $data['location'];
            $event->date = $data['event_date'];
            $event->description = $data['description'];
            $event->url = $data['event_link'];
            $event->logo_url = $data['logo_url'];
            $event->blog_url = $data['blog_url'];
            $event->video_link = $data['video_link'];
            if(strtotime($data['event_date']) < strtotime(date("Y/m/d")))
                $event->type = "past";
            else    
                $event->type = "upcoming";
            $event->created_by = Auth::id();
            if(!$event->save()){
                $response['message'] = $event->getErrors();
                $response['status'] = true;
                return $response;
            }
            $response['status'] = true;
            $response['message'] = "Event Added Successfully";
            $response['data'] = $event->id;
            return $response;
        }
        
        $response['message'] = "Failed to Add";
        $response['data'] = null;
        return $response;

    }
    
    public static function updateData($data,$id)
    {
        $response['status'] = false;
        $response['message'] = null;
        $response['data'] = null;
        $event = Event::find($id);
        if($event){
            $event->img_url=$data['img_url'];
            $event->title = !empty($data['title'])?$data['title']:null;
            $event->name = $data['name'];
            $event->location = $data['location'];
            $event->date = $data['event_date'];
            $event->description = $data['description'];
            $event->url = $data['event_link'];
            $event->logo_url = $data['logo_url'];
            $event->blog_url = $data['blog_url'];
            $event->video_link = $data['video_link'];
                        if(strtotime($data['event_date']) < strtotime(date("Y/m/d")))
                $event->type = "past";
            else    
                $event->type = "upcoming";
            $event->created_by = Auth::id();
            if(!$event->save()){
                $response['message'] = $event->getErrors();
                $response['status'] = true;
                return $response;
            }
            $response['status'] = true;
            $response['message'] = "Event Updated Successfully";
            $response['data'] = $event->id;
            return $response;
        }
        $response['message'] = "Failed to Update";
        $response['data'] = null;
        return $response;
    }
}
