<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\User;

class Seat extends Model
{

	use ValidatingTrait;
	protected $table = 'seats';
	public $timestamps = false;

	private $rules = array(
			'name' => 'required'
		);
		
	public function user() {
        return $this->hasOne('App\Models\User', 'id', 'user_id' );
    }

	public function user_profile()
    {
        return $this->hasOne('App\Models\Profile', 'user_id', 'user_id');
    }

	public static function floorList()
	{
		$result = [];
		$floors = Seat::select('floor')->distinct()->orderBy('floor')->get()->toArray();

		foreach( $floors as $floor )
		{
			$floorObject = [];
			$floorObject['floor'] = $floor['floor'];
			$floorObject['details'] = self::floorDetails($floorObject['floor']);
			$result[] = $floorObject;
		}
		
		return $result;
	}

	public static function isUserAllocated($user_id)
	{
		$all_seats = Seat::all();
		$flag = false;
		$allocated_seat = '';
		$result = [];
		
		if ( !empty($all_seats) )
		{
			foreach( $all_seats as $seat )
			{
				if ( $seat->user_id == $user_id )
				{
					$flag = true;
					$allocated_seat = $seat->name;
				}
			}
		}
		$result['flag'] = $flag;
		$result['allocated_seat'] = $allocated_seat;
		return $result;
	}

	public static function floorDetails($floor)
	{
		$occupied_seats = 0;
		$empty_seats = 0;
		$total_seats = Seat::where('floor',$floor)->get()->count();
		$empty_seats = Seat::where('floor',$floor)->whereNull('user_id')->get()->count();
		$occupied_seats = $total_seats - $empty_seats;

		$result = [];
		$result['total_seats'] = $total_seats;
		$result['occupied_seats'] = $occupied_seats;
		$result['empty_seats'] = $empty_seats;
		return $result;
	}
}
