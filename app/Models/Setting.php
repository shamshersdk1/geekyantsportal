<?php namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;
use Auth;

class Setting extends Model {

	
	use ValidatingTrait;
	protected $table = 'settings';
	public $timestamps = false;

	private $rules = array(
	        'user_id' => 'required',
	        'username'  => 'required',
	        'password'  => 'required'
	    );

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	// protected $fillable = ['name', 'email', 'password'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	public static function saveData($data) {

		$response['status'] = true;

		$setting = Setting::where('user_id',Auth::user()->id)->first();

		if(!$setting){
			$setting = new Setting;
		}
		$setting->username = $data['username'];
		$setting->password = $data['password'];
		$setting->user_id = Auth::user()->id;
		if(!$setting->save()){
			throw new Exception("Could not save setting", 1);
		}
		return true;
	}
}
