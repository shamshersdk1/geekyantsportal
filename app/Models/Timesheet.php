<?php

namespace App\Models;
use App\Services\LeaveService;
use Watson\Validating\ValidatingTrait;

use Illuminate\Database\Eloquent\Model;

use App\Models\User;
use App\Models\Admin\Project;
use App\Models\TimesheetDetail;
use Mail;

class Timesheet extends Model
{

	use ValidatingTrait;

    protected $table = 'timesheets';
    public $timestamps = true;
    private $rules = array(
            'user_id' => 'required',
            'project_id' => 'required',
            'date' => 'required',
            'hours' => 'required',
        );

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id' );
    }

    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id', 'id' );
    }

    public function approver()
    {
        return $this->belongsTo(User::class, 'approver_id', 'id' );
    }
    public function timesheet_detail()
    {
        return $this->hasMany(TimesheetDetail::class, 'timesheet_id', 'id' );
    }
    public function sumOfHours(){
        $total_hours = $this->timesheet_detail->where('timesheet_id', $this->id)->sum('duration');
        return $total_hours;
    }
    public static function saveTimelog($data) {
        $response= [];
        if(empty($data['date']) || empty($data['project_id']) || empty('timesheet_details')) {
            $response['status'] = false;
            $response['message'] = "Invalid inputs";
        }
        
        $date = $data['date'];
        $projectId = $data['project_id'];
        $timesheetDetails = $data['timesheet_details'];
        $hours = 0;
        $project = ProjectResource::where('user_id',$user_id)
							 ->where('project_id',$project_id)
                             ->first();
        if(!$project) {
            $response['status'] = false;
            $response['message'] = "Project #".$project->id." not assigned to logged-in user";
        }

        $timesheet = Timesheet::where('user_id', $userId)
                ->where('project_id', $projectId)
                ->where('date','=', $date)
                ->first();
        if($timesheet) {
            $hours = $timesheet->sumOfHours();
            $response['status'] = false;
            $response['message'] = "false";
        }
    }

    public static function createTimesheet( $project, $selected_date, $hours )
    {
        $timesheetObj = new Timesheet();
		$timesheetObj->user_id = $project->user_id;
		$timesheetObj->project_id = $project->project_id;
		$timesheetObj->date = $selected_date;
		$timesheetObj->hours = $hours;
		$timesheetObj->save();
        return $timesheetObj;
    }
    public static function createTimesheetFromTmpTable( $tmpTimesheetObj )
    {
        $timesheetObj = new Timesheet();
		$timesheetObj->user_id = $tmpTimesheetObj->user_id;
		$timesheetObj->project_id = $tmpTimesheetObj->project_id;
		$timesheetObj->date = $tmpTimesheetObj->date;
		$timesheetObj->hours = $tmpTimesheetObj->duration;
		$timesheetObj->save();
        return $timesheetObj;
    }
}
