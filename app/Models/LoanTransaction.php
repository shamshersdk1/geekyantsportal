<?php

namespace App\Models;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;


class LoanTransaction extends Model
{
    use ValidatingTrait;
	protected $table = 'loan_transactions';
	public $timestamps = true;
	public $fillable = ['loan_id','amount','type','status','comment','paid_on'];

    private $rules = array(
            'loan_id'   => 'required',
            'amount'    => 'required',
            'status'    => 'required'
        );

    public function loan()
    {
        return $this->belongsTo(Loan::class, 'loan_id', 'id');
    }
    public static function saveToDB($data) {
        
        $reponse['status'] = true;
        $reponse['message'] = "";
        
        if(empty($data['loan_id']) || empty($data['amount'])) {
            $reponse['status'] = false;
            $reponse['message'] = "Invalid data";
            return $reponse;
        }
        $data['status'] = 'paid';
        $obj = new self;
        $obj->fill($data);
        if(!$obj->save()) {
            $err = $obj->getErrors();
            $reponse['status'] = false;
            $reponse['message'] = $err->toArray();
        } else {
            \Log::info('LoanTransaction saved');
        }
        return $reponse;
    }
}
