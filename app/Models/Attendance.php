<?php

namespace App\Models;
use App\Services\LeaveService;
use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\Model;

use Mail;

class Attendance extends Model
{

	use ValidatingTrait;

    protected $table = 'attendance';
    public $timestamps = false;
    private $rules = array(
            'date' => 'required'
        );

    public function user_attandance() {
        return $this->hasMany('App\Models\UserAttendance', 'attendance_id', 'id' );
    }

}
