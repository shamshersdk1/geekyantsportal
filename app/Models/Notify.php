<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Location;

class Notify extends Model
{
    use SoftDeletes;
    protected $table = 'notify';
    public $timestamps = true;

    public static function saveData($action, $payload){
        $locations = Location::all();
        if(count($locations) > 0) {
            foreach ($locations as $location) {
                $obj = new Notify;
                $obj->location_id = $location->id;
                $obj->action = $action;
                $obj->payload = json_encode($payload);  
                $obj->save();  
            }
        }
        return true;
    }

    // action = user_create
    // action = user_edit
    // action = user_delete

    //payload = ['action'=>,email'=>,'first_name'=>,'last_name'=>,'password'=>'']

    //action = dhcp_sync
    //payload = ['dhcp_changed' => 1]


}
