<?php namespace App\Models\Leave;

use App\Models\Admin\CalendarYear;
use App\Models\BaseModel;
use App\Models\Leave\UserLeaveTransaction;
use App\Models\User;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserLeaveBalance extends BaseModel
{

    protected $table = 'user_leave_balances';
    public $timestamps = true;
    use SoftDeletes;

    public function scopeBuildQuery($query)
    {
        return $query;
    }

    public function calendarYear()
    {
        return $this->belongsTo('App\Models\Admin\CalendarYear', 'calendar_year_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public static function addUserLeaves($data)
    {
        if (empty($data)) {
            return false;
        }
        $user = User::find($data['user_id']);
        if (!$user) {
            return false;
        }
        if (empty($data['calendar_year_id'])) {
            $calendarObj = CalendarYear::where('year', date('Y'))->first();
            if (!$calendarObj) {
                return false;
            }
            $data['calendar_year_id'] = $calendarObj->id;
        }
        $carryForward = 0;
        $checkIfExists = UserLeaveTransaction::where('calendar_year_id', $data['calendar_year_id'])->where('user_id', $data['user_id'])->where('reference_type', $data['reference_type'])->where('reference_id', $data['reference_id'])->where('leave_type_id', $data['leave_type_id'])->first();
        if ($data['reference_type'] == 'App\Models\Leave\LeaveCalendarYear' && $checkIfExists) {
            return false;
        }
        return UserLeaveTransaction::saveData($data);
    }
}
