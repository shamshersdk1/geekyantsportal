<?php
namespace App\Models\Leave;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class LeaveCalendarYear extends BaseModel
{

    protected $table = 'leave_calendar_years';
    public $timestamps = true;
    use SoftDeletes;

    public function scopeBuildQuery($query)
    {
        return $query;
    }
    public function calendarYear()
    {
        return $this->belongsTo('App\Models\Admin\CalendarYear', 'calendar_year_id', 'id');
    }
    public function leaveType()
    {
        return $this->belongsTo('App\Models\Admin\LeaveType', 'leave_type_id', 'id');
    }
}
