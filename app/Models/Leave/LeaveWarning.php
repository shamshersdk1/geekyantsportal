<?php

namespace App\Models\Leave;

use Illuminate\Database\Eloquent\Model;

class LeaveWarning extends Model
{

    public function warnings()
    {
        return $this->hasOne('App\Models\Leave\Leave', 'leave_id', 'id');
    }

    public static function saveData($leaveId, $warning)
    {
        $status = true;

        $warningObj = new LeaveWarning();

        $warningObj->leave_id = $leaveId;
        $warningObj->warning = $warning;

        if (!$warningObj->save()) {
            $status = false;
        }

        return $status;
    }
}
