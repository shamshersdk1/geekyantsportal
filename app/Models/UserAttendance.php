<?php

namespace App\Models;

use App\Models\Admin\Leave;
use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class UserAttendance extends Model
{

    use ValidatingTrait;

    protected $table = 'user_attendance';
    public $timestamps = true;
    private $rules = array(
        'user_id' => 'required',
    );

    protected $auditInclude = [
        'is_onsite',
        'is_wfh',
        'is_onsite_meeting',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function getConnectionHistoryByAsset($asset_id)
    {
        return GatewayIpConnection::where([['date', '=', $this->date], ['user_id', '=', $this->user_id], ['asset_id', '=', $asset_id]])->get();
    }

    public function getAssets()
    {
        $asset = [];
        $assetsConnection = GatewayIpConnection::where([['date', '=', $this->date], ['user_id', '=', $this->user_id]])->groupBy('asset_id')->get();
        foreach ($assetsConnection as $row) {
            $asset[] = array('asset' => $row->getAsset(), 'location' => $row->gateway_id);
        }
        return $asset;
    }

    public static function getReport($date)
    {

        $users_ignore = [];
        $viewData = [];
        $viewData['date'] = $date;
        $users_attendance = UserAttendance::with('user')->where('date', $viewData['date'])->get();
        $viewData['active_users'] = [];
        $viewData['users_on_leave'] = Leave::with(['leaveType', 'user'])->where('status', 'approved')->where('start_date', '<=', $date)->where('end_date', '>=', $date)->orderBy('start_date')->get()->keyBy('user_id');

        $users = User::where([['is_active', '=', 1], ['is_attendance_enabled', '=', 1]])->get()->keyBy('id');

        foreach ($users_attendance as $each_user) {
            $primaryAssets = [];
            $otherAssets = [];
            $location = 'Unknow';
            $is_on_leave = 0;

            $assets = $each_user->getAssets();
            foreach ($assets as $asset) {
                //\Log::info(print_r($asset, true));
                if ($asset['asset']->asset_category_id == 1) {
                    $primaryAssets[] = $asset['asset'];
                } else {
                    $otherAssets[] = $asset['asset'];
                }

                $location_id = $asset['location'];
                if ($location_id == 1) {
                    $location = 'Old Office';
                } else if ($location_id == 2) {
                    $location = 'New Office';
                }
            }

            if ($users[$each_user->user->id]) {
                //\Log::info('Delete:'.$each_user->user->id);
                $users_ignore[$each_user->user->id] = 1;
            }

            $is_on_leave = ($viewData['users_on_leave'][$each_user->user->id]) ? 1 : 0;

            $viewData['active_users'][] = array('UserAttendance' => $each_user, 'PrimaryAssets' => $primaryAssets, 'OtherAssets' => $otherAssets, 'Location' => $location, 'is_on_leave' => $is_on_leave);

        }

        $non_active_users = [];
        foreach ($users as $user_id => $user) {
            if ($users_ignore[$user_id] == 1) {
                continue;
            }

            if ($viewData['users_on_leave'][$user_id]) {
                continue;
            }

            $non_active_users[$user_id] = $user;
        }

        $viewData['non_active_users'] = $non_active_users;

        return $viewData;
    }

    public static function createUserAttendanceMarkedAs($user_id, $date, $mark)
    {
        // We need to enable audit for user actions, once server is merged - we need to update here again.
        $userAttendance = UserAttendance::where([['user_id', '=', $user_id], ['date', '=', $date]])->first();
        if (!$userAttendance) {
            $userAttendance = new UserAttendance();
            $userAttendance->user_id = $user_id;
            $userAttendance->date = $date;
        }
        switch ($mark) {
            case 'is_onsite':$userAttendance->is_onsite = 1;
                break;
            case 'is_wfh':$userAttendance->is_wfh = 1;
                break;
            case 'is_onsite_meeting':$userAttendance->is_onsite_meeting = 1;
                break;
            default:
                \Log::info('Invalid createUserAttendanceMarkedAs - MarkedAs - ' . $mark);
                return false;
        }

        if ($userAttendance->save()) {
            return true;
        }

        \Log::info('Unable to Save - createUserAttendanceWithType');
        return false;
    }

    public static function capture($user_id, $count, $date, $hr, $min)
    {
        //\Log::info('UserAttendance Capture? '.$user_id);
        $userAttendance = UserAttendance::where([['user_id', '=', $user_id], ['date', '=', $date]])->first();
        if ($userAttendance) {
            if ($count > 10) {
                $userAttendance->increment('active_count');
            }

            $userAttendance->increment('tick_count');

            if (strtotime($userAttendance->in_time) < strtotime($date . " 06:00:00")) {
                $userAttendance->in_time = $date . " " . $hr . ":" . $min . ":00";
                if (!$userAttendance->save()) {
                    \Log::info('Unable to update Login Time - after 6am');
                }
            }
        } else {
            $obj = new UserAttendance();
            $obj->user_id = $user_id;
            $obj->date = $date;
            $obj->in_time = $date . " " . $hr . ":" . $min . ":00";
            $obj->tick_count = 1;
            $obj->active_count = 0;
            if ($count > 10) {
                $obj->active_count = 1;
            }

            if (!$obj->save()) {
                \Log::info('Unable to save - UserAttendance::capture');
            }
        }
    }
}
