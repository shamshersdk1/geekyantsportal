<?php

namespace App\Models;

use App\Models\Admin\PayslipCsvData;
use App\Models\Admin\PayslipCsvMonth;
use App\Models\MonthSetting;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;

class LoanEmi extends Model
{
    use SoftDeletes;
    public function loan()
    {
        return $this->belongsTo(Loan::class, 'loan_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function month()
    {
        return $this->belongsTo('App\Models\Month', 'month_id', 'id');
    }

    public function appraisalBonus()
    {
        return $this->belongsTo('App\Models\Appraisal\AppraisalBonus');
    }
    public static function approveLoanEmi(Request $request, $id)
    {
        $response = ['status' => false, 'message' => ''];
        $emi_id = empty($request->emi_id) ? null : $request->emi_id;
        $amount = empty($request->amount) ? null : $request->amount;
        if (is_null($emi_id) || is_null($amount)) {
            $response['message'] = 'Incomplete request';
            return $response;
        }
        $csvMonth = PayslipCsvMonth::find($id);
        if (!$csvMonth) {
            $response['message'] = 'Invalid link followed';
            return $response;
        } else {
            $emi = LoanEmi::find($emi_id);
            if (empty($emi)) {
                $response['message'] = 'Invalid link followed';
                return $response;
            } else {
                if ($amount > $emi->amount) {
                    $response['message'] = 'Amount can\'t be greater than emi';
                    return $response;
                }
                $current_amount = is_null($emi->actual_amount) ? 0 : $emi->actual_amount;
                $emi->actual_amount = $amount;
                $emi->status = "processing";
                $emi->comment = "Approved by user_id " . \Auth::id();
                $emi->save();
                if (PayslipCsvData::addLoan($id, $emi_id, $current_amount)) {
                    $response = ['status' => true, 'message' => 'Emi confirmed'];
                    return $response;
                } else {
                    $response['message'] = 'Payment details saved but unable to add loan to payslip, please modify manually';
                    return $response;
                }
                // $emi->status = "paid";
                // $emi->paid_on = date("Y-m-d");
                // $emi->comment = "Deducted from Salary";
                // if($amount != $emi->amount) {
                //     $diff = $emi->amount - $amount;
                //     if(LoanEmi::alterEmi($emi_id, $diff)) {
                //         $emi->amount = $amount;
                //         // $emi->save();
                //         if(PayslipCsvData::addLoan($id, $emi_id)) {
                //             return $this->successResponse('Emi confirmed');
                //         } else {
                //             return $this->failResponse('Payment details saved but unable to add loan to payslip, please modify manually');
                //         }
                //     }
                // } else {
                //     // $emi->save();
                //     if(PayslipCsvData::addLoan($id, $emi_id)) {
                //         return $this->successResponse('Emi confirmed');
                //     } else {
                //         return $this->failResponse('Payment details saved but unable to add loan to payslip, please modify manually');
                //     }
                //     return $this->successResponse('Emi confirmed');
                // }
            }
        }
    }

    public static function rejectLoanEmi(Request $request, $id)
    {
        $response = ['status' => false, 'message' => ''];
        $emi_id = empty($request->emi_id) ? null : $request->emi_id;
        if (is_null($emi_id)) {
            $response['message'] = 'Incomplete request';
            return $response;
        }
        $csvMonth = PayslipCsvMonth::find($id);
        if (!$csvMonth) {
            $response['message'] = 'Invalid link followed';
            return $response;
        } else {
            $emi = LoanEmi::find($emi_id);
            if (empty($emi)) {
                $response['message'] = 'Invalid link followed';
                return $response;
            } else {
                $current_amount = is_null($emi->actual_amount) ? 0 : $emi->actual_amount;
                $emi->actual_amount = 0;
                $emi->status = "rejected";
                $emi->comment = "Rejected by user_id " . \Auth::id();
                $emi->save();
                if (PayslipCsvData::addLoan($id, $emi_id, $current_amount)) {
                    $response = ['status' => true, 'message' => 'Emi rejected'];
                    return $response;
                } else {
                    $response['message'] = 'Payment details saved but unable to add loan to payslip, please modify manually';
                    return $response;
                } // $emi->status = "paid";
                // $emi->paid_on = date("Y-m-d");
                // $emi->comment = "Moved to a new date";
                // $amount = 0;
                // $diff = $emi->amount - $amount;
                // if(LoanEmi::alterEmi($id, $diff)) {
                //     $emi->amount = 0;
                //     $emi->save();
                //     return $this->successResponse('Emi rejected');
                // }
            }
        }
    }

    public static function alterEmi($id, $diff)
    {
        $emi = LoanEmi::find($id);
        if ($emi) {
            $loan = $emi->loan;
            if ($diff > 0) {
                $last = LoanEmi::where('loan_id', $loan->id)->where('status', 'pending')->orderBy('due_date', 'desc')->first();
                switch ($loan->type) {
                    case "monthly":$date = date("Y-m-25", strtotime("+1 month", strtotime($last->due_date)));
                        break;
                    case "qtr":$date = date("Y-m-25", strtotime("+3 months", strtotime($last->due_date)));
                        break;
                    case "annual":$date = date("Y-m-25", strtotime("+1 year", strtotime($last->due_date)));
                        break;
                    case "default":return false;
                }
                $new = new LoanEmi();
                $new->loan_id = $loan->id;
                $new->amount = $diff;
                $new->status = "pending";
                $new->due_date = $date;
                if ($new->save()) {
                    return true;
                }
            } else {
                $diff = $diff * -1;
                while ($diff > 0) {
                    $last = LoanEmi::where('loan_id', $loan->id)->where('status', 'pending')->orderBy('due_date', 'desc')->first();
                    if ($diff < $last->amount) {
                        $last->amount = $diff;
                    }
                    $last->status = "approved";
                    $last->paid_on = date();
                    $last->comment = "Paid alongwith emi with id: " . $id;
                    $last->save();
                    $diff = $diff - $last->amount;
                }
                if ($diff == 0) {
                    return true;
                }
            }
        }
        return false;
    }
    public static function getEmis($monthId)
    {
        $emis = LoanEmi::where('month_id', $monthId)->get();
        $month = Month::find($monthId);

        if ($month && $month->loan_check) {
            return $emis;
        }

        if (count($emis) > 0) {
            return $emis;
        } else {
            $loans = Loan::where('remaining', '>', 0)->get();
            if (count($loans) > 0) {
                foreach ($loans as $loan) {
                    $obj = new LoanEmi;
                    $obj->amount = -$loan->emi;
                    $obj->loan_id = $loan->id;
                    $obj->month_id = $monthId;
                    $obj->user_id = $loan->user_id;
                    $obj->actual_amount = $loan->amount;
                    $obj->due_date = $month->getLastDay();
                    $obj->status = 'pending';
                    $obj->save();
                }
                $emis = LoanEmi::where('month_id', $monthId)->get();
            }
        }
        return $emis;
    }

    public static function regenerateEmi($month_id)
    {
        $response['status'] = false;
        $response['message'] = null;
        $response['data'] = null;
        $monthObj = Month::find($month_id);
        if (!$monthObj) {
            $response['message'] = "Invalid month id " . $month_id;
            return $response;
        }
        if ($monthObj->vpfSetting == 'locked') {
            $response['message'] = "Cannot regenrate. Month is locked";
            return $response;
        }
        LoanEmi::where('month_id', $month_id)->delete();
        $loans = Loan::where('status', 'in_progress')->WhereHas('appraisalBonus', function (Builder $query) use ($monthObj) {
            $query->whereDate('value_date', '>=', $monthObj->getFirstDay())->whereDate('value_date', '<=', $monthObj->getLastDay());
        })->orWhereNull('appraisal_bonus_id')->where('status', 'in_progress')->get();
        $errors = [];
        foreach ($loans as $loan) {
            $deductionObj = new LoanEmi();
            $deductionObj->month_id = $month_id;
            $deductionObj->loan_id = $loan->id;
            $deductionObj->user_id = $loan->user_id;
            $deductionObj->amount = -$loan->emi;
            $deductionObj->actual_amount = $loan->amount;
            $deductionObj->appraisal_bonus_id = $loan->appraisal_bonus_id;
            $deductionObj->status = "pending";
            $deductionObj->due_date = $monthObj->getLastDay();
            //$deductionObj->method = "NA";
            if (!$deductionObj->save()) {
                $errors[] = $deductionObj->getErrors();
            }
        }
        $response['status'] = true;
        $response['message'] = "Loan deduction regenerated";
        $response['data'] = null;
        return $response;
    }

    public static function updateData($data, $monthId)
    {
        $response['status'] = false;
        $response['message'] = null;
        $response['data'] = null;
        foreach ($data as $index => $loan) {
            $deductionObj = LoanEmi::find($index);
            $deductionObj->amount = $data[$index];
            if (!$deductionObj->save()) {
                $errors[] = $deductionObj->getErrors();
                return $response;
            }
        }
        $response['status'] = true;
        $response['message'] = "Loan deduction regenerated";
        $response['data'] = null;
        return $response;
    }

    public static function getCurrMonthDeduction($monthId, $userId)
    {
        $loan = 0;
        $checkLock = MonthSetting::where('key', 'loan-deduction')->first();
        if ($checkLock && $checkLock->value == 'locked') {
            $loan = self::where('month_id', $monthId)->where('user_id', $userId)->sum('amount');
            return $loan;
        }
        return null;
    }

}
