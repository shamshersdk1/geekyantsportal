<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Event;
use App\Models\User;
use App\Models\UserEventAttachment;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserEvent extends Model
{
    use SoftDeletes;
    protected $table = 'user_events';
    protected $fillable = ['user_id','event_id','added_on','approved_by','uploaded_by','note'];

    public function event()
    {
        return $this->belongsTo(Event::class);
    }
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
    public function approver()
    {
        return $this->hasOne(User::class, 'id', 'approved_by');
    }
    public function uploader()
    {
        return $this->hasOne(User::class, 'id', 'uploaded_by');
    }
    public function attachments()
    {
        return $this->hasMany(UserEventAttachment::class, 'user_event_id', 'id');
    }

    public static function saveData($data,$id){
        $response['status'] = false;
        $response['message'] = null;
        $response['data'] = null;
        if($data && $id)
        {
            foreach($data as $obj)
            {
                $eventUserObj = new UserEvent();
                $eventUserObj->user_id = $obj;
                $eventUserObj->event_id = $id;
                if(!$eventUserObj->save()){
                    $response['message'] = $event->getErrors();
                    $response['status'] = true;
                    return $response;    
                }
            }
            $response['status'] = true;
            $response['message'] = "User Event Added Successfully";
            $response['data'] = null;
            return $response;
        }
        $response['message'] = "Failed to Add User Event";
        $response['data'] = null;
        return $response;

    }

    public static function updateData($data,$event_id)
    {
        $response['status'] = false;
        $response['message'] = null;
        $response['data'] = null;
        if($data && $event_id)
        {
            $deleteRows = UserEvent::where('event_id',$event_id)->delete();
            foreach($data as $obj)
            {
                $eventUserObj = new UserEvent();
                $eventUserObj->user_id = $obj;
                $eventUserObj->event_id = $event_id;
                if(!$eventUserObj->save()){
                    $response['message'] = $event->getErrors();
                    $response['status'] = true;
                    return $response;    
                }
            }
          //  die;
            $response['status'] = true;
            $response['message'] = "User Event Updated Successfully";
            $response['data'] = null;
            return $response;
        }
        $response['message'] = "Failed to Update User Event";
        $response['data'] = null;
        return $response;
    }
}
