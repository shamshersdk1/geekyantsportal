<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\User;

use App\Jobs\HireMail;

class HireEmail extends Model
{

	use ValidatingTrait;
	protected $table = 'hire_email';
	public $timestamps = false;

	private $rules = array(
		'name' => 'required',
        'company' => 'required',
        'email' => 'required|email',
        'referred_by' => 'required',
        'requirement' => 'required'
		);

		public static function reviewAction($action, $id)
		{
			$response = ['status' => false, 'message' => ""];
			$email = HireEmail::find($id);
			if($email) {
				if($action == "spam") {
					$email->review_status = "spam";
					$email->save();
					$response['message'] = "Marked as spam";
				} elseif($action == "send") {
					$email->review_status = "reviewed";
					$email->save();
					$response['message'] = "Email sent";
					$job = (new HireMail($id))->onQueue('hire-us-email');
					dispatch($job);
				} elseif($action == "next") {
					$email->review_status = "reviewed";
					$email->save();
				} else {
					$response['message'] = "Invalid request";
					return $response;
				}
				$response['status'] = true;
				return $response;
			} else {
				$response['message'] = "Invalid link followed";
				return $response;
			}
		}
}
