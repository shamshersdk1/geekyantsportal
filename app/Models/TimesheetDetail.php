<?php

namespace App\Models;
use App\Services\LeaveService;
use Watson\Validating\ValidatingTrait;

use Illuminate\Database\Eloquent\Model;

use App\Models\Timesheet;
use App\Models\Admin\Project;
use Mail;

class TimesheetDetail extends Model
{

	use ValidatingTrait;

    protected $table = 'timesheet_details';
    public $timestamps = true;
    private $rules = array(
            'timesheet_id' => 'required',
            'duration' => 'required',
            'reason' => 'required',
        );

    public function timesheet()
    {
        return $this->belongsTo(Timesheet::class, 'timesheet_id', 'id' );
    }

    public static function saveTimesheetDetails($timesheet_id, $timesheet_details)
    {
        $response = [];
        $status = true;
        $message = '';
        $deletedRows = TimesheetDetail::where('timesheet_id',$timesheet_id)
						        ->delete();
        if ( count($timesheet_details) > 0 ){
            foreach ($timesheet_details as $timesheet_detail){
                if ( $timesheet_detail['duration'] && $timesheet_detail['reason'] ){
                    $timesheetDetailObj = new TimesheetDetail();
                    $timesheetDetailObj->timesheet_id = $timesheet_id;
                    $timesheetDetailObj->duration = $timesheet_detail['duration'];
                    $timesheetDetailObj->reason = $timesheet_detail['reason'];
                    if( !$timesheetDetailObj->save() )
                    {
                        $status = false;
                        $message = 'Error while updating timesheet detail';
                    }
                }
            }
        }
        $timesheet_update = Timesheet::where('id',$timesheet_id)->first();
        $hours = $timesheet_update->sumOfHours();  
        $timesheet_update->approved_hours = $hours;
        if( !$timesheet_update->save() )
        {
            $status = false;
            $message = 'Error while updating timesheet hours';
        }    
        $response['status'] = $status;
        $response['message'] = $message;
        return $response;
    }
    
    public static function createTimesheetDetailFromTempTable($timesheet_id, $tempTableObj)
    {
        $timesheetDetailObj = new TimesheetDetail();
        $timesheetDetailObj->timesheet_id = $timesheet_id;
        $timesheetDetailObj->duration = $tempTableObj->duration;
        $timesheetDetailObj->reason = $tempTableObj->task;
        $timesheetDetailObj->save();
        return $timesheetDetailObj;
    }
}
