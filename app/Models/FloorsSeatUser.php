<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;
use App\Models\User;

class FloorsSeatUser extends Model
{

	use ValidatingTrait;
	protected $table = 'floors_seat_users';
	public $timestamps = true;
	private $rules = array(
			'user_id' => 'required'
		);
	
	public function floors_seat() {
        return $this->belongsTo('App\Models\FloorsSeat', 'floor_seat_id', 'id');
    }
	public function assigned_user() {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
	public function user_profile()
    {
        return $this->hasOne('App\Models\Profile', 'user_id', 'user_id');
    }
	public static function freeAllocatedUser($user_id)
	{
		$all_seats = FloorsSeatUser::with('floors_seat')->get();
		$flag = false;
		$allocated_seat = '';
		$result = [];
		
		if ( !empty($all_seats) )
		{
			foreach( $all_seats as $seat )
			{
				if ( $seat->user_id == $user_id )
				{
					$flag = true;
					$seat->delete();
				}
			}
		}
	}
}
