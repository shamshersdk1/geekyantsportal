<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;
use App\Models\Admin\Role;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Support\Arr;
class UserRole extends Model implements Auditable {


	use ValidatingTrait;
	use \OwenIt\Auditing\Auditable;
	protected $table = 'user_roles';
	public $timestamps = false;

	private $rules = array(
	        // 'role' => 'required',
	        // 'description'  => 'required'
	    );

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	// protected $fillable = ['name', 'email', 'password'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	//public
	public function transformAudit(array $data): array
    {
        if (Arr::has($data, 'new_values.role_id')) {
            $data['old_values']['role'] = Role::find($this->getOriginal('role_id'))->name;
            $data['new_values']['role'] = Role::find($this->getAttribute('role_id'))->name;
        }
        unset($data['old_values']['role_id']);
        unset($data['new_values']['role_id']);
        return $data;
    }
	public static function createUserRole($userId, $role)
	{
		$userRoleObj = Role::where('code', $role)->first();
		if($userRoleObj->id) {
			$userRole = new UserRole();
			$userRole->user_id = $userId;
			$userRole->role_id = $userRoleObj->id;
			if($userRole->save()) {
				return true;
			}
		}
		return false;
	}
}