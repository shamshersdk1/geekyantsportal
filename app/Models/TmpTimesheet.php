<?php

namespace App\Models;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\BaseModel;


use Auth;
use DB;
use Config;

class TmpTimesheet extends BaseModel
{
    protected $dates = ['deleted_at'];
    protected $table = 'tmp_timesheet';
    public $timestamps = true;
    use SoftDeletes;

    public function user() {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    public function project() {
        return $this->hasOne('App\Models\Admin\Project', 'id', 'project_id');
    }


    public static function saveData($userId, $projectId, $channelId, $date, $duration, $task)
    {
        $status = true;
		$message = '';
        
        $tmpTimesheetObj = new TmpTimesheet();
        $tmpTimesheetObj->user_id = $userId;
        $tmpTimesheetObj->project_id = $projectId;
        $tmpTimesheetObj->channel_id = $channelId;
        $tmpTimesheetObj->date = $date;
        $tmpTimesheetObj->duration = $duration;
        $tmpTimesheetObj->task = $task;
        if( !$tmpTimesheetObj->save() )
        {
            $status = false;
            $message = $tmpTimesheetObj->getErrors();
        }else {
            $message = $tmpTimesheetObj->id;
        }

		$response['status'] = $status;
		$response['message'] = $message;
		
		return $response;
    }

}
