<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserEventAttachment extends Model
{
    protected $table = 'user_event_attachments';
    protected $fillable = ['user_event_id','attachment'];

    public function userevent()
    {
        return $this->belongsTo(UserEvent::class, 'user_event_id', 'id');
    }
}
