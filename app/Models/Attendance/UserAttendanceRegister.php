<?php

namespace App\Models\Attendance;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class UserAttendanceRegister extends Model
{
    use ValidatingTrait;

    protected $fillable = ['user_id', 'date', 'in_time', 'out_time', 'tick_count', 'active_count', 'status'];
    private $rules = [
        'user_id' => 'required',
        'date' => 'required|date',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
