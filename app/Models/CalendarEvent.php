<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Admin\Leave;
use App\Models\Admin\Role;
use App\Models\Admin\UserSetting;
use App\Services\GoogleCalendarService\GoogleCalendarService;

class CalendarEvent extends Model
{
    public function events()
    {
        return $this->morphTo();
    }
    
    public static function createCalendarEvent($leave_id)
    {
        $leave = Leave::with('user')->find($leave_id);
        if($leave && $leave->status == "approved") {
            $name = $leave->user->name." is on ".ucfirst($leave->leaveType->title)." leave";// from ".date_in_view($leave->start_date)." to ".date_in_view($leave->end_date);
            $start_date_time = date("Y-m-d 9:00:00", strtotime($leave->start_date));
            $end_date_time = date("Y-m-d 23:59:59", strtotime($leave->end_date));
            $emails_array = self::getUserEmailToNotify($leave_id);
            GoogleCalendarService::saveCalendarEvent($name, $start_date_time, $end_date_time, $emails_array, $leave->id, "App\Models\Admin\Leave");
        }
    }
    public static function deleteCalendarEvent($leave_id)
    {
        $calendarEvent = CalendarEvent::where('reference_id',$leave_id)
                            ->where('reference_type','App\Models\Admin\Leave')->where('status','confirmed')->first();
        if ( !empty($calendarEvent) )
        {
            $calendarEvent->status = 'cancelled';
            $calendarEvent->save();
            GoogleCalendarService::deleteEvent($calendarEvent->calendar_event_id);
        }
    }
    public static function getUserEmailToNotify($leaveId) {
        $emails_array =[];
        $notifyUsers = UserSetting::where('key','google-calendar-leave-notification')->where('value',1)->get();

        // notify to Allowed Users 
        foreach($notifyUsers as $notifyUser) {
            if($notifyUser->user && $notifyUser->user->is_active == 1) {
                $emails_array[] = $notifyUser->user->email;
            }
        }

        $leaveObj = Leave::find($leaveId);
        if($leaveObj) {
            //notify to RM
            if($leaveObj->user) {
                //echo "RM".$leaveObj->user->reportingManager;
                $emails_array[] = $leaveObj->user->reportingManager->email;
            }
            //notify to TLS
            if(count($leaveObj->projectManagerApprovals) > 0) {

                foreach($leaveObj->projectManagerApprovals as $projectManagerApproval) {
            
                    if($projectManagerApproval->manager && $projectManagerApproval->manager->is_active == 1) {
            
                        $emails_array[] = $projectManagerApproval->manager->email;
                    }
                }
            }
        }
        $removeNotifications = UserSetting::where('key','google-calendar-leave-notification')->where('value',0)->get();
        foreach($removeNotifications as $removeNotification) {
            $pos = array_search($removeNotification->user->email, $emails_array);
            if($pos >=0 ) {
                unset($emails_array[$pos]);
            }
        }
        $emails_array = array_unique($emails_array);
        return $emails_array;
    }
}
