<?php

namespace App\Models\Timesheet;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserTimesheetLock extends Model
{

    use SoftDeletes;

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function dateUpdated()
    {
        return $this->belongsTo('App\Models\User', 'date_updated_by', 'id');
    }

    public function billingUpdated()
    {
        return $this->belongsTo('App\Models\User', 'billing_updated_by', 'id');
    }
}
