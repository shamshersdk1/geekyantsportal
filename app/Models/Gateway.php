<?php

namespace App\Models;
use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\Model;

class Gateway extends Model
{

	use ValidatingTrait;

    protected $table = 'gateways';
    public $timestamps = true;

}
