<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Input;
use App\Models\User;
use App\Models\Admin\Technology;

class UserTechnologies extends Model
{
	protected $fillable = [];

    protected $table = 'user_technologies';
	public $timestamps = true;

	protected $rules = [
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function technology()
    {
        return $this->belongsTo('App\Models\Admin\Technology', 'technology_id', 'id');
    }


    public static function filter()
    {
        $result=array('status' => false,'errors'=>"",'message' => "");
        $userList = User::with('technologies', 'userSkills');
        if (Input::get('searchuser')!='' ) {
            $key=str_replace('+', ' ', Input::get('searchuser'));
            $searchUserId = User::where('name', $key)->first();
            if (empty($searchUserId)) {
                $result['errors']="Select a valid User";
                return $result;
            }
            $name=$searchUserId->name;
            $searchUserId=$searchUserId->id;
            $userList=$userList->where('id', $searchUserId);
        }    
            $userList = $userList;
            $result=['status'=>true, 'userList' => $userList];
            return $result;

    }


}
