<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Admin\Project;
use App\Models\Admin\Calendar;
use App\Models\LeaveOverlapRequest;

class ProjectManagerLeaveApproval extends Model
{
    public function manager()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
    public function leave()
    {
        return $this->hasOne('App\Models\Admin\Leave', 'id', 'leave_id');
    }
    public static function getOverlappingLeaves($user_id, $projects, $start_date, $end_date, $leaveId)
    {
        $data = [];
        $max= date_diff(date_create($start_date), date_create($end_date))->format('%a');
        $dates = [];
        $code=[];
        $holidays = Calendar::whereDate('date', '>=', $start_date)->whereDate('date', '<=', $end_date)->pluck('date')->toArray();
        $date=$start_date;
        for($i=0;$i<=$max;$i++)
        {
            $dates[]= date('D \<\/\b\r\> j M', strtotime($date));
            if(date('N', strtotime($date)) > 5) {
                $code[] = 1;
            } else {
                if(in_array($date, $holidays)) {
                    $code[] = 1;
                } else {
                    $code[] = 0;
                }
            }
            $date = date('Y-m-d', strtotime($date . ' +1 day'));
        }
        $count=0;
        foreach ($projects as $project_id)
        {
            $project = Project::find($project_id);
            $coworkers = $project->resources()->with('leaves')->whereNotIn('user_id',[$user_id])
                ->where('start_date', '<=', $end_date)
                ->where(function ($query) use ($start_date){
                    $query->where('end_date', null)
                    ->orWhere('end_date', '>=', $start_date);
                })->get();
            $dataArray =[];
            $f=0;
            if(!empty($coworkers)) {
                foreach($coworkers as $coworker)
                {
                    $userDates = array_fill(0,count($dates),0);
                    $flag = 0;
                    $leaves = $coworker->leaves()->where('leaves.start_date', '<=', $end_date)
                        ->where('leaves.end_date', '>=', $start_date)
                        ->whereIn('leaves.status', ['pending','approved'])->get();
                    if(!empty($leaves)&&count($leaves)>0 ) {
                        $flag = 1;
                        foreach($leaves as $leave)
                        {
                            $start = strtotime($leave->start_date) > strtotime($start_date) ?
                                date_diff(date_create($leave->start_date), date_create($start_date))->format('%a') :
                                    0; 
                            $end = strtotime($end_date) > strtotime($leave->end_date) ?
                                date_diff(date_create($leave->end_date), date_create($start_date))->format('%a') :
                                    count($dates)-1;
                            for($i=$start;$i<=$end;$i++)
                            {
                                $userDates[$i] = ucfirst($leave->type);
                            }
                        }
                    }
                    if($flag == 1) {
                        $f=1;
                        $name = $coworker->name;
                        $dataArray[] = ['name' => $name, 'dates' => $userDates];
                    }
                }
                if($f==1) {
                $count++;
                $name= $project->project_name;
                $reason = LeaveOverlapRequest::where('user_id', $user_id)->where('project_id', $project->id)->where('leave_id', $leaveId)->first()->reason;
                $data[]= ['name' => $name, 'users' => $dataArray, 'id' => $project->id, 'reason' => $reason];
                }
            }
        }
        return ['data' => $data, 'count' => $count, 'dates' => $dates, 'code' => $code];
    }

    public static function leaveAction($leave_id, $user_id, $action)
    {
        $response = ['status' => true, 'message' => ""];
        $leave_approval = ProjectManagerLeaveApproval::with('leave')->where('leave_id', $leave_id)->where('user_id', $user_id)->first();
        if($leave_approval) {
            if($leave_approval->leave && $leave_approval->leave->status == "pending") {
                if($leave_approval->status == "pending") {
                    $leave_approval->status = $action;
                    $leave_approval->save();
                    $response['message'] = "Leave ".$action;
                } else {
                    $response = ['status' => false, 'message' => "You have already responded to this leave"];
                }
            } else {
                $response = ['status' => false, 'message' => "The leave has already been ".$leave_approval->leave->status];
            }
        } else {
            $response = ['status' => false, 'message' => "Invalid link followed"];
        }
        return $response;
    }
}
