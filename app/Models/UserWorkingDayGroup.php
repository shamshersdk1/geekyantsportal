<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserWorkingDayGroup extends Model
{
    public function workingDays()
    {
        return $this->belongsTo('App\Models\WorkingDayGroup', 'working_day_group_id', 'id' );
    }
}
