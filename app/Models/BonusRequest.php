<?php
namespace App\Models;

use App\Events\BonusRequest\BonusRequestRaised;
use App\Models\Activity;
use App\Models\Admin\FinancialYear;
use App\Models\BaseModel;
use App\Models\BonusRequestProject;
use App\Models\Month;
use App\Models\UserTimesheet;
use App\Services\BonusService;
use App\Services\TimesheetService;
use DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;

class BonusRequest extends BaseModel
{
    use ValidatingTrait;
    use SoftDeletes;
    protected $rules = [];
    protected $table = 'bonus_requests';
    protected $dates = ['deleted_at'];
    // protected $appends =['local_bonus','domestic_bonus','international_bonus','working_hours','notes','timesheet_entries','onsite_eligibility'];
    protected $appends = ['amount', 'working_hours', 'onsite_eligibility'];
    protected $hidden = array('amount');
    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
    public function referral()
    {
        return $this->belongsTo('App\Models\User', 'referral_for', 'id');
    }
    public function approvedBy()
    {
        return $this->belongsTo('App\Models\User', 'approver_id', 'id');
    }
    public function approvedBonus()
    {
        return $this->belongsTo('App\Models\Admin\Bonus', 'id', 'bonus_request_id');
    }
    public function bonusRequestProjects()
    {
        return $this->hasMany('App\Models\BonusRequestProject', 'bonus_request_id', 'id');
    }
    // public function getLocalBonusAttribute()
    // {
    //     $amount = 0;
    //     $systemSettingObj = SystemSetting::where('key','onsite_local_bonus')->first();
    //     if ( $systemSettingObj )
    //     {
    //         $amount = (int)$systemSettingObj->value;
    //     }
    //     return $amount;
    // }
    public function getAmountAttribute()
    {
        $user = \Auth::user();

        if (!$user->hasRole('admin')) {
            return 0;
        }

        $amount = BonusService::getBonusAmount($this->id);
        return $amount;
    }

    // public function getDomesticBonusAttribute()
    // {
    //     $amount = 0;
    //     $systemSettingObj = SystemSetting::where('key','onsite_domestic_bonus')->first();
    //     if ( $systemSettingObj )
    //     {
    //         $amount = (int)$systemSettingObj->value;
    //     }
    //     return $amount;
    // }

    // public function getInternationalBonusAttribute()
    // {
    //     $amount = 0;
    //     $systemSettingObj = SystemSetting::where('key','onsite_international_bonus')->first();
    //     if ( $systemSettingObj )
    //     {
    //         $amount = (int)$systemSettingObj->value;
    //     }
    //     return $amount;
    // }
    public static function saveData($data)
    {
        $obj = new self();
        $obj->month_id = !empty($data['month_id']) ? $data['month_id'] : null;
        $obj->user_id = !empty($data['user_id']) ? $data['user_id'] : null;
        $obj->date = !empty($data['date']) ? $data['date'] : null;
        $obj->type = !empty($data['type']) ? $data['type'] : null;
        $obj->sub_type = !empty($data['sub_type']) ? $data['sub_type'] : null;
        $obj->title = !empty($data['title']) ? $data['title'] : null;
        $obj->referral_for = !empty($data['referral_for']) ? $data['referral_for'] : null;
        $obj->redeem_type = !empty($data['redeem_type']) ? json_encode($data['redeem_type']) : null;
        $obj->notes = !empty($data['notes']) ? $data['notes'] : null;

        if (!empty($data['status']) && $data['status'] == 'approved') {
            $obj->status = 'approved';
            $obj->approver_id = \Auth::User()->id;
        }
        DB::beginTransaction();
        try {
            if (!$obj->save()) {
                throw new Exception('Bonus Request not Saved!Try Again');
                return false;
            }
            if (isset($data['projects']) && count($data['projects']) > 0) {
                foreach ($data['projects'] as $projectId) {
                    $bonusReqProject = new BonusRequestProject;
                    $bonusReqProject->bonus_request_id = $obj->id;
                    $bonusReqProject->project_id = $projectId;
                    if (!$bonusReqProject->save()) {
                        throw new Exception('Bonus Request Project Not updated');
                    }
                }
            }
            DB::commit();
            $response['message'] = 'Sucessfully Updated';
            $response['status'] = true;
        } catch (exception $e) {
            DB::rollback();
            $response['message'] = $e;
            $response['status'] = false;
        }

        event(new BonusRequestRaised($obj->id));
        return $obj;
    }
    public function getWorkingHoursAttribute()
    {
        $hours = 0;
        $userTimesheets = UserTimesheet::where('user_id', $this->user_id)->where('date', $this->date)->where('status', 'approved')->get();
        if (!empty($userTimesheets)) {
            foreach ($userTimesheets as $userTimesheet) {
                if ($userTimesheet->review) {
                    $hours = $hours + $userTimesheet->review->approved_duration;
                }
            }
        }
        return $hours;
    }

    // public function getNotesAttribute()
    // {
    //     $notes = '';
    //     $bonusObj = Bonus::where('bonus_request_id',$this->id)->first();
    //     if ( $bonusObj )
    //     {
    //         $notes = !empty($bonusObj->notes) ? $bonusObj->notes : '' ;
    //     }
    //     return $notes;
    // }

    public function getTimesheetEntriesAttribute()
    {
        $data = [];
        $data = TimesheetService::getUserDailyTimesheet($this->user_id, $this->date);
        return $data;
    }
    public function getOnsiteEligibilityAttribute()
    {
        $data = [];
        $data = BonusService::onsiteAllowanceEligibility($this->user_id, $this->date);
        return $data;
    }

    public static function isMonthLocked($month, $year)
    {
        $monthObj = Month::where('year', $year)->where('month', $month)->first();
        if (null == ($year)) {
            $year = date("Y", time());
        }
        if (null == ($month)) {
            $month = date("m", time());
        }
        $financialYearObj = FinancialYear::where('year', $year)->first();
        if ($financialYearObj) {
            $monthObj = Month::where('financial_year_id', $financialYearObj->id)->where('month', $month)->first();
            if ($monthObj && $monthObj->bonusSetting && $monthObj->bonusSetting->value == 'locked') {
                return true;
            }
        } else {
            return false;
        }
    }
    public function addActivity($action, $actionDetail = null)
    {

        $userObj = \Auth::User();
        $userId = null;
        if ($userObj) {
            $userId = $userObj->id;
        }
        $data['action'] = $action;
        $data['reference_id'] = $this->id;
        $data['reference_type'] = 'App\Models\BonusRequest';
        $data['action_details'] = [$actionDetail];
        $data['user_id'] = $userId;

        Activity::storeData($data);
    }

    public static function getBonusStatusCounts($month_id)
    {
        $counts = [];
        $total = BonusRequest::where('month_id', $month_id)->count();
        $approved = BonusRequest::where('month_id', $month_id)->where('status', 'approved')->count();
        $rejected = BonusRequest::where('month_id', $month_id)->where('status', 'rejected')->count();
        $pending = BonusRequest::where('month_id', $month_id)->where('status', 'pending')->count();

        $counts['total'] = $total;
        $counts['approved'] = $approved;
        $counts['rejected'] = $rejected;
        $counts['pending'] = $pending;

        return $counts;
    }

}
