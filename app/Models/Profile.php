<?php
namespace App\Models;

use App\Models\Admin\ProfileProject;
use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class Profile extends Model
{

    use ValidatingTrait;

    protected $table = 'user_profile';

    public $timestamps = false;

    protected $fillable = ['status', 'is_director', 'has_profile', 'profile_url', 'name', 'title', 'image', 'email', 'skills_json', 'join_date', 'confirmation_date', 'employee_number', 'facebook', 'github', 'stack_overflow', 'google', 'designation', 'linkedin', 'skype', 'user_id', 'location', 'phone', 'amazing', 'keywords', 'intro', 'twitter'];

    private $rules = array(
        'is_director' => 'required',
        'title' => 'required',
        'email' => 'required',
    );

    public function project()
    {
        return $this->belongsToMany('App\Models\Admin\CmsProject', 'profile_project', 'profile_id', 'project_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function profile()
    {
        return $this->morphMany('App\Models\Admin\File', 'reference');
    }

    public static function saveData($request, $id)
    {

        $has_profile = 0;
        $profile_url = null;
        $experience_json = null;
        $profile = Profile::find($id);
        if ($request->onoffswitch == "on") {
            $has_profile = 1;
        } else {
            $has_profile = 0;
        }
        if (!empty($request->slug)) {
            $profile_url = $request->slug;
        } else {
            $profile_url = str_replace(" ", "-", strtolower($profile->user->name));
        }
        if (!empty($request->experience_json)) {
            $experience_json = $request->experience_json;
            $a = [];
            $b = [];
            $i = 1;
            foreach ($experience_json as $experience) {
                if (array_key_exists('description', $experience)) {
                    $b['skill'] = $experience['description'];
                }
                if (array_key_exists('percentage', $experience)) {
                    $b['percentage'] = intval($experience['percentage']);
                }
                if ($i % 2 == 0) {
                    array_push($a, $b);
                    $b = [];
                }
                $i++;
            }
            $experience_json = json_encode($a);
        }

        if (!empty($request->projects)) {
            $oldProjects = ProfileProject::where('profile_id', $id)->select('project_id')->get();

            $oldProjects = $oldProjects->toArray();

            $oldProjectsOf = [];
            foreach ($oldProjects as $key => $value) {
                array_push($oldProjectsOf, $value['project_id']);
            }

            $deleteProjects = array_diff($oldProjectsOf, $request->projects);

            $result = ProfileProject::deleteData($id, $deleteProjects);

            foreach ($request->projects as $project) {
                $result = ProfileProject::saveEditData($id, $project);
            }
        }

        if ($profile) {
            // $profile->is_director = $request->is_director;
            $profile->title = $request->title;
            $profile->designation = $request->title;
            $profile->google = $request->google;
            $profile->facebook = $request->facebook;
            $profile->twitter = $request->twitter;
            $profile->linkedin = $request->linkedin;
            $profile->github = $request->github;
            $profile->stack_overflow = $request->stack_overflow;
            $profile->location = $request->location;
            $profile->intro = $request->intro;
            $profile->keywords = $request->keywords;
            $profile->amazing = $request->amazing;
            $profile->skype = $request->skype;
            $profile->email = $request->email;
            $profile->phone = $request->phone;
            $profile->expertise_json = $experience_json;
            $profile->achievements = $request->achievements;
            $profile->knowledge = $request->knowledge;
            $profile->has_profile = $has_profile;
            $profile->profile_url = $profile_url;
            $profile->is_active = ($request->onoffswitch == "on") ? 1 : 0;
        } else {
            return false;
        }
        if (count($profile->getErrors()) > 0) {
            return false;
        }

        if ($request->image) {
            $path = public_path('uploads/images/team');
            // $image = $request->file('image')->getClientOriginalName();
            $extensionType = $request->file('image')->getClientOriginalExtension();
            $new_path = time() . '.' . $extensionType;

            $request->file('image')->move($path, $new_path);

            $profile->image = "/uploads/images/team/" . $new_path;
        }

        if ($profile->save()) {
            return true;
        } else {
            return false;
        }
    }

    public static function saveExtraData($request, $id)
    {

        $experience_json = null;
        $interest_json = null;
        $achivements_json = null;

        if (!empty($request->achivements_json)) {
            $achivements_json = $request->achivements_json;
            $a = [];
            $b = [];
            $i = 1;
            $firstTimeLabel = true;
            $hasHeading = false;
            $html = '';
            foreach ($achivements_json as $achivement) {
                if (is_array($achivement)) {
                    if (array_key_exists('heading', $achivement)) {
                        $firstTimeLabel = true;
                        if (!empty($achivement['heading'])) {
                            $hasHeading = true;
                            $heading = $achivement['heading'];
                            if ($i == 1) {
                                $html .= '<div class="recognition"><strong>' . $heading . '</strong>';
                            } else {
                                $html .= '</ul></div><div class="recognition"><strong>' . $heading . '</strong>';
                            }
                        } else {
                            $hasHeading = false;
                            $html .= '</ul></div>';
                        }
                        $i++;
                    }
                    if (array_key_exists('label', $achivement)) {
                        if (!empty($achivement['label'])) {
                            $label = $achivement['label'];
                            if ($hasHeading) {
                                if ($firstTimeLabel) {
                                    $firstTimeLabel = false;
                                    $html .= '<ul class="small-text"><li>' . $label . '</li>';
                                } else {
                                    $html .= '<li>' . $label . '</li>';
                                }
                            } else {
                                $html .= '<div class="recognition"><strong>' . $label . '</strong></div>';
                            }
                        }
                    }
                }
            }
            $achivements_json = $html;
            //print_r($html);die;
        }

        if (!empty($request->projects)) {
            foreach ($request->projects as $project) {
                $result = ProfileProject::saveData($id, $project);
            }
        }
        if (!empty($request->experience_json)) {
            $experience_json = $request->experience_json;
            $a = [];
            $b = [];
            $i = 1;
            foreach ($experience_json as $experience) {
                if (array_key_exists('description', $experience)) {
                    $b['skill'] = $experience['description'];
                }
                if (array_key_exists('percentage', $experience)) {
                    $b['percentage'] = intval($experience['percentage']);
                }
                if ($i % 2 == 0) {
                    array_push($a, $b);
                    $b = [];
                }
                $i++;
            }
            $experience_json = json_encode($a);
        }
        if (!empty($request->interest_json)) {
            $a = '';
            $temp = $request->interest_json;
            $i = 0;
            foreach ($temp as $experience) {
                if (!empty($experience)) {
                    $a = $a . $experience . ',';
                }
            }
            $a = rtrim($a, ",");
            $interest_json = $a;
        }
        $profile = Profile::find($id);
        if ($profile) {
            $profile->expertise_json = $experience_json;
            $profile->knowledge = $interest_json;
            $profile->achievements = $achivements_json;
        } else {
            return false;
        }
        if ($profile->save()) {
            return true;
        } else {
            return false;
        }
    }

    public static function existsSlug($slug)
    {
        $profile = Profile::where('profile_url', $slug)->first();
        if ($profile) {
            return true;
        }
        return false;
    }

    public static function createProfileFromUser($user)
    {
        $response = [
            'status' => true,
            'message' => "success",
            'id' => null,
            'log' => [],
            'errors' => [],
        ];

        if (empty($user) || empty($user->id)) {
            $response['status'] = false;
            $response['log'][] = 'User not found to create profile';
            return $response;
        }

        $profileObj = new Profile();
        $profileObj->user_id = $user->id;
        $profileObj->email = 'info@sahusoft.com';
        $profileObj->is_director = 0;
        $profileObj->has_profile = 0;
        $profileObj->title = 'Software Engineer';
        if (!$profileObj->save()) {
            $response['status'] = false;
            $response['log'][] = 'Profile not created, something went wrong.';
            return $response;
        }
        $response['id'] = $profileObj->id;
        $response['message'] = 'Profile created';
        return $response;
    }

    public static function socialUpdate($data, $id)
    {
        $res['status'] = false;
        $res['message'] = "error";

        $profileObj = Profile::where('user_id', $id)->first();

        if ($profileObj) {
            $profileObj->social_json = $data;
        } else {
            $res['message'] = "profile not found";
            return $res;
        }

        if ($profileObj->save()) {
            $res['status'] = true;
            $res['message'] = "success";
            return $res;
        } else {
            $res['message'] = $profileObj->getErrors();
            return $res;
        }
        return $res;
    }
}
