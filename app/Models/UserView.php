<?php

namespace App\Models;

use App\Models\Admin\PayslipMonth;
use App\Models\Admin\ProjectResource;
use App\Models\SystemSetting;
use App\Models\User;
use App\Services\LeaveService;
use Illuminate\Database\Eloquent\Model;

class UserView extends Model
{
    public static function leaves($user)
    {
        // Return the count of total leave taken.

        $totalSickLeaves = SystemSetting::where('key', 'max_sick_leave')->first();
        $totalPaidLeaves = SystemSetting::where('key', 'max_paid_leave')->first();

        $from_year = date("Y", strtotime("-1 year"));
        $to_year = date("Y");
        $from_date_string = $from_year . '-12-25';
        $to_date_string = $to_year . '-12-25';
        $from_date = date($from_date_string);
        $to_date = date($to_date_string);

        if ($totalSickLeaves) {
            $totalSickLeaves = $totalSickLeaves->value;
        }
        if ($totalPaidLeaves) {
            $totalPaidLeaves = $totalPaidLeaves->value;
        }
        if (isset($user->joining_date)) {
            $leaves = $user->leaves->where('type', 'sick')->where('status', 'approved')->where('end_date', '>=', $from_date)->where('start_date', '<=', $to_date);
            $joiningDate = $user->joining_date;
            $totalSickLeaves = LeaveService::maxSickLeave($joiningDate);
            $consumedSickLeaves = 0;
            foreach ($leaves as $leave) {
                $consumedSickLeaves = $consumedSickLeaves + $leave->days;
            }
        } else {
            $totalSickLeaves = 0;
            $consumedSickLeaves = 0;
        }
        if (isset($user->confirmation_date)) {
            $leaves = $user->leaves->whereIn('type', ['paid', 'half'])->where('status', 'approved')->where('end_date', '>=', $from_date)->where('start_date', '<=', $to_date);
            $joiningDate = $user->joining_date;
            $totalPaidLeaves = LeaveService::maxPersonalLeave($joiningDate);
            $consumedPaidLeaves = 0;
            foreach ($leaves as $leave) {
                $consumedPaidLeaves = $consumedPaidLeaves + $leave->days;
            }
        } else {
            $totalPaidLeaves = 0;
            $consumedPaidLeaves = 0;
        }

        $consumedUnpaidLeaves = 0;
        $unpaidLeaves = $user->leaves->where('type', 'unpaid')->where('status', 'approved');
        foreach ($unpaidLeaves as $leave) {
            $consumedUnpaidLeaves = $consumedUnpaidLeaves + $leave->days;
        }
        $result['consumedSickLeaves'] = $consumedSickLeaves;
        $result['totalSickLeaves'] = $totalSickLeaves;
        $result['consumedPaidLeaves'] = $consumedPaidLeaves;
        $result['totalPaidLeaves'] = $totalPaidLeaves;
        $result['consumedUnpaidLeaves'] = $consumedUnpaidLeaves;
        return $result;
    }
    public static function payslip($id, $limit)
    {
        $date = date('Y-m-d');
        $max = date("Y", strtotime($date)) + 1;
        if ($limit == -1) {
            $payslips = PayslipMonth::with('user')->where('status', 'approved')->orderBy('year', 'DESC')->orderBy('month', 'DESC')->paginate(12);
        } else {
            $payslips = PayslipMonth::with('user')->where('status', 'approved')->where('year', '<', $max)->where('year', '>', $max - 3)->orderBy('year', 'DESC')->orderBy('month', 'DESC')->paginate(12);
        }
        $result = [];
        $c = 0;
        foreach ($payslips as $payslip) {
            foreach ($payslip->payslipData as $data) {
                if ($data->user_id == $id) {
                    $result['payslip'][] = ['month' => $payslip->month, 'year' => $payslip->year];
                    $result['data'][] = $data;
                    $c++;
                    continue;
                }
            }
            if ($c == $limit) {
                break;
            }
        }
        return $result;
    }
    public static function bdate($limit)
    {
        $date = date('Y-m-d');
        $bdays = User::where('dob', '!=', null)->where('dob', '!=', '0000-00-00')->where('is_active', 1)->get();
        if (empty($bdays)) {
            return null;
        }
        $array['user'] = [];
        $array['date'] = [];
        foreach ($bdays as $bday) {
            $array['user'][] = $bday;
            $d = date(date("Y", strtotime($date)) . "-m-d", strtotime($bday->dob));
            if ($d >= $date) {
                $array['date'][] = $d;
            } else {
                $array['date'][] = date((date("Y", strtotime($date)) + 1) . "-m-d", strtotime($bday->dob));
            }
        }
        $result = [];
        if ($limit == -1) {
            $limit = count($array['user']) + 1;
        }if (count($array['user']) > 0) {
            array_multisort($array['date'], $array['user']);
        }
        for ($i = 0; $i < $limit && $i < count($array['user']); $i++) {
            $result[$i]['user'] = $array['user'][$i];
            $result[$i]['active'] = 0;
            if ($array['date'][$i] == $date) {
                $result[$i]['active'] = 1;
            }
        }
        return $result;
    }

    public static function managerDetails($id)
    {
        $user = User::find($id);
        $data = [];
        $reporting_manager = empty($user->reportingManager) ? null : $user->reportingManager;
        if ($reporting_manager) {
            $data['reporting_manager_name'] = $reporting_manager->name;
            $data['reporting_manager_phone'] = empty($reporting_manager->profile) || empty($reporting_manager->profile->phone) ? "Unavailable" : $reporting_manager->phone;
        }
        $data['team_leads'] = [];
        $projectResources = ProjectResource::with('project')
            ->where('user_id', $user->id)
            ->where('start_date', '<=', date('Y-m-d'))
            ->where(function ($query) {
                $query->where('end_date', null)
                    ->orWhere('end_date', '>=', date('Y-m-d'));
            })->get();
        if (!empty($projectResources)) {
            foreach ($projectResources as $resource) {
                if ($resource->project && $resource->project->projectManager && $resource->project->projectManager->id != $user->id) {
                    $tl = $resource->project->projectManager;
                    $data['team_leads'][$tl->name] = empty($tl->profile) || empty($tl->profile->phone) ? "Unavailable" : $tl->profile->phone;
                }
            }
        }
        return $data;
    }
}
