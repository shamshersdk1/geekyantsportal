<?php

namespace App\Models;

use App\Models\BaseModel;
use Auth;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Events\Comment\CommentStored;

class Comment extends BaseModel
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function commentable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    public static function saveComment($commentable_id, $commentable_type, $value, $is_private)
    {
        $status = true;
        $message = '';
        $user = Auth::user();
        $recordExists = Comment::where('commentable_id', $commentable_id)->where('commentable_type', $commentable_type)->first();
        if ($recordExists && $commentable_type == 'App\Models\Admin\FeedbackUser\Restricted') {
            $recordExists->delete();
        }
        $commentObj = new Comment();
        $commentObj->commentable_id = $commentable_id;
        $commentObj->commentable_type = $commentable_type;
        $commentObj->message = $value;
        $commentObj->user_id = $user->id;
        $commentObj->is_private = $is_private == "true" ? true : false;
        if (!$commentObj->save()) {
            $status = false;
            $message = $commentObj->getMessage();
        }
        else {
            event(new CommentStored($commentObj->id));
        }
        $response['status'] = $status;
        $response['message'] = $message;
        $newObj = Comment::with('user')->find($commentObj->id);
        $response['comment'] = $newObj;
        return $response;
    }

    public static function getComments($commentable_id, $commentable_type, $page, $per_page, $private_flag)
    {
        if ($private_flag) {
            $comments = Comment::with('user')->where('commentable_id', $commentable_id)->where('commentable_type', $commentable_type)->orderBy('created_at', 'DESC')->get();
        } else {
            $comments = Comment::with('user')->where('commentable_id', $commentable_id)->where('commentable_type', $commentable_type)
                ->where('is_private', false)->orderBy('created_at', 'DESC')->get();
        }
        $comments = $comments->slice(($page - 1) * $per_page)->take($per_page);
        $comments = $comments->values();
        return $comments;
    }

}
