<?php

namespace App\Models;

use App\Models\Admin\CalendarYear;
use App\Models\Admin\FinancialYear;
use App\Services\CalendarService;
use Illuminate\Database\Eloquent\Model;

class Month extends Model
{
    protected $filllable = ['working_days'];
    public function financialYear()
    {
        return $this->belongsTo('App\Models\Admin\FinancialYear', 'financial_year_id', 'id');
    }
    public function salaryTransfer()
    {
        return $this->hasOne('App\Models\Audited\Salary\SalaryTransfer', 'month_id', 'id');
    }
    public function bonusRequests()
    {
        return $this->hasMany('App\Models\BonusRequest', 'month_id', 'id');
    }
    public function bonusSetting()
    {
        return $this->hasOne('App\Models\MonthSetting', 'month_id', 'id')->where('key', 'bonus-request');
    }
    public function vpfSetting()
    {
        return $this->hasOne('App\Models\MonthSetting', 'month_id', 'id')->where('key', 'vpf-deduction');
    }
    public function insuranceSetting()
    {
        return $this->hasOne('App\Models\MonthSetting', 'month_id', 'id')->where('key', 'insurance-deduction');
    }
    public function vpfDeduction()
    {
        return $this->hasOne('App\Models\Admin\VpfDeduction', 'month_id', 'id');
    }
    public function leaveSetting()
    {
        return $this->hasOne('App\Models\MonthSetting', 'month_id', 'id')->where('key', 'leave-deduction');
    }

    // public function insuranceDeductionSum()
    // {
    //     return $this->hasOne('App\Models\Admin\Insurance\InsuranceDeduction', 'month_id', 'id')->sum('amount');
    // }

    public function payrollSetting()
    {
        return $this->hasOne('App\Models\MonthSetting', 'month_id', 'id')->where('key', 'payroll');
    }
    public function payrollRuleDataSetting()
    {
        return $this->hasOne('App\Models\MonthSetting', 'month_id', 'id')->where('key', 'payroll-rule-data');
    }
    public function feedbackReviewSetting()
    {
        return $this->hasOne('App\Models\MonthSetting', 'month_id', 'id')->where('key', 'feedback-review');
    }
    public function loanSetting()
    {
        return $this->hasOne('App\Models\MonthSetting', 'month_id', 'id')->where('key', 'loan-deduction');
    }
    public function loanInterestSetting()
    {
        return $this->hasOne('App\Models\MonthSetting', 'month_id', 'id')->where('key', 'loan-interest-income');
    }
    public function itSavingMonthSetting()
    {
        return $this->hasOne('App\Models\MonthSetting', 'month_id', 'id')->where('key', 'it-saving-month');
    }

    public function getStatus($key)
    {
        return $this->hasOne('App\Models\MonthSetting', 'month_id', 'id')->where('key', $key);
    }

    public function fixedBonusSetting()
    {
        return $this->hasOne('App\Models\MonthSetting', 'month_id', 'id')->where('key', 'fixed-bonus');
    }
    public function monthlyFoodDeductionSetting()
    {
        return $this->hasOne('App\Models\MonthSetting', 'month_id', 'id')->where('key', 'monthly-food-deduction');
    }

    public function variablePaySetting()
    {
        return $this->hasOne('App\Models\MonthSetting', 'month_id', 'id')->where('key', 'variable-pay');
    }

    public function salaries()
    {
        return $this->hasMany('App\Models\Audited\Salary\Salary', 'month_id', 'id');
    }

    public function adhocPaymentSetting()
    {
        return $this->hasOne('App\Models\MonthSetting', 'month_id', 'id')->where('key', 'adhoc-payment');
    }

    public function workingDaySetting()
    {
        return $this->hasOne('App\Models\MonthSetting', 'month_id', 'id')->where('key', 'working-day');
    }

    public function prepUserSetting()
    {
        return $this->hasOne('App\Models\MonthSetting', 'month_id', 'id')->where('key', 'prep-user');
    }

    public function checkAllStatus()
    {
        $payroll_rule = $this->payrollSetting ? ($this->payrollSetting->value == 'locked' ? 1 : 0) : 0;
        $bonus = $this->bonusSetting ? ($this->bonusSetting->value == 'locked' ? 1 : 0) : 0;
        $loan = $this->loanSetting ? ($this->loanSetting->value == 'locked' ? 1 : 0) : 0;
        $leave = $this->leaveSetting ? ($this->leaveSetting->value == 'locked' ? 1 : 0) : 0;
        $feedback_review = $this->feedbackReviewSetting ? ($this->feedbackReviewSetting->value == 'locked' ? 1 : 0) : 0;
        $payroll_rule_data = $this->payrollRuleDataSetting ? ($this->payrollRuleDataSetting->value == 'locked' ? 1 : 0) : 0;

        if ($payroll_rule && $bonus && $loan && $leave && $feedback_review && $payroll_rule_data) {
            return true;
        } else {
            return false;
        }
    }
    public function formatMonth()
    {
        $dateObj = \DateTime::createFromFormat('Y-m', $this->year . '-' . $this->month);
        $monthName = $dateObj->format('F, Y');
        return $monthName;
        //return $dt->format('F');
    }
    public static function getMonth($date)
    {
        // return the id of the month
        $month = date('m', strtotime($date));
        $year = date('Y', strtotime($date));

        $monthObj = Month::where('year', $year)->where('month', $month)->first();
        if ($monthObj) {
            return $monthObj->id;
        } else {
            return 0;
        }
    }
    public static function getMonthByDate($date)
    {
        // return the id of the month
        $month = date('m', strtotime($date));
        $year = date('Y', strtotime($date));

        $monthObj = Month::where('year', $year)->where('month', $month)->first();
        if ($monthObj) {
            return $monthObj;
        } else {
            return 0;
        }
    }

    public function getLastDay()
    {
        $startDate = date('Y-m-d', strtotime($this->year . '-' . $this->month . '-01'));
        return date("Y-m-t", strtotime($startDate));
    }
    public static function getMonthList($year = null)
    {
        if ($year != null) {
            $months = Month::where('year', $year)->orderBy('year', 'DESC')->orderBy('month', 'DESC')->get();
        } else {
            $months = Month::orderBy('year', 'DESC')->orderBy('month', 'DESC')->get();
        }

        return $months;
    }
    public function getFirstDay()
    {
        return date('Y-m-01', strtotime($this->year . '-' . $this->month . '-01'));

    }
    public function getTotalDay()
    {
        return date('t', strtotime($this->month . ' 01, ' . $this->year));
    }
    public function getWorkingDays()
    {
        return CalendarService::getWorkingDays($this->month, $this->year);
    }

    public static function monthCreate()
    {
        $response['status'] = false;
        $response['message'] = null;
        $currentMonth = date('m');
        $currentYear = date('Y');
        $exist = Month::where('month', $currentMonth)->where('year', $currentYear)->first();
        $data['month'] = $currentMonth;
        $data['year'] = $currentYear;

        $financialYear = $currentYear;
        if ($exist->month < 04) {
            $financialYear = $currentYear - 1;
        }

        $calendarYearExists = CalendarYear::where('year', $currentYear)->first();
        if (!$calendarYearExists) {
            $calendarYearObj = new CalendarYear();
            $calendarYearObj->year = $currentYear;
            if (!$calendarYearObj->save()) {
                $response['message'] = "Unable to add Calendar Year";
                return $response;
            }
        }

        $financialYear = FinancialYear::where('year', $currentYear)->first();
        if (!$financialYear) {
            if ($currentMonth < 04) {
                $data['year'] = $currentYear - 1;
            } else {
                $data['year'] = $currentYear;
            }

            $responseFinance = FinancialYear::insertFinacialYear($data);
            if (!$responseFinance) {
                $response['message'] = "Unable to add Finanical Year";
                return $response;
            }
        }
        if ($exist) {
            $response['message'] = "Already Exits";
            return $response;
        }
        $data['year'] = $currentYear;
        $financialYear = FinancialYear::where('year', $currentYear)->first();
        $data['financial_year_id'] = $financialYear->id;
        $responseMonth = Month::saveData($data);
        if (!$responseMonth) {
            $response['message'] = "Unable to add Month";
            return $response;
        }
        $response['status'] = true;
        $response['message'] = "Month Entry Inserted Succesfully ";
        return $response;
    }

    public static function saveData($data)
    {
        $month = !empty($data['month']) ? $data['month'] : null;
        $year = !empty($data['year']) ? $data['year'] : null;
        $exist = Month::where('month', $month)->where('year', $year)->first();
        if ($exist) {
            return false;
        }

        $monthObj = new Month();
        $monthObj->year = $year;
        $monthObj->month = $month;
        $monthObj->financial_year_id = !empty($data['financial_year_id']) ? $data['financial_year_id'] : null;
        $working_days = CalendarService::getWorkingDays($month, $year);
        $monthObj->working_days = $working_days ? $working_days : "0";
        $monthObj->month_number = ($month < 4) ? ($month + 12) - 3 : $month - 3;
        $monthObj->status = "open";
        $monthObj->payroll_rule = "0";
        $monthObj->bonus_check = "0";
        $monthObj->loan_check = "0";
        $monthObj->feedback_review_check = "0";
        $monthObj->lop_check = "0";
        if (!$monthObj->save()) {
            return false;
        }

        return true;
    }
    public function prevoius()
    {
        $firstDayOfMonth = $this->year . '-' . $this->month . '-01';

        $next_month_ts = strtotime($firstDayOfMonth . ' +1 month');
        $prev_month_ts = strtotime($firstDayOfMonth . ' -1 month');

        $next_month = date('Y/m/d', $next_month_ts);
        $prev_month = date('Y/m/d', $prev_month_ts);
        $prevousMonthObj = $this->getMonthByDate($prev_month);

        if ($prevousMonthObj) {
            return $prevousMonthObj;
        }

        return false;
    }
    public function next()
    {
        $firstDayOfMonth = $this->year . '-' . $this->month . '-01';

        $next_month_ts = strtotime($firstDayOfMonth . ' +1 month');

        $next_month = date('Y/m/d', $next_month_ts);
        $nextMonthObj = $this->getMonthByDate($next_month);

        if ($nextMonthObj) {
            return $nextMonthObj;
        }

        return false;
    }
    public static function getMonthId($prevYear, $monthId)
    {
        $monthObj = Month::where('year', $prevYear->year)->where('month', $monthId)->first();
        return $monthObj;
    }
    public static function getNextMonthFirstDate($monthId)
    {
        $monthObj = Month::find($monthId);

        $date = $monthObj->getFirstDay();

        return date('Y-m-d', strtotime("+1 month", strtotime($date)));

    }
    public static function getFinancialYearEndDate($year)
    {
        return date('Y-m-01', strtotime($year + 1 . '-' . '03' . '-31'));
    }

    public static function getLastMonth($monthId)
    {if ($monthId) {
        $lastMonth = Month::where('id', '<>', $monthId)->orderBy('year', 'DESC')->orderBy('month', 'DESC')->first();
        if ($lastMonth) {
            return $lastMonth;
        }

        return null;
    }
    }
}
