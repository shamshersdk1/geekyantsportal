<?php
namespace App\Models;

use App\Models\BaseModel;


class BonusRequestProject extends BaseModel
{
    
    protected $rules = [];
    protected $table = 'bonus_request_projects';
    
    public $timestamps = false;

    public function bonusRequest()
    {
        return $this->belongsTo('App\Models\BonusRequest', 'bonus_request_id', 'id');
    }
    public function project()
    {
        return $this->belongsTo('App\Models\Admin\Project', 'project_id', 'id');
    }

}
