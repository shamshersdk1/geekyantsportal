<?php

namespace App\Models;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\BaseModel;
use App\Events\SshKey\SshKeyEntered;
use App\Events\SshKey\StoreSshFingerprint;
use violuke\RsaSshKeyFingerprint\FingerprintGenerator;

use Auth;
use DB;
use Config;

class SshKey extends BaseModel
{
    protected $dates = ['deleted_at'];
    protected $table = 'ssh_keys';
    public $timestamps = true;
    use SoftDeletes;

    public function user() {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    public static function boot()
    {
        SshKey::saving(function ($input) {
            try{
                $fingerprint = FingerprintGenerator::getFingerprint($input->value);
            }
            catch (\Exception $exception) {
                $fingerprint = null;
            }
            $input->fingerprint = $fingerprint;
            return $input;
        });
        parent::boot();
    }

    public static function saveData($input)
    {
        $status = true;
		$message = '';
        
        $exists = SshKey::where('user_id',$input['user_id'])->first();
        if ( !empty($exists) )
        {
            $response['status'] = false;
            $response['message'] = 'User Exists';
		    return $response;   
        }

		$sshKeyObj = new SshKey();
		$sshKeyObj->user_id = $input['user_id'];
		$sshKeyObj->value = $input['ssh_key'];
		$sshKeyObj->is_verified = false;
		if( !$sshKeyObj->save() )
		{
			$status = false;
			$message = $sshKeyObj->getErrors()->toArray();
		}
        if( $status )
        {
            $message = $sshKeyObj->id;
            event(new SshKeyEntered($sshKeyObj->id));
        }
		$response['status'] = $status;
		$response['message'] = $message;
		
		return $response;
    }

    public static function updateData($id, $input)
    {
        $status = true;
		$message = '';
		
        $sshKeyObj = SshKey::find($id);
        if ( empty($sshKeyObj) )
        {
            $status = false;
            $message = 'No Record found';
        }
        else
        {
            $sshKeyObj->value = $input['ssh_key'];
            $sshKeyObj->is_verified = ($input['is_verified'] == 'false') ? 0 : 1 ;
            $message = $sshKeyObj->id;
            if( !$sshKeyObj->save() )
            {
                $status = false;
                $message = $sshKeyObj->getErrors()->toArray();
            }
        }
		
		$response['status'] = $status;
		$response['message'] = $message;
		
		return $response;
    }

    public static function requestAction($id, $action)
    {
        $status = true;
        $message = '';
        $sshKeyObj = SshKey::find($id);
        if( empty($sshKeyObj) )
        {
            $status = false;
            $message = 'No Record found';
        }
        else 
        {
            if( $action == 'yes' )
            {
                $sshKeyObj->is_verified = true;
                $message = 'Your ssh is verified';
            }
            else
            {
                $sshKeyObj->is_verified = false;
                $message = 'Please update your correct ssh';
            }
            $sshKeyObj->save();
        }
        $response['status'] = $status;
        $response['message'] = $message;
        return $response;
    }

    public static function getSshKey($user_id)
    {
        $status = true;
        $message = '';
        
        $sshKeyObj = SshKey::where('user_id',$user_id)->first();
        if( empty($sshKeyObj) )
        {
            $status = false;
            $message = '*ssh-key* is not configured on portal';
        }
        else 
        {
            $message = $sshKeyObj->value;
        }
        $response['status'] = $status;
        $response['message'] = $message;
        return $response;
    }

    public static function saveMyData($input)
    {
        $status = true;
		$message = '';
        $user = Auth::user();

		$sshKeyObj = new SshKey();
		$sshKeyObj->user_id = $user->id;
		$sshKeyObj->value = $input['ssh_key'];
		$sshKeyObj->is_verified = true;
		if( !$sshKeyObj->save() )
		{
			$status = false;
			$message = $sshKeyObj->getErrors()->toArray();
		}
        
		$response['status'] = $status;
		$response['message'] = $message;
		
		return $response;
    }
}
