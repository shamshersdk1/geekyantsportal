<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;
use App\Models\User;
use App\Models\FloorsSeatUser;

class FloorsSeat extends Model
{

	use ValidatingTrait;
	protected $table = 'floors_seats';
	public $timestamps = false;

	private $rules = array(
		);
		
	public function floor() {
        return $this->belongsTo('App\Models\Floor', 'floor_id', 'id');
    }

	public function seatUser() {
        return $this->hasOne('App\Models\FloorsSeatUser', 'floor_seat_id', 'id');
    }

	public static function checkSeat($seat)
	{
		$flag = 0;
		$floorsSeats = FloorsSeat::all();
		foreach ( $floorsSeats as $floorsSeat )
		{
			if ( $seat == $floorsSeat->name )
			{
				$flag = 1;
			}
		}
		if ( $flag == 1 )
			return true;
		else
			return false;

	}

	public static function getCount($floor_id)
	{
		$count = 0;
		$floorsSeats = FloorsSeat::where('floor_id',$floor_id)->get();
		foreach ( $floorsSeats as $floorsSeat )
		{
			$floorSeatUser = FloorsSeatUser::where('floor_seat_id',$floorsSeat->id)->first();
			if( isset($floorSeatUser) ){
				$count++;	
			}
		}
		return $count;
	}
	public static function createSeats($number_of_seats, $floor_id)
	{
		for ( $i=0; $i<$number_of_seats; $i++  ){
			$floorSeatObj = new FloorsSeat();
			$floorSeatObj->name = null;
			$floorSeatObj->floor_id = $floor_id;
			$floorSeatObj->save();
		}
	}
}
