<?php
namespace App\Models;

use App\Events\ReportingManager\RmAssigned;
use App\Events\User\UserCreated;
use App\Models\Admin\Designation;
use App\Models\Admin\Role;
use App\Models\BaseModel;
use App\Models\UserRole;
use Exception;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;
use Input;
use Log;
use OwenIt\Auditing\Contracts\Auditable;
use Watson\Validating\ValidatingTrait;

class User extends BaseModel implements AuthenticatableContract, CanResetPasswordContract, Auditable
{

    use Authenticatable;
    use CanResetPassword;
    use ValidatingTrait;
    use SoftDeletes;
    use \OwenIt\Auditing\Auditable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';
    protected $dates = ['deleted_at'];
    protected $auditExclude = [
        'password',
        'remember_token',
        'is_billable',
        'role',
    ];
    private $rules = array(
        'name' => 'required',
        'role' => 'required',
        'email' => 'required|email|unique',
        'employee_id' => 'unique',
    );

    //use SoftDeletes;

    //protected $dates = ['deleted_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'first_name', 'middle_name', 'last_name', 'email', 'dob', 'password', 'role', 'is_active'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token', 'bank_ac_no', 'bank_ifsc_code', 'pf_no', 'uan_no', 'esi_no', 'pan', 'one_day_salary'];
    protected $appends = ['one_day_salary'];
    public function transformAudit(array $data): array
    {
        if (Arr::has($data, 'new_values.parent_id')) {
            $data['old_values']['reporting_manager_name'] = User::find($this->getOriginal('parent_id'))->name;
            $data['new_values']['reporting_manager_name'] = User::find($this->getAttribute('parent_id'))->name;
        }
        if (Arr::has($data, 'new_values.designation')) {
            $data['old_values']['user_designation'] = Designation::find($this->getOriginal('designation'))->designation;
            $data['new_values']['user_designation'] = Designation::find($this->getAttribute('designation'))->designation;
        }
        unset($data['old_values']['parent_id']);
        unset($data['new_values']['parent_id']);
        unset($data['old_values']['designation']);
        unset($data['new_values']['designation']);
        return $data;
    }
    public function checkRole($code)
    {
        if ($this->roles()->where('code', $code)->count()) {
            return true;
        }
        return false;
    }
    public function isAdmin()
    {
        if ($this->roles()->where('code', 'admin')->count()) {
            return true;
        }
        return false;
    }
    public function userDetails()
    {
        return $this->hasOne('App\Models\UserDetail', 'user_id', 'id');
    }
    public function userAttendanceRegister($date)
    {
        return $this->hasOne('App\Models\Attendance\UserAttendanceRegister', 'user_id', 'id')->where('date', $date)->first();
    }
    public function setting()
    {
        return $this->hasOne('App\Models\Setting', 'user_id', 'id');
    }
    public function activeAppraisal()
    {
        return $this->hasOne('App\Models\Admin\Appraisal', 'user_id', 'id')->where('is_active', 1);
    }
    public function timesheetLock()
    {
        return $this->hasOne('App\Models\Timesheet\UserTimesheetLock', 'user_id', 'id');
    }
    public function timesheetRequired()
    {
        return $this->hasOne('App\Models\Admin\UserSetting', 'user_id', 'id')->where('key', 'timesheet-required');
    }
    public function RMNotificationReminder()
    {
        return $this->hasOne('App\Models\Admin\UserSetting', 'user_id', 'id')->where('key', 'rm-notification-reminder');
    }
    public function user_profile()
    {
        return $this->hasOne('App\Models\Profile', 'user_id', 'id');
    }

    public function payslips()
    {
        return $this->hasOne('App\Models\Admin\Payslip', 'email_id', 'email');
    }

    public function roles()
    {
        return $this->belongsToMany('App\Models\Admin\Role', 'user_roles', 'user_id', 'role_id');
    }
    public function events()
    {
        return $this->belongsToMany('App\Models\UserEvent', 'user_id', 'id');
    }

    public function profile()
    {

        return $this->hasOne('App\Models\Profile', 'user_id', 'id');
    }

    public function leaves()
    {
        return $this->hasMany('App\Models\Admin\Leave', 'user_id', 'id');
    }
    public function loans()
    {
        return $this->hasMany('App\Models\Loan', 'user_id', 'id');
    }
    public function bonus()
    {
        return $this->hasMany('App\Models\Admin\Bonus', 'user_id', 'id');
    }
    public function technologies()
    {
        return $this->belongsToMany('App\Models\Admin\Technology', 'user_technologies', 'user_id', 'technology_id');
    }

    public function social()
    {
        return $this->hasMany('App\Models\Social');
    }
    // public function ctc()
    // {
    //     return $this->hasOne('App\Models\Admin\UserCtc', 'user_id', 'id');
    // }
    public function appraisals()
    {
        return $this->hasMany('App\Models\Admin\Appraisal', 'user_id', 'id');
    }
    public function sprint()
    {
        return $this->hasOne('App\Models\Admin\SprintResource', 'employee_id', 'id');
    }
    public function workingDayGroup()
    {
        return $this->hasOne('App\Models\UserWorkingDayGroup', 'user_id', 'id')->where('is_active', 1);
    }
    public function projects()
    {
        return $this->belongsToMany('App\Models\Admin\Project', 'project_resources', 'user_id', 'project_id');
    }
    public function reportingManager()
    {
        return $this->belongsTo('App\Models\User', 'parent_id');
    }
    public function children()
    {
        return $this->hasMany('App\Models\User', 'parent_id', 'id');
    }
    public function userSkills()
    {
        return $this->hasMany('App\Models\UserTechnologies', 'user_id', 'id');
    }
    public function reportees()
    {
        return $this->hasMany('App\Models\User', 'parent_id', 'id')->where('is_active', 1);
    }
    public function insurances()
    {
        return $this->morphMany('App\Models\Admin\Insurance\Insurance', 'insurable');
    }
    public function projectResources()
    {
        return $this->hasManyThrough(
            'App\Models\Admin\ProjectResource',
            'App\Models\Admin\Project',
            'project_manager_id',
            'project_id',
            'id',
            'id'
        );
    }
    public function slackUsers()
    {
        return $this->belongsToMany('App\Models\Admin\SlackUser', 'user_slack_user', 'user_id', 'slack_user_id');
    }
    public function user_contact()
    {
        return $this->hasOne('App\Models\Admin\UserContact', 'user_id', 'id');
    }

    public function taskGroups()
    {
        return $this->hasMany('App\Models\Admin\TaskGroup', 'created_by', 'id');
    }

    public function getSlackUserAttribute()
    {
        return $this->slackUsers()->first();
    }

    public function hasRole($role)
    {
        if ($this->roles()->where('code', $role)->count()) {
            return true;
        }
        return false;
    }

    public function goals()
    {
        return $this->hasMany('App\Models\Admin\UserGoal', 'user_id', 'id')->orderBy('created_at', 'DESC');
    }
    public function settings()
    {
        return $this->hasMany('App\Models\Admin\UserSetting', 'user_id', 'id');
    }
    public function userDesignation()
    {
        return $this->hasOne('App\Models\Admin\Designation', 'id', 'designation');
    }
    public function ipAddress()
    {
        return $this->hasMany('App\Models\Admin\UserIpAddress', 'user_id', 'id');
    }
    public function headOfDepartments()
    {
        return $this->hasMany('App\Models\Admin\Department', 'head', 'id');
    }
    public function firstContactOfDepartments()
    {
        return $this->hasMany('App\Models\Admin\Department', 'first_contact', 'id');
    }

    public function getOneDaySalaryAttribute()
    {
        return 1100;
    }
    public function getOneHourSalaryAttribute()
    {
        return 100;
    }

    public static function saveData($request)
    {

        try {
            $response = [
                'status' => true,
                'message' => "success",
                'id' => null,
                'log' => [],
                'errors' => [],
            ];

            if (empty($request['email'])) {
                $response['status'] = false;
                $response['log'][] = 'Email required';
                return $response;
            }
            if (empty($request['dob'])) {
                $response['status'] = false;
                $response['log'][] = 'Date of birth required';
                return $response;
            }

            if (!empty($request['onoffswitch']) == "on") {
                $is_active = 1;
            } else {
                $is_active = 0;
            }
            if (!empty($request['employee_id'])) {
                $employeeIdExists = User::where('employee_id', $request['employee_id'])->first();
                if ($employeeIdExists) {
                    $response['status'] = false;
                    $response['log'][] = 'Employee exists with this empoyee id, please enter another employee id';
                    return $response;
                }
            }

            $userObj = new User();
            $userObj->email = $request['email'];
            $userObj->name = $request['name'];
            $userObj->password = !empty($request['password']) ? bcrypt($request['password']) : bcrypt('goldtree9');
            $userObj->role = 'user';
            $userObj->confirmation_date = !empty($request['confirmation_date']) ? $request['confirmation_date'] : null;
            $userObj->joining_date = !empty($request['joining_date']) ? $request['joining_date'] : null;
            $userObj->employee_id = !empty($request['employee_id']) ? $request['employee_id'] : null;
            $userObj->dob = !empty($request['dob']) ? $request['dob'] : null;
            $userObj->gender = !empty($request['gender']) ? $request['gender'] : null;
            $userObj->pan = !empty($request['pan']) ? $request['pan'] : null;
            $userObj->bank_ac_no = !empty($request['bank_ac_no']) ? $request['bank_ac_no'] : null;
            $userObj->bank_ifsc_code = !empty($request['bank_ifsc_code']) ? $request['bank_ifsc_code'] : null;
            $userObj->pf_no = !empty($request['pf_no']) ? $request['pf_no'] : null;
            $userObj->uan_no = !empty($request['uan_no']) ? $request['uan_no'] : null;
            $userObj->esi_no = !empty($request['esi_no']) ? $request['esi_no'] : null;
            $userObj->months = !empty($request['months']) ? $request['months'] : null;
            $userObj->years = !empty($request['years']) ? $request['years'] : null;
            $userObj->designation = !empty($request['designation']) ? $request['designation'] : null;
            $userObj->is_active = $is_active;
            $userObj->parent_id = !empty($request['parent_id']) ? $request['parent_id'] : null;
            if (!$userObj->save()) {
                $response['status'] = false;

                $response['log'] = $userObj->getErrors(); //'Email is already taken, please use another email';
                return $response;
            }
            $response['id'] = $userObj->id;

            if (!empty($request['levels'])) {
                foreach ($request['levels'] as $role) {
                    $user_role = new UserRole();
                    $user_role->user_id = $userObj->id;
                    $user_role->role_id = $role;
                    $user_role->save();
                }
            }
            $default_role = Role::where('code', 'user')->first();
            $user_role = new UserRole();
            $user_role->user_id = $userObj->id;
            $user_role->role_id = $default_role->id;
            $user_role->save();
            $createProfile = Profile::createProfileFromUser($userObj);
            event(new UserCreated($userObj->id));
            if (!empty($request['parent_id'])) {
                event(new RmAssigned($userObj->id, $request['parent_id']));
            }
            return $response;
        } catch (\Exception $e) {
            Log::info('error here user model saveData function' . $e->getMessage());
        }
    }
    public static function filter()
    {
        $search = "";
        $arrayId = [];
        $result = array('status' => false, 'errors' => "", 'message' => "");
        $users = User::with('user_profile')->orderBy('employee_id', 'asc');
        if (Input::get('searchuser') != '') {
            $search = str_replace('+', ' ', Input::get('searchuser'));
            $userst = $users->get();
            foreach ($userst as $user) {
                if ((stripos($user->name, $search) !== false) || (stripos($user->email, $search) !== false) || (stripos($user->employee_id, $search) !== false)) {
                    array_push($arrayId, $user->id);
                }
            }
            $users = $users->whereIn('id', $arrayId);
            $users = $users->paginate(100);
            $result = ['status' => true, 'users' => $users, 'search' => $search];
            return $result;
        }
        if (Input::get('filter') == '') {
            $userst = $users->get();
            foreach ($userst as $user) {
                if ($user->is_active == 1) {
                    array_push($arrayId, $user->id);
                }
            }
            $users = $users->whereIn('id', $arrayId);
        }
        if (Input::get('filter') != '') {
            if (preg_replace('/[\s]+.*/', '', Input::get('filter')) == "unconfirmed") {
                $userst = $users->get();
                foreach ($userst as $user) {
                    if ((($user->confirmation_date == null) || ($user->confirmation_date == '0000-00-00')) && ($user->is_active == 1)) {
                        array_push($arrayId, $user->id);
                    }
                }
                $users = $users->whereIn('id', $arrayId);
            }
        }
        $arrayId = [];
        if (Input::get('filter') != '') {
            if (preg_replace('/[\s]+.*/', '', Input::get('filter')) == "deactive") {
                $userst = $users->get();
                foreach ($userst as $user) {
                    if ($user->is_active != 1) {
                        array_push($arrayId, $user->id);
                    }
                }
                $users = $users->whereIn('id', $arrayId);
            }
        }
        if (Input::get('filter') != '') {
            if (preg_replace('/[\s]+.*/', '', Input::get('filter')) == "billable") {
                $userst = $users->get();
                foreach ($userst as $user) {
                    if ($user->is_billable == 1) {
                        array_push($arrayId, $user->id);
                    }
                }
                $users = $users->whereIn('id', $arrayId);
            }
        }
        if (Input::get('filter') != '') {
            if (preg_replace('/[\s]+.*/', '', Input::get('filter')) == "non_billable") {
                $userst = $users->get();
                foreach ($userst as $user) {
                    if ($user->is_billable != 1) {
                        array_push($arrayId, $user->id);
                    }
                }
                $users = $users->whereIn('id', $arrayId);
            }
        }
        if (Input::get('filter') != '') {
            if (preg_replace('/[\s]+.*/', '', Input::get('filter')) == "admin") {
                $userst = $users->get();
                foreach ($userst as $user) {
                    if ($user->isAdmin()) {
                        array_push($arrayId, $user->id);
                    }
                }
                $users = $users->whereIn('id', $arrayId);
            }
        }
        if (Input::get('filter') != '') {
            if (preg_replace('/[\s]+.*/', '', Input::get('filter')) == "all") {
                $userst = $users->get();
                foreach ($userst as $user) {
                    array_push($arrayId, $user->id);
                }
                $users = $users->whereIn('id', $arrayId);
            }
        }
        $arrayId = [];
        $users = $users->paginate(100);
        $result = ['status' => true, 'users' => $users, 'search' => $search];
        return $result;
    }
    public static function getUsers($role = null)
    {
        if ($role == null) {
            $allUsers = User::where('is_active', 1)->get();
        } else {
            $allUsers = User::where('role', $role)->where('is_active', 1)->get();
        }
        $jsonuser = [];

        foreach ($allUsers as $user) {
            array_push($jsonuser, $user->name);
        }
        return json_encode($jsonuser);
    }

    public function getExperience()
    {
        $time1 = empty($this->release_date) || ($this->release_date < $this->confirmation_date) ? date("Y-m-d") : $this->release_date;
        $time2 = $this->confirmation_date;
        $precision = 3;
        // If not numeric then convert timestamps
        if (!is_int($time1)) {
            $time1 = strtotime($time1);
        }
        if (!is_int($time2)) {
            $time2 = strtotime($time2);
        }
        // If time1 > time2 then swap the 2 values
        if ($time1 > $time2) {
            list($time1, $time2) = array($time2, $time1);
        }
        // Set up intervals and diffs arrays
        $intervals = array('year', 'month', 'day', 'hour', 'minute', 'second');

        $diffs = array();
        foreach ($intervals as $interval) {
            // Create temp time from time1 and interval
            $ttime = strtotime('+1 ' . $interval, $time1);
            // Set initial values
            $add = 1;
            $looped = 0;
            // Loop until temp time is smaller than time2
            while ($time2 >= $ttime) {
                // Create new temp time from time1 and interval
                $add++;
                $ttime = strtotime("+" . $add . " " . $interval, $time1);
                $looped++;
            }
            $time1 = strtotime("+" . $looped . " " . $interval, $time1);
            if (!empty($this->years) && $interval == 'year') {
                $looped = $looped + $this->years;
            }
            if (!empty($this->months) && $interval == 'month') {
                $looped = $looped + $this->months;
                if ($looped > 11) {
                    $diffs['year'] = $diffs['year'] + 1;
                    $looped = $looped - 12;
                }
            }
            $diffs[$interval] = $looped;
        }
        $count = 0;
        $times = array();
        foreach ($diffs as $interval => $value) {
            // Break if we have needed precission
            if ($count >= $precision) {
                break;
            }
            // Add value and interval if value is bigger than 0
            if ($value > 0) {
                if ($value != 1) {
                    $interval .= "s";
                }
                // Add value and interval to times array
                $times[] = $value . " " . $interval;
                $count++;
            }
        }
        // Return string with times
        return implode(", ", $times);
    }
    public static function getUpcomingBirthdays()
    {

        $date = date('m', strtotime(date('Y-m-d') . ' -1 day'));

        $currentMonth = date("m");
        $currentDay = date("d");
        if ($currentMonth == 01 && $currentDay < 7) {
            $date = '01';
        }

        if ($currentMonth == 01 && $currentDay < 7) {
            $date = '01';
        }

        $currentMonthPreviousCount = 0;
        $currentMonthUpcomingCount = 0;
        $previousMonthCount = 0;
        $upcomingMonthCount = 0;

        $users_a = User::where('is_active', 1)->whereMonth('dob', '>=', $date)->orderByRaw('MONTH(dob)', 'ASC')->orderByRaw('DAY(dob)', 'ASC')->take(10)->get();

        $currentMonthPrevoiusList = User::where('is_active', 1)->whereMonth('dob', '=', $date)->whereDay('dob', '<', $currentDay)
            ->orderByRaw('DAY(dob)', 'DESC')->get();

        $currentMonthUpcomingList = User::where('is_active', 1)->whereMonth('dob', '=', $date)->whereDay('dob', '>=', $currentDay)->orderByRaw('DAY(dob)', 'ASC')->get();

        $currentMonthPreviousCount = count($currentMonthPrevoiusList);
        $currentMonthUpcomingCount = count($currentMonthUpcomingList);

        if ($currentMonthPreviousCount >= 4) {
            $toSlice = $currentMonthPreviousCount - 4;
            $currentMonthPrevoiusList = $currentMonthPrevoiusList->slice($toSlice)->values();
        } else {
            $previousBirthdayList = User::where('is_active', 1)->whereMonth('dob', '>=', '01')->whereMonth('dob', '<', $date)->orderByRaw('MONTH(dob)', 'DESC')
                ->orderByRaw('DAY(dob)', 'DESC')->get();
            $previousMonthCount = count($previousBirthdayList);
            $toSlice = ($previousMonthCount + $currentMonthPreviousCount - 4);
            $previousBirthdayList = $previousBirthdayList->slice($toSlice)->values();
        }

        if ($currentMonthUpcomingCount >= 6) {
            // $toSlice = count($currentMonthUpcomingList) - 6;
            $removed = $currentMonthUpcomingList->splice(6);
        } else {
            $toTake = (6 - $currentMonthUpcomingCount);
            $upcomingBirthdayList = User::where('is_active', 1)->whereMonth('dob', '>', $date)->orderByRaw('MONTH(dob)', 'DESC')->orderByRaw('DAY(dob)', 'ASC')->take($toTake)->get();
        }

        if (isset($previousBirthdayList)) {
            $previousList = $previousBirthdayList->merge($currentMonthPrevoiusList);
        } else {
            $previousList = $currentMonthPrevoiusList;
        }
        if (isset($upcomingBirthdayList)) {
            $upcomingList = $currentMonthUpcomingList->merge($upcomingBirthdayList);
        } else {
            $upcomingList = $currentMonthUpcomingList;
        }

        $users = $previousList->merge($upcomingList);
        $upcomingBirthdayList = [];
        $upcomingBirthday = [];
        $total_count = 0;
        $next_check = 0;
        $is_next = false;
        $is_next = false;
        $monthCounter = $currentMonth;

        foreach ($users as $user) {
            if (!empty($user->dob)) {
                $upcomingBirthday = [];
                $upcomingBirthday['is_next'] = false;
                if (!$is_next && date('m-d', strtotime($user->dob)) >= date('m-d')) {
                    $is_next = true;
                    $upcomingBirthday['is_next'] = true;
                }

                $user_dob_month = date("m", strtotime($user->dob));
                $user_dob_day = date("d", strtotime($user->dob));

                $upcomingBirthday['name'] = $user->name;
                $upcomingBirthday['dob'] = $user->dob;

                $upcomingBirthdayList[] = $upcomingBirthday;
            }
        }

        return $upcomingBirthdayList;

    }
    public function getRmTree()
    {

    }
}
