<?php

namespace App\Models;

use App\Models\BaseModel;
use App\Services\BonusService;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserTimesheet extends BaseModel
{
    protected $dates = ['deleted_at'];
    protected $table = 'user_timesheets';
    public $timestamps = true;
    use SoftDeletes;

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    public function project()
    {
        return $this->hasOne('App\Models\Admin\Project', 'id', 'project_id');
    }

    public function review()
    {
        return $this->hasOne('App\Models\UserTimesheetReview', 'user_timesheet_id', 'id');
    }
    // public function getExtraHourAttribute()
    // {
    //     $user = \Auth::user();

    //     if (!$user->hasRole('admin')) {
    //         return 0;
    //     }

    //     $amount = BonusService::getBonusAmount($this->id);
    //     return $amount;
    // }
    public static function enterUserTimesheet($userId, $projectId, $date, $duration, $task)
    {
        $status = true;
        $message = "Already Exits";
        if ($duration < 0) {
            $response['status'] = false;
            $response['message'] = 'Duration cannot be a negative value';
            return $response;
        }
        $response = UserTimesheet::where('user_id', $userId)->where('project_id', $projectId)->where('date', $date)->where('duration', $duration)->where('task', $task)->first();
        if (empty($response)) {

            $userTimesheetObj = new UserTimesheet();
            $userTimesheetObj->user_id = $userId;
            $userTimesheetObj->project_id = $projectId;
            $userTimesheetObj->date = $date;
            $userTimesheetObj->duration = $duration;

            // if ($duration > 8) {
            //     $userTimesheetObj->approved_duration = 8;
            //     $userTimesheetObj->extra_hours = $duration - 8;

            // } else {
            //     $userTimesheetObj->approved_duration = $duration;
            //     $userTimesheetObj->extra_hours = 0;

            // }

            $userTimesheetObj->task = addslashes($task);
            $userTimesheetObj->comment = addslashes($task);

            $userTimesheetObj->status = 'pending';
            if (!$userTimesheetObj->save()) {
                $status = false;
                $message = $userTimesheetObj->getErrors();
            } else {
                $message = $userTimesheetObj->id;
            }
        }

        $response['status'] = $status;
        $response['message'] = $message;

        return $response;
    }

}
