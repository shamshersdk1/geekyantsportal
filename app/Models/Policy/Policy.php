<?php

namespace App\Models\Policy;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;
use OwenIt\Auditing\Contracts\Auditable;

class Policy extends Model implements Auditable
{
    use ValidatingTrait;
    use \OwenIt\Auditing\Auditable;

    protected $table = 'policies';
    public $timestamps=true;
    protected $fillable = ['name','reference_number','effective_date','url','status','type'];
    
    protected $rules = [
        'name'=>'required|string',
        'effective_date'=>'required|date',
        'url' => 'required',
        'status' => 'required',
        'type' => 'required',
    ];
}
