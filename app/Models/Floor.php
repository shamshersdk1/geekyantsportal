<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\User;

class Floor extends Model
{

	use ValidatingTrait;
	protected $table = 'floors';
	public $timestamps = true;
	private $rules = array('name' => 'required');
	
	public function seats() {
        return $this->hasMany('App\Models\FloorsSeat', 'floor_id', 'id' );
    }
	public static function checkFloorNumber($floor_number)
	{
		$flag = 0;
		$floors = Floor::all();
		foreach ( $floors as $floor )
		{
			if ( $floor_number == $floor->floor_number )
			{
				$flag = 1;
			}
		}
		if ( $flag == 1 )
			return true;
		else
			return false;
	}

	public function getOccupiedCount()
	{
		$count = 0;
		$floorsSeats = FloorsSeat::where('floor_id',$this->id)->get();
		foreach ( $floorsSeats as $floorsSeat )
		{
			$floorSeatUser = FloorsSeatUser::where('floor_seat_id',$floorsSeat->id)->first();
			if( isset($floorSeatUser) ){
				$count++;	
			}
		}
		return $count;
	}
	
}
