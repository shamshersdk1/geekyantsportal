<?php
namespace App\Models;
use Log;
use Exception;

use Illuminate\Database\Eloquent\Model;
use App\Models\Admin\Leave;

class NonworkingCalendar extends Model
{

    protected $fillable = [];

    protected $table = 'non_working_calendar'; 

    protected $rules = [
    ];

    public static $cache;

    public static function is_holiday($date){
        $result = self::where([
            ['date','=',date('Y-m-d', strtotime($date))],
            ['type','=','Holiday']
        ]
        )->first();
        if($result){
            return $result;
        }
        return false;
    }

    public static function isWeekend($date) {
        return (date('N', strtotime($date)) >= 6);
    }

    public static function getCalendarDatewise($month, $year){
        if(isset(self::$cache[$month.$year])){
            return self::$cache[$month.$year];
        }

        $summary = ['workingDays'=>[], 'holidays'=>[], 'weekends' =>[]];
        $calendar = [];
        $firstDay = $year."-".$month."-01"; // first date;
        $lastDay = date('t', strtotime($firstDay));

        for($i=1; $i<=$lastDay; $i++){
            $day = $year."-".$month."-".$i;
            $calendar[$day] = array(
                'holiday' => self::is_holiday($day), 
                'weekend' => self::isWeekend($day),
                'day'=>date('D', strtotime($day))
            );

            if($calendar[$day]['holiday'] !== false){
                 $summary['holidays'][] = $calendar[$day];
            }else if($calendar[$day]['weekend'] !== false){
                 $summary['weekends'][] = $calendar[$day];
            }else {
                 $summary['workingDays'][] = $calendar[$day];
            }
        }
        self::$cache[$month.$year] = array("summary"=>$summary, "calendar"=>$calendar);

        return self::$cache[$month.$year];
    }

    public static function getUserCalendarDatewise($user_id, $month, $year){
        $data = self::getCalendarDatewise($month, $year);
        $data['summary']['leaves'] = [];
        foreach($data['calendar'] as $date => $date_summary){
            $leave = Leave::isOnLeaveToday($user_id, $date);
            if($leave){
                $data['summary']['leaves'][] = $leave;
                $data['summary'][$leave->type][] = $leave;
                $data['calendar'][$date]['leave'] = $leave;
            }
        }

        
        $workingDays = count($data['summary']['workingDays']);
        $paidLeaves = isset($data['summary']['paid'])?count($data['summary']['paid']):0;
        $sickLeaves = isset($data['summary']['sick'])?count($data['summary']['sick']):0;
        $halfLeaves = isset($data['summary']['half'])?count($data['summary']['half']):0;
        $halfLeaves = round($halfLeaves/2, 2);
        $workedDays = $workingDays - $paidLeaves - $sickLeaves - $halfLeaves;

        $data['summary']['counts'] = array(
            'workedDays'=>$workedDays,
            'workingDays'=>$workingDays,
            'paidLeaves'=>$paidLeaves,
            'sickLeaves'=>$sickLeaves,
            'halfLeaves'=>$halfLeaves
            );
        return $data;
    }
}