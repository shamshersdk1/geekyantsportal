<?php

namespace App\Models\Audited\MonthlyVariableBonus;

use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\Model;

class MonthlyVariableBonus extends Model
{
    use ValidatingTrait;

    protected $table = 'monthly_variable_bonuses';
    public $timestamps = false;

    protected $fillable = ['user_id','month_id','percentage','amount','actual_amount'];

    protected $rules = [
        'user_id' => 'required | exists:users,id',
        'month_id' => 'required | exists:months,id',
        'percentage' => 'required',
        'amount' => 'required',
        'actual_amount' => 'required',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
