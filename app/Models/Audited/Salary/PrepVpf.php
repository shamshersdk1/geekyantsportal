<?php

namespace App\Models\Audited\Salary;

use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\Model;

class PrepVpf extends Model
{
    use ValidatingTrait;

    protected $table = 'prep_vpfs';
    public $timestamps = false;
    protected $fillable = ['prep_salary_id','user_id','vpf_deduction_id','value'];

    protected $rules = [
        'prep_salary_id' => 'required | exists:prep_salary,id',
        'user_id' => 'required | exists:users,id',
        'vpf_deduction_id' => 'required | exists:vpf_deductions,id',
        'value' => 'required | numeric'
    ];

}
