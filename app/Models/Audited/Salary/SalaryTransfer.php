<?php

namespace App\Models\Audited\Salary;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class SalaryTransfer extends Model
{
    use ValidatingTrait;

    protected $table = 'salary';
    public $timestamps = true;
    protected $fillable = ['month_id', 'status'];

    protected $rules = [
        'month_id' => 'required | exists:months,id',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
