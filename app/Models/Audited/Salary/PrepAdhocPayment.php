<?php

namespace App\Models\Audited\Salary;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class PrepAdhocPayment extends Model
{
    use ValidatingTrait;

    public $timestamps = false;
    protected $fillable = ['prep_salary_id','user_id'];
    public $table = 'prep_adhoc_payments';

    protected $rules = [
        'prep_salary_id' => 'required|exists:prep_salary,id',
        'user_id' => 'required|exists:users,id',
    ];

    public function components()
    {
        return $this->hasMany('App\Models\Audited\Salary\PrepAdhocPaymentComponent', 'prep_adhoc_payment_id', 'id');
    }

    // public function componentByKey($key)
    // {
    //     return $this->hasOne('App\Models\Audited\Salary\PrepAdhocPaymentComponent', 'prep_adhoc_payment_id', 'id')->where('key', $key)->first();
    // }

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($model) {
            $model->components()->delete();
        });
    }
}
