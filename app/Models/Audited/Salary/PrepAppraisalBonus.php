<?php

namespace App\Models\Audited\Salary;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class PrepAppraisalBonus extends Model
{
    use ValidatingTrait;
    
    protected $table = 'prep_appraisal_bonuses';
    public $timestamps = false;
    protected $fillable = ['prep_salary_id','user_id','appraisal_bonus_id','value'];

    protected $rules = [
        'prep_salary_id' => 'required|exists:prep_salary,id',
        'user_id' => 'required|exists:users,id',
        'appraisal_bonus_id' => 'required|exists:appraisal_bonuses,id',
        'value' => 'required',
    ];

    public function salary()
    {
        return $this->belongsTo('App\Models\Audited\Salary\PrepSalary', 'prep_salary_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
    public function appraisalBonus()
    {
        return $this->belongsTo('App\Models\Appraisal\AppraisalBonus', 'appraisal_bonus_id', 'id');
    }
    public function appraisalBonusTypes()
    {
        return $this->belongsTo('App\Models\Appraisal\AppraisalBonusType', 'appraisal_bonus_id', 'id');
    }
}
