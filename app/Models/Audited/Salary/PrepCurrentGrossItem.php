<?php

namespace App\Models\Audited\Salary;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class PrepCurrentGrossItem extends Model
{
    use ValidatingTrait;

    public $timestamps = true;

    protected $rules = [
        'prep_current_gross_id' => 'required|exists:prep_current_gross,id',
        'key' => 'required',
        'value' => 'required',
    ];

}
