<?php

namespace App\Models\Audited\Salary;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class PrepGrossPaid extends Model
{
    use ValidatingTrait;
    public $timestamps = true;

    protected $rules = [
        'prep_salary_id' => 'required|exists:prep_salary,id',
        'user_id' => 'required|exists:users,id',
    ];

    public function items()
    {
        return $this->hasMany('App\Models\Audited\Salary\PrepGrossPaidItem', 'prep_gross_paid_id', 'id');
    }
    public function prepSalary()
    {
        return $this->belongsTo('App\Models\Audited\Salary\PrepSalary', 'prep_salary_id', 'id');
    }
    public static function boot()
    {
        parent::boot();
        static::deleting(function($model) {
            foreach ($model->items as $item) {
                $item->delete();
            }
        });
    }

    public function itemByKey($key)
    {
        return $this->hasOne('App\Models\Audited\Salary\PrepGrossPaidItem', 'prep_gross_paid_id', 'id')->where('key', $key)->first();
    }
}
