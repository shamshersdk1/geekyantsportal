<?php

namespace App\Models\Audited\Salary;

use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\BaseModel;
class PrepSalaryComponentTypeDependency extends BaseModel
{
    use ValidatingTrait;

    protected $table = 'prep_salary_component_type_dependencies';
    public $timestamps = false;

    protected $rules = [
		'component_id' => 'required | exists:prep_salary_component_types,id',
        'dependent_id' => 'required | exists:prep_salary_component_types,id',
    ];

    public function dependentOn(){
        return $this->belongsTo('App\Models\Audited\Salary\PrepSalaryComponentType','dependent_id','id');
    }

    public function dependentReverseOn(){
        return $this->belongsTo('App\Models\Audited\Salary\PrepSalaryComponentType','component_id','id');
    }

}
