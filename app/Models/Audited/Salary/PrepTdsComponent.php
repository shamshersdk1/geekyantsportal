<?php

namespace App\Models\Audited\Salary;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class PrepTdsComponent extends Model
{
    use ValidatingTrait;

    public $timestamps = false;
    public $table = "prep_tds_components";

    protected $rules = [
        'prep_tds_id' => 'required|exists:prep_tds,id',
        'key' => 'required',
        'value' => 'required',
    ];

}
