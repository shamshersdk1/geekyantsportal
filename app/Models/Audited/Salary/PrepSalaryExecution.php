<?php

namespace App\Models\Audited\Salary;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;
use App\Models\Audited\Salary\PrepSalary;

class PrepSalaryExecution extends Model
{
    use ValidatingTrait;

    protected $table = 'prep_salary_executions';
    public $timestamps = false;
    protected $fillable = ['prep_salary_id','component_id','user_id','status','counter'];
    
    protected $rules = [
		 'prep_salary_id' => 'required | exists:prep_salary,id',
         'user_id' => 'required|exists:users,id',
         'component_id' => 'required|exists:prep_salary_components,id'
    ];

    public static function componentStatus($prepSalaryId)
    {
        $prepSalaryObj = PrepSalary::find($prepSalaryId);
        if(!$prepSalaryObj && !(count($prepSalaryObj->components)>0))
        {
            return false;
        }
        $prepExecutions = self::where('prep_salary_id',$prepSalaryId)->get();
        if(!count($prepExecutions)>0)
        {
            return false;
        }
        foreach($prepSalaryObj->components as $component)
        {
            $count = self::where('prep_salary_id',$prepSalaryId)->where('component_id',$component->id)->where('status','!=','completed')->where('status','!=','failed')->count();
            if($count == 0)
            {
                $component->is_generated = 1;
                $component->save();
            }  
        }
        $count = $prepSalaryObj->components()->where('is_generated','0')->count();
        if($count == 0 && $prepSalaryObj->status == 'in_progress')
        {
            $prepSalaryObj->status = 'processed';
            $prepSalaryObj->save();
        }
        return true;
    }

}
