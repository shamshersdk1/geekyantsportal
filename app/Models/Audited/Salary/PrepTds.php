<?php

namespace App\Models\Audited\Salary;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class PrepTds extends Model
{
    use ValidatingTrait;

    public $timestamps = false;
    public $table = 'prep_tds';

    protected $rules = [
        'prep_salary_id' => 'required|exists:prep_salary,id',
        'user_id' => 'required|exists:users,id',
    ];

    public function components()
    {
        return $this->hasMany('App\Models\Audited\Salary\PrepTdsComponent', 'prep_tds_id', 'id');
    }

    public function componentByKey($key)
    {
        return $this->hasOne('App\Models\Audited\Salary\PrepTdsComponent', 'prep_tds_id', 'id')->where('key', $key)->first();
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($model) {
            $model->components()->delete();
        });
    }
}
