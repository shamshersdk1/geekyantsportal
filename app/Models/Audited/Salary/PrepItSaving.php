<?php

namespace App\Models\Audited\Salary;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class PrepItSaving extends Model
{
    use ValidatingTrait;

    protected $table = 'prep_it_savings';
    public $timestamps = false;

    protected $rules = [
        'prep_salary_id' => 'required|exists:prep_salary,id',
        'user_id' => 'required|exists:users,id',
    ];

    public static function boot()
    {
        parent::boot();
        static::deleting(function ($prepItSavingObj) {
            $prepItSavingObj->components()->delete();
        });
    }

    public function components()
    {
        return $this->hasMany('App\Models\Audited\Salary\PrepItSavingComponent', 'prep_it_saving_id', 'id');
    }

    public function componentByKey($key)
    {
        return $this->hasOne('App\Models\Audited\Salary\PrepItSavingComponent', 'prep_it_saving_id', 'id')->where('key', $key)->first();
    }

    public function salary()
    {
        return $this->belongsTo('App\Models\Audited\Salary\PrepSalary', 'prep_salary_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

}
