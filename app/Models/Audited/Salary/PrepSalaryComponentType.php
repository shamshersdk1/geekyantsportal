<?php

namespace App\Models\Audited\Salary;

use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\BaseModel;
class PrepSalaryComponentType extends BaseModel
{
    use ValidatingTrait;
    
    protected $table = 'prep_salary_component_types';
    public $timestamps = false;

    protected $rules = [
		'code' => 'required|unique:prep_salary_component_types',
        'name' => 'required | string',
        'service_class' => 'required | string | unique:prep_salary_component_types',
    ];

    public  function prepSalaryComponent(){

        return $this->hasMany('App\Models\Audited\Salary\PrepSalaryComponent','prep_salary_component_type_id');
    }

    public function dependsOn(){
        return $this->hasMany('App\Models\Audited\Salary\PrepSalaryComponentTypeDependency','component_id','id');
    }

    public function dependentOn(){
        return $this->hasMany('App\Models\Audited\Salary\PrepSalaryComponentTypeDependency','dependent_id','id');
    }
}
