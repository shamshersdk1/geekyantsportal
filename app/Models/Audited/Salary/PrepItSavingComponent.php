<?php

namespace App\Models\Audited\Salary;

use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\Model;

class PrepItSavingComponent extends Model
{
    use ValidatingTrait;

    protected $table = 'prep_it_savings_components';
    public $timestamps = false;

    protected $rules = [
        'prep_it_saving_id' => 'required|exists:prep_it_savings,id',
    ];
}
