<?php

namespace App\Models\Audited\Salary;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class PrepGrossToBePaid extends Model
{
    use ValidatingTrait;

    public $timestamps = true;

    protected $rules = [
        'prep_salary_id' => 'required|exists:prep_salary,id',
        'user_id' => 'required|exists:users,id',
    ];

    public function items()
    {
        return $this->hasMany('App\Models\Audited\Salary\PrepGrossToBePaidItem');
    }

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($model) {
            $model->items()->delete();
        });
    }

    public function itemByKey($key)
    {
        return $this->hasOne('App\Models\Audited\Salary\PrepGrossToBePaidItem', 'prep_gross_to_be_paid_id', 'id')->where('key', $key)->first();
    }
}
