<?php

namespace App\Models\Audited\Salary;

use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\Model;

class PrepSalaryUser extends Model
{
    use ValidatingTrait;

    protected $table = 'prep_salary_users';
    public $timestamps = false;
    
    protected $rules = [
		 'prep_salary_id' => 'required | exists:prep_salary,id',
         'user_id' => 'required|exists:users,id',
         'prep_salary_component_id' => 'required|exists:prep_salary_component_types,id'
    ];
}
