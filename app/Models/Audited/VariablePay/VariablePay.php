<?php

namespace App\Models\Audited\VariablePay;

use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\Model;

class VariablePay extends Model
{
    use ValidatingTrait;

    protected $table = 'variable_pays';
    public $timestamps = false;

    protected $fillable = ['user_id','month_id','appraisal_id'];

    protected $rules = [
        'user_id' => 'required | exists:users,id',
        'month_id' => 'required | exists:months,id',
        'appraisal_id' => 'required | exists:appraisals,id',
    ];

    public static function boot()
    {
        parent::boot();
        static::deleting(function ($lastMonthPay) {
            $lastMonthPay->lastMonthPayComponents()->delete();
        });
    }

    public function lastMonthPayComponents()
    {
        return $this->hasMany('App\Models\Audited\VariablePay\VariablePayComponent', 'variable_pay_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
