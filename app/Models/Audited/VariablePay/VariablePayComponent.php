<?php

namespace App\Models\Audited\VariablePay;

use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\Model;

class VariablePayComponent extends Model
{
    use ValidatingTrait;

    protected $table = 'variable_pay_components';
    public $timestamps = false;

    protected $fillable = ['variable_pay_id','key','value','appraisal_bonus_id'];

    protected $rules = [
        'variable_pay_id' => 'required | exists:variable_pays,id',
        'key' => 'required',
        'value' => 'required',
        'appraisal_bonus_id' => 'required | exists:appraisal_bonuses,id',
    ];
}
