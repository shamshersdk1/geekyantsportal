<?php

namespace App\Models\Audited\FixedBonus;

use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\Model;

class FixedBonus extends Model
{
    use ValidatingTrait;

    protected $table = 'fixed_bonuses';
    public $timestamps = false;

    protected $fillable = ['user_id','month_id','appraisal_id'];

    protected $rules = [
        'user_id' => 'required | exists:users,id',
        'month_id' => 'required | exists:months,id',
        'appraisal_id' => 'required | exists:appraisals,id',
    ];

    public static function boot()
    {
        parent::boot();
        static::deleting(function ($monthlyDeduction) {
            $monthlyDeduction->monthlyDeductionComponents()->delete();
        });
    }

    public function monthlyDeductionComponents()
    {
        return $this->hasMany('App\Models\Audited\FixedBonus\FixedBonusComponent', 'fixed_bonus_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
