<?php

namespace App\Models\Audited\WorkingDay;

use Illuminate\Database\Eloquent\Model;
use App\Models\MonthSetting;
use Watson\Validating\ValidatingTrait;

class UserWorkingDay extends Model
{
    use ValidatingTrait;

    protected $table = 'working_days';
    public $timestamps = false;

    protected $fillable = ['user_id', 'month_id', 'working_days', 'lop','comment','type'];

    protected $rules = [
        'user_id' => 'required | exists:users,id',
        'month_id' => 'required | exists:months,id',
        'working_days' => 'required | integer | min:0',
        'lop' => 'required | numeric | max:0',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
    public function month()
    {
        return $this->belongsTo('App\Models\Month', 'month_id', 'id');
    }
}
