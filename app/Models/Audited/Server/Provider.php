<?php

namespace App\Models\Audited\Server;

use App\Traits\ValidationTrait;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Provider extends Model implements Auditable
{

    use ValidationTrait;
    use \OwenIt\Auditing\Auditable;

    protected $table = 'server_providers';
    public $timestamps = false;

    protected $rules = array(
        'name' => 'required | unique :server_providers',
    );
    public function rulesFunc($id)
    {
        $this->rules['name'] = $this->rules['name'] . ',name,' . $id;
    }
    public function providerAccounts()
    {
        return $this->hasMany('App\Models\Audited\Server\ProviderAccount', 'provider_id', 'id');
    }

    public function account()
    {
        return $this->hasMany('App\Models\Audited\Server\ProviderAccount', 'provider_id');
    }
    public function servers()
    {
        return $this->hasMany('App\Models\Audited\Server\Server', 'provider_id');
    }
    public static function boot()
    {
        parent::boot();
        static::deleting(function ($obj) {
            $obj->providerAccounts()->delete();
        });
    }

}
