<?php

namespace App\Models\Audited\Server;

use App\Traits\ValidationTrait;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Account extends Model implements Auditable
{
    use ValidationTrait;
    use \OwenIt\Auditing\Auditable;

    protected $table = 'server_accounts';
    public $timestamps = true;

    protected $rules = array(
        'username' => 'required|unique:server_accounts',
        'server_id' => 'required|exists:servers,id',
    );

    public function access()
    {
        return $this->hasMany('App\Models\Audited\Server\AccountAccess', 'account_id');
    }
    public static function boot()
    {
        parent::boot();

        static::deleting(function ($accountObj) {
            $accountObj->access()->delete();
        });
    }
}
