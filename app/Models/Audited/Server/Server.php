<?php

namespace App\Models\Audited\Server;

use App\Traits\ValidationTrait;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Server extends Model implements Auditable
{

    use ValidationTrait;
    use \OwenIt\Auditing\Auditable;

    protected $table = 'servers';
    public $timestamps = true;

    protected $rules = array(
        'name' => 'required|unique:servers',
        'city' => 'required',
        'country' => 'required',
        'primary_ip' => 'required|ip|unique:servers',
        'hostname' => 'required|unique:servers',
        'provider_id' => 'required|exists:server_providers,id',
        'type_id' => 'required|exists:server_type,id',
        'provider_account_id' => 'required|exists:server_provider_accounts,id',
    );
    public function rulesFunc($id)
    {
        $this->rules['name'] = $this->rules['name'] . ',name,' . $id;
        $this->rules['primary_ip'] = $this->rules['primary_ip'] . ',id,' . $id;
        $this->rules['hostname'] = $this->rules['hostname'] . ',id,' . $id;
    }
    public function returnRules()
    {
        return $this->rules;
    }
    public function accountAccess()
    {
        return $this->hasMany('App\Models\Audited\Server\AccountAccess', 'server_id');
    }

    public function provider()
    {
        return $this->belongsTo('App\Models\Audited\Server\Provider', 'provider_id');
    }

    public function providerAccount()
    {
        return $this->belongsTo('App\Models\Audited\Server\ProviderAccount', 'provider_account_id');
    }

    public function type()
    {
        return $this->belongsTo('App\Models\Audited\Server\ServerType', 'type_id');
    }
    public function note()
    {
        return $this->hasMany('App\Models\Audited\Server\ServerNote', 'server_id');
    }

    public function meta()
    {
        return $this->hasMany('App\Models\Audited\Server\ServerMeta', 'server_id');
    }

    public function account()
    {
        return $this->hasMany('App\Models\Audited\Server\Account', 'server_id');
    }

    public function saveServer($data)
    {
        $response = [];
        $response['status'] = false;
        $response['message'] = 'Something went wrong!';

        $this->name = $data['name'];
        $this->primary_ip = $data['primary_ip'];
        $this->hostname = $data['hostname'];
        $this->root_password = $data['root_password'];
        $this->city = $data['city'];
        $this->country = $data['country'];
        $this->provider_id = $data['provider_id'];
        $this->type_id = $data['type_id'];
        $this->provider_account_id = $data['provider_account_id'];
        $this->notes = $data['notes'];

        if ($this->save()) {
            $response['status'] = true;
            $response['message'] = '';
        }
        return $response;
    }

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($serverObj) {
            $serverObj->meta()->delete();
            $serverObj->note()->delete();
            $serverObj->account()->delete();
        });
    }

}
