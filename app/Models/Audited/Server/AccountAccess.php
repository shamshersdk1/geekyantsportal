<?php

namespace App\Models\Audited\Server;

use App\Traits\ValidationTrait;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class AccountAccess extends Model implements Auditable
{
    use ValidationTrait;
    use \OwenIt\Auditing\Auditable;

    protected $table = 'server_accounts_access';
    public $timestamps = true;
    public $errors;

    protected $rules = array(
        'user_id' => 'required|exists:users,id',
        'server_id' => 'required|exists:servers,id',
        'account_id' => 'required|exists:server_accounts,id',
    );

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
    public function account()
    {
        return $this->belongsTo('App\Models\Audited\Server\Account', 'account_id', 'id');
    }
    public function server()
    {
        return $this->belongsTo('App\Models\Audited\Server\Server', 'server_id', 'id');
    }

}
