<?php

namespace App\Models\Audited\Server;

use App\Traits\ValidationTrait;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class ServerType extends Model implements Auditable
{

    use ValidationTrait;

    use \OwenIt\Auditing\Auditable;

    protected $table = 'server_type';
    public $timestamps = false;

    protected $rules = array(
        'name' => 'required : server_type',
        'code' => 'required | unique:server_type',
    );
    public function rulesFunc($id)
    {
        $this->rules['code'] = $this->rules['code'] . ',code,' . $id;
    }
}
