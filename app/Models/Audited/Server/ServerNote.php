<?php

namespace App\Models\Audited\Server;

use App\Traits\ValidationTrait;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class ServerNote extends Model implements Auditable
{

    use ValidationTrait;
    use \OwenIt\Auditing\Auditable;

    protected $table = 'server_notes';
    public $timestamps = true;
    
    protected $rules = array(
        'server_id' => 'required|exists:servers,id',
        'notes' => 'required',
    );

    public static function saveData($data)
    {
        $response['status'] = false;
        $response['message'] = null;
        $serverNoteObj = new ServerNote();

        if ($serverNoteObj->validate($data)) {
            $serverNoteObj->server_id = $data['server_id'];
            $serverNoteObj->notes = $data['notes'];
            if ($serverNoteObj->save()) {
                $response['status'] = true;
                return $response;
            } else {
                $response['message'] = "Failed to save data in ServerNote";
                return $response;
            }
        } else {
            $response['message'] = $serverNoteObj->getError();
        }

    }
}
