<?php

namespace App\Models\Audited\Server;

use App\Traits\ValidationTrait;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class ServerMeta extends Model implements Auditable
{

    use ValidationTrait;
    use \OwenIt\Auditing\Auditable;

    protected $table = 'server_meta';
    public $timestamps = false;
    protected $rules = array(
        'server_id' => 'required|exists:servers,id',
        'key' => 'required',
        'value' => 'required',
    );

    public static function saveData($data)
    {
        $response['status'] = false;
        $response['message'] = null;
        $serverMetaObj = new ServerMeta();
        if ($serverMetaObj->validate($data)) {
            $serverMetaObj->server_id = $data['server_id'];
            $serverMetaObj->key = $data['key'];
            $serverMetaObj->value = $data['value'];
            try {
                $serverMetaObj->save();
                $response['status'] = true;
                return $response;
            } catch (\Exception $e) {
                $response['message'] = $e->getMessage();
                return $response;
            }
        } else {
            $response['message'] = $serverMetaObj->getError();
        }

    }

    public static function deleteData($metaId)
    {
        $response['status'] = false;
        $response['message'] = null;
        if (Self::destroy($metaId)) {
            $response['status'] = true;
            return $response;
        } else {
            $response['message'] = "Failed to delete data from ServerMeta";
            return $response;
        }
    }
}
