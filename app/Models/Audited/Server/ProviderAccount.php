<?php

namespace App\Models\Audited\Server;

use App\Traits\ValidationTrait;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class ProviderAccount extends Model implements Auditable
{

    use ValidationTrait;
    use \OwenIt\Auditing\Auditable;

    protected $table = 'server_provider_accounts';
    protected $appends = ['provider_name'];

    public $timestamps = true;
    protected $rules = array(
        'provider_id' => 'required|exists:server_providers,id',
        'username' => 'required|unique:server_provider_accounts',
        'password' => 'required ',
        'url' => 'required',
        'notes' => 'required',
    );
    public function rulesFunc($id)
    {
        $this->rules['username'] = $this->rules['username'] . ',username,' . $id;
    }
    public function getProviderNameAttribute()
    {
        if ($this->provider) {
            return $this->provider->name . ' - ' . $this->username;
        }

        return null;
    }

    public function provider()
    {
        return $this->belongsTo('App\Models\Audited\Server\Provider', 'provider_id', 'id');
    }

}
