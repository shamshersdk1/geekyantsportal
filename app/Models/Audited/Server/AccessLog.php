<?php

namespace App\Models\Audited\Server;

use App\Traits\ValidationTrait;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class AccessLog extends Model implements Auditable
{

    use ValidationTrait;
    use \OwenIt\Auditing\Auditable;

    protected $table = 'server_access_log';
    public $timestamps = true;

    protected $rules = array(
        'user_id' => 'required|exists:users,id',
        'server_id' => 'required|exists:servers,id',
    );
    protected $auditEvents = ['deleted', 'restored', 'updated'];

}
