<?php

namespace App\Models\Audited\AdhocPayments;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class AdhocPaymentComponent extends Model
{
    use ValidatingTrait;

    protected $table = 'adhoc_payment_components';
    public $timestamps = false;

    protected $fillable = ['key','description','type'];

    protected static function boot()
    {
        parent::boot();
        static::deleting(function ($appraisalComponentTypeObj) {
            if (count($appraisalComponentTypeObj->values) > 0) {
                return false;
            }
        });
    }

    protected $rules = [
        'key' => 'required | unique:adhoc_payment_components',
        'description' => 'required | string',
        'type' => 'required',
    ];

    public function values()
    {
        return $this->hasMany('App\Models\Audited\AdhocPayments\AdhocPayment', 'adhoc_payment_component_id', 'id');
    }

    public static function getIdByKey($key)
    {
        $obj = self::where('key', $key)->first();
        return $obj->id;
    }
}
