<?php

namespace App\Models\Audited\BankTransfer;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class BankTransferUser extends Model
{
    use ValidatingTrait;

    public $timestamps = true;
    protected $fillable = ['bank_transfer_id','user_id','amount','actual_amount','mode_of_transfer','reference_number','status'];
    public $table = 'bank_transfer_users';

    protected $rules = [
        'bank_transfer_id' => 'required | exists:bank_transfers,id',
        'user_id' => 'required | exists:users,id',
        'amount' => 'required | numeric',
        //'actual_amount' => 'required | numeric',
        //'mode_of_transfer' => 'required',
        //'reference_number' => 'required',
        //'status' => 'required',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
