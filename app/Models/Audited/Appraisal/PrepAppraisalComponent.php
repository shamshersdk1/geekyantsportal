<?php

namespace App\Models\Audited\Appraisal;

use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\Audited\Appraisal\PrepAppraisal;
use App\Models\Appraisal\AppraisalComponentType;


class PrepAppraisalComponent extends Model
{
    use ValidatingTrait;

    protected $table = 'prep_appraisal_components';
    public $timestamps=false;
    
    protected $rules = [
        'prep_appraisal_id'=>'required|exists:prep_appraisals,id',
        'appraisal_component_id'=>'required|exists:appraisal_component_types,id',
        'value'=>'required'
    ];

    public function prepAppraisal()
    {
        return $this->belongsTo('App\Models\Audited\Salary\PrepAppraisal','prep_appraisal_id','id');
    }
    public function appraisalComponentTypes()
    {
        return $this->belongsTo('App\Models\Appraisal\AppraisalComponentType','appraisal_component_id','id');
    }


    public static function saveData($prep_appraisal_id){
        $response=[];
        $prepAppraisalObj=PrepAppraisal::find($prep_appraisal_id);
        $appraisalComponent=$prepAppraisalObj->appraisal->appraisalComponent;

        foreach($appraisalComponent as $component){
            $prepSalaryComponent= new self();
            $prepSalaryComponent->prep_appraisal_id=$prep_appraisal_id;
            $prepSalaryComponent->appraisal_component_id=$component->component_id;
            $prepSalaryComponent->value=$component->value;

            if($prepSalaryComponent->isValid()){
                if(!$prepSalaryComponent->save()){
                    $response['status']=false;
                     $response['error']=$prepSalaryComponent->getErrors();
                }
                $response['status']=false;
                $response['message']='Appraisal generated Successfully';
            }
            else{
                $response['status']=false;
                $response['error']=$prepSalaryComponent->getErrors();
            }

        }
        return $response;
    }

    public static function getPrepSalaryComponentValues($prepAppraisalId){

        $response=[];

        $prepSalaryComponentTypes=AppraisalComponentType::all();

        $prepAppraisal=PrepAppraisal::find($prepAppraisalId);

        if($prepAppraisalId==null || ! $prepAppraisal){
            foreach($prepSalaryComponentTypes as $type){
                $response[$type->name]=0;

            }
        }
        else{
            //$prepAppraisalComponent=$prepAppraisal->prepAppraisalComponent();


            if(count($prepSalaryComponentTypes)>0){
                foreach($prepSalaryComponentTypes as $type){
                    $prepAppraisalComponent=self::where('prep_appraisal_id',$prepAppraisalId)->where('appraisal_component_id',$type->id)->first();
                    
                    if($prepAppraisalComponent){
                        $response[$type->name]=$prepAppraisalComponent->value;
                    }
                    else{
                        $response[$type->name]=0;
    
                    }
                }
            }
        }
    return $response;
    }


    
}
