<?php

namespace App\Models\Audited\FoodDeduction;

use App\Models\MonthSetting;
use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class FoodDeduction extends Model
{
    use ValidatingTrait;

    protected $table = 'food_deductions';
    public $timestamps = false;

    protected $fillable = ['user_id', 'month_id', 'amount', 'actual_amount'];

    protected $rules = [
        'user_id' => 'required | exists:users,id',
        'month_id' => 'required | exists:months,id',
        'amount' => 'required | numeric | max:0',
        'actual_amount' => 'required | numeric | max:0',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
    public function month()
    {
        return $this->belongsTo('App\Models\Month', 'month_id', 'id');
    }
    public static function getCurrMonthDeduction($monthId, $userId)
    {
        $food = 0;
        $checkLock = MonthSetting::where('key', 'monthly-food-deduction')->first();
        if ($checkLock && $checkLock->value == 'locked') {
            $food = FoodDeduction::where('month_id', $monthId)->where('user_id', $userId)->sum('amount');
            return $food;
        }
        return null;
    }
}
