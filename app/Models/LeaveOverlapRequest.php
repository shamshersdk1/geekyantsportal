<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class LeaveOverlapRequest extends Model
{
    use ValidatingTrait;
	protected $table = 'leave_overlap_requests';
	public $timestamps = true;

	private $rules = array(
		'leave_id' => 'required'
		);
    public function project()
    {
        return $this->belongsTo('App\Models\Admin\Project', 'project_id', 'id');
    }
    public function leave()
    {
        return $this->belongsTo('App\Models\Admin\Leave', 'leave_id', 'id');
    }
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
