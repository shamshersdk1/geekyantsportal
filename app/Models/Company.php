<?php
namespace App\Models;
use App\Models\BaseModel;
use Auth;
use OwenIt\Auditing\Contracts\Auditable;

class Company extends BaseModel implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    private $rules = array(
            'name' => 'required',
            'gst'  => 'required'
        );
}
