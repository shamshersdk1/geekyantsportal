<?php

namespace App\Models;
use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Log;

use App\Models\Admin\Asset;
use App\Models\Admin\AssetAssignmentDetail;
use App\Models\Admin\MyNetworkDevice;

class GatewayArp extends Model
{

	use ValidatingTrait;

    private $rules = ['gateway_id' => 'required'];

    protected $table = 'gateway_arp';
    public $timestamps = false;

    public static function parseAndStore($arp, $gateway_id, $date, $hr, $min){
        $active_ips = [];
        //Log::info('GatewayArp - parseAndStore - start');
        $arpArray = explode("\n", $arp);
        foreach($arpArray as $line){
           /* This information is taken from arp file format */
           $head_ip_address = "IP address       ";
           $head_hw_type = "HW type     ";
           $head_flags = "Flags       ";
           $head_hw_address = "HW address            ";
           $head_mask = "Mask     ";
           $head_device = "Device";
           /* Do not change the above until /proc/net/arp has changed" */

           /* Parsing the Line */
           $item = [];

           $next_position = 0;
           $item['ip_address'] = trim(substr($line, $next_position, strlen($head_ip_address)));
           $next_position += strlen($head_ip_address);

           $item['hw_type'] = trim(substr($line, $next_position, strlen($head_hw_type)));
           $next_position += strlen($head_hw_type);

           $item['flags'] = trim(substr($line, $next_position, strlen($head_flags)));
           $next_position += strlen($head_flags);

           $item['hw_address'] = strtoupper(trim(substr($line, $next_position, strlen($head_hw_address))));
           $next_position += strlen($head_hw_address);

           $item['mask'] = trim(substr($line, $next_position, strlen($head_mask)));
           $next_position += strlen($head_mask);

           $item['device'] = trim(substr($line, $next_position));
           /* End Parsing */

           if($item['flags'] == '0x2'){
              if(substr($item['ip_address'],0,3) == '10.'){
                $active_ips[] = $item['ip_address'];
                // Looks mapping for Local Network.
                //Log::info($item['ip_address']."->".$item['hw_address']);
                if(substr($item['ip_address'],0,5) == '10.1.'){
                  $item['ip_address'] = '10.0.'.substr($item['ip_address'],5);
                }

                $asset_id = 0;
                $user_id = 0;
                $asset_type = '';
                $asset = Asset::findByHwAddress($item['hw_address']);
                if($asset){
                    $asset_id = $asset->id;
                    $asset_type = 'App\Models\Asset';
                    $asset_last_assign = AssetAssignmentDetail::where('asset_id', $asset_id)->orderBy('id', 'DESC')->first();
                    if($asset_last_assign){
                        if($asset_last_assign->to_date == NULL)
                            $user_id = $asset_last_assign->user_id;
                    }
                }else{
                    $asset = MyNetworkDevice::where('mac_address',$item['hw_address'])->first();
                    if($asset){
                        $asset_id = $asset->id;
                        $user_id  = $asset->user_id;
                        $asset_type = 'App\Models\MyNetworkDevice';
                    }
                }

                $itemObject = new GatewayArp();
                $itemObject->gateway_id = $gateway_id;
                $itemObject->ip_address = $item['ip_address'];
                $itemObject->hw_type = $item['hw_type'];
                $itemObject->flags = $item['flags'];
                $itemObject->hw_address = $item['hw_address'];
                $itemObject->mask = $item['mask'];
                $itemObject->device = $item['device'];
                $itemObject->user_id = $user_id;
                $itemObject->asset_id = $asset_id;
                $itemObject->asset_type = $asset_type;
                $itemObject->date = $date;
                $itemObject->hr = $hr;
                $itemObject->min = $min;
                if(!$itemObject->save()){
                    Log::info('Error Saving ARP Info - '.json_encode($item));
                }
              }
            }

           //Log::info(print_r($item, true));
        }

        return $active_ips;
        //Log::info('GatewayArp - parseAndStore - end');
    }
}
