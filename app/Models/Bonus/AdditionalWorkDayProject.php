<?php

namespace App\Models\Bonus;

use Illuminate\Database\Eloquent\Model;

//use Illuminate\Database\Eloquent\SoftDeletes;

class AdditionalWorkDayProject extends Model
{
    protected $table = 'additional_work_day_projects';
    public $timestamps = false;

    public static function saveData($data)
    {
        $res['status'] = false;
        $res['message'] = null;
        $workshopObj = new Workshop();
        $workshopObj->user_id = $data['user_id'];
        $workshopObj->title = $data['title'];
        $workshopObj->description = $data['description'];
        if (!$workshopObj->save()) {
            $res['status'] = true;
            $res['message'] = $workshopObj->getErrors();
            return $res;
        }
        return $workshopObj;
    }
}
