<?php
namespace App\Models\User;

use App\Models\BaseModel;
use App\Models\UserTimesheet;
use App\Models\Week;
use Auth;
use DB;
use Exception;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;

class UserTimesheetWeek extends BaseModel
{
 use ValidatingTrait;
 use SoftDeletes;
 protected $rules = [];
 ///protected $table = 'user_timesheet_extras';
 protected $dates = ['deleted_at'];
 public function month()
 {
  return $this->belongsTo('App\Models\Month', 'month_id', 'id');
 }
 public function approver()
 {
  return $this->belongsTo('App\Models\User', 'approved_by', 'id');
 }
 public function user()
 {
  return $this->belongsTo('App\Models\User', 'user_id', 'id');
 }
 public function parent()
 {
  return $this->belongsTo('App\Models\User', 'parent_id', 'id');
 }
 public function week()
 {
  return $this->belongsTo('App\Models\Week', 'week_id', 'id');
 }
 // public function status()
 // {
 //     return $this->belongsTo('App\Models\User', 'status', 'id');
 // }

 public static function approveAllTimesheet($weekId, $userId)
 {

  $authUser = Auth::User();
  $weekObj  = Week::find($weekId);

  // echo "$userId";
  // die;
  if ($weekObj) {
   $reviewed_at = date("Y-m-d h:i:s");

   $response['message'] = '';
   $response['error']   = '';
   $response['status']  = true;

   DB::beginTransaction();
   try {
    $userTimesheet = UserTimesheet::where('user_id', $userId)->whereBetween('date', [$weekObj->start_date, $weekObj->end_date])->get();
    if ($userTimesheet) {
     foreach ($userTimesheet as $userTimesheet) {
      $userTimesheet->status      = "approved";
      $userTimesheet->reviewed_by = $authUser->id;
      $userTimesheet->reviewed_at = $reviewed_at;
      if (!$userTimesheet->save()) {
       throw new Exception("User Timesheet Table not updated");
      }
     }
    }
    $userTimesheetWeek = UserTimesheetWeek::where('user_id', $userId)->where('week_id', $weekId)->first();

    if ($userTimesheetWeek) {
     $userTimesheetWeek->status      = "approved";
     $userTimesheetWeek->approved_by = $authUser->id;
     $userTimesheetWeek->approved_at = $reviewed_at;

     if (!$userTimesheetWeek->save()) {
      throw new Exception("User Timesheet Week Table not updated");
     }
    }

    $userTimesheetExtra = UserTimesheetExtra::where('user_id', $userId)->where('week_id', $weekId)->first();
    // \Log::info($userTimesheetExtra);
    if ($userTimesheetExtra) {
     $userTimesheetExtra->status      = 'approved';
     $userTimesheetExtra->approved_by = $authUser->id;

     if (!$userTimesheetExtra->save()) {
      throw new Exception("User Timesheet Extra Table not updated");
     }
    }

    DB::commit();
    $response['status']  = true;
    $response['message'] = 'Successfully update';
   } catch (exception $e) {
    DB::rollback();
    \Log::info($e->getMessage());
    \Log::info($e->getTraceAsString());
    $response['status']  = false;
    $response['message'] = $e;
   }
   return $response;

  }
 }
 public static function getRMUsers($weekId, $rmUserId)
 {
  $weekObj   = Week::find($weekId);
  $rmUserObj = User::find($rmUserId);

  if ($weekObj && $rmUserObj) {
   //UserTimesheetWeek::where
   $startDate = $weekObj->start_date;
   $endDate   = $weekObj->end_date;
  }
 }
 //ALTER TABLE `user_timesheets` ADD `approved_duration` INT NULL AFTER `duration`, ADD `comments` VARCHAR(255) NULL AFTER `approved_duration`, ADD `approved_by` INT NULL AFTER `comments`;

 public static function updateData($weeklyTimesheet)
 {
  foreach ($weeklyTimesheet as $timesheet) {
   $timesheet->status = 'approved';
   $timesheet->save();

  }
 }
}
