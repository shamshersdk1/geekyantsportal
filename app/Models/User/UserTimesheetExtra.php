<?php
namespace App\Models\User;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;
use Auth;
use  App\Models\Week;
use App\Models\Month;
use App\Models\Admin\Bonus;

class UserTimesheetExtra extends BaseModel
{
 use ValidatingTrait;
 use SoftDeletes;
 protected $rules = [];
 protected $table = 'user_timesheet_extras';
 protected $dates = ['deleted_at'];
 public function month()
 {
  return $this->belongsTo('App\Models\Month', 'month_id', 'id');
 }

 public function approver()
 {
  return $this->belongsTo('App\Models\User', 'approved_by', 'id');
 }
 public function user()
 {
  return $this->belongsTo('App\Models\User', 'user_id', 'id');
 }
 //ALTER TABLE `user_timesheets` ADD `approved_duration` INT NULL AFTER `duration`, ADD `comments` VARCHAR(255) NULL AFTER `approved_duration`, ADD `approved_by` INT NULL AFTER `comments`;

 public static function saveExtra($data)
 {
    $authUser=Auth::user();
        $data['week_id'] = 9999;
     $weekObj=Week::where('start_date','<=',$data['date'])->where('end_date','>=',$data['date'])->first();
        if($weekObj){
            $data['week_id']=$weekObj->id;
        }
            $data['month_id']=Month::getMonth($data['date']);

        $response['message'] = '';
        $response['data']   = '';
        $response['status']  = false;
       
        $userTimesheet=new self();
        $userTimesheet->user_id= $data['user_id'];
        $userTimesheet->week_id= $data['week_id'];
        $userTimesheet->month_id= $data['month_id'];
        $userTimesheet->project_id= $data['project_id'];
        $userTimesheet->date=$data['date'];
        $userTimesheet->extra_hours=$data['extra_hours'];
        $userTimesheet->approved_by= $authUser->id;
        $userTimesheet->created_by= $authUser->id;
        $userTimesheet->status=$data['status'];
        $userTimesheet->amount=$data['amount'];

       
        if(!$userTimesheet->save()){
            $reponse['message']='Unable to save record in User Timesheet Extra';
        }
        $data['reference_type']='App\Models\User\UserTimesheetExtra';
        $data['reference_id']=$userTimesheet->id;
    
        $extraObj = UserTimesheetExtra::with('approver')->whereDate('date', $data['date'])->where('user_id', $data['user_id'])->first();
    
        if($userTimesheet->status && $userTimesheet->status=='approved'){

        $saveBonus=Bonus::saveApprovedBonus($data);
        }

        $response['status']=true;
        $response['message']='Saved Successfully';
        $response['data']=$extraObj;
    
        return $response;

    
 }
 public static function saveToBonus($extra){

    $data['month_id']=!empty($extra->month_id) ? $extra->month_id : '';
    $data['user_id']=!empty($extra->user_id) ? $extra->user_id : '';
    $data['date']=!empty($extra->date) ? $extra->date : '';
    $data['type']='extra';
    $data['status']=!empty($extra->status) ? $extra->status : '';
    $data['amount']=!empty($extra->amount) ? $extra->amount : '';
    $data['approved_by']=!empty($extra->approved_by) ? $extra->approved_by : '';
    $data['created_by']=!empty($extra->created_by) ? $extra->created_by : '';
    $data['created_at']=!empty($extra->created_at) ? $extra->created_at : '';
    $data['updated_at']=!empty($extra->updated_at) ? $extra->updated_at : '';
    $data['reference_type']='App\Models\User\UserTimesheetExtra';
    $data['reference_id']=!empty($extra->id) ? $extra->id : '';
    $data['created_at']=!empty($extra->created_at) ? $extra->created_at : '';
    $data['updated_at']=!empty($extra->updated_at) ? $extra->updated_at : '';
    $data['approved_at']=!empty($extra->updated_at) ? $extra->updated_at : '';


    Bonus::saveApprovedBonus($data);
 }

}
