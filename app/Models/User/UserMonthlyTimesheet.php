<?php
namespace App\Models\User;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;

class UserMonthlyTimesheet extends BaseModel
{
    use ValidatingTrait;
    use SoftDeletes;
    protected $rules = [];
    //protected $table = 'user_monthly_timesheet';
    protected $dates = ['deleted_at'];
    //ALTER TABLE `user_timesheets` ADD `approved_duration` INT NULL AFTER `duration`, ADD `comments` VARCHAR(255) NULL AFTER `approved_duration`, ADD `approved_by` INT NULL AFTER `comments`;
    public function approver()
    {
        return $this->belongsTo('App\Models\User','approved_id','id');
    }
}
