<?php

namespace App\Models\Appraisal;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Watson\Validating\ValidatingTrait;

class AppraisalComponentType extends Model implements Auditable
{
    use ValidatingTrait;
    use \OwenIt\Auditing\Auditable;

    protected $table = 'appraisal_component_types';
    public $timestamps = false;

    protected $fillable = ['code', 'name', 'is_computed', 'is_company_expense', 'is_conditional_prorata', 'prorata_function'];

    protected static function boot()
    {
        parent::boot();
        static::deleting(function ($appraisalComponentTypeObj) {
            if (count($appraisalComponentTypeObj->components) > 0) {
                return false;
            }
        });
    }

    protected $rules = [
        'code' => 'required|unique:appraisal_component_types',
        'name' => 'required | string',
        'is_computed' => 'required | boolean',
        'is_company_expense' => 'required | boolean',
    ];

    public function components()
    {
        return $this->hasMany('App\Models\Appraisal\AppraisalComponent', 'component_id', 'id');
    }

    public static function getIdByKey($key)
    {
        $obj = self::where('code', $key)->first();
        return $obj->id;
    }
}
