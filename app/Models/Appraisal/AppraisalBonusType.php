<?php

namespace App\Models\Appraisal;

use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class AppraisalBonusType extends Model implements Auditable
{
    use ValidatingTrait;
    use \OwenIt\Auditing\Auditable;


    protected $table='appraisal_bonus_type';
    public $timestamps = false;

    protected static function boot()
    {
    parent::boot();
    static::deleting(function($appraisalBonusObj) {
        if(count($appraisalBonusObj->appraisalBonus) > 0 )
        {
            return false;
        }
        });
    }

    protected $rules = [
		'code'    => 'required|unique:appraisal_bonus_type',
        'description' => 'required | string',
        'is_loanable' => 'required'
    ];

    public function appraisalBonus(){
        return $this->hasMany('App\Models\Appraisal\AppraisalBonus','appraisal_bonus_type_id');
    }
}
