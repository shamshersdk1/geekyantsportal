<?php

namespace App\Models\Appraisal;

use App\Traits\ValidationTrait;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Validator;
use Illuminate\Support\Arr;
use App\Models\Appraisal\Appraisal;

class AppraisalComponent extends Model implements Auditable
{
    use ValidationTrait;
    use \OwenIt\Auditing\Auditable;
    protected $auditExclude = [ 'id'];
    protected $table='appraisal_components';
    public $timestamps=false;
    protected $rules=array(
        'appraisal_id'=>'required|exists:appraisals,id',
        'component_id'=>'required|exists:appraisal_component_types,id',
        'value' =>'required | numeric',
        );

    public function appraisalComponentType(){
        return $this->belongsTo('App\Models\Appraisal\AppraisalComponentType','component_id','id');
    }
    public function appraisalType(){
        return $this->belongsTo('App\Models\Appraisal\AppraisalType','appraisal_id','id');
    }
    public function appraisal(){
        return $this->belongsTo('App\Models\Appraisal\Appraisal','appraisal_id','id');
    }

    public function transformAudit(array $data): array
    {
        //$data['new_values']['user_name'] = AppraisalComponent::find($data['auditable_id'])->appraisal->user->name;
        if (Arr::has($data, 'new_values.value')) {
            $code = AppraisalComponent::find($data['auditable_id'])->appraisalComponentType->code;
            $data['new_values'][$code] = $data['new_values']['value'];
            $data['old_values'][$code] = $data['old_values']['value'];
        }
        unset($data['old_values']['component_id']);
        unset($data['new_values']['component_id']);
        unset($data['old_values']['appraisal_id']);
        unset($data['new_values']['appraisal_id']);
        unset($data['old_values']['value']);
        unset($data['new_values']['value']);
        return $data;
    }

   
}
