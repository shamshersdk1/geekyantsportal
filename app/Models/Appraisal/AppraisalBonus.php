<?php

namespace App\Models\Appraisal;

use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use App\Models\Appraisal\AppraisalBonusType;
use Illuminate\Support\Arr;

class AppraisalBonus extends Model implements Auditable
{
    use ValidatingTrait;
    use \OwenIt\Auditing\Auditable;
    protected $auditExclude = [ 'id','appraisal_id'];
    protected $table='appraisal_bonuses';
    public $timestamps = false;
    protected $fillable = ['appraisal_id','appraisal_bonus_type_id','value','value_date'];
    public function transformAudit(array $data): array
    {
        //$data['new_values']['user_name'] = AppraisalComponent::find($data['auditable_id'])->appraisal->user->name;
        if (Arr::has($data, 'new_values.appraisal_bonus_type_id')) {
            $code = AppraisalBonus::find($data['auditable_id'])->appraisalBonusType->code;
            $newValue = $data['new_values']['value'];
            $oldValue = $data['old_values']['value'];
            if(!empty($data['new_values']['value_date']))
                $newValue .= ' on '. date("jS M Y", strtotime($data['new_values']['value_date']));
            if(!empty($data['old_values']['value_date']))
                $oldValue .= ' on '. $data['old_values']['value_date'];
                
            $data['new_values'][$code] = $newValue;
            $data['old_values'][$code] = $oldValue;
        }
        unset($data['old_values']['appraisal_bonus_type_id']);
        unset($data['new_values']['appraisal_bonus_type_id']);
        unset($data['old_values']['value']);
        unset($data['new_values']['value']);
        unset($data['old_values']['value_date']);
        unset($data['new_values']['value_date']);
        return $data;
    }
    
    protected $rules=[
        'appraisal_id'=>'required|exists:appraisals,id',
        'appraisal_bonus_type_id'=>'required|exists:appraisal_bonus_type,id',
        'value' =>'required'
    ];

    public function appraisalBonusType(){
        return $this->belongsTo('App\Models\Appraisal\AppraisalBonusType','appraisal_bonus_type_id','id');
    }
    public function appraisalType(){
        return $this->belongsTo('App\Models\Appraisal\AppraisalType','appraisal_id','id');
    }
    public function appraisal(){
        return $this->hasOne('App\Models\Appraisal\Appraisal','id','appraisal_id');
    }

    // public function transformAudit(array $data): array
    // {
    //     $data['new_values']['user_name'] = AppraisalBonus::find($data['auditable_id'])->appraisal->user->name;
    //     $data['new_values']['bonus_type'] = AppraisalBonus::find($data['auditable_id'])->appraisalBonusType->code;
    //     $data['old_values']['bonus_type'] = AppraisalBonus::find($data['auditable_id'])->appraisalBonusType->code;
    //     // if($data['old_values']['value_date'] === null)
    //     // {
    //     //     $data['old_values']['value_date'] = 'Nothing';
    //     // }
    //     // if($data['new_values']['value_date'] === null)
    //     // {
    //     //     $data['new_values']['value_date'] = 'Nothing';
    //     // }
    //     unset($data['old_values']['appraisal_bonus_type_id']);
    //     unset($data['new_values']['appraisal_bonus_type_id']);
    //     unset($data['old_values']['appraisal_id']);
    //     unset($data['new_values']['appraisal_id']);
    //     return $data;
    // }

}
