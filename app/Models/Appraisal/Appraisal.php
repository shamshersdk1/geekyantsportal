<?php

namespace App\Models\Appraisal;

use App\Traits\ValidationTrait;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use App\Models\Month;
use App\Services\CalendarService;
use App\Models\User;
use App\Models\Appraisal\AppraisalType;
use Illuminate\Support\Arr;

class Appraisal extends Model implements Auditable
{
    use ValidationTrait;
    use \OwenIt\Auditing\Auditable;
    protected $auditExclude = [ 'id','user_id'];
    protected $table = 'appraisals';

    protected $rules = array(
        'user_id' => 'required | exists:users,id',
        'effective_date' => 'required | date',
        'type_id' => 'required | exists:appraisal_types,id',
    );

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function type()
    {
        return $this->belongsTo('App\Models\Appraisal\AppraisalType', 'type_id', 'id');
    }
    public function appraisalComponent()
    {
        return $this->hasMany('App\Models\Appraisal\AppraisalComponent', 'appraisal_id');

    }
    public function appraisalBonus()
    {
        return $this->hasMany('App\Models\Appraisal\AppraisalBonus', 'appraisal_id');

    }

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($appraisal) {
            $appraisal->appraisalComponent()->delete();
            $appraisal->appraisalBonus()->delete();
        });
    }

    public function transformAudit(array $data): array
    {
        if (Arr::has($data, 'new_values.type_id')) {
            AppraisalType::find($this->getOriginal('type_id'))->code;
            $data['old_values']['appraisal_type'] = AppraisalType::find($this->getOriginal('type_id'))->name;
            $data['new_values']['appraisal_type'] = AppraisalType::find($this->getAttribute('type_id'))->name;
        }
        if (Arr::has($data, 'new_values.effective_date')) {
            $oldValue = $data['old_values']['effective_date'];
            $newValue = $data['new_values']['effective_date'];
            $data['new_values']['effective_date'] = date("D, jS M Y", strtotime($newValue));
            $data['old_values']['effective_date'] = date("D, jS M Y", strtotime($oldValue));
        }
        unset($data['old_values']['type_id']);
        unset($data['new_values']['type_id']);
        return $data;
    }

    private function getComponentValueByKey($key)
    {
        $keyValue = 0;
        foreach ($this->appraisalComponent as $type) {
            if ($type->appraisalComponentType->code == $key) {
                $keyValue = $type->value;
                break;
            }
        }
        return $keyValue;
    }

    private function getBonusValueByKey($key)
    {
        $keyValue = 0;
        foreach ($this->appraisalBonus as $type) {
            if ($type->appraisalBonusType->code == $key) {
                $keyValue = $type->value;
                break;
            }
        }
        return $keyValue;
    }

    public function getMonthlyGrossSalary()
    {
        $value = 0;
        foreach ($this->appraisalComponent as $type) {
            if ($type->appraisalComponentType->code == 'basic' || $type->appraisalComponentType->code == "hra" || $type->appraisalComponentType->code == "car-allowance" || $type->appraisalComponentType->code == "food-allowance" ||
                $type->appraisalComponentType->code == "lta" || $type->appraisalComponentType->code == "special-allowance" || $type->appraisalComponentType->code == "stipend") {
                    $value += ($type->value);
            }
        }
        return round($value, 2);
    }

    public function getAnnualGrossSalary()
    {
        $monthlyGrossSalary = $this->getMonthlyGrossSalary();
        $annualGrossSalary = ($monthlyGrossSalary * 12);
        return round($annualGrossSalary, 2);
    }

    public function getAnnualCtc()
    {
        $annualGrossSalary = $this->getAnnualGrossSalary();
        $monthlyVariableBonus = $this->getBonusValueByKey("monthly-variable-bonus");
        $bonus_value = 0;
        foreach ($this->appraisalBonus as $bonus) {
            if ($bonus->value_date == null) {
                $bonus_value += ($bonus->value * 12);
            } else {
                $bonus_value += $bonus->value;
            }
        }
        $annualCtc = $annualGrossSalary + $bonus_value;
        return round($annualCtc, 2);
    }

    public function getAnnualGrossEarnings()
    {
        $annualCtc = $this->getAnnualCtc();
        $employerPf = abs($this->getComponentValueByKey("pf-employeer"));
        $employerEsi = abs($this->getComponentValueByKey("esi-employer"));
        $pfAdminCharges = abs($this->getComponentValueByKey("pf-other"));
        return round($annualCtc - ($employerPf + $pfAdminCharges + $employerEsi), 2);
    }

    public function getMonthlyCtc()
    {
        $annualCtc = $this->getAnnualCtc();
        return round($annualCtc / 12, 2);
    }

    public function getMonthlyGrossEarnings()
    {
        $monthlyCtc = $this->getMonthlyCtc();
        $employerPf = abs($this->getComponentValueByKey("pf-employeer"));
        $employerEsi = abs($this->getComponentValueByKey("esi-employer"));
        $pfAdminCharges = abs($this->getComponentValueByKey("pf-other"));
        return round($monthlyCtc - ($employerPf + $pfAdminCharges + $employerEsi), 2);
    }

    public function getSpecialAllowanceInternal()
    {
        $monthlyCtc = $this->getMonthlyCtc();
        $basic = $this->getComponentValueByKey("basic");
        $hra = $this->getComponentValueByKey("hra");
        $carAllowance = $this->getComponentValueByKey("car-allowance");
        $foodAllowance = $this->getComponentValueByKey("food-allowance");
        $lta = $this->getComponentValueByKey("lta");
        return round($monthlyCtc - ($basic - $hra - $carAllowance - $foodAllowance - $lta), 2);
    }

    public function getSpecialAllowance()
    {
        $monthlyGrossEarnings = $this->getMonthlyGrossEarnings();
        $basic = $this->getComponentValueByKey("basic");
        $hra = $this->getComponentValueByKey("hra");
        $carAllowance = $this->getComponentValueByKey("car-allowance");
        $foodAllowance = $this->getComponentValueByKey("food-allowance");
        $lta = $this->getComponentValueByKey("lta");
        return round($monthlyGrossEarnings - ($basic - $hra - $carAllowance - $foodAllowance - $lta), 2);
    }

    public function getAppraisalComponentsSum(){
        $total = 0;
        foreach ($this->appraisalComponent as $type) {
            if ($type->appraisalComponentType->is_computed == 0) {
                $total += abs($type->value);
            }
        }
        return $total;
    }

    //returns single user all appraisals for current month. If no appraisal is found for current month of user then returns latest effective appraisal before current month.
    public static function getAllAppraisal($monthId)
    {
        $month = Month::find($monthId);
        if($month)
        {
            $object_a = Appraisal::orderBy('user_id')->orderBy('effective_date','desc')->whereDate('effective_date', '>=',$month->getFirstDay())->whereDate('effective_date', '<=',$month->getLastDay())->get();
            $appraisals_data = Appraisal::orderBy('user_id')->orderBy('effective_date','desc')->whereDate('effective_date', '>=',$month->getFirstDay())->whereDate('effective_date', '<=',$month->getLastDay())->get()->pluck('user_id');
            $object_b = Appraisal::orderBy('user_id')->orderBy('effective_date','desc')->whereDate('effective_date', '<=',$month->getFirstDay())->whereNotIn('user_id',$appraisals_data)->get()->unique('user_id');
            $appraisals = $object_b->merge($object_a);
            if(count($appraisals) > 0)
                return $appraisals;
        }
        return null;
    }

    public static function getAppraisalByDate($userId, $date)
    {
        $appraisal = self::where('user_id', $userId)->where('effective_date', '<=', $date)->orderBy('effective_date','DESC')->first();
        if (!$appraisal) {
            return false;
        }
        return $appraisal;
    }

    public static function getOneDaySalary($userId, $date)
    {
        $userObj = User::find($userId);
        $month = date('m', strtotime($date));
        $year = date('Y', strtotime($date));
        if (!$userObj) {
            return 0;
        }
        $appraisal = self::getAppraisalByDate($userId, $date);
        if ($appraisal) {
            $noOfWorkingDay = CalendarService::getWorkingDays($month, $year);
            $annualGrossSalary = $appraisal->getAnnualGrossSalary();
            $oneDaySalary = round(($annualGrossSalary / 12) / $noOfWorkingDay);
            return $oneDaySalary;
        }
        return false;
    }

    public static function getBonusAmount($userId, $date)
    {
        $amount = 2000;
        $oneDaySalary = self::getOneDaySalary($userId,$date);
        if($oneDaySalary){
            if($oneDaySalary > $amount){
                $amount = $oneDaySalary;
            }
            return $amount;
        }
        return false;
    }

}
