<?php

namespace App\Models;
use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

use App\Models\Admin\UserIpAddress;
use App\Models\Admin\IpAddressMapper;
use App\Models\Admin\AssetMeta;
use App\Models\Admin\Asset;
use App\Models\UserAttendance;
use App\Models\Admin\MyNetworkDevice;

class GatewayIpConnection extends Model
{

    use ValidatingTrait;

    private $rules = ['gateway_id' => 'required'];

    protected $table = 'gateway_ip_connections';
    public $timestamps = false;

    public function getAsset(){
        if($this->asset_type == 'App\Models\Admin\Asset')
            return Asset::where('id','=',$this->asset_id)->first();
        else if($this->asset_type == 'App\Models\Admin\MyNetworkDevice')
            return MyNetworkDevice::where('id','=', $this->asset_id)->first();
    }

    public static function parseAndStore($connections, $gateway_id, $date, $hr, $min, $active_ips){
        //Log::info('GatewayIpConnection - parseAndStore - start');
        //Log::info($connections);
        $connectionCount = array();
        $connectionArray = explode("\n", $connections);
        foreach($connectionArray as $line){
            /* This information is taken from conn_track file format */

            $head_type = "ipv4     ";
            $head_proto = "2 tcp      ";
            $head_details = "6 431999 ESTABLISHED src=10.0.233.237 dst=13.233.242.214 sport=52401 dport=443 src=13.233.242.214 dst=106.51.67.220 sport=443 dport=52401 [ASSURED] mark=32769 zone=0 use=2";
            /* end of conn_track format */

            /* Parsing the Line */
            $item = [];

            $next_position = 0;
            $item['type'] = trim(substr($line, $next_position, strlen($head_type)));
            $next_position += strlen($head_type);

            $item['proto'] = trim(substr($line, $next_position+2, strlen($head_proto)-2));
            $next_position += strlen($head_proto);

            $item['details'] = trim(substr($line, $next_position));

            $item['detailsArray'] = explode(' ', $item['details']); 

            if($item['proto'] == 'tcp'){
                list($item_src, $item['src']) = explode('=',$item['detailsArray'][3]);
                list($item_dst, $item['dst']) = explode('=',$item['detailsArray'][4]);
            }else if($item['proto'] == 'udp'){
                list($item_src, $item['src']) = explode('=',$item['detailsArray'][2]);
                list($item_dst, $item['dst']) = explode('=',$item['detailsArray'][3]);
            }
            /* End Parsing */

            if(substr($item['src'],0,3) == '10.'){
                if(!in_array($item['src'], $active_ips)){
                    continue;
                }

                if(substr($item['src'],0,5) == '10.1.'){
                  $item['src'] = '10.0.'.substr($item['src'],5);
                }

                if($connectionCount[$item['src']]){
                    $connectionCount[$item['src']] += 1;
                }else{
                    $connectionCount[$item['src']] = 1;
                }
            }
            //Log::info(print_r($item, true));
        }

        foreach($connectionCount as $ip_address => $count){
            $user_id = 0;
            $asset_id = 0;
            $asset_type = '';
            $is_laptop = false;

            $user_ip_address = UserIpAddress::findByIpAddress($ip_address);
            if($user_ip_address){
                $user_id = $user_ip_address->user_id;
                $ip_address_mapper = IpAddressMapper::where('user_ip_address_id', $user_ip_address->id)->first();
                //is any device mapped to it? yes, find the asset details.
                if($ip_address_mapper){
                    $asset_id = $ip_address_mapper->reference_id;
                    $asset_type = $ip_address_mapper->reference_type;

                    if($ip_address_mapper->reference_type == 'App\Models\Admin\AssetMeta'){
                        $assetMeta = AssetMeta::where('id',$ip_address_mapper->reference_id)->first();
                        if($assetMeta){
                            $asset_id = $assetMeta->asset_id;
                            $asset_type = 'App\Models\Admin\Asset';
                            $asset = Asset::where('id',$asset_id)->first();
                            //Log::info($asset_id."->".$asset->asset_category_id);
                            if($asset){
                                if($asset->asset_category_id == 1 || $asset->asset_category_id == 26) // Hard coded for Laptop & PC - tracking
                                    $is_laptop = true;
                            }
                        }   
                    }
                }
            }

            $objectIpConnection = new GatewayIpConnection();
            $objectIpConnection->gateway_id = $gateway_id;
            $objectIpConnection->ip_address = $ip_address;
            $objectIpConnection->user_id = $user_id;
            $objectIpConnection->asset_id = $asset_id;
            $objectIpConnection->asset_type = $asset_type;
            $objectIpConnection->date = $date;
            $objectIpConnection->hr = $hr;
            $objectIpConnection->min = $min;
            $objectIpConnection->count = $count;
            if(!$objectIpConnection->save()){
                Log::info('Error: Unable to save the connection details - '.$ip_address);
            }

            if($is_laptop && $user_id>0){
                UserAttendance::capture($user_id, $count, $date, $hr, $min);
            }
        }

        //Log::info('GatewayIpConnection - parseAndStore - end');
    }
}
