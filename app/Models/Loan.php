<?php

namespace App\Models;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

use App\Models\AccessLog;
use App\Models\Admin\Appraisal;
use App\Models\Admin\PayslipCsvData;
use App\Models\Admin\PayslipCsvMonth;
use App\Models\Admin\LoanRequest;
use App\Models\LoanTransaction;
use App\Services\FileService;
use App\Jobs\Loan\LoanRequestJob;
use StdClass;
use Validator;
use Exception;
use Carbon\Carbon;

class Loan extends Model
{
    protected $appends = ['outstanding_amount'];
    public function getOutstandingAmountAttribute()
    {
        $value = $this->loan_transactions()->where('type','credit')->sum('amount');
        return $this->amount - $value;
    }
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
    public function emis()
    {
        return $this->hasMany(LoanEmi::class, 'loan_id', 'id');
    }
    public function approver()
    {
        return $this->hasOne(User::class, 'id', 'approved_by');
    }
    public function loan_transactions()
    {
        return $this->hasMany(LoanTransaction::class, 'loan_id', 'id');
    }
    public function loan_request()
    {
        return $this->hasOne(LoanRequest::class, 'id', 'loan_request_id');
    }

    public function appraisalBonus()
    {
        return $this->belongsTo('App\Models\Appraisal\AppraisalBonus');
    }

    public function file()
    {
        return $this->morphOne('App\Models\Admin\File', 'reference');
    }
    public static function validateLoan(Request $request)
    {
        $response = ['status' => false, 'message' => []];
        $validator = Validator::make($request->all(), [
        'name' => 'required',
        'type' => 'required|in:annual,qtr,monthly',
        'amount' => 'required|regex:/^\d*(\.\d{0,2})?$/|not_in:[0,0.0,0.00]',
        'date' => 'required',
        'emi' => 'required|regex:/^\d*(\.\d{1,2})?$/|not_in:0,0.0,0.00'
        ]);
        if ($validator->fails()) {
            $response['message'] = $validator->errors();
            return $response;
        }
        $user=User::where('name', $request->name)->first();
        if (empty($user) || count($user)==0) {
            $response['message'] = ['User not found'];
            return $response;
        }
        if (substr($user->name, -1)=="s") {
            $userName=$user->name.'\'';
        } else {
            $userName=$user->name.'\'s';
        }
        switch($request->type)
        {
            case 'annual': 
                if(empty($user->activeAppraisal)||empty($user->activeAppraisal->year_end)) {
                    $response['message'] = [$userName.' annual bonus is unavailable'];
                    return $response;
                } else {
                    if($request->emi > $user->activeAppraisal->year_end) {
                        $response['message'] = ['Requested amount is greater than '.$userName.' annual bonus'];
                        return $response;
                    }
                }
                break;
            case 'qtr':
                if(empty($user->activeAppraisal)||empty($user->activeAppraisal->qtr_bonus)) {
                    $response['message'] = [$userName.' quarterly bonus is unavailable'];
                    return $response;
                } else {
                    if($request->emi > ($user->activeAppraisal->qtr_bonus/4)) {
                        $response['message'] = ['Requested amount is greater than '.$userName.' quarterly bonus'];
                        return $response;
                    }
                }
                break;
            case 'monthly':
                if ($request->emi > $request->amount) {
                    $response['message'] = ["EMI can't be greater than the amount"];
                    return $response;
                }
                break;
        }
        $response['status'] = true;
        return $response;
    }

    public static function saveLoan(Request $request)
    {
        $response = ['status' => false, 'message' => []];
        try {
            $loan= new Loan;
            $user=User::where('name', $request->name)->first();
            $loan->user_id=$user->id;
            $loan->amount=$request->amount;
            $loan->type = $request->type;
            $loan->application_date = date_to_yyyymmdd($request->date);
            $loan->description = empty($request->description) ? null : $request->description;
            $loan->approved_by=\Auth::id();
            $loan->emi=$request->emi;
            $term=ceil($loan->amount/$loan->emi);
            $last=$loan->amount-($term-1)*$loan->emi;
            $loan->status = 'in_progress';
            $loan->save();
            // Dispatch Loan Request Job
            $job = (new LoanRequestJob($loan))->onQueue('loan-request');
            dispatch($job);
            
            if($request->type == 'monthly') {
                $date=Carbon::create(null, null, '25');
                $increase = 1;
            } elseif($request->type == 'annual') {
                $increase = 12;
                // $date=Carbon::create(null, null, '25');
                if(!empty($user->activeAppraisal) && !empty($user->activeAppraisal->year_end)) {
                    $result = Appraisal::getBonus($user->activeAppraisal->id, $loan->application_date);
                    if($result['annual'] != 'Not confirmed yet') {
                        $date = Carbon::parse($result['annual']);
                    } else {
                            $response['message'] = 'Annual bonus unavailable';
                            $loan->delete();
                            return $response;
                        }
                } 
            } elseif($request->type == 'qtr') {
                $increase = 3;
                // $date=Carbon::create(null, null, '25');
                if(!empty($user->activeAppraisal) && !empty($user->activeAppraisal->qtr_bonus)) {
                    $result = Appraisal::getBonus($user->activeAppraisal->id, $loan->application_date);
                    if(!empty($result['Qtr'])) {
                        $array = $result['Qtr'];
                        $temp = '';
                        foreach($array as $key => $value)
                        {
                            if($value > 0) {
                                $temp = $key;
                            }
                        }
                        if($temp != '') {
                            $date = Carbon::parse(date('Y-m-25', strtotime($first_key)));
                        } else {
                            $response['message'] = 'Quarterly bonus unavailable';
                            $loan->delete();
                            return $response;
                        }
                    }
                }
            }
            $date->subMonths($increase);
            
            $response['status'] = true;
            return $response;
        } catch(Exception $e) {
            $response['message'] = 'UNEXPECTED ERROR, PLEASE CONTACT ADMIN';
    		AccessLog::accessLog(NULL, 'App\Models', 'Loan', 'saveLoan', 'catch-block', $e->getMessage());
            return $response;
        }
    }
    public function remaining()
    {
        return ($this->amount - abs($this->emis()->where('status', 'paid')->sum('amount')));
    }

    public static function getLoans($id)
    {
        $csvMonth = PayslipCsvMonth::find($id);
        $userIdArray = PayslipCsvData::userIdArray($id);
        $date = $csvMonth->year."-".substr("00".$csvMonth->month, -2)."-20";
        $loans = Loan::where('application_date', '<=', $date)->where('remaining', '>', 0)->get();
        $array = [];
        foreach($loans as $loan)
        {
            $user = $loan->user;
            if($user && in_array($user->id, $userIdArray)) {
                try {
                    $data = new StdClass();
                    $csv_loan = PayslipCsvData::getLoanStatus($id, $user->id);
                    if($csv_loan['status'] == "error") {
                        continue;
                    }
                    $data->employee_id = $user->employee_id;
                    $data->name = $user->name;
                    $data->date = date_in_view($loan->application_date);
                    $data->loan_amount = $loan->amount;
                    $data->remaining = $loan->remaining;
                    $data->emi = $loan->emi;
                    $data->amount = $csv_loan['status'] == "unpaid" ? $loan->emi : $csv_loan['value'];
                    $data->status = $csv_loan['status'];
                    $array[] = $data;
                } catch(Exception $e) {
                    continue;
                }
            }
        }
        return $array;
    }

    public static function updateLoanPaymentDetails($data)
    {
        $date = date('Y-m-d'); 
        $loanObj = Loan::where('user_id',$data->user_id)->where('status','!=','completed')->first();

        if ( !empty($loanObj) )
        {
            if ( $loanObj->remaining < $data->loan )
            {
                self::enterLoanTransaction($loanObj,$loanObj->remaining,$date);

                $remaining_amount = $data->loan - $loanObj->remaining;
                $loanObj->status = 'completed';
                $loanObj->remaining = 0;
                $loanObj->save();

                $loanObj2 = Loan::where('user_id',$data->user_id)->where('status','!=','completed')->first();
                self::enterLoanTransaction($loanObj2,$remaining_amount,$date);
                $loanObj2->remaining =$loanObj2->remaining - $remaining_amount;
                $loanObj2->save();
            }
            else
            {
                $loanObj->remaining = $loanObj->remaining - $data->loan;
                self::enterLoanTransaction($loanObj,$data->loan,$date);
                $loanObj->save();
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    public static function enterLoanTransaction($loanObj,$loan,$date)
    {
        $loan_transaction = new LoanTransaction();
        $loan_transaction->loan_id = $loanObj->id;
        $loan_transaction->amount = $loan;
        $loan_transaction->paid_on = $date;
        $loan_transaction->status = 'paid';
        $loan_transaction->method = 'Deducted from salary';
        $loan_transaction->type = 'credit';
        $loan_transaction->save();
    }    

    public static function uploadFile(Request $request, $id)
    {
        $response = ['status' => false, 'message' => ""];
        $file = $request->file;
        $fileSize = $file->getClientSize();
        if ($fileSize > 20000000) {
            $response['message'] = "Filesize exceeds 20MB";
            return $response;
        }
        $content = file_get_contents($file->getRealPath());
        $type = $file->extension();
        $fileName = FileService::getFileName($type);
        $originalName = $file->getClientOriginalName();
        $store = FileService::uploadFileInDB($content, $originalName, $type, $id, "App\Models\Admin\Loan");
        return ['status' => true, 'message' => "File Added"];
    }
}
