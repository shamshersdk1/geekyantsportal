<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

use App\Models\Gateway;
use App\Models\GatewayArp;
use App\Models\GatewayIpConnection;

class GatewayRequest implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $data;
    
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //Log::info('Processing');

        $request        = json_decode($this->data, true);
        $arp            = isset($request['arp'])?$request['arp']:'';
        $connections    = isset($request['connections'])?$request['connections']:'';
        $date           = isset($request['date'])?$request['date']:'';
        $gateway_id     = isset($request['gateway_id'])?$request['gateway_id']:'';
        $hr             = isset($request['hr'])?$request['hr']:'';
        $min            = isset($request['min'])?$request['min']:'';
        $ts             = isset($request['ts'])?$request['ts']:'';

        //Log::info('Gateway ID'. $gateway_id);
        $gateway = Gateway::find($gateway_id);
        //Log::info('Gateway ID'. $gateway->id);
        if($gateway){
            $active_ips = GatewayArp::parseAndStore($arp, $gateway_id, $date, $hr, $min);
            GatewayIpConnection::parseAndStore($connections, $gateway_id, $date, $hr, $min, $active_ips);
            //Log::info(print_r($request,true));
        }else{
            Log::info('Invalid Gateway - Skipping Data');
        }
        //Log::info('Processing completed');

    }
}
