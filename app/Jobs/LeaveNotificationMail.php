<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Session;
use App\Mail\LeaveNotificationEmail;
use Exception;
use Log;
use App\Models\User;
use App\Models\Admin\Leave;
use App\Models\Admin\ProjectResource;


class LeaveNotificationMail implements ShouldQueue
{
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    protected $leave;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Leave $leave)
    {
        $this->leave=$leave;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try 
        {
            $leave = $this->leave;
            $user=$leave->user;
            $array=array('leave'=>$this->leave,'user'=>$user->name);
            \Mail::to($this->leave->user->email)->send(new LeaveNotificationEmail($array));
            $this->delete();
            if(!empty($user->reportingManager)) {
                $array=array('leave'=>$this->leave,'user'=>$leave->user->reportingManager->name);
                \Mail::to($leave->user->reportingManager->email)->cc(config('app.leave-request-email'))->send(new LeaveNotificationEmail($array));
                $this->delete();
            } else {
                $array=array('leave'=>$this->leave,'user'=> 'Admin');
                \Mail::to(config('app.leave-request-email'))->send(new LeaveNotificationEmail($array));
                $this->delete();
            }
            $projectResources = ProjectResource::with('project')
            ->where('user_id', $user->id)
            ->where('start_date', '<=', $leave->end_date)
            ->where(function ($query) {
                $query->where('end_date', null)
                ->orWhere('end_date', '>=', $leave->start_date);
            })->get();
            if(!empty($projectResources)) {
                foreach ($projectResources as $resource) {
                    $array=array('leave'=>$this->leave,'user'=>$resource->project->projectManager->name);
                    \Mail::to($resource->project->projectManager->email)->send(new LeaveNotificationEmail($array));
                    $this->delete();
                }
            }
            Session::flash('message', 'Mail sent');
        } catch (Exception $e) {
            // Log::info('Leave notification mail');
            Session::flash('message', 'Mail not sent!');
            Session::flash('alert-class', 'alert-danger');
        }
    }
}
