<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use \App\MOdels\Admin\PayslipCsvData;

use Exception;

class DeletePayslipCsvData implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try
        {
            $data = PayslipCsvData::where('payslip_csv_month_id', $this->id)->delete();
        }
        catch(Exception $e)
        {
            \Log::info($e->getMessage());
        }
    }
}
