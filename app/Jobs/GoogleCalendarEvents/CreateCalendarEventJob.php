<?php

namespace App\Jobs\GoogleCalendarEvents;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Exception;
use Log;

use App\Models\CalendarEvent;

class CreateCalendarEventJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $leaveId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($leaveId)
    {
        $this->leaveId=$leaveId;
    }

    /**
     * CalendarEvent on 
     *
     * @return void
     */
    public function handle()
    {
        try {
            CalendarEvent::createCalendarEvent($this->leaveId);
        } catch (Exception $e) {
            \Log::info('Unable to process CalendarEvent Leave#'.$this->leaveId);
            \Log::info(json_encode($e->getMessage()));
        }
    }
}
