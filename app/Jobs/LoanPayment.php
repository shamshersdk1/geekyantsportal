<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Session;
use Exception;
use Log;

use App\Models\User;
use App\Models\Admin\PayslipData;
use App\Models\Loan;

class LoanPayment implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(PayslipData $data)
    {
        $this->data=$data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $paySlipData = $this->data;
            $loanPaymentEntry = Loan::updateLoanPaymentDetails($paySlipData);
        } catch (Exception $e) {
            \Log::info($e);
        }
    }
}
