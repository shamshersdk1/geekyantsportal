<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mail\LeaveRequestEmail;
use App\Mail\LeavePMRequestEmail;
use Session;
use Exception;
use Log;

use App\Models\User;
use App\Models\Admin\Leave;
use App\Models\Admin\ProjectResource;

class LeaveRequestMail implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $leave;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Leave $leave)
    {
        $this->leave=$leave;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $user = $this->leave->user;
            $year = date('Y-01-01', strtotime($this->leave->start_date));
            $previousLeaves = $user->leaves()->where('end_date', '>=', $year)->where('status', 'approved')->orderBy('start_date', 'desc')->get();
            $sick = $user->leaves()->where('end_date', '>=', $year)->where('start_date', '<=', date('Y-m-d' ,strtotime('+1 year', strtotime($year))))->where('type', 'sick')->where('status', 'approved')->sum('days');
            $earned = $user->leaves()->where('end_date', '>=', $year)->where('start_date', '<=', date('Y-m-d' ,strtotime('+1 year', strtotime($year))))->where('type', 'paid')->where('status', 'approved')->sum('days');
            if(!empty($this->leave->user->reportingManager)) {
                $array=array('leave'=>$this->leave,'admin'=>$this->leave->user->reportingManager->name, 'previousLeaves' => $previousLeaves, 'sick' => $sick, 'earned' => $earned);
                \Mail::to($this->leave->user->reportingManager->email)->cc(config('app.leave-request-email'))->send(new LeaveRequestEmail($array));
                $this->delete();
            } else {
                $array=array('leave'=>$this->leave,'admin'=>"Admin",  'previousLeaves' => $previousLeaves, 'sick' => $sick, 'earned' => $earned);
                \Mail::to(config('app.leave-request-email'))->send(new LeaveRequestEmail($array));
                $this->delete();
            }
            $teamLeadsArray = $this->leave->projectManagerApprovals()->pluck('user_id')->toArray();
            $teamLeads = User::whereIn('id', $teamLeadsArray)->get();
            foreach ($teamLeads as $lead) {
                $array=array('leave'=>$this->leave,'admin'=>$lead->name, 'previousLeaves' => $previousLeaves, 'sick' => $sick, 'earned' => $earned);
                \Mail::to($lead->email)->cc(config('app.leave-request-email'))->send(new LeavePMRequestEmail($array));
                $this->delete();
            }
        } catch (Exception $e) {
            // Log::info($e->getMessage());
            // Log::info('leaverequestmail');
            // Session::flash('message', 'Mail not sent!');
            // Session::flash('alert-class', 'alert-danger');
        }
    }
}
