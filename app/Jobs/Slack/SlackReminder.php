<?php

namespace App\Jobs\Slack;

use Config;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Log;

class SlackReminder implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $data;

    public function __construct($data)
    {
        //
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $userEmail = $this->data['userEmail'];
        $message = $this->data['message'];
        $channelName = $this->data['channelName'];

        $channelId = null;
        if ($channelName) {

            try {
                $client = new Client(['base_uri' => config('app.slack_url')]);
                $response = $client->request("POST", "channels.list", [
                    'form_params' => [
                        'token' => config('app.slack_token'),
                    ],
                ]);
            } catch (Exception $e) {
                \Log::info('Slack Api did not worked.');
                return ['text' => 'Slack Api did not worked.'];
            }

            $userCreateResponse = $response->getBody();

            $response = json_decode($userCreateResponse);

            //\Log::info(print_r($response,true));
            $channels = $response->channels;

            foreach ($channels as $channel) {
                if ($channel) {
                    $slackChannelName = null;
                    if (isset($channel->name)) {
                        $slackChannelName = $channel->name;
                    }

                    if ($channelName == $slackChannelName) {
                        $channelId = $channel->id;
                        break;
                    }
                }
            }

            if ($channelId && $message && $userEmail) {
                $slackUserName = null;
                try {
                    $client = new Client(['base_uri' => config('app.slack_url')]);
                    $response = $client->request("POST", "users.list", [
                        'form_params' => [
                            'token' => config('app.slack_token'),
                        ],
                    ]);
                } catch (Exception $e) {
                    \Log::info('Slack Api did not worked.');
                    return ['text' => 'Slack Api did not worked.'];
                }

                $userCreateResponse = $response->getBody();

                $response = json_decode($userCreateResponse);

                $slackUsers = $response->members;

                foreach ($slackUsers as $slackUser) {
                    if ($slackUser) {
                        $email = null;
                        if (isset($slackUser->profile->email)) {
                            $email = $slackUser->profile->email;
                        }

                        if ($email == $userEmail) {
                            $slackUserName = $slackUser->name;
                            break;
                        }
                    }
                }
                if (isset($slackUserName)) {
                    try {
                        $messageContent = '@' . $slackUserName . ' ' . $message;
                        $client = new Client(['base_uri' => config('app.slack_url')]);
                        $response = $client->request("POST", "chat.postMessage", [
                            'form_params' => [
                                'token' => config('app.slack_token'),
                                'channel' => $channelId,
                                'text' => $messageContent,
                                'link_names' => 1,
                            ],
                        ]);
                    } catch (Exception $e) {
                        return ['text' => 'Slack Api did not worked.'];
                    }
                } else {
                    return ['text' => 'User not found.'];
                }
            } else {
                \Log::info('some data is missing');
                return ['text' => 'Some data is missing.'];
            }
        } else {
            return ['text' => 'Project has no channel assossiated'];
        }
    }
}
