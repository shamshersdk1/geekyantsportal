<?php

namespace App\Jobs\Slack;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Services\ProjectService;
use App\Services\SlackService\SlackMessageService\SlackMessageService;

use App\Models\User;

use stdClass;
use View;


class SlackTimesheetNotification implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $id = $this->data['id'];
        $start_date = $this->data['start_date'];
        $end_date = $this->data['end_date'];
        $response = ProjectService::projectsArray($id, $start_date, $end_date);
        $result = [];
        foreach($response['projects'] as $project)
        {
            $dates = "";
            for($i=0; $i < count($project['data']); $i++)
            {
                if($project['data'][$i]['timesheet_id'] == null) {
                    if($dates == "") {
                        $dates = "*Dates: * ".$response['dates'][$i];
                    } else {
                        $dates = $dates." | ".$response['dates'][$i];
                    }
                }
            }
            if($dates != "") {
                $attachment = new stdClass;
                $attachment->fallback = "Unable to fetch project, please check project manually";
                $attachment->color = "#EF3B40";
                $attachment->title = $project['name'];
                $attachment->text = $dates;
                $attachment->mrkdwn_in = ["text"];
                $result[] = $attachment;
            }
        }
        if(!empty($result)) {
            $user = User::find($id);
            if($user) {
                $name = $user->name;
                $url = config('app.geekyants_portal_url')."/user/timesheet";
                $message = "Hey ".$name.", please goto\n".$url."\nand fill the timesheet for the following project for the dates mentioned";
                SlackMessageService::sendMessageToUser($id, $message, $result);
            }
        }
    }
}
