<?php

namespace App\Jobs\Slack;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\AccessLog;

use App\Services\SlackService\SlackMessageService\SlackMessageService;


class SlackSendMessage implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if($this->data['send_to'] == "user") {
            $result = SlackMessageService::sendMessageToUser($this->data['user_id'], $this->data['message'], $this->data['attachments']);
        } else {
            $result = SlackMessageService::sendMessageToChannel($this->data['channel_id'], $this->data['message'], $this->data['link_names']);
        }
        if(!$result['status']) {
            AccessLog::accessLog(NULL, 'App\Jobs\Slack', 'SlackSendMessage', 'handle', 'result', $result['message']);
        }
    }
}
