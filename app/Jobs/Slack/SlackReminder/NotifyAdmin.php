<?php

namespace App\Jobs\Slack\SlackReminder;

use App\Models\AccessLog;
use App\Models\Admin\Leave;
use App\Models\Admin\SlackAccessLog;
use App\Services\SlackService\SlackMessageService\SlackMessageService;
use Config;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use View;

class NotifyAdmin implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $leave_id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($leave_id)
    {
        $this->leave_id = $leave_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try
        {
            $leave_id = $this->leave_id;
            $leave = Leave::find($leave_id);
            $channel_id = config('app.leave_notification');

            if ($leave && $leave->user) {
                $user = $leave->user;
                if ($leave->approver) {
                    $approver = $leave->approver->name;
                } else {
                    $approver = "*unavailable*";
                }
                if (substr($user->name, -1) == "s") {
                    $userName = $user->name . "'";
                } else {
                    $userName = $user->name . "'s";
                }
                $message = View::make('slack.reminder.leave.notification.leave-reporting-admin', ['manager' => $user->reportingManager->name, 'user' => $userName, 'leave' => $leave, 'approver' => $approver])->render();
                $data = ["userId" => $user->reportingManager->id, "message" => $message];
                $result = SlackMessageService::sendMessageToChannel($channel_id, $data['message'], null, false);
                SlackAccessLog::saveData('Leave Reminder', 'User Id: ' . $data['userId'], json_encode(['message' => $data['message']]), $result['message']);

            }
        } catch (Exception $e) {
            AccessLog::accessLog(null, 'App\Jobs\Slack\SlackReminder', 'NotifyAdmin', 'handle', 'catch-block', $e->getMessage());
        }
    }
}
