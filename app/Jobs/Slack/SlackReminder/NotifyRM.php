<?php

namespace App\Jobs\Slack\SlackReminder;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\Admin\Leave;
use App\Models\AccessLog;
use App\Models\Admin\SlackAccessLog;
use App\Services\SlackService\SlackMessageService\SlackMessageService;

use View;
use Exception;

class NotifyRM implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $leave_id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($leave_id)
    {
        $this->leave_id = $leave_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try
        {
            $leave_id = $this->leave_id;
            $leave = Leave::find($leave_id);
            if($leave && $leave->user) 
            {
                $user = $leave->user;
                $admin_url = config('app.geekyants_portal_url')."/admin/reporting-manager-leaves";
                if($user->reportingManager) 
                {
                    if($leave->status == 'pending') 
                    {
                        $message = View::make('slack.reminder.leave.application.leave-reporting-manager', ['manager' => $user->reportingManager->name, 'user' => $user->name, 'leave' => $leave, 'url' => $admin_url])->render();
                        $actions = [["name" => "approved", "text" => "Approve", "type" => "button", "value" => $leave->id, "style" => "primary"],
                                    ["name" => "rejected", "text" => "Reject", "type" => "button", "value" => $leave->id, "style" => "danger"],
                                    ["text" => "View", "type" => "button", "url" => $admin_url]                               
                                ];
                        $attachment = [["text" => "Select an action", "fallback" => "Goto the portal to approve/reject the leave", "attachment_type" => "default", "callback_id" => "leave_action_reporting_manager", "actions" => $actions ]];
                        $data = ["userId" => $user->reportingManager->id, "message" => $message, "attachment" => $attachment, "as_bot" => true];
                    } 
                    else 
                    {
                        if($leave->approver) 
                        {
                            if($leave->approver->id == $user->reportingManager->id) {
                                $approver = "you";
                            } else {
                                $approver = $leave->approver->name;
                            }
                        } 
                        else 
                        {
                            $approver = "*unavailable*";
                        }
                        if (substr($user->name, -1)=="s") 
                        {
                            $userName=$user->name."'";
                        } 
                        else 
                        {
                            $userName=$user->name."'s";
                        }
                        $message = View::make('slack.reminder.leave.notification.leave-reporting-manager', ['manager' => $user->reportingManager->name, 'user' => $userName, 'leave' => $leave, 'approver' => $approver])->render();
                        $data = ["userId" => $user->reportingManager->id, "message" => $message];
                        
                    }
                    $attachment = isset($data['attachment']) ? $data['attachment'] : null;
                    $result = SlackMessageService::sendMessageToUser($data['userId'], $data['message'], $attachment);
                    SlackAccessLog::saveData('Leave Reminder', 'User Id: '.$data['userId'], json_encode(['message' => $data['message']]), $result['message']);
                }
            }    
        }
        catch (Exception $e)
        {
            AccessLog::accessLog(null, 'App\Jobs\Slack\SlackReminder', 'NotifyRM', 'handle', 'catch-block', $e->getMessage());
        }
    }
}
