<?php

namespace App\Jobs\Slack\SlackReminder\External;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\User;
use App\Models\AccessLog;
use App\Models\Admin\SlackAccessLog;
use App\Services\SlackService\SlackMessageService\SlackMessageService;

use View;
use Exception;

class ExternalApiCallUser implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $userId;
    protected $messageText;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId, $messageText)
    {
        $this->userId = $userId;
        $this->messageText = $messageText;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try
        {
            $userId = $this->userId;
            $messageText = $this->messageText;

            $user = User::find($userId);
            if ( $user )
            {
              $message = View::make('slack.reminder.external.message', ['messageText' => $messageText])->render(); 
              $attachment = null;   
              $data = ["userId" => $userId, "message" => $message, "attachment" => $attachment, "as_bot" => true];
              $result = SlackMessageService::sendMessageToUser($data['userId'], $data['message'], $attachment);
              SlackAccessLog::saveData('External API Call User', 'User Id: '.$data['userId'], json_encode(['message' => $data['message']]), $result['message']);
            }
            else{
              SlackAccessLog::saveData('External API Call User', 'User Id: '.$data['userId'], 'User not found', 'User not found');
            }
        }
        catch (Exception $e)
        {
            AccessLog::accessLog(null, 'App\Jobs\Slack\SlackReminder\External', 'ExternalApiCallUser', 'handle', 'catch-block', $e->getMessage());
        }
    }
}
