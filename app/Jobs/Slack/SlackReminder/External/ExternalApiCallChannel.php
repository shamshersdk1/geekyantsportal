<?php

namespace App\Jobs\Slack\SlackReminder\External;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\User;
use App\Models\AccessLog;
use App\Models\Admin\SlackAccessLog;
use App\Services\SlackService\SlackMessageService\SlackMessageService;

use View;
use Exception;

class ExternalApiCallChannel implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $channel_id;
    protected $lead_id;
    protected $name;
    protected $email;
    protected $company;
    protected $skype;
    protected $created_at;
    protected $referred_by;
    protected $requirement;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($channel_id, $lead_id, $name, $email, $company, $created_at, $skype = null, $referred_by = null, $requirement = null)
    {
        $this->channel_id = $channel_id;
        $this->lead_id = $lead_id;
        $this->name = $name;
        $this->email = $email;
        $this->company = $company;
        $this->skype = $skype;
        $this->created_at = $created_at;
        $this->referred_by = $referred_by;
        $this->requirement = $requirement;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try
        {
            
            $message = View::make('slack.reminder.external.message', ['name' => $this->name, 'email' => $this->email, 'company' => $this->company, 
                                  'skype' => $this->skype, 'created_at' => $this->created_at, 'referred_by' => $this->referred_by, 'requirement' => $this->requirement])->render(); 
            
            $actions = [
                        ["name" => "follow_up", "text" => "I will follow up", "type" => "button", "value" => $this->lead_id],
                        ["name" => "spam", "text" => "Mark as spam", "type" => "button", "value" => $this->lead_id]
                       ];
            $attachment = [["text" => "", "fallback" => "Goto the leads management to follow up", "attachment_type" => "default", "callback_id" => "leads_action", "actions" => $actions ]];

            $result = SlackMessageService::sendMessageToChannel($this->channel_id, $message, null, false, $attachment);
            SlackAccessLog::saveData('External API Call', 'User Id: '.$this->channel_id, json_encode(['message' => $message]), $result['message']);
            
        }
        catch (Exception $e)
        {
            AccessLog::accessLog(null, 'App\Jobs\Slack\SlackReminder\External', 'ExternalApiCall', 'handle', 'catch-block', $e->getMessage());
        }
    }
}
