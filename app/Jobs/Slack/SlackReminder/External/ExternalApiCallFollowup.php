<?php

namespace App\Jobs\Slack\SlackReminder\External;

use App\Models\AccessLog;
use App\Models\Admin\SlackAccessLog;
use App\Services\SlackService\SlackMessageService\SlackMessageService;
use Carbon\Carbon;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use View;

class ExternalApiCallFollowup implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $channel_id;
    protected $lead_id;
    protected $name;
    protected $email;
    protected $company;
    protected $skype;
    protected $created_at;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($channel_id, $lead_id, $name, $email, $company, $created_at, $skype = null)
    {
        $this->channel_id = $channel_id;
        $this->lead_id = $lead_id;
        $this->name = $name;
        $this->email = $email;
        $this->company = $company;
        $this->skype = $skype;
        $this->created_at = $created_at;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try
        {
            $leadId = $this->lead_id;
            $url = '#';
            $message = View::make('slack.leads.follow-up-message', ['name' => $this->name, 'email' => $this->email, 'company' => $this->company,
                'skype' => $this->skype, 'created_at' => $this->created_at])->render();

            $actions = [
                "name" => "date_list",
                "value" => $leadId,
                "text" => "Pick a date",
                "type" => "select",
                "options" => [
                    [
                        "text" => "After 7 days",
                        "value" => Carbon::now()->addDays(7) . '_' . $leadId,
                    ],
                    [
                        "text" => "After 2 weeks",
                        "value" => Carbon::now()->addDays(14) . '_' . $leadId,
                    ],
                    [
                        "text" => "After a month",
                        "value" => Carbon::now()->addDays(30) . '_' . $leadId,
                    ],
                    [
                        "text" => "After 2 months",
                        "value" => Carbon::now()->addDays(60) . '_' . $leadId,
                    ],
                ],
                "selected_options" => [
                    [
                        "text" => "After 7 days",
                        "value" => Carbon::now()->addDays(7) . '_' . $leadId,
                    ],
                ],
            ];
            $attachment = [
                [
                    "text" => "I will follow up in - ",
                    "fallback" => "Go to Lead-Management to check the list of not followed up leads",
                    "attachment_type" => "default",
                    "callback_id" => "Followup_action",
                    "actions" => [$actions,
                        ["name" => "close", "text" => "Submit", "type" => "button", "value" => Carbon::now()->addDays(7) . '_' . $leadId, "style" => "primary"],
                        ["name" => "lost", "text" => "Lost", "type" => "button", "style" => "default", "value" => $this->lead_id],
                        ["name" => "spam", "text" => "Mark as Spam", "type" => "button", "value" => $this->lead_id, "style" => "danger"],
                        ["name" => "converted", "text" => "Converted", "type" => "button", "value" => $this->lead_id, "style" => "primary"],
                    ],
                ],
            ];

            $result = SlackMessageService::sendMessageToChannel($this->channel_id, $message, null, false, $attachment);
            SlackAccessLog::saveData('External API Call', 'User Id: ' . $this->channel_id, json_encode(['message' => $message]), $result['message']);

        } catch (Exception $e) {
            AccessLog::accessLog(null, 'App\Jobs\Slack\SlackReminder\External', 'ExternalApiCall', 'handle', 'catch-block', $e->getMessage());
        }
    }
}
