<?php

namespace App\Jobs\Slack\SlackReminder;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\Admin\Leave;
use App\Models\AccessLog;
use App\Models\Admin\SlackAccessLog;
use App\Services\SlackService\SlackMessageService\SlackMessageService;

use View;
use Exception;

class NotifyUser implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $leave_id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($leave_id)
    {
        $this->leave_id = $leave_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try
        {
            $leave_id = $this->leave_id;
            $leave = Leave::find($leave_id);
            if ( $leave && $leave->user )
            {
                $user = $leave->user;
                $user_url = config('app.geekyants_portal_url')."/user/leaves";
                if ( $leave->status != 'pending' )
                {
                    if($leave->approver) {
                        $approver = $leave->approver->name;
                    } 
                    else {
                        $approver = "*unavailable*";
                    }
                    if($approver == $user->name) {
                        $approver = "you";
                    }
                    $message = View::make('slack.reminder.leave.notification.leave-user', ['user' => $user->name, 'leave' => $leave, 'approver' => $approver])->render();
                    $data = ["userId" => $user->id, "message" => $message];
                    $attachment = isset($data['attachment']) ? $data['attachment'] : null;
                    $result = SlackMessageService::sendMessageToUser($data['userId'], $data['message'], $attachment);
                    SlackAccessLog::saveData('Leave Reminder', 'User Id: '.$data['userId'], json_encode(['message' => $data['message']]), $result['message']);
                }
            }
        }
        catch (Exception $e)
        {
            AccessLog::accessLog(null, 'App\Jobs\Slack\SlackReminder', 'NotifyUser', 'handle', 'catch-block', $e->getMessage());
        }
    }
}
