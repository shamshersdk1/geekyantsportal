<?php

namespace App\Jobs\Slack\SlackReminder;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\AccessLog;
use App\Models\Admin\SlackAccessLog;
use App\Services\SlackService\SlackMessageService\SlackMessageService;


class ProjectResourceReminder implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try
        {
            $result = SlackMessageService::sendMessageToChannel($this->data['channelId'], $this->data['message'], false, 'Ant-Manager');
            SlackAccessLog::saveData('Loan Reminder', 'User Id: '.$this->data['channelId'], json_encode(['message' => $this->data['message']]), $result['message']);
        }
        catch (Exception $e)
        {
            AccessLog::accessLog(null, 'App\Jobs\Slack\SlackReminder', 'ProjectResourceReminder', 'handle', 'catch-block', $e->getMessage());
        }
    }
}
