<?php

namespace App\Jobs\Slack\SlackReminder\SshKey;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\User;
use App\Models\SshKey;
use App\Models\AccessLog;
use App\Models\Admin\SlackAccessLog;
use App\Services\SlackService\SlackMessageService\SlackMessageService;

use View;
use Exception;

class NotifyUser implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $sshKeyId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($sshKeyId)
    {
        $this->sshKeyId = $sshKeyId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try
        {
            $sshKeyObj = SshKey::find($this->sshKeyId);
            $url = config('app.geekyants_portal_url')."/admin/ssh-keys/".$sshKeyObj->id;
           
            if ( $sshKeyObj )
            {
                $ssh_key = $sshKeyObj->value;
                $message = View::make('slack.reminder.ssh-key.message-user', ['ssh_key' => $ssh_key, 'url' => $url ])->render();
                $actions = [["name" => "yes", "text" => "Yes", "type" => "button", "value" => $sshKeyObj->id, "style" => "primary"],
                            ["name" => "no", "text" => "No", "type" => "button", "value" => $sshKeyObj->id, "style" => "danger"],
                            ["text" => "View", "type" => "button", "url" => $url]                               
                        ];
                $attachment = [["text" => "Select an action", "fallback" => "Goto the portal to verify your ssh-key", "attachment_type" => "default", "callback_id" => "ssh_action", "actions" => $actions ]];
                $data = ["userId" => $sshKeyObj->user_id, "message" => $message, "attachment" => $attachment, "as_bot" => true];


                $attachment = isset($data['attachment']) ? $data['attachment'] : null;
                $result = SlackMessageService::sendMessageToUser($data['userId'], $data['message'], $attachment);
                SlackAccessLog::saveData('SSH KEY', 'User Id: '.$data['userId'], json_encode(['message' => $data['message']]), $result['message']);
            }
        }
        catch (Exception $e)
        {
            AccessLog::accessLog(null, 'App\Jobs\Slack\SlackReminder\SshKey', 'NotifyUser', 'handle', 'catch-block', $e->getMessage());
        }
    }
}
