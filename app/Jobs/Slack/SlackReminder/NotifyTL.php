<?php

namespace App\Jobs\Slack\SlackReminder;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\Admin\Leave;
use App\Models\AccessLog;
use App\Models\Admin\SlackAccessLog;
use App\Models\Admin\ProjectResource;
use App\Models\Admin\Project;
use App\Services\SlackService\SlackMessageService\SlackMessageService;

use View;
use Exception;

class NotifyTL implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $leave_id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($leave_id)
    {
        $this->leave_id = $leave_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try
        {
            $leave_id = $this->leave_id;
            $leave = Leave::find($leave_id);
            if ( $leave && $leave->user )
            {
                $user = $leave->user;
                $admin_url = config('app.geekyants_portal_url')."/admin/reporting-manager-leaves";
                
                $resources = ProjectResource::where('user_id', $user->id)->where('start_date', '<=', $leave->end_date)
                            ->where(function ($query) use ($leave){
                            $query->where('end_date', null)->orWhere('end_date', '>=', $leave->start_date);
                        })->pluck('project_id')->toArray();
                $projects = Project::with('projectManager')->whereIn('id', $resources)->get();
                
                $teamLeads = [];
                foreach($projects as $project)
                {
                    if($project->projectManager) {
                        $teamLeads[] = ['id' => $project->projectManager->id, 'name' => $project->projectManager->name];
                    }
                }
                if(count($teamLeads) > 0) {
                    $teamLeads = array_map("unserialize", array_unique(array_map("serialize", $teamLeads)));
                }

                if($leave->status == 'pending') 
                {
                    if($teamLeads && count($teamLeads) > 0) 
                    {
                        foreach($teamLeads as $lead)
                        {
                            if($lead['id'] == $user->id) {
                                continue;
                            }
                            if($user->reportingManager && $user->reportingManager->id == $lead['id']) {
                                continue;
                            }
                            $message = View::make('slack.reminder.leave.application.leave-team-lead', ['lead' => $lead['name'], 'user' => $user->name, 'leave' => $leave, 'url' => $admin_url])->render();
                            $actions = [["name" => "approved", "text" => "Approve", "type" => "button", "value" => $leave->id, "style" => "primary"],
                                        ["name" => "rejected", "text" => "Reject", "type" => "button", "value" => $leave->id, "style" => "danger"],
                                        ["text" => "View", "type" => "button", "url" => $admin_url]                               
                                        ];
                            $attachment = [["text" => "Select an action", "fallback" => "Goto the portal to approve/reject the leave", "attachment_type" => "default", "callback_id" => "leave_action_team_lead", "actions" => $actions ]];
                            $data = ["userId" => $lead['id'], "message" => $message, "attachment" => $attachment, "as_bot" => true];
                            
                            $attachment = isset($data['attachment']) ? $data['attachment'] : null;
                            $result = SlackMessageService::sendMessageToUser($data['userId'], $data['message'], $attachment);
                            SlackAccessLog::saveData('Leave Reminder', 'User Id: '.$data['userId'], json_encode(['message' => $data['message']]), $result['message']);
                        }
                    }
                }
                else 
                {
                    if($leave->approver) {
                        $approver = $leave->approver->name;
                    } else {
                        $approver = "*unavailable*";
                    }

                    if($approver == $user->name) {
                        $approver = $user->name;
                    }
                    if (substr($user->name, -1)=="s") {
                        $userName=$user->name."'";
                    } else {
                        $userName=$user->name."'s";
                    }
                    if(count($teamLeads) > 0) {
                        foreach($teamLeads as $lead)
                        {
                            if($lead['id'] == $user->id) {
                                continue;
                            }
                            if($user->reportingManager && $user->reportingManager->id == $lead['id']) {
                                continue;
                            }
                            if($approver == $lead['name']) {
                                $approver = "you";
                            }
                            $message = View::make('slack.reminder.leave.notification.leave-team-lead', ['lead' => $lead['name'], 'user' => $userName, 'leave' => $leave, 'url' => $admin_url, 'approver' => $approver])->render();
                            $data = ["userId" => $lead['id'], "message" => $message];

                            $attachment = isset($data['attachment']) ? $data['attachment'] : null;
                            $result = SlackMessageService::sendMessageToUser($data['userId'], $data['message'], $attachment);
                            SlackAccessLog::saveData('Leave Reminder', 'User Id: '.$data['userId'], json_encode(['message' => $data['message']]), $result['message']);
                        }
                    }
                }
            }
        }
        catch (Exception $e)
        {
            AccessLog::accessLog(null, 'App\Jobs\Slack\SlackReminder', 'NotifyTL', 'handle', 'catch-block', $e->getMessage());
        }
    }
}
