<?php

namespace App\Jobs\Slack\SlackReminder\Late;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\User;
use App\Models\AccessLog;
use App\Models\Admin\SlackAccessLog;
use App\Services\SlackService\SlackMessageService\SlackMessageService;

use View;
use Exception;

class NotifyAdmin implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $userId;
    protected $reportedById;
    protected $isMarked;
    protected $date;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId, $reportedById, $isMarked, $date)
    {
        $this->userId = $userId;
        $this->reportedById = $reportedById;
        $this->isMarked = $isMarked;
        $this->date = $date;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try
        {
            $channel_id = config('app.late_notification');
            $currentDate = date('Y-m-d');
            if ( $this->date == $currentDate )
            {
                $date = 'today';
            }
            else
            {
                $dateObj = \Carbon\Carbon::parse( $this->date);
                $date = $dateObj->format('d/m/Y');
            }

            if ( $this->isMarked )
            {
                $status = 'marked as Late Comer';
            }
            else
            {
                $status = 'removed from the Late Comer list';
            }
            $user = User::find($this->userId);
            $reportedBy = User::find($this->reportedById);
            if ( $user && $reportedBy )
            {
              $message = View::make('slack.reminder.late..message-admin', ['user' => $user->name, 'reportedBy' => $reportedBy->name, 'status' => $status, 'date' => $date])->render();
              $data = ["userId" => $user->id, "message" => $message];
              $attachment = isset($data['attachment']) ? $data['attachment'] : null;
              $result = SlackMessageService::sendMessageToChannel($channel_id, $data['message'], null, false);
              SlackAccessLog::saveData('Late Reminder', 'User Id: '.$data['userId'], json_encode(['message' => $data['message']]), $result['message']);
            }
        }
        catch (Exception $e)
        {
            AccessLog::accessLog(null, 'App\Jobs\Slack\SlackReminder\Late', 'NotifyAdmin', 'handle', 'catch-block', $e->getMessage());
        }
    }
}
