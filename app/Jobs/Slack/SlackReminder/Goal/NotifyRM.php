<?php

namespace App\Jobs\Slack\SlackReminder\Goal;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\Admin\UserGoal;
use App\Models\AccessLog;
use App\Models\Admin\SlackAccessLog;
use App\Services\SlackService\SlackMessageService\SlackMessageService;

use View;
use Exception;

class NotifyRM implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $goal_id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($goal_id)
    {
        $this->goal_id = $goal_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try
        {
            $goal_id = $this->goal_id;
            $goal = UserGoal::find($goal_id);
            if ( $goal && $goal->user )
            {
                $user = $goal->user;
                $admin_url = config('app.geekyants_portal_url')."/admin/goal-user-list/".$user->id.'/'.$goal->id.'/view';
                $manager = $goal->user->reportingManager->name;
                switch($goal->status)
                {
                    case 'confirmed' : 
                        $message = View::make('slack.reminder.goal.notification.message-rm', ['manager' => $manager, 'user' => $user->name, 'goal' => $goal, 'url' => $admin_url])->render();
                        break;
                    case 'running' :
                        $message = View::make('slack.reminder.goal.notification.message-rm-freeze', ['manager' => $manager, 'user' => $user->name, 'goal' => $goal, 'url' => $admin_url])->render();
                        break;
                }
                
                $actions = [["text" => "View", "type" => "button", "url" => $admin_url]];
                $attachment = [["text" => "Select an action", "fallback" => "Goto the portal to check the goal", "attachment_type" => "default", "callback_id" => "goal_action_admin", "actions" => $actions ]];
                
                $data = ["userId" => $user->reportingManager->id, "message" => $message, "attachment" => $attachment, "as_bot" => true];
                
                $attachment = isset($data['attachment']) ? $data['attachment'] : null;
                $result = SlackMessageService::sendMessageToUser($data['userId'], $data['message'], $attachment);
                SlackAccessLog::saveData('Goal Reminder', 'User Id: '.$data['userId'], json_encode(['message' => $data['message']]), $result['message']);
        
            }
        }
        catch (Exception $e)
        {
            AccessLog::accessLog(null, 'App\Jobs\Slack\SlackReminder\Goal', 'NotifyRM', 'handle', 'catch-block', $e->getMessage());
        }
    }
}
