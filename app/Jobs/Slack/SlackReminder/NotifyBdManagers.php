<?php

namespace App\Jobs\Slack\SlackReminder;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\AccessLog;
use App\Models\Admin\SlackAccessLog;
use App\Services\SlackService\SlackMessageService\SlackMessageService;

use View;
use Exception;
use Config;

class NotifyBdManagers implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $channelName;
    protected $channelId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($channelId, $channelName)
    {
        $this->channelId = $channelId;
        $this->channelName = $channelName;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try
        {
            $channelId = Config::get('slack.bd_channel_id');
            $createProjectUrl = config('app.geekyants_portal_url')."/admin/project/create";
            $mapToProject = config('app.geekyants_portal_url')."/admin/project";
            $message = View::make('slack.timesheet.timesheet-bd-managers', ['channelName' => $this->channelName])->render();
            $actions =  [
                            ["text" => "Click To Create New Project", "type" => "button", "url" => $createProjectUrl],
                            ["text" => "Map To Project", "type" => "button", "url" => $mapToProject]
                        ];
            $attachment = [["text" => "", "fallback" => "Goto the portal to link the porject", "attachment_type" => "default", "callback_id" => "project_channel_link", "actions" => $actions ]];
            
            $result = SlackMessageService::sendMessageToChannel($channelId, $message, null, false, $attachment);
            SlackAccessLog::saveData('Link project to channel', 'Channel id: '.$channelId, json_encode(['message' => $message]), $result['message']);
                
            
        }
        catch (Exception $e)
        {
            AccessLog::accessLog(null, 'App\Jobs\Slack\SlackReminder', 'NotifyBdManagers', 'handle', 'catch-block', $e->getMessage());
        }
    }
}
