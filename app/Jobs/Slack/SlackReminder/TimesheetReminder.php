<?php

namespace App\Jobs\Slack\SlackReminder;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\Admin\Project;
use App\Models\Admin\SlackAccessLog;
use App\Models\User;
use App\Services\SlackService\SlackMessageService\SlackMessageService;

use View;

class TimesheetReminder implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = User::find($this->data['user_id']);
        if($user) {
            $projects = Project::whereIn('id', $this->data['projects'])->get();
            if(!empty($projects)) {
                $project_names = "";
                for($i = 0; $i<count($projects); $i++)
                {
                    $project = $projects[$i];
                    $lead = $project->projectManager;
                    if($lead && ($lead->id != $user->id)) {
                        $message = View::make('slack.reminder.timesheet.timesheet-team-lead', ['user' => $user->name, 'project' => $project->project_name, 'lead' => $lead->name])->render();
                        SlackMessageService::sendMessageToUser($lead->id, $message);
                        SlackAccessLog::saveData('Timesheet Reminder', 'User Id: '.$user->id, json_encode(['message' => $message]), $result['message']);
                    }
                    if($i == 0) {
                        $project_names = "*".$project->project_name."*";
                    } elseif($i == (count($projects)-1)) {
                        $project_names = $project_names." and *".$project->project_name."*";
                    } else {
                        $project_names = $project_names.", *".$project->project_name."*";
                    }
                }
                $url = config('app.geekyants_portal_url')."/user/timesheet";
                $message = View::make('slack.reminder.timesheet.timesheet-user', ['user' => $user->name, 'project' => $project_names, 'url' => $url])->render();
                $result = SlackMessageService::sendMessageToUser($user->id, $message);
                SlackAccessLog::saveData('Timesheet Reminder', 'User Id: '.$user->id, json_encode(['message' => $message]), $result['message']);
                $manager = $user->reportingManager;
                if($manager && $manager->id != $user->id) {
                    $message = View::make('slack.reminder.timesheet.timesheet-reporting-manager', ['user' => $user->name, 'project' => $project_names, 'manager' => $manager->name])->render();
                    $result = SlackMessageService::sendMessageToUser($manager->id, $message);
                    SlackAccessLog::saveData('Timesheet Reminder', 'User Id: '.$user->id, json_encode(['message' => $message]), $result['message']);    
                }
            }
        }
    }
}
