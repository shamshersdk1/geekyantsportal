<?php

namespace App\Jobs\Slack\SlackReminder\Timesheet;

use App\Models\AccessLog;
use App\Models\Admin\SlackAccessLog;
use App\Models\User;
use App\Services\SlackService\SlackMessageService\SlackMessageService;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use View;

class NotifyRM implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $userId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId)
    {
        $this->userId = $userId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try
        {
            $userId = $this->userId;
            $userObj = User::find($userId);
            if ($userObj->reportingManager) {
                $rm = $userObj->reportingManager;
                $userSettingObj = $rm->RMNotificationReminder;

                if (empty($userSettingObj) || (!empty($userSettingObj) && $userSettingObj->value == 1)) {
                    $message = View::make('slack.timesheet.timesheet-rm-reminder', ['userObj' => $userObj])->render();
                    // $actions = [["text" => "Log Timesheet", "type" => "button", "url" => $url]];
                    // $attachment = [["text" => "", "fallback" => "Goto the portal to check the request", "attachment_type" => "default", "callback_id" => "timesheet_teamlead_action", "actions" => $actions ]];
                    $attachment = [];
                    $data = ["userId" => $userObj->reportingManager->id, "message" => $message, "attachment" => $attachment, "as_bot" => true];
                    $attachment = isset($data['attachment']) ? $data['attachment'] : null;
                    $result = SlackMessageService::sendMessageToUser($data['userId'], $data['message'], $attachment);
                    SlackAccessLog::saveData('Notify User', 'User Id: ' . $data['userId'], json_encode(['message' => $data['message']]), $result['message']);
                }
            }
            // $url = config('app.geekyants_portal_url')."/admin/new-timesheet";
        } catch (Exception $e) {
            AccessLog::accessLog(null, 'App\Jobs\Slack\SlackReminder\Timesheet', 'NotifyRM', 'handle', 'catch-block', $e->getMessage());
        }
    }
}
