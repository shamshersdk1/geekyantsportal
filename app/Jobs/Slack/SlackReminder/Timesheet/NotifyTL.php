<?php

namespace App\Jobs\Slack\SlackReminder\Timesheet;

use App\Models\AccessLog;
use App\Models\Admin\SlackAccessLog;
use App\Models\User;
use App\Services\SlackService\SlackMessageService\SlackMessageService;
use App\Services\TimesheetService;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use View;

class NotifyTL implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $teamLeadId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($teamLeadId)
    {
        $this->teamLeadId = $teamLeadId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try
        {
            $teamLeadId = $this->teamLeadId;

            $userObj = User::find($teamLeadId);
            $userSettingObj = $userObj->RMNotificationReminder;
            if (empty($userSettingObj) || (!empty($userSettingObj) && $userSettingObj->value == 1)) {
                $url = config('app.geekyants_portal_url') . "/admin/new-timesheet/lead-view";
                // $date = date('Y-m-d', strtotime("-1 days"));
                $day = date('N');
                if ($day == 1) // For Monday
                {
                    $date = date("Y-m-d", strtotime("-3 days"));
                } else // For all days except monday
                {
                    $date = date('Y-m-d', strtotime("-1 days"));
                }
                $projectDetails = TimesheetService::leadTimesheetDetailsDateWise($teamLeadId, $date);
                $message = View::make('slack.timesheet.timesheet-team-leads', ['projectDetails' => $projectDetails])->render();
                $actions = [["text" => "Click to Review & Approve on Portal", "type" => "button", "url" => $url]];
                $attachment = [["text" => "", "fallback" => "Goto the portal to check the request", "attachment_type" => "default", "callback_id" => "timesheet_teamlead_action", "actions" => $actions]];
                $data = ["userId" => $teamLeadId, "message" => $message, "attachment" => $attachment, "as_bot" => true];
                $attachment = isset($data['attachment']) ? $data['attachment'] : null;
                $result = SlackMessageService::sendMessageToUser($data['userId'], $data['message'], $attachment);
                SlackAccessLog::saveData('Notify Tl', 'User Id: ' . $data['userId'], json_encode(['message' => $data['message']]), $result['message']);
            }
        } catch (Exception $e) {
            AccessLog::accessLog(null, 'App\Jobs\Slack\SlackReminder\Timesheet', 'NotifyTL', 'handle', 'catch-block', $e->getMessage());
        }
    }
}
