<?php

namespace App\Jobs\Slack\SlackReminder\Timesheet;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\AccessLog;
use App\Models\Admin\SlackAccessLog;
use App\Models\User;
use App\Services\SlackService\SlackMessageService\SlackMessageService;

use View;
use Exception;

class NotifyUser implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $userId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId)
    {
        $this->userId = $userId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try
        {
            $userId = $this->userId;
            $userObj = User::find($userId);
            $url = config('app.geekyants_portal_url')."/admin/new-timesheet";

            $message = View::make('slack.timesheet.timesheet-user-reminder', ['userObj' => $userObj])->render();
            $actions = [["text" => "Log Timesheet", "type" => "button", "url" => $url]];
            $attachment = [["text" => "", "fallback" => "Goto the portal to check the request", "attachment_type" => "default", "callback_id" => "timesheet_teamlead_action", "actions" => $actions ]];
            $data = ["userId" => $userId, "message" => $message, "attachment" => $attachment, "as_bot" => true];
            $attachment = isset($data['attachment']) ? $data['attachment'] : null;
            $result = SlackMessageService::sendMessageToUser($data['userId'], $data['message'], $attachment);
            SlackAccessLog::saveData('Notify User', 'User Id: '.$data['userId'], json_encode(['message' => $data['message']]), $result['message']);
            
        }
        catch (Exception $e)
        {
            AccessLog::accessLog(null, 'App\Jobs\Slack\SlackReminder\Timesheet', 'NotifyUser', 'handle', 'catch-block', $e->getMessage());
        }
    }
}
