<?php

namespace App\Jobs\Slack\SlackReminder\FeedbackReview;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\AccessLog;
use App\Models\Admin\SlackAccessLog;
use App\Models\User;
use App\Models\Admin\FeedbackMonth;
use App\Services\FeedbackService;
use App\Services\SlackService\SlackMessageService\SlackMessageService;

use View;
use Exception;

class NotifyMultiple implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $userId;
    protected $reviewerId;
    protected $month;
    protected $year;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId, $reviewerId, $month, $year)
    {
        $this->userId = $userId;
        $this->reviewerId = $reviewerId;
        $this->month = $month;
        $this->year = $year;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try
        {
            $user = User::find($this->userId);
            $reviewer = User::find($this->reviewerId);
            $month = $this->month;
            $year = $this->year;
            $feedbackMonthObj = FeedbackMonth::where('month',$month)->where('year',$year)->first();
            $month_name_array = [
                1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 
                10 => 'Ocotber', 11 => 'November', 12 => 'December'
              ];
            $monthDisplayText = $month_name_array[$month].', '.$year;
            if ( !$feedbackMonthObj )
            {
                AccessLog::accessLog(null, 'App\Jobs\Slack\SlackReminder\Feedback', 'NotifyMulti', 'handle', 'catch-block', 'feedbackMonth object not found');
                return;
            }
            $url = config('app.geekyants_portal_url')."/admin/feedback/review/".$this->userId."/".$feedbackMonthObj->id;
            
            $message = View::make('slack.feedback-review.message-multiple', ['user' => $user, 'reviewer' => $reviewer, 'monthDisplayText' => $monthDisplayText])->render();
            $actions = [["text" => "Click to Review on Portal", "type" => "button", "url" => $url]];
            $attachment = [["text" => "", "fallback" => "Goto the portal to check the request", "attachment_type" => "default", "callback_id" => "timesheet_teamlead_action", "actions" => $actions ]];
            $data = ["userId" => $reviewer->id, "message" => $message, "attachment" => $attachment, "as_bot" => true];
            $attachment = isset($data['attachment']) ? $data['attachment'] : null;
            $result = SlackMessageService::sendMessageToUser($data['userId'], $data['message'], $attachment);
            SlackAccessLog::saveData('Notify Reviewer', 'User Id: '.$data['userId'], json_encode(['message' => $data['message']]), $result['message']);
            
        }
        catch (Exception $e)
        {
            AccessLog::accessLog(null, 'App\Jobs\Slack\SlackReminder\Feedback', 'NotifyMulti', 'handle', 'catch-block', $e->getMessage());
        }
    }
}
