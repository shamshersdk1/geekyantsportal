<?php

namespace App\Jobs\Slack\SlackReminder\FeedbackReview;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\AccessLog;
use App\Models\Admin\SlackAccessLog;
use App\Models\Admin\User;
use App\Models\Admin\FeedbackUser;
use App\Services\FeedbackService;
use App\Services\SlackService\SlackMessageService\SlackMessageService;

use View;
use Exception;

class NotifySingle implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $feedbackUserId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($feedbackUserId)
    {
        $this->feedbackUserId = $feedbackUserId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try
        {
            $feedbackUserId = $this->feedbackUserId;
            $feedbackUserObj = FeedbackUser::find($feedbackUserId);
            $month = $feedbackUserObj->feedbackMonth->month;
            $year = $feedbackUserObj->feedbackMonth->year;
            $month_name_array = [
                1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 
                10 => 'Ocotber', 11 => 'November', 12 => 'December'
              ];
            $monthDisplayText = $month_name_array[$month].', '.$year;
            if ( !$feedbackUserObj )
            {
                AccessLog::accessLog(null, 'App\Jobs\Slack\SlackReminder\Timesheet', 'NotifySingle', 'handle', 'catch-block', 'FeedbackUser object not found');
                return;
            }
            $url = config('app.geekyants_portal_url')."/admin/feedback/review/".$feedbackUserObj->user_id."/".$feedbackUserObj->feedback_month_id;
            
            $message = View::make('slack.feedback-review.message-reviewer', ['feedbackUserObj' => $feedbackUserObj, 'monthDisplayText' => $monthDisplayText])->render();
            $actions = [["text" => "Click to Review on Portal", "type" => "button", "url" => $url]];
            $attachment = [["text" => "", "fallback" => "Goto the portal to check the request", "attachment_type" => "default", "callback_id" => "timesheet_teamlead_action", "actions" => $actions ]];
            $data = ["userId" => $feedbackUserObj->reviewer_id, "message" => $message, "attachment" => $attachment, "as_bot" => true];
            $attachment = isset($data['attachment']) ? $data['attachment'] : null;
            $result = SlackMessageService::sendMessageToUser($data['userId'], $data['message'], $attachment);
            SlackAccessLog::saveData('Notify Tl', 'User Id: '.$data['userId'], json_encode(['message' => $data['message']]), $result['message']);
            
        }
        catch (Exception $e)
        {
            AccessLog::accessLog(null, 'App\Jobs\Slack\SlackReminder\Timesheet', 'NotifySingle', 'handle', 'catch-block', $e->getMessage());
        }
    }
}
