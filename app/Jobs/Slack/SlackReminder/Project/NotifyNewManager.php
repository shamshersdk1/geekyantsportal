<?php

namespace App\Jobs\Slack\SlackReminder\Project;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\AccessLog;
use App\Models\Admin\SlackAccessLog;
use App\Services\SlackService\SlackMessageService\SlackMessageService;
use App\Models\User;
use App\Models\Admin\Project;

use View;
use Exception;

class NotifyNewManager implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $userId;
    protected $projectId;
    protected $type;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId, $projectId, $type)
    {
        $this->userId = $userId;
        $this->projectId = $projectId;
        $this->type = $type;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try
        {
            $userObj = User::find($this->userId);
            $projectObj = Project::find($this->projectId);
            $managerType = '';
            switch($this->type)
            {
                case 'account-manager' : 
                    $managerType = 'Account Manager';
                    break;
                case 'project-manager' :
                    $managerType = 'Project Manager';
                    break;
                case 'sales-manager' :
                    $managerType = 'Sales Manager';
                    break;
            }
            if($userObj && $projectObj) 
            { 
                $message = View::make('slack.reminder.project.message-new-manager', ['manager_type' => $managerType, 'projectObj' => $projectObj])->render();
                $data = ["userId" => $userObj->id, "message" => $message];
                $attachment = isset($data['attachment']) ? $data['attachment'] : null;
                $result = SlackMessageService::sendMessageToUser($data['userId'], $data['message'], $attachment);
                SlackAccessLog::saveData('Project'.$this->type.'Assign', 'User Id: '.$data['userId'], json_encode(['message' => $data['message']]), $result['message']);  
            }
            {
                AccessLog::accessLog(null, 'App\Jobs\Slack\SlackReminder\Project', 'NotifyNewManager', 'handle', 'catch-block', 'User/Project not found');
            }
        }
        catch (Exception $e)
        {
            AccessLog::accessLog(null, 'App\Jobs\Slack\SlackReminder\Project', 'NotifyNewManager', 'handle', 'catch-block', $e->getMessage());
        }
    }
}
