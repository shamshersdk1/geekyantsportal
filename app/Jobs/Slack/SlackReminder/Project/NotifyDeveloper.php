<?php

namespace App\Jobs\Slack\SlackReminder\Project;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\AccessLog;
use App\Models\Admin\SlackAccessLog;
use App\Services\SlackService\SlackMessageService\SlackMessageService;
use App\Models\User;
use App\Models\Admin\Project;

use View;
use Exception;

class NotifyDeveloper implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $userId;
    protected $projectId;
    protected $type;
    protected $startDate;
    protected $endDate;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId, $projectId, $type, $startDate, $endDate)
    {
        $this->userId = $userId;
        $this->projectId = $projectId;
        $this->type = $type;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try
        {
            $userObj = User::find($this->userId);
            $projectObj = Project::find($this->projectId);
            if($userObj && $projectObj) 
            { 
                $message = View::make('slack.reminder.project.message-developer', ['type' => $this->type, 'projectObj' => $projectObj,
                                                                                    'startDate' => $this->startDate, 'endDate' => $this->endDate])->render();
                $data = ["userId" => $userObj->id, "message" => $message];
                $attachment = isset($data['attachment']) ? $data['attachment'] : null;
                $result = SlackMessageService::sendMessageToUser($data['userId'], $data['message'], $attachment);
                SlackAccessLog::saveData('Project Developer '.$this->type, 'User Id: '.$data['userId'], json_encode(['message' => $data['message']]), $result['message']);  
            }
            {
                AccessLog::accessLog(null, 'App\Jobs\Slack\SlackReminder\Project', 'NotifyDeveloper', 'handle', 'catch-block', 'User/Project not found');
            }
        }
        catch (Exception $e)
        {
            AccessLog::accessLog(null, 'App\Jobs\Slack\SlackReminder\Project', 'NotifyDeveloper', 'handle', 'catch-block', $e->getMessage());
        }
    }
}
