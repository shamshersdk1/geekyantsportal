<?php

namespace App\Jobs\Slack\SlackReminder\Project;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\AccessLog;
use App\Models\Admin\SlackAccessLog;
use App\Services\SlackService\SlackMessageService\SlackMessageService;
use App\Models\User;
use App\Models\Admin\Project;

use View;
use Exception;
use Config;

class NotifyChannel implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $userId;
    protected $projectId;
    protected $type;
    protected $startDate;
    protected $endDate;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId, $projectId, $type, $startDate, $endDate)
    {
        $this->userId = $userId;
        $this->projectId = $projectId;
        $this->type = $type;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try
        {
            $userObj = User::find($this->userId);
            $projectObj = Project::find($this->projectId);
            $channelId = Config::get('slack.project_channel_id');
            if($userObj && $projectObj) 
            { 
                $message = View::make('slack.reminder.project.message-channel', ['user' => $userObj,'type' => $this->type, 'projectObj' => $projectObj,
                                                                                    'startDate' => $this->startDate, 'endDate' => $this->endDate])->render();
                $data = ["userId" => $userObj->id, "message" => $message];
                
                $result = SlackMessageService::sendMessageToChannel($channelId, $data['message'], null, false);
                SlackAccessLog::saveData('Project Developer '.$this->type, 'User Id: '.$data['userId'], json_encode(['message' => $data['message']]), $result['message']);
            }
            {
                AccessLog::accessLog(null, 'App\Jobs\Slack\SlackReminder\Project', 'NotifyChannel', 'handle', 'catch-block', 'User/Project not found');
            }
        }
        catch (Exception $e)
        {
            AccessLog::accessLog(null, 'App\Jobs\Slack\SlackReminder\Project', 'NotifyChannel', 'handle', 'catch-block', $e->getMessage());
        }
    }
}
