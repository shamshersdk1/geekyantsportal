<?php

namespace App\Jobs\Slack\SlackReminder;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\AccessLog;
use App\Models\Admin\SlackAccessLog;
use App\Services\SlackService\SlackMessageService\SlackMessageService;

class LoanReminder implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try
        {
            $result = SlackMessageService::sendMessageToUser($this->data['userId'], $this->data['message']);
            SlackAccessLog::saveData('Loan Reminder', 'User Id: '.$this->data['userId'], json_encode(['message' => $this->data['message']]), $result['message']);
        }
        catch (Exception $e)
        {
            AccessLog::accessLog(null, 'App\Jobs\Slack\SlackReminder', 'LoanReminder', 'handle', 'catch-block', $e->getMessage());
        }
    }
}
