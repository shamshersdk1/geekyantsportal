<?php

namespace App\Jobs\Slack\SlackReminder\Loan;

use App\Models\AccessLog;
use App\Models\Loan;
use App\Models\Admin\SlackAccessLog;
use App\Services\SlackService\SlackMessageService\SlackMessageService;
use Config;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use View;

class Notify implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $loan_id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($loan_id)
    {
        $this->loan_id = $loan_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try
        {
            $loan_id = $this->loan_id;
            
            $loan = Loan::find($loan_id);
            if($loan->status != 'completed')
            {
                $status = 'disbursed';
            }
            else
            {
                $status = 'completed';
            }
            $channel_id = config('app.loan_notification');

            if ($loan && $loan->user) {
                $user = $loan->user;
                if (substr($user->name, -1) == "s") {
                    $userName = $user->name . "'";
                } else {
                    $userName = $user->name . "'s";
                }
                $message = View::make('slack.reminder.loan.disbursed', ['user' => $userName, 'loan' => $loan, 'status' => $status])->render();
                $data = ["userId" => $user->reportingManager->id, "message" => $message];
                $result = SlackMessageService::sendMessageToChannel($channel_id, $data['message'], null, false);
                SlackAccessLog::saveData('Loan Reminder', 'User Id: ' . $data['userId'], json_encode(['message' => $data['message']]), $result['message']);

            }
        } catch (Exception $e) {
            AccessLog::accessLog(null, 'App\Jobs\Slack\SlackReminder', 'Notify', 'handle', 'catch-block', $e->getMessage());
        }
    }
}
