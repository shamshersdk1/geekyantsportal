<?php

namespace App\Jobs\Slack\SlackReminder\RequestApproval;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\User;
use App\Models\Admin\RequestApproval;
use App\Models\AccessLog;
use App\Models\Admin\SlackAccessLog;
use App\Services\SlackService\SlackMessageService\SlackMessageService;

use View;
use Exception;

class NotifyRequestor implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $requestId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($requestId)
    {
        $this->requestId = $requestId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try
        {
            $requestObj = RequestApproval::find($this->requestId);
            $url = config('app.geekyants_portal_url')."/admin/request-approval/".$requestObj->id;
            $user = $requestObj->requestedBy->name;
            if ( $user && $requestObj )
            {
                foreach( $requestObj->approvalUsers as $approvers )
                {
                    $responded = RequestApproval::isResponded($this->requestId, $approvers->user_id);
                    if ( !$responded )
                    {
                        $message = View::make('slack.reminder.request-approvals.message-requestor', ['user' => $user, 'description' => $requestObj->description, 'url' => $url ])->render();
                        $actions = [["name" => "approved", "text" => "Approve", "type" => "button", "value" => $requestObj->id, "style" => "primary"],
                                    ["name" => "rejected", "text" => "Reject", "type" => "button", "value" => $requestObj->id, "style" => "danger"],
                                    ["name" => "one_day", "text" => "Delay by one day", "type" => "button", "value" => $requestObj->id, "style" => "info"],
                                    ["name" => "one_week", "text" => "Delay by a week", "type" => "button", "value" => $requestObj->id, "style" => "warning"],
                                    ["text" => "View", "type" => "button", "url" => $url]                               
                                ];
                        $attachment = [["text" => "Select an action", "fallback" => "Goto the portal to approve/reject request", "attachment_type" => "default", "callback_id" => "request_action_requestor", "actions" => $actions ]];
                        $data = ["userId" => $approvers->user_id, "message" => $message, "attachment" => $attachment, "as_bot" => true];


                        $attachment = isset($data['attachment']) ? $data['attachment'] : null;
                        $result = SlackMessageService::sendMessageToUser($data['userId'], $data['message'], $attachment);
                        SlackAccessLog::saveData('Request Approval', 'User Id: '.$data['userId'], json_encode(['message' => $data['message']]), $result['message']);
                    }
                }
            }
        }
        catch (Exception $e)
        {
            AccessLog::accessLog(null, 'App\Jobs\Slack\SlackReminder\RequestApproval', 'NotifyRequestor', 'handle', 'catch-block', $e->getMessage());
        }
    }
}
