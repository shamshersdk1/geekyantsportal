<?php

namespace App\Jobs\Slack\SlackReminder\RequestApproval;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\User;
use App\Models\Admin\RequestApproval;
use App\Models\AccessLog;
use App\Models\Admin\SlackAccessLog;
use App\Services\SlackService\SlackMessageService\SlackMessageService;

use View;
use Exception;

class NotifyUser implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $requestId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($requestId)
    {
        $this->requestId = $requestId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try
        {
            $requestObj = RequestApproval::find($this->requestId);
            $url = config('app.geekyants_portal_url')."/admin/request-approval/".$requestObj->id;
            $status = '';
            switch($requestObj->status)
                {
                    case 'pending' : 
                        $status = 'raised';
                        break;
                    case 'approved' :
                        $status = 'approved';
                        break;
                    case 'rejected' :
                        $status = 'rejected';
                        break;
                }
            if ( $requestObj )
            {
                $message = View::make('slack.reminder.request-approvals.message-user', ['description' => $requestObj->description, 'status' => $status , 'url' => $url ])->render();
                $actions = [["text" => "View", "type" => "button", "url" => $url]];
                $attachment = [["text" => "Select an action", "fallback" => "Goto the portal to check the request", "attachment_type" => "default", "callback_id" => "request_action_user", "actions" => $actions ]];
                $data = ["userId" => $requestObj->user_id, "message" => $message, "attachment" => $attachment, "as_bot" => true];
                $attachment = isset($data['attachment']) ? $data['attachment'] : null;
                $result = SlackMessageService::sendMessageToUser($data['userId'], $data['message'], $attachment);
                SlackAccessLog::saveData('Request Approval', 'User Id: '.$data['userId'], json_encode(['message' => $data['message']]), $result['message']);
            }
        }
        catch (Exception $e)
        {
            AccessLog::accessLog(null, 'App\Jobs\Slack\SlackReminder\RequestApproval', 'NotifyUser', 'handle', 'catch-block', $e->getMessage());
        }
    }
}
