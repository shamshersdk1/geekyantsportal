<?php

namespace App\Jobs\Slack\SlackReminder\RM;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\AccessLog;
use App\Models\Admin\SlackAccessLog;
use App\Services\SlackService\SlackMessageService\SlackMessageService;
use App\Models\User;

use View;
use Exception;

class NotifyNewRM implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $userId;
    protected $rmId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId, $rmId)
    {
        $this->userId = $userId;
        $this->rmId = $rmId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try
        {
            $userObj = User::find($this->userId);
            $rmObj = User::find($this->rmId);
            if($userObj && $rmObj) 
            {
                $message = View::make('slack.reminder.rm-change.message-new-rm', ['manager' => $rmObj, 'user' => $userObj])->render();
                $data = ["userId" => $rmObj->id, "message" => $message];
                $attachment = isset($data['attachment']) ? $data['attachment'] : null;
                $result = SlackMessageService::sendMessageToUser($data['userId'], $data['message'], $attachment);
                SlackAccessLog::saveData('RM Assign Reminder', 'User Id: '.$data['userId'], json_encode(['message' => $data['message']]), $result['message']);  
            }
            {
                AccessLog::accessLog(null, 'App\Jobs\Slack\SlackReminder\RmAssign', 'NotifyNewRM', 'handle', 'catch-block', 'User/RM not found');
            }
        }
        catch (Exception $e)
        {
            AccessLog::accessLog(null, 'App\Jobs\Slack\SlackReminder\RmAssign', 'NotifyNewRM', 'handle', 'catch-block', $e->getMessage());
        }
    }
}
