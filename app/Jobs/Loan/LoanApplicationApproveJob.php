<?php

namespace App\Jobs\Loan;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mail\Loan\LoanApproveMail;
use Session;
use Exception;
use Log;

use App\Models\User;
use App\Models\Loan;
use App\Models\Admin\LoanRequest;

class LoanApplicationApproveJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $loan_request_id;
    protected $data_array;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($loanRequestId, $data_array)
    {
        $this->loan_request_id = $loanRequestId;
        $this->data_array = $data_array;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $loanRequestId = $this->loan_request_id;
            $data_array = $this->data_array;
            LoanRequest::processLoanApplication($loanRequestId, $data_array);
        } catch (Exception $e) {
            Log::info('LoanApplicationApproveJob Exception');
            Log::info($e->getMessage());
        }
    }
}
