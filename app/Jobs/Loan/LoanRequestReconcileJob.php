<?php

namespace App\Jobs\Loan;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mail\Loan\LoanReconcileMail;
use Session;
use Exception;
use Log;

use App\Models\User;
use App\Models\Admin\LoanRequest;
use App\Models\Admin\ProjectResource;

class LoanRequestReconcileJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $loan_request_id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($loan_request_id)
    {
        $this->loan_request_id=$loan_request_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Step 4 :On Management team apporval, loan application to be sent to HR team again to upload the agreement file
        // email  : loan-request@geekyants.com (template)
        // status : reconcile
        // Next request goes to HR
        try {
            $loan_request = LoanRequest::find($this->loan_request_id);
            $array=array('amount' => $loan_request->amount, 'date' => datetime_in_view($loan_request->created_at), 'user' => $loan_request->user->name, 'approver' => $loan_request->reconciledBy(), 'id' => $loan_request->id);
            \Mail::to(config('loan.request-email'))->send(new LoanReconcileMail($array));
        } catch (Exception $e) {
            Log::info($e->getMessage());
            Log::info('loan request');
            Session::flash('message', 'Mail not sent!');
            Session::flash('alert-class', 'alert-danger');
        }
    }
}
