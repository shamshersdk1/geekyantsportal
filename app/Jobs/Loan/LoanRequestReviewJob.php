<?php

namespace App\Jobs\Loan;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mail\Loan\LoanReviewMail;
use Session;
use Exception;
use Log;

use App\Models\User;
use App\Models\Admin\LoanRequest;
use App\Models\Admin\Role;

class LoanRequestReviewJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $loan_request_id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($loan_request_id)
    {
        $this->loan_request_id=$loan_request_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        // Step 3 : On HR team approval, loan application to be sent to Management
        // email  : to all user having role as management
        // status : review
        // Next request goes to Management
        try{
            $management_role = Role::with('users')->has('users')->where('code', 'management')->first();
            $emails_array = [];
            if($management_role) {
                $management_team = $management_role->users;
                foreach($management_team as $manager) {
                    $emails_array[] = $manager->email;
                }
            }
            if(count($emails_array) > 0) {
                $loan_request = LoanRequest::find($this->loan_request_id);
                $array=array('amount' => $loan_request->amount, 'date' => datetime_in_view($loan_request->created_at), 'user' => $loan_request->user->name, 'id' => $loan_request->id);
                \Mail::to($emails_array)->send(new LoanReviewMail($array));
            } else {
                \Log::info("No user found with management role");
            }
        } catch (Exception $e) {
            Log::info($e->getMessage());
            Log::info('loan request');
            Session::flash('message', 'Mail not sent!');
            Session::flash('alert-class', 'alert-danger');
        }
    }
}
