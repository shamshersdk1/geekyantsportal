<?php

namespace App\Jobs\Loan;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mail\Loan\LoanDisburseMail;
use Session;
use Exception;
use Log;

use App\Models\User;
use App\Models\Admin\LoanRequest;
use App\Models\Admin\ProjectResource;

class LoanRequestDisburseJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id =$id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Step4 :On Accounts team apporval loan application process completes and loan is processed 
        // email  : loan-request@geekyants.com (template)
        // status : reconcile
        // Next request goes to Management
        try {
            $loan = LoanRequest::find($this->id);
            $array=array('loan'=>$loan,'user'=>$loan->user->name,'approver'=>$loan->approvedBy());
            \Mail::to(config('loan.disburse-email'))->send(new LoanDisburseMail($array));
        } catch (Exception $e) {
            Log::info($e->getMessage());
            Log::info('loan request');
            Session::flash('message', 'Mail not sent!');
            Session::flash('alert-class', 'alert-danger');
        }
    }
}
