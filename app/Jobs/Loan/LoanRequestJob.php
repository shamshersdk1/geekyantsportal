<?php

namespace App\Jobs\Loan;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mail\Loan\LoanRequestMail;
use Session;
use Exception;
use Log;

use App\Models\User;
use App\Models\Loan;
use App\Models\Admin\ProjectResource;

class LoanRequestJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $loan_request;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($loan_request)
    {
        $this->loan_request=$loan_request;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Step 1 : Loan application to be sent to RM of the user
        // email  : to RM
        // current status : pending
        // Next request goes to HR

        try {
            $loan_request = $this->loan_request;
            $user = $loan_request->user;
            $email = config('loan.request-email');
            $admin = "Admin";
            if($user->reportingManager) {
                $email = $user->reportingManager->email;
                $admin = $user->reportingManager->name;
            }
            $array=array('admin' => $admin, 'amount' => $loan_request->amount, 'date' => datetime_in_view($loan_request->created_at), 'user' => $loan_request->user->name, 'id' => $loan_request->id);
            \Mail::to($email)->send(new LoanRequestMail($array));
        } catch (Exception $e) {
            Log::info($e->getMessage());
            Log::info('loan request');
            Session::flash('message', 'Mail not sent!');
            Session::flash('alert-class', 'alert-danger');
        }
    }
}