<?php

namespace App\Jobs\Loan;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mail\Loan\LoanSubmittedMail;
use Session;
use Exception;
use Log;

use App\Models\User;
use App\Models\Admin\LoanRequest;
use App\Models\Admin\ProjectResource;

class LoanRequestSubmittedJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $loanId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($loanId)
    {
        $this->loanId = $loanId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Step 2 : On RM approval, Loan application to be sent to HR team
        // email  : loan-request@geekyants.com (template)
        // status : review
        // Next request goes to Management

        try {

            $loan = LoanRequest::find($this->loanId);
            $array=array('loan'=>$loan,'user'=>$loan->user->name,'approver'=>$loan->submittedBy());
            \Mail::to(config('loan.request-email'))->send(new LoanSubmittedMail($array));
        } catch (Exception $e) {
            Log::info($e->getMessage());
            Log::info('loan request');
            Session::flash('message', 'Mail not sent!');
            Session::flash('alert-class', 'alert-danger');
        }
    }
}
