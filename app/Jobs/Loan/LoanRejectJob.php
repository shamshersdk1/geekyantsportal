<?php

namespace App\Jobs\Loan;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mail\Loan\LoanRejectMail;
use Session;
use Exception;
use Log;

use App\Models\User;
use App\Models\LoanRequest;

class LoanRejectJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id=$id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $loan = LoanRequest::find($this->id);
            $array=array('loan'=>$loan,'user'=>$loan->user->name);
            \Mail::to(config('loan.reject-email'))->send(new LoanRejectMail($array));
        } catch (Exception $e) {
            Session::flash('message', 'Mail not sent!');
            Session::flash('alert-class', 'alert-danger');
        }
    }
}
