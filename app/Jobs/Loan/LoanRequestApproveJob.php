<?php

namespace App\Jobs\Loan;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mail\Loan\LoanApproveMail;
use Session;
use Exception;
use Log;

use App\Models\User;
use App\Models\Admin\LoanRequest;

class LoanRequestApproveJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id=$id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Step4 :On HR team apporval (2nd time), loan application to be sent to Accounts team 
        // email  : disburse-request@geekyants.com (template)
        // status : reconcile
        // Next request goes to Finance Team
        try {
            $loan = LoanRequest::find($this->id);
            $array=array('loan'=>$loan,'user'=>$loan->user->name,'approver'=>$loan->approvedBy());
            \Mail::to(config('loan.approve-email'))->send(new LoanApproveMail($array));
        } catch (Exception $e) {
            Log::info($e->getMessage());
            Log::info('loan approve');
            Session::flash('message', 'Mail not sent!');
            Session::flash('alert-class', 'alert-danger');
        }
    }
}
