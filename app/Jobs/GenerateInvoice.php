<?php

namespace App\Jobs;

use App\Models\AccessLog;
use App\Models\Admin\BillingScheduleReminder;
use App\Models\Admin\SlackAccessLog;
use App\Services\SlackService\SlackMessageService\SlackMessageService;
use Config;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use View;

class GenerateInvoice implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $id;
    protected $channel_id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id, $channelId)
    {
        $this->id = $id;
        $this->channel_id = $channelId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try
        {
            $id = $this->id;
            $channelId = $this->channel_id;
            $reminderObj = BillingScheduleReminder::find($id);
            if ($reminderObj) {
                $url = Config::get('app.geekyants_portal_url') . "/admin/billing-schedule-reminder/" . $reminderObj->id;
                $message = View::make('slack.reminder.project-invoice.invoice-reminder', ['invoice' => $reminderObj])->render();
                $actions = [["text" => "View", "type" => "button", "url" => $url]];
                $attachment = [["text" => "", "fallback" => "Goto the portal to check the request", "attachment_type" => "default", "callback_id" => "generate-invoice-action", "actions" => $actions]];
                $result = SlackMessageService::sendMessageToChannel($channelId, $message, null, false, $attachment);
                SlackAccessLog::saveData('Generate Invoice', 'Channel Id: ' . $channel_id, json_encode(['message' => $message]), $result['message']);
            }
        } catch (Exception $e) {
            AccessLog::accessLog(null, 'App\Jobs\GenerateInvoice', 'GenerateInvoice', 'handle', 'catch-block', $e->getMessage());
        }
    }
}
