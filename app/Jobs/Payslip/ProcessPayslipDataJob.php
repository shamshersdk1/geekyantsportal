<?php
namespace App\Jobs\Payslip;

use Log;
use Exception;

use App\Jobs\Job;

use App\Models\AccessLog;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Services\PayslipService;

class ProcessPayslipDataJob extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $payslipObj;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($payslipObj)
    {
        $this->payslipObj = $payslipObj;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            \Log::info('ProcessPayslipDataJob ID'.$this->payslipObj->id);

            $paymentServiceObject = new PayslipService;
            $paymentServiceObject->processPayslipData($this->payslipObj->id);

            $this->delete();

        } catch(Exception $e) {

            Log::info('Pyalsip job here error'.$e->getMessage());
        }
    }
}