<?php
namespace App\Jobs\Payslip;

use Log;
use Exception;

use App\Jobs\Job;

use App\Models\AccessLog;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Services\PayslipService;

class ReadPayslipMonthDataJob extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $payslipMonth;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($payslipMonth)
    {
        $this->payslipMonth = $payslipMonth;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {

            Log::info('job here');
            $paymentServiceObject = new PayslipService;
            $paymentServiceObject->savePayslipData($this->payslipMonth);

            $this->delete();

        } catch(Exception $e) {

            Log::info('Pyalsip job here error'.$e->getMessage());
        }
    }
}