<?php

namespace App\Jobs\Timesheet;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Session;
use Exception;
use Log;

use App\Models\User;
use App\Models\TmpTimesheet;
use App\Models\Admin\ProjectSlackChannel;
use App\Services\TimesheetService;

class MoveTempRecordsToTimesheetTable implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $slackChannelId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($slackChannelId)
    {
        $this->slackChannelId=$slackChannelId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $channelId = $this->slackChannelId;
            $project = ProjectSlackChannel::where('slack_id',$channelId)->first()->project;
            $tmpTimesheetList = TmpTimesheet::where('channel_id', $channelId)->get();
            if ( !empty($project) && count($tmpTimesheetList) > 0 )
            {
                foreach($tmpTimesheetList as $tempObj)
                {
                    TimesheetService::updateTmpTimesheetProject($tempObj->id, $project->id);
                    TimesheetService::storeTimesheetDetails($tempObj->id);
                }
            }
            
        } catch (Exception $e) {
            
        }
    }
}
