<?php

namespace App\Jobs\BankTransfer;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Audited\BankTransfer\BankTransferUser;
use App\Models\Transaction;
use Exception;
use App\Models\AccessLog;
use App\Models\Month;

class ReverseBankTransferJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $bankUserId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($bankUserId)
    {
        $this->bankUserId = $bankUserId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            $currentDate = date("Y-m-d");
            $month = Month::getMonthByDate($currentDate);
            $bankTransferUserObj = BankTransferUser::find($this->bankUserId);
            if(!$bankTransferUserObj)
                throw new Exception("Bank Transfer not found");  
            $amount = $bankTransferUserObj->amount;
            $amount > 0 ? $type = "credit" : $type = "debit";
            $transactionObj = Transaction::create(['month_id' => $month->id,'financial_year_id' => $month->financial_year_id ,'user_id' => $bankTransferUserObj->user_id,'reference_id' => $bankTransferUserObj->id,'reference_type' => 'App\Models\Audited\BankTransfer\BankTransferUser','amount' => $amount,'type' => $type]);
            if(!$transactionObj->isValid())
                throw new Exception($transactionObj->getErrors());  
        }
        catch(Exception $e)
        {
            AccessLog::accessLog(null, 'App\Jobs\BankTransfer\ReverseBankTransferJob', 'BankTransferJob', 'handle', 'catch-block', $e->getMessage().$this->bankUserId);
        }
    }
}
