<?php

namespace App\Jobs\BankTransfer;

use App\Models\AccessLog;
use App\Models\Audited\BankTransfer\BankTransfer;
use App\Models\Audited\BankTransfer\BankTransferUser;
use App\Models\Month;
use App\Models\Transaction;
use App\Models\UserDetail;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class BankTransferJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $userId;
    protected $bankTransferId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($bankTransferId, $userId)
    {
        $this->userId = $userId;
        $this->bankTransferId = $bankTransferId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $currentDate = date("Y-m-d");
            $month = Month::getMonthByDate($currentDate);
            $amount = Transaction::where('user_id', $this->userId)->where('is_company_expense', 0)->sum('amount');

            $userDetailObj = UserDetail::where('user_id', $this->userId)->first();
            if ($userDetailObj) {
                $bankAccountNumber = $userDetailObj->bank_ac_no;
                if ($bankAccountNumber != null && $bankAccountNumber != 0) {
                    DB::beginTransaction();
                    $bankTransferUserObj = BankTransferUser::create(['bank_transfer_id' => $this->bankTransferId, 'user_id' => $this->userId, 'bank_acct_number' => $bankAccountNumber, 'amount' => $amount, 'transaction_amount' => $amount, 'mode_of_transfer' => 'NEFT']);
                    $amount = ($amount * -1);
                    $amount > 0 ? $type = "credit" : $type = "debit";
                    $transactionObj = Transaction::create(['month_id' => $month->id, 'financial_year_id' => $month->financial_year_id, 'user_id' => $this->userId, 'reference_id' => $bankTransferUserObj->id, 'reference_type' => 'App\Models\Audited\BankTransfer\BankTransferUser', 'amount' => $amount, 'type' => $type]);
                    if (!$transactionObj->isValid() || !$bankTransferUserObj->isValid()) {
                        throw new Exception($transactionObj->getErrors() . $bankTransferUserObj->getErrors());
                    }

                    DB::commit();
                }
            } else 
                throw new Exception("User Detail not found!");
        } catch (Exception $e) {
            DB::rollBack();
            AccessLog::accessLog(null, 'App\Jobs\BankTransfer\BankTransferJob', 'BankTransferJob', 'handle', 'catch-block', $e->getMessage() . 'user_id ' . $this->userId . '. month_id ' . $this->monthId);
        }
    }
}
