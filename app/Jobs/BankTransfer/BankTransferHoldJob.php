<?php

namespace App\Jobs\BankTransfer;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Month;
use App\Models\Audited\BankTransfer\BankTransfer;
use App\Models\Transaction;
use Illuminate\Support\Facades\DB;
use App\Models\Audited\BankTransfer\BankTransferHoldUser;
use Exception;
use App\Models\AccessLog;

class BankTransferHoldJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $userId;
    protected $bankTransferId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($bankTransferId,$userId)
    {
        $this->userId = $userId;
        $this->bankTransferId = $bankTransferId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            $currentDate = date("Y-m-d");
            $month = Month::getMonthByDate($currentDate);
            $amount = Transaction::where('user_id',$this->userId)->where('is_company_expense',0)->sum('amount');
            DB::beginTransaction();
                $bankTransferHoldUserObj = BankTransferHoldUser::create(['bank_transfer_id' => $this->bankTransferId,'user_id' => $this->userId, 'amount' => $amount]);
                $amount = ($amount * -1);
                $amount > 0 ? $type = "credit" : $type = "debit";
                $transactionObj = Transaction::create(['month_id' => $month->id,'financial_year_id' => $month->financial_year_id ,'user_id' => $this->userId,'reference_id' => $bankTransferHoldUserObj->id,'reference_type' => 'App\Models\Audited\BankTransfer\BankTransferHoldUser','amount' => $amount,'type' => $type]);
            if(!$transactionObj->isValid() || !$bankTransferHoldUserObj->isValid())
                throw new Exception($transactionObj->getErrors().$bankTransferHoldUserObj->getErrors());  
            DB::commit();
        }
        catch(Exception $e)
        {
            DB::rollBack(); 
            AccessLog::accessLog(null, 'App\Jobs\BankTransfer\BankTransferHoldJob', 'BankTransferHoldJob', 'handle', 'catch-block', $e->getMessage().'user_id '.$this->userId.'. month_id '.$this->monthId);
        }
    }
}
