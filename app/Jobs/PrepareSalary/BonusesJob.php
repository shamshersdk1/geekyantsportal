<?php

namespace App\Jobs\PrepareSalary;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Transaction;
use App\Models\Audited\Salary\PrepSalary;
use App\Models\Audited\Bonus\PrepBonus;
use App\Models\AccessLog;
use Exception;
use DB;
use App\Models\Admin\Bonus;

class BonusesJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $userId;
    protected $salaryId;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId,$salaryId)
    {
        $this->userId = $userId;
        $this->salaryId = $salaryId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
         try{
            $status = false;
            $errors = "";
            $prepSalaryObj = PrepSalary::find($this->salaryId);
            $bonusObjs = PrepBonus::where("prep_salary_id",$this->salaryId)->where("user_id",$this->userId)->get();
            DB::beginTransaction(); 
            if(count($bonusObjs)>0)
            {
                foreach($bonusObjs as $bonusObj)
                {
                    if($bonusObj->bonus->amount){
                        $bonusObj->bonus->amount > 0 ? $type = "credit" : $type = "debit";
                        $transactionObj = Transaction::create(['prep_salary_id' => $this->salaryId,'month_id' => $prepSalaryObj->month_id,'financial_year_id' => $prepSalaryObj->month->financial_year_id ,'user_id' => $this->userId,'reference_id' => $bonusObj->bonus->id,'reference_type' => 'App\Models\Admin\Bonus','amount' => $bonusObj->bonus->amount,'type' => $type]);
                        if(!$transactionObj->isValid())
                        {
                            $status = true;
                            $errors = $errors.$transactionObj->getErrors();
                        }            
                        $bonus = Bonus::find($bonusObj->bonus->id);
                        $bonus->transaction_id = $transactionObj->id;
                        $bonus->paid = 1;
                        $bonus->paid_at = date('Y-m-d H:i:s');
                        if(!$bonus->save())
                        {
                            $status = true;
                            $errors = $errors.$bonus->getErrors();
                        }
                    }
                }
            }
            if($status)
                throw new Exception($errors);
            DB::commit();
        }
        catch(Exception $e)
        {
            DB::rollBack(); 
            AccessLog::accessLog(null, 'App\Jobs\PrepareSalary\BonusesJob', 'Transaction', 'handle', 'catch-block', $e->getMessage().'user_id '.$this->userId.'. salary_id '.$this->salaryId);
        }

    }
}
