<?php

namespace App\Jobs\PrepareSalary;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Audited\Salary\PrepSalary;
use App\Models\Audited\Salary\PrepAdhocPayment;
use App\Models\Transaction;
use App\Models\AccessLog;
use Exception;
use DB;

class AdhocPaymentJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $userId;
    protected $salaryId;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId,$salaryId)
    {
        $this->userId = $userId;
        $this->salaryId = $salaryId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            $status = false;
            $errors = "";
            $prepSalaryObj = PrepSalary::find($this->salaryId);
            $prepAdhocPaymentObjs = PrepAdhocPayment::where("prep_salary_id",$this->salaryId)->where("user_id",$this->userId)->get();
            DB::beginTransaction();
            if(count($prepAdhocPaymentObjs)>0)
            {
                foreach($prepAdhocPaymentObjs as $prepAdhocPaymentObj)
                {
                    foreach($prepAdhocPaymentObj->components as $component)
                    {
                        if($component->value)
                        {
                            $component->value > 0 ? $type = "credit" : $type = "debit";
                            $transactionObj = Transaction::create(['prep_salary_id' => $this->salaryId,'month_id' => $prepSalaryObj->month_id,'financial_year_id' => $prepSalaryObj->month->financial_year_id ,'user_id' => $this->userId,'reference_id' => $component->adhoc_payment_id,'reference_type' => 'App\Models\Audited\AdhocPayments\AdhocPayment','amount' => $component->value,'type' => $type]);
                            if(!$transactionObj->isValid())
                            {
                                $status = true;
                                $errors = $errors.$transactionObj->getErrors();
                            }
                        }
                    }
                }
            }
            if($status)
                throw new Exception($errors);
            DB::commit();
        }
        catch(Exception $e)
        {
            DB::rollBack();
            AccessLog::accessLog(null, 'App\Jobs\PrepareSalary\AdhocPaymentJob', 'Transaction', 'handle', 'catch-block', $e->getMessage().'user_id '.$this->userId.'. salary_id '.$this->salaryId);
        }
    }
}
