<?php

namespace App\Jobs\PrepareSalary\Components;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Audited\VariablePay\VariablePay;
use App\Models\Audited\FixedBonus\FixedBonus;
use App\Models\Month;
use App\Models\AccessLog;
use Exception;
use App\Models\Audited\Salary\PrepSalary;
use App\Models\Audited\Salary\PrepAppraisalBonus;
use App\Models\Audited\Salary\PrepSalaryComponentType;
use Carbon\Carbon;
use App\Services\SalaryService;
use App\Models\Audited\Salary\PrepSalaryComponent;
use App\Models\Audited\Salary\PrepSalaryExecution;

class AppraisalBonusJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $userId;
    protected $salaryId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId,$salaryId)
    {
        $this->userId = $userId;
        $this->salaryId = $salaryId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $prepSalary = PrepSalary::find($this->salaryId);
        if($prepSalary && $prepSalary->status == 'completed')
        {
            return false;
        }
        $appraisalBonusComponentType = PrepSalaryComponentType::where('code','appraisal-bonus')->first();
        if(!$appraisalBonusComponentType)
            return false;
        $appraisalBonusComponent = $prepSalary->components->where('prep_salary_component_type_id',$appraisalBonusComponentType->id)->first();
        if(!$appraisalBonusComponent)
            return false;
        $prepSalaryExecutionObj = PrepSalaryExecution::where('prep_salary_id',$this->salaryId)->where('user_id',$this->userId)->where('component_id',$bonusComponent->id)->first();            
        if($prepSalaryExecutionObj)
        {
            if($prepSalaryExecutionObj->counter > 15)
            {
                return false;
            }
            $prepSalaryExecutionObj->status = "in_progress";
            $prepSalaryExecutionObj->counter++;
            $prepSalaryExecutionObj->save();
        }
        try{
            $status = true;
            $response = SalaryService::checkDependency($appraisalBonusComponent->id);
            if($response['status'])
            {
                foreach($response['dependentOn'] as $dependent)
                {
                    $prepSalaryComponentId = PrepSalaryComponent::where('prep_salary_id',$this->salaryId)->where('prep_salary_component_type_id',$dependent)->first()->id;
                    $executionStatus = SalaryService::checkExecutionStatus($prepSalaryComponentId,$this->userId,$this->salaryId);
                    if(!$executionStatus)
                    {
                        $status = false;    
                        break;
                    }
                }
                if(!$status)
                {
                    $job = (new AppraisalBonusJob($this->userId,$this->salaryId))->delay(Carbon::now()->addSeconds(30));
                    dispatch($job);
                }
            }
            if($status)
            {
                $data['status'] = true;
                $data['errors'] = "";
                $jobResponse['status'] = true;
                $prepSalary = PrepSalary::find($this->salaryId);
                $monthObj = Month::find($prepSalary->month_id);
                $start_date = $monthObj->getFirstDay();
                $end_date = $monthObj->getLastDay();
                $variablePays = VariablePay::where('user_id',$this->userId)->where('month_id',$monthObj->id)->get();
                PrepAppraisalBonus::where('prep_salary_id', $this->salaryId)->where('user_id',$this->userId)->get()->each(function ($prepBonus) {
                    $prepBonus->delete();
                });
                foreach($variablePays as $variablePay)
                {
                    foreach($variablePay->lastMonthPayComponents as $component)
                    {
                        $prepAppraisalBonus = PrepAppraisalBonus::create(['prep_salary_id' => $this->salaryId,'user_id' => $this->userId,'appraisal_bonus_id' => $component->appraisal_bonus_id,'value' => $component->value,'status' => 'open']);
                        if(!$prepAppraisalBonus->isValid())
                        {
                            $data['errors'] = $data['errors'].$prepAppraisalBonus->getErrors();
                            $data['status'] = false;
                        }
                    }
                }
                $fixedBonuses = FixedBonus::where('user_id',$this->userId)->where('month_id',$monthObj->id)->get();
                foreach($fixedBonuses as $fixedBonus)
                {
                    foreach($fixedBonus->monthlyDeductionComponents as $component)
                    {
                        $prepAppraisalBonus = PrepAppraisalBonus::create(['prep_salary_id' => $this->salaryId,'user_id' => $this->userId,'appraisal_bonus_id' => $component->appraisal_bonus_id,'value' => $component->value,'status' => 'open']);
                        if(!$prepAppraisalBonus->isValid())
                        {
                            $data['errors'] = $data['errors'].$prepAppraisalBonus->getErrors();
                            $data['status'] = false;
                        }
                    }
                }
                if(isset($data['status']) && $data['status']) {
                    $prepSalaryExecutionObj = PrepSalaryExecution::where('prep_salary_id',$this->salaryId)->where('user_id',$this->userId)->where('component_id',$appraisalBonusComponent->id)->first();
                    if($prepSalaryExecutionObj)
                    {
                        $prepSalaryExecutionObj->status = "completed";
                        $prepSalaryExecutionObj->save();
                    }
                } else {
                    throw new Exception($data['errors']);
                }
            }
        }
        catch(Exception $e)
        {
            $prepSalaryExecutionObj = PrepSalaryExecution::where('prep_salary_id',$this->salaryId)->where('user_id',$this->userId)->where('component_id',$appraisalBonusComponent->id)->first();
            if($prepSalaryExecutionObj)
            {
                $prepSalaryExecutionObj->status = "failed";
                $prepSalaryExecutionObj->save();
            }
            AccessLog::accessLog(null, 'App\Jobs\PrepareSalary\Components\AppraisalBonusJob', 'Generate', 'handle', 'catch-block', $e->getMessage().' user_id'.$this->userId.' salary_id '.$this->salaryId);
        }
    }
}
