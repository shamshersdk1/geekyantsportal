<?php

namespace App\Jobs\PrepareSalary\Components;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Audited\Salary\PrepSalary;
use App\Models\Month;
use App\Models\Admin\ItSavingMonth;
use App\Models\Audited\Salary\PrepItSaving;
use App\Models\Admin\ItSavingMonthOther;
use App\Models\AccessLog;
use Exception;
use App\Models\Audited\Salary\PrepItSavingComponent;
use App\Models\Audited\Salary\PrepSalaryComponentType;
use Carbon\Carbon;
use App\Services\SalaryService;
use App\Models\Audited\Salary\PrepSalaryComponent;
use App\Models\Audited\Salary\PrepSalaryExecution;
use App\Models\Admin\ItSavingOther;
use App\Services\SalaryService\ItSavingSalaryComponent;

class ItSavingSalaryJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $userId;
    protected $salaryId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId,$salaryId)
    {
        $this->userId = $userId;
        $this->salaryId = $salaryId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $prepSalary = PrepSalary::find($this->salaryId);
        if($prepSalary && $prepSalary->status == 'completed' )
            return false;
        $itSavingComponentType = PrepSalaryComponentType::where('code','it-saving')->first();
        if(!$itSavingComponentType)
            return false;
        $itSavingComponent = $prepSalary->components->where('prep_salary_component_type_id',$itSavingComponentType->id)->first();
        if(!$itSavingComponent)
            return false;
        $prepSalaryExecutionObj = PrepSalaryExecution::where('prep_salary_id',$this->salaryId)->where('user_id',$this->userId)->where('component_id',$loanComponent->id)->first();
        
        if($prepSalaryExecutionObj){
            if($prepSalaryExecutionObj->counter > 15)
            {
                return false;
            }
            $prepSalaryExecutionObj->status = "in_progress";
            $prepSalaryExecutionObj->counter++;
            $prepSalaryExecutionObj->save();
        }    
        try{
            $status = true;
            $response = SalaryService::checkDependency($itSavingComponent->id);
            if($response['status'])
            {
                foreach($response['dependentOn'] as $dependent)
                {
                    $prepSalaryComponentId = PrepSalaryComponent::where('prep_salary_id',$this->salaryId)->where('prep_salary_component_type_id',$dependent)->first()->id;

                    $executionStatus = SalaryService::checkExecutionStatus($prepSalaryComponentId, $this->userId, $this->salaryId);
                    if(!$executionStatus)
                    {
                        $status = false;
                        break; // do not dispatch multiple 
                    }
                }
                if(!$status)
                {
                    $job = (new ItSavingSalaryJob($this->userId,$this->salaryId))->delay(Carbon::now()->addSeconds(30));
                    dispatch($job);
                }
            }
            if($status)
            {
                $salaryObj = new ItSavingSalaryComponent;
                
                $salaryObj->setUserId($this->userId);
                $data = $salaryObj->generateUserData($this->salaryId);

                if(isset($data['status']) && $data['status']) {
                    $prepSalaryExecutionObj = PrepSalaryExecution::where('prep_salary_id',$this->salaryId)->where('user_id',$this->userId)->where('component_id',$itSavingComponent->id)->first();   
                    if($prepSalaryExecutionObj)
                    {
                        $prepSalaryExecutionObj->status = "completed";
                        $prepSalaryExecutionObj->save();
                    }
                } else {
                    throw new Exception($data['errors']);
                }
            }
        }
        catch(Exception $e)
        {
            $prepSalaryExecutionObj = PrepSalaryExecution::where('prep_salary_id',$this->salaryId)->where('user_id',$this->userId)->where('component_id',$itSavingComponent->id)->first();
            if($prepSalaryExecutionObj)
            {
                $prepSalaryExecutionObj->status = "failed";
                $prepSalaryExecutionObj->save();
            }
            AccessLog::accessLog(null, 'App\Jobs\PrepareSalary\Components\ItSavingSalaryJob', 'Generate', 'handle', 'catch-block', $e->getMessage().'. salary_id '.$this->salaryId);
        }
    }

    private function saveData($prep_it_saving_id, $key, $value)
    {
        $response['errors'] = "";
        $response['status'] = true;

        $prepItSavingComponentObj = new PrepItSavingComponent();
        $prepItSavingComponentObj->prep_it_saving_id = $prep_it_saving_id;
        $prepItSavingComponentObj->key = $key;
        $prepItSavingComponentObj->value = $value;
        if ($prepItSavingComponentObj->isValid()) {
            if (!$prepItSavingComponentObj->save()) {
                $response['errors'] = "Something went wrong";
                $response['status'] = false;
            }
        } else {
            $response['errors'] = $prepItSavingComponentObj->getErrors();
            $response['status'] = false;
        }
        return $response;
    }
}