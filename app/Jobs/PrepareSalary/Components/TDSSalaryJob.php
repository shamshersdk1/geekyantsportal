<?php

namespace App\Jobs\PrepareSalary\Components;

use App\Models\AccessLog;
use App\Models\Audited\Salary\PrepSalary;
use App\Models\Audited\Salary\PrepTds;
use App\Models\Audited\Salary\PrepTdsComponent;
use App\Services\SalaryService\TDSSalaryComponent;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Audited\Salary\PrepSalaryComponentType;
use App\Services\SalaryService;
use App\Models\Audited\Salary\PrepSalaryComponent;
use App\Models\Audited\Salary\PrepSalaryExecution;
use Carbon\Carbon;

class TDSSalaryJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $userId;
    protected $salaryId;
    protected $componentId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId, $salaryId, $componentId)
    {
        $this->userId = $userId;
        $this->salaryId = $salaryId;
        $this->componentId = $componentId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $prepSalary = PrepSalary::find($this->salaryId);
        if($prepSalary && $prepSalary->status == 'completed')
        {
            return false;
        }
        $grossComponentType = PrepSalaryComponentType::where('code','tds')->first();
        if(!$grossComponentType)
            return false;
        $grossComponent = $prepSalary->components->where('prep_salary_component_type_id',$grossComponentType->id)->first();
        if(!$grossComponent)
            return false;
        $prepSalaryExecutionObj = PrepSalaryExecution::where('prep_salary_id',$this->salaryId)->where('user_id',$this->userId)->where('component_id',$grossComponent->id)->first();     
        if($prepSalaryExecutionObj){
            if($prepSalaryExecutionObj->counter > 15)
            {
                return false;
            }
            $prepSalaryExecutionObj->status = "in_progress";
            $prepSalaryExecutionObj->counter++;
            $prepSalaryExecutionObj->save();
        }   
        
        try {
            $status = true;
            $response = SalaryService::checkDependency($grossComponent->id);
            if($response['status'])
            {
                foreach($response['dependentOn'] as $dependent)
                {
                    $prepSalaryComponentId = PrepSalaryComponent::where('prep_salary_id',$this->salaryId)->where('prep_salary_component_type_id',$dependent)->first()->id;
                    $executionStatus = SalaryService::checkExecutionStatus($prepSalaryComponentId,$this->userId,$this->salaryId);
                    if(!$executionStatus)
                    {
                        $status = false;
                        break; // do not dispatch multiple 
                    }
                }
                if(!$status)
                {
                    $job = (new TDSSalaryJob($this->userId,$this->salaryId,$this->componentId))->delay(Carbon::now()->addSeconds(30));
                    dispatch($job);
                }
            }
            if($status)
            {
                $tdsObj = new TDSSalaryComponent;
                $tdsObj->setComponent($this->componentId);
                $tdsObj->setUserId($this->userId);
                $tdsObj->setMonthYear($prepSalary->month->month, $prepSalary->month->year);
                $data = $tdsObj->generateUserData($this->salaryId);
                if(isset($data['status']) && $data['status']) {
                    $prepSalaryExecutionObj = PrepSalaryExecution::where('prep_salary_id',$this->salaryId)->where('user_id',$this->userId)->where('component_id',$grossComponent->id)->first();
                    if($prepSalaryExecutionObj)
                    {
                        $prepSalaryExecutionObj->status = "completed";
                        $prepSalaryExecutionObj->save();
                    }
                } else {
                    throw new Exception($data['errors']);
                }
            }
        } catch (Exception $e) {
            $prepSalaryExecutionObj = PrepSalaryExecution::where('prep_salary_id',$this->salaryId)->where('user_id',$this->userId)->where('component_id',$grossComponent->id)->first();
            if($prepSalaryExecutionObj)
            {
                $prepSalaryExecutionObj->status = "failed";
                $prepSalaryExecutionObj->save();
            }
            AccessLog::accessLog(null, 'App\Jobs\PrepareSalary\Components\TDSSalaryJob', 'Generate', 'handle', 'catch-block', $e->getMessage() . '. salary_id ' . $this->salaryId);
        }
    }
}
