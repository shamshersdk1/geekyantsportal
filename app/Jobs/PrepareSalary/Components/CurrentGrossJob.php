<?php

namespace App\Jobs\PrepareSalary\Components;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\AccessLog;
use Exception;
use App\Models\Appraisal\AppraisalComponentType;
use App\Models\Audited\Appraisal\PrepAppraisal;
use App\Models\Audited\Salary\PrepCurrentGross;
use App\Models\Audited\Salary\PrepCurrentGrossItem;
use App\Models\Audited\Salary\PrepSalaryComponent;
use App\Models\Audited\Salary\PrepUser;
use App\Models\Audited\Salary\PrepWorkingDay;
use App\Services\SalaryService\ApprisalSalaryComponent;
use App\Services\SalaryService\PrepSalaryComponentInterface;
use App\Services\SalaryService\VpfSalaryComponent;
use App\Services\SalaryService\WorkingDayComponent;
use App\Models\Audited\Salary\PrepSalary;
use App\Models\Audited\Salary\PrepSalaryComponentType;
use App\Services\SalaryService;
use App\Models\Audited\Salary\PrepSalaryExecution;
use Carbon\Carbon;
use App\Services\SalaryService\CurrentGrossComponent;

class CurrentGrossJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $userId;
    protected $salaryId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId,$salaryId)
    {
        $this->userId = $userId;
        $this->salaryId = $salaryId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $prepSalary = PrepSalary::find($this->salaryId);
        if($prepSalary && $prepSalary->status == 'completed')
        {
            return false;
        }
        $grossComponentType = PrepSalaryComponentType::where('code','current-gross')->first();
        if(!$grossComponentType)
            return false;
        $grossComponent = $prepSalary->components->where('prep_salary_component_type_id',$grossComponentType->id)->first();
        if(!$grossComponent)
            return false;
        $prepSalaryExecutionObj = PrepSalaryExecution::where('prep_salary_id',$this->salaryId)->where('user_id',$this->userId)->where('component_id',$grossComponent->id)->first();     
        if($prepSalaryExecutionObj){
            if($prepSalaryExecutionObj->counter > 15)
            {
                return false;
            }
            $prepSalaryExecutionObj->status = "in_progress";
            $prepSalaryExecutionObj->counter++;
            $prepSalaryExecutionObj->save();
        } 
        try{
            $status = true;
            $response = SalaryService::checkDependency($grossComponent->id);
            if($response['status'])
            {
                foreach($response['dependentOn'] as $dependent)
                {
                    $prepSalaryComponentId = PrepSalaryComponent::where('prep_salary_id',$this->salaryId)->where('prep_salary_component_type_id',$dependent)->first()->id;
                    $executionStatus = SalaryService::checkExecutionStatus($prepSalaryComponentId,$this->userId,$this->salaryId);
                    if(!$executionStatus)
                    {
                        $status = false;
                        break; // do not dispatch multiple 
                    }
                }
                if(!$status)
                {
                    $job = (new CurrentGrossJob($this->userId,$this->salaryId))->delay(Carbon::now()->addSeconds(30));
                    dispatch($job);
                }
            }
            if($status)
            {
                $salaryObj = new CurrentGrossComponent;
                $salaryObj->setUserId($this->userId);
                $data = $salaryObj->generateUserData($this->salaryId);
                if(isset($data['status']) && $data['status']) {
                    $prepSalaryExecutionObj = PrepSalaryExecution::where('prep_salary_id',$this->salaryId)->where('user_id',$this->userId)->where('component_id',$grossComponent->id)->first();
                    if($prepSalaryExecutionObj)
                    {
                        $prepSalaryExecutionObj->status = "completed";
                        $prepSalaryExecutionObj->save();
                    }
                } else {
                    throw new Exception($data['errors']);
                }
            }
        }
        catch(Exception $e)
        {
            $prepSalaryExecutionObj = PrepSalaryExecution::where('prep_salary_id',$this->salaryId)->where('user_id',$this->userId)->where('component_id',$grossComponent->id)->first();
            if($prepSalaryExecutionObj)
            {
                $prepSalaryExecutionObj->status = "failed";
                $prepSalaryExecutionObj->save();
            }
            AccessLog::accessLog(null, 'App\Jobs\PrepareSalary\Components\CurrentGrossJob', 'Generate', 'handle', 'catch-block', $e->getMessage().'. salary_id '.$this->salaryId);
        }
    }
}