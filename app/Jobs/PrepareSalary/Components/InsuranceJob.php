<?php

namespace App\Jobs\PrepareSalary\Components;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Exception;
use App\Models\AccessLog;
use App\Models\Audited\Salary\PrepSalary;
use App\Models\Audited\Salary\PrepSalaryComponentType;
use Carbon\Carbon;
use App\Services\SalaryService;
use App\Models\Audited\Salary\PrepSalaryComponent;
use App\Models\Audited\Salary\PrepSalaryExecution;
use App\Models\Admin\Insurance\InsuranceDeduction;
use App\Models\Audited\Salary\PrepInsurance;

class InsuranceJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $userId;
    protected $salaryId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId,$salaryId)
    {
        $this->userId = $userId;
        $this->salaryId = $salaryId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $prepSalary = PrepSalary::find($this->salaryId);
        if($prepSalary && $prepSalary->status == 'completed')
        {
            return false;
        }
        $insuranceComponentType = PrepSalaryComponentType::where('code','insurance')->first();
        if(!$insuranceComponentType)
            return false;
        $insuranceComponent = $prepSalary->components->where('prep_salary_component_type_id',$insuranceComponentType->id)->first();
        if(!$insuranceComponent)
            return false;
        $prepSalaryExecutionObj = PrepSalaryExecution::where('prep_salary_id',$this->salaryId)->where('user_id',$this->userId)->where('component_id',$insuranceComponent->id)->first();            
        if($prepSalaryExecutionObj)
        {
            if($prepSalaryExecutionObj->counter > 15)
            {
                return false;
            }
            $prepSalaryExecutionObj->status = "in_progress";
            $prepSalaryExecutionObj->counter++;
            $prepSalaryExecutionObj->save();
        }
        try{
            $status = true;
            $response = SalaryService::checkDependency($insuranceComponent->id);
            if($response['status'])
            {
                foreach($response['dependentOn'] as $dependent)
                {
                    $prepSalaryComponentId = PrepSalaryComponent::where('prep_salary_id',$this->salaryId)->where('prep_salary_component_type_id',$dependent)->first()->id;
                    $executionStatus = SalaryService::checkExecutionStatus($prepSalaryComponentId,$this->userId,$this->salaryId);
                    if($resp['status'])
                    {
                        $status = false;
                        break; // do not dispatch multiple 
                    }
                }
                if(!$status)
                {
                    $job = (new InsuranceJob($this->userId,$this->salaryId))->delay(Carbon::now()->addSeconds(30));
                    dispatch($job);
                }
            }
            if($status)
            {
                $data['status'] = true;
                $data['errors'] = "";
                $insuranceDeductionObjs  = InsuranceDeduction::where('user_id',$this->userId)->where('month_id',$prepSalary->month_id)->get();
                PrepInsurance::where('prep_salary_id', $this->salaryId)->where('user_id',$this->userId)->delete();
                foreach($insuranceDeductionObjs as $insuranceDeductionObj)
                {
                    $prepInsuranceDeduction = PrepInsurance::create(['prep_salary_id' => $this->salaryId ,'user_id' => $this->userId, 'insurance_deduction_id' =>  $insuranceDeductionObj->id,'value' => $insuranceDeductionObj->amount]);
                    if(!$prepInsuranceDeduction->isValid())
                    {
                        $data['errors'] = $data['errors'].$prepInsuranceDeduction->getErrors();
                        $data['status'] = true;
                    }
                    
                }
            }
            if(isset($data['status']) && $data['status']) {
                $prepSalaryExecutionObj = PrepSalaryExecution::where('prep_salary_id',$this->salaryId)->where('user_id',$this->userId)->where('component_id',$insuranceComponent->id)->first();
                if($prepSalaryExecutionObj)
                {
                    $prepSalaryExecutionObj->status = "completed";
                    $prepSalaryExecutionObj->save();
                }
            } else {
                throw new Exception($data['errors']);
            }
        }
        catch(Exception $e)
        {
            $prepSalaryExecutionObj = PrepSalaryExecution::where('prep_salary_id',$this->salaryId)->where('user_id',$this->userId)->where('component_id',$insuranceComponent->id)->first();
            if($prepSalaryExecutionObj)
            {
                $prepSalaryExecutionObj->status = "failed";
                $prepSalaryExecutionObj->save();
            }
            AccessLog::accessLog(null, 'App\Jobs\PrepareSalary\Components\InsuranceJob', 'Generate', 'handle', 'catch-block', $e->getMessage().' user_id'.$this->userId.' salary_id '.$this->salaryId);
        }
    }
}
