<?php

namespace App\Jobs\PrepareSalary\Components;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\AccessLog;
use Exception;
use App\Models\Audited\Salary\PrepSalary;
use App\Models\Audited\Salary\PrepAppraisalBonus;
use App\Models\Audited\Salary\PrepSalaryComponentType;
use Carbon\Carbon;
use App\Services\SalaryService;
use App\Models\Audited\Salary\PrepSalaryComponent;
use App\Models\Audited\Salary\PrepSalaryExecution;
use App\Models\LoanEmi;
use App\Models\Audited\Salary\PrepLoanEmi;


class LoanJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $userId;
    protected $salaryId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId,$salaryId)
    {
        $this->userId = $userId;
        $this->salaryId = $salaryId;
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $prepSalary = PrepSalary::find($this->salaryId);
        
        if($prepSalary && $prepSalary->status == 'completed' )
            return false;

        $loanComponentType = PrepSalaryComponentType::where('code','loan')->first();
        if(!$loanComponentType)
            return false;
        $loanComponent = $prepSalary->components->where('prep_salary_component_type_id',$loanComponentType->id)->first();
        if(!$loanComponent)
            return false;
        $prepSalaryExecutionObj = PrepSalaryExecution::where('prep_salary_id',$this->salaryId)->where('user_id',$this->userId)->where('component_id',$loanComponent->id)->first();
        
        if($prepSalaryExecutionObj){
            if($prepSalaryExecutionObj->counter > 15)
            {
                return false;
            }
            $prepSalaryExecutionObj->status = "in_progress";
            $prepSalaryExecutionObj->counter++;
            $prepSalaryExecutionObj->save();
        }    
        try{
            $status = true;
            $response = SalaryService::checkDependency($loanComponent->id);
            if($response['status'])
            {
                foreach($response['dependentOn'] as $dependent)
                {
                    $prepSalaryComponentId = PrepSalaryComponent::where('prep_salary_id',$this->salaryId)->where('prep_salary_component_type_id',$dependent)->first()->id;
                    $executionStatus = SalaryService::checkExecutionStatus($prepSalaryComponentId,$this->userId, $this->salaryId);
                    if(!$executionStatus)
                    {
                        $status = false;
                        break; // do not dispatch multiple 
                    }
                }
                if(!$status)
                {
                    $job = (new LoanJob($this->userId,$this->salaryId))->delay(Carbon::now()->addSeconds(30));
                    dispatch($job);
                }
            }
            if($status)
            {
                PrepLoanEmi::where('prep_salary_id', $this->salaryId)->where('user_id',$this->userId)->get()->each(function ($prepLoanEmi) {
                        $prepLoanEmi->delete();
                    });
                
                $loanObjs  = LoanEmi::where('user_id',$this->userId)->where('month_id',$prepSalary->month_id)->get();
                foreach($loanObjs as $loanObj)
                {
                    $prepLoanDeduction = PrepLoanEmi::create(['prep_salary_id' => $this->salaryId,'user_id' => $this->userId, 'loan_emi_id' =>  $loanObj->id,'emi_amount' => $loanObj->amount]);
                    if(!$prepLoanDeduction->isValid())
                    {
                        $data['errors'] = $response['errors'].$prepLoanDeduction->getErrors();
                        $data['status'] = true;
                    }
                }             
            }
  
            if($data['status'])
                throw new Exception($data['errors']);

            $prepSalaryExecutionObj = PrepSalaryExecution::where('prep_salary_id',$this->salaryId)->where('user_id',$this->userId)->where('component_id',$loanComponent->id)->first();
            if($prepSalaryExecutionObj)
            {
                $prepSalaryExecutionObj->status = "completed";
                $prepSalaryExecutionObj->save();
            }
        }
        catch(Exception $e)
        {
            $prepSalaryExecutionObj = PrepSalaryExecution::where('prep_salary_id',$this->salaryId)->where('user_id',$this->userId)->where('component_id',$loanComponent->id)->first();
            if($prepSalaryExecutionObj)
            {
                $prepSalaryExecutionObj->status = "failed";
                $prepSalaryExecutionObj->save();
            }
            AccessLog::accessLog(null, 'App\Jobs\PrepareSalary\Components\LoanJob', 'Generate', 'handle', 'catch-block', $e->getMessage().' user_id'.$this->userId.' salary_id '.$this->salaryId);
        }
    }
}
