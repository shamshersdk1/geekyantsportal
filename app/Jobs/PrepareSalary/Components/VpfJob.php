<?php

namespace App\Jobs\PrepareSalary\Components;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Exception;
use App\Models\AccessLog;
use App\Models\Audited\Salary\PrepSalary;
use App\Models\Audited\Salary\PrepFoodDeduction;
use App\Models\Admin\VpfDeduction;
use App\Models\Audited\Salary\PrepVpf;
use App\Models\Audited\Salary\PrepSalaryComponentType;
use Carbon\Carbon;
use App\Services\SalaryService;
use App\Models\Audited\Salary\PrepSalaryComponent;
use App\Models\Audited\Salary\PrepSalaryExecution;

class VpfJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $userId;
    protected $salaryId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId,$salaryId)
    {
        $this->userId = $userId;
        $this->salaryId = $salaryId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $prepSalary = PrepSalary::find($this->salaryId);

        $vpfComponentType = PrepSalaryComponentType::where('code','vpf')->first();

        if(!$vpfComponentType)
            return false;
        $vpfComponent = $prepSalary->components->where('prep_salary_component_type_id',$vpfComponentType->id)->first();
        if(!$vpfComponent)
            return false;
        $prepSalaryExecutionObj = PrepSalaryExecution::where('prep_salary_id',$this->salaryId)->where('user_id',$this->userId)->where('component_id',$grossComponent->id)->first();     
        if($prepSalaryExecutionObj){
            if($prepSalaryExecutionObj->counter > 15)
            {
                return false;
            }
            $prepSalaryExecutionObj->status = "in_progress";
            $prepSalaryExecutionObj->counter++;
            $prepSalaryExecutionObj->save();
        }    
        try{
            $status = true;
            $response = SalaryService::checkDependency($vpfComponent->id);
            if($response['status'])
            {
                foreach($response['dependentOn'] as $dependent)
                {
                    $prepSalaryComponentId = PrepSalaryComponent::where('prep_salary_id',$this->salaryId)->where('prep_salary_component_type_id',$dependent)->first()->id;
                    $executionStatus = SalaryService::checkExecutionStatus($dependent, $this->userId, $this->salaryId);
                    if(!$executionStatus)
                    {
                        $status = false;
                        break; // do not dispatch multiple 
                    }
                }
                if(!$status)
                {
                    $job = (new VpfJob($this->userId,$this->salaryId))->delay(Carbon::now()->addSeconds(30));
                    dispatch($job);
                }
            }
            if($status)
            {
                $vpfDeductionObj = VpfDeduction::where('user_id',$this->userId)->where('month_id',$prepSalary->month_id)->first();
                PrepVpf::where('prep_salary_id', $this->salaryId)->where('user_id',$this->userId)->delete();
                if($vpfDeductionObj)
                {
                    $prepVpf = PrepVpf::create(['prep_salary_id' => $this->salaryId ,'user_id' => $this->userId, 'vpf_deduction_id' =>  $vpfDeductionObj->id,'value' => $vpfDeductionObj->amount]);
                    if(!$prepVpf->isValid())
                    {
                        throw new Exception($prepVpf->getErrors());
                    }
                }
            }
            
            $prepSalaryExecutionObj = PrepSalaryExecution::where('prep_salary_id',$this->salaryId)->where('user_id',$this->userId)->where('component_id',$vpfComponent->id)->first();
            if($prepSalaryExecutionObj)
            {
                $prepSalaryExecutionObj->status = "completed";
                $prepSalaryExecutionObj->save();
            }
        }
        catch(Exception $e)
        {
            $prepSalaryExecutionObj = PrepSalaryExecution::where('prep_salary_id',$this->salaryId)->where('user_id',$this->userId)->where('component_id',$vpfComponent->id)->first();
            if($prepSalaryExecutionObj)
            {
                $prepSalaryExecutionObj->status = "failed";
                $prepSalaryExecutionObj->save();
            }
            AccessLog::accessLog(null, 'App\Jobs\PrepareSalary\Components\VpfJob', 'Generate', 'handle', 'catch-block', $e->getMessage().' user_id'.$this->userId.' salary_id '.$this->salaryId);
        }
    }
}
