<?php

namespace App\Jobs\PrepareSalary\Components;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Exception;
use App\Models\AccessLog;
use App\Models\Audited\Salary\PrepSalary;
use App\Models\Audited\Salary\PrepSalaryComponentType;
use Carbon\Carbon;
use App\Services\SalaryService;
use App\Models\Audited\Salary\PrepSalaryComponent;
use App\Models\Audited\Salary\PrepSalaryExecution;
use App\Models\Audited\AdhocPayments\AdhocPayment;
use App\Models\Audited\Salary\PrepAdhocPayment;
use App\Models\Audited\Salary\PrepAdhocPaymentComponent;

class AdhocPaymentJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $userId;
    protected $salaryId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId,$salaryId)
    {
        $this->userId = $userId;
        $this->salaryId = $salaryId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data['status'] = false;
        $data['errors'] = "";
        $prepSalary = PrepSalary::find($this->salaryId);
        if($prepSalary && $prepSalary->status == 'completed')
        {
            return false;
        }
        $adhocPaymentType = PrepSalaryComponentType::where('code','adhoc-payment')->first();
        if(!$adhocPaymentType)
            return false;
        $adhocPaymentComponent = $prepSalary->components->where('prep_salary_component_type_id',$adhocPaymentType->id)->first();
        if(!$adhocPaymentComponent)
            return false;
        $prepSalaryExecutionObj = PrepSalaryExecution::where('prep_salary_id',$this->salaryId)->where('user_id',$this->userId)->where('component_id',$adhocPaymentComponent->id)->first();            
        if($prepSalaryExecutionObj){
            if($prepSalaryExecutionObj->counter > 15)
            {
                return false;
            }
            $prepSalaryExecutionObj->status = "in_progress";
            $prepSalaryExecutionObj->counter++;
            $prepSalaryExecutionObj->save();
        }

        try{
            $status = true;
            $response = SalaryService::checkDependency($adhocPaymentComponent->id);
            if($response['status'])
            {
                foreach($response['dependentOn'] as $dependent)
                {
                    $prepSalaryComponentId = PrepSalaryComponent::where('prep_salary_id',$this->salaryId)->where('prep_salary_component_type_id',$dependent)->first()->id;
                    $executionStatus = SalaryService::checkExecutionStatus($prepSalaryComponentId,$this->userId,$this->salaryId);
                    if(!$executionStatus)
                    {
                        $status = false;
                        break;
                    }
                }
                if(!$status)
                {
                    $job = (new AdhocPaymentJob($this->userId,$this->salaryId))->delay(Carbon::now()->addSeconds(30));
                    dispatch($job);
                }
            }
            if($status)
            {
                $adhocPaymentObjs = AdhocPayment::where('user_id',$this->userId)->where('month_id',$prepSalary->month_id)->get();
                if(count($adhocPaymentObjs)>0)
                {
                    PrepAdhocPayment::where('prep_salary_id', $this->salaryId)->where('user_id',$this->userId)->get()->each(function ($adhocObj) {
                        $adhocObj->delete();
                    });
                    $prepAdhocPayment = PrepAdhocPayment::create(['prep_salary_id' => $this->salaryId ,'user_id' => $this->userId]);
                    if(!$prepAdhocPayment->isValid())
                    {
                        $data['errors'] = $data['errors'].$prepAdhocPayment->getErrors();
                        $data['status'] = true;
                    }
                    foreach($adhocPaymentObjs as $adhocPaymentObj)
                    {
                        $prepAdhocPaymentComponent = PrepAdhocPaymentComponent::create(['prep_adhoc_payment_id' => $prepAdhocPayment->id, 'key' => $adhocPaymentObj->component->key,'value' => $adhocPaymentObj->amount,'adhoc_payment_id' => $adhocPaymentObj->id]);
                        if(!$prepAdhocPaymentComponent->isValid())
                        {
                            $data['errors'] = $data['errors'].$prepAdhocPaymentComponent->getErrors();
                            $data['status'] = true;
                        }
                    }
                }
            }
            if($data['status'])
                throw new Exception($data['errors']);
            $prepSalaryExecutionObj = PrepSalaryExecution::where('prep_salary_id',$this->salaryId)->where('user_id',$this->userId)->where('component_id',$adhocPaymentComponent->id)->first();
            if($prepSalaryExecutionObj)
            {
                $prepSalaryExecutionObj->status = "completed";
                $prepSalaryExecutionObj->save();
            }
        }
        catch(Exception $e)
        {
            $prepSalaryExecutionObj = PrepSalaryExecution::where('prep_salary_id',$this->salaryId)->where('user_id',$this->userId)->where('component_id',$adhocPaymentComponent->id)->first();
            if($prepSalaryExecutionObj)
            {
                $prepSalaryExecutionObj->status = "failed";
                $prepSalaryExecutionObj->save();
            }
            AccessLog::accessLog(null, 'App\Jobs\PrepareSalary\Components\AdhocPaymentJob', 'Generate', 'handle', 'catch-block', $e->getMessage().' user_id'.$this->userId.' salary_id '.$this->salaryId);
        }
    }
}
