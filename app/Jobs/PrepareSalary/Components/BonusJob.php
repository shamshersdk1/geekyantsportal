<?php

namespace App\Jobs\PrepareSalary\Components;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Exception;
use App\Models\AccessLog;
use App\Models\Audited\Salary\PrepSalary;
use App\Models\Audited\Bonus\BonusConfirmed;
use App\Models\Audited\Bonus\PrepBonus;
use App\Models\Audited\Salary\PrepSalaryComponentType;
use Carbon\Carbon;
use App\Services\SalaryService;
use App\Models\Audited\Salary\PrepSalaryComponent;
use App\Models\Audited\Salary\PrepSalaryExecution;


class BonusJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $userId;
    protected $salaryId;
    /**P
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId,$salaryId)
    {
        $this->userId = $userId;
        $this->salaryId = $salaryId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $prepSalary = PrepSalary::find($this->salaryId);
        if($prepSalary && $prepSalary->status == 'completed')
        {
            return false;
        }
        $bonusComponentType = PrepSalaryComponentType::where('code','bonus')->first();
        if(!$bonusComponentType)
            return false;
        $bonusComponent = $prepSalary->components->where('prep_salary_component_type_id',$bonusComponentType->id)->first();
        if(!$bonusComponent)
            return false;
        $prepSalaryExecutionObj = PrepSalaryExecution::where('prep_salary_id',$this->salaryId)->where('user_id',$this->userId)->where('component_id',$bonusComponent->id)->first();            
        if($prepSalaryExecutionObj)
        {
            if($prepSalaryExecutionObj->counter > 15)
            {
                return false;
            }
            $prepSalaryExecutionObj->status = "in_progress";
            $prepSalaryExecutionObj->counter++;
            $prepSalaryExecutionObj->save();
        }
        try{
            $status = true;
            $response = SalaryService::checkDependency($bonusComponent->id);
            if($response['status'])
            {
                foreach($response['dependentOn'] as $dependent)
                {
                    $prepSalaryComponentId = PrepSalaryComponent::where('prep_salary_id',$this->salaryId)->where('prep_salary_component_type_id',$dependent)->first()->id;
                    $executionStatus = SalaryService::checkExecutionStatus($prepSalaryComponentId,$this->userId,$this->salaryId);
                    if(!$executionStatus)
                    {
                        $status = false;
                        break; // do not dispatch multiple 
                    }
                }
                if(!$status)
                {
                    $job = (new BonusJob($this->userId,$this->salaryId))->delay(Carbon::now()->addSeconds(30));
                    dispatch($job);
                }
            }
            if($status)
            {
                $data['status'] = true;
                $data['errors'] = "";
                PrepBonus::where('prep_salary_id', $this->salaryId)->where('user_id',$this->userId)->get()->each(function ($prepBonus) {
                    $prepBonus->delete();
                });
                $bonuses = BonusConfirmed::where('user_id',$this->userId)->get();
                foreach($bonuses as $bonus)
                {
                    $prepBonus = PrepBonus::create(['prep_salary_id' => $this->salaryId,'user_id' => $this->userId,'bonus_id' => $bonus->bonus_id]);
                    if(!$prepBonus->isValid())
                    {
                        $data['errors'] = $data['errors'].$data->getErrors();
                    }
                }
                if(isset($data['status']) && $data['status']) {
                    $prepSalaryExecutionObj = PrepSalaryExecution::where('prep_salary_id',$this->salaryId)->where('user_id',$this->userId)->where('component_id',$bonusComponent->id)->first();
                    if($prepSalaryExecutionObj)
                    {
                        $prepSalaryExecutionObj->status = "completed";
                        $prepSalaryExecutionObj->save();
                    }
                } else {
                    throw new Exception($data['errors']);
                }
            }
        }
        catch(Exception $e)
        {
            $prepSalaryExecutionObj = PrepSalaryExecution::where('prep_salary_id',$this->salaryId)->where('user_id',$this->userId)->where('component_id',$bonusComponent->id)->first();
            if($prepSalaryExecutionObj)
            {
                $prepSalaryExecutionObj->status = "failed";
                $prepSalaryExecutionObj->save();
            }
            AccessLog::accessLog(null, 'App\Jobs\PrepareSalary\Components\BonusJob', 'Generate', 'handle', 'catch-block', $e->getMessage().' user_id'.$this->userId.' salary_id '.$this->salaryId);
        }
    }
}
