<?php

namespace App\Jobs\PrepareSalary\Components;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Exception;
use App\Models\AccessLog;
use App\Models\Audited\Salary\PrepSalary;
use App\Models\Audited\FoodDeduction\FoodDeduction;
use App\Models\Audited\Salary\PrepFoodDeduction;
use App\Models\Audited\Salary\PrepSalaryComponentType;
use Carbon\Carbon;
use App\Services\SalaryService;
use App\Models\Audited\Salary\PrepSalaryComponent;
use App\Models\Audited\Salary\PrepSalaryExecution;

class FoodDeductionJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $userId;
    protected $salaryId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId,$salaryId)
    {
        $this->userId = $userId;
        $this->salaryId = $salaryId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $prepSalary = PrepSalary::find($this->salaryId);
        if($prepSalary && $prepSalary->status == 'completed')
        {
            return false;
        }
        $foodComponentType = PrepSalaryComponentType::where('code','food-deduction')->first();
        if(!$foodComponentType)
            return false;
        $foodComponent = $prepSalary->components->where('prep_salary_component_type_id',$foodComponentType->id)->first();
        if(!$foodComponent)
            return false;
        $prepSalaryExecutionObj = PrepSalaryExecution::where('prep_salary_id',$this->salaryId)->where('user_id',$this->userId)->where('component_id',$foodComponent->id)->first();            
        if($prepSalaryExecutionObj){
            if($prepSalaryExecutionObj->counter > 15)
            {
                return false;
            }
            $prepSalaryExecutionObj->status = "in_progress";
            $prepSalaryExecutionObj->counter++;
            $prepSalaryExecutionObj->save();
        }
       // \Log::info("Food TimeStamp:".time());
        try{
            $status = true;
            $response = SalaryService::checkDependency($foodComponentType->id);
            if($response['status'])
            {
                foreach($response['dependentOn'] as $dependent)
                {
                    $prepSalaryComponentId = PrepSalaryComponent::where('prep_salary_id',$this->salaryId)->where('prep_salary_component_type_id',$dependent)->first()->id;
                    $executionStatus = SalaryService::checkExecutionStatus($prepSalaryComponentId,$this->userId,$this->salaryId);
                    if(!$executionStatus)
                    {
                        $status = false;
                        break; // do not dispatch multiple 
                    }
                }
                if(!$status)
                {
                    $job = (new FoodDeductionjob($this->userId,$this->salaryId))->delay(Carbon::now()->addSeconds(30));
                    dispatch($job);
                }
            }
            if($status)
            {
                $data['status'] = true;
                $data['errors'] = "";
                $foodDeductionObj = FoodDeduction::where('month_id',$prepSalary->month_id)->where('user_id',$this->userId)->first();
                PrepFoodDeduction::where('prep_salary_id', $this->salaryId)->where('user_id',$this->userId)->delete();
                if($foodDeductionObj)
                {
                    $prepFoodDeduction = PrepFoodDeduction::create(['prep_salary_id' => $this->salaryId,'user_id' => $this->userId,'food_deduction_id' => $foodDeductionObj->id, 'value' => $foodDeductionObj->amount]);
                    if(!$prepFoodDeduction->isValid())
                    {
                        $data['status'] = false;
                        $data['errors'] = $prepFoodDeduction->getErrors();
                    }
                }
            }

            if(isset($data['status']) && $data['status']) {
                $prepSalaryExecutionObj = PrepSalaryExecution::where('prep_salary_id',$this->salaryId)->where('user_id',$this->userId)->where('component_id',$foodComponent->id)->first();
                if($prepSalaryExecutionObj)
                {
                    $prepSalaryExecutionObj->status = "completed";
                    $prepSalaryExecutionObj->save();
                }
            } else {
                throw new Exception($data['errors']);
            }
        }
        catch(Exception $e)
        {
            $prepSalaryExecutionObj = PrepSalaryExecution::where('prep_salary_id',$this->salaryId)->where('user_id',$this->userId)->where('component_id',$foodComponent->id)->first();
            if($prepSalaryExecutionObj)
            {
                $prepSalaryExecutionObj->status = "failed";
                $prepSalaryExecutionObj->save();
            }
            AccessLog::accessLog(null, 'App\Jobs\PrepareSalary\Components\FoodDeductionJob', 'Generate', 'handle', 'catch-block', $e->getMessage().' user_id'.$this->userId.' salary_id '.$this->salaryId);
        }
    }
}
