<?php

namespace App\Jobs\PrepareSalary\Regenerate;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Month;
use App\Models\Admin\ItSaving;
use App\Models\Admin\ItSavingMonth;
use App\Models\Admin\ItSavingMonthOther;
use App\Models\AccessLog;
use Exception;

class ItSavingMonthJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $monthId;
    protected $itSavingId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($monthId,$itSavingId)
    {
        $this->monthId = $monthId;
        $this->itSavingId = $itSavingId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            $saving = ItSaving::find($this->itSavingId);
            $monthObj = Month::find($this->monthId);
            if($saving->user->is_active)
            {
                $saving->pf = $saving->getMonthTotalPf($this->monthId);
                $monthlySavingId = ItSavingMonth::createMonthlyRecord($monthObj->id, $saving);
                ItSavingMonthOther::createMonthlyRecord($saving,$monthlySavingId->id);
            }
        }
        catch(Exception $e)
        {
            AccessLog::accessLog(null, 'App\Jobs\PrepareSalary\Regenerate\ItSavingMonthJob', 'Generate', 'handle', 'catch-block', $e->getMessage());
        }
    }
}
