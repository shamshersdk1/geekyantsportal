<?php

namespace App\Jobs\PrepareSalary;

use App\Models\AccessLog;
use App\Models\Appraisal\AppraisalComponentType;
use App\Models\Audited\Salary\PrepCurrentGross;
use App\Models\Audited\Salary\PrepCurrentGrossItem;
use App\Models\Audited\Salary\PrepSalary;
use App\Models\Transaction;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use DB;

class AppraisalComponentJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $userId;
    protected $salaryId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId, $salaryId)
    {
        $this->userId = $userId;
        $this->salaryId = $salaryId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $status = false;
            $errors = "";
            $prepSalaryObj = PrepSalary::find($this->salaryId);
            $grossObj = PrepCurrentGross::where("prep_salary_id", $this->salaryId)->where("user_id", $this->userId)->first();
            $appraisalTypes = AppraisalComponentType::where("is_computed", "0")->get();
            DB::beginTransaction(); 
            foreach ($appraisalTypes as $appraisalType) {
                $currentGrossObj = $grossObj->itemByKey($appraisalType->code);
                if ($currentGrossObj->value != 0) {
                    $currentGrossObj->value > 0 ? $type = "credit" : $type = "debit";
                        $transactionObj = Transaction::create(['prep_salary_id' => $this->salaryId,'month_id' => $prepSalaryObj->month_id, 'financial_year_id' => $prepSalaryObj->month->financial_year_id, 'user_id' => $this->userId, 'reference_id' => $appraisalType->id, 'reference_type' => 'App\Models\Appraisal\AppraisalComponentType', 'amount' => $currentGrossObj->value, 'type' => $type, 'is_company_expense' => $appraisalType->is_company_expense]);
                        if (!$transactionObj->isValid()) {
                            $status = true;
                            $errors = $errors.$transactionObj->getErrors();
                        }
                }
            }
            if($status)
                throw new Exception($errors);
            DB::commit();
        } 
        catch (Exception $e) {
            DB::rollBack(); 
            AccessLog::accessLog(null, 'App\Jobs\PrepareSalary\AppraisalComponentJob', 'Transaction', 'handle', 'catch-block', $e->getMessage() . 'user_id ' . $this->userId . '. salary_id ' . $this->salaryId);
        }
    }
}
