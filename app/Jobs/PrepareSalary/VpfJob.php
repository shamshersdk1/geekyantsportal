<?php

namespace App\Jobs\PrepareSalary;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Audited\Salary\PrepVpf;
use App\Models\Audited\Salary\PrepSalary;
use App\Models\Transaction;
use App\Models\AccessLog;
use Exception;
use DB;

class VpfJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $userId;
    protected $salaryId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId,$salaryId)
    {
        $this->userId = $userId;
        $this->salaryId = $salaryId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            $status = false;
            $errors = "";
            $prepSalaryObj = PrepSalary::find($this->salaryId);
            $prepVpfObj=PrepVpf::where('prep_salary_id',$this->salaryId)->where('user_id',$this->userId)->first();
            DB::beginTransaction(); 
            if($prepVpfObj->value ? $prepVpfObj->value : null){
                $prepVpfObj->value > 0 ? $type = "credit" : $type = "debit";
                $transactionObj = Transaction::create(['prep_salary_id' => $this->salaryId,'month_id' => $prepSalaryObj->month_id,'financial_year_id' => $prepSalaryObj->month->financial_year_id ,'user_id' => $this->userId,'reference_id' => $prepVpfObj->vpf_deduction_id,'reference_type' => 'App\Models\Admin\VpfDeduction','amount' => $prepVpfObj->value,'type' => $type]);
                if(!$transactionObj->isValid())
                {
                    $status = true;
                    $errors = $errors.$transactionObj->getErrors();
                }
            }
            if($status)
                throw new Exception($errors);
            DB::commit();
        }catch(Exception $e)
        {
            DB::rollBack(); 
            AccessLog::accessLog(null, 'App\Jobs\PrepareSalary\VpfJob', 'Transaction', 'handle', 'catch-block', $e->getMessage().'user_id '.$this->userId.'. salary_id '.$this->salaryId);
        }

    }
}
