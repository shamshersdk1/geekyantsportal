<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mail\LeaveRequestEmail;
use App\Mail\LeavePMRequestEmail;
use App\Mail\LeaveNotificationEmail;
use App\Models\Admin\Leave;
use App\Services\UserService;
use App\Services\LeaveService;
use Session;
use Exception;
use Log;

class SendEmailJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $leave_id;
    protected $type; // 'request' & 'notify'
    protected $to; // 'TL' or 'PM' or 'User'
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($leave_id, $type, $to)
    {
        $this->leave_id=$leave_id;
        $this->type=$type;
        $this->to=$to;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $leave_id = $this->leave_id;
            $type = $this->type;
            $to = $this->to;

            if ( $type == 'request' )
            {
                if ( $to == 'TL' )
                {
                    $leave = Leave::find($leave_id);
                    $year = date('Y-01-01', strtotime($leave->start_date));
                    $previousLeaves = LeaveService::previousLeaves($leave->user_id, $year);
                    $sick = LeaveService::sickLeaves($leave->user_id, $year);
                    $earned = LeaveService::earnedLeaves($leave->user_id, $year);
                    $teamLeads = LeaveService::getTeamLeads($leave->id);
                    if ( !empty($teamLeads) )
                    {
                        foreach ($teamLeads as $lead) 
                        {
                            $array=array('leave'=>$leave,'admin'=>$lead->name, 'previousLeaves' => $previousLeaves, 'sick' => $sick, 'earned' => $earned,'email' => $lead->email);
                            \Mail::to($lead->email)->send(new LeavePMRequestEmail($array));
                            
                        }
                    }
                
                }
                elseif ( $to == 'RM' )
                {
                    $leave = Leave::find($leave_id);
                    $year = date('Y-01-01', strtotime($leave->start_date));
                    $previousLeaves = LeaveService::previousLeaves($leave->user_id, $year);
                    $sick = LeaveService::sickLeaves($leave->user_id, $year);
                    $earned = LeaveService::earnedLeaves($leave->user_id, $year);
                    $reporting_manager = UserService::getReportingManager($leave->user_id);

                    if ( !empty($reporting_manager['name']) && !empty($reporting_manager['email']) ) 
                    {
                        $array=array('leave'=>$leave,'admin'=>$reporting_manager['name'], 'previousLeaves' => $previousLeaves, 'sick' => $sick, 'earned' => $earned,'email' => $reporting_manager['email'] );
                        \Mail::to($reporting_manager['email'])->cc(config('app.leave-request-email'))->send(new LeaveRequestEmail($array));
                    }
                    else 
                    {
                        $array=array('leave'=>$leave,'admin'=>"Admin",  'previousLeaves' => $previousLeaves, 'sick' => $sick, 'earned' => $earned);
                        \Mail::to(config('app.leave-request-email'))->send(new LeaveRequestEmail($array));
                    }
                }
                else
                {
                    \Log::info('Type not in TL or RM');
                }
            }
            else
            {
                if ( $to == 'RM' )
                {
                    $leave = Leave::find($leave_id);
                    $reporting_manager = UserService::getReportingManager($leave->user_id);

                    if ( !empty($reporting_manager['name']) && !empty($reporting_manager['email']) ) 
                    {
                        $array=array('leave'=>$leave,'user'=>$reporting_manager['name'],'email' => $reporting_manager['email']);
                        \Mail::to($reporting_manager['email'])->cc(config('app.leave-request-email'))->send(new LeaveNotificationEmail($array));
                    }
                    else 
                    {
                        $array=array('leave'=>$leave,'user'=>'Admin');
                        \Mail::to(config('app.leave-request-email'))->send(new LeaveNotificationEmail($array));
                    }
                }
                elseif ( $to == 'TL' )
                {
                    $leave = Leave::find($leave_id);
                    $projectManagers = LeaveService::getProjectsManagers($leave->id);
                    
                    if( !empty($projectManagers)  )
                    {
                        foreach ( $projectManagers as $projectManager )
                        {
                            $array=array('leave'=>$leave,'user'=>$projectManager['name'],'email'=>$projectManager['email']);
                            \Mail::to($projectManager['email'])->send(new LeaveNotificationEmail($array));
                        }
                    }
                }
                else // USER
                {
                    $leave = Leave::find($leave_id);
                    $userName = !empty($leave->user->name) ? $leave->user->name : '';
                    $userEmail = !empty($leave->user->email) ? $leave->user->email : '';
                    if ( $userName != '' && $userEmail != '' )
                    {
                        $array=array('leave'=>$leave,'user'=>$userName,'email' => $userEmail);
                        \Mail::to($userEmail)->send(new LeaveNotificationEmail($array));
                    }
                }
            }

        } catch (Exception $e) {
            Log::info($e->getMessage());
            Session::flash('message', 'Mail not sent!');
            Session::flash('alert-class', 'alert-danger');
        }
    }
}
