<?php

namespace App\Jobs\Leave;

use App\Services\NewLeaveService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class LeaveDeductionMonthJob implements ShouldQueue
{
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    protected $leaveDeductionId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($leaveDeductionId)
    {
        $this->leaveDeductionId = $leaveDeductionId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Log::info('in handle' . $this->leaveDeductionId);

        NewLeaveService::deductLeave($this->leaveDeductionId);

    }
}
