<?php
namespace App\Jobs\Leave;

use App\Models\Admin\UserLeaveBalance;
use App\Models\Admin\UserLeaveLog;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UserLeavePLAvailableJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $user_log_id;

    public function __construct($UserLogId)
    {
        //
        $this->user_log_id = $UserLogId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $userLeave = UserLeaveLog::find($this->user_log_id);
        $userLeaveBalance = UserLeaveBalance::find($userLeave->user_leave_balance_id);
        if ($userLeaveBalance) {
            if ($userLeave->leaveType->code == 'paid') {
                $userLeaveBalance->paid_leave_balance = $userLeaveBalance->paid_leave_balance + $userLeave->days;
            }
            if ($userLeave->leaveType->code == 'sick') {
                $userLeaveBalance->sick_leave_balance = $userLeaveBalance->sick_leave_balance + $userLeave->days;
            }
        }
        $userLeaveBalance->save();
    }
}
