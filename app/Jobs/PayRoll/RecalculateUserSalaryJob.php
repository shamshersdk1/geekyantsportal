<?php
namespace App\Jobs\PayRoll;

use App\Models\Admin\Finance\SalaryUserData;
use App\Services\Payroll\PayrollService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class RecalculateUserSalaryJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $user_slary_data_id;

    public function __construct($userSalaryDataId)
    {
        //
        $this->user_slary_data_id = $userSalaryDataId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        PayrollService::calculateUserSalary($this->user_slary_data_id);
        $salaryUser = SalaryUserData::find($this->user_slary_data_id);
        if ($salaryUser) {
            $salaryUser->status = "completed";
            $salaryUser->save();
        }
    }
}
