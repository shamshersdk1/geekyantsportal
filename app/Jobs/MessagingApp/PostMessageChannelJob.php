<?php
namespace App\Jobs\MessagingApp;

use App\Models\AccessLog;
use App\Services\MessagingAppService\PostMessageService\PostUserMessageService;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class PostMessageChannelJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $channelId;
    protected $message;
    protected $data;

    public function __construct($channelId, $message, $data = null)
    {
        //
        $this->channelId = $channelId;
        $this->message = $message;
        $this->data = $data;
        $this->asUser = false;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try
        {
            PostUserMessageService::postMessageToChannel($this->channelId, $this->message, null, false, $this->data);

        } catch (Exception $e) {
            AccessLog::accessLog(null, 'PostMessageChannelJob', 'NotifyAdmin', 'handle', 'catch-block', $e->getMessage());
        }
    }
}
