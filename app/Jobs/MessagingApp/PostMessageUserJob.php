<?php

namespace App\Jobs\MessagingApp;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use GuzzleHttp\Client;
use App\Services\MessagingAppService\PostMessageService\PostUserMessageService;
use App\Models\AccessLog;
use Config;
use Exception;
use Log;

class PostMessageUserJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $userId;
    protected $message;
    protected $attachment;

    public function __construct($userId, $message, $attachment)
    {
        //
        $this->userId = $userId;
        $this->message = $message;
        $this->attachment = $attachment;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
       try {
        PostUserMessageService::postMessageToUser($this->userId, $this->message, $this->attachment);
       }
       catch (Exception $e)
        {
            AccessLog::accessLog(null, 'PostUserMessageService', 'postMessageToUser', 'handle', 'catch-block', $e->getMessage());
        }
    }
}
