<?php

namespace App\Jobs;

use Session;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\{SerializesModels, InteractsWithQueue};
use App\Services\Payroll\AppraisalService;
use App\Services\Payroll\ITSavingService;
use OwenIt\Auditing\Models\Audit;

use App\Models\AccessLog;
use App\Mail\AuditEmail;

class AuditMail implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    
    private $fromDate;
    private $toDate;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($fromDate,$toDate)
    {
        
        $this->fromDate = $fromDate;
        $this->toDate = $toDate;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        try {
            $data = [];
            $data['date'] = $this->toDate;
            $data['audits'] = Audit::orderBy('created_at')
                                ->whereBetween('created_at', [$this->fromDate.' 00:00:00', $this->toDate.' 06:00:00'])
                                ->get();
            $data['appraisalAudits'] = AppraisalService::getAudits();
            $data['itSavingAudits'] = ITSavingService::getAudits();
            if((count($data['audits'])>0) || (count($data['appraisalAudits'])>0) || (count($data['itSavingAudits'])>0))
            {
                \Mail::to(
                    config('app.audit-email')
                )->
                send(
                    new AuditEmail($data)
                );
                $this->delete();
                AccessLog::accessLog(null, 'App\Jobs', 'AuditMail', 'handle', 'try-block', $data);
            }
        } catch (Exception $e) {

            AccessLog::accessLog(null, 'App\Jobs', 'AuditMail', 'handle', 'catch-block', $e->getMessage());
            Session::flash('message', 'Mail not sent!');
            Session::flash('alert-class', 'alert-danger');
        }
    }
}
