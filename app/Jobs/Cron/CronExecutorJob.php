<?php

namespace App\Jobs\Cron;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Services\CronService\CronConstants;
use App\Models\Admin\Cron;
use App\Models\AccessLog;

class CronExecutorJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $cronId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($cronId)
    {
        $this->cronId = $cronId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $today = date('Y-m-d');
        $cronObj = Cron::find($this->cronId);
        $className = $cronObj->job_handler;
        if ( !empty($cronObj->occurrence_counter) )
        {
            $lastCount = !empty($cronObj->executed_counter) ? $cronObj->executed_counter : 0;
            $cronObj->executed_counter = $lastCount + 1;
        }
        $cronObj->last_processed_at = $today;
        $cronObj->save();
        $class = CronConstants::INTERACTIVE_COMPONENT_NAMESPACE."\\".$className;
        $result = $class::handle($cronObj->reference_id, $cronObj->reference_type, $cronObj->reference_data_json);
    }
}
