<?php

use Illuminate\Database\Seeder;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users_role')->insert([
           'role' => 'Super Admin',
           'description' => 'This User have the full access',
           ]);
        DB::table('users_role')->insert([
           'role' => 'HR Manager',
           'description' => 'superuser',
           ]);
        DB::table('users_role')->insert([
           'role' => 'Project Manager',
           'description' => 'superuser',
           ]);        
    }
}
