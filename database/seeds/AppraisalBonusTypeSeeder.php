<?php

use Illuminate\Database\Seeder;

class AppraisalBonusTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'code' => 'confirmation-bonus',
                'description' => 'Confirmation Bonus',
            ],
            [
                'code' => 'variable-bonus',
                'description' => 'Monthly Variable Bonus',
            ],
            [
                'code' => 'year-end',
                'description' => 'Annual Loyalty Bonus',
            ],
            [
                'code' => 'annual-variable',
                'description' => 'Annual Variable Or Annual Performance Bonus',
            ],
            [
                'code' => 'open-source-contribution-em',
                'description' => 'Open Source Contribution Bonus (EM)',
            ],
            [
                'code' => 'tech-talk-video-em',
                'description' => 'Tech Talk Video Bonus (EM)',
            ],
            [
                'code' => 'tech-articles-em',
                'description' => 'Tech Articles Bonus (EM)',
            ],
            [
                'code' => 'study-jam-em',
                'description' => 'Study Jam Bonus (EM)',
            ],
            [
                'code' => 'case-study-em',
                'description' => 'Case Study Bonus (EM)',
            ],
        ];
        DB::table('appraisal_bonus_type')->delete();
        foreach ($data as $bonusType) {
            DB::table('appraisal_bonus_type')->insert($bonusType);
        }
    }
}
