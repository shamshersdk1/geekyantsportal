<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		// Model::unguard();

		// $this->call('UserTableSeeder');
		$this->call(EnterRoleSeeder::class);
		$this->call(EnterUserRolesSeeder::class);
		$this->call(PrepSalaryComponentSeeder::class);

		// $this->call('SystemSettingTableSeeder');
		$this->call(AppraisalComponentTypeSeeder::class);
		$this->call(AppraisalBonusTypeSeeder::class);


	}

}
