<?php

use Illuminate\Database\Seeder;

class AppraisalComponentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Basic',
                'code' => 'basic',
                'is_computed' => 0,
            ],
            [
                'name' => 'HRA',
                'code' => 'hra',
                'is_computed' => 0,
            ],
            [
                'name' => 'Car Allowance',
                'code' => 'car-allowance',
                'is_computed' => 0,
            ],
            [
                'name' => 'Food Allowance',
                'code' => 'food-allowance',
                'is_computed' => 0,
            ],
            [
                'name' => 'P.F Employee',
                'code' => 'pf-employee',
                'is_computed' => 0,
            ],
            [
                'name' => 'P.F Employeer',
                'code' => 'pf-employeer',
                'is_computed' => 0,
            ],
            [
                'name' => 'P.F Other',
                'code' => 'pf-other',
                'is_computed' => 0,
            ],
            [
                'name' => 'E.S.I',
                'code' => 'esi',
                'is_computed' => 0,
            ],
            [
                'name' => 'E.S.I Employeer',
                'code' => 'esi-employeer',
                'is_computed' => 0,
            ],
            [
                'name' => 'Professional Tax',
                'code' => 'professional-tax',
                'is_computed' => 0,
            ],
            [
                'name' => 'LTA',
                'code' => 'lta',
                'is_computed' => 0,
            ],
            [
                'name' => 'Stipend',
                'code' => 'stipend',
                'is_computed' => 0,
            ],
            [
                'name' => 'Special Allowance',
                'code' => 'special-allowance',
                'is_computed' => 0,
            ],
            [
                'name' => 'TDS',
                'code' => 'tds',
                'is_computed' => 1,
            ],
        ];
        DB::table('appraisal_component_types')->delete();
        DB::table('appraisal_component_types')->insert($data);
    }
}
