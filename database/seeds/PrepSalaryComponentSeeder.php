<?php

use Illuminate\Database\Seeder;

class PrepSalaryComponentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $prepComponentData=[
            [
                'name'=>'User',
                'code'=>'user',
                'service_class'=>'App\Services\SalaryService\UserSalaryComponent',
            ],
            [
             'name'=>'Appraisal',
             'code'=>'appraisal',
             'service_class'=>'App\Services\SalaryService\ApprisalSalaryComponent',
            ],
            [
             'name'=>'Working Day',
             'code'=>'working-day',
             'service_class'=>'App\Services\SalaryService\WorkingDayComponent',
            ],
            [
             'name'=>'Bonus',
             'code'=>'bonus',
             'service_class'=>'App\Services\SalaryService\BonusSalaryComponent',
            ],
            [
                'name' => 'Appraisal Bonus',
                'code' => 'appraisal-bonus',
                'service_class' => 'App\Services\SalaryService\AppraisalBonusSalaryComponent'
            ],
            [
             'name'=>'Loan',
             'code'=>'loan',
             'service_class'=>'App\Services\SalaryService\LoanSalaryComponent',
            ],
            [
             'name'=>'It Saving',
             'code'=>'it-saving',
             'service_class'=>'App\Services\SalaryService\ItSavingSalaryComponent',
            ],
            [
                'name'=>'VPF',
                'code'=>'vpf',
                'service_class'=>'App\Services\SalaryService\VpfSalaryComponent',
            ],
            [
                'name'=>'Food Deduction',
                'code'=>'food-deduction',
                'service_class'=>'App\Services\SalaryService\FoodDeductionSalaryComponent',
            ],
            [
                'name'=>'Insurance',
                'code'=>'insurance',
                'service_class'=>'App\Services\SalaryService\InsuranceSalaryComponent',
            ],
            [
                'name'=>'Adhoc Payment',
                'code'=>'adhoc-payment',
                'service_class'=>'App\Services\SalaryService\AdhocPaymentSalaryComponent',
            ],
            [
                'name'=>'Gross Salary Till Now',
                'code'=>'gross-paid-till-now',
                'service_class'=>'App\Services\SalaryService\GrossPaidComponent',
            ],
            [
                'name'=>'Current Gross Salary',
                'code'=>'current-gross',
                'service_class'=>'App\Services\SalaryService\CurrentGrossComponent',
            ],
            [
                'name'=>'Gross To Be Paid',
                'code'=>'gross-to-be-paid',
                'service_class'=>'App\Services\SalaryService\GrossToBePaidComponent',
            ],
            [
                'name'=>'TDSSalaryComponent',
                'code'=>'tds',
                'service_class'=>'App\Services\SalaryService\TDSSalaryComponent',
            ],
            
            ];
            DB::table('prep_salary_component_types')->delete();
            DB::table('prep_salary_component_types')->insert($prepComponentData);


    }
}
