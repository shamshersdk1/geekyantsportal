<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('roles')->insert([
           'name' => 'cms',
           'code' => 'cms',
           ]);
        DB::table('roles')->insert([
           'name' => 'View',
           'code' => 'view',
           ]);
        DB::table('roles')->insert([
           'name' => 'Asset Management',
           'code' => 'asset-management',
           ]);
        DB::table('roles')->insert([
            'name' => 'Reporting Mangager leaves',
            'code' => 'reporting-manager-leaves',
            ]);
        DB::table('roles')->insert([
            'name' => 'Timesheet',
            'code' => 'timesheet',
            ]);
        DB::table('roles')->insert([
            'name' => 'Leave Section',
            'code' => 'leave-section',
            ]);
        DB::table('roles')->insert([
            'name' => 'Loans',
            'code' => 'loans',
            ]);
        DB::table('roles')->insert([
            'name' => 'Upload paylsip',
            'code' => 'upload-payslip',
            ]);
        DB::table('roles')->insert([
            'name' => 'Payroll',
            'code' => 'payroll',
            ]);
        DB::table('roles')->insert([
            'name' => 'Bonus',
            'code' => 'bonus',
            ]);
        DB::table('roles')->insert([
            'name' => 'Create Payslip',
            'code' => 'create-payslip',
            ]);
        DB::table('roles')->insert([
            'name' => 'Project',
            'code' => 'project',
            ]);
        DB::table('roles')->insert([
            'name' => 'Resource Report',
            'code' => 'resource-report',
            ]);
        DB::table('roles')->insert([
            'name' => 'Users',
            'code' => 'users',
            ]);
        DB::table('roles')->insert([
            'name' => 'Calendar',
            'code' => 'calendar',
            ]);
        DB::table('roles')->insert([
            'name' => 'Role',
            'code' => 'role',
            ]);
        DB::table('roles')->insert([
            'name' => 'Dashboard Event',
            'code' => 'dashboard-event',
            ]);
        DB::table('roles')->insert([
            'name' => 'Technology',
            'code' => 'technology',
            ]);
        DB::table('roles')->insert([
            'name' => 'Logs',
            'code' => 'logs',
            ]);
        DB::table('roles')->insert([
            'name' => 'Skill',
            'code' => 'skill',
            ]);
        DB::table('roles')->insert([
            'name' => 'Company',
            'code' => 'company',
            ]);
        DB::table('roles')->insert([
            'name' => 'Leads',
            'code' => 'leads',
            ]);
        DB::table('roles')->insert([
            'name' => 'Payroll Rules CSV',
            'code' => 'payroll-rules-csv',
            ]);
        DB::table('roles')->insert([
            'name' => 'Checklist',
            'code' => 'checklist',
            ]);
        DB::table('roles')->insert([
            'name' => 'Slack',
            'code' => 'slack',
            ]);
    }
}
