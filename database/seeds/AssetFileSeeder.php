<?php

use Illuminate\Database\Seeder;
use App\Models\Admin\{File, FileContent};

class AssetFileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = File::where([['path', '<>', ''], ['path', '<>', 'empty_path'], ['reference_type', 'App\Models\Admin\Asset']])->withTrashed()->get();

        foreach ($datas as $data)
        {
            $file = file_get_contents(public_path() . $data->path);
            $type = pathinfo($data->path)["extension"];

            $data->update(['type' => $type]);

            $fileContent = new FileContent();
            $fileContent->insert([
                'file_id' => $data->id,
                'content' => $file
            ]);
        }
    }
}
