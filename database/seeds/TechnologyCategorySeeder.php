<?php

use Illuminate\Database\Seeder;

class TechnologyCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('technology_categories')->insert([
           'name' => 'Languages',
           ]);
        DB::table('technology_categories')->insert([
           'name' => 'Frameworks',
           ]);
        DB::table('technology_categories')->insert([
           'name' => 'Libraries/APIs',
           ]);
        DB::table('technology_categories')->insert([
           'name' => 'Tools',
           ]);
    }
}
