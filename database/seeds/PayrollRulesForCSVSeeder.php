<?php

use Illuminate\Database\Seeder;

class PayrollRulesForCSVSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payroll_rule_for_csvs')->insert([
           'key' => 'basic',
           'value' => 'Basic',
        ]);
        DB::table('payroll_rule_for_csvs')->insert([
           'key' => 'hra',
           'value' => 'HRA',
        ]);
        DB::table('payroll_rule_for_csvs')->insert([
           'key' => 'conveyance_allowance',
           'value' => 'Conveyance Allowance',
        ]);
        DB::table('payroll_rule_for_csvs')->insert([
           'key' => 'car_allowance',
           'value' => 'Car Allowance',
        ]);
        DB::table('payroll_rule_for_csvs')->insert([
           'key' => 'medical_allowance',
           'value' => 'Medical Allowance',
        ]);
        DB::table('payroll_rule_for_csvs')->insert([
           'key' => 'food_allowance',
           'value' => 'Food Allowance',
        ]);
        DB::table('payroll_rule_for_csvs')->insert([
           'key' => 'special_allowance',
           'value' => 'Special Allowance',
        ]);
        DB::table('payroll_rule_for_csvs')->insert([
           'key' => 'gross_salary',
           'value' => 'Gross Salary',
        ]);
        DB::table('payroll_rule_for_csvs')->insert([
           'key' => 'pf',
           'value' => 'P.F. @12% Employee',
        ]);
        DB::table('payroll_rule_for_csvs')->insert([
           'key' => 'esi',
           'value' => 'E.S.I. @1.75% Employee',
        ]);
        DB::table('payroll_rule_for_csvs')->insert([
           'key' => 'vpf',
           'value' => 'VPF',
        ]);
        DB::table('payroll_rule_for_csvs')->insert([
           'key' => 'professional_tax',
           'value' => 'Professional Tax',
        ]);
        DB::table('payroll_rule_for_csvs')->insert([
           'key' => 'food_deduction',
           'value' => 'Food Deduction',
        ]);
        DB::table('payroll_rule_for_csvs')->insert([
           'key' => 'medical_insurance',
           'value' => 'Medical Insurance',
        ]);
        DB::table('payroll_rule_for_csvs')->insert([
           'key' => 'tds',
           'value' => 'T.D.S.',
        ]);
        DB::table('payroll_rule_for_csvs')->insert([
           'key' => 'loan',
           'value' => 'Loan',
        ]);
        DB::table('payroll_rule_for_csvs')->insert([
           'key' => 'total_deduction_for_month',
           'value' => 'TOTAL DEDUCTION FOR MONTH',
        ]);
        DB::table('payroll_rule_for_csvs')->insert([
           'key' => 'net_salary_for_the_month',
           'value' => 'NET SALARY FOR THE MONTH',
        ]);
        DB::table('payroll_rule_for_csvs')->insert([
           'key' => 'arrear_salary',
           'value' => 'Arrear Salary',
        ]);
        DB::table('payroll_rule_for_csvs')->insert([
           'key' => 'annual_bonus',
           'value' => 'Annual Bonus',
        ]);
        DB::table('payroll_rule_for_csvs')->insert([
           'key' => 'qtr_variable_bonus',
           'value' => 'Qtr Variable Bonus',
        ]);
        DB::table('payroll_rule_for_csvs')->insert([
           'key' => 'performance_bonus',
           'value' => 'Performance Bonus',
        ]);
        DB::table('payroll_rule_for_csvs')->insert([
           'key' => 'non_cash_incentive',
           'value' => 'Non-Cash Incentive',
        ]);
        DB::table('payroll_rule_for_csvs')->insert([
           'key' => 'gross_earnings',
           'value' => 'Gross Earnings',
        ]);
        DB::table('payroll_rule_for_csvs')->insert([
           'key' => 'amount_payable',
           'value' => 'Amount Payable',
        ]);
    }
}
