<?php

use Illuminate\Database\Seeder;

class CredentialCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('credential_categories')->insert([
           'id' => 1,
           'name' => 'Mobile App',
           'json_data' => '[{"key": "login_url","title": "Login URL","value": ""},{"key": "user_name","title": "User Name","value": ""},{"key": "password","title": "Password","value": ""}]',
           ]);       
    }
}
