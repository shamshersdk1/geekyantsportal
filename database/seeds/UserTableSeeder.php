<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
           'name' => 'Varun',
           'email' => 'varun@geekyants.com',
           'password' => null,
           'created_at' => date('Y-m-d H:i:s'),
           'updated_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('users')->insert([
           'name' => 'Pratik',
           'email' => 'pratik@geekyants.com',
           'password' => null,
           'created_at' => date('Y-m-d H:i:s'),
           'updated_at' => date('Y-m-d H:i:s')
        ]);DB::table('users')->insert([
           'name' => 'Rahul',
           'email' => 'rahul@geekyants.com',
           'password' => null,
           'created_at' => date('Y-m-d H:i:s'),
           'updated_at' => date('Y-m-d H:i:s')
        ]);DB::table('users')->insert([
           'name' => 'Safi',
           'email' => 'safiullah@geekyants.com',
           'password' => null,
           'created_at' => date('Y-m-d H:i:s'),
           'updated_at' => date('Y-m-d H:i:s')
        ]);DB::table('users')->insert([
           'name' => 'Manav',
           'email' => 'manav@geekyants.com',
           'password' => null,
           'created_at' => date('Y-m-d H:i:s'),
           'updated_at' => date('Y-m-d H:i:s')
        ]);DB::table('users')->insert([
           'name' => 'Ila',
           'email' => 'ila@geekyants.com',
           'password' => null,
           'created_at' => date('Y-m-d H:i:s'),
           'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
