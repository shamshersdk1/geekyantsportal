<?php

use Illuminate\Database\Seeder;

class EnterWorkingDayGroup extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('working_day_groups')->insert([
           'name' => 'Working on saturday',
           'monday' => true,
           'tuesday' => true,
           'wednesday' => true,
           'thursday' => true,
           'friday' => false,
           'saturday' => true,
           'sunday' => false,
           ]);
    }
}
