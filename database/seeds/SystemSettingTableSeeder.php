<?php

use Illuminate\Database\Seeder;

class SystemSettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('system_settings')->insert([
           'key' => 'max_sick_leave',
           'value' => '6',
           ]);
        DB::table('system_settings')->insert([
           'key' => 'max_paid_leave',
           'value' => '16',
           ]);
    }
}
