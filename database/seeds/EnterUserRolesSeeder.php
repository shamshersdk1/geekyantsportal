<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\UserRole;
use App\Models\Admin\Role;

class EnterUserRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $userRole = Role::where('code', 'user')->first();
        if (!empty($userRole->id)) {
            $userRoleId = $userRole->id;
            $now = date("Y-m-d H:i:s");
            User::where('is_active', 1)->chunk(10, function ($users) use ($userRoleId) {
                foreach ($users as $user) {
                    if(UserRole::where('user_id', $user->id)->where('role_id', $userRoleId)->count()) {
                        continue;
                    }
                    $userRole = new UserRole();
                    $userRole->user_id = $user->id;
                    $userRole->role_id = $userRoleId;
                    $userRole->save();
                }
            });
        }
    }
}
