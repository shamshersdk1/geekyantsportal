<?php

use Illuminate\Database\Seeder;

class EnterRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('roles')->insert([
            'name' => 'User',
            'code' => 'user',
            'description' => 'Default role, it should be assigned to every user.'

           ]);
        DB::table('roles')->insert([
            'name' => 'Admin',
            'code' => 'admin',
            'description' => 'Super Admin. This user can access all routes.'
           ]);
        DB::table('roles')->insert([
            'name' => 'Reporting Manager',
            'code' => 'reporting-manager',
            'description' => 'Reporting Manager role. Can view leaves, bonuses, project and resource report routes.'
           ]);
        DB::table('roles')->insert([
            'name' => 'Team Lead',
            'code' => 'team-lead',
            'description' => 'Role for all Team Leads. Access to RM leaves.'
           ]);
        DB::table('roles')->insert([
            'name' => 'Human Resources',
            'code' => 'human-resources',
            'description' => 'Role for HR. Access to user CRUD, calendar and dashboard events.'
        ]);
        DB::table('roles')->insert([
            'name' => 'Office Admin',
            'code' => 'office-admin',
            'description' => 'Role for office admin. Access to asset management.'
        ]);
        DB::table('roles')->insert([
            'name' => 'Finance/Account',
            'code' => 'account',
            'description' => 'Role for finance team. Access to loan, payslip and bonus.'
        ]);
        DB::table('roles')->insert([
            'name' => 'CMS',
            'code' => 'cms-manager',
            'description' => ''
        ]);
        DB::table('roles')->insert([
            'name' => 'System Admin',
            'code' => 'system-admin',
            'description' => 'Role for syetem admin. Access to enter and modify credentials for servers and app configurtaion and have access to asset management'
        ]);
        DB::table('roles')->insert([
            'name' => 'Management',
            'code' => 'management',
            'description' => 'Management Role'
        ]);
        DB::table('roles')->insert([
            'name' => 'Event Manager',
            'code' => 'event-manager',
            'description' => 'Tech Event manager'
        ]);
        DB::table('roles')->insert([
            'name' => 'Sales Executive / Manager',
            'code' => 'sales-manager',
            'description' => 'Sales executive / manager'
        ]);
        DB::table('roles')->insert([
            'name' => 'Code Lead',
            'code' => 'code-lead',
            'description' => 'Code Lead'
        ]);
        DB::table('roles')->insert([
            'name' => 'Delivery Lead',
            'code' => 'delivery-lead',
            'description' => 'Delivery Lead'
        ]);

    }
}
