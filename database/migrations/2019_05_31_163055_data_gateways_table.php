<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DataGatewaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('gateways')->insert([
           ['id'=>1,'name'=>'BLR1-OFFICE', 'desc'=>'Old office','token'=>'BLR1#*%NN!','primary_ip'=>'106.51.67.220'],
           ['id'=>2,'name'=>'BLR2-OFFICE', 'desc'=>'New office','token'=>'BLR2#*%WW(','primary_ip'=>'106.51.80.211']
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('users')->where('id', '=', 1)->delete();
        DB::table('users')->where('id', '=', 2)->delete();
    }
}
