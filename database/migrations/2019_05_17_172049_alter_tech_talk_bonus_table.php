<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTechTalkBonusTable extends Migration
{
 /**
  * Run the migrations.
  *
  * @return void
  */
 public function up()
 {
  Schema::table('tech_talk_bonuses', function (Blueprint $table) {
   $table->dropColumn('status');

  });

 }

 /**
  * Reverse the migrations.
  *
  * @return void
  */
 public function down()
 {

 }
}
