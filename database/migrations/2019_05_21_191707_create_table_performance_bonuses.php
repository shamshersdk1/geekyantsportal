<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablePerformanceBonuses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('performance_bonuses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('month_id');
            $table->date('date');
            $table->decimal('amount')->default(0);
            $table->enum('status', ['pending', 'approved', 'cancelled']);
            $table->string('comment');
            $table->integer('created_by');
            $table->integer('reviewed_by');
            $table->date('reviewed_at');

            $table->softDeletes();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('performance_bonuses');
    }
}
