<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableLoansModifyStatusCloumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('loans', function (Blueprint $table) {
            $table->dropColumn(['status','approved_by']);
        });
        
        Schema::table('loans', function (Blueprint $table) {
            $table->integer('approved_by')->nullable()->after('emi');
            $table->enum('status', ['pending', 'approved', 'rejected', 'cancelled', 'disbursed', 'completed'])->after('emi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('loans', function (Blueprint $table) {
          $table->dropColumn(['status']);
        });
    }
}
