<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAdhocPaymentComponents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adhoc_payment_components', function (Blueprint $table) {
            $table->increments('id');
            $table->string('key')->index();
            $table->string('description');
            $table->enum('type', ['credit', 'debit']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('adhoc_payment_components');
    }
}
