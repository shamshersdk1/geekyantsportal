<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTimesheetDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('timesheet_details', function (Blueprint $table) {

            $table->dropColumn('start_time');
            $table->dropColumn('end_time');
            $table->float('approved_duration', 4, 2 )->default(null)->after('duration');
          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('timesheet_details', function (Blueprint $table) {

            $table->dropColumn('approved_duration');
            $table->time('start_time')->after('timesheet_id');
            $table->time('end_time')->after('timesheet_id');       

        });
    }
}



