<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RefactorUsersCtcTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('users_ctc', function (Blueprint $table) {
            $table->dropColumn(['in_hand','variable_qtr','year_end']);
        });
        Schema::table('users_ctc', function (Blueprint $table) {
            $table->double('in_hand', 30, 2);
            $table->double('variable_qtr', 30, 2);
            $table->double('year_end', 30, 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('users_ctc', function (Blueprint $table) {
            $table->dropColumn(['in_hand','variable_qtr','year_end']);
        });
    }
}
