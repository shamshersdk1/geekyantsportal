<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnAdhocPaymentIdToPrepAdhocPaymentComponents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('prep_adhoc_payment_components', function(Blueprint $table)
        {
            $table->integer('adhoc_payment_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('prep_adhoc_payment_components', function(Blueprint $table) {
            $table->dropColumn('adhoc_payment_id');
        });
    }
}
