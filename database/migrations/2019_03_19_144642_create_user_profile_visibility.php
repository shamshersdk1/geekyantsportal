<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserProfileVisibility extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profile_visibility', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->boolean('basic');
            $table->boolean('about');
            $table->boolean('most_amazing_thing');
            $table->boolean('tech_stack');
            $table->boolean('training');
            $table->boolean('social_info');
            $table->boolean('projects');
            $table->boolean('workshop');
            $table->boolean('achivements');
            $table->boolean('interest');
            $table->boolean('education');
            $table->boolean('mini_project');
            $table->boolean('r_and_d');
            $table->timestamps();
            $table->softDeletes();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profile_visibility');
    }
}
