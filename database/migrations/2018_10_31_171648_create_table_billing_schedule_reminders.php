<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableBillingScheduleReminders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billing_schedule_reminders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cron_id');
            $table->date('invoice_date');
            $table->enum('status', ["notified", "pending", "completed", "rejected"]);
            $table->integer('reviewer_id');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billing_schedule_reminders');
    }

}
