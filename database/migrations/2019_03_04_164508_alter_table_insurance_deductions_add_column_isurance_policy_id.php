<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableInsuranceDeductionsAddColumnIsurancePolicyId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('insurance_deductions', function (Blueprint $table) {
            $table->integer('insurance_policy_id')->after('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('insurance_deductions', function (Blueprint $table) {
            $table->dropColumn(['insurance_policy_id']);
        });
    }
}
