<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePayslipMonthAddColumnJsonDataAndComment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payslip_month', function (Blueprint $table) {
            $table->text('comment')->after('file_path')->nullable();
            $table->text('json_data')->after('file_path')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payslip_month', function (Blueprint $table) {
            $table->dropColumn('comment');
            $table->dropColumn('json_data');
        });
    }
}
