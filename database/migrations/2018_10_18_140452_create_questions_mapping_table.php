<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsMappingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions_mapping', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('question_id');
            $table->string('reference_type');
            $table->integer('reference_id');
            $table->timestamps();
            $table->softDeletes();
            $table->index('question_id');
            $table->index('reference_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions_mapping');
    }
}
