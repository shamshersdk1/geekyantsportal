<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableCmsDevelopersAddColumnYoutubeLink extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_developers', function (Blueprint $table) {
            $table->string('youtube_link',512)->after('hire_text')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_developers', function (Blueprint $table) {
            $table->dropColumn(['youtube_link']);
        });
    }
}
