<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUserGoalsModifyStatusColoumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_goals', function (Blueprint $table) {
            $table->dropColumn('status');
        });
        Schema::table('user_goals', function (Blueprint $table) {
            $table->enum('status', ["pending", "submitted", "confirmed", "running","completed","closed"])->after('to_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_goals', function (Blueprint $table) {
            $table->enum('status', ["pending", "running","completed","closed"])->after('to_date');
        });
    }
}
