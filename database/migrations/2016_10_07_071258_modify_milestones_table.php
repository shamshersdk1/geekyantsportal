<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyMilestonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_quotation_milestones', function(Blueprint $table)
        {
            $table->string('title')->nullable();
        });

        Schema::create('milestone_sprint', function(Blueprint $table){
            $table->increments('id');
            $table->integer('milestone_id');
            $table->integer('sprint_id');
            $table->integer('project_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('milestone_sprint');
    }
}
