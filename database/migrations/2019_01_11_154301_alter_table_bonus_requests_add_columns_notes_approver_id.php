<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableBonusRequestsAddColumnsNotesApproverId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bonus_requests', function (Blueprint $table) {
            $table->integer('approver_id')->after('status')->nullable();
            $table->string('notes',512)->after('status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bonus_requests', function (Blueprint $table) {
            $table->dropColumn(['approver_id','notes']);
        });
    }
}
