<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class AlterTableOnSiteBonuses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('on_site_bonuses', function ($table) {
            $table->dropColumn('employee_id');
            $table->integer('onsite_allowance_id')->after('id');
            $table->integer('user_id')->after('id');
            $table->integer('reviewed_by')->before('created_at')->nullable();
            $table->date('reviewed_at')->before('created_at')->nullable();
            $table->dropColumn('from_date');
            $table->dropColumn('to_date');
            $table->dropColumn('type');
            $table->dropColumn('amount');
            $table->dropColumn('reference_id');
            $table->dropColumn('reference_type');
            $table->dropColumn('approved_by');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('on_site_bonuses', function ($table) {
            $table->integer('employee_id')->after('id')->nullable();
            $table->date('to_date')->after('date')->nullable();
            $table->date('from_date')->after('date')->nullable();
            $table->string('type')->after('date')->nullable();
            $table->integer('amount')->after('date')->nullable();
            $table->integer('reference_id')->after('date')->nullable();
            $table->integer('reference_type')->after('date')->nullable();
            $table->integer('approved_by')->after('date')->nullable();
            $table->dropColumn('reviewed_by');
            $table->dropColumn('reviewed_at');
            $table->dropColumn('user_id');
            $table->dropColumn('onsite_allowance_id');
        });
    }
}
