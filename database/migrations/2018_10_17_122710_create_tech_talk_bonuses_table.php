<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTechTalkBonusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tech_talk_bonuses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id');
            $table->string('topic');
            $table->date('date');
            $table->decimal('amount', 15, 2);
            $table->longText('notes')->nullable();
            $table->enum('status', ['pending', 'approved', 'rejected'])->default('pending')->nullable();
            $table->integer('approved_by')->nullable();
            $table->integer('created_by');
            $table->timestamps();
            $table->softDeletes()->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tech_talk_bonuses');
    }
}
