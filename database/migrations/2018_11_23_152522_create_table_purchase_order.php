<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablePurchaseOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vendor_id')->index();
            $table->string('reference_number');
            $table->string('payment_terms');
            $table->date('delivery_date')->nullable();
            $table->integer('created_by');
            $table->integer('approved_by');
            $table->boolean('gst_included')->default(0);
            $table->enum('status', ["raised", "received", "cancelled"]);
            $table->double('total', 15, 2);
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_orders');

    }
}
