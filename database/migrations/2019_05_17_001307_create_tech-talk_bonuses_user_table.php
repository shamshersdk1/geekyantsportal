<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTechTalkBonusesUserTable extends Migration
{
 /**
  * Run the migrations.
  *
  * @return void
  */
 public function up()
 {
  Schema::create('tech_talk_bonuses_user', function (Blueprint $table) {
   $table->increments('id');
   $table->integer('tech_talk_id');
   $table->integer('user_id');
   $table->decimal('rating', 5, 2);
   $table->decimal('amount', 15, 2);
   $table->string('comments');
   $table->integer('approved_by')->nullable();
   $table->timestamps();
   $table->softDeletes()->nullable();
  });
 }

 /**
  * Reverse the migrations.
  *
  * @return void
  */
 public function down()
 {
  //
 }
}
