
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableCronsChangeStructureAddColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crons', function (Blueprint $table) {
            $table->enum('event_type', ["once", "recurring"])->after('reference_id');
            $table->longText('reference_data_json')->after('reference_id')->nullable();
            $table->string('minutes')->after('reference_data_json')->nullable();
            $table->string('hours')->after('minutes')->nullable();
            $table->string('day_of_month')->after('hours')->nullable();
            $table->string('month_of_year')->after('day_of_month')->nullable();
            $table->string('day_of_week')->after('month_of_year')->nullable();
            $table->integer('occurrence_counter')->after('day_of_week')->nullable();
            $table->integer('executed_counter')->after('occurrence_counter')->nullable();
            $table->dateTime('last_processed_at')->after('executed_counter')->nullable();
            $table->date('end_on')->after('last_processed_at')->nullable();
            $table->string('job_handler')->after('end_on')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crons', function (Blueprint $table) {
            $table->dropColumn(['event_type','reference_data_json', 'minutes', 'hours', 'day_of_month', 'month_of_year','day_of_week','occurrence_counter','executed_counter','last_processed_at']);
        });
    }
}
