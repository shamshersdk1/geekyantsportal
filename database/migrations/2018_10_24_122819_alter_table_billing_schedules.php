<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class AlterTableBillingSchedules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('billing_schedules', function ($table) {
            $table->decimal('amount', 15, 2)->nullable()->after('status');
            $table->longText('notes')->nullable()->after('amount');
            $table->string('title')->nullable()->after('project_id');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('billing_schedules', function (Blueprint $table) {
            $table->dropColumn(['amount']);
            $table->dropColumn(['notes']);
            $table->dropColumn(['title']);
        });

    }
}
