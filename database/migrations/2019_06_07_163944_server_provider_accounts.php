<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ServerProviderAccounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('server_provider_accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('datacenter_id')->index();
            $table->tinyInteger('is_self_owned')->index();
            $table->string('username');
            $table->string('password');
            $table->string('url');
            $table->text('notes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('server_provider_accounts');
    }
}
