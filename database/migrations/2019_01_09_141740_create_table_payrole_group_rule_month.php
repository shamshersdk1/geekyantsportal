<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePayroleGroupRuleMonth extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payrole_group_rule_months', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('payrole_group_id');
            $table->integer('payroll_rule_for_csv_id');
            $table->integer('month_id');
            $table->string('name');
            $table->enum('type',['credit', 'debit'])->default('debit');
            $table->string('calculation_type')->nullable();
            $table->double('calculation_value', 15, 5);
            $table->integer('calculated_upon');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payrole_group_rule_month');
    }
}
