<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableBonusRequests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bonus_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('month_id');
            $table->integer('user_id');
            $table->date('date');
            $table->enum('status', ['pending', 'approved', 'cancelled', 'rejected'])->default('pending');
            $table->enum('type', ['onsite', 'additional', 'techtalk', 'referral'])->default('additional');
            $table->string('sub_type')->nullable();
            $table->string('title')->nullable();
            $table->integer('referral_for')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
