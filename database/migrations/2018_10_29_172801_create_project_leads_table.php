<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_leads', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id');
            $table->string('key');
            $table->boolean('is_ours');
            $table->string('value');
            $table->timestamps();
            $table->softDeletes();
            $table->index('project_id');
            $table->index('key');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_leads');
    }
}
