<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableExpenseRemoveTypeVendorId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('expenses', function (Blueprint $table) {
            $table->dropColumn('type');
            $table->dropColumn('vendor_id');
        });

        Schema::table('expenses', function (Blueprint $table) {
            $table->integer('payment_by')->after('created_by')->nullable();          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('expenses', function (Blueprint $table) {
            $table->dropColumn('payment_by');
        });
    }
}
