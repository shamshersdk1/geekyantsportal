<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableProjectsAddColumnsForLeads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->integer('sales_manager_id')->after('project_category_id')->nullable();
            $table->integer('code_lead_id')->after('sales_manager_id')->nullable();
            $table->integer('delivery_lead_id')->after('code_lead_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->dropColumn(['sales_manager_id','code_lead_id','delivery_lead_id']);
        });
    }
}
