<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableAddColumnItemPriceModelMake extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assets', function (Blueprint $table) {
            $table->string('make')->after('serial_number')->nullable();
            $table->string('model')->after('serial_number')->nullable();
            $table->decimal('tax', 8, 2)->after('serial_number')->nullable();
            $table->decimal('price', 8, 2)->after('serial_number')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assets', function (Blueprint $table) {
            $table->dropColumn(['make']);
            $table->dropColumn(['model']);
            $table->dropColumn(['tax']);
            $table->dropColumn(['price']);
        });
    }
}
