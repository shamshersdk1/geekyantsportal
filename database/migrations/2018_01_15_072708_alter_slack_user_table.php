<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSlackUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('slack_user', function ($table) {
            $table->string('slack_id')->nullable()->after('id')->change();
            $table->string('first_name')->nullable()->after('color')->change();
            $table->string('email')->nullable()->after('avatar_hash')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('slack_user', function ($table) {
            $table->integer('slack_id')->after('id')->nullable()->change();
            $table->string('first_name')->after('color')->change();
            $table->string('email')->after('avatar_hash')->change();
        });
    }
}
