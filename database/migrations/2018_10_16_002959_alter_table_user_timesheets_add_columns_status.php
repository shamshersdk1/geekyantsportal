<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUserTimesheetsAddColumnsStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_timesheets', function (Blueprint $table) {
            $table->integer('approved_duration')->default(0);
            $table->enum('status',['pending','approved','rejected'])->after('task');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_timesheets', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
}
