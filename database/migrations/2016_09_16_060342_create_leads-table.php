<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leads', function(Blueprint $table)
        {   
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('email');
            $table->string('subject');
            $table->enum('status',['New Lead','Closed Lost','Closed Won','In Discussion']);
            $table->string('message',512);
            $table->timestamps();
            $table->softDeletes()->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('leads');
    }
}
