<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAttendaceTrackerEnableUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function(Blueprint $table)
        {   
            $table->tinyInteger('is_attendance_enabled')->default(1);
        });

        // 230 - Amar Singh
        // 226 - Gopal Mondal
        // 225 - Shivakumar
        // 155 - Ravichandran V
        // 111 - Mani Meghala S
        // 112 - Chandra Bahadur Thapa
        // 110 - Kanhu Charan Jena
        // 57 - Madhushree Gupta
        // 35 - Madhu Sahu
        // 113 - Girija Sankar Senapati
        // 141 - Kunal

        DB::table('users')->where('id',230)->update(['is_attendance_enabled' => 0]);
        DB::table('users')->where('id',226)->update(['is_attendance_enabled' => 0]);
        DB::table('users')->where('id',225)->update(['is_attendance_enabled' => 0]);
        DB::table('users')->where('id',155)->update(['is_attendance_enabled' => 0]);
        DB::table('users')->where('id',111)->update(['is_attendance_enabled' => 0]);
        DB::table('users')->where('id',112)->update(['is_attendance_enabled' => 0]);
        DB::table('users')->where('id',110)->update(['is_attendance_enabled' => 0]);
        DB::table('users')->where('id',57)->update(['is_attendance_enabled' => 0]);
        DB::table('users')->where('id',35)->update(['is_attendance_enabled' => 0]);
        DB::table('users')->where('id',113)->update(['is_attendance_enabled' => 0]);
        DB::table('users')->where('id',141)->update(['is_attendance_enabled' => 0]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table)
        {   
            $table->dropColumn('is_attendance_enabled');
        });
    }
}
