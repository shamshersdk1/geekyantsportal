<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableUsersTimesheetLock extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_timesheet_locks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            // $table->integer('project_id');
            $table->date('date');
            $table->date('billing_date')->nullable();
            $table->string('comment');
            $table->string('date_updated_by')->nullable();
            $table->string('billing_updated_by')->nullable();
            $table->boolean('is_processed')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_timesheet_locks');

    }
}
