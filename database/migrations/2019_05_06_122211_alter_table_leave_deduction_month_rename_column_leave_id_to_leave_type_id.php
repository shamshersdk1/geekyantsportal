<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class AlterTableLeaveDeductionMonthRenameColumnLeaveIdToLeaveTypeId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leave_deduction_month', function ($table) {
            $table->dropColumn('leave_id');
            $table->dropColumn('amount');
            $table->integer('leave_type_id')->after('id');
            $table->integer('transaction_id')->after('duration')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leave_deduction_month', function ($table) {
            $table->integer('leave_id');
            $table->decimal('amount');
            $table->dropColumn('transaction_id');
            $table->dropColumn('leave_type_id');
        });
    }
}
