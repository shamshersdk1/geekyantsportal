<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserGoalItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_goal_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_goal_id');
            $table->string('title');
            $table->string('success_metric')->nullable();
            $table->longText('process');
            $table->string('points')->nullable();
            $table->string('comment')->nullable(); 
            $table->boolean('is_idp');
            $table->timestamps();
            $table->index(['user_goal_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_goal_items');
    }
}
