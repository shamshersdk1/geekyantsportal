<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAppraisals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('appraisals', 'appraisals_bkp');
        
        Schema::create('appraisals', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('user_id');
            $table->date('effective_date');
            $table->integer('type_id');
            $table->timestamps();
            $table->index(['user_id','effective_date']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('appraisals');
    }
}
