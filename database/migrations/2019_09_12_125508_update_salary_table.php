<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateSalaryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('salary', function (Blueprint $table) {
            $table->dropColumn('user_id');
            $table->dropColumn('monthly_gross_salary');
            $table->dropColumn('total_deductions');
            $table->dropColumn('net_payable');
            $table->enum('status', ['pending', 'transaction', 'bank_transfer', 'paid', 'completed'])->after('net_payable');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('salary', function (Blueprint $table) {
            $table->integer('user_id')->index();
            $table->integer('monthly_gross_salary');
            $table->integer('total_deductions');
            $table->integer('net_payable');
            $table->dropColumn('status');
        });
    }
}
