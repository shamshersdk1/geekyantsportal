<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAdhocPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adhoc_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('adhoc_payment_component_id')->index();
            $table->integer('month_id')->index();
            $table->integer('user_id')->index();
            $table->string('comment');
            $table->decimal('amount',10,2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('adhoc_payments');
    }
}
