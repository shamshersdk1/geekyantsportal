<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeaveBalanceManager extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leave_balance_managers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('month_id');
            $table->integer('leave_id');
            $table->decimal('days')->default(0);
            $table->enum('status', ['credited', 'debited']);
            $table->string('reason');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leave_balance_manager');
    }
}
