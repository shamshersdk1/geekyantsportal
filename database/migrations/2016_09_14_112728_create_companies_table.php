<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('companies', function(Blueprint $table)
		{	
			$table->increments('id');
			$table->string('name');
			$table->string('email');
			$table->integer('work_phone')->nullable();
			$table->integer('mobile')->nullable();
			$table->string('website');
			$table->string('billing_add_street')->nullable();
			$table->string('billing_add_city')->nullable();
			$table->string('billing_add_state')->nullable();
			$table->integer('billing_add_zip_code')->nullable();
			$table->string('billing_add_country')->nullable();
			$table->string('billing_add_fax')->nullable();
			$table->string('shipping_add_street')->nullable();
			$table->string('shipping_add_city')->nullable();
			$table->string('shipping_add_state')->nullable();
			$table->integer('shipping_add_zip_code')->nullable();
			$table->string('shipping_add_country')->nullable();
			$table->string('shipping_add_fax')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('companies');
	}

}
