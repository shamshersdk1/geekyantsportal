<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class UpdateApprasialComponents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('appraisal_components', function ($table) {
            $table->decimal('value', 12, 2)->default(0)->after('component_id')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('appraisal_components', function ($table) {
            $table->string('value')->after('component_id')->nullable()->change();
        });
    }
}
