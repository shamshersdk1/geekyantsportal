<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ItSavings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('it_savings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('financial_year_id');
            $table->integer('user_id');
            $table->decimal('rent_monthly', 15, 2)->nullable();
            $table->decimal('rent_yearly', 15, 2)->nullable();
            $table->decimal('pf', 15, 2)->nullable();
            $table->decimal('pension_scheme_1', 15, 2)->nullable();
            $table->decimal('pension_scheme_1b', 15, 2)->nullable();
            $table->decimal('ppf', 15, 2)->nullable();
            $table->decimal('central_pension_fund', 15, 2)->nullable();
            $table->decimal('lic', 15, 2)->nullable();
            $table->decimal('housing_loan_repayment', 15, 2)->nullable();
            $table->decimal('term_deposit', 15, 2)->nullable();
            $table->decimal('national_saving_scheme', 15, 2)->nullable();
            $table->decimal('tax_saving', 15, 2)->nullable();
            $table->decimal('children_expense', 15, 2)->nullable();
            $table->decimal('other_investment', 15, 2)->nullable();
            $table->decimal('medical_insurance_premium', 15, 2)->nullable();
            $table->decimal('medical_treatment_expense', 15, 2)->nullable();
            $table->decimal('educational_loan', 15, 2)->nullable();
            $table->decimal('donation', 15, 2)->nullable();
            $table->decimal('rent_without_receipt', 15, 2)->nullable();
            $table->decimal('physical_disablity', 15, 2)->nullable();
            $table->decimal('other_deduction', 15, 2)->nullable();
            $table->decimal('salary_paid', 15, 2)->nullable();
            $table->decimal('tds', 15, 2)->nullable();
            $table->boolean('previous_form_16_12b')->default(0);
            $table->enum('status', ['pending', 'in_progress', 'completed'])->nullable();;
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('it_savings');
    }
}
