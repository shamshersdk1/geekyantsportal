<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableLoanAddNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('loans', function (Blueprint $table) {
            $table->dropColumn('emi');
            $table->dropColumn('emi_start_date');
        });
        
        Schema::table('loans', function (Blueprint $table) {
            $table->decimal('emi')->nullable()->after('annual_bonus_type_id');
            $table->date('emi_start_date')->nullable()->after('annual_bonus_type_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropColumn('emi');
        Schema::dropColumn('emi_start_date');
    }
}
