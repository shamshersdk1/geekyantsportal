<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableLeaveBalanceManagers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leave_balance_managers', function (Blueprint $table) {
            //$table->renameColumn('leave_id', 'leave_type_id');
            $table->dropColumn('leave_id');
            $table->dropColumn('status');
            $table->integer('leave_type_id')->after('id');
            $table->enum('type', ['credit', 'debit'])->after('days');
            $table->string('balanced_by')->after('reason');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
