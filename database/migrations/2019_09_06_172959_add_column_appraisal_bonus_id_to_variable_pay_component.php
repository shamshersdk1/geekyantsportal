<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnAppraisalBonusIdToVariablePayComponent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('variable_pay_components', function(Blueprint $table)
        {
            $table->integer('appraisal_bonus_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('variable_pay_components', function(Blueprint $table) {
            $table->dropColumn('appraisal_bonus_id');
        });
    }
}
