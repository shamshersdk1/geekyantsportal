<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableLeaveCalendarYearsModifyColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leave_calendar_years', function (Blueprint $table) {
            $table->dropColumn('leave_type_id');
        });
        Schema::table('leave_calendar_years', function (Blueprint $table) {
            $table->renameColumn('max_leaves','max_paid_leaves');
            $table->integer('max_sick_leaves')->after('max_leaves');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leave_calendar_years', function (Blueprint $table) {
            $table->dropColumn('max_paid_leaves,max_sick_leaves');
        });
        Schema::table('leave_calendar_years', function (Blueprint $table) {
            $table->integer('leave_type_id')->after('id')->default(0);
        });
    }
}
