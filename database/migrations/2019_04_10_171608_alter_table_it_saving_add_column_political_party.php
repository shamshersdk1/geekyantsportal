<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableItSavingAddColumnPoliticalParty extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('it_savings', function (Blueprint $table) {
            $table->text('other_multiple_investments')->after('other_investment')->nullable();
            $table->decimal('political_party')->after('physical_disablity')->default(0);
            $table->text('other_multiple_deductions')->after('other_deduction')->nullable();
            $table->boolean('agree')->after('previous_form_16_12b')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
