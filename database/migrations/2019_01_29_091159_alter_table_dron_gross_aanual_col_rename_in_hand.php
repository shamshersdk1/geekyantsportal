<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableDronGrossAanualColRenameInHand extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('appraisals', function (Blueprint $table) {
            $table->dropColumn('annual_gross_salary');
            $table->dropColumn('monthly_var_bonus');
            $table->dropColumn('annual_bonus');
        });

        Schema::table('appraisals', function (Blueprint $table) {
            $table->renameColumn('in_hand', 'annual_gross_salary');
            $table->renameColumn('qtr_bonus', 'monthly_var_bonus');
            $table->renameColumn('year_end', 'annual_bonus');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
