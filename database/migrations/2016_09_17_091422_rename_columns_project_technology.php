<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumnsProjectTechnology extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_technology', function ($table) {
            $table->dropColumn(['technology_id', 'project_id']);
        });
        Schema::table('project_technology', function ($table) {
            $table->integer('cms_project_id')->unsigned()->index();
            $table->integer('cms_technology_id')->unsigned()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_technology', function(Blueprint $table)
        {
            $table->dropColumn('cms_technology_id');
            $table->dropColumn('cms_project_id');
        });
    }
}
