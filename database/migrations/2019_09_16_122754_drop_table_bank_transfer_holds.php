<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropTableBankTransferHolds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('bank_transfer_holds');
        Schema::table('bank_transfer_hold_users', function(Blueprint $table) {
            $table->dropColumn('bank_transfer_hold_id');
            $table->integer('bank_transfer_id')->index()->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
