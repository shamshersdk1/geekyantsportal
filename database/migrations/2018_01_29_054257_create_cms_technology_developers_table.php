<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsTechnologyDevelopersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_technology_developers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cms_technology_id');
            $table->integer('user_id');
            $table->boolean('status');
            $table->string('about');
            $table->string('area_of_interest');
            $table->string('achievement');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_technology_developers');
    }
}
