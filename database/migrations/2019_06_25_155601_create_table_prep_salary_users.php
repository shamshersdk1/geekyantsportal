<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePrepSalaryUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prep_salary_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('prep_salary_id')->index();
            $table->integer('user_id')->index();
            $table->integer('prep_salary_component_id');
            $table->text('value');
            $table->unique(['prep_salary_id', 'user_id','prep_salary_component_id'],'user_salary_component_unique');

        });
    }

    /**
     * Reverse the migrations.s
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('prep_salary_users');
    }
}
