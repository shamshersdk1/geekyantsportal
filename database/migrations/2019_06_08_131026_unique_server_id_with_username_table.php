<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UniqueServerIdWithUsernameTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('server_accounts', function (Blueprint $table) {
            $table->unique(['server_id','username'], 'unique_server_id_username');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('server_accounts', function (Blueprint $table) {
            $table->dropUnique('unique_server_id_username');
        });
    }
}
