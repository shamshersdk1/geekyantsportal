<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE `profile` 
                                            MODIFY `intro` text null,
                                            MODIFY `location` varchar(100) null,
                                            MODIFY `keywords` text null,
                                            MODIFY `amazing` text null,
                                            MODIFY `phone` varchar(100) null,
                                            MODIFY `twitter` varchar(100) null,
                                            MODIFY `facebook` varchar(100) null,
                                            MODIFY `google` varchar(100) null,
                                            MODIFY `linkedin` varchar(100) null,
                                            MODIFY `skype` varchar(100) null,
                                            MODIFY `github` varchar(100) null,
                                            MODIFY `stack_overflow` varchar(100) null,
                                            MODIFY `expertise_json` text null,
                                            MODIFY `skills_json` text null,
                                            MODIFY `knowledge` text null,
                                            MODIFY `hobbies_json` text null,
                                            MODIFY `achievements` text null,
                                            MODIFY `designation` varchar(100) null,
                                            MODIFY `blogs_json` text null,
                                            MODIFY `image` varchar(100) null,
                                            MODIFY `profile_url` varchar(100) null
                     ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
