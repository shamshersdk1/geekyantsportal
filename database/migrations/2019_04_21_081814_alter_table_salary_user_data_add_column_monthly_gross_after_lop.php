<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableSalaryUserDataAddColumnMonthlyGrossAfterLop extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('salary_user_data', function ($table) {
            $table->decimal('monthly_gross_after_lop')->after('monthly_gross_salary')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('salary_user_data', function ($table) {
            $table->dropColumn('monthly_gross_after_lop');
        });
    }
}
