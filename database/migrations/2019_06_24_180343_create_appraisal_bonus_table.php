<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppraisalBonusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('appraisal_bonuses', 'appraisal_bonuses_bkp');
        Schema::create('appraisal_bonuses',function(Blueprint $table){
            $table->increments('id');
            $table->integer('appraisal_id');
            $table->integer('appraisal_bonus_type_id');
            $table->string('value');
            $table->date('value_date');
            $table->unique(['appraisal_id','appraisal_bonus_type_id']);
            $table->index(['appraisal_id','appraisal_bonus_type_id']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('appraisal_bonus');
    }
}
