<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableIpAddressRangeAddColumnCompanyAsset extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ip_address_ranges', function (Blueprint $table) {
            $table->boolean('company_asset')->after('net_mask_ip_long')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ip_address_ranges', function (Blueprint $table) {
            $table->dropColumn('company_asset');
        });

    }
}
