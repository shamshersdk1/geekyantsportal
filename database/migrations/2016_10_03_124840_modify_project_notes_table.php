<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyProjectNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_notes', function(Blueprint $table)
        {   
            $table->dropColumn('note');
        });
        Schema::table('project_notes', function(Blueprint $table)
        {   
            $table->text('note');
            $table->boolean('private_to_admin');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropColumn('private_to_admin');
        Schema::dropColumn('note');
    }
}
