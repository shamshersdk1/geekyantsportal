<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableItSavingsOthers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('it_savings_others', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('it_savings_id')->index();
            $table->string('title');
            $table->string('type');
            $table->decimal('value', 10, 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('it_savings_others');
    }
}
