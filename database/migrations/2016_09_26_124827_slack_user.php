<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SlackUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slack_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('slack_id')->nullable();
            $table->string('team_id')->nullable();
            $table->string('name')->nullable();
            $table->boolean('deleted')->nullable();
            $table->string('status')->nullable();
            $table->string('color')->nullable();
            $table->string('first_name');
            $table->string('last_name')->nullable();
            $table->string('avatar_hash')->nullable();
            $table->string('email');
            $table->boolean('is_admin')->nullable();
            $table->boolean('is_owner')->nullable();
            $table->boolean('is_primary_owner')->nullable();
            $table->boolean('is_restricted')->nullable();
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('slack_user');
    }
}
