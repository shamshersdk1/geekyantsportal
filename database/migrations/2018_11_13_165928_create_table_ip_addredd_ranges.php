<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableIpAddreddRanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ip_address_ranges', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('start_ip');
            $table->string('end_ip');
            $table->string('net_mask_ip')->nullable();
            $table->bigInteger('start_ip_long');
            $table->bigInteger('end_ip_long');
            $table->bigInteger('net_mask_ip_long')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ip_address_ranges');

    }
}
