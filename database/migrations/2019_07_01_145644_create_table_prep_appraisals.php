<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePrepAppraisals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prep_appraisals',function(Blueprint $table){
            $table->increments('id');
            $table->integer('prep_salary_id')->index();
            $table->integer('user_id')->index();
            $table->integer('appraisal_id')->index();
            $table->date('start_date');
            $table->date('end_date');
            $table->unique(['prep_salary_id','user_id','appraisal_id'],'prep_appraisal__unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('prep_appraisals');

    }
}
