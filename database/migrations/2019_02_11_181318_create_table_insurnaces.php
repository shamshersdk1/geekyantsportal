<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableInsurnaces extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insurances', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',125);
            $table->date('start_date');
            $table->date('end_date');
            $table->integer('coverage');
            $table->integer('created_by');
            $table->enum('status',['pending','approved','closed'])->default('pending');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('insurance_premiums', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('insurance_id');
            $table->integer('user_id');
            $table->integer('total_amount');
            $table->integer('amount');
            $table->date('date')->nullable();
            $table->integer('requested_by')->nullable();
            $table->integer('approved_by')->nullable();
            $table->integer('closed_by')->nullable();
            $table->integer('parent_id')->nullable();
            $table->enum('status',['pending','requested','approved','closed'])->default('pending');
            $table->timestamps();   
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insurances');
        Schema::dropIfExists('insurances_premiums');
    }
}
