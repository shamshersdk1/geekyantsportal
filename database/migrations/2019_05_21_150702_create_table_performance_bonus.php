<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablePerformanceBonus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('performance_bonus', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('month_id');
            $table->date('date');
            $table->decimal('amount', 15, 2);
            $table->string('comment');
            $table->softDeletes();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('performance_bonus');
    }
}
