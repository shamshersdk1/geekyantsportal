<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePrepAdhocPaymentComponents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prep_adhoc_payment_components',function (Blueprint $table){
            $table->increments('id');
            $table->integer('prep_adhoc_payment_id')->index();
            $table->string('key');
            $table->decimal('value',10,2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('prep_adhoc_payment_components');
    }
}
