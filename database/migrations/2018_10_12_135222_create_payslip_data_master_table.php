<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayslipDataMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payslip_data_master', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('payslip_data_month_id');
            $table->string('variable_pay')->nullable();
            $table->string('annual_in_hand')->nullable();
            $table->integer('created_by');
            $table->timestamps();
            $table->softDeletes();
            $table->index('user_id');
            $table->index('payslip_data_month_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payslip_data_master');
    }
}
