<?php

use Illuminate\Database\Migrations\Migration;

class CreateRenameGrossSalaryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('prep_gross_salary');
        Schema::drop('prep_tds');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('prep_gross_salary', function ($table) {
            $table->increments('id');
            $table->integer('prep_salary_id')->index();
            $table->integer('user_id')->index();
            $table->decimal('value');
            $table->timestamps();
        });

        Schema::create('prep_tds', function ($table) {
            $table->increments('id');
            $table->integer('prep_salary_id')->index();
            $table->integer('user_id')->index();
            $table->decimal('value');
            $table->timestamps();
        });

    }
}
