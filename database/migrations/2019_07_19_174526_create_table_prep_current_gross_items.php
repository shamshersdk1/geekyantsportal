<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePrepCurrentGrossItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prep_current_gross_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('prep_current_gross_id')->index();
            $table->string('key');
            $table->decimal('value');
            $table->unique(['prep_current_gross_id','key'],'prep_current_gross');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
