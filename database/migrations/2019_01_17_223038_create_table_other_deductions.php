<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOtherDeductions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('other_deductions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('month_id');
            $table->double('amount',10,2);
            $table->string('notes')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('approved_by')->nullable();
            $table->enum('type',['pf_additional','advance_salary','damage','other'])->default('other');
            $table->enum('status',['pending','approved','rejected'])->default('pending');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::table('months', function (Blueprint $table) {
            $table->string('working_days')->after('year')->nullable();
        });
        Schema::table('salary_user_data', function (Blueprint $table) {
            $table->string('days_worked')->after('appraisal_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('other_deductions');
    }
}
