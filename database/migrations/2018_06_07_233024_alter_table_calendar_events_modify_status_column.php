<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableCalendarEventsModifyStatusColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('calendar_events', function (Blueprint $table) {
            $table->dropColumn('status');
        });
        Schema::table('calendar_events', function (Blueprint $table) {
            $table->string('calendar_event_id')->nullable()->change();
            $table->enum('status', ["pending", "confirmed", "tentative", "cancelled"])->after('reference_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('calendar_events', function (Blueprint $table) {
            $table->enum('status', ["confirmed", "tentative", "cancelled"])->after('reference_type');
        });
    }
}
