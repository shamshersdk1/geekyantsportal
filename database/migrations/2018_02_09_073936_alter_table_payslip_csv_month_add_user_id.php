<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePayslipCsvMonthAddUserId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('payslip_csv_month', function (Blueprint $table) {
          $table->dropColumn(['csv']);
          $table->integer('created_by')->after('days');
          $table->integer('approved_by')->after('created_by');
          $table->string('csv_path')->nullable()->after('status');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('payslip_csv_month', function (Blueprint $table) {
          $table->dropColumn(['created_by', 'approved_by', 'csv_path']);
          $table->string('csv')->nullable()->after('status');
      });
    }
}
