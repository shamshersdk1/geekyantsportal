<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->integer('vendor_id');
            $table->integer('po_id')->nullable();
            $table->integer('expense_head_id')->nullable();
            $table->double('amount', 15, 2);
            $table->enum('payment_status', ["pending", "paid", "hold", "due_immediate_pending", "due_next_cycle_pending", "due_immediate_approved", "due_next_cycle_approved"]);
            $table->string('payment_method')->nullable();
            $table->string('invoice')->nullable();
            $table->integer('expense_by')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('approved_by')->nullable();
            $table->string('reference_number')->nullable();
            $table->date('payment_date')->nullable();
            $table->longText('other_info')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expenses');
    }
}
