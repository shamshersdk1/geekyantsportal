<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserAttendanceTickAndActiveTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('user_attendance', function(Blueprint $table)
        {   
            $table->integer('tick_count');
            $table->integer('active_count');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_attendance', function(Blueprint $table)
        {   
            $table->dropColumn('tick_count');
            $table->dropColumn('active_count');
        });
    }
}
