<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableCrons extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->enum('event_type', ["fixed", "recurring"]);
            $table->enum('event_term', ["daily", "weekly", "monthly"])->nullable();;
            $table->string('reference_type');
            $table->integer('reference_id');
            $table->date('start_date');
            $table->date('end_date')->nullable();;
            $table->time('remind_at');
            $table->string('sub_type')->nullable();
            $table->boolean('mon')->default(0);
            $table->boolean('tue')->default(0);
            $table->boolean('wed')->default(0);
            $table->boolean('thu')->default(0);
            $table->boolean('fri')->default(0);
            $table->boolean('first_working_day')->default(0);
            $table->boolean('last_working_day')->default(0);
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crons');
    }
}
