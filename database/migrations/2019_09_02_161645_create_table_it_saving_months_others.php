<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableItSavingMonthsOthers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('it_saving_months_others', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('it_saving_month_id')->index();
            $table->string('title');
            $table->string('type');
            $table->decimal('value',10,2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('it_saving_months_others');
    }
}
