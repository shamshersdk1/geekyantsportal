<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoanTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('loan_id');
            $table->double('amount', 15, 2);
            $table->enum('status', ['pending', 'processing','paid','rejected']);
            $table->date('paid_on')->nullable();
            $table->longText('comment')->nullable();
            $table->timestamps();
            $table->string('method', 512);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan_transactions');
    }
}
