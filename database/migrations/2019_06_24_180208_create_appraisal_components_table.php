<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppraisalComponentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appraisal_components',function(Blueprint $table){
            $table->increments('id');
            $table->integer('appraisal_id');
            $table->integer('component_id');
            $table->string('value');
            $table->unique(['appraisal_id','component_id']);
            $table->index('appraisal_id');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('appraisal_components');
    }
}
