<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTmpTimesheetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmp_timesheet', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('project_id')->nullable();
            $table->string('channel_id')->nullable();
            $table->date('date')->nullable();
            $table->float('duration', 4, 2 )->default(0);
            $table->string('task');
            $table->timestamps();
            $table->softDeletes();
            $table->index('user_id');
            $table->index('project_id');
            $table->index('channel_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tmp_timesheet');
    }
}
