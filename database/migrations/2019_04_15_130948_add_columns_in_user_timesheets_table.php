<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class AddColumnsInUserTimesheetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_timesheets', function ($table) {
            $table->string('comment')->after('task');
            $table->integer('approved_duration')->after('duration');
            $table->integer('approved_by')->after('approved_duration');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_timesheets', function ($table) {
            $table->dropColumn(['comment', 'approved_duration', 'extra_hours', 'extra_approver_id', 'extra_approved_at']);
        });
    }
}
