<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTableAppraisalCompType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('appraisal_component_types', function (Blueprint $table) {
            $table->boolean('is_conditional_prorata')->after('is_company_expense')->default(0);
            $table->string('prorata_function')->after('is_conditional_prorata')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('appraisal_component_types', function (Blueprint $table) {
            $table->dropColumn('is_conditional_prorata');
            $table->dropColumn('prorata_function');
        });
    }
}
