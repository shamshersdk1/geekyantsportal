<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCmsDevelopersTableAddMetaInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('cms_developers', function (Blueprint $table) {
          $table->string('keywords', 1000)->after('slug')->nullable();
          $table->string('meta_description', 1000)->after('keywords')->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('cms_developers', function (Blueprint $table) {
          $table->dropColumn(['keywords','meta_description']);
      });
    }
}
