<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableInsurancePolicies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('insurances');
        Schema::dropIfExists('insurance_premiums');

         Schema::create('insurance_policies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vendor_id');
            $table->string('name',125);
            $table->string('policy_number',125);
            $table->string('description',255)->nullable();
            $table->date('start_date');
            $table->date('end_date');
            $table->float('coverage_amount',8, 2)->nullable();
            $table->float('amount_paid',8, 2)->nullable();
            $table->float('premium_amount', 8, 2)->nullable();
            $table->integer('created_by');
            $table->string('applicable_for',125); //user Model / asset Model / contract Model/
            $table->enum('accessible_to',['group','individual'])->default('group');
            $table->string('insurance_category')->nullable();
            $table->enum('status',['pending','approved','closed'])->default('pending');
            $table->boolean('sub_group')->default(false);
            $table->integer('parent_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('insurances', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('insurance_policy_id');
            $table->integer('insurable_id');
            $table->string('insurable_type');
            $table->string('comments')->nullable();
            $table->string('inactive_date')->nullable();
            $table->integer('created_by');
            $table->integer('parent_id')->nullable();
            $table->enum('status',['pending','approved','closed'])->default('pending');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
