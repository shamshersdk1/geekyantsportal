<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBankTransferUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_transfer_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bank_transfer_id')->index();
            $table->integer('user_id')->index();
            $table->decimal('amount',12,2);
            $table->decimal('actual_amount',12,2);
            $table->string('mode_of_transfer');
            $table->string('reference_number');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bank_transfer_users');
    }
}
