<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUniqueToSalary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('prep_adhoc_payments', function (Blueprint $table) {
            $table->unique(['prep_salary_id', 'user_id'])->change();
        });
        Schema::table('prep_food_deductions', function (Blueprint $table) {
            $table->unique(['prep_salary_id', 'user_id','food_deduction_id'],'unique_prep_food')->change();
        });
        Schema::table('prep_gross_paids', function (Blueprint $table) {
            $table->unique(['prep_salary_id', 'user_id'])->change();
        });
        Schema::table('prep_gross_to_be_paids', function (Blueprint $table) {
            $table->unique(['prep_salary_id', 'user_id'])->change();
        });
        Schema::table('prep_insurances', function (Blueprint $table) {
            $table->unique(['prep_salary_id', 'user_id','insurance_deduction_id'],'unique_prep_insurance')->change();
        });
        Schema::table('prep_it_savings', function (Blueprint $table) {
            $table->unique(['prep_salary_id', 'user_id','it_savings_id'])->change();
        });
        Schema::table('prep_loan_emi', function (Blueprint $table) {
            $table->unique(['prep_salary_id', 'user_id','loan_emi_id'])->change();
        });
        Schema::table('prep_tds', function (Blueprint $table) {
            $table->unique(['prep_salary_id', 'user_id'])->change();
        });
        Schema::table('prep_users', function (Blueprint $table) {
            $table->unique(['prep_salary_id', 'user_id'])->change();
        });
        Schema::table('prep_vpfs', function (Blueprint $table) {
            $table->unique(['prep_salary_id', 'user_id','vpf_deduction_id'])->change();
        });
        Schema::table('prep_working_days', function (Blueprint $table) {
            $table->unique(['prep_salary_id', 'user_id'])->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
