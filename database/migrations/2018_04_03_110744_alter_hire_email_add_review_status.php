<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterHireEmailAddReviewStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hire_email', function (Blueprint $table) {
            $table->enum('review_status', ['unreviewed', 'reviewed', 'spam'])->after('comment')->default('unreviewed');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hire_email', function (Blueprint $table) {
            $table->dropColumn(['review_status']);
        });
    }
}
