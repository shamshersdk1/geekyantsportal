<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayroleGroupRuleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payrole_group_rule', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('payrole_group_id');
            $table->string('name');
            $table->enum('type',['credit', 'debit'])->default('debit');
            $table->string('calculation_type')->nullable();
            $table->double('calculation_value', 15, 5);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payrole_group_rule');
    }
}
