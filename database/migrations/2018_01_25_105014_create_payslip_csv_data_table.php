<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Models\Admin\PayrollRuleForCsv;

class CreatePayslipCsvDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $head_string = ['Sl No','Employee Code','Employee Name','Email ID','PAN No','DOB','DOJ','Designation','Gender','Bank A/c. No.','Bank IFSC Code','PF No','UAN No','ESI No','Month','Year'];
        $head_int = ['Total Working Day in Month','No of Days Worked'];
        $head_int = array_merge($head_int, PayrollRuleForCsv::orderBy('id')->pluck('key')->toArray());
        $head_int = array_merge($head_int, ['SL(Op Bal)','CL (Op Bal)','PL (Op Bal)','CME SL','CME CL','CME PL','SL Availed(utilised)','CL Availed','PL Availed(utilised)','Bal c/f SL','Bal c/f CL','Bal c/f PL']);    
        Schema::create('payslip_csv_data', function (Blueprint $table) use ($head_string, $head_int) {
            $table->bigIncrements('id');
            $table->integer('payslip_csv_month_id');
            $table->integer('user_id');
            foreach($head_string as $key)
            {
                $key = preg_replace('/[\(\)\/]/', null, $key);
                $key = preg_replace('/[^a-zA-Z0-9_]/', '_', $key);
                $key = strtolower($key);
                $table->string($key)->nullable();
            }
            foreach($head_int as $key)
            {
                $key = preg_replace('/[\(\)\/]/', null, $key);
                $key = preg_replace('/[^a-zA-Z0-9_]/', '_', $key);
                $key = strtolower($key);
                $table->decimal($key, 10, 2)->nullable();
            }
        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payslip_csv_data');
    }
}
