<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedbackQuestionDesignationTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feedback_question_designation_templates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('feedback_question_template_id');
            $table->integer('designation_id');
            $table->timestamps();
            $table->softDeletes();
            $table->index('designation_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feedback_question_designation_templates');
    }
}
