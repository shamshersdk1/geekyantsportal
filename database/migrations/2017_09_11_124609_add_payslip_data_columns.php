<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPayslipDataColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payslip_data', function(Blueprint $table)
        {
            $table->string('pan_no')->nullable();
            $table->string('date_of_birth')->nullable();
            $table->string('date_of_joining')->nullable();
            $table->string('designation')->nullable();
            $table->string('gender')->nullable();
            $table->string('bank_account_no')->nullable();
            $table->string('bank_ifsc')->nullable();
            $table->string('pf_no')->nullable();
            $table->string('uan_no')->nullable();
            $table->string('esi_no')->nullable();
            $table->string('month')->nullable();
            $table->string('year')->nullable();
            $table->string('working_day_month')->nullable();
            $table->string('days_worked')->nullable();
            $table->string('basic')->nullable();
            $table->string('hra')->nullable();
            $table->string('conveyance_allowance')->nullable();
            $table->string('car_allowance')->nullable();
            $table->string('medical_allowance')->nullable();
            $table->string('food_allowance')->nullable();
            $table->string('special_allowance')->nullable();
            $table->string('gross_salary')->nullable();
            $table->string('pf_12_employee')->nullable();
            $table->string('esi_1_75_employee')->nullable();
            $table->string('vpf')->nullable();
            $table->string('professional_tax')->nullable();
            $table->string('food_deduction')->nullable();
            $table->string('medical_insurance')->nullable();
            $table->string('tds')->nullable();
            $table->string('loan')->nullable();
            $table->string('total_deduction_month')->nullable();
            $table->string('net_salary_month')->nullable();
            $table->string('arrear_salary')->nullable();
            $table->string('annual_bonus')->nullable();
            $table->string('qtr_variable_bonus')->nullable();
            $table->string('performance_bonus')->nullable();
            $table->string('non_cash_incentive')->nullable();
            $table->string('gross_earnings')->nullable();
            $table->string('amount_payable')->nullable();
            $table->string('sl_op_bal')->nullable();
            $table->string('cl_op_bal')->nullable();
            $table->string('pl_op_bal')->nullable();
            $table->string('cme_sl')->nullable();
            $table->string('cme_cl')->nullable();
            $table->string('cme_pl')->nullable();
            $table->string('sl_availed')->nullable();
            $table->string('cl_availed')->nullable();
            $table->string('pl_availed')->nullable();
            $table->string('bal_cf_sl')->nullable();
            $table->string('bal_cf_cl')->nullable();
            $table->string('bal_cf_pl')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payslip_data', function(Blueprint $table)
        {
            $table->dropColumn(['pan_no','date_of_birth','date_of_joining','designation','gender','bank_account_no','bank_ifsc','pf_no','uan_no','esi_no','month','year','working_day_month','days_worked','basic','hra','conveyance_allowance','car_allowance','medical_allowance','food_allowance','special_allowance','gross_salary','pf_12_employee','esi_1_75_employee','vdf','professional_tax','food_deduction','medical_insurance','tds','loan','total_deduction_month','net_salary_month','arrear_salary','annual_bonus','qtr_variable_bonus','performance_bonus','non_cash_incentive','gross_earnings','amount_payable','sl_op_bal','cl_op_bal','pl_op_bal','cme_sl','cme_cl','cme_pl','sl_availed','cl_availed','pl_availed','bal_cf_sl','bal_cf_cl','bal_cf_pl']);
        });
    }
}
