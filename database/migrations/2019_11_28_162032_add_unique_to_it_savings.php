<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUniqueToItSavings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('it_savings', function (Blueprint $table) {
            $table->unique(['financial_year_id', 'user_id'])->change();
        });
        // Schema::table('it_saving_months', function (Blueprint $table) {
        //     $table->unique(['month_id','financial_year_id', 'user_id'])->change();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
