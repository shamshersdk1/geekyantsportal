<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableOnSiteBonusAddDateColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('on_site_bonuses', function (Blueprint $table) {
            $table->date('date')->after('type');          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('on_site_bonuses', function (Blueprint $table) {
            $table->dropColumn('date');
        });
    }
}
