<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestApprovalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_approvals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->longText('description');
            $table->longText('comment');
            $table->enum('status', ["pending", "approved","rejected"]);
            $table->string('notify_to');
            $table->date('reminder_date');
            $table->timestamps();
            $table->softDeletes();
            $table->index('user_id');
            $table->index('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_approvals');
    }
}
