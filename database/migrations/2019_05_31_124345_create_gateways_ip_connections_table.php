<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGatewaysIpConnectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('gateway_ip_connections', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('gateway_id')->index();
            $table->string('ip_address')->index();
            $table->integer('user_id')->index();
            $table->integer('asset_id')->index();
            $table->integer('count');
            $table->date('date')->index();
            $table->tinyInteger('hr');
            $table->tinyInteger('min');
            $table->index(['date','hr','min']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gateway_ip_connections');
    }
}
