<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedbackGoalReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feedback_goal_reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('reviewer_id');
            $table->integer('user_goal_id');
            $table->integer('feedback_month_id');
            $table->integer('rating')->nullable();
            $table->enum('status', ["pending", "completed","not_applicable"]);
            $table->timestamps();
            $table->softDeletes();
            $table->index('user_id');
            $table->index('reviewer_id');
            $table->index('feedback_month_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feedback_goal_reviews');
    }
}
