<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdditionalWorkDaysBonusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('additional_work_days_bonuses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id');
            $table->enum('type', ["weekend", "national-holiday", "leave-or-work-from-home"]);
            $table->date('from_date');
            $table->date('to_date');
            $table->decimal('amount', 15, 2);
            $table->enum('status', ["pending", "approved", "rejected"])->default('pending');
            $table->longText('notes')->nullable();;
            $table->integer('reference_id')->nullable();
            $table->string('reference_type')->nullable();
            $table->integer('approved_by')->nullable();;
            $table->integer('created_by');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('additional_work_days_bonuses');
    }
}
