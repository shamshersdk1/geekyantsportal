<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectQuotationDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_quotation_milestones', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('project_quotation_id');
            $table->date('delivery_date')->nullable();
            $table->longText('details');
            $table->double('cost')->nullable();
            $table->timeStamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('project_quotation_milestones');
    }
}
