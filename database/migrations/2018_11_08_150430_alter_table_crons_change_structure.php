<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableCronsChangeStructure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crons', function (Blueprint $table) {
            $table->dropColumn(['title','event_term', 'event_type', 'start_date', 'end_date', 'remind_at','mon', 'sub_type', 
            'tue', 'wed', 'thu', 'fri', 'first_working_day', 'last_working_day', ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}