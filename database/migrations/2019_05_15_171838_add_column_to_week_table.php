<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class AddColumnToWeekTable extends Migration
{
 /**
  * Run the migrations.
  *
  * @return void
  */
 public function up()
 {
  Schema::table('weeks', function ($table) {
   $table->integer('is_locked')->after('end_date')->nullable();
   $table->timestamp('locked_at')->after('is_locked')->nullable();
  });
 }

 /**
  * Reverse the migrations.
  *
  * @return void
  */
 public function down()
 {
  //
 }
}
