<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableSalaryUserData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salary_user_data', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('month_id');
            $table->integer('user_id');
            $table->integer('employee_id');
            $table->integer('appraisal_id');
            $table->enum('status', ['pending','hold','completed']);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salary_user_data');
    }
}
