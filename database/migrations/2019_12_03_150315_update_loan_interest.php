<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateLoanInterest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('loan_interests', function (Blueprint $table) {
            $table->dropColumn('loan_id');
            $table->renameColumn('outstanding_amount', 'total_outstanding_amount');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('loan_interests', function (Blueprint $table) {
            $table->integer('loan_id')->after('id');
            $table->renameColumn('total_outstanding_amount', 'outstanding_amount');
        });
    }
}
