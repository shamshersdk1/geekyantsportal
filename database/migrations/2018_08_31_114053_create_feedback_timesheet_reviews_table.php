<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedbackTimesheetReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feedback_timesheet_reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('reviewer_id');
            $table->integer('feedback_month_id');
            $table->float('billed_hours', 6, 2 )->default(0);
            $table->float('tl_hours', 6, 2 )->default(0);
            $table->float('user_hours', 6, 2 )->default(0);
            $table->timestamps();
            $table->softDeletes();
            $table->index('user_id');
            $table->index('reviewer_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feedback_timesheet_reviews');
    }
}
