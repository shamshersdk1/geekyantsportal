<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersTableAddFinanceInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function(Blueprint $table)
		{	
            $table->string('gender')->nullable();
            $table->string('pan')->nullable();
            $table->string('bank_ac_no')->nullable();
            $table->string('bank_ifsc_code')->nullable();
            $table->string('pf_no')->nullable();
            $table->string('uan_no')->nullable();
            $table->string('esi_no')->nullable();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropColumn(['gender', 'pan', 'bank_ac_no', 'bank_ifsc_code', 'pf_no', 'uan_no', 'esi_no']);
    }
}
