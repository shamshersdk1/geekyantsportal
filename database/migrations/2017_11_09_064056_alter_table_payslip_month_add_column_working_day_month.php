<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePayslipMonthAddColumnWorkingDayMonth extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('payslip_month', function (Blueprint $table) {
            $table->string('working_day_month')->after('file_path')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('payslip_month', function (Blueprint $table) {
            $table->dropColumn('working_day_month');
        });
    }
}
