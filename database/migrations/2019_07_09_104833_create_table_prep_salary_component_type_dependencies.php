<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePrepSalaryComponentTypeDependencies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prep_salary_component_type_dependencies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('component_id');
            $table->integer('dependent_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('prep_salary_component_type_dependencies');
    }
}
