<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnStatusToPrepFoodDeductions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('prep_food_deductions', function(Blueprint $table)
        {
            $table->enum('status', ['open', 'processing','completed']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('prep_food_deductions', function(Blueprint $table) {
            $table->dropColumn('status');
        });
    }
}
