<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableProjectInvoice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id');
            $table->date('from_date');
            $table->date('to_date');
            $table->enum('type', ["fixed", "recurring"]);
            $table->enum('term', ["weekly", "bi-weekly", "monthly", "custom"])->nullable();
            $table->enum('status', ["pending", "completed", "rejected"])->default('pending');;
            $table->integer('reviewer_id');
            $table->boolean('reminder_sent')->default('0');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_invoices');

    }
}
