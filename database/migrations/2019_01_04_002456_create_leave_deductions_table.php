<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeaveDeductionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leave_deductions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('leave_id');
            $table->integer('loss_of_pay')->default(0);
            $table->integer('paid_leave')->default(0);
            $table->integer('sick_leave')->default(0);
            $table->integer('other_paid')->default(0);
            $table->enum('status', ['pending', 'processed'])->default('pending');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leave_deductions');
    }
}
