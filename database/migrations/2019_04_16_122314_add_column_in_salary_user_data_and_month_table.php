<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class AddColumnInSalaryUserDataAndMonthTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('salary_user_data', function ($table) {
            $table->integer('monthly_gross_salary')->after('days_worked');
            $table->integer('monthly_variable_bonus')->after('monthly_gross_salary')->nulllable();
            $table->integer('annual_gross_salary')->after('monthly_variable_bonus');
        });
        Schema::table('months', function ($table) {
            $table->integer('month_number')->after('month');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('salary_user_data', function ($table) {
            $table->dropColumn(['monthly_gross_salary', 'monthly_variable_bonus', 'annual_gross_salary']);
        });
        Schema::table('months', function ($table) {
            $table->dropColumn(['month_number']);
        });
    }
}
