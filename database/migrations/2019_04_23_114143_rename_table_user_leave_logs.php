<?php

use Illuminate\Database\Migrations\Migration;

class RenameTableUserLeaveLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('user_leave_logs', 'user_leave_transactions');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
