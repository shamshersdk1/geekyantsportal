<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayslipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payslips', function (Blueprint $table) {
            $table->increments('id');
            $table->string('employee_number');
            $table->string('employee_name');
            $table->enum('gender', ['M', 'F'])->nullable();
            $table->string('payroll_month');
            $table->string('status')->nullable();
            $table->string('status_description')->nullable();
            $table->integer('actual_payable_days')->nullable();
            $table->integer('working_days')->nullable();
            $table->integer('loss_of_pay_days')->nullable();
            $table->integer('days_payable')->nullable();
            $table->integer('basic')->nullable();
            $table->integer('transport_allowance')->nullable();
            $table->integer('car_allowance')->nullable();
            $table->integer('hra')->nullable();
            $table->integer('medical_reimbursement')->nullable();
            $table->integer('food_allowance')->nullable();
            $table->integer('special_allowance')->nullable();
            $table->integer('telephone_allowance')->nullable();
            $table->integer('perfomance_bonus')->nullable();
            $table->integer('annual_bonus')->nullable();
            $table->integer('grossa')->nullable();
            $table->integer('gross_less_bonus')->nullable();
            $table->integer('pf_employee')->nullable();
            $table->integer('pf_employer')->nullable();
            $table->integer('pf_other_charges')->nullable();
            $table->integer('arrear_pf_employee')->nullable();
            $table->integer('arrear_pf_employer')->nullable();
            $table->integer('arrear_pf_other_charges')->nullable();
            $table->integer('total_contributionsb')->nullable();
            $table->integer('professional_tax')->nullable();
            $table->integer('special_medical_insurance')->nullable();
            $table->integer('deduction_food_allowance')->nullable();
            $table->integer('total_income_tax')->nullable();
            $table->integer('loan_emi')->nullable();
            $table->integer('total_deductionsc')->nullable();
            $table->integer('net_payd_a_b_c')->nullable();
            $table->integer('total_reimbursementse')->nullable();
            $table->integer('total_net_payde')->nullable();
            $table->integer('gross_earning')->nullable();
            $table->integer('total_deduction')->nullable();
            $table->integer('net_pay_check')->nullable();
            $table->integer('arrear_deduction')->nullable();
            $table->integer('adjusted_gross_salary')->nullable();
            $table->integer('special_allowance_adjusted')->nullable();
            $table->integer('total_deduction_payslip')->nullable(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payslips');
    }
}
