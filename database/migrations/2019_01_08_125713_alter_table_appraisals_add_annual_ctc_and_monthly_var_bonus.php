<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableAppraisalsAddAnnualCtcAndMonthlyVarBonus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('appraisals', function (Blueprint $table) {
            $table->integer('monthly_var_bonus')->after('effective_date')->default(0);
            $table->integer('annual_bonus')->after('effective_date')->default(0);
            $table->integer('annual_gross_salary')->after('effective_date')->default(0);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
