<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableMonthsAddYear extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('months', function (Blueprint $table) {
            $table->integer('year')->after('month');
            $table->integer('payroll_rule')->after('year');
            $table->integer('bonus_check')->after('payroll_rule');
            $table->integer('loan_check')->after('bonus_check');
            $table->integer('feedback_review_check')->after('loan_check');
            $table->integer('lop_check')->after('feedback_review_check');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
