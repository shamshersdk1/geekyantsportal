<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableStatusInPrepSalary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('prep_salary', function (Blueprint $table) {
            $table->dropColumn('status');
          
        });
         Schema::table('prep_salary', function (Blueprint $table) {
          $table->enum('status', ['open', 'in_progress','processed','closed','discarded'])->after('month_id');
          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('prep_salary', function (Blueprint $table) {
            $table->dropColumn('status');
          
        });
    }
}
