<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdatePrepAmountColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('prep_adhoc_payment_components', function (Blueprint $table) {
            $table->dropColumn('value');
        });
        Schema::table('prep_adhoc_payment_components', function (Blueprint $table) {
            $table->decimal('value', 12, 2)->after('key');
        });

        Schema::table('prep_appraisal_bonuses', function (Blueprint $table) {
            $table->dropColumn('value');
        });
        Schema::table('prep_appraisal_bonuses', function (Blueprint $table) {
            $table->decimal('value', 12, 2)->after('appraisal_bonus_id');
        });

        Schema::table('prep_appraisal_components', function (Blueprint $table) {
            $table->dropColumn('value');
        });
        Schema::table('prep_appraisal_components', function (Blueprint $table) {
            $table->decimal('value', 12, 2)->after('appraisal_component_id');
        });

        Schema::table('prep_current_gross_items', function (Blueprint $table) {
            $table->dropColumn('value');
        });
        Schema::table('prep_current_gross_items', function (Blueprint $table) {
            $table->decimal('value', 12, 2)->after('key');
        });

        Schema::table('prep_food_deductions', function (Blueprint $table) {
            $table->dropColumn('value');
        });
        Schema::table('prep_food_deductions', function (Blueprint $table) {
            $table->decimal('value', 12, 2)->after('food_deduction_id');
        });

        Schema::table('prep_gross_paid_items', function (Blueprint $table) {
            $table->dropColumn('value');
        });
        Schema::table('prep_gross_paid_items', function (Blueprint $table) {
            $table->decimal('value', 12, 2)->after('key');
        });

        Schema::table('prep_gross_to_be_paid_items', function (Blueprint $table) {
            $table->dropColumn('value');
        });
        Schema::table('prep_gross_to_be_paid_items', function (Blueprint $table) {
            $table->decimal('value', 12, 2)->after('key');
        });

        Schema::table('prep_insurances', function (Blueprint $table) {
            $table->dropColumn('value');
        });
        Schema::table('prep_insurances', function (Blueprint $table) {
            $table->decimal('value', 12, 2)->after('insurance_id');
        });

        Schema::table('prep_it_savings_components', function (Blueprint $table) {
            $table->dropColumn('value');
        });
        Schema::table('prep_it_savings_components', function (Blueprint $table) {
            $table->decimal('value', 12, 2)->after('key');
        });

        Schema::table('prep_tds_components', function (Blueprint $table) {
            $table->dropColumn('value');
        });
        Schema::table('prep_tds_components', function (Blueprint $table) {
            $table->decimal('value', 12, 2)->after('key');
        });

        Schema::table('prep_vpfs', function (Blueprint $table) {
            $table->dropColumn('value');
        });
        Schema::table('prep_vpfs', function (Blueprint $table) {
            $table->decimal('value', 12, 2)->after('vpfs_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('prep_adhoc_payment_components', function (Blueprint $table) {
            $table->dropColumn('value');
        });
        Schema::table('prep_adhoc_payment_components', function (Blueprint $table) {
            $table->decimal('value', 10, 2)->after('key');
        });

        Schema::table('prep_appraisal_bonuses', function (Blueprint $table) {
            $table->dropColumn('value');
        });
        Schema::table('prep_appraisal_bonuses', function (Blueprint $table) {
            $table->integer('value')->after('appraisal_bonus_id');
        });

        Schema::table('prep_appraisal_components', function (Blueprint $table) {
            $table->dropColumn('value');
        });
        Schema::table('prep_appraisal_components', function (Blueprint $table) {
            $table->integer('value')->change();
        });

        Schema::table('prep_current_gross_items', function (Blueprint $table) {
            $table->dropColumn('value');
        });
        Schema::table('prep_current_gross_items', function (Blueprint $table) {
            $table->decimal('value', 8, 2)->change();
        });

        Schema::table('prep_food_deductions', function (Blueprint $table) {
            $table->dropColumn('value');
        });
        Schema::table('prep_food_deductions', function (Blueprint $table) {
            $table->integer('value')->change();
        });

        Schema::table('prep_gross_paid_items', function (Blueprint $table) {
            $table->dropColumn('value');
        });
        Schema::table('prep_gross_paid_items', function (Blueprint $table) {
            $table->decimal('value', 8, 2)->change();
        });

        Schema::table('prep_gross_to_be_paid_items', function (Blueprint $table) {
            $table->dropColumn('value');
        });
        Schema::table('prep_gross_to_be_paid_items', function (Blueprint $table) {
            $table->decimal('value', 8, 2)->change();
        });

        Schema::table('prep_insurances', function (Blueprint $table) {
            $table->dropColumn('value');
        });
        Schema::table('prep_insurances', function (Blueprint $table) {
            $table->integer('value')->change();
        });

        Schema::table('prep_it_savings_components', function (Blueprint $table) {
            $table->dropColumn('value');
        });
        Schema::table('prep_it_savings_components', function (Blueprint $table) {
            $table->string('value')->change();
        });

        Schema::table('prep_tds_components', function (Blueprint $table) {
            $table->dropColumn('value');
        });
        Schema::table('prep_tds_components', function (Blueprint $table) {
            $table->decimal('value', 8, 2)->change();
        });

        Schema::table('prep_vpfs', function (Blueprint $table) {
            $table->dropColumn('value');
        });
        Schema::table('prep_vpfs', function (Blueprint $table) {
            $table->integer('value')->change();
        });
    }
}
