<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterBonusesTableAddColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bonuses', function (Blueprint $table) {
            $table->dropColumn('status');
            $table->dropColumn('amount');

        });
        Schema::table('bonuses', function (Blueprint $table) {
            $table->double('amount', 15, 8)->after('date')->nullable();
            $table->enum('status', ["pending", "approved", "rejected"])->after('notes');
            $table->integer('bonus_request_id')->after('approved_by');
            $table->enum('type', ["onsite", "additional", "referral", "techtalk"])->after('approved_by');
            $table->string('sub_type')->after('approved_by')->nullable();
            $table->string('title')->after('approved_by')->nullable();
            $table->integer('referral_for')->after('approved_by')->nullable();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bonuses', function (Blueprint $table) {
            $table->dropColumn('bonus_request_id');
            $table->dropColumn('type');
            $table->dropColumn('sub_type');
            $table->dropColumn('title');
            $table->dropColumn('referral_for');

        });

    }
}
