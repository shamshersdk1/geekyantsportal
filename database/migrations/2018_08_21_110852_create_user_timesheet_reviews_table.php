<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTimesheetReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_timesheet_reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_timesheet_id');
            $table->integer('approver_id');
            $table->float('approved_duration', 4, 2 )->default(0);
            $table->string('approval_comments',512);
            $table->timestamps();
            $table->softDeletes();
            $table->index('user_timesheet_id');
            $table->index('approver_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_timesheet_reviews');
    }
}
