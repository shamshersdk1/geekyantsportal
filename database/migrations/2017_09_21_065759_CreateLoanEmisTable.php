<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoanEmisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_emis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('loan_id');
            $table->decimal('amount', 15, 2);
            $table->enum('status', ['pending','paid'])->default('pending');
            $table->date('due_date');
            $table->date('paid_on')->nullable();
            $table->string('reference_no')->nullable();
            $table->string('reference_id')->nullable();
            $table->longText('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan_emis');
    }
}
