<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnAppraisalBonusIdInLoanEmi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('loan_emis', function(Blueprint $table)
        {
            $table->integer('appraisal_bonus_id')->after('actual_amount')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('loan_emis', function(Blueprint $table)
        {
            $table->dropColumn('appraisal_bonus_id');
        });
        
    }
}
