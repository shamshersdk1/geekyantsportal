<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableExpenseChangeColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('expenses', function(Blueprint $table)
        {
            $table->dropColumn(['payment_status','payment_method','invoice']);
        });

        Schema::table('expenses', function (Blueprint $table) {
            $table->string('invoice_number')->after('amount')->nullable();
            $table->renameColumn('reference_number', 'payment_reference_number');
            $table->integer('payment_method_id')->after('amount')->nullable();
            $table->date('invoice_date')->after('invoice_number')->nullable();
            $table->enum('payment_status',["pending","paid","hold","due_immediate_pending","due_next_cycle_pending","due_immediate_approved","due_next_cycle_approved"])->after('amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('expenses', function (Blueprint $table) {
            $table->dropColumn('invoice_date');
        });
    }
}
