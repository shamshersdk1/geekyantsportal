<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePrepSalaryComponent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prep_salary_components', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('prep_salary_id');
            $table->integer('prep_salary_component_type_id');
            $table->integer('is_generated');
            $table->enum('status',['open','closed'])->default('open');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('prep_salary_component_types');
    }
}
