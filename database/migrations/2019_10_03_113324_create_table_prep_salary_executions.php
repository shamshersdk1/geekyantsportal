<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePrepSalaryExecutions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prep_salary_executions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('prep_salary_id')->index();
            $table->integer('component_id')->index();
            $table->integer('user_id')->index();
            $table->enum('status', ['init', 'in_progress','completed','failed']);
            $table->integer('counter')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('prep_salary_executions');
    }
}
