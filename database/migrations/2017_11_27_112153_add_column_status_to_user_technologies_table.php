<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnStatusToUserTechnologiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('user_technologies', function (Blueprint $table) {
             $table->enum('status', ['pending','approved','rejected'])->default('pending')->after('technology_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_technologies', function (Blueprint $table) {
            $table->dropColumn(['status']);
        });
    }
}
