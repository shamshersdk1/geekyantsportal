<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnNameTechnologies extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('technologies', function(Blueprint $table)
		{
		    $table->dropColumn('url');
		});
		Schema::table('technologies', function(Blueprint $table)
		{
		    $table->string('slug')->unique()->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('technologies', function(Blueprint $table)
		{
		    $table->dropColumn('slug');
		});
	}

}
