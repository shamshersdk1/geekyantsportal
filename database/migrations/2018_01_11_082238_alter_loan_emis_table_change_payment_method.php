<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLoanEmisTableChangePaymentMethod extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('loan_emis', function ($table) {
            $table->dropColumn(['reference_id', 'reference_no']);
            $table->string('method');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('loan_emis', function ($table) {
            $table->dropColumn('method');
            $table->string('reference_id')->nullable();
            $table->string('reference_no');
        });
    }
}
