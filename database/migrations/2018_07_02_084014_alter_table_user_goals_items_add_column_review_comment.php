<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUserGoalsItemsAddColumnReviewComment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_goal_items', function (Blueprint $table) {
            $table->string('review_comment')->after('comment')->nullable();
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_goal_items', function (Blueprint $table) {
            $table->dropColumn(['review_comment']);
          });
    }
}
