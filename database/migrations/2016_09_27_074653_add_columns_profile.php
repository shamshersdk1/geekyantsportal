<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsProfile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profile', function(Blueprint $table)
        {   
            $table->date('join_date')->nullable();
            $table->date('confirmation_date')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('employee_number')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropColumn('join_date');
        Schema::dropColumn('confirmation_date');
        Schema::dropColumn('user_id');
        Schema::dropColumn('employee_number');
    }
}
