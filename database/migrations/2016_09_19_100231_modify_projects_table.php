<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects', function(Blueprint $table)
        {   
            $table->dropColumn('status');
            $table->dropColumn('notes');
        });
        Schema::table('projects', function(Blueprint $table)
        {   
            $table->string('status')->nullabe();
            $table->string('notes', 512)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropColumn('status');
        Schema::dropColumn('notes');
    }
}
