<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTechnologiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('technologies', function(Blueprint $table)
		{	
			$table->increments('id');
			$table->string('name')->index();
			$table->string('logo');
			$table->string('url');
			$table->string('short_description');
			$table->string('detailed_description');
			$table->boolean('is_featured');
			$table->timestamp('created_at');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('technologies');
	}

}
