<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAdvanceSalary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advance_salary', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('date');
            $table->double('amount',10,2);
            $table->string('notes')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('approved_by')->nullable();
            $table->enum('status',['pending','in_progress','completed','rejected'])->default('pending');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('advance_salary_deductions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('advance_salary_id');
            $table->integer('month_id');
            $table->double('amount',10,2);
            $table->integer('created_by')->nullable();
            $table->integer('approved_by')->nullable();
            $table->enum('status',['pending','approved','rejected'])->default('pending');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advance_salary');
    }
}
