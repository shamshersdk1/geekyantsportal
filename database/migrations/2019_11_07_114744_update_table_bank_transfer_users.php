<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTableBankTransferUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bank_transfer_users', function (Blueprint $table) {
            $table->renameColumn('actual_amount', 'transaction_amount')->nullable();
            $table->renameColumn('reference_number', 'bank_transaction_id')->nullable();
            $table->date('txn_posted_date')->after('status')->nullable();
            $table->string('bank_acct_number')->after('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bank_transfer_users', function (Blueprint $table) {
            $table->renameColumn('transaction_amount', 'actual_amount')->nullable();
            $table->renameColumn('bank_transaction_id', 'reference_number')->nullable();
            $table->dropColumn(['txn_posted_date', 'bank_acct_number']);
        });
    }
}
