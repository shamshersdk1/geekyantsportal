<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableSalaryUserDataAddColumnMonthCtcMonthlyVariableBonusAnnualBonusForTheMonth extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('salary_user_data', function (Blueprint $table) {
            $table->string('status')->after('annual_bonus')->default('pending');
            $table->decimal('tds_annual')->after('annual_bonus')->default(0);
            $table->decimal('tds_for_the_month')->after('annual_bonus')->default(0);
            $table->decimal('annual_monthly_variable_bonus')->after('annual_bonus')->default(0);
            $table->decimal('annual_ctc')->after('annual_bonus')->default(0);
            $table->decimal('annual_bonus_for_the_month')->after('annual_bonus')->default(0);
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('salary_user_data', function (Blueprint $table) {
            $table->dropColumn(['annual_bonus','annual_monthly_variable_bonus','annual_ctc','annual_bonus_for_the_month','monthly_variable_bonus','monthly_ctc']);
        }); 
    }
}
