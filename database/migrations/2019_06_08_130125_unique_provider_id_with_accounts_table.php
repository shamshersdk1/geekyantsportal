<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UniqueProviderIdWithAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('server_provider_accounts', function (Blueprint $table) {
            $table->unique(['provider_id','username'], 'unique_provider_id_username');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('server_provider_accounts', function (Blueprint $table) {
            $table->dropUnique('unique_provider_id_username');
        });
    }
}
