<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class AlterTableLeaveDeductionMonthAddColumnsApproverIdAndComments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leave_deduction_month', function ($table) {
            $table->integer('approver_id')->after('status')->nullable();
            $table->string('comments')->after('status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function ($table) {
            $table->dropColumn('approver_id');
            $table->dropColumn('comments');
        });
    }
}
