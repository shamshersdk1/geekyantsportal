<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RefactorUsersProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_profile', function(Blueprint $table)
        {
            $table->dropColumn(['join_date', 'confirmation_date', 'employee_number']);
        });

        Schema::table('users', function(Blueprint $table)
        {
            $table->date('joining_date')->nullable();
            $table->date('confirmation_date')->nullable();
            $table->string('employee_id')->nullable()->unique();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table)
        {
            $table->dropColumn(['joining_date', 'confirmation_date', 'employee_id']);
        });
    }
}
