<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableAdditionalWorkingBonusRemoveFromAndToDateAddDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        // Schema::table('additional_work_days_bonuses', function (Blueprint $table) {
        //     $table->dropColumn('from_date');
        //     $table->dropColumn('to_date');
        // });

        Schema::table('additional_work_days_bonuses', function (Blueprint $table) {
            $table->date('date')->after('type')->nullable();          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('additional_work_days_bonuses', function (Blueprint $table) {
            $table->dropColumn('date');
        });
    }
}
