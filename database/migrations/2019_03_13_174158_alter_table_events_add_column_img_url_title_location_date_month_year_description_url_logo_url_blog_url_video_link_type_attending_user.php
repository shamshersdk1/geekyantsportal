<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableEventsAddColumnImgUrlTitleLocationDateMonthYearDescriptionUrlLogoUrlBlogUrlVideoLinkTypeAttendingUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->string('img_url')->after('id');
            $table->string('title')->after('img_url');
            $table->string('location')->after('title');
            $table->date('date')->after('location');
            $table->string('month')->after('date');
            $table->integer('year')->after('month');
            $table->string('description')->after('year');
            $table->string('url')->after('description');
            $table->string('logo_url')->after('url');
            $table->string('blog_url')->after('logo_url');
            $table->string('video_link')->after('blog_url');
            $table->enum('type',['upcoming','past'])->after('video_link');
            $table->integer('attending_user')->after('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
