<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAppraisalBonuses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appraisal_bonuses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('appraisal_id');
            $table->integer('month_id');
            $table->date('date');
            $table->double('amount',10,2);
            $table->enum('type',['confirmation','monthly_valriable','annual']);
            $table->enum('status',['pending','hold','approved','rejected'])->default('pending');
            $table->integer('transaction_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appraisal_bonuses');
    }
}
