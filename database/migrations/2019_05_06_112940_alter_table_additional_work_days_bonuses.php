<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableAdditionalWorkDaysBonuses extends Migration
{
 /**
  * Run the migrations.
  *
  * @return void
  */
 public function up()
 {
  Schema::table('additional_work_days_bonuses', function (Blueprint $table) {
   $table->dropColumn('employee_id');
   $table->dropColumn('from_date');
   $table->dropColumn('to_date');
   $table->dropColumn('reference_id');
   $table->dropColumn('reference_type');

   $table->integer('user_id')->after('id')->nullable();
   $table->integer('project_id')->after('date')->nullable();
   $table->integer('duration')->after('status')->nullable();
   $table->enum('redeem_type', ['encash', 'comp-off'])->default('encash');

  });
 }

 /**
  * Reverse the migrations.
  *
  * @return void
  */
 public function down()
 {
  //
 }
}
