<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterHireEmailTableAddCommentAndModifyStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hire_email', function (Blueprint $table) {
            $table->renameColumn('status', 'comment')->nullable();
        });
        Schema::table('hire_email', function (Blueprint $table) {
            $table->enum('status',['pending', 'sent', 'rejected'])->after('documents');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hire_email', function (Blueprint $table) {
          $table->dropColumn(['status']);
        });
        Schema::table('hire_email', function (Blueprint $table) {
            $table->renameColumn('comment', 'status');
        });
    }
}
