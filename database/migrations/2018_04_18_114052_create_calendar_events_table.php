<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalendarEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendar_events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('calendar_event_id');
            $table->string('name');
            $table->dateTime('start_date_time');
            $table->dateTime('end_date_time');
            $table->integer('reference_id');
            $table->string('reference_type');
            $table->enum('status', ["confirmed", "tentative", "cancelled"]);
            $table->longText('invites')->nullable();
            $table->longText('response')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calendar_events');
    }
}
