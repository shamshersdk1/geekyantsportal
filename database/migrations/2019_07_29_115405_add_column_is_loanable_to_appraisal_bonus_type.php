<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIsLoanableToAppraisalBonusType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('appraisal_bonus_type', function(Blueprint $table)
        {
            $table->tinyInteger('is_loanable')->after('description')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('appraisal_bonus_type', function($table) {
            $table->dropColumn('is_loanable');
        });
    }
}
