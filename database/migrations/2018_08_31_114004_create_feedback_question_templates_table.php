<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedbackQuestionTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feedback_question_templates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('question');
            $table->integer('role_type');
            $table->boolean('status');
            $table->timestamps();
            $table->softDeletes();
            $table->index('role_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feedback_question_templates');
    }
}
