<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTableBankTransfer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bank_transfer_users', function (Blueprint $table) {
            $table->string('comment')->after('reference_number')->nullable();
        });
        Schema::table('bank_transfer_hold_users', function (Blueprint $table) {
            $table->string('comment')->after('amount')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bank_transfer_users', function (Blueprint $table) {
            $table->dropColumn('comment');
        });
        Schema::table('bank_transfer_hold_users', function (Blueprint $table) {
            $table->dropColumn('comment');
        });
    }
}
