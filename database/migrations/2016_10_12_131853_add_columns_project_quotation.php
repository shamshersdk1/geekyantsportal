<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsProjectQuotation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_quotation', function(Blueprint $table)
        {   
            $table->enum('status', ['open', 'in_progress', 'completed', 'lost', 'cancelled'])->default('in_progress');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_quotation', function(Blueprint $table)
        {   
            $table->dropColumn(['status']);
        });
    }
}
