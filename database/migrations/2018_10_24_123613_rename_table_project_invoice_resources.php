<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class RenameTableProjectInvoiceResources extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('project_invoice_resources', 'billing_schedule_resources');
        Schema::table('billing_schedule_resources', function ($table) {
            $table->renameColumn('project_invoice_id', 'billing_schedule_id');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
