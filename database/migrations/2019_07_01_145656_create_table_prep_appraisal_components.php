<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePrepAppraisalComponents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prep_appraisal_components',function(Blueprint $table){
            $table->increments('id');
            $table->integer('prep_appraisal_id')->index();
            $table->integer('appraisal_component_id')->index();
            $table->integer('value');
            $table->unique(['prep_appraisal_id','appraisal_component_id'],'prep_appraisal_component_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('prep_appraisal_components');

    }
}
