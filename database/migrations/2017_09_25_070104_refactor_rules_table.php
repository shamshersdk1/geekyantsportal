<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RefactorRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payrole_group_rule', function (Blueprint $table) {
            $table->dropColumn('calculation_value');
        });

        Schema::table('payrole_group_rule', function (Blueprint $table) {
            $table->double('calculation_value', 30, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payrole_group_rule', function (Blueprint $table) {
            $table->dropColumn('calculation_value');
        });
    }
}
