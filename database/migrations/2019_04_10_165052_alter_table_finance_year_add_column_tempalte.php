<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableFinanceYearAddColumnTempalte extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('financial_years', function (Blueprint $table) {
            $table->date('end_date')->after('year');
            $table->date('start_date')->after('year');
            $table->string('it_saving_template')->after('is_locked');
            $table->decimal('standard_deduction')->after('is_locked');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
