<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserLeaveLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_leave_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_leave_balance_id');
            $table->integer('leave_type_id');
            $table->integer('days');
            $table->string('reference_type', 125);
            $table->integer('reference_id');
            $table->enum('status', ['pending', 'approved'])->default('pending');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_leave_logs');
    }
}
