<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLoanEmiTableAddActualAmount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('loan_emis', function (Blueprint $table) {
            $table->dropColumn(['status']);
        });
        
        Schema::table('loan_emis', function (Blueprint $table) {
            $table->decimal('actual_amount', 15, 2)->after('amount')->nullable()->default(null);
            $table->enum('status', ['pending', 'processing', 'paid', 'rejected'])->after('actual_amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('loan_emis', function (Blueprint $table) {
          $table->dropColumn(['actual_amount']);
          $table->dropColumn(['status']);
      });
    }
}
