<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTablePrepWorkingDays extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('prep_working_days', function (Blueprint $table) {
            $table->renameColumn('working_days', 'company_working_days')->nullable();
            $table->renameColumn('days_worked', 'user_worked_days')->nullable();
            $table->integer('user_working_days')->after('working_days');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('prep_working_days', function (Blueprint $table) {
            $table->renameColumn('company_working_days', 'working_days')->nullable();
            $table->renameColumn('user_worked_days', 'days_worked')->nullable();
            $table->dropColumn('user_working_days');
        });
    }
}
