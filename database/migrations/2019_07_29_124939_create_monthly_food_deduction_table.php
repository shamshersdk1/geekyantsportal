<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMonthlyFoodDeductionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('food_deductions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('month_id')->index();
            $table->integer('user_id');
            $table->decimal('actual_amount',6,2);
            $table->decimal('amount',6,2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
