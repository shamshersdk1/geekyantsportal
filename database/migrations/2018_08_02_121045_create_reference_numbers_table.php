<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferenceNumbersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reference_numbers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('year');
            $table->integer('month');
            $table->integer('sl_no');
            $table->string('for','255');
            $table->longText('info');
            $table->string('value','255')->unique();
            $table->string('signed_by','255');
            $table->integer('document_type_id');
            $table->enum('status', ["new", "cancelled"]);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reference_numbers');
    }
}
