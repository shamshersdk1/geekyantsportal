<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableLoanInterests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_interests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('loan_id')->index();
            $table->integer('user_id')->index();
            $table->integer('month_id')->index();
            $table->integer('financial_year_id');
            $table->decimal('outstanding_amount', 12, 2)->nullable();
            $table->decimal('interest', 12, 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan_interests');
    }
}
