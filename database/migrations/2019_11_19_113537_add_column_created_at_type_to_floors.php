<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCreatedAtTypeToFloors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('floors', function (Blueprint $table) {
            $table->enum('type', ['building_1', 'building_2','building_3','building_4'])->after('floor_number');
            $table->timestamps(); 
        });
        Schema::table('floors', function (Blueprint $table) {
            $table->unique(['floor_number', 'type'])->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('floors', function (Blueprint $table) {
            $table->dropColumn('type');
            $table->timestamps();
        });
    }
}
