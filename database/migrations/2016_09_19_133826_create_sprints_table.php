<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSprintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_sprints', function(Blueprint $table)

        {
            $table->increments('id');
            $table->string('title');
            $table->string('description',512);
            $table->string('status')->nullable();
            $table->integer('project_id');
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->softDeletes()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('project_sprints');
    }
}
