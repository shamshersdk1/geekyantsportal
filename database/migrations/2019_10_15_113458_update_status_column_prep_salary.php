<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateStatusColumnPrepSalary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('prep_salary', function (Blueprint $table) {
            $table->dropColumn('status');

        });
        Schema::table('prep_salary', function (Blueprint $table) {
            $table->enum('status', ['open', 'in_progress', 'closed', 'discarded'])->after('month_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('prep_salary', function (Blueprint $table) {
            $table->dropColumn('status');

        });
        Schema::table('prep_salary', function (Blueprint $table) {
            $table->enum('status', ['open', 'in_progress', 'closed'])->after('month_id');

        });
    }
}
