<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class AlterTableSalaryUserData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('salary_user_data', function ($table) {
            $table->integer('annual_monthly_bonus')->after('monthly_gross_salary')->nullable();
            $table->integer('annual_gross_bonus')->after('annual_monthly_bonus')->nullable();
            $table->integer('other_bonus')->after('annual_gross_bonus')->nullable();
            $table->dropColumn(['monthly_variable_bonus', 'annual_bonus', 'annual_bonus_for_the_month']);

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('salary_user_data', function ($table) {
            $table->dropColumn(['annual_monthly_bonus', 'annual_gross_bonus', 'other_bonus']);
        });

    }
}
