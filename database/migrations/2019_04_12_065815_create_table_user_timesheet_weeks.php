<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableUserTimesheetWeeks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_timesheet_weeks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('week_id');
            $table->integer('user_id');
            $table->integer('approved_by')->nullable();
            $table->date('approved_at')->nullable();
            $table->enum('status', ['pending', 'approved'])->default('pending');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
