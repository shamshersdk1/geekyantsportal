<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUserTimesheetExtrasAddColumnProjectId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_timesheet_extras', function (Blueprint $table) {
            $table->integer('project_id')->after('month_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_timesheet_extras', function (Blueprint $table) {
            $table->dropColumn(['project_id']);
        });
    }
}
