<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class AlterTableBonusesAddColumnTransactionIdDropAmount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bonuses', function ($table) {
            $table->integer('transaction_id')->after('updated_at')->nullable();
            $table->dropColumn('amount');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leave_deduction_month', function ($table) {
            $table->dropColumn('transaction_id');
            $table->decimal('amount')->after('id')->nullable();
        });

    }
}
