<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCreatedByToSharedTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shared_tasks', function (Blueprint $table) {
            $table->increments('id')->first();
            $table->integer('created_by');
            $table->integer('task_group_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shared_tasks', function (Blueprint $table) {
            $table->dropColumn(['created_by', 'task_group_id', 'id']);
        });
    }
}
