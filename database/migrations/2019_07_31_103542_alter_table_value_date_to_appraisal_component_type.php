<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableValueDateToAppraisalComponentType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('appraisal_bonuses', function($table) {
            $table->dropColumn('value_date');
        });
         Schema::table('appraisal_bonuses', function(Blueprint $table)
        {
            $table->date('value_date')->after('value')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('appraisal_bonuses', function($table) {
            $table->dropColumn('value_date');
        });
    }
}
