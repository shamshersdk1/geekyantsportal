<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayslipDataItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payslip_data_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('payslip_data_month_id');
            $table->string('tech_talk_bonus')->nullable();
            $table->string('annual_bonus')->nullable();
            $table->string('confirmation_bonus')->nullable();
            $table->string('domestic_onsite_outstation')->nullable();
            $table->string('domestic_onsite_local')->nullable();
            $table->string('international_onsite')->nullable();
            $table->string('extra_working_day')->nullable();
            $table->string('national_holiday_working_bonus')->nullable();
            $table->integer('created_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->index('user_id');
            $table->index('payslip_data_month_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payslip_data_items');
    }
}
