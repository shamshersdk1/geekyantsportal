<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableServerMetaAddUniquePropertyToKeyServerIdAndKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('server_meta', function (Blueprint $table) {
            $table->unique(['server_id', 'key'], 'unique_key_server');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('server_meta', function (Blueprint $table) {
            $table->dropUnique('unique_key_server');
        });
    }
}
