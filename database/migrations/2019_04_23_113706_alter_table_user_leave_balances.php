<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class AlterTableUserLeaveBalances extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_leave_balances', function ($table) {
            $table->integer('leave_type_id')->after('calendar_year_id');
            $table->decimal('available_leave')->after('leave_type_id');
            $table->decimal('allowed_leave')->after('available_leave');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_leave_balances', function ($table) {
            $table->dropColumn(['leave_type_id', 'available_leave', 'allowed_leave']);
        });
    }
}
