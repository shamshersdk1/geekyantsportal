<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldOnsiteWfhMeetingUserAttedanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_attendance', function(Blueprint $table){   
            $table->tinyInteger('is_onsite');
            $table->tinyInteger('is_wfh');
            $table->tinyInteger('is_onsite_meeting');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_attendance', function(Blueprint $table){   
            $table->dropColumn('is_onsite');
            $table->dropColumn('is_wfh');
            $table->dropColumn('is_onsite_meeting');
        });
    }
}
