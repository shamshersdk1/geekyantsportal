<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePayslipDataItemAddColumnsEmployeeId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payslip_data_items', function(Blueprint $table)
		{	
            $table->string('employee_id')->after('user_id');
            $table->index('employee_id');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payslip_data_items', function (Blueprint $table) {
            $table->dropColumn(['employee_id']);
        });
    }
}
