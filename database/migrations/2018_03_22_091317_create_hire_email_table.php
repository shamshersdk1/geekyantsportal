<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHireEmailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hire_email', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',1024);
            $table->string('email');
            $table->string('company');
            $table->string('skype')->nullable();
            $table->string('preferred_timezone')->nullable();
            $table->longText('referred_by');
            $table->longText('requirement');
            $table->string('project_type',512)->nullable();
            $table->string('technologies',512)->nullable();
            $table->string('documents',1024)->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hire_email');
    }
}