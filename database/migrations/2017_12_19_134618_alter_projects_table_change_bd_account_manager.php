<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProjectsTableChangeBdAccountManager extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->dropColumn(['account_manager', 'bd_manager']);
            
        });

        Schema::table('projects', function (Blueprint $table) {
            $table->integer('account_manager_id')->after('project_manager_id')->nullable();
            $table->integer('bd_manager_id')->after('project_manager_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->dropColumn(['account_manager_id', 'bd_manager_id']);
        });
        Schema::table('projects', function (Blueprint $table) {
            $table->string('account_manager')->after('project_manager_id')->nullable();
            $table->string('bd_manager')->after('project_manager_id')->nullable();      
        });
    }
}
