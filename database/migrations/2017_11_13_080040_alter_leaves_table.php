<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLeavesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leaves', function(Blueprint $table)
        {
            $table->dropColumn(['type','days']);
        });
        Schema::table('leaves', function(Blueprint $table)
        {
            $table->enum('type', ['sick', 'paid', 'unpaid','half'])->nullable();
            $table->decimal('days',3,1)->nullable();
            $table->enum('half',['first','second'])->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leaves', function(Blueprint $table)
        {
            $table->dropColumn(['type', 'days', 'half']);
        });
    }
}
