<?php

use Illuminate\Database\Migrations\Migration;

class AlterTableUserLeaveTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_leave_transactions', function ($table) {
            $table->integer('user_id')->after('id');
            $table->integer('calendar_year_id')->after('user_id');
            $table->decimal('calculation_date')->after('leave_type_id');
            $table->dropColumn(['status', 'deleted_at', 'user_leave_balance_id', 'days']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
