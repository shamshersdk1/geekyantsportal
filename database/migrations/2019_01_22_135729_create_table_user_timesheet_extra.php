<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUserTimesheetExtra extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('user_timesheet_extras', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('month_id');
            $table->date('date');
            $table->integer('extra_hours');
            $table->integer('created_by')->default(null);
            $table->integer('approved_id')->default(null);
            $table->enum('status',['pending','approved','rejected'])->default('pending');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_timesheet_extras');
    }
}
