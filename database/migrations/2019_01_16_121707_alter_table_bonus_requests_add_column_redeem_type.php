<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableBonusRequestsAddColumnRedeemType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bonus_requests', function (Blueprint $table) {
            $table->string('redeem_type')->after('sub_type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bonus_requests', function (Blueprint $table) {
            $table->dropColumn(['redeem_type']);
        });
    }
}
