<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableBillingScheduleRemindersChangeCronId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('billing_schedule_reminders', function (Blueprint $table) {
            $table->renameColumn('cron_id', 'project_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('billing_schedule_reminders', function (Blueprint $table) {
            $table->renameColumn('project_id','cron_id');
        });
    }
}
