<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserLeaveBalancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_leave_balances', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('calendar_year_id');
            $table->integer('paid_leave_balance')->default(0);
            $table->integer('sick_leave_balance')->default(0);
            $table->integer('carry_forward')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_leave_balances');
    }
}
