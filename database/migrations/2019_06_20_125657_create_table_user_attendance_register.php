<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUserAttendanceRegister extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_attendance_registers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->date('date');
            $table->dateTime('in_time')->nullable();
            $table->dateTime('out_time')->nullable();
            $table->integer('tick_count')->default(0);
            $table->integer('active_count')->default(0);
            $table->enum('status' , ['on_leave','present_in_office','present_wfh','present_onsite','present_meeting','present_on_leave','unknown'])->default('unknown');
            $table->timestamps();

            $table->index('user_id');
            $table->index('date');
            $table->index('status');
            $table->index(['user_id','date']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('user_attendance_registers');
    }
}
