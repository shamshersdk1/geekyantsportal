<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTimestampsToSharedTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shared_tasks', function (Blueprint $table) {
            $table->timestamps();
            $table->softDeletes();
            $table->boolean('is_removed')->after('task_group_id')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shared_tasks', function (Blueprint $table) {
            $table->dropColumn(['created_at', 'updated_at', 'is_removed']);
            $table->dropSoftDeletes();
        });
    }
}
