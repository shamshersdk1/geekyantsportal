<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class AlterTableSalaryUserDataTds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('salary_user_data_tds', function ($table) {
            $table->dropColumn(['it_saving_id', 'tds_amount']);
            $table->integer('tds_for_the_month')->after('user_id')->nulllable();
            $table->integer('gross_tax_payable')->after('tds_for_the_month')->nulllable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('salary_user_data_tds', function ($table) {
            $table->dropColumn(['tds_for_the_month', 'gross_tax_payable']);
        });

    }
}
