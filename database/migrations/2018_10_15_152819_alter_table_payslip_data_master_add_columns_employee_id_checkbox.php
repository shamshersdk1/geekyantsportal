<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePayslipDataMasterAddColumnsEmployeeIdCheckbox extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payslip_data_master', function(Blueprint $table)
		{	
            $table->string('employee_id')->after('user_id');
            $table->boolean('salary_on_hold')->default(0)->after('payslip_data_month_id');
            $table->index('employee_id');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payslip_data_master', function (Blueprint $table) {
            $table->dropColumn(['employee_id', 'salary_on_hold']);
        });
    }
}
