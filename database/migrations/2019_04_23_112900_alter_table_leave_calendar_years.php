<?php

use Illuminate\Database\Migrations\Migration;

class AlterTableLeaveCalendarYears extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leave_calendar_years', function ($table) {
            $table->integer('leave_type_id')->after('calendar_year_id');
            $table->decimal('max_allowed_leave')->after('leave_type_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leave_calendar_years', function ($table) {
            $table->dropColumn(['leave_type_id', 'max_allowed_leave']);
        });
    }
}
