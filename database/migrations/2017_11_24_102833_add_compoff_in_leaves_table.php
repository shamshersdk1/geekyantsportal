<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompoffInLeavesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE leaves CHANGE COLUMN type type ENUM('paid', 'unpaid', 'sick', 'half', 'compoff')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE leaves CHANGE COLUMN type type ENUM('paid', 'unpaid', 'sick', 'half')");
    }
}
