<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGatewaysArpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gateway_arp', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('gateway_id')->index();
            $table->string('ip_address')->index();
            $table->string('hw_type');
            $table->string('flags');
            $table->string('hw_address')->index();
            $table->string('mask');
            $table->string('device');
            $table->integer('user_id')->index();
            $table->integer('asset_id')->index();
            $table->date('date')->index();
            $table->tinyInteger('hr');
            $table->tinyInteger('min');
            $table->index(['date','hr','min']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gateway_arp');
    }
}
