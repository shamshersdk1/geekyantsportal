<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProjectsTableAddEnumFreeStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $all=DB::table('projects')->get();

        Schema::table('projects', function (Blueprint $table) {
            $table->dropColumn('status');
        });

        Schema::table('projects', function (Blueprint $table) {
            $table->enum('status',['open', 'initial','sow','nda', 'assigned','in_progress','completed','closed','free'])->after('project_category_id');
        });

        foreach($all as $item)
        {
            DB::table('projects')->where('id',$item->id)->update(['status'=>$item->status]);
        }
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $all=DB::table('projects')->get();

        Schema::table('projects', function (Blueprint $table) {
            $table->dropColumn('status');
        });

        Schema::table('projects', function (Blueprint $table) {
            $table->enum('status',['open', 'initial','sow','nda', 'assigned','in_progress','completed','closed']);
        });

        foreach($all as $item)
        {
            DB::table('projects')->where('id',$item->id)->update(['status'=>$item->status]);
        }
    }
}
