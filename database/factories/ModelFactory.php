<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\Admin\Bonus;
use App\Models\Audited\Bonus\PrepBonus;
use App\Models\Audited\Salary\PrepSalary;
use Faker\Generator as Faker;
use Faker\Provider\DateTime;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
 */

$factory->define(App\Models\User::class, function (Faker $faker) {
    return [
        'id' => $faker->numberBetween($min = 1000, $max = 9000) ,
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'is_active' => 1,
        'role' => 'admin',
        'joining_date' => $faker->dateTime($max = 'now', $timezone = null),
        'employee_id' => 'b23',
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
    ];
});

$factory->define(App\Models\Appraisal\Appraisal::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return factory(App\Models\User::class)->create()->id;
        },
        'type_id' => function () {
            return factory(App\Models\Appraisal\AppraisalType::class)->create()->id;
        },
        'effective_date' => $faker->dateTime($max = 'now', $timezone = null),
    ];
});

$factory->define(App\Models\Appraisal\AppraisalType::class, function (Faker $faker) {
    return [
        'code' => $faker->word,
        'name' => $faker->name,
    ];
});

$factory->define(App\Models\Audited\Salary\PrepSalaryComponent::class, function (Faker $faker) {
    return [
        'prep_salary_id' => function () {
            return factory(App\Models\Audited\Salary\PrepSalary::class)->create()->id;
        },
        'prep_salary_component_type_id' => function () {
            return factory(App\Models\Audited\Salary\PrepSalaryComponentType::class)->create()->id;
        },
    ];
});

$factory->define(App\Models\Audited\Salary\PrepSalary::class, function (Faker $faker) {
    return [
        'month_id' => function () {
            return factory(App\Models\Month::class)->create()->id;
        },
        'status' => $faker->randomElement(['open' ,'closed']),
    ];
});

$factory->define(App\Models\Month::class, function (Faker $faker) {
    return [
        'financial_year_id' => function () {
            return factory(App\Models\Admin\FinancialYear::class)->create()->id;
        },
        'month_number' => $faker->month($max = 'now'),
        'month' => date('m'),
        'year' => $faker->dateTimeBetween($startDate = '-0 years', $endDate = 'now', $timezone = null,$format = 'Y'),
        'status' => $faker->word,
    ];
});

$factory->define(App\Models\Admin\FinancialYear::class, function (Faker $faker) {
    return [
        'year' => $faker->year($max = 'now', $min = $max - 1),
        'start_date' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'end_date' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'status' => $faker->randomElement(['pending', 'running', 'completed']),
        'standard_deduction' => $faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = NULL),
        'it_saving_template' => $faker->word,
    ];
});

$factory->define(App\Models\Audited\Salary\PrepSalaryComponentType::class, function (Faker $faker) {
    return [
        'code' => $faker->word,
        'name' => $faker->name,
        'service_class' => 'App\Services\SalaryService\UserSalaryComponent',
    ];
});

$factory->define(App\Models\Audited\Salary\PrepUser::class, function (Faker $faker) {
    return [
        'prep_salary_id' => function () {
            return factory(App\Models\Audited\Salary\PrepSalary::class)->create()->id;
        },
        'user_id' => function () {
            return factory(App\Models\User::class)->create()->id;
        },
    ];
});

$factory->define(App\Models\Admin\Leave::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return factory(App\Models\User::class)->create()->id;
        },
        'calendar_year_id' => function () {
            return factory(App\Models\Admin\CalendarYear::class)->create()->id;
        },
        'start_date' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'end_date' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'status' => $faker->randomElement(['pending', 'approved', 'rejected','cancelled']),
        'reason' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'approver_id' => function () {
            return factory(App\Models\User::class)->create()->id;
        },
        'leave_type_id' => $faker->numberBetween($min = 0, $max = 8),
    ];
});

$factory->define(App\Models\Admin\CalendarYear::class, function (Faker $faker) {
    return [
        'year' => $faker->year($max = 'now'),
    ];
});
